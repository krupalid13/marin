<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Services\UserService;
use App\Services\JobService;
use App\Services\SendEmailService;

class SendEmailCompanyAccordingJob implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    private $scheduleService;
    private $userService;
    private $sendEmailService;
    protected $data;

    public function __construct($data)
    {
        $this->userService = New UserService();
        $this->jobService = New JobService();
        $this->sendEmailService = new SendEmailService();
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->sendEmailService->sendCandidatesToCompany($this->data);
    }
}
