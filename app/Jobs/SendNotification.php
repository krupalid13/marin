<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Services\NotificationService;

class SendNotification implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    protected $user_id;
    protected $notification_id;
    protected $message;
    protected $notificationService;

    public function __construct($user_id,$notification_id,$message='')
    {
        $this->user_id = $user_id;
        $this->notification_id = $notification_id;
        $this->message = $message;
        $this->notificationService = new NotificationService();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        \Log::info('sending notification');
        
        $channel = $this->user_id.'_Notification';
        
	   \Log::info('sending notification job running');

        $data['notification_id'] = '';
        if (!empty($this->notification_id)) {
            $data['notification_id'] = $this->notification_id;
        }
        $this->notificationService->initPubnubAndPublish($channel,$data);
        \Log::info('notification send');
    }
}
