<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Services\SendEmailService;

class SendEmailSeafarerForFullPaymentJob implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    private $sendEmailService;
    protected $user;
    protected $batch;

    public function __construct($user,$batch)
    {   
        $this->user = $user;
        $this->batch = $batch;
        $this->sendEmailService = new SendEmailService();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->sendEmailService->sendFullPaymentNotification($this->user,$this->batch);
    }
}
