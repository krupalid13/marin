<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Services\SendEmailService;

class SendEmailExpireAvailabilityToSeafarer implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    private $sendEmailService;
    protected $data;
    protected $day;

    public function __construct($data,$day)
    {
        $this->sendEmailService = new SendEmailService();
        $this->data = $data;
        $this->day = $day;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->sendEmailService->sendExpireAvalailityEmailToSeafarer($this->data,$this->day);
    }
}
