<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Services\ScheduleService;
use App\Services\UserService;
use App\Services\SendEmailService;


class SendScheduledEmail implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    private $scheduleService;
    private $userService;
    private $sendEmailService;

    protected $scheduled_email;

    public function __construct($scheduled_email)
    {
        $this->scheduleService = New ScheduleService();
        $this->userService = New UserService();
        $this->sendEmailService = New SendEmailService();
        $this->scheduled_email = $scheduled_email;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    { 
        $cities = [];
        $country = 95;
        $users = [];
        $emailSubArr = [];

            if( !empty($this->scheduled_email['cities']) ) {
                $cities = json_decode($this->scheduled_email['cities']);
                $users = $this->userService->getUserByCities($cities);

            } elseif ( !empty($this->scheduled_email['country']) ) {
                $country = $this->scheduled_email['country'];
                $users = $this->userService->getUserByCountryId($country);
                
            }

            if (count($users) > 0) {
                $users = $users->toArray();
                foreach ($users as $index => $user) {
                    $recipientArr[$index]['address'] = [];
                    $recipientArr[$index]['address']['name'] = ucwords($user['first_name']);
                    $recipientArr[$index]['address']['email'] = $user['email'];
                    $recipientArr[$index]['substitution_data']['user_name'] = ucwords($user['first_name']);
                    $recipientArr[$index]['substitution_data']['message'] = $this->scheduled_email['message'];
                }

                $emailResponse = $this->sendEmailService->sendScheduleEmail($emailSubArr, $recipientArr,$this->scheduled_email['subject']);

                if ($emailResponse['status'] == 200) {
                    $shedule_update = $this->scheduleService->changeStatusTo($this->scheduled_email['id'],1);
                } else {
                    $shedule_update = $this->scheduleService->changeStatusTo($this->scheduled_email['id'],2,$emailResponse['message']);
                }
            }
    }
}
