<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Services\SubscriptionService;

use App\Services\SendEmailService;

class SendSubscriptionExpiryEmailJob implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $day;
    protected $subscriptionService;
    protected $sendEmailService;
    public function __construct($day)
    {
        $this->sendEmailService = new SendEmailService();
        $this->subscriptionService = new SubscriptionService();
        $this->day = $day;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $company_user = $this->subscriptionService->getCompanyUsersWhosSubscriptionExpires($this->day)->toArray();
        if( count($company_user) > 0 ) {
            $recipientArr = [];
            foreach ($company_user as $key => $data){

                $recipientArr[$key]['address'] = [];
                $recipientArr[$key]['substitution_data'] = [];
                    $recipientArr[$key]['address']['name'] = ucwords($data['company_registration']['user']['first_name']);
                    $recipientArr[$key]['address']['email'] = $data['company_registration']['user']['email'];
                    //$recipientArr[$key]['address']['email'] = 'shaikhmukhtar.09@gmail.com';

                    $recipientArr[$key]['substitution_data']['user_name'] = ucwords($data['company_registration']['user']['first_name']);

                    $recipientArr[$key]['substitution_data']['message'] = 'Your package is going to expire in '.$this->day.' days, to continue with your subscription please renew / upgrade the package by clicking below link or by visiting '.route('site.user.subscription').'?type=company page';

                    $recipientArr[$key]['substitution_data']['page_url'] = '<a href="'.route('site.user.subscription').'?type=company" style="width: 200px;background-color: #d43221;color: #fff;text-decoration: unset;padding: 10px;border-radius: 4px;box-shadow: 1px 2px 6px 1px #848484;">Renew / Upgrade Subscription</a>';

            }
             
            $subject = 'Consultanseas:Subscription expire in '.$this->day.' days';
            $this->sendEmailService->sendSubscriptionExpiryEmailService($recipientArr,$subject);
        }

        $institute_user = $this->subscriptionService->getInstituteUsersWhosSubscriptionExpires($this->day)->toArray();

        if( count($institute_user) > 0 ) {
            $recipientArr = [];
            foreach ($institute_user as $key => $data){

                $recipientArr[$key]['address'] = [];
                $recipientArr[$key]['substitution_data'] = [];
                    $recipientArr[$key]['address']['name'] = ucwords($data['institute_registration']['user']['first_name']);
                    $recipientArr[$key]['address']['email'] = $data['institute_registration']['user']['email'];
                    //$recipientArr[$key]['address']['email'] = 'shaikhmukhtar.09@gmail.com';

                    $recipientArr[$key]['substitution_data']['user_name'] = ucwords($data['institute_registration']['user']['first_name']);

                    $recipientArr[$key]['substitution_data']['message'] = 'Your subscription is going to expire after '.$this->day.' days, to continue with your subscription please renew / upgrade the subscription by clicking below link or by visiting '.route('site.user.subscription').'?type=institute page';

                    $recipientArr[$key]['substitution_data']['page_url'] = '<a href="'.route('site.user.subscription').'?type=institute" style="width: 200px;background-color: #d43221;color: #fff;text-decoration: unset;padding: 10px;border-radius: 4px;box-shadow: 1px 2px 6px 1px #848484;">Renew / Upgrade Subscription</a>';

            }
             
            $subject = 'Consultanseas:Subscription expire in '.$this->day.' days';
            $this->sendEmailService->sendSubscriptionExpiryEmailService($recipientArr,$subject);
        }

        $advertiser_user = $this->subscriptionService->getAdvertiserUsersWhosSubscriptionExpires($this->day)->toArray();
        if( count($advertiser_user) > 0 ) {
            $recipientArr = [];
            foreach ($advertiser_user as $key => $data){

                $recipientArr[$key]['address'] = [];
                $recipientArr[$key]['substitution_data'] = [];
                    $recipientArr[$key]['address']['name'] = ucwords($data['company_registration']['user']['first_name']);
                    $recipientArr[$key]['address']['email'] = $data['company_registration']['user']['email'];
                    //$recipientArr[$key]['address']['email'] = 'shaikhmukhtar.09@gmail.com';

                    $recipientArr[$key]['substitution_data']['user_name'] = ucwords($data['company_registration']['user']['first_name']);

                    $recipientArr[$key]['substitution_data']['message'] = 'Your subscription is going to expire after '.$this->day.' days, to continue with your subscription please renew / upgrade the subscription by clicking below link or by visiting '.route('site.user.subscription').'?type=advertiser page';

                    $recipientArr[$key]['substitution_data']['page_url'] = '<a href="'.route('site.user.subscription').'?type=advertiser" style="width: 200px;background-color: #d43221;color: #fff;text-decoration: unset;padding: 10px;border-radius: 4px;box-shadow: 1px 2px 6px 1px #848484;">Renew / Upgrade Subscription</a>';

            }
             
            $subject = 'Consultanseas:Subscription expire in '.$this->day.' days';
            $this->sendEmailService->sendSubscriptionExpiryEmailService($recipientArr,$subject);
        }
    }
}
