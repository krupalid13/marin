<?php

namespace App\Jobs;

use App\Mail\ShareDocs;
use App\Mail\ShareResume;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

class SendEmailShareDocs implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    protected $data;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $mail=Mail::to($this->data['email'], $this->data['token'], $this->data['valid_time']);

        if (isset($this->data['resume_path']))
            $mail->send(
                new ShareResume( $this->data )
            );
        else
            $mail->send(
                new ShareDocs($this->data)
            );
    }
}
