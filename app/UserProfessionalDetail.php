<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserProfessionalDetail extends Model
{
    protected $table = 'user_professional_details';

    protected $fillable = ['user_id', 'current_rank', 'current_rank_exp', 'applied_rank', 'availability', 'last_salary','name_of_school','school_from','school_to','school_qualification','institute_name','institute_from','institute_to','institute_degree','salary','other_exp','about_me'];

    public function user_details(){
        return $this->hasOne('App\User','id','user_id');
    }

    public function user_personal_details(){
        return $this->hasOne('App\UserPersonalDetail','id','user_id');
    }
}
