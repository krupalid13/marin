<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserAppliedJob extends Model
{
    protected $table = 'user_applied_jobs';

    protected $fillable = ['user_id', 'company_id', 'job_id', 'message'];
}
