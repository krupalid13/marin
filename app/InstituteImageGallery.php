<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InstituteImageGallery extends Model
{
    protected $table = 'institute_image_galleries';

    protected $fillable = ['institute_id','image','status'];
}
