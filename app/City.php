<?php namespace App;
use Illuminate\Database\Eloquent\Model;

class City extends Model{

	protected $table = 'cities';

	protected $fillable = ['name'];

	public function pincode_cities(){
		return $this->hasMany('App\PincodeCity');
	}

	public function products()
    {
        return $this->belongsToMany('App\Product','product_shipping_area');
    }

    public function state()
    {
        return $this->belongsTo('App\State');
    }
	
}

?>