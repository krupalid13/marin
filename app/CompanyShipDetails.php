<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyShipDetails extends Model
{
    protected $table = 'company_ship_details';

    protected $fillable = ['company_id', 'ship_type', 'ship_flag', 'grt', 'engine_type', 'bhp', 'voyage', 'built_year', 'p_i_cover', 'p_i_cover_company_name','ship_name','scope_type'];

    /*public function company_details(){
    	return $this->hasOne('App\CompanyDetail','company_id', 'company_id');
    }*/

    public function company_details(){
    	return $this->belongsTo('App\CompanyDetail','company_id', 'company_id');
    }
}
