<?php

namespace App\Exports;

use Carbon\Carbon;
use App\Attendence;
use App\Employee;
use Auth;
use DB;
use Illuminate\Contracts\View\View;
use App\Affiliate;

use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\BeforeExport;
use Maatwebsite\Excel\Events\AfterSheet;


class AffiliateUsersExport  {


	public function request($request) {          	
		$this->data = $request;
	}
    
	public function registerEvents(): array {
		return [
			AfterSheet::class => function (AfterSheet $event) {
				$event->sheet->getStyle('A1:H1')->applyFromArray([
					'font' => [
						'bold' => true
					],
				]);
							
				$event->sheet->getStyle('A1')->getAlignment()->applyFromArray(
					array('horizontal' => 'center')
				);
							
				$event->sheet->getDelegate()->mergeCells('A1:H1');
				$event->sheet->getStyle('A2:H2')->applyFromArray([
					'font' => [
						'bold' => true
					]
				]);
			},
		];
	}
   
	public function view(): View {
		return view('admin.affiliate.exportusers', [
			'obj' => $this->query()
		]);
	}
	
	public function query() {
		return Affiliate::get();
	}
}