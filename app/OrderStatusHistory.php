<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderStatusHistory extends Model
{
    protected $table = 'order_status_history';

    protected $fillable = ['order_id','user_id','from_status','to_status','reason'];

    public function user(){
        return $this->hasOne('App\User','id','user_id');
    }

}
