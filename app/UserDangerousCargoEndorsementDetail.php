<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use App\UserDocumentsType;
use App\UserDocuments;
use File;
use App\User;
class UserDangerousCargoEndorsementDetail extends Model
{
    public function store($data) {

        $userId = Auth::user()->id;

        // dd($data);
        $userDangerousCargoEndorement = new UserDangerousCargoEndorsementDetail;
        $checkData = UserDangerousCargoEndorsementDetail::whereUserId($userId)->first();
        if($checkData != null){

            $userDangerousCargoEndorement = $checkData;
        }

        $userDangerousCargoEndorement->user_id = $userId;

        $status['status'] = !empty($data['dce_radio']) ? $data['dce_radio'] : null;
        $status['type'] = !empty($data['dce']) ? $data['dce'] : null;

        $userDangerousCargoEndorement->status = json_encode($status);
        $userDangerousCargoEndorement->oil = json_encode($data['oil']);
        $userDangerousCargoEndorement->chemical = json_encode($data['chemical']);
        $userDangerousCargoEndorement->lequefied_gas = json_encode($data['lequefied_gas']);
        $userDangerousCargoEndorement->all = json_encode($data['all']);
        $userDangerousCargoEndorement->save();

    }

    public static function destroy($userId) {
        return UserDangerousCargoEndorsementDetail::whereUserId($userId)->delete();
    }
}
