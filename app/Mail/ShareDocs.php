<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ShareDocs extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->subject = $data['subject'];
        $this->valid_time = $data['valid_time'];
        $this->name = $data['name'];
        $this->token = $data['token'];
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $data = $this->data;
        return $this->from(env('MAIL_FROM_ADDRESS','donot-reply@flanknot.com'), $this->name . ' - ' . env('APP_NAME','FLANKNOT'))
            ->view('emails.sharing.share_docs', compact('data'));
    }
}
