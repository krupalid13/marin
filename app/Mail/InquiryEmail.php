<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Mail;

class InquiryEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {        
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $data = $this->data;
        return Mail::send([], [], function ($message) use ($data) {
                $message->from(env('MAIL_FROM_ADDRESS','donot-reply@flanknot.com'));
                foreach($data['users'] as $user){
                        $message->to($user);
                }                               
                $message->subject($data['subject']);
                $message->setBody($data['template'], 'text/html');
            });
    }
}
