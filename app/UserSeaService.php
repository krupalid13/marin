<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserSeaService extends Model
{
    protected $table = 'user_sea_services';

    protected $fillable = ['user_id', 'rank_id', 'company_name', 'ship_name', 'ship_flag', 'ship_type', 'grt', 'nrt', 'engine_type', 'bhp', 'from', 'to', 'total_duration','course_type','other_engine_type', 'manning_by'];

    public function getRateData(){
        return $this->hasOne('App\RateReview','user_sea_service_id','id');
    }
}
