<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\UserVisaDetail;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'gender', 'otp', 'email', 'password', 'mobile', 'status', 'is_mob_verified', 'is_email_verified','welcome_email','welcome_sms','registered_as','added_by','updated_on','last_logged_on', 'country_code','affiliate_referral_code','user_affiliate_code'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

	public function languagesknown(){

        return $this->hasMany('App\UserLanguage','user_id','id');
    }

    public function visas(){

        return $this->hasMany('App\UserVisaDetail','user_id','id');
    }


    public function sendedJob(){

        return $this->hasMany('App\SendedJob','user_id','id');
    }

    public function sendedLastJob(){

        return $this->belongsTo('App\SendedJob','sended_jobs_id','id');
    }

    public function shareHistory(){

        return $this->belongsTo('App\ShareHistory','sended_jobs_id','sended_job_id');
    }

    public function courseJob(){
        return $this->belongsTo('App\CourseJob','last_job_id','id');
    }

    public function personal_detail(){
        return $this->hasOne('App\UserPersonalDetail');
    }

    public function professional_detail(){
        return $this->hasOne('App\UserProfessionalDetail');
    }

    public function passport_detail(){
        return $this->hasOne('App\UserPassportDetail');
    }

    public function seaman_book_detail(){
        return $this->hasMany('App\UserSemanBookDetail')->orderBy('id','asc');
    }

    public function coc_detail(){
        return $this->hasMany('App\UserCocDetail')->orderBy('id','asc');
    }

    public function gmdss_detail(){
        return $this->hasOne('App\UserGmdssDetail');
    }

    public function wkfr_detail(){
        return $this->hasOne('App\UserWkfrDetail');
    }

    public function course_detail(){
        return $this->hasMany('App\UserCoursesCertificate')->orderBy('id','desc');
    }

    public function sea_service_detail(){
        return $this->hasMany('App\UserSeaService')->orderBy('from','desc');
    }

    public function enginType(){
        return $this->hasMany('App\EnginType');
    }

    public function cop_detail(){
        return $this->hasMany('App\UserCopDetail');
    }

    public function company_registration_detail(){
        return $this->hasOne('App\CompanyRegistration');
    }

    public function institute_registration_detail(){
        return $this->hasOne('App\InstituteRegistration');
    }

    public function user_applied_jobs(){
        return $this->hasMany('App\JobApplication','user_id','id');
    }

    public function coe_detail(){
        return $this->hasMany('App\UserCoeDetails')->orderBy('id','asc');
    }

    public function advertisment_locations(){
        return $this->hasOne('App\AdvertisementsLocations','company_id','id');
    }

    public function company_subscriptions(){
        return $this->hasMany('app\UserSubscription');
    }

    public function user_documents_type(){
        return $this->hasMany('App\UserDocumentsType','user_id','id');
    }

    public function user_documents_type_with_document(){
        return $this->hasMany('App\UserDocumentsType','user_id','id')->with('user_documents');
    }

    public function document_permissions(){
        return $this->hasMany('App\DocumentPermissions','owner_id','id')->orderBy('id','desc');
    }

    public function shared_docs_to_others(){
        return $this->hasMany('App\SharedDocsToOthers');
    }

    public function userDangerousCargoEndorsementDetail(){
        return $this->hasOne('App\UserDangerousCargoEndorsementDetail', 'user_id', 'id');
    }

}
