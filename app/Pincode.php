<?php 
namespace App;
use Illuminate\Database\Eloquent\Model;

class Pincode extends Model{

	protected $table = 'pincodes';

	protected $fillable = ['code'];

	public function pincodes_states(){
		return $this->hasMany('App\PincodeState');
	}

	public function pincodes_cities(){
		return $this->hasMany('App\PincodeCity');
	}

	public function pincodes_areas(){
		return $this->hasMany('App\Area','pincode_id');
	}

	
}

?>