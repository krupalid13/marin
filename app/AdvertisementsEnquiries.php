<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdvertisementsEnquiries extends Model
{
    protected $table = 'advertisements_enquiries';

    protected $fillable = ['advertisement_id','name','phone','email','message','user_id','company_id'];

    public function advertisement_details(){
        return $this->hasOne('App\AdvertiseDetails','id','advertisement_id');
    }
}
