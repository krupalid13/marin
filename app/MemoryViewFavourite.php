<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MemoryViewFavourite extends Model
{
    protected $table = 'memory_views_favourites';
}
