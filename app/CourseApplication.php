<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CourseApplication extends Model
{
    protected $table = 'course_applications';

    protected $fillable = ['course_id', 'institute_id', 'user_id'];

    public function user(){
    	return $this->belongsTo('App\User');
    }

    public function institute_details(){
    	return $this->hasOne('App\InstituteRegistration','id', 'institute_id');
    }

    public function batch_details(){
        return $this->hasOne('App\InstituteBatches','id', 'course_id');
    }

    public function courses(){
    	return $this->hasOne('App\Courses','id', 'course_id');
    }
}
