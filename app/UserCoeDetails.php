<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserCoeDetails extends Model
{
    protected $table = 'user_coe_details';

    protected $fillable = ['user_id', 'coe', 'coe_number', 'coe_grade', 'coe_expiry_date','coe_verification_date','sequence'];
}
