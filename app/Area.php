<?php namespace App;
use Illuminate\Database\Eloquent\Model;

class Area extends Model{

	protected $table = 'areas';

	protected $fillable = ['name','pincode_id'];

	public function pincode() {
		return $this->belongsTo('App\Pincode');
	}
	
}

?>