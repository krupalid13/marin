<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShareContacts extends Model
{
    //
    protected $table = 'share_contacts';

    protected $fillable = ['user_id', 'name', 'group', 'p_name', 'email', 'mobile_no', 'alternate_no', 'title', 'job_title', 'mobile_code', 'type'];
}
