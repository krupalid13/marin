<?php
namespace App\Validator;
use Illuminate\Validation\Validator;
use Illuminate\Support\Facades\Input;
use Hash;
class CustomeValidator extends Validator
{
	/**
	 * [validatecheckTagTitleExit To check tag name alredy used or not]
	 * @param  [type] $attribute  [description]
	 * @param  [type] $value      [description]
	 * @param  [type] $parameters [description]
	 * @return [type]             [description]
	 */
	public function validateCheckCompanyDetailUniqueName($attribute, $value, $parameters)
	{	

		if (isset($parameters[0]) && !empty($parameters[0])) {

            $count = \App\Company::where("id", "!=",$parameters[0])
                ->where("name", $value)
                ->count();

        } else {

            $count = \App\Company::where("name", $value)->count();
        }

        if ($count === 0) {

            return true;

        } else {

            return false;
        }
	}

	public function validateCheckCompanyDetailUniqueEmail($attribute, $value, $parameters)
	{	

		if (isset($parameters[0]) && !empty($parameters[0])) {

            $count = \App\Company::where("id", "!=",$parameters[0])
                ->where("email", $value)
                ->count();

        } else {

            $count = \App\Company::where("email",$value)->count();
        }

        if ($count === 0) {

            return true;

        } else {

            return false;
        }
	}	

}
?>