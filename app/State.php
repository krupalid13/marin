<?php namespace App;
use Illuminate\Database\Eloquent\Model;

class State extends Model{

	protected $table = 'states';

	protected $fillable = ['name'];

	public function pincode_states(){
		return $this->hasMany('App\PincodeState');
	}

	public function products()
    {
        return $this->belongsToMany('App\Product','product_shipping_area');
    }

    public function cities()
    {
        return $this->hasMany('App\City','state_id');
    }
	
}

?>