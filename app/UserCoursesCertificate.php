<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserCoursesCertificate extends Model
{
    protected $table = 'user_courses_certificates';

    protected $fillable = ['user_id', 'course_id', 'issue_date', 'issue_by', 'issuing_authority', 'certification_number', 'place_of_issue','course_type','expiry_date'];

    public function user(){
        return $this->hasOne('App\User','id', 'user_id');
    }

    public function user_personal_detail(){
        return $this->hasOne('App\UserPersonalDetail','user_id', 'user_id');
    }

    public function professional_detail(){
        return $this->hasOne('App\UserProfessionalDetail','user_id', 'user_id');
    }

    public function coursess_data(){
        return $this->hasOne('App\Courses','id', 'course_id');
    }
}
