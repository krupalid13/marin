<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InstituteNotice extends Model
{
    protected $table = 'institute_notices';

    protected $fillable = ['institute_id','start_date','end_date','notice'];

}
