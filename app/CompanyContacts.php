<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Yajra\Datatables\Datatables;
use Crypt;
use App\CourseJob;

class CompanyContacts extends Model
{
    protected $table = 'company_contacts';

    public function deleteCompanyContact($id){        	
       
        $obj = $this->findOrFail(Crypt::decrypt($id));
        $obj->delete();
        $status = 1;
    	$msg = "Company Contact number successfully deleted.";      


        return response()->json(['success'=> $status, 'msg' => $msg]);
    }
}
