<?php namespace App;
use Illuminate\Database\Eloquent\Model;

class PincodeCity extends Model{

	protected $table = 'pincodes_cities';

	protected $fillable = ['pincode_id','city_id'];

	public function pincode(){
		return $this->belongsTo('App\Pincode');
	}

	public function city(){
		return $this->belongsTo('App\City');
	}
	
}

?>