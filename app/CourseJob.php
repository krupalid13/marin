<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Yajra\Datatables\Datatables;
use Crypt;
use App\CompanyEmailAddress;
use App\Company;
use App\CourseJobDce;
use App\JobMultipleCourse;
use App\JobMultipleShipType;
use App\JobMultipleEngineType;
use App\JobStatus;
use App\Vaccination;
use App\MultiplePassportCountry;
use App\User;
use Carbon\Carbon;
use App\JobMultipleDpCountry;
use App\JobMultipleCoeCountry;
use App\JobMultipleCopCountry;
use App\JobMultipleCocCountry;
use App\JobMultipleGmdssCountry;
use App\sendedJob;
use Illuminate\Database\Eloquent\SoftDeletes;

class CourseJob extends Model
{
    use SoftDeletes;

    protected $table = 'course_jobs';
    protected $softDelete = true;

    public function company(){

        return $this->belongsTo('App\Company','company_id','id');
    }

    public function companyEmailAddress(){

        return $this->belongsTo('App\CompanyEmailAddress','company_email_address_id','id');
    }

    public function jobStatus(){

        return $this->belongsTo('App\JobStatus','job_status_id','id');
    }

    public function getVaccination(){

        return $this->belongsTo('App\Vaccination','vaccination','id');
    }

    public function courseJobDce(){

        return $this->hasMany('App\CourseJobDce','course_job_id','id');
    }

    public function jobMultipleCourse(){

        return $this->hasMany('App\JobMultipleCourse','course_job_id','id');
    }

    public function jobMultipleShipType(){

        return $this->hasMany('App\JobMultipleShipType','course_job_id','id');
    }

    public function jobMultipleEngineType(){

        return $this->hasMany('App\JobMultipleEngineType','course_job_id','id');
    }

    public function jobMultiplePassportCountry(){

        return $this->hasMany('App\MultiplePassportCountry','course_job_id','id');
    }

    public function jobMultipleDpCountry(){

        return $this->hasMany('App\JobMultipleDpCountry','course_job_id','id');
    }

    public function jobMultipleCocCountry(){

        return $this->hasMany('App\JobMultipleCocCountry','course_job_id','id');
    }

    public function jobMultipleCoeCountry(){

        return $this->hasMany('App\JobMultipleCoeCountry','course_job_id','id');
    }

    public function jobMultipleCopCountry(){

        return $this->hasMany('App\JobMultipleCopCountry','course_job_id','id');
    }

    public function jobMultipleGmdssCountry(){

        return $this->hasMany('App\JobMultipleGmdssCountry','course_job_id','id');
    }

    public function deleteCompanyJobContact($id){          
       
        $obj = $this->findOrFail(Crypt::decrypt($id));
        $obj->delete();
        $status = 1;
        $msg = "Company Job successfully deleted.";      


        return response()->json(['success'=> $status, 'msg' => $msg]);
    }

    /**
     * get Company List Datatable function
     * @author Chirag G
     * @return void
     * @author
     **/
    public function get($request)
    {
    	if ($request->method() == 'GET') {

        	$sql = self::select("*")->with(['company'])->where('deleted_at',null);
            
            return Datatables::of($sql)
	                ->editColumn('id', function ($data) {

                        $string = '<a class="btn btn-xs btn-primary" href="' . route('admin.j.edit',\Crypt::encrypt($data->id)) . '"><i class="fa fa-pencil-square-o" aria-hidden="true" title="Edit Job"></i></a> <a class="btn btn-xs btn-danger delete_record" data-route="' . route('admin.j.delete',\Crypt::encrypt($data->id)) . '"><i class="fa fa-trash" aria-hidden="true" title="Delete Job"></i></a>';
                    
                        return $string;

                    })
                    ->editColumn('created_at', function ($data) {

                        return ($data->company !=null) ? $data->company->name : '-' ;

                    })                         
                    ->editColumn('unique_code', function ($data) {

                        return ($data->unique_code !=null) ? $data->unique_code : '-' ;

                    })                    
                    ->editColumn('rank_id', function ($data) {
                        
                        if (empty($data->rank_id)) {

                            return '-';

                        }else{

                            return getRankCommonFuncion($data->rank_id);
                        }

                    })
                    ->make(true);
            
        }
    }    

    /**
     * get Company List Datatable function
     * @author Chirag G
     * @return void
     * @author
     **/
    public function sendGet($request)
    {
         // dd($request->where_in);
        $input = $request->all();

        if ($request->method() == 'GET'){

            $sql = \App\User::select('*')->with(['sendedJob','sendedLastJob','courseJob','professional_detail','wkfr_detail','passport_detail','seaman_book_detail','coc_detail','coe_detail','gmdss_detail','sea_service_detail','enginType','shareHistory','shareHistory.history_log_latest'])
                    ->where('registered_as','seafarer');


            if (isset($input['earlier']) && $input['earlier'] != "") {
                    
            }else{

                $sql->whereDoesntHave('sendedJob',function($query) use($input){

                    $query->where('course_job_id',$input['course_job_id']);    
                });
            }        


            // Ship Type
            if (isset($input['experience_ship_type']) && !empty($input['experience_ship_type'])) {
                    
                $sql->whereHas('sea_service_detail',function($query) use ($request) {

                    $query->whereIn('ship_type',$request->experience_ship_type);

                });
            }

            // Engine Type
            if (isset($input['experience_engine_type']) && !empty($input['experience_engine_type'])) {
                
                $sql->whereHas('sea_service_detail',function($query) use ($request) {

                    $query->whereIn('engine_type',$request->experience_engine_type)
                            ->orWhereIn('other_engine_type',$request->experience_engine_type);

                })->orWhereHas('enginType',function($query) use($request){

                    $query->whereIn('id',$request->experience_engine_type);                        
                });
            }

            // watch_keeping // cop // wkfr_detail

            if (isset($input['gmdss']) && !empty($input['gmdss'])) {
                
                $sql->whereHas('gmdss_detail',function($query) use ($request) {

                    $query->whereIn('gmdss',$request->gmdss);

                });
            }

            if (isset($input['watch_keeping']) && !empty($input['watch_keeping'])) {
                
                $sql->whereHas('wkfr_detail',function($query) use ($request) {

                    $query->whereIn('watch_keeping',$request->watch_keeping);

                });
            }

            if (isset($input['coe']) && !empty($input['coe'])) {
                
                $sql->whereHas('coe_detail',function($query) use ($request) {

                    $query->whereIn('coe',$request->coe);

                });
            }

            if (isset($input['coc']) && !empty($input['coc'])) {
                
                $sql->whereHas('coc_detail',function($query) use ($request) {

                    $query->whereIn('coc',$request->coc);

                });
            }

            if (isset($input['pass_country']) && !empty($input['pass_country'])) {
                
                $sql->whereHas('passport_detail',function($query) use ($request) {

                    $query->whereIn('pass_country',$request->pass_country);

                });
            }

            if (isset($input['cdc']) && !empty($input['cdc'])) {
                
                $sql->whereHas('seaman_book_detail',function($query) use ($request) {

                    $query->whereIn('cdc',$request->cdc);

                });
            }

            if (isset($input['current_rank']) && !empty($input['current_rank'])) {
                
                $sql->whereHas('professional_detail',function($query) use ($request) {

                    $query->where('current_rank',$request->current_rank);

                });
            } 

            if (isset($input['character_reference']) && ($input['character_reference'] == 1 || $input['character_reference'] == 0) ) {
                
                $sql->whereHas('wkfr_detail',function($query) use ($request) {

                    $query->where('character_reference',$request->character_reference);

                });
            }

            if (isset($input['fromo']) && ($input['fromo'] == 1 || $input['fromo'] == 0) ) {
                
                $sql->whereHas('passport_detail',function($query) use ($request) {

                    $query->where('fromo',$request->fromo);

                });
            }
            
            if (isset($input['wk_cop']) && ($input['wk_cop'] == 1 || $input['wk_cop'] == 0) ) {
                
                $sql->whereHas('wkfr_detail',function($query) use ($request) {

                    $query->where('wk_cop',$request->wk_cop);

                });
            }

            if (isset($input['rank_experience_year'])  && !empty($input['rank_experience_year']) && isset($input['rank_experience_month']) && !empty($input['rank_experience_month'])) {

                $sql->whereHas('professional_detail',function($query) use ($request) {

                    $yearMonth = $request->rank_experience_year.'.'.$request->rank_experience_month;

                    $query->where('current_rank_exp','>=',$yearMonth);

                });

            }elseif (isset($input['rank_experience_year']) && !empty($input['rank_experience_year'])) {
                

                $sql->whereHas('professional_detail',function($query) use ($request) {

                    $year = $request->rank_experience_year.'.00';
                    $query->where('current_rank_exp','>=',$year);

                });    

            }elseif (isset($input['rank_experience_month']) && !empty($input['rank_experience_month'])) {
                    
                $sql->whereHas('professional_detail',function($query) use ($request) {

                    $month = '00.'.$request->rank_experience_month;
                    $query->where('current_rank_exp','>=',$month);

                });
            }

            if (isset($input['minimum_wages']) && $input['minimum_wages'] !='' && isset($input['maximum_wages']) && $input['maximum_wages'] !="") {

                $sql->whereHas('professional_detail',function($query) use ($request) {
                        
                    $query->whereBetween('last_salary',
                                        [(int)$request->minimum_wages,(int)$request->maximum_wages]);

                });

            }elseif (isset($input['minimum_wages']) && $input['minimum_wages'] !="") {
                    

                $sql->whereHas('professional_detail',function($query) use ($request) {

                    $query->where('last_salary','>=',(int)$request->minimum_wages);

                });    

            }elseif (isset($input['maximum_wages']) && $input['maximum_wages'] !="") {
                    
                $sql->whereHas('professional_detail',function($query) use ($request) {

                    $query->where('last_salary','<=',(int)$request->maximum_wages);

                });
            }


            if (isset($input['currency']) && !empty($input['currency'])) {
                
                $sql->whereHas('professional_detail',function($query) use ($request) {

                    $query->where('currency',$request->currency);

                });
            }

            if (isset($input['availability']) && !empty($input['availability'])) {
                
                $sql->whereHas('professional_detail',function($query) use ($request) {

                    $query->where('availability','<=',$request->availability);

                });
            }

            if (isset($input['where_in_home']) && $input['where_in_home'] == 'Home' && isset($input['where_in_ship']) && $input['where_in_ship'] == 'Ship') {
                    
                $sql->whereIn('where_in',['Home','Ship']);
                    
            }elseif (isset($input['where_in_home']) && $input['where_in_home'] == 'Home') {
                
                 $sql->where('where_in','Home');

            }elseif (isset($input['where_in_ship']) && $input['where_in_ship'] == 'Ship') {
                    
                $sql->where("where_in",'Ship');
            }

            return Datatables::of($sql)
                    
                    ->editColumn('id', function ($data) use($input){

                        if (!$data->sendedJob->isEmpty()) {
                            
                            $jobArray = $data->sendedJob->pluck('course_job_id')->toArray();

                            if (in_array($input['course_job_id'], $jobArray)) {
                                
                                return '';

                            }else{

                                return '<input type="checkbox" name="checkbox[]" class="select_checkbox_value" value="'.Crypt::encrypt($data->id).'" />';                                
                            }

                        }else{

                            return '<input type="checkbox" name="checkbox[]" class="select_checkbox_value" value="'.Crypt::encrypt($data->id).'" />';
                        }

                    })
                    ->editColumn('shareHistory.latest_qr', function ($data) {

                        return ($data->shareHistory !=null && !empty($data->shareHistory->latest_qr)) ? date('d M Y',strtotime($data->shareHistory->latest_qr)) : '-' ;

                    })
                    ->editColumn('shareHistory.history_log_latest.description', function ($data) {

                        if ($data->shareHistory !=null) {

                            if ($data->shareHistory->history_log_latest !=null && $data->shareHistory->history_log_latest->description) {
                                    
                                return $data->shareHistory->history_log_latest->description;

                            }else{

                                return '-';
                            }

                        }else{

                            return '-';
                        }   

                    })
                    ->editColumn('courseJob.unique_code', function ($data) {
                            
                        return (!empty($data->courseJob)) ? $data->courseJob->unique_code : '-';
                    })
                    ->editColumn('first_name', function ($data) {

                        return (!empty($data->first_name)) ? $data->first_name : '-';
                    })
                    ->editColumn('last_name', function ($data) {

                        return (!empty($data->last_name)) ? $data->last_name : '-';
                    })
                    ->editColumn('last_job_post_date', function ($data) {

                        return !empty($data->last_job_post_date) ? date('d-M-y',strtotime($data->last_job_post_date)) : '-';
                    })
                    ->editColumn('sended_jobs_id', function ($data) {

                        if ($data->sendedLastJob !=null) {

                            if ($data->sendedLastJob->applied_date !=null) {
                                
                                return date('d-M-y',strtotime($data->sendedLastJob->applied_date));          
                            }else{

                                return '-';
                            }

                        }else{

                            return '-';
                        } 
                    })
                    ->editColumn('qr_date', function ($data) {

                        return '-';
                    })
                    ->editColumn('qr', function ($data) {

                        return '-';
                    })
                    ->editColumn('view_resume', function ($data) {

                        return '<a href="javascript:void(0)" />View</a>';
                    })
                    ->editColumn('view_profile', function ($data) {

                        return '<a href="javascript:void(0)" />View</a>';
                    })
                    ->editColumn('mobile', function ($data) {

                        return $data->id;
                    })                    
                    ->editColumn('professional_detail.current_rank', function ($data) {

                        $rank ='-'; 
                        if ($data->professional_detail !=null) {
                                
                            if ($data->professional_detail->current_rank !=null) {
                                            
                                $rank = getRankCommonFuncion($data->professional_detail->current_rank);
                            }        

                        }

                        return $rank;
                    })
                    ->editColumn('professional_detail.current_rank_exp', function ($data) {

                        $exp ='0.00'; 
                        if ($data->professional_detail !=null) {
                                
                            if ($data->professional_detail->current_rank_exp !=null) {
                                            
                                $exp = $data->professional_detail->current_rank_exp;
                            }        

                        }

                        return $exp;
                    })                    
                    ->editColumn('professional_detail.last_salary', function ($data) {

                        $wages =''; 
                        if ($data->professional_detail !=null) {
                                
                            if ($data->professional_detail->currency !=null) {
                                
                                if ($data->professional_detail->currency == 'rupees') {
                                    
                                    $wages .="₹";

                                }elseif ($data->professional_detail->currency == 'dollar') {
                                    
                                    $wages .="$";
                                }
                            }

                            if ($data->professional_detail->last_salary !=null) {
                                            
                                $wages .=$data->professional_detail->last_salary;
                            }        

                        }


                        return $wages;
                    })
                    ->editColumn('action', function ($data) {

                        $string = '<a class="btn btn-xs btn-primary" href="' . route('admin.editCompany',Crypt::encrypt($data->id)) . '"><i class="fa fa-pencil-square-o" aria-hidden="true" title="Edit company"></i></a> <a href="javascript:void(0)" data-route="'.route('admin.deleteCompanyDetails',Crypt::encrypt($data->id)).'" class="btn btn-xs btn-danger delete_record"><i class="fa fa-trash" aria-hidden="true"></i></a>';
                    
                        return $string;

                    })
                    ->setRowData(['mobile','id','action','first_name','last_name','professional_detail.current_rank','professional_detail.current_rank_exp','professional_detail.last_salary','courseJob.unique_code','sended_jobs_id','last_job_post_date','qr_date','qr','view_resume','view_profile'])
                    ->make(true);
            
        }
    }
    /**
     * get Company List Datatable function
     * @author Chirag G
     * @return void
     * @author
     **/
    public function getViewAllJob($request)
    {
        $input = $request->all();

        if ($request->method() == 'GET') {

            $sql = \App\SendedJob::select('*')
                                ->with([
                                        'courseJob',
                                        'courseJob.jobStatus',
                                        'courseJob.company',
                                        'user',
                                        'shareData',
                                        'shareData.history_log_latest'
                                    ])
                                ->has('courseJob')
                                ->has('user');

            if (isset($input['job_post_to']) && !empty($input['job_post_to']) && isset($input['job_post_from']) && !empty($input['job_post_from'])) {
                                              
                $sql->whereDate('created_at','>=',$input['job_post_to'])
                    ->whereDate('created_at','<=',$input['job_post_from']);                              

            }elseif (isset($input['job_post_to']) && !empty($input['job_post_to'])) {
                
                $sql->whereDate('created_at','>=',$input['job_post_to']);                              

            }elseif (isset($input['job_post_from']) && !empty($input['job_post_from'])) {
                    
                $sql->whereDate('created_at','<=',$input['job_post_from']);                              
            }

            if (isset($input['application_to']) && !empty($input['application_to']) && isset($input['application_from']) && !empty($input['application_from'])) {
                                              
                $sql->whereDate('applied_date','>=',$input['application_to'])
                    ->whereDate('applied_date','<=',$input['application_from']);                              

            }elseif (isset($input['application_to']) && !empty($input['application_to'])) {
                
                $sql->whereDate('applied_date','>=',$input['application_to']);                              

            }elseif (isset($input['application_from']) && !empty($input['application_from'])) {
                    
                $sql->whereDate('applied_date','<=',$input['application_from']);                              
            }

            if (isset($input['job_created_to']) && !empty($input['job_created_to']) && isset($input['job_created_from']) && !empty($input['job_created_from'])) {
                

                $sql->whereHas('courseJob',function($query) use($input){

                    $query->whereDate('created_at','>=',$input['job_created_to'])
                        ->whereDate('created_at','<=',$input['job_created_from']);                              
                }); 


            }elseif (isset($input['job_created_to']) && !empty($input['job_created_to'])) {
                
                $sql->whereHas('courseJob',function($query) use($input){

                    $query->whereDate('created_at','>=',$input['job_created_to']);                              
                });

            }elseif (isset($input['job_created_from']) && !empty($input['job_created_from'])) {
                    
                $sql->whereHas('courseJob',function($query) use($input){

                    $query->whereDate('created_at','<=',$input['job_created_from']);                              
                });                               
            } 

            if (isset($input['all_applied']) && $input['all_applied'] == 2) {
                                              
               $sql->whereNotNull('applied_date');                              
            }           

            if (isset($input['company_id']) && !empty($input['company_id'])) {
                                              
               $sql->whereHas('courseJob',function($query) use($input){

                     $query->whereIn('company_id',$input['company_id']);
               });      
            }

            if (isset($input['rank_id']) && !empty($input['rank_id'])) {
                                              
               $sql->whereHas('courseJob',function($query) use($input){

                     $query->where('rank_id',$input['rank_id']);
               });      
            }           

            if (isset($input['title']) && !empty($input['title'])) {
                                              
               $sql->whereHas('courseJob',function($query) use($input){

                     $query->where('title', 'LIKE', '%' . $input['title'] . '%');
               });      
            }

            if (isset($input['user_name']) && !empty($input['user_name'])) {
                                              
               $sql->whereHas('user',function($query) use($input){

                     $query->where('first_name', 'LIKE', '%' . $input['user_name'] . '%')
                            ->orWhere('last_name', 'LIKE', '%' . $input['user_name'] . '%');
               });      
            }                    

            return Datatables::of($sql)
                    ->editColumn('id', function ($data) {

                        return $data->id;

                    })
                    ->editColumn('shareData.latest_qr', function ($data) {

                        return !empty($data->shareData->latest_qr) ? date('d M Y',strtotime($data->shareData->latest_qr)) : '-' ;

                    })
                    ->editColumn('shareData.history_log_latest.description', function ($data) {

                        if ($data->shareData !=null) {

                            if ($data->shareData->history_log_latest !=null && $data->shareData->history_log_latest->description) {
                                    
                                return $data->shareData->history_log_latest->description;

                            }else{

                                return '-';
                            }

                        }else{

                            return '-';
                        }   

                    })
                    ->editColumn('courseJob.title', function ($data) {

                        return !empty($data->courseJob) ? $data->courseJob->title : '-' ;

                    })
                    ->editColumn('courseJob.unique_code', function ($data) {

                        return !empty($data->courseJob) ? $data->courseJob->unique_code : '-' ;

                    })
                    ->editColumn('courseJob.jobStatus.name', function ($data) {

                        return !empty($data->courseJob->jobStatus) ? $data->courseJob->jobStatus->name : '-' ;

                    })
                    ->editColumn('courseJob.company.name', function ($data) {

                        return !empty($data->courseJob->company) ? $data->courseJob->company->name : '-' ;

                    })
                    ->editColumn('created_at', function ($data) {

                        return ($data->created_at !=null) ? date('d M y',strtotime($data->created_at)) : '-';

                    })
                    ->editColumn('applied_date', function ($data) {

                        return ($data->applied_date !=null) ? date('d M y',strtotime($data->applied_date)) : '-';

                    })
                    ->editColumn('courseJob.rank_id', function ($data) {
                        
                        if (empty($data->courseJob->rank_id)) {

                            return '-';

                        }else{

                            return getRankCommonFuncion($data->courseJob->rank_id);
                        }

                    })
                    ->make(true);
            
        }
    }

    public function getViewCompanyJob($request)
    {
        $input = $request->all();

        if ($request->method() == 'GET') {

            $sql = \App\SendedJob::select('*')
                                ->with([
                                        'courseJob',
                                        'courseJob.jobStatus',
                                        'courseJob.company',
                                        'user',
                                        'shareData',
                                        'shareData.history_log_latest'
                                    ])
                                ->has('courseJob')
                                ->has('user');

            if (isset($input['job_post_to']) && !empty($input['job_post_to']) && isset($input['job_post_from']) && !empty($input['job_post_from'])) {
                                              
                $sql->whereDate('created_at','>=',$input['job_post_to'])
                    ->whereDate('created_at','<=',$input['job_post_from']);                              

            }elseif (isset($input['job_post_to']) && !empty($input['job_post_to'])) {
                
                $sql->whereDate('created_at','>=',$input['job_post_to']);                              

            }elseif (isset($input['job_post_from']) && !empty($input['job_post_from'])) {
                    
                $sql->whereDate('created_at','<=',$input['job_post_from']);                              
            }

            if (isset($input['application_to']) && !empty($input['application_to']) && isset($input['application_from']) && !empty($input['application_from'])) {
                                              
                $sql->whereDate('applied_date','>=',$input['application_to'])
                    ->whereDate('applied_date','<=',$input['application_from']);                              

            }elseif (isset($input['application_to']) && !empty($input['application_to'])) {
                
                $sql->whereDate('applied_date','>=',$input['application_to']);                              

            }elseif (isset($input['application_from']) && !empty($input['application_from'])) {
                    
                $sql->whereDate('applied_date','<=',$input['application_from']);                              
            }

            if (isset($input['job_created_to']) && !empty($input['job_created_to']) && isset($input['job_created_from']) && !empty($input['job_created_from'])) {
                

                $sql->whereHas('courseJob',function($query) use($input){

                    $query->whereDate('created_at','>=',$input['job_created_to'])
                        ->whereDate('created_at','<=',$input['job_created_from']);                              
                }); 


            }elseif (isset($input['job_created_to']) && !empty($input['job_created_to'])) {
                
                $sql->whereHas('courseJob',function($query) use($input){

                    $query->whereDate('created_at','>=',$input['job_created_to']);                              
                });

            }elseif (isset($input['job_created_from']) && !empty($input['job_created_from'])) {
                    
                $sql->whereHas('courseJob',function($query) use($input){

                    $query->whereDate('created_at','<=',$input['job_created_from']);                              
                });                               
            } 

            if (isset($input['all_applied']) && $input['all_applied'] == 2) {
                                              
               $sql->whereNotNull('applied_date');                              
            }           

            if (isset($input['company_id']) && !empty($input['company_id'])) {
                                              
               $sql->whereHas('courseJob',function($query) use($input){

                     $query->whereIn('company_id',$input['company_id']);
               });      
            }

            if (isset($input['valid_only']) && !empty($input['valid_only'])) {
                                              
               $sql->whereHas('courseJob',function($query) use($input){
                    $query->whereDate('valid_till_date','>=',Carbon::now()->format('Y-m-d'));
               });      
            }

            if (isset($input['rank_id']) && !empty($input['rank_id'])) {
                                              
               $sql->whereHas('courseJob',function($query) use($input){

                     $query->where('rank_id',$input['rank_id']);
               });      
            }           

            if (isset($input['title']) && !empty($input['title'])) {
                                              
               $sql->whereHas('courseJob',function($query) use($input){

                     $query->where('title', 'LIKE', '%' . $input['title'] . '%');
               });      
            }

            return Datatables::of($sql)
                    ->editColumn('id', function ($data) {

                        return $data->id;

                    })
                    ->editColumn('shareData.latest_qr', function ($data) {

                        return !empty($data->shareData->latest_qr) ? date('d M Y',strtotime($data->shareData->latest_qr)) : '-' ;

                    })
                    ->editColumn('shareData.history_log_latest.description', function ($data) {

                        if ($data->shareData !=null) {

                            if ($data->shareData->history_log_latest !=null && $data->shareData->history_log_latest->description) {
                                    
                                return $data->shareData->history_log_latest->description;

                            }else{

                                return '-';
                            }

                        }else{

                            return '-';
                        }   

                    })
                    ->editColumn('courseJob.title', function ($data) {

                        return !empty($data->courseJob) ? $data->courseJob->title : '-' ;

                    })
                    ->editColumn('courseJob.unique_code', function ($data) {

                        return !empty($data->courseJob) ? $data->courseJob->unique_code : '-' ;

                    })                    
                    ->editColumn('courseJob.valid_till_date', function ($data) {

                        return !empty($data->courseJob) ? date('d M Y',strtotime($data->courseJob->valid_till_date)) : '-' ;

                    })
                    ->editColumn('courseJob.jobStatus.name', function ($data) {

                        return !empty($data->courseJob->jobStatus) ? $data->courseJob->jobStatus->name : '-' ;

                    })
                    ->editColumn('courseJob.company.name', function ($data) {

                        return !empty($data->courseJob->company) ? $data->courseJob->company->name : '-' ;

                    })
                    ->editColumn('created_at', function ($data) {

                        return ($data->created_at !=null) ? date('d M y',strtotime($data->created_at)) : '-';

                    })
                    ->editColumn('applied_date', function ($data) {

                        return ($data->applied_date !=null) ? date('d M y',strtotime($data->applied_date)) : '-';

                    })
                    ->editColumn('courseJob.rank_id', function ($data) {
                        
                        if (empty($data->courseJob->rank_id)) {

                            return '-';

                        }else{

                            return getRankCommonFuncion($data->courseJob->rank_id);
                        }

                    })
                    ->make(true);
            
        }
    }

    public function getViewAllUser($request)
    {
        $input = $request->all();

        if ($request->method() == 'GET') {

            $sql = \App\SendedJob::select('*')
                                ->with([
                                        'courseJob',
                                        'courseJob.jobStatus',
                                        'courseJob.company',
                                        'user',
                                        'shareData',
                                        'shareData.history_log_latest'
                                    ])
                                ->has('courseJob')
                                ->has('user');

            if (isset($input['job_post_to']) && !empty($input['job_post_to']) && isset($input['job_post_from']) && !empty($input['job_post_from'])) {
                                              
                $sql->whereDate('created_at','>=',$input['job_post_to'])
                    ->whereDate('created_at','<=',$input['job_post_from']);                              

            }elseif (isset($input['job_post_to']) && !empty($input['job_post_to'])) {
                
                $sql->whereDate('created_at','>=',$input['job_post_to']);                              

            }elseif (isset($input['job_post_from']) && !empty($input['job_post_from'])) {
                    
                $sql->whereDate('created_at','<=',$input['job_post_from']);                              
            }

            if (isset($input['application_to']) && !empty($input['application_to']) && isset($input['application_from']) && !empty($input['application_from'])) {
                                              
                $sql->whereDate('applied_date','>=',$input['application_to'])
                    ->whereDate('applied_date','<=',$input['application_from']);                              

            }elseif (isset($input['application_to']) && !empty($input['application_to'])) {
                
                $sql->whereDate('applied_date','>=',$input['application_to']);                              

            }elseif (isset($input['application_from']) && !empty($input['application_from'])) {
                    
                $sql->whereDate('applied_date','<=',$input['application_from']);                              
            }

           
            if (isset($input['all_applied']) && $input['all_applied'] == 2) {
                                              
               $sql->whereNotNull('applied_date');                              
            }           

            if (isset($input['rank_id']) && !empty($input['rank_id'])) {
                                              
               $sql->whereHas('courseJob',function($query) use($input){

                     $query->where('rank_id',$input['rank_id']);
               });      
            }           

            if (isset($input['title']) && !empty($input['title'])) {
                                              
               $sql->whereHas('courseJob',function($query) use($input){

                     $query->where('title', 'LIKE', '%' . $input['title'] . '%');
               });      
            }

            if (isset($input['user_name']) && !empty($input['user_name'])) {
                                              
               $sql->whereHas('user',function($query) use($input){

                     $query->where('first_name', 'LIKE', '%' . $input['user_name'] . '%')
                            ->orWhere('last_name', 'LIKE', '%' . $input['user_name'] . '%');
               });      
            }                    

            return Datatables::of($sql)
                    ->editColumn('id', function ($data) {

                        return $data->id;

                    })
                    ->editColumn('shareData.latest_qr', function ($data) {

                        return !empty($data->shareData->latest_qr) ? date('d M Y',strtotime($data->shareData->latest_qr)) : '-' ;

                    })
                    ->editColumn('shareData.history_log_latest.description', function ($data) {

                        if ($data->shareData !=null) {

                            if ($data->shareData->history_log_latest !=null && $data->shareData->history_log_latest->description) {
                                    
                                return $data->shareData->history_log_latest->description;

                            }else{

                                return '-';
                            }

                        }else{

                            return '-';
                        }   

                    })
                    ->editColumn('courseJob.title', function ($data) {

                        return !empty($data->courseJob) ? $data->courseJob->title : '-' ;

                    })
                    ->editColumn('courseJob.unique_code', function ($data) {

                        return !empty($data->courseJob) ? $data->courseJob->unique_code : '-' ;

                    })
                    ->editColumn('courseJob.jobStatus.name', function ($data) {

                        return !empty($data->courseJob->jobStatus) ? $data->courseJob->jobStatus->name : '-' ;

                    })
                    ->editColumn('courseJob.company.name', function ($data) {

                        return !empty($data->courseJob->company) ? $data->courseJob->company->name : '-' ;

                    })
                    ->editColumn('created_at', function ($data) {

                        return ($data->created_at !=null) ? date('d M y',strtotime($data->created_at)) : '-';

                    })
                    ->editColumn('applied_date', function ($data) {

                        return ($data->applied_date !=null) ? date('d M y',strtotime($data->applied_date)) : '-';

                    })
                    ->editColumn('courseJob.rank_id', function ($data) {
                        
                        if (empty($data->courseJob->rank_id)) {

                            return '-';

                        }else{

                            return getRankCommonFuncion($data->courseJob->rank_id);
                        }

                    })
                    ->make(true);
            
        }
    }

    public function saveUpdateCourseJob($r,$id=NULL){
      
    	$errors="";
    	$input = $r->all();

    	if ($id !== NULL) {

            $id=Crypt::decrypt($id);
  			$obj = self::where('id',$id)->firstOrFail();	  		
            $msg = "Job record successfully updated.";

            if ($obj->unique_code == null) {
                
                $obj->unique_code = '#'.generateRandomNumberCode();
            }

        }else{
            
        	$obj = new self;
            $msg = "Job record successfully saved.";
            $obj->unique_code = '#'.generateRandomNumberCode();
        }
        

        $obj->company_email_address_id = $input['company_email_address_id'];
        $obj->company_id = $input['company_id'];
        $obj->rank_id = $input['rank_id'];
        $obj->title = $input['title'];

        $obj->message = isset($input['message']) ? $input['message'] : NULL;
        $obj->valid_till_date = isset($input['valid_till_date']) ? $input['valid_till_date'] : NULL;
        $obj->source = isset($input['source']) ? $input['source'] : NULL;
        $obj->job_status_id = isset($input['job_status_id']) ? $input['job_status_id'] : NULL;
        
        // $obj->rank_experience_year = isset($input['rank_experience_year']) ? $input['rank_experience_year'] : NULL;
        // $obj->rank_experience_month = isset($input['rank_experience_month']) ? $input['rank_experience_month'] : NULL;
        
        $obj->salary_type = isset($input['salary_type']) ? $input['salary_type'] : NULL;
        $obj->minimum_wages = isset($input['minimum_wages']) ? $input['minimum_wages'] : NULL;
        $obj->maximum_wages = isset($input['maximum_wages']) ? $input['maximum_wages'] : NULL;
        $obj->coc = isset($input['coc']) ? $input['coc'] : NULL;

        $obj->coe = isset($input['coe']) ? $input['coe'] : NULL;
        
        
        $obj->cop = isset($input['cop']) ? $input['cop'] : NULL;
        
        $obj->certification = isset($input['certification']) ? $input['certification'] : NULL;
        $obj->vaccination = isset($input['vaccination']) ? $input['vaccination'] : NULL;

        $obj->dp = isset($input['dp']) ? $input['dp'] : NULL;
        


        $obj->coc_experience_year = isset($input['coc_experience_year']) ? $input['coc_experience_year'] : 0;
        $obj->coc_experience_month = isset($input['coc_experience_month']) ? $input['coc_experience_month'] : 0;
        $obj->coe_experience_year = isset($input['coe_experience_year']) ? $input['coe_experience_year'] : 0;
        $obj->coe_experience_month = isset($input['coe_experience_month']) ? $input['coe_experience_month'] : 0;
        $obj->dp_experience_year = isset($input['dp_experience_year']) ? $input['dp_experience_year'] : 0;
        $obj->dp_experience_month = isset($input['dp_experience_month']) ? $input['dp_experience_month'] : 0;
        $obj->gmdss_experience_year = isset($input['gmdss_experience_year']) ? $input['gmdss_experience_year'] : 0;
        $obj->gmdss_experience_month = isset($input['gmdss_experience_month']) ? $input['gmdss_experience_month'] : 0;
        $obj->cop_experience_year = isset($input['cop_experience_year']) ? $input['cop_experience_year'] : 0;
        $obj->cop_experience_month = isset($input['cop_experience_month']) ? $input['cop_experience_month'] : 0;



        if (isset($input['image']) && !empty($input['image'])) {

            $imageName = UPLOAD_FILE($r,'image',COURSE_JOB_IMAGE_UPLOAD_PATH());
            if ($imageName !="") {
              $obj->image = $imageName;
            }
        }

        $obj->save();

        // Multiple Dce
        CourseJobDce::where('course_job_id',$obj->id)->delete();

        if (isset($input['dce_id']) && !empty($input['dce_id'])) {
            
            foreach($input['dce_id'] as $key => $value) {
                
                if (!empty($value)) {
                                
                    $cj = new CourseJobDce;        
                    $cj->course_job_id = $obj->id;
                    $cj->dce_id = $value;
                    $cj->save();
                }        
            }    
        }

        // Multiple Course
        JobMultipleCourse::where('course_job_id',$obj->id)->delete();

        if (isset($input['course_id']) && !empty($input['course_id'])) {
            
            foreach($input['course_id'] as $key => $value) {
                
                if (!empty($value)) {
                                
                    $cj = new JobMultipleCourse;        
                    $cj->course_job_id = $obj->id;
                    $cj->course_id = $value;
                    $cj->save();
                }        
            }    
        }

        // Passport Multiple Country
        MultiplePassportCountry::where('course_job_id',$obj->id)->delete();

        if (isset($input['passport_country_id']) && !empty($input['passport_country_id'])) {
            
            foreach($input['passport_country_id'] as $key => $value) {
                
                if (!empty($value)) {
                                
                    $cj = new MultiplePassportCountry;        
                    $cj->course_job_id = $obj->id;
                    $cj->passport_country_id = $value;
                    $cj->save();
                }        
            }    
        }

        // COE Multiple Country
        JobMultipleCoeCountry::where('course_job_id',$obj->id)->delete();

        if (isset($input['coe_country_id']) && !empty($input['coe_country_id'])) {
            
            foreach($input['coe_country_id'] as $key => $value) {
                
                if (!empty($value)) {
                                
                    $cj = new JobMultipleCoeCountry;        
                    $cj->course_job_id = $obj->id;
                    $cj->coe_country_id = $value;
                    $cj->save();
                }        
            }    
        }

        // COC Multiple Country
        JobMultipleCocCountry::where('course_job_id',$obj->id)->delete();

        if (isset($input['coc_country_id']) && !empty($input['coc_country_id'])) {
            
            foreach($input['coc_country_id'] as $key => $value) {
                
                if (!empty($value)) {
                                
                    $cj = new JobMultipleCocCountry;        
                    $cj->course_job_id = $obj->id;
                    $cj->coc_country_id = $value;
                    $cj->save();
                }        
            }    
        }

        // COP Multiple Country
        JobMultipleCopCountry::where('course_job_id',$obj->id)->delete();

        if (isset($input['cop_country_id']) && !empty($input['cop_country_id'])) {
            
            foreach($input['cop_country_id'] as $key => $value) {
                
                if (!empty($value)) {
                                
                    $cj = new JobMultipleCopCountry;        
                    $cj->course_job_id = $obj->id;
                    $cj->cop_country_id = $value;
                    $cj->save();
                }        
            }    
        }
        
        // DP Multiple Country
        JobMultipleDpCountry::where('course_job_id',$obj->id)->delete();

        if (isset($input['dp_country_id']) && !empty($input['dp_country_id'])) {
            
            foreach($input['dp_country_id'] as $key => $value) {
                
                if (!empty($value)) {
                                
                    $cj = new JobMultipleDpCountry;        
                    $cj->course_job_id = $obj->id;
                    $cj->dp_country_id = $value;
                    $cj->save();
                }        
            }    
        }

        // GMDSS Multiple Country
        JobMultipleGmdssCountry::where('course_job_id',$obj->id)->delete();

        if (isset($input['gmdss_country_id']) && !empty($input['gmdss_country_id'])) {
            
            foreach($input['gmdss_country_id'] as $key => $value) {
                
                if (!empty($value)) {
                                
                    $cj = new JobMultipleGmdssCountry;        
                    $cj->course_job_id = $obj->id;
                    $cj->gmdss_country_id = $value;
                    $cj->save();
                }        
            }    
        }

        $obj->experience_engine_type = isset($input['experience_engine_type']) ? $input['experience_engine_type'] : NULL;

        // Multiple Ship Type
        JobMultipleShipType::where('course_job_id',$obj->id)->delete();

        if (isset($input['experience_ship_type']) && !empty($input['experience_ship_type'])) {
            
            foreach($input['experience_ship_type'] as $key => $value) {
                
                if (!empty($value)) {
                                
                    $ship = new JobMultipleShipType;        
                    $ship->course_job_id = $obj->id;
                    $ship->experience_ship_type_id = $value;
                    $ship->save();
                }        
            }    
        }

        // Multiple Engine Type
        JobMultipleEngineType::where('course_job_id',$obj->id)->delete();

        if (isset($input['experience_engine_type']) && !empty($input['experience_engine_type'])) {
            
            foreach($input['experience_engine_type'] as $key => $value) {
                
                if (!empty($value)) {
                                
                    $engine = new JobMultipleEngineType;        
                    $engine->course_job_id = $obj->id;
                    $engine->experience_engine_type_id = $value;
                    $engine->save();
                }        
            }    
        }
            
        flashMessage('success',$msg);

        return response()->json(['success' => true,'msg'=>$msg, 'status'=>1,'errors' => $errors]);
    }

    public function edit($id){

        $company = self::where('id',$id)
                        ->firstOrFail();

        return $company;
    }

    public function deleteAll($r){

        $input=$r->all();
        foreach ($input['checkbox'] as $key => $c) {
            
            $obj = $this->findOrFail(Crypt::decrypt($c));
            $obj->delete();
        }

        $msg = "Company record successfully deleted.";

        return response()->json(['success' => 1, 'msg' => $msg]);
    }

    public static function getJobDropDown(){

        $job = self::pluck('title','id')->toArray();

        return $job;
    }

    public static function getCustomeJobDropDown(){

        $data = self::with(['company'])->whereDate('valid_till_date','>=',Carbon::now())->get();

        $arrayData = array();

        if (!$data->isEmpty()) {

            foreach ($data as $key => $value) {

                $name =''; 
                if (!empty($value->company)) {

                    $name .= $value->company->name.' / ';
                }

                $name .= $value->title;
                $name .= ' / '.$value->unique_code; 


                $arrayData[$value->id] = $name;

            }                             
        }
        return $arrayData;
    }
}
