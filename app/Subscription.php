<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subscription extends Model
{
    protected $table = 'subscriptions';

    protected $fillable =['title','description','duration_title','duration','is_active','amount'];

    public function subscriptionFeatures()
    {
        return $this->hasMany('App\SubscriptionFeature');
    }
}
