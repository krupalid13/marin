<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdvertisementsLocations extends Model
{
    protected $table = 'advertisements_locations';

    protected $fillable = ['advertise_id','state_id','city_id'];

    public function advertisement_details(){
    	return $this->belongsTo('App\AdvertiseDetails','advertise_id','id');
    }
}
