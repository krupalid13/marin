<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SharedDocsToOthers extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable =['user_id','token','valid_time','otp','data','receiver_email'];

    public function user(){
        return $this->hasOne('App\User');
    }
}
