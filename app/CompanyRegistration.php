<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyRegistration extends Model
{
    protected $table = 'company_registration';

    protected $fillable = ['company_id','user_id','company_name', 'email', 'password', 'contact_number', 'contact_person', 'contact_email', 'website', 'status','company_logo','bussiness_category','bussiness_description','b_i_member','address','type','contact_display','email_display','product_description','buss_years','other_buss_category','other_bi_member','is_featured','is_featured_order'];

    public function company_login_details(){
        return $this->hasMany('App\User','id','company_id');
    }

    public function company_location(){
    	return $this->hasMany('App\CompanyLocationDetails','company_id', 'company_id');
    }

    public function user(){
        return $this->hasOne('App\User','id','company_id');
    }

    public function company_detail(){
        return $this->hasOne('App\CompanyDetail','company_id', 'id');
    }

    public function company_locations(){
        return $this->hasMany('App\CompanyLocationDetails','company_id', 'id');
    } 

    public function company_jobs(){
        return $this->hasMany('App\CompanyJob','company_id', 'id');
    }

    public function next_company_jobs(){
        return $this->hasMany('App\CompanyJob','company_id', 'id')->where('valid_to','>=', date("Y-m-d H:i:s"))->where('status','1')->orderBy('valid_to','asc');
    }

    public function company_ship_details(){
        return $this->hasMany('App\CompanyShipDetails','company_id', 'id');
    }

    public function company_documents(){
        return $this->hasOne('App\CompanyDocuments','company_id', 'id');
    }

    public function user_details(){
        return $this->hasOne('App\User','id', 'user_id');
    }

    public function advertisment_details(){
        return $this->hasMany('App\AdvertiseDetails','company_id','id');
    }

    public function advertisment_company_details(){
        return $this->hasMany('App\AdvertiseDetails','company_id','user_id');
    }

    public function active_subscription(){
        return $this->hasMany('App\CompanySubscription','company_reg_id','id');
    }

    public function associate_agent(){
        return $this->hasMany('App\CompanyAgents','from_company_id','id');
    }

    public function appointed_agent(){
        return $this->hasMany('App\CompanyAgents','to_company_id','id');
    }

    public function company_team(){
        return $this->hasMany('App\CompanyTeamDetails','company_id','id');
    }

}
