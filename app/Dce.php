<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Yajra\Datatables\Datatables;
use Crypt;
use App\CompanyEmailAddress;

class Dce extends Model
{
    protected $table = 'dce';

    public static function getDesDropdown(){

       $data = self::pluck('name','id')->toArray();
       return $data;     
    }
}
