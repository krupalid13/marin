<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserSubscription extends Model
{
    protected $table = 'user_subscriptions';

    protected $fillable = ['user_id', 'order_id', 'subscription_details', 'duration', 'valid_from', 'valid_to','subscription_id', 'count', 'status'];

    public function subscription_feature(){
        return $this->hasOne('App\Subscription','id','subscription_id');
    }
}
