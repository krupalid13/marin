<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdvertiseDetails extends Model
{
    protected $table = 'advertisements_details';

    protected $fillable = ['company_id','img_path','status','from_date','to_date','state','city','advertisement_as','full_img','ad_updated_on'];

    public function user_details(){
    	return $this->hasOne('App\User','id','company_id');
    }
    public function company_registration_details(){
    	return $this->hasOne('App\CompanyRegistration','id','company_id');
    }

    public function advertisement_locations(){
        return $this->hasMany('App\CompanyRegistration','id','company_id');
    }

    public function user(){
    	return $this->hasMany('App\user','id','company_id');
    }

}
