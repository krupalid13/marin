<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InstituteDetail extends Model
{
    protected $table = 'institute_details';

    protected $fillable = ['institute_id', 'country','state', 'city', 'zip', 'fax', 'address', 'address1', 'address2', 'institute_description','institute_email','institute_contact_number'];
}
