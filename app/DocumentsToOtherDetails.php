<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DocumentsToOtherDetails extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable =['shared_docs_to_other_id','ip','visiting_time'];

}
