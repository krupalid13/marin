<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rewards extends Model
{

	protected $table = 'affiliate_rewards';

    protected $guarded = [];

     public function rewards(){
        return $this->hasOne('App\User','id','referral_user_id');
    }
}
