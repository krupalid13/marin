<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Yajra\Datatables\Datatables;
use Crypt;

class JobStatus extends Model
{
    protected $table = 'job_status';

    public static function getJobStatusDropdown(){

       $data = self::pluck('name','id')->toArray();
       return $data;     
    }
}
