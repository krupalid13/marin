<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RateReview extends Model
{
    protected $table = 'rate_and_reviews';

    public function getUserData(){
        return $this->belongsTo('App\User','user_id','id');
    }

    public function getShipData(){
        return $this->belongsTo('App\UserSeaService','user_sea_service_id','id');
    }
}
