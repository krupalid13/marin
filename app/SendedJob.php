<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Yajra\Datatables\Datatables;
use Crypt;
use App\CourseJob;
use App\User;
use Carbon\Carbon;
use App\Services\UserService;
use Auth;
use App\ShareHistory;

class SendedJob extends Model
{
    protected $table = 'sended_jobs';

    private $userService;

    public function __construct()
    {
        $this->userService = new UserService();
    }

    public function courseJob(){

        return $this->belongsTo('App\CourseJob','course_job_id','id');
    }

    public function shareData(){

        return $this->belongsTo('App\ShareHistory','id','sended_job_id');
    }

    public function user(){

        return $this->belongsTo('App\User','user_id','id');
    }

    public function jobSendToUser($request){

        $input = $request->all();

        // $job = CourseJob::where('id',$input['course_job_id'])->firstOrFail();
        // $array = \App\User::where('id',185)->first()->toArray();

        // $blade_file = 'emails.admin_send_job_mail_to_user';
        // $title = 'Course4Sea.com: Job openings';
        // $subject = 'Course4Sea.com: Job openings';
        // $message = 'We Have Found a New job Opening that suits your profile.';

        // $data = $array;
        // $data['subject'] = $subject;
        // $data['course_job_id'] =$job->id;

        // return view('emails.admin_send_job_mail_to_user',compact('data','title','subject','message'));exit;
         
        // course_job_id
        
        if (isset($input['checkbox']) && !empty($input['checkbox'])) {
            
            foreach ($input['checkbox'] as $key => $value) {
                
                if (!empty($value)) {

                    $userId = Crypt::decrypt($value);

                    $checkAlreadySended = self::where('course_job_id',$input['course_job_id'])
                            ->where('user_id',$userId)
                            ->first(); 

                    if ($checkAlreadySended == null) {

                        $obj = new self;
                        $obj->user_id = $userId;
                        $obj->course_job_id = $input['course_job_id'];
                        $obj->save();

                        $user = User::where('id',$userId)->first();
                        $user->last_job_id = $input['course_job_id'];
                        $user->sended_jobs_id = $obj->id;
                        $user->last_job_post_date = Carbon::now();
                        $user->save();

                        $data = \App\User::where('id',$userId)->first();
                        // dd($data);exit;
                        // if (env('ENABLE_MAIL') == true) {
                            
                            $result = $this->userService->sendJobMailToUser($data,$input['course_job_id']);
                        // }

                    }                   
                }        
            }    

        }
        
        $msg = "Job has been sended succcessfully.";
        flashMessage('success',$msg);

        return response()->json(['success' => true,'msg'=>$msg, 'status'=>1]);
    }

   
}
