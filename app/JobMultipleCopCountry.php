<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Yajra\Datatables\Datatables;
use Crypt;
use App\CourseJob;

class JobMultipleCopCountry extends Model
{
    protected $table = 'job_multiple_cop_countries';
}
