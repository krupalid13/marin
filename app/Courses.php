<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Courses extends Model
{
    protected $table = 'courses';

    protected $fillable = ['course_name','course_type','status'];

    public function institute_courses(){
        return $this->hasMany('App\InstituteCourses','course_id', 'id');
    }

    public static function getAllCourse(){

    	return self::where('status',1)->pluck('course_name','id')->toArray();
    }

    public static function getCoursenameById($id){
        return self::where('id',$id)->first();
    }
}
