<?php

use Hashids\Hashids;
use Illuminate\Support\Facades\Auth;
use App\EnginType;

class CommonHelper {
    public static function createProfilePhotoThumbnails($source_file, $data = false, $resizeDimensions, $destination_path, $destination_filename, $extension) {

        try {
            $thumbnail = \PhpThumbFactory::create($source_file);
            if ($data != false) {
                $thumbnail->resize($data['image-w'], $data['image-h']);
                $thumbnail->crop($data['image-x'], $data['image-y'], $data['crop-w'], $data['crop-h']);
            }
            foreach ($resizeDimensions as $dimension) {
                $thumbnail->resize($dimension[0], $dimension[1]);
                $thumbnail->save($destination_path . "/" . $destination_filename . $extension);

                $s3 = new S3(env('AWS_ACCESS_KEY_ID'), env('AWS_SECRET_ACCESS_KEY'), false, 's3.amazonaws.com',env('AWS_DEFAULT_REGION'));
                $moveable_file = $destination_path . "/" . $destination_filename . $extension;
                if(env("APP_ENV") == 'local') {
                    if ($s3->putObjectFile(str_replace([DIRECTORY_SEPARATOR,DIRECTORY_SEPARATOR], ["/","\\"], $moveable_file), env('AWS_BUCKET'), str_replace([DIRECTORY_SEPARATOR,DIRECTORY_SEPARATOR], ["/","\\"], $moveable_file), S3::ACL_PUBLIC_READ)) {
                        unlink($moveable_file);
                    }
                } else {
                    if ($s3->putObjectFile($moveable_file, env('AWS_BUCKET'), $moveable_file, S3::ACL_PUBLIC_READ)) {
                        unlink($moveable_file);
                    }
                }
            }
        } catch (Exception $e) {
            if (isset($e->errorInfo[2])) {
              $msg = $e->errorInfo[2];
          } else {
              $msg = $e->getMessage();
          }
            \Log::info([$msg]);
        }
        return 1;
    }

    public static function coverall_size() {
        $size = [
            1 => "S",
            2 => "M",
            3 => "L",
            4 => "XL",
            5 => "2XL",
            6 => "3XL",
            7 => "4XL",
            8 => "5XL"
        ];
        return $size;

    }

    public static function safety_shoe_size() {
        $size = [
            9 => "6",
            10 => "7",
            1 => "8",
            2 => "8.5",
            3 => "9",
            4 => "9.5",
            5 => "10",
            6 => "10.5",
            7 => "11",
            8 => "11.5"
        ];
        return $size;

    }

    public static function blood_type() {
        $blood_type = [
            1 => "A+",
            2 => "A-",
            3 => "B+",
            4 => "B-",
            5 => "AB+",
            6 => "AB-",
            7 => "O+",
            8 => "O-"
        ];
        return $blood_type;

    }
    public static function countries() {
        $countries = [
            1 => "Afghanistan",
            2 => "Albania",
            3 => "Algeria",
            4 => "American Samoa",
            5 => "Andorra",
            6 => "Angola",
            7 => "Anguilla",
            8 => "Antarctica",
            9 => "Antigua and Barbuda",
            10 => "Argentina",
            11 => "Armenia",
            12 => "Aruba",
            13 => "Australia",
            14 => "Austria",
            15 => "Azerbaijan",
            16 => "Bahamas",
            17 => "Bahrain",
            18 => "Bangladesh",
            19 => "Barbados",
            20 => "Belarus",
            21 => "Belgium",
            22 => "Belize",
            23 => "Benin",
            24 => "Bermuda",
            25 => "Bhutan",
            26 => "Bolivia",
            27 => "Bosnia and Herzegovina",
            28 => "Botswana",
            29 => "Brazil",
            30 => "British Indian Ocean Territory",
            31 => "British Virgin Islands",
            32 => "Brunei",
            33 => "Bulgaria",
            34 => "Burkina Faso",
            35 => "Burundi",
            36 => "Cambodia",
            37 => "Cameroon",
            38 => "Canada",
            39 => "Cape Verde",
            40 => "Cayman Islands",
            41 => "Central African Republic",
            42 => "Chad",
            43 => "Chile",
            44 => "China",
            45 => "Christmas Island",
            46 => "Cocos Islands",
            47 => "Colombia",
            48 => "Comoros",
            49 => "Cook Islands",
            50 => "Costa Rica",
            51 => "Croatia",
            52 => "Cuba",
            53 => "Curacao",
            54 => "Cyprus",
            55 => "Czech Republic",
            56 => "Democratic Republic of the Congo",
            57 => "Denmark",
            58 => "Djibouti",
            59 => "Dominica",
            60 => "Dominican Republic",
            61 => "East Timor",
            62 => "Ecuador",
            63 => "Egypt",
            64 => "El Salvador",
            65 => "Equatorial Guinea",
            66 => "Eritrea",
            67 => "Estonia",
            68 => "Ethiopia",
            69 => "Falkland Islands",
            70 => "Faroe Islands",
            71 => "Fiji",
            72 => "Finland",
            73 => "France",
            74 => "French Polynesia",
            75 => "Gabon",
            76 => "Gambia",
            77 => "Georgia",
            78 => "Germany",
            79 => "Ghana",
            80 => "Gibraltar",
            81 => "Greece",
            82 => "Greenland",
            83 => "Grenada",
            84 => "Guam",
            85 => "Guatemala",
            86 => "Guernsey",
            87 => "Guinea",
            88 => "Guinea-Bissau",
            89 => "Guyana",
            90 => "Haiti",
            91 => "Honduras",
            92 => "Hong Kong",
            93 => "Hungary",
            94 => "Iceland",
            95 => "India",
            96 => "Indonesia",
            97 => "Iran",
            98 => "Iraq",
            99 => "Ireland",
            100 => "Isle of Man",
            101 => "Israel",
            102 => "Italy",
            103 => "Ivory Coast",
            104 => "Jamaica",
            105 => "Japan",
            106 => "Jersey",
            107 => "Jordan",
            108 => "Kazakhstan",
            109 => "Kenya",
            110 => "Kiribati",
            111 => "Kosovo",
            112 => "Kuwait",
            113 => "Kyrgyzstan",
            114 => "Laos",
            115 => "Latvia",
            116 => "Lebanon",
            117 => "Lesotho",
            118 => "Liberia",
            119 => "Libya",
            120 => "Liechtenstein",
            121 => "Lithuania",
            122 => "Luxembourg",
            123 => "Macau",
            124 => "Macedonia",
            125 => "Madagascar",
            126 => "Malawi",
            127 => "Malaysia",
            128 => "Maldives",
            129 => "Mali",
            130 => "Malta",
            131 => "Marshall Islands",
            132 => "Mauritania",
            133 => "Mauritius",
            134 => "Mayotte",
            135 => "Mexico",
            136 => "Micronesia",
            137 => "Moldova",
            138 => "Monaco",
            139 => "Mongolia",
            140 => "Montenegro",
            141 => "Montserrat",
            142 => "Morocco",
            143 => "Mozambique",
            144 => "Myanmar",
            145 => "Namibia",
            146 => "Nauru",
            147 => "Nepal",
            148 => "Netherlands",
            149 => "Netherlands Antilles",
            150 => "New Caledonia",
            151 => "New Zealand",
            152 => "Nicaragua",
            153 => "Niger",
            154 => "Nigeria",
            155 => "Niue",
            156 => "North Korea",
            157 => "Northern Mariana Islands",
            158 => "Norway",
            159 => "Oman",
            160 => "Pakistan",
            161 => "Palau",
            162 => "Palestine",
            163 => "Panama",
            164 => "Papua New Guinea",
            165 => "Paraguay",
            166 => "Peru",
            167 => "Philippines",
            168 => "Pitcairn",
            169 => "Poland",
            170 => "Portugal",
            171 => "Puerto Rico",
            172 => "Qatar",
            173 => "Republic of the Congo",
            174 => "Reunion",
            175 => "Romania",
            176 => "Russia",
            177 => "Rwanda",
            178 => "Saint Barthelemy",
            179 => "Saint Helena",
            180 => "Saint Kitts and Nevis",
            181 => "Saint Lucia",
            182 => "Saint Martin",
            183 => "Saint Pierre and Miquelon",
            184 => "Saint Vincent and the Grenadines",
            185 => "Samoa",
            186 => "San Marino",
            187 => "Sao Tome and Principe",
            188 => "Saudi Arabia",
            189 => "Senegal",
            190 => "Serbia",
            191 => "Seychelles",
            192 => "Sierra Leone",
            193 => "Singapore",
            194 => "Sint Maarten",
            195 => "Slovakia",
            196 => "Slovenia",
            197 => "Solomon Islands",
            198 => "Somalia",
            199 => "South Africa",
            200 => "South Korea",
            201 => "South Sudan",
            202 => "Spain",
            203 => "Sri Lanka",
            204 => "Sudan",
            205 => "Suriname",
            206 => "Svalbard and Jan Mayen",
            207 => "Swaziland",
            208 => "Sweden",
            209 => "Switzerland",
            210 => "Syria",
            211 => "Taiwan",
            212 => "Tajikistan",
            213 => "Tanzania",
            214 => "Thailand",
            215 => "Togo",
            216 => "Tokelau",
            217 => "Tonga",
            218 => "Trinidad and Tobago",
            219 => "Tunisia",
            220 => "Turkey",
            221 => "Turkmenistan",
            222 => "Turks and Caicos Islands",
            223 => "Tuvalu",
            224 => "U.S. Virgin Islands",
            225 => "Uganda",
            226 => "Ukraine",
            227 => "United Arab Emirates",
            228 => "United Kingdom",
            229 => "United States",
            230 => "Uruguay",
            231 => "Uzbekistan",
            232 => "Vanuatu",
            233 => "Vatican",
            234 => "Venezuela",
            235 => "Vietnam",
            236 => "Wallis and Futuna",
            237 => "Western Sahara",
            238 => "Yemen",
            239 => "Zambia",
            240 => "Zimbabwe"
        ];
        return $countries;
    }

    public static function states() {
        $states = [
            1 => "Afghanistan",
            2 => "Albania",
            3 => "Algeria",
            4 => "American Samoa",
            5 => "Andorra",
            6 => "Angola",
            7 => "Anguilla",
            8 => "Antarctica",
            9 => "Antigua and Barbuda",
            10 => "Argentina",
            11 => "Armenia",
            12 => "Aruba",
            13 => "Australia",
            14 => "Austria",
            15 => "Azerbaijan",
            16 => "Bahamas",
            17 => "Bahrain",
            18 => "Bangladesh",
            19 => "Barbados",
            20 => "Belarus",
            21 => "Belgium",
            22 => "Belize",
            23 => "Benin",
            24 => "Bermuda",
            25 => "Bhutan",
            26 => "Bolivia",
            27 => "Bosnia and Herzegovina",
            28 => "Botswana",
            29 => "Brazil",
            30 => "British Indian Ocean Territory",
            31 => "British Virgin Islands",
            32 => "Brunei",
            33 => "Bulgaria",
            34 => "Burkina Faso",
            35 => "Burundi",
            36 => "Cambodia",
            37 => "Cameroon",
            38 => "Canada",
            39 => "Cape Verde",
            40 => "Cayman Islands",
            41 => "Central African Republic",
            42 => "Chad",
            43 => "Chile",
            44 => "China",
            45 => "Christmas Island",
            46 => "Cocos Islands",
            47 => "Colombia",
            48 => "Comoros",
            49 => "Cook Islands",
            50 => "Costa Rica",
            51 => "Croatia",
            52 => "Cuba",
            53 => "Curacao",
            54 => "Cyprus",
            55 => "Czech Republic",
            56 => "Democratic Republic of the Congo",
            57 => "Denmark",
            58 => "Djibouti",
            59 => "Dominica",
            60 => "Dominican Republic",
            61 => "East Timor",
            62 => "Ecuador",
            63 => "Egypt",
            64 => "El Salvador",
            65 => "Equatorial Guinea",
            66 => "Eritrea",
            67 => "Estonia",
            68 => "Ethiopia",
            69 => "Falkland Islands",
            70 => "Faroe Islands",
            71 => "Fiji",
            72 => "Finland",
            73 => "France",
            74 => "French Polynesia",
            75 => "Gabon",
            76 => "Gambia",
            77 => "Georgia",
            78 => "Germany",
            79 => "Ghana",
            80 => "Gibraltar",
            81 => "Greece",
            82 => "Greenland",
            83 => "Grenada",
            84 => "Guam",
            85 => "Guatemala",
            86 => "Guernsey",
            87 => "Guinea",
            88 => "Guinea-Bissau",
            89 => "Guyana",
            90 => "Haiti",
            91 => "Honduras",
            92 => "Hong Kong",
            93 => "Hungary",
            94 => "Iceland",
            95 => "India",
            96 => "Indonesia",
            97 => "Iran",
            98 => "Iraq",
            99 => "Ireland",
            100 => "Isle of Man",
            101 => "Israel",
            102 => "Italy",
            103 => "Ivory Coast",
            104 => "Jamaica",
            105 => "Japan",
            106 => "Jersey",
            107 => "Jordan",
            108 => "Kazakhstan",
            109 => "Kenya",
            110 => "Kiribati",
            111 => "Kosovo",
            112 => "Kuwait",
            113 => "Kyrgyzstan",
            114 => "Laos",
            115 => "Latvia",
            116 => "Lebanon",
            117 => "Lesotho",
            118 => "Liberia",
            119 => "Libya",
            120 => "Liechtenstein",
            121 => "Lithuania",
            122 => "Luxembourg",
            123 => "Macau",
            124 => "Macedonia",
            125 => "Madagascar",
            126 => "Malawi",
            127 => "Malaysia",
            128 => "Maldives",
            129 => "Mali",
            130 => "Malta",
            131 => "Marshall Islands",
            132 => "Mauritania",
            133 => "Mauritius",
            134 => "Mayotte",
            135 => "Mexico",
            136 => "Micronesia",
            137 => "Moldova",
            138 => "Monaco",
            139 => "Mongolia",
            140 => "Montenegro",
            141 => "Montserrat",
            142 => "Morocco",
            143 => "Mozambique",
            144 => "Myanmar",
            145 => "Namibia",
            146 => "Nauru",
            147 => "Nepal",
            148 => "Netherlands",
            149 => "Netherlands Antilles",
            150 => "New Caledonia",
            151 => "New Zealand",
            152 => "Nicaragua",
            153 => "Niger",
            154 => "Nigeria",
            155 => "Niue",
            156 => "North Korea",
            157 => "Northern Mariana Islands",
            158 => "Norway",
            159 => "Oman",
            160 => "Pakistan",
            161 => "Palau",
            162 => "Palestine",
            163 => "Panama",
            164 => "Papua New Guinea",
            165 => "Paraguay",
            166 => "Peru",
            167 => "Philippines",
            168 => "Pitcairn",
            169 => "Poland",
            170 => "Portugal",
            171 => "Puerto Rico",
            172 => "Qatar",
            173 => "Republic of the Congo",
            174 => "Reunion",
            175 => "Romania",
            176 => "Russia",
            177 => "Rwanda",
            178 => "Saint Barthelemy",
            179 => "Saint Helena",
            180 => "Saint Kitts and Nevis",
            181 => "Saint Lucia",
            182 => "Saint Martin",
            183 => "Saint Pierre and Miquelon",
            184 => "Saint Vincent and the Grenadines",
            185 => "Samoa",
            186 => "San Marino",
            187 => "Sao Tome and Principe",
            188 => "Saudi Arabia",
            189 => "Senegal",
            190 => "Serbia",
            191 => "Seychelles",
            192 => "Sierra Leone",
            193 => "Singapore",
            194 => "Sint Maarten",
            195 => "Slovakia",
            196 => "Slovenia",
            197 => "Solomon Islands",
            198 => "Somalia",
            199 => "South Africa",
            200 => "South Korea",
            201 => "South Sudan",
            202 => "Spain",
            203 => "Sri Lanka",
            204 => "Sudan",
            205 => "Suriname",
            206 => "Svalbard and Jan Mayen",
            207 => "Swaziland",
            208 => "Sweden",
            209 => "Switzerland",
            210 => "Syria",
            211 => "Taiwan",
            212 => "Tajikistan",
            213 => "Tanzania",
            214 => "Thailand",
            215 => "Togo",
            216 => "Tokelau",
            217 => "Tonga",
            218 => "Trinidad and Tobago",
            219 => "Tunisia",
            220 => "Turkey",
            221 => "Turkmenistan",
            222 => "Turks and Caicos Islands",
            223 => "Tuvalu",
            224 => "U.S. Virgin Islands",
            225 => "Uganda",
            226 => "Ukraine",
            227 => "United Arab Emirates",
            228 => "United Kingdom",
            229 => "United States",
            230 => "Uruguay",
            231 => "Uzbekistan",
            232 => "Vanuatu",
            233 => "Vatican",
            234 => "Venezuela",
            235 => "Vietnam",
            236 => "Wallis and Futuna",
            237 => "Western Sahara",
            238 => "Yemen",
            239 => "Zambia",
            240 => "Zimbabwe"
        ];
        return $states;
    }

    public static function cities() {

        $temp = 'Afghanistan,Albania,Algeria,American Samoa,Andorra,Angola,Anguilla,Antarctica,Antigua and Barbuda,Argentina,Armenia,Aruba,Australia,Austria,Azerbaijan,Bahamas,Bahrain,Bangladesh,Barbados,Belarus,Belgium,Belize,Benin,Bermuda,Bhutan,Bolivia,Bosnia and Herzegovina,Botswana,Brazil,British Indian Ocean Territory,British Virgin Islands,Brunei,Bulgaria,Burkina Faso,Burundi,Cambodia,Cameroon,Canada,Cape Verde,Cayman Islands,Central African Republic,Chad,Chile,China,Christmas Island,Cocos Islands,Colombia,Comoros,Cook Islands,Costa Rica,Croatia,Cuba,Curacao,Cyprus,Czech Republic,Democratic Republic of the Congo,Denmark,Djibouti,Dominica,Dominican Republic,East Timor,Ecuador,Egypt,El Salvador,Equatorial Guinea,Eritrea,Estonia,Ethiopia,Falkland Islands,Faroe Islands,Fiji,Finland,France,French Polynesia,Gabon,Gambia,Georgia,Germany,Ghana,Gibraltar,Greece,Greenland,Grenada,Guam,Guatemala,Guernsey,Guinea,Guinea-Bissau,Guyana,Haiti,Honduras,Hong Kong,Hungary,Iceland,India,Indonesia,Iran,Iraq,Ireland,Isle of Man,Israel,Italy,Ivory Coast,Jamaica,Japan,Jersey,Jordan,Kazakhstan,Kenya,Kiribati,Kosovo,Kuwait,Kyrgyzstan,Laos,Latvia,Lebanon,Lesotho,Liberia,Libya,Liechtenstein,Lithuania,Luxembourg,Macau,Macedonia,Madagascar,Malawi,Malaysia,Maldives,Mali,Malta,Marshall Islands,Mauritania,Mauritius,Mayotte,Mexico,Micronesia,Moldova,Monaco,Mongolia,Montenegro,Montserrat,Morocco,Mozambique,Myanmar,Namibia,Nauru,Nepal,Netherlands,Netherlands Antilles,New Caledonia,New Zealand,Nicaragua,Niger,Nigeria,Niue,North Korea,Northern Mariana Islands,Norway,Oman,Pakistan,Palau,Palestine,Panama,Papua New Guinea,Paraguay,Peru,Philippines,Pitcairn,Poland,Portugal,Puerto Rico,Qatar,Republic of the Congo,Reunion,Romania,Russia,Rwanda,Saint Barthelemy,Saint Helena,Saint Kitts and Nevis,Saint Lucia,Saint Martin,Saint Pierre and Miquelon,Saint Vincent and the Grenadines,Samoa,San Marino,Sao Tome and Principe,Saudi Arabia,Senegal,Serbia,Seychelles,Sierra Leone,Singapore,Sint Maarten,Slovakia,Slovenia,Solomon Islands,Somalia,South Africa,South Korea,South Sudan,Spain,Sri Lanka,Sudan,Suriname,Svalbard and Jan Mayen,Swaziland,Sweden,Switzerland,Syria,Taiwan,Tajikistan,Tanzania,Thailand,Togo,Tokelau,Tonga,Trinidad and Tobago,Tunisia,Turkey,Turkmenistan,Turks and Caicos Islands,Tuvalu,U.S. Virgin Islands,Uganda,Ukraine,United Arab Emirates,United Kingdom,United States,Uruguay,Uzbekistan,Vanuatu,Vatican,Venezuela,Vietnam,Wallis and Futuna,Western Sahara,Yemen,Zambia,Zimbabwe';

        // $temp_arr = explode(',', $temp);
        // $countries = [];
        // foreach($temp_arr as $index => $country) {
        // 	$index++;
        // 	$countries[$index] = $country;
        // }
        // return $countries;

        $cities = [
            1 => "Afghanistan",
            2 => "Albania",
            3 => "Algeria",
            4 => "American Samoa",
            5 => "Andorra",
            6 => "Angola",
            7 => "Anguilla",
            8 => "Antarctica",
            9 => "Antigua and Barbuda",
            10 => "Argentina",
            11 => "Armenia",
            12 => "Aruba",
            13 => "Australia",
            14 => "Austria",
            15 => "Azerbaijan",
            16 => "Bahamas",
            17 => "Bahrain",
            18 => "Bangladesh",
            19 => "Barbados",
            20 => "Belarus",
            21 => "Belgium",
            22 => "Belize",
            23 => "Benin",
            24 => "Bermuda",
            25 => "Bhutan",
            26 => "Bolivia",
            27 => "Bosnia and Herzegovina",
            28 => "Botswana",
            29 => "Brazil",
            30 => "British Indian Ocean Territory",
            31 => "British Virgin Islands",
            32 => "Brunei",
            33 => "Bulgaria",
            34 => "Burkina Faso",
            35 => "Burundi",
            36 => "Cambodia",
            37 => "Cameroon",
            38 => "Canada",
            39 => "Cape Verde",
            40 => "Cayman Islands",
            41 => "Central African Republic",
            42 => "Chad",
            43 => "Chile",
            44 => "China",
            45 => "Christmas Island",
            46 => "Cocos Islands",
            47 => "Colombia",
            48 => "Comoros",
            49 => "Cook Islands",
            50 => "Costa Rica",
            51 => "Croatia",
            52 => "Cuba",
            53 => "Curacao",
            54 => "Cyprus",
            55 => "Czech Republic",
            56 => "Democratic Republic of the Congo",
            57 => "Denmark",
            58 => "Djibouti",
            59 => "Dominica",
            60 => "Dominican Republic",
            61 => "East Timor",
            62 => "Ecuador",
            63 => "Egypt",
            64 => "El Salvador",
            65 => "Equatorial Guinea",
            66 => "Eritrea",
            67 => "Estonia",
            68 => "Ethiopia",
            69 => "Falkland Islands",
            70 => "Faroe Islands",
            71 => "Fiji",
            72 => "Finland",
            73 => "France",
            74 => "French Polynesia",
            75 => "Gabon",
            76 => "Gambia",
            77 => "Georgia",
            78 => "Germany",
            79 => "Ghana",
            80 => "Gibraltar",
            81 => "Greece",
            82 => "Greenland",
            83 => "Grenada",
            84 => "Guam",
            85 => "Guatemala",
            86 => "Guernsey",
            87 => "Guinea",
            88 => "Guinea-Bissau",
            89 => "Guyana",
            90 => "Haiti",
            91 => "Honduras",
            92 => "Hong Kong",
            93 => "Hungary",
            94 => "Iceland",
            95 => "India",
            96 => "Indonesia",
            97 => "Iran",
            98 => "Iraq",
            99 => "Ireland",
            100 => "Isle of Man",
            101 => "Israel",
            102 => "Italy",
            103 => "Ivory Coast",
            104 => "Jamaica",
            105 => "Japan",
            106 => "Jersey",
            107 => "Jordan",
            108 => "Kazakhstan",
            109 => "Kenya",
            110 => "Kiribati",
            111 => "Kosovo",
            112 => "Kuwait",
            113 => "Kyrgyzstan",
            114 => "Laos",
            115 => "Latvia",
            116 => "Lebanon",
            117 => "Lesotho",
            118 => "Liberia",
            119 => "Libya",
            120 => "Liechtenstein",
            121 => "Lithuania",
            122 => "Luxembourg",
            123 => "Macau",
            124 => "Macedonia",
            125 => "Madagascar",
            126 => "Malawi",
            127 => "Malaysia",
            128 => "Maldives",
            129 => "Mali",
            130 => "Malta",
            131 => "Marshall Islands",
            132 => "Mauritania",
            133 => "Mauritius",
            134 => "Mayotte",
            135 => "Mexico",
            136 => "Micronesia",
            137 => "Moldova",
            138 => "Monaco",
            139 => "Mongolia",
            140 => "Montenegro",
            141 => "Montserrat",
            142 => "Morocco",
            143 => "Mozambique",
            144 => "Myanmar",
            145 => "Namibia",
            146 => "Nauru",
            147 => "Nepal",
            148 => "Netherlands",
            149 => "Netherlands Antilles",
            150 => "New Caledonia",
            151 => "New Zealand",
            152 => "Nicaragua",
            153 => "Niger",
            154 => "Nigeria",
            155 => "Niue",
            156 => "North Korea",
            157 => "Northern Mariana Islands",
            158 => "Norway",
            159 => "Oman",
            160 => "Pakistan",
            161 => "Palau",
            162 => "Palestine",
            163 => "Panama",
            164 => "Papua New Guinea",
            165 => "Paraguay",
            166 => "Peru",
            167 => "Philippines",
            168 => "Pitcairn",
            169 => "Poland",
            170 => "Portugal",
            171 => "Puerto Rico",
            172 => "Qatar",
            173 => "Republic of the Congo",
            174 => "Reunion",
            175 => "Romania",
            176 => "Russia",
            177 => "Rwanda",
            178 => "Saint Barthelemy",
            179 => "Saint Helena",
            180 => "Saint Kitts and Nevis",
            181 => "Saint Lucia",
            182 => "Saint Martin",
            183 => "Saint Pierre and Miquelon",
            184 => "Saint Vincent and the Grenadines",
            185 => "Samoa",
            186 => "San Marino",
            187 => "Sao Tome and Principe",
            188 => "Saudi Arabia",
            189 => "Senegal",
            190 => "Serbia",
            191 => "Seychelles",
            192 => "Sierra Leone",
            193 => "Singapore",
            194 => "Sint Maarten",
            195 => "Slovakia",
            196 => "Slovenia",
            197 => "Solomon Islands",
            198 => "Somalia",
            199 => "South Africa",
            200 => "South Korea",
            201 => "South Sudan",
            202 => "Spain",
            203 => "Sri Lanka",
            204 => "Sudan",
            205 => "Suriname",
            206 => "Svalbard and Jan Mayen",
            207 => "Swaziland",
            208 => "Sweden",
            209 => "Switzerland",
            210 => "Syria",
            211 => "Taiwan",
            212 => "Tajikistan",
            213 => "Tanzania",
            214 => "Thailand",
            215 => "Togo",
            216 => "Tokelau",
            217 => "Tonga",
            218 => "Trinidad and Tobago",
            219 => "Tunisia",
            220 => "Turkey",
            221 => "Turkmenistan",
            222 => "Turks and Caicos Islands",
            223 => "Tuvalu",
            224 => "U.S. Virgin Islands",
            225 => "Uganda",
            226 => "Ukraine",
            227 => "United Arab Emirates",
            228 => "United Kingdom",
            229 => "United States",
            230 => "Uruguay",
            231 => "Uzbekistan",
            232 => "Vanuatu",
            233 => "Vatican",
            234 => "Venezuela",
            235 => "Vietnam",
            236 => "Wallis and Futuna",
            237 => "Western Sahara",
            238 => "Yemen",
            239 => "Zambia",
            240 => "Zimbabwe"
        ];
        return $cities;
    }

    public static function rank_required_fields() {
        $rank_required_fields = [
            '1' => [
                0 => 'COC',
                1 => 'COE-Optional',
                2 => 'GMDSS-Optional',
                3 => 'DCE-Optional',
            ],
            '2' => [
                0 => 'COC',
                1 => 'COE-Optional',
                2 => 'GMDSS-Optional',
                3 => 'DCE-Optional',
            ],
            '3' => [
                0 => 'COC',
                1 => 'COE-Optional',
                2 => 'GMDSS-Optional',
                3 => 'DCE-Optional',
            ],
            '4' => [
                0 => 'COC',
                1 => 'COE-Optional',
                2 => 'GMDSS-Optional',
                3 => 'DCE-Optional',
            ],
            '5' => [
                0 => 'COC-Optional',
                1 => 'COE-Optional',
                2 => 'GMDSS-Optional',
                3 => 'DCE-Optional',
            ],
            '6' => [
                0 => 'COC-Optional',
                1 => 'COE-Optional',
                2 => 'GMDSS-Optional',
            ],
            '7' => [
                0 => 'COC-Optional',
                1 => 'COE-Optional',
                2 => 'GMDSS-Optional',
                3 => 'DCE-Optional',
            ],
            '8' => [
                0 => 'COC-Optional',
                1 => 'COE-Optional',
                2 => 'GMDSS-Optional',
                3 => 'DCE-Optional',
            ],
            '9' => [
                0 => 'COC',
                1 => 'COE-Optional',
                2 => 'GMDSS-Optional',
                3 => 'DCE-Optional',
            ],
            '10' => [
                0 => 'COC',
                1 => 'COE-Optional',
                2 => 'GMDSS-Optional',
                3 => 'DCE-Optional',
            ],
            '11' => [
                0 => 'COC',
                1 => 'COE-Optional',
                2 => 'GMDSS-Optional',
                3 => 'DCE-Optional',
            ],
            '54' => [
                0 => 'COC-Optional',
                1 => 'COE-Optional',
                2 => 'GMDSS-Optional',
            ],
            '12' => [
                0 => 'COC',
                1 => 'COE-Optional',
                2 => 'GMDSS-Optional',
                3 => 'DCE-Optional',
            ],
            '13' => [
                0 => 'COC',
                1 => 'COE-Optional',
                2 => 'GMDSS-Optional',
                3 => 'DCE-Optional',
            ],
            '14' => [
                0 => 'COC',
                1 => 'COE-Optional',
                2 => 'GMDSS-Optional',
                3 => 'DCE-Optional',
            ],
            '15' => [
                0 => 'COC',
                1 => 'COE-Optional',
                2 => 'GMDSS-Optional',
                3 => 'DCE-Optional',
            ],
            '16' => [
                0 => 'COC',
                1 => 'COE-Optional',
                2 => 'GMDSS-Optional',
                3 => 'DCE-Optional',
            ],
            '17' => [
                0 => 'COC',
                1 => 'COE-Optional',
                2 => 'GMDSS-Optional',
                3 => 'DCE-Optional',
            ],
            '18' => [
                0 => 'DCE-Optional',
            ],
            '19' => [
                0 => 'COC-Optional',
                1 => 'GMDSS-Optional',
                2 => 'DCE-Optional',
            ],
            '20' => [
                0 => 'COC-Optional',
                1 => 'GMDSS-Optional',
                2 => 'DCE-Optional',
            ],
            '21' => [
                0 => 'COC-Optional',
                1 => 'GMDSS-Optional',
                2 => 'DCE-Optional',
            ],
            '22' => [
                0 => 'COC-Optional',
                1 => 'GMDSS-Optional',
                2 => 'DCE-Optional',
            ],
            '23' => [
                0 => 'COC-Optional',
                1 => 'COE-Optional',
            ],
            '24' => [
                0 => 'COC',
                1 => 'COE-Optional',
                2 => 'FROMO',
                3 => 'DCE-Optional',
            ],
            '25' => [
                0 => 'COC',
                1 => 'COE-Optional',
                2 => 'FROMO',
                3 => 'DCE-Optional',
            ],
            '26' => [
                0 => 'COC',
                1 => 'COE-Optional',
                2 => 'FROMO',
                3 => 'DCE-Optional',
            ],
            '27' => [
                0 => 'COC',
                1 => 'COE-Optional',
                2 => 'FROMO',
                3 => 'DCE-Optional',
            ],
            '28' => [
                0 => 'COC',
                1 => 'COE-Optional',
                2 => 'FROMO',
                3 => 'DCE-Optional',
            ],
            '29' => [
                0 => 'COC',
                1 => 'COE-Optional',
                2 => 'FROMO',
                3 => 'DCE-Optional',
            ],
            '30' => [
                0 => 'FROMO',
                1 => 'DCE-Optional',
            ],
            '53' => [
                0 => 'COC',
                1 => 'FROMO',
                2 => 'DCE-Optional',
            ],
            '31' => [
                0 => 'FROMO',
                1 => 'DCE-Optional',
            ],
            '32' => [
                0 => 'FROMO',
                1 => 'DCE-Optional',
            ],
            '33' => [
                0 => 'FROMO',
                1 => 'DCE-Optional',
            ],
            '34' => [
                0 => 'COC-Optional',
                1 => 'FROMO',
                2 => 'DCE-Optional',
            ],
            '35' => [
                0 => 'WATCH_KEEPING-Optional',
                1 => 'DCE-Optional',
            ],
            '36' => [
                0 => 'WATCH_KEEPING-Optional',
                1 => 'DCE-Optional1',
            ],
            '37' => [
                0 => 'WATCH_KEEPING-Optional',
                1 => 'DCE-Optional',
            ],
            '38' => [
                0 => 'WATCH_KEEPING-Optional',
                1 => 'DCE-Optional',
            ],
            '39' => [
                0 => 'WATCH_KEEPING-Optional',
                1 => 'DCE-Optional',
            ],
            '40' => [
                0 => 'WATCH_KEEPING-Optional',
                1 => 'DCE-Optional',
            ],
            '41' => [
                0 => 'WATCH_KEEPING-Optional',
                1 => 'DCE-Optional',
            ],
            '42' => [
                0 => 'WATCH_KEEPING-Optional',
                1 => 'DCE-Optional',
            ],
            '43' => [
                0 => 'WATCH_KEEPING-Optional',
                1 => 'DCE-Optional',
            ],
            '44' => [
            ],
            '45' => [
                0 => 'COC-Optional',
                1 => 'DCE-Optional',
            ],
            '46' => [
                0 => 'COC-Optional',
                1 => 'DCE-Optional',
            ],
            '47' => [
                0 => 'DCE-Optional',
            ],
            '48' => [
                0 => 'DCE-Optional',
            ],
            '49' => [
                0 => 'DCE-Optional',
            ],
            '50' => [
                0 => 'DCE-Optional',
            ],
            '51' => [
                0 => 'DCE-Optional',
            ],
            '52' => [
            ],

        ];
        return $rank_required_fields;
    }
    public static function new_rank() {
        $new_rank = [
            "Deck Officers" => [ 
                '1' => 'Master',
                '2' => 'Master DPO',
                '3' => 'Master NCV',
                '4' => 'Master Handler',
                '5' => 'Master Inland',
                '6' => 'Master Yatch',
                '7' => 'Master Home Trade',
                '8' => 'Master Skipper',
                '9' => 'Chief Officer',
                '10' => 'Chief Officer DPO',
                '11' => 'Chief Officer NCV',
                '54' => 'Chief Officer Inland',
                '12' => 'Second Officer',
                '13' => 'Second Officer DPO',
                '14' => 'Third Officer',
                '15' => 'Third Officer DPO',
                '19' => 'Junior Officer',
                '16' => 'NWKO',
                '17' => 'NWKO NCV',
                '18' => 'Deck Cadet',
                '20' => 'R. O. Handler',
                '21' => 'Radio Officer',
                '22' => 'OOW Yatch',
            ],
            "Engineers" => [
                '24' => 'Chief Engineer',
                '25' => 'Chief Engineer NCV',
                '26' => 'Second Engineer',
                '27' => 'Second Engineer NCV',
                '28' => 'Third Engineer',
                '29' => 'Fourth Engineer',
                '34' => 'Junior Engineer',
                '53' => 'Electro Technical Officer',
                '30' => 'Electrical Officer',
                '31' => 'Tr. Electrical Officer',
                '32' => 'Engine Cadet',
                '33' => 'Trainee Marine Engineer',
                '23' => 'Engine Driver',
            ],
            "Others" => [
                '35' => 'Bosun',
                '36' => 'Able Seaman',
                '37' => 'Ordinary Seaman',
                '38' => 'Pumpman',
                '39' => 'Welder',
                '40' => 'Fitter',
                '41' => 'Crane Operator',
                '42' => 'Motarman/Oiler',
                '43' => 'Wiper',
                '44' => 'G.P Rating',
                '45' => 'Cook',
                '46' => '2nd Cook',
                '47' => 'General Steward',
                '48' => 'Messman',
                '49' => 'Technician',
                '50' => 'Roustabout',
                '51' => 'Rigger',
                '52' => 'Labour'
            ]
        ];

        return $new_rank;
    }

    public static function ranks() {

        $ranks = [
            "1" => 'Fresher',
            "2" => 'Master (Unlimited Tonnage)',
            "3" => 'Master DPO',
            "4" => 'Master (NCV)',
            "5" => 'Master Handler',
            "6" => '2nd Officer',
            "7" => '2nd Officer DPO',
            "8" => '3rd Officer',
            "9" => '3rd Officer DPO',
            "10" => 'NWKO',
            "11" => 'Senior DPO',
            "12" => 'Third Engineer',
            "13" => 'Deck Cadet',
            "14" => 'R.O. Handler',
            "15" => 'Radio Officer',
            "16" => 'Chief Engineer (Unlimited)',
            "17" => 'Chief Engineer(NCV)',
            "18" => 'Second Engineer (Unlimited)',
            "19" => 'Second Engineer(NCV)',
            "20" => 'Third Engineer NCV',
            "21" => 'Third Engineer Off Shore',
            "22" => 'Forth Engineer',
            "23" => 'Fitter',
            "24" => 'PumpMan',
            "25" => 'Crane Operator',
            "26" => 'Bosun',
            "27" => 'Able Seaman',
            "28" => 'MotorMan',
            "29" => 'Wiper',
            "30" => 'G.P.Rating',
            "31" => 'Chief Cook',
            "32" => '2nd Cook',
            "33" => 'General Steward',
            "34" => 'MessMan',
            "35" => 'Master NWKO',
            "36" => 'Master Inland',
            "37" => 'Master Yacht',
            "38" => 'Master Home Trade',
            "39" => 'Master Fishing Vessel Skipper',
            "40" => 'Chief Mate (Unlimited Tonnage)',
            "41" => 'Chief Mate DPO',
            "42" => 'Chief Mate NCV',
            "43" => 'Chief Maste NWKO',
            "44" => 'Chief Mate Handler',
            "45" => 'Chief Mate Inland',
            "46" => 'Chief Mate Yacht',
            "47" => '2nd Officer NCV',
            "48" => '3rd Officer NCV',
            "49" => 'OOW Yacht',
            "50" => 'Ordinary Seaman',
            "51" => 'Junior Officer',
            "52" => 'Chief Engineer - Fishing Vessel',
            "53" => 'Chief Engineer - Offshore Vessel',
            "54" => 'Chief Engineer - Engine Driver',
            "55" => 'Second Engineer - Offshore Vessel',
            "56" => 'Engine Cadet',
            "57" => 'Junior Engineer',
            "58" => 'Electrical Officer',
            "59" => 'Jr.Electrical Officer',
            "60" => 'Technician',
            "61" => 'Roustabout',
            "62" => 'Seaman',
            "63" => 'Additional Master',
            "64" => 'Additional Chief Engineer',
            "65" => 'Riger',
            "66" => 'Labour',
        ];

        return $ranks;
    }

    public static function years() {
        $years = [
            "0" => '0',
            "1" => '1',
            "2" => '2',
            "3" => '3',
            "4" => '4',
            "5" => '5',
            "6" => '6',
            "7" => '7',
            "8" => '8',
            "9" => '9',
            "10" => '10',
            "11" => '11',
            "12" => '12',
            "13" => '13',
            "14" => '14',
            "15" => '15',
        ];

        return $years;
    }

    public static function months() {
        $months = [
            "00" => '0',
            "01" => '1',
            "02" => '2',
            "03" => '3',
            "04" => '4',
            "05" => '5',
            "06" => '6',
            "07" => '7',
            "08" => '8',
            "09" => '9',
            "10" => '10',
            "11" => '11',
        ];

        return $months;
    }

    public static function member_category() {
        $member_category = [
            '1' => 'BNI Member',
            '2' => 'Rotary Club',
            '3' => 'Lions Club',
            'other' => 'Other'
        ];

        return $member_category;
    }

    public static function year_of_built() {

        $year_of_built = [];
        for ($i = date('Y'); $i >= 1960; $i--) {
            $year_of_built[$i] = $i;
        }

        return $year_of_built;
    }

    public static function bussiness_category() {

        $bussiness_category = [
            "Appearance" => [
                '1' => 'Appearance Products',
                '2' => 'Appliance Sales',
                '3' => 'Image Consulting',
                '4' => 'Appearance Services',
                '5' => 'Hair Stylist',
                '6' => 'Cosmetics-Skin Care',
                '7' => 'Salon-Spa',
                '8' => 'Beauty Consultant',
            ],
            "Furniture" => [
                '9' => 'Furniture Sales',
                '10' => 'Modular Office Furniture',
                '11' => 'Seating Systems',
                '12' => 'Upholstery',
                '13' => 'Furniture Repair-Restoration',
                '14' => 'Chair Manufacturing',
            ],
            "Information Technology" => [
                '15' => 'Information Technology',
                '16' => 'ERP Software',
            ],
            "Financial" => [
                '17' => 'Loans',
                '18' => 'Financial Advisor/ Consultant',
                '19' => 'Financial Services',
                '20' => 'Corporate Loans',
                '21' => 'Company Secretary',
                '22' => 'Stock Broker',
                '23' => 'Home Loans',
                '24' => 'Certified Financial Planner',
                '25' => 'Investments',
                '26' => 'Bank Services',
                '27' => 'Retail (Consumer) Loans',
                '28' => 'Forex Dealer',
                '29' => 'Credit Services',
                '30' => 'Cash Flow Consulting',
                '31' => 'Business Equipment Financing',
                '32' => 'Credit Card, Merchant Services',
                '33' => 'Small Business Lending',
                '34' => 'Accounts Receivable-Factoing',
                '35' => 'Commercial Bank Services',
                '36' => 'Project Financing',
            ],
            "Employment" => [
                '37' => 'Human Resources',
                '38' => 'Employment Services',
                '39' => 'Employment Recruiter',
                '40' => 'Executive Search',
                '41' => 'Payroll Services',
                '42' => 'Employment-Temporary Service',
            ],
            "Medical" => [
                '43' => 'Medical Products',
                '44' => 'Dentist',
                '45' => 'Alternative Medicine',
                '46' => 'Physician',
                '47' => 'Optician',
                '48' => 'Medical Services',
                '49' => 'Physical Therapist',
                '50' => 'Naturopathic Medicine',
                '51' => 'Psychiatrist',
                '52' => 'Eye Care',
                '53' => 'Counselor-Psychotherapist',
                '54' => 'Gynecologist',
                '55' => 'Hearing Care',
                '56' => 'Medical Supplies',
                '57' => 'Pharmacist',
                '58' => 'Acupuncture',
                '59' => 'Psychotherapy',
            ],
            "Business" => [
                '60' => 'Manufacturing',
                '61' => 'Business Personal Assistant',
                '62' => 'Customs Broker/Freight Forwarding',
                '63' => 'Business Training',
                '64' => 'Business Advisor',
                '65' => 'Incorporating Services',
            ],
            "Architectural" => [
                '66' => 'Architectural Services',
                '67' => 'Architect',
                '68' => 'Architectural Interior Design',
                '69' => 'Landscape Planning',
            ],
            "Interior" => [
                '70' => 'Commercial Interior Design',
                '71' => 'Wall Coverings',
                '72' => 'Residential Interior Designs',
                '73' => 'Interior Designs',
                '74' => 'Kitchens',
                '75' => 'Home Furnishings',
                '76' => 'Tiles',
                '77' => 'Vaastu Consultant',
                '78' => 'Lighting',
                '79' => 'Arte-facts',
                '80' => 'Interior Decorating',
                '81' => 'Bedroom/Bath Accessories',
                '82' => 'Lighting-LED',
                '83' => 'Painting Decorative-Faux',
                '84' => 'Feng Shui Consultant',
                '85' => 'Motorized Window Covering',
                '86' => 'Lighting',
                '87' => 'Artificial Plants',
            ],
            'Printing' => [
                '88' => 'Printing Services',
                '89' => 'Printer Ink Cartridges',
                '90' => 'Printing Products',
                '91' => 'Offset Printing',
                '92' => 'Printing Supplies',
                '93' => 'Digital Printing',
                '94' => 'Screen Printing',
                '95' => 'Large Format Printing',
            ],
            'Gifts' => [
                '96' => 'Gifts',
                '97' => 'Choclatier',
                '98' => 'Awards- Trophies',
                '99' => 'Pens',
                '100' => 'Gifts Baskets',
                '101' => 'Greeting Cards',
            ],
            'Fire & Security' => [
                '102' => 'Security Products',
                '103' => 'Security Systems',
                '104' => 'CCTV',
                '105' => 'Security Guards',
                '106' => 'Fire Protection',
            ],
            'Insurance' => [
                '107' => 'Life Insurance',
                '108' => 'General Insurance',
                '109' => 'Health Insurance',
                '110' => 'Health, Life, Disability Insurance',
                '111' => 'Insurance Claims Assistance',
                '112' => 'Long Term Care Insurance',
                '113' => 'Insurance Public Adjuster',
            ],
            'Computer' => [
                '114' => 'Computer Sales',
                '115' => 'Web Development',
                '116' => 'Computer Software Sales Service',
                '117' => 'Computer Accessories',
                '118' => 'Web Hosting Design',
                '119' => 'Computer Networks',
                '120' => 'Web Design',
                '121' => 'Internet E-Business',
                '122' => 'Computer Repair',
                '123' => 'Computer Gaming',
                '124' => 'Internet Service Provider',
                '125' => 'Web Hosting',
                '126' => 'Computer Databases',
            ],
            'Advertising' => [
                '127' => 'Advertising Agency',
                '128' => 'Advertising Specialties-Promotional Products',
                '129' => 'Feature Films',
                '130' => 'Television Advertising',
                '131' => 'Publicist',
                '132' => 'Radio Advertising',
                '133' => 'Animated Films',
            ],
            'Moving & Storage' => [
                '134' => 'Moving Company',
                '135' => 'Storage Facility',
                '136' => 'Storage Containers',
            ],
            'Travel' => [
                '137' => 'Ticketing',
                '138' => 'Cruises, Tours',
                '139' => 'Travel Agent',
                '140' => 'Tour Guide, Operator',
                '141' => 'MICE',
            ],
            'Food / Drink' => [
                '142' => 'Baker',
                '143' => 'Caterer',
                '144' => 'Food-Beverage Products',
                '145' => 'Restaurant',
                '146' => 'Chef',
                '147' => 'Vending Machines',
            ],
            'Property' => [
                '148' => 'Residential Real Estate',
                '149' => 'Commercial Real Estate',
                '150' => 'Real Estate Development',
                '151' => 'Property Search',
                '152' => 'Apartments',
                '153' => 'Real Estate Planning Consultant',
                '154' => 'Home Interior Staging',
                '155' => 'Lease Negotiator',
                '156' => 'Relocation Services',
            ],
            'Media' => [
                '157' => 'Audio-Video Visual Media',
                '158' => 'Video Services',
                '159' => 'Media Services',
                '160' => 'Digital Imaging',
                '161' => 'Multimedia Productions',
                '162' => 'Audio Production',
                '163' => 'Home Automation',
                '164' => 'Editor',
            ],
            'Accounting' => [
                '165' => 'Chartered Accountant - Direct Taxes & Audits',
                '166' => 'Accounting Services',
                '167' => 'Chartered Accountant- Specialized Services',
                '168' => 'Bookkeeping',
                '169' => 'Taxes',
                '170' => 'Certified Public Accountant',
            ],
            'Jewelry' => [
                '171' => 'Diamond Jewelry',
                '172' => 'Jeweler',
                '173' => 'Jewelry-Fashion',
                '174' => 'Gemstones',
            ],
            'Consulting' => [
                '175' => 'Computer Software Consulting',
                '176' => 'Marketing Consulting',
                '177' => 'Internet Consulting',
                '178' => 'Energy Consulting',
                '179' => 'Business Consulting ',
                '180' => 'Consulting Services',
                '181' => 'Search Engine Optimization',
                '182' => 'Brand Consulting',
                '183' => 'Management Consulting',
                '184' => 'Sales/Training Consulting',
                '185' => 'Social Media Consulting',
                '186' => 'Health and Safety Consulting',
                '187' => 'Quality Systems Consulting',
            ],
            'Legal' => [
                '188' => 'Lawyer-Indirect Taxation',
                '189' => 'Advocate',
                '190' => 'Attorney-Family',
                '191' => 'Attorney-Real Estate',
                '192' => 'Legal Services Plans',
                '193' => 'Attorney-Intellectual Property',
                '194' => 'Attorney-Employment/Labor',
                '195' => 'Notary Public',
                '196' => 'Attorney-Litigation',
                '197' => 'Legal Document Preparation',
                '198' => 'Lawyer-Cyber Crime',
                '199' => 'Attorney-Business',
                '200' => 'Solicitor',
                '201' => 'Legal services Support',
                '202' => 'Trustee-Wills,Executor',
            ],
            'Health & Wellness' => [
                '203' => 'Nutrition',
                '204' => 'Water Purification',
                '205' => 'Meditation- Yoga',
                '206' => 'Health and Wellness Services',
                '207' => 'Health and Wellness Products',
                '208' => 'Health Spa',
                '209' => 'Fitness Trainer',
                '210' => 'Massage Therapist',
                '211' => 'Psychologist',
                '212' => 'Hypnotherapy',
                '213' => 'Homeopath',
                '214' => 'Health Club',
                '215' => 'Pregnancy Services-Birth Doula',
                '216' => 'Aroma Therapist',
                '217' => 'Hypnotist',
            ],
            'Automotive' => [
                '218' => 'Automotive Services',
                '219' => 'Automotive Parts/Accessories',
                '220' => 'Automotive Sales',
                '221' => 'Automotive Rental',
                '222' => 'Tires',
            ],
            'Photography' => [
                '223' => 'Photographer',
                '224' => 'Photography-Commercial',
                '225' => 'Photography Products',
                '226' => 'Photography -Portrait',
                '227' => 'Photo-processing',
            ],
            'Security' => [
                '228' => 'Security Systems',
                '229' => 'Security Products',
                '230' => 'Fire Protection',
                '231' => 'Locksmith',
                '232' => 'CCTV',
                '233' => 'Security Personnel',
                '234' => 'Security Guards',
                '235' => 'Detective Services',
                '236' => 'Identity Theft',
            ],
            'Trades' => [
                '237' => 'Alternate Energy (Wind & Solar)',
                '238' => 'Civil Contractor',
                '239' => 'Building Materials',
                '240' => 'Doors',
                '241' => 'Window-Doors',
                '242' => 'Builder Residential',
                '243' => 'Heating-Air Conditioning- HVAC',
                '244' => 'Central Air Conditioning',
                '245' => 'Building Products',
                '246' => 'Flooring: Carpet, Tile, Wood, etc',
                '247' => 'Wood',
                '248' => 'Waterproofing-Weatherproofing',
                '249' => 'Electrical Goods',
                '250' => 'Electrical Contractor',
                '251' => 'Steel Fabrication',
                '252' => 'Solar',
                '253' => 'Plumber',
                '254' => 'Electrical Consultant',
                '255' => 'Concrete Pouring',
                '256' => 'Builder Commercial',
                '257' => 'Construction Products',
                '258' => 'Construction Project Management',
                '259' => 'Roofing-Gutters',
                '260' => 'Pools & Hot Tub Services',
                '261' => 'Renovations-Remodeling',
                '262' => 'Building Supplies',
                '263' => 'Windows',
                '264' => 'Painter Commercial/Residential',
                '265' => 'Paving',
                '266' => 'Carpenter',
                '267' => 'Pools & Hot Tub Sales',
                '268' => 'Plasterer',
                '269' => 'Stonemason',
                '270' => 'Garage Doors',
                '271' => 'Cabinets-Closets',
                '272' => 'Fireplace-Chimney',
                '273' => 'Excavation',
                '274' => 'Outdoor Fireplaces-Wells',
                '275' => 'Gas',
                '276' => 'Fences-Decks',
                '277' => 'Counter Tops',
            ],
            'Marketing' => [
                '278' => 'Marketing Services',
                '279' => 'Branding',
                '280' => 'Trade Show-Exhibitions',
                '281' => 'Market Research',
                '282' => 'Content Writing/Development',
                '283' => 'PR Consultant',
            ],
            'Food/Beverages' => [
                '284' => 'Caterer',
                '285' => 'Baker',
                '286' => 'Vending Machines',
                '287' => 'Food-Beverage Products',
                '288' => 'Restaurant ',
                '289' => 'Food-Beverage Services',
                '290' => 'Wine Importer/Merchant',
            ],
            'Transportation' => [
                '291' => 'Commercial Transportation',
                '292' => 'Aircraft',
                '293' => 'Vehicle Rentals',
                '294' => 'Taxi Service',
                '295' => 'Parking-Shuttle Services',
            ],
            'Telecommunications' => [
                '296' => 'SMS Service Provider',
                '297' => 'Phone Services',
                '298' => 'Telecommunication Systems',
                '299' => 'Telecommunication Products',
                '300' => 'Telecommunication Services',
                '301' => 'Mobile Telecommunication',
                '302' => 'Wireless Services',
                '303' => 'Cabling-Telephones-Computer',
            ],
            'Window' => [
                '304' => 'Window Treatments',
            ],
            'Landscaping' => [
                '305' => 'Landscape Products',
                '306' => 'Landscape Design',
                '307' => 'Landscape Contractors',
            ],
            'Event Planning' => [
                '308' => 'Event Planning',
                '309' => 'Social Events',
                '310' => 'Corporate Events',
            ],
            'Entertainment' => [
                '311' => 'Choreographer',
                '312' => 'Entertainment Services',
                '313' => 'Musician',
                '314' => 'Shows',
                '315' => 'Magician',
                '316' => 'Disc Jockey',
                '317' => 'Entertainment Products',
            ],
            'Real Estate' => [
                '318' => 'Residential Real Estate',
                '319' => 'Real Estate Development',
                '320' => 'Commercial Real Estate',
                '321' => 'Real Estate Planning Consultant',
                '322' => 'Home Inspector',
                '323' => 'Property Management',
                '324' => 'Home Warranty Services',
                '325' => 'Real Estate Investments',
                '326' => 'Apartments',
                '327' => 'Relocation Services',
            ],
            'Electronics' => [
                '328' => 'Electronics',
                '329' => 'UPS / Inverter Sales',
                '330' => 'Home Appliances',
            ],

            'Cleaning' => [
                '331' => 'Cleaning Maintenance',
                '332' => 'Cleaning-Carpets-Floors-Upholstery',
                '333' => 'Cleaning Products',
                '334' => 'Dry Cleaning',
                '335' => 'Laundry',
                '336' => 'Janitorial-Commercial Cleaning',
            ],
            'Engineering' => [
                '337' => 'Engineering',
                '338' => 'Surveyor',
            ],
            'Coach' => [
                '339' => 'Business Coach',
                '340' => 'Leadership development',
                '341' => 'Life Coach',
                '342' => 'Career Coach',
                '343' => 'Sales Coach',
                '344' => 'Communication Coach',
            ],
            'Sports' => [
                '345' => 'Sporting Goods',
                '346' => 'Sports Activities',
                '347' => 'Sports Instructor',
                '348' => 'Martial Arts',
            ],
            'Educational' => [
                '349' => 'Educational',
                '350' => 'Art Educational',
            ],
            'Recreation' => [
                '351' => 'Club',
                '352' => 'Recreation Activities',
                '353' => 'Recreation RVs-Campers',
            ],
            'Graphic Design' => [
                '354' => 'Graphic Design',
            ],
            'Mortgage' => [
                '355' => 'Residential Mortgages',
                '356' => 'Mortgages',
                '357' => 'Commercial Mortgage',
            ],
            'Mail Services' => [
                '358' => 'Mailing Services',
                '359' => 'Mailing -Packing-Copy Services',
            ],
            'Flowers' => [
                '360' => 'Florist',
            ],
            'Glass' => [
                '361' => 'Glass Company',
                '362' => 'Stained Glass',
                '363' => 'Glass Auto',
            ],
            'Office' => [
                '364' => 'Office Services',
                '365' => 'Office Furnishings',
                '366' => 'Office Supplies',
                '367' => 'Office Equipment',
                '368' => 'Office Machines',
                '369' => 'Data Services',
            ],
            'Pest Control' => [
                '370' => 'Pest Control',
            ],
            'Apparel' => [
                '371' => 'Custom Apparel',
                '372' => 'Apparel',
                '373' => 'Shoes',
                '374' => 'Womens Apparel',
                '375' => 'Apparel Accessories',
                '376' => 'Formal Wear',
            ],
            'Personal Services' => [
                '377' => 'Tarot Card Reader',
                '378' => 'Mediation Services',
                '379' => 'Handwriting Analyst',
                '380' => 'Astrologist',
                '381' => 'Numerologist',
                '382' => 'Voice Coach',
                '383' => 'Home Shopping',
                '384' => 'Yoga Instructor/Trainer',
                '385' => 'Adult Day Care',
                '386' => 'Counseling Services',
                '387' => 'Personal Trainer',
                '388' => 'In-Home Non-Medical Services',
            ],
            'Environmental' => [
                '389' => 'Chemical Manufacturer',
                '390' => 'Environmental Services',
                '391' => 'Environmental Products',
                '392' => 'Waste Disposal Services',
                '393' => 'Water Systems',
                '394' => 'Protective Equipment',
            ],

            'Facilities' => [
                '395' => 'Facilities',
                '396' => 'Lodging-Conference Facilities',
                '397' => 'Health Care Facilities',
            ],
            'Deliveries' => [
                '398' => 'Shipping',
                '399' => 'Delivery-Courier Service',
            ],
            'Child Services' => [
                '400' => 'Childrens Products',
                '401' => 'Childrens Services',
                '402' => 'Child Day Care',
            ],
            'Speaking' => [
                '403' => 'Seminars',
                '404' => 'Speaker',
            ],
            'Art' => [
                '405' => 'Artist',
                '406' => 'Art & Craft Supplies',
                '407' => 'Painter Artistic',
                '408' => 'Quilts',
                '409' => 'Art Gallery',
                '410' => 'Art Sales',
                '411' => 'Picture Framing',
                '412' => 'Antiques',
            ],
            'Public Relations' => [
                '413' => 'Public Relations',
            ],
            'Equipment' => [
                '414' => 'Equipment Sales-Services',
                '415' => 'Equipment Rental-Leasing',
            ],
            'Wedding' => [
                '416' => 'Wedding Planner',
            ],
            'Appliances' => [
                '417' => 'Appliance Repair',
            ],
            'Non-Profit Organization' => [
                '418' => 'Non-Profit Organization',
                '419' => 'Charitable-Fund raising',
                '420' => 'Charitable-Fund raising Organization',
            ],
            'Animals' => [
                '421' => 'Pet Care',
                '422' => 'Animals Products',
                '423' => 'Animals Services',
                '424' => 'Veterinarian',
            ],
            'Signs' => [
                '425' => 'Sign Company',
            ],

            'Fuels' => [
                '426' => 'Fuels',
            ],
            'Clothing' => [
                '427' => 'Apparel',
                '428' => 'Womens Apparel',
                '429' => 'Custom Apparel',
                '430' => 'Formal Wear',
                '431' => 'Apparel Accessories',
                '432' => 'Shoes',
            ],
            'Car/Vehicle' => [
                '433' => 'Automotive Services',
                '434' => 'Automotive Detailing ',
                '435' => 'Automotive Sales',
                '436' => 'Automotive Repair',
                '437' => 'Tires',
                '438' => 'Automotive Parts/Accessories',
                '439' => 'Automotive Rental',
            ],
            'Auction' => [
                '440' => 'Auction Real Estate',
            ],
            'Writing Services' => [
                '441' => 'Writing Services',
                '442' => 'Writer',
            ],
            'Agricultural' => [
                '443' => 'Agricultural',
            ],
            'Engraving' => [
                '444' => 'Engraving Services',
            ],
            'Appraisal' => [
                '445' => 'Appraisal Real Estate',
                '446' => 'Valuer',
            ],
            'Events' => [
                '447' => 'Corporate Events',
                '448' => 'Social Event',
                '449' => 'Event Planning',
            ],
            'Organizing' => [
                '450' => 'Organizing Professional',
            ],
            'Language' => [
                '451' => 'Language Services',
            ],
            'Books' => [
                '452' => 'Books',
            ],
            'Embroidery' => [
                '453' => 'Embroidery',
            ],
            'Fuels' => [
                '454' => 'Fuels',
            ],
            'Barter Exchange' => [
                '455' => 'Barter Exchange',
            ],
        ];

        return $bussiness_category;
    }

    public static function company_type() {
        $company_type = [
            "1" => "RPSL Company",
            "2" => "Non RPSL Company",
            "3" => "Foreign Company",
            "4" => "ITF Company",
            "5" => "Indian Company",
        ];
        return $company_type;
    }

    public static function ship_type() {

        $ship_type = [
            "1" => "LPG Tanker",
            "2" => "LNG Tanker",
            "3" => "Chemical Tanker",
            "4" => "Oil Tanker",
            "5" => "Product Tanker",
            "6" => "Crude Oil",
            "7" => "Bituman Tanker",
            "8" => "ULCC",
            "9" => "VLCC",
            "10" => "FPSO/ FSO",
            "11" => "Tug Harbour",
            "12" => "Tug ASD 180 Harbour",
            "13" => "Tug ASD 360 Harbour",
            "14" => "Tug Tractor Harbour",
            "15" => "Tug Seagoing",
            "16" => "Tug Towing",
            "17" => "Tug Anchor Handling",
            "18" => "Supply Vessel",
            "19" => "Research Vessel",
            "20" => "Anchor Handling",
            "21" => "Anchor Handling DP I",
            "22" => "Anchor Handling DP II",
            "23" => "Anchor Handling DP III",
            "24" => "OSV",
            "25" => "OSV DP I",
            "26" => "OSV DP II",
            "27" => "OSV DP III",
            "28" => "PSV",
            "29" => "PSV DP I",
            "30" => "PSV DP II",
            "31" => "PSV DP III",
            "32" => "DSV",
            "33" => "DSV DP I",
            "34" => "DSV DP II",
            "35" => "DSV DP III",
            "36" => "Container",
            "37" => "Bulk Carrier",
            "38" => "Car Carrier",
            "39" => "Ore Carrier",
            "40" => "Wood/Log Carrier",
            "41" => "OBO",
            "42" => "RORO",
            "43" => "General Cargo",
            "44" => "Heavy Lift",
            "45" => "Live Stock",
            "46" => "Reefer",
            "47" => "Derdger",
            "48" => "Derdger Cutter Suction",
            "49" => "Derdger TSHD",
            "50" => "Derdger Reclamation",
            "51" => "Derdger Augur Suction",
            "52" => "Drill Ship",
            "53" => "Oil Platform",
            "54" => "Rig",
            "55" => "Jackup Rig",
            "56" => "Submersible drilling Rig",
            "57" => "Semi-Submersible",
            "58" => "ROV Support Vessel",
            "59" => "Pipe Laying Vessel",
            "60" => "Barge",
            "61" => "Jack Up Barge",
            "62" => "Accomadation Barge",
            "63" => "Cruise Ship",
            "64" => "Passenger Vessel",
            "65" => "Crew Boat",
            "66" => "Yatch",
            "67" => "Ferry Boat",
            "68" => "Fishing Vessel",
        ];

        return $ship_type;
    }

    public static function institute_course_types() {
        $institute_course_types = [
            "1" => "Basic Modular Course",
            "2" => "Advanced Modular Course",
            "3" => "Add-On Course",
            "4" => "Competency Course / Preparatory Course",
            "5" => "Revalidation & Upgradation Course",
            "6" => "Offshore Course",
            "7" => "Simulator Course",
            "8" => "DP Course",
            "9" => "Packaged Course",
            "10" => "Management Courses",
            "11" => "Welding & Fitting Course",
            "12" => "Pre Sea Courses",
        ];
        return $institute_course_types;
    }


    public static function course_types_by_institute_id($institute_id) {
        $result = \DB::table('courses_type_master')
            ->join('institute_courses', 'courses_type_master.id', '=', 'institute_courses.course_type')
            ->select('courses_type_master.id', 'courses_type_master.course_type')->distinct()->get();

        return $result;
    }
    
    public static function courses_by_table() {
        $result = \DB::table('courses')
            ->select('courses.id', 'courses.course_name')
            ->get();
        return $result;
    }
    
    public static function course_data($courseId) {
        $result = \DB::table('courses')
        ->select('courses.id', 'courses.course_type', 'courses.course_name')
        ->where('id', $courseId)
        ->first();
        return $result;
    }

    public static function course_name_by_course_type($course_type_id) {
        $result = \DB::table('courses')->select('courses.id', 'courses.course_name')
            ->where('course_type', $course_type_id)->get();

        return $result;
    }

    public static function moneyFormatIndia($current_num) {

        $input_number = number_format((float)$current_num, 2, '.', '');
        $explrestunits = "";
        $temp_num = explode('.', $input_number);
        $num = $temp_num[0];
        if (strlen($num) > 3) {
            $lastthree = substr($num, strlen($num) - 3, strlen($num));
            $restunits = substr($num, 0, strlen($num) - 3); // extracts the last three digits
            $restunits = (strlen($restunits) % 2 == 1) ? "0" .
                $restunits : $restunits; // explodes the remaining digits in 2's formats, adds a zero in the beginning to maintain the 2's grouping.
            $expunit = str_split($restunits, 2);
            for ($i = 0; $i < sizeof($expunit); $i++) {
                // creates each of the 2's group and adds a comma to the end
                if ($i == 0) {
                    $explrestunits .= (int)$expunit[$i] . ","; // if is first value , convert into integer
                } else {
                    $explrestunits .= $expunit[$i] . ",";
                }
            }
            $thecash = $explrestunits . $lastthree;
        } else {
            $thecash = $num;
        }

        if (count($temp_num) > 1) {
            $decimal_value = strlen($temp_num[1]) > 1 ? $temp_num[1] : $temp_num[1] . '0';
            $thecash = $thecash . '.' . $decimal_value;
        }
        return $thecash; // writes the final format where $currency is the currency symbol.
    }

    public static function generateRandomCartKey() {
        $time = strtotime(Date('Y-m-d H:i:s'));
        $rand_num = rand(000, 999);
        return $time . $rand_num;
    }

    public static function generateRandomString($string) {
      
          // $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
          $charactersLength = strlen($string);
          $randomString = '';
          for ($i = 0; $i < $charactersLength; $i++) {
              $randomString .= $string[rand(0, $charactersLength - 1)];
          }
          return $randomString;
      }
    public static function marital_status() {
        $marital_status = [
            1 => 'Single',
            2 => 'Married',
            3 => 'Single Parent',
        ];
        return $marital_status;
    }

    public static function userSubscriptionStatus() {
        $user_subscription_status = [
            0 => 'initiated',
            1 => 'active',
            2 => 'expired',
            3 => 'pending',
        ];

        return $user_subscription_status;
    }

    public static function productOrderStatus() {
        $status = [
            0 => 'Order Initiated',
            1 => 'Order Confirmed',
            2 => 'Order Failed',
            3 => 'Order Cancelled',
        ];
        return $status;
    }

    public static function paymentModes() {
        $modes = [
            'CC' => 'Credit Card',
            'DC' => 'Debit Card',
            'NB' => 'Net Banking',
            'WALLET' => 'Wallet'
        ];
        return $modes;
    }

    public static function paymentStatus() {
        $status = [
            0 => 'Initiated',
            1 => 'Complete',
            2 => 'Failed',
            3 => 'Refund Initiated',
            4 => 'Refund Complete',
            5 => 'Refund Failed',
        ];
        return $status;
    }

    public static function seafarer_job_category() {
        $category = [
            "1" => "Sea Going Jobs",
            "2" => "Offshore Jobs",
            "3" => "Rig Jobs",
            "4" => "Cruise Jobs",
            "5" => "Shore Jobs",
            "6" => "Harbour",
        ];
        return $category;
    }

    public static function feature_name_to_show() {
        return [
            config('feature.feature1') => 'CV download',
            config('feature.feature2') => 'email',
            config('feature.feature3') => 'active advertisement',
        ];
    }

    public static function get_feature_name_to_show($feature) {
        $all_feature = \CommonHelper::feature_name_to_show();

        return $all_feature[$feature];
    }

    public static function engine_type($userId = null) {
        if($userId == null){
            $userId = Auth::user()->id;
        }
        // $engine_type = [
        //     "1" => 'MAN',
        //     "2" => 'B & W',
        //     "3" => 'MAN B & W',
        //     "4" => 'Suzler',
        //     "5" => 'Pielstick',
        //     "6" => 'Cummins',
        //     "7" => 'Akasaka',
        //     "8" => 'H & W',
        //     "9" => 'Cater Pillar',
        //     "10" => 'Yanmar',
        //     "other" => 'Other',
        // ];
        $engine_type = EnginType::where('user_id',$userId)->orWhereNull('user_id')->pluck('name','id')->toArray();
        return $engine_type;
    }

    public static function engine_type_admin() {

        $engine_type = EnginType::pluck('name','id')->toArray();
        return $engine_type;
    }
    
    public static function engine_type_by_user_id($userId) {

        // $engine_type = [
        //     "1" => 'MAN',
        //     "2" => 'B & W',
        //     "3" => 'MAN B & W',
        //     "4" => 'Suzler',
        //     "5" => 'Pielstick',
        //     "6" => 'Cummins',
        //     "7" => 'Akasaka',
        //     "8" => 'H & W',
        //     "9" => 'Cater Pillar',
        //     "10" => 'Yanmar',
        //     "other" => 'Other',
        // ];
        $engine_type = EnginType::where('user_id',$userId)->orWhereNull('user_id')->pluck('name','id')->toArray();
        return $engine_type;
    }

    public static function courses() {
        $courses = [
            "1" => "Personal Survival Techniques",
            "2" => "Fire Prevention and Fire Fighting",
            "3" => "Elementary First Aid",
            "4" => "Personal Safety/Social Responsibility",
            "5" => "Designated Security Duties / STSDSD",
            "6" => "Proficiency in Survival Craft / PSCRB",
            "7" => "Advanced Fire Fighting",
            "8" => "Medical First Aid",
            "9" => "Medical Care On Board",
            "10" => "Ship Security Officer",
            "11" => "Ship Board Safety Officer",
            "12" => "Ship safety officer",
            "13" => "Oil Tanker Familiarization",
            "14" => "Advanced Oil Tanker Course",
            "15" => "Chemical Familiarization Course",
            "16" => "Advanced Chemical Training",
            "17" => "Gas Familiarization",
            "18" => "LPG Familiarization",
            "19" => "Advance Gas Course",
            "20" => "Advance LPG Course",
            "21" => "Radar Navigator / Observer",
            "22" => "Radar simulator (Ransco)",
            "23" => "Ship handling simulator",
            "24" => "Ship Simulator",
            "25" => "ARPA",
            "26" => "Bridge Team and Resource Management",
            "27" => "Bridge Team Management",
            "28" => "Engine Room Resource Management",
            "29" => "Engine Room Simulator",
            "30" => "LCHS",
            "31" => "ECDIS",
            "32" => "Ships Cook Training Certificate",
            "33" => "Steward / Messman certificate",
            "34" => "English Course",
            "35" => "Tanker Endorsement",
            "36" => "Advance Tanker Endorsement",
            "37" => "Chemical Endorsement",
            "38" => "Advance Chemical Endorsement",
            "39" => "LPG/Gas Endorsement Endorsement",
            "40" => "Advance LPG/Gas Endorsement",
        ];
        return $courses;
    }

    public static function value_added_courses() {
        $value_added_courses = [
            "1" => "Refresher & Updating Training",
            "2" => "Helicopter Underwater Escape Training",
            "3" => "Dynamic Positioning Basic(Induction) Course",
            "4" => "DP Advanced Simulator Course",
            "5" => "DP Unlimited",
            "6" => "DP Limited",
            "7" => "Hydrogen Sulfide Safety Training Certificate (H2O)",
            "8" => "ISPS Code Familiarization",
            "9" => "Cargo & Ballast Handling Simulator",
            "10" => "Tripod Incident Investigation",
            "11" => "Risk Assessment",
            "12" => "Ship Board Safety officer",
            "13" => "Ship Security officer",
            "14" => "Crowd Management",
            "15" => "Crisis Management & Human Behaviour",
            "16" => "Pax Safety, Cargo Safety & Hull Integrity",
            "17" => "Pax Safety",
            "18" => "Familiarization Training",
            "19" => "Safety Training",
            "20" => "Ship Handling Simulator Course",
        ];
        return $value_added_courses;
    }

    public static function certificate_issued_by() {
        $certificate_issued_by = [
            "1" => "SCMS MARITIME TRAINING INS",
            "2" => "MMTI (MUMBAI)",
            "3" => "IIMS (MUMBAI)",
            "4" => "ARK MARINE",
            "5" => "MMTI",
        ];
        return $certificate_issued_by;
    }

    public static function issuing_authority() {
        $issuing_authority = [
            "1" => "D.G. Shipping",
            "2" => "M.M.D.(GOA)",
            "3" => "M.M.D.(MUMBAI)",
            "4" => "ARK MARINE",
            "5" => "MMTI",
        ];
        return $issuing_authority;
    }

    public static function work_experience() {
        $work_experience = [
            "0-1" => "0-1 Years",
            "1-2" => "1-2 Years",
            "2-3" => "2-3 Years",
            "3-4" => "3-4 Years",
            "4-" => "4+ Years",
        ];
        return $work_experience;
    }

    public static function institute_branch_type() {
        $institute_branch_type = [
            "1" => "Office Only",
            "2" => "Training Center",
            "3" => "Office + Training Center",
        ];
        return $institute_branch_type;
    }

    public static function order_status() {
        $order_status = [
            "0" => "Pending",
            "1" => "Payment Successful",
            "2" => "Payment Failed",
            "3" => "Cancelled",
            "4" => "Approved",
            "5" => "Denied",
            "6" => "Partial Payment Successful",
        ];
        return $order_status;
    }

    public static function req_documents() {
        $req = [
            "1" => "Photograph",
            "2" => "Education Certificate",
            "3" => "Passport",
            "4" => "INDOS",
            "5" => "CDC",
            "6" => "COC",
            "7" => "GMDSS",
            "8" => "GOC",
            "9" => "Training Record Book",
            "10" => "Sea Service letter",
            "11" => "ILO Fit Medical Cert",
            "12" => "Personal Survival Techniques (PST)",
            "13" => "Personal Safety and Social Responsibilities (PSSR)",
            "14" => "Elementary First Aid (EFA)",
            "15" => "Fire Fighting and Fire Prevention (FPFF)",
            "16" => "Basic STCW Courses (PSSR + PST + EFA + FPFF)",
            "17" => "PSSR PST + EFA + FPFF + STSDSD",
            "18" => "Security Training for Seafarers with Designated Security Duties (STSDSD)",
            "19" => "Augmentation of Fire Prevention & Fire Fighting (AUGFF)",
            "20" => "Basic Training in Oil and Chemical Tanker Cargo Operations (OCTCO)",
            "21" => "Basic Training in Oil Tanker Familiarization (OTFC)",
            "22" => "Basic Training in Chemical Tanker Familiarization (CTF)",
            "23" => "Basic Training in Gas Tanker Familiarization (GTF)",
            "24" => "Basic Training in Liquefied Gas Tanker Familiarization (LGTF)",
            "25" => "Passenger Ship Familiarization (PSF)",
            "26" => "Passenger Ship Familiarization Crowd Management (PSFCM)",
            "27" => "Medical Examiners Familiarization (MEF)",
            "28" => "Advance Fire Fighting (AFF)",
            "29" => "Medical First Aid (MFA)",
            "30" => "Augmentation of Fire Fighting and Fire Prevention (Aug FPFF)",
            "31" => "Basic Training in Oil Tanker Familiarization (OTFC)",
            "32" => "Masters Medical Care (MC)",
            "33" => "Proficiency in Survival Craft & Rescue Boats (PSCRB)",
            "34" => "Ship Security Officer (SSO)",
            "35" => "Ship Master Medicare (SMM)",
            "36" => "Passenger Ship Familiarization (PSF)",
            "37" => "Practical Tanker Fire Fighting Course for Tanker Cargo Operations (PTFF)",
            "38" => "Specialized Training on Chemical Tanker Cargo Operations (CHEMCO)",
            "39" => "Specialized Training on Liquefied Gas Tanker Cargo Operations (GASCO)",
            "40" => "Specialized Training on Oil Tankers Cargo Operations (STPOTO / TASCO)",
            "41" => "Bridge Resource Management (BRM)",
            "42" => "Engine Room Resource Management (ERRM)",
            "43" => "Bridge & Engine Room Resource Management (BERRM)",
            "44" => "Bridge Team Management (BTM)",
            "45" => "Bridge Team & Resource Management (BTRM)",
            "46" => "Bridge Team Management & Ship Handling (BTMSH)",
            "47" => "High Voltage (HV)",
            "48" => "High Voltage Safety & Switch Gear Operational Level (HVSGO)",
            "49" => "High Voltage Safety & Switch Gear Management Level (HVSGM)",
            "50" => "Enclosed Space Entry (ESE)",
            "51" => "Abrasive Wheel Safety (AWS)",
            "52" => "FRAMO",
            "53" => "Shipboard Safety Officer (SBSO)",
            "54" => "Ice Navigation (ICE)",
            "55" => "Advance Fire Fighting Value Added (AFF VA)",
            "56" => "Proficiency in Survival Craft and Rescue Boats Value Added (PSCRB VA)",
            "57" => "Maritime English (ME)",
            "58" => "Oil Pollution Act (OPA 90)",
            "59" => "ISM Awareness",
            "60" => "Masters Advanced Shipboard Management (ASM)",
            "61" => "Chief Mate Phase I (CMFG 1)",
            "62" => "Chief Mate Phase II (CMFG 2)",
            "63" => "2nd Mate Function (2MFG)",
            "64" => "NWKO NCV",
            "65" => "Preparatory Course for MEO Class II",
            "66" => "Preparatory Course for MEO Class I",
            "67" => "ETO Orals",
            "68" => "Personal Survival Techniques Refresher (PST [R])",
            "69" => "Fire Prevention & Fire Fighting Refresher (FPFF [R])",
            "70" => "Advance Fire Fighting Refresher (AFF [R])",
            "71" => "Medical First Aid Refresher (MFA [R])",
            "72" => "Ship Master Medicare Refresher (SMM [R])",
            "73" => "Ship Security Officer Refresher (SSO [R])",
            "74" => "Crane Operator Refresher (CO [R])",
            "75" => "Forklift Operator Refresher (FLO [R])",
            "76" => "Bridge Team Management Refresher (BTM [R])",
            "77" => "Bridge Resource Management Refresher (BRM [R])",
            "78" => "Bridge Team & Resource Management Refresher (BTRM [R])",
            "79" => "Bridge & Engine Room Resource Management Refresher (BERM [R])",
            "80" => "Offshore Emergency Response Team Member Refresher (OERTM [R])",
            "81" => "Offshore Emergency Response Team Leader Refresher (OERTL [R])",
            "82" => "Offshore Emergency Helideck Team Member Refresher (OEHTM [R])",
            "83" => " Radar Arpa Navigation & Simulator Course Refresher (RANSCO [R])",
            "84" => " Electronic Chart Display & Information System Refresher (ECDIS [R])",
            "85" => " Engineer Refresher (Class IV)",
            "86" => "Offshore Emergency Response Team Member Refresher (OERTM [R])",
            "87" => " Engineer Refresher (Class I & Class II)",
            "88" => " Crane Operator (CO)",
            "89" => " Forklift Operator (FLO)",
            "90" => " Helicopter Underwater Escape Training (HUET)",
            "91" => " Helicopter Underwater Escape & Sea Survival (HUESS)",
            "92" => " Helicopter Landing Officer (HLO)",
            "93" => "Helicopter Landing Officer Further Training (HLOFT)",
            "94" => "Helicopter Refuelling (HR)",
            "95" => "Helideck Assistant (HDA)",
            "96" => "Hydrogen Sulphide Safety (H2S)",
            "97" => "Hazardous Materials (HAZMAT)",
            "98" => "Fast Rescue Craft/Boat (FRC/FRB)",
            "99" => "Offshore Lifeboat Coxwain (OLC)",
            "100" => "Offshore Lifeboat Coxwain Further Training (OLCFT)",
            "101" => "Basic Offshore Safety Emergency Training (BOSIET)",
            "102" => "Further Offshore Emergency Training (FOET)",
            "103" => "Offshore Emergency Response Team Member (OERTM)",
            "104" => "Offshore Emergency Response Team Leader (OERTL)",
            "105" => "Offshore Emergency Helideck Team Member (OEHTM)",
            "106" => "Automatic Radar Plotting Aids (ARPA)",
            "107" => "Radar Observer Simulator (ROS)",
            "108" => "Radar Arpa Navigation & Simulator Course (RANSCO)",
            "109" => "Global Maritime Distress & Safety System (GMDSS)",
            "110" => "Electronic Chart Display & Information System (ECDIS)",
            "111" => "Electronic Chart Display & Information System Type Specific (ECDIS TS)",
            "112" => "Ship Manoeuvring Simulator & Bridge Team Work (SMS & BTW)",
            "113" => "Tug Simulator (TS)",
            "114" => "DP Introduction",
            "115" => "DP Advanced",
            "116" => "Company Security Officer (CSO)",
            "117" => "Port Facility Security Officer (PFSO)",
            "118" => "Flag State Inspection (FSI)",
            "119" => "Vertical Integration Course for Trainers (VICT/TOTA)",
            "120" => "Welding Safety (WS)",
            "121" => "Pumpman Training (PUMP)",
            "122" => "Diploma in Nautical Science (DNS)",
            "123" => "BSC Nautical Science",
            "124" => "Certificate Course in Maritime Catering (CCMC)",
            "125" => "Orientation Course for Catering Personnel (OCCP)",
            "126" => "Diploma in Marine Engineering (DME)",
            "127" => "Trainee Marine Engineering (TME)",
            "128" => "B.Tech Marine Engineering",
            "129" => "Electro Technical Officer (ETO)",
        ];
        return $req;
    }

    public static function approved_by() {
        $approved_by = [
            "1" => "DG India",
            "2" => "OPITO",
            "3" => "Value Added"
        ];
        return $approved_by;
    }

    public static function dd($array, $array1 = NULL) {
        echo "<pre>";
        print_r($array);
        echo "/<pre>";

        if (!empty($array1)) {
            echo "<pre>";
            print_r($array1);
            echo "/<pre>";
        }
        exit;
    }

    public static function encodeKey($string) {
        return self::encodeDecodeKey($string, 'encode');
    }

    public static function decodeKey($string) {
        $decodeKey = self::encodeDecodeKey($string, 'decode');
        return is_array($decodeKey) ? array_first($decodeKey) : $decodeKey;
    }

    protected static function encodeDecodeKey($string, $action) {
        try {
            $hasIds = new Hashids(config('app.key'), 10, 'abcdefghijklmnopqrstuvwxyz1234567890');
            return $hasIds->{$action}($string);
        } catch (\Exception $exception) {
            throw $exception;
        }
    }

    public static function getResumeNumber($current_rank = null) {
        $resume_1 = range(1, 4);
        array_push($resume_1, 7, 8, 24, 25);
        $resume_2 = [5, 6, 9, 10, 11, 54, 12, 13, 16, 17, 26, 27, 28, 53, 30];
        $resume_3 = [14, 15, 19, 20, 21, 22, 18, 35, 36, 37, 41, 44, 45, 46, 47, 48, 50, 51, 52, 29, 34, 31, 32, 33, 38, 39, 40, 42, 43, 49, 23];
        if($current_rank == null){
            $current_rank = Auth::user()->professional_detail()->first()->current_rank;
        }
        $show_resume = 3;
        if (in_array($current_rank, $resume_1))
            return $show_resume = 1;
        elseif (in_array($current_rank, $resume_2)) 
            return $show_resume = 2;
        else 
            return $show_resume = 3;
    }

    public static function checkImageSize($fileSize){
        $maxSize = Auth::user()->max_kb;
        $usedSize = Auth::user()->used_kb;
        $totalUsedSize = $maxSize - ($usedSize + $fileSize);
        $status = true;
        if($totalUsedSize <= 0){
            $status = false;
        }
        return ['status'=>$status];
    }
    
    public static function getRankOfRequiredSeaServiceForResume(){
        $rankList = [
            1,
            2,
            3,
            4,
            7,
            8,
            24,
            25,
            5,
            6,
            9,
            10,
            11,
            54,
            12,
            13,
            16,
            17,
            26,
            27,
            28,
            53,
            30
        ];
        return $rankList;
    }
    
    public static function getRankOfRequiredPassportCountryForResume(){
        $rankList = [
            14,
            15,
            19,
            18,
            20,
            21,
            22,
            29,
            34,
            53,
            32,
            33,
            23,
            35,
            36,
            37,
            41,
            44,
            45,
            46,
            47,
            48,
            50,
            51,
            52,
            38,
            39,
            40,
            42,
            43,
            49
            
        ];
        return $rankList;
    }
    
    public static function report_issue(){
        $reportIssue = [
            'Edit Profile',
            'Upload Document',
            'Download Document',
            'Share Details',
            'Onboard Feedback',
            'My Document',
            'Onboard Experience',
            'Infographics',
            'My Profile',
            'My Resume',
            'New Jobs',
            'Share Profile',
            'Share Resume',
            'Share Document',
            'Other'
        ];
        return $reportIssue;
    }
    
    public static function report_sub_issue() {
        $report_sub_issue = [
            'Edit Profile' => [
                'I am getting page error',
                'Trouble using Edit Profile',
                'Can’t find my Postal code',
                'My Postal code shows wrong State / City',
                'Can’t find my Rank',
                'Can’t find Course',
                'Can’t find Ship Type',
                'Can’t find option in Medical',
                'Can’t find Vaccination in list',
                'Other'
            ],
            'Upload Document' => [
                "I am getting page error",
                "There is no container for my documents",
                "Can not upload",
                "Need to upload more documents",
                "Can't change visibility to Public",
                "Can not change visibility to Private",
                "My Used Storage is wrong",
                "Need more storage capacity"
            ],
            'Download Document' => [
                "Uploaded document not showing",
                "Can not download document ",
                "Other"
            ],
            'Share Details' => [
                "Can’t share Resume",
                "Can’t share Documents",
                "Can’t share Profile",
                "While Sharing can’t view created contact",
                "Issue while creating / editing contacts",
                "My Contact data is wrong",
                "Resume history is incorrect",
                "QR not showing",
                "Document history is incorrect",
                "Can’t delete records",
                "Other"
            ],
            'Onboard Feedback' => [
                "Can’t find my Sea Service",
                "Can’t submit Job Profile",
                "Can’t submit Rating and Feedback",
                "Other"
            ],
            'My Document' => [
                "Uploaded document not showing",
                "Can’t view documents marked public",
                "Documents marked private also visible",
                "Other"
            ],
            'Onboard Experience' => [
                "Can’t see my Feedback",
                "Ratings are showing wrong",
                "Other"
            ],
            'Infographics' => [
                "Data in graph is wrong",
                "Engine Type not showing",
                "Ship Type not showing",
                "Other"
            ],
            'My Profile' => [
                "Profile Pic not showing",
                "Data not showing correctly",
                "Other"
            ],
            'My Resume' => [
                "Data not showing correctly",
                "Unable to download Resume",
                "Other"
            ],
            'New Jobs' => [
                "Notification not showing",
                "Can’t apply",
                "Can’t give not interested comment",
                "Job apply log not showing correctly",
                "QR not showing",
                "Can’t delete records",
                "Other"
            ],
            'Share Profile' => [
                "Data not showing correctly on profile",
                "uploaded documents not showing in my documents",
                "Private marked documents showing in my documents",
                "Public marked documents not showing in my documents",
                "Feedback not showing in Onboard Experience",
                "Graphs not showing correctly in Infographics",
                "Other"
            ],
            'Share Resume' => [
                "Data not showing correctly",
                "Can’t submit QR",
                "Can’t download Resume",
                "Other"
            ],
            'Share Document' => [
                "Shared documents not showing",
                "Can’t download documents",
                "Other"
            ],
        ];
        return $report_sub_issue;
    }

}