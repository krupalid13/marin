<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserCocDetail extends Model
{
    protected $table = 'user_coc_details';

    protected $fillable = ['user_id', 'coc', 'coc_number', 'coc_expiry_date', 'coc_grade', 'other_coc', 'other_coc_number', 'other_coc_expiry_date', 'other_coc_grade','status','coc_verification_date','sequence'];
}
