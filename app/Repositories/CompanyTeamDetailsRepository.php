<?php

namespace App\Repositories;
use App\CompanyTeamDetails;

class CompanyTeamDetailsRepository
{
    private $companyTeamDetailsModel;

    function __construct() {
        $this->companyTeamDetailsModel = new CompanyTeamDetails();
    }

    public function store($data){
        return $this->companyTeamDetailsModel->create($data);
    }

    public function getAllTeamDetails($company_id){
        return $this->companyTeamDetailsModel->where('company_id', $company_id)->get();
    }

    public function getTeamDetails($company_id){
        return $this->companyTeamDetailsModel->where('company_id', $company_id)->get();
    }

    public function deleteTeamDetails($team_id,$company_id){
        return $this->companyTeamDetailsModel->where('id', $team_id)->where('company_id', $company_id)->delete();
    }

    public function getTeamDataByCompanyId($company_id){
        $options = ['location','location.state','location.city'];
        return $this->companyTeamDetailsModel->where('company_id', $company_id)->with($options)->get();
    }

    public function getTeamDataByTeamId($team_id){
        return $this->companyTeamDetailsModel->where('id', $team_id)->get();
    }

    public function updateTeamDetailsByTeamId($team_details,$team_data_id){
        return $this->companyTeamDetailsModel->where('id', $team_data_id)->update($team_details);
    }

}

?>