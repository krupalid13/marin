<?php

namespace App\Repositories;
use App\InstituteBatches;
use App\InstituteCourses;
use App\Courses;
use App\InstituteBatchLocation;
use App\InstituteLocation;


class BatchRepository
{
	private $batchModel;
    function __construct()
    {
        $this->batchModel = new InstituteBatches(); 
        $this->courseModel = new InstituteCourses(); 
        $this->mainCourseModel = new Courses(); 
        $this->batchLocationModel = new InstituteBatchLocation(); 
        $this->instituteLocationModel = new InstituteLocation(); 
    }

    public function store($data){
        return $this->batchModel->create($data);
    }

    public function storeBatchLocation($data){
        return $this->batchLocationModel->create($data);
    }

    public function deleteInstituteLocation($batches,$course_id){
        $this->batchModel->whereNotIn('id',$batches)->where('institute_course_id',$course_id)->delete();
    }

    public function deleteBatchLocationByBatchId($batch_id){
        return $this->batchLocationModel->where('batch_id', $batch_id)->delete();
    }

    public function update($data){
    	$batch_id = $data['batch_id'];
    	unset($data['batch_id']);
    	return $this->batchModel->where('id', $batch_id)->update($data);
    }

    public function getDataByBatchId($batch_id){
    	return $this->batchModel->where('id',$batch_id)->with('discount','child_batches.batch_location','child_batches.orders','courses','batch_location.location','batch_location.location.state','batch_location.location.city','course_details.institute_details','course_details.institute_registration_detail.user','course_details.course_details')->get();
    }

    public function storeCourse($data){
        if(isset($data['existing_course_id'])){
            $course_id = $data['existing_course_id'];
            unset($data['existing_course_id']);
                
            return $this->courseModel->where('id',$course_id)->update($data);
        }
        return $this->courseModel->create($data);
    }

    public function storeOtherCoursesInCourses($data){
    	return $this->mainCourseModel->create($data);
    }

    public function getInstitutesCoursesByInstituteCourseId($institute_course_id){
        return $this->courseModel->where('id',$institute_course_id)->first();
    }

    public function getAllInstitutesCourses($options){
        $date_now = date("Y-m-d H:i:s");
        
        $courseModel = $this->courseModel->where('status','<>','0');

        if(isset($options['with']) && in_array("institute_registration_details.active_subscription", $options['with'])){
                $courseModel->with(['institute_registration_details'=> function($query) use ($date_now) {
                        $query->with(['active_subscription' => function($qt) use ($date_now) {
                            $qt->where('status',1)->where('valid_from','<=',$date_now)->where('valid_to','>=',$date_now);
                        }]);
                 }]);
                
                $courseModel->whereHas('institute_registration_details', function($query) use ($date_now) {
                        $query->whereHas('active_subscription', function($qt) use ($date_now) {
                            $qt->where('status',1)->where('valid_from','<=',$date_now)->where('valid_to','>=',$date_now);
                        });
                 });

        }

        $courseModel = $courseModel->whereHas('institute_batches', function ($c){
            $c->where('start_date','>',date('Y-m-d'));
        })->with(['institute_batches' => function ($c){
            $c->with(['institute_location'])->where('start_date','>',date('Y-m-d'));
        }]);

        $courseModel = $this->courseModel->orderBy('id','desc')->with($options['with'])->get();

        return $courseModel;

    }

    public function getAllBatchesByInstituteId($institute_id,$filter,$batch_id,$paginate){
        $batches = $this->batchModel->orderBy('id','desc');
        if(isset($filter) &&!empty($filter)){

            if (isset($filter['start_date']) && !empty($filter['start_date'])){
                $batches = $batches->where('start_date','>=',date('Y-m-d',strtotime($filter['start_date'])));
            }
        }
        
        $batches = $batches->whereHas('course_details', function ($c) use($institute_id,$filter){
            $c->where('institute_id',$institute_id);

            if(isset($filter) &&!empty($filter)){
                
                if (isset($filter['institute_name']) && !empty($filter['institute_name'])){
                    $c = $c->where('institute_id',$filter['institute_name']);
                }
            
                if (isset($filter['course_type']) && !empty($filter['course_type'])){
                    $c = $c->where('course_type',$filter['course_type']);
                }

                if (isset($filter['course_name']) && !empty($filter['course_name'])){
                    $c = $c->where('course_id',$filter['course_name']);
                }
            }

        })->with(['course_details' => function ($c) use($institute_id,$filter){
            $c->where('institute_id',$institute_id);

            if(isset($filter) &&!empty($filter)){
                    
                if (isset($filter['institute_name']) && !empty($filter['institute_name'])){
                    $c = $c->where('institute_id',$filter['institute_name']);
                }
            
                if (isset($filter['course_type']) && !empty($filter['course_type'])){
                    $c = $c->where('course_type',$filter['course_type']);
                }

                if (isset($filter['course_name']) && !empty($filter['course_name'])){
                    $c = $c->where('course_id',$filter['course_name']);
                }
            }
        }]);
        
        $batches = $batches->with('batch_location.location.state','batch_location.location.city');
        
        if(isset($batch_id) && !empty($batch_id)){
            return $batches->where('id',$batch_id)->get();
        }

        if($paginate == 'no'){
            return $batches->get();
        }
        return $batches->paginate('10');

    }

    public function getAllCourseDetailsByInstituteId($institute_id,$options,$filter){

        if(isset($options['with']) AND !empty($options['with'])){
            $courseModel = $this->courseModel;

            if(isset($institute_id) AND !empty($institute_id)){
                $courseModel = $courseModel->where('institute_id',$institute_id);
            }
            
            if(isset($options['with']) AND !empty($options['with'])){

                if(in_array('institute_batches', $options['with'])){
                    $courseModel = $courseModel->whereHas('institute_batches', function ($c){
                        $c->where('start_date','>',date('Y-m-d'));
                    })->with(['institute_batches' => function ($c){
                        $c->with(['institute_location'])->where('start_date','>=',date('Y-m-d'));
                    }]);
                }
                
                if(!in_array('institute_batches.institute_location', $options['with'])){
                    $courseModel = $courseModel->with($options['with']);
                }
                //return $courseModel->where('id',$course_id)->with($options['with'])->get();
            }
            //dd($courseModel->paginate('10')->toArray());
            if(isset($filter) &&!empty($filter)){
                
                if (isset($filter['institute_name']) && !empty($filter['institute_name'])){
                    $courseModel = $courseModel->where('institute_id',$filter['institute_name']);
                }
            
                if (isset($filter['course_type']) && !empty($filter['course_type'])){
                    $courseModel = $courseModel->where('course_type',$filter['course_type']);
                }

                if (isset($filter['course_name']) && !empty($filter['course_name'])){
                    $courseModel = $courseModel->where('course_id',$filter['course_name']);
                }

                if (isset($filter['start_date']) && !empty($filter['start_date'])){
                    $courseModel = $courseModel->whereHas('institute_batches', function ($c) use($filter){
                        $c->where('start_date','=',date('Y-m-d',strtotime($filter['start_date'])));
                    });
                }
            }
            if(isset($filter['no_order_by']) && $filter['no_order_by'] == 1) {
                $courseModel->orderBy('institute_courses.id', 'asc');
            }else {
                $courseModel->orderBy('institute_courses.id', 'desc');
                
            }
            if (isset($filter['no_pagination']) && !empty($filter['no_pagination'])){
                return $courseModel->get();
            }
            $courseModel = $courseModel->paginate('10');

            return $courseModel;
        }
        if(isset($options) AND !empty($options)){
            return $this->courseModel->orderBy('institute_courses.id','desc')->with($options)->paginate('10');
        }
        return $this->courseModel->where('institute_id',$institute_id)->orderBy('institute_courses.id','desc')->paginate('10');
    }

    public function getAllCourseDetailsByInstituteCourseId($course_id,$options){
        $courseModel = $this->courseModel;
        if(isset($options['with']) AND !empty($options['with'])){
            $courseModel = $courseModel->whereHas('institute_batches', function ($c){
                $c->where('start_date','>',date('Y-m-d'));
            })->with(['institute_batches' => function ($c){
                $c->with(['institute_location'])->where('start_date','>=',date('Y-m-d'))->orderBy('start_date');
            }]);
            
            return $courseModel->where('id',$course_id)->with($options['with'])->get();
        }
    }

    public function getPastCourseDetailsByInstituteCourseId($course_id,$options){
        $courseModel = $this->courseModel;
        if(isset($options['with']) AND !empty($options['with'])){
            $courseModel = $courseModel->whereHas('institute_batches', function ($c){
                $c->where('start_date','>',date('Y-m-d'));
            })->with(['institute_batches' => function ($c){
                $c->with(['institute_location'])->where('start_date','<',date('Y-m-d'));
            }]);

            return $courseModel->where('id',$course_id)->with($options['with'])->get();
        }
    }

    public function disableBatchByBatchId($batch_id){
        return $this->batchModel->where('id',$batch_id)->update(['status'=>'0']);
    }

    public function enableBatchByBatchId($batch_id){
        return $this->batchModel->where('id',$batch_id)->update(['status'=>'1']);
    }

    public function disableCourseByCourseId($id){
        return $this->courseModel->where('id',$id)->update(['status'=>'0']);
    }

    public function enableCourseByCourseId($id){
        return $this->courseModel->where('id',$id)->update(['status'=>'1']);
    }

    public function deleteCourse($institute_course_id,$institute_id){
        return $this->courseModel->where('id',$institute_course_id)->where('institute_id',$institute_id)->delete();
    }

    public function getBatchDetailsByUserId($user_id,$filter){

        if(isset($filter) &&!empty($filter)){
            $batch_model = $this->batchModel;

            if (isset($filter['course_type']) && !empty($filter['course_type'])){
                $batch_model = $this->batchModel->where('course_type',$filter['course_type']);
            }

            if (isset($filter['course_name']) && !empty($filter['course_name'])){
                $batch_model = $this->batchModel->where('course_name',$filter['course_name']);
            }

            if (isset($filter['start_date']) && !empty($filter['start_date'])){
                $batch_model = $this->batchModel->where('start_date','>=',date('Y-m-d h:i:s',strtotime($filter['start_date'])));
            }

            return $batch_model->with('courses')->where('institute_id',$user_id)->paginate('10');
        }else{
            return $this->batchModel->where('institute_id',$user_id)->with('courses')->paginate('10');
        }
    }

    public function getAllActiveCourseLocations(){
        // ->with('location.state','location.city','location.pincode.pincodes_states.state','location.pincode.pincodes_cities.city')
        $instituteLocation = $this->instituteLocationModel->select('city_id')->where('city_id','!=',NULL)->with('city')->groupBy('city_id');
        

        // $batchLocationModel = $this->batchLocationModel->select('location_id')->with('location.state','location.city','location.pincode.pincodes_states.state','location.pincode.pincodes_cities.city')->groupBy('location_id');

        return $instituteLocation->get();
    }

    public function getAllCourseBatchDetails($user_id,$filter){
        
        $batch_model = $this->batchModel->where('start_date','>',date('Y-m-d'))->where('status','1')->with('course_details.institute_registration_details','batch_location.location.state','batch_location.location.city','batch_location.location.pincode.pincodes_states.state','batch_location.location.pincode.pincodes_cities.city')->orderBy('start_date','asc');

            $batch_model = $batch_model->with(['batch_discount.company_registration.advertisment_details' => function ($c){
                $c->where('status','2');
            }]);

            $batch_model = $batch_model->whereHas('course_details', function ($c){
                $c->where('status','1');
            })->with(['course_details' => function ($c){
                $c->where('status','1');
            }]);
            // dd($filter);
            if (isset($filter['institute_date']) && !empty($filter['institute_date'])){
                $batch_model = $batch_model->where('start_date',$filter['institute_date']);
            }

            if (isset($filter['course_type']) && !empty($filter['course_type'])){

                $batch_model = $batch_model->whereHas('course_details', function ($c) use($filter){
                    $c->where('course_type',$filter['course_type']);
                })->with(['course_details' => function ($c) use($filter){
                    $c->where('course_type',$filter['course_type']);
                }]);

                
                if (isset($filter['course_name']) && !empty($filter['course_name'])){

                    $batch_model = $batch_model->whereHas('course_details', function ($c) use($filter){
                        $c->where('course_id',$filter['course_name']);
                    })->with(['course_details' => function ($c) use($filter){
                        $c->where('course_id',$filter['course_name']);
                    }]);
                }
            }

            if (isset($filter['institute_name']) && !empty($filter['institute_name']) && $filter['institute_name'][0] != ''){
                $batch_model = $batch_model->whereHas('course_details.institute_registration_details', function ($c) use($filter){
                    $c->whereIn('id',$filter['institute_name']);
                })->with(['course_details.institute_registration_details' => function ($c) use($filter){
                    $c->whereIn('id',$filter['institute_name']);
                }]);
            }

            if (isset($filter['institute_location']) && !empty($filter['institute_location'])){

                $batch_model = $batch_model->whereHas('batch_location.location', function ($c) use($filter){
                    $c->whereIn('city_id',$filter['institute_location']);
                })->with(['batch_location.location' => function ($c) use($filter){
                    $c->whereIn('city_id',$filter['institute_location']);
                }]);
            }

            if(isset($filter['discount']) && $filter['discount'] == '1'){
                $batch_model = $batch_model->whereHas('batch_discount.company_registration.advertisment_details', function ($c) use($filter){
                    $c->where('status','2');
                })->with(['batch_discount.company_registration.advertisment_details' => function ($c) use($filter){
                    $c->where('status','2');
                }]);
            }

        return $batch_model->paginate('15');
    }

    public function getAllBatches($options,$filter=Null){
        $batch_model = $this->batchModel;

        if(isset($filter) &&!empty($filter)){
            
            if (isset($filter['institute_name']) && !empty($filter['institute_name'])){
                $batch_model = $batch_model->where('institute_id',$filter['institute_name']);
            }

            if (isset($filter['course_type']) && !empty($filter['course_type'])){
                $batch_model = $batch_model->where('course_type',$filter['course_type']);
            }

            if (isset($filter['course_name']) && !empty($filter['course_name'])){
                $batch_model = $batch_model->where('course_name',$filter['course_name']);
            }

            if (isset($filter['start_date']) && !empty($filter['start_date'])){
                $batch_model = $batch_model->where('start_date','>=',date('Y-m-d h:i:s',strtotime($filter['start_date'])));
            }
            
            return $batch_model->with($options['with'])->orderBy('id','desc')->paginate('10');
        }
        else{
            return $batches = $batch_model->with($options['with'])->orderBy('id','desc')->paginate('10');
        }
    }

    public function getInstituteBatchedByInstituteCourseId($institute_course_id,$location_id){
        $batchModel = $this->batchModel->where('institute_course_id',$institute_course_id);

        $batchModel = $batchModel->whereHas('batch_location', function ($c) use($location_id){
            $c->where('location_id',$location_id);
        })->with(['batch_location' => function ($c) use($location_id){
            $c->where('location_id',$location_id);
        }]);

        return $batchModel->get();
    }

    public function getAllOnDemandBatchesByInstituteId($institute_id,$filter,$batch_id,$paginate){
        $batches = $this->batchModel->with('course_details.courses','orders','child_batches')->where('batch_type','on_demand')->where('institute_id',$institute_id)
        ->where('status','!=','0')->orderBy('id','desc');

        return $batches->paginate('10');

    }

    public function updateBatchDataByBatchId($batch_id,$data){
        return $this->batchModel->where('id',$batch_id)->update($data);
    }

}