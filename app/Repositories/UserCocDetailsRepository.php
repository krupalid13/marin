<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 4/21/2017
 * Time: 4:51 PM
 */

namespace App\Repositories;
use App\UserCocDetail;

class UserCocDetailsRepository
{
    Private $userCocDetailsModel;
    function __construct()
    {
        $this->userCocDetailsModel = new UserCocDetail();
    }

    public function store($data)
    {
        $data = $this->userCocDetailsModel->create($data);
        return $data;
    }

    public function getDetailsByUserId($user_id){
        return $this->userCocDetailsModel->where('user_id', $user_id)->get();
    }

    public function updateByUserId($data, $user_id) {
        return $this->userCocDetailsModel->where('user_id', $user_id)->update($data);
    }

    public function deleteServiceById($user_id){
        $this->userCocDetailsModel->where('user_id', $user_id)->delete();
    }
    
    public function updateByCocId($data, $cocId) {
        return $this->userCocDetailsModel->where('id', $cocId)->update($data);
    }
    
    public function deleteServiceByCocIds($cocIds){
        $this->userCocDetailsModel->whereIn('id', $cocIds)->delete();
    }
}