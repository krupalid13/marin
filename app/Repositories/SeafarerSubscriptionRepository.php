<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 5/2/2017
 * Time: 4:21 PM
 */

namespace App\Repositories;
use App\SeafarerSubscription;

class SeafarerSubscriptionRepository
{
    private $SeafarerSubscriptionModel;

    function __construct() {
        $this->SeafarerSubscriptionModel = new SeafarerSubscription();
    }

    public function getSeafarerSubscriptionsById($user_id){

        return $this->SeafarerSubscriptionModel->where('user_id',$user_id)->get();
    }

    public function store($order_seafarer_subscriptions)
    {
        return $this->SeafarerSubscriptionModel->create($order_seafarer_subscriptions);
    }

    public function getSeafarerSubscriptionsByUserIdWithActiveSubscription($user_id){

        return $this->SeafarerSubscriptionModel->where(['user_id' => $user_id, 'status' => 1])->get();
    }
}