<?php

namespace App\Repositories;
use App\CompanyShipDetails;

class CompanyShipDetailsRepository
{
    private $companyShipDetailsModel;

    function __construct() {
        $this->companyShipDetailsModel = new CompanyShipDetails();
    }

    public function store($data){

        $inserted_data = $this->companyShipDetailsModel->create($data);

        if(!isset($data['ship_name'])){
            $ship_data['ship_name'] = "vessel ". $inserted_data->id;
            $this->companyShipDetailsModel->where('id', $inserted_data->id)->update($ship_data);
        }
        return $inserted_data;
    }

    public function update($data,$ship_id){

        if(!isset($data['ship_name'])){
            $data['ship_name'] = "vessel ". $ship_id;
        }

        return $this->companyShipDetailsModel->where('id', $ship_id)->update($data);
    }

    public function deleteRecordsByCompanyId($company_id){
        $data = $this->companyShipDetailsModel->where('company_id' , $company_id)->get();

        if(count($data) > 0)
            $this->companyShipDetailsModel->where('company_id' , $company_id)->delete();
    }

    public function getCompanyShipsByShipTypeId($company_id,$ship_type_id){
        return $this->companyShipDetailsModel->where('company_id' , $company_id)->where('ship_type' , $ship_type_id)->get();
    }

    public function dataByCompanyShipId($id){
        return $this->companyShipDetailsModel->where('id' , $id)->get();
    }

    public function getShipDetails($company_id, $scope_type){
        return $this->companyShipDetailsModel->where('company_id', $company_id)->where('scope_type', $scope_type)->get();
    }

    public function deleteVesselByVesselId($vessel_id, $company_id){

        $vessel_detail = $this->companyShipDetailsModel->where('id', $vessel_id)->first();
        
        if(!empty($vessel_detail)){

            if($vessel_detail['company_id'] == $company_id){
                $this->companyShipDetailsModel->where('id' , $vessel_id)->delete();
                $status_array = ['status' => 'success','message' => 'Your vessel details has been deleted successfully.'];
            }
            else{
                $status_array = ['status' => 'failed','message' => 'This vessel not belongs to logged in company.'];
            }
        }else{
            $status_array = ['status' => 'failed','message' => 'Company vessel not found.'];
        }
    
        return $status_array;
    }

    public function getShipDetailsByshipId($ship_id, $company_id){
        return $this->companyShipDetailsModel->where('company_id' , $company_id)->where('id',$ship_id)->first();
    }
}

?>