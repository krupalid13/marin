<?php
namespace App\Repositories;
use App\OrderPayment;

class OrderPaymentRepository
{
    private $orderModel;

    function __construct() {

        $this->orderPaymentModel = new OrderPayment();
    }

    public function store($array){
    	// dd($array);
        return $this->orderPaymentModel->create($array);
    }

    public function getOrderDetailsByOrderId($order_id){
        return $this->orderPaymentModel->where('order_id',$order_id)->with('order_detail','subscription_detail')->get();
    }
}