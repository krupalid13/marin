<?php

namespace App\Repositories;
use App\CompanyAgents;

class CompanyAgentsRepository
{
    private $companyAgentsModel;

    function __construct() {
        $this->companyAgentsModel = new CompanyAgents();
    }

    public function store($data){
        return $this->companyAgentsModel->create($data);
    }

    public function update($data,$agent_id){
        return $this->companyAgentsModel->where('id', $agent_id)->update($data);
    }

    public function getAgentsByAgentType($company_id, $agent_type){
        $model = $this->companyAgentsModel->where('agent_type',$agent_type);

        if($agent_type == 'appointed'){
            $model = $model->where('to_company_id',$company_id);
        }  

        if($agent_type == 'associate'){
            $model = $model->where('from_company_id',$company_id);
        }  

        return $model->get();
    }

    public function deleteAgentByAgentId($agent_id, $company_id){
        $agent_details = $this->companyAgentsModel->where('id', $agent_id)->first();
        
        if(!empty($agent_details)){

            if($agent_details->agent_type == 'appointed'){
                $agent_details['company_id'] = $agent_details['to_company_id'];
            }  

            if($agent_details->agent_type == 'associate'){
                $agent_details['company_id'] = $agent_details['from_company_id'];
            }

            if($agent_details->company_id == $company_id){
                $this->companyAgentsModel->where('id' , $agent_id)->delete();
                $status_array = ['status' => 'success','message' => 'Agent details has been deleted successfully.'];
            }
            else{
                $status_array = ['status' => 'failed','message' => 'This agent not belongs to logged in company.'];
            }
        }else{
            $status_array = ['status' => 'failed','message' => 'Agent not found.'];
        }
    
        return $status_array;
    }

    public function getAgentDetailsByAgentId($agent_id){
        return $this->companyAgentsModel->where('id',$agent_id)->first();
    }

}