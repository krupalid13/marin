<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 4/21/2017
 * Time: 4:51 PM
 */

namespace App\Repositories;
use App\UserCopDetail;

class UserCopDetailsRepository
{
    Private $userCopDetailsModel;
    function __construct()
    {
        $this->userCopDetailsModel = new UserCopDetail();
    }

    public function store($data)
    {
        $data = $this->userCopDetailsModel->create($data);
        return $data;
    }

    public function getDetailsByUserId($user_id){
        return $this->userCopDetailsModel->where('user_id', $user_id)->get();
    }

    public function updateByUserId($data, $user_id) {
        return $this->userCopDetailsModel->where('user_id', $user_id)->update($data);
    }

    public function deleteServiceById($user_id){
        $this->userCopDetailsModel->where('user_id', $user_id)->delete();
    }
}