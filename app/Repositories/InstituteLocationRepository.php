<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 5/18/2017
 * Time: 5:37 PM
 */

namespace App\Repositories;
use App\InstituteLocation;


class InstituteLocationRepository
{
    private $instituteLocationModel;

    function __construct()
    {
        $this->instituteLocationModel = new InstituteLocation;
    }

    public function store($data){
        return $this->instituteLocationModel->create($data);
    }

    public function update($data,$location_id){
        return $this->instituteLocationModel->where('id',$location_id)->update($data);
    }

    public function deleteRecordsByInstituteId($institute_id){
        $institute_locations = $this->instituteLocationModel->where('institute_id' , $institute_id)->get();

        if(count($institute_locations) > 0)
            $this->instituteLocationModel->where('institute_id' , $institute_id)->delete();
    }

    public function deleteRecordsByInstituteIdAndLocationIds($institute_id,$location_ids){
        
        if(isset($location_ids) && !empty($location_ids)){
            $this->instituteLocationModel->where('institute_id',$institute_id)->whereNotIn('id', $location_ids)->delete();
        }
    }
}