<?php

namespace App\Repositories;
use App\Schedule;

class ScheduleRepository
{
    private $schedulesModel;

    function __construct() {
        $this->schedulesModel = new Schedule();
    }

    public function store($array) {
        return $this->schedulesModel->create($array);
    }

    public function getAll($pagination_limit = null) {
		return $this->schedulesModel->whereIn('status',[0,2])->orderBy('created_at', 'desc')->paginate($pagination_limit);
	}

	public function delete($id)
	{
		return $this->schedulesModel->where('id',$id)->delete();
	}

	public function getScheduledEmailByCurrentDateTime($now, $next_five_minutes) {
		return $this->schedulesModel->where('schedule_date_time','<=',$next_five_minutes)->where(['status' => 0,'type'=>'email'])->get();
	}

	public function changeStatusTo($id, $new_status, $err_msg = null) {
      return $this->schedulesModel->where('id', $id)->update(['status' => $new_status,'error_message'=>$err_msg]);
    }

	// public function getById($id)
	// {
	// 	return $this->schedulesModel->where('id',$id)->first();
	// }

    
}