<?php

namespace App\Repositories;
use App\CompanyRegistration;

class CompanyRepository
{
    private $companyRegistrationModel;

    function __construct() {
        $this->companyRegistrationModel = new CompanyRegistration();
    }

    public function store($data){
        return $this->companyRegistrationModel->create($data);
    }

    public function getDataByUserId($user_id,$options)
    {
        if(empty($options)){
            $user =  $this->companyRegistrationModel->where('user_id', $user_id)->get();
        }elseif(isset($options['with'])){
            $user =  $this->companyRegistrationModel->where('user_id', $user_id)->with('company_details')->get();
        }
        return $user;
    }

    public function getDetailsByCompanyId($id){
        return $this->companyRegistrationModel->where('user_id', $id)->get();
    }

    public function update($data,$id){
        return $this->companyRegistrationModel->where('user_id', $id)->update($data);
    }

    public function getLocationDataByCompanyId($company_id){
        return $this->companyRegistrationModel->where('id', $company_id)->with('company_locations','company_locations.state','company_locations.city')->get();
    }

}