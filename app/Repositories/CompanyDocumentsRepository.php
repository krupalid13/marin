<?php

namespace App\Repositories;
use App\CompanyDocuments;

class CompanyDocumentsRepository
{
    private $companyDocumentsModel;

    function __construct() {
        $this->companyDocumentsModel = new CompanyDocuments();
    }

    public function store($data){
        $check_data = $this->companyDocumentsModel->where('company_id' , $data['company_id'])->get()->toArray();

        if(count($check_data) > 0){
            return $this->companyDocumentsModel->where('company_id' , $data['company_id'])->update($data);
        }
    	else
        	return $this->companyDocumentsModel->create($data);
    }

}