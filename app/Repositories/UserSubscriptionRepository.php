<?php

namespace App\Repositories;
use App\UserSubscription;

class UserSubscriptionRepository
{
    private $userSubscriptionModel;

    function __construct() {
        $this->userSubscriptionModel = new UserSubscription();
    }

    public function store($data){
    	return $this->userSubscriptionModel->create($data);
    }

    public function getSubscriptionsByUserIdWithActiveSubscription($user_id){
        return $this->userSubscriptionModel->where('user_id' , $user_id)->where('status','1')->get();
    }

    public function getSubscriptionsByUserId($user_id){
    	return $this->userSubscriptionModel->where('user_id' , $user_id)->with('subscription_feature')->orderBy('id','desc')->get();
    }

    public function updateSubscriptionByOrderId($data,$order_id){
        return $this->userSubscriptionModel->where('order_id',$order_id)->update($data);
    }

    public function deleteSubscriptionById($id){
        return $this->userSubscriptionModel->where('id',$id)->delete();
    }
}