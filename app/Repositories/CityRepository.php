<?php

namespace App\Repositories;
use App\City;

class CityRepository
{
    private $cityModel;

    function __construct() {
        $this->cityModel = new City();
    }

    public function getIdByCityName($name){
    	return $this->cityModel->where('name', 'like', '%'.$name.'%')->first();
    }

    public function getAll()
    {
    	return $this->cityModel->get();
    }

}