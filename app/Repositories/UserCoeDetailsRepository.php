<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 4/21/2017
 * Time: 4:51 PM
 */

namespace App\Repositories;
use App\UserCoeDetails;

class UserCoeDetailsRepository
{
    Private $userCoeDetailsModel;
    function __construct()
    {
        $this->userCoeDetailsModel = new UserCoeDetails();
    }

    
    public function store($data)
    {
        $data = $this->userCoeDetailsModel->create($data);
        return $data;
    }

    public function getDetailsByUserId($user_id){
        return $this->userCoeDetailsModel->where('user_id', $user_id)->get();
    }

    public function updateByUserId($data, $user_id) {
        return $this->userCoeDetailsModel->where('user_id', $user_id)->update($data);
    }

    public function deleteServiceById($user_id){
        $this->userCoeDetailsModel->where('user_id', $user_id)->delete();
    }
    
    public function updateByCoeId($data, $coeId) {
        return $this->userCoeDetailsModel->where('id', $coeId)->update($data);
    }
    
    public function deleteServiceByCoeIds($coeIds){
        $this->userCoeDetailsModel->whereIn('id', $coeIds)->delete();
    }
}