<?php 

namespace App\Repositories;

use App\Pincode;

class PincodeRepository {

    private $pincodeModel;

    function __construct() {

        $this->pincodeModel = new Pincode();
    }

    public function getStatesCities($pincode) {
        return $this->pincodeModel->where('code',$pincode)->with('pincodes_states.state','pincodes_cities.city', 'pincodes_areas')->get();
    }

    public function getAll(){
        return $this->areaModel->get();
    }

    public function getByName($name){
        return $this->areaModel->where('name',"LIKE",$name."%")->get();
    }

    public function store($array){
        return $this->areaModel->create($array);
    }


}