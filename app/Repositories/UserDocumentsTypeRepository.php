<?php

namespace App\Repositories;
use App\UserDocumentsType;
use App\DocumentPermissions;
use App\UserDocumentDownloads;

class UserDocumentsTypeRepository
{
    private $userDocumentsTypeModel;

    function __construct() {
        $this->userDocumentsTypeModel = new UserDocumentsType();
        $this->documentPermissionsModel = new DocumentPermissions();
        $this->userDocumentDownloads = new UserDocumentDownloads();
    }

    public function store($data){
        return $this->userDocumentsTypeModel->create($data);
    }

    public function deleteDocument($img_id,$img_name){
    	return $this->userDocumentsTypeModel->where('id',$img_id)->delete();
    }

    public function getUserDocuments($user_id){
    	return $this->userDocumentsTypeModel->where('user_id',$user_id)->with('user_documents')->get();
    }

    public function checkSeafarerTypeExist($type,$type_id,$user_id){
        return $this->userDocumentsTypeModel->where('user_id',$user_id)->where('type',$type)->where('type_id',$type_id)->get();
    }

    public function deactivateAllDocumentsByUserId($user_id){
        $data['status'] = 0;
        return $this->userDocumentsTypeModel->where('user_id',$user_id)->update($data);
    }

    public function storeDocumentsPermissions($type,$type_id,$user_id){
        $data['status'] = 1;
        return $this->userDocumentsTypeModel->where('user_id',$user_id)->where('type',$type)->where('type_id',$type_id)->update($data);
    }

    public function getUserDocumentsWithFilter($user_id,$type,$option){
        if(isset($user_id) && !empty($user_id)){
            $documentsTypeData = $this->userDocumentsTypeModel->where('user_id',$user_id);

            if(isset($type) && !empty($type)){
                $documentsTypeData = $documentsTypeData->where('type',$type);
            }
            if(isset($option) && !empty($option)){
                $documentsTypeData = $documentsTypeData->where('type_id',$option);
            }

            return $documentsTypeData->get();
        }
        else{
            return false;
        }
    }

    public function getAllSeafarerPermissionRequest($user_id,$filter){

        $documentPermissionsModel = $this->documentPermissionsModel;

        if(isset($filter) && !empty($filter)){
                                       
            if(isset($filter['name']) && !empty($filter['name'])){
                $documentPermissionsModel = $documentPermissionsModel->whereHas('owner', function ($c) use ($filter) {
                    $c->where('first_name', 'like', '%'.$filter['name'].'%');
               });
            }

            if(isset($filter['email']) && !empty($filter['email'])){
                $documentPermissionsModel = $documentPermissionsModel->whereHas('owner', function ($c) use ($filter) {
                    $c->where('email', 'like', '%'.$filter['email'].'%');
                });
            }

            if(isset($filter['mob']) && !empty($filter['mob'])){
                $documentPermissionsModel = $documentPermissionsModel->whereHas('owner', function ($c) use ($filter) {
                    $c->where('mobile', $filter['mob']);
                });
            }
        }

        return $documentPermissionsModel->where('requester_id',$user_id)->with('owner','document_type.type')->orderBy('id','desc')->paginate('10');
    }

    public function checkPermissionBelongsToUser($permission_id,$user_id){
        return $this->documentPermissionsModel->where('id',$permission_id)->where('requester_id',$user_id)->where('status','1')->get();
    }

    public function getPermissionDetailsByPermissionId($permission_id){
        return $this->documentPermissionsModel->where('id',$permission_id)->with('document_type.type')->first();
    }

    public function storeRequestedTypeDocumentsList($data){
        return UserDocumentDownloads::create($data);
    }


    public function changeUserDocumentStatus($document_details,$user_id){
        $model = $this->userDocumentsTypeModel->where('user_id',$user_id);

        if(isset($document_details['type']) && !empty($document_details['type'])){
            $model = $model->where('type',$document_details['type']);
        }
        if(isset($document_details['type_id']) && !empty($document_details['type_id'])){
            $model = $model->where('type_id',$document_details['type_id']);
        }

        $document = $model->get();

        if($document){
            $data['status'] = $document_details['value'];
            return $model->update($data);
        }else{
            return false;
        }
    }

    public function ListDownloadedDocuments($options = NULL){
        return $this->userDocumentDownloads->with($options['with'])->paginate(10);
    }

    public function getRequestedDocumentList($options,$filter=NULL){
        if(empty($filter)){
            return $this->documentPermissionsModel->with($options['with'])->paginate(10);
        }
        else{
            $data = $this->documentPermissionsModel;

            if (isset($filter['company_name']) && !empty($filter['company_name'])){
                $data = $data->whereHas('requester.company_registration_detail', function ($c) use ($filter) {
                    $c->where('company_name', 'like', '%'.$filter['company_name'].'%');
                });
            }

            if (isset($filter['company_email']) && !empty($filter['company_email'])){
                $data = $data->whereHas('requester.company_registration_detail', function ($c) use ($filter) {
                    $c->where('email', 'like', '%'.$filter['company_email'].'%');
                });
            }

            if (isset($filter['seafarer_name']) && !empty($filter['seafarer_name'])){
                $data = $data->whereHas('owner', function ($c) use ($filter) {
                    $c->where('first_name', 'like', '%'.$filter['seafarer_name'].'%');
                });
            }

            if (isset($filter['seafarer_email']) && !empty($filter['seafarer_email'])){
                $data = $data->whereHas('owner', function ($c) use ($filter) {
                    $c->where('email', 'like', '%'.$filter['seafarer_email'].'%');
                });
            }

            return $data->with($options['with'])->paginate(10);
        }
    }
}