<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 4/20/2017
 * Time: 6:36 PM
 */

namespace App\Repositories;
use App\UserProfessionalDetail;

class UserProfessionalDetailsRepository
{
    private $userProfessionDetailModel;
    function __construct()
    {
        $this->userProfessionDetailModel = new UserProfessionalDetail();
    }

    public function store($data)
    {
        $data = $this->userProfessionDetailModel->create($data);
        return $data;
    }

    public function updateByUserId($data,$user_id)
    {
        $data = $this->userProfessionDetailModel->where('user_id', $user_id)->update($data);
        return $data;
    }

    public function getUserProfessionalDetailsById($user_id)
    {
        $user =  $this->userProfessionDetailModel->where('user_id', $user_id)->get();
        return $user;
    }

    public function getAllSeafarersByCurrentDayAvailability($date){
        $data = $this->userProfessionDetailModel->where('availability','>',$date)->with('user_details','user_personal_details')->get();
        return $data;
    }

    public function getAllSeafarersExpiringAvailabilityBeforeGivenDays($current_date,$to_date){
        $data = $this->userProfessionDetailModel->where('availability','>', $current_date)->where('availability','<=', $to_date)->with('user_details','user_personal_details')->get();
        
        return $data;
    }
}