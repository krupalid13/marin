<?php

namespace App\Repositories;

use App\ShareContacts;
use App\EnginType;
use Auth;
use Datatables;

class ShareContactsRepository {

    private $shareContactsModel;

    function __construct() {
        $this->shareContactsModel = new ShareContacts();
    }

    public function store($data) {
        unset($data['_token']);
        if (isset($data['share_contact_id']) && !empty($data['share_contact_id'])) {
            $shareContactId = $data['share_contact_id'];
            unset($data['share_contact_id']);
            $this->shareContactsModel->where('id', $shareContactId)->update($data);
            return $this->shareContactsModel->where('id', $shareContactId)->first();
        }
        return $this->shareContactsModel->create($data);
    }
    
    public function deleteShareContact($data, $shareContactId, $userId) {
        $this->shareContactsModel->where('id', $shareContactId)->where('user_id', $userId)->update($data);
        return $this->shareContactsModel->where('id', $shareContactId)->first();
    }

    public function checkShareContactExist($email, $user_id, $shareContactId = null) {
        $model = $this->shareContactsModel->where('email', $email);
        $model = $model->where('user_id', $user_id);
        $model = $model->where('is_delete', 0);
        if (!empty($shareContactId)) {
            $model = $model->where('id', '!=', $shareContactId);
        }
        return $model->get();
    }

    public function getShareContact($user_id) {
        $model = $this->shareContactsModel
                ->where('is_delete', 0)
                ->where('user_id', $user_id);
        if (!empty($shareContactId)) {
            $model = $model->where('id', '!=', $shareContactId);
        }
        return $model->get();
    }
    
    public function getShareContactList($user_id) {
        $model = $this->shareContactsModel
                ->where('is_delete', 0)
                ->where('user_id', $user_id);
        if (!empty($shareContactId)) {
            $model = $model->where('id', '!=', $shareContactId);
        }
        return $model->get();
    }

    private function storeEngineType($userId, $engine) {
        // $enginType = EnginType::where('user_id',$userId)->where('name',$engine)->first();
        // if(!$enginType){
        $enginType = new EnginType;
        // }
        $enginType->user_id = $userId;
        $enginType->name = $engine;
        $enginType->save();
        return['id' => $enginType->id];
    }

    public function deleteServiceById($user_id) {
        $user_services = $this->userModel->where('user_id', $user_id)->get();

        if (count($user_services) > 0)
            $this->userModel->where('user_id', $user_id)->delete();
    }

    public function deleteServiceByServiceId($service_id, $user_id) {

        $user_services = $this->userModel->where('id', $service_id)->first();

        if (!empty($user_services)) {

            if ($user_services['user_id'] == $user_id) {
                $this->userModel->where('id', $service_id)->delete();
                $status_array = ['status' => 'success', 'message' => 'Your service details has been deleted successfully.'];
            } else {
                $status_array = ['status' => 'failed', 'message' => 'This user service not belongs to logged in user.'];
            }
        } else {
            $status_array = ['status' => 'failed', 'message' => 'User service not found.'];
        }

        return $status_array;
    }

    public function getSeaServiceDetails($service_id, $user_id) {

        $user_services = $this->userModel->where('id', $service_id)->first();

        if (!empty($user_services)) {
            $engineType = EnginType::where('user_id', Auth::user()->id)->orWhereNull('user_id')->pluck('name', 'id')->toArray();
            if ($user_services['user_id'] == $user_id) {
                $status_array = ['status' => 'success', 'message' => 'Your service details has been found successfully.', 'sea_service_details' => $user_services, 'engineType' => $engineType];
            } else {
                $status_array = ['status' => 'failed', 'message' => 'This user service not belongs to logged in user.'];
            }
        } else {
            $status_array = ['status' => 'failed', 'message' => 'User service not found.'];
        }

        return $status_array;
    }

    public function getSeaServiceDetailsByUserId($user_id) {
        return $this->userModel->where('user_id', $user_id)->orderBy('from', 'desc')->get();
    }

    public function getUserShippingCompanyWithId($user_id, $text) {
        return $this->userModel->distinct()->select('company_name')->where('user_id', $user_id)->where('company_name', 'LIKE', '%' . $text . '%')->get();
    }

    public function getUserShippingManningWithId($user_id, $text) {
        return $this->userModel->distinct()->select('manning_by')->where('user_id', $user_id)->where('manning_by', 'LIKE', '%' . $text . '%')->get();
    }

    public function getUserShipName($user_id, $text, $company_name) {
        return $this->userModel->distinct()->select('ship_name', 'ship_type', 'grt', 'bhp', 'engine_type', 'ship_flag', 'other_engine_type')->where('user_id', $user_id)->where('company_name', $company_name)->where('ship_name', 'LIKE', '%' . $text . '%')->get();
    }

}

?>