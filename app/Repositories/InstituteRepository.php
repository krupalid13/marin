<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 5/17/2017
 * Time: 4:19 PM
 */

namespace App\Repositories;
use App\InstituteRegistration;
use App\InstituteNotice;
use App\InstituteImageGallery;
use App\AdvertiserCourseDiscountMapping;


class InstituteRepository
{
    private $instituteRegistrationModel;
    private $instituteNoticeModel;

    function __construct() {
        $this->instituteRegistrationModel = new InstituteRegistration();
        $this->instituteNoticeModel = new InstituteNotice();
        $this->instituteImageGalleryModel = new InstituteImageGallery();
    }

    public function store($data){
        return $this->instituteRegistrationModel->create($data);
    }

    public function getDataByUserId($user_id,$options)
    {
        if(empty($options)){
            $user =  $this->instituteRegistrationModel->where('institute_id', $user_id)->get();
        }elseif(isset($options['with'])){
            $user =  $this->instituteRegistrationModel->where('institute_id', $user_id)->with('institute_details')->get();
        }
        return $user;
    }

    public function getDetailsByCompanyId($user_id){
        $a = $this->instituteRegistrationModel->where('user_id', $user_id)->get();
        return $a;
    }

    public function getDetailsById($id){
        return $this->instituteRegistrationModel->where('id', $id)->get();
    }

    public function update($data,$institute_id){
        return $this->instituteRegistrationModel->where('user_id', $institute_id)->update($data);
    }

    public function storeOrUpdateNotice($data,$institute_id){

        $institute = $this->instituteNoticeModel->where('institute_id',$institute_id)->first();
        
        if($institute){
            unset($data['_token']);
            return $this->instituteNoticeModel->where('institute_id',$institute_id)->update($data);
        }
        return $this->instituteNoticeModel->create($data);
    }

    public function storeImageGallery($data,$institute_id){
        return $this->instituteImageGalleryModel->create($data);
    }

    public function getInstituteListByCourse($course_type,$course_id,$institute_id){
        $instituteRegistrationModel = $this->instituteRegistrationModel;

        if(!empty($institute_id)){
            $instituteRegistrationModel = $instituteRegistrationModel->whereHas('institute_courses', function ($c) use ($course_type,$course_id,$institute_id){
                $c->where('course_type',$course_type)->where('course_id',$course_id)->where('institute_id',$institute_id);
            });
        }
        $instituteRegistrationModel = $instituteRegistrationModel->orderBy('id','desc');

        $instituteRegistrationModel = $instituteRegistrationModel->whereHas('institute_courses', function ($c) use ($course_type,$course_id){
            $c->where('course_type',$course_type)->where('course_id',$course_id);

        })->with(['institute_courses' => function ($c) use ($course_type,$course_id){
            $c->whereHas('institute_batches', function ($ct){
                $ct->where('start_date','>=',date('Y-m-d'))->where('status','1');
            })->with(['institute_batches'=> function ($ct){
                $ct->where('start_date','>=',date('Y-m-d'))->where('status','1');
            }])->where('course_type',$course_type)->where('course_id',$course_id);
        }]);

        return $instituteRegistrationModel->get();
    }

    public function getExistitngCourseDiscount($filter){
        $model = new AdvertiserCourseDiscountMapping();

        if(!empty($filter)){
            if (isset($filter['institute_name']) && !empty($filter['institute_name'])){
                
                $model = $model->whereHas('company_registration', function ($c) use ($filter){
                    $c->where('user_id',$filter['institute_name']);

                })->with(['company_registration']);
            }
        
            if (isset($filter['course_type']) && !empty($filter['course_type'])){
                $model = $model->whereHas('batch_details.course_details.courses', function ($c) use ($filter){
                    $c->where('course_type',$filter['course_type']);

                })->with(['batch_details.course_details.courses']);
            }

            if (isset($filter['course_name']) && !empty($filter['course_name'])){
                $model = $model->whereHas('batch_details.course_details.courses', function ($c) use ($filter){
                    $c->where('course_type',$filter['course_type'])->where('course_id',$filter['course_name']);

                })->with(['batch_details.course_details.courses']);
            }
        }

        $model = $model->with('company_registration','batch_details.course_details.courses','batch_details.course_details.institute_registration_detail')->orderBy('id','desc')->paginate(10);

        return $model;
    }
}