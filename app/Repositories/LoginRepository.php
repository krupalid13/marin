<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 5/11/2017
 * Time: 10:50 AM
 */

namespace App\Repositories;
use App\User;

class LoginRepository
{
    private $contactUsModel;

    function __construct() {
        $this->contactUsModel = new User();
    }

    public function store($contact_us_data){
        return $this->contactUsModel->create($contact_us_data);
    }
}