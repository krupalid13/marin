<?php

namespace App\Repositories;
use App\AdvertisementsLocations;

class AdvertiseLocationRepository
{
	private $advertiseModel;
    function __construct()
    {
        $this->advertiseModel = new AdvertisementsLocations();
    }

    public function store($data){
        return $this->advertiseModel->create($data);
    }

    public function get_advertisements($limit,$state_id,$city_id){
    	
        //$result = AdvertisementsLocations::select('advertise_id')->distinct()->with('advertisement_details');

        if(isset($city_id) && !empty($city_id)){
    	   $results =  AdvertisementsLocations::select('advertise_id')->distinct()->with('advertisement_details.company_registration_details')->where('city_id',$city_id)->whereHas('advertisement_details',function ($query) {
                    $query->where('status', 2);
            })->orderByRaw("RAND()")->take($limit)->get()->toArray();
        }else{
            $results =  AdvertisementsLocations::select('advertise_id')->distinct()->with('advertisement_details.company_registration_details')->whereHas('advertisement_details',function ($query) {
                    $query->where('status', 2);
            })->orderByRaw("RAND()")->take($limit)->get()->toArray();
        }
    	
    	$advertise_list = [];
    	if(count($results) > 0){
	    	foreach ($results as $key => $value) {
	    		$advertise_list[] = $value['advertise_id'];
	    	}
    	}
    	if(count($results) < $limit){
    		$new_count = count($results);
    		$new_limit = $limit - $new_count;
    		$results1 =  AdvertisementsLocations::select('advertise_id')->distinct()->with('advertisement_details.company_registration_details')->where('state_id',$state_id)->where('city_id','!=',$city_id)->whereNotIn('advertise_id',$advertise_list)->whereHas('advertisement_details',function ($query) {
                    $query->where('status', 2);
            })->orderByRaw("RAND()")->take($new_limit)->get()->toArray();

            if(count($results1) > 0){

        		foreach ($results1 as $key => $value) {
        			array_push($results, $results1[$key]);
        		}
            }
    	}
    	
    	if(count($results) > 0){
	    	foreach ($results as $key => $value) {
	    		$advertise_list[] = $value['advertise_id'];
	    	}
	    }
    	
    	if(count($results) < $limit){
    		$new_count = count($results);
    		$new_limit = $limit - $new_count;

    		$results1 =  AdvertisementsLocations::select('advertise_id')->distinct()->with('advertisement_details.company_registration_details')->whereNotIn('advertise_id',$advertise_list)->whereHas('advertisement_details',function ($query) {
                    $query->where('status', 2);
            })->orderByRaw("RAND()")->take($new_limit)->get()->toArray();

    		foreach ($results1 as $key => $value) {
    			array_push($results, $results1[$key]);
    		}
    	}
        
    	return $results;
    }

}