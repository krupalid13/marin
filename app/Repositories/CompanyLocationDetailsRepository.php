<?php

namespace App\Repositories;
use App\CompanyLocationDetails;

class CompanyLocationDetailsRepository
{
    private $companyLocationDetailsModel;

    function __construct() {
        $this->companyLocationDetailsModel = new CompanyLocationDetails();
    }

    public function store($data){

        if(isset($data['existing_location_id']) && !empty($data['existing_location_id'])){
            $location_id = $data['existing_location_id'];
            unset($data['existing_location_id']);
            return $this->companyLocationDetailsModel->where('id', $location_id)->update($data);
        }

        if(isset($data['existing_location_id'])){
            unset($data['existing_location_id']);
        }

        return $this->companyLocationDetailsModel->create($data);
    }

    public function deleteRecordsByCompanyId($company_id){
        $data = $this->companyLocationDetailsModel->where('company_id' , $company_id)->get();

        if(count($data) > 0)
            $this->companyLocationDetailsModel->where('company_id' , $company_id)->delete();
    }

    public function deleteRecordsByCompanyIdWhereIdNotExist($company_list,$company_id){
        $this->companyLocationDetailsModel->whereNotIn('id',$company_list)->where('company_id',$company_id)->delete();
    }
}

?>