<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 4/29/2017
 * Time: 12:15 PM
 */

namespace App\Repositories;
use App\Subscription;
use App\CompanySubscription;
use App\InstituteSubscription;
use App\AdvertiserSubscription;
use App\SeafarerSubscription;

class SubscriptionRepository
{
    private $subscription;
    private $companySubscription;
    private $instituteSubscription;
    private $advertiserSubscription;
    private $seafarerSubscription;

    function __construct()
    {
        $this->subscription = new Subscription();
        $this->companySubscription = new CompanySubscription();
        $this->instituteSubscription = new InstituteSubscription();
        $this->advertiserSubscription = new AdvertiserSubscription();
        $this->seafarerSubscription = new SeafarerSubscription();
    }

    public function getAllSubscriptionDetails($role = NULL)
    {
        return $this->subscription->with('subscriptionFeatures')->where('type',$role)->get();
    }

    public function getAllActiveSubscription($role = NULL)
    {
        $result = $this->subscription->with('subscriptionFeatures')->where('is_active',1);
        if (!empty($role)) {
            $result->where('type',$role);
        }

      return $result->get();
    }

    public function getAllSubscriptionDetailsBySubscriptionId($subscription_id)
    {
        return $this->subscription->with('subscriptionFeatures')->where('id',$subscription_id)->get();
    } 

    public function getFreeSubscriptionDetailsByUserRole($role){
        return $this->subscription->where('type',$role)->where('duration_title','free')->first();
    }

    public function getCompanySubscriptionByRegistrationId($id){
        return $this->companySubscription->with('order','order.order_payment')->where('company_reg_id',$id)->get();
    }

    public function getCompanySubscriptionByRegistrationIdWithPagination($id){
        return $this->companySubscription->with('order','order.order_payment')->where('company_reg_id',$id)->with('order.order_payment')->orderBy('id','desc')->paginate('12');
    }

    public function getInstituteSubscriptionByRegistrationId($id){
        return $this->instituteSubscription->with('order','order.order_payment')->where('institute_reg_id',$id)->get();
    }

    public function getInstituteSubscriptionByRegistrationIdWithPagination($id){
        return $this->instituteSubscription->with('order','order.order_payment')->where('institute_reg_id',$id)->with('order.order_payment')->orderBy('id','desc')->paginate('12');
    }

    public function getAdvertiserSubscriptionByRegistrationId($id){
        return $this->advertiserSubscription->with('order','order.order_payment')->where('company_reg_id',$id)->get();
    }

    public function getAdvertiserSubscriptionByRegistrationIdWithPagination($id){
        return $this->advertiserSubscription->with('order','order.order_payment')->where('company_reg_id',$id)->with('order.order_payment')->orderBy('id','desc')->paginate('12');
    }

    public function getSeafarerSubscriptionByRegistrationId($id){
        return $this->seafarerSubscription->with('order','order.order_payment')->where('user_id',$id)->get();
    }

    public function getSeafarerSubscriptionByRegistrationIdWithPagination($id){
        return $this->seafarerSubscription->with('order','order.order_payment')->where('user_id',$id)->with('order.order_payment')->orderBy('id','desc')->paginate('12');
    }

    public function saveEditCompanySubscription($array)
    {   
        if (isset($array['id']) && !empty($array['id'])) {
            $obj = $this->companySubscription->find($array['id']);
        } else {
            $obj = new CompanySubscription;
        }

        if (isset($array['company_reg_id']) && !empty($array['company_reg_id'])) {
            $obj->company_reg_id = $array['company_reg_id'];
        }

        if (isset($array['order_id']) && !empty($array['order_id'])) {
            $obj->order_id = $array['order_id'];
        }

        if (isset($array['subscription_details']) && !empty($array['subscription_details'])) {

            $obj->subscription_details = $array['subscription_details'];
        }

        if (isset($array['status']) && !empty($array['status'])) {
            $obj->status = $array['status'];
        }

        if (isset($array['valid_from']) && !empty($array['valid_from'])) {
            $obj->valid_from = $array['valid_from'];
        }

        if (isset($array['valid_to']) && !empty($array['valid_to'])) {
            $obj->valid_to = $array['valid_to'];
        }


        return  $obj->save();
    }

    public function saveEditInstituteSubscription($array)
    {
        if (isset($array['id']) && !empty($array['id'])) {
            $obj = $this->instituteSubscription->find($array['id']);
        } else {
            $obj = new InstituteSubscription;
        }

        if (isset($array['institute_reg_id']) && !empty($array['institute_reg_id'])) {
            $obj->institute_reg_id = $array['institute_reg_id'];
        }

        if (isset($array['order_id']) && !empty($array['order_id'])) {
            $obj->order_id = $array['order_id'];
        }

        if (isset($array['subscription_details']) && !empty($array['subscription_details'])) {
            $obj->subscription_details = $array['subscription_details'];
        }

        if (isset($array['status']) && !empty($array['status'])) {
            $obj->status = $array['status'];
        }

        if (isset($array['valid_from']) && !empty($array['valid_from'])) {
            $obj->valid_from = $array['valid_from'];
        }

        if (isset($array['valid_to']) && !empty($array['valid_to'])) {
            $obj->valid_to = $array['valid_to'];
        }


        return  $obj->save();
    }

    public function saveEditAdvertiserSubscription($array)
    {
        if (isset($array['id']) && !empty($array['id'])) {
            $obj = $this->advertiserSubscription->find($array['id']);
        } else {
            $obj = new AdvertiserSubscription;
        }

        if (isset($array['company_reg_id']) && !empty($array['company_reg_id'])) {
            $obj->company_reg_id = $array['company_reg_id'];
        }

        if (isset($array['order_id']) && !empty($array['order_id'])) {
            $obj->order_id = $array['order_id'];
        }

        if (isset($array['subscription_details']) && !empty($array['subscription_details'])) {
            $obj->subscription_details = $array['subscription_details'];
        }

        if (isset($array['status']) && !empty($array['status'])) {
            $obj->status = $array['status'];
        }

        if (isset($array['valid_from']) && !empty($array['valid_from'])) {
            $obj->valid_from = $array['valid_from'];
        }

        if (isset($array['valid_to']) && !empty($array['valid_to'])) {
            $obj->valid_to = $array['valid_to'];
        }


        return  $obj->save();
    }

    public function saveEditSeafarerSubscription($array)
    {
        if (isset($array['id']) && !empty($array['id'])) {
            $obj = $this->seafarerSubscription->find($array['id']);
        } else {
            $obj = new SeafarerSubscription;
        }

        if (isset($array['user_id']) && !empty($array['user_id'])) {
            $obj->user_id = $array['user_id'];
        }

        if (isset($array['order_id']) && !empty($array['order_id'])) {
            $obj->order_id = $array['order_id'];
        }

        if (isset($array['subscription_details']) && !empty($array['subscription_details'])) {
            $obj->subscription_details = $array['subscription_details'];
        }

        if (isset($array['status']) && !empty($array['status'])) {
            $obj->status = $array['status'];
        }

        if (isset($array['valid_from']) && !empty($array['valid_from'])) {
            $obj->valid_from = $array['valid_from'];
        }

        if (isset($array['valid_to']) && !empty($array['valid_to'])) {
            $obj->valid_to = $array['valid_to'];
        }


        return  $obj->save();
    }

    public function getCompanyUsersWhosSubscriptionExpires($day)
    {
        $nxt_day = $day + 1;
        return $this->companySubscription->with('company_registration.user')->where('status',1)->where('valid_to',date('Y-m-d', strtotime('+'.$day.' days')))->whereNotIn('company_reg_id', function($q) use ($nxt_day){
                   $q->select('company_reg_id')->from(with(new CompanySubscription)->getTable())->where('status',1)->where('valid_from','>=',date('Y-m-d', strtotime('+'.$nxt_day.' days')));
                })->get();

    }

    public function getInstituteUsersWhosSubscriptionExpires($day)
    {
        $nxt_day = $day + 1;
        return $this->instituteSubscription->with('institute_registration.user')->where('status',1)->where('valid_to',date('Y-m-d', strtotime('+'.$day.' days')))->whereNotIn('institute_reg_id', function($q) use ($nxt_day){
                   $q->select('institute_reg_id')->from(with(new InstituteSubscription)->getTable())->where('status',1)->where('valid_from','>=',date('Y-m-d', strtotime('+'.$nxt_day.' days')));
                })->get();

    }

    public function getAdvertiserUsersWhosSubscriptionExpires($day)
    {
        $nxt_day = $day + 1;
        return $this->advertiserSubscription->with('company_registration.user')->where('status',1)->where('valid_to',date('Y-m-d', strtotime('+'.$day.' days')))->whereNotIn('company_reg_id', function($q) use ($nxt_day){
                   $q->select('company_reg_id')->from(with(new AdvertiserSubscription)->getTable())->where('status',1)->where('valid_from','>=',date('Y-m-d', strtotime('+'.$nxt_day.' days')));
                })->get();

    }

    // public function getSeafarerUsersWhosSubscriptionExpires($day)
    // {
    //     $nxt_day = $day + 1;
    //     return $this->seafarerSubscription->with('company_registration.user')->where('status',1)->where('valid_to',date('Y-m-d', strtotime('+'.$day.' days')))->whereNotIn('company_reg_id', function($q) use ($nxt_day){
    //                $q->select('company_reg_id')->from(with(new SeafarerSubscription)->getTable())->where('status',1)->where('valid_from','>=',date('Y-m-d', strtotime('+'.$nxt_day.' days')));
    //             })->get();

    // }
    
}