<?php

namespace App\Repositories;
use App\JobApplication;

class JobApplicationRepository
{
    private $jobApplicationModel;

    function __construct() {
        $this->jobApplicationModel = new JobApplication();
    }

    public function store($data){
        return $this->jobApplicationModel->create($data);
    }

    public function getUserAppliedJobsByUserId($user_id){
        return $this->jobApplicationModel->where('user_id', $user_id)->get();
    }

    public function checkJobApplyAvailability($data){
        
        $data = $this->jobApplicationModel->where(['job_id' => $data['job_id'],'company_id' => $data['company_id'], 'user_id' => $data['user_id']])->orderBy('created_at','desc')->first();

        $apply_job = '1';
        if(isset($data) && !empty($data)){
            
            $current_date = time();
            $job_create_date = strtotime($data['created_at']);
            $datediff = $current_date - $job_create_date;

            $days = floor($datediff / (60 * 60 * 24));
            
            if ($days > 15) {
                $apply_job = '1';
            }else{
                $apply_job = '0';
            }
        }
        return $apply_job;
    }

}