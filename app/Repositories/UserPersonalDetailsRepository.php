<?php

namespace App\Repositories;
use App\UserPersonalDetail;

class UserPersonalDetailsRepository{
    Private $userPersonalDetailsModel;
    function __construct()
    {
        $this->userPersonalDetailsModel = new UserPersonalDetail();
    }

    public function getUserPersonalDetailsById($user_id)
    {
        $user = $this->userPersonalDetailsModel->where('user_id', $user_id)->get();
        return $user;
    }

    public function store($data)
    {
        $data = $this->userPersonalDetailsModel->create($data);
        return $data;
    }

    public function updateByUserId($data,$user_id)
    {
        $data =  $this->userPersonalDetailsModel->where('user_id', $user_id)->update($data);
        return $data;
    }
}

?>
