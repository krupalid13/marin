<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 5/17/2017
 * Time: 4:19 PM
 */

namespace App\Repositories;
use App\InstituteDetail;
use App\InstituteCourses;
use App\Courses;

class InstituteDetailsRepository
{
    private $instituteDetailsModel;

    function __construct() {
        $this->instituteDetailsModel = new InstituteDetail();
        $this->instituteCourseModel = new InstituteCourses();
        $this->courseModel = new InstituteCourses();
        $this->allCourseModel = new Courses();
    }

    public function store($data){
        return $this->instituteDetailsModel->create($data);
    }

    public function getDetailsByInstituteId($institute_id){
        return $this->instituteDetailsModel->where('institute_id', $institute_id)->get();
    }

    public function update($data,$institute_id){
        return $this->instituteDetailsModel->where('institute_id', $institute_id)->update($data);
    }

    public function getCourseNameByCourseType($course_type,$institute_id){
        $courseModel = $this->courseModel;
        
        if(!empty($institute_id)){
            $courseModel = $courseModel->where('course_type',$course_type)->where('status','<>','0')->where('institute_id',$institute_id)->with('course_details')->get();
        }else{
            return $this->allCourseModel->where('course_type',$course_type)->where('status','<>','0')->get();
        }
        
        return $courseModel;
    }

    public function getInstituteCourseDetailsById($course_id){
        $courseModel = $this->courseModel;
        
        if(!empty($course_id)){
            $courseModel = $courseModel->where('id',$course_id)->where('status','<>','0')->first();
        }
        
        return $courseModel;
    }
}