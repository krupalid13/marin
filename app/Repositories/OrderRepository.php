<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 5/2/2017
 * Time: 3:17 PM
 */

namespace App\Repositories;
use App\Order;

class OrderRepository
{
    private $orderModel;

    function __construct() {

        $this->orderModel = new Order();
    }

    public function store($array){

        return $this->orderModel->create($array);
    }

    public function getOrderDetailById($order_id,$options){
        if(!empty($options)){
            return $this->orderModel->where('id',$order_id)->with($options['with'])->get();
        }
    	return $this->orderModel->where('id',$order_id)->get();
    }

    public function updateOrderStatus($update_status, $order_id){
    	return $this->orderModel->where('id',$order_id)->update(['status' => $update_status]);
    }

    public function updateOrder($data, $order_id){
        return $this->orderModel->where('id',$order_id)->update($data);
    }

    public function checkIfAppliedToBatch($batch_id,$user_id){
        //3 for cancelled status
        return $this->orderModel->where('batch_id',$batch_id)->where('user_id',$user_id)->where('status','!=','3')->with('order_payments')->get();
    }

    public function getOrderByInstituteBatches($batches_array,$paginate,$filter){
        $orders = $this->orderModel->orderBy('id','desc');
        $date_now = date("Y-m-d H:i:s");

        if(!empty($filter)){
            if(isset($filter['name']) && !empty($filter['name'])){
                $orders = $orders->whereHas('user', function ($c) use ($filter) {
                    $c->where('first_name', 'like', '%'.$filter['name'].'%');
               });
            }

            if(isset($filter['email']) && !empty($filter['email'])){
                $orders = $orders->whereHas('user', function ($c) use ($filter) {
                    $c->where('email', 'like', '%'.$filter['email'].'%');
                });
            }

            if(isset($filter['mobile']) && !empty($filter['mobile'])){
                $orders = $orders->whereHas('user', function ($c) use ($filter) {
                    $c->where('mobile', $filter['mobile']);
                });
            }

            if(isset($filter['status']) && $filter['status'] != ''){
                $orders = $orders->where('status', $filter['status']);
            }

            if(isset($filter['from_date']) && !empty($filter['from_date'])){
                $orders = $orders->whereHas('batch_details', function ($c) use ($filter) {
                    $c->where('start_date', '>=' , date('Y-m-d',strtotime($filter['from_date'])));
                });
            }

            if(isset($filter['to_date']) && !empty($filter['to_date'])){
                $orders = $orders->whereHas('batch_details', function ($c) use ($filter) {
                    $c->where('start_date', '<' , date('Y-m-d',strtotime($filter['to_date'])));
                });
            }

            if(isset($filter['batch_type']) && !empty($filter['batch_type'])){
                    $orders = $orders->whereHas('batch_details', function ($c) use ($filter) {
                    $c->where('batch_type', $filter['batch_type']);
                });
            }
        }
        $orders = $orders->whereIn('batch_id',$batches_array)->with('order_history','user.professional_detail','user.passport_detail','user.wkfr_detail','batch_details.course_details.courses','batch_details.batch_location.location.state','batch_details.batch_location.location.city');

        if($paginate == '1'){
            return $orders->paginate('10');
        }
        return $orders->get();
    }

    public function changeInstituteBatchOrderStatus($order_id,$batch_id,$status){
        return $this->orderModel->where('id',$order_id)->where('batch_id',$batch_id)->update(['status' => $status]);
    }

    public function cancelOrder($order_id,$user_id,$batch_id){
        return $this->orderModel->where('id',$order_id)->where('user_id',$user_id)->where('batch_id',$batch_id)->update(['status' => '3']);
    }

    public function getOrderDetailByUserId($user_id,$options=null){
        if(!empty($options)){
            return $this->orderModel->where('user_id',$user_id)->with($options['with'])->orderBy('id','desc')->paginate('10');
        }
        return $this->orderModel->where('user_id',$user_id)->orderBy('id','desc')->get();
    }

    public function getOrderDetailByStatus($status,$options=null){
        $orderModel = $this->orderModel->where('status',$status);

        if(isset($options) && !empty($options)){
            $orderModel = $orderModel->with($options);
        }

        return $orderModel->get();
    }

    public function updateOrderByBatchIdAndPreffType($batch_id,$preffered_type,$data){
        return $this->orderModel->where('batch_id',$batch_id)->where('preffered_type',$preffered_type)->update($data);
    }
}