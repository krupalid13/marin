<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 4/21/2017
 * Time: 4:54 PM
 */

namespace App\Repositories;
use App\UserGmdssDetail;

class UserGmdssDetailsRepository
{
    Private $userGmdssDetailsModel;
    function __construct()
    {
        $this->userGmdssDetailsModel = new UserGmdssDetail();
    }

    public function store($data)
    {
        $data = $this->userGmdssDetailsModel->create($data);
        return $data;
    }

    public function getDetailsByUserId($user_id){
        return $this->userGmdssDetailsModel->where('user_id', $user_id)->get();
    }

    public function updateByUserId($data, $user_id) {
        return $this->userGmdssDetailsModel->where('user_id', $user_id)->update($data);
    }

}