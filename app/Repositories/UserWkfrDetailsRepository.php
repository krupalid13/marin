<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 4/21/2017
 * Time: 4:56 PM
 */

namespace App\Repositories;
use App\UserWkfrDetail;

class UserWkfrDetailsRepository
{
    Private $userWkfrDetailsModel;
    function __construct()
    {
        $this->userWkfrDetailsModel = new UserWkfrDetail();
    }

    public function store($data)
    {
        $data = $this->userWkfrDetailsModel->create($data);
        return $data;
    }

    public function getDetailsByUserId($user_id){
        return $this->userWkfrDetailsModel->where('user_id', $user_id)->get();
    }

    public function updateByUserId($data, $user_id) {
        return $this->userWkfrDetailsModel->where('user_id', $user_id)->update($data);
    }
}