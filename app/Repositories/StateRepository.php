<?php

namespace App\Repositories;
use App\State;

class StateRepository
{
    private $stateModel;

    function __construct() {
        $this->stateModel = new State();
    }

    public function getIdByStateName($name){
    	return $this->stateModel->where('name', 'like', '%'.$name.'%')->first();
    }

    public function getAllStatesWithCities(){
    	return $this->stateModel->with('cities')->get();
    }
}