<?php

namespace App\Repositories;
use App\CompanyDownloads;
use App\FeatureEmail;
use DB;

class CompanyDownloadsRepository
{
    private $companyDownloadsModel;
    private $featureEmailModel;

    function __construct() {
        $this->companyDownloadsModel = new CompanyDownloads();
        $this->featureEmailModel = new FeatureEmail();
    }

    public function resumeDownloadCountPerDay($user_id,$subscription_feature_id){
    	$resume = $this->companyDownloadsModel->where('company_id',$user_id)->where('subscription_feature_id',$subscription_feature_id)->whereDay('created_at', '=', date('d'))->get();
    	return count($resume);
    }

    public function store($data){
    	return $this->companyDownloadsModel->create($data);
    }

    public function getResumeDownloadList($company_id){

        /*$result =  $this->companyDownloadsModel->select('seafarer_id','company_id','created_at','updated_at')->where('company_id',$company_id)->with('user_details')->orderBy('id','DESC')->paginate('10');
        
        return $result;*/

        $result = $this->companyDownloadsModel->select('seafarer_id','company_id')->where('company_id',$company_id)->with('user_details.professional_detail')->groupBy('seafarer_id','company_id')->paginate('10');

        return $result;

        /*$users = DB::table('company_dowloads')->select('seafarer_id','company_id','created_at')->where('company_id',$company_id)->orderBy('created_at','DESC')->groupBy('seafarer_id','company_id')->paginate('10');

        dd($users->toArray());*/
    }

    public function getFeatureCount($company_id,$start_date,$end_date,$feature_type){
        return $this->companyDownloadsModel->where(['company_id' => $company_id,'type' => $feature_type])->whereBetween('created_at', [$start_date,$end_date])->get()->count(); 
    }

    public function storeEmailFeature($data){
        return $this->featureEmailModel->create($data);
    }

    public function getEmailFeatureCount($parent_id,$start_date,$end_date,$registered_as){
        return $this->featureEmailModel->where(['parent_id' => $parent_id,'registered_as' => $registered_as])->whereBetween('created_at', [$start_date,$end_date])->get()->count(); 
    }

}