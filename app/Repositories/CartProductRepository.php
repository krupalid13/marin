<?php namespace App\Repositories;

use App\CartProduct;

class CartProductRepository {

	private $cartProductModel;

	function __construct() {

		$this->cartProductModel = new CartProduct();
	}

	public function storeSubscriptionCart($array) {
		return $this->cartProductModel->create($array);
	}

    public function updateSubscriptionCart($array) {
        return $this->cartProductModel->where(['user_id' => $array['user_id']])->update($array);
    }

	public function getCartDetailsBySubidanduserid($user_id,$subscription_id){
        return $this->cartProductModel->where(['user_id' => $user_id , 'subscription_id' => $subscription_id])->get();
    }

    public function getCartDetails($user_id){
        return $this->cartProductModel->where('user_id' , $user_id)->with('subscription')->get();
    }

	public function store($array, $user_id = null) {

		if(!empty($user_id)) {
			$array['user_id'] = $user_id;
			$array['cart_key'] = null;
			return $this->cartProductModel->create($array);
		} else {
			return $this->cartProductModel->create($array);
		}
	}

	public function updateCartProductVaraintDetailsByCartId($array, $cart_id) {
		return $this->cartProductModel->where('id', $cart_id)->update($array);
	}

	public function getCartDetailByProductIdAndVariantIdAndCartKey($product_id, $product_variant_id, $cart_key){
		return $this->cartProductModel->where(['product_id' => $product_id,'product_variant_id' => $product_variant_id, 'cart_key' => $cart_key])->with('subscription')->get();
	}
	public function getCartDetailByProductIdAndVariantIdAndUserId($product_id, $product_variant_id, $user_id){
		return $this->cartProductModel->where(['product_id' => $product_id,'product_variant_id' => $product_variant_id, 'user_id' => $user_id])->with('subscription')->get();
	}

	public function updateByCartKeyProductIdProductVariantId($array, $cart_key = null, $product_id, $product_variant_id, $user_id = null) {

		if(!empty($user_id)) {
			$array['cart_key'] = null;
			return $this->cartProductModel->where(['product_id' => $product_id, 'product_variant_id' => $product_variant_id, 'user_id' => $user_id])
						->update($array);
		} elseif ( !empty($cart_key) ) {
			return $this->cartProductModel->where(['cart_key' => $cart_key,'product_id' => $product_id, 'product_variant_id' => $product_variant_id])
						->update($array);
		}		
	}

	public function getCartProductDetailsByUserId($user_id) {
		return $this->cartProductModel->where('user_id', $user_id)->with('site_p_category', 'subscription')->get();
	}

	public function getCartProductDetailsByCartKey($cart_key) {
		return $this->cartProductModel->where('cart_key', $cart_key)->whereNull('user_id')->with('site_p_category', 'subscription')->get();
	}

	public function deleteFromCart($cart_product_id) {
		return $this->cartProductModel->where('id', $cart_product_id)->delete();
	}

	public function updateCartUserId($cart_key, $user_id) {
		return $this->cartProductModel->where('cart_key', $cart_key)->update(['cart_key' => null, 'user_id' => $user_id]);
	}

	public function deleteByUserId($user_id) {
		return $this->cartProductModel->where('user_id', $user_id)->delete();
	}

	public function getDetailByCartId($cart_id) {
		return $this->cartProductModel->where('id', $cart_id)->with('subscription')->get();
	}

	public function getCartDetailByUserId($user_id) {
		return $this->cartProductModel->where('user_id', $user_id)->get();
	}
	public function updateCartByCartId($array, $cart_id) {
		return $this->cartProductModel->where('id', $cart_id)->update($array);
	}

	public function updateCartProductAvailabilityByUserId($user_id) {
		return $this->cartProductModel->where('user_id', $user_id)->update(['is_pincode_applicable' => 1]);
	}


}