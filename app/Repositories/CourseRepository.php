<?php

namespace App\Repositories;
use App\CourseApplication;

class CourseRepository
{
	private $courseModel;
    function __construct()
    {
        $this->courseModel = new CourseApplication(); 
    }

    public function store($data){
        
        return $this->courseModel->create($data);
    }

    public function prev_applied($data){
        $prev_date = date('Y-m-d H:i:s', strtotime('-15 days'));
        $result = $this->courseModel->where('course_id',$data['course_id'])->where('institute_id',$data['institute_id'])->where('user_id',$data['user_id'])->where('created_at','>',$prev_date)->get()->toArray();

        if(count($result) > 0){
            return true;
        }
        return false;
    }

    public function getAllUserAppliedCourses($institute_id, $options, $paginate, $filter){

    	$course_applied = $this->courseModel;
        
        if(isset($options['with'])){

            if(isset($institute_id) && !empty($institute_id)){
                $course_applied = $course_applied->with($options['with'])->where('institute_id',$institute_id)->orderBy('id','desc');
            }else{
                $course_applied = $course_applied->with($options['with'])->orderBy('id','desc');
            }
        }

        if(isset($filter) && !empty($filter)){

            if(isset($filter['seafarer_name']) && !empty($filter['seafarer_name'])){
                $course_applied = $course_applied->whereHas('user', function ($c) use ($filter) {
                    $c->where('first_name', 'like', '%'.$filter['seafarer_name'].'%');
                });
            }

            if(isset($filter['course_type']) && !empty($filter['course_type'])){
                $course_applied = $course_applied->whereHas('batch_details.course_details', function ($c) use ($filter) {
                    $c->where('course_type', $filter['course_type']);
                });
            }

            if(isset($filter['course_name']) && !empty($filter['course_details.course_name'])){
                $course_applied = $course_applied->whereHas('batch_details', function ($c) use ($filter) {
                    $c->where('course_name', $filter['course_name']);
                });
            }

            if(isset($filter['start_date']) && !empty($filter['start_date'])){
                $course_applied = $course_applied->whereHas('batch_details', function ($c) use ($filter) {
                    $c->where('start_date','>=',date('Y-m-d h:i:s',strtotime($filter['start_date'])));
                });
            }
        }
        if(!empty($paginate)){
            $course_applied = $course_applied->paginate($paginate);
        }
        return $course_applied;
    }
}
