<?php

    namespace App\Repositories;
    use App\User;
    use App\AdvertiseDetails;
    use App\Courses;
    use App\Rewards;
    use App\DocumentPermissions;

    class UserRepository{
        private $userModel;
        function __construct()
        {
            $this->userModel = new User();
            $this->rewardsModel = new Rewards();
            $this->courseModel = new Courses();
            $this->advertiseModel = new AdvertiseDetails();
        }

        public function getAdminDataByEmail($data){
            return $this->userModel->where(['email' => $data['email']],['registered_as' => 'admin'])->get();
        }

        public function checkEmail($email,$id=NULL){
            $userModel = $this->userModel;

            if(isset($id) && !empty($id)){
                $ids[0] = $id;
                $userModel = $userModel->where('email', $email)->whereNotIn('id', $ids)->get()->toArray();
            }else{
                $userModel = $userModel->where(['email' => $email])->get()->toArray();
            }

            return $userModel;
        }

        public function checkMobile($mobile,$id=NULL){
            $userModel = $this->userModel;
            
            if(isset($id) && !empty($id)){
                $ids[0] = $id;
                $userModel = $userModel->where('mobile', $mobile)->whereNotIn('id', $ids)->get()->toArray();
            }else{
                $userModel = $userModel->where(['mobile' => $mobile])->get()->toArray();
            }
            

            return $userModel;
        }

        public function getDataByUserId($user_id,$options=NULL)
        {
            if(empty($options)){
                $user =  $this->userModel->where('id', $user_id)->get();
            }elseif(isset($options['with'])){
                $user =  $this->userModel->where('id', $user_id)->with($options['with'])->get();
            }

            return $user;
        }

        public function store($user_data)
        {
           $user =  $this->userModel->create($user_data);
           return $user;
        }

        public function update($user_data,$user_id)
        {
           return $this->userModel->where('id', $user_id)->update($user_data);
        }

        public function addReferalDetails($user_id,$data){
            return $this->userModel->where('id', $user_id)->update($data);
        }

        public function getDetailByMobileAndOtp($mobile,$otp)
        {
            return $this->userModel->where(['mobile'=> $mobile , 'otp' => $otp])->get();
        }

        public function markMobileVerified($user_id)
        {
            return $this->userModel->where('id', $user_id)->update(['is_mob_verified'=> 1, 'otp' => NULL ]);
        }

        public function update_otp($user_id, $otp)
        {
            return $this->userModel->where('id', $user_id)->update(['otp'=> $otp]);
        }

        public function getUserByEmail($email)
        {
            return $this->userModel->where('email', $email)->get();
        }

        public function markEmailVerified($user_id)
        {
            return $this->userModel->where('id', $user_id)->update(['is_email_verified'=> 1]);
        }

        public function getAllSeafarersData($options){
            return $this->userModel->where('registered_as', NULL)->orWhere('registered_as', 'seafarer')->with($options['with'])->where('first_name','!=','temp')->orderby('id','desc')->paginate(10);
        }

        public function getAllCompanyData($options){
            return $this->userModel->where('registered_as', 'company')->with($options['with'])->where('first_name','!=','temp')->orderby('id','desc')->paginate(10);
        }

        public function getAllAdvertiserData($options,$filter){

            if(empty($filter)){
                $users = $this->userModel->where('registered_as', 'advertiser')->with($options['with'])->where('first_name','!=','temp')->orderby('id','desc')->paginate(10);
            }
            else {
                $users = $this->userModel->where('registered_as', 'advertiser');

                if (isset($filter['first_name']) && !empty($filter['first_name'])){
                    $users = $this->userModel->whereHas('company_registration_detail', function ($c) use ($filter) {
                            $c->where('first_name', 'like', '%'.$filter['first_name'].'%');
                    });
                }

                if (isset($filter['company_name']) && !empty($filter['company_name'])){
                    $users = $users->whereHas('company_registration_detail', function ($c) use ($filter) {
                            $c->where('company_name', 'like', '%'.$filter['company_name'].'%');
                    });
                }

                if (isset($filter['bussiness_category']) && !empty($filter['bussiness_category'])){
                    $users = $users->whereHas('company_registration_detail', function ($c) use ($filter) {
                        $c->where('bussiness_category', $filter['bussiness_category']);
                    });
                }

                if (isset($filter['bi_member']) && !empty($filter['bi_member'])){
                    $users = $users->whereHas('company_registration_detail', function ($c) use ($filter) {
                        $c->where('b_i_member', $filter['bi_member']);
                    });
                }

                if (isset($filter['state']) && !empty($filter['state'])){
                    $users = $users->whereHas('company_registration_detail.company_locations', function ($c) use ($filter) {
                        $c->where('state_id', $filter['state']);
                    });
                }

                if (isset($filter['city']) && !empty($filter['city'])){
                    $users = $users->whereHas('company_registration_detail.company_locations', function ($c) use ($filter) {
                        $c->where('city_id', $filter['city']);
                    });
                }

                $users = $users->where('registered_as', 'advertiser')->with($options['with'])->where('first_name','!=','temp')->orderby('id','desc')->paginate(10);
            }
            return $users;
        }

        public function getAllInstituteData($options,$filter){
            
            if(empty($filter)){
                $users = $this->userModel->where('registered_as', 'institute')->with($options['with'])->where('first_name','!=','temp')->orderby('id','desc')->paginate(10);
            }
            else {
                $users = $this->userModel->where('registered_as', 'institute');

                if (isset($filter['institute_name']) && !empty($filter['institute_name'])){
                    $users = $users->whereHas('institute_registration_detail', function ($c) use ($filter) {
                            $c->where('institute_name', 'like', '%'.$filter['institute_name'].'%');
                    });
                }

                if (isset($filter['state']) && !empty($filter['state'])){
                    $users = $users->whereHas('institute_locations', function ($c) use ($filter) {
                        $c->where('state_id', $filter['state']);
                    });
                }

                if (isset($filter['city']) && !empty($filter['city'])){
                    $users = $users->whereHas('company_locations', function ($c) use ($filter) {
                        $c->where('city_id', $filter['city']);
                    });
                }

                $users = $users->with($options['with'])->where('first_name','!=','temp')->orderby('id','desc')->paginate(10);
            }
            return $users;
        }

        public function getAllAdvertiserDataWithoutFilter($options){
            
            $users = $this->userModel->where('registered_as', 'advertiser')->with($options['with'])->where('first_name','!=','temp')->orderby('id','desc')->get();

            return $users;
        }

        public function changeStatus($status, $id){
            $data['status'] = 0;
            if($status == 'active'){
                $data['status'] = 1;
            }
            return $this->userModel->where('id', $id)->update($data);
        }

        public function getAllCompanyDataByFilters($filter,$options){
            if(empty($filter)){
                $users = $this->userModel->where('registered_as', 'company')->with($options['with'])->orderBy('id','desc')->paginate('10');
            }else {
                $users = $this->userModel->where('registered_as', 'company');

                if (isset($filter['company_name']) && !empty($filter['company_name'])){

                    $users = $users->whereHas('company_registration_detail', function ($c) use ($filter) {
                        if (isset($filter['company_name']) && !empty($filter['company_name']))
                            $c->where('company_name', 'like', '%'.$filter['company_name'].'%');
                    });
                }

                if(isset($filter['company_type']) && !empty($filter['company_type'])) {
                    $users = $users->whereHas('company_registration_detail.company_detail', function ($c) use ($filter) {
                        if (isset($filter['company_type']) && !empty($filter['company_type']))
                            $c->where('company_type', $filter['company_type']);
                    });
                }

                if(isset($filter['state']) && !empty($filter['state'])) {
                    $users = $users->whereHas('company_registration_detail.company_locations', function ($c) use ($filter) {
                            $c->where('state_id', $filter['state']);
                    });
                }

                if(isset($filter['city']) && !empty($filter['city'])) {
                    $users = $users->whereHas('company_registration_detail.company_locations', function ($c) use ($filter) {
                            $c->where('city_id', $filter['city']);
                    });
                }

            // dd($users->get()->toArray());
                $users=  $users->where('first_name','!=','temp')->with($options['with'])->orderBy('id','desc')->paginate('10');//dd($users->toArray());
                //$users = $this->userModel->where('registered_as', NULL)->paginate($paginate);
            }

            return $users;
        }

        public function getAllCompanyDataByFiltersWithoutPagination($filter,$options){
            if(empty($filter)){
                $users = $this->userModel->where('registered_as', 'company')->with($options['with'])->orderBy('id','desc')->whereHas('company_registration_detail')->get();
            }else {
                $users = $this->userModel->where('registered_as', 'company')->whereHas('company_registration_detail');

                if (isset($filter['company_name']) && !empty($filter['company_name'])){

                    $users = $users->whereHas('company_registration_detail', function ($c) use ($filter) {
                        if (isset($filter['company_name']) && !empty($filter['company_name']))
                            $c->where('company_name', 'like', '%'.$filter['company_name'].'%');
                    });
                }

                if(isset($filter['company_type']) && !empty($filter['company_type'])) {
                    $users = $users->whereHas('company_detail', function ($c) use ($filter) {
                        if (isset($filter['company_type']) && !empty($filter['company_type']))
                            $c->where('company_type', $filter['company_type']);
                    });
                }

                if(isset($filter['state']) && !empty($filter['state'])) {
                    $users = $users->whereHas('company_locations', function ($c) use ($filter) {
                            $c->where('state_id', $filter['state']);
                    });
                }

                if(isset($filter['city']) && !empty($filter['city'])) {
                    $users = $users->whereHas('company_locations', function ($c) use ($filter) {
                            $c->where('city_id', $filter['city']);
                    });
                }

            // dd($users->get()->toArray());
                $users=  $users->where('first_name','!=','temp')->with($options['with'])->orderBy('id','desc')->get();//dd($users->toArray());
                //$users = $this->userModel->where('registered_as', NULL)->paginate($paginate);
            }

            return $users;
        }

        public function getAllCompanySubscriptionsDetails($options,$filter){
            if(empty($filter)){
                $users = $this->userModel->where('registered_as', 'company')->with($options['with'])->orderBy('id','desc')->paginate('10');
            }else{
                $users = $this->userModel->where('registered_as', 'company');

                if(isset($filter['subscription_id']) && !empty($filter['subscription_id'])) {
                    $users = $users->whereHas('company_subscriptions', function ($c) use ($filter) {
                        $c->where('subscription_id', $filter['subscription_id']);
                    });
                }

                if (isset($filter['company_name']) && !empty($filter['company_name'])){
                    $users = $users->whereHas('company_registration_detail', function ($c) use ($filter) {
                        if (isset($filter['company_name']) && !empty($filter['company_name']))
                            $c->where('company_name', 'like', '%'.$filter['company_name'].'%');
                    });
                }

                if (isset($filter['valid_from']) && !empty($filter['valid_from'])){
                    $users = $users->whereHas('company_subscriptions', function ($c) use ($filter) {

                        $c->where('valid_from', '>', $filter['valid_from']);
                    });
                }

                if (isset($filter['valid_to']) && !empty($filter['valid_to'])){
                    $users = $users->whereHas('company_subscriptions', function ($c) use ($filter) {
                        $c->where('valid_to', '<', $filter['valid_to']);
                    });
                }

                $users=  $users->with($options['with'])->orderBy('id','desc')->paginate('10');
            }

            return $users;
        }

        public function storeUpdatedOnForUsers($user_id){
            $data['updated_on'] = date("Y-m-d h:i:s");
            return $this->userModel->where('id', $user_id)->update($data);
        }

        public function storeLastLoggedOndate($user_id){
            $data['last_logged_on'] = date("Y-m-d h:i:s");
            return $this->userModel->where('id', $user_id)->update($data);
        }

        public function getUserByCities($cities_array){
            return $this->userModel->whereHas('personal_detail', function ($c) use ($cities_array) {
                            $c->whereIn('city_id', $cities_array);
                    })->get();
        }

        public function getUserByCountryId($country_id){
            return $this->userModel->whereHas('personal_detail', function ($c) use ($country_id) {
                            $c->where('country', $country_id);
                    })->get();
        }

        public function getSeafarerByRankAndAvailability($rank,$date_of_joining){

            $d = date('Y-m-d',strtotime($date_of_joining));
            $now = date('Y-m-d');
            
            $options['with'] = ['professional_detail','personal_detail','personal_detail.state','personal_detail.city','personal_detail.pincode.pincodes_states.state','personal_detail.pincode.pincodes_cities.city'];
            
            $data = $this->userModel->whereHas('professional_detail', function ($c) use ($rank,$date_of_joining,$options,$d,$now) {
                        $c->where('applied_rank', $rank)
                        ->whereBetween('availability', [$now,$d])->orderby('id','desc')
                        ;
                })->with($options['with'])->get();

            return $data;
        }

        public function getAllCourses(){
            return $this->courseModel->get();
        }

        public function getUserWithGivenAvilability(){
            return $this->userModel->where(['registered_as' => 'seafarer'])->with('professional_detail')->get();
        }

        public function checkDetailsExist($redemption,$email,$mobile_number,$affiliate_user_id){

           if($affiliate_user_id > 0 && $affiliate_user_id != ""){ 
                if($mobile_number != ""){  
                    return \DB::select('Select * from users where (email = "'.$email.'" OR mobile = "'.$mobile_number.'" OR user_affiliate_code = "'.$redemption.'") AND id !='.$affiliate_user_id);
                }
                return \DB::select('Select * from users where (email = "'.$email.'"  OR user_affiliate_code = "'.$redemption.'") AND id !='.$affiliate_user_id);
          
           }else{           
                if($mobile_number != ""){                
                   return $this->userModel->where('email', $email)->orWhere('user_affiliate_code', $redemption)->orWhere('mobile',$mobile_number)->first();
                }
               
             return  $this->userModel->where('email' , $email)->orWhere('user_affiliate_code' , $redemption)->first();
           }
          
        }

        public function addAffiliateData($data){
            $affiliate_user =  $this->userModel->create($data);
            return $affiliate_user;
        }
        public function updateAffiliateData($data,$id){
            $affiliate_user =  $this->userModel->where('id',$id)->update($data);
            return $affiliate_user;
        }

        public function getRecordByEmail($email,$id){ 
            if($id != "" && $id > 0){
                return $this->userModel->where('email',$email)->where('id','!=',$id)->first();
            }
            return $this->userModel->where('email',$email)->first();   
        }

        public function getReferalCode($code){ 
            return $this->userModel->where('user_affiliate_code',$code)->first();              
        }  

        public function addRewardsData($data){ 
            return $this->rewardsModel->create($data);              
        }    


        public function getReferralUsersRewardsData($userid){ 
           return $this->rewardsModel->where('user_id',$userid)->get();              
        }

    }

?>
