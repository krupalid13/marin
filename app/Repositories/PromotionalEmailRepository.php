<?php

/**
 * Created by PhpStorm.
 * User: admin
 * Date: 5/23/2017
 * Time: 6:33 PM
 */

namespace App\Repositories;

use App\PromotionalEmail;
use Carbon\Carbon;
use DB;

class PromotionalEmailRepository {

    private $promotionalEmailModel;

    function __construct() {
        $this->promotionalEmailModel = new PromotionalEmail();
    }

    public function store($data) {
        
        return $this->promotionalEmailModel->insertGetId($data);
    }

}
