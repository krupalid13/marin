<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 4/29/2017
 * Time: 12:15 PM
 */

namespace App\Repositories;
use App\SubscriptionFeature;

class SubscriptionFeatureRepository
{
    private $subscriptionFeatureModel;

    function __construct()
    {
        $this->subscriptionFeatureModel = new SubscriptionFeature();
    }

    public function getResumeFeatureDetailsBySubscriptionId($subscription_id)
    {
        return $this->subscriptionFeatureModel->where('subscription_id', $subscription_id)->where('title', 'LIKE', '%Free% %Resume%')->first();

    }


    
}