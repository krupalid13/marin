<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 5/25/2017
 * Time: 4:27 PM
 */

namespace App\Repositories;
use App\User;
use DB;
use Auth;
use phpDocumentor\Reflection\Types\Null_;

class CandidateRepository
{
    private $userModel;
    function __construct()
    {
        $this->userModel = new User();
    }

    public function getAllCandidateDataByFilters($filter,$paginate){
        $users = $this->userModel;
        $now = date('Y-m-d H:m:s');
        $prev_date = date('Y-m-d', strtotime('-2 months', strtotime($now))); 
        
        $role = Auth::User()->registered_as;
        
        if(isset($role) && $role != 'admin'){
            $users = $users->select('users.id','users.profile_pic','users.first_name','users.last_name','users.status','user_professional_details.availability')
                        ->leftJoin('user_professional_details', 'users.id', '=', 'user_professional_details.user_id')
                        ->where('user_professional_details.availability', '>', $prev_date)
                        ->orderBy('user_professional_details.availability','asc');
        }else{
             $users = $users->orderBy('id','desc');
        }
        
        if(empty($filter)){

            $users = $users->where('registered_as', 'seafarer')->where('first_name','!=','temp')->with(['professional_detail','coc_detail','personal_detail','passport_detail','seaman_book_detail','gmdss_detail','wkfr_detail'])->paginate($paginate);

        }else {
            $users = $users->where('registered_as', 'seafarer');

            if (isset($filter['first_name']) && !empty($filter['first_name'])){
                $users = $users->where('first_name', 'like', '%'.$filter['first_name'].'%');
            }

            if (isset($filter['user_verified']) && !empty($filter['user_verified'])){

                if($filter['user_verified'] == 'mob_v'){
                    $users = $users->where('is_mob_verified', '1');
                }

                if($filter['user_verified'] == 'email_v'){
                    $users = $users->where('is_email_verified', '1');
                }

                if($filter['user_verified'] == 'both'){
                    $users = $users->where('is_email_verified', '1')->where('is_mob_verified', '1');
                }

                if($filter['user_verified'] == 'no_v'){
                    $users = $users->where('is_email_verified', '0')->where('is_mob_verified', '0');
                }

            }

            if (isset($filter['last_logged_from']) && !empty($filter['last_logged_from'])){
                $users = $users->where('last_logged_on','>=',date('Y-m-d',strtotime($filter['last_logged_from'])));
            }

            if (isset($filter['last_logged_to']) && !empty($filter['last_logged_to'])){
                $users = $users->where('last_logged_on','<',date('Y-m-d',strtotime($filter['last_logged_to'])));
            }

            if((isset($filter['rank']) && !empty($filter['rank'])) || (isset($filter['rank_exp_from']) && !empty($filter['rank_exp_from'])) || (isset($filter['rank_exp_to']) && !empty($filter['rank_exp_to'])) ) {

                $users = $users->whereHas('professional_detail', function ($q) use ($filter) {
                    
                    if(isset($filter['rank_exp_from'])){
                        $rank_exp = $filter['rank_exp_from'];
                        unset($filter['rank_exp_from']);
                        $range_exp = explode('-',$rank_exp);
                    }

                    if (isset($filter['rank']) && !empty($filter['rank'])) {
                        $q->where('applied_rank', $filter['rank']);
                    }
                    if (isset($range_exp[0]) && !empty($range_exp[0])) {
                        $q->where('current_rank_exp', '>' , $range_exp[0]);
                    }
                    if (isset($range_exp[1]) && !empty($range_exp[1])) {
                        $q->where('current_rank_exp', '<=', $range_exp[1]);
                    }
                });
            }


            if (isset($filter['passport']) && !empty($filter['passport']) && $filter['passport'] == 'on'){
                $users = $users->whereHas('passport_detail', function ($p) {
                });
            }

            if(isset($filter['coc_status']) && !empty($filter['coc_status'])) {

                $users = $users->whereHas('coc_detail', function ($c) use ($filter) {
                    if ($filter['coc_status'] == 'atleast_coc_v'){
                        $c->where('status', '1');
                    }
                    if ($filter['coc_status'] == 'atleast_coc_uv'){
                        $c->where('status', '0');
                    }
                });
                
                if($filter['coc_status'] == 'coc_v' || $filter['coc_status'] == 'coc_uv'){
                    $users = $users->whereDoesntHave('coc_detail', function ($c) use ($filter) {
                        if ($filter['coc_status'] == 'coc_v'){
                            $c->where('status', '0');
                        }
                        if ($filter['coc_status'] == 'coc_uv'){
                            $c->where('status', '1');
                        }
                    })->whereHas('coc_detail', function ($c) use ($filter) {
                        if ($filter['coc_status'] == 'coc_v'){
                            $c->where('status', '1');
                        }
                        if ($filter['coc_status'] == 'coc_uv'){
                            $c->where('status', '0');
                        }
                    });
                }
            }

            if(isset($filter['cdc_status']) && !empty($filter['cdc_status'])) {

                $users = $users->whereHas('seaman_book_detail', function ($c) use ($filter) {
                    if ($filter['cdc_status'] == 'atleast_cdc_v'){
                        $c->where('status', '1');
                    }
                    if ($filter['cdc_status'] == 'atleast_cdc_uv'){
                        $c->where('status', '0');
                    }
                });

                if($filter['cdc_status'] == 'cdc_v' || $filter['cdc_status'] == 'cdc_uv'){
                    $users = $users->whereDoesntHave('seaman_book_detail', function ($c) use ($filter) {
                        if ($filter['cdc_status'] == 'cdc_v'){
                            $c->where('status', '0');
                        }
                        if ($filter['cdc_status'] == 'cdc_uv'){
                            $c->where('status', '1');
                        }
                    })->whereHas('seaman_book_detail', function ($c) use ($filter) {
                        if ($filter['cdc_status'] == 'cdc_v'){
                            $c->where('status', '1');
                        }
                        if ($filter['cdc_status'] == 'cdc_uv'){
                            $c->where('status', '0');
                        }
                    });
                }
            }

            if(isset($filter['nationality']) && !empty($filter['nationality'])) {
                $users = $users->whereHas('personal_detail', function ($c) use ($filter) {
                    if (isset($filter['nationality']) && !empty($filter['nationality']))
                        $c->where('nationality', $filter['nationality']);
                });
            }

            if(isset($filter['availability_from']) && !empty($filter['availability_from'])) {
                $users = $users->whereHas('professional_detail', function ($c) use ($filter) {
                        $c->where('availability', '>', date('Y-m-d',strtotime($filter['availability_from'])));
                });
            }

            if(isset($filter['availability_to']) && !empty($filter['availability_to'])) {
                $users = $users->whereHas('professional_detail', function ($c) use ($filter) {
                        $c->where('availability', '<', date('Y-m-d',strtotime($filter['availability_to'])));
                });
            }

            if(isset($filter['cdc']) && !empty($filter['cdc'])) {
                $users = $users->whereHas('seaman_book_detail', function ($c) use ($filter) {
                    if (isset($filter['cdc']) && !empty($filter['cdc']))
                        $c->where('cdc', $filter['cdc']);
                });
            }

            if(isset($filter['watch_keeping']) && !empty($filter['watch_keeping'])) {
                $users = $users->whereHas('wkfr_detail', function ($cq) use ($filter) {
                    $cq->where('watch_keeping', $filter['watch_keeping']);
                });
            }

            if(isset($filter['ship_type']) && !empty($filter['ship_type'])) {
                $users = $users->whereHas('sea_service_detail', function ($c) use ($filter) {
                    if (isset($filter['ship_type']) && !empty($filter['ship_type']))
                        $c->where('ship_type', $filter['ship_type']);
                });
            }

            $users=  $users->where('first_name','!=','temp')->with(['professional_detail','coc_detail','personal_detail','passport_detail','seaman_book_detail','gmdss_detail','wkfr_detail','sea_service_detail'])->paginate($paginate);
        }
        //dd($users->toArray());
        return $users;
    }

    public function getAllCandidateDataByFiltersWithoutPagination($filter){
        $users = $this->userModel->orderBy('id','desc');
        $now = date('Y-m-d H:m:s');

        $role = Auth::User()->registered_as;
        
        if(isset($role) && $role != 'admin'){
            $users = $users->select('users.id','users.profile_pic','users.first_name','users.last_name','users.status')        ->leftJoin('user_professional_details', 'users.id', '=', 'user_professional_details.user_id')
                        ->where('user_professional_details.availability', '>', $now)
                        ->orderBy('user_professional_details.availability','asc');
        }
        
        if(empty($filter)){
            
            $users=  $users->orderBy('id','desc');
            
            $users = $users->where('registered_as', 'seafarer')->where('first_name','!=','temp')->with(['professional_detail','coc_detail','personal_detail','passport_detail','seaman_book_detail','gmdss_detail','wkfr_detail','sea_service_detail','personal_detail.state','personal_detail.city','personal_detail.pincode.pincodes_states.state','personal_detail.pincode.pincodes_cities.city'])->get();

        }else {
            $users = $users->where('registered_as', 'seafarer');

            if (isset($filter['first_name']) && !empty($filter['first_name'])){
                $users = $users->where('first_name', 'like', '%'.$filter['first_name'].'%');
            }

            if (isset($filter['last_logged']) && !empty($filter['last_logged'])){
                $users = $users->where('last_logged_on','>=',date('Y-m-d h:i:s',strtotime($filter['last_logged'])));
            }

            if((isset($filter['rank']) && !empty($filter['rank'])) || (isset($filter['rank_exp_from']) && !empty($filter['rank_exp_from'])) || (isset($filter['rank_exp_to']) && !empty($filter['rank_exp_to'])) ) {

                $users = $users->whereHas('professional_detail', function ($q) use ($filter) {
                    
                    if(isset($filter['rank_exp_from'])){
                        $rank_exp = $filter['rank_exp_from'];
                        unset($filter['rank_exp_from']);
                        $range_exp = explode('-',$rank_exp);
                    }

                    if (isset($filter['rank']) && !empty($filter['rank'])) {
                        $q->where('applied_rank', $filter['rank']);
                    }
                    if (isset($range_exp[0]) && !empty($range_exp[0])) {
                        $q->where('current_rank_exp', '>' , $range_exp[0]);
                    }
                    if (isset($range_exp[1]) && !empty($range_exp[1])) {
                        $q->where('current_rank_exp', '<=', $range_exp[1]);
                    }
                });
            }


            if (isset($filter['passport']) && !empty($filter['passport']) && $filter['passport'] == 'on'){
                $users = $users->whereHas('passport_detail', function ($p) {
                });
            }

            if(isset($filter['coc']) && !empty($filter['coc'])) {
                $users = $users->whereHas('coc_detail', function ($c) use ($filter) {
                    if (isset($filter['coc']) && !empty($filter['coc']))
                        $c->where('coc', $filter['coc']);
                });
            }

            if(isset($filter['nationality']) && !empty($filter['nationality'])) {
                $users = $users->whereHas('personal_detail', function ($c) use ($filter) {
                    if (isset($filter['nationality']) && !empty($filter['nationality']))
                        $c->where('nationality', $filter['nationality']);
                });
            }

            if(isset($filter['availability_from']) && !empty($filter['availability_from'])) {
                $users = $users->whereHas('professional_detail', function ($c) use ($filter) {
                        $c->where('availability', '>', date('Y-m-d',strtotime($filter['availability_from'])));
                });
            }

            if(isset($filter['availability_to']) && !empty($filter['availability_to'])) {
                $users = $users->whereHas('professional_detail', function ($c) use ($filter) {
                        $c->where('availability', '<', date('Y-m-d',strtotime($filter['availability_to'])));
                });
            }

            if(isset($filter['cdc']) && !empty($filter['cdc'])) {
                $users = $users->whereHas('seaman_book_detail', function ($c) use ($filter) {
                    if (isset($filter['cdc']) && !empty($filter['cdc']))
                        $c->where('cdc', $filter['cdc']);
                });
            }

            if(isset($filter['watch_keeping']) && !empty($filter['watch_keeping'])) {
                $users = $users->whereHas('wkfr_detail', function ($cq) use ($filter) {
                    $cq->where('watch_keeping', $filter['watch_keeping']);
                });
            }

            if(isset($filter['ship_type']) && !empty($filter['ship_type'])) {
                $users = $users->whereHas('sea_service_detail', function ($c) use ($filter) {
                    if (isset($filter['ship_type']) && !empty($filter['ship_type']))
                        $c->where('ship_type', $filter['ship_type']);
                });
            }

        // dd($users->get()->toArray());
            $users=  $users->orderBy('id','desc');

            $users=  $users->where('first_name','!=','temp')->with(['professional_detail','coc_detail','personal_detail','passport_detail','seaman_book_detail','gmdss_detail','wkfr_detail','sea_service_detail','personal_detail.state','personal_detail.city','personal_detail.pincode.pincodes_states.state','personal_detail.pincode.pincodes_cities.city'])->get();
        }
        //dd($users->toArray());
        return $users;
    }
}