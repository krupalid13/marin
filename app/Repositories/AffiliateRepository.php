<?php

namespace App\Repositories;
use App\Affiliate;

class AffiliateRepository
{
    private $affiliateModel;

    function __construct() {
        $this->affiliateModel = new Affiliate();
    }

    public function getRecordById($id){
    	return $this->affiliateModel->find($id);
    }

    public function getRecordByEmail($email,$id){ 
        if($id != "" && $id > 0){
            return $this->affiliateModel->where('email',$email)->where('id','!=',$id)->first();
        }
        return $this->affiliateModel->where('email',$email)->first();    
                
    }

    public function getRecordByCode($code,$id){ 
       // \DB::enableQueryLog();
        if($id > 0){
              return $this->affiliateModel->where('redemption',$code)->where('id','!=',$id)->orderBy('id','DESC')->first();       
        }
         return $this->affiliateModel->where('redemption',$code)->orderBy('id','DESC')->first();       
     
      // dd(\DB::getQueryLog());
    }

    public function getReferalCode($code){ 
       return $this->affiliateModel->where('redemption',$code)->first();              
    }    

    public function getAllRecords(){
    	return $this->affiliateModel->get();
    }

    public function store($data)
    {
        $data = $this->affiliateModel->create($data);
        return $data;
    }

    public function update($data,$id)
    {   
        $data = $this->affiliateModel->where('id',$id)->update($data);
        return $data;
    }

    public function deleteById($id){
        return $this->affiliateModel->where('id',$id)->delete();
    }
}