<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 5/12/2017
 * Time: 5:38 PM
 */

namespace App\Repositories;
use App\CompanyDetail;

class CompanyDetailsRepository
{
    private $companyDetailsModel;

    function __construct() {
        $this->companyDetailsModel = new CompanyDetail();
    }

    public function store($data){
        return $this->companyDetailsModel->create($data);
    }

    public function getDetailsByCompanyId($company_id){
        var_dump($company_id);
        return $this->companyDetailsModel->where('company_id', $company_id)->get();
    }

    public function update($data,$company_id){
        return $this->companyDetailsModel->where('company_id', $company_id)->update($data);
    }

    public function getCompanyShipDetailsById($company_id){
        return $this->companyDetailsModel->where('company_id', $company_id)->with('ship_type')->get();
    }

    public function storeAdvertise($data,$company_id){
        return $this->companyDetailsModel->where('company_id', $company_id)->update($data);
    }
}