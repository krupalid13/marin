<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 4/21/2017
 * Time: 4:47 PM
 */

namespace App\Repositories;
use App\UserSemanBookDetail;

class UserSeamanBookDetailsRepository
{
    Private $userSeamanBookDetailsModel;
    function __construct()
    {
        $this->userSeamanBookDetailsModel = new UserSemanBookDetail();
    }

    public function store($data)
    {
        $data = $this->userSeamanBookDetailsModel->insertGetId($data);
        return $data;
    }

    public function getDetailsByUserId($user_id){
        return $this->userSeamanBookDetailsModel->where('user_id', $user_id)->get();
    }

    public function updateByUserId($data, $user_id) {
        return $this->userSeamanBookDetailsModel->where('user_id', $user_id)->update($data);
    }

    public function deleteServiceById($user_id){
        $this->userSeamanBookDetailsModel->where('user_id', $user_id)->delete();
    }
    
    public function updateBySeamanId($data, $semanId) {
        return $this->userSeamanBookDetailsModel->where('id', $semanId)->update($data);
    }
    
    public function deleteServiceBySeamanIds($semanIds){
        $this->userSeamanBookDetailsModel->whereIn('id', $semanIds)->delete();
    }
}