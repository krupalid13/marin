<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 5/23/2017
 * Time: 6:33 PM
 */

namespace App\Repositories;
use App\CompanyJob;
use App\JobApplication;
use App\CompanyJobNationality;
use Carbon\Carbon;
use DB;

class JobRepository
{
    private $companyJobModel;

    function __construct() {
        $this->companyJobModel = new CompanyJob();
        $this->companyJobNationalityModel = new CompanyJobNationality();
        $this->jobApplicationModel = new JobApplication();
    }

    public function store($data){
        return $this->companyJobModel->create($data);
    }

    public function getJobDetailsByJobId($job_id)
    {
        return $this->companyJobModel->with('job_nationality')->where('id', $job_id)->get();
    }

    public function update($data)
    {
        $job_id = $data['job_id'];
        unset($data['job_id']);
        return $this->companyJobModel->where('id', $job_id)->update($data);
    }

    public function getJobDetailsByCompanyId($company_id){
        return $this->companyJobModel->where('company_id', $company_id)->with('company_details','company_registration_details','user_details')->get();
    }

    public function getJobDetailsByCompanyIdByPaginate($company_id){
        return $this->companyJobModel->where('company_id', $company_id)->with('ship_type_id','company_details','company_registration_details','company_registration_details.user_details','job_nationality')->orderBy('id','desc')->paginate(10);
    }

    public function getJobDetailsByPaginate($paginate,$filter){


        if(isset($filter) && !empty($filter)){
            $companyJobModel = $this->companyJobModel;

            if(isset($filter['job_id']) && !empty($filter['job_id'])){
                $companyJobModel = $companyJobModel->where('id',$filter['job_id']);
            }
            
            if(isset($filter['company_id']) && !empty($filter['company_id'])){
                $companyJobModel = $companyJobModel->where('company_id',$filter['company_id']);
            }

            if(isset($filter['rank']) && !empty($filter['rank'])){
                $companyJobModel = $companyJobModel->where('rank',$filter['rank']);
            }

            if(isset($filter['ship_type']) && !empty($filter['ship_type'])){
                $companyJobModel = $companyJobModel->where('ship_type',$filter['ship_type']);
            }
            
            return $companyJobModel->with('company_registration_details.user_details','company_registration_details.company_detail','company_registration_details')->orderBy('id','desc')->paginate($paginate);
        }else{
            return $this->companyJobModel->with('company_registration_details.company_detail','company_registration_details','company_registration_details.user_details')->orderBy('id','desc')->paginate($paginate);
        }
    }

    public function disableJobByJobId($job_id){
        return $this->companyJobModel->where('id', $job_id)->update(['status' => 0]);
    }

    public function enableJobByJobId($job_id){
        return $this->companyJobModel->where('id', $job_id)->update(['status' => 1]);
    }

    public function getAllJobDetails($search_data,$paginate = NULL,$options = NULL,$user_id = NULL){

        $company_jobs = $this->companyJobModel;
        $date_now = date("Y-m-d H:i:s");

        
        if(isset($search_data['_token'])){
            unset($search_data['_token']);
        }

        // if(isset($search_data['auto_apply'])){
        //     unset($search_data['auto_apply']);
        // }

        if(isset($search_data['company_id'])){
            unset($search_data['company_id']);
        }

        if(isset($search_data['min_rank_exp'])){
            $rank_exp = $search_data['min_rank_exp'];
            unset($search_data['min_rank_exp']);
            $range_exp = explode('-',$rank_exp);

            if(isset($rank_exp[0]) AND !empty($rank_exp[0]))
                $company_jobs = $company_jobs->where('min_rank_exp','>=',$range_exp[0]);


            if(isset($rank_exp[1]) And $range_exp[1] != ''){
                $company_jobs = $company_jobs->where('min_rank_exp','<',$range_exp[1]);
            }
        }

        if(isset($search_data['rank'])){
            if(isset($search_data['rank']) AND !empty($search_data['rank']))
                $company_jobs = $company_jobs->where('rank',$search_data['rank']);
        }

        if(isset($search_data['user_id']) && !empty($search_data['user_id'])){
            $user_id = $search_data['user_id'];
            unset($search_data['user_id']);
        }

        if(isset($search_data['user_id'])){
            unset($search_data['user_id']);
        }

        if(isset($search_data['job_id'])){
            $job_id = $search_data['job_id'];
            unset($search_data['job_id']);
        }

        if(isset($search_data['auto_apply'])){
            $auto_apply = '1';
            unset($search_data['auto_apply']);
        }

        if(isset($search_data['nationality']) AND !empty($search_data['nationality'])){
            $company_jobs = $company_jobs->whereHas('job_nationality', function ($c) use ($search_data) {
                $c->where('nationality', $search_data['nationality']);
            });
            unset($search_data['nationality']);
        }

        if(isset($search_data['company_name']) AND !empty($search_data['company_name'])){
            $company_jobs = $company_jobs->whereHas('company_registration_details', function ($c) use ($search_data) {
                $c->where('company_name', 'like' , '%'.$search_data['company_name'].'%');
            });
            unset($search_data['company_name']);
        }
        
        if(isset($search_data) && !empty($search_data))
            $company_jobs = $company_jobs->where('valid_to','>=',$date_now)->where($search_data)->where('status','1');
        else
            $company_jobs = $company_jobs->where('valid_to','>=',$date_now)->where('status','1');

        if(!empty($options)){

            if(isset($options['with_condition']) && !empty($user_id)){
                $company_jobs = $company_jobs->with([$options['with_condition'][0] => function($q) use ($user_id){
                    $q->where('user_id',$user_id)->orderBy('id','desc')->get();
                }]);
            }

            //dd($user_id,$options,$company_jobs->get()->toArray());

            if(isset($options['with']) && in_array("company_registration_details.active_subscription", $options['with'])){
                $company_jobs->with(['company_registration_details'=> function($query) use ($date_now) {
                        $query->with(['active_subscription' => function($qt) use ($date_now) {
                            $qt->where('status',1)->where('valid_from','<=',$date_now)->where('valid_to','>=',$date_now);
                        }]);
                 }]);
                
                $company_jobs->whereHas('company_registration_details', function($query) use ($date_now) {
                        $query->whereHas('active_subscription', function($qt) use ($date_now) {
                            $qt->where('status',1)->where('valid_from','<=',$date_now)->where('valid_to','>=',$date_now);
                        });
                 });

            } else {
                if(isset($options['with'])){
                    
                    $company_jobs = $company_jobs->with($options['with']);
                }
            }
        }

        if(!empty($paginate)){
            $company_jobs = $company_jobs->orderBy('valid_to','asc')->paginate($paginate);
        }else{
            $company_jobs = $company_jobs->orderBy('valid_to','asc')->get();
        }
        
        return $company_jobs;
    }

    public function getAllUserAppliedJobs($company_id, $options, $paginate, $filter){

        $company_jobs = $this->jobApplicationModel;
       

        if(isset($options['with'])){

            if(isset($company_id) && !empty($company_id)){
                $company_jobs = $company_jobs->with($options['with'])->where('company_id',$company_id)->orderBy('id','desc');
            }else{
                $company_jobs = $company_jobs->with($options['with'])->orderBy('id','desc');
            }
        }

        if(isset($filter) && !empty($filter)){
            if(isset($filter['job_id']) && !empty($filter['job_id'])){
                $company_jobs = $company_jobs->where('job_id',$filter['job_id']);
            }

            if(isset($filter['seafarer_name']) && !empty($filter['seafarer_name'])){
                $company_jobs = $company_jobs->whereHas('user', function ($c) use ($filter) {
                    $c->where('first_name', 'like', '%'.$filter['seafarer_name'].'%');
                });
            }

            if(isset($filter['email']) && !empty($filter['email'])){
                $company_jobs = $company_jobs->whereHas('user', function ($c) use ($filter) {
                    $c->where('email', $filter['email']);
                });
            }

            if(isset($filter['mobile']) && !empty($filter['mobile'])){
                $company_jobs = $company_jobs->whereHas('user', function ($c) use ($filter) {
                    $c->where('mobile', $filter['mobile']);
                });
            }
        }

        if(!empty($paginate)){
            $company_jobs = $company_jobs->paginate($paginate);
        }
        return $company_jobs;
    }

    public function getJobDetailsByRankAndLocation($rank_id,$user_id){
        // $user_id = 9;
        // $rank_id = 2;
        $date = strtotime(date('Y-m-d'));

        $newDate = date('Y-m-d',strtotime('-15 days',$date));

        //print_r($rank_id,$user_id);
        $JobModel = $this->companyJobModel;
        $JobModel =  $JobModel->where('rank', $rank_id)->where('status','1');
        /*if(isset($user_id) && !empty($user_id)){
            $JobModel = $JobModel->whereHas('job_application', function ($c) use ($newDate,$user_id) {
                $c->where('created_at', '<=', $newDate)->where('user_id','<>',$user_id);
            });
        }*/

        // dd($JobModel->get()->toArray());
        //echo $location['country']; echo "<br>";
        /*$JobModel =  $JobModel->whereHas('company_location', function ($c) use ($location) {
                        $c->where('country', $location['country']);
                    });*/

        $JobModel = $JobModel->with('company_registration_details.user')->get();
        // print_r($JobModel->toArray());
        return $JobModel;
    }

    public function updateJobNationalities($nationalities, $job_id){
        $this->companyJobNationalityModel->where('job_id', $job_id)->delete();
        
        foreach ($nationalities as $key => $nationality) {
            $this->companyJobNationalityModel->create(['nationality' => $nationality, 'job_id' => $job_id]);
        }

        return true;
    }

    public function storeJobNationalities($nationalities, $job_id){
        
        if(isset($nationalities) && !empty($nationalities)){
            foreach ($nationalities as $key => $nationality) {
                $this->companyJobNationalityModel->create(['nationality' => $nationality, 'job_id' => $job_id]);
            }
        }

        return true;
    }
}