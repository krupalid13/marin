<?php

namespace App\Repositories;

use App\ShareHistory;
use App\EnginType;
use Auth;
use Datatables;

class ShareHistoryRepository {

    private $shareHistoryModel;

    function __construct() {
        $this->shareHistoryModel = new ShareHistory();
    }

    public function getShareHistory($user_id, $type) {
        $model = $this->shareHistoryModel
                ->where('user_id', $user_id)
                ->where('roll_type', $type);
        $model = $model->with(['share_contact', 'history_log', 'document']);
        return $model->get();
    }
    
}

?>