<?php

namespace App\Repositories;
use App\AdvertiseDetails;
use App\AdvertisementsEnquiries;

class AdvertiseRepository
{
	private $advertiseModel;
    function __construct()
    {
        $this->advertiseModel = new AdvertiseDetails(); 
        $this->advertisementEnquiryModel = new AdvertisementsEnquiries();
    }

    public function store($data){
        /*if(isset($data['company_id']) && !empty($data['company_id'])){
            $company = $this->advertiseModel->where('company_id',$data['company_id'])->get();
            if(count($company) > 0){
                return $this->advertiseModel->where('company_id',$data['company_id'])->update($data);
            }else{
                return $this->advertiseModel->create($data);
            }
        }*/
        return $this->advertiseModel->create($data);
    }

    public function storeCompanyAdvertise($data){
        if(isset($data['company_id']) && !empty($data['company_id'])){
            $company = $this->advertiseModel->where('company_id',$data['company_id'])->get();
            if(count($company) > 0){
                return $this->advertiseModel->where('company_id',$data['company_id'])->update($data);
            }else{
                return $this->advertiseModel->create($data);
            }
        }
        return $this->advertiseModel->create($data);
    }

    public function storeEnquiry($data){
        return $this->advertisementEnquiryModel->create($data);
    }

    public function getAdvertiseEnquiryByCompanyId($company_id){
        return $this->advertisementEnquiryModel->where('company_id', $company_id)->with('advertisement_details')->orderBy('id','desc')->paginate(10);
    }

    public function getAllAdvertiseEnquiries($paginate){
        return $this->advertisementEnquiryModel->with('advertisement_details')->orderBy('id','desc')->paginate($paginate);
    }

    public function getAdvertisementsById($id){
        return $this->advertiseModel->where('id',$id)->first();
    }

    public function getAllAdvertisementsDetails($paginate,$filter){
        
        if(empty($filter)){
            return $this->advertiseModel->with('company_registration_details.user_details','company_registration_details')->whereHas('company_registration_details.user_details',function ($query) {
                    $query->where('registered_as', 'advertiser');
            })->orderBy('id','desc')->paginate($paginate);
        }else{

            $advertiseModel = $this->advertiseModel->with('company_registration_details.user_details','company_registration_details')->whereHas('company_registration_details.user_details',function ($query) 
                {
                    $query->where('registered_as', 'advertiser');
                })->orderBy('id','desc');

            if (isset($filter['first_name']) && !empty($filter['first_name'])){
                $advertiseModel = $advertiseModel->whereHas('company_registration_details.user_details', function ($c) use ($filter) {
                        $c->where('first_name', 'like', '%'.$filter['first_name'].'%');
                });
            }

            if (isset($filter['company_name']) && !empty($filter['company_name'])){
                $advertiseModel = $advertiseModel->whereHas('company_registration_details', function ($c) use ($filter) {
                        $c->where('company_name', 'like', '%'.$filter['company_name'].'%');
                });
            }

            if (isset($filter['bussiness_category']) && !empty($filter['bussiness_category'])){
                $advertiseModel = $advertiseModel->whereHas('company_registration_details', function ($c) use ($filter) {
                    $c->where('bussiness_category', $filter['bussiness_category']);
                });
            }

            if (isset($filter['bi_member']) && !empty($filter['bi_member'])){
                $advertiseModel = $advertiseModel->whereHas('company_registration_details', function ($c) use ($filter) {
                    $c->where('b_i_member', $filter['bi_member']);
                });
            }
            
            return $advertiseModel = $advertiseModel->paginate($paginate);
        }
    }

    public function changeStatus($status, $id){
        $data['status'] = 0;
        if($status == 'active'){
            $data['status'] = 1;
        } elseif ($status == 'live') {
            $data['status'] = 2;
        }
        return $this->advertiseModel->where('id', $id)->update($data);
    }

    public function setAdvertiseStatusDeactivate($company_id){
        $this->advertiseModel->where('company_id',$company_id)->update(['status' => 0]);
    }

    public function getAllAdvertisementCompanyDetails(){
        return $this->advertiseModel->with('company_registration_details.user_details','company_registration_details')->where('advertisement_as','company')->get();
    }

    public function getCompanyAdvertises($filter){
        $advertiseModel = $this->advertiseModel;

        $advertiseModel = $advertiseModel->whereHas('company_registration_details.user_details', function ($c){
                    $c->where('registered_as', 'company');
                });

        if(isset($filter)){
            if(isset($filter['company_name']) && !empty($filter['company_name'])){
                $advertiseModel = $advertiseModel->where('company_id', $filter['company_name']);
            }
            if(isset($filter['status']) && is_numeric($filter['status'])){
                $advertiseModel = $advertiseModel->where('status', $filter['status']);
            }
        }

        $data = $advertiseModel->orderBy('company_id','desc')->where('advertisement_as','company')->with('company_registration_details')->paginate(10);

        return $data;
    }

    public function getAdvertisementCountByStatus($company_id,$status){
        return $this->advertiseModel->where(['company_id' => $company_id,'status' => $status])->get()->count(); 
    }

    public function getAllAdvertisementsWithActiveAdvertisements(){

        $advertiserModel = \App\User::where('registered_as', 'advertiser');

        $advertiserModel = $advertiserModel->with('company_registration_detail.user_details','company_registration_detail.advertisment_details')
            ->whereHas('company_registration_detail.user_details',function ($query) {
                $query->where('registered_as', 'advertiser');
            });

        $advertiserModel = $advertiserModel->whereHas('company_registration_detail.advertisment_details', function ($c) {
            $c->where('status', '2');
        });

        return $advertiserModel->orderBy('id','desc')->get();
    }
}

