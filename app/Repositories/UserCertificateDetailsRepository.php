<?php

    namespace App\Repositories;
    use App\UserCoursesCertificate;

    class UserCertificateDetailsRepository{
        private $userModel;
        function __construct()
        {
            $this->userModel = new UserCoursesCertificate();
        }

        public function store($data){
            if(isset($data['existing_course_id']) && !empty($data['existing_course_id'])){
                $id = $data['existing_course_id'];
                unset($data['existing_course_id']);
                return $this->userModel->where('id',$id)->update($data);
            }
            return $this->userModel->create($data);
        }

        public function deleteCoursesById($user_id){
            $user_courses = $this->userModel->where('user_id' , $user_id)->get();

            if(count($user_courses) > 0)
                $this->userModel->where('user_id' , $user_id)->delete();
        }

        public function getSeafarerWithCourseId($course_details,$city_ids,$options){

            $userModel = $this->userModel;
            $current_date = date('Y-m-d',strtotime('+60 days'));
            $prev_date = date('Y-m-d',strtotime('-30 days'));
            //dd($current_date,$prev_date);
            $userModel = $userModel->whereBetween('expiry_date',array($prev_date,$current_date));

            $userModel = $this->userModel->whereHas('user_personal_detail', function ($c) use ($city_ids) {
                $c->whereIn('city_id', $city_ids);
            })->with('user_personal_detail.state','user_personal_detail.city','user_personal_detail.pincode.pincodes_states.state','user_personal_detail.pincode.pincodes_cities.city');

            $userModel = $userModel->where('course_id',$course_details['course_id'])->with($options['with'])->get();
            
            return $userModel;
        }

        public function getCourseDetailsByUserId($user_id){
            return $this->userModel->where('user_id',$user_id)->orderBy('id','desc')->get();
        }

        public function deleteSeafarerCourseByCourseId($course_id,$user_id){
            return $this->userModel->where('user_id',$user_id)->where('id',$course_id)->delete();
        }

        public function getSeafarerCourseByCourseId($course_id,$user_id){
            return $this->userModel->where('user_id' , $user_id)->where('id',$course_id)->first();
        }

        public function getCourseIssueBy($user_id,$text){
            return $this->userModel->select('issue_by')->where('user_id' , $user_id)->where('issue_by', 'like', '%'.$text.'%')->groupBy('issue_by')->get();
        }
    }

?>