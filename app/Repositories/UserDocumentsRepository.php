<?php

namespace App\Repositories;
use App\UserDocuments;
use App\DocumentPermissions;
use App\DocumentPermissionRequests;

class UserDocumentsRepository
{
    private $userDocumentsModel;
    private $documentPermissionsModel;
    private $documentPermissionRequestsModel;

    function __construct() {
        $this->userDocumentsModel = new UserDocuments();
        $this->documentPermissionsModel = new DocumentPermissions();
        $this->documentPermissionRequestsModel = new DocumentPermissionRequests();
    }

    public function store($data){
        return $this->userDocumentsModel->create($data);
    }

    public function deleteDocument($img_id){
    	return $this->userDocumentsModel->where('id',$img_id)->delete();
    }

    public function getRequestedUserDocumentsList($owner_id,$requester_id,$user_document_id){
        return $this->documentPermissionsModel->where('owner_id',$owner_id)->where('requester_id',$requester_id)->where('user_document_id',$user_document_id)->get()->toArray();
    }

    public function storeRequestedUserDocumentsList($data){
        return $this->documentPermissionsModel->create($data);
    }

    public function updateRequestedDocumentStatus($status,$document_permission_id,$requester_id,$owner_id){
        return $this->documentPermissionsModel->where('id', $document_permission_id)->where('requester_id', $requester_id)->where('owner_id', $owner_id)->update($status);
    }

    public function storeDocumentPermissionReq($data){
        $already_exist = $this->documentPermissionRequestsModel->where('document_permission_id',$data['document_permission_id'])->where('user_document_id',$data['user_document_id'])->first();
        if(!$already_exist){
            return $this->documentPermissionRequestsModel->create($data);
        }

        return true;
    }

    public function getDocumentsPermissionAllList($owner_id,$requester_id){
        return $this->documentPermissionsModel->where('owner_id',$owner_id)->where('requester_id',$requester_id)->where('status','0')->get();
    }

    public function deleteDocumentsPermissionRequestList($permission_id,$user_document_id){
        return $this->documentPermissionRequestsModel->whereNotIn('user_document_id', $user_document_id)->where('document_permission_id',$permission_id)->delete();
    }

}