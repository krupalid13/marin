<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 4/21/2017
 * Time: 3:49 PM
 */

namespace App\Repositories;
use App\UserPassportDetail;

class UserPassportDetailsRepository
{
    Private $userPassportDetailsModel;
    function __construct()
    {
        $this->userPassportDetailsModel = new UserPassportDetail();
    }

    public function getUserPassportDetailsById($user_id){
        $user =  $this->userPassportDetailsModel->where('user_id', $user_id)->get();
        return $user;
    }

    public function store($data)
    {
        $data = $this->userPassportDetailsModel->create($data);
        return $data;
    }

    public function updateByUserId($data, $user_id) {
        return $this->userPassportDetailsModel->where('user_id', $user_id)->update($data);
    }
}