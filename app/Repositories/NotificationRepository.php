<?php

namespace App\Repositories;

use App\Notification;

class NotificationRepository
{
    protected $notificationModal;

    function __construct()
    {
        $this->notificationModal = new Notification();
    }

    public function createOrUpdate($data)
    {
        return $this->notificationModal->create($data);
    }

    public function getUserNotifications($user_id,$count)
    {
        $model = $this->notificationModal->where('to_user_id',$user_id)->orderBy('id','desc');

        if(!empty($count)){
            return $model->take($count)->get();
        }

        return $model->get();
    }  

    public function getUserNotificationByUserId($user_id,$paginate)
    {
        $model = $this->notificationModal->where('to_user_id',$user_id)->orderBy('id','desc');

        if(!empty($paginate)){
            return $model->paginate($paginate);
        }

        return $model->get();
    } 

    public function getUserAllNotifications()
    {
        $model = $this->notificationModal->orderBy('id','desc');
        return $model->get();
    }

    public function getUserNotificationById($notification_id)
    {
        return $this->notificationModal->where('id',$notification_id)->first();
    }

}