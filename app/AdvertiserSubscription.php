<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdvertiserSubscription extends Model
{
    protected $table = 'advertiser_subscriptions';

    protected $fillable =['company_reg_id','order_id','subscription_details','status','valid_from','valid_to','created_at','updated_at'];

    public function subscriptionFeatures()
    {
        return $this->hasMany('App\SubscriptionFeature');
    }

    public function order()
    {
        return $this->hasOne('App\Order','id','order_id');
    }

    public function company_registration()
    {
        return $this->hasOne('App\CompanyRegistration','id','company_reg_id');
    }
}