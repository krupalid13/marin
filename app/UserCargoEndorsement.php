<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserCargoEndorsement extends Model
{
    protected $table = 'user_cargo_endorsement';

    protected $fillable = ['user_id', 'endorsement_id', 'endorsement_number', 'endorsement_expiry_date'];
}
