<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserGmdssDetail extends Model
{
    protected $table = 'user_gmdss_details';

    protected $fillable = ['user_id', 'gmdss', 'gmdss_number', 'gmdss_expiry_date','gmdss_endorsement_number','gmdss_endorsement_expiry_date'];
}
