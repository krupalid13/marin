<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Yajra\Datatables\Datatables;
use Crypt;
use App\CourseJob;

class CompanyEmailAddress extends Model
{
    protected $table = 'company_email_address';

    public function deleteCompanyEmailAddress($id){
        	
        $checkUsed = CourseJob::where('company_email_address_id',Crypt::decrypt($id))->first();

        if ($checkUsed != null) {
        		
        	$msg = "Company email address already in used can not be deleted.";
        	$status = 2;
        }else{

	        $obj = $this->findOrFail(Crypt::decrypt($id));
	        $obj->delete();
	        $status = 1;
        	$msg = "Company emaiil address successfully deleted.";
        }


        return response()->json(['success'=> $status, 'msg' => $msg]);
    }
}
