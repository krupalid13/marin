<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Crypt;
use App\Courses;

class JobMultipleCourse extends Model
{
    protected $table = 'job_multiple_course';

    public function course(){

        return $this->belongsTo('App\Courses','course_id','id');
    }
}
