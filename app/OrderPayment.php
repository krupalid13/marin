<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderPayment extends Model
{
    protected $table = 'orders_payment';

    protected $fillable = ['user_id','order_id','txnid','transaction_id','bank_ref_num','mode','amount','message','status'];

    public function order_detail(){
        return $this->belongsTo('App\Order');
    }

     
}
