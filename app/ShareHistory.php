<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ShareHistory extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    //
    public function share_contact(){
        return $this->hasOne(ShareContacts::class, 'id', 'contact_id');
    }
    
    public function history_log(){
        return $this->hasMany(HistoryLog::class, 'share_history_id', 'id')->orderBy('id', 'DESC');
    }
    
    public function document(){
        return $this->hasOne(SharedDocsToOthers::class, 'share_history_id', 'id');
    }

    public function history_log_latest(){
        return $this->belongsTo(HistoryLog::class, 'share_history_id', 'id')->orderBy('id', 'DESC');
    }
    
    public function user(){
        return $this->hasOne(User::class, 'id', 'user_id');
    }
}
