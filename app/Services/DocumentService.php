<?php 
namespace App\Services;
use App\Repositories\UserDocumentsTypeRepository;
use App\Repositories\UserDocumentsRepository;
use App\User;
use Auth;
use Log;
use S3;

class DocumentService
{
    private $userDocumentsTypeRepository;
    private $userDocumentsRepository;

    function __construct()
    {
        $this->userDocumentsTypeRepository = New UserDocumentsTypeRepository();
        $this->userDocumentsRepository = New UserDocumentsRepository();
    }

    public function store($type, $type_id, $user_id){

        $data['user_id'] = $user_id;
        $data['type'] = $type;
        $data['type_id'] = $type_id;
        $data['order'] = 0;
        if ($data['type']=="pancard" || $data['type']=="aadhaar" || $data['type']=="passbook")
            $data['status']=0;
        else
            $data['status']=1;

        return $this->userDocumentsTypeRepository->store($data);
    }

    public function storeDocument($filename,$document_id){

        $data['document_path'] = $filename;
        $data['user_document_id'] = $document_id;
        return $this->userDocumentsRepository->store($data);
    }

    public function deleteDocument($img_id,$img_name,$user_id,$type,$type_id){
        if($type_id != '0'){
            $path = 'public/uploads/user_documents/'.$user_id."/".$type."/".$type_id."/".$img_name;
        }else{
            $path = 'public/uploads/user_documents/'.$user_id."/".$type."/".$img_name;
        }
        $headers = get_headers(env('AWS_URL').$path);
        if($headers[0] == 'HTTP/1.1 200 OK'){
        // if(file_exists(env('AWS_URL').$path)){
            $filesize = str_replace('Content-Length: ', '', $headers[9]) / 1024;
            $user = User::find(Auth::user()->id);
            $user->used_kb = abs($user->used_kb - $filesize);
            $user->save(); 
            $s3 = new S3(env('AWS_ACCESS_KEY_ID'), env('AWS_SECRET_ACCESS_KEY'), false, 's3.amazonaws.com',env('AWS_DEFAULT_REGION'));
            if ($s3->deleteObject(env('AWS_BUCKET'), $path)) {

            }
        }

        $document = $this->userDocumentsRepository->deleteDocument($img_id);
        return $document;
    }

    public function getUserDocuments($user_id){
        $data = $this->userDocumentsTypeRepository->getUserDocuments($user_id);
      
        $type = [];
        $document = [];
        if(!empty($data)){
            $all_documents = $data->toArray();
            foreach ($all_documents as $key => $value) {
				
                if(!in_array($value['type'],$type)){
                    $document[$value['type']][$value['type_id']] = $value;
                }
            }
        }
        return $document;
    }

    public function checkSeafarerTypeExist($type,$type_id,$user_id){
        return $this->userDocumentsTypeRepository->checkSeafarerTypeExist($type,$type_id,$user_id);
    }

    public function storeDocumentsPermissions($data,$user_id){

        $this->userDocumentsTypeRepository->deactivateAllDocumentsByUserId($user_id);
        if(isset($data['permissions']) && !empty($data['permissions'])){
            foreach ($data['permissions'] as $permission) {
                //dd($data);
                $this->userDocumentsTypeRepository->storeDocumentsPermissions($permission['type'],$permission['type_id'],$user_id);
            }
        }

        return true;
        
    }

    public function getUserDocumentsList($user_id,$type,$option){
        $allowed_documents = [];
        $get_user_documents_list = $this->userDocumentsTypeRepository->getUserDocumentsWithFilter($user_id,$type,$option)->toArray();

        $permission = true;
        if(isset($get_user_documents_list) && !empty($get_user_documents_list)){
            foreach($get_user_documents_list as $user_doc){
                if($user_doc['status'] != '0'){
                    $allowed_documents[] = $user_doc;
                }
            }
        }

        return $allowed_documents;
    }

    public function getAllSeafarerPermissionRequest($user_id,$filter){
        return $this->userDocumentsTypeRepository->getAllSeafarerPermissionRequest($user_id,$filter);
    }

    public function checkPermissionBelongsToUser($permission_id,$user_id){
        return $this->userDocumentsTypeRepository->checkPermissionBelongsToUser($permission_id,$user_id);
    }

    public function getPermissionDetailsByPermissionId($permission_id){
        return $this->userDocumentsTypeRepository->getPermissionDetailsByPermissionId($permission_id)->toArray();
    }

    public function storeRequestedTypeDocumentsList($requested_type,$owner_id,$requester_id){

        if(isset($requested_type) && !empty($requested_type)){
            $data['documents_list'] = implode(",", $requested_type);
        }

        if(isset($requester_id) && !empty($requester_id)){
            $data['requester_id'] = $requester_id;
        }

        if(isset($owner_id) && !empty($owner_id)){
            $data['owner_id'] = $owner_id;
        }

        return $this->userDocumentsTypeRepository->storeRequestedTypeDocumentsList($data);
    }


    public function changeUserDocumentStatus($document_details,$user_id){
        return $this->userDocumentsTypeRepository->changeUserDocumentStatus($document_details,$user_id);
    }

    public function ListDownloadedDocuments($options = NULL){
        return $this->userDocumentsTypeRepository->ListDownloadedDocuments($options)->toArray();
    }

    public function getRequestedDocumentList($options,$filter = NULL){
        return $this->userDocumentsTypeRepository->getRequestedDocumentList($options,$filter);
    }
    
}