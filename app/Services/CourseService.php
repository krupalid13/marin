<?php 
namespace App\Services;
use App\Repositories\CourseRepository;
use Auth;


class CourseService
{
    private $courseRepository;

    function __construct()
    {
        $this->courseRepository = New CourseRepository();
    }

    public function applyForCourse($data){

        $prev_applied = $this->courseRepository->prev_applied($data);

        if($prev_applied){
            $status_arr = ['status' => 'error','prev_applied' => 'true'];
        }else{
            $result = $this->courseRepository->store($data);
            if( count($result) > 0 ) {
                $status_arr = ['status' => 'success'];
            } else {
                $status_arr = ['status' => 'error'];
            }
        }
    	
        return $status_arr;
    }
}