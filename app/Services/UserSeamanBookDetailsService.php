<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 4/21/2017
 * Time: 4:44 PM
 */

namespace App\Services;
use App\Repositories\UserSeamanBookDetailsRepository;

class UserSeamanBookDetailsService
{
    private $userSeamanBookDetailsRepository;

    public function __construct()
    {
        $this->userSeamanBookDetailsRepository = new UserSeamanBookDetailsRepository();
    }

    public function store($data)
    {
        return $this->userSeamanBookDetailsRepository->store($data);
        
    }

    public function getDetailsByUserId($user_id) {
        return $this->userSeamanBookDetailsRepository->getDetailsByUserId($user_id);
    }

    public function updateByUserId($data, $user_id) {
        $this->userSeamanBookDetailsRepository->updateByUserId($data, $user_id);
    }

    public function deleteServiceById($user_id){
        $this->userSeamanBookDetailsRepository->deleteServiceById($user_id);
    }
    
    public function updateBySeamanId($data, $semanId) {
        $this->userSeamanBookDetailsRepository->updateBySeamanId($data, $semanId);
    }
    
    public function deleteServiceBySeamanIds($semanIds) {
        $this->userSeamanBookDetailsRepository->deleteServiceBySeamanIds($semanIds);
    }
}