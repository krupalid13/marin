<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 5/2/2017
 * Time: 4:19 PM
 */
namespace App\Services;
use App\Repositories\SeafarerSubscriptionRepository;

class SeafarerSubscriptionService
{
    private $seafarerSubscriptionRepository;

    function __construct()
    {
        $this->seafarerSubscriptionRepository =new SeafarerSubscriptionRepository();
    }

    public function getSeafarerSubscriptionsById($user_id){
        return $this->seafarerSubscriptionRepository->getSeafarerSubscriptionsById($user_id);
    }

    public function store($subscription_details)
    {
        $order_id = $subscription_details[0]['order_id'];
        foreach($subscription_details as $subscription_data){

            $order_seafarer_subscriptions[] = '';
            $seafarer_subscriptions = $this->seafarerSubscriptionRepository->getSeafarerSubscriptionsByUserIdWithActiveSubscription($subscription_data['user_id'])->toArray();

            $order_seafarer_subscriptions['user_id'] = $subscription_data['user_id'];
            $order_seafarer_subscriptions['order_id'] = $order_id;
            $order_seafarer_subscriptions['subscription_id'] = $subscription_data['subscription_id'];
            $order_seafarer_subscriptions['subscription_details'] = $subscription_data['subscription_details'];
            $order_seafarer_subscriptions['duration'] = $subscription_data['duration'];

            if(count($seafarer_subscriptions) > 0){
                $order_seafarer_subscriptions['valid_from'] = NULL;
                $order_seafarer_subscriptions['valid_to'] = NULL;
                $order_seafarer_subscriptions['status'] = 0;
            }else{
                $order_seafarer_subscriptions['valid_from'] = date('Y/m/d');
                $order_seafarer_subscriptions['valid_to'] = date('Y/m/d', strtotime('+'.$subscription_data['duration'].' months'));
                $order_seafarer_subscriptions['status'] = 1;
            }
            $this->seafarerSubscriptionRepository->store($order_seafarer_subscriptions);

        }

        return true;
       /* echo $user_id;
        if(count($seafarer_subscriptions) > 0){
            return $this->subscriptionRepository->storeSeafarerSubscriptionsByUserId($subscription_details,$user_id);

        }*/
    }
}