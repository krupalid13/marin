<?php 

namespace App\Services;

use App\Repositories\PincodeRepository;

class PincodeService
{

    private $pincodeRepository;

    function __construct() {
        $this->pincodeRepository = new PincodeRepository();
    }

    public function getStatesCities($pincode) {
        return $this->pincodeRepository->getStatesCities($pincode);
    }
}