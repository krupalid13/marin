<?php namespace App\Services;

use App\Repositories\UserProfessionalDetailsRepository;

class UserProfessionalDetailsService
{
    private $userProfessionalDetailsRepository;

    public function __construct()
    {
        $this->userProfessionalDetailsRepository = new UserProfessionalDetailsRepository();
    }

    public function store($data)
    {
        $this->userProfessionalDetailsRepository->store($data);
    }

    public function updateByUserId($data,$user_id)
    {
        return $this->userProfessionalDetailsRepository->updateByUserId($data,$user_id);
    }

    public function getDetailsByUserId($user_id)
    {
        return $this->userProfessionalDetailsRepository->getUserProfessionalDetailsById($user_id);
    }

    public function getAllSeafarersByCurrentDayAvailability($date){
        return $this->userProfessionalDetailsRepository->getAllSeafarersByCurrentDayAvailability($date);
    }

    public function getAllSeafarersExpiringAvailabilityBeforeGivenDays($current_date,$to_date){
        return $this->userProfessionalDetailsRepository->getAllSeafarersExpiringAvailabilityBeforeGivenDays($current_date,$to_date);
    }
}

?>