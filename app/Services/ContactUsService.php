<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 4/29/2017
 * Time: 12:12 PM
 */

namespace App\Services;
use App\Repositories\ContactUsRepository;
use App\Services\SendEmailService;

class ContactUsService
{
    private $contactUsRepository;
    private $sendEmailService;

    public function __construct()
    {
        $this->contactUsRepository = new ContactUsRepository();
        $this->sendEmailService = new SendEmailService();
    }

    public function store($contact_us_data)
    {
        $this->sendEmailService->sendEnquiryMail($contact_us_data);
        return $this->contactUsRepository->store($contact_us_data);
    }
}