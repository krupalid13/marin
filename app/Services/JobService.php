<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 5/23/2017
 * Time: 6:33 PM
 */

namespace App\Services;
use App\Repositories\JobRepository;
use App\Repositories\JobApplicationRepository;

class JobService
{
    private $jobRepository;
    private $jobApplicationRepository;

    function __construct()
    {
        $this->jobRepository = new JobRepository;
        $this->jobApplicationRepository = new JobApplicationRepository;
    }

    public function store($data){

        if(isset($data['date_of_joining']) && !empty($data['date_of_joining'])){

            $parts = explode('-', $data['date_of_joining']);
            $month = $parts[0];
            $year = $parts[1];
            
            $last_day = cal_days_in_month(CAL_GREGORIAN,$month,$year);
            $data['date_of_joining'] = $last_day."-".$month."-".$year;

            $data['valid_to'] = date("Y-m-d", strtotime($year."-".$month."-".$last_day));
        }

        if(isset($data['nationality']) && !empty($data['nationality'])){
            $nationalities = $data['nationality'];
            unset($data['nationality']);
        }
        
        $result = $this->jobRepository->store($data);

        if(isset($nationalities)){
            $this->jobRepository->storeJobNationalities($nationalities,$result->id);
        }

        if( count($result) > 0 ) {
            $status_arr = ['status' => 'success'];
        } else {
            $status_arr = ['status' => 'error'];
        }
        return $status_arr;
    }

    public function getJobDetailsByJobId($job_id){
        $result = $this->jobRepository->getJobDetailsByJobId($job_id);
        return $result;
    }

    public function update($data){

        if(isset($data['date_of_joining']) && !empty($data['date_of_joining'])){

            $parts = explode('-', $data['date_of_joining']);
            $month = $parts[0];
            $year = $parts[1];
            
            $last_day = cal_days_in_month(CAL_GREGORIAN,$month,$year);
            $data['date_of_joining'] = $last_day."-".$month."-".$year;

            $data['valid_to'] = date("Y-m-d", strtotime($year."-".$month."-".$last_day));
        }

        if(isset($data['nationality']) && !empty($data['nationality'])){
            $nationalities = $data['nationality'];
            unset($data['nationality']);
        }
        
        $result = $this->jobRepository->update($data);

        if(isset($nationalities)){
            $this->jobRepository->updateJobNationalities($nationalities,$data['job_id']);
        }

        if( count($result) > 0 ) {
            $status_arr = ['status' => 'success'];
        } else {
            $status_arr = ['status' => 'error'];
        }
        return $status_arr;
    }

    public function getJobDetailsByCompanyId($company_id){
        $result = $this->jobRepository->getJobDetailsByCompanyId($company_id);
        return $result;
    }

    public function getJobDetailsByCompanyIdByPaginate($company_id){
        $result = $this->jobRepository->getJobDetailsByCompanyIdByPaginate($company_id);
        return $result;
    }

    public function getJobDetailsByPaginate($paginate,$filter){
        $result = $this->jobRepository->getJobDetailsByPaginate($paginate,$filter);
        return $result;
    }

    public function disableJobByJobId($job_id){
        $result = $this->jobRepository->disableJobByJobId($job_id);
        if( count($result) > 0 ) {
            $status_arr = ['status' => 'success'];
        } else {
            $status_arr = ['status' => 'error'];
        }
        return $status_arr;
    }

    public function enableJobByJobId($job_id){
        $result = $this->jobRepository->enableJobByJobId($job_id);
        if( count($result) > 0 ) {
            $status_arr = ['status' => 'success'];
        } else {
            $status_arr = ['status' => 'error'];
        }
        return $status_arr;
    }

    public function getAllJobDetails($search_data = NULL, $paginate = NULL, $options = NULL, $user_id = NULL){

        if(isset($search_data['_token'])){
            unset($search_data['_token']);
        }
        if(isset($search_data['page'])){
            unset($search_data['page']);
        }
        if(isset($search_data['ship_type']) && $search_data['ship_type'] == ''){
            unset($search_data['ship_type']);
        }
        if(isset($search_data['min_rank_exp']) && $search_data['min_rank_exp'] == ''){
            unset($search_data['min_rank_exp']);
        }
        if(isset($search_data['rank']) && $search_data['rank'] == ''){
            unset($search_data['rank']);
        }
        if(isset($search_data['date_of_joining']) && $search_data['date_of_joining'] == ''){
            unset($search_data['date_of_joining']);
        }
        if(isset($search_data['nationality']) && $search_data['nationality'] == ''){
            unset($search_data['nationality']);
        }
        $result = $this->jobRepository->getAllJobDetails($search_data,$paginate,$options,$user_id);
        return $result;
    }

    public function getAllUserAppliedJobs($company_id, $options, $paginate, $filter=NULL){
        $result = $this->jobRepository->getAllUserAppliedJobs($company_id, $options, $paginate, $filter);
        return $result;
    }

    public function applyForJob($data){
        
        $job_apply = $this->jobApplicationRepository->checkJobApplyAvailability($data);
        if($job_apply == 1){
            $result = $this->jobApplicationRepository->store($data);
            if( count($result) > 0 ) {
                $status_arr = ['status' => 'success'];
            } else {
                $status_arr = ['status' => 'error'];
            }
        }
        else {
            $status_arr = ['status' => 'success','job_apply' => '0'];
        }
        return $status_arr;
    }

    public function getUserAppliedJobsByUserId($user_id){
        return $this->jobApplicationRepository->getUserAppliedJobsByUserId($user_id);
    }

    public function getJobDetailsByRankAndLocation($rank_id,$location = NULL){
        return $this->jobRepository->getJobDetailsByRankAndLocation($rank_id,$location);
    }
}