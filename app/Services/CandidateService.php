<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 5/25/2017
 * Time: 4:24 PM
 */

namespace App\Services;
use App\Repositories\CandidateRepository;


class CandidateService
{
    private $candidateRepository;

    function __construct()
    {
        $this->candidateRepository = New CandidateRepository();
    }

    function getAllCandidateDataByFilters($filter,$paginate){
        return $this->candidateRepository->getAllCandidateDataByFilters($filter,$paginate);
    }

    function getAllCandidateDataByFiltersWithoutPagination($filter=NULL){
        return $this->candidateRepository->getAllCandidateDataByFiltersWithoutPagination($filter);
    }
}