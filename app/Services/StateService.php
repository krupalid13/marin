<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 4/29/2017
 * Time: 12:12 PM
 */

namespace App\Services;
use App\Repositories\StateRepository;

class StateService
{
    private $stateRepository;

    public function __construct()
    {
        $this->stateRepository = new StateRepository();
    }

    public function getAllStatesWithCities()
    {
    	return $this->stateRepository->getAllStatesWithCities();
    }

}