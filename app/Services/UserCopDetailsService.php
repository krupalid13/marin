<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 4/21/2017
 * Time: 4:44 PM
 */

namespace App\Services;
use App\Repositories\UserCopDetailsRepository;

class UserCopDetailsService
{
    private $userCopDetailsRepository;

    public function __construct()
    {
        $this->userCopDetailsRepository = new UserCopDetailsRepository();
    }

    public function store($data)
    {
        $this->userCopDetailsRepository->store($data);
    }

    public function getDetailsByUserId($user_id) {
        return $this->userCopDetailsRepository->getDetailsByUserId($user_id);
    }

    public function updateByUserId($data, $user_id) {
        $this->userCopDetailsRepository->updateByUserId($data, $user_id);
    }

     public function deleteServiceById($user_id){
        $this->userCopDetailsRepository->deleteServiceById($user_id);
    }
}