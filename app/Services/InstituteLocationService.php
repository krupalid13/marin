<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 5/18/2017
 * Time: 5:34 PM
 */

namespace App\Services;
use App\Repositories\InstituteLocationRepository;

class InstituteLocationService
{
    private $instituteLocationRepository;

    function __construct()
    {
        $this->instituteLocationRepository = New InstituteLocationRepository();
    }

    public function store($data){
        return $this->instituteLocationRepository->store($data);
    }

    public function update($data,$location_id){
        return $this->instituteLocationRepository->update($data,$location_id);
    }

    public function deleteRecordsByInstituteId($institute_id){
        return $this->instituteLocationRepository->deleteRecordsByInstituteId($institute_id);
    }
    public function deleteRecordsByInstituteIdAndLocationIds($institute_id,$location_ids){
        return $this->instituteLocationRepository->deleteRecordsByInstituteIdAndLocationIds($institute_id,$location_ids);
    }
}