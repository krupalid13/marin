<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 4/21/2017
 * Time: 4:45 PM
 */

namespace App\Services;
use App\Repositories\UserGmdssDetailsRepository;

class UserGmdssDetailsService
{
    private $userGmdssDetailsRepository;

    public function __construct()
    {
        $this->userGmdssDetailsRepository = new UserGmdssDetailsRepository();
    }

    public function store($data)
    {
        $detail = $this->getDetailsByUserId($data['user_id'])->toArray();
        if( count($detail) > 0 ) {
            $this->updateByUserId($data, $data['user_id']);
        } else {
            $this->userGmdssDetailsRepository->store($data);
        }
    }

    public function getDetailsByUserId($user_id) {
        return $this->userGmdssDetailsRepository->getDetailsByUserId($user_id);
    }

    public function updateByUserId($data, $user_id) {
        $this->userGmdssDetailsRepository->updateByUserId($data, $user_id);
    }
}