<?php
namespace App\Services;

use App\Repositories\NotificationRepository;
use PubNub\PNConfiguration;
use PubNub\PubNub;
use App\Notification;
use App\User;
use View;

class NotificationService
{
	protected $repository;

	public function __construct()
	{
		$this->repository = new NotificationRepository();
    }

    public function initPubnubAndPublish($channel,$data)
    {
    	$pnConfiguration = new PNConfiguration();
 
		$pnConfiguration->setSubscribeKey(env('PUBNUB_SUBSCRIBE_KEY'));
		$pnConfiguration->setPublishKey(env('PUBNUB_PUBLISH_KEY'));
		$pnConfiguration->setSecure(false);
		 
		$pubnub = new PubNub($pnConfiguration);

		//$subscribeCallback = new MySubscribeCallback();

		//$pubnub->addListener($subscribeCallback);
    
    	$result = $pubnub->publish()
	      ->channel($channel)
	      ->message($data)
	      ->sync();

        /*$admin_id = \App\User::where('registered_as','admin')->first();

        if(!empty($admin_id->id)){
            $result = $pubnub->publish()
              ->channel($admin_id->id.'_Notification')
              ->message($data)
              ->sync();
        }
*/    	// $pubnub->subscribe()
	    // ->channels('aaaaaa')
	    // ->execute();
	   return $result;
    }


	public function getUserNotifications($user_id,$count=NULL)
    {
        return $this->repository->getUserNotifications($user_id,$count)->toArray();
    }

    public function getUserNotificationByUserId($user_id,$paginate=NULL)
    {
        return $this->repository->getUserNotificationByUserId($user_id,$paginate);
    }

    public function getUserAllNotifications()
    {
        return $this->repository->getUserAllNotifications()->toArray();
    }

    public function getUserNotificationById($notification_id)
    {
        return $this->repository->getUserNotificationById($notification_id)->toArray();
    }

    public function createOrUpdate($data)
    {
    	return $this->repository->createOrUpdate($data);
    }

    public function changeStatusSeen($user_id)
    {
    	return $this->repository->changeStatusSeen($user_id);
    }

    public function changeStatusRead($notification_id)
    {
    	return $this->repository->changeStatusRead($notification_id);
    }

    public function getMessage($type,$payload,$notification_id){

        switch ($type) {
            case 'batch_approved':
                    return $this->message_batch_approved($payload,$notification_id);
                break;

            case 'batch_booked':
                    return $this->message_batch_booked($payload,$notification_id);
                break;

            case 'batch_cancelled':
                    return $this->message_batch_cancelled($payload,$notification_id);
                break;

            case 'batch_confirmed':
                    return $this->message_batch_confirmed($payload,$notification_id);
                break; 

            case 'batch_partially_confirmed':
                    return $this->message_batch_partially_confirmed($payload,$notification_id);
                break;

            case 'batch_denied':
                    return $this->message_batch_denied($payload,$notification_id);
                break;
            
            default:
                # code...
                break;
        }
    }

    public function message_batch_booked($payload,$notification_id){

        $message = '';
        $payload = json_decode($payload,true);

        $course_name = isset($payload['batch_details']['course_details']['course_details']['course_name']) && !empty($payload['batch_details']['course_details']['course_details']['course_name']) ? $payload['batch_details']['course_details']['course_details']['course_name'] : 'course';

        $user_name = isset($payload['user_details']['first_name']) && !empty($payload['user_details']['first_name']) ? ucwords($payload['user_details']['first_name']) : 'User';

        if(isset($payload['order_details']) && !empty($payload['order_details'])){

            if(isset($payload['order_details']['updated_at']) && !empty($payload['order_details']['updated_at'])){
                $date = $payload['order_details']['updated_at'];
            }else{
                $date = $payload['order_details']['created_at'];
            }
        }

        $data['title'] = 'Batch Booked';
        $data['message'] = "Batch for $course_name has been booked by $user_name.";
        $data['notification_id'] = $notification_id;
        $data['status'] = Notification::find($notification_id)->status;
        $data['redirect_url'] = route('site.institute.list.batch.applicant',['email' => $payload['user_details']['email']]);

        if(isset($date)){
            $data['date'] = date('H:i - M d, Y',strtotime('+5 hour +30 minutes',strtotime($date)));
            //$data['date'] = '12:51 PM - June 19, 2018';
        }

        if(isset($payload['user_details']['id'])){
            $image = \App\User::find($payload['user_details']['id'])->profile_pic;
            if(!empty($image)){
                $data['image_path'] = env('SEAFARER_PROFILE_PATH')."".$payload['user_details']['id']."/".$image;
            }else{
                $data['image_path'] = 'images/user_default_image.png';
            }
        }else{
            $data['image_path'] = 'images/user_default_image.png';
        }

        return $this->loadMessage($data);

    }

    public function message_batch_confirmed($payload,$notification_id){

        $message = '';
        $payload = json_decode($payload,true);

        $course_name = isset($payload['batch_details']['course_details']['course_details']['course_name']) && !empty($payload['batch_details']['course_details']['course_details']['course_name']) ? $payload['batch_details']['course_details']['course_details']['course_name'] : 'course';

        $user_name = isset($payload['user_details']['first_name']) && !empty($payload['user_details']['first_name']) ? ucwords($payload['user_details']['first_name']) : 'User';

        if(isset($payload['order_details']) && !empty($payload['order_details'])){

            if(isset($payload['order_details']['updated_at']) && !empty($payload['order_details']['updated_at'])){
                $date = $payload['order_details']['updated_at'];
            }else{
                $date = $payload['order_details']['created_at'];
            }
        }

        $data['title'] = 'Batch Confirmed';
        $data['message'] = "Full payment has been made for $course_name by $user_name.";
        $data['notification_id'] = $notification_id;
        $data['status'] = Notification::find($notification_id)->status;
        $data['redirect_url'] = route('site.institute.list.batch.applicant',['email' => $payload['user_details']['email']]);

        if(isset($date)){
            $data['date'] = date('H:i - M d, Y',strtotime('+5 hour +30 minutes',strtotime($date)));
            //$data['date'] = '12:51 PM - June 19, 2018';
        }

        if(isset($payload['user_details']['id'])){
            $image = \App\User::find($payload['user_details']['id'])->profile_pic;
            if(!empty($image)){
                $data['image_path'] = env('SEAFARER_PROFILE_PATH')."".$payload['user_details']['id']."/".$image;
            }else{
                $data['image_path'] = 'images/user_default_image.png';
            }
        }else{
            $data['image_path'] = 'images/user_default_image.png';
        }

        return $this->loadMessage($data);

    }

    public function message_batch_partially_confirmed($payload,$notification_id){

        $message = '';
        $payload = json_decode($payload,true);

        $course_name = isset($payload['batch_details']['course_details']['course_details']['course_name']) && !empty($payload['batch_details']['course_details']['course_details']['course_name']) ? $payload['batch_details']['course_details']['course_details']['course_name'] : 'course';

        $user_name = isset($payload['user_details']['first_name']) && !empty($payload['user_details']['first_name']) ? ucwords($payload['user_details']['first_name']) : 'User';

        if(isset($payload['order_details']) && !empty($payload['order_details'])){

            if(isset($payload['order_details']['updated_at']) && !empty($payload['order_details']['updated_at'])){
                $date = $payload['order_details']['updated_at'];
            }else{
                $date = $payload['order_details']['created_at'];
            }
        }

        $data['title'] = 'Batch Confirmed';
        $data['message'] = "Partial payment has been made for $course_name by $user_name.";
        $data['notification_id'] = $notification_id;
        $data['status'] = Notification::find($notification_id)->status;
        $data['redirect_url'] = route('site.institute.list.batch.applicant',['email' => $payload['user_details']['email']]);

        if(isset($date)){
            $data['date'] = date('H:i - M d, Y',strtotime('+5 hour +30 minutes',strtotime($date)));
            //$data['date'] = '12:51 PM - June 19, 2018';
        }

        if(isset($payload['user_details']['id'])){
            $image = \App\User::find($payload['user_details']['id'])->profile_pic;
            if(!empty($image)){
                $data['image_path'] = env('SEAFARER_PROFILE_PATH')."".$payload['user_details']['id']."/".$image;
            }else{
                $data['image_path'] = 'images/user_default_image.png';
            }
        }else{
            $data['image_path'] = 'images/user_default_image.png';
        }

        return $this->loadMessage($data);

    }

    public function message_batch_cancelled($payload,$notification_id){

        $message = '';
        $payload = json_decode($payload,true);

        $course_name = isset($payload['batch_details']['course_details']['course_details']['course_name']) && !empty($payload['batch_details']['course_details']['course_details']['course_name']) ? $payload['batch_details']['course_details']['course_details']['course_name'] : 'course';

        $user_name = isset($payload['user_details']['first_name']) && !empty($payload['user_details']['first_name']) ? ucwords($payload['user_details']['first_name']) : 'User';

        if(isset($payload['order_details']) && !empty($payload['order_details'])){

            if(isset($payload['order_details']['updated_at']) && !empty($payload['order_details']['updated_at'])){
                $date = $payload['order_details']['updated_at'];
            }else{
                $date = $payload['order_details']['created_at'];
            }
        }

        $data['title'] = 'Batch Cancelled';
        $data['message'] = "Batch for $course_name has been cancelled by $user_name.";
        $data['notification_id'] = $notification_id;
        $data['status'] = Notification::find($notification_id)->status;
        $data['redirect_url'] = route('user.bookings');

        if(isset($date)){
            $data['date'] = date('H:i - M d, Y',strtotime('+5 hour +30 minutes',strtotime($date)));
            //$data['date'] = '12:51 PM - June 19, 2018';
        }

        if(isset($payload['user_details']['id'])){
            $image = \App\User::find($payload['user_details']['id'])->profile_pic;
            if(!empty($image)){
                $data['image_path'] = env('SEAFARER_PROFILE_PATH')."".$payload['user_details']['id']."/".$image;
            }else{
                $data['image_path'] = 'images/user_default_image.png';
            }
        }else{
            $data['image_path'] = 'images/user_default_image.png';
        }

        return $this->loadMessage($data);

    }

    public function message_batch_approved($payload,$notification_id){

        $message = '';
        $payload = json_decode($payload,true);

        $course_name = isset($payload['batch_details']['course_details']['course_details']['course_name']) && !empty($payload['batch_details']['course_details']['course_details']['course_name']) ? $payload['batch_details']['course_details']['course_details']['course_name'] : 'course';

        $institute_name = isset($payload['batch_details']['course_details']['institute_registration_detail']['institute_name']) && !empty($payload['batch_details']['course_details']['institute_registration_detail']['institute_name']) ? $payload['batch_details']['course_details']['institute_registration_detail']['institute_name'] : 'institute';

        if(isset($payload['order_details']) && !empty($payload['order_details'])){

            if(isset($payload['order_details']['updated_at']) && !empty($payload['order_details']['updated_at'])){
                $date = $payload['order_details']['updated_at'];
            }else{
                $date = $payload['order_details']['created_at'];
            }
        }

        $data['title'] = 'Batch Approved';
        $data['message'] = "Batch for $course_name has been approved by $institute_name.";
        $data['notification_id'] = $notification_id;
        $data['status'] = Notification::find($notification_id)->status;
        $data['redirect_url'] = route('user.bookings');

        if(isset($date)){
            $data['date'] = date('H:i - M d, Y',strtotime('+5 hour +30 minutes',strtotime($date)));
            //$data['date'] = '12:51 PM - June 19, 2018';
        }
       
        if(isset($payload['batch_details']['course_details']['institute_registration_detail']['user_id'])){
            $image = \App\User::find($payload['batch_details']['course_details']['institute_registration_detail']['user_id'])->profile_pic;
            if(!empty($image)){
                $data['image_path'] = env('INSTITUTE_LOGO_PATH')."".$payload['batch_details']['course_details']['institute_registration_detail']['user_id']."/".$image;
            }else{
                $data['image_path'] = 'images/user_default_image.png';
            }
        }else{
            $data['image_path'] = 'images/user_default_image.png';
        }

        return $this->loadMessage($data);

    }

    public function message_batch_denied($payload,$notification_id){

        $message = '';
        $payload = json_decode($payload,true);

        $course_name = isset($payload['batch_details']['course_details']['course_details']['course_name']) && !empty($payload['batch_details']['course_details']['course_details']['course_name']) ? $payload['batch_details']['course_details']['course_details']['course_name'] : 'course';

        $institute_name = isset($payload['batch_details']['course_details']['institute_registration_detail']['institute_name']) && !empty($payload['batch_details']['course_details']['institute_registration_detail']['institute_name']) ? $payload['batch_details']['course_details']['institute_registration_detail']['institute_name'] : 'institute';

        if(isset($payload['order_details']) && !empty($payload['order_details'])){

            if(isset($payload['order_details']['updated_at']) && !empty($payload['order_details']['updated_at'])){
                $date = $payload['order_details']['updated_at'];
            }else{
                $date = $payload['order_details']['created_at'];
            }
        }

        $data['title'] = 'Batch Denied';
        $data['message'] = "Batch for $course_name has been denied by $institute_name.";
        $data['notification_id'] = $notification_id;
        $data['status'] = Notification::find($notification_id)->status;
        $data['redirect_url'] = route('user.bookings');

        if(isset($date)){
            $data['date'] = date('H:i - M d, Y',strtotime('+5 hour +30 minutes',strtotime($date)));
            //$data['date'] = '12:51 PM - June 19, 2018';
        }
       
        if(isset($payload['batch_details']['course_details']['institute_registration_detail']['user_id'])){
            $image = \App\User::find($payload['batch_details']['course_details']['institute_registration_detail']['user_id'])->profile_pic;
            if(!empty($image)){
                $data['image_path'] = env('INSTITUTE_LOGO_PATH')."".$payload['batch_details']['course_details']['institute_registration_detail']['user_id']."/".$image;
            }else{
                $data['image_path'] = 'images/user_default_image.png';
            }
        }else{
            $data['image_path'] = 'images/user_default_image.png';
        }

        return $this->loadMessage($data);

    }

    public function loadMessage($message){

        $message = View::make("site.partials.notification_message")->with(['data' => $message]);

        return $message->render();
    }

}