<?php namespace App\Services;


use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;
use Mail;
use Response;
use SparkPost\SparkPost;
use GuzzleHttp\Client;
use Http\Adapter\Guzzle6\Client as GuzzleAdapter;
use App\Services\ShortenUrlService;


class SendEmailService {
    
    private $shortenUrlService;
    
    function __construct(){
        $this->shortenUrlService = new ShortenUrlService();
    }

    public function sendVerificationEmailToUser($array){
    

	
        $array['link'] = route('site.user.verify.email', base64_encode($array['email']));

        $blade_file = 'emails.seafarerEmailVerification';
        $title = 'Flanknot: Verify your email';
        $subject = 'Flanknot: Verify your email';
        $message = 'Please verify your email by clicking';
        $data = $array;
        
        return $this->sendEmail($blade_file, $data, $array, $title, $subject, null);
    }
   
    public function sendJobEmailToUser($array){

        $blade_file = 'emails.admin_send_job_mail_to_user';
        $title = 'Flanknot: Job openings';
        $subject = 'Flanknot: Job openings';
        $message = 'We Have Found a New job Opening that suits your profile.';
        $data = $array;
        $data['subject'] = $subject;
        
        return $this->sendEmail($blade_file, $data, $array, $title, $subject, null);
    }

    public function sendUserJobAppliedMailToCompany($array){

        $blade_file = 'emails._send_mail_to_company_user_applied_job';
        $title = 'Flanknot: Job Application';
        $subject = 'Flanknot: Job Application';
        $data = $array;
        $data['subject'] = $subject;
        
        return $this->sendEmailToCompany($blade_file, $data, $array, $title, $subject, null);
    }

    public function sendAvalailityEmailToSeafarer($array){
        
        $blade_file = 'emails.seafarerUpdateAvailability';
        $title = 'Flanknot: Update your availability';
        $subject = 'Flanknot: Update your availability';
        $message = 'Please update your availability';
        $data = $array;
        $data['route'] = route('user.update.availability',$array['user_id']);
        $user_details = $array['user_details'];
        
        return $this->sendEmail($blade_file, $data, $user_details, $title, $subject, null);
    }

    public function sendExpireAvalailityEmailToSeafarer($array,$expire_day){
        
        $blade_file = 'emails.seafarerExpireAvailability';
        $title = 'Flanknot: Update your availability';
        $subject = 'Flanknot: Update your availability';
        $message = 'Please update your availability';
        $data = $array;
        $data['expire_before'] = $expire_day;
        $data['route'] = route('user.update.availability',$array['id']);
        $user_details = $array;
        
        return $this->sendEmail($blade_file, $data, $user_details, $title, $subject, null);
    }

    public function sendSeafarerEmailToInstitute($array){
        
        $blade_file = 'emails.instituteCourseSeafarer';
        $title = 'Flanknot: Seafarers according to course expiry';
        $subject = 'Flanknot: Seafarers according to course expiry';
        $message = 'eafarers according to course expiry';
        $data = $array;
        $user_details = $array['user_details'];
        
        return $this->sendEmail($blade_file, $data, $user_details, $title, $subject, null);
    }

    public function sendJobsToSeafarer($array){
        
        $blade_file = 'emails.seafarerJobAvailability';
        $title = 'Flanknot: Available jobs for you';
        $subject = 'Flanknot: Available jobs for you';
        $message = 'Available jobs for you';
        $data = $array;
        
        return $this->sendEmail($blade_file, $data, $array['user_details'], $title, $subject, null);
    }

    public function sendCandidatesToCompany($array){

        $blade_file = 'emails.companyMatchedJobSeafarers';
        $title = 'Flanknot: Candidate search according to job';
        $subject = 'Flanknot: Candidate search according to job';
        $message = 'Candidate search according to job';
        $data = $array;
        $user_details = $array['company_registration_details'];
        
        return $this->sendEmail($blade_file, $data, $user_details, $title, $subject, null);
    }


    public function sendEnquiryMail($array){

            $blade_file = 'emails.enquiryMail';
            $data = $array;
            $array['email'] = 'sachin.chavan03@gmail.com';
            $title = 'Flanknot: New Enquiry';
            $subject = 'Flanknot: New Enquiry';

        //$this->sendEmail($blade_file, $data, $array, $title, $subject);

    }

    public function sendResetPasswordEmail($array){
        $array['link'] = route('admin.set.new.password', base64_encode($array['email']));
        $blade_file = 'emails.resetPassword';
        $data = $array;
        $title = 'Flanknot: Reset Password';
        $subject = 'Flanknot: Reset Password';
        $this->sendEmail($blade_file, $data, $array, $title, $subject);
    }

    public function sendWelcomeEmailWithPasswordResetMail($array,$token){
        $array['link'] = route('site.reset.password.with.token', $token."?set_password=1");
        $blade_file = 'emails.welcomeWithSetPassword';
        $data = $array;
        $title = 'Flanknot: Welcome!!!';
        $subject = 'Flanknot: Password Reset!!!';
        return $this->sendEmail($blade_file, $data, $array, $title, $subject);
    }

    //Institute full payment notification
    public function sendFullPaymentNotification($array,$batch_details){
        $blade_file = 'emails.fullPaymentNotificationInstitute';
        $title = 'Flanknot: Complete payment of your order';
        $subject = 'Flanknot: Complete payment of your order';
        $message = 'Please complete your payment';
        $data = $array;
        
        $data['route'] = route('site.seafarer.course.book',['batch_id' => $batch_details['id'],'location_id' => $batch_details['batch_location'][0]['location_id']]);
        $user_details = $array;
        
        return $this->sendEmail($blade_file, $data, $user_details, $title, $subject, null);
    }

    public function sendEmailToNewRegisteredUser($array){
        
        $blade_file = 'emails.seafarerRegistration';
        $data = $array;
        $title = 'FLANKNOT.com: Welcome!!!';
        $subject = 'FLANKNOT.com: Registration!!!';
        if($array['registered_as'] == 'seafarer'){
            $data['profile'] = route('user.view.profile',$array['id']);
        }
        if($array['registered_as'] == 'advertiser'){
            $data['profile'] = route('site.advertiser.profile.id',$array['id']);
        }
        if($array['registered_as'] == 'company'){
            $data['profile'] = route('site.show.company.details.user',$array['id']);
        }
        if($array['registered_as'] == 'institute'){
            $data['profile'] = route('site.show.institute.details.id',$array['id']);
        }
        
        $email = $this->sendEmail($blade_file, $data, $array, $title, $subject);
        return $email;
       
    }

    public function sendPdfToCompanyResumeDownload($array,$file_path){
        $blade_file = 'emails.seafarerPdf';
        $data = $array;
        $title = 'Flanknot: Resume Downloaded!!!';
        $subject = "Flanknot: Resume Downloaded - ".$array['candidate_name']."!!!";
        $data['send_pdf'] = 1;
        $data['file_path'] = $file_path;
        
        $email = $this->sendEmail($blade_file, $data, $array, $title, $subject);
        return $email;
    }

    //Email to seafarer on approval of batch
    public function sendBatchApprovalEmailToUser($array,$reason=NULL){
        
        $array['link'] = route('site.seafarer.course.book', ['batch_id' => $array['batch_id'],'location_id' => $array['batch_details']['batch_location'][0]['location_id'],'order_id' => $array['id']]);

        //$array['link'] = $this->shortenUrlService->shortenUrl(route('site.seafarer.course.book', ['batch_id' => $array['batch_id'],'location_id' => $array['batch_details']['batch_location'][0]['location_id'],'make_payment' => 1, 'order_id' => $array['id']]));
        //dd($array['link']);

        $blade_file = 'emails.batchApproval';
        $title = 'Flanknot: Course Booking Approved';
        $subject = 'Flanknot: Course Booking Approved';
        $data = $array->toArray();

        if($reason != ''){
            $data['reason'] = $reason;
        }
        
        return $this->sendEmail($blade_file, $data, $array['user'], $title, $subject, null);
    }

    public function sendBatchBlockSeatEmailToInstitute($array,$seafarer_details){

        $blade_file = 'emails.instituteBlockSeat';
        $title = 'Flanknot: Institute Batch Seat Block';
        $subject = 'Flanknot: Institute Batch Seat Block';
        $data = $array->toArray();
        $data['seafarer'] = $seafarer_details->toArray();
        
        return $this->sendEmail($blade_file, $data, $array['course_details']['institute_registration_detail'], $title, $subject, null);
    }

    public function sendOrderCancelEmailToInstitute($array,$seafarer_details){

        $blade_file = 'emails.instituteCancelOrder';
        $title = 'Flanknot: Institute Batch Order Cancel';
        $subject = 'Flanknot: Institute Batch Order Cancel';
        $data = $array->toArray();
        $data['seafarer'] = $seafarer_details->toArray();
        
        return $this->sendEmail($blade_file, $data, $array['course_details']['institute_registration_detail'], $title, $subject, null);
    }

    public function sendBatchBlockSeatConfirmEmailToInstitute($array,$seafarer_details){

        $blade_file = 'emails.instituteBlockSeat';
        $title = 'Flanknot: Institute Batch Seat Block';
        $subject = 'Flanknot: Institute Batch Seat Block';
        $data = $array->toArray();
        $data['seafarer'] = $seafarer_details->toArray();
        
        return $this->sendEmail($blade_file, $data, $array['course_details']['institute_registration_detail'], $title, $subject, null);
    }

    public function sendBatchSeatConfirmEmailToSeafarer($array,$seafarer_details){
        
        $blade_file = 'emails.instituteBatchBookSuccess';

        if(isset($array['order_details'][0]['status']) && $array['order_details'][0]['status'] == '6'){
            $pay_type = 'Partial';
        }else{
            $pay_type = 'Full';
        }

        $title = "Flanknot: Course Booking $pay_type Payment Successful";
        $subject = "Flanknot: Course Booking $pay_type Payment Successful";
        $data = $array->toArray();
        $data['seafarer'] = $seafarer_details->toArray();
        //dd($data['order_details']);
        return $this->sendEmail($blade_file, $data, $data['seafarer'][0], $title, $subject, null);
    }

    public function sendEmailToCompany($blade_file,$data, $array, $title, $subject, $type = null)
    {
        $data['host'] = 'Course4Sea.com';
        $data['home'] = route('home');
        
        if(isset($array['first_name']) && !empty($array['first_name'])) {
            $name = $array['first_name'];
        } else {
            $name = 'Hello';
        }
        
        try {
            Mail::send($blade_file,['template' => $data,'data'=>$data], function ($message) use ($data, $array, $type, $title, $subject, $name) 
            {   
                
                $jobData = \App\CourseJob::with(['companyEmailAddress'])->where('id',$data['course_job_id'])->first();
                // dd($array['email']);
                $message->from($data['email'], $title);
                //$m->to('sachin@yopmail.com', $name)->subject($subject);
                $message->to($jobData->companyEmailAddress->email,$name)->subject($subject);

                if(isset($data['send_pdf']) && !empty($data['send_pdf'])){
                    $message->attach($data['file_path'], array(
                        'mime' => 'application/pdf')
                    );
                    if(isset($array['company_email']) && !empty($array['company_email'])){
                        $message->cc($array['email']);
                    }
                }
            });
            
            $status_arr = ['status' => 'success'];
            return $status_arr;


        } catch(Exception $e) {
            $status_arr = ['status' => 'failed', 'message' => $e->getMessage()];
            return $status_arr;
        }

    }    

    public function sendEmail($blade_file,$data, $array, $title, $subject, $type = null)
    {
        $data['host'] = 'smtpout.secureserver.net';
        $data['home'] = route('home');
        
        if(isset($array['first_name']) && !empty($array['first_name'])) {
            $name = $array['first_name'];
        } else {
            $name = 'Hello';
        }
        
        try {
            Mail::send($blade_file,['template' => $data,'data'=>$data], function ($message) use ($data, $array, $type, $title, $subject, $name) 
            {
                // dd($array['email']);
                $message->from('donot-replay@flankknot.com', $title);
                //$m->to('sachin@yopmail.com', $name)->subject($subject);
                $message->to($array['email'], $name)->subject($subject);

                if(isset($data['send_pdf']) && !empty($data['send_pdf'])){
                    $message->attach($data['file_path'], array(
                        'mime' => 'application/pdf')
                    );
                    if(isset($array['company_email']) && !empty($array['company_email'])){
                        $message->cc($array['email']);
                    }
                }
            });

            $status_arr = ['status' => 'success'];
            return $status_arr;


        } catch(Exception $e) {
            $status_arr = ['status' => 'failed', 'message' => $e->getMessage()];
            return $status_arr;
        }

    }

    public function sendScheduleEmail($emailSubArr, $recipientArr,$subject = null) 
    {
        if (!empty($subject)) {
            $subject = $subject; 
        } else {
            $subject = 'ConsultanSeas: New message'; 
        }
        $template = view('emails.scheduled_email')->render();
        return $this->sendBulkEmail($emailSubArr, $recipientArr, $template, $subject);
    }

    public function sendSubscriptionExpiryEmailService($recipientArr,$subject) 
    {
        $emailSubArr = [];
        $template = view('emails.subscriptionExpiryTemplate')->render();
        return $this->sendBulkEmail($emailSubArr, $recipientArr, $template, $subject);
    }

    public function sendBulkEmail($emailSubArr, $recipientArr, $template, $subject)
    { 
        $options = ['key' => 'e6261be74908c700d6ffd0223ce414d6ffc6fb79'];
        $httpClient = new GuzzleAdapter(new Client());
        $sparky = new SparkPost($httpClient, $options);

        $promise = $sparky->transmissions->post([
            'content' => [
                'from' => [
                    'name' => 'Team ConsultanSeas',
                    'email' => 'no-reply@Course4Sea.com',
                ],
                'subject' => $subject,
                'html' => $template
            ],
            'recipients' => $recipientArr,
        ]);
        $result = [];
        try {
            $response = $promise->wait();
            $result['status'] = $response->getStatusCode();
            $result['message'] = $response->getBody();
            return $result;
            
        } catch (\Exception $e) {
            $result['status'] = $e->getCode();
            $result['message'] = $e->getMessage();
            return $result;
        }
    }

    public function sendRequestedDocumentListEmailToSeafarer($array){
        $array['link'] = route('user.profile',"#user_document_request");

        $blade_file = 'emails.seafarerRequestedDocumentList';
        $title = 'Flanknot: Permission for requested documents';
        $subject = 'Flanknot: Permission for requested documents';
        $message = 'Please click on "Accept" button to give the permission or "Reject" to deny the permission to see the documents';
        $data = $array;

        $this->sendEmail($blade_file, $data, $array, $title, $subject, null);
    }

    public function sendRequestedDocumentStatusEmailToCompany($array){
        if($array['registered_as'] == 'institute'){
            $array['link'] = route('site.institute.list.permission.requests');
        }
        else{
            $array['link'] = route('site.company.list.permission.requests');
        }
        $blade_file = 'emails.companyRequestedDocumentListStatus';
        $title = 'Flanknot: Requested Documents Status';
        $subject = 'Flanknot: Requested Documents Status';
        $data = $array;

        $this->sendEmail($blade_file, $data, $array, $title, $subject, null);
    }

}