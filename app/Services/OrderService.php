<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 5/2/2017
 * Time: 3:15 PM
 */

namespace App\Services;
use App\Repositories\OrderRepository;
use App\Services\CartProductService;
use App\Services\SeafarerSubscriptionService;
use App\Services\SubscriptionService;
use App\Services\OrderPaymentService;
use Auth;
use View;

class OrderService
{
    private $OrderRepository;
    private $cartService;
    private $subscriptionService;
    private $seafarerSubscriptionService;
    private $orderPaymentRepository;
    private $orderPaymentService;

    function __construct()
    {
        $this->OrderRepository = new OrderRepository();
        $this->cartService = new CartProductService();
        $this->subscriptionService = new SubscriptionService();
        $this->seafarerSubscriptionService = new SeafarerSubscriptionService();
        $this->orderPaymentService = new OrderPaymentService();
    }

    public function proceedToPayment($data){
        $order_data = [];
        $order_total_amount = 0;

        // $cart_data = $this->cartService->getCartDetailByUserId($data['user_id'])->toArray();
        $subscription = $this->subscriptionService->getAllSubscriptionDetailsBySubscriptionId($data['subscription_id'])->toArray();

        $order_data['user_id'] = $data['user_id'];
        $order_data['subscription_id'] = $data['subscription_id'];
        $order_data['subscription_cost'] = $subscription[0]['amount'];
        $order_data['tax'] = json_encode(['cgst'=>env('CGST_PERCENT'),'sgst'=>env('SGST_PERCENT')]);
        $order_data['tax_amount'] = round((($subscription[0]['amount']*env('CGST_PERCENT'))/100)+(($subscription[0]['amount']*env('SGST_PERCENT'))/100),2);
        $order_data['total'] = round((($subscription[0]['amount']*env('CGST_PERCENT'))/100)+(($subscription[0]['amount']*env('SGST_PERCENT'))/100)+($subscription[0]['amount']*1),2);
        $order_data['status'] = 0;

        // foreach ($cart_data as $c_index => $cart){
        //     $total_subscriptions = $this->subscriptionService->getAllSubscriptionDetailsBySubscriptionId($cart['subscription_id'])->toArray();
        //     $order_data['total'] += $total_subscriptions[0]['amount'];

        //     $subscriptions[$c_index]['user_id'] = $data['user_id'];
        //     $subscriptions[$c_index]['subscription_id'] = $cart['subscription_id'];
        //     $subscriptions[$c_index]['subscription_details'] = json_encode($total_subscriptions[0]);
        //     $subscriptions[$c_index]['duration'] = $total_subscriptions[0]['duration'];
        // }

        $order = $this->OrderRepository->store($order_data);

        if(count($order) > 0){
            // $subscriptions[0]['order_id'] = $order['id'];
            // $this->subscriptionService->store($subscriptions);
            $parameters = $this->payUMoneyParameters($order,$data['subscription_date']);

            $payumoney_form = View::make('site.checkout.payumoney_form',['parameters' => $parameters]);
            $payumoney_form = $payumoney_form->render();
            return ['status' => 'success' , 'payumoney_form' => $payumoney_form];
        }else{
            return ['status' => 'failed'];
        }    
    }

    public function payUMoneyParameters($data,$subscription_date){

        $currentuser = Auth::user()->toArray();
        $order_details = $data->toArray();

        $parameters['payumoney_url'] = $_ENV['PAYUMONEY_URL'];
        $parameters['key'] = $_ENV['PAYUMONEY_KEY'];
        $parameters['salt'] = $_ENV['PAYUMONEY_SALT'];
        $parameters['txnid'] = 'CONS'.time().$data['id'];
        $parameters['amount'] = $data['total'];
        $parameters['productinfo'] = 'Testing for payumoney';
        $parameters['firstname'] = $currentuser['first_name'];
        $parameters['email'] = $currentuser['email'];
        $parameters['phone'] = $currentuser['mobile'];
        $parameters['surl'] = route('site.payment.response',['order_id'=>$data['id'], 'subscription_date'=>$subscription_date]);
        $parameters['furl'] = route('site.payment.response',['order_id'=>$data['id'], 'subscription_date'=>$subscription_date]);

        $hash_data = $parameters['key']."|".$parameters['txnid']."|".$parameters['amount']."|".$parameters['productinfo']."|".$parameters['firstname']."|".$parameters['email']."|".$parameters['phone']."|".$parameters['surl']."|".$parameters['furl']."||||||||".$parameters['salt'];

        $parameters['hash'] = (hash('sha512', $hash_data));
        $parameters['service_provider'] = 'payu_paisa';
        
        return $parameters;
    }

    public function getOrderDetailById($order_id,$options=null){
        return $this->OrderRepository->getOrderDetailById($order_id,$options);
    }

    public function updateOrderStatus($update_status, $order_id){
        return $this->OrderRepository->updateOrderStatus($update_status, $order_id);
    }

    public function updateOrder($data, $order_id){
        return $this->OrderRepository->updateOrder($data, $order_id);
    }

    public function orderPaymentGateWayResponse($array,$order_id,$payment_type=NULL,$split_type=NULL) {
        $options['with'] = ['user'];
        $order_detail = $this->getOrderDetailById($order_id)->toArray();
        //$user_email = $order_detail[0]['user']['email'];
        //$user_mobile = $order_detail[0]['user']['mobile'];

        $retHashSeq = $array['salt'].'|'.$array['status'].'|||||||||||'.$array['email'].'|'.$array['firstname'].'|'.$array['productinfo'].'|'.$array['amount'].'|'.$array['txnid'].'|'.$array['key'];

        $hash = hash("sha512", $retHashSeq);

        $update_status = 0;
        if ($hash != $array['posted_hash']) {

            /*$update_status = 2;
            $this->updateProductOrderStatus($update_status, $order_id);

            $update_order_item['status'] = \CommonHelper::ORD_FAILED;
            $update_order_item['changed_by_user_id'] = $order_detail[0]['user_id'];
            $update_order_item['message'] = 'Order purchase failed';
            $this->ProductSubOrderItemService->updateStatusByProductOrderId($update_order_item, $order_id);*/

        } else {

            $store_arr['user_id'] = $order_detail[0]['user_id'];
            $store_arr['txnid'] = $array['txnid'];
            $store_arr['transaction_id'] = $array['transaction_id'];
            $store_arr['bank_ref_num'] = $array['bank_ref_num'];
            $store_arr['amount'] = $array['amount'];
            $store_arr['mode'] = !empty($array['mode']) ? $array['mode'] : '';
            $store_arr['order_id'] = $order_id;

            if($array['status'] == 'success') {
                if(isset($payment_type) && $payment_type == 'split'){
                    $update_status = 6;
                    if($split_type == '2'){
                        $update_status = 1;
                    }
                }else{
                    $update_status = 1;
                }
                
                $store_arr['status'] = $update_status;

                $payment_order['payment_type'] = !empty($payment_type) ? $payment_type : '';
                $this->updateOrder($payment_order, $order_id);

                $this->updateOrderStatus($update_status, $order_id);
            } else {
                
                $update_status = 2;
                $this->updateOrderStatus($update_status, $order_id);
                $update_order_item['changed_by_user_id'] = $order_detail[0]['user_id'];

                $store_arr['status'] = $update_status;
                $temp_message = json_encode(['error_code' => $array['error'], 'message' => $array['message']]);
                $store_arr['message'] = $temp_message;
            }
                
            $result = $this->orderPaymentService->store($store_arr)->toArray();
            // if($result){
            //     $this->cartService->deleteByUserId($result['user_id']);
            // }
        }
        return $order_id;
    }

    public function getOrderInfo($order_id){

        $options['with'] = ['order_payment','company_subscription','institute_subscription','advertiser_subscription','seafarer_subscription','subscription'];

        $order_details = $this->getOrderDetailById($order_id,$options)->toArray();
        
        if($order_details[0]['order_payment']['message']){
            $order_details[0]['order_payment']['message'] = json_decode($order_details[0]['order_payment']['message'], true);
        }
        
        if ($order_details[0]['company_subscription']['subscription_details']) {
            $order_details[0]['company_subscription']['subscription_details'] = json_decode($order_details[0]['company_subscription']['subscription_details'],true);
            $order_details[0]['my_subscription'] = $order_details[0]['company_subscription'];
        } elseif ($order_details[0]['institute_subscription']['subscription_details']) {
            $order_details[0]['institute_subscription']['subscription_details'] = json_decode($order_details[0]['institute_subscription']['subscription_details'],true);
            $order_details[0]['my_subscription'] = $order_details[0]['institute_subscription'];
        } elseif ($order_details[0]['advertiser_subscription']['subscription_details']) {
            $order_details[0]['advertiser_subscription']['subscription_details'] = json_decode($order_details[0]['advertiser_subscription']['subscription_details'],true);
            $order_details[0]['my_subscription'] = $order_details[0]['advertiser_subscription'];
        } elseif ($order_details[0]['seafarer_subscription']['subscription_details']) {
            $order_details[0]['seafarer_subscription']['subscription_details'] = json_decode($order_details[0]['seafarer_subscription']['subscription_details'],true);
            $order_details[0]['my_subscription'] = $order_details[0]['seafarer_subscription'];
        }

        return $order_details;
    }

    public function storeOrder($order_data){
        return $this->OrderRepository->store($order_data);
    }

    public function checkIfAppliedToBatch($batch_id,$user_id){
        return $this->OrderRepository->checkIfAppliedToBatch($batch_id,$user_id)->toArray();
    }

    public function getOrderByInstituteBatches($batches_array,$paginate = NULL,$filter = NULL){
        return $this->OrderRepository->getOrderByInstituteBatches($batches_array,$paginate,$filter);
    }

    public function changeInstituteBatchOrderStatus($order_id,$batch_id,$status){
        return $this->OrderRepository->changeInstituteBatchOrderStatus($order_id,$batch_id,$status);
    }

    public function payUMoneyInstituteBatchParameters($data){

        $currentuser = Auth::user()->toArray();
        $order_details = $data;
        
        $parameters['payumoney_url'] = $_ENV['PAYUMONEY_URL'];
        $parameters['key'] = $_ENV['PAYUMONEY_KEY'];
        $parameters['salt'] = $_ENV['PAYUMONEY_SALT'];
        $parameters['txnid'] = 'CONS'.time().$data['order_id'];
        $parameters['amount'] = $data['total'];
        $parameters['productinfo'] = 'Testing for payumoney';
        $parameters['firstname'] = $currentuser['first_name'];
        $parameters['email'] = $currentuser['email'];
        $parameters['phone'] = $currentuser['mobile'];
        $parameters['surl'] = route('site.institute.batch.payment.response',['order_id' => $data['order_id'],'payment_type' => $data['payment_type'],'split_type' => isset($data['split_payment']) && !empty($data['split_payment']) ? $data['split_payment'] : '']);
        $parameters['furl'] = route('site.institute.batch.payment.response',['order_id' => $data['order_id'],'payment_type' => $data['payment_type'],'split_type' => isset($data['split_payment']) && !empty($data['split_payment']) ? $data['split_payment'] : '']);

        $hash_data = $parameters['key']."|".$parameters['txnid']."|".$parameters['amount']."|".$parameters['productinfo']."|".$parameters['firstname']."|".$parameters['email']."|||||||||||".$parameters['salt'];

        $parameters['hash'] = (hash('sha512', $hash_data));
        $parameters['service_provider'] = 'payu_paisa';

        $payumoney_form = View::make('site.checkout.payumoney_form',['parameters' => $parameters]);
        $payumoney_form = $payumoney_form->render();
        return ['status' => 'success' , 'payumoney_form' => $payumoney_form];
    }

    public function cancelOrder($order_id,$user_id,$batch_id){
        return $this->OrderRepository->cancelOrder($order_id,$user_id,$batch_id);
    }

    public function getOrderDetailByUserId($user_id,$options=null){
        return $this->OrderRepository->getOrderDetailByUserId($user_id,$options);
    }

    public function getOrderDetailByStatus($status,$options=null){
        return $this->OrderRepository->getOrderDetailByStatus($status,$options);
    }

    public function updateOrderByBatchIdAndPreffType($batch_id,$preffered_type,$data){
        return $this->OrderRepository->updateOrderByBatchIdAndPreffType($batch_id,$preffered_type,$data);
    }

}