<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 4/29/2017
 * Time: 12:12 PM
 */

namespace App\Services;
use App\Repositories\SubscriptionRepository;
use App\Repositories\UserSubscriptionRepository;
use Carbon\Carbon;


class SubscriptionService
{
    private $subscriptionRepository;
    private $userSubscriptionRepository;

    public function __construct()
    {
        $this->subscriptionRepository = new SubscriptionRepository();
        $this->userSubscriptionRepository = new UserSubscriptionRepository();
    }

    public function getAllSubscriptionDetails($role = NULL)
    {
        return $this->subscriptionRepository->getAllSubscriptionDetails($role);
    }

    public function getAllSubscriptionDetailsBySubscriptionId($subscription_id)
    {
        return $this->subscriptionRepository->getAllSubscriptionDetailsBySubscriptionId($subscription_id);
    }

    public function getSubscriptionsByUserId($user_id)
    {
        return $this->userSubscriptionRepository->getSubscriptionsByUserId($user_id);
    }

    public function storeSubscription($data){
        return $this->userSubscriptionRepository->store($data);
    }

    public function getFreeSubscriptionDetailsByUserRole($role){
        return $this->subscriptionRepository->getFreeSubscriptionDetailsByUserRole($role); 
    }

    public function store($subscription_details)
    {
        $order_id = $subscription_details[0]['order_id'];
        foreach($subscription_details as $subscription_data){

            $order_subscriptions['user_id'] = $subscription_data['user_id'];
            $order_subscriptions['order_id'] = $order_id;
            $order_subscriptions['subscription_id'] = $subscription_data['subscription_id'];
            $order_subscriptions['subscription_details'] = $subscription_data['subscription_details'];
            $order_subscriptions['duration'] = $subscription_data['duration'];
            $order_subscriptions['valid_from'] = NULL;
            $order_subscriptions['valid_to'] = NULL;
            $order_subscriptions['status'] = 0;
            
                /*$order_seafarer_subscriptions['valid_from'] = date('Y/m/d');
                $order_seafarer_subscriptions['valid_to'] = date('Y/m/d', strtotime('+'.$subscription_data['duration'].' months'));
                $order_seafarer_subscriptions['status'] = 1;*/
            
            return $this->userSubscriptionRepository->store($order_subscriptions);

        }

        return true;
    }

    public function getSubscriptionsByUserIdWithActiveSubscription($user_id){
        return $this->userSubscriptionRepository->getSubscriptionsByUserIdWithActiveSubscription($user_id);
    }

    public function updateSubscriptionByOrderId($data,$order_id){
        return $this->userSubscriptionRepository->updateSubscriptionByOrderId($data,$order_id);
    }

    public function deleteSubscriptionById($id){
        return $this->userSubscriptionRepository->deleteSubscriptionById($id);
    }

    public function getAllActiveSubscription($role = Null){
        return $this->subscriptionRepository->getAllActiveSubscription($role); 
    }
    

    public function storeFreeSubscription($role,$id){
        $data = $this->getFreeSubscriptionDetailsByUserRole($role)->toArray();
        if($data){
            $subscription_data['user_id'] = $id;
            $subscription_data['subscription_id'] = $data['id'];
            $subscription_data['status'] = '1';

            $subscribed_plan_details = $this->getAllSubscriptionDetailsBySubscriptionId($data['id']);

            if($subscribed_plan_details)
                $subscribed_plan = $subscribed_plan_details->toArray();

            $subscription_data['duration'] = $subscribed_plan[0]['duration'];
            $subscription_data['valid_from'] = date('Y-m-d');
            $subscription_data['valid_to'] = date('Y-m-d', strtotime('+'.$subscribed_plan[0]['duration'].' months'));

            $subscribed_data = $this->storeSubscription($subscription_data);
            return $subscribed_data;
        }
    }

    public function saveEditAnySubscription($array,$role)
    {
        if ($role == 'company') {
            return $this->subscriptionRepository->saveEditCompanySubscription($array); 
        } elseif ($role == 'institute') {
           return $this->subscriptionRepository->saveEditInstituteSubscription($array);
        } elseif ($role == 'advertiser') {
           return $this->subscriptionRepository->saveEditAdvertiserSubscription($array);
        } elseif ($role == 'seafarer') {
           return $this->subscriptionRepository->saveEditSeafarerSubscription($array);
        }
    }

    public function getCompanySubscriptionByRegistrationId($id){
        return $this->subscriptionRepository->getCompanySubscriptionByRegistrationId($id); 
    }

    public function getCompanySubscriptionByRegistrationIdWithPagination($id){
        return $this->subscriptionRepository->getCompanySubscriptionByRegistrationIdWithPagination($id); 
    }

    public function getInstituteSubscriptionByRegistrationId($id){
        return $this->subscriptionRepository->getInstituteSubscriptionByRegistrationId($id); 
    }

    public function getInstituteSubscriptionByRegistrationIdWithPagination($id){
        return $this->subscriptionRepository->getInstituteSubscriptionByRegistrationIdWithPagination($id); 
    }

    public function getAdvertiserSubscriptionByRegistrationId($id){
        return $this->subscriptionRepository->getAdvertiserSubscriptionByRegistrationId($id); 
    }

    public function getAdvertiserSubscriptionByRegistrationIdWithPagination($id){
        return $this->subscriptionRepository->getAdvertiserSubscriptionByRegistrationIdWithPagination($id); 
    }

    public function getSeafarerSubscriptionByRegistrationId($id){
        return $this->subscriptionRepository->getSeafarerSubscriptionByRegistrationId($id); 
    }
    
    public function getSeafarerSubscriptionByRegistrationIdWithPagination($id){
        return $this->subscriptionRepository->getSeafarerSubscriptionByRegistrationIdWithPagination($id); 
    }

    public function getCompanyUsersWhosSubscriptionExpires($day){
         return $this->subscriptionRepository->getCompanyUsersWhosSubscriptionExpires($day);
    }
    
    public function getInstituteUsersWhosSubscriptionExpires($day){
         return $this->subscriptionRepository->getInstituteUsersWhosSubscriptionExpires($day);
    }

    public function getAdvertiserUsersWhosSubscriptionExpires($day){
         return $this->subscriptionRepository->getAdvertiserUsersWhosSubscriptionExpires($day);
    }

    // public function getSeafarerUsersWhosSubscriptionExpires($day){
    //      return $this->subscriptionRepository->getSeafarerUsersWhosSubscriptionExpires($day);
    // }

}