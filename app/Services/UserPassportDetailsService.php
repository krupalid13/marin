<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 4/21/2017
 * Time: 3:43 PM
 */

namespace App\Services;
use App\Repositories\UserPassportDetailsRepository;
use Auth;

class UserPassportDetailsService
{
    private $userPassportDetailsRepository;

    public function __construct()
    {
        $this->userPassportDetailsRepository = new UserPassportDetailsRepository();
    }

    public function updateByUserId($data, $user_id) {
        $this->userPassportDetailsRepository->updateByUserId($data, $user_id);
    }

    public function store($data)
    {
        $detail = $this->getDetailsByUserId($data['user_id'])->toArray();
        if( count($detail) > 0 ) {
            $this->updateByUserId($data, $data['user_id']);
        } else {
            $this->userPassportDetailsRepository->store($data);
        }
    }

    public function getDetailsByUserId($user_id)
    {
        return $this->userPassportDetailsRepository->getUserPassportDetailsById($user_id);
    }

}