<?php

namespace App\Services;
use App\Repositories\BatchRepository;
use Auth;

class BatchService
{
    private $batchRepository;

    function __construct()
    {
        $this->batchRepository = New BatchRepository();
    }

    public function store($data){
        if(isset($data['batch_id']) && !empty($data['batch_id'])){
            $this->batchRepository->deleteInstituteLocation($data['batch_id'],$data['institute_course_batch_id']);
        }
        
        if(isset($data['start_date'])){

            foreach ($data['start_date'] as $i => $value) {
                // $batch_details['institute_course_id'] = $data['course_name']; 
                $batch_details['start_date'] = date('Y-m-d',strtotime($data['start_date'][$i])); 
                $batch_details['duration'] = $data['duration'][$i]; 
                $batch_details['cost'] = $data['cost'][$i]; 
                $batch_details['size'] = $data['size'][$i];
                $batch_details['reserved_seats'] = $data['reserved_seats'][$i];

                $batch_details['available_size'] = ($data['size'][$i] - $data['reserved_seats'][$i]);

                $batch_details['payment_type'] = $data['payment_type'][$i];
                $batch_details['batch_type'] = $data['batch_type'][$i];

                if($batch_details['payment_type'] == 'split'){
                    $batch_details['initial_per'] = $data['initial_per'][$i];
                    $batch_details['initial_amt'] = $data['initial_amt'][$i];
                    $batch_details['pending_per'] = $data['pending_per'][$i];
                    $batch_details['pending_amt'] = $data['pending_amt'][$i];
                }else{
                    $batch_details['initial_per'] = NULL;
                    $batch_details['initial_amt'] = NULL;
                    $batch_details['pending_per'] = NULL;
                    $batch_details['pending_amt'] = NULL;
                }

                if($batch_details['batch_type'] == 'on_demand'){
                    $batch_details['on_demand_batch_type'] = $data['batch_type_on_demand'][$i];
                    
                }else{
                    $batch_details['on_demand_batch_type'] = NULL;
                    $batch_details['course_start_date'] = date('Y-m-d',strtotime($data['start_date'][$i]));
                }
                
                $batch_details['institute_id'] = $data['institute_id'];

                if(isset($batch_details['action_type']) && $batch_details['action_type'] == 'UPDATE' && $batch_details['batch_type'] == 'on_demand')  {
                    $batch_details['course_start_date'] = date('Y-m-d',strtotime($data['start_date'][$i]));
                }
                
                $institute_id =  $data['institute_id'];
               
                $institute_batches_array =  \DB::table('institute_courses')
                        ->where('institute_id', $institute_id)
                        ->where('course_id', $data['course_name'])
                        ->where('course_type', $data['course_type'])
                        ->where('status', 1)
                        ->get()
                        ->toArray();

                if(!empty($institute_batches_array)) {
                    $batch_details['institute_course_id'] = $institute_batches_array[0]->id;
                }
                
                
                $batch_details['status'] = '1';
                
                if(isset($data['batch_id'][$i]) AND !empty($data['batch_id'][$i])){
                    $batch_details['batch_id'] = $data['batch_id'][$i];
                    $result = $this->batchRepository->update($batch_details);
                    $data1['id'] = $data['batch_id'][$i];
                }else{
                    $result = $this->batchRepository->store($batch_details);
                    $data1 = $result->toArray();
                }
               
                $location = $data['location'][$i];

                if(isset($data['batch_id'][$i]) AND !empty($data['batch_id'][$i])){
                    $this->batchRepository->deleteBatchLocationByBatchId($data['batch_id'][$i]);
                }

                foreach ($location as $key => $value) {
                    $location['batch_id'] = $data1['id'];
                    $location['location_id'] = $value;
                    $location['status'] = '1';
                    $this->batchRepository->storeBatchLocation($location);
                }
            }
        }
        
        if(isset($data['_token']) && !empty($data['_token'])){
            unset($data['_token']);
        }
    	
    	if($result){
            $status_array =['status'=>'success','message' => 'Batch has been added successfully.','redirect_url' => Route('site.institute.batch.listing')];
        }else{
            $status_array =['status'=>'error'];
        }
        return $status_array;
    }

    public function storeBatchLocation($location){
        return $this->batchRepository->storeBatchLocation($location);
    }

    public function getDataByBatchId($batch_id){
        return $this->batchRepository->getDataByBatchId($batch_id);
    }

    public function getBatchDetailsByUserId($user_id,$filter=Null){
    	return $this->batchRepository->getBatchDetailsByUserId($user_id,$filter);
    }

    public function disableBatchByBatchId($batch_id){
        $result = $this->batchRepository->disableBatchByBatchId($batch_id);
        if( count($result) > 0 ) {
            $status_arr = ['status' => 'success'];
        } else {
            $status_arr = ['status' => 'error'];
        }
        return $status_arr;
    }

    public function enableBatchByBatchId($batch_id){
        $result = $this->batchRepository->enableBatchByBatchId($batch_id);
        if( count($result) > 0 ) {
            $status_arr = ['status' => 'success'];
        } else {
            $status_arr = ['status' => 'error'];
        }
        return $status_arr;
    }

    public function getAllCourseBatchDetails($user_id = Null,$filter = Null){
        return $this->batchRepository->getAllCourseBatchDetails($user_id,$filter);
    }

    public function getAllBatches($options,$filter=Null){
        return $this->batchRepository->getAllBatches($options,$filter);
    }

    public function getAllActiveCourseLocations(){
        return $this->batchRepository->getAllActiveCourseLocations();
    }

    public function getInstituteBatchedByInstituteCourseId($institute_course_id,$location_id){
        return $this->batchRepository->getInstituteBatchedByInstituteCourseId($institute_course_id,$location_id);
    }

    public function updateBatchDataByBatchId($batch_id,$data){
        return $this->batchRepository->updateBatchDataByBatchId($batch_id,$data);
    }
}