<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 4/29/2017
 * Time: 12:12 PM
 */

namespace App\Services;
use App\Repositories\CityRepository;

class CityService
{
    private $cityRepository;

    public function __construct()
    {
        $this->cityRepository = new CityRepository();
    }

    public function getAll()
    {
    	return $this->cityRepository->getAll();
    }


}