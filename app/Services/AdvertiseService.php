<?php

namespace App\Services;
use App\Repositories\AdvertiseRepository;
use App\Repositories\AdvertiseLocationRepository;
use App\Repositories\StateRepository;
use App\Repositories\CityRepository;
use App\Repositories\CompanyRepository;
use Auth;


class AdvertiseService
{
    private $advertiseRepository;
    private $companyRegistrationRepository;

    function __construct()
    {
        $this->advertiseRepository = New AdvertiseRepository();
        $this->advertiseLocationRepository = New AdvertiseLocationRepository();
        $this->companyRegistrationRepository = New CompanyRepository;
        $this->stateRepository = New StateRepository();
        $this->cityRepository = New CityRepository();
    }

    public function store($data){
        
        if(isset($data['company_id']) AND !empty($data['company_id'])){
            $id = $data['company_id'];
            $registration_data = $this->companyRegistrationRepository->getDetailsByCompanyId($id);
            $company_id = $registration_data[0]['id'];
        }
        else{
            $user_id = Auth::User()->id;
            $registration_data = $this->companyRegistrationRepository->getDetailsByCompanyId($user_id);
            $company_id = $registration_data[0]['id'];
        }
        // print_r($company_id);
        // exit;
        
        $path = env('ADVERTISE_PATH');
        $advertise_upload_dir = str_replace(["/","\\"], [DIRECTORY_SEPARATOR,DIRECTORY_SEPARATOR], $path);
        $destination_path = $advertise_upload_dir . $company_id;

        if(!is_dir($destination_path)) {
            mkdir($destination_path, 0777,true);
        }
        $filename_img_obj0 = '';
        $filename_img_obj1 = '';

        if(isset($data['img_obj0']) && !empty($data['img_obj0'])){
            $extension = '.' . $data['img_obj0']->getClientOriginalExtension();
            $destination_filename =  time().'-pic';
            $filename_img_obj0 = $destination_filename.$extension;

            $thumbnail = \PhpThumbFactory::create($data['img_obj0']);
            $thumbnail->resize('400', '400');
            $thumbnail->save($destination_path."/".$destination_filename.$extension);
        }

        if(isset($data['img_obj1']) && !empty($data['img_obj1'])){
            $extension = '.' . $data['img_obj1']->getClientOriginalExtension();
            $destination_filename =  time().'-pic';
            $filename = $destination_filename.$extension;

            $thumbnail = \PhpThumbFactory::create($data['img_obj1']);
            $thumbnail->save($destination_path."/".$destination_filename.$extension);
        }

        $store['company_id'] = $company_id;
        $store['from_date'] = date('Y-m-d',strtotime($data['adv_from_date']));
        $store['to_date'] = date('Y-m-d',strtotime($data['adv_to_date']));
        $store['img_path'] = $filename_img_obj0;
        $store['full_img'] = $filename_img_obj0;
        $adv_result = $this->advertiseRepository->store($store);

        $states = json_decode($data['states'],true);
        if(isset($states) && !empty($states) && (count($adv_result) > 0)){
            foreach ($states as $key => $state_value) {
                foreach ($state_value['cities'] as $index => $value) {
                    $location['advertise_id'] = $adv_result->id;
                    $location['state_id'] = $state_value['id'];
                    $location['city_id'] = $state_value['cities'][$index];
                    $result = $this->advertiseLocationRepository->store($location);
                }
            }
        }

        if(count($adv_result) > 0){
            $status_array = ['status' => 'success'];
        }else{
            $status_array = ['status' => 'failed']; 
        }

        return $status_array;
    }

    public function get_advertisements($limit,$state,$city,$type=NULL){

        if($type == 'IP'){
           /* if($state)
                $state = $this->stateRepository->getIdByStateName($state)->toArray();

            if($city)
                $city = $this->cityRepository->getIdByCityName($city)->toArray();*/

            
            $state_id = '';
            $city_id = '';
            if(isset($state) && !empty($state)){
                $state_id = $state['id'];
            }

            if(isset($city) && !empty($city)){
                $city_id = $city['id'];
            }
        }else{
            $state_id = $state;
            $city_id = $city;
        }
        
        $result =  $this->advertiseLocationRepository->get_advertisements($limit,$state_id,$city_id);
        
        return $result;
    }

    public function storeEnquiry($data){
        $result = $this->advertiseRepository->storeEnquiry($data);

        if(count($result) > 0){
            $status_array = ['status' => 'success'];
        }else{
            $status_array = ['status' => 'failed']; 
        }

        return $status_array;
    }

    public function getAdvertiseEnquiryByCompanyId($company_id){
        return $this->advertiseRepository->getAdvertiseEnquiryByCompanyId($company_id);
    }

    public function getAllAdvertiseEnquiries($paginate){
        return $this->advertiseRepository->getAllAdvertiseEnquiries($paginate);
    }
    public function getAllAdvertisementCompanyDetails(){
        return $this->advertiseRepository->getAllAdvertisementCompanyDetails();
    }

    public function getAllAdvertisementDetails($paginate=NULL,$filter=NULL){
        return $this->advertiseRepository->getAllAdvertisementsDetails($paginate,$filter);
    }

    public function getAdvertisementsById($id){
        return $this->advertiseRepository->getAdvertisementsById($id);
    }

    public function changeStatus($data){
        if(isset($data['status']) && isset($data['id'])){
            $result = $this->advertiseRepository->changeStatus($data['status'],$data['id']);
        }

        if($result){
            $status_array =['status'=>'success'];
        }
        else{
            $status_array =['status'=>'failed'];
        }
        return $status_array;
    }

    public function getAdvertisementCountByStatus($company_id,$status){
        return $this->advertiseRepository->getAdvertisementCountByStatus($company_id,$status);
    }

    public function getAllAdvertisementsWithActiveAdvertisements(){
        return $this->advertiseRepository->getAllAdvertisementsWithActiveAdvertisements();
    }
}