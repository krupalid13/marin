<?php

namespace App\Services;
use App\Repositories\ScheduleRepository;
use Auth;

class ScheduleService
{
    private $scheduleRepository;

    function __construct()
    {
        $this->scheduleRepository = New ScheduleRepository();
    }

    public function store($data) {

        $store_arr = [];

        if (isset($data['schedule_type']) && !empty($data['schedule_type'])) {
            foreach ($data['schedule_type'] as $key => $value) {

                if (isset($data['user_type']) && !empty($data['user_type'])) {
                   $store_arr['role'] = $data['user_type'];
                }

                if (isset($data['country']) && !empty($data['country'])) {
                   $store_arr['country'] = $data['country'];
                }

                if (isset($data['states']) && !empty($data['states'])) {
                   $store_arr['states'] = json_encode($data['states']);
                }

                if (isset($data['cities']) && !empty($data['cities'])) {
                   $store_arr['cities'] = json_encode($data['cities']);
                }

                if ((isset($data['date']) && !empty($data['date'])) && (isset($data['time']) && !empty($data['time']))) {
                    $date_str = $data['date'].' '.$data['time'];
                    $date = date_create($date_str);
                    $store_arr['schedule_date_time'] = date_format($date,'Y-m-d H:i:s');
                }

               if ($value == 'email') {
                   $store_arr['type'] = 'email';
                   if (isset($data['msg_email']) && !empty($data['msg_email'])) {
                      $store_arr['message'] = $data['msg_email'];
                   }
               } elseif ($value == 'sms') {
                   $store_arr['type'] = 'sms';
                   if (isset($data['msg_sms']) && !empty($data['msg_sms'])) {
                      $store_arr['message'] = $data['msg_sms'];
                   }
               }

                if (isset($data['email_subject']) && !empty($data['email_subject'])) {
                   $store_arr['subject'] = $data['email_subject'];
                }

                $result = $this->scheduleRepository->store($store_arr);
            }
        }

        return true;

    }

    public function getAll($pagination_limit = null) {
      return $this->scheduleRepository->getAll($pagination_limit);
    }

    public function delete($id)
    {
      return $this->scheduleRepository->delete($id);
    }

    public function getScheduledEmailByCurrentDateTime($now, $next_five_minutes) {
      return $this->scheduleRepository->getScheduledEmailByCurrentDateTime($now, $next_five_minutes);
    }

    public function changeStatusTo($id, $new_status, $err_msg = null) {
      return $this->scheduleRepository->changeStatusTo($id, $new_status, $err_msg);
    }

  // public function getById($id)
  // {
  //   return $this->scheduleRepository->getById($id);
  // }

    

}