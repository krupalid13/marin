<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 4/21/2017
 * Time: 4:44 PM
 */

namespace App\Services;
use App\Repositories\UserCocDetailsRepository;

class UserCocDetailsService
{
    private $userCocDetailsRepository;

    public function __construct()
    {
        $this->userCocDetailsRepository = new UserCocDetailsRepository();
    }

    public function store($data)
    {
        return $this->userCocDetailsRepository->store($data);
        
    }

    public function getDetailsByUserId($user_id) {
        return $this->userCocDetailsRepository->getDetailsByUserId($user_id);
    }

    public function updateByUserId($data, $user_id) {
        $this->userCocDetailsRepository->updateByUserId($data, $user_id);
    }

     public function deleteServiceById($user_id){
        $this->userCocDetailsRepository->deleteServiceById($user_id);
    }
    
    public function updateByCocId($data, $cocId) {
        $this->userCocDetailsRepository->updateByCocId($data, $cocId);
    }
    
    public function deleteServiceByCocIds($cocIds) {
        $this->userCocDetailsRepository->deleteServiceByCocIds($cocIds);
    }
}