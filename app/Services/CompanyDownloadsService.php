<?php

namespace App\Services;
use App\Services\SubscriptionService;
use App\Services\SubscriptionFeatureService;
use App\Repositories\CompanyDownloadsRepository;
use Route;

class CompanyDownloadsService
{
    private $companyDownloadsRepository;
    private $subscriptionService;
    private $subscriptionFeatureService;

    public function __construct()
    {
        $this->companyDownloadsRepository = new CompanyDownloadsRepository();
        $this->subscriptionService = new SubscriptionService();
        $this->subscriptionFeatureService = new SubscriptionFeatureService();
    }

    public function checkCompanyResumeDownloads($request,$user_id,$candidate_id=null){

    	$subscription = $this->subscriptionService->getSubscriptionsByUserIdWithActiveSubscription($user_id)->toArray();
        
        if(isset($subscription) AND !empty($subscription)){
    	    $subscription_details = $this->subscriptionFeatureService->getResumeFeatureDetailsBySubscriptionId($subscription[0]['subscription_id']);
            
            if(isset($subscription_details) && !empty($subscription_details))
    		    $subscription_details = $subscription_details->toArray();

    		$resume_download_limit = $subscription_details['count'];
    	
	    	$count_resume_downloads_per_day = $this->companyDownloadsRepository->resumeDownloadCountPerDay($user_id,$subscription_details['id']);

	    	if($count_resume_downloads_per_day < $resume_download_limit){
                $data['company_id'] = $user_id;
                if(isset($candidate_id) && !empty($candidate_id))
	    		    $data['seafarer_id'] = $candidate_id;

		    	$data['url'] = $request->path();
		    	$data['subscription_feature_id'] = $subscription_details['id'];

	    		$status_array = ['status'=>'success','data' => $data];
	    	}else{
	    		$status_array = ['status'=>'failed','message' => 'Maximum resume download limit reached.'];
	    	}
	    }else{
	    	$status_array = ['status'=>'failed','message' => 'You are not subscribed to download resume feature.'];
	    }
	    return $status_array;
    }

    public function store($data){
    	return $this->companyDownloadsRepository->store($data);
    }

    public function getResumeDownloadList($company_id){
        return $result = $this->companyDownloadsRepository->getResumeDownloadList($company_id);
    }
}