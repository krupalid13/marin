<?php

namespace App\Services;
use App\Repositories\OrderPaymentRepository;

class OrderPaymentService{

	private $orderPaymentRepository;

	function __construct()
    {
        $this->orderPaymentRepository = new OrderPaymentRepository;
    }

	public function store($array){
        return $this->orderPaymentRepository->store($array);
    }   

}

