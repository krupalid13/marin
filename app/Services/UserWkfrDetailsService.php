<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 4/21/2017
 * Time: 4:45 PM
 */

namespace App\Services;
use App\Repositories\UserWkfrDetailsRepository;

class UserWkfrDetailsService
{
    private $userWkfrDetailsRepository;

    public function __construct()
    {
        $this->userWkfrDetailsRepository = new UserWkfrDetailsRepository();
    }

    public function store($data)
    {
        $detail = $this->getDetailsByUserId($data['user_id'])->toArray();
        if( count($detail) > 0 ) {
            $this->updateByUserId($data, $data['user_id']);
        } else {
            $this->userWkfrDetailsRepository->store($data);
        }
    }

    public function getDetailsByUserId($user_id) {
        return $this->userWkfrDetailsRepository->getDetailsByUserId($user_id);
    }

    public function updateByUserId($data, $user_id) {
        $this->userWkfrDetailsRepository->updateByUserId($data, $user_id);
    }
}