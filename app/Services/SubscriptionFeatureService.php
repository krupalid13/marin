<?php

namespace App\Services;
use App\Repositories\SubscriptionFeatureRepository;

class SubscriptionFeatureService
{
    private $subscriptionFeatureRepository;

    public function __construct()
    {
        $this->subscriptionFeatureRepository = new SubscriptionFeatureRepository();
    }

   public function getResumeFeatureDetailsBySubscriptionId($subscription_id)
    {
        return $this->subscriptionFeatureRepository->getResumeFeatureDetailsBySubscriptionId($subscription_id);
    }
}