<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 5/12/2017
 * Time: 1:07 PM
 */

namespace App\Services;
use App\Repositories\CompanyRepository;
use App\Repositories\UserRepository;
use App\Repositories\CompanyDetailsRepository;
use App\Repositories\CompanyShipDetailsRepository;
use App\Repositories\CompanyLocationDetailsRepository;
use App\Repositories\CompanyDocumentsRepository;
use App\Services\SendEmailService;
use App\Services\SendSmsService;
use App\Services\SubscriptionService;
use App\Repositories\AdvertiseRepository;
use App\Repositories\CompanyAgentsRepository;
use App\Repositories\CompanyTeamDetailsRepository;
use Auth;
use Route;

class CompanyService
{
    private $companyRegistrationRepository;
    private $userRepository;
    private $companyDetailsRepository;
    private $companyShipDetailsRepository;
    private $companyLocationDetailsRepository;
    private $companyDocumentsRepository;
    private $sendEmailService;
    private $subscriptionService;
    private $sendSmsService;
    private $advertiseRepository;
    private $companyAgentsRepository;
    private $companyTeamDetailsRepository;

    function __construct()
    {
        $this->companyRegistrationRepository = new CompanyRepository;
        $this->userRepository = new UserRepository;
        $this->companyDetailsRepository = new CompanyDetailsRepository;
        $this->companyShipDetailsRepository = new CompanyShipDetailsRepository;
        $this->companyLocationDetailsRepository = new CompanyLocationDetailsRepository;
        $this->companyDocumentsRepository = new CompanyDocumentsRepository;
        $this->sendEmailService = new SendEmailService();
        $this->subscriptionService = new SubscriptionService();
        $this->sendSmsService = new SendSmsService();
        $this->advertiseRepository = New AdvertiseRepository();
        $this->companyAgentsRepository = New CompanyAgentsRepository();
        $this->companyTeamDetailsRepository = New companyTeamDetailsRepository();
    }

    public function store_location_ship_details($data, $logged_company_data = NULL){

        if(Auth::check()){
            $company_list = [];

            //$this->companyLocationDetailsRepository->deleteRecordsByCompanyId($logged_company_data[0]['id']);
            foreach ($data['country'] as $index => $value){
                if(isset($data['existing_location_id'][$index]) && !empty($data['existing_location_id'][$index])){
                    $company_list[] = $data['existing_location_id'][$index];
                }
            }

            if(!empty($company_list))
                $this->companyLocationDetailsRepository->deleteRecordsByCompanyIdWhereIdNotExist($company_list,$logged_company_data[0]['id']);

            foreach ($data['country'] as $index => $value){
                $company_location_details['existing_location_id'] = '';
                $company_location_details['company_id'] = $logged_company_data[0]['id'];
                $company_location_details['country'] = $data['country'][$index];
                $company_location_details['pincode_text'] = $data['pincode'][$index];
                $company_location_details['address'] = $data['address'][$index];

                if(isset($data['is_headoffice'][$index]) && !empty($data['is_headoffice'][$index]))
                    $company_location_details['is_headoffice'] = $data['is_headoffice'][$index];
                else
                    $company_location_details['is_headoffice'] = '0'    ;

                if(isset($data['pincode_id'][$index]) && !empty($data['pincode_id'][$index]))
                    $company_location_details['pincode_id'] = $data['pincode_id'][$index];

                if(isset($data['telephone'][$index]) && !empty($data['telephone'][$index]))
                    $company_location_details['telephone'] = $data['telephone'][$index];

                if(isset($data['existing_location_id'][$index]) && !empty($data['existing_location_id'][$index]))
                    $company_location_details['existing_location_id'] = $data['existing_location_id'][$index];

                if(isset($data['state'][$index]) && !empty($data['state'][$index])){
                    $company_location_details['state_id'] = $data['state'][$index];
                    $company_location_details['city_id'] = $data['city'][$index];
                    $company_location_details['state_text'] = NULL;
                    $company_location_details['city_text'] = NULL;
                }else{
                    $company_location_details['state_text'] = $data['state_text'][$index];
                    $company_location_details['city_text'] = $data['city_text'][$index];
                    $company_location_details['state_id'] = NULL;
                    $company_location_details['city_id'] = NULL;
                }
                $this->companyLocationDetailsRepository->store($company_location_details);
            }

            if(isset($data['pancard']) || isset( $data['rpsl_certificate']) || isset($data['incorporation_certificate']) || isset($data['incorporation_number']) || isset($data['pancard_number'])){
                 //document-upload
                $store_doc_arr['company_id'] = $logged_company_data[0]['id'];
                if(isset($data['pancard']) && !is_string($data['pancard']) && $data['pancard'] !== 'undefined' && $data['pancard']->isValid()){

                    $title = 'doc-pancard';
                    $file_extension = $data['pancard']->getClientOriginalExtension();

                    $directory = str_replace(["/","\\"], [DIRECTORY_SEPARATOR,DIRECTORY_SEPARATOR],env('COMPANY_DOC_PATH'));
                    $directory = $directory.$logged_company_data[0]['id'];
                    if(!is_dir($directory)){
                        mkdir($directory,0777,true);
                    }
                    $filename = $logged_company_data[0]['id'].'-'.rand(000,999).'-'.time().'-'.$title.'.'.$file_extension;
                    $store_doc_arr['pancard'] = $filename;

                    // $data['pancard']->move($directory,$filename);
                    $data['pancard']->move($directory,$filename);
                   
                }
            
                 //document-upload
                if(isset($data['incorporation_certificate']) && !is_string($data['incorporation_certificate']) && $data['incorporation_certificate'] !== 'undefined' && $data['incorporation_certificate']->isValid()){

                    $title = 'doc-incorporation-method';
                    $file_extension = $data['incorporation_certificate']->getClientOriginalExtension();

                    $directory = str_replace(["/","\\"], [DIRECTORY_SEPARATOR,DIRECTORY_SEPARATOR],env('COMPANY_DOC_PATH'));
                    $directory = $directory.$logged_company_data[0]['id'];
                    if(!is_dir($directory)){
                        mkdir($directory,0777,true);
                    }
                    $filename = $logged_company_data[0]['id'].'-'.rand(000,999).'-'.time().'-'.$title.'.'.$file_extension;
                    $store_doc_arr['incorporation_certificate'] = $filename;
                    $store_doc_arr['company_id'] = $logged_company_data[0]['id'];

                    // $data['pancard']->move($directory,$filename);
                    
                    $data['incorporation_certificate']->move($directory,$filename);
                }
            
                 //document-upload

                if(isset($data['rpsl_certificate']) && !is_string($data['rpsl_certificate']) && $data['rpsl_certificate'] !== 'undefined' && $data['rpsl_certificate']->isValid()){

                    $title = 'doc-rpsl_certificate';
                    $file_extension = $data['rpsl_certificate']->getClientOriginalExtension();

                    $directory = str_replace(["/","\\"], [DIRECTORY_SEPARATOR,DIRECTORY_SEPARATOR],env('COMPANY_DOC_PATH'));
                    $directory = $directory.$logged_company_data[0]['id'];
                    if(!is_dir($directory)){
                        mkdir($directory,0777,true);
                    }
                    $filename = $logged_company_data[0]['id'].'-'.rand(000,999).'-'.time().'-'.$title.'.'.$file_extension;
                    $store_doc_arr['rpsl_certificate'] = $filename;
                    $store_doc_arr['company_id'] = $logged_company_data[0]['id'];

                    // $data['pancard']->move($directory,$filename);
              
                    $data['rpsl_certificate']->move($directory,$filename);
                }
                
                if(isset($data['incorporation_number'])){
                    $store_doc_arr['incorporation_number'] = $data['incorporation_number'];
                }
                if(isset($data['pancard_number'])){
                    $store_doc_arr['pancard_number'] = $data['pancard_number'];
                }

                $result = $this->companyDocumentsRepository->store($store_doc_arr);
            }

            $status_array = ['status'=>'success'];
        }
        else{
            $status_array = ['status'=>'failed'];
        }
        return $status_array;

    }

    public function store_company_details($data,$company_id){

        $result = '';
        $company_registration_details['user_id'] = $company_id;
        $company_registration_details['company_name'] = $data['company_name'];
        $company_registration_details['bussiness_category'] = $data['bussiness_category'];
        $company_registration_details['bussiness_description'] = $data['bussiness_description'];
        $company_registration_details['b_i_member'] = $data['b_i_member'];
        $company_registration_details['address'] = $data['company_address'];
        $company_registration_details['contact_display'] = $data['contact_display'];
        $company_registration_details['email_display'] = $data['email_display'];
        $company_registration_details['product_description'] = $data['product_description'];
        $company_registration_details['buss_years'] = $data['buss_years'];
        $company_registration_details['type'] = 'advertiser';


        if(isset($data['website']) && !empty($data['website']))
            $company_registration_details['website'] = $data['website'];
        else
            $company_registration_details['website'] = NULL;

        if(isset($data['bussiness_category']) && !empty($data['bussiness_category']) && $data['bussiness_category'] == 'other')
            $company_registration_details['other_buss_category'] = $data['other_buss_category'];
        else
            $company_registration_details['other_buss_category'] = NULL;

        if(isset($data['b_i_member']) && !empty($data['b_i_member']) && $data['b_i_member'] == 'other')
            $company_registration_details['other_bi_member'] = $data['other_b_i_member'];
        else
            $company_registration_details['other_bi_member'] = NULL;

        if(isset($data['company_contact_number']) && !empty($data['company_contact_number']))
            $company_registration_details['contact_number'] = $data['company_contact_number'];
        else
            $company_registration_details['contact_number'] = NULL;
        
        if(isset($data['uploaded-file-name']) && !empty($data['uploaded-file-name'])){
            $user_data['company_logo'] = $data['uploaded-file-name'] ;

            $storage_path = env('COMPANY_LOGO_PATH');

            $from = str_replace(["/","\\"], [DIRECTORY_SEPARATOR,DIRECTORY_SEPARATOR], $storage_path.'temp/'.$data['uploaded-file-name']);

            if(file_exists($from)){
            
                $to = str_replace(["/","\\"], [DIRECTORY_SEPARATOR,DIRECTORY_SEPARATOR], $storage_path. $company_id).DIRECTORY_SEPARATOR;

                if(!is_dir($to)) {
                    mkdir($to, 0777,true);
                }
                $to = $to.$data['uploaded-file-name'];
                if(copy($from, $to)) {
                    unlink($from);
                }
                $this->companyRegistrationRepository->update($user_data,$company_id);
            }
        }

        
        $registration_data = $this->companyRegistrationRepository->getDetailsByCompanyId($company_id);

        if(!empty($registration_data->toArray())){
            $this->companyRegistrationRepository->update($company_registration_details,$company_id);

            $company_id = $registration_data[0]['id'];
            $result['redirect_url'] = route('site.advertiser.profile');
        }else{
            $company_registration_details['user_id'] = $company_id;
            $advertiser_obj = $this->companyRegistrationRepository->store($company_registration_details);

            $company_id = $advertiser_obj->id;
            // Add free subscription on company registration
            $advertiser_data = $this->subscriptionService->getAdvertiserSubscriptionByRegistrationId($advertiser_obj->id);
            if (count($advertiser_data) == 0) {
                $subscription = $this->subscriptionService->getAllActiveSubscription('advertiser')->toArray();
                foreach ($subscription as $key => $sub_val) {
                   if (strtolower($sub_val['duration_title']) == 'free') {
                        $free_sub_arr = [];
                        $free_sub_arr['company_reg_id'] = $advertiser_obj->id;
                        $free_sub_arr['subscription_details'] = json_encode($sub_val);
                        $free_sub_arr['status'] = 1;
                        $free_sub_arr['valid_from'] = date('Y-m-d 00:00:00');
                        $free_sub_arr['valid_to'] = date('Y-m-d 23:59:59', strtotime('+'.(($sub_val['duration'] * 30) -1 ).' days'));
                        $added_subscription = $this->subscriptionService->saveEditAnySubscription($free_sub_arr,'advertiser');
                   }
                }
            }

            $subscriptions = $this->subscriptionService->getAllActiveSubscription('advertiser');
            if (!empty($subscriptions)) {
                $result['redirect_url'] = route('site.user.subscription'); 
            } else {
                $result['redirect_url'] = route('home',['auto_action' => 'welcome']);
            }

             
        }

        $this->companyLocationDetailsRepository->deleteRecordsByCompanyId($company_id);
        
        $company_location_details['company_id'] = $company_id;
        $company_location_details['country'] = $data['country[0]'];
        $company_location_details['pincode_text'] = $data['pincode[0]'];
        $company_location_details['address'] = $data['company_address'];

        if(isset($data['pincode_id[0]']) && !empty($data['pincode_id[0]']))
            $company_location_details['pincode_id'] = $data['pincode_id[0]'];

        if(isset($data['state[0]']) && !empty($data['state[0]'])){
            $company_location_details['state_id'] = $data['state[0]'];
            $company_location_details['city_id'] = $data['city[0]'];
            $company_location_details['state_text'] = NULL;
            $company_location_details['city_text'] = NULL;
        }else{
            $company_location_details['state_text'] = $data['state_text[0]'];
            $company_location_details['city_text'] = $data['city_text[0]'];
            $company_location_details['state_id'] = NULL;
            $company_location_details['city_id'] = NULL;
        }
        
        $this->companyLocationDetailsRepository->store($company_location_details);
        return $result;
    }

    public function store($data, $logged_company_data = NULL){
        
        if(isset($data['contact_person']) && !empty($data['contact_person']))
            $login_details['first_name'] = $data['contact_person'];

        if(isset($data['email']) && !empty($data['email']))
            $login_details['email'] = $data['email'];

        if(isset($data['email']) && !empty($data['email']))
            $login_details['email'] = $data['email'];

        if(isset($data['password']) && !empty($data['password']))
            $login_details['password'] = bcrypt($data['password']);

        if(isset($data['added_by']) && !empty($data['added_by']))
            $login_details['added_by'] = $data['added_by'];

        if(isset($data['contact_person_number']) && !empty($data['contact_person_number']))
            $login_details['mobile'] = $data['contact_person_number'];

        if(isset($data['otp']) && !empty($data['otp']))
            $login_details['otp'] = $data['otp'];

        if(!isset($data['registered_as']))
            $login_details['registered_as'] = 'company';
        else
            $login_details['registered_as'] = $data['registered_as'];

        if(isset($logged_company_data) && !empty($logged_company_data)){
            $data['id'] = $logged_company_data['id'];
            $data['registered_as'] = $login_details['registered_as'];

            if(isset($data['added_by']) AND $data['added_by'] == 'admin'){

            }else{
                $login_details['welcome_email'] = 1;
                if(isset($data['contact_person_number']) && strlen($data['contact_person_number']) == 10){
                    $login_details['welcome_sms'] = 1;
                }
            }

            if(isset($logged_company_data['email']) && $logged_company_data['email'] != $data['email']){ 
                //if email changed from Profile
                $login_details['is_email_verified'] = 0;
                
                $this->sendEmailService->sendVerificationEmailToUser($data);
            }
            
            if(isset($logged_company_data['mobile']) && $logged_company_data['mobile'] != $data['contact_person_number']){ 
                //if mobile changed from Profile

                $login_details['is_mob_verified'] = 0;
                $user['update_mobile'] = 1;
                $this->sendSmsService->sendMobileVerifySms($login_details['mobile'],$login_details['otp']);
            }

            $this->userRepository->update($login_details,$logged_company_data['id']);
            $company_login_data['id'] = $logged_company_data['id'];

            // $active_subscriptions = $this->subscriptionService->getSubscriptionsByUserIdWithActiveSubscription($logged_company_data['id'])->toArray();

            // //default free subscription
            // if(count($active_subscriptions) == 0){
            //     $free_subscription = $this->subscriptionService->storeFreeSubscription('company',$logged_company_data['id']);
            // }
            
            if(isset($data['added_by']) AND $data['added_by'] == 'admin'){
            
            }else{
                if(isset($logged_company_data['welcome_email']) && $logged_company_data['welcome_email'] == 0){
                   
                    $this->sendEmailService->sendEmailToNewRegisteredUser($data);
                    $this->sendEmailService->sendVerificationEmailToUser($data);
                }

                if(isset($logged_company_data['welcome_sms']) && $logged_company_data['welcome_sms'] == 0){
                    $logged_company_data['mobile'] = $data['contact_person_number'];
                    if(isset($data['contact_person_number']) && strlen($data['contact_person_number']) == 10){
                        $this->sendSmsService->sendSmsToNewRegistered($data);
                        $this->sendSmsService->sendMobileVerifySms($logged_company_data['mobile'],$logged_company_data['otp']);
                    }
                }
            }
        }else{

            if(isset($data['added_by']) AND $data['added_by'] == 'admin'){
            
            }else{
                $login_details['welcome_email'] = 1;
                if(isset($data['contact_person_number']) && strlen($data['contact_person_number']) == 10){
                    $login_details['welcome_sms'] = 1;
                }
            }
            
            $company_login_data = $this->userRepository->store($login_details)->toArray();

            $company_login_data['mobile'] = $data['contact_person_number'];
            if(isset($data['otp']) && !empty($data['otp']))
                $company_login_data['otp'] = $data['otp'];
            
            if(isset($data['added_by']) && $data['added_by'] = 'admin'){

            }else{
                Auth::loginUsingID($company_login_data['id']);
            }

            //$free_subscription = $this->subscriptionService->storeFreeSubscription('company',$company_login_data['id']);
            
            if(isset($data['added_by']) AND $data['added_by'] == 'admin'){
            
            }else{
                $this->sendEmailService->sendEmailToNewRegisteredUser($company_login_data);
                $this->sendEmailService->sendVerificationEmailToUser($company_login_data);

                if(isset($data['contact_person_number']) && strlen($data['contact_person_number']) == 10){
                    $this->sendSmsService->sendSmsToNewRegistered($company_login_data);
                    $this->sendSmsService->sendMobileVerifySms($company_login_data['mobile'],$company_login_data['otp']);
                }
            }
        }

        if(isset($data['uploaded-file-name']) && !empty($data['uploaded-file-name'])){
            $user_data['profile_pic'] = $data['uploaded-file-name'] ;

            $storage_path = env('COMPANY_LOGO_PATH');

            $from = str_replace(["/","\\"], [DIRECTORY_SEPARATOR,DIRECTORY_SEPARATOR], $storage_path.'temp/'.$user_data['profile_pic']);
            
            $to = str_replace(["/","\\"], [DIRECTORY_SEPARATOR,DIRECTORY_SEPARATOR], $storage_path. $company_login_data['id']).DIRECTORY_SEPARATOR;

            if(!is_dir($to)) {
                mkdir($to, 0777,true);
            }
            $to = $to.$user_data['profile_pic'];
            if(is_file($from) && copy($from, $to)) {
                unlink($from);
            }
            $this->userRepository->update($user_data,$company_login_data['id']);
        }

        if(isset($company_login_data['id']) && !empty($company_login_data['id'])) {

            $company_registration_details['user_id'] = $company_login_data['id'];
            $company_registration_details['company_name'] = $data['company_name'];
            $company_registration_details['email'] = $data['email'];
            if(isset($data['password']) && !empty($data['password']))
                $company_registration_details['password'] = $data['password'];

            $company_registration_details['contact_person'] = $data['contact_person'];
            $company_registration_details['contact_number'] = $data['contact_person_number'];
            $company_registration_details['contact_email'] = $data['email'];
            $company_registration_details['website'] = $data['website'];

            $registration_data = $this->companyRegistrationRepository->getDetailsByCompanyId($company_login_data['id']);
            if(count($registration_data) > 0){
                $this->companyRegistrationRepository->update($company_registration_details , $company_login_data['id']);

                $company_registration_login_data['id'] = $registration_data[0]['id'];
            }else{
                $reg_obj = $this->companyRegistrationRepository->store($company_registration_details);
                
                $company_registration_login_data['id'] = $reg_obj->id;
                // Add free subscription on company registration
                $company_data = $this->subscriptionService->getCompanySubscriptionByRegistrationId($reg_obj->id);
                if (count($company_data) == 0) {
                    $subscription = $this->subscriptionService->getAllActiveSubscription('company')->toArray();
                    foreach ($subscription as $key => $sub_val) {
                       if (strtolower($sub_val['duration_title']) == 'free') {
                            $free_sub_arr = [];
                            $free_sub_arr['company_reg_id'] = $reg_obj->id;
                            $free_sub_arr['subscription_details'] = json_encode($sub_val);
                            $free_sub_arr['status'] = 1;
                            $free_sub_arr['valid_from'] = date('Y-m-d 00:00:00');
                            $free_sub_arr['valid_to'] = date('Y-m-d 23:59:59', strtotime('+'.(($sub_val['duration'] * 30) -1 ).' days'));
                            $added_subscription = $this->subscriptionService->saveEditAnySubscription($free_sub_arr,'company');
                       }
                    }
                }
            }

            $company_details['company_id'] = $company_registration_login_data['id'];
            $company_details['fax'] = $data['fax'];
            $company_details['company_type'] = $data['company_type'];
            $company_details['rpsl_no'] = '';
            if($data['company_type'] == 1){
                $company_details['rpsl_no'] = $data['rpsl_no'];
            }
            $company_details['company_description'] = $data['company_description'];
            $company_details['company_email'] = $data['company_email'];
            $company_details['company_contact_number'] = $data['company_contact_number'];
    

            $company_data = $this->companyDetailsRepository->getDetailsByCompanyId($company_registration_login_data['id']);
            if(count($company_data) > 0){
                $this->companyDetailsRepository->update($company_details , $company_registration_login_data['id']);
            }else{
                $this->companyDetailsRepository->store($company_details);
            }
            
            /*$company_details['country'] = $data['country'][0];
            $company_details['zip'] = $data['pincode'][0];

            if(isset($data['pincode_id'][0]) && !empty($data['pincode_id'][0]))
                $company_details['pincode_id'] = $data['pincode_id'][0];

            $india_value = array_search('India',\CommonHelper::countries());

            if($data['country'][0] == $india_value ){
                $company_details['state_id'] = $data['state'][0];
                $company_details['city_id'] = $data['city'][0];
                $company_details['state_text'] = NULL;
                $company_details['city_text'] = NULL;
            }else{
                $company_details['state_id'] = NULL;
                $company_details['city_id'] = NULL;
                $company_details['state_text'] = $data['state_name'][0];
                $company_details['city_text'] = $data['city_text'][0];
            }*/

            $status_array = ['status'=>'success','id'=>$company_registration_details['user_id']];
        }else{
            $status_array = ['status'=>'failed'];
        }
        return $status_array;
    }

    public function getDataByUserId($user_id, $options = NULL){
        return $this->companyRegistrationRepository->getDataByUserId($user_id, $options);
    }

    public function getDetailsByCompanyId($company_id){
        return $this->companyRegistrationRepository->getDetailsByCompanyId($company_id);
    }

    public function uploadLogo($array) {

        $storage_path = env('COMPANY_LOGO_PATH');

        if(isset($array['role']) AND ($array['role'] == 'admin' || $array['role'] == 'advertiser')){
            $storage_path = $storage_path.'temp/';
        }

        // image default storage directory path...
        $profile_upload_dir = str_replace(["/","\\"], [DIRECTORY_SEPARATOR,DIRECTORY_SEPARATOR], $storage_path);

        $new_user = false;

        if(!isset($array['role'])){
            //if(($array['role'] != 'admin'|| $array['role'] != 'advertiser')){
                if(Auth::check()){
                    $user_id = Auth::user()->id;
                }else{
                    $dummyData['first_name'] = 'temp';
                    $data =  $this->userRepository->store($dummyData);
                    $user_id = $data['id'];
                    $new_user = true;
                }
                $destination_path = $profile_upload_dir . $user_id;
            //}
        }else{
            $destination_path = $profile_upload_dir;
        }
        

        if(isset($array['profile_pic']) && $array['profile_pic']->isValid()) {

            if(!is_dir($destination_path)) {
                mkdir($destination_path, 0777,true);
            }

            $extension = '.' . $array['profile_pic']->getClientOriginalExtension();

            $destination_filename =  time().'-pic';

            $resizeDimensions = array([150, 150]);

            //create thumbnails
            \CommonHelper::createProfilePhotoThumbnails($array['profile_pic'], $array, $resizeDimensions, $destination_path, $destination_filename, $extension);

            //save profile photo to db
            $filename = $destination_filename.$extension;
            $store_arr['profile_pic'] = $filename;

            if(!isset($array['role'])){
                //if($array['role'] != 'admin'){
                    $this->userRepository->update($store_arr, $user_id);

                    if($new_user){
                        Auth::loginUsingId($user_id);
                    }
                //}
            }else{
                $user_id = '';
            }

            $status_array = ['status' => 'success', 'filename' => $filename, 'stored_file' => $destination_path.'/'.$filename, 'user_id' => $user_id];
        } else {
            $status_array = ['status' => 'failed'];
        }

        return $status_array;
    }

    public function getCompanyShipDetailsById($company_id){
        return $this->companyDetailsRepository->getCompanyShipDetailsById($company_id);
    }

    public function getAllCompanyData($options = NULL){
        return $this->companyRegistrationRepository->getAllCompanyData($options);
    }

    public function getAllCompanySubscriptionsDetails(){
        return $this->userSubscriptionRepository->getAllCompanySubscriptionsDetails();
    }
    
    public function getCompanyShipsByShipTypeId($company_id,$ship_type_id){
        return $this->companyShipDetailsRepository->getCompanyShipsByShipTypeId($company_id,$ship_type_id);
    }

    public function dataByCompanyShipId($ship_id){
        return $this->companyShipDetailsRepository->dataByCompanyShipId($ship_id);
    }

    public function storeAdvertise($data,$company_id){

        if(isset($company_id) AND !empty($company_id))
            $company_id = $company_id;
        else
            $company_id = Auth::User()->id;

        $this->advertiseRepository->setAdvertiseStatusDeactivate($company_id);

        $path = env('COMPANY_LOGO_PATH');
        $advertise_upload_dir = str_replace(["/","\\"], [DIRECTORY_SEPARATOR,DIRECTORY_SEPARATOR], $path);
        $destination_path = $advertise_upload_dir . $company_id;

        if(!is_dir($destination_path)) {
            mkdir($destination_path, 0777,true);
        }
        if(isset($data['img_obj']) && !empty($data['img_obj'])){
            $extension = '.' . $data['img_obj']->getClientOriginalExtension();
            $destination_filename =  time().'-pic';
            $filename = $destination_filename.$extension;

            $thumbnail = \PhpThumbFactory::create($data['img_obj']);
            $thumbnail->save($destination_path."/".$destination_filename.$extension);
        }

        $advertisement_data['advertisement'] = $filename;

        $data1['company_id'] = $company_id;
        $data1['img_path'] = $filename;
        $data1['status'] = 1;
        $data1['advertisement_as'] = 'company';
        $data1['ad_updated_on'] = date('Y-m-d H:i:s');
        
        $result = $this->advertiseRepository->storeCompanyAdvertise($data1);

        if($result){
            $status_array = ['status' => 'success'];
        } else {
            $status_array = ['status' => 'failed'];
        }

        return $status_array;
    }

    public function getCompanyAdvertises($filter=NULL){
        return $this->advertiseRepository->getCompanyAdvertises($filter);
    }

    public function storeShipDetails($data){

        $ship_details['company_id'] = $data['company_id'];

        if(isset($data['ship_name']) && !empty($data['ship_name']))
            $ship_details['ship_name'] = $data['ship_name'];
        
        $ship_details['ship_type'] = $data['ship_type'];

        if(isset($data['ship_flag']) && !empty($data['ship_flag']))
            $ship_details['ship_flag'] = $data['ship_flag'];

        if(isset($data['engine_type']) && !empty($data['engine_type']))
            $ship_details['engine_type'] = $data['engine_type'];

        $ship_details['grt'] = $data['grt'];
        $ship_details['bhp'] = $data['bhp'];
        $ship_details['scope_type'] = $data['scope_type'];
        // $ship_details['voyage'] = $data['voyage'];
        // $ship_details['built_year'] = $data['built-year'];
        $ship_details['p_i_cover'] = $data['p-i-cover'];

       /* if($data['p-i-cover'] == 1){
            $ship_details['p_i_cover_company_name'] = $data['p_i_cover_company_name'];
        }else{
            $ship_details['p_i_cover_company_name'] = '';
        }*/

        if(isset($data['existing_vessel_id']) && !empty($data['existing_vessel_id'])){
            $vessel_id = $data['existing_vessel_id'];
            unset($data['existing_vessel_id']);
            $result = $this->companyShipDetailsRepository->update($ship_details, $vessel_id);
        }else{
            $result = $this->companyShipDetailsRepository->store($ship_details);
        }

        if($result){
            $status_array = ['status' => 'success'];
        } else {
            $status_array = ['status' => 'failed'];
        }

        return $status_array;

    }

    public function getShipDetails($company_id, $scope_type){
        return $this->companyShipDetailsRepository->getShipDetails($company_id, $scope_type);
    }

    public function deleteVesselDetails($vessel_id, $company_id){
        return $this->companyShipDetailsRepository->deleteVesselByVesselId($vessel_id, $company_id);
    }

    public function getShipDetailsByshipId($ship_id, $company_id){
        $result = $this->companyShipDetailsRepository->getShipDetailsByshipId($ship_id, $company_id);

        if(count($result) > 0){
            $status_array = ['status' => 'success', 'result' => $result];
        }else{
            $status_array = ['status' => 'failed']; 
        }

        return $status_array;
    }

    public function storeAgents($data){

        if(isset($data['location']) && !empty($data['location']))
            $agent_details['location'] = $data['location'];

        if(isset($data['agent_type']) && !empty($data['agent_type'])){
            $agent_details['agent_type'] = $data['agent_type'];
            if($data['agent_type'] == 'appointed'){
                if(isset($data['company_name']) && !empty($data['company_name']))
                    $agent_details['to_company'] = $data['company_name']; 
                    $agent_details['to_company_id'] = $data['company_id']; 
            }  

            if($data['agent_type'] == 'associate'){
                if(isset($data['company_name']) && !empty($data['company_name']))
                    $agent_details['from_company'] = $data['company_name'];
                    $agent_details['from_company_id'] = $data['company_id'];
            }  
        }

        $agent_details['status'] = '1'; 


        if(isset($data['uploaded-file-name']) && !empty($data['uploaded-file-name'])){
            $agent_details['logo'] = $data['uploaded-file-name'];
        }

        if(isset($data['existing_agent_id']) && !empty($data['existing_agent_id'])){
            $agent_id = $data['existing_agent_id'];
            $upload_pic_id = $data['existing_agent_id'];
            unset($data['existing_agent_id']);
            $result = $this->companyAgentsRepository->update($agent_details, $agent_id);
        }else{
            $result = $this->companyAgentsRepository->store($agent_details);
            $upload_pic_id = $result->id;
        }

        if(isset($data['uploaded-file-name']) && !empty($data['uploaded-file-name'])){
            $user_data['profile_pic'] = $data['uploaded-file-name'] ;

            $storage_path = env('COMPANY_TEAM_AGENT_PIC_PATH');

            $from = str_replace(["/","\\"], [DIRECTORY_SEPARATOR,DIRECTORY_SEPARATOR], $storage_path.'temp/'.$user_data['profile_pic']);
            
            $to = str_replace(["/","\\"], [DIRECTORY_SEPARATOR,DIRECTORY_SEPARATOR], $storage_path. $upload_pic_id).DIRECTORY_SEPARATOR;

            if(!is_dir($to)) {
                mkdir($to, 0777,true);
            }
            $to = $to.$user_data['profile_pic'];
            if(is_file($from) && copy($from, $to)) {
                unlink($from);
            }
            //$this->userRepository->update($user_data,$company_login_data['id']);
        }


        if($result){
            $status_array = ['status' => 'success'];
        } else {
            $status_array = ['status' => 'failed'];
        }

        return $status_array;
    }
    
    public function getAgentsByAgentType($company_id, $agent_type){
        return $this->companyAgentsRepository->getAgentsByAgentType($company_id, $agent_type);
    }

    public function deleteAgentDetails($agent_id, $company_id){
        return $this->companyAgentsRepository->deleteAgentByAgentId($agent_id, $company_id);
    }

    public function getAgentDetailsByAgentId($agent_id){
        $result = $this->companyAgentsRepository->getAgentDetailsByAgentId($agent_id);

        if(count($result) > 0){
            $status_array = ['status' => 'success', 'result' => $result];
        }else{
            $status_array = ['status' => 'failed']; 
        }

        return $status_array;
    }

    public function uploadAgentProfilePic($file)
    {

        $storage_path = env('COMPANY_TEAM_AGENT_PIC_PATH').'temp';

        $destination_path = str_replace(["/","\\"], [DIRECTORY_SEPARATOR,DIRECTORY_SEPARATOR], $storage_path);

        if(isset($file['profile_pic']) && $file['profile_pic']->isValid()) {

            if (!is_dir($destination_path)) {
                mkdir($destination_path, 0777, true);
            }

            $extension = '.' . $file['profile_pic']->getClientOriginalExtension();

            $destination_filename =  time().'-pic';

            $resizeDimensions = array([150, 150]);

            //create thumbnails
            \CommonHelper::createProfilePhotoThumbnails($file['profile_pic'], $file, $resizeDimensions, $destination_path, $destination_filename, $extension);

            //save profile photo to db
            $filename = $destination_filename.$extension;

            $status_array = ['status' => 'success', 'filename' => $filename, 'stored_file' => $destination_path.'/'.$filename];

            return $status_array;
        }
    }

    public function uploadTeamProfilePic($file, $user_id)
    {
        $storage_path = env('COMPANY_TEAM_PROFILE_PIC_PATH').'temp';

        $destination_path = str_replace(["/","\\"], [DIRECTORY_SEPARATOR,DIRECTORY_SEPARATOR], $storage_path);

        if(isset($file['profile_pic']) && $file['profile_pic']->isValid()) {

            if (!is_dir($destination_path)) {
                mkdir($destination_path, 0777, true);
            }

            $extension = '.' . $file['profile_pic']->getClientOriginalExtension();

            $destination_filename =  time().'-pic';

            $resizeDimensions = array([150, 150]);

            //create thumbnails
            \CommonHelper::createProfilePhotoThumbnails($file['profile_pic'], $file, $resizeDimensions, $destination_path, $destination_filename, $extension);

            //save profile photo to db
            $filename = $destination_filename.$extension;

            $status_array = ['status' => 'success', 'filename' => $filename, 'stored_file' => $destination_path.'/'.$filename];

            return $status_array;
        }
    }

    public function storeTeamDetails($data){

        if(isset($data['agent_name']) && !empty($data['agent_name'])){
            $company_team_details['agent_name'] = $data['agent_name'];
        }
        if(isset($data['location_id']) && !empty($data['location_id'])){
            $company_team_details['location_id'] = $data['location_id'];
        }
        if(isset($data['company_id']) && !empty($data['company_id'])){
            $company_team_details['company_id'] = $data['company_id'];
        }
        if(isset($data['agent_designation']) && !empty($data['agent_designation'])){
            $company_team_details['agent_designation'] = $data['agent_designation'];
        }
        if(isset($data['uploaded-file-name']) && !empty($data['uploaded-file-name'])){
            $company_team_details['profile_pic'] = $data['uploaded-file-name'];
        }

        $result = $this->companyTeamDetailsRepository->store($company_team_details);

        if(isset($data['uploaded-file-name']) && !empty($data['uploaded-file-name'])){
            $user_data['profile_pic'] = $data['uploaded-file-name'] ;

            $storage_path = env('COMPANY_TEAM_PROFILE_PIC_PATH');

            $from = str_replace(["/","\\"], [DIRECTORY_SEPARATOR,DIRECTORY_SEPARATOR], $storage_path.'temp/'.$user_data['profile_pic']);
            
            $to = str_replace(["/","\\"], [DIRECTORY_SEPARATOR,DIRECTORY_SEPARATOR], $storage_path. $result->id).DIRECTORY_SEPARATOR;

            if(!is_dir($to)) {
                mkdir($to, 0777,true);
            }
            $to = $to.$user_data['profile_pic'];
            if(copy($from, $to)) {
                unlink($from);
            }
            //$this->userRepository->update($user_data,$company_login_data['id']);
        }

        
        if($result){
            $status_array = ['status' => 'success'];
        } else {
            $status_array = ['status' => 'failed'];
        }

        return $status_array;
    }

    public function getAllTeamDetails($company_id){
        return $this->companyTeamDetailsRepository->getAllTeamDetails($company_id);
    }

    public function getTeamDetails($company_id){
        return $this->companyTeamDetailsRepository->getTeamDetails($company_id);
    }

    public function deleteTeamDetails($team_id,$company_id){
        return $this->companyTeamDetailsRepository->deleteTeamDetails($team_id,$company_id);
    }

    public function getTeamDataByCompanyId($company_id){
        return $this->companyTeamDetailsRepository->getTeamDataByCompanyId($company_id);
    }

    public function getLocationDataByCompanyId($company_id){
        return $this->companyRegistrationRepository->getLocationDataByCompanyId($company_id);
    }

    public function getTeamDataByTeamId($team_id){
        return $this->companyTeamDetailsRepository->getTeamDataByTeamId($team_id)->toArray();
    }

    public function updateTeamDetailsByTeamId($team_id){

        $company_team_details = [];

        if(isset($team_id['agent_name']) && !empty($team_id['agent_name'])){
            $team_details['agent_name'] = $team_id['agent_name'];
        }
        if(isset($team_id['location_id']) && !empty($team_id['location_id'])){
            $team_details['location_id'] = $team_id['location_id'];
        }
        if(isset($team_id['company_id']) && !empty($team_id['company_id'])){
            $team_details['company_id'] = $team_id['company_id'];
        }
        if(isset($team_id['agent_designation']) && !empty($team_id['agent_designation'])){
            $team_details['agent_designation'] = $team_id['agent_designation'];
        }
        if(isset($team_id['exiting_team_id']) && !empty($team_id['exiting_team_id'])){
            $team_data_id['exiting_team_id'] = $team_id['exiting_team_id'];
        }

        if(isset($team_id['uploaded-file-name']) && !empty($team_id['uploaded-file-name'])){
            $team_details['profile_pic'] = $team_id['uploaded-file-name'];
        }
        

        $result = $this->companyTeamDetailsRepository->updateTeamDetailsByTeamId($team_details,$team_id['exiting_team_id']);

        if(isset($team_id['uploaded-file-name']) && !empty($team_id['uploaded-file-name'])){

            $user_team_id['profile_pic'] = $team_id['uploaded-file-name'] ;

            $storage_path = env('COMPANY_TEAM_PROFILE_PIC_PATH');

            $from = str_replace(["/","\\"], [DIRECTORY_SEPARATOR,DIRECTORY_SEPARATOR], $storage_path.'temp/'.$team_details['profile_pic']);
            
            $to = str_replace(["/","\\"], [DIRECTORY_SEPARATOR,DIRECTORY_SEPARATOR], $storage_path. $team_id['exiting_team_id']).DIRECTORY_SEPARATOR;
            
            if(!is_dir($to)) {
                mkdir($to, 0777,true);
            }
            $to = $to.$team_details['profile_pic'];
            if(is_file($from) && copy($from, $to)) {
                unlink($from);
            }
        }

        if($result){
            $status_array = ['status' => 'success'];
        } else {
            $status_array = ['status' => 'failed'];
        }

        return $status_array;

    }


}