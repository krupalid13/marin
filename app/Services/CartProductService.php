<?php namespace App\Services;

use App\Repositories\CartProductRepository;
/*use App\Services\Api\construction\ProductVariantService;
use App\Services\Api\construction\PincodeService;
use App\Services\Api\construction\PincodeCityService;
use App\Services\Api\construction\ProductShippingAreaService;
use App\Services\Api\construction\ProductShippingCostService;
use App\Services\Api\construction\ProductService;
use App\Services\Api\construction\CompanyDetailService;
use App\Services\Api\construction\SellerProductTaxService;
use App\Services\Api\construction\ProductVariantImageService;*/

class CartProductService
{

	private $CartProductRepository;
/*	private $ProductVariantService;
	private $PincodeService;
	private $PincodeCityService;
	private $ProductShippingAreaService;
	private $ProductShippingCostService;
	private $ProductService;
	private $CompanyDetailService;
	private $SellerProductTaxService;
	private $ProductVariantImageService;*/
	
	function __construct() {
		$this->CartProductRepository = new CartProductRepository();
/*		$this->ProductVariantService = new ProductVariantService();
		$this->PincodeService = new PincodeService();
		$this->PincodeCityService = new PincodeCityService();
		$this->ProductShippingAreaService = new ProductShippingAreaService();
		$this->ProductShippingCostService = new ProductShippingCostService();
		$this->ProductService = new ProductService();
		$this->CompanyDetailService = new CompanyDetailService();
		$this->SellerProductTaxService = new SellerProductTaxService();
		$this->ProductVariantImageService = new ProductVariantImageService();*/
	}

	public function addSubscriptionToCart($data) {

		$store_arr['subscription_id'] = $data['subscription_id'];
		$store_arr['user_id'] = $data['user_id'];
		$store_arr['quantity'] = $data['quantity'];
        $store_arr['price'] = $data['price'];
        $data['id'] = $data['user_id'];

        $subscription_data = $this->getCartDetails($data);

        if(count($subscription_data) > 0){
            $result = $this->CartProductRepository->updateSubscriptionCart($store_arr);
        }else{
            $result = $this->CartProductRepository->storeSubscriptionCart($store_arr);
        }

		if( count($result) > 0 ) {
			$status_arr = ['status' => 'success'];
		} else {
			$status_arr = ['status' => 'error'];
		}

		return $status_arr;
	}

    public function updateSubscriptionToCart($data) {

        $store_arr['subscription_id'] = $data['subscription_id'];
        $store_arr['user_id'] = $data['user_id'];
        $store_arr['quantity'] = $data['quantity'];
        $store_arr['price'] = $data['price'];

        $subscription_data = $this->getCartDetailsBySubidanduserid($data['user_id'],$data['subscription_id']);

        if(count($subscription_data) > 0){
            $result = $this->CartProductRepository->updateSubscriptionCart($store_arr);
        }else{
            $result = $this->CartProductRepository->storeSubscriptionCart($store_arr);
        }

        if( count($result) > 0 ) {
            $status_arr = ['status' => 'success'];
        } else {
            $status_arr = ['status' => 'error'];
        }

        return $status_arr;
    }

	public function getCartDetailsBySubidanduserid($user_id,$subscription_id)
    {
        return $this->CartProductRepository->getCartDetailsBySubidanduserid($user_id,$subscription_id);
    }

    public function getCartDetails($user)
    {
        return $this->CartProductRepository->getCartDetails($user['id']);
    }

    public function getCartDetailByUserId($user_id)
    {
        return $this->CartProductRepository->getCartDetailByUserId($user_id);
    }

    public function getDetailByCartId($cart_product_id){
        return $this->CartProductRepository->getDetailByCartId($cart_product_id);
    }

    public function deleteFromCart($cart_product_id) {
	    $details = $this->getDetailByCartId($cart_product_id)->toArray();

	    if(count($details) > 0){
            $result = $this->CartProductRepository->deleteFromCart($cart_product_id);
            if( $result == 1) {
                $user_id = $details[0]['user_id'];
                $cart_details = $this->getCartDetailByUserId($user_id)->toArray();

                $cart_total = 0;

                if(count($cart_details) > 0){
                    foreach ($cart_details as $c_detail){
                        $sub_total = $c_detail['price'] * $c_detail['quantity'];
                        $cart_total += $sub_total;
                    }
                }

                $status_arr = ['status' => 'success' , 'cart_total' => $cart_total];
            } else {
                $status_arr = ['status' => 'error'];
            }
        }else{
            $status_arr = ['status' => 'invalid'];
        }

        return $status_arr;
    }

    public function deleteByUserId($user_id){
    	return $this->CartProductRepository->deleteByUserId($user_id);
    }
/*
	public function addToCart($request, $user_id = null) {
		$request_product_variant_id = $request['product_variant_id'];
		$request_site_p_category_id = $request['site_p_category_id'];
		$request_pincode = $request['pincode'];
		$request_product_id = $request['product_id'];
		$request_quantity = $request['quantity'];


		if(isset($_COOKIE['cart_key'])) {
			$cart_key = $_COOKIE['cart_key'];
			if(empty($user_id)) {
				$cart_product_details = $this->getCartDetailByProductIdAndVariantIdAndCartKey($request_product_id, $request_product_variant_id, $cart_key)->toARray();
			} else {
				$cart_product_details = $this->getCartDetailByProductIdAndVariantIdAndUserId($request_product_id, $request_product_variant_id, $user_id)->toARray();
			}
			if(count($cart_product_details) > 0) {
				$result = $this->update($request, $user_id, $cart_key);
				return $result;
			} else {

				$result = $this->store($request, $user_id);
				return $result;
			}

		} else {
			$result = $this->store($request, $user_id);
			return $result;

		}
	}

	public function store($request, $user_id = null) {

		$request_product_variant_id = $request['product_variant_id'];
		$request_pincode = $request['pincode'];
		$request_product_id = $request['product_id'];
		$request_quantity = $request['quantity'];

		$store_cart_arr = [];

		// store to cart store array
		$store_cart_arr['product_id'] = $request_product_id;
		$store_cart_arr['product_variant_id'] = $request_product_variant_id;
		$store_cart_arr['quantity'] = $request_quantity;
		$store_cart_arr['site_p_category_id'] = $request['site_p_category_id'];

		if(isset($_COOKIE['cart_key'])) {
			$store_cart_arr['cart_key'] = $_COOKIE['cart_key'];
		} else {
			$cart_key = \CommonHelper::generateRandomCartKey();
			$store_cart_arr['cart_key'] = $cart_key;
		}



		// get product details===
		$product = $this->ProductService->getProductById($request_product_id)->toArray();
		$product_details_json_array = json_encode($product[0]);

		// place product details in the store array---
		$store_cart_arr['product_details'] = $product_details_json_array;

		// get variant details ===
		$variant = $this->ProductVariantService->getDetailByVariantId($request_product_variant_id)->toArray();
		if(count($variant) > 0) {
			$variant_details_json_array = json_encode($variant[0]);
			// place variant details in the store array---
			$store_cart_arr['product_variant_details'] = $variant_details_json_array;

			// get product variant images ---
			$variant_images = $this->ProductVariantImageService->getImagesByVariantId($request_product_variant_id)->toArray();
			$store_cart_arr['product_variant_image'] = $variant_images[0]['name'];
		}



		if(count($variant) > 0) {

			$variant_seller_product_id = $variant[0]['seller_product_id'];
			$variant_seller_id = $variant[0]['seller_product']['seller_id'];

			// place seller id in the store array---
			$store_cart_arr['seller_id'] = $variant_seller_id;

			// get seller details
			$company_detail = $this->CompanyDetailService->getCompanyDetailById($variant_seller_id)->toArray();
			$seller_details = [];
			if(count($company_detail) > 0) {
				$seller_details['name'] = $company_detail[0]['name'];
				$seller_details['email'] = $company_detail[0]['email'];
				$seller_details['landline'] = $company_detail[0]['landline'];
				$seller_details['state_id'] = $company_detail[0]['state_id'];
			}

			$company_detail_json_array = json_encode($seller_details);

			// place seller details in the store array---
			$store_cart_arr['seller_details'] = $company_detail_json_array;

			// get product_price and product_offer_price
			$product_price = $variant[0]['price'] * $request_quantity;

			//flag to determine, whether to apply offer or not ?
			$apply_offer = false;

			$strtotime_current_date = strtotime(Date('Y-m-d H:i:s'));

			// determine the offer valid date ===
			if(!empty($variant[0]['offer_start_date'])) {
				$offer_start_strtotime = strtotime($variant[0]['offer_start_date']);
				$offer_end_strtotime = strtotime($variant[0]['offer_end_date']);
				if($strtotime_current_date >= $offer_start_strtotime && $strtotime_current_date <= $offer_end_strtotime) {
					$apply_offer = true;
				}
			}


			//  if variant moq is not null, then get the ppu price as product price
			if(!empty($variant[0]['moq_json'])) {
				$moq_arr = json_decode($variant[0]['moq_json']);
				foreach($moq_arr as $moq) {
					if($request['quantity'] >= $moq->from && $request['quantity'] <= $moq->to) {

						if($apply_offer == true) {

							// get the offer value--
							if(!empty($moq->offer_value) && $moq->offer_type == '%') {
								$discount = ($moq->offer_value/100) * $moq->ppu;
							} else if(!empty($moq->offer_value) && $moq->offer_type !== '%') {
								$discount = $moq->offer_value;
							}

							$store_cart_arr['offer_price'] = $discount;
							// place offer price to store array---
							// $update_cart_arr['offer_price'] = $offer_price;
							$product_price = ($moq->ppu - $discount) * $request_quantity;
						} else {
							$product_price = $moq->ppu * $request_quantity;
						}
					}
				}
			} else {
				if($apply_offer == true) {
					if(!empty($variant[0]['offer_type'])) {
						if($variant[0]['offer_type'] == '%') {
							$discount = ($variant[0]['offer_price'] / 100) * $variant[0]['price'];
						} else {
							$discount = $variant[0]['offer_price'];
						}

						$store_cart_arr['offer_price'] = $discount;
					}

					$product_price = ($variant[0]['price'] - $discount) * $request_quantity;

				} else {
					$product_price = $variant[0]['price'] * $request_quantity;

				}
			}


			$store_cart_arr['price'] = $product_price;

			// get pincodes by request pin_code ---
            $pincodes = $this->PincodeService->getRelatedDetails($request['pincode'])->toArray();

            // check whether pincode details is there or not?----
            $city_ids = [];
            if(count($pincodes) > 0) {
                $pincode_ids = [];
                foreach($pincodes as $pincode) {
                    $pincode_ids[] = $pincode['id'];
                }
                $pincode_cites = $this->PincodeCityService->getAllCitiesByPincodeIds($pincode_ids)->toArray();

                if(count($pincode_cites) > 0) {
                    foreach($pincode_cites as $city) {
                        $city_ids[] = $city['id'];
                    }
                }
            }

            $temp_additional_charge = [];
            $temp_additional_charge['delivery_charge'] = 0;

            if(count($city_ids) > 0) {
                $product_shipping_area_details = $this->ProductShippingAreaService->getBySellerProductIdAndCityIds($variant_seller_product_id, $city_ids)->toArray();
                if(count($product_shipping_area_details)){

                	$shipping_area_detail_json_array = $product_shipping_area_details[0];
                	$temp_ship_detail['product_shipping_area_detail'] = $shipping_area_detail_json_array;

                    $from = $product_shipping_area_details[0]['delivery_from'];
                    $to = $product_shipping_area_details[0]['delivery_to'];

                    $detail = $from.' - '.$to;
                    $store_cart_arr['delivery_days'] = $detail;

                    $state_id = $product_shipping_area_details[0]['state_id'];

                    $product_shipping_cost = $this->ProductShippingCostService->getDetailBySellerProductAndStateId($variant_seller_product_id, $state_id)->toArray();
                    if(($product_shipping_cost) > 0) {

                    	$temp_additional_charge['delivery_charge'] = $product_shipping_cost[0]['cost'];

                    	$temp_ship_detail['product_shipping_cost_detail'] = $product_shipping_cost;
                    }
                    $ship_details = json_encode($temp_ship_detail);
                    $store_cart_arr['shipping_detail']= $ship_details;

            		$store_cart_arr['is_pincode_applicable'] = 1;

                }
            }

            // get seller_product_tax details====---
            $seller_product_tax_details = $this->SellerProductTaxService->getDetailBySellerProductId($variant_seller_product_id)->toArray();
            if(count($seller_product_tax_details) > 0) {
            	foreach($seller_product_tax_details as $tax_detail) {
            		$temp_additional_charge[$tax_detail['name']] = $tax_detail['rate'];
            	}
            }

            if(count($temp_additional_charge) > 0) {
            	$additional_charges = json_encode($temp_additional_charge);
            	$store_cart_arr['additional_charges'] = $additional_charges;
            }

            $store_result = $this->CartProductRepository->store($store_cart_arr, $user_id);
            if(count($store_result) > 0) {
            	$status_arr = ['status' => 'success','type' => 'store','cart_key' => $store_cart_arr['cart_key']];
            } else {
            	$status_arr = ['status' => 'error','type' => 'store'];
            }

            return $status_arr;


		} else {
			$status_arr = ['status' => 'invalid_data','type' => 'store'];
			return $status_arr;
		}
	}

	public function update($request, $user_id = null, $cart_key = null) {
		$request_product_variant_id = $request['product_variant_id'];
		$request_pincode = $request['pincode'];
		$request_product_id = $request['product_id'];
		$request_quantity = $request['quantity'];

		$update_cart_arr = [];
		// store to cart update array
		$update_cart_arr['quantity'] = $request_quantity;

		// get product details===
		$product = $this->ProductService->getProductById($request_product_id)->toArray();

		// get variant details ===
		$variant = $this->ProductVariantService->getDetailByVariantId($request_product_variant_id)->toArray();

		if(count($variant) > 0) {

			$variant_seller_product_id = $variant[0]['seller_product_id'];
			$variant_seller_id = $variant[0]['seller_product']['seller_id'];

			//flag to determine, whether to apply offer or not ?
			$apply_offer = false;

			$strtotime_current_date = strtotime(Date('Y-m-d H:i:s'));

			// determine the offer valid date ===
			if(!empty($variant[0]['offer_start_date'])) {
				$offer_start_strtotime = strtotime($variant[0]['offer_start_date']);
				$offer_end_strtotime = strtotime($variant[0]['offer_end_date']);
				if($strtotime_current_date >= $offer_start_strtotime && $strtotime_current_date <= $offer_end_strtotime) {
					$apply_offer = true;
				}
			}

			//  if variant moq is not null, then get the ppu price as product price
			$offer_price = $variant[0]['offer_price'];
			$discount = 0;
			$product_price = 0;
			if(!empty($variant[0]['moq_json'])) {
				$moq_arr = json_decode($variant[0]['moq_json']);
				foreach($moq_arr as $moq) {
					if($request['quantity'] >= $moq->from && $request['quantity'] <= $moq->to) {

						if($apply_offer == true) {

							// get the offer value--
							if(!empty($moq->offer_value) && $moq->offer_type == '%') {
								$discount = ($moq->offer_value/100) * $moq->ppu;
							} else if(!empty($moq->offer_value) && $moq->offer_type !== '%') {
								$discount = $moq->offer_value;
							}
							// place offer price to store array---
							// $update_cart_arr['offer_price'] = $offer_price;
							$product_price = ($moq->ppu - $discount) * $request_quantity;
						} else {
							$product_price = $moq->ppu * $request_quantity;
						}
					}
				}
			} else {
				if($apply_offer == true) {
					if(!empty($variant[0]['offer_type'])) {
						if($variant[0]['offer_type'] == '%') {
							$discount = ($offer_price / 100) * $variant[0]['price'];
						} else {
							$discount = $offer_price;
						}
					}

					$product_price = ($variant[0]['price'] - $discount) * $request_quantity;

				} else {
					$product_price = $variant[0]['price'] * $request_quantity;

				}
			}

			$update_cart_arr['offer_price'] = $discount;


			$update_cart_arr['price'] = $product_price;

			// get pincodes by request pin_code ---
            $pincodes = $this->PincodeService->getRelatedDetails($request['pincode'])->toArray();

            // check whether pincode details is there or not?----

            $city_ids= [];
            if(count($pincodes) > 0) {
                $pincode_ids = [];
                foreach($pincodes as $pincode) {
                    $pincode_ids[] = $pincode['id'];
                }
                $pincode_cites = $this->PincodeCityService->getAllCitiesByPincodeIds($pincode_ids)->toArray();

                if(count($pincode_cites) > 0) {
                    foreach($pincode_cites as $city) {
                        $city_ids[] = $city['id'];
                    }
                }
            }

            $temp_additional_charge = [];
            $temp_additional_charge['delivery_charge'] = 0;
            $update_cart_arr['is_pincode_applicable'] = 0;


            if(count($city_ids) > 0) {
                $product_shipping_area_details = $this->ProductShippingAreaService->getBySellerProductIdAndCityIds($variant_seller_product_id, $city_ids)->toArray();

                if(count($product_shipping_area_details)){

                	$shipping_area_detail_json_array = $product_shipping_area_details[0];
                	$temp_ship_detail['product_shipping_area_detail'] = $shipping_area_detail_json_array;

                    $from = $product_shipping_area_details[0]['delivery_from'];
                    $to = $product_shipping_area_details[0]['delivery_to'];

                    $detail = $from.' - '.$to;
                    $update_cart_arr['delivery_days'] = $detail;

                    $state_id = $product_shipping_area_details[0]['state_id'];

                    $product_shipping_cost = $this->ProductShippingCostService->getDetailBySellerProductAndStateId($variant_seller_product_id, $state_id)->toArray();
                    if(($product_shipping_cost) > 0) {
                    	$temp_additional_charge['delivery_charge'] = $product_shipping_cost[0]['cost'];
                    	$temp_ship_detail['product_shipping_cost_detail'] = $product_shipping_cost;
                    }
                    $ship_details = json_encode($temp_ship_detail);
                    $update_cart_arr['shipping_detail']= $ship_details;
            		$update_cart_arr['is_pincode_applicable'] = 1;

                } else {
                	$update_cart_arr['delivery_days'] = null;
                	$update_cart_arr['shipping_detail'] = null;
                	$update_cart_arr['is_pincode_applicable'] = 0;
                }
            }

            // get seller_product_tax details====---
            $seller_product_tax_details = $this->SellerProductTaxService->getDetailBySellerProductId($variant_seller_product_id)->toArray();
            if(count($seller_product_tax_details) > 0) {
            	foreach($seller_product_tax_details as $tax_detail) {
            		$temp_additional_charge[$tax_detail['name']] = $tax_detail['rate'];
            	}
            }

            if(count($temp_additional_charge) > 0) {
            	$additional_charges = json_encode($temp_additional_charge);
            	$update_cart_arr['additional_charges'] = $additional_charges;
            }

            $update_result = $this->CartProductRepository->updateByCartKeyProductIdProductVariantId($update_cart_arr, $cart_key, $request_product_id, $request_product_variant_id, $user_id);
            if($update_result > 0) {
            	$status_arr = ['status' => 'success','type' => 'update'];
            } else {
            	$status_arr = ['status' => 'error','type' => 'update'];
            }

            return $status_arr;


		} else {
			$status_arr = ['status' => 'invalid_data','type' => 'update'];
			return $status_arr;
		}
	}

	public function getCartProductsListByUserId($user_id, $cookie_pin_code = null) {
        $cart_products = $this->getCartProductDetailsByUserId($user_id, $cookie_pin_code)->toArray();
        $update_result = $this->updateCartsDetails($cart_products, $cookie_pin_code);
        $cart_products = $this->getCartProductDetailsByUserId($user_id, $cookie_pin_code)->toArray();
        return $cart_products;
	}

	public function getCartProductsListByCartKey($cart_key, $cookie_pin_code = null) {
        $cart_products = $this->getCartProductDetailsByCartKey($cart_key, $cookie_pin_code)->toArray();
        $update_result = $this->updateCartsDetails($cart_products, $cookie_pin_code);
        $cart_products = $this->getCartProductDetailsByCartKey($cart_key, $cookie_pin_code)->toArray();
        return $cart_products;
	}

	public function updateCartsDetails($cart_products, $cookie_pin_code = null) {
		$cart_count = count($cart_products);
        if(count($cart_products) > 0) {
            foreach($cart_products as $cart_product) {
            	$cart_product['pincode'] = $cookie_pin_code;
            	$this->update($cart_product, $cart_product['user_id'], $cart_product['cart_key']);
            }
        }
	}

	public function updateCartAvailability($request, $user_id = null, $cart_key = null) {
		$request_product_variant_id = $request['product_variant_id'];
		$request_pincode = '';
		if( isset($request['pincode']) && !empty($request['pincode']) ) {
			$request_pincode = $request['pincode'];
		}
		$request_product_id = $request['product_id'];
		$request_quantity = $request['quantity'];

		$update_cart_arr = [];


		// store to cart update array
		if( isset($request['quantity']) ) {
			$update_cart_arr['quantity'] = $request_quantity;
		}

		if( !empty($product_variant_id) ){

			// get product details===
			$product = $this->ProductService->getProductById($request_product_id)->toArray();

			// get variant details ===
			$variant = $this->ProductVariantService->getDetailByVariantId($request_product_variant_id)->toArray();

			if(count($variant) > 0) {
				$variant_seller_product_id = $variant[0]['seller_product_id'];

				// get pincodes by request pin_code ---
	            $pincodes = $this->PincodeService->getRelatedDetails($request_pincode)->toArray();

	            // check whether pincode details is there or not?----

	            $city_ids= [];
	            if(count($pincodes) > 0) {
	                $pincode_ids = [];
	                foreach($pincodes as $pincode) {
	                    $pincode_ids[] = $pincode['id'];
	                }
	                $pincode_cites = $this->PincodeCityService->getAllCitiesByPincodeIds($pincode_ids)->toArray();

	                if(count($pincode_cites) > 0) {
	                    foreach($pincode_cites as $city) {
	                        $city_ids[] = $city['id'];
	                    }
	                }
	            }


	            if(count($city_ids) > 0) {
	                $product_shipping_area_details = $this->ProductShippingAreaService->getBySellerProductIdAndCityIds($variant_seller_product_id, $city_ids)->toArray();

	                if(count($product_shipping_area_details)){

	                	$shipping_area_detail_json_array = $product_shipping_area_details[0];
	                	$temp_ship_detail['product_shipping_area_detail'] = $shipping_area_detail_json_array;

	                    $from = $product_shipping_area_details[0]['delivery_from'];
	                    $to = $product_shipping_area_details[0]['delivery_to'];

	                    $detail = $from.' - '.$to;
	                    $update_cart_arr['delivery_days'] = $detail;

	                    $state_id = $product_shipping_area_details[0]['state_id'];

	            		$update_cart_arr['is_pincode_applicable'] = 1;

	                }
	            }

	            $update_cart_arr['product_variant_details'] = json_encode($variant[0]);

	            $update_result = $this->CartProductRepository->updateByCartKeyProductIdProductVariantId($update_cart_arr, $cart_key, $request_product_id,
	            						$request_product_variant_id, $user_id);
	            if($update_result > 0) {
	            	$status_arr = ['status' => 'success','type' => 'update'];
	            } else {
	            	$status_arr = ['status' => 'error','type' => 'update'];
	            }

	            return $status_arr;


			} else {
				$status_arr = ['status' => 'invalid_data','type' => 'update'];
				return $status_arr;
			}
		} else {
			$status_arr = ['status' => 'success','type' => 'update'];
			return $status_arr;
		}
	}

	public function updateCartUserId($cart_key, $user_id) {
		$cart_products = $this->getCartProductDetailsByCartKey($cart_key)->toArray();
		if(count($cart_products) > 0) {
			$this->CartProductRepository->updateCartUserId($cart_key, $user_id);
		}
	}

	public function updateCartProductVaraintDetailsByCartId($array, $cart_id) {
		return $this->CartProductRepository->updateCartProductVaraintDetailsByCartId($array, $cart_id);
	}

	public function getCartDetailByProductIdAndVariantIdAndCartKey($product_id, $product_variant_id, $cart_key){
		return $this->CartProductRepository->getCartDetailByProductIdAndVariantIdAndCartKey($product_id, $product_variant_id, $cart_key);
	}
	public function getCartDetailByProductIdAndVariantIdAndUserId($product_id, $product_variant_id, $user_id){
		return $this->CartProductRepository->getCartDetailByProductIdAndVariantIdAndUserId($product_id, $product_variant_id, $user_id);
	}

	public function getCartProductDetailsByUserId($user_id) {
		return $this->CartProductRepository->getCartProductDetailsByUserId($user_id);
	}

	public function getCartProductDetailsByCartKey($cart_key) {
		return $this->CartProductRepository->getCartProductDetailsByCartKey($cart_key);
	}


	public function updateCartPincode($request) {

		if(!empty($request['user_id'])) {
			$cart_products = $this->getCartProductDetailsByUserId($request['user_id'])->toArray();
		} else {
			$cart_products = $this->getCartProductDetailsByCartKey($request['cart_key'])->toArray();
		}
		if(count($cart_products) > 0) {
			foreach ($cart_products as $cart_product) {
				$cart_product['pincode'] = $request['pincode'];
				$this->update($cart_product, $request['user_id'], $request['cart_key']);

				$this->updateCartAvailability($request, $user_id = null, $cart_key = null);
			}
			$status_arr = ['status' => 'success'];
			return $status_arr;
		} else {
			$status_arr =['status' => 'invalid_cart_key'];
			return $status_arr;
		}
	}

	public function deleteByUserId($user_id) {
		return $this->CartProductRepository->deleteByUserId($user_id);
	}

	public function getDetailByCartId($cart_id) {
		return $this->CartProductRepository->getDetailByCartId($cart_id);
	}

	public function updateCartVariantDetail($cart_id, $product_variant_id) {
		$cart_detail = $this->getDetailByCartId($cart_id)->toArray();
		if(count($cart_detail) > 0) {
			$variant_detail = $this->ProductVariantService->getDetailByVariantId($product_variant_id)->toArray();
			if(count($variant_detail) > 0) {
				$update_cart['product_variant_details'] = json_encode($variant_detail[0]);
				$result = $this->CartProductRepository->updateCartByCartId($update_cart, $cart_id);
				if($result > 0) {
					$status_arr = ['status' => 'success'];
				} else {
					$status_arr = ['status' => 'error'];
				}
				return $status_arr;
			} else {
				$status_arr = ['status' => 'invalid_variant'];
				return $status_arr;
			}
		} else {
			$status_arr = ['status' => 'invalid_cart'];
			return $status_arr;
		}
	}

	public function updateCartProductAvailabilityByUserId($user_id) {
		return $this->CartProductRepository->updateCartProductAvailabilityByUserId($user_id);
	}*/

}