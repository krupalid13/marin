<?php

/**
 * Created by PhpStorm.
 * User: admin
 * Date: 5/23/2017
 * Time: 6:33 PM
 */

namespace App\Services;

use Mail;
use App\Repositories\PromotionalEmailRepository;
use App\Mail\PromotionalEmail;

class PromotionalEmailService {

    private $promotionalEmailRepository;

    function __construct() {
        $this->promotionalEmailRepository = new PromotionalEmailRepository;
    }

    public function sendEmail($emails) {
        foreach ($emails as $email) {
            if (!empty($email)) {
                $emailData = array();
                $emailData['email'] = $email;
                $emailData['email_type'] = 1;
                $emailData['created_at'] = date('Y-m-d H:i:s');;
                $result = $this->promotionalEmailRepository->store($emailData);
                if($result > 0){
                    $userData['email'] = $email;
                    $userData['promotional_email_id'] = \CommonHelper::encodeKey($result);
                    $blade_file = 'emails.promotional_1';
                    $title = 'FLANKNOT: Hello Seafarer';
                    $subject = 'FLANKNOT: Hello Seafarer';
                    $message = '';
                    $data = $userData;
//                    $check = $this->sendEmailTemplate($blade_file, $data, $userData, $title, $subject, null);
                    
                    $mail_data = [
                        'email' => $email,
                        'subject' => $subject,
                        'name' => $title,
                        'token' => $userData['promotional_email_id'],
                    ];
                    Mail::to($email)->send(new PromotionalEmail($mail_data));
                }
            }
        }
        $status_arr = ['status' => 'success'];
        return $status_arr;
    }

    public function sendEmailTemplate($blade_file, $data, $array, $title, $subject, $type = null) {
        $data['host'] = 'FLANKNOT';
        $data['home'] = route('home');
        $name = 'Hello Seaferer';
        try {
//            $this->from(env('MAIL_FROM_ADDRESS','donot-reply@flanknot.com'), $this->name . ' - ' . env('APP_NAME','FLANKNOT'))
//            ->view('emails.sharing.share_docs', compact('data'));
            Mail::send($blade_file, ['template' => $data], function ($message) use ($data, $array, $type, $title, $subject, $name) {
                $message->from(env('MAIL_FROM_ADDRESS', 'info@flanknot.com'), $title);
                $message->to($array['email'], $name)->subject($subject);
                $message->view('emails.sharing.share_docs', compact('data'));
                if (isset($data['send_pdf']) && !empty($data['send_pdf'])) {
                    $message->attach($data['file_path'], array(
                        'mime' => 'application/pdf')
                    );
                }
            });
            $status_arr = ['status' => 'success'];
            return $status_arr;
        } catch (Exception $e) {
            $status_arr = ['status' => 'failed', 'message' => $e->getMessage()];
            return $status_arr;
        }
    }

}
