<?php namespace App\Services;

use App\Repositories\UserPersonalDetailsRepository;

class UserPersonalDetailsService
{
    private $userPersonalDetailsRepository;

    public function __construct()
    {
        $this->userPersonalDetailsRepository = new UserPersonalDetailsRepository();
    }

    public function getDetailsByUserId($user_id)
    {
        return $this->userPersonalDetailsRepository->getUserPersonalDetailsById($user_id);
    }

    public function store($data)
    {
        $this->userPersonalDetailsRepository->store($data);
    }

    public function updateByUserId($data,$user_id){
        return $this->userPersonalDetailsRepository->updateByUserId($data,$user_id);
    }
}

?>