<?php

namespace App\Services;
use App\Repositories\UserCoeDetailsRepository;

class UserCoeDetailsService
{
    

    private $userCoeDetailsRepository;

    public function __construct()
    {
        $this->userCoeDetailsRepository = new UserCoeDetailsRepository();
    }

    public function store($data)
    {
        $this->userCoeDetailsRepository->store($data);
    }

    public function getDetailsByUserId($user_id) {
        return $this->userCoeDetailsRepository->getDetailsByUserId($user_id);
    }

    public function updateByUserId($data, $user_id) {
        $this->userCoeDetailsRepository->updateByUserId($data, $user_id);
    }

     public function deleteServiceById($user_id){
        $this->userCoeDetailsRepository->deleteServiceById($user_id);
    }

    public function updateByCoeId($data, $coeId) {
        $this->userCoeDetailsRepository->updateByCoeId($data, $coeId);
    }
    
    public function deleteServiceByCoeIds($coeIds) {
        $this->userCoeDetailsRepository->deleteServiceByCoeIds($coeIds);
    }
}