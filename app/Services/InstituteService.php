<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 5/17/2017
 * Time: 4:12 PM
 */

namespace App\Services;
use App\Repositories\InstituteRepository;
use App\Repositories\UserRepository;
use App\Repositories\InstituteDetailsRepository;
use App\Repositories\UserCertificateDetailsRepository;
use App\Repositories\CourseRepository;
use App\Repositories\BatchRepository;
use App\Services\InstituteLocationService;
use App\Services\SubscriptionService;
use App\Repositories\AdvertiseRepository;

use Auth;


class InstituteService
{
    private $instituteRegistrationRepository;
    private $userRepository;
    private $instituteDetailsRepository;
    private $instituteLocationService;
    private $courseRepository;
    private $batchRepository;
    private $sendEmailService;
    private $sendSmsService;
    private $subscriptionService;
    private $advertiseRepository;

    function __construct()
    {
        $this->instituteRegistrationRepository = new InstituteRepository;
        $this->userRepository = new UserRepository;
        $this->userCertificateDetailsRepository = new UserCertificateDetailsRepository;
        $this->instituteDetailsRepository = new InstituteDetailsRepository;
        $this->batchDetailsRepository = new BatchRepository;
        $this->courseRepository = new CourseRepository;
        $this->instituteLocationService = New InstituteLocationService;
        $this->sendEmailService = new SendEmailService();
        $this->sendSmsService = new SendSmsService();
        $this->subscriptionService = new SubscriptionService();
        $this->advertiseRepository = New AdvertiseRepository();
    }

    public function store($data, $logged_institute_data = NULL){

        $login_details['first_name'] = $data['contact_person'];
        $login_details['email'] = $data['email'];


        $login_details['mobile'] = $data['contact_person_number'];

        if(isset($data['password']) && !empty($data['password']))
            $login_details['password'] = bcrypt($data['password']);

        $login_details['registered_as'] = 'institute';

        if(isset($data['added_by']) && !empty($data['added_by']))
            $login_details['added_by'] = $data['added_by'];
        
        if(isset($data['otp']))
            $login_details['otp'] = $data['otp'];

        if(isset($data['mobile']))
            $login_details['mobile'] = $data['mobile'];

        
        if(isset($logged_institute_data) && !empty($logged_institute_data)){

            if(isset($logged_institute_data['added_by']) AND $logged_institute_data['added_by'] == 'admin'){
               
            }else{
                $login_details['welcome_email'] = 1;
                if(isset($data['contact_person_number']) && strlen($data['contact_person_number']) == 10){
                    $login_details['welcome_sms'] = 1;
                }
            }

            if(isset($logged_institute_data['email']) && $logged_institute_data['email'] != $data['email']){ 
                //if email changed from Profile
                $login_details['is_email_verified'] = 0;
                $user['update_email'] = 1;
                $this->sendEmailService->sendVerificationEmailToUser($login_details);
            }

            if(isset($logged_institute_data['mobile']) && $logged_institute_data['mobile'] != $data['contact_person_number']){ 
                //if mobile changed from Profile

                $login_details['is_mob_verified'] = 0;
                $user['update_mobile'] = 1;
                $this->sendSmsService->sendMobileVerifySms($login_details['mobile'],$login_details['otp']);
            }

            $this->userRepository->update($login_details,$logged_institute_data['id']);
            $institute_login_data['id'] = $logged_institute_data['id'];

            if(isset($logged_institute_data['added_by']) AND $logged_institute_data['added_by'] == 'admin'){
               
            }else{
                if(isset($logged_institute_data['welcome_email']) && $logged_institute_data['welcome_email'] == 0){
                    $login_details['id'] = $logged_institute_data['id'];
                    $this->sendEmailService->sendEmailToNewRegisteredUser($login_details);
                    $this->sendEmailService->sendVerificationEmailToUser($login_details);
                }

                if(isset($data['contact_person_number']) && strlen($data['contact_person_number']) == 10){
                    if(isset($logged_institute_data['welcome_sms']) && $logged_institute_data['welcome_sms'] == 0){
                        $logged_institute_data['registered_as'] = 'institute';
                        $login_details['id'] = $logged_institute_data['id'];
                        $this->sendSmsService->sendSmsToNewRegistered($login_details);
                        $this->sendSmsService->sendMobileVerifySms($logged_institute_data['mobile'],$login_details['otp']);
                    }
                }
            }

        }else{
            
            if(isset($data['added_by']) AND $data['added_by'] == 'admin'){
               
            }else{
                $login_details['welcome_email'] = 1;
                if(isset($data['contact_person_number']) && strlen($data['contact_person_number']) == 10){
                    $login_details['welcome_sms'] = 1;
                }
            }

            $institute_login_data = $this->userRepository->store($login_details)->toArray();

            if(isset($institute_login_data['id']) && !empty($institute_login_data['id'])){
                $login_details['id'] = $institute_login_data['id'];
            }

            if(isset($data['added_by']) AND $data['added_by'] == 'admin'){
               
            }else{

                $this->sendEmailService->sendEmailToNewRegisteredUser($login_details);
                $this->sendEmailService->sendVerificationEmailToUser($login_details);
                if(isset($data['contact_person_number']) && strlen($data['contact_person_number']) == 10){
                    $this->sendSmsService->sendSmsToNewRegistered($login_details);
                    $this->sendSmsService->sendMobileVerifySms($login_details['mobile'],$login_details['otp']);
                }
            }

            if(!isset($data['added_by']))
                Auth::loginUsingID($institute_login_data['id']);
        }


        if(isset($data['uploaded-file-name']) && !empty($data['uploaded-file-name'])){
            $user_data['profile_pic'] = $data['uploaded-file-name'] ;

            $storage_path = env('INSTITUTE_LOGO_PATH');

            $from = str_replace(["/","\\"], [DIRECTORY_SEPARATOR,DIRECTORY_SEPARATOR], $storage_path.'temp/'.$data['uploaded-file-name']);

            if(file_exists($from)){
            
                $to = str_replace(["/","\\"], [DIRECTORY_SEPARATOR,DIRECTORY_SEPARATOR], $storage_path. $institute_login_data['id']).DIRECTORY_SEPARATOR;

                if(!is_dir($to)) {
                    mkdir($to, 0777,true);
                }
                $to = $to.$data['uploaded-file-name'];
                if(copy($from, $to)) {
                    unlink($from);
                }
                $this->userRepository->update($user_data,$institute_login_data['id']);
            }
        }

        if(isset($institute_login_data['id']) && !empty($institute_login_data['id'])) {

            $institute_registration_details['user_id'] = $institute_login_data['id'];
            $institute_registration_details['institute_name'] = $data['institute_name'];
            $institute_registration_details['email'] = $data['email'];
            if(isset($data['password']) && !empty($data['password']))
                $institute_registration_details['password'] = $data['password'];

            $institute_registration_details['contact_person'] = $data['contact_person'];
            $institute_registration_details['contact_number'] = $data['contact_person_number'];
            $institute_registration_details['website'] = $data['website'];
            $institute_registration_details['institute_alias'] = $data['institute_alias'];
        
            
            $registration_data = $this->instituteRegistrationRepository->getDetailsByCompanyId($institute_login_data['id']);

            if(count($registration_data) > 0){
                $this->instituteRegistrationRepository->update($institute_registration_details , $institute_login_data['id']);
                
                $institute_registration_login_data['id'] = $registration_data[0]['id'];
            }else{
                $institute_obj = $this->instituteRegistrationRepository->store($institute_registration_details);

                $institute_registration_login_data['id'] = $institute_obj->id;
                // Add free subscription on institute registration
                $institute_data = $this->subscriptionService->getInstituteSubscriptionByRegistrationId($institute_obj->id);
                if (count($institute_data) == 0) {
                    $subscription = $this->subscriptionService->getAllActiveSubscription('institute')->toArray();
                    foreach ($subscription as $key => $sub_val) {
                       if (strtolower($sub_val['duration_title']) == 'free') {
                            $free_sub_arr = [];
                            $free_sub_arr['institute_reg_id'] = $institute_obj->id;
                            $free_sub_arr['subscription_details'] = json_encode($sub_val);
                            $free_sub_arr['status'] = 1;
                            $free_sub_arr['valid_from'] = date('Y-m-d 00:00:00');
                            $free_sub_arr['valid_to'] = date('Y-m-d 23:59:59', strtotime('+'.(($sub_val['duration'] * 30) -1 ).' days'));
                            $added_subscription = $this->subscriptionService->saveEditAnySubscription($free_sub_arr,'institute');
                       }
                    }
                }
            }

            if(isset($data['fax']) && !empty($data['fax']))
                $institute_details['fax'] = $data['fax'];
            else
                $institute_details['fax'] = NULL;

            $institute_details['institute_id'] = $institute_registration_login_data['id'];
            $institute_details['institute_description'] = $data['institute_description'];
            /*$institute_details['institute_email'] = $data['institute_email'];
            $institute_details['institute_contact_number'] = $data['institute_contact_number'];*/
            

            $institute_data = $this->instituteDetailsRepository->getDetailsByInstituteId($institute_registration_login_data['id']);
            if(count($institute_data) > 0){
                $this->instituteDetailsRepository->update($institute_details , $institute_registration_login_data['id']);
            }else{
                $this->instituteDetailsRepository->store($institute_details);
            }
            $location_id= '';
            if(isset($data['location_id']) AND !empty($data['location_id'])){
                $location_id = $data['location_id'];
            }
            
            $this->instituteLocationService->deleteRecordsByInstituteIdAndLocationIds($institute_registration_login_data['id'],$location_id);
            foreach ($data['country'] as $index => $value){
                $institute_location_details[$index]['institute_id'] = $institute_registration_login_data['id'];
                $institute_location_details[$index]['country'] = $data['country'][$index];
                $institute_location_details[$index]['pincode_text'] = $data['pincode'][$index];
                $institute_location_details[$index]['address'] = $data['address'][$index];
                $institute_location_details[$index]['headbranch'] = $data['is_headoffice'][$index];
                $institute_location_details[$index]['branch_type'] = $data['branch_type'][$index];
                $institute_location_details[$index]['email'] = $data['loc_institute_email'][$index];
                $institute_location_details[$index]['number'] = $data['loc_phone_number'][$index];

                if(isset($data['pincode_id'][$index]) && !empty($data['pincode_id'][$index]))
                    $institute_location_details[$index]['pincode_id'] = $data['pincode_id'][$index];

                if(isset($data['state'][$index]) && !empty($data['state'][$index])){
                    $institute_location_details[$index]['state_id'] = $data['state'][$index];
                    $institute_location_details[$index]['city_id'] = $data['city'][$index];
                }else{
                    $institute_location_details[$index]['state_text'] = $data['state_text'][$index];
                    $institute_location_details[$index]['city_text'] = $data['city_text'][$index];
                }

                if(isset($data['location_id'][$index]) && !empty($data['location_id'][$index])){
                    $location_id = $data['location_id'][$index];
                    $this->instituteLocationService->update($institute_location_details[$index],$data['location_id'][$index]);
                }else{
                    $loc_result = $this->instituteLocationService->store($institute_location_details[$index]);
                    $location_id = $loc_result->id;
                }
                
                if(!empty($location_id)){
                    $loc_contact = [];
                    if(isset($data['loc_contact_person'][$index]) && !empty($data['loc_contact_person'][$index])){
                        $loc_contact['contact_name'] = $data['loc_contact_person'][$index];
                    }
                    if(isset($data['loc_contact_designation'][$index]) && !empty($data['loc_contact_designation'][$index])){
                        $loc_contact['designation'] = $data['loc_contact_designation'][$index];
                    }
                    if(isset($data['loc_contact_phone'][$index]) && !empty($data['loc_contact_phone'][$index])){
                        $loc_contact['contact_number'] = $data['loc_contact_phone'][$index];
                    }

                    if(isset($loc_contact) && !empty($loc_contact)){
                        $loc_contact['location_id'] = $location_id;
                        \App\InstituteLocationContact::where('location_id',$location_id)->delete();
                        \App\InstituteLocationContact::create($loc_contact);
                    }
                }
            }
            
            $status_array = ['status'=>'success'];
        }else{
            $status_array = ['status'=>'failed'];
        }
        return $status_array;
    }

    public function uploadLogo($array) {
        
        $storage_path = env('INSTITUTE_LOGO_PATH');

        if(isset($array['role']) AND ($array['role'] == 'admin' || $array['role'] == 'advertiser')){
            $storage_path = $storage_path.'temp/';
        }
        // image default storage directory path...
        $profile_upload_dir = str_replace(["/","\\"], [DIRECTORY_SEPARATOR,DIRECTORY_SEPARATOR], $storage_path);

        $new_user = false;

        if(!isset($array['role'])){
            if(Auth::check()){
                $user_id = Auth::user()->id;
            }else{
                $dummyData['first_name'] = 'temp';
                $data =  $this->userRepository->store($dummyData);
                $user_id = $data['id'];
                $new_user = true;
            }
            $destination_path = $profile_upload_dir . $user_id;
        }else{
            $destination_path = $profile_upload_dir;
        }

        if(isset($array['profile_pic']) && $array['profile_pic']->isValid()) {

            if(!is_dir($destination_path)) {
                mkdir($destination_path, 0777,true);
            }

            $extension = '.' . $array['profile_pic']->getClientOriginalExtension();

            $destination_filename =  time().'-pic';

            $resizeDimensions = array([150, 150]);

            //create thumbnails
            \CommonHelper::createProfilePhotoThumbnails($array['profile_pic'], $array, $resizeDimensions, $destination_path, $destination_filename, $extension);

            //save profile photo to db
            $filename = $destination_filename.$extension;
            $store_arr['profile_pic'] = $filename;

            if(!isset($array['role'])){
                $this->userRepository->update($store_arr, $user_id);

                if($new_user){
                    Auth::loginUsingId($user_id);
                }
            }else{
                $user_id = '';
            }

            $status_array = ['status' => 'success', 'filename' => $filename, 'stored_file' => $destination_path.'/'.$filename, 'user_id' => $user_id];
        } else {
            $status_array = ['status' => 'failed'];
        }

        return $status_array;
    }

    public function getCourseNameByCourseType($course_id=null,$institute_id=null){
        return $this->instituteDetailsRepository->getCourseNameByCourseType($course_id,$institute_id);
    }

    public function getInstituteCourseDetailsById($course_id){
        return $this->instituteDetailsRepository->getInstituteCourseDetailsById($course_id);
    }

    public function getAllUserAppliedCourses($institute_id, $options, $paginate, $filter = NULL){
        return $this->courseRepository->getAllUserAppliedCourses($institute_id, $options, $paginate, $filter);
    }

    public function getAllBatchesByInstituteId($institute_id, $filter = NULL, $batch_id = NULL, $paginate = NULL){
        return $this->batchDetailsRepository->getAllBatchesByInstituteId($institute_id, $filter, $batch_id, $paginate);
    }

    public function getAllOnDemandBatchesByInstituteId($institute_id, $filter = NULL, $batch_id = NULL, $paginate = NULL){
        return $this->batchDetailsRepository->getAllOnDemandBatchesByInstituteId($institute_id, $filter, $batch_id, $paginate);
    }

    public function storeCourses($data){
        
        if(isset($data) && !empty($data)){
            if(isset($data['course_name']) && $data['course_name'] == 'other'){
                $course_data['course_name'] = ucwords($data['other_course_name']);
                $course_data['course_type'] = $data['course_type'];
                $course_data['status'] = '2';
                $course_result = $this->batchDetailsRepository->storeOtherCoursesInCourses($course_data)->toArray();
                if(isset($course_result) && isset($course_result['id'])){
                    $courses['course_id'] = $course_result['id'];
                }
            }else{
               $courses['course_id'] = $data['course_name']; 
            }

            $courses['course_type'] = isset($data['course_type']) ? $data['course_type'] : '';
            $courses['institute_id'] = isset($data['institute_id']) ? $data['institute_id'] : '';
            $courses['cost'] = isset($data['cost']) ? $data['cost'] : '';
            $courses['duration'] = isset($data['duration']) ? $data['duration'] : '';
            $courses['description'] = isset($data['description']) ? $data['description'] : '';
            $courses['eligibility'] = isset($data['eligibility']) ? $data['eligibility'] : '';
            $courses['documents_req'] = isset($data['documents_req']) ? implode($data['documents_req'],',') : '';
            $courses['size'] = isset($data['size']) ? $data['size'] : '';
            $courses['certificate_validity'] = isset($data['certificate_validity']) ? $data['certificate_validity'] : '';
            $courses['key_topics'] = isset($data['key_topics']) ? $data['key_topics'] : '';
            $courses['approved_by'] = isset($data['approved_by']) ? $data['approved_by'] : '';

            if(isset($data['course_id']) && !empty($data['course_id'])){
                $courses['existing_course_id'] = $data['course_id'];
            }else{
                $courses['status'] = '1';
            }
            
            $result = $this->batchDetailsRepository->storeCourse($courses);
            
            if(count($result) > 0){
                $status_array = ['status' => 'success'];
            }else{
                $status_array = ['status' => 'failed'];
            }

            return $status_array;
        }
    }

    public function getAllInstitutesCourses($options=NULL){
        return $this->batchDetailsRepository->getAllInstitutesCourses($options);
    }

    public function getAllCourseDetailsByInstituteId($institute_id,$options=NULL,$filter=NULL){
        return $this->batchDetailsRepository->getAllCourseDetailsByInstituteId($institute_id,$options,$filter);
    }

    public function getAllCourseDetailsByInstituteCourseId($course_id,$options=NULL){
        return $this->batchDetailsRepository->getAllCourseDetailsByInstituteCourseId($course_id,$options);
    }

    public function getPastCourseDetailsByInstituteCourseId($course_id,$options=NULL){
        return $this->batchDetailsRepository->getPastCourseDetailsByInstituteCourseId($course_id,$options);
    }

    public function getSeafarerWithCourseId($course_id,$city_ids=NULL,$options=NULL){
         return $this->userCertificateDetailsRepository->getSeafarerWithCourseId($course_id,$city_ids,$options);
    } 

    public function getInstitutesCoursesByInstituteCourseId($institute_course_id){
         return $this->batchDetailsRepository->getInstitutesCoursesByInstituteCourseId($institute_course_id);
    }

    public function disableCourseByCourseId($course_id){
        $result = $this->batchDetailsRepository->disableCourseByCourseId($course_id);
        if( count($result) > 0 ) {
            $status_arr = ['status' => 'success'];
        } else {
            $status_arr = ['status' => 'error'];
        }
        return $status_arr;
    }

    public function getDetailsByCompanyId($user_id){
        return $this->instituteRegistrationRepository->getDetailsByCompanyId($user_id);
    }

    public function getDetailsById($id){
        return $this->instituteRegistrationRepository->getDetailsById($id);
    }

    public function enableCourseByCourseId($course_id){
        $result = $this->batchDetailsRepository->enableCourseByCourseId($course_id);
        if( count($result) > 0 ) {
            $status_arr = ['status' => 'success'];
        } else {
            $status_arr = ['status' => 'error'];
        }
        return $status_arr;
    }

    public function deleteCourse($course_id,$institute_id){
        $result = $this->batchDetailsRepository->deleteCourse($course_id,$institute_id);
        if( count($result) > 0 ) {
            $status_arr = ['status' => 'success'];
        } else {
            $status_arr = ['status' => 'error'];
        }
        return $status_arr;
    }

    public function storeAdvertise($data,$company_id){

        if(isset($company_id) AND !empty($company_id))
            $company_id = $company_id;
        else
            $company_id = Auth::User()->id;

        $this->advertiseRepository->setAdvertiseStatusDeactivate($company_id);

        $path = env('INSTITUTE_LOGO_PATH');
        $advertise_upload_dir = str_replace(["/","\\"], [DIRECTORY_SEPARATOR,DIRECTORY_SEPARATOR], $path);
        $destination_path = $advertise_upload_dir . $company_id;

        if(!is_dir($destination_path)) {
            mkdir($destination_path, 0777,true);
        }
        if(isset($data['img_obj']) && !empty($data['img_obj'])){
            $extension = '.' . $data['img_obj']->getClientOriginalExtension();
            $destination_filename =  time().'-pic';
            $filename = $destination_filename.$extension;

            $thumbnail = \PhpThumbFactory::create($data['img_obj']);
            $thumbnail->save($destination_path."/".$destination_filename.$extension);
        }

        $advertisement_data['advertisement'] = $filename;

        $data1['company_id'] = $company_id;
        $data1['img_path'] = $filename;
        $data1['status'] = 1;
        $data1['advertisement_as'] = 'institute';
        $data1['ad_updated_on'] = date('Y-m-d H:i:s');
        
        $result = $this->advertiseRepository->storeCompanyAdvertise($data1);

        if($result){
            $status_array = ['status' => 'success'];
        } else {
            $status_array = ['status' => 'failed'];
        }

        return $status_array;
    }

    public function storeNotice($data,$institute_id){
        $result = $this->instituteRegistrationRepository->storeOrUpdateNotice($data,$institute_id);

        if($result){
            $status_array = ['status' => 'success'];
        } else {
            $status_array = ['status' => 'failed'];
        }

        return $status_array;
    }

    public function storeImageGallery($data,$institute_id){
        
        $path = env('INSTITUTE_IMAGE_GALLERY_PATH');
        $advertise_upload_dir = str_replace(["/","\\"], [DIRECTORY_SEPARATOR,DIRECTORY_SEPARATOR], $path);
        $destination_path = $advertise_upload_dir . $institute_id;

        if(!is_dir($destination_path)) {
            mkdir($destination_path, 0777,true);
        }
        if(isset($data['img_obj']) && !empty($data['img_obj'])){
            $extension = '.' . $data['img_obj']->getClientOriginalExtension();
            $destination_filename =  time().'-pic';
            $filename = $destination_filename.$extension;

            $thumbnail = \PhpThumbFactory::create($data['img_obj']);
            $thumbnail->save($destination_path."/".$destination_filename.$extension);
        }

        $data['image'] = $filename;

        $result = $this->instituteRegistrationRepository->storeImageGallery($data,$institute_id);

        if($result){
            $status_array = ['status' => 'success'];
        } else {
            $status_array = ['status' => 'failed'];
        }

        return $status_array;
    }

    public function getInstituteListByCourse($course_type,$course_id,$institute_id=NULL){
        return $this->instituteRegistrationRepository->getInstituteListByCourse($course_type,$course_id,$institute_id);
    }

    public function getExistitngCourseDiscount($filter){
        return $this->instituteRegistrationRepository->getExistitngCourseDiscount($filter);
    }
}