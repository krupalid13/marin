<?php namespace App\Services;

use App\Repositories\UserRepository;
use App\Repositories\CompanyDownloadsRepository;
use App\SeamanBookEnquiry;
use App\UserDangerousCargoEndorsementDetail;
use App\Services\UserPersonalDetailsService;
use App\Services\UserProfessionalDetailsService;
use App\Services\UserCopDetailsService;
use App\Services\UserCoeDetailsService;
use App\Services\SubscriptionService;
use App\Services\CompanyService;
use App\Services\InstituteService;
use App\Services\AdvertiseService;
use App\Services\DocumentService;
use App\Repositories\UserSeaServiceRepository;
use App\Repositories\UserCertificateDetailsRepository;
use App\Repositories\UserDocumentsRepository;
use App\Repositories\ShareContactsRepository;
use App\Repositories\ShareHistoryRepository;
use App\User;
use App\UserCocDetail;
use App\UserCoeDetails;
use App\UserDocuments;
use App\UserDocumentsType;
use App\UserGmdssDetail;
use App\UserProfessionalDetail;
use App\UserSemanBookDetail;
use App\UserWkfrDetail;
use App\UserSeaService;
use App\Http\Controllers\Site\UserController;
use Auth;
use File;
use Password;
use phpDocumentor\Reflection\Types\Null_;
use DB;
use App\UserLanguage;
use Carbon\Carbon;
use App\UserVisaDetail;
use S3;

class UserService
{
    private $userRepository;
    private $userPersonalDetailsService;
    private $userProfessionalDetailsService;
    private $userPassportDetailsService;
    private $userSeamanBookDetailsService;
    private $userCocDetailsService;
    private $userGmdssDetailsService;
    private $userWkfrDetailsService;
    private $sendEmailService;
    private $sendSmsService;
    private $userSeaServiceRepository;
    private $userCertificateDetailsRepository;
    private $userCopDetailsService;
    private $userCoeDetailsService;
    private $subscriptionService;
    private $companyService;
    private $instituteService;
    private $advertiseService;
    private $documentService;
    private $companyDownloadsRepository;
    private $userDocumentsRepository;
    private $shareContactRepository;
    // private $userController;

    public function __construct()
    {
        $this->userRepository = new UserRepository();
        $this->userPersonalDetailsService = new UserPersonalDetailsService();
        $this->userProfessionalDetailsService = new UserProfessionalDetailsService();
        $this->userPassportDetailsService = new UserPassportDetailsService();
        $this->userSeamanBookDetailsService = new UserSeamanBookDetailsService();
        $this->userCocDetailsService = new UserCocDetailsService();
        $this->userGmdssDetailsService = new UserGmdssDetailsService();
        $this->userWkfrDetailsService = new UserWkfrDetailsService();
        $this->sendEmailService = new SendEmailService();
        $this->sendSmsService = new SendSmsService();
        $this->userSeaServiceRepository = new UserSeaServiceRepository();
        $this->userCertificateDetailsRepository = new UserCertificateDetailsRepository();
        $this->userCopDetailsService = new UserCopDetailsService();
        $this->userCoeDetailsService = new UserCoeDetailsService();
        $this->subscriptionService = new SubscriptionService();
        $this->companyService = new CompanyService();
        $this->instituteService = new InstituteService();
        $this->advertiseService = new AdvertiseService();
        $this->documentService = new DocumentService();
        $this->companyDownloadsRepository = new CompanyDownloadsRepository();
        $this->userDocumentsRepository = new UserDocumentsRepository();
        $this->shareContactRepository = new ShareContactsRepository();
        $this->shareHistoryRepository = new ShareHistoryRepository();
        // $this->userController = new UserController();
    }

    public function checkLogin($data){

        if(isset($data['remember']) && $data['remember'] == 'on'){
            $data['remember'] = true;
        }else{
            $data['remember'] = false;
        }

        if (Auth::attempt(['email' => $data['email'], 'password' => $data['password'] , 'registered_as' => $data['type']], $data['remember'])) {
            // Authentication passed...
            return ['status'=>'success'];
        }
        return ['status'=>'failed'];
    }

    public function checkEmail($data){

        if(isset($data) && isset($data['email']) && !empty($data['email'])){
            $result = $this->userRepository->checkEmail($data['email'],$data['id']);
            
            if(count($result) > 0){
                echo json_encode(FALSE);
            }else{
                echo json_encode(TRUE);
            }
        }
    }

    public function checkMobile($data){

        if(isset($data) && isset($data['mobile']) && !empty($data['mobile'])){
            $result = $this->userRepository->checkMobile($data['mobile'],$data['id']);
            
            if(count($result) > 0){
                echo json_encode(FALSE);
            }else{
                echo json_encode(TRUE);
            }
        }
    }

    public function checkUserLogin($data){
        
        if(isset($data['remember']) && $data['remember'] == 'on'){
            $data['remember'] = true;
        }else{
            $data['remember'] = false;
        }
        
        if (Auth::attempt(['email' => $data['email'], 'password' => $data['password']], $data['remember'])) {
            // Authentication passed...
            $user = Auth::User()->toArray();
            $redirect_url = '';
            if(isset($user['registered_as']) AND !empty($user['registered_as'])){
                $role = $user['registered_as'];
                if($role == 'seafarer'){
                    $redirect_url = 'user.profile';
                }
                if($role == 'company'){
                    $redirect_url = 'site.show.company.details';
                }
                if($role == 'advertiser'){
                    $redirect_url = 'site.advertiser.profile';
                }
                if($role == 'institute'){
                    $redirect_url = 'site.show.institute.details';
                }
                if($redirect_url == ''){
                    return ['status'=>'failed'];
                }
            }
            return ['status'=>'success','redirect_url' =>  $redirect_url];
        }
        return ['status'=>'failed'];
    }

    public function getAdminDataByEmail($data){
        return $this->userRepository->getAdminDataByEmail($data);
    }

    public function getDataByUserId($user_id, $options = NULL){
        return $this->userRepository->getDataByUserId($user_id,$options);
    }

    public function changeStatus($data){
        if(isset($data['status']) && isset($data['id'])){
            $result = $this->userRepository->changeStatus($data['status'],$data['id']);
        }

        if($result){
            $status_array =['status'=>'success'];
        }
        else{
            $status_array =['status'=>'failed'];
        }
        return $status_array;
    }

    public function store($userData, $logged_user_data = NULL) {
        
        // print_r($userData);die;
        $india_value = array_search('India',\CommonHelper::countries());
        $user_data['where_in']= !empty($userData['where_in']) ? $userData['where_in'] : null;
        if(isset($userData['firstname']))
            $user_data['first_name'] = $userData['firstname'];
        //$user_data['last_name'] = $userData['lastname'];

        if(isset($userData['gender']))
            $user_data['gender'] = $userData['gender'];

        $user_data['email'] = $userData['email'];
        $user_data['mobile'] = $userData['mobile'];
        $user_data['country_code'] = $userData['country_code'] ?? '91';
        $user_data['PPMname'] = $userData['PPMname'] ?? null;
        $user_data['title'] = $userData['title'] ?? null;
        $user_data['PPLname'] = $userData['PPLname'] ?? null;
        $user_data['shiphometype'] = $userData['home_ship_type'] ?? null;
        $user_data['registered_as'] = 'seafarer';
        $languages = array();
        $languages = $userData['language'] ?? 'en';
        $user['update_email'] = 0;
        $user['update_mobile'] = 0;
        $user['update'] = 0;

        if(isset($userData['added_by'])){
            $user_data['added_by'] = $userData['added_by'];
           // $this->sendEmailService->sendVerificationEmailToUser($user_data);

        }
        if(isset($userData['registered_as'])){
            $user_data['registered_as'] = $userData['registered_as'];
        }

        if(isset($userData['password']))
            $user_data['password'] = bcrypt($userData['password']);

        if(isset($userData['otp']))
            $user_data['otp'] = $userData['otp'];



        if(isset($logged_user_data['id']) && $logged_user_data['id']!=''){
            $user['id'] = $logged_user_data['id'];
            $user_data['id'] = $logged_user_data['id'];

            UserLanguage::where('user_id',$user['id'])->delete();
            if(!empty($languages)){
                foreach ($languages as $language) {
                    if(!empty($language['name'])){
                        $canread = $canspeak = '0';
                        if(isset($language['can_read']) && $language['can_read'] == '1'){
                            $canread = '1';
                        }
                        if(isset($language['can_speak']) && $language['can_speak'] == '1'){
                            $canspeak = '1';
                        }
                        $lang = New UserLanguage;
                        $lang->user_id = $user['id'];
                        $lang->language = $language['name'];
                        $lang->can_read = $canread;
                        $lang->can_speak = $canspeak;
                        $lang->save();
                    }
                }
            }

            if(isset($userData['added_by']) AND $userData['added_by'] == 'admin'){

            }else{
                $user_data['welcome_email'] = 1;
                if(isset($userData['country'][0]) && $userData['country'][0] == $india_value){
                    $user_data['welcome_sms'] = 1;
                }
            }
           // dd($logged_user_data['email']); die;
            if(isset($logged_user_data['email']) && $logged_user_data['email'] != $userData['email']){
                //if email changed from Profile
                $user_data['is_email_verified'] = 0;
                $user['update_email'] = 1;
                $this->sendEmailService->sendVerificationEmailToUser($user_data);
            }

            if(isset($logged_user_data['is_mob_verified']) && $logged_user_data['is_mob_verified'] == '0'){
                // $this->sendSmsService->sendMobileVerifySms($user_data['mobile'],$user_data['otp']);
            }else{
                if(isset($logged_user_data['mobile']) && $logged_user_data['mobile'] != $userData['mobile']){
                    //if mobile changed from Profile
                    $user_data['is_mob_verified'] = 1;
                    $user['update_mobile'] = 1;
                    // $this->sendSmsService->sendMobileVerifySms($user_data['mobile'],$user_data['otp']);
                }
            }
            $user_data['is_mob_verified'] = 1;
            $this->storeUpdatedOnForUsers($user['id']);
            $this->userRepository->update($user_data,$user['id']);

            if(isset($userData['added_by']) AND $userData['added_by'] == 'admin'){

            }else{
                if(isset($logged_user_data['welcome_email']) && $logged_user_data['welcome_email'] == 0){
                    $this->sendEmailService->sendEmailToNewRegisteredUser($user_data);
                    $this->sendEmailService->sendVerificationEmailToUser($user_data);
                }

                if(isset($userData['country'][0]) && $userData['country'][0] == $india_value){
                    if(isset($logged_user_data['welcome_sms']) && $logged_user_data['welcome_sms'] == 0){
                        $this->sendSmsService->sendSmsToNewRegistered($user_data);
                        // $this->sendSmsService->sendMobileVerifySms($user_data['mobile'],$user_data['otp']);
                    }
                }
            }
        }
        else{
            if(isset($userData['added_by']) AND $userData['added_by'] == 'admin'){
                $user_data['welcome_email'] = 1;
                if(isset($userData['country'][0]) && $userData['country'][0] == $india_value){
                    $user_data['welcome_sms'] = 1;
                }
            }else{
                $user_data['welcome_email'] = 1;
                if(isset($userData['country'][0]) && $userData['country'][0] == $india_value){
                    $user_data['welcome_sms'] = 1;
                }
            }
			$user_data['is_mob_verified'] = 1;
            $user = $this->userRepository->store($user_data);


            if(isset($user) && !empty($user['id'])){
                $user_data['id'] = $user['id'];
                $this->storeUpdatedOnForUsers($user['id']);
            }

            if(isset($userData['added_by']) AND $userData['added_by'] == 'admin'){
                //welcome email with set password
                $new_user_data = Auth::user()->find($user->id);
                $this->sendWelcomeEmailWithPasswordResetMail($new_user_data);
                if(isset($userData['country'][0]) && $userData['country'][0] == $india_value){
                    $this->sendSmsService->sendSmsToNewRegistered($user_data);
                }
            }else{
                if(isset($userData['country'][0]) && $userData['country'][0] == $india_value){
                    $this->sendEmailService->sendEmailToNewRegisteredUser($user_data);
                    $this->sendEmailService->sendVerificationEmailToUser($user_data);
                    $this->sendSmsService->sendSmsToNewRegistered($user_data);
                    // $this->sendSmsService->sendMobileVerifySms($user_data['mobile'],$user_data['otp']);
                }
            }
        }
        //for pre registration of seafarer
        if(isset($userData['pre_registration'])){
            Auth::login($user);

            if(isset($userData['current_rank']) && !empty($userData['current_rank'])){
                $user_professional_details['current_rank'] = $userData['current_rank'];
                $user_professional_details['current_rank_exp'] = '0.0';
                $user_professional_details['user_id'] = $user['id'];
                $this->userProfessionalDetailsService->store($user_professional_details);
            }

            if(isset($userData['passport']) && !empty($userData['passport'])){
                $user_passport_details['user_id'] = $user['id'];
                $user_passport_details['pass_number'] = $userData['passport'];
                $this->userPassportDetailsService->store($user_passport_details);
            }

            if(isset($userData['indosno']) && !empty($userData['indosno'])){
                $user_wkfr_details['user_id'] = $user['id'];
                $user_wkfr_details['indos_number'] = $userData['indosno'];
                $this->userWkfrDetailsService->store($user_wkfr_details);
            }
            $user_personal_data['user_id'] = $user['id'];
            $user_personal_data['dob'] = date('Y-m-d',strtotime($userData['dob']));
            $user_personal_data['nationality'] =$userData['nationality'];
            $this->userPersonalDetailsService->store($user_personal_data);
            $status_array = ['status'=>'success' , 'user'=> $user];
            return $status_array;
        }

        if(isset($userData['uploaded-file-name']) && !empty($userData['uploaded-file-name'])){
            $user_data['profile_pic'] = $userData['uploaded-file-name'] ;

            $storage_path = env('SEAFARER_PROFILE_PATH','public/images/uploads/seafarer_profile_pic/');

            $from = str_replace(["/","\\"], [DIRECTORY_SEPARATOR,DIRECTORY_SEPARATOR], $storage_path.'temp/'.$user_data['profile_pic']);

            if(file_exists($from)){
                $to = str_replace(["/","\\"], [DIRECTORY_SEPARATOR,DIRECTORY_SEPARATOR], $storage_path. $user['id']).DIRECTORY_SEPARATOR;

                if(!is_dir($to)) {
                    mkdir($to, 0777,true);
                }
                $to = $to.$user_data['profile_pic'];
                if(copy($from, $to)) {
                    unlink($from);
                }
                $this->userRepository->update($user_data,$user['id']);
            }
        }

        if(isset($userData['registered_as']) && $userData['registered_as'] == 'advertiser'){
            if(!Auth::check())
                Auth::loginUsingId($user['id']);

            $status_array =['status'=>'success' , 'user'=> $user];
            return $status_array;
        }

        if (count($user) > 0 && isset($userData['place_of_birth'])){
            $user_personal_data['dob'] = date('Y-m-d',strtotime($userData['dob']));
            $user_personal_data['place_of_birth'] =$userData['place_of_birth'];
            if(!empty($userData['nationality'])){
                $user_personal_data['nationality'] = $userData['nationality'];
            }
            $user_personal_data['marital_status'] =$userData['marital_status'];
            $user_personal_data['height'] =$userData['height'];
            $user_personal_data['weight'] =$userData['weight'];           
            $user_personal_data['permanent_add'] =$userData['permananent_address'];
            $user_personal_data['country'] = $userData['country'][0];
            $user_personal_data['pincode_text'] = $userData['pincode'][0];

            if(isset($userData['permananent_address2']) && !empty($userData['permananent_address2']))
                $user_personal_data['permanent_add2'] =$userData['permananent_address2'];
            else
                $user_personal_data['permanent_add2'] = '';

            if(isset($userData['nearest_place']) && !empty($userData['nearest_place']))
                $user_personal_data['nearest_place'] =$userData['nearest_place'];
            else
                $user_personal_data['nearest_place'] = '';

            if(isset($userData['kin_alternate_no']) && !empty($userData['kin_alternate_no']))
                $user_personal_data['kin_alternate_no'] =$userData['kin_alternate_no'];           

            if(isset($userData['landline']) && !empty($userData['landline']))
                 $user_personal_data['landline'] = $userData['landline'];

            if(isset($userData['landline_code']) && !empty($userData['landline_code']))
                $user_personal_data['landline_code'] = $userData['landline_code'];

            if(isset($userData['kin_number']) && !empty($userData['kin_number']))
                $user_personal_data['kin_number'] = $userData['kin_number'];

           
            if(isset($userData['pincode_id'][0]) && !empty($userData['pincode_id'][0]))
                 $user_personal_data['pincode_id'] = $userData['pincode_id'][0];

            $india_value = array_search('India',\CommonHelper::countries());

            if($userData['country'][0] == $india_value ){
                $user_personal_data['state_id'] = $userData['state'][0];
                $user_personal_data['city_id'] = $userData['city'][0];
                $user_personal_data['state_text'] = NULL;
                $user_personal_data['city_text'] = NULL;
            }else{
                $user_personal_data['state_id'] = NULL;
                $user_personal_data['city_id'] = NULL;
                $user_personal_data['state_text'] = $userData['state_name'][0];
                $user_personal_data['city_text'] = $userData['city_text'][0];
            }
            // print_r($user_personal_data);die;

            if(Auth::check()){
                $check_user_personal_exist = $this->userPersonalDetailsService->getDetailsByUserId($user['id'])->toArray();

                if(empty($check_user_personal_exist)){
                    $user_personal_data['user_id'] = $user['id'];
                    $this->userPersonalDetailsService->store($user_personal_data);
                }else{
                    $this->userPersonalDetailsService->updateByUserId($user_personal_data,$user['id']);
                }
            }else {
                $user_personal_data['user_id'] = $user['id'];
                $this->userPersonalDetailsService->store($user_personal_data);
            }
            //if current rank update
            if(UserProfessionalDetail::whereUserId($user['id'])->first()->current_rank != $userData['current_rank']) {
                $user['update']=1;
                // dd("hetetetettetee");
                UserCoeDetails::whereUserId($user['id'])->delete();
                UserGmdssDetail::whereUserId($user['id'])->delete();
                UserCocDetail::whereUserId($user['id'])->delete();
                //$_for_docs=UserDocumentsType::whereUserId($user['id'])->whereIn('type',['wk_cop','gmdss','cdc','coc','passport'])->pluck('id');
                UserDocumentsType::whereUserId($user['id'])->whereIn('type',['wk_cop','gmdss','coc','coe'])->delete();

                if (!empty($userData['current_rank']) && !in_array('DCE-Optional', \CommonHelper::rank_required_fields()[$userData['current_rank']]) && !in_array('DCE', \CommonHelper::rank_required_fields()[$userData['current_rank']])) {
                    $types = ['oil', 'lequefied_gas', 'chemical', 'all'];
                    $userId = Auth::user()->id;
                    UserController::destroyUserDocuments($types, $userId);
                    UserDangerousCargoEndorsementDetail::whereUserId($userId)->delete();
                }

                $user_wk_cop=UserWkfrDetail::whereUserId($user['id']);
                $user_wk_cop->update([
                    'watch_keeping' => "",
                    'type' => "",
                    'wkfr_number' => "",
                    'issue_date' => null,
                    'wk_cop' => ''
                ]);
            }
            $user_professional_details['current_rank'] = $userData['current_rank'];
            $user_professional_details['current_rank_exp'] = $userData['years'].".".$userData['months'];
            $user_professional_details['about_me'] = $userData['about_me'];

            $user_professional_details['applied_rank'] = $userData['applied_rank'];
            $user_professional_details['availability'] = date('Y-m-d',strtotime($userData['date_avaibility']));

            if(isset($userData['last_wages'])){
                $user_professional_details['last_salary'] = $userData['last_wages'];
                $user_professional_details['currency'] = $userData['wages_currency'];
            }


            if( Auth::check()){
                $check_user_professional_exist = $this->userProfessionalDetailsService->getDetailsByUserId($user['id'])->toArray();

                if(empty($check_user_professional_exist)){
                    $user_professional_details['user_id'] = $user['id'];
                    $this->userProfessionalDetailsService->store($user_professional_details);
                }else{
                    $this->userProfessionalDetailsService->updateByUserId($user_professional_details,$user['id']);
                }

            }else {
                $user_professional_details['user_id'] = $user['id'];
                $this->userProfessionalDetailsService->store($user_professional_details);
                if(isset($userData['added_by']) AND $userData['added_by'] == 'admin'){

                }else{
                    Auth::login($user);
                }

            }
            $status_array =['status'=>'success' , 'user'=> $user];

        }else{
            $status_array =['status'=>'failed'];

        }
        return $status_array;
    }

    public function storeDocuments($userData,$user_id=NULL){

        if( Auth::check()){
            if(!isset($user_id) && $user_id == ''){
                $user = Auth::user();
                $user_id = $user->id;
                $user['id'] = $user->id;
            }else{
                $user = $this->userRepository->getDataByUserId($user_id);
            }

            

            $this->storeUpdatedOnForUsers($user_id);

            $user_passport_details['user_id'] = $user_id;
            $user_passport_details['pass_country'] = $userData['passcountry'];
            $user_passport_details['pass_number'] = $userData['passno'];
            $user_passport_details['place_of_issue'] = $userData['passplace'];
            $user_passport_details['pass_expiry_date'] = date('Y-m-d',strtotime($userData['passdateofexp']));
            $user_passport_details['pass_issue_date'] = date('Y-m-d',strtotime($userData['passdateofissue']));
           // $user_passport_details['us_visa'] = $userData['us'];

            if(isset($userData['us']) && !empty($userData['us']) && $userData['us'] == '1')
                $user_passport_details['us_visa_expiry_date'] = date('Y-m-d',strtotime($userData['usvalid']));
            else
                $user_passport_details['us_visa_expiry_date'] = NULL;

            $visas = $userData['visa'] ?? [];
            
            UserVisaDetail::where('user_id',$user['id'])->delete();
            if(!empty($visas)){
                foreach ($visas as $visa) {
                    if(isset($visa['expiry_date']) && !empty($visa['expiry_date']) && isset($visa['country']) && !empty($visa['country']) && isset($visa['visa_type']) && !empty($visa['visa_type'])){
                        $lang = New UserVisaDetail;
                        $lang->user_id = $user['id'];
                        $lang->country_id = $visa['country'];
                        $lang->visa_type = $visa['visa_type'];
                        $lang->visa_expiry_date = date('Y-m-d',strtotime($visa['expiry_date']));
                        $lang->save();
                    }
                    
                }
            }

            $user_passport_details['fromo'] = isset($userData['fromo']) && $userData['fromo'] == '1' ? '1' : '0';

            $this->userPassportDetailsService->store($user_passport_details);

//            $this->userSeamanBookDetailsService->deleteServiceById($user_id);
            
            $userSemanBookDetails = $this->userSeamanBookDetailsService->getDetailsByUserId($user_id);
            $deletedUserSemanBookDetailsIds = [];
            foreach ($userSemanBookDetails as $userSemanBookDetail){
                $deletedUserSemanBookDetailsIds[] = $userSemanBookDetail['id'];
            }

            $i=1;
            foreach($userData["cdccountry"] as $index => $cdc_data){

                $user_seaman_book_details['user_id'] = $user_id;
                $user_seaman_book_details['cdc'] = $userData["cdccountry"][$index];
                $user_seaman_book_details['cdc_number'] = $userData["cdcno"][$index];
                $user_seaman_book_details['cdc_issue_date'] = date('Y-m-d',strtotime($userData['cdc_issue'][$index]));
                $user_seaman_book_details['cdc_expiry_date'] = date('Y-m-d',strtotime($userData['cdc_exp'][$index]));
                $user_seaman_book_details['sequence'] = $index;

                if(isset($userData['cdc_status'][$index]) && !empty($userData['cdc_status'][$index])){
                    $user_seaman_book_details['status'] = $userData['cdc_status'][$index];
                }else{
                    $user_seaman_book_details['status'] = 0;
                }

                if(isset($userData['cdc_ver'][$index]) && !empty($userData['cdc_ver'][$index]))
                    $user_seaman_book_details['cdc_verification_date'] = date('Y-m-d',strtotime($userData['cdc_ver'][$index]));
                else
                    $user_seaman_book_details['cdc_verification_date'] = NULL;
                
                if(isset($userData["cdc_id"][$index]) && !empty($userData["cdc_id"][$index])){
                    $_unique_id = $userData["cdc_id"][$index];
                    $this->userSeamanBookDetailsService->updateBySeamanId($user_seaman_book_details, $userData["cdc_id"][$index]);
                    $deleteKey = array_search($_unique_id, $deletedUserSemanBookDetailsIds);
                    if ($deleteKey !== false) {
                        unset($deletedUserSemanBookDetailsIds[$deleteKey]);
                    }
                } else {
                    $_unique_id = $this->userSeamanBookDetailsService->store($user_seaman_book_details);
                }
//                $_unique_id= $user_seaman_book_details['cdc_issue_date'].$user_seaman_book_details['cdc_expiry_date'];
                $_unique_id_array[]=$_unique_id;
				$cdc_count = DB::table('user_documents_type')->where('user_id', $user_id)->where('type', 'cdc')->where('type_id',$_unique_id)->count();
				if(!$cdc_count)
				{
					DB::table('user_documents_type')->insert(['user_id'=>$user_id,'type_id'=>$_unique_id,'order'=>0,'type' => 'cdc','status'=>1]);
				}
				$i++;
            }
            if(!empty($deletedUserSemanBookDetailsIds)){
                $this->userSeamanBookDetailsService->deleteServiceBySeamanIds($deletedUserSemanBookDetailsIds);
            }
            $previosCdcDcoumentsTypes = UserDocumentsType::whereUserId($user_id)->whereType('cdc')->whereNotIn('type_id',$_unique_id_array)->with('user_documents')->get();
            $this->destroyCdcDocuments($previosCdcDcoumentsTypes);
            is_null(UserDocumentsType::whereUserId($user_id)->whereType('cdc')->whereNotIn('type_id',$_unique_id_array)->get());


            $userCoeDetails = UserCoeDetails::whereUserId($user_id)->get();
            if (count($userCoeDetails) > 0) {
                if ($userCoeDetails[0]->type == 1 && $userData['coe_radio'] == 0) {
                    $typeId = [];
                    foreach($userCoeDetails as $key => $value) {
                        $typeId[$key] = $value->coe_number . $value->coe_grade;
                    }

                    $userDcoumentsTypes = UserDocumentsType::whereUserId($user_id)->whereType('coe')->whereIn('type_id',$typeId)->with('user_documents')->get();
                    $this->destroyCoeDocuments($userDcoumentsTypes);
                }
            }
            
            $userCoeDetails = $this->userCoeDetailsService->getDetailsByUserId($user_id);
            $deletedUserCoeDetailsIds = [];
            foreach ($userCoeDetails as $userCoeDetail){
                $deletedUserCoeDetailsIds[] = $userCoeDetail['id'];
            }
            
//            $this->userCoeDetailsService->deleteServiceById($user_id);
            if(($userData['coe_radio'] == 1)){
                $i=1;

                foreach($userData["coecountry"] as $index => $coe_data){
                    $user_coe_details['user_id'] = $user_id;
                    $user_coe_details['coe'] = $userData["coecountry"][$index];
                    $user_coe_details['coe_number'] = $userData["coeno"][$index];
                    $user_coe_details['coe_grade'] = $userData['coe_grade'][$index];
                    $user_coe_details['sequence'] = $index;

                    if(isset($userData['coe_exp'][$index]) && !empty($userData['coe_exp'][$index]))
                        $user_coe_details['coe_expiry_date'] = date('Y-m-d',strtotime($userData['coe_exp'][$index]));
                    else
                        $user_coe_details['coe_expiry_date'] = NULL;

                    if(isset($userData['coe_ver'][$index]) && !empty($userData['coe_ver'][$index]))
                        $user_coe_details['coe_verification_date'] = date('Y-m-d',strtotime($userData['coe_ver'][$index]));
                    else
                        $user_coe_details['coe_verification_date'] = NULL;

                    
                    if (isset($userData["coe_id"][$index]) && !empty($userData["coe_id"][$index])) {
                        $_unique_id = $userData["coe_id"][$index];
                        $this->userCoeDetailsService->updateByCoeId($user_coe_details, $userData["coe_id"][$index]);
                        $deleteKey = array_search($_unique_id, $deletedUserCoeDetailsIds);
                        if ($deleteKey !== false) {
                            unset($deletedUserCoeDetailsIds[$deleteKey]);
                        }
                    } else {
                        $_unique_id = $this->userCoeDetailsService->store($user_coe_details);
                    }

                    $_unique_id_coe=$user_coe_details['coe_number'].$user_coe_details['coe_grade'];
                    $_unique_id_coe_array[]=$_unique_id;
                    $coe_count = DB::table('user_documents_type')->where('user_id', $user_id)->where('type', 'coe')->where('type_id',$_unique_id_coe)->count();
                    if(!$coe_count)
                    {
                        DB::table('user_documents_type')->insert(['user_id'=>$user_id,'type_id'=>$_unique_id_coe,'order'=>0,'type' => 'coe','status'=>1]);
                    }
                    $i++;
                }
                
                if(!empty($deletedUserCoeDetailsIds)){
                    $this->userCoeDetailsService->deleteServiceByCoeIds($deletedUserCoeDetailsIds);
                }
                $previosCoeDcoumentsTypes = UserDocumentsType::whereUserId($user_id)->whereType('coe')->whereNotIn('type_id',$_unique_id_coe_array)->with('user_documents')->get();
                $this->destroyCdcDocuments($previosCoeDcoumentsTypes);
                
//                if (!empty($_unique_id_coe_array)) {
//                    $userDcoumentsTypes = UserDocumentsType::whereUserId($user_id)->whereType('coe')->whereNotIn('type_id',$_unique_id_coe_array)->with('user_documents')->get();
//                    $this->destroyCoeDocuments($userDcoumentsTypes);
//                }
            }else{
                if(!empty($deletedUserCoeDetailsIds)){
                    $this->userCoeDetailsService->deleteServiceByCoeIds($deletedUserCoeDetailsIds);
                    $previosCoeDcoumentsTypes = UserDocumentsType::whereUserId($user_id)->whereType('coe')->whereIn('type_id',$deletedUserCoeDetailsIds)->with('user_documents')->get();
                    $this->destroyCdcDocuments($previosCoeDcoumentsTypes);
                }
            }

            $this->userCopDetailsService->deleteServiceById($user_id);

            if(!empty($userData["cop_grade"][$index]) || !empty($userData["cop_no"][$index]) || !empty($userData["cop_issue_date"][$index]) || !empty($userData["cop_exp_date"][$index])){
                foreach($userData['cop_no'] as $index => $cop_data){
                    $user_cop_details['user_id'] = $user_id;

                    if(!empty($userData["cop_grade"][$index]))
                        $user_cop_details['cop_grade'] = $userData["cop_grade"][$index];

                    if(!empty($userData["cop_no"][$index]))
                        $user_cop_details['cop_number'] = $userData["cop_no"][$index];

                    if(!empty($userData['cop_issue'][$index]))
                        $user_cop_details['cop_issue_date'] = date('Y-m-d',strtotime($userData['cop_issue'][$index]));
                    else
                        $user_cop_details['cop_issue_date'] = NULL;

                    if(!empty($userData['cop_exp'][$index]))
                        $user_cop_details['cop_exp_date'] = date('Y-m-d',strtotime($userData['cop_exp'][$index]));
                    else
                        $user_cop_details['cop_exp_date'] = NULL;

                    $this->userCopDetailsService->store($user_cop_details);
                }
            }



//            $this->userCocDetailsService->deleteServiceById($user_id);
            
            $userCocDetails = $this->userCocDetailsService->getDetailsByUserId($user_id);
            $deletedUserCocDetailsIds = [];
            foreach ($userCocDetails as $userCocDetail){
                $deletedUserCocDetailsIds[] = $userCocDetail['id'];
            }
            
            $i=1;
            
            foreach ($userData['coc_country'] as $index => $coc_data) {
                $user_coc_details['user_id'] = $user_id;
                $user_coc_details['coc'] = $userData["coc_country"][$index];
                $user_coc_details['coc_number'] = $userData["coc_no"][$index];
                $user_coc_details['coc_grade'] = $userData["grade"][$index];
                if (!empty($userData['coc_exp'][$index]))
                    $user_coc_details['coc_expiry_date'] = date('Y-m-d', strtotime($userData['coc_exp'][$index]));
                else
                    $user_coc_details['coc_expiry_date'] = NULL;

                if (isset($userData['coc_status'][$index]) and ! empty($userData['coc_status'][$index])) {
                    $user_coc_details['status'] = $userData['coc_status'][$index];
                } else {
                    $user_coc_details['status'] = 0;
                }
                $user_coc_details['sequence'] = $index;

                if (isset($userData['coc_ver'][$index]) && !empty($userData['coc_ver'][$index]))
                    $user_coc_details['coc_verification_date'] = date('Y-m-d', strtotime($userData['coc_ver'][$index]));
                else
                    $user_coc_details['coc_verification_date'] = NULL;

                if (isset($userData["coc_id"][$index]) && !empty($userData["coc_id"][$index])) {
                    $_unique_id = $userData["coc_id"][$index];
                    $this->userCocDetailsService->updateByCocId($user_coc_details, $userData["coc_id"][$index]);
                    $deleteKey = array_search($_unique_id, $deletedUserCocDetailsIds);
                    if ($deleteKey !== false) {
                        unset($deletedUserCocDetailsIds[$deleteKey]);
                    }
                } else {
                    $_unique_id = $this->userCocDetailsService->store($user_coc_details);
                }
                $_unique_id_coc = $user_coc_details['coc_number'];
                $_unique_id_coc_array[] = $_unique_id;
                $coc_count = DB::table('user_documents_type')->where('user_id', $user_id)->where('type', 'coc')->where('type_id', $_unique_id_coc)->count();
                if (!$coc_count) {
                    DB::table('user_documents_type')->insert(['user_id' => $user_id, 'type_id' => $_unique_id_coc, 'order' => 0, 'type' => 'coc', 'status' => 1]);
                }

                $i++;
            }
            if(!empty($deletedUserCocDetailsIds)){
                $this->userCocDetailsService->deleteServiceByCocIds($deletedUserCocDetailsIds);
            }
            $previosCocDcoumentsTypes = UserDocumentsType::whereUserId($user_id)->whereType('coc')->whereNotIn('type_id',$_unique_id_coc_array)->with('user_documents')->get();
            $this->destroyCdcDocuments($previosCocDcoumentsTypes);
            is_null(UserDocumentsType::whereUserId($user_id)->whereType('coc')->whereNotIn('type_id',$_unique_id_coc_array)->delete());


            // dd($userData);

            $userGmdssDetails = UserGmdssDetail::whereUserId($user_id)->first();

            if (in_array('GMDSS-Optional', \CommonHelper::rank_required_fields()[$userData['current_rank_id']]) && $userGmdssDetails != null && $userData['gmdss_radio'] == 0) {
                UserGmdssDetail::whereUserId($user_id)->delete();
                $userDocumentsTypes = UserDocumentsType::whereUserId($user_id)->whereType('gmdss')->with('user_documents')->get();
                if(!empty($userDocumentsTypes)){
                    $this->destroyCdcDocuments($userDocumentsTypes);
                }
                // dd($userDocumentsTypes);
//                if ($userDocumentsTypes != null) {
//
//                    $totalSize = 0;
//                    if (count($userDocumentsTypes->user_documents) > 0) {
//                        foreach (File::allFiles(public_path('uploads/user_documents/' . $user_id . '/gmdss')) as $file) {
//                            $totalSize += $file->getSize();
//                        }
//                    }
//    
//                    $folderPath = public_path('uploads/user_documents/' . $user_id . '/gmdss');
//                    if (File::exists($folderPath)) {
//                        File::deleteDirectory($folderPath);
//                    }
//    
//                    $userDocuments = ($userDocumentsTypes->user_documents)->pluck('id');
//                    if (count($userDocuments) > 0) {
//                        foreach($userDocuments as $key => $documentId) {
//                            UserDocuments::whereId($documentId)->delete();
//                        }
//                    }
//                    UserDocumentsType::whereId($userDocumentsTypes->id)->delete();
//        
//                    $oldUsedKb = Auth::user()->used_kb;
//                    User::whereId($user_id)->update([
//                        'used_kb' => (int)($oldUsedKb - ((int)($totalSize / 1024)))
//                    ]);
//                }

            }


            if (in_array('GMDSS-Optional', \CommonHelper::rank_required_fields()[$userData['current_rank_id']]) && $userData['gmdss_radio'] == 1) {
                
                $india_value = array_search('India',\CommonHelper::countries());

                $user_gmdss_details['user_id'] = $user_id;
                $user_gmdss_details['gmdss'] = $userData["gmdss_country"];
                $user_gmdss_details['gmdss_number'] = $userData["gmdss_no"];

                if(!empty($userData['gmdss_doe']))
                    $user_gmdss_details['gmdss_expiry_date'] = date('Y-m-d',strtotime($userData['gmdss_doe']));
                else
                    $user_gmdss_details['gmdss_expiry_date'] = NULL;



                if(!empty($userData['gmdss_valid_till']) && $userData["gmdss_country"] == $india_value)
                    $user_gmdss_details['gmdss_endorsement_expiry_date'] = date('Y-m-d',strtotime($userData['gmdss_valid_till']));
                else
                    $user_gmdss_details['gmdss_endorsement_expiry_date'] = NULL;



                if(!empty($userData['gmdss_endorsement']) && $userData["gmdss_country"] == $india_value)
                    $user_gmdss_details['gmdss_endorsement_number'] = $userData['gmdss_endorsement'];
                else
                    $user_gmdss_details['gmdss_endorsement_number'] = NULL;


                $userGmdssDetail = new UserGmdssDetail;
                $oldGmdssDetails = UserGmdssDetail::whereUserId($user_id)->first();
                if ($oldGmdssDetails != null) {
                    $userGmdssDetail = $oldGmdssDetails;
                }
                $userGmdssDetail->user_id = $user_id;
                $userGmdssDetail->gmdss = $userData["gmdss_country"];
                $userGmdssDetail->gmdss_number = $userData["gmdss_no"];
                $userGmdssDetail->gmdss_expiry_date = $user_gmdss_details['gmdss_expiry_date'];
                $userGmdssDetail->gmdss_endorsement_number = $user_gmdss_details['gmdss_endorsement_number'];
                $userGmdssDetail->gmdss_endorsement_expiry_date = $user_gmdss_details['gmdss_endorsement_expiry_date'];
                // dd($userGmdss)
                $userGmdssDetail->save();

                    // dd($user_gmdss_details);
                // $this->userGmdssDetailsService->store($user_gmdss_details);

            }

            $user_wkfr_details['user_id'] = $user_id;
            $user_wkfr_details['watch_keeping'] = $userData["watch_country"];
            $user_wkfr_details['type'] = $userData["deckengine"];
            $user_wkfr_details['wkfr_number'] = $userData["watch_no"];
            if(isset($userData["indosno"])){
                $user_wkfr_details['indos_number'] = $userData["indosno"];
            }else{
                $user_wkfr_details['indos_number'] = null;
            }
            
            if (isset($userData['sid_number']) && isset($userData['sid_issue_date']) && isset($userData['sid_expire_date']) && isset($userData['issue_place'])) {
                $dataProfessional['sid_number'] = $userData['sid_number'];
                $dataProfessional['sid_issue_date'] = '';
                if (!empty($userData['sid_issue_date'])) {
                    $dataProfessional['sid_issue_date'] = date('Y-m-d', strtotime($userData['sid_issue_date']));
                }
                $dataProfessional['sid_expire_date'] = '';
                if (!empty($userData['sid_expire_date'])) {
                    $dataProfessional['sid_expire_date'] = date('Y-m-d', strtotime($userData['sid_expire_date']));
                }

                $dataProfessional['issue_place'] = $userData['issue_place'];
                DB::table('user_professional_details')
                        ->where('user_id', $user_id)
                        ->limit(1)
                        ->update($dataProfessional);
                $docsType = UserDocumentsType::where('type','sid')->where('user_id',$user_id)->first();
                if(empty($docsType)){
                    DB::table('user_documents_type')->insert(array(
                        array('user_id' => $user_id, 'type' => 'sid')
                    ));
                }
            }

            //$user_wkfr_details['yellow_fever'] = $userData["yellowfever"];

            if(isset( $userData["wk_status"]) && !empty($userData["wk_status"]))
                $user_wkfr_details['status'] = $userData["wk_status"];

            /*if(!empty($userData['yf_issue_date']) && $userData["yellowfever"] == 1)
                $user_wkfr_details['yf_issue_date'] = date('Y-m-d',strtotime($userData['yf_issue_date']));
            else
                $user_wkfr_details['yf_issue_date'] = NULL;*/

            if(!empty($userData['watchissud']))
                $user_wkfr_details['issue_date'] = date('Y-m-d',strtotime($userData['watchissud']));
            else
                $user_wkfr_details['issue_date'] = NULL;

            $this->userWkfrDetailsService->store($user_wkfr_details);

            $status_array =['status'=>'success' , 'user'=> $user];
        }else{
            $status_array =['status'=>'failed'];
        }
        return $status_array;
    }


    public function destroyCoeDocuments($userDcoumentsTypes) {
        $userId = Auth::user()->id;
        if (count($userDcoumentsTypes) > 0) {
            foreach ($userDcoumentsTypes as $key => $userDcoumentsType) {
                $totalSize = 0;
                if (count($userDcoumentsType->user_documents) > 0) {
                    foreach (File::allFiles(public_path('uploads/user_documents/' . $userId . '/coe' . '/' . $userDcoumentsType->type_id)) as $file) {
                        $totalSize += $file->getSize();
                    }
                }

                $folderPath = public_path('uploads/user_documents/' . $userId . '/coe' . '/' . $userDcoumentsType->type_id);
                if (File::exists($folderPath)) {
                    File::deleteDirectory($folderPath);
                }

                $userDocuments = ($userDcoumentsType->user_documents)->pluck('id');
                if (count($userDocuments) > 0) {
                    foreach($userDocuments as $key => $documentId) {
                        UserDocuments::whereId($documentId)->delete();
                    }
                }
                UserDocumentsType::whereId($userDcoumentsType->id)->delete();
            }

            $oldUsedKb = Auth::user()->used_kb;
            User::whereId($userId)->update([
                'used_kb' => (int)($oldUsedKb - ((int)($totalSize / 1024)))
            ]);
        }
    }
    
    public function destroyCdcDocuments($userDcoumentsTypes) {
        $userId = Auth::user()->id;
        if (count($userDcoumentsTypes) > 0) {
            foreach ($userDcoumentsTypes as $key => $userDcoumentsType) {
                $totalSize = 0;
                if (count($userDcoumentsType->user_documents) > 0) {
                    foreach ($userDcoumentsType->user_documents as $userDocuments) {
                        $this->documentService->deleteDocument($userDocuments->id, $userDocuments->document_path, $userId, $userDcoumentsType->type, $userDcoumentsType->type_id);
                    }
                }
                UserDocumentsType::whereId($userDcoumentsType->id)->delete();
            }
        }
    }

    public function getDetailByMobileAndOtp($mobile,$otp){
        return $this->userRepository->getDetailByMobileAndOtp($mobile,$otp);
    }

    public function markMobileVerified($user_id){
        return $this->userRepository->markMobileVerified($user_id);
    }

    public function verifyMobile($data){
        $detail = $this->getDetailByMobileAndOtp($data['mobile'],$data['mob_otp'])->toArray();
        if(count($detail) > 0){
            $result = $this->markMobileVerified($data['user_id']);
            if($result > 0){
                $status_array =['status'=>'success'];
            }else{
                $status_array =['status'=>'error'];
            }
        }else{
            $status_array =['status'=>'failed'];
        }
        return $status_array;
    }


    public function resendOtp($user_id=null, $otp, $mobile = null){

	    $otp='567886';
	    /*$mobile='9971660364';
        $curl = curl_init();
        $api_key = env('SMS_API_KEY');
        	//echo "https://2factor.in/API/V1/".$api_key."/SMS/".$mobile."/".$otp."/mobileotp";

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://2factor.in/API/V1/$api_key/SMS/$mobile/$otp/mobileotp",

            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
          echo "cURL Error #:" . $err;
        } else {
          echo $response;
        }
        die;*/
        $result=1;

        if($result){
            $status_array =['status'=>'success','otp'=>$otp];
        }else{
            $status_array =['status'=>'failed'];
        }
        return $status_array;
    }

    public function resendEmail($user_data){
        $user_data = $user_data->toArray();
        return $this->sendEmailService->sendVerificationEmailToUser($user_data);
    }


    public function sendJobMailToUser($user_data,$jobDetail){
        $user_data = $user_data->toArray();
        $user_data['course_job_id'] = $jobDetail;
        return $this->sendEmailService->sendJobEmailToUser($user_data);
    }
    
    public function sendUserJobAppliedMailToCompany($user_data,$jobDetail,$shareHistoryIdKey){
        $user_data = $user_data->toArray();
        $user_data['course_job_id'] = $jobDetail;
        $user_data['share_encrypted_id'] = $shareHistoryIdKey;
        return $this->sendEmailService->sendUserJobAppliedMailToCompany($user_data);
    }

    public function sendWelcomeEmailWithPasswordResetMail($user_data){

        $token = app('auth.password.broker')->createToken($user_data);
        $user_data = $user_data->toArray();
        $credentials = ['email' => $user_data['email']];

        if(empty($token)){
            return false;
        }

        return $this->sendEmailService->sendWelcomeEmailWithPasswordResetMail($user_data,$token);
    }

    public function verifyEmail($email){

        if(Auth::check()){
            $logged_in_user_email = Auth::user()->email;
        }

        $email = base64_decode($email);
        $userData = $this->getUserByEmail($email)->toArray();

        if(count($userData) > 0){
            if(isset($logged_in_user_email) && $logged_in_user_email != $userData[0]['email']){
                return ['status' => 'another_user', 'route_name' => 'home'];
            }
            $result = $this->markEmailVerified($userData[0]['id']);
            if($result){
                $status_array =['status'=>'success','route_name' => 'home'];
            }else{
                $status_array =['status'=>'error'];
            }
        }else{
             return ['status' => 'invalid_user', 'route_name' => 'login'];
        }
        return $status_array;

    }

    public function getUserByEmail($email){
        $result = $this->userRepository->getUserByEmail($email);
        return $result;
    }

    public function markEmailVerified($user_id){
        return $this->userRepository->markEmailVerified($user_id);
    }

    public function uploadProfile($array) {

        $storage_path = env('SEAFARER_PROFILE_PATH','public/images/uploads/seafarer_profile_pic/');
        if(isset($array['role']) AND ($array['role'] == 'admin' || $array['role'] == 'advertiser')){
            $storage_path = $storage_path.'temp/';
        }
        // image default storage directory path...
        $profile_upload_dir = str_replace(["/","\\"], [DIRECTORY_SEPARATOR,DIRECTORY_SEPARATOR], $storage_path);

        $new_user = false;

        if(!isset($array['role'])){
           if(Auth::check()){
               $user_id = Auth::user()->id;
           }else{
               $dummyData['first_name'] = 'temp';
               $data =  $this->userRepository->store($dummyData);
               $user_id = $data['id'];
               $new_user = true;
           }
            $destination_path = $profile_upload_dir . $user_id;
        }else{
             $destination_path = $profile_upload_dir;
        }

        if(isset($array['profile_pic']) && $array['profile_pic']->isValid()) {


            if(!is_dir($destination_path)) {
                mkdir($destination_path, 0777,true);
            }
            
            if(!empty(Auth::user()->profile_pic)){
                $existProfilePic = 'public/images/uploads/seafarer_profile_pic/'.$user_id.'/'.Auth::user()->profile_pic;

                $headers = get_headers(env('AWS_URL') . $existProfilePic);
                // if(File::exists($existProfilePic)){
                if($headers[0] == 'HTTP/1.1 200 OK'){
                    $fileSize = str_replace('Content-Length: ', '', $headers[9]) / 1024;
                    $fileSize = $fileSize / 1024;
                    $s3 = new S3(env('AWS_ACCESS_KEY_ID'), env('AWS_SECRET_ACCESS_KEY'), false, 's3.amazonaws.com',env('AWS_DEFAULT_REGION'));
                    if ($s3->deleteObject(env('AWS_BUCKET'), $existProfilePic)) {
        
                    }
                    $user = User::find($user_id);
                    $user->used_kb = abs($user->used_kb - $fileSize);
                    $user->save();
                }
            }

            $extension = '.' . $array['profile_pic']->getClientOriginalExtension();

            $destination_filename =  time().'-pic';

            $resizeDimensions = array([150, 150]);

            //create thumbnails
            \CommonHelper::createProfilePhotoThumbnails($array['profile_pic'], $array, $resizeDimensions, $destination_path, $destination_filename, $extension);

            //save profile photo to db
            $filename = $destination_filename.$extension;
            $store_arr['profile_pic'] = $filename;

            if(!isset($array['role'])){
                $this->userRepository->update($store_arr, $user_id);

                if($new_user){
                    Auth::loginUsingId($user_id);
                }
            }else{
                $user_id = '';
            }

            $status_array = ['status' => 'success', 'filename' => $filename, 'stored_file' =>env("AWS_URL").($destination_path.'/'.$filename), 'user_id' => $user_id];
        } else {
            $status_array = ['status' => 'failed'];
        }

        return $status_array;
    }

    public function generateOTP(){
        $otp = rand(100000,999999);
        return $otp;
    }


    public function updateUser($data , $user_id){
        return $this->userRepository->update($data, $user_id);
    }

    public function getAllSeafarersData($options){
        return $this->userRepository->getAllSeafarersData($options);
    }

    public function getAllCompanyData($options){
        return $this->userRepository->getAllCompanyData($options);
    }

    public function getAllAdvertiserData($options,$filter=NULL){
        return $this->userRepository->getAllAdvertiserData($options,$filter);
    }

    public function getAllInstituteData($options,$filter=NULL){
        return $this->userRepository->getAllInstituteData($options,$filter);
    }

    public function getAllAdvertiserDataWithoutFilter($options){
        return $this->userRepository->getAllAdvertiserDataWithoutFilter($options);
    }

    public function getAllCompanyDataByFilters($filter,$options){
        return $this->userRepository->getAllCompanyDataByFilters($filter,$options);
    }

    public function getAllCompanyDataByFiltersWithoutPagination($filter=NULL,$options=NULL){
        return $this->userRepository->getAllCompanyDataByFiltersWithoutPagination($filter,$options);
    }

    public function storeServiceDetails($data, $user_id){
        $this->storeUpdatedOnForUsers($user_id);

        $this->userSeaServiceRepository->deleteServiceById($user_id);
        foreach ($data['shipping_company'] as $key => $value) {

            $services['user_id'] = $user_id;
            $services['company_name'] = $value;
            $services['ship_name'] = $data['name_of_ship'][$key];
            $services['ship_type'] = $data['ship_type'][$key];
            $services['rank_id'] = $data['rank'][$key];

            if(isset($data['grt'][$key]))
                $services['grt'] = $data['grt'][$key];

            if(isset($data['engine_type'][$key]))
                $services['engine_type'] = $data['engine_type'][$key];

            if(isset($data['bhp'][$key]))
                $services['bhp'] = $data['bhp'][$key];

            if(isset($data['ship_flag'][$key]))
                $services['ship_flag'] = $data['ship_flag'][$key];

            if(isset($data['other_engine_type'][$key]) && isset($data['engine_type'][$key]) && $data['engine_type'][$key] == 'other')
                $services['other_engine_type'] = $data['other_engine_type'][$key];
            else
                $services['other_engine_type'] = NULL;

            //$services['nrt'] = $data['nrt'][$key];
            $services['from'] = date('Y-m-d',strtotime($data['date_from'][$key]));
            $services['to'] = date('Y-m-d',strtotime($data['date_to'][$key]));

            $result = $this->userSeaServiceRepository->store($services);
        }
        if(count($result) > 0){
            $status_array = ['status' => 'success'];
        }else{
            $status_array = ['status' => 'failed'];
        }

        return $status_array;
    }
    
    
    public function saveShareContact($data){
        $shareContactId = isset($data['share_contact_id']) ? $data['share_contact_id'] : '';
        $exist = $this->shareContactRepository->checkShareContactExist($data['email'], $data['user_id'], $shareContactId);
        if(isset($exist) && !empty($exist) && !empty($exist->toArray())){
           return ['status' => 'exist'];
        }
        
        $result = $this->shareContactRepository->store($data);
        if (!empty($result)) {
            $status_array = ['status' => 'success'];
        } else {
            $status_array = ['status' => 'failed'];
        }
        return $status_array;
    }
    
    public function deleteShareContact($data, $shareContactId, $userId){
        $result = $this->shareContactRepository->deleteShareContact($data, $shareContactId, $userId);
        if (!empty($result)) {
            $status_array = ['status' => 'success'];
        } else {
            $status_array = ['status' => 'failed'];
        }
        return $status_array;
    }
    
    public function getShareContact($user_id){
        return $this->shareContactRepository->getShareContact($user_id);
    }
    
    public function getShareHistory($user_id, $type){
        return $this->shareHistoryRepository->getShareHistory($user_id, $type);
    }
    
    public function getShareContactList($user_id){
        return $this->shareContactRepository->getShareContactList($user_id);
    }

    public function storeGeneralDetails($data, $user_id){

        $this->storeUpdatedOnForUsers($user_id);

        $data_professional['name_of_school']=$data['name_of_school'];
		$data_professional['school_to']=$data['school_to'];
		$data_professional['school_qualification']=$data['school_qualification'];

        if($data['name_of_school'] == '' && $data['school_to'] == '' && $data['school_qualification'] == '') {
            $docsType = UserDocumentsType::where('type','school_qualification')->where('user_id',$user_id)->first();
            if($docsType){
                $docs = UserDocuments::where("user_document_id",$docsType->id)->first();
                if($docs){
                    $type = $docsType->type;
                    $typeId = $docsType->type_id;

                    $img_id = $docs->id;
                    $img_name = $docs->document_path;

                    if(isset($typeId) && $typeId !== 0) {
                        $type_id = $typeId;
                    } else {
                        $type_id = 0;
                    }
                    $this->deleteDocument($img_id,$img_name,$user_id,$type,$type_id);
                }
            }
        }


        if($data['institute_name'] == '' && $data['institute_to'] == '' && $data['institute_degree'] == '') {
            $docsType = UserDocumentsType::where('type','institute_degree')->where('user_id',$user_id)->first();
            if($docsType){
                $type = $docsType->type;
                $typeId = $docsType->type_id;
                $docs = UserDocuments::where("user_document_id",$docsType->id)->get();
                if($docs){
                    foreach($docs as $doc) {
                        $img_id = $doc->id;
                        $img_name = $doc->document_path;

                        if(isset($typeId) && $typeId !== 0) {
                            $type_id = $typeId;
                        } else {
                            $type_id = 0;
                        }
                        $this->deleteDocument($img_id,$img_name,$user_id,$type,$type_id);
                    }
                }
            }
        }

		$data_professional['other_exp']=$data['other_exp'];
                isset($data['institute_name']) ? $data_professional['institute_name'] = $data['institute_name'] : '';
                isset($data['institute_to']) ? $data_professional['institute_to'] = $data['institute_to'] : '';
                isset($data['institute_degree']) ? $data_professional['institute_degree'] = $data['institute_degree'] : '';
		DB::table('user_professional_details')
        ->where('user_id', $user_id)
        ->limit(1)
        ->update($data_professional);

      
        $data_wkfr['blood_type']   = isset($data['blood_type']) ? $data['blood_type'] : null;
		$data_wkfr['yellow_fever']   = isset($data['yellow_fever']) ? $data['yellow_fever'] : 0;
		$data_wkfr['yf_issue_date']  =  isset($data['yf_issue_date']) ? date('Y-m-d',strtotime($data['yf_issue_date'])) : null;
        $data_wkfr['yf_country'] = isset($data['yf_country']) ? $data['yf_country'] : null;

        
        if($data_wkfr['yellow_fever'] == 0){
           
            $data_wkfr['yf_issue_date'] = $data_wkfr['yf_country'] = null;
            $types = ['yellow_fever'];
            $userId = Auth::user()->id;
            UserController::destroyUserDocumentsnew($types, $userId);
        }
      
		$data_wkfr['ilo_medical']    = isset($data['ilo_medical']) ? $data['ilo_medical'] : 0;
		$data_wkfr['ilo_issue_date'] = isset($data['ilo_issue_date']) ? date('Y-m-d',strtotime($data['ilo_issue_date'])) : null;
        $data_wkfr['ilo_country'] = isset($data['ilo_country']) ? $data['ilo_country'] : null;
        if($data_wkfr['ilo_medical'] == 0){
           
            $data_wkfr['ilo_issue_date'] = $data_wkfr['ilo_country'] = null;
            $types = ['ilo_medical'];
            $userId = Auth::user()->id;
            UserController::destroyUserDocumentsnew($types, $userId);
        }


        $data_wkfr['cholera']    = isset($data['cholera']) ? $data['cholera'] : 0;
        $data_wkfr['cholera_issue_date'] = isset($data['cholera_issue_date']) ? date('Y-m-d',strtotime($data['cholera_issue_date'])) : null;
        $data_wkfr['cholera_country'] = isset($data['cholera_country']) ? $data['cholera_country'] : null;
        if($data_wkfr['cholera'] == 0){
           
            $data_wkfr['cholera_issue_date'] = $data_wkfr['cholera_country'] = null;
            $types = ['cholera'];
            $userId = Auth::user()->id;
            UserController::destroyUserDocumentsnew($types, $userId);
        }

        $data_wkfr['hepatitis_b']    = isset($data['hepatitis_b']) ? $data['hepatitis_b'] : 0;
        $data_wkfr['hepatitis_b_issue_date'] = isset($data['hepatitis_b_issue_date']) ? date('Y-m-d',strtotime($data['hepatitis_b_issue_date'])) : null;
        $data_wkfr['hepatitis_b_country'] = isset($data['hepatitis_b_country']) ? $data['hepatitis_b_country'] : null;
        if($data_wkfr['hepatitis_b'] == 0){
           
            $data_wkfr['hepatitis_b_issue_date'] = $data_wkfr['hepatitis_b_country'] = null;
            $types = ['hepatitis_b'];
            $userId = Auth::user()->id;
            UserController::destroyUserDocumentsnew($types, $userId);
        }



        $data_wkfr['hepatitis_c']    = isset($data['hepatitis_c']) ? $data['hepatitis_c'] : 0;
        $data_wkfr['hepatitis_c_issue_date'] = isset($data['hepatitis_c_issue_date']) ? date('Y-m-d',strtotime($data['hepatitis_c_issue_date'])) : null;
        $data_wkfr['hepatitis_c_country'] = isset($data['hepatitis_c_country']) ? $data['hepatitis_c_country'] : null;

        if($data_wkfr['hepatitis_c'] == 0){
           
            $data_wkfr['hepatitis_c_issue_date'] = $data_wkfr['hepatitis_c_country'] = null;
            $types = ['hepatitis_c'];
            $userId = Auth::user()->id;
            UserController::destroyUserDocumentsnew($types, $userId);
        }


        $data_wkfr['diphtheria']    = isset($data['diphtheria']) ? $data['diphtheria'] : 0;
        $data_wkfr['diphtheria_issue_date'] = isset($data['diphtheria_issue_date']) ? date('Y-m-d',strtotime($data['diphtheria_issue_date'])) : null;
        $data_wkfr['diphtheria_country'] = isset($data['diphtheria_country']) ? $data['diphtheria_country'] : null;

        if($data_wkfr['diphtheria'] == 0){
           
            $data_wkfr['diphtheria_issue_date'] = $data_wkfr['diphtheria_country'] = null;
            $types = ['diphtheria'];
            $userId = Auth::user()->id;
            UserController::destroyUserDocumentsnew($types, $userId);
        }


        $data_wkfr['covid']    = isset($data['covid']) ? $data['covid'] : 0;
        $data_wkfr['covid_issue_date'] = isset($data['covid_issue_date']) ? date('Y-m-d',strtotime($data['covid_issue_date'])) : null;
        $data_wkfr['covid_country'] = isset($data['covid_country']) ? $data['covid_country'] : null;
         if($data_wkfr['covid'] == 0){
           
            $data_wkfr['covid_issue_date'] = $data_wkfr['covid_country'] = null;
            $types = ['covid'];
            $userId = Auth::user()->id;
            UserController::destroyUserDocumentsnew($types, $userId);
        }


        $data_wkfr['screening_test']    = isset($data['screening_test']) ? $data['screening_test'] : 0;
        $data_wkfr['screening_test_date'] = isset($data['screening_test_date']) ? date('Y-m-d',strtotime($data['screening_test_date'])) : null;
        $data_wkfr['screening_test_county'] = isset($data['screening_test_county']) ? $data['screening_test_county'] : null;
        if($data_wkfr['screening_test'] == 0){
           
            $data_wkfr['screening_test_date'] = $data_wkfr['screening_test_county'] = null;
            $types = ['screening_test'];
            $userId = Auth::user()->id;
            UserController::destroyUserDocumentsnew($types, $userId);
        }

		$data_wkfr['character_reference'] = isset($data['cc']) ? $data['cc'] : 0;
		$data_wkfr['character_reference_issue_date'] = isset($data['ccdate']) ? date('Y-m-d',strtotime($data['ccdate'])) : null;
        $data_wkfr['type_of_certificate'] = isset($data['type_of_certificate']) ? $data['type_of_certificate'] : null;

        if($data_wkfr['character_reference'] == 0) {
            $data_wkfr['character_reference_issue_date'] = null;
            $data_wkfr['type_of_certificate'] = null;
        }
		DB::table('user_wkfr_details')
        ->where('user_id', $user_id)
        ->limit(1)
        ->update($data_wkfr);

        if($data_wkfr['character_reference'] == 0 || ($data_wkfr['character_reference_issue_date'] == null && $data_wkfr['type_of_certificate'] == null)) {
            $docsType = UserDocumentsType::where('type','character_reference')->where('user_id',$user_id)->first();
            if($docsType) {
                $docs = UserDocuments::where("user_document_id",$docsType->id)->first();
                if($docs) {
                    $type = $docsType->type;
                    $typeId = $docsType->type_id;

                    $img_id = $docs->id;
                    $img_name = $docs->document_path;

                    if(isset($typeId) && $typeId !== 0) {
                        $type_id = $typeId;
                    } else {
                        $type_id = 0;
                    }
                    $this->deleteDocument($img_id,$img_name,$user_id,$type,$type_id);
                }
            }
        }

		//update
        $data_personal['kin_name'] = isset($data['kin_name']) ? $data['kin_name'] : null;
        $data_personal['kin_relation'] = isset($data['kin_relation']) ? $data['kin_relation'] : null;
        $data_personal['kin_number'] = isset($data['kin_number']) ? $data['kin_number'] : null;
        $data_personal['kin_alternate_no'] = isset($data['kin_alternate_no']) ? $data['kin_alternate_no'] : null;
        $data_personal['kin_alternate_number_code'] = isset($data['kin_alternate_number_code']) ? $data['kin_alternate_number_code'] : 91;
        $data_personal['kin_number_code'] = isset($data['kin_number_code']) ? $data['kin_number_code'] : 91;
        $data_personal['pancard'] = isset($data['pancard']) ? $data['pancard'] : null;
        $data_personal['aadhaar'] = isset($data['aadhaar']) ? $data['aadhaar'] : null;
        $data_personal['bank_name'] = isset($data['bank_name']) ? $data['bank_name'] : null;
        $data_personal['account_holder_name'] = isset($data['account_holder_name']) ? $data['account_holder_name'] : null;
        $data_personal['account_no'] = isset($data['account_no']) ? $data['account_no'] : null;
        $data_personal['ifsc_code'] = isset($data['ifsc_code']) ? $data['ifsc_code'] : null;
        $data_personal['branch_address'] = isset($data['branch_address']) ? $data['branch_address'] : null;
        $data_personal['coverall_size'] = isset($data['coverall_size']) ? $data['coverall_size'] : null;
        $data_personal['safety_shoe_size'] = isset($data['safety_shoe_size']) ? $data['safety_shoe_size'] : null;
        $data_personal['is_bank_details'] = isset($data['is_bank_details']) ? $data['is_bank_details'] : 0;
        if($data_personal['is_bank_details'] == 0){
            $data_personal['pancard'] = null;
            $data_personal['aadhaar'] = null;
            $data_personal['account_holder_name'] = null;
            $data_personal['bank_name'] = null;
            $data_personal['account_no'] = null;
            $data_personal['ifsc_code'] = null;
            $data_personal['branch_address'] = null;
        }
        if($data_personal['account_no'] == null) {
            $docsType = UserDocumentsType::where('type','passbook')->where('user_id',$user_id)->first();
            if($docsType) {
                $type = $docsType->type;
                $docs = UserDocuments::where("user_document_id",$docsType->id)->get();
                $typeId = $docsType->type_id;
                if($docs) {
                    foreach ($docs as $doc){

                        $img_id = $doc->id;
                        $img_name = $doc->document_path;

                        if(isset($typeId) && $typeId !== 0) {
                            $type_id = $typeId;
                        } else {
                            $type_id = 0;
                        }
                        $this->deleteDocument($img_id,$img_name,$user_id,$type,$type_id);
                    }
                }
            }
        }

        if($data_personal['pancard'] == null) {
            $docsType = UserDocumentsType::where('type','pancard')->where('user_id',$user_id)->first();
            if($docsType) {
                $type = $docsType->type;
                $docs = UserDocuments::where("user_document_id",$docsType->id)->get();
                $typeId = $docsType->type_id;
                if($docs) {
                    foreach ($docs as $doc){

                        $img_id = $doc->id;
                        $img_name = $doc->document_path;

                        if(isset($typeId) && $typeId !== 0) {
                            $type_id = $typeId;
                        } else {
                            $type_id = 0;
                        }
                        $this->deleteDocument($img_id,$img_name,$user_id,$type,$type_id);
                    }
                }
            }
        }

        if($data_personal['aadhaar'] == null) {
            $docsType = UserDocumentsType::where('type','aadhaar')->where('user_id',$user_id)->first();
            if($docsType) {
                $type = $docsType->type;
                $docs = UserDocuments::where("user_document_id",$docsType->id)->get();
                $typeId = $docsType->type_id;
                if($docs) {
                    foreach ($docs as $doc){

                        $img_id = $doc->id;
                        $img_name = $doc->document_path;

                        if(isset($typeId) && $typeId !== 0) {
                            $type_id = $typeId;
                        } else {
                            $type_id = 0;
                        }
                        $this->deleteDocument($img_id,$img_name,$user_id,$type,$type_id);
                    }
                }
            }
        }
		DB::table('user_personal_details')
        ->where('user_id', $user_id)
        ->limit(1)
        ->update($data_personal);

		    /*$user_passport_details['indian_pcc'] = $data['pcc'];
            if(isset($data['pcc']) && !empty($data['pcc']) && $data['pcc'] == '1')
                 $user_passport_details['indian_pcc_issue_date'] = date('Y-m-d',strtotime($data['pccdate']));
            else
                $user_passport_details['indian_pcc_issue_date'] = NULL;

			DB::table('user_passport_details')
           ->where('user_id', $user_id)
			->limit(1)
			->update($user_passport_details);*/

        $status_array = ['status' => 'success'];
        return $status_array;
    }

    public function storeSpecificServiceDetails($data, $user_id){
        if(isset($data['date_from'][0]) && !empty($data['date_from'][0])){
            $service_id = '';
            if(isset($data['exiting_service_id']) && !empty($data['exiting_service_id'])){
                $service_id = $data['exiting_service_id'];
            }

            $result_from = $this->userSeaServiceRepository->checkSeaServiceExists($data['date_from'][0], $user_id, $service_id);

            if(isset($result_from) && !empty($result_from) && !empty($result_from->toArray())){
               return ['status' => 'sea_service_from_present'];
            }

            if (isset($data['date_to'][0]) && !empty($data['date_to'][0])) {
                $result_to = $this->userSeaServiceRepository->checkSeaServiceExists($data['date_to'][0], $user_id, $service_id);

                if(isset($result_to) && !empty($result_to) && !empty($result_to->toArray())){
                    return ['status' => 'sea_service_to_present'];
                }
            }            
        }

        if(empty($data['exiting_service_id'])){
            $lastSeaService = UserSeaService::whereUserIdAndTo($user_id, null)->orderBy('id', 'DESC')->first();
            if(!empty($lastSeaService)){
                if(empty($data['date_to'][0])){
                    return ['status' => 'two_onboard_not_allow'];
                }
                $lastSeaServiceFrom = date('Y-m-d',strtotime($lastSeaService['from']));
                $dateFrom = date('Y-m-d',strtotime($data['date_from'][0]));
                $dateTo = date('Y-m-d',strtotime($data['date_to'][0]));
                if($lastSeaServiceFrom < $dateFrom || $lastSeaServiceFrom <= $dateTo){
                    return ['status' => 'sign_on_and_sign_of_less_then_to_on_board'];
                }
                
            }
//            $lastSeaService = UserSeaService::whereUserIdAndTo($user_id, null)->orderBy('id', 'DESC')->first();
//            if ($lastSeaService != null) {
//                return ['status' => 'sea_service_to_sign_off'];
//            }
//            
//            $lastSeaService = UserSeaService::whereUserId($user_id)->orderBy('id', 'DESC')->first();
//            if ($lastSeaService != null && $data['date_from'][0] < Carbon::parse($lastSeaService->to)->format('d-m-Y')) {
//                return ['status' => 'sea_service_exists'];
//            }

        }
        /*echo "<pre>";
            print_r($result->toArray());
            print_r($data['date_from'][0]);
            print_r($data);
        echo "</pre>";
        die();*/


        $this->storeUpdatedOnForUsers($user_id);



        foreach ($data['shipping_company'] as $key => $value) {

            $services['user_id'] = $user_id;
            $services['company_name'] = $value;
            $services['ship_name'] = $data['name_of_ship'][$key];
            $services['ship_type'] = $data['ship_type'][$key];
            $services['rank_id'] = $data['rank'][$key];

            if(isset($data['manning_by'][$key]))
                $services['manning_by'] = $data['manning_by'][$key];

            if(isset($data['grt'][$key]))
                $services['grt'] = $data['grt'][$key];

            if(isset($data['engine_type'][$key]))
                $services['engine_type'] = $data['engine_type'][$key];

            if(isset($data['bhp'][$key]))
                $services['bhp'] = $data['bhp'][$key];

            if(isset($data['ship_flag'][$key]))
                $services['ship_flag'] = $data['ship_flag'][$key];

            if(isset($data['exiting_service_id']) && !empty($data['exiting_service_id']))
                $services['exiting_service_id'] = $data['exiting_service_id'];

            if(isset($data['other_engine_type'][$key]) && isset($data['engine_type'][$key]) && $data['engine_type'][$key] == '11')
                $services['other_engine_type'] = $data['other_engine_type'][$key];
            else
                $services['other_engine_type'] = NULL;

            //$services['nrt'] = $data['nrt'][$key];
            if (isset($data['date_from'][$key]) && !empty($data['date_from'][$key])) {
                $services['from'] = date('Y-m-d',strtotime($data['date_from'][$key]));
            }
            if (isset($data['date_to'][$key]) && !empty($data['date_to'][$key])) {
                $services['to'] = date('Y-m-d',strtotime($data['date_to'][$key]));
                $diff = strtotime($services['from']) - strtotime($services['to']); 
                $services['total_duration'] = abs(round($diff / 86400)) + 1; 
            }
            // $services['to'] = isset($data['date_to'][$key]) && !empty($data['date_to'][$key]) ?  date('Y-m-d',strtotime($data['date_to'][$key])) : null;

            $result = $this->userSeaServiceRepository->store($services);
        }
        if(count($result->toArray()) > 0){
            $status_array = ['status' => 'success','result' => $result->toArray()];
        }else{
            $status_array = ['status' => 'failed'];
        }

        return $status_array;
    }

    public function deleteServiceDetails($service_id, $user_id){
        return $this->userSeaServiceRepository->deleteServiceByServiceId($service_id, $user_id);
    }

    public function getSeaServiceDetails($service_id, $user_id){
        $result = $this->userSeaServiceRepository->getSeaServiceDetails($service_id, $user_id);

        if(isset($result['sea_service_details']['from']) && !empty($result['sea_service_details']['from'])){
            $result['sea_service_details']['from'] = date('d-m-Y',strtotime($result['sea_service_details']['from']));
        }

        if(isset($result['sea_service_details']['to']) && !empty($result['sea_service_details']['to'])){
            $result['sea_service_details']['to'] = date('d-m-Y',strtotime($result['sea_service_details']['to']));
        }

        return $result;
    }

    public function getSeaServiceDetailsByUserId($user_id){
        return $this->userSeaServiceRepository->getSeaServiceDetailsByUserId($user_id);
    }

    public function storeCourseDetails($data, $user_id){
        $this->storeUpdatedOnForUsers($user_id);

//        isset($data['name_of_school']) ? $user_professional_details['name_of_school'] = $data['name_of_school'] : '';
//        isset($data['school_from']) ? $user_professional_details['school_from'] = $data['school_from'] : '';
//        isset($data['school_to']) ? $user_professional_details['school_to'] = $data['school_to'] : '';
//        isset($data['school_qualification']) ? $user_professional_details['school_qualification'] = $data['school_qualification'] : '';
//        isset($data['institute_name']) ? $user_professional_details['institute_name'] = $data['institute_name'] : '';
//        isset($data['institute_from']) ? $user_professional_details['institute_from'] = $data['institute_from'] : '';
//        isset($data['institute_to']) ? $user_professional_details['institute_to'] = $data['institute_to'] : '';
//        isset($data['institute_degree']) ? $user_professional_details['institute_degree'] = $data['institute_degree'] : '';
//
//        isset($data['other_exp']) ? $user_professional_details['other_exp'] = $data['other_exp'] : NULL;
//
//        if(isset($user_professional_details) && !empty($user_professional_details))
//            $this->userProfessionalDetailsService->updateByUserId($user_professional_details,$user_id);

        //$this->userCertificateDetailsRepository->deleteCoursesById($user_id);

        if(isset($data['course_date_of_issue']) && !empty($data['course_date_of_issue'])){
            $test = array_filter($data['course_date_of_issue']);

            foreach($test as $key => $value){
               $courses['user_id'] = $user_id;
               $courses['course_id'] = $key;
               $courses['issue_date'] = date('Y-m-d',strtotime($value));
               $courses['course_type'] = 'Normal';
               $result = $this->userCertificateDetailsRepository->store($courses);
//			   $cdc_count = \DB::table('user_documents_type')->where('user_id', $user_id)->where('type', 'course')->count();

            }
        }
        /*else{

            $certificates = collect($data['certificate_name'])->sortBy('asc')->toArray();
            foreach ($certificates as $key => $value) {

                $courses['user_id'] = $user_id;
                $courses['course_id'] = $value;
                $courses['issue_date'] = date('Y-m-d',strtotime($data['date_of_issue'][$key]));
                if(isset($data['date_of_expiry'][$key]) AND !empty($data['date_of_expiry'][$key]))
                    $courses['expiry_date'] = date('Y-m-d',strtotime($data['date_of_expiry'][$key]));
                else
                    $courses['expiry_date'] = NULL;

                $courses['issue_by'] = $data['issue_by'][$key];
                $courses['certification_number'] = $data['certificate_no'][$key];
                $courses['course_type'] = 'Normal';

                $result = $this->userCertificateDetailsRepository->store($courses);
            }
        }*/

        if(isset($data['value_added_course_date_of_issue']) && !empty($data['value_added_course_date_of_issue'])){
            $test = array_filter($data['value_added_course_date_of_issue']);

            foreach($test as $key => $value){
               $courses['user_id'] = $user_id;
               $courses['course_id'] = $key;
               $courses['issue_date'] = date('Y-m-d',strtotime($value));
               $courses['course_type'] = 'Value Added';
               $result = $this->userCertificateDetailsRepository->store($courses);

            }
        }
        /*else{
            $value_added_certificates = collect($data['value_added_certificate_name'])->sortBy('asc')->toArray();

            foreach ($value_added_certificates as $key => $value) {

                if(isset($value) && !empty($value))
                    $value_added_courses['course_id'] = $value;

                if(isset($data['value_added_date_of_issue'][$key]) && !empty($data['value_added_date_of_issue'][$key]))
                    $value_added_courses['issue_date'] = date('Y-m-d',strtotime($data['value_added_date_of_issue'][$key]));

                if(isset($data['value_added_date_of_expiry'][$key]) && !empty($data['value_added_date_of_expiry'][$key]))
                    $value_added_courses['expiry_date'] = date('Y-m-d',strtotime($data['value_added_date_of_expiry'][$key]));

                if(isset($data['value_added_issue_by'][$key]) && !empty($data['value_added_issue_by'][$key]))
                    $value_added_courses['issue_by'] = $data['value_added_issue_by'][$key];

                if(isset($data['value_added_issuing_authority'][$key]) && !empty($data['value_added_issuing_authority'][$key]))
                    $value_added_courses['issuing_authority'] = $data['value_added_issuing_authority'][$key];

                if(isset($data['value_added_certificate_no'][$key]) && !empty($data['value_added_certificate_no'][$key]))
                    $value_added_courses['certification_number'] = $data['value_added_certificate_no'][$key];

                if(isset($data['value_added_place_of_issue'][$key]) && !empty($data['value_added_place_of_issue'][$key]))
                    $value_added_courses['place_of_issue'] = $data['value_added_place_of_issue'][$key];

                if(!empty($value_added_courses)){
                    $value_added_courses['course_type'] = 'Value Added';
                    $value_added_courses['user_id'] = $user_id;
                    $result = $this->userCertificateDetailsRepository->store($value_added_courses);
                }
            }
        }*/

        /*if(count($result) > 0){
            $status_array = ['status' => 'success'];
        }else{
            $status_array = ['status' => 'failed'];
        }*/
        return ['status' => 'success'];
    }

    public function getAllCompanySubscriptionsDetails($options,$filter){
        return $this->userRepository->getAllCompanySubscriptionsDetails($options,$filter);
    }

    public function storeUpdatedOnForUsers($seafarer_id){
        return $this->userRepository->storeUpdatedOnForUsers($seafarer_id);
    }

    public function storeLastLoggedOndate($user_id){
        return $this->userRepository->storeLastLoggedOndate($user_id);
    }

    public function getAllSeafarersByCurrentDayAvailability(){
        $now = date('Y-m-d');
        return $this->userProfessionalDetailsService->getAllSeafarersByCurrentDayAvailability($now);
    }

    public function getAllSeafarersExpiringAvailabilityBeforeGivenDays($days){

        $current_date = date('Y-m-d');
        $to_date = date('Y-m-d', strtotime('+'.$days.'days'));

        return $this->userProfessionalDetailsService->getAllSeafarersExpiringAvailabilityBeforeGivenDays($current_date,$to_date);
    }

    public function getUserByCities($cities_array){
        return $this->userRepository->getUserByCities($cities_array);
    }

    public function getUserByCountryId($country_id){
        return $this->userRepository->getUserByCountryId($country_id);
    }

    public function getSeafarerByRankAndAvailability($rank = null,$date_of_joining = null){
        return $this->userRepository->getSeafarerByRankAndAvailability($rank,$date_of_joining);
    }

    public function getAllCourses(){
        $data = $this->userRepository->getAllCourses();
        $value_added = array_search('Add-On Course',\CommonHelper::institute_course_types());

        $data = $data->toArray();

        foreach ($data as $key => $value) {
            if($value['course_type'] == $value_added){
                $course['value_added'][] = $data[$key];
            }else{
                $course['normal'][] = $data[$key];
            }
        }
        return $course;
    }

    public function storeEmailFeature($data){
        return $this->companyDownloadsRepository->storeEmailFeature($data);
    }

    public function mySubscriptionAllType($user_id, $user_role)
    {
        $data = [];
        if ($user_role == 'company') {
            $company = $this->companyService->getDetailsByCompanyId($user_id)->toArray();
            if (!empty($company)) {
                $data = $this->subscriptionService->getCompanySubscriptionByRegistrationId($company[0]['id']);
            }
            if (!empty($data)) {
                $data = $data->toArray();
            }
        } elseif ($user_role == 'institute') {
            $institute = $this->instituteService->getDetailsByCompanyId($user_id)->toArray();
            if (!empty($institute)) {
                $data = $this->subscriptionService->getInstituteSubscriptionByRegistrationId($institute[0]['id']);
            }

            if (!empty($data)) {
                $data = $data->toArray();
            }
        } elseif ($user_role == 'advertiser') {
            $advertiser = $this->companyService->getDetailsByCompanyId($user_id)->toArray();
            if (!empty($advertiser)) {

                $data = $this->subscriptionService->getAdvertiserSubscriptionByRegistrationId($advertiser[0]['id']);
            }
            if (!empty($data)) {
                $data = $data->toArray();
            }
        } elseif ($user_role == 'seafarer') {
            $data = $this->subscriptionService->getSeafarerSubscriptionByRegistrationId($user_id);
            if (!empty($data)) {
                $data = $data->toArray();
            }
        }

        $result = [];
        $result['active'] = [];
        $result['expired'] = [];
        $result['up_coming'] = [];
        if (!empty($data)) {

           $date_now = Carbon::now();

           foreach ($data as $key => $value) {
                $valid_from = Carbon::parse($value['valid_from'])->startOfDay();
                $valid_to = Carbon::parse($value['valid_to'])->endOfDay();

                if ($value['status'] == 0 || $value['status'] == 2) {
                    $result['expired'] = $value;
                    if (!empty($result['expired']) && !empty($result['expired']['subscription_details'])) {
                        $result['expired']['subscription_details'] = json_decode($result['expired']['subscription_details'],true);
                    }
                } elseif (($value['status'] == 1) && ($date_now >= $valid_from) && ($date_now <= $valid_to)) {
                    $result['active'] = $value;
                    if (!empty($result['active']) && !empty($result['active']['subscription_details'])) {
                        $result['active']['subscription_details'] = json_decode($result['active']['subscription_details'],true);
                    }
                } elseif (($value['status'] == 1) && ($date_now < $valid_from)) {
                    $result['up_coming'] = $value;
                    if (!empty($result['up_coming']) && !empty($result['up_coming']['subscription_details'])) {
                        $result['up_coming']['subscription_details'] = json_decode($result['up_coming']['subscription_details'],true);
                    }
                }

           }
        }
        $new_result = [];
        $new_result['all'] = $data;
        $new_result['my'] = $result;

        return $new_result;
    }

    public function checkSubscriptionAvailability($user_id, $user_role)
    {
        $result = [];
        $all_subscription = $this->mySubscriptionAllType($user_id, $user_role);
        $my_active_subscription = $all_subscription['my']['active'];
        if (!empty($my_active_subscription)) {
                $all_feature = $my_active_subscription['subscription_details']['subscription_features'];
                $result['status'] = 'success';
                $result['plan_name'] = $my_active_subscription['subscription_details']['title'];
                $result['result'] = $this->getAllFeaturesCount($my_active_subscription['company_reg_id'], $user_role,$all_feature,$my_active_subscription['valid_from'],$my_active_subscription['valid_to']);
        } else {
             $result['status'] = 'error';
             $result['error']['message'] = 'No active subscription';
        }

        return $result;
    }

    public function getAllFeaturesCount($id, $user_role,$all_feature,$valid_from,$valid_to){
        $result = [];
        $now = date('Y-m-d h:i:s');
        foreach ($all_feature as $key => $value) {
            $feature_type = $value['type'];
            if ($value['duration_type'] == 'day') {
                $start_date = date('Y-m-d 00:00:00');
                $end_date = date('Y-m-d 23:59:59');
            } elseif ($value['duration_type'] == 'month') {
                $first_day_this_month = date('Y-m-01 00:00:00');
                $last_day_this_month  = date('Y-m-d 23:59:59');

                if (strtotime($valid_from) > strtotime($first_day_this_month)) {
                    $start_date = date('Y-m-d h:i:s',strtotime($valid_from));
                } else {
                    $start_date = $first_day_this_month;
                }

                if (strtotime($valid_to) < strtotime($last_day_this_month)) {
                    $end_date = date('Y-m-d h:i:s',strtotime($valid_to));
                } else {
                    $end_date = $last_day_this_month;
                }

            } else {
                $start_date = date('Y-m-d h:i:s',strtotime($valid_from));
                $end_date = date('Y-m-d h:i:s',strtotime($valid_to));
            }

            $result[$feature_type]['feature'] = $value;
            if ($user_role == 'company') {
                $result[$feature_type]['total'] = $value['count'];

                if ($feature_type == config('feature.feature1')) {
                    $result[$feature_type]['used'] = $this->companyDownloadsRepository->getFeatureCount($id,$start_date,$end_date,$feature_type);
                    $result[$feature_type]['available'] = $value['count'] - $result[$feature_type]['used'];
                } elseif ($feature_type == config('feature.feature2')) {
                    $result[$feature_type]['used'] = $this->companyDownloadsRepository->getEmailFeatureCount($id,$start_date,$end_date,'company');
                    $result[$feature_type]['available'] = $value['count'] - $result[$feature_type]['used'];
                }

                if (isset($result[$feature_type]['available']) && $result[$feature_type]['available'] > 0) {
                   $result[$feature_type]['status'] = true;
                } else {
                    $result[$feature_type]['status'] = false;
                }
            } elseif ($user_role == 'institute') {
                $result[$feature_type]['total'] = $value['count'];

                if ($feature_type == config('feature.feature2')) {
                    $result[$feature_type]['used'] = $this->companyDownloadsRepository->getEmailFeatureCount($id,$start_date,$end_date,'institute');
                    $result[$feature_type]['available'] = $value['count'] - $result[$feature_type]['used'];
                }

                if (isset($result[$feature_type]['available']) && $result[$feature_type]['available'] > 0) {
                   $result[$feature_type]['status'] = true;
                } else {
                    $result[$feature_type]['status'] = false;
                }
            } elseif ($user_role == 'advertiser') {
                $result[$feature_type]['total'] = $value['count'];

                if ($feature_type == config('feature.feature3')) {

                    $result[$feature_type]['used'] = $this->advertiseService->getAdvertisementCountByStatus($id,2);
                    $result[$feature_type]['available'] = $value['count'] - $result[$feature_type]['used'];
                }

                if (isset($result[$feature_type]['available']) && $result[$feature_type]['available'] > 0) {
                   $result[$feature_type]['status'] = true;
                } else {
                    $result[$feature_type]['status'] = false;
                }
            } elseif ($user_role == 'seafarer') {
                # code...
            }
        }
         return $result;

    }

    public function saveAnyFreeSubscription($user_id,$registered_as)
    {
        $free_sub_arr = [];
        $data  = [];
        if ($registered_as == 'seafarer') {
            $data = $this->subscriptionService->getSeafarerSubscriptionByRegistrationId($user_id);
            if (!empty($data)) {
                $data = $data->toArray();
                $free_sub_arr['user_id'] = $user_id;
            }

        } elseif ($registered_as == 'advertiser') {
            $advertiser = $this->companyService->getDetailsByCompanyId($user_id)->toArray();
            if (!empty($advertiser)) {
                $data = $this->subscriptionService->getAdvertiserSubscriptionByRegistrationId($advertiser[0]['id']);
                $free_sub_arr['company_reg_id'] = $advertiser[0]['id'];
            }

        } elseif ($registered_as == 'institute') {
            $institute = $this->instituteService->getDetailsByCompanyId($user_id)->toArray();
            if (!empty($institute)) {
                $data = $this->subscriptionService->getInstituteSubscriptionByRegistrationId($institute[0]['id']);
                $free_sub_arr['institute_reg_id'] = $institute[0]['id'];
            }

        } elseif ($registered_as == 'company') {
            $company = $this->companyService->getDetailsByCompanyId($user_id)->toArray();
            if (!empty($company)) {
                $data = $this->subscriptionService->getCompanySubscriptionByRegistrationId($company[0]['id']);
                $free_sub_arr['company_reg_id'] = $company[0]['id'];
            }
        }

        if (count($data) == 0) {
            $subscription = $this->subscriptionService->getAllActiveSubscription($registered_as)->toArray();
            foreach ($subscription as $key => $sub_val) {
               if (strtolower($sub_val['duration_title']) == 'free') {
                    $free_sub_arr['subscription_details'] = json_encode($sub_val);
                    $free_sub_arr['status'] = 1;
                    $free_sub_arr['valid_from'] = date('Y-m-d 00:00:00');
                    $free_sub_arr['valid_to'] = date('Y-m-d 23:59:59', strtotime('+'.(($sub_val['duration'] * 30) -1 ).' days'));
                    $added_subscription = $this->subscriptionService->saveEditAnySubscription($free_sub_arr,$registered_as);
               }
            }
            return true;
        } else {
            return false;
        }

    }

    public function getUserShippingCompanyWithId($user_id,$text){
        return $this->userSeaServiceRepository->getUserShippingCompanyWithId($user_id,$text);
    }
    
    public function getUserShippingManningWithId($user_id,$text){
        return $this->userSeaServiceRepository->getUserShippingManningWithId($user_id,$text);
    }

    public function getUserShipName($user_id,$text,$company_name = null){
        return $this->userSeaServiceRepository->getUserShipName($user_id,$text,$company_name);
    }

    public function storeSingleCourseDetail($data,$user_id){

        $courses['user_id'] = $user_id;
        
        $courses['issue_date'] = isset($data['date_of_issue']) && !empty($data['date_of_issue']) ? date('Y-m-d',strtotime($data['date_of_issue'])) : NULL;

        if(isset($data['date_of_expiry']) AND !empty($data['date_of_expiry']))
            $courses['expiry_date'] = date('Y-m-d',strtotime($data['date_of_expiry']));
        else
            $courses['expiry_date'] = NULL;

        $courses['issue_by'] = isset($data['issue_by']) && !empty($data['issue_by']) ? $data['issue_by'] : '';
        $courses['certification_number'] = isset($data['certificate_no']) && !empty($data['certificate_no']) ? $data['certificate_no'] : '';
        
        

        if(isset($data['existing_course_id']) && !empty($data['existing_course_id'])){
            $courses['existing_course_id'] = $data['existing_course_id'];
        }else{
            $courses['course_type'] = 0;
            $courses['course_id'] = $data['certificate_name'];
            $courseData = \CommonHelper::course_data($data['certificate_name']);
            if(isset($courseData->course_type) && !empty($courseData->course_type)){
                $courses['course_type'] = $courseData->course_type;
            }
        }

        $result = $this->userCertificateDetailsRepository->store($courses);

        if(isset($data['existing_course_id']) && !empty($data['existing_course_id'])){
            if($result == 1){
                $status_array = ['status' => 'success'];
            }else{
                $status_array = ['status' => 'failed'];
            }
        }else if(count($result->toArray()) > 0){
            $status_array = ['status' => 'success'];
        }else{
            $status_array = ['status' => 'failed'];
        }
        return $status_array;
    }

    public function getCourseDetailsByUserId($user_id){
        return $this->userCertificateDetailsRepository->getCourseDetailsByUserId($user_id);
    }

    public function deleteSeafarerCourseByCourseId($course_id,$user_id){
        $result = $this->userCertificateDetailsRepository->deleteSeafarerCourseByCourseId($course_id,$user_id);
        if($result){
            $user_documents = $this->getUserDocuments($user_id);
            if (isset($user_documents['courses'][$course_id]['user_documents']) && !empty($user_documents['courses'][$course_id]['user_documents'])) {
                $sea_service_contract = $user_documents['courses'][$course_id]['user_documents'];
                foreach ($sea_service_contract as $seaServiceContract) {
                    $img_id = $seaServiceContract['id'];
                    $img_name = $seaServiceContract['document_path'];
                    $type = 'courses';
                    $type_id = $course_id;
                    $this->deleteDocument($img_id, $img_name, $user_id, $type, $type_id);
                }
            }
        }
        if($result){
            $status_array = ['status' => 'success','message' => 'Course has been deleted successfully.'];
        }else{
            $status_array = ['status' => 'failed','message' => 'Error while deleting course.'];
        }

        return $status_array;
    }

    public function getSeafarerCourseByCourseId($course_id,$user_id){
        $result = $this->userCertificateDetailsRepository->getSeafarerCourseByCourseId($course_id,$user_id);

        if(count($result->toArray()) > 0){
            $status_array = ['status' => 'success', 'result' => $result];
        }else{
            $status_array = ['status' => 'failed'];
        }

        return $status_array;
    }

    public function getCourseIssueBy($user_id,$text){
        return $this->userCertificateDetailsRepository->getCourseIssueBy($user_id,$text);
    }



	 public function storeUserDocuments1($filename, $type, $type_id = 0, $user_id){

        /*$storage_path = 'public/uploads/user_documents/';

        $profile_upload_dir = str_replace(["/","\\"], [DIRECTORY_SEPARATOR,DIRECTORY_SEPARATOR], $storage_path);

        if($type_id != '0'){
            $destination_path = $profile_upload_dir . $user_id ."\\". $type."\\". $type_id;
            $destination_path = str_replace(["/","\\"], [DIRECTORY_SEPARATOR,DIRECTORY_SEPARATOR], $destination_path);
        }else{
            $destination_path = $profile_upload_dir . $user_id ."\\". $type;
            $destination_path = str_replace(["/","\\"], [DIRECTORY_SEPARATOR,DIRECTORY_SEPARATOR], $destination_path);
        }*/

       /*if(isset($file) && $file->isValid()) {

            if(!is_dir($destination_path)) {
                mkdir($destination_path, 0777,true);
            }

            $extension = '.' . $file->getClientOriginalExtension();
            $destination_filename =  time().'-pic';

            $filename = $destination_filename.$extension;

            $moveable_file = str_replace(["/","\\"], [DIRECTORY_SEPARATOR,DIRECTORY_SEPARATOR], $destination_path.'\\'.$filename);
            move_uploaded_file($file,$moveable_file);*/

            $document_exist = $this->documentService->checkSeafarerTypeExist($type,$type_id,$user_id)->toArray();

            if(!empty($document_exist)){
                $document_id = $document_exist['0']['id'];
            }else{
                $document = $this->documentService->store($type,$type_id,$user_id);
                $document_id = $document->id;
            }

            if($document_id){
                $result = $this->documentService->storeDocument($filename,$document_id);
            }

            if(isset($result) && isset($document_id)){
                $status_array = ['status' => 'success', 'filename' => $filename, 'id' => $result->id];
            }else{
                 $status_array = ['status' => 'failed'];
            }

       /* } else {
            $status_array = ['status' => 'failed'];
        }*/
        return $status_array;
    }

    public function storeUserDocuments($file, $type, $type_id = 0, $user_id){


        $storage_path = 'public/uploads/user_documents/';

        $profile_upload_dir = str_replace(["/","\\"], [DIRECTORY_SEPARATOR,DIRECTORY_SEPARATOR], $storage_path);

        if($type_id != '0'){
            $destination_path = $profile_upload_dir . $user_id ."\\". $type."\\". $type_id;
            $destination_path = str_replace(["/","\\"], [DIRECTORY_SEPARATOR,DIRECTORY_SEPARATOR], $destination_path);
        }else{
            $destination_path = $profile_upload_dir . $user_id ."\\". $type;
            $destination_path = str_replace(["/","\\"], [DIRECTORY_SEPARATOR,DIRECTORY_SEPARATOR], $destination_path);
        }

        if(isset($file) && $file->isValid()) {

            if(!is_dir($destination_path)) {
                mkdir($destination_path, 0777,true);
            }

            $extension = '.' . $file->getClientOriginalExtension();
            $destination_filename =  time().'-pic';

            $filename = $destination_filename.$extension;

            $moveable_file = str_replace(["/","\\"], [DIRECTORY_SEPARATOR,DIRECTORY_SEPARATOR], $destination_path.'\\'.$filename);
            move_uploaded_file($file,$moveable_file);

            $s3 = new S3(env('AWS_ACCESS_KEY_ID'), env('AWS_SECRET_ACCESS_KEY'), false, 's3.amazonaws.com',env('AWS_DEFAULT_REGION'));

            if(env("APP_ENV") == 'local') {
                if ($s3->putObjectFile(str_replace([DIRECTORY_SEPARATOR,DIRECTORY_SEPARATOR], ["/","\\"], $moveable_file), env('AWS_BUCKET'), str_replace([DIRECTORY_SEPARATOR,DIRECTORY_SEPARATOR], ["/","\\"], $moveable_file), S3::ACL_PUBLIC_READ)) {
                    unlink($moveable_file);
                }
            } else {
                if ($s3->putObjectFile($moveable_file, env('AWS_BUCKET'), $moveable_file, S3::ACL_PUBLIC_READ)) {
                    unlink($moveable_file);
                }
            }
            $document_exist = $this->documentService->checkSeafarerTypeExist($type,$type_id,$user_id)->toArray();
            // dd( $document_exist );
            if(!empty($document_exist)){
                $document_id = $document_exist['0']['id'];
                if ($type!="pancard" && $type!="aadhaar" && $type!="passbook"){
                    $document_details = array();
                    $document_details['type'] = $type;
                    $document_details['type_id'] = $type_id;
                    $document_details['value'] = 1;
                    $this->changeUserDocumentStatus($document_details,$user_id);
                }
            }else{
                $document = $this->documentService->store($type,$type_id,$user_id);
                $document_id = $document->id;
            }

            if($document_id){
                $result = $this->documentService->storeDocument($filename,$document_id);
            }

            if(isset($result) && isset($document_id)){
                $status_array = ['status' => 'success', 'filename' => $moveable_file, 'id' => $result->id];
            }else{
                 $status_array = ['status' => 'failed'];
            }

        } else {
            $status_array = ['status' => 'failed'];
        }
        return $status_array;
    }

    public static function deleteDocument($img_id,$img_name,$user_id,$type,$type_id){
        $documentService = new DocumentService();
        return $documentService->deleteDocument($img_id,$img_name,$user_id,$type,$type_id);
    }

    public function getUserDocuments($user_id){
        return $this->documentService->getUserDocuments($user_id);
    }

    public function storeRequestedUserDocumentsList($data){

        $data_new = [];
        $data_inserted = 0;

        if(isset($data['user_document_id']) && !empty($data['user_document_id'])) {

            if(isset($data['owner_id']) && !empty($data['owner_id'])) {
                $data_new['owner_id'] = $data['owner_id'];
            }
            if(isset($data['requester_id']) && !empty($data['requester_id'])) {
                $data_new['requester_id'] = $data['requester_id'];
            }

            $owner_id = $data_new['owner_id'];
            $requester_id = $data_new['requester_id'];

            if(isset($owner_id) && !empty($owner_id) && isset($requester_id) && !empty($requester_id)){
                $get_document_permission_data = $this->userDocumentsRepository->getDocumentsPermissionAllList($owner_id,$requester_id)->toArray();
            }

            if(isset($get_document_permission_data) && !empty($get_document_permission_data)){
                $permission_id = $get_document_permission_data[0]['id'];
                $get_document_permission_request_data = $this->userDocumentsRepository->deleteDocumentsPermissionRequestList($permission_id,$data['user_document_id']);
            }

            if(!isset($permission_id)){
                $doc_permission = $this->userDocumentsRepository->storeRequestedUserDocumentsList($data_new);
            }

            foreach($data['user_document_id'] as $user_document_id)
            {
                if(isset($permission_id) && !empty($permission_id)){
                    $permission_req['document_permission_id'] = $permission_id;
                }
                else{
                    $permission_req['document_permission_id'] = $doc_permission->id;
                }
                $permission_req['user_document_id'] = $user_document_id;

                $result = $this->userDocumentsRepository->storeDocumentPermissionReq($permission_req);
            }

            if($result){
                $options['with'] = ['document_permissions.requester.company_registration_detail','document_permissions.requester.institute_registration_detail','document_permissions.document_type.type'];
                $user_data = $this->getDataByUserId($data['owner_id'], $options)->toArray();
                $collated_data = [];

                if(isset($user_data) && !empty($user_data)){
                    $permission = $user_data[0]['document_permissions'][0];
                    $document_type = $permission['document_type'];
                    $collated_data['type'] = '';
                }

                foreach ($document_type as $k => $document_type_details){
                    if(isset($document_type_details) && !empty($document_type_details)){
                        if(empty($collated_data['type'])){

                            if($document_type_details['type']['type_id'] != '0')
                                $collated_data['type'] = strtoupper($document_type_details['type']['type'] .$document_type_details['type']['type_id']);
                            else
                                $collated_data['type'] = strtoupper($document_type_details['type']['type']);

                        }else{
                            if($document_type_details['type']['type_id'] != '0')
                                $collated_data['type'] = $collated_data['type']." , ".strtoupper($document_type_details['type']['type'] . "" . $document_type_details['type']['type_id']);
                            else
                                $collated_data['type'] = $collated_data['type']." , ".strtoupper($document_type_details['type']['type']);
                        }
                    }
                }

                if(isset($user_data[0]['first_name']) && !empty($user_data[0]['first_name'])){
                    $collated_data['first_name'] = $user_data[0]['first_name'];
                }
                if(isset($user_data[0]['email']) && !empty($user_data[0]['email'])){
                    $collated_data['email'] = $user_data[0]['email'];
                }
                if(isset($document_type_details['document_permission_id']) && !empty($document_type_details['document_permission_id'])){
                    $collated_data['document_permission_id'] = $document_type_details['document_permission_id'];
                }
                if(isset($permission['requester']['registered_as']) && !empty($permission['requester']['registered_as'])){
                    $collated_data['registered_as'] = $permission['requester']['registered_as'];
                }

                if($permission['requester']['registered_as'] == 'institute')
                {
                    $institute_details = $permission['requester']['institute_registration_detail'];

                    if(isset($institute_details['user_id']) && !empty($institute_details['user_id'])){
                        $collated_data['requester_user_id'] = $institute_details['user_id'];
                    }
                    if(isset($institute_details['institute_name']) && !empty($institute_details['institute_name'])){
                        $collated_data['requester_name'] = $institute_details['institute_name'];
                    }
                    if(isset($institute_details['email']) && !empty($institute_details['email'])){
                        $collated_data['requester_email'] = $institute_details['email'];
                    }
                }

                if($permission['requester']['registered_as'] == 'company')
                {
                    $company_details = $permission['requester']['company_registration_detail'];

                    if(isset($user_data[0]['id']) && !empty($user_data[0]['id'])){
                        $collated_data['id'] = $user_data[0]['id'];
                    }
                    if(isset($company_details['email']) && !empty($company_details['email'])){
                        $collated_data['company_email_id'] = $company_details['email'];
                    }
                    if(isset($company_details['company_name']) && !empty($company_details['company_name'])){
                        $collated_data['company_name'] = $company_details['company_name'];
                    }
                }


                if(isset($user_data) && !empty($user_data)){
                    if(!isset($permission_id)){
                        $this->sendEmailService->sendRequestedDocumentListEmailToSeafarer($collated_data);
                    }
                    $status_array =['status'=>'success'];
                }
            }
            else{
                $status_array =['status'=>'failed'];
            }

        }else{
            $status_array =['status'=>'failed'];
        }
        return $status_array;

    }

    public function updateRequestedDocumentStatus($status_data)
    {
        if (isset($status_data['document_permission_id']) && !empty($status_data['document_permission_id'])) {
            $document_permission_id = $status_data['document_permission_id'];
        }
        if (isset($status_data['requester_id']) && !empty($status_data['requester_id'])) {
            $requester_id = $status_data['requester_id'];
        }
        if (isset($status_data['owner_id']) && !empty($status_data['owner_id'])) {
            $owner_id = $status_data['owner_id'];
        }
        if (isset($status_data['status']) && !empty($status_data['status'])) {
            $doc_status = $status_data['status'];
        }

        $status = [];
        if(isset($doc_status) && !empty($doc_status)){
            $status['status'] = $doc_status;
        }

        $result = $this->userDocumentsRepository->updateRequestedDocumentStatus($status,$document_permission_id,$requester_id,$owner_id);

        $options['with'] = ['document_permissions.requester.company_registration_detail','document_permissions.requester.institute_registration_detail','document_permissions.document_type.type'];
        $user_data = $this->getDataByUserId($owner_id, $options)->toArray();

        $company_id = $user_data[0]['document_permissions'][0]['requester']['id'];

        $collated_data = [];

        if(isset($user_data) && !empty($user_data)){
            $permission = $user_data[0]['document_permissions'][0];

            if($permission['requester']['registered_as'] == 'institute'){
                $institute_details = $permission['requester']['institute_registration_detail'];
            }
            if($permission['requester']['registered_as'] == 'company'){
                $company_details = $permission['requester']['company_registration_detail'];
            }

            $document_type = $permission['document_type'];
            $collated_data['type'] = '';
        }

        foreach ($document_type as $k => $document_type_details){
            if(isset($document_type_details) && !empty($document_type_details)){
                if(empty($collated_data['type'])){

                    if($document_type_details['type']['type_id'] != '0')
                        $collated_data['type'] = strtoupper($document_type_details['type']['type'] .$document_type_details['type']['type_id']);
                    else
                        $collated_data['type'] = strtoupper($document_type_details['type']['type']);

                }else{
                    if($document_type_details['type']['type_id'] != '0')
                        $collated_data['type'] = $collated_data['type']." , ".strtoupper($document_type_details['type']['type'] . "" . $document_type_details['type']['type_id']);
                    else
                        $collated_data['type'] = $collated_data['type']." , ".strtoupper($document_type_details['type']['type']);
                }
            }
        }

        if(isset($user_data[0]['email']) && !empty($user_data[0]['email'])){
            $collated_data['seafarer_email_id'] = $user_data[0]['email'];
        }

        if(isset($permission['requester']['registered_as']) && !empty($permission['requester']['registered_as'])){
            $collated_data['registered_as'] = $permission['requester']['registered_as'];
        }

        if($permission['requester']['registered_as'] == 'institute')
        {
            if(isset($institute_details['user_id']) && !empty($institute_details['user_id'])){
                $collated_data['requester_user_id'] = $institute_details['user_id'];
            }
            if(isset($document_type_details['id']) && !empty($document_type_details['id'])){
                $collated_data['document_permission_id'] = $document_type_details['id'];
            }
            if(isset($user_data[0]['first_name']) && !empty($user_data[0]['first_name'])){
                $collated_data['first_name'] = $user_data[0]['first_name'];
            }
            if(isset($institute_details['email']) && !empty($institute_details['email'])){
                $collated_data['email'] = $institute_details['email'];
            }
        }

        if($permission['requester']['registered_as'] == 'company')
        {
            if(isset($company_id) && !empty($company_id)){
                $collated_data['id'] = $company_id;
            }
            if(isset($document_type_details['id']) && !empty($document_type_details['id'])){
                $collated_data['document_permission_id'] = $document_type_details['id'];
            }
            if(isset($permission['requester']['first_name']) && !empty($permission['requester']['first_name'])){
                $collated_data['first_name'] = $permission['requester']['first_name'];
            }
            if(isset($user_data[0]['first_name']) && !empty($user_data[0]['first_name'])){
                $collated_data['first_name'] = $user_data[0]['first_name'];
            }
            if(isset($company_details['email']) && !empty($company_details['email'])){
                $collated_data['email'] = $company_details['email'];
            }
            if(isset($company_details['company_name']) && !empty($company_details['company_name'])){
                $collated_data['company_name'] =$company_details['company_name'];
            }
        }

        if(isset($doc_status) && !empty($doc_status)){
            $collated_data['status'] = $doc_status;
        }

        if($collated_data['status'] == '1'){
            $collated_data['message'] = 'Your request is accepted, now you can see private documents';
        }
        else{
            $collated_data['message'] = 'Your request is rejected.';
        }

        if ($result) {
            if(isset($user_data) && !empty($user_data)){
                $this->sendEmailService->sendRequestedDocumentStatusEmailToCompany($collated_data);
            }
            $status_array = ['status' => 'success', 'status_data' => $result];
        } else {
            $status_array = ['status' => 'failed'];
        }

        return $status_array;
    }

    public function storeDocumentsPermissions($permissions,$user_id){
        return $this->documentService->storeDocumentsPermissions($permissions,$user_id);
    }

    public function storeRequestedTypeDocumentsList($requested_type,$owner_id,$requester_id){
        return $this->documentService->storeRequestedTypeDocumentsList($requested_type,$owner_id,$requester_id);
    }

    public function changeUserDocumentStatus($document_details,$user_id){
        return $this->documentService->changeUserDocumentStatus($document_details,$user_id);
    }

    public function getUserWithGivenAvilability(){
        $users = $this->userRepository->getUserWithGivenAvilability()->toArray();
        $availability = [];

        foreach ($users as $key => $user) {
            if(!empty($user['professional_detail']['availability'])){
                $start = $user['professional_detail']['availability'];

                $start  = date_create(date('Y-m-d',strtotime($start)));
                $end    = date_create(); // Current time and date
                $diff   = date_diff( $start, $end );

                $uploaded_before = $diff->format("%a");

                if($uploaded_before == '1'){
                    $availability['1'][] = $user;
                }
                elseif($uploaded_before == '5'){
                    $availability['5'][] = $user;
                }
                elseif($uploaded_before == '90'){
                    $availability['90'][] = $user;
                }
            }
        }

        return $availability;

    }
}

?>