<?php namespace App\Services;

use Illuminate\Support\Facades\Auth;
use App\Services\ShortenUrlService;


class SendSmsService {

    private $shortenUrlService;
    
    function __construct(){
        $this->shortenUrlService = new ShortenUrlService();
    }

    public function sendSmsToNewRegistered($array, $type = null) {

        $curl = curl_init();
        $api_key = env('SMS_API_KEY');
        
        if(isset($array['first_name']) && !empty($array['first_name']))
            $first_name = $array['first_name'];

        if(isset($array['firstname']) && !empty($array['firstname']))
            $first_name = $array['firstname']; 

        if(isset($array['contact_person']) && !empty($array['contact_person']))
            $first_name = $array['contact_person'];

        if(isset($array['mobile']) && !empty($array['mobile']))
            $mobile = $array['mobile'];

        if(isset($array['contact_person_number']) && !empty($array['contact_person_number']))
            $mobile = $array['contact_person_number'];

        if($array['registered_as'] == 'seafarer'){
            $profile = route('user.view.profile',$array['id']);
        }
        if($array['registered_as'] == 'advertiser'){
            $profile = route('site.advertiser.profile.id',$array['id']);
        }
        if($array['registered_as'] == 'company'){
            $profile = route('site.show.company.details.user',$array['id']);
        }
        if($array['registered_as'] == 'institute'){
           $profile = route('site.show.institute.details.id',$array['id']);
        }
        
        try {
            $shorten_url = $this->shortenUrlService->shortenUrl($profile);
        } catch( \Exception $e ){
            $url = $profile;
        }

        if(isset($shorten_url) && !empty($shorten_url)){
            $url = $shorten_url['url'];
        }
        
        $api_key = env('SMS_API_KEY');

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://2factor.in/API/V1/$api_key/ADDON_SERVICES/SEND/TSMS",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "{\"From\": \"COSEAS\",\"To\": \"$mobile\", \"TemplateName\": \"newregistration\", \"VAR1\": \"$first_name\", \"VAR2\": \"$url\"}",
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
          echo "cURL Error #:" . $err;
        } else {
          //echo $response;
        }
    }

    public function sendSmsToNewRegisteredThroughUploads($array, $type = null) {
        $current_route = route('complete_registration',base64_encode($array['mobile']));
        $temp_route = explode('.com', $current_route);
        $final_route = env('DOMAIN').'/complete-registration/'.base64_encode($array['mobile']);

        try {
            $shorten_url = $this->shortenUrlService->shortenUrl($final_route);
        } catch( \Exception $e ){
            $shorten_url['url'] = $final_route;
        }

        if($type == 'company') {
            $sms_arr['msg'] = 'Welcome to Constructionwale.com. You are already registered with us, please click the following link '.$shorten_url['url'].' to update your profile to expand your network and to get invitation for knowledge sharing seminars being organised in your city.';
        } elseif($type == 'user') {
            $sms_arr['msg'] = 'Welcome to Constructionwale.com. You are already registered with us, please click the following link '.$shorten_url['url'].' to update your profile to expand your network and to get invitation for knowledge sharing seminars being organised in your city.';
        } else{
            $sms_arr['msg']  = 'Welcome to Constructionwale.com. You are already Registered with us, please click the following link '.$shorten_url['url'].' to update your profile to expand your business/ network and attend seminars for knowledge sharing.';
        }

        $sms_arr['msisdn'] = $array['mobile'];

        // $sms_arr['mobile'] = 7738818447;
        $this->sendSms($sms_arr);
    }

    public function sendMobileVerifySms($mobile, $otp){

        $curl = curl_init();
        $api_key = env('SMS_API_KEY');

		echo '123456';
		die;
        //https://2factor.in/API/V1/{api_key}/SMS/{phone_number}/{otp}/{template_name}
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://2factor.in/API/V1/$api_key/SMS/$mobile/$otp/mobileotp",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
          echo "cURL Error #:" . $err;
        } else {
          echo $response;
        }
    }


    public function sendResetPasswordSms($array) {
        $sms_arr['msg'] = 'Hello, please click this link '.$array["link"].' to reset your password.';
        $sms_arr['msisdn'] = $array['mobile'];
        $this->sendSms($sms_arr);
    }
    public function sendOrderConfirmationSms($array) {
        $shorten_url = $this->shortenUrlService->shortenUrl(route('view_product_order',$array[0]['id']));
        // $shorten_url['url'] = 'test_url';

        $sms_arr['msg'] = 'Order Successful: Your order no. '.$array[0]['id'].' on Constructionwale.com for Rs. '.$array[0]['total_cost'].' was successful. To view the order click '.$shorten_url['url'].'.';
        $sms_arr['msisdn'] = $array[0]['user']['mobile'];
        // dd($sms_arr);
        $this->sendSms($sms_arr);
    }

    public function sendWholeOrderCancelledSms($array) {
        $shorten_url = $this->shortenUrlService->shortenUrl(route('view_product_order',$array[0]['id']));
        // $shorten_url['url'] = 'test_url';

        $sms_arr['msg'] = 'Order Cancelled: Your order no. '.$array[0]['id'].' on Constructionwale.com for Rs. '.$array[0]['total_cost'].' has been cancelled. To view the order click '.$shorten_url['url'].'.';
        $sms_arr['msisdn'] = $array[0]['user']['mobile'];
        // dd($sms_arr);
        // $this->sendSms($sms_arr);
    }

    public function sendOrderCancelledSms($product_order_item) {
        $shorten_url = $this->shortenUrlService->shortenUrl(route('view_product_order',$product_order_item[0]['product_order_id']));
        // $shorten_url['url'] = 'test_url';
        $product_detail = json_decode($product_order_item[0]['product_variant_details']);
        $product_name = $product_detail->name;
        $short_product_name = ucwords(substr(trim($product_name), 0, 10)).'...';

        $sms_arr['msg'] = 'Order Cancelled: '.$short_product_name.' from your order no. '.$product_order_item[0]['product_order_id'].' on Constructionwale.com has been cancelled. To view the order click '.$shorten_url['url'].'.';
        $sms_arr['msisdn'] = $product_order_item[0]['product_order']['user']['mobile'];
        // dd($sms_arr);
        $this->sendSms($sms_arr);
    }

    public function sendOrderDispatchSms($array) {
        $shorten_url = $this->shortenUrlService->shortenUrl(route('view_product_order',$array[0]['dispatched_order_items'][0]['product_order_id']));
        // $shorten_url['url'] = 'test_url';
        $product_detail = json_decode($array[0]['dispatched_order_items'][0]['product_variant_details']);
        $product_name = $product_detail->name;
        $delivery_to_date = $array[0]['dispatched_order_items'][0]['delivery_to'];
        $order_placed_date = $array[0]['dispatched_order_items'][0]['product_order']['created_at'];
        $dispateched_at = $array[0]['dispatched_order_items'][0]['updated_at'];

        $day_difference = floor((strtotime($dispateched_at) - strtotime($order_placed_date)) / (60 * 60 * 24));

        $excepted_date = $delivery_to_date - $day_difference;
        $short_product_name = ucwords(substr(trim($product_name), 0, 10)).'...';
        $sms_arr['msg'] = "Order Dispatched: ".$short_product_name." with order no. ".$array[0]['dispatched_order_items'][0]['product_order']['id']." on Constructionwale.com will be delivered within ".$excepted_date." days. To view the order click ".$shorten_url['url'].'.';
        $sms_arr['msisdn'] = $array[0]['dispatched_order_items'][0]['product_order']['user']['mobile'];
        $this->sendSms($sms_arr);
    }

    public function sendOrderDeliveredSms($array) {
        $shorten_url = $this->shortenUrlService->shortenUrl(route('view_product_order',$array[0]['delivered_order_items'][0]['product_order']['id']));
        // $shorten_url['url'] = 'test_url';
        $product_detail = json_decode($array[0]['delivered_order_items'][0]['product_variant_details']);
        $product_name = $product_detail->name;
        $delivered_at = date('H:i a, d/m/Y', strtotime($array[0]['delivered_order_items'][0]['updated_at']));

        $short_product_name = ucwords(substr(trim($product_name), 0, 10)).'...';
        $sms_arr['msg'] = "Order Delivered: ".$short_product_name." with order no. ".$array[0]['delivered_order_items'][0]['product_order']['id']." from Constructionwale.com was successfully delivered at ".$delivered_at.". To view the order click ".$shorten_url['url'].'.';
        $sms_arr['msisdn'] = $array[0]['delivered_order_items'][0]['product_order']['user']['mobile'];
        // dd($sms_arr);
        $this->sendSms($sms_arr);
    }

    public function sendOrderItemReturnInitiatedSms($array) {
        $shorten_url = $this->shortenUrlService->shortenUrl(route('view_product_order',$array[0]['id'])); // same order id as it is..
        // $shorten_url['url'] = 'test_url';
        $product_detail = json_decode($array[0]['returned_items_detail'][0]['product_sub_order_item']['product_variant_details']);
        $product_name = $product_detail->name;
        $initiated_date = date('', strtotime($array[0]['returned_items_detail'][0]['return_initated']));

        $short_product_name = ucwords(substr(trim($product_name), 0, 10)).'...';
        $sms_arr['msg'] = "Return Initiated: ".$short_product_name." against return request no. ".$array[0]['returned_items_detail'][0]['id']." is being processed. To view the return request click ".$shorten_url['url'].'.';
        $sms_arr['msisdn'] = $array[0]['user']['mobile'];
        // dd($sms_arr);
        $this->sendSms($sms_arr);
    }

    public function sendOrderItemReturnAcceptedSms($array) {
        $shorten_url = $this->shortenUrlService->shortenUrl(route('view_product_order',$array['product_sub_order_item']['product_order']['id']));
        // $shorten_url['url'] = 'test_url';
        $product_detail = json_decode($array['product_sub_order_item']['product_variant_details']);
        $product_name = $product_detail->name;
        
        $short_product_name = ucwords(substr(trim($product_name), 0, 10)).'...';
        $sms_arr['msg'] = "Return Accepted: ".$short_product_name." against return request no. ".$array['id']." is accepted. Pickup has been scheduled and refund will be initiated once the pickup is done successfully. To view the return request click ".$shorten_url['url'].'.';
        $sms_arr['msisdn'] = $array['product_sub_order_item']['product_order']['user']['mobile'];
        // dd($sms_arr);
        $this->sendSms($sms_arr);
    }

    public function sendOrderItemReturnPickedUpSms($array) {
        $shorten_url = $this->shortenUrlService->shortenUrl(route('view_product_order',$array['product_sub_order_item']['product_order']['id']));
        // $shorten_url['url'] = 'test_url';
        $product_detail = json_decode($array['product_sub_order_item']['product_variant_details']);
        $product_name = $product_detail->name;
        
        $short_product_name = ucwords(substr(trim($product_name), 0, 10)).'...';
        $sms_arr['msg'] = "Return Picked-Up: We have received the product returned against return request no. ".$array['id'].". To view the return request click ".$shorten_url['url'].'.';
        $sms_arr['msisdn'] = $array['product_sub_order_item']['product_order']['user']['mobile'];
        // dd($sms_arr);
        // 
        $this->sendSms($sms_arr);
    }

    public function sendOrderItemReturnRefundedSms($array) {
        $shorten_url = $this->shortenUrlService->shortenUrl(route('view_product_order',$array['product_sub_order_item']['product_order']['id']));
        // $shorten_url['url'] = 'test_url';
        $product_detail = json_decode($array['product_sub_order_item']['product_variant_details']);
        $product_name = $product_detail->name;
       
        $sms_arr['msg'] = "Return Refunded: We have processed your refund for ".$product_name." returned against return request no. ".$array['id'].". To view the return request click ".$shorten_url['url'].'.';
        $sms_arr['msisdn'] = $array['product_sub_order_item']['product_order']['user']['mobile'];
        // dd($sms_arr);
        $this->sendSms($sms_arr);
    }

    public function sendSms($array){

    }

    public function build_query(Array $params) {

        $ret = array();
        $fmt = '%s=%s';
        $separator = '&';
        foreach ($params as $k => $v) {
            if (is_array($v)) {
                foreach ($v as $_v) {
                    array_push($ret, sprintf($fmt, $k, urlencode($_v)));
                }
            } else {
                array_push($ret, sprintf($fmt, $k, urlencode($v)));
            }
        }
        return implode($separator, $ret);
    }
}