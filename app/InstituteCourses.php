<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InstituteCourses extends Model
{
    protected $table = 'institute_courses';

    protected $fillable = ['course_type', 'course_id','institute_id','status','duration','cost','description','size','eligibility','documents_req','key_topics','certificate_validity','approved_by'];

    public function course_details(){
        return $this->hasOne('App\Courses','id','course_id');
    }

    public function courses(){
        return $this->hasMany('App\Courses','id','course_id');
    }

    public function course_type_details(){
    	return $this->hasOne('App\Courses','course_type','course_type');
    }

    public function institute_batches(){
        return $this->hasMany('App\InstituteBatches','institute_course_id','id');
    }

    public function company_registration_details(){
    	return $this->hasOne('App\CompanyRegistration','company_id','company_id');
    }

    public function user(){
        return $this->belongsTo('App\User','institute_id','id');
    }

    public function institute_details(){
        return $this->belongsTo('App\InstituteDetail','institute_id','institute_id');
    }

    public function institute_registration_detail(){
        return $this->belongsTo('App\InstituteRegistration','institute_id','id');
    }

    public function institute_registration_details(){
        return $this->belongsTo('App\InstituteRegistration','institute_id','id');
    }

    public function active_institute_batches(){
        return $this->hasMany('App\InstituteBatches','institute_course_id','id');
    }
}
