<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InstituteBatchLocation extends Model
{
    protected $table = 'institute_batch_locations';

    protected $fillable = ['batch_id','location_id','status'];

    public function location(){
        return $this->hasOne('App\InstituteLocation','id', 'location_id');
    }
}
