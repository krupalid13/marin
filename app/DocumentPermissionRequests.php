<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DocumentPermissionRequests extends Model
{
    protected $table = 'document_permission_requests';

    protected $fillable = ['user_document_id','document_permission_id'];

    public function type(){
        return $this->belongsTo('App\UserDocumentsType' ,'user_document_id' ,'id');
    }
}
