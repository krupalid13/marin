<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InstituteLocation extends Model
{
    protected $table = 'institute_locations';

    protected $fillable = ['institute_id', 'country', 'state_id', 'city_id', 'pincode','pincode_text', 'address','pincode_id','state_text','city_text','headbranch','branch_type','email','number'];

    public function state(){
        return $this->belongsTo('App\State');
    }

    public function city(){
        return $this->belongsTo('App\City');
    }

    public function pincode(){
        return $this->belongsTo('App\Pincode');
    }

    public function location_contact(){
        return $this->hasMany('App\InstituteLocationContact','location_id', 'id');
    }
}
