<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyJobNationality extends Model
{
    protected $table = 'company_job_nationalities';

    protected $fillable = ['job_id','nationality'];
}
