<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Crypt;
use App\CourseJob;

class Vaccination extends Model
{
    protected $table = 'vaccination';

    public static function getVaccinationDropDown(){

    	return self::pluck('name','id')->toArray();
    }
}
