<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InstituteRegistration extends Model
{
    protected $table = 'institute_registration';

    protected $fillable = ['institute_id','user_id','institute_name', 'email', 'password', 'contact_number', 'contact_person', 'contact_email', 'website', 'status'];

    public function institute_login_details(){
        return $this->hasMany('App\User','id','institute_id');
    }

    public function institute_detail(){
        return $this->hasOne('App\InstituteDetail','institute_id', 'id');
    }

    public function institute_details(){
        return $this->hasOne('App\InstituteDetail','institute_id', 'id');
    }

    public function institute_locations(){
        return $this->hasMany('App\InstituteLocation','institute_id', 'id');
    }

    public function institute_courses(){
        return $this->hasMany('App\InstituteCourses','institute_id', 'id');
    }

    public function user(){
        return $this->hasOne('App\User','id', 'user_id');
    }

    public function active_subscription(){
        return $this->hasMany('App\InstituteSubscription','institute_reg_id','id');
    }

    public function advertisment_details(){
        return $this->hasMany('App\AdvertiseDetails','company_id','user_id');
    }

    public function institute_notice(){
        return $this->hasOne('App\InstituteNotice','institute_id','user_id');
    }

    public function institute_gallery(){
        return $this->hasMany('App\InstituteImageGallery','institute_id','user_id');
    }

}
