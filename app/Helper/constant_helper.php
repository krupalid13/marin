<?php

use App\UserHomeShip;

	 function flashMessage($type,$message)
	 {
	 	\Session::put($type,$message);
	 }

	 function gethomeship(){
	 	$homeships = UserHomeShip::select('id','type','typevalue')->get();
	 	return $homeships;
	 }
	 function getStatusIcon($data){

	 	if ($data->status == 1) {
	 		return '<span title="'.trans('lang_data.click_on_button_change_status_label').'" class="btn btn-xs btn-success" id="active_inactive"
	 		 status="1" data-id="'.\Crypt::encrypt($data->id).'">'.trans('lang_data.active_label').'</span>';
	 	}else{
	 		return '<span title="'.trans('lang_data.click_on_button_change_status_label').'" class="btn btn-xs btn-danger" id="active_inactive" 
	 		status="0" data-id="'.\Crypt::encrypt($data->id).'">'.trans('lang_data.inactive_label').'</span>';
	 	}
	 }

	function GENERATE_CSRF_TOKEN()
	{
		return "<input type='hidden' name='_token' value='".csrf_token()."'>";
	}
	
    /**
     * This funciton is used to conver any title to slug
     * @param  $str
     * @param  $delimeter
     * @return string
     */
    function CREATE_SLUG($str, $delimiter = '-'){

        $slug = strtolower(trim(preg_replace('/[\s-]+/', $delimiter, preg_replace('/[^A-Za-z0-9-]+/', $delimiter, preg_replace('/[&]/', 'and', preg_replace('/[\']/', '', iconv('UTF-8', 'ASCII//TRANSLIT', $str))))), $delimiter));
        return $slug;
    } 

	function GENERATE_TOKEN()
	{
		$characters = 'abcdefghijklmnopqrstuvwxyz0123456789';
		$token = '';
		for ($i = 0; $i < 6; $i++)
		{
			$token .= $characters[rand(0, strlen($characters) - 1)];
		}
		$token = time().$token.time();
		return $token;
	}

	 function COURSE_JOB_IMAGE_UPLOAD_PATH(){

	 	return UPLOAD_AND_DOWNLOAD_PATH().'/upload/courseJob/';
	 }
	 function COURSE_JOB_IMAGE_UPLOAD_URL(){

	 	return UPLOAD_AND_DOWNLOAD_URL().'public/upload/courseJob/';
	 }

 	 function UPLOAD_AND_DOWNLOAD_PATH(){

	 	return public_path();
	 }

 	function UPLOAD_FILE($r,$name,$uploadPath){
 		
 	 	$image = $r->$name;
 		$name = time().'.'.$image->getClientOriginalName();
 		
        $destinationPath = $uploadPath;
        $image->move($destinationPath, $name);
        return  $name;
	}

	function generateRandomNumberCode($n = null) { 
		$n=10; 
	    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'; 
	    $randomString = ''; 
	  
	    for ($i = 0; $i < $n; $i++) { 
	        $index = rand(0, strlen($characters) - 1); 
	        $randomString .= $characters[$index]; 
	    } 
	  
	    return $randomString; 
	} 

	function getRankCommonFuncion($id){

	  	$rankData = \CommonHelper::new_rank();
	  	
        if (isset($rankData['Deck Officers'][$id])) {
            
            return $rankData['Deck Officers'][$id];

        }elseif (isset($rankData['Engineers'][$id])) {

            return $rankData['Engineers'][$id];
        	
        }elseif (isset($rankData['Others'][$id])) {
        	
            return $rankData['Others'][$id];

        }else{

            return '-';
        }
	}

	function getMultipleCountryName($countryArray){

		$countryName = '';
	  	$country = \CommonHelper::countries();

	  	if (isset($countryArray) && !empty($countryArray)) {
	  		
	  		$total =  count($countryArray);
	  		$i=1;
	  		foreach ($countryArray as $key => $value) {
	  			
	  			if(!empty($value)) {
	  				$commep ='';
	  				if (isset($country[$value])) {
	  					if ($total != $i) {
	  						$commep = ",";
  						}			
	  					$countryName .=	$country[$value].''.$commep;		

					}		
	  			}
	  			$i = $i+1;
	  		}
	  	}

	  	return $countryName;
	  	
	}

	function getCountryWithExperience($year,$month){

		$exp = '';

		if ($year !=0) {
			
			if ($year > 1) {
				$st = 'years';
			}else{
				$st = 'year ';
			}
			$exp .=$year.' '.$st; 
		}

		if ($month !=0) {
			
			if ($month > 1) {
				$st = 'months';
			}else{
				$st = 'month';
			}
			$exp .=$month.' '.$st; 

		}
		return $exp;
	}

	function getCommonFunctionForJobSearch(){

		$input = request()->all();

		$sql = \App\SendedJob::select('*')
                                ->with([
                                        'courseJob',
                                        'courseJob.jobStatus',
                                        'courseJob.company',
                                        'user'
                                    ])
                                ->has('courseJob')
                                ->has('user');

        if (isset($input['job_post_to']) && !empty($input['job_post_to']) && isset($input['job_post_from']) && !empty($input['job_post_from'])) {
                                          
            $sql->where('created_at','>=',$input['job_post_to'])
                ->where('created_at','<=',$input['job_post_from']);                              

        }elseif (isset($input['job_post_to']) && !empty($input['job_post_to'])) {
            
            $sql->where('created_at','>=',$input['job_post_to']);                              

        }elseif (isset($input['job_post_from']) && !empty($input['job_post_from'])) {
                
            $sql->where('created_at','<=',$input['job_post_from']);                              
        }

        if (isset($input['application_to']) && !empty($input['application_to']) && isset($input['application_from']) && !empty($input['application_from'])) {
                                          
            $sql->where('applied_date','>=',$input['application_to'])
                ->where('applied_date','<=',$input['application_from']);                              

        }elseif (isset($input['application_to']) && !empty($input['application_to'])) {
            
            $sql->where('applied_date','>=',$input['application_to']);                              

        }elseif (isset($input['application_from']) && !empty($input['application_from'])) {
                
            $sql->where('applied_date','<=',$input['application_from']);                              
        }

        if (isset($input['job_created_to']) && !empty($input['job_created_to']) && isset($input['job_created_from']) && !empty($input['job_created_from'])) {
            

            $sql->whereHas('courseJob',function($query) use($input){

                $query->where('created_at','>=',$input['job_created_to'])
                    ->where('created_at','<=',$input['job_created_from']);                              
            }); 


        }elseif (isset($input['job_created_to']) && !empty($input['job_created_to'])) {
            
            $sql->whereHas('courseJob',function($query) use($input){

                $query->where('created_at','>=',$input['job_created_to']);                              
            });

        }elseif (isset($input['job_created_from']) && !empty($input['job_created_from'])) {
                
            $sql->whereHas('courseJob',function($query) use($input){

                $query->where('created_at','<=',$input['job_created_from']);                              
            });                               
        } 

        if (isset($input['all_applied']) && $input['all_applied'] == 2) {
                                          
           $sql->whereNotNull('applied_date');                              
        }           

        if (isset($input['company_id']) && !empty($input['company_id'])) {
                                          
           $sql->whereHas('courseJob',function($query) use($input){

                 $query->whereIn('company_id',$input['company_id']);
           });      
        }

        if (isset($input['rank_id']) && !empty($input['rank_id'])) {
                                          
           $sql->whereHas('courseJob',function($query) use($input){

                 $query->where('rank_id',$input['rank_id']);
           });      
        }           

        if (isset($input['title']) && !empty($input['title'])) {
                                          
           $sql->whereHas('courseJob',function($query) use($input){

                 $query->where('title', 'LIKE', '%' . $input['title'] . '%');
           });      
        }

        if (isset($input['user_name']) && !empty($input['user_name'])) {
                                          
           $sql->whereHas('user',function($query) use($input){

                 $query->where('first_name', 'LIKE', '%' . $input['user_name'] . '%')
                        ->orWhere('last_name', 'LIKE', '%' . $input['user_name'] . '%');
           });      
        }                    

        return $sql;
	} 

	function getTotalCompany(){

		$commonSearchQuery = getCommonFunctionForJobSearch();
		
		$total = $commonSearchQuery->groupBy('course_job_id')->get();
		
		$array = array();

		if (!$total->isEmpty()) {
			
			foreach ($total as $key => $value) {
				
				$array[] = $value->courseJob->company_id; 
			}

			return count(array_unique($array));

		}else{

			return count($array);
		}
	}

	function getTotalJobCreated(){

		$commonSearchQuery = getCommonFunctionForJobSearch();
		
		$data = $commonSearchQuery->groupBy('course_job_id')->get();

		return count($data); 
	}

	function getTotalPostSent(){

		$commonSearchQuery = getCommonFunctionForJobSearch();
		
		$data = $commonSearchQuery->get();

		return count($data); 
	}

	function getTotalJobApplication(){

		$commonSearchQuery = getCommonFunctionForJobSearch();
		
		$data = $commonSearchQuery->whereNotNull('applied_date')->get();

		return count($data); 
	}

	function getTotalUserApproached(){

		$commonSearchQuery = getCommonFunctionForJobSearch();
		
		$data = $commonSearchQuery->groupBy('user_id')->get();

		return count($data); 
	}

	function getVaccinationList(){

		$data = [

			0=>'Yellow Fever',
			1=>'Cholera',
			2=>'Hepatitis B',
			3=>'Hepatitis C',
			4=>'Diphtheria & Tetanus',
			5=>'Covid19',
		];

		return $data;
	}

	function getDceList(){

		$data = [

			0=>'Oil',
			1=>'Chemical',
			2=>'Liquefied Gas',
			3=>'Oil + Chemical + Liquefied Gas',
		];
		
		return $data;
	}

	function getJobStatusList(){

		$data = [

			0=>'Urgent',
			1=>'Reoccurring',
			2=>'One Time',
			3=>'Short',
		];
		
		return $data;
	}

?>
