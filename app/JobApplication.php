<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobApplication extends Model
{
    protected $table = 'job_application';

    protected $fillable = ['job_id', 'user_id', 'company_id'];

    public function user(){
    	return $this->belongsTo('App\User');
    }

    public function company_details(){
    	return $this->hasOne('App\CompanyDetail','company_id', 'company_id');
    }

    public function user_applied_jobs(){
        return $this->hasMany('App\JobApplication','job_id', 'id');
    }

    public function company_job(){
        return $this->hasOne('App\CompanyJob','id', 'job_id');
    }
}
