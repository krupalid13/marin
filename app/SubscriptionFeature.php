<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubscriptionFeature extends Model
{
    protected $table = 'subscription_features';

    protected $fillable =['subscription_id','title','count','is_active'];


}
