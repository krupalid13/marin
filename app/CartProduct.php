<?php namespace App;
use Illuminate\Database\Eloquent\Model;

class CartProduct extends Model{

	protected $table = 'cart_products';

	protected $fillable = ['cart_key','user_id','price','quantity','subscription_id'];

	public function user(){
		return $this->belongsTo('App\User');
	}

	public function product(){
		return $this->belongsTo('App\Product');
	}

	public function product_variant(){
		return $this->belongsTo('App\ProductVariant');
	}

	public function seller(){
		return $this->belongsTo('App\CompanyDetail');
	}

	public function site_p_category(){
		return $this->belongsTo('App\SitePCategory');
	}

	public function subscription(){
		return $this->belongsTo('App\Subscription');
	}

}
?>