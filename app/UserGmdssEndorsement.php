<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserGmdssEndorsement extends Model
{
    protected $table = 'user_gmdss_endorsement';

    protected $fillable = ['user_id', 'gmdss_number', 'gmdss_expiry_date'];
}
