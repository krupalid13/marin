<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Schedule extends Model
{
    protected $table = 'schedules';

    protected $fillable = ['role','country','states','cities','schedule_date_time','type','message','error_message','status','subject'];
}
