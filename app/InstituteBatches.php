<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InstituteBatches extends Model
{
    protected $table = 'institute_batches';

    protected $fillable = ['institute_course_id','course_type', 'course_name','start_date','duration','cost','size','status','reserved_seats','payment_type','initial_per','pending_per','initial_amt','pending_amt','batch_type','on_demand_batch_type','parent_batch_id','preffered_type','institute_id','course_start_date','available_size'];

    public function Courses(){
        return $this->hasMany('App\Courses','course_type', 'course_type');
    }

    public function institute_details(){
        return $this->hasMany('App\User','id', 'institute_id');
    }

    public function institute_registration_details(){
        return $this->hasMany('App\InstituteRegistration','user_id', 'institute_id');
    }

    public function user_applied_courses(){
        return $this->hasMany('App\CourseApplication','course_id', 'id');
    }

    public function batch_location(){
        return $this->hasMany('App\InstituteBatchLocation','batch_id', 'id');
    }

    public function institute_location(){
        return $this->hasMany('App\InstituteBatchLocation','batch_id', 'id');
    }

    public function user(){
        return $this->hasMany('App\User','id', 'institute_id');
    }

    public function orders(){
        return $this->hasMany('App\Order','batch_id', 'id');
    }

    public function course_details(){
        return $this->hasOne('App\InstituteCourses','id', 'institute_course_id');
    }

    public function child_batches()
    {
        return $this->hasMany('App\InstituteBatches','parent_batch_id', 'id');
    }

    public function batch_discount(){
        return $this->hasOne('App\AdvertiserCourseDiscountMapping','batch_id', 'id');
    }

    public function discount(){
        return $this->hasMany('App\AdvertiserCourseDiscountMapping','batch_id', 'id');
    }
}
