<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyDownloads extends Model
{
     protected $table = 'company_dowloads';

    protected $fillable = ['company_id','url','type','seafarer_id'];

    public function user_details(){
        return $this->hasOne('App\User','id','seafarer_id');
    }
}
