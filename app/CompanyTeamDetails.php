<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyTeamDetails extends Model
{
    protected $table = 'company_team_details';

    protected $fillable = ['company_id','location_id','agent_name','agent_designation','profile_pic'];

    public function location(){
        return $this->hasOne('App\CompanyLocationDetails','id','location_id');
    }

}
