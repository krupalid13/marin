<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Yajra\Datatables\Datatables;
use Crypt;
use App\CourseJob;

class JobMultipleGmdssCountry extends Model
{
    protected $table = 'job_multiple_gmdss_countries';
}
