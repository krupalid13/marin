<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SeamanBookEnquiry extends Model
{
    protected $table = 'seaman_book_enquiries';

    protected $fillable = ['enquiry_for','name','phone','email','message'];
}
