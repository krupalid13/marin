<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FeatureEmail extends Model
{
    protected $table = 'feature_emails';

    protected $fillable = ['parent_id','registered_as'];

}
