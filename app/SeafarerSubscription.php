<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SeafarerSubscription extends Model
{
    protected $table = 'seafarer_subscriptions';

    protected $fillable =['user_id','order_id','subscription_details','status','valid_from','valid_to','created_at','updated_at'];

    public function subscriptionFeatures()
    {
        return $this->hasMany('App\SubscriptionFeature');
    }

    public function order()
    {
        return $this->hasOne('App\Order','id','order_id');
    }
}
