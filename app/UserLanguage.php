<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserLanguage extends Model
{
    protected $table = 'user_languages';

    protected $fillable = ['user_id', 'language', 'can_read', 'can_speak'];
}
