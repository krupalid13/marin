<?php 

namespace App;
use Illuminate\Database\Eloquent\Model;

class PincodeState extends Model{

	protected $table = 'pincodes_states';

	protected $fillable = ['pincode_id','state_id'];

	public function pincode(){
		return $this->belongsTo('App\Pincode');
	}

	public function state(){
		return $this->belongsTo('App\State');
	}
	
}

?>