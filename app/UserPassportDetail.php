<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserPassportDetail extends Model
{
    protected $table = 'user_passport_details';

    protected $fillable = ['user_id', 'pass_country', 'pass_number', 'place_of_issue', 'pass_expiry_date', 'us_visa', 'us_visa_expiry_date', 'indian_pcc', 'indian_pcc_issue_date','pass_issue_date','fromo'];
}
