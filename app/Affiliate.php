<?php 
namespace App;
use Illuminate\Database\Eloquent\Model;
use Yajra\Datatables\Datatables;

class Affiliate extends Model{

	protected $table = 'affiliate';

	protected $guarded = [];

	public function user_details(){
    	return $this->hasOne('App\User','id','user_id');
    }

    public function getAjaxAffiliateListDatatable($request){
        if ($request->method() == 'GET') {
            $sql = self::select('*');

            return Datatables::of($sql)
                    ->editColumn('name', function ($data) {
                        return $data->name;
                    })
                    ->editColumn('email', function ($data) {
                        return !empty($data->email) ? $data->email : '-' ;
                    })
                    ->editColumn('phone_number', function ($data) {
                        return !empty($data->phone_number) ? $data->phone_number : '-' ;
                    })
                    ->editColumn('redemption', function ($data) {
                             return !empty($data->redemption) ? $data->redemption : '-' ;
                           
                    })
                    ->addColumn('action', function ($data) {
                        $string = '<a class="btn btn-xs btn-primary" href="#" onclick="viewData('.$data->user_id.')"><i class="fa fa-eye" aria-hidden="true" title="View Affiliate"></i></a>&nbsp;<a class="btn btn-xs btn-primary" href="' . route('admin.affiliate.edit',$data->id) . '"><i class="fa fa-pencil-square-o" aria-hidden="true" title="Edit Affiliate"></i></a> <a href="javascript:void(0)" data-redemption="'.$data->redemption.'" title="Copy link"  class="btn btn-xs btn-info copy_link" target="_blank"><i class="fa fa-copy" aria-hidden="true"></i></a>';
                        return $string;
                    })
                    ->make(true);
            
        }
    }
	
}

?>