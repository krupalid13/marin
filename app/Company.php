<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Yajra\Datatables\Datatables;
use Crypt;
use App\CompanyEmailAddress;
use App\CourseJob;

class Company extends Model
{
    protected $table = 'companies';

    public function companyEmailAddress(){

        return $this->hasMany('App\CompanyEmailAddress','company_id','id')->where('is_default',2);
    }

    public function companyContacts(){

        return $this->hasMany('App\CompanyContacts','company_id','id');
    }
    /**
     * get Company List Datatable function
     * @author Chirag G
     * @return void
     * @author
     **/
    public function getAjaxCompanyListDatatable($request)
    {
    	if ($request->method() == 'GET') {

        	$sql = self::select('*');

            return Datatables::of($sql)
	                ->editColumn('name', function ($data) {

	                    return $data->name;

	                })
	                ->editColumn('email', function ($data) {

	                    return !empty($data->email) ? $data->email : '-' ;

	                })
	                ->editColumn('website', function ($data) {

	                	return !empty($data->website) ? $data->website : '-' ;

	                })
	                ->editColumn('type', function ($data) {

                            if (!empty($data->type)) {
                                
        	                    return  ($data->type == 1) ? 'Owner Company':'Manning Company';

                            }else{

                                return "";
                            }

	                })
	                ->editColumn('official_no', function ($data) {

	                    return !empty($data->official_no) ? $data->official_no : '-' ;

	                })
	                ->addColumn('address', function ($data) {

	                	$string = '<a class="btn btn-xs btn-primary" href="' . route('admin.editCompany',Crypt::encrypt($data->id)) . '"><i class="fa fa-pencil-square-o" aria-hidden="true" title="Edit company"></i></a> <a href="javascript:void(0)" data-route="'.route('admin.deleteCompanyDetail',Crypt::encrypt($data->id)).'" class="btn btn-xs btn-danger delete_record"><i class="fa fa-trash" aria-hidden="true"></i></a>';
                        return $string;

	                })
                    ->make(true);
            
        }
    }

    public function saveUpdateCompany($r,$id=NULL){
    
    	$errors="";
    	$input = $r->all();
        $checkcontacts = [];
      
    	if ($id !== NULL) {

            $id=Crypt::decrypt($id);
  			$obj = self::where('id',$id)->firstOrFail();	  		
            $msg = "Company record successfully updated.";

        }else{
            
        	$obj = new self;
            $msg = "Company record successfully saved.";
        }
        
        $obj->name = $input['name'];
        $obj->slug = CREATE_SLUG($input['name']);
        $obj->official_no = $input['official_no'];
        $obj->address = $input['address'];
        $obj->operating_vessel_type = $input['operating_vessel_type'];
        $obj->type = $input['type'];
        $obj->email = $input['email'];
        $obj->website = isset($input['website']) ? $input['website'] : NULL;
        $obj->expiry_date = isset($input['expiry_date']) ? $input['expiry_date'] : NULL;
        $obj->save();

        $checkDefault = CompanyEmailAddress::where('company_id',$obj->id)
                                            ->where('is_default',1)
                                            ->first();        

        if ($checkDefault == null) {

            $companyAddress = new CompanyEmailAddress;
            $companyAddress->company_id = $obj->id;
            $companyAddress->email = $input['email'];
            $companyAddress->is_default = 1;
            $companyAddress->save();

        }else{

            $checkDefault->email = $input['email'];
            $checkDefault->save();
        }

        if (isset($input['update_email_group']) && !empty($input['update_email_group'])) {
            
            foreach($input['update_email_group'] as $key => $value) {
                    
                $checkAddress = CompanyEmailAddress::where('id',$key)->first();        

                if ($checkAddress !=null) {
                    
                    $checkAddress->company_id = $obj->id;
                    $checkAddress->email = $value;
                    $checkAddress->is_default = 2;
                    $checkAddress->save();
                }
            }    
        }


        if (isset($input['update_contact_group']) && !empty($input['update_contact_group'])) {
            
            foreach($input['update_contact_group'] as $key => $value) {
                
                if (!empty($value)) {  
                    $getRecordById = CompanyContacts::where('id',$key)->first();    
                    $checkcontacts = CompanyContacts::where('id','!=',$key)->where('contact_number',$value)->first();  

                    if (!empty($getRecordById)  && empty($checkcontacts)) {                    
                        CompanyContacts::where('id',$key)->update(['contact_number'=>$value]); 
                    }else{
                        return response()->json(['status' => 'error','msg' => $value.' Contact Number already taken by other!']);
                    }     
                }            
            } 
               
        }

        if (isset($input['email_group']) && !empty($input['email_group'])) {
                
            foreach($input['email_group'] as $key => $value) {
                
                if (!empty($value)) {
                    
                    $address = new CompanyEmailAddress;        
                    $address->company_id = $obj->id;
                    $address->email = $value;
                    $address->save();
                }
            }    

        }

       

        if (isset($input['contact_group']) && !empty($input['contact_group'])) {
                
            foreach($input['contact_group'] as $key => $value) {

                if(!empty($value)){
                    $checkexistContact = CompanyContacts::where('contact_number',$value)->first(); 
                    if (empty($checkexistContact))  { 
                        $contacts = new CompanyContacts;        
                        $contacts->company_id = $obj->id;
                        $contacts->contact_number = $value;
                        $id = $contacts->save(); 
                    }else{
                        return response()->json(['status' => 'error','msg' => $value.' Contact Number already taken by other!']);
                    } 
                } 
            }    

        }

        // flashMessage('success',$msg);

        return response()->json(['status' => 'success','msg' => $msg]);
    }

    public function editcompanyDetail($id){

        $company = self::with(['companyEmailAddress','companyContacts'])                        
                        ->where('id',$id)
                        ->firstOrFail();

        return $company;
    }

    public function deleteAll($r){

        $input=$r->all();
        $msg = "Please select at least one record.";
        $status = 3;
        foreach ($input['checkbox'] as $key => $c) {
            
            $dId = Crypt::decrypt($c);
            $checkCompanyExit = CourseJob::where('company_id',$dId)->first();

            if ($checkCompanyExit !=null) {
                
                $msg = "This Company record already used in other module you can not delete now.";
                $status = 2;
            }else{

                $obj = $this->findOrFail($dId);
                $obj->delete();
                $msg = "Company record successfully deleted.";
                $status = 1;
            }
        }

        return response()->json(['success' => $status, 'msg' => $msg]);
    }

    public static function getCompanyDropdown(){

       $data = self::pluck('name','id')->toArray();
       return $data;     
    }
}
