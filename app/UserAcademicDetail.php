<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserAcademicDetail extends Model
{
    protected $table = 'user_academic_details';

    protected $fillable = ['user_id', 'course_id', 'institute', 'year_of_passing'];
}
