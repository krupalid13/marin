<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyJob extends Model
{
    protected $table = 'company_jobs';

    protected $fillable = ['company_id', 'job_title', 'job_category', 'rank', 'ship_type', 'sal_from', 'sal_to', 'valid_from', 'valid_to', 'date_of_joining', 'nationality', 'min_rank_exp', 'job_description','status', 'engine_type', 'grt', 'bhp','ship_detail_id','ship_flag','wages_currency','wages_offered'];

    public function company_details(){
        return $this->hasOne('App\CompanyDetail','company_id', 'company_id');
    }

    public function user(){
        return $this->hasOne('App\CompanyDetail','id', 'company_id');
    }

    public function company_location(){
    	return $this->hasMany('App\CompanyLocationDetails','company_id', 'company_id');
    }

    public function company_registration_details(){
    	return $this->hasOne('App\CompanyRegistration','id', 'company_id');
    }

    public function user_applied_jobs(){
        return $this->hasMany('App\JobApplication','job_id', 'id');
    }

    public function ship_type_id(){
        return $this->hasOne('App\CompanyShipDetails','id', 'ship_detail_id');
    }

    public function job_matching_seafarers(){
        return $this->hasMany('App\UserProfessionalDetail','applied_rank', 'rank');
    }

    public function job_application(){
        return $this->hasOne('App\JobApplication','job_id', 'id');
    }

    public function job_nationality(){
        return $this->hasMany('App\CompanyJobNationality','job_id', 'id');
    }

}
