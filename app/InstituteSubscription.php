<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InstituteSubscription extends Model
{
    protected $table = 'institute_subscriptions';

    protected $fillable =['institute_reg_id','order_id','subscription_details','status','valid_from','valid_to','created_at','updated_at'];

    public function subscriptionFeatures()
    {
        return $this->hasMany('App\SubscriptionFeature');
    }

    public function order()
    {
        return $this->hasOne('App\Order','id','order_id');
    }

    public function institute_registration()
    {
        return $this->hasOne('App\InstituteRegistration','id','institute_reg_id');
    }
}
