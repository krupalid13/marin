<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Services\NotificationService;
use App\Notification;
use Auth;

class NotificationController extends Controller
{
    private $notificationService;

    function __construct() {
        $this->notificationService = new NotificationService();
    }

    public function setUserNotificationsRead(Request $request){

        if(Auth::check()){
            $notification_id = $request->notification_id;

            $data['status'] = '0';
            $result = Notification::where('id',$notification_id)->update($data);
            
            if($result){
                return response()->json(['message' => 'success'], 200);
            }else{
                return response()->json(['message' => 'error'], 400);
            }

        }
    }

    public function getUserNotifications(){
        if(Auth::check()){
            $user_id = Auth::User()->id;
            $notification = '';
            $count = '10';
            $notification_count = '0';

            $result = $this->notificationService->getUserNotifications($user_id,$count);

            if(isset($result) && !empty($result)){

                foreach ($result as $key => $value) {
                    if($value['status'] == '1'){
                        $notification_count++;
                    }
                    $notification = $notification.$this->notificationService->getMessage($value['type'],$value['payload'],$value['id']);
                }
            }
            //dd($user_id,$result,$notification,$notification_count);
            if(isset($notification) && !empty($notification) && $notification != ''){
                return response()->json(['template' => $notification,'notification_count' => $notification_count], 200);
            }else{
                return response()->json(['template' => '<div class="no-notification text-center p-15">No notifications to show</div>'], 200);
            }

        }else{
            return response()->json(['template' => '<div class="no-notification text-center p-15">No notifications to show</div>'], 200);
        }
    }

    public function uploadUserNotifications(Request $request){

        if(Auth::check()){
            $notification_id = $request->notification_id;

            $notification_details = $this->notificationService->getUserNotificationById($notification_id);

            $notification = $this->notificationService->getMessage($notification_details['type'],$notification_details['payload'],$notification_id);
            
            return response()->json(['template' => $notification], 200);
        }else{
             return response()->json(['template' => ''], 400);
        }

    }

    public function viewNotifications(Request $request){

        if(Auth::check()){

            $user_id = Auth::User()->id;

            $notification_details = $this->notificationService->getUserNotificationByUserId($user_id,'10');

            $notifications = $notification_details->toArray();

            if(!empty($notifications['data'])){
                foreach ($notifications['data'] as $index => $value) {
                    $notifications['data'][$index]['message'] = $this->notificationService->getMessage($value['type'],$value['payload'],$value['id']);
                }
            }
            //dd($notifications);

            return view('admin.institute.notification_list',['data'=>$notification_details,'notifications' => $notifications]);

        }
    }
}
