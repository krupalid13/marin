@extends('site.index')
@section('page-level-style')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.3.2/jquery.rateyo.min.css">
    <style>
        .modal-content {
            background: lightblue;
        }

        @media (min-width: 768px){
            .modal-dialog {
                top: 80px;
            }
        }

        .modal-header, .modal-footer {
            border: 0 !important;
        }

        .modalButton {
            background: #3399FF;
            color: white;
        }

        .my-review {
            padding: 80px 0 80px 0;
        }

        .timeline {
            position: relative;
        }

        /*Line*/
        .timeline > li::before {
            content: '';
            position: absolute;
            width: 1px;
            background-color: #3399FF;
            top: 0;
            bottom: 0;
            left: -19px;
        }

        /*Circle*/
        .timeline > li::after {
            text-align: center;
            padding-top: 2px;
            content: counter(item);
            position: absolute;
            width: 27px;
            height: 27px;
            border: 1px dotted white;
            background-color: #3399FF;
            border-radius: 50%;
            top: 0;
            left: -32px;
            color: #3399FF;
        }

        /*Content*/
        .timeline > li {
            counter-increment: item;
            padding: 1px 10px;
            margin-left: 0px;
            min-height: 70px;
            position: relative;
            list-style: none;
            font-size: small;
        }

        .timeline > li:nth-last-child(1)::before {
            width: 0px;
            background: #043458;
        }

        .clicked::after {
            background-color: #5233ff !important;
            color: #5233ff !important;
        }

        .borderl {
            border-bottom: 0.7px solid black !important;
            margin-top: 13px;
            margin-bottom: 10px;
        }

        .stick {
            display: flex;
            color: black;
        }

        #lname {
            width: 96%;
            margin: 0 auto;
        }

        #lname textarea.form-control.custom-control {
            width: 96%;
        }

        .box-header > p {
            margin-bottom: 0px !important;
            color: white;
        }

        .box-header {
            background: #3399FF;
            padding: 10px;
            width: 100%;
            margin-bottom: 15px;
        }

        /* Float four columns side by side */
        .column {
            float: left;
            width: 32.50%;
            margin: 3px;
            text-align: center;
        }

        /*!* Remove extra left and right margins, due to padding *!*/
        /*.row {*/
        /*    margin: 0 -5px;*/
        /*}*/

        /*!* Clear floats after the columns *!*/
        /*.row:after {*/
        /*    content: "";*/
        /*    display: table;*/
        /*    clear: both;*/
        /*}*/

        /* Responsive columns */
        @media screen and (max-width: 600px) {
            .column {
                width: 100%;
                display: block;
                margin-bottom: 20px;
            }
        }

        /* Style the counter cards */
        .card {
            background: white;
            padding: 30px 20px 30px 20px;
            text-align: center;
            border-color: #3399FF;
            border-radius: 0px;
            margin: 0;
            border: 1px solid #ccc;
        }

        .box {
            width: 100%;
            border: 0.6px solid black;
            /*padding: 0px 20px 20px 20px;*/
        }

        .boxRaw {
            /*padding-top: 21px; */
            width: 100%;
            margin: 0 auto;
            /* border: 0.6px solid black; */
            padding: 25px 20px 20px 20px;
        }

        ::-webkit-input-placeholder { /* Chrome/Opera/Safari */
            color: darkgrey;
            font-size: small;
            padding-left: 10px;
            top: 1px;
        }

        ::-moz-placeholder { /* Firefox 19+ */
            color: darkgrey;
            font-size: small;
            padding-left: 10px;
            top: 1px;
        }

        :-ms-input-placeholder { /* IE 10+ */
            color: darkgrey;
            font-size: small;
            padding-left: 10px;
            top: 1px;
        }

        :-moz-placeholder { /* Firefox 18- */
            color: darkgrey;
            font-size: small;
            padding-left: 10px;
            top: 1px;
        }

        .box label {
            text-underline-position: under;
            padding-left: 19px;
            text-decoration: underline;
        }

        .head-border {
            border-bottom: 1px solid black;
            padding-bottom: 6px;
            margin-bottom: 14px;
            width: 121px;
            margin-left: 19px;
        }

        .card-header {
            background: none;
            border: none;
            padding: 0 !important;
            font-weight: bold;
            color: black;
        }

        #lname .input-group-addon {
            line-height: 7.5;
            background: #3399FF;
            border-radius: 0 !important;
        }

        #lname textarea.form-control.custom-control {
            border-radius: 0px;
        }

        .rightButton, .rightButton:hover {
            background: #22b0e2;
            border: 0 !important;
            outline: 1.1px solid #22b0e2;
            color: white;
            border-radius: 0 !important;
            padding: 24px 7px 24px 5px;
        }

        .checked {
            color: orange;
        }

        p.sea {
            margin-bottom: 0px;
            margin-left: -15px;
        }

        html {
            box-sizing: border-box;
        }

        *,
        *::before,
        *::after {
            box-sizing: inherit;
        }

        body, td, th, p {
            color: #333;
            font: 16px/1.6 Arial, Helvetica, sans-serif;
        }

        body {
            background-color: #fdfdfd;
            margin: 0;
            position: relative;
        }

        h2 {
            display: inline-block;
        }

        #review-add-btn {
            float: right;
            margin-left: 23px;
            margin-top: 21px;
            padding: 0;
            font-size: 1.6em;
            cursor: pointer;
        }

        /* ====================== Review Form ====================== */
        #modal {
            /* position: absolute;
            left: 10vh;
            top: 10vh; */
            /* fix exactly center: https://css-tricks.com/considerations-styling-modal/ */
            /* begin css tricks */
            position: fixed;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
            /* end css tricks */
            /* z-index: -10; */
            z-index: 3;
            display: flex;
            flex-direction: column;
            width: 50vw;
            height: 55vh;
            border: 1px solid #043458;
            border-radius: 10px;
            opacity: 0;
            transition: all .3s;
            overflow: hidden;
            background-color: #043458;
            /* visibility: hidden; */
            display: none;
        }

        #modal.show {
            /* visibility: visible;   */
            opacity: 1;
            /* z-index: 10; */
            display: flex;
        }

        .modal-overlay {
            width: 100%;
            height: 100%;
            z-index: 2; /* places the modalOverlay between the main page and the modal dialog */
            background-color: #000;
            opacity: 0;
            transition: all .3s;
            position: fixed;
            top: 0;
            left: 0;
            display: none;
            margin: 0;
            padding: 0;
        }

        .modal-overlay.show {
            display: block;
            opacity: 0.5;
        }

        #modal .close-btn {
            float: right;
            align-self: flex-end;
            font-size: 1em;
            margin: 2px 0px 0;
            padding: 2px 20px 2px 13px;
            cursor: pointer;
            color: white;
            border: 0 !important;
            box-shadow: none;
            background: #043458;
        }

        .review-form-container > form {
            max-width: 900px;
            padding: 0 20px 20px 20px;
        }

        /*
        input,
        label {
          display: block;
          width: 100%;
        }

        label {
          font-weight: bold;
          margin-bottom: 5px;
        } */

        review-form-container > input,
            /* input:not(input[type='radio']), */
            /* input:not(type='radio'), */
        review-form-container >
        review-form-container > select, .rate, review-form-container > textarea, review-form-container > button {
            background: #f9f9f9;
            border: 1px solid #e5e5e5;
            border-radius: 8px;
            box-shadow: inset 0 1px 1px #e1e1e1;
            font-size: 16px;
            padding: 8px;
        }

        review-form-container > input[type="radio"] {
            box-shadow: none;
        }

        /*button {*/
        /*    min-width: 48px;*/
        /*    min-height: 48px;*/
        /*}*/

        /*button:hover {*/
        /*    border: 1px solid #ccc;*/
        /*    background-color: #fff;*/
        /*}*/

        button#review-add-btn,
        button.close-btn,
        button#submit-review-btn {
            min-height: 40px;
        }

        button#submit-review-btn {
            font-weight: bold;
            cursor: pointer;
            padding: 0 16px;
        }

        .fieldset {
            margin-top: 8px;
        }

        .right {
            align-self: flex-end;
        }

        #review-form-container {
            width: 100%;
            /* background-color: #eee; */
            padding: 0 17px 17px 17px;
            color: #333;
            overflow-y: auto;
            overflow-y: hidden;
        }

        #review-form-container h2 {
            margin: 0 0 0 6px;
        }

        #review-form {
            display: flex;
            flex-direction: column;
            background: #043458;
            border-radius: 4px;
        }

        #review-form label, #review-form input {
            display: block;
            /* width: 100%; */
        }

        #review-form label {
            font-weight: bold;
            margin-bottom: 5px;
        }

        #review-form .rate label, #review-form .rate input,
        #review-form .rate1 label, #review-form .rate1 input {
            display: inline-block;
        }

        #review-form p {
            color: white;
            font-size: small;
            font-weight: bold;
        }

        /* Modified from: https://codepen.io/tammykimkim/pen/yegZRw */
        .rate {
            background: no-repeat;
            border: none;
            box-shadow: none;
            /* float: left; */
            /* display: inline-block; */
            height: 36px;
            display: inline-flex;
            align-items: center;
            justify-content: center;
            width: 100%;
        }

        #review-form .rate > label {
            margin-bottom: 0;
            margin-top: -5px;
            height: 30px;
        }

        .rate:not(:checked) > input {
            /* position: absolute; */
            top: -9999px;
            margin-left: -24px;
            width: 20px;
            padding-right: 14px;
            z-index: -10;
        }

        .rate:not(:checked) > label {
            float: right;
            width: 1em;
            overflow: hidden;
            white-space: nowrap;
            cursor: pointer;
            font-size: 30px;
            color: #ccc;
        }

        /* #star1:focus{

        } */
        .rate2 {
            float: none;
        }

        #reviewComments {
            width: 100%;
            margin: 0 auto;
        }

        .rate:not(:checked) > label::before {
            content: '★ ';
            position: relative;
            top: -10px;
            /*left: -17px;*/
        }

        .rate > input:checked ~ label {
            color: #ffc700;
            /* outline: -webkit-focus-ring-color auto 5px; */
        }

        .rate > input:checked:focus + label, .rate > input:focus + label {
            outline: -webkit-focus-ring-color auto 5px;
        }

        .rate:not(:checked) > label:hover,
        .rate:not(:checked) > label:hover ~ label {
            color: #deb217;
            /* outline: -webkit-focus-ring-color auto 5px; */
        }

        .rate > input:checked + label:hover,
        .rate > input:checked + label:hover ~ label,
        .rate > input:checked ~ label:hover,
        .rate > input:checked ~ label:hover ~ label,
        .rate > label:hover ~ input:checked ~ label {
            color: #c59b08;
        }

        #submit-review {
            align-self: flex-end;
        }

        /*.speech-bubble, .speech-bubble:hover {*/
        /*    background: #00aabb;*/
        /*    !*border-radius: .1em;*!*/
        /*    !*padding: 1px !important;*!*/
        /*    !*width: 100px;*!*/
        /*    border: 0 !important;*/
        /*    !*margin: 0 auto;*!*/
        /*    !*min-height: auto;*!*/
        /*}*/

        .speech-bubble:focus {
            outline: none !important;
        }

        ::-webkit-scrollbar {
            width: 5px;
        }

        /* Track */
        ::-webkit-scrollbar-track {
            background: #3399FF;
        }

        /* Handle */
        ::-webkit-scrollbar-thumb {
            background: #5ad3ff;
        }

        /* Handle on hover */
        ::-webkit-scrollbar-thumb:hover {
            background: #5ad3ff;
        }
        .speech-bubble {
            position: relative;
            background: #00aabb;
            border-radius: .1em;
            padding: 1px !important;
            width: 100px;
            border: 0 !important;
            margin: 0 auto;
            min-height: auto;
            color: white;
            margin-bottom: 10px;
        }
        .timeline{
            height: 500px;
            overflow-y: scroll;
        }

        .speech-bubble:after {
            content: '';
            position: absolute;
            bottom: 0;
            left: 50%;
            width: 0;
            height: 0;
            border: 14px solid transparent;
            border-top-color: #00aabb;
            border-bottom: 0;
            border-right: 0;
            margin-left: -7px;
            margin-bottom: -14px;
        }

        .rate > .fa {
            color: white;
            font-size: 37px;
            padding: 6px;
            margin-bottom: 20px;
        }

        i.fa.fa-pencil {
            float: right;
            margin-top: -17px;
            margin-right: -17px;
        }

        .float-right {
            float: right;
        }

        #content-wrapper {
            margin-top: -55px;
            min-height: 100%;
            background-color: #F1F3F7;
        }

        .rightButton {
            background: #3399ff;
            border: 0 !important;
            color: white;
        }
        .h5.center-border.center {
            border-bottom: 1px solid black;
            padding-bottom: 6px;
            width: 135px;
            text-align: center;
            align-items: center;
            /* width: 100%; */
            margin-left: 42%;
        }

        .speech-bubble-text{
            display: none;
        }
        .d-none{
            display: none !important;
        }
        .rate-error{
            color: red;
            font-size: 13px;
        }
        .comment-text{
            font-size: 12px;
            color: #2d64c8;
            word-wrap: break-word;
        }
        .bold-font{
            font-weight: bolder;
        }
        .experience{
            margin-bottom: 10px !important;
        }
        #shipRate{
            margin-bottom: 10px;
        }
        .color-gray{
            color: gray;
            margin-bottom: 5px;
        }
        .s-msg{
            margin: 0px !important;
        }
    </style>
@stop
@section('content')
    <div class="my-review">
        <div class="container">
            <div class="content-section-wrapper">
                <div class="alert alert-success alert-dismissible show s-msg d-none" role="alert">
                    <strong>Successfully!</strong><span class="success-msg"></span>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="row">
                    <div class="col-md-12 borderl">
                        <h2>Mark your Experience</h2>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-3">
                        <ol class="timeline">
                            @foreach($shipName as $row)
                                    @php
                                        $class = '';
                                        $fontClass = '';
                                        if($row->id == $firstShipName->id){
                                            $class = 'clicked';
                                        }
                                        if(!$row->getRateData){
                                            $fontClass = 'bold-font';
                                        }
                                    @endphp
                                    <li class="{{$class.' '.$fontClass.' '.'ship-name-'.$row->id}}" data-id="{{encrypt($row->id)}}" data-name="{{$row->ship_name}}">{{$row->ship_name}}
                                        <span class="stick">
                                            @php
                                                $toDate = isset($row->to) && (!empty($row->to)) ? \Carbon\Carbon::parse($row->to)->format('d-m-Y') : 'On Board';
                                            @endphp
                                            {{\Carbon\Carbon::parse($row->from)->format('d-m-Y')}} To {{ $toDate }}
                                        </span>
                                    </li>
                            @endforeach
                        </ol>
                    </div>
                    <div class="ship-data">
                        {{-- all ship data here rendor --}}
                    </div>
                </div>

                {{-- modal data --}}
                <div class="modal fade" id="rateting-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h5 class="modal-title" id="myModalLabel"> Rate and review - The <span class="ship"></span></h5>
                            </div>
                            <div class="modal-body">
                                <form class="rateForm">
                                    <div id="shipRate"></div>
                                    <span class="rate-error rate-msg-error d-none">Please give your rating.</span>
                                    {{Form::textarea('comment','',['id'=>'reviewComments','class'=>'rate-comment','required','cols'=>20,'rows'=>5])}}
                                    <span class="rate-error rate-comment-error d-none">Please enter your comment.</span>
                                    {{Form::hidden('company_type','',['class'=>'company-type'])}}
                                    {{Form::hidden('rating','',['class'=>'ship-rate'])}}
                                </form>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn modalButton save-shipdata">Save</button>
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@stop
@section('js_script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.3.2/jquery.rateyo.min.js"></script>
    <script type="text/javascript">
        var shipId = "{{$fShipId}}";
        var token = "{{csrf_token()}}";
        $(document).ready(function () {
            $("#shipRate").rateYo({
                onSet: function (rating, rateYoInstance) {
                    $('.ship-rate').val(rating);
                },
                rating: 0.0,
            });
            $('.fa-pencil').click(function (e) {
                if($(this).closest('.card').find('.speech-bubble').is(":hidden")){
                    $(this).closest('.card').find('.speech-bubble').show();
                } else{
                    $(this).closest('.card').find('.speech-bubble').hide();
                }
                if($(this).closest('.card').find('.speech-bubble-text').is(":hidden")){
                    $(this).closest('.card').find('.speech-bubble-text').show();
                } else{
                    $(this).closest('.card').find('.speech-bubble-text').hide();
                }
            });

            $('ol.timeline li').click(function (e) {
                shipId = $(this).data('id');
                getShipData(shipId);
                $('li.clicked').removeClass('clicked');
                if ($(this).hasClass('clicked')) {
                    $(this).removeClass('clicked')
                } else {
                    // $(this).parents('li').find(">ul").addClass('clicked');
                    $(this).addClass('clicked');
                    // $(this).parent('li').parents('li').addClass('clicked');
                }
            });

            $(document).on('click','.save-experience',function(){
                $('.exterience-error-msg').addClass('d-none');
                var experience = $('.experience').val();
                if(experience == ''){
                    $('.exterience-error-msg').removeClass('d-none');
                    return true;
                }
                storeRateData(experience);
            });

            $(document).on('click','.comment-data',function(){
                var type = $(this).data('type');
                var name = $(this).data('modalname');
                $('.company-type').val(type);
                $('.ship').text('');
                $('.ship').text(name+ ' "'+$(this).data('name')+'"');
            });

            $(document).on('click','.save-shipdata',function(){
                $('.rate-comment-error').addClass('d-none');
                $('.rate-msg-error').addClass('d-none');
                var comment = $('.rate-comment').val();
                var rateData = $('.rateForm').serialize();
                var rate = $('.ship-rate').val();
                if(comment == ''){
                    $('.rate-comment-error').removeClass('d-none');
                    return true;
                }
                if(rate <= 0 || rate == ''){
                    $('.rate-msg-error').removeClass('d-none');
                    return true;
                }
                storeRateData(null,rateData);
            });
            getShipData(shipId);
        });

        function getShipData(shipId){
            $.ajax({
                url: "{{URL::to('get-ship-data')}}"+'/'+shipId,
                dataType: 'json',
            }).done(function(data){
                $('.ship-data').html(data.shipData);
            }).fail(function(error){

            });
        }

        function storeRateData(experience=null,rateData=null){
            $('.s-msg').addClass('d-none');
            $('.success-msg').text('');
            var type = 1;
            var data = {experience:experience,_token:token,shipId:shipId}
            if(experience == null){
                data = rateData+'&_token='+token+'&shipId='+shipId;
                type = 2;
            }
            $.ajax({
                url: "{{URL::to('store-ratedata')}}",
                dataType: 'json',
                type: 'POST',
                data: data,
            }).done(function(data){
                $('.s-msg').removeClass('d-none');
                var msg = ' Your experience save';
                if(type == 2){
                    msg = ' Your rate and comment save';
                }
                $('.success-msg').text(msg);
                $('.ship-name-'+data.shipId).removeClass('bold-font');
                $('#rateting-modal').modal('hide');
                $('.rateForm').trigger('reset');
                getShipData(shipId);
                $("#shipRate").rateYo("option", "rating", "0");
            }).fail(function(error){

            });

        }
    </script>
@stop
