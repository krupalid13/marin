<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\UserService;
use App\Services\CandidateService;
use App\Services\DocumentService;
use App\Http\Requests;
use Route;
use View;
use Auth;
use Maatwebsite\Excel\Facades\Excel as Excel;

class SeafarerController extends Controller
{
	private $userService;
    private $documentService;

    public function __construct()
    {
        $this->userService = new UserService();
        $this->candidateService = new candidateService();
        $this->documentService = new DocumentService();
    } 

  /*  public function storeSeafarerDetails(Requests\SeafarerRegistrationRequest $seafarerRegistrationRequest){*/

    public function storeSeafarerDetails(Request $seafarerRegistrationRequest, $type, $seafarer_id=NULL){

    	$seafarer_data = $seafarerRegistrationRequest->toArray();

        if($type == 'basic_details'){
            $seafarer_data['added_by'] = 'admin';
            $seafarer_data['registered_as'] = 'seafarer';

            if(isset($seafarer_data['user_id']) && !empty($seafarer_data['user_id'])){
                $user['id'] = $seafarer_data['user_id'];
                $user_id = $seafarer_data['user_id'];
                $data = $this->userService->store($seafarer_data,$user);
            }else{
    	       $data = $this->userService->store($seafarer_data);

               $added_user = $data['user']->toArray();
                $user_id = $added_user['id'];
            } 
        }

        if($type == 'imp_documents'){
    	   $data =  $this->userService->storeDocuments($seafarer_data,$seafarer_id);
        }

        if($type == 'service_details'){
    	   $data = $this->userService->storeServiceDetails($seafarer_data,$seafarer_id);
        }

        if($type == 'course_details'){
    	   $data = $this->userService->storeCourseDetails($seafarer_data,$seafarer_id);
        }

    	if($data['status'] == 'success'){
            return response()->json(['status' => 'success' , 'message' => 'Your details has been store successfully.','redirect_url' => route('admin.view.seafarer.id',isset($user_id) ? $user_id : isset($seafarer_id) ? $seafarer_id : ''),'user_id' => isset($user_id) ? $user_id : ''],200);
        }else{
            return response()->json(['status' => 'failed' , 'message' => 'Error while adding data.'],400);
        }

    }

    public function storeSpecificServiceDetails(Request $request){

        if(Auth::check()){
            $data = $request->toArray();
            $user = Auth::user()->toArray();
            $user_id = $request->user_id;
            
            /*  echo "<pre>";
                print_r($data);
                echo "</pre>";
                die();
            */

            $result = $this->userService->storeSpecificServiceDetails($data,$user_id);

            if($result['status'] == 'sea_service_from_present'){
                return response()->json(['status' => 'failed' , 'message' => 'Sea service already added between given sign on date.'],400);
            } 

            if($result['status'] == 'sea_service_to_present'){
                return response()->json(['status' => 'failed' , 'message' => 'Sea service already added between given sign off date.'],400);
            }

            $services = $this->userService->getSeaServiceDetailsByUserId($user_id);

            if(isset($data['exiting_service_id']) && !empty($data['exiting_service_id'])){
                $sea_service = View::make("site.partials.seafarer_service_details")->with(['services' => $services]);
                $template = $sea_service->render();
                
                return response()->json(['status' => 'success' , 'message' => 'Your service details has been updated successfully.', 'template' => $template, 'update' => '1'],200);
            }else{
                $sea_service = View::make("site.partials.seafarer_service_details")->with(['services' => $services]);
            }
            
            $template = $sea_service->render();

            if($result['status'] == 'success'){
                return response()->json(['status' => 'success' , 'message' => 'Your service details has been store successfully.', 'template' => $template],200);
            }else{
                return response()->json(['status' => 'failed' , 'message' => 'Error while adding data.'],400);
            }
        }
        else{
            return response()->json(['status' => 'failed' , 'message' => 'Error while adding data.'],400);
        }
    }

    public function deleteServiceDetails(Request $request){
        if(Auth::check()){
            $data = $request->toArray();
            $user = Auth::user()->toArray();
            $user_id = $request->user_id;
            
            /*echo "<pre>";
                print_r($data);
            echo "</pre>";
            die();*/
            $result = $this->userService->deleteServiceDetails($data['service_id'],$user_id);

            if($result['status'] == 'success'){
                return response()->json( $result,200);
            }else{
                return response()->json( $result,400);
            }
        }
        else{
            return response()->json(['status' => 'failed' , 'message' => 'Error while adding data.'],400);
        }
    }

    public function getSeaServiceDetails(Request $request){
        if(Auth::check()){
            $data = $request->toArray();
            $user_id = $request->user_id;
            
            $result = $this->userService->getSeaServiceDetails($data['service_id'],$user_id);

            if($result['status'] == 'success'){
                return response()->json( $result,200);
            }else{
                return response()->json( $result,400);
            }
        }
        else{
            return response()->json(['status' => 'failed' , 'message' => 'Error while adding data.'],400);
        }
    } 

    public function editSeafarer($seafarer_id){

    	if(isset($seafarer_id) AND !empty($seafarer_id)){
    		$options['with'] = ['personal_detail', 'professional_detail', 'passport_detail', 'seaman_book_detail', 'coc_detail', 'gmdss_detail', 'wkfr_detail', 'personal_detail.state', 'personal_detail.city', 'personal_detail.pincode.pincodes_states.state', 'personal_detail.pincode.pincodes_cities.city','sea_service_detail','course_detail','cop_detail','coe_detail'];

        	$result = $this->userService->getDataByUserId($seafarer_id, $options)->toArray();
            
        	if (isset($result[0]['professional_detail']['current_rank_exp']) && !empty($result[0]['professional_detail']['current_rank_exp'])){
                $rank_exp = explode(".", $result[0]['professional_detail']['current_rank_exp']);
                $result[0]['professional_detail']['years'] = $rank_exp[0];
                $result[0]['professional_detail']['months'] = $rank_exp[1];
            }

            $courses = $this->userService->getAllCourses();

            $count = 0;
            if(isset($result[0]['course_detail'])){
                foreach ($result[0]['course_detail'] as $key => $value) {
                    if($value['course_type'] == 'Value Added'){
                        $result[0]['value_added_course_detail'][$count] = $value;
                        $count++;
                        unset($result[0]['course_detail'][$key]);
                    }
                }
            }

            $name = Route::currentRouteName();
            $result['current_route'] = $name;

            if(count($result) > 0){
                return view('admin.seafarer.add_seafarer',['user_data' => $result,'all_courses' => $courses]);
            }else{
                dd('404 Page');
            }
        }
    }

    public function updateSeafarer(Requests\SeafarerProfileRegistrationRequest $seafarerProfileRegistrationRequest,$seafarer_id,$type){

    	$userData = $seafarerProfileRegistrationRequest->toArray();
        $userData['otp'] = $this->userService->generateOTP();
        $update_user_data = $this->userService->getDataByUserId($seafarer_id)->toArray();
        
        if(isset($update_user_data[0]) && !empty($update_user_data[0]))
            $update_user_data = $update_user_data[0];

        if($type == 'basic_details'){
            $userData['added_by'] = 'admin';
            $userData['registered_as'] = 'seafarer';
            $result = $this->userService->store($userData,$update_user_data);
        }

        if($type == 'imp_documents'){
            $result = $this->userService->storeDocuments($userData,$seafarer_id);
        }

        if($type == 'service_details'){
            $result = $this->userService->storeServiceDetails($userData,$seafarer_id);
        }

        if($type == 'course_details'){
            $result = $this->userService->storeCourseDetails($userData,$seafarer_id);
        }

        if($result['status'] == 'success'){
            return response()->json(['status' => 'success' , 'message' => 'Your details has been store successfully.','redirect_url' => route('admin.view.seafarer.id',$seafarer_id)],200);
        }else{
            return response()->json(['status' => 'failed' , 'message' => 'Error while adding data.'],400);
        }
    }

    public function viewSeafarer(Request $request){

        $searched_data[0] = $request->toArray();

        $filter = $request->toArray();

    	$options['with'] = ['personal_detail','professional_detail','passport_detail','seaman_book_detail','coc_detail','gmdss_detail','wkfr_detail','personal_detail.state','personal_detail.city','personal_detail.pincode.pincodes_states.state','personal_detail.pincode.pincodes_cities.city','sea_service_detail','course_detail','cop_detail','coe_detail'];

        
        $paginate = env('PAGINATE_LIMIT',10);
        if (isset($filter) && !empty($filter)) {
            $seafarer_data = $this->candidateService->getAllCandidateDataByFilters($filter,$paginate);
        }else {
            $seafarer_data = $this->candidateService->getAllCandidateDataByFilters('',$paginate);
        }

        $result = $seafarer_data->toArray();

        $job_search = View::make("admin.partials.seafarer_list")->with(['seafarer_data' => $result, 'pagination' => $seafarer_data]);
        $template = $job_search->render();

        if($request->isXmlHttpRequest()){
            return response()->json(['template' => $template], 200);
        }else{
            return view('admin.seafarer.list_seafarer',['seafarer_data' => $result, 'pagination' => $seafarer_data, 'job_data' => $searched_data, 'filter' => $filter]);
        }
    }

    public function storeProfilePic(Request $request){

    	$array['image-x'] = $request['image-x'];
        $array['image-y'] = $request['image-y'];
        $array['image-x2'] = $request['image-x2'];
        $array['image-y2'] = $request['image-y2'];
        $array['image-w'] = $request['image-w'];
        $array['image-h'] = $request['image-h'];
        $array['crop-w'] = $request['crop-w'];
        $array['crop-h'] = $request['crop-h'];
        $array['profile_pic'] = $request['profile_pic'];
        $array['role'] = 'admin';

        $result = $this->userService->uploadProfile($array);

        if( $result['status'] == 'success' ) {
            return response()->json($result, 200);
        } else {
            return response()->json($result, 400);
        }
    }

    public function changeStatus(Request $request){

    	$data = $request->toArray();

    	$result = $this->userService->changeStatus($data);

        if($result['status'] == 'success'){

        	$message = 'User Deactivated';
        	if($data['status'] == 'active'){
        		$message = 'User Activated';
        	}

            return response()->json(['status' => 'success' , 'message' => $message],200);
        }else{
            return response()->json(['status' => 'failed' , 'message' => 'Error.'],400);
        }
    }

    public function addSeafarer(){
        $courses = $this->userService->getAllCourses();
        return view('admin.seafarer.add_seafarer',['all_courses' => $courses]);
    }

    public function view($seafarer_id){
        
        if(isset($seafarer_id) AND !empty($seafarer_id)){
            $options['with'] = ['personal_detail', 'professional_detail', 'passport_detail', 'seaman_book_detail', 'coc_detail', 'gmdss_detail', 'wkfr_detail', 'personal_detail.state', 'personal_detail.city', 'personal_detail.pincode.pincodes_states.state', 'personal_detail.pincode.pincodes_cities.city','sea_service_detail','course_detail','cop_detail','coe_detail'];

            $result = $this->userService->getDataByUserId($seafarer_id, $options)->toArray();
            

            if(isset($result[0]['professional_detail']['current_rank_exp'])){
                $rank_exp = explode(".", $result[0]['professional_detail']['current_rank_exp']);
                $result[0]['professional_detail']['years'] = $rank_exp[0];
                $result[0]['professional_detail']['months'] = ltrim($rank_exp[1],0);
            }

            $courses = $this->userService->getAllCourses();

            $user_documents = $this->userService->getUserDocuments($seafarer_id);

            $count = 0;
            if(isset($result[0]['course_detail']) && !empty($result[0]['course_detail'])){
                foreach ($result[0]['course_detail'] as $key => $value) {
                    if($value['course_type'] == 'Value Added'){
                        $result[0]['value_added_course_detail'][$count] = $value;
                        $count++;
                        unset($result[0]['course_detail'][$key]);
                    }
                }
            }
        
            return view('admin.seafarer.view_seafarer',['data' => $result,'all_courses' => $courses,'user_documents' => $user_documents]);
        }
    }

    public function checkEmail(Request $request){
        $data = $request->toArray();

        //dd($data);
        $result = $this->userService->checkEmail($data);
        return $result;
    }

    public function checkMobile(Request $request){
        $data = $request->toArray();

        //dd($data);
        $result = $this->userService->checkMobile($data);
        return $result;
    }

    public function downloadXL(Request $request){

        $filter = $request->toArray();

        $options['with'] = ['personal_detail','professional_detail','passport_detail','seaman_book_detail','coc_detail','gmdss_detail','wkfr_detail','personal_detail.state','personal_detail.city','personal_detail.pincode.pincodes_states.state','personal_detail.pincode.pincodes_cities.city','sea_service_detail','course_detail','cop_detail','coe_detail'];

        if(isset($filter) && !empty($filter)) {
            $seafarer_data = $this->candidateService->getAllCandidateDataByFiltersWithoutPagination($filter);
        }else {
            $seafarer_data = $this->candidateService->getAllCandidateDataByFiltersWithoutPagination();
        }

        $result = $seafarer_data->toArray();
        // dd($result);
        //return view('admin.seafarer.seafarer_excel',['data' => $result]);
        Excel::create('Seafarer Details', function($excel) use($result){

            $excel->sheet('New sheet', function($sheet) use($result) {

                $sheet->loadView('admin.seafarer.seafarer_excel',array('data' => $result));

            });

        })->download('xlsx');
    }

    public function storeSeafarerCourseDetails(Request $request){

        if(Auth::check()){
                        
            $user_id = $request->user_id;

            $data = $request->toArray();
            $result = $this->userService->storeSingleCourseDetail($data,$user_id);

            $course_type = $data['seafarer_course_type'] == 'normal' ? 'Normal' : 'Value Added';
            $course_details = $this->userService->getCourseDetailsByUserId($user_id,$course_type);
            
            $courses = View::make("site.partials.seafarer_course_details")->with(['course_details' => $course_details]);
            $template = $courses->render();

            if ($result['status'] == 'success') {

                if(isset($data['existing_course_id']) && !empty(isset($data['existing_course_id']))){
                    return response()->json(['status' => 'success' , 'message' => 'Course has been updated successfully.', 'courses' => $template],200);
                }
                
                return response()->json(['status' => 'success' , 'message' => 'Course has been added successfully.', 'courses' => $template],200);
            } else {
                return response()->json(['status' => 'error' , 'message' => $result['error']['message']],400);
            }
        }
    }

    public function deleteSeafarerCourseDetails(Request $request){

        if(Auth::check()){
            $data = $request->toArray();
            $user_id = $request->user_id;
            
            $result = $this->userService->deleteSeafarerCourseByCourseId($data['course_id'],$user_id);

            if($result['status'] == 'success'){
                return response()->json($result,200);
            }else{
                return response()->json($result,400);
            }
        }
        else{
            return response()->json(['status' => 'failed' , 'message' => 'Error while adding data.'],400);
        }
    }

    public function getSeafarerCourseByCourseId(Request $request){

        if(Auth::check()){
            $data = $request->toArray();
            $user_id = $request->user_id;
            
            $result = $this->userService->getSeafarerCourseByCourseId($data['course_id'],$user_id);
            
            $course_type = $result['result']['course_type'];

            if($course_type == 'Normal'){
                $courses = \CommonHelper::courses();
                foreach ($courses as $key => $value) {
                    $courses[] = "<option value=$key>$value</option>";
                }
            }else{
                $courses = \CommonHelper::value_added_courses();
                foreach ($courses as $key => $value) {
                    $courses[] = "<option value=$key>$value</option>";
                }
            }

            if($result['status'] == 'success'){
                return response()->json(['result' => $result,'courses' => $courses],200);
            }else{
                return response()->json(['result' => $result,'courses' => $courses],400);
            }
        }
        else{
            return response()->json(['status' => 'failed' , 'message' => 'Error while adding data.'],400);
        }
    }

    public function getSeafarerCoursesByCourseType(Request $request){

        if(Auth::check()){
            $course_type = $request->course_type;
           
            if($course_type == 'normal'){
                $courses = \CommonHelper::courses();
                foreach ($courses as $key => $value) {
                    $courses[] = "<option value=$key>$value</option>";
                }
            }else{
                $courses = \CommonHelper::value_added_courses();
                foreach ($courses as $key => $value) {
                    $courses[] = "<option value=$key>$value</option>";
                }
            }
            
            if($course_type){
                return response()->json(['courses' => $courses],200);
            }else{
                return response()->json(['courses' => ''],400);
            }
        }
        else{
            return response()->json(['status' => 'failed' , 'message' => 'Error while adding data.'],400);
        }
    }

    public function resendWelcomeEmail(Request $request){

        if(isset($request->id) && !empty($request->id)){
            $data = Auth::user()->find($request->id);

            $result = $this->userService->sendWelcomeEmailWithPasswordResetMail($data);

            if($result['status'] == 'success'){
                return response()->json(['status' => 'Success' , 'message' => 'Email has been sent successfully.'],200);
            }else{
                return response()->json(['status' => 'failed' , 'message' => 'Error in Email sending.'],400);
            }
        }else{
            return response()->json(['status' => 'failed' , 'message' => 'Error in Email sending.'],400);
        }
    }

    public function getRequestedDocumentList(Request $request){
        $filter = $request->toArray();

        $options['with'] = ['requester.company_registration_detail','owner','document_type.type'];
        $list = $this->documentService->getRequestedDocumentList($options,$filter);
        return view('admin.seafarer.list_requested_documents',['document_list' => $list,'filter' => $filter]);
    }
}
