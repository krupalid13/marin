<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\StateService;
use App\Services\CityService;
use App\Services\ScheduleService;
use App\Services\UserService;
use Response;
use App\Services\SendEmailService;
use Carbon\Carbon;

use App\Jobs\SendScheduledEmail;
class ScheduleController extends Controller
{	
    private $stateService;
	private $cityService;
    private $scheduleService;
    private $userService;
	private $sendEmailService;

    function __construct()
    {
        $this->stateService = New StateService();
        $this->cityService = New CityService();
        $this->scheduleService = New ScheduleService();
        $this->userService = New UserService();
        $this->sendEmailService = New SendEmailService();
    }

    public function schedule()
    {
        // $next_five_minutes = Carbon::now()->addday(1);
        // $now = Carbon::now();
        // $scheduled_emails = $this->scheduleService->getScheduledEmailByCurrentDateTime($now, $next_five_minutes)->toArray();
        // $cities = [];
        // $country = 95;
        // $users = [];
        // $emailSubArr = [];
        // if( count($scheduled_emails) > 0 ) {
        //     foreach($scheduled_emails as $scheduled_email) {

        //         // if( !empty($scheduled_email['cities']) ) {
        //         //     $cities = json_decode($scheduled_email['cities']);
        //         //     $users = $this->userService->getUserByCities($cities);

        //         // } elseif ( !empty($scheduled_email['country']) ) {
        //         //     $country = $scheduled_email['country'];
        //         //     $users = $this->userService->getUserByCountryId($country);
                    
        //         // }

        //         // if (count($users) > 0) {
        //         //     $users = $users->toArray();
        //         //     foreach ($users as $index => $user) {
        //         //         $recipientArr[$index]['address'] = [];
        //         //         $recipientArr[$index]['address']['name'] = ucwords($user['first_name']);
        //         //         $recipientArr[$index]['address']['email'] = $user['email'];
        //         //         $recipientArr[$index]['substitution_data']['user_name'] = ucwords($user['first_name']);
        //         //         $recipientArr[$index]['substitution_data']['message'] = $scheduled_email['message'];
        //         //     }
        //         //     $emailResponse = $this->sendEmailService->sendScheduleEmail($emailSubArr, $recipientArr,$scheduled_email['subject']);
        //         //     if ($emailResponse['status'] == 200) {
        //         //         $shedule_update = $this->scheduleService->changeStatusTo($scheduled_email['id'],1);
        //         //     } else {
        //         //         $shedule_update = $this->scheduleService->changeStatusTo($scheduled_email['id'],2,$emailResponse['message']);
        //         //     }
                    
        //         // }
        //         dispatch(new SendScheduledEmail($scheduled_email));
        //     }

        // }




    	$states = $this->stateService->getAllStatesWithCities()->toArray();
    	return view('admin.schedule.schedule',['states' => $states]);
    }

    public function addSchedule(Request $request)
    {
    	$input = $request->all();
    	$result = $this->scheduleService->store($input);
    	return redirect()->back()->with('status','success');
    }

    public function listSchedule()
    {
        $pagination_limit = env('PAGINATE_LIMIT');
        $schedules = $this->scheduleService->getAll($pagination_limit);
        $paginate = $schedules;
        $schedules = $schedules->toArray();
        $cities = $this->cityService->getAll()->toArray();
        return view('admin.schedule.list_schedule',['schedules'=>$schedules,'cities' => $cities,'paginate'=>$paginate]);
    }

    public function deleteSchedule(Request $request)
    {
        $content_id = $request->input('id');
        $delete_result = $this->scheduleService->delete($content_id);
        if ($delete_result > 0) {
            return Response::json('success', 200);
        } else {
            return Response::json('error', 400);
        }
    }

    // public function editSchedule($id)
    // {   
    //     $result = $this->scheduleService->getById($id)->toArray();
    //     $states = $this->stateService->getAllStatesWithCities()->toArray();
    //     return view('admin.schedule.schedule',['states' => $states,'result'=>$result]);
    // }
}
