<?php

namespace App\Http\Controllers\Admin;

use App\DocumentsToOtherDetails;
use App\HistoryLog;
use App\Http\Controllers\Controller;
use App\Jobs\SendEmailShareDocs;
use App\Mail\ShareProfile;
use App\Mail\ShareResume;
use App\Repositories\ShareContactsRepository;
use App\ShareContacts;
use App\SharedDocsToOthers;
use App\ShareHistory;
use App\User;
use App\UserPersonalDetail;
use App\UserSemanBookDetail;
use Carbon\Carbon;
use CommonHelper;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Mail;
use Yajra\Datatables\Datatables;
use ZanySoft\Zip\Zip;
use League\Flysystem\Filesystem;
use S3;
use Illuminate\Support\Facades\Storage;
use League\Flysystem\ZipArchive\ZipArchiveAdapter;

class ManageDocumentsController extends Controller
{
    private $shareContactRepository;

    protected function getData($user_id)
    {
        $order = ['Photo', 'Passport', 'US_Visa', 'Indos', 'CDC', 'sea_service', 'sea_service_contract', 'sea_service_article', 'COC', 'COE', 'wk_cop', 'DCE', 'gmdss', 'Courses', 'INSTITUTE_DEGREE', 'SCHOOL_QUALIFICATION', 'ILO_medical', 'screening_test', 'YELLOW_FEVER', 'Cholera', 'hepatitis_b', 'hepatitis_c', 'diphtheria', 'covid', 'Aadhaar', 'CHARACTER_REFERENCE', 'pancard', 'passbook'];
        $userProfileData = \App\UserProfessionalDetail::where('user_id', $user_id)->first();
        if(isset($userProfileData->sid_number) && !empty($userProfileData->sid_number)){
            $order = ['Photo', 'Passport', 'US_Visa', 'Indos', 'SID', 'CDC', 'sea_service', 'sea_service_contract', 'sea_service_article', 'COC', 'COE', 'wk_cop', 'DCE', 'gmdss', 'Courses', 'INSTITUTE_DEGREE', 'SCHOOL_QUALIFICATION', 'ILO_medical', 'screening_test', 'YELLOW_FEVER', 'Cholera', 'hepatitis_b', 'hepatitis_c', 'diphtheria', 'covid', 'Aadhaar', 'CHARACTER_REFERENCE', 'pancard', 'passbook'];
        }
        $user_contract = User::whereId($user_id)->with('user_documents_type.user_documents')->get()->map(function ($q) {
            $_container = $q->user_documents_type->reject(function ($iq, $key) {
                return (count($iq->user_documents) > 0 ? false : true);
            });
            $_docs_maintainer = [];
            $_docs_maintainer['is_docs'] = false;
            foreach ($_container as $_is_docs_added) {
                if (count($_is_docs_added['user_documents'])) {
                    $_docs_maintainer['is_docs'] = true;
                    break;
                }
            }
            if (!$_docs_maintainer['is_docs']) {
                return $_docs_maintainer;
            }

            $_for_coc = $q->coc_detail;
            // dd($_for_coc);
            $_for_passport = $q->passport_detail;
            $_for_cdc = $q->seaman_book_detail;

            $_for_coe = $q->coe_detail;
            $_for_sea_service = ($q->sea_service_detail)->toArray();
            $_for_sea_service_by_id = [];
            foreach($_for_sea_service as $_sea_service_data){
                $_for_sea_service_by_id[$_sea_service_data['id']] = $_sea_service_data;
            }
            $_for_courses = $q->course_detail;
            $_for_gmdss = $q->gmdss_detail;
            $_professional_details = $q->professional_detail;
            $_wk = $q->wkfr_detail;
            $services_article = [];
            foreach ($_container as $docs) {
                if ($docs['type'] == 'coc') {
                    foreach ($_for_coc as $_coc_unset => $coc) {
                        if ($coc['id'] == $docs['type_id']) {
                            $_docs_maintainer[$docs['type']][$docs['type_id']]['heading'] = $coc['coc_grade'] . ' ' .
                            CommonHelper::countries()[$coc['coc']];
//                            $_docs_maintainer[$docs['type']][$docs['type_id']]['coc']= $coc['coc'];
                            //                            $_docs_maintainer[$docs['type']][$docs['type_id']]['coc_expiry_date']= $coc['coc_expiry_date'];
                            //                            $_docs_maintainer[$docs['type']][$docs['type_id']]['coc_grade']= $coc['coc_grade'];

                            unset($_for_coc[$_coc_unset]);
                        }
                    }
                } else
                if ($docs['type'] == 'passport') {
                    $_docs_maintainer[$docs['type']][$docs['type_id']]['heading'] = CommonHelper::countries()[$_for_passport['pass_country']];

                    unset($_for_passport);
                } else
                if ($docs['type'] == 'cdc') {
                    foreach ($_for_cdc as $_cdc_unset => $cdc) {
                        if ($docs['type_id'] == $cdc['id']) {
                            $_docs_maintainer[$docs['type']][$docs['type_id']]['heading'] = CommonHelper::countries()[$cdc['cdc']];
//                            $_docs_maintainer[$docs['type']][$docs['type_id']]['cdc']=$cdc['cdc'];
                            //                            $_docs_maintainer[$docs['type']][$docs['type_id']]['cdc_number']=$cdc['cdc_number'];
                            //                            $_docs_maintainer[$docs['type']][$docs['type_id']]['cdc_issue_date']=$cdc['cdc_issue_date'];
                            //                            $_docs_maintainer[$docs['type']][$docs['type_id']]['cdc_expiry_date']=$cdc['cdc_expiry_date'];

                            unset($_for_cdc[$_cdc_unset]);
                        }
                    }
                } else
                if ($docs['type'] == 'coe') {
                    foreach ($_for_coe as $_coe_unset => $coe) {
                        if ($docs['type_id'] == $coe['id']) {
                            $_docs_maintainer[$docs['type']][$docs['type_id']]['heading'] = (isset($coe['coe_grade']) ? $coe['coe_grade'] : '') .
                                ' ' . ((isset($coe['coe']) &&
                                $coe['coe'] != '') ? CommonHelper::countries()[$coe['coe']] : '');
//                            $_docs_maintainer[$docs['type']][$docs['type_id']]['coe']=$coe['coe'];
                            //                            $_docs_maintainer[$docs['type']][$docs['type_id']]['coe_number']=$coe['coe_number'];
                            //                            $_docs_maintainer[$docs['type']][$docs['type_id']]['coe_expiry_date']=$coe['coe_expiry_date'];
                            //                            $_docs_maintainer[$docs['type']][$docs['type_id']]['coe_grade']=$coe['coe_grade'];

                            unset($_for_coe[$_coe_unset]);
                        }
                    }
                } else
                if ($docs['type'] == 'sea_service') {
                    foreach ($_for_sea_service as $_sea_service_unset => $sea_service) {
                        if ($docs['type_id'] == $sea_service['id']) {
                            $toDate = isset($sea_service['to']) && !empty($sea_service['to']) ? date('d-m-Y', strtotime($sea_service['to'])) : 'On Board';
                            $_docs_maintainer[$docs['type']][$docs['type_id']]['heading'] = date('d-m-Y', strtotime($sea_service['from'])) .
                                ' / ' . $toDate;
                            $_docs_maintainer[$docs['type']][$docs['type_id']]['sdd'] = ($sea_service['from']);
                            unset($_for_sea_service[$_sea_service_unset]);
                        }
                    }
                } else
                if ($docs['type'] == 'courses') {
                    foreach ($_for_courses as $_courses_unset => $courses) {
                        if ($docs['type_id'] == $courses['course_id']) {
                            $arr = CommonHelper::course_name_by_course_type($courses['course_type']);
                            //will understand later
                            foreach ($arr as $val) {
                                if ($courses['course_id'] == $val->id) {
                                    $_docs_maintainer[$docs['type']][$docs['type_id']]['heading'] = $val->course_name;
                                }
                            }

                            unset($_for_courses[$_courses_unset]);
                        }
                    }
                } else
                if ($docs['type'] == 'gmdss') {
//                                            if (isset($_for_gmdss['gmdss']) && $_for_gmdss['gmdss'] != 0){

                    $_docs_maintainer[$docs['type']][$docs['type_id']]['heading'] = CommonHelper::countries()[$_for_gmdss['gmdss']];
//                                                $_docs_maintainer[$docs['type']][$docs['type_id']]['heading'] = CommonHelper::countries()[$_for_gmdss['gmdss']];
                    //                                            }
                    //                                            $_docs_maintainer[$docs['type']][$docs['type_id']]['heading'] ="GMDSS";
                } else
                if ($docs['type'] == 'character_certificate') {
                    $_docs_maintainer[$docs['type']][$docs['type_id']]['heading'] = 'Character Certificate';
                } else
                if ($docs['type'] == 'institute_degree') {
                    $_docs_maintainer[$docs['type']][$docs['type_id']]['heading'] = $_professional_details['institute_degree'];
                } else
                if ($docs['type'] == 'school_qualification') {
                    $_docs_maintainer[$docs['type']][$docs['type_id']]['heading'] = $_professional_details['school_qualification'];
                } else
                if ($docs['type'] == 'passbook') {
                    $_docs_maintainer[$docs['type']][$docs['type_id']]['heading'] = 'Passbook';
                } else
                if ($docs['type'] == 'ilo_medical') {
                    $_docs_maintainer[$docs['type']][$docs['type_id']]['heading'] = 'ILO Medical';
                } else
                if ($docs['type'] == 'screening_test') {
                    $_docs_maintainer[$docs['type']][$docs['type_id']]['heading'] = 'Medical Screening Test';
                } else
                if ($docs['type'] == 'yellow_fever') {
                    $_docs_maintainer[$docs['type']][$docs['type_id']]['heading'] = 'Vaccination - Yellow Fever';
                } else
                if ($docs['type'] == 'cholera') {
                    $_docs_maintainer[$docs['type']][$docs['type_id']]['heading'] = 'Vaccination - Cholera';
                } else
                if ($docs['type'] == 'hepatitis_b') {
                    $_docs_maintainer[$docs['type']][$docs['type_id']]['heading'] = 'Vaccination - Hepatitis B';
                } else
                if ($docs['type'] == 'hepatitis_c') {
                    $_docs_maintainer[$docs['type']][$docs['type_id']]['heading'] = 'Vaccination - Hepatitis C';
                } else
                if ($docs['type'] == 'diphtheria') {
                    $_docs_maintainer[$docs['type']][$docs['type_id']]['heading'] = 'Vaccination - Diphtheria & Tetanus';
                } else
                if ($docs['type'] == 'wk_cop') {
                    $_docs_maintainer[$docs['type']][$docs['type_id']]['heading'] = $_wk['wk_cop'] . ' ' . CommonHelper::countries()[$_wk['watch_keeping']];
                } else
                if ($docs['type'] == 'sea_service_article') {
                    $string = str_replace('article', '', $docs['type_id']);
                    $sea_service_article_heading = '';
                    if(isset($_for_sea_service_by_id[$string])){
                        $curSeaServiceData = $_for_sea_service_by_id[$string];
                        if(isset($curSeaServiceData['from']) && !empty($curSeaServiceData['from'])){
                            $sea_service_article_heading = date('d-m-Y', strtotime($curSeaServiceData['from']));
                        }
                        if(isset($curSeaServiceData['to']) && !empty($curSeaServiceData['to'])){
                            $sea_service_article_heading = $sea_service_article_heading . " to " . date('d-m-Y', strtotime($curSeaServiceData['to']));
                        }else{
                            $sea_service_article_heading = $sea_service_article_heading . " to " . " On Board";
                        }
                    }
                    $_docs_maintainer[$docs['type']][$docs['type_id']]['heading'] = $sea_service_article_heading;
                } else
                if ($docs['type'] == 'sea_service_contract') {
                    $string = str_replace('contract', '', $docs['type_id']);
                    $sea_service_contract_heading = '';
                    if(isset($_for_sea_service_by_id[$string])){
                        $curSeaServiceData = $_for_sea_service_by_id[$string];
                        if(isset($curSeaServiceData['from']) && !empty($curSeaServiceData['from'])){
                            $sea_service_contract_heading = date('d-m-Y', strtotime($curSeaServiceData['from']));
                        }
                        if(isset($curSeaServiceData['to']) && !empty($curSeaServiceData['to'])){
                            $sea_service_contract_heading = $sea_service_contract_heading . " to " . date('d-m-Y', strtotime($curSeaServiceData['to']));
                        }else{
                            $sea_service_contract_heading = $sea_service_contract_heading . " to " . " On Board";
                        }
                    }
                    $_docs_maintainer[$docs['type']][$docs['type_id']]['heading'] = $sea_service_contract_heading;
                    array_push($services_article, $docs);
                } else
                if ($docs['type'] == 'indos' ||
                    $docs['type'] == 'sid' ||
                    $docs['type'] == 'aadhaar' ||
                    $docs['type'] == 'pancard' ||
                    $docs['type'] == 'photo') {
                    $_docs_maintainer[$docs['type']][$docs['type_id']]['heading'] = str_replace('_', ' ', ucfirst($docs['type']));
                }
                $_docs_maintainer[$docs['type']][$docs['type_id']][] = $docs;
                $_docs_maintainer[$docs['type']]['id'] = $docs['type'];
            }
            return $_docs_maintainer;
        })->first();
        $order_tolower = array_map('strtolower', $order);

        $contracts = ["is_docs" => $user_contract['is_docs']];
        foreach ($order_tolower as $order) {
            if (array_key_exists($order, $user_contract)) {
                $contracts[$order] = $user_contract[$order];
            }
        }
//        return $order_tolower;

//         uksort($user_contract, function ($user1, $user2) use ($order_tolower) {
        //             return (array_search($user1, $order_tolower) > array_search($user2, $order_tolower));
        //         });
        // dd($user_contract);
        // dd($contracts);
        return $contracts;
    }

    public function downloadZipPost(Request $request, $token = null)
    {
        $user_id = '';
        $shared_docs = "";
        if ($token == null) {
            $user_id = Auth::id();
        } else {
            $shared_docs = SharedDocsToOthers::whereToken($token)->firstOrFail();
            $user_id = $shared_docs->user_id;

        }

        $is_request_sent = "false";
        if ($shared_docs && (($shared_docs->valid_time + (3600 * 48)) < time())) {

            $History = HistoryLog::where("share_history_id", $shared_docs->share_history_id)->where("description", "This Document link has expired")->first();
            if (!$History) {
                $HistoryLog = new HistoryLog();
                $HistoryLog->share_history_id = $shared_docs->share_history_id;
                $HistoryLog->description = "This Document link has expired";
                $HistoryLog->ip = '';
                $HistoryLog->created_at = date('Y-m-d H:i:s');
                $HistoryLog->save();
            }

            if ($request->isMethod('post')) {
                $HistoryLog = new HistoryLog();
                $HistoryLog->share_history_id = $shared_docs->share_history_id;
                $HistoryLog->description = "Requested for a New Link (Pending)";
                $HistoryLog->ip = '';
                $HistoryLog->created_at = date('Y-m-d H:i:s');
                $HistoryLog->save();
                $is_request_sent = "true";
            }
            return abort(410, 'The link to the Document you are trying to access has Expired###' . $token . "###" . $is_request_sent);
        }
        $request_in_array = $request->toArray();
        if (!empty($request_in_array['downloadable_docs'])) {

            if ($shared_docs) {
                $existTodayHistory = HistoryLog::where("share_history_id", $shared_docs->share_history_id)->where("description", "Documents Downloaded")->whereDate('created_at', '=', date('Y-m-d'))->first();
                if(empty($existTodayHistory)){
                    $HistoryLog = new HistoryLog();
                    $HistoryLog->share_history_id = $shared_docs->share_history_id;
                    $HistoryLog->description = "Documents Downloaded";
                    $HistoryLog->ip = '';
                    $HistoryLog->created_at = date('Y-m-d H:i:s');
                    $HistoryLog->save();
                }
            }

            $headers = ["Content-Type" => "application/zip"];
            $docName = 'uploaded-document';
            $currentDateAndTime = date('Y-m-d H-i-s');
            $userData = User::whereId($user_id)->first();
            if($userData && isset($userData->first_name) && !empty($userData->first_name)){
               $docName = $userData->first_name;
            }
            $docName = $docName . ' ' . $currentDateAndTime . ' Flankknot';
            $path = $this->createZip($user_id, $docName, $request_in_array['downloadable_docs']);
            return response()->download($path, $docName . '.zip', $headers)->deleteFileAfterSend(true);
        } else {
            return redirect()->back()->with("error", 'No Documents Selected');
        }

    }

    protected function createZip($user_id, $name, $data)
    {
        
//        $file_names = Storage::disk($source_disk)->files($source_path);
        $source_disk = 's3';
        $full_path = env('DOCUMENT_STORAGE_PATH') . $user_id;
//        $zip = new Filesystem(new ZipArchiveAdapter(public_path('archive123.zip')));
        $zip = new Filesystem(new ZipArchiveAdapter(env('DOCUMENT_STORAGE_PATH') . $user_id . '/' . $name . '.zip'));
        foreach ($data as $path) {
            $fullFilePath = $full_path . $path;
            if (Storage::disk('s3')->exists($fullFilePath)) {
                $file_content = Storage::disk($source_disk)->get($fullFilePath);
                $renameFilePath = str_replace("public/uploads/user_documents/" . $user_id . "/", "", $fullFilePath);
                if (strpos($renameFilePath, 'cdc/') !== false) {
                    $cdcId = $this->get_string_between($renameFilePath, 'cdc/', '/');
                    if (!empty($cdcId)) {
                        $cdcData = UserSemanBookDetail::where("id", $cdcId)->first();
                        if (!empty($cdcData) && !empty($cdcData['cdc'])) {
                            $cdcCountry = CommonHelper::countries()[$cdcData['cdc']];
                            if (!empty($cdcCountry)) {
                                $renameFilePath = str_replace("cdc/" . $cdcId . "/", "cdc/" . $cdcCountry . "/", $renameFilePath);
                            }
                        }
                    }
                }
                if (strpos($renameFilePath, 'coc/') !== false) {
                    $cocId = $this->get_string_between($renameFilePath, 'coc/', '/');
                    if (!empty($cocId)) {
                        $cocData = \App\UserCocDetail::whereId($cocId)->first();
                        if (!empty($cocData) && !empty($cocData['coc_grade'])) {
                            $cocGrade = $cocData['coc_grade'];
                            if (!empty($cocGrade)) {
                                $renameFilePath = str_replace("coc/" . $cocId . "/", "coc/" . $cocGrade . "/", $renameFilePath);
                            }
                        }
                    }
                }
                if (strpos($renameFilePath, 'coe/') !== false) {
                    $coeId = $this->get_string_between($renameFilePath, 'coe/', '/');
                    if (!empty($coeId)) {
                        $coeData = \App\UserCoeDetails::whereId($coeId)->first();
                        if (!empty($coeData) && !empty($coeData['coe_grade'])) {
                            $coeGrade = $coeData['coe_grade'];
                            if (!empty($coeGrade)) {
                                $renameFilePath = str_replace("coe/" . $coeId . "/", "coe/" . $coeGrade . "/", $renameFilePath);
                            }
                        }
                    }
                }
                if (strpos($renameFilePath, 'courses/') !== false) {
                    $courseId = $this->get_string_between($renameFilePath, 'courses/', '/');
                    if (!empty($courseId)) {
                        $courses = \App\Courses::where('id', $courseId)->value('course_name');
                        $courseDisplay = 'Course' . ' - ' . str_replace(")", "", str_replace("(", "", substr($courses, strpos($courses, '('))));
                        $renameFilePath = str_replace("courses/" . $courseId . "/", "courses/" . $courseDisplay . "/", $renameFilePath);
                    }
                }
                if (strpos($renameFilePath, 'sea_service/') !== false) {
                    $seaServiceId = $this->get_string_between($renameFilePath, 'sea_service/', '/');
                    if (!empty($seaServiceId)) {
                        $seaDataDisplay = "";
                        $seaServiceData = \App\UserSeaService::where('id', $seaServiceId)->first();
                        if (!empty($seaServiceData)) {
                            if (isset($seaServiceData['from']) && !empty($seaServiceData['from'])) {
                                $seaDataDisplay = date('d M Y', strtotime($seaServiceData['from']));
                                if (isset($seaServiceData['to']) && !empty($seaServiceData['to'])) {
                                    $seaDataDisplay = $seaDataDisplay . ' - ' . date('d M Y', strtotime($seaServiceData['to']));
                                } else {
                                    $seaDataDisplay = $seaDataDisplay . ' On Board';
                                }
                                $renameFilePath = str_replace("sea_service/" . $seaServiceId . "/", "sea service/" . $seaDataDisplay . "/", $renameFilePath);
                            }
                        }
                    }
                }
                if (strpos($renameFilePath, 'sea_service_contract/') !== false) {
                    $seaServiceId = $this->get_string_between($renameFilePath, 'sea_service_contract/', '/');
                    $seaServiceId = str_replace('contract', '', $seaServiceId);
                    $seaDataDisplay = "";
                    $seaServiceData = \App\UserSeaService::where('id', $seaServiceId)->first();
                    if (!empty($seaServiceData)) {
                        if (isset($seaServiceData['from']) && !empty($seaServiceData['from'])) {
                            $seaDataDisplay = date('d M Y', strtotime($seaServiceData['from']));
                            if (isset($seaServiceData['to']) && !empty($seaServiceData['to'])) {
                                $seaDataDisplay = $seaDataDisplay . ' - ' . date('d M Y', strtotime($seaServiceData['to']));
                            } else {
                                $seaDataDisplay = $seaDataDisplay . ' On Board';
                            }
                        }
                        $renameFilePath = str_replace("sea_service_contract/" . $seaServiceId . "contract/", "sea service contract/" . $seaDataDisplay . "/", $renameFilePath);
                    }
                }
                if (strpos($renameFilePath, 'sea_service_article/') !== false) {
                    $seaServiceId = $this->get_string_between($renameFilePath, 'sea_service_article/', '/');
                    $seaServiceId = str_replace('article', '', $seaServiceId);
                    $seaDataDisplay = "";
                    $seaServiceData = \App\UserSeaService::where('id', $seaServiceId)->first();
                    if (!empty($seaServiceData)) {
                        if (isset($seaServiceData['from']) && !empty($seaServiceData['from'])) {
                            $seaDataDisplay = date('d M Y', strtotime($seaServiceData['from']));
                            if (isset($seaServiceData['to']) && !empty($seaServiceData['to'])) {
                                $seaDataDisplay = $seaDataDisplay . ' - ' . date('d M Y', strtotime($seaServiceData['to']));
                            } else {
                                $seaDataDisplay = $seaDataDisplay . ' On Board';
                            }
                        }
                        $renameFilePath = str_replace("sea_service_article/" . $seaServiceId . "article/", "sea service article/" . $seaDataDisplay . "/", $renameFilePath);
                    }
                }
                $zip->put($name.'/'.$renameFilePath, $file_content);
            }
        }
        $zip->getAdapter()->getArchive()->close();
        return env('DOCUMENT_STORAGE_PATH') . $user_id . '/' . $name . '.zip';
        
        
//        $zip = Zip::create(env('DOCUMENT_STORAGE_PATH') . $user_id . '/' . $name . '.zip', true);
//        $full_path = env('DOCUMENT_STORAGE_PATH') . $user_id;
//        foreach ($data as $path) {
////            $zip->add(env("AWS_URL") . str_replace("//", "/", $full_path . '/' . $path));
//            $zip->add('https://flankknot.com/public/images/homepage/banner/cruise-img.jpg');
//        }
//        $zip->close();
////        dd($zip);
//        return env('DOCUMENT_STORAGE_PATH') . $user_id . '/' . $name . '.zip';
    }
    
    function get_string_between($string, $start, $end) {
        $string = ' ' . $string;
        $ini = strpos($string, $start);
        if ($ini == 0)
            return '';
        $ini += strlen($start);
        $len = strpos($string, $end, $ini) - $ini;
        return substr($string, $ini, $len);
    }

    public function downloadIndex()
    {
        $pageTitle = "Document - Collective or individual download of user seafarer documents | Flanknot";
        $metaDescription = "At Flanknot, Seafarer User can download all the documents that has been uploaded in edit profile. Documents are downloaded in zip format.";
        $metaKeywords = "download, zip file, documents";
        $user_contract = $this->getData(Auth::id());
        return view('admin.manage_documents.download_index', compact('user_contract', 'pageTitle', 'metaDescription', 'metaKeywords'));
    }

    // share docs index
    public function shareDocuments()
    {
        $pageTitle = "Seafarer document list - uploaded document list | Flanknot";
        $metaDescription = "At Flanknot, Seafarer User can select individually or all the documents that were uploaded in edit profile. 
            The Seafarer can share via email a link to the list of documents.";
        $metaKeywords = "Documents, listing, Passport, Visa, Indos, CDC, Continuous discharge Certificate, Seaman book information, WK, Watchkeeping Certificate, COP, Certificate of Proficiency, COC, Certificate of Competency, COE, Certificate of Endorsement, DCE, Dangerous Cargo Endorsement, GMDSS, Global Maritime Distress and Safety System, FRAMO, DP, Dynamic positioning, Sea Service, vessel name, owner company, manning company, vessel flag, vessel type, vessel GRT, vessel BHP, vessel engine type, sing on date and sign off date, Course details, basic type courses, Advance type courses, Offshore related courses, Tanker related courses, Refresher and Upgradation courses, Management courses, Academic details, ILO Medical and Screening Medical details, Yellow fever vaccination, Cholera vaccination, Hepatitis B vaccination, D&T vaccination, covid vaccination, Character Certificate, PCC, PAN, Aadhar, Bank details.";
        $user_contract = $this->getData(Auth::id());
        // dd($user_contract);

        //return $user_contract;

        $user_details = UserPersonalDetail::where('user_id', Auth::id())->first();
        $bank_detail['bank_name'] = $user_details['bank_name'];
        $bank_detail['account_holder_name'] = $user_details['account_holder_name'];
        $bank_detail['account_no'] = $user_details['account_no'];
        $bank_detail['ifsc_code'] = $user_details['ifsc_code'];
        $bank_detail['branch_address'] = $user_details['branch_address'];
        $personal_detail['aadhaar'] = $user_details['aadhaar'];
        $personal_detail['pancard'] = $user_details['pancard'];
        $show_resume = CommonHelper::getResumeNumber();
        $request = new \Illuminate\Http\Request();
        $request['PDF'] = CommonHelper::encodeKey($show_resume);
        $pdf_ctl = new \App\Http\Controllers\Admin\PdfController();

        $personalDetails = ['aadhaar', 'character_reference', 'pancard', 'passbook'];
        $bank_identity = [];
        $is_document_uploaded = false;

//        dd($user_contract);
        foreach ($user_contract as $key => $value) {
            if (in_array($key, $personalDetails)) {
                $bank_identity[$key] = $value;
                unset($user_contract[$key]);
                $is_document_uploaded = true;
            }
        }
        $share_contact = ShareContacts::where('user_id', Auth::id())->get();
        //        dd($share_contact);
        //        return $user_contract;

//        $pdf_ctl->pdfview($request,false);
        return view('admin.manage_documents.share_docs', compact('user_contract', 'bank_detail', 'personal_detail', 'share_contact', 'bank_identity', 'is_document_uploaded', 'pageTitle', 'metaDescription', 'metaKeywords'));
    }

    public function shareIndex()
    {
        $pageTitle = "Sharing via email - Seafarer can email a link to share Resume, Documents, Profile | Flanknot ";
        $metaDescription = "At Flanknot, Seafarer can share via email a link to the user Profile that holds complete informative description.
            Seafarer can share via email a link to the user Resume where a QR can be given.
            Seafarer can share via email a link to user documents where it can be downloaded.";
        $metaKeywords = "Resume, documents, profile, contacts, share, link, email";
        $user_contract = $this->getData(Auth::id());
        $share_contact = ShareContacts::where('user_id', Auth::id())->get();
//        dd($share_contact);
        //        return $user_contract;

        $user_details = UserPersonalDetail::where('user_id', Auth::id())->first();
        $bank_detail['bank_name'] = $user_details['bank_name'];
        $bank_detail['account_no'] = $user_details['account_no'];
        $bank_detail['ifsc_code'] = $user_details['ifsc_code'];
        $bank_detail['branch_address'] = $user_details['branch_address'];
        $personal_detail['aadhaar'] = $user_details['aadhaar'];
        $personal_detail['pancard'] = $user_details['pancard'];
        return view('admin.manage_documents.index', compact('user_contract', 'bank_detail', 'personal_detail', 'share_contact', 'pageTitle', 'metaDescription', 'metaKeywords'));
    }

    public function viaEmail(Request $request)
    {
        $data_arr = $request->toArray();
        $to_emails = [];
        if ($data_arr['to_emails']) {
            $emails = explode("###", $data_arr['to_emails']);
            foreach ($emails as $row) {
                array_push($to_emails, str_replace("(", "", str_replace(")", "", substr($row, strpos($row, '(')))));
            }
        } else {
            array_push($to_emails, $data_arr['email']);
        }

        $_bank_details = [];
        $_only_bank = false;
        $_is_parent = empty($data_arr['shareable_docs_parent']);
        $_is_children = empty($data_arr['shareable_docs_children']);

        $user = Auth::user();
//        return $data_arr;

        if ((!$_is_parent || !$_is_children || isset($data_arr['resume'])) && isset($data_arr['email'])) {
            try {
                if (!$_is_children && !$_is_parent) {
                    $_filtered_parents = array_keys(($data_arr['shareable_docs_children']));
                    $data_arr['shareable_docs_parent'] = array_filter($data_arr['shareable_docs_parent'], function ($value) use ($_filtered_parents) {
                        if (!in_array($value, $_filtered_parents)) {
                            return true;
                        }
                    }); // remove child name from parent docs
                }

                foreach ($to_emails as $to_email) {
                    $store = new SharedDocsToOthers();
                    $docs = [];
                    if (!$_is_parent) {
                        $docs['parent'] = $data_arr['shareable_docs_parent'];
                    }

                    if (!$_is_children) {
                        $docs['child'] = $data_arr['shareable_docs_children'];
                    }

                    $store->user_id = $user->id;
                    $store->token = str_random(25);
                    $store->valid_time = time();
                    $store->data = json_encode($docs);
                    $store->receiver_email = $to_email;

                    $shareContactId = 0;
                    $this->shareContactRepository = new ShareContactsRepository();
                    $shareContactexist = $this->shareContactRepository->checkShareContactExist($to_email, $user->id);
                    if (!empty($shareContactexist[0])) {
                        $shareContactId = $shareContactexist[0]['id'];
                    } else {
                        $shareContactData = array();
                        $shareContactData['user_id'] = $user->id;
                        $shareContactData['email'] = $to_email;
                        $shareContactData['type'] = 1;
                        $this->shareContactRepository = new ShareContactsRepository();
                        $saveContact = $this->shareContactRepository->store($shareContactData);
                        $shareContactId = $saveContact->id;
                    }

                    $shareHistory = new ShareHistory();
                    $shareHistory->roll_type = 3;
                    $shareHistory->user_id = $user->id;
                    $shareHistory->is_allow_qr = 0;
                    $shareHistory->contact_id = $shareContactId;
                    $shareHistory->email = $to_email;
                    $shareHistory->save();
                    $shareHistoryId = $shareHistory->id;

                    if (!$_is_parent && isset($data_arr['shareable_docs_parent'][0]) && ($data_arr['shareable_docs_parent'][0] == 'bank_details')) {
                        $_only_bank = true;
                    }

                    if (!$_is_parent && in_array('bank_details', $data_arr['shareable_docs_parent'])) {
                        $__user_details = UserPersonalDetail::where('user_id', $user->id)->first();
                        $_bank_details['bank_name'] = $__user_details['bank_name'];
                        $_bank_details['account_holder_name'] = $__user_details['account_holder_name'];
                        $_bank_details['account_no'] = $__user_details['account_no'];
                        $_bank_details['ifsc_code'] = $__user_details['ifsc_code'];
                        $_bank_details['branch_address'] = $__user_details['branch_address'];
                        $_bank_details['aadhaar'] = $__user_details['aadhaar'];
                        $_bank_details['pancard'] = $__user_details['pancard'];
                    }

                    $_full_name = $user->first_name . ' ' . $user->last_name;
                    $data = json_decode($store->data);
                    $parent = !empty($data->parent) ? $data->parent : [];
                    $child = !empty($data->child) ? $data->child : null;
                    $sharedData['parent'] = (array) $parent;
                    $sharedDataDisplay['parent'] = (array) $parent;
                    // dd($child);
                    if ($child) {
                        foreach ($child as $key => $row) {
                            if ($key == 'courses') {
                                foreach ($child->courses as $value) {
                                    $courses = \App\Courses::where('id', $value)->value('course_name');
                                    $sharedData['parent'][] = 'Course' . ' - ' . str_replace(")", "", str_replace("(", "", substr($courses, strpos($courses, '('))));
                                    $sharedDataDisplay['parent'][] = 'Course' . ' - ' . str_replace(")", "", str_replace("(", "", substr($courses, strpos($courses, '('))));
                                }
                            } elseif ($key == 'sea_service') {
                                foreach ($row as $value) {
                                    // dump(strlen($value));
                                    if (strlen($value) == 10) {
                                        $seaData = $value . ' On Board';
                                    } else {
                                        $seaData = substr_replace($value, " / ", 10, -strlen($value));
                                    }
                                    $sharedData['parent'][] = 'Service' . ' - ' . $seaData;
                                    $seaDataDisplay = "";
                                    $seaServiceData = \App\UserSeaService::where('id', $value)->first();
                                    if(!empty($seaServiceData)){
                                        if(isset($seaServiceData['from']) && !empty($seaServiceData['from'])){
                                            $seaDataDisplay = $seaServiceData['from'];
                                            if(isset($seaServiceData['to']) && !empty($seaServiceData['to'])){
                                                $seaDataDisplay = $seaDataDisplay . ' / ' . $seaServiceData['to'];
                                            }else{
                                                $seaDataDisplay = $seaDataDisplay . ' On Board';
                                            }
                                        }
                                    }
                                    $sharedDataDisplay['parent'][] = 'Service' . ' - ' . $seaDataDisplay;
                                }
                            } elseif ($key == 'sea_service_article') {

                                foreach ($row as $value) {
                                    $string = str_replace('article', '', $value);
                                    $fromDate = substr($string, 0, 10);
                                    $toDate = (strlen($string) >= 20) ? date('d-m-Y', strtotime(substr($string, 10))) : "On Board";

                                    $sharedData['parent'][] = 'Article' . ' - ' . date('d-m-Y', strtotime($fromDate)) . ' to ' . $toDate;
                                    $seaDataDisplay = "";
                                    $seaServiceData = \App\UserSeaService::where('id', $value)->first();
                                    if(!empty($seaServiceData)){
                                        if(isset($seaServiceData['from']) && !empty($seaServiceData['from'])){
                                            $seaDataDisplay = $seaServiceData['from'];
                                            if(isset($seaServiceData['to']) && !empty($seaServiceData['to'])){
                                                $seaDataDisplay = $seaDataDisplay . ' / ' . $seaServiceData['to'];
                                            }else{
                                                $seaDataDisplay = $seaDataDisplay . ' On Board';
                                            }
                                        }
                                    }
                                    $sharedDataDisplay['parent'][] = 'Article' . ' - ' . $seaDataDisplay;
                                }
                            } elseif ($key == 'sea_service_contract') {

                                foreach ($row as $value) {
                                    $string = str_replace('contract', '', $value);
                                    $fromDate = substr($string, 0, 10);
                                    $toDate = (strlen($string) >= 20) ? date('d-m-Y', strtotime(substr($string, 10))) : "On Board";

                                    $sharedData['parent'][] = 'Contract' . ' - ' . date('d-m-Y', strtotime($fromDate)) . ' to ' . $toDate;
                                    $seaDataDisplay = "";
                                    $seaServiceData = \App\UserSeaService::where('id', $value)->first();
                                    if(!empty($seaServiceData)){
                                        if(isset($seaServiceData['from']) && !empty($seaServiceData['from'])){
                                            $seaDataDisplay = $seaServiceData['from'];
                                            if(isset($seaServiceData['to']) && !empty($seaServiceData['to'])){
                                                $seaDataDisplay = $seaDataDisplay . ' / ' . $seaServiceData['to'];
                                            }else{
                                                $seaDataDisplay = $seaDataDisplay . ' On Board';
                                            }
                                        }
                                    }
                                    $sharedDataDisplay['parent'][] = 'Contract' . ' - ' . $seaDataDisplay;
                                }
                            } elseif ($key == 'coe') {
                                $coes = Auth::user()->coe_detail->toArray();
                                foreach ($row as $key_child => $value) {
                                    foreach ($coes as $key_coe => $coe) {
                                        if ($value == $coe['id']) {
                                            $sharedData['parent'][] = "Coe - " . ((isset($coe['coe']) && $coe['coe'] != '') ? CommonHelper::countries()[$coe['coe']] : '');
                                            $sharedDataDisplay['parent'][] = "Coe - " . ((isset($coe['coe']) && $coe['coe'] != '') ? CommonHelper::countries()[$coe['coe']] : '');
                                        }
                                    }
                                }
                            } elseif ($key == 'coc') {
                                $cocs = Auth::user()->coc_detail->toArray();
                                foreach ($row as $key_child => $value) {
                                    foreach ($cocs as $key_coc => $coc) {
                                        if ($value == $coc['id']) {
                                            $sharedData['parent'][] = "Coc - " . CommonHelper::countries()[$coc['coc']];
                                            $sharedDataDisplay['parent'][] = "Coc - " . CommonHelper::countries()[$coc['coc']];
                                        }
                                    }
                                }
                            } elseif ($key == 'cdc') {
                                $cdcs = Auth::user()->seaman_book_detail;
                                foreach ($row as $key_child => $value) {
                                    foreach ($cdcs as $cdc) {
                                        if ($value == $cdc['id']) {
                                            $sharedData['parent'][] = "Cdc - " . CommonHelper::countries()[$cdc['cdc']];
                                            $sharedDataDisplay['parent'][] = "Cdc - " . CommonHelper::countries()[$cdc['cdc']];
                                            break;
                                        } else {
                                            $sharedData['parent'][] = 'Cdc';
//                                            if(!isset($sharedDataDisplay['parent']['Cdc'])){
//                                                $sharedDataDisplay['parent'][] = 'CDC';
//                                            }
                                        }
                                    }
                                }
                            } elseif ($key == 'dce') {
                                foreach ($row as $value) {
                                    if ($value == 'all') {
                                        $data = 'Oil + Chemical + Liquefied Gas';
                                    } else {
                                        $data = "Dce - " . ucwords(strtolower(str_replace('_', ' ', str_replace("lequefied_gas", "liquified_gas", $value))));
                                    }
                                    $sharedData['parent'][] = $data;
                                    $sharedDataDisplay['parent'][] = $data;
                                }
                            } else {
                                $sharedData['parent'][] = $key;
                                $sharedDataDisplay['parent'][] = $key;
                            }
                        }
                    }
                    // dd('rrr');
                    $sharedData = array_filter($sharedData);
                    $sharedData = !empty($sharedData['parent']) ? $sharedData['parent'] : [];
                    $sharedDataDisplay = array_filter($sharedDataDisplay);
                    $sharedDataDisplay = !empty($sharedDataDisplay['parent']) ? $sharedDataDisplay['parent'] : [];
                    $userCocDetail = \App\UserCocDetail::whereUserId($user->id)->first();
                    $userWkfrDetail = \App\UserWkfrDetail::whereUserId($user->id)->first();
                    $mail_data = [
                        'email' => $to_email,
                        'subject' => '' . $_full_name . ' has shared Documents with you.',
                        'token' => $store->token,
                        'valid_time' => $store->valid_time,
                        'name' => $_full_name,
                        'bank_details' => $_bank_details,
                        'only_bank' => $_only_bank,
                        'sharedData' => $sharedDataDisplay,
                        'userCocDetail' => $userCocDetail,
                        'userWkfrDetail' => $userWkfrDetail,
                    ];
                    dispatch(new SendEmailShareDocs($mail_data));
                    $store->share_history_id = $shareHistoryId;
                    $store->data_display = json_encode($sharedDataDisplay);;
                    $store->save();
                }
                return response()->json(['msg' => 'E-mail Sent',
                    'status' => 200,
                ], 200);
            } catch (Exception $e) {
                dd($e);
            }
        }
        return response()->json(['msg' => 'Something went Wrong',
            'status' => 400,
        ], 200);
    }

    public function viaEmailOld(Request $request)
    {
        $data_arr = $request->toArray();
        dd($data_arr);
        $_bank_details = [];
        $_only_bank = false;
        $_is_parent = empty($data_arr['shareable_docs_parent']);
        $_is_children = empty($data_arr['shareable_docs_children']);

        $user = Auth::user();
//        return $data_arr;

        if ((!$_is_parent || !$_is_children || isset($data_arr['resume'])) && isset($data_arr['email'])) {
            try {
                if (!$_is_children && !$_is_parent) {
                    $_filtered_parents = array_keys(($data_arr['shareable_docs_children']));
                    $data_arr['shareable_docs_parent'] = array_filter($data_arr['shareable_docs_parent'], function ($value) use ($_filtered_parents) {
                        if (!in_array($value, $_filtered_parents)) {
                            return true;
                        }
                    }); // remove child name from parent docs
                }

                $store = new SharedDocsToOthers();
                $docs = [];
                if (!$_is_parent) {
                    $docs['parent'] = $data_arr['shareable_docs_parent'];
                }

                if (!$_is_children) {
                    $docs['child'] = $data_arr['shareable_docs_children'];
                }

                $store->user_id = $user->id;
                $store->token = str_random(25);
                $store->valid_time = time();
                $store->data = json_encode($docs);
                $store->receiver_email = $data_arr['email'];

                $shareContactId = 0;
                $this->shareContactRepository = new ShareContactsRepository();
                $shareContactexist = $this->shareContactRepository->checkShareContactExist($data_arr['email'], $user->id);
                if (!empty($shareContactexist[0])) {
                    $shareContactId = $shareContactexist[0]['id'];
                } else {
                    $shareContactData = array();
                    $shareContactData['user_id'] = $user->id;
                    $shareContactData['email'] = $data_arr['email'];
                    $shareContactData['type'] = 1;
                    $this->shareContactRepository = new ShareContactsRepository();
                    $saveContact = $this->shareContactRepository->store($shareContactData);
                    $shareContactId = $saveContact->id;
                }

                $shareHistory = new ShareHistory();
                $shareHistory->roll_type = 3;
                $shareHistory->user_id = $user->id;
                $shareHistory->is_allow_qr = 0;
                $shareHistory->contact_id = $shareContactId;
                $shareHistory->email = $data_arr['email'];
                $shareHistory->save();
                $shareHistoryId = $shareHistory->id;

                if (!$_is_parent && isset($data_arr['shareable_docs_parent'][0]) && ($data_arr['shareable_docs_parent'][0] == 'bank_details')) {
                    $_only_bank = true;
                }

                if (!$_is_parent && in_array('bank_details', $data_arr['shareable_docs_parent'])) {
                    $__user_details = UserPersonalDetail::where('user_id', $user->id)->first();
                    $_bank_details['bank_name'] = $__user_details['bank_name'];
                    $_bank_details['account_no'] = $__user_details['account_no'];
                    $_bank_details['ifsc_code'] = $__user_details['ifsc_code'];
                    $_bank_details['branch_address'] = $__user_details['branch_address'];
                    $_bank_details['aadhaar'] = $__user_details['aadhaar'];
                    $_bank_details['pancard'] = $__user_details['pancard'];
                }

                $_full_name = $user->first_name . ' ' . $user->last_name;
                $data = json_decode($store->data);
                $parent = !empty($data->parent) ? $data->parent : [];
                $child = !empty($data->child) ? $data->child : null;
                $sharedData['parent'] = (array) $parent;
                // dd($child);
                if ($child) {
                    foreach ($child as $key => $row) {
                        if ($key == 'courses') {
                            foreach ($child->courses as $value) {
                                $courses = \App\Courses::where('id', $value)->value('course_name');
                                $sharedData['parent'][] = 'Course' . ' - ' . $courses;
                            }
                        } elseif ($key == 'sea_service') {
                            foreach ($row as $value) {
                                // dump(strlen($value));
                                if (strlen($value) == 10) {
                                    $seaData = $value . ' To Still On Board';
                                } else {
                                    $seaData = substr_replace($value, " / ", 10, -strlen($value));
                                }
                                $sharedData['parent'][] = 'Service' . ' - ' . $seaData;
                            }
                        } elseif ($key == 'dce') {
                            foreach ($row as $value) {
                                if ($value == 'all') {
                                    $data = 'Oil + Chemical + Liquefied Gas';
                                } else {
                                    $data = ucwords(strtolower(str_replace('_', ' ', $value)));
                                }
                                $sharedData['parent'][] = $data;
                            }
                        } else {
                            $sharedData['parent'][] = $key;
                        }
                    }
                }
                // dd('rrr');
                $sharedData = array_filter($sharedData);
                $sharedData = !empty($sharedData['parent']) ? $sharedData['parent'] : [];
                $userCocDetail = \App\UserCocDetail::whereUserId($user->id)->first();
                $userWkfrDetail = \App\UserWkfrDetail::whereUserId($user->id)->first();
                $mail_data = [
                    'email' => $data_arr['email'],
                    'subject' => '' . $_full_name . ' has shared Documents with you.',
                    'token' => $store->token,
                    'valid_time' => $store->valid_time,
                    'name' => $_full_name,
                    'bank_details' => $_bank_details,
                    'only_bank' => $_only_bank,
                    'sharedData' => $sharedData,
                    'userCocDetail' => $userCocDetail,
                    'userWkfrDetail' => $userWkfrDetail,
                ];
                // $mail_data = [
                //     'email' => $data_arr['email'],
                //     'subject' => $_full_name . ' share docs with you',
                //     'token' => $store->token,
                //     'valid_time' => $store->valid_time,
                //     'name' => $_full_name,
                //     'bank_details' => $_bank_details,
                //     'only_bank' => $_only_bank
                // ];
                dispatch(new SendEmailShareDocs($mail_data));
                $store->share_history_id = $shareHistoryId;
                $store->save();
                return response()->json(['msg' => 'E-mail Sent',
                    'status' => 200,
                ], 200);
            } catch (Exception $e) {
                dd($e);
            }
        }
        return response()->json(['msg' => 'Something went Wrong',
            'status' => 400,
        ], 200);
    }
    /*
     * Share Reseume
     * */
    public function shareResume(Request $request)
    {
        $user = Auth::user();
        $data_arr = $request->toArray();
        $checkSeaService = false;
        $checkPassportCountry = false;
        $currentRank = 0;
        $userSeaServices = User::whereId($user->id)->with(['sea_service_detail', 'professional_detail', 'passport_detail'])->first();
        if(isset($userSeaServices['professional_detail']['current_rank'])){
            $currentRank = $userSeaServices['professional_detail']['current_rank'];
        }
        if(isset($userSeaServices['sea_service_detail'][0]) && !empty($userSeaServices['sea_service_detail'][0])){
            $checkSeaService = true;
        }else{
            $RequiredSeaServiceRanks = CommonHelper::getRankOfRequiredSeaServiceForResume();
            if(in_array($currentRank, $RequiredSeaServiceRanks)){
                return response()->json(['msg' => 'Resume not created.',
                            'status' => 400,
                                ], 200);
            } else {
                $checkSeaService = true;
            }
        }
        if(isset($userSeaServices['passport_detail']['pass_country']) && !empty($userSeaServices['passport_detail']['pass_country'])){
            $checkPassportCountry = true;
        }else{
            $RequiredPassportCountryRanks = CommonHelper::getRankOfRequiredPassportCountryForResume();
            if(in_array($currentRank, $RequiredPassportCountryRanks)){
                return response()->json(['msg' => 'Resume not created.',
                            'status' => 400,
                                ], 200);
            } else {
                $checkPassportCountry = true;
            }
        }
        // dd(isset($data_arr['email']));
        if (isset($data_arr['email']) && $checkSeaService == true && $checkPassportCountry == true) {

            try {

                if (!empty($data_arr['email'])) {
                    foreach ($data_arr['email'] as $contactId) {

                        $pattern = '/[a-z0-9_\-\+\.]+@[a-z0-9\-]+\.([a-z]{2,4})(?:\.[a-z]{2})?/i';
                        preg_match_all("/[\._a-zA-Z0-9-]+@[\._a-zA-Z0-9-]+/i", $contactId, $matches);

                        if (isset($matches[0][0])) {
                            $contactId = $matches[0][0];
                            $contactData = ShareContacts::where('email', $contactId)->where('user_id', $user->id)->first();

                            if (empty($contactData)) {

                                $earray = explode("@", $contactId);
                                $scontact = new ShareContacts;
                                $scontact->user_id = $user->id;
                                $scontact->email = $contactId;
                                $scontact->p_name = $earray[0];
                                $scontact->group = '1';
                                $scontact->save();
                                $contactData = ShareContacts::where('id', $scontact->id)->first();
                            }

                            $store = new SharedDocsToOthers();
                            $docs = [];

                            $store->user_id = $user->id;
                            $store->token = str_random(25);
                            $store->valid_time = time();
                            $store->data = 'share_profile';
                            $store->receiver_email = $contactData['email'];
                            $userCocDetail = \App\UserCocDetail::whereUserId($user->id)->first();
                            $userWkfrDetail = \App\UserWkfrDetail::whereUserId($user->id)->first();
                            $_full_name = $user->first_name . ' ' . $user->last_name;

                            $shareHistory = new ShareHistory();
                            $shareHistory->roll_type = 1;
                            $shareHistory->user_id = $user->id;
                            $shareHistory->is_allow_qr = $data_arr['is_allow_qr'];
                            $shareHistory->contact_id = $contactData['id'];
                            $shareHistory->email = $contactData['email'];
                            $shareHistory->save();
                            $shareHistoryId = $shareHistory->id;

                            $shareHistoryIdKey = CommonHelper::encodeKey($shareHistoryId);
                            $mail_data = [
                                'email' => $contactData['email'],
                                'subject' => $_full_name . ' has shared Resume with you',
                                'name' => $_full_name,
                                'token' => $store->token,
                                'valid_time' => $store->valid_time,
                                'userCocDetail' => $userCocDetail,
                                'userWkfrDetail' => $userWkfrDetail,
                                'resume_link' => 'share-pdf?resume=' . $shareHistoryIdKey,
                            ];

                            Mail::to($contactData['email'])->send(new ShareResume($mail_data));

                            $store->save();

                        }

                    }
                }
                return response()->json(['msg' => 'E-mail Sent',
                    'status' => 200,
                ], 200);
            } catch (Exception $e) {
                dd($e);
            }
        }
        return response()->json(['msg' => 'Something went Wrong',
            'status' => 400,
        ], 200);
    }

    public function shareProfile(Request $request)
    {
        $user = Auth::user();
//        if(empty($user['PPMname'])){
//            return response()->json(['msg' => 'Profile not created.',
//                        'status' => 400,
//                            ], 200);
//        }
        $data_arr = $request->toArray();
        $to_emails = [];
        if ($data_arr['email']) {
            foreach ($data_arr['email'] as $row) {
                array_push($to_emails, str_replace("(", "", str_replace(")", "", substr($row, strpos($row, '(')))));
            }
        }
        if (count($data_arr['email']) > 0) {
            try {
                foreach ($to_emails as $email) {
                    $store = new SharedDocsToOthers();
                    $store->user_id = $user->id;
                    $store->token = str_random(25);
                    $store->valid_time = time();
                    $store->data = 'share_profile';
                    $store->receiver_email = $email;
                    $userCocDetail = \App\UserCocDetail::whereUserId($user->id)->first();
                    $userWkfrDetail = \App\UserWkfrDetail::whereUserId($user->id)->first();
                    $_full_name = $user->first_name . ' ' . $user->last_name;
                    $mail_data = [
                        'email' => $email,
                        'subject' => $_full_name . ' has shared Profile',
                        'name' => $_full_name,
                        'token' => $store->token,
                        'valid_time' => $store->valid_time,
                        'userCocDetail' => $userCocDetail,
                        'userWkfrDetail' => $userWkfrDetail,
                    ];
                    Mail::to($email)->send(new ShareProfile($mail_data));
                    $store->save();
                }
                return response()->json(['msg' => 'E-mail Sent',
                    'status' => 200,
                ], 200);
            } catch (Exception $e) {
            }
        }
        return response()->json(['msg' => 'Something went Wrong',
            'status' => 400,
        ], 200);
    }

    public function viewDocument()
    {
        $pageTitle = "Documents – Seafarer Sailing and Course Certificates | Flanknot";
        $metaDescription = "With Flanknot, Seafarer User’s Sailing Documents, Traveling Documents, Course Certificates and Personal Documents, can be organized in a systematic manner.";
        $metaKeywords = "sailing documents, traveling documents, Passport, Visa, Indos, CDC, Continuous discharge Certificate, Seaman book information, WK, Watchkeeping Certificate, COP, Certificate of Proficiency, COC, Certificate of Competency, COE, Certificate of Endorsement, DCE, Dangerous Cargo Endorsement, GMDSS, Global Maritime Distress and Safety System, FRAMO, DP, Dynamic positioning, Sea Service, Course, Medical, Vaccination, Character Certificate, PAN, Aadhar, AOA, Article of Agreement";
        $user_contract = $this->getData(Auth::id());
        return view('admin.manage_documents.view_docs', compact('user_contract', 'pageTitle', 'metaDescription', 'metaKeywords'));
    }

    public function shareDocument(Request $request)
    {
        $pageTitle = "Documents – Seafarer Sailing and Course Certificates | Flanknot";
        $metaDescription = "At Flanknot, view Seafarer User’s Sailing Documents, Traveling Documents, Course Certificates and Personal Documents.
            Send Request to user for link to share documents.";
        $metaKeywords = "sailing documents, traveling documents, Passport, Visa, Indos,CDC, Continuous discharge Certificate, Seaman book information, WK, Watchkeeping Certificate, COP, Certificate of Proficiency,COC, Certificate of Competency, COE, Certificate of Endorsement,DCE, Dangerous Cargo Endorsement, GMDSS, Global Maritime Distress and Safety System, FRAMO, DP, Dynamic positioning, Sea Service, Course, Medical, Vaccination, Character Certificate, PAN, Aadhar, AOA, Article of Agreement";
        $user_id = \CommonHelper::decodeKey($request['documents']);
        if (!isset($user_id)) {
            $user_id = '';
        }
        $user_contract = $this->getData($user_id);
        return view('admin.manage_documents.view_docs', compact('user_contract', 'user_id', 'pageTitle', 'metaDescription', 'metaKeywords'));
    }

    /**
     * Shared Docs with someone
     */
    public function getSharedData(Request $request, $token)
    {
        $shared_docs = SharedDocsToOthers::whereToken($token)->firstOrFail();

        // dd(date("Y-m-d H:i:s", ($shared_docs->valid_time + (3600 * 18))), date("Y-m-d H:i:s", time()));
        $is_request_sent = "false";
        if (($shared_docs->valid_time + (3600 * 48)) < time()) {

            $History = HistoryLog::where("share_history_id", $shared_docs->share_history_id)->where("description", "This Document link has expired")->first();
            if (!$History) {
                $HistoryLog = new HistoryLog();
                $HistoryLog->share_history_id = $shared_docs->share_history_id;
                $HistoryLog->description = "This Document link has expired";
                $HistoryLog->ip = '';
                $HistoryLog->created_at = date('Y-m-d H:i:s');
                $HistoryLog->save();
            }

            if ($request->isMethod('post')) {
                $HistoryLog = new HistoryLog();
                $HistoryLog->share_history_id = $shared_docs->share_history_id;
                $HistoryLog->description = "Requested for a New Link (Pending)";
                $HistoryLog->ip = '';
                $HistoryLog->created_at = date('Y-m-d H:i:s');
                $HistoryLog->save();
                $is_request_sent = "true";
            }

//            $shared_docs->delete();
            return abort(410, 'The link to the Document you are trying to access has Expired###' . $token . "###" . $is_request_sent);
        }
        if ($shared_docs->share_history_id) {
            $existTodayHistory = HistoryLog::where("share_history_id", $shared_docs->share_history_id)->whereDate('created_at', '=', date('Y-m-d'))->first();
            if(empty($existTodayHistory)){
                $HistoryLog = new HistoryLog();
                $HistoryLog->share_history_id = $shared_docs->share_history_id;
                $HistoryLog->description = "Documents Viewed";
                $HistoryLog->ip = '';
                $HistoryLog->created_at = date('Y-m-d H:i:s');
                $HistoryLog->save();
            }
        }

        $user_id = $shared_docs['user_id'];

        $visitor_details = new DocumentsToOtherDetails();
        $visitor_details['shared_docs_to_other_id'] = $shared_docs['id'];
        $visitor_details['ip'] = file_get_contents('https://api.ipify.org');
        $visitor_details['visiting_time'] = Carbon::now()->toDateTimeString();
        $visitor_details->save();

        $data_to_share = (array) json_decode($shared_docs['data']);
        $_child_docs = [];
        $_parent_docs = [];
        if (!empty($data_to_share['parent'])) {
            $_parent_docs = (array) (($data_to_share['parent']));
        }

        if (!empty($data_to_share['child'])) {
            $_child_docs = (array) (($data_to_share['child']));
        }

        $user_all_docs = $this->getData($user_id);
        unset($user_all_docs['is_docs']);
// dump($_child_docs);
        // dump($_child_docs);
        // dd($user_all_docs);
        $only_shared_docs = array_filter($user_all_docs, function ($_values) use ($_parent_docs, $_child_docs) {
            // dump($_values);
            // dd('rer');
            if (in_array($_values['id'], array_merge(array_keys($_child_docs), array_values($_parent_docs)))) {
                return true;
            }

//                if(in_array($_values['id'],$_parent_docs)) return true;
            //                if(in_array($_values['id'],array_keys($_child_docs))) return true; //will be merge
        }); // remove children from docs, only Parented docs

//        $_only_child_docs=array_filter($user_all_docs, function ($_values) use ($_child_docs) { //will be del
        //            if(in_array($_values['id'],array_keys($_child_docs))) return true;
        //        }); // remove parent and unselected from docs, only Selected children docs

//        foreach ($_only_child_docs as $_type => $_values)    {
        //                    foreach ($_values as $__type_id => $__docs) {
        //                        if($__type_id!='id'){
        //                            if (!array_key_exists($__type_id,array_flip($_child_docs[$_type]))) {
        //                                unset($_only_child_docs[$_type][$__type_id]);
        //                            }
        //                        }
        //                    }
        //        }
        foreach ($only_shared_docs as $_type => $_values) {
            foreach ($_values as $__type_id => $__docs) {
                if ($__type_id != 'id') {
                    if (!array_key_exists($__type_id, array_flip($_child_docs[$_type]))) {
                        unset($only_shared_docs[$_type][$__type_id]);
                    }
                }
            }
        }
        return view('admin.manage_documents.shared_docs.index', compact('only_shared_docs', 'user_id', 'token'));
    }

    public function getUserJobHistory(Request $request)
    {

        if ($request->method() == 'GET') {

            $authDetail = Auth::user();

            $sql = \App\SendedJob::with(['courseJob', 'courseJob.company', 'user', 'shareData'])
                ->where('user_id', $authDetail->id)
                ->has('courseJob')
                ->has('user')
                ->has('courseJob.company')
                ->where('applied', 1);

            return Datatables::of($sql)

                ->editColumn('courseJob.title', function ($data) {

                    return $data->courseJob->title;

                })
                ->editColumn('shareData.latest_qr', function ($data) {

                    return !empty($data->shareData->latest_qr) ? date('d M Y', strtotime($data->shareData->latest_qr)) : '-';

                })

                ->editColumn('resume_view', function ($data) {

                    return '';

                })
                ->editColumn('applied_date', function ($data) {

                    return ($data->applied_date != null) ? date('d M y', strtotime($data->applied_date)) : '-';

                })
                ->editColumn('created_at', function ($data) {

                    return date('d M y', strtotime($data->created_at));

                })
                ->editColumn('courseJob.company_id', function ($data) {

                    return ($data->courseJob->company != null) ? $data->courseJob->company->name : '-';

                })
                ->make(true);
        }

    }


    public function getUserJobHistoryList(Request $request)
    {
        $pageTitle = "History & log - of Job Application | Flanknot";
        $metaDescription = "At Flanknot, Seafarer User can view the history and log of jobs the user has applied for.
            The log details include the job name, date and time or application. 
            A Quick Response also noted as QR that is in response to the job application.";
        $metaKeywords = "log, history, Job, application, share, email, link, shipping company, Quick Response, QR";
        $authDetail = \Auth::user();
         
        $sql = \App\SendedJob::with(['courseJob','courseJob.company','user','shareData'])
                       ->where('user_id',$authDetail->id)
                       ->where('applied',1)
                       ->get();
        return view('admin.manage_documents.job_history', compact('sql', 'pageTitle', 'metaDescription', 'metaKeywords'));
    }
}
