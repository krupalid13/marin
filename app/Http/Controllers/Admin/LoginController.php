<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\UserService;
use App\Services\SendEmailService;
use Auth;
use function MongoDB\BSON\toJSON;
use Route;
use redirect;

class LoginController extends Controller
{
    private $userService;
    private $sendEmailService;

    function __construct(){
        $this->userService = new UserService();
        $this->sendEmailService = new SendEmailService();
    }

    public function login(){

        return view('admin.login.admin_login');
    }

    public function checkLogin(Request $request){
        
        $login_data = $request->toArray();
        $login_data['type'] = 'admin';
        $login_data['email'] = $login_data['username'];

        $login = $this->userService->checkLogin($login_data);

        if($login['status'] == 'success'){
            return redirect()->route('admin.view.seafarer');
        }else{
            return redirect()->route('admin.login')->with('error','Wrong username or password');
        }

    }

    public function resetPassword(Request $request){

        $data = $request->toArray();
        $admin_data = $this->userService->getAdminDataByEmail($data)->toArray();

        if(count($admin_data) > 0){
            $admin_data = $admin_data[0];
            $this->sendEmailService->sendResetPasswordEmail($admin_data);
            return response()->json(['status' => 'success' , 'message' => 'We have sent you email on your mentioned email.'],200);
        }else{
            return response()->json(['status' => 'failure' , 'message' => 'Invalid email id provided.'],400);
        }
    }

    public function setNewPassword(Request $request,$email){
        $data['email'] =  base64_decode($email);
        $admin_data = $this->userService->getAdminDataByEmail($data)->toArray();
        if(count($admin_data) > 0){
            return view('admin.login.change_password',['email' => $email]);
        }
    }

    public function changePassword(Request $request){
        $data = $request->toArray();
        $data['email'] =  base64_decode($data['email']);
        $admin_data = $this->userService->getAdminDataByEmail($data)->toArray();

        if(count($admin_data) > 0){
            $data_change['password'] = bcrypt($data['password']);
            $result = $this->userService->updateUser($data_change,$admin_data[0]['id']);

            if($result){
                return response()->json(['status' => 'success' , 'message' => 'Your password has been change successfully.' , 'redirect_url' => route('admin.login')],200);
            }else{
                return response()->json(['status' => 'failure' , 'message' => 'Error while changing password.'],400);
            }

        }else{
            return response()->json(['status' => 'failure' , 'message' => 'Invalid email id provided.'],400);
        }
    }
    
    public function logout(){
        Auth::logout();
        return redirect()->route('admin.login');
    }
}
