<?php
namespace App\Http\Controllers\Admin;

use App\ShareContacts;
//use Mail;
use Carbon\Carbon;
use CommonHelper;
use Exception;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use ZanySoft\Zip\Zip;

class ShareContactsController extends Controller
{

    protected function getData($user_id)
    {
        $order = ['Photo', 'Passport', 'US_Visa', 'Indos', 'CDC', 'sea_service', 'COC', 'COE', 'DCE', 'gmdss', 'wk_cop', 'Courses', 'ILO_medical', 'YELLOW_FEVER', 'SCHOOL_QUALIFICATION', 'INSTITUTE_DEGREE', 'CHARACTER_REFERENCE', 'Aadhaar', 'pancard', 'passbook'];
        $user_contract = User::whereId($user_id)->with('user_documents_type.user_documents')->get()->map(function ($q) {
//            $_container = $q->user_documents_type;
//            return $q->user_documents_type;
            $_container = $q->user_documents_type->reject(function ($iq, $key) {
                return (count($iq->user_documents) > 0 ? false : true);
            });
//            return $_container;
            $_docs_maintainer = [];
            $_docs_maintainer['is_docs'] = false;
            foreach ($_container as $_is_docs_added) {
                if (count($_is_docs_added['user_documents'])) {
                    $_docs_maintainer['is_docs'] = true;
                    break;
                }
            }
            if (!$_docs_maintainer['is_docs']) {
                return $_docs_maintainer;
            }

            $_for_coc = $q->coc_detail;
            $_for_passport = $q->passport_detail;
            $_for_cdc = $q->seaman_book_detail;
            $_for_coe = $q->coe_detail;
            $_for_sea_service = ($q->sea_service_detail)->toArray();
            $_for_courses = $q->course_detail;
            $_for_gmdss = $q->gmdss_detail;
            $_professional_details = $q->professional_detail;
            $_wk = $q->wkfr_detail;
            foreach ($_container as $docs) {
                if ($docs['type'] == 'coc') {
                    foreach ($_for_coc as $_coc_unset => $coc) {
                        if ($coc['coc_number'] == $docs['type_id']) {
                            $_docs_maintainer[$docs['type']][$docs['type_id']]['heading'] = $coc['coc_grade'] . ' ' .
                                CommonHelper::countries()[$coc['coc']];
//                            $_docs_maintainer[$docs['type']][$docs['type_id']]['coc']= $coc['coc'];
//                            $_docs_maintainer[$docs['type']][$docs['type_id']]['coc_expiry_date']= $coc['coc_expiry_date'];
//                            $_docs_maintainer[$docs['type']][$docs['type_id']]['coc_grade']= $coc['coc_grade'];

                            unset($_for_coc[$_coc_unset]);
                        }
                    }
                } else
                    if ($docs['type'] == 'passport') {
                        $_docs_maintainer[$docs['type']][$docs['type_id']]['heading'] = CommonHelper::countries()[$_for_passport['pass_country']];

                        unset($_for_passport);
                    } else
                        if ($docs['type'] == 'cdc') {
                            foreach ($_for_cdc as $_cdc_unset => $cdc) {
                                if ($docs['type_id'] == $cdc['cdc_issue_date'] . $cdc['cdc_expiry_date']) {
                                    $_docs_maintainer[$docs['type']][$docs['type_id']]['heading'] = CommonHelper::countries()[$cdc['cdc']];
//                            $_docs_maintainer[$docs['type']][$docs['type_id']]['cdc']=$cdc['cdc'];
//                            $_docs_maintainer[$docs['type']][$docs['type_id']]['cdc_number']=$cdc['cdc_number'];
//                            $_docs_maintainer[$docs['type']][$docs['type_id']]['cdc_issue_date']=$cdc['cdc_issue_date'];
//                            $_docs_maintainer[$docs['type']][$docs['type_id']]['cdc_expiry_date']=$cdc['cdc_expiry_date'];

                                    unset($_for_cdc[$_cdc_unset]);
                                }
                            }
                        } else
                            if ($docs['type'] == 'coe') {
                                foreach ($_for_coe as $_coe_unset => $coe) {
                                    if ($docs['type_id'] == $coe['coe_number'] . $coe['coe_grade']) {
                                        $_docs_maintainer[$docs['type']][$docs['type_id']]['heading'] = (isset($coe['coe_grade']) ? $coe['coe_grade'] : '') .
                                            ' ' . ((isset($coe['coe']) &&
                                                $coe['coe'] != '') ? CommonHelper::countries()[$coe['coe']] : '');
//                            $_docs_maintainer[$docs['type']][$docs['type_id']]['coe']=$coe['coe'];
//                            $_docs_maintainer[$docs['type']][$docs['type_id']]['coe_number']=$coe['coe_number'];
//                            $_docs_maintainer[$docs['type']][$docs['type_id']]['coe_expiry_date']=$coe['coe_expiry_date'];
//                            $_docs_maintainer[$docs['type']][$docs['type_id']]['coe_grade']=$coe['coe_grade'];

                                        unset($_for_coe[$_coe_unset]);
                                    }
                                }
                            } else
                                if ($docs['type'] == 'sea_service') {
                                    foreach ($_for_sea_service as $_sea_service_unset => $sea_service) {
                                        if ($docs['type_id'] == $sea_service['from'] . $sea_service['to']) {
                                            $toDate = isset($sea_service['to']) && !empty($sea_service['to']) ? date('d-m-Y', strtotime($sea_service['to'])) : 'On Board';
                                            $_docs_maintainer[$docs['type']][$docs['type_id']]['heading'] = date('d-m-Y', strtotime($sea_service['from'])) .
                                                ' / ' . $toDate;
                                            $_docs_maintainer[$docs['type']][$docs['type_id']]['sdd'] = ($sea_service['from']);

//                            $_docs_maintainer[$docs['type']][$docs['type_id']]['from']=$sea_service['from'];
//                            $_docs_maintainer[$docs['type']][$docs['type_id']]['to']=$sea_service['to'];

                                            unset($_for_sea_service[$_sea_service_unset]);
                                        }
                                    }
                                } else
                                    if ($docs['type'] == 'courses') {
                                        foreach ($_for_courses as $_courses_unset => $courses) {
                                            if ($docs['type_id'] == $courses['course_id']) {
                                                $arr = CommonHelper::course_name_by_course_type($courses['course_type']);
                                                //will understand later
                                                foreach ($arr as $val) {
                                                    if ($courses['course_id'] == $val->id) {
                                                        $_docs_maintainer[$docs['type']][$docs['type_id']]['heading'] = $val->course_name;
                                                    }
                                                }

                                                unset($_for_courses[$_courses_unset]);
                                            }
                                        }
                                    } else
                                        if ($docs['type'] == 'gmdss') {
//                                            if (isset($_for_gmdss['gmdss']) && $_for_gmdss['gmdss'] != 0){
                                                $_docs_maintainer[$docs['type']][$docs['type_id']]['heading'] = $_for_gmdss['gmdss'];
//                                                $_docs_maintainer[$docs['type']][$docs['type_id']]['heading'] = CommonHelper::countries()[$_for_gmdss['gmdss']];
//                                            }
//                                            $_docs_maintainer[$docs['type']][$docs['type_id']]['heading'] ="GMDSS";
                                        } else
                                            if ($docs['type'] == 'character_certificate') {
                                                $_docs_maintainer[$docs['type']][$docs['type_id']]['heading'] = 'Character Certificate';
                                            } else
                                                if ($docs['type'] == 'institute_degree') {
                                                    $_docs_maintainer[$docs['type']][$docs['type_id']]['heading'] = $_professional_details['institute_degree'];
                                                } else
                                                    if ($docs['type'] == 'school_qualification') {
                                                        $_docs_maintainer[$docs['type']][$docs['type_id']]['heading'] = $_professional_details['school_qualification'];
                                                    } else
                                                        if ($docs['type'] == 'passbook') {
                                                            $_docs_maintainer[$docs['type']][$docs['type_id']]['heading'] = 'Passbook';
                                                        } else
                                                            if ($docs['type'] == 'yellow_fever') {
                                                                $_docs_maintainer[$docs['type']][$docs['type_id']]['heading'] = 'Yellow Fever';
                                                            } else
                                                                if ($docs['type'] == 'ilo_medical') {
                                                                    $_docs_maintainer[$docs['type']][$docs['type_id']]['heading'] = 'ILO Medical';
                                                                } else
                                                                    if ($docs['type'] == 'wk_cop') {
                                                                        $_docs_maintainer[$docs['type']][$docs['type_id']]['heading'] = CommonHelper::countries()[$_wk['watch_keeping']];
                                                                    } else
                                                                        if ($docs['type'] == 'indos' ||
                                                                            $docs['type'] == 'aadhaar' ||
                                                                            $docs['type'] == 'pancard' ||
                                                                            $docs['type'] == 'photo') {
                                                                            $_docs_maintainer[$docs['type']][$docs['type_id']]['heading'] = str_replace('_', ' ', ucfirst($docs['type']));
                                                                        }
                $_docs_maintainer[$docs['type']][$docs['type_id']][] = $docs;
                $_docs_maintainer[$docs['type']]['id'] = $docs['type'];
            }
            return $_docs_maintainer;
        })->first();
        $order_tolower = array_map('strtolower', $order);

//        return $order_tolower;

        uksort($user_contract, function ($user1, $user2) use ($order_tolower) {
            return (array_search($user1, $order_tolower) > array_search($user2, $order_tolower));
        });
        return $user_contract;
    }

    public function contactsIndex()
    {
        $pageTitle = "Contact List - Listing of contacts, shared or created by seafarer User | Flanknot";
        $metaDescription = "At Flanknot, Seafarer User can create and maintain a list of contacts.
            The Contact list can be used by the Seafarer User for sharing the link to Resume, Documents or Profile.";
        $metaKeywords = "email id, mobile number, contact name, company name, person name";
        $where = [];
        $where['user_id'] = Auth::id();
        $contractList = ShareContacts::where($where)->get();
        return view('admin.manage_documents.share_contacts', compact('pageTitle', 'metaDescription', 'metaKeywords'));
    }

    
}
