<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use function MongoDB\BSON\toJSON;
use Auth;
use App\SeamanBookEnquiry;
use App\ContactUs;

class DashboardController extends Controller
{

    function __construct(){
    }

    public function dashboard() {
        $user_data = Auth::user()->toArray();
        return view('admin.seafarer.list_seafarer',['user_data' => $user_data]);
    }

    public function addCompany(){
    	return view('admin.company.add_company');
    }

    public function addAdvertiser(){
        return view('admin.advertiser.add_advertiser');
    }

    public function seamanBookEnquiryList(Request $request){
        
        $paginate = env('PAGINATE_LIMIT',10);
        $enquiry = SeamanBookEnquiry::orderBy('id','desc')->paginate($paginate);
        
        return view('admin.enquiry.seaman_book_enquiry',['data' => $enquiry]);
    }

    public function contactUsEnquiryList(Request $request){
        
        $paginate = env('PAGINATE_LIMIT',10);
        $enquiry = ContactUs::where('name','!=','')->where('email','!=','')->orderBy('id','desc')->paginate($paginate);
        
        return view('admin.enquiry.contact_us',['data' => $enquiry]);
    }

}
