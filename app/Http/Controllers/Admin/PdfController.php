<?php

namespace App\Http\Controllers\Admin;

use App\Services\DocumentService;
use App\Services\OrderService;
use App\Services\UserProfessionalDetailsService;
use App\Services\UserService;
use App\User;
use App\ShareHistory;
use App\HistoryLog;
use Carbon\Carbon;
use CommonHelper;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use PDF;
use App\Http\Controllers\Admin\GraphController;
use App\Mail\ReceivedQr;
use Mail;

class PdfController extends Controller {
    private $GraphController;
    public function __construct() {
        $this->userService = new UserService();
        $this->orderService = new OrderService();
        $this->documentService = new DocumentService();
        $this->userProfessionalDetailsService = new UserProfessionalDetailsService();
        $this->GraphController = new GraphController();
    }


    public function pdfview(Request $request, $is_download = true) {
        $pageTitle = "Seafarer Resume – Engineers, Deck Officers, Ratings | Flanknot";
        $metaDescription = "Seafarer User can create and download their dynamic Resumes.";
        $metaKeywords = "Rank, Master, Captain, Deck Officers, Engineers, Passport, Visa, Indos, CDC, Continuous discharge Certificate, Seaman book information, WK, Watchkeeping Certificate, COP, Certificate of Proficiency, COC, Certificate of Competency, COE, Certificate of Endorsement, DCE, Dangerous Cargo Endorsement, GMDSS, Global Maritime Distress and Safety System, FRAMO, DP, Dynamic positioning, Sea Service, AOA, Article of Agreement, Service Contract, Course, Medical, Vaccination, Character Certificate, PAN, Aadhar";
        $user_id = null;
        $user = '';
        $role = '';
        $isResume = false;
        if(isset($request['user']) && !empty($request['user'])){
            $user_id = CommonHelper::decodeKey($request['user']);
        }
        if (isset($user_id)) {
            $user = 'another_user';
        }
        if (Auth::check() && !isset($user_id)) {
            $user_id = Auth::user()->id;

        } elseif (isset($user_id) && $user_id != '') {
            # code...
        }
        
        if (Auth::check()) {
            $role = Auth::user()->registered_as;
        }
        if (isset($user_id)) {
            $currentRank = null;
            $options['with'] = ['userDangerousCargoEndorsementDetail','personal_detail', 'professional_detail','visas', 'passport_detail', 'seaman_book_detail', 'coc_detail', 'gmdss_detail', 'wkfr_detail', 'personal_detail.state', 'personal_detail.city', 'personal_detail.pincode.pincodes_states.state', 'personal_detail.pincode.pincodes_cities.city', 'sea_service_detail', 'course_detail', 'cop_detail', 'coe_detail', 'document_permissions.requester.company_registration_detail', 'document_permissions.requester.institute_registration_detail', 'document_permissions.document_type.type'];
            $result = $this->userService->getDataByUserId($user_id, $options)->toArray();
            if (isset($result[0]['professional_detail']['current_rank_exp'])) {
                $rank_exp = explode(".", $result[0]['professional_detail']['current_rank_exp']);
                $result[0]['professional_detail']['years'] = $rank_exp[0];
                $result[0]['professional_detail']['months'] = ltrim($rank_exp[1], 0);
            }
            $show_resume = '';
            if(isset($result[0]['professional_detail']['current_rank'])){
                $currentRank = $result[0]['professional_detail']['current_rank'];
                $show_resume = CommonHelper::getResumeNumber($currentRank);
            }
            $courses = $this->userService->getAllCourses();
            
            // $profilePhoto = UserDocumentsType::where("type","photo")->where("user_id",$user_id)->first();
            // dd($profilePhoto);
            $user_documents = $this->userService->getUserDocuments($user_id);
            $uploaded_documents = [];

            if (isset($user_documents['photo']) && !empty($user_documents['photo'])) {
                if(count($user_documents['photo']) > 0 && count($user_documents['photo'][0]['user_documents']) > 0) {
                    $result[0]['photo'] = $user_documents['photo'][0]['user_documents'][0];
                }
                
            }
            if (isset($result[0]['professional_detail']['current_rank']) &&
                !empty($result[0]['professional_detail']['current_rank'])) {
                $required_documents = \CommonHelper::rank_required_fields()[$result[0]['professional_detail']['current_rank']];
            }

            //if(isset($required_documents) && !empty($required_documents)){

            //Passport visa block
            if (isset($result[0]['passport_detail']) && !empty($result[0]['passport_detail'])) {
                $uploaded_documents['passport'] = $result[0]['passport_detail'];
                $uploaded_documents['passport']['title'] = 'Passport / Visa';

                if (isset($user_documents['passport']) && !empty($user_documents['passport'])) {
                    $uploaded_documents['passport']['imp_doc'] = $user_documents['passport'];
                }
            }
            if (isset($result[0]['passport_detail']['us_visa']) && $result[0]['passport_detail']['us_visa'] == '1') {
                $uploaded_documents['visa'] = $result[0]['passport_detail'];
                if (isset($result[0]['passport_detail']) && !empty($result[0]['passport_detail'])) {
                    $uploaded_documents['visa']['hide'] = 1;
                    $uploaded_documents['visa']['title'] = 'Passport / Visa';
                }

                if (isset($user_documents['visa']) && !empty($user_documents['visa'])) {
                    $uploaded_documents['visa']['imp_doc'] = $user_documents['visa'];
                }
            }

            //CDC
            if (isset($result[0]['seaman_book_detail']) && !empty($result[0]['seaman_book_detail'])) {
                $uploaded_documents['cdc'] = $result[0]['seaman_book_detail'];

                if (isset($uploaded_documents['cdc']) && !empty($uploaded_documents['cdc'])) {
                    foreach ($uploaded_documents['cdc'] as $key => $value) {
                        if (isset($user_documents['cdc'][$key + 1]['user_documents']) &&
                            !empty($user_documents['cdc'][$key + 1]['user_documents'])) {
                            $uploaded_documents['cdc'][$key]['title'] = 'CDC';

                            if (isset($user_documents['cdc'][$key + 1]) && !empty($user_documents['cdc'][$key + 1])) {
                                $uploaded_documents['cdc'][$key]['imp_doc'] = $user_documents['cdc'][$key + 1];
                            }
                        }
                    }
                }
            }

            //Coc Coe Block
            //if(in_array('COC', $required_documents)){
            $uploaded_documents['coc'] = $result[0]['coc_detail'];

            if (isset($uploaded_documents['coc']) && !empty($uploaded_documents['coc'])) {
                foreach ($uploaded_documents['coc'] as $key => $value) {
                    if (isset($user_documents['coc'][$key + 1]['user_documents']) &&
                        !empty($user_documents['coc'][$key + 1]['user_documents'])) {
                        $uploaded_documents['coc'][$key]['title'] = 'COC / COE';

                        if (isset($user_documents['coc'][$key + 1]) && !empty($user_documents['coc'][$key + 1]))
                            $uploaded_documents['coc'][$key]['imp_doc'] = $user_documents['coc'][$key + 1];
                    }
                }
            }
            //}

            //if(in_array('COE', $required_documents) || in_array('COE-Optional', $required_documents)){
            $uploaded_documents['coe'] = $result[0]['coe_detail'];

            if (in_array('COC', $required_documents)) {
                foreach ($uploaded_documents['coe'] as $key => $value) {
                    if (isset($user_documents['coe'][$key + 1]['user_documents']) &&
                        !empty($user_documents['coe'][$key + 1]['user_documents'])) {
                        $uploaded_documents['coe'][$key]['hide'] = 1;
                        $uploaded_documents['coe'][$key]['title'] = 'COC / COE';

                        if (isset($user_documents['coe'][$key + 1]) && !empty($user_documents['coe'][$key + 1]))
                            $uploaded_documents['coe'][$key]['imp_doc'] = $user_documents['coe'][$key + 1];
                    }
                }
            }

            //}

            //GMDSS watch keeping
            //if(in_array('GMDSS', $required_documents) || in_array('GMDSS-Optional', $required_documents)){
            $uploaded_documents['gmdss'] = $result[0]['gmdss_detail'];
            $uploaded_documents['gmdss']['title'] = 'GMDSS / WK';

            if (isset($user_documents['gmdss']) && !empty($user_documents['gmdss'])) {
                $uploaded_documents['gmdss']['imp_doc'] = $user_documents['gmdss'];
            }

            //}
            //if(in_array('WATCH_KEEPING-Optional', $required_documents)){
            $uploaded_documents['watch keeping'] = $result[0]['wkfr_detail'];
            $uploaded_documents['watch keeping']['title'] = 'GMDSS / WK';

            if (in_array('GMDSS', $required_documents) || in_array('GMDSS-Optional', $required_documents)) {
                $uploaded_documents['watch keeping']['hide'] = 1;

                if (isset($user_documents['watch keeping']) && !empty($user_documents['watch keeping'])) {
                    $uploaded_documents['watch keeping']['imp_doc'] = $user_documents['watch keeping'];
                }

            }
            // }

            //courses
            if (isset($result[0]['course_detail']) && !empty($result[0]['course_detail'])) {
                $uploaded_documents['courses'] = $result[0]['course_detail'];

                foreach ($uploaded_documents['courses'] as $key => $value) {
                    if (isset($user_documents['courses'][$key + 1]['user_documents']) &&
                        !empty($user_documents['courses'][$key + 1]['user_documents'])) {
                        $uploaded_documents['courses'][$key]['title'] = 'PreSea / PostSea Course Certificates';

                        if (isset($user_documents['courses'][$key + 1]) && !empty($user_documents['courses'][$key + 1]))
                            $uploaded_documents['courses'][$key]['imp_doc'] = $user_documents['courses'][$key + 1];
                    }
                }
            }

            //sea service
            if (isset($result[0]['sea_service_detail']) && !empty($result[0]['sea_service_detail'])) {
                $isResume = true;
                $uploaded_documents['sea service'] = $result[0]['sea_service_detail'];

                foreach ($result[0]['sea_service_detail'] as $key => $value) {
                    if (isset($user_documents['sea service'][$key + 1]['user_documents']) &&
                        !empty($user_documents['sea service'][$key + 1]['user_documents'])) {
                        $uploaded_documents['sea service'][$key]['title'] = 'Sea Service Letters';

                        if (isset($user_documents['sea service'][$key + 1]) &&
                            !empty($user_documents['sea service'][$key + 1])) {
                            $uploaded_documents['sea service'][$key]['imp_doc'] = $user_documents['sea service'][$key +
                            1];
                        }
                    }
                }
            }



            //yf vaccination
            if (isset($result[0]['wkfr_detail']) && $result[0]['wkfr_detail']['yellow_fever'] == '1') {
                $uploaded_documents['yellow fever'] = $result[0]['wkfr_detail'];
                if (isset($user_documents['yellow fever'][0]['user_documents']) &&
                    !empty($user_documents['yellow fever'][0]['user_documents'])) {
                    $uploaded_documents['yellow fever']['title'] = 'Medicals / YF Vaccination';

                    if (isset($user_documents['yellow fever']) && !empty($user_documents['yellow fever'])) {
                        $uploaded_documents['yellow fever']['imp_doc'] = $user_documents['yellow fever'];
                    }
                }
            }
            //}

            $count = 0;
            if (isset($result[0]['course_detail'])) {
                foreach ($result[0]['course_detail'] as $key => $value) {
                    if ($value['course_type'] == 'Value Added') {
                        $result[0]['value_added_course_detail'][$count] = $value;
                        $count++;
                        unset($result[0]['course_detail'][$key]);
                    }
                }
            }

            $user_contract = $this->GraphController->createGraphData($user_id);
// echo json_encode($user_contract);exit;
            if (count($result) > 0 &&
                ($result[0]['registered_as'] == null || $result[0]['registered_as'] != 'advertiser')) {
                $pdfFormat = false;
                if ($request->has('PDF')) {
                    $_save_path = 'public/uploads/user_documents/' . DIRECTORY_SEPARATOR . $user_id . DIRECTORY_SEPARATOR;
                    $pdfFormat = true;
                    $_decode_pdf=$show_resume;
                    $is_resume = $isResume;
                    if ($_decode_pdf == 1) {
                        return $this->pdfGenerator('admin.pdf', $pdfFormat, $result, $user, $courses, $role, $user_documents, $user_id, $uploaded_documents, $user_contract, $is_download, $is_resume);
                    } elseif ($_decode_pdf == 2) {
                        return $this->pdfGenerator('admin.pdf2', $pdfFormat, $result, $user, $courses, $role, $user_documents, $user_id, $uploaded_documents, $user_contract, $is_download, $is_resume);
                         
                    } elseif ($_decode_pdf == 3) {
                        return $this->pdfGenerator('admin.pdf3', $pdfFormat, $result, $user, $courses, $role, $user_documents, $user_id, $uploaded_documents, $user_contract, $is_download, $is_resume);
                    } else {
                        return $this->pdfGenerator('admin.pdf', $pdfFormat, $result, $user, $courses, $role, $user_documents, $user_id, $uploaded_documents, $user_contract, $is_download, $is_resume);
                    }
                }

                if ($request->has('resume')) {
                    $decoded=$show_resume;
                    
                    // echo json_encode($result); exit;
                    if ($decoded ==
                        1) return view('admin.pdf', ['pdfFormat' => $pdfFormat, 'data' => $result, 'user' => $user, 'all_courses' => $courses, 'role' => $role, 'user_documents' => $user_documents, 'user_id' => $user_id, 'documents' => $uploaded_documents, 'user_contract' => $user_contract, 'is_resume' => $isResume, 'pageTitle' => $pageTitle, 'metaDescription' => $metaDescription, 'metaKeywords' => $metaKeywords]);
                    elseif ($decoded ==
                        2) return view('admin.pdf2', ['pdfFormat' => $pdfFormat, 'data' => $result, 'user' => $user, 'all_courses' => $courses, 'role' => $role, 'user_documents' => $user_documents, 'user_id' => $user_id, 'documents' => $uploaded_documents, 'user_contract' => $user_contract, 'is_resume' => $isResume, 'pageTitle' => $pageTitle, 'metaDescription' => $metaDescription, 'metaKeywords' => $metaKeywords]);
                    elseif ($decoded ==
                        3) return view('admin.pdf3', ['pdfFormat' => $pdfFormat, 'data' => $result, 'user' => $user, 'all_courses' => $courses, 'role' => $role, 'user_documents' => $user_documents, 'user_id' => $user_id, 'documents' => $uploaded_documents, 'user_contract' => $user_contract, 'is_resume' => $isResume, 'pageTitle' => $pageTitle, 'metaDescription' => $metaDescription, 'metaKeywords' => $metaKeywords]);
                    else return view('admin.pdf', ['pdfFormat' => $pdfFormat, 'data' => $result, 'user' => $user, 'all_courses' => $courses, 'role' => $role, 'user_documents' => $user_documents, 'user_id' => $user_id, 'documents' => $uploaded_documents, 'user_contract' => $user_contract, 'is_resume' => $isResume, 'pageTitle' => $pageTitle, 'metaDescription' => $metaDescription, 'metaKeywords' => $metaKeywords]);
                } else return view('admin.pdf', ['pdfFormat' => $pdfFormat, 'data' => $result, 'user' => $user, 'all_courses' => $courses, 'role' => $role, 'user_documents' => $user_documents, 'user_id' => $user_id, 'documents' => $uploaded_documents, 'user_contract' => $user_contract, 'is_resume' => $isResume, 'pageTitle' => $pageTitle, 'metaDescription' => $metaDescription, 'metaKeywords' => $metaKeywords]);
            } else {
                return view('errors.404');
            }
            return view('errors.404');
            //return view('site.user.profile');
        }
    }

    public function pdfviewOld(Request $request, $is_download = true) {
        $user_id = null;
        $user = '';
        $role = '';
        if (isset($user_id)) {
            $user = 'another_user';
        }
        if (Auth::check() && !isset($user_id)) {
            $user_id = Auth::user()->id;

        } elseif (isset($user_id) && $user_id != '') {
            # code...
        }
        
        if (Auth::check()) {
            $role = Auth::user()->registered_as;
        }
        if (isset($user_id)) {

            $options['with'] = ['userDangerousCargoEndorsementDetail','personal_detail', 'professional_detail', 'passport_detail', 'seaman_book_detail', 'coc_detail', 'gmdss_detail', 'wkfr_detail', 'personal_detail.state', 'personal_detail.city', 'personal_detail.pincode.pincodes_states.state', 'personal_detail.pincode.pincodes_cities.city', 'sea_service_detail', 'course_detail', 'cop_detail', 'coe_detail', 'document_permissions.requester.company_registration_detail', 'document_permissions.requester.institute_registration_detail', 'document_permissions.document_type.type'];
            $result = $this->userService->getDataByUserId($user_id, $options)->toArray();
            if (isset($result[0]['professional_detail']['current_rank_exp'])) {
                $rank_exp = explode(".", $result[0]['professional_detail']['current_rank_exp']);
                $result[0]['professional_detail']['years'] = $rank_exp[0];
                $result[0]['professional_detail']['months'] = ltrim($rank_exp[1], 0);
            }
            $courses = $this->userService->getAllCourses();
            
            $user_documents = $this->userService->getUserDocuments($user_id);
            $uploaded_documents = [];
            

            if (isset($result[0]['professional_detail']['current_rank']) &&
                !empty($result[0]['professional_detail']['current_rank'])) {
                $required_documents = \CommonHelper::rank_required_fields()[$result[0]['professional_detail']['current_rank']];
            }

            //if(isset($required_documents) && !empty($required_documents)){

            //Passport visa block
            if (isset($result[0]['passport_detail']) && !empty($result[0]['passport_detail'])) {
                $uploaded_documents['passport'] = $result[0]['passport_detail'];
                $uploaded_documents['passport']['title'] = 'Passport / Visa';

                if (isset($user_documents['passport']) && !empty($user_documents['passport'])) {
                    $uploaded_documents['passport']['imp_doc'] = $user_documents['passport'];
                }
            }
            if (isset($result[0]['passport_detail']['us_visa']) && $result[0]['passport_detail']['us_visa'] == '1') {
                $uploaded_documents['visa'] = $result[0]['passport_detail'];
                if (isset($result[0]['passport_detail']) && !empty($result[0]['passport_detail'])) {
                    $uploaded_documents['visa']['hide'] = 1;
                    $uploaded_documents['visa']['title'] = 'Passport / Visa';
                }

                if (isset($user_documents['visa']) && !empty($user_documents['visa'])) {
                    $uploaded_documents['visa']['imp_doc'] = $user_documents['visa'];
                }
            }

            //CDC
            if (isset($result[0]['seaman_book_detail']) && !empty($result[0]['seaman_book_detail'])) {
                $uploaded_documents['cdc'] = $result[0]['seaman_book_detail'];

                if (isset($uploaded_documents['cdc']) && !empty($uploaded_documents['cdc'])) {
                    foreach ($uploaded_documents['cdc'] as $key => $value) {
                        if (isset($user_documents['cdc'][$key + 1]['user_documents']) &&
                            !empty($user_documents['cdc'][$key + 1]['user_documents'])) {
                            $uploaded_documents['cdc'][$key]['title'] = 'CDC';

                            if (isset($user_documents['cdc'][$key + 1]) && !empty($user_documents['cdc'][$key + 1])) {
                                $uploaded_documents['cdc'][$key]['imp_doc'] = $user_documents['cdc'][$key + 1];
                            }
                        }
                    }
                }
            }

            //Coc Coe Block
            //if(in_array('COC', $required_documents)){
            $uploaded_documents['coc'] = $result[0]['coc_detail'];

            if (isset($uploaded_documents['coc']) && !empty($uploaded_documents['coc'])) {
                foreach ($uploaded_documents['coc'] as $key => $value) {
                    if (isset($user_documents['coc'][$key + 1]['user_documents']) &&
                        !empty($user_documents['coc'][$key + 1]['user_documents'])) {
                        $uploaded_documents['coc'][$key]['title'] = 'COC / COE';

                        if (isset($user_documents['coc'][$key + 1]) && !empty($user_documents['coc'][$key + 1]))
                            $uploaded_documents['coc'][$key]['imp_doc'] = $user_documents['coc'][$key + 1];
                    }
                }
            }
            //}

            //if(in_array('COE', $required_documents) || in_array('COE-Optional', $required_documents)){
            $uploaded_documents['coe'] = $result[0]['coe_detail'];

            if (in_array('COC', $required_documents)) {
                foreach ($uploaded_documents['coe'] as $key => $value) {
                    if (isset($user_documents['coe'][$key + 1]['user_documents']) &&
                        !empty($user_documents['coe'][$key + 1]['user_documents'])) {
                        $uploaded_documents['coe'][$key]['hide'] = 1;
                        $uploaded_documents['coe'][$key]['title'] = 'COC / COE';

                        if (isset($user_documents['coe'][$key + 1]) && !empty($user_documents['coe'][$key + 1]))
                            $uploaded_documents['coe'][$key]['imp_doc'] = $user_documents['coe'][$key + 1];
                    }
                }
            }

            //}

            //GMDSS watch keeping
            //if(in_array('GMDSS', $required_documents) || in_array('GMDSS-Optional', $required_documents)){
            $uploaded_documents['gmdss'] = $result[0]['gmdss_detail'];
            $uploaded_documents['gmdss']['title'] = 'GMDSS / WK';

            if (isset($user_documents['gmdss']) && !empty($user_documents['gmdss'])) {
                $uploaded_documents['gmdss']['imp_doc'] = $user_documents['gmdss'];
            }

            //}
            //if(in_array('WATCH_KEEPING-Optional', $required_documents)){
            $uploaded_documents['watch keeping'] = $result[0]['wkfr_detail'];
            $uploaded_documents['watch keeping']['title'] = 'GMDSS / WK';

            if (in_array('GMDSS', $required_documents) || in_array('GMDSS-Optional', $required_documents)) {
                $uploaded_documents['watch keeping']['hide'] = 1;

                if (isset($user_documents['watch keeping']) && !empty($user_documents['watch keeping'])) {
                    $uploaded_documents['watch keeping']['imp_doc'] = $user_documents['watch keeping'];
                }

            }
            // }

            //courses
            if (isset($result[0]['course_detail']) && !empty($result[0]['course_detail'])) {
                $uploaded_documents['courses'] = $result[0]['course_detail'];

                foreach ($uploaded_documents['courses'] as $key => $value) {
                    if (isset($user_documents['courses'][$key + 1]['user_documents']) &&
                        !empty($user_documents['courses'][$key + 1]['user_documents'])) {
                        $uploaded_documents['courses'][$key]['title'] = 'PreSea / PostSea Course Certificates';

                        if (isset($user_documents['courses'][$key + 1]) && !empty($user_documents['courses'][$key + 1]))
                            $uploaded_documents['courses'][$key]['imp_doc'] = $user_documents['courses'][$key + 1];
                    }
                }
            }

            //sea service
            if (isset($result[0]['sea_service_detail']) && !empty($result[0]['sea_service_detail'])) {
                $uploaded_documents['sea service'] = $result[0]['sea_service_detail'];

                foreach ($result[0]['sea_service_detail'] as $key => $value) {
                    if (isset($user_documents['sea service'][$key + 1]['user_documents']) &&
                        !empty($user_documents['sea service'][$key + 1]['user_documents'])) {
                        $uploaded_documents['sea service'][$key]['title'] = 'Sea Service Letters';

                        if (isset($user_documents['sea service'][$key + 1]) &&
                            !empty($user_documents['sea service'][$key + 1])) {
                            $uploaded_documents['sea service'][$key]['imp_doc'] = $user_documents['sea service'][$key +
                            1];
                        }
                    }
                }
            }



            //yf vaccination
            if (isset($result[0]['wkfr_detail']) && $result[0]['wkfr_detail']['yellow_fever'] == '1') {
                $uploaded_documents['yellow fever'] = $result[0]['wkfr_detail'];
                if (isset($user_documents['yellow fever'][0]['user_documents']) &&
                    !empty($user_documents['yellow fever'][0]['user_documents'])) {
                    $uploaded_documents['yellow fever']['title'] = 'Medicals / YF Vaccination';

                    if (isset($user_documents['yellow fever']) && !empty($user_documents['yellow fever'])) {
                        $uploaded_documents['yellow fever']['imp_doc'] = $user_documents['yellow fever'];
                    }
                }
            }
            //}

            $count = 0;
            if (isset($result[0]['course_detail'])) {
                foreach ($result[0]['course_detail'] as $key => $value) {
                    if ($value['course_type'] == 'Value Added') {
                        $result[0]['value_added_course_detail'][$count] = $value;
                        $count++;
                        unset($result[0]['course_detail'][$key]);
                    }
                }
            }

            $user_contract = $this->GraphController->createGraphData();
            
            if (count($result) > 0 &&
                ($result[0]['registered_as'] == null || $result[0]['registered_as'] != 'advertiser')) {
                $pdfFormat = false;
                if ($request->has('PDF')) {
                    $_save_path = 'public/uploads/user_documents/' . DIRECTORY_SEPARATOR . Auth::id() . DIRECTORY_SEPARATOR;
                    $pdfFormat = true;
                    $_decode_pdf=CommonHelper::decodeKey($request->PDF);
                   
                    if ($_decode_pdf == 1) {
                        return $this->pdfGenerator('admin.pdf', $pdfFormat, $result, $user, $courses, $role, $user_documents, $user_id, $uploaded_documents, $user_contract, $is_download);
                    } elseif ($_decode_pdf == 2) {
                        return $this->pdfGenerator('admin.pdf2', $pdfFormat, $result, $user, $courses, $role, $user_documents, $user_id, $uploaded_documents, $user_contract, $is_download);
                         
                    } elseif ($_decode_pdf == 3) {
                        return $this->pdfGenerator('admin.pdf3', $pdfFormat, $result, $user, $courses, $role, $user_documents, $user_id, $uploaded_documents, $user_contract, $is_download);
                    } else {
                        return $this->pdfGenerator('admin.pdf', $pdfFormat, $result, $user, $courses, $role, $user_documents, $user_id, $uploaded_documents, $user_contract, $is_download);
                    }
                }

                if ($request->has('resume')) {
                    $decoded=CommonHelper::decodeKey($request->resume);
                    
                    if ($decoded ==
                        1) return view('admin.pdf', ['pdfFormat' => $pdfFormat, 'data' => $result, 'user' => $user, 'all_courses' => $courses, 'role' => $role, 'user_documents' => $user_documents, 'user_id' => $user_id, 'documents' => $uploaded_documents, 'user_contract' => $user_contract]);
                    elseif ($decoded ==
                        2) return view('admin.pdf2', ['pdfFormat' => $pdfFormat, 'data' => $result, 'user' => $user, 'all_courses' => $courses, 'role' => $role, 'user_documents' => $user_documents, 'user_id' => $user_id, 'documents' => $uploaded_documents, 'user_contract' => $user_contract]);
                    elseif ($decoded ==
                        3) return view('admin.pdf3', ['pdfFormat' => $pdfFormat, 'data' => $result, 'user' => $user, 'all_courses' => $courses, 'role' => $role, 'user_documents' => $user_documents, 'user_id' => $user_id, 'documents' => $uploaded_documents, 'user_contract' => $user_contract]);
                    else return view('admin.pdf', ['pdfFormat' => $pdfFormat, 'data' => $result, 'user' => $user, 'all_courses' => $courses, 'role' => $role, 'user_documents' => $user_documents, 'user_id' => $user_id, 'documents' => $uploaded_documents, 'user_contract' => $user_contract]);
                } else return view('admin.pdf', ['pdfFormat' => $pdfFormat, 'data' => $result, 'user' => $user, 'all_courses' => $courses, 'role' => $role, 'user_documents' => $user_documents, 'user_id' => $user_id, 'documents' => $uploaded_documents, 'user_contract' => $user_contract]);
            } else {
                return view('errors.404');
            }
            return view('errors.404');
            //return view('site.user.profile');
        }
    }
    
    public function sharePdf(Request $request, $is_download = true) {
        $pageTitle = "Seafarer Resume – Engineers, Deck Officers, Ratings | Flanknot";
        $metaDescription = "At Flanknot, view Seafarer Resumes. Give Quick Response QR, download Resume.";
        $metaKeywords = "Rank, Master, Captain, Deck Officers, Engineers, Passport, Visa, Indos,CDC, Continuous discharge Certificate, Seaman book information, WK, Watchkeeping Certificate, COP, Certificate of Proficiency,COC, Certificate of Competency, COE, Certificate of Endorsement,DCE, Dangerous Cargo Endorsement, GMDSS, Global Maritime Distress and Safety System, FRAMO, DP, Dynamic positioning, Sea Service, AOA, Article of Agreement, Service Contract, Course, Medical, Vaccination, Character Certificate, PAN, Aadhar";
        $shareHistoryId = \CommonHelper::decodeKey($request['resume']);
        if(!$shareHistoryId){
            $shareHistoryId = \CommonHelper::decodeKey($request['PDF']);
        }
        $shareHistory = ShareHistory::whereId($shareHistoryId)->first();
    //    dd($shareHistory);
        if(!empty($shareHistory)){
            $existTodayHistory = HistoryLog::where("share_history_id", $shareHistoryId)->whereDate('created_at', '=', date('Y-m-d'))->first();
            if(empty($existTodayHistory)){
                $user_id = $shareHistory['user_id'];
                $HistoryLog = new HistoryLog();
                $HistoryLog->share_history_id = $shareHistoryId;
                $HistoryLog->description = "Resumed Viewed";
                $HistoryLog->ip = '';
                $HistoryLog->created_at = date('Y-m-d H:i:s');
                $HistoryLog->save();

                $ShareHistoryUpdate = ShareHistory::whereId($shareHistoryId);
                $ShareHistoryUpdate->update([
                    'is_new' => 1
                ]);
            }
        }
//        $user_id = \CommonHelper::decodeKey($request['resume']);
        $user = '';
        $role = '';
        if (isset($user_id)) {
            $user = 'another_user';
        }
        if (Auth::check() && !isset($user_id)) {
            $user_id = Auth::user()->id;

        } elseif (isset($user_id) && $user_id != '') {
            # code...
        }

//        if (Auth::check()) {
//            $role = Auth::user()->registered_as;
//        }
        if (isset($user_id)) {

            $options['with'] = ['userDangerousCargoEndorsementDetail','personal_detail', 'professional_detail', 'passport_detail', 'seaman_book_detail', 'coc_detail', 'gmdss_detail', 'wkfr_detail', 'personal_detail.state', 'personal_detail.city', 'personal_detail.pincode.pincodes_states.state', 'personal_detail.pincode.pincodes_cities.city', 'sea_service_detail', 'course_detail', 'cop_detail', 'coe_detail', 'document_permissions.requester.company_registration_detail', 'document_permissions.requester.institute_registration_detail', 'document_permissions.document_type.type', 'visas', 'wkfr_detail'];
            $result = $this->userService->getDataByUserId($user_id, $options)->toArray();
            $currentRank = null;
            if(isset($result[0]['professional_detail']['current_rank'])){
                $currentRank = $result[0]['professional_detail']['current_rank'];
                $show_resume = CommonHelper::getResumeNumber($currentRank);
            }
            if (isset($result[0]['professional_detail']['current_rank_exp'])) {
                $rank_exp = explode(".", $result[0]['professional_detail']['current_rank_exp']);
                $result[0]['professional_detail']['years'] = $rank_exp[0];
                $result[0]['professional_detail']['months'] = ltrim($rank_exp[1], 0);
            }
//            dd($result);
            $courses = $this->userService->getAllCourses();
            
            $user_documents = $this->userService->getUserDocuments($user_id);
            $uploaded_documents = [];

            if (isset($user_documents['photo']) && !empty($user_documents['photo'])) {
                if(count($user_documents['photo']) > 0 && count($user_documents['photo'][0]['user_documents']) > 0) {
                    $result[0]['photo'] = $user_documents['photo'][0]['user_documents'][0];
                }
            }

            if (isset($result[0]['professional_detail']['current_rank']) &&
                !empty($result[0]['professional_detail']['current_rank'])) {
                $required_documents = \CommonHelper::rank_required_fields()[$result[0]['professional_detail']['current_rank']];
            }

            //if(isset($required_documents) && !empty($required_documents)){

            //Passport visa block
            if (isset($result[0]['passport_detail']) && !empty($result[0]['passport_detail'])) {
                $uploaded_documents['passport'] = $result[0]['passport_detail'];
                $uploaded_documents['passport']['title'] = 'Passport / Visa';

                if (isset($user_documents['passport']) && !empty($user_documents['passport'])) {
                    $uploaded_documents['passport']['imp_doc'] = $user_documents['passport'];
                }
            }
            if (isset($result[0]['passport_detail']['us_visa']) && $result[0]['passport_detail']['us_visa'] == '1') {
                $uploaded_documents['visa'] = $result[0]['passport_detail'];
                if (isset($result[0]['passport_detail']) && !empty($result[0]['passport_detail'])) {
                    $uploaded_documents['visa']['hide'] = 1;
                    $uploaded_documents['visa']['title'] = 'Passport / Visa';
                }

                if (isset($user_documents['visa']) && !empty($user_documents['visa'])) {
                    $uploaded_documents['visa']['imp_doc'] = $user_documents['visa'];
                }
            }

            //CDC
            if (isset($result[0]['seaman_book_detail']) && !empty($result[0]['seaman_book_detail'])) {
                $uploaded_documents['cdc'] = $result[0]['seaman_book_detail'];

                if (isset($uploaded_documents['cdc']) && !empty($uploaded_documents['cdc'])) {
                    foreach ($uploaded_documents['cdc'] as $key => $value) {
                        if (isset($user_documents['cdc'][$key + 1]['user_documents']) &&
                            !empty($user_documents['cdc'][$key + 1]['user_documents'])) {
                            $uploaded_documents['cdc'][$key]['title'] = 'CDC';

                            if (isset($user_documents['cdc'][$key + 1]) && !empty($user_documents['cdc'][$key + 1])) {
                                $uploaded_documents['cdc'][$key]['imp_doc'] = $user_documents['cdc'][$key + 1];
                            }
                        }
                    }
                }
            }

            //Coc Coe Block
            //if(in_array('COC', $required_documents)){
            $uploaded_documents['coc'] = isset($result[0]['coc_detail']) ? $result[0]['coc_detail'] : '';

            if (isset($uploaded_documents['coc']) && !empty($uploaded_documents['coc'])) {
                foreach ($uploaded_documents['coc'] as $key => $value) {
                    if (isset($user_documents['coc'][$key + 1]['user_documents']) &&
                        !empty($user_documents['coc'][$key + 1]['user_documents'])) {
                        $uploaded_documents['coc'][$key]['title'] = 'COC / COE';

                        if (isset($user_documents['coc'][$key + 1]) && !empty($user_documents['coc'][$key + 1]))
                            $uploaded_documents['coc'][$key]['imp_doc'] = $user_documents['coc'][$key + 1];
                    }
                }
            }
            //}

            //if(in_array('COE', $required_documents) || in_array('COE-Optional', $required_documents)){
            $uploaded_documents['coe'] = isset($result[0]['coe_detail']) ? $result[0]['coe_detail'] : '';

            if (in_array('COC', $required_documents)) {
                foreach ($uploaded_documents['coe'] as $key => $value) {
                    if (isset($user_documents['coe'][$key + 1]['user_documents']) &&
                        !empty($user_documents['coe'][$key + 1]['user_documents'])) {
                        $uploaded_documents['coe'][$key]['hide'] = 1;
                        $uploaded_documents['coe'][$key]['title'] = 'COC / COE';

                        if (isset($user_documents['coe'][$key + 1]) && !empty($user_documents['coe'][$key + 1]))
                            $uploaded_documents['coe'][$key]['imp_doc'] = $user_documents['coe'][$key + 1];
                    }
                }
            }
            //}

            //GMDSS watch keeping
            //if(in_array('GMDSS', $required_documents) || in_array('GMDSS-Optional', $required_documents)){
            $uploaded_documents['gmdss'] = $result[0]['gmdss_detail'];
            $uploaded_documents['gmdss']['title'] = 'GMDSS / WK';

            if (isset($user_documents['gmdss']) && !empty($user_documents['gmdss'])) {
                $uploaded_documents['gmdss']['imp_doc'] = $user_documents['gmdss'];
            }

            //}
            //if(in_array('WATCH_KEEPING-Optional', $required_documents)){
            $uploaded_documents['watch keeping'] = $result[0]['wkfr_detail'];
            $uploaded_documents['watch keeping']['title'] = 'GMDSS / WK';

            if (in_array('GMDSS', $required_documents) || in_array('GMDSS-Optional', $required_documents)) {
                $uploaded_documents['watch keeping']['hide'] = 1;

                if (isset($user_documents['watch keeping']) && !empty($user_documents['watch keeping'])) {
                    $uploaded_documents['watch keeping']['imp_doc'] = $user_documents['watch keeping'];
                }

            }
            // }
            
            //yf vaccination
            if (isset($result[0]['wkfr_detail']) && $result[0]['wkfr_detail']['yellow_fever'] == '1') {
                $uploaded_documents['yellow fever'] = $result[0]['wkfr_detail'];
                if (isset($user_documents['yellow fever'][0]['user_documents']) &&
                    !empty($user_documents['yellow fever'][0]['user_documents'])) {
                    $uploaded_documents['yellow fever']['title'] = 'Medicals / YF Vaccination';

                    if (isset($user_documents['yellow fever']) && !empty($user_documents['yellow fever'])) {
                        $uploaded_documents['yellow fever']['imp_doc'] = $user_documents['yellow fever'];
                    }
                }
            }
            //}
            
            //courses
            if (isset($result[0]['course_detail']) && !empty($result[0]['course_detail'])) {
                $uploaded_documents['courses'] = $result[0]['course_detail'];

                foreach ($uploaded_documents['courses'] as $key => $value) {
                    if (isset($user_documents['courses'][$key + 1]['user_documents']) &&
                        !empty($user_documents['courses'][$key + 1]['user_documents'])) {
                        $uploaded_documents['courses'][$key]['title'] = 'PreSea / PostSea Course Certificates';

                        if (isset($user_documents['courses'][$key + 1]) && !empty($user_documents['courses'][$key + 1]))
                            $uploaded_documents['courses'][$key]['imp_doc'] = $user_documents['courses'][$key + 1];
                    }
                }
            }

            //sea service
            if (isset($result[0]['sea_service_detail']) && !empty($result[0]['sea_service_detail'])) {
                $uploaded_documents['sea service'] = $result[0]['sea_service_detail'];

                foreach ($result[0]['sea_service_detail'] as $key => $value) {
                    if (isset($user_documents['sea service'][$key + 1]['user_documents']) &&
                        !empty($user_documents['sea service'][$key + 1]['user_documents'])) {
                        $uploaded_documents['sea service'][$key]['title'] = 'Sea Service Letters';

                        if (isset($user_documents['sea service'][$key + 1]) &&
                            !empty($user_documents['sea service'][$key + 1])) {
                            $uploaded_documents['sea service'][$key]['imp_doc'] = $user_documents['sea service'][$key +
                            1];
                        }
                    }
                }
            }

            //yf vaccination
            if (isset($result[0]['wkfr_detail']) && $result[0]['wkfr_detail']['yellow_fever'] == '1') {
                $uploaded_documents['yellow fever'] = $result[0]['wkfr_detail'];
                if (isset($user_documents['yellow fever'][0]['user_documents']) &&
                    !empty($user_documents['yellow fever'][0]['user_documents'])) {
                    $uploaded_documents['yellow fever']['title'] = 'Medicals / YF Vaccination';

                    if (isset($user_documents['yellow fever']) && !empty($user_documents['yellow fever'])) {
                        $uploaded_documents['yellow fever']['imp_doc'] = $user_documents['yellow fever'];
                    }
                }
            }
            //}

            $count = 0;
            if (isset($result[0]['course_detail'])) {
                foreach ($result[0]['course_detail'] as $key => $value) {
                    if ($value['course_type'] == 'Value Added') {
                        $result[0]['value_added_course_detail'][$count] = $value;
                        $count++;
                        unset($result[0]['course_detail'][$key]);
                    }
                }
            }

            $user_contract = $this->GraphController->createGraphData($user_id);
            if (count($result) > 0 &&
                ($result[0]['registered_as'] == null || $result[0]['registered_as'] != 'advertiser')) {
                $pdfFormat = false;
                if ($request->has('PDF')) {
                    $_save_path = 'public/uploads/user_documents/' . DIRECTORY_SEPARATOR . $user_id . DIRECTORY_SEPARATOR;
                    $pdfFormat = true;
                    $_decode_pdf=$show_resume;
                    if ($_decode_pdf == 1) {
                        return $this->pdfGenerator('admin.pdf', $pdfFormat, $result, $user, $courses, $role, $user_documents, $user_id, $uploaded_documents, $user_contract, $is_download);
                    } elseif ($_decode_pdf == 2) {
                        return $this->pdfGenerator('admin.pdf2', $pdfFormat, $result, $user, $courses, $role, $user_documents, $user_id, $uploaded_documents, $user_contract, $is_download);
                    } elseif ($_decode_pdf == 3) {
                        return $this->pdfGenerator('admin.pdf3', $pdfFormat, $result, $user, $courses, $role, $user_documents, $user_id, $uploaded_documents, $user_contract, $is_download);
                    } else {
                        return $this->pdfGenerator('admin.pdf', $pdfFormat, $result, $user, $courses, $role, $user_documents, $user_id, $uploaded_documents, $user_contract, $is_download);
                    }
                }

                if ($show_resume) {
                    $decoded=$show_resume;
                    if ($decoded ==
                        1) return view('admin.share_pdf', ['pdfFormat' => $pdfFormat, 'data' => $result, 'user' => $user, 'all_courses' => $courses, 'role' => $role, 'user_documents' => $user_documents, 'user_id' => $user_id, 'documents' => $uploaded_documents, 'user_contract' => $user_contract, 'shareHistory' => $shareHistory, 'pageTitle' => $pageTitle, 'metaDescription' => $metaDescription, 'metaKeywords' => $metaKeywords]);
                    elseif ($decoded ==
                        2) return view('admin.share_pdf2', ['pdfFormat' => $pdfFormat, 'data' => $result, 'user' => $user, 'all_courses' => $courses, 'role' => $role, 'user_documents' => $user_documents, 'user_id' => $user_id, 'documents' => $uploaded_documents, 'user_contract' => $user_contract, 'shareHistory' => $shareHistory, 'pageTitle' => $pageTitle, 'metaDescription' => $metaDescription, 'metaKeywords' => $metaKeywords]);
                    elseif ($decoded ==
                        3) return view('admin.share_pdf3', ['pdfFormat' => $pdfFormat, 'data' => $result, 'user' => $user, 'all_courses' => $courses, 'role' => $role, 'user_documents' => $user_documents, 'user_id' => $user_id, 'documents' => $uploaded_documents, 'user_contract' => $user_contract, 'shareHistory' => $shareHistory, 'pageTitle' => $pageTitle, 'metaDescription' => $metaDescription, 'metaKeywords' => $metaKeywords]);
                    else return view('admin.share_pdf', ['pdfFormat' => $pdfFormat, 'data' => $result, 'user' => $user, 'all_courses' => $courses, 'role' => $role, 'user_documents' => $user_documents, 'user_id' => $user_id, 'documents' => $uploaded_documents, 'user_contract' => $user_contract, 'shareHistory' => $shareHistory, 'pageTitle' => $pageTitle, 'metaDescription' => $metaDescription, 'metaKeywords' => $metaKeywords]);
                } else return view('admin.share_pdf', ['pdfFormat' => $pdfFormat, 'data' => $result, 'user' => $user, 'all_courses' => $courses, 'role' => $role, 'user_documents' => $user_documents, 'user_id' => $user_id, 'documents' => $uploaded_documents, 'user_contract' => $user_contract, 'shareHistory' => $shareHistory, 'pageTitle' => $pageTitle, 'metaDescription' => $metaDescription, 'metaKeywords' => $metaKeywords]);
            } else {
                return view('errors.404');
            }
            return view('errors.404');
            //return view('site.user.profile');
        }
    }

    protected function pdfGenerator($pdf_view, $pdfFormat, $result, $user, $courses, $role, $user_documents, $user_id, $uploaded_documents, $user_contract, $is_download)
    {
        ini_set('max_execution_time', 600);
        
        $_save_path = 'public/uploads/user_documents/' . DIRECTORY_SEPARATOR . $user_id . DIRECTORY_SEPARATOR;
        
        $pdf = PDF::loadView($pdf_view, [
            'pdfFormat' => $pdfFormat,
            'data' => $result,
            'user' => $user,
            'all_courses' => $courses,
            'role' => $role,
            'user_documents' => $user_documents,
            'user_id' => $user_id,
            'documents' => $uploaded_documents,
            'user_contract' => $user_contract
        ]);

        try {
            $pdf->setOption('enable-javascript', true)
                ->setOption('images', true)
                ->setOption('viewport-size', '1366x800')
                ->setOption('dpi', '200')
                ->setOption('lowquality', false)
               //->setOption('encoding', 'utf-8')
                ->setOption('javascript-delay', 13000)
                ->setOption('enable-smart-shrinking', true)
                ->setOption('no-stop-slow-scripts', true)
                ->save($_save_path . $result[0]['first_name'] . ' ' . date('d-m-Y H.i.s') . 'Resume Flanknot.pdf', true);
        } catch (\Exception $ex) {
            $msg = $ex->getMessage();
                if(isset($ex->errorInfo[2])) {
                    $msg = $ex->errorInfo[2];
                }
                echo '<pre>';
                print_r($msg);
                exit;
            abort(598);
        }
        if ($is_download)
            return $pdf->download($result[0]['first_name'] . ' ' . date('d-m-Y H.i.s') . 'ResumeFlanknot.pdf');
        else return true;
    }
    
    public function saveQr(Request $request){
        $data_arr = $request->toArray();
        if (isset($data_arr['response_id'])) {
            $contactDetails = (($data_arr['response_id'] == 3 || $data_arr['response_id'] == 4) ? ". Contact Name : " . $data_arr['contact_name']. " | Contact Number : " . $data_arr['contact_number'] : "");
            $shareHistory = ShareHistory::whereId($data_arr['share_history_id'])->with('user')->first();
            if(isset($shareHistory['user']) && !empty($shareHistory['user'])){
                $mail_data = [
                    'email' => $shareHistory['user']['email'],
                    'subject' => 'You have Received QR',
                    'name' => $shareHistory['user']['first_name'],
                    'qr_link' => 'share-history-resume',
                    'qr_resp' => $data_arr['response'] . $contactDetails,
                    'user' => $shareHistory['user']
                ];
                Mail::to($shareHistory['user']['email'])->send(new ReceivedQr($mail_data));
            }
            
            $HistoryLog = new HistoryLog();
            $HistoryLog->share_history_id = $data_arr['share_history_id'];
            $HistoryLog->description = "QR : " . $data_arr['response'] . $contactDetails;
            $HistoryLog->ip = '';
            $HistoryLog->created_at = date('Y-m-d H:i:s');
            $HistoryLog->save();
            $ShareHistory = ShareHistory::whereId($data_arr['share_history_id']);
            $ShareHistory->update([
                'latest_qr' => date('Y-m-d H:i:s'),
                'is_new' => 1
            ]);
            
            

            return response()->json(['msg' => 'Quick Response Saved.',
                            'status' => 200
                                ], 200);
        }
        return response()->json(['msg' => 'Something went Wrong',
                    'status' => 400
                        ], 200);
    }
}
