<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use route;
use View;
use App\Affiliate;
use Illuminate\Support\Facades\Input;
use App\Repositories\AffiliateRepository;
use Yajra\Datatables\Datatables;
use App\Repositories\UserRepository;
use App\Services\UserService;
use App\Courses;
use Maatwebsite\Excel\Facades\Excel as Excel;

class AffiliateController extends Controller {

   
    private $affiliate;    
    private $modelObj;
    private $userRepository;
    private $userService;

    function __construct() {
        $this->affiliate = new AffiliateRepository();
        $this->modelObj = new Affiliate();
        $this->userRepository = new UserRepository();
        $this->userService = new UserService();
        $this->courses = new Courses();
    }
    
    public function index()
    {   
        return view('admin.affiliate.index');
    } 

    public function getAjaxAffiliateList(Request $request)
    {
        return $this->modelObj->getAjaxAffiliateListDatatable($request);
    }

    public function addForm(){
        return view('admin.affiliate.add');
    }

    public function editForm($id){
        $data = $this->affiliate->getRecordById($id);
        return view('admin.affiliate.add',compact('data'));
    }

    public function store(Request $request){
        try{  
            $params = $request->all();           
            $is_edit = $this->affiliate->getRecordById($params['id']);
         
            $this->validate($request, [
                'name' => 'required',
                'email' => 'required'
            ]);

            $random_number = mt_rand(100,999);
            $params['name'] = str_replace(' ', '', $params['name']);
            $params['redemption'] = $params['name'];
            $redemption =  $params['redemption'].$random_number;

            $checkIfCodeExist = $this->affiliate->getRecordByCode($redemption,$params['id']); 
            if(empty($checkIfCodeExist)){ 
                $params['redemption'] = $redemption;
            }
  
            $checkIfExist = $this->userRepository->checkDetailsExist($params['redemption'],$params['email'],$params['phone_number'],$params['user_id']);           
           
            if($is_edit) {                
            // Edit 
                if(empty($checkIfExist)){
                    $data['first_name'] = $params['name'];                 
                    $data['mobile'] = isset($params['phone_number']) && !empty($params['phone_number']) ? $params['phone_number'] : null;
                 
                    $result = $this->userRepository->updateAffiliateData($data,$params['user_id']);
                    if($result){
                        $affiliate_data['name'] = $params['name'];
                        $affiliate_data['phone_number'] =  isset($params['phone_number']) && !empty($params['phone_number']) ? $params['phone_number'] : null;
                        $affiliate_data['commission'] =  $params['commission'];
                        $result = $this->affiliate->update($affiliate_data,$params['id']);
                    }
                }else{
                    return response()->json(['status' => 'error','message' => 'Email or Mobile Number already exist!']);
                } 
               
            } else {
                // Add
                if(empty($checkIfExist)){   
                    $data['first_name'] = $params['name'];
                    $data['email'] = $params['email'];
                    $data['mobile'] = isset($params['phone_number']) && !empty($params['phone_number']) ? $params['phone_number'] : null;
                    $data['user_affiliate_code'] = $params['redemption'];
                    $data['registered_as'] = 'affiliate';    

                    $userData = $this->userRepository->addAffiliateData($data);
                    // store data in affiliate
                    if($userData){
                        $params['user_id'] = $userData->id; 
                        $result = $this->affiliate->store($params);
                    }                     
                }else{
                    return response()->json(['status' => 'error','message' => 'Email or Mobile Number already exist!']);
                }
            }
              
            if($is_edit && $result){
                return response()->json(['status' => 'success','message' => 'Successfully Updated']);
            }else{
                return response()->json(['status' => 'success','message' => 'Successfully Added']);
            }
        }catch(\Exception $e){
               return response()->json(['status' => 'error','message' => 'Something went wrong!']);
        }
    }

    public function deleteData(Request $request,$id){
        try{
            $result = $this->affiliate->getRecordById($id);
            if(!empty($result)) {
                $deleteRecord = $this->affiliate->deleteById($id);
                if($deleteRecord){
                    return response()->json(['status' => 'success','message' => 'Successfully deleted!']);
                }
            }
        }catch(\Exception $e){
            return response()->json(['status' => 'error','message' => 'Something went wrong!']);
        }
       
    }

    public function getaffiliate_form(){
        return view('site.affiliate_form');
    }

    public function add_affiliate_form(Request $request){        
        try{
            $params = $request->except('_token');
            $data = array();
            $user_id = \Auth::id(); 
            if($params['referal'] == 1){
                $record = $this->userRepository->getReferalCode($params['referalCode']);
                if(empty($record)){
                    return response()->json(['status' => 'error','message' => 'Sorry! Referral code not found.']);
                }else{
                    $data = [
                        'affiliate_user_id' => $record->id,
                        'affiliate_referral_code' => $record->user_affiliate_code
                    ];
                    $affiliate_data = [
                        'user_id' => $record->id,
                        'referral_user_id' => \Auth::id(),
                        'type' => 1
                    ];
                    $rewardsData = $this->userRepository->addRewardsData($affiliate_data);
                }
            }
            $data['affiliate_referral_by'] = $params['referal'];        
            $updatedRecord = $this->userRepository->addReferalDetails($user_id,$data);
            return response()->json(['status' => 'success','message' => 'Successfully updated!']);

        }catch(\Exception $e){
            return response()->json(['status' => 'error','message' => 'Something went wrong!']);
        }
      } 

       public function getRewardsUserData(Request $request){  
            $affiliate_user_data = $this->userRepository->getReferralUsersRewardsData($request->userid);    
            $obj = []; 
            $result = [];    
            $totalRecords = 0;  
            if(!empty($affiliate_user_data)){
                foreach($affiliate_user_data as $key=>$value){
                    $userData =  $value->rewards;
                    if(!empty($userData)){
                        $user_id = $userData->id;
                        $rank_details = [];
                        $upload_documents_details = [];
                        $course = [];
                        $options['with'] = ['passport_detail','seaman_book_detail','course_detail','professional_detail','user_documents_type_with_document'];

                        $result[] = $this->userService->getDataByUserId($user_id, $options)->toArray();
                    }
                }
            }              
            $obj = $result; 
            if(!empty($obj)){
                $totalRecords = count($obj);
            } 
            $userId = $request->userid;
            $data = view('admin.affiliate.view',compact('obj','totalRecords','userId'))->render();
            return response()->json( ['status' => 'success', 'html'=> $data] );
       }

       public function exportToExcel(Request $request){

        $affiliate_user_data = $this->userRepository->getReferralUsersRewardsData($request->affiliate_user_id);    
            $obj = []; 
            $result = [];    
            $totalRecords = 0;  
            if(!empty($affiliate_user_data)){
                foreach($affiliate_user_data as $key=>$value){
                    $userData =  $value->rewards;
                    if(!empty($userData)){
                        $user_id = $userData->id;
                        $rank_details = [];
                        $upload_documents_details = [];
                        $course = [];
                        $options['with'] = ['passport_detail','seaman_book_detail','course_detail','professional_detail','user_documents_type_with_document'];

                        $result[] = $this->userService->getDataByUserId($user_id, $options)->toArray();
                    }
                }
            }              
            $obj = $result; 
           
            Excel::create('Affiliate Registered Users', function($excel) use($result){
                $excel->sheet('New sheet', function($sheet) use($result) {
                    $sheet->loadView('admin.affiliate.exportusers',array('obj' => $result));
                });
            })->download('xlsx');
       }
   

}
