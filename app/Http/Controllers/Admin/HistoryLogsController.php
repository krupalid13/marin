<?php
namespace App\Http\Controllers\Admin;

use App\ShareContacts;
//use Mail;
use Carbon\Carbon;
use CommonHelper;
use Exception;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use ZanySoft\Zip\Zip;

class HistoryLogsController extends Controller
{

    public function historyLogIndex()
    {
        $pageTitle = "History & log - of Resume sharing with Quick Response received | Flanknot";
        $metaDescription = "At Flanknot, Seafarer User can view the history and log of the resume shared via email.
            The log details include the date and time of resume shared, the date and time of the resume viewed, the date and time of the resume downloaded, A Quick Response also noted as QR.";
        $metaKeywords = "log, history, resume, share, email, link, shipping company, company, personal, viewed, downloaded, Quick Response, QR";
        return view('admin.manage_documents.history_log', compact('pageTitle', 'metaDescription', 'metaKeywords'));
    }
   
    public function historyLogDocument()
    {
        $pageTitle = "History & log - of Document sharing | Flanknot";
        $metaDescription = "At Flanknot, Seafarer User can view the history and log of the document shared via email.
            The log details include the date and time of document shared, the list of documents shared, the date and time of the document viewed, the date and time of the document downloaded. Approvals and rejection for resharing of expired document links.";
        $metaKeywords = "log, history, documents, share, email, link, shipping company, company, personal, viewed, downloaded";
        return view('admin.manage_documents.history_log_document', compact('pageTitle', 'metaDescription', 'metaKeywords'));
    }

    
}
