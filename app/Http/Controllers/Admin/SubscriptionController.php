<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\SubscriptionService;
use App\Services\InstituteService;
use App\Services\CompanyService;
use App\Services\UserService;
use View;

class SubscriptionController extends Controller
{
     private $subscriptionService;
     private $instituteService;
     private $userService;
     private $companyService;

    function __construct() {
        $this->subscriptionService = new SubscriptionService();
        $this->instituteService = new InstituteService();
        $this->userService = new UserService();
        $this->companyService = new CompanyService();
    }

    public function viewALLCompanySubscriptions(Request $request){

        $filter = '';
        if(isset($request) && !empty($request))
            $filter = $request->toArray();

        $paginate = env('PAGINATE_LIMIT',10);

        $all_subscriptions =  \App\Subscription::all()->toArray();

        $options['with'] = ['company_registration_detail.company_detail','company_registration_detail','company_subscriptions','company_subscriptions.subscription_feature'];

        $subscriptions = $this->userService->getAllCompanySubscriptionsDetails($options,$filter);

        $job_search = View::make("admin.partials.company_subscriptions")->with(['company_data' => $subscriptions,'data' => $filter, 'all_subscriptions' => $all_subscriptions]);
        $template = $job_search->render();

        if($request->isXmlHttpRequest()){
            return response()->json(['template' => $template], 200);
        }else{
           return view('admin.company.company_subscriptions',['company_data' => $subscriptions,'data' => $filter, 'all_subscriptions' => $all_subscriptions]);
        }
    }

    public function getAllUserSubscription(Request $request){
        
        $data = $request->toArray();
        $user_role = $data['role'];
        
        if ($user_role == 'company') {
            $user_id = $data['company_id'];
            $company = $this->companyService->getDetailsByCompanyId($user_id)->toArray();
            
            $subscriptions = $this->subscriptionService->getCompanySubscriptionByRegistrationIdWithPagination($company[0]['id']);
            if (!empty($subscriptions)) {
                $data = $subscriptions->toArray();
            }
        } elseif ($user_role == 'institute') {
            $user_id = $data['institute_id'];
            $institute = $this->instituteService->getDetailsByCompanyId($user_id)->toArray();
            $subscriptions = $this->subscriptionService->getInstituteSubscriptionByRegistrationIdWithPagination($institute[0]['id']);
            if (!empty($subscriptions)) {
                $data = $subscriptions->toArray();
            }
        } elseif ($user_role == 'advertiser') {
            $user_id = $data['advertiser_id'];
            $advertiser = $this->companyService->getDetailsByCompanyId($user_id)->toArray();
            $subscriptions = $this->subscriptionService->getAdvertiserSubscriptionByRegistrationIdWithPagination($advertiser[0]['id']);
            if (!empty($subscriptions)) {
                $data = $subscriptions->toArray();
            }
        } elseif ($user_role == 'seafarer') {
            $user_id = $data['seafarer_id'];
            $subscriptions = $this->subscriptionService->getSeafarerSubscriptionByRegistrationIdWithPagination($user_id);
            if (!empty($subscriptions)) {
                $data = $subscriptions->toArray();
            }
        }
        
        return view("admin.partials.view_subscription")->with(['pagination' => $subscriptions,'data' => $data]);

        if(isset($data) && !empty($data['data'])){

            $job_search = View::make("admin.partials.view_subscription")->with(['pagination' => $subscriptions,'data' => $data]);
            $template = $job_search->render();

            return response()->json(['template' => $template], 200);
            
        }else{
            return response()->json(['message' => 'No Subscriptions Found.'], 400);
        }

    }
}
