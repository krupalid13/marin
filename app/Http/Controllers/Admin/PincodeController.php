<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Services\PincodeService;

class PincodeController extends Controller
{
    private $pincodeService;

    function __construct() {
        $this->pincodeService = new PincodeService();
    }

    public function getStatesCities(Request $request) {
        $result =  $this->pincodeService->getStatesCities($request['pincode'])->toArray();
        if(count($result) > 0) {
            return response()->json(['status' => 'success','result' => $result],200);
        } else {
            return response()->json(['status' => 'failed'],400);
        }
    }
}