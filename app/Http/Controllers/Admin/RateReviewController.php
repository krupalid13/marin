<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\RateReview;
use App\UserSeaService;
use App\Services\UserService;
use Auth;
use Exception;
use Illuminate\Http\Request;
use View;

class RateReviewController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
     private $userService;
    public function __construct()
    {
//        $this->middleware('auth');
        $this->userService = new UserService();

    }
    
    public function index()
    {
        $pageTitle = "Feedback - Onboard Experience & Review Rating of vessel and company | Flanknot";
        $metaDescription = "At Flanknot, Seafarer User can give a detailed experience of work done during onboard tenure. The Seafarer User can also give a profile of his Rank duties while onboard.
            Flanknot provides a rating system where the Seafarer User can rate and give a detailed review and feedback for the vessel, owner company, manning company contracted with.";
        $metaKeywords = "rating, review, feedback, stars, job profile, vessel rating, vessel review, owner company rating, owner company review, manning company rating, manning company review";
        $userId = Auth::user()->id;
        $shipName = collect(UserSeaService::where('user_id', $userId)->where('to', '!=', null)->with('getRateData')->orderBy('from', 'DESC')->orderby('to', 'DESC')->get());
        
        $firstShipName = $shipName->first();
        $fShipId = '';
        if($firstShipName){
            $fShipId = encrypt($firstShipName->id);
        }
        return view('admin.rate_review.rate_review', compact('shipName', 'firstShipName', 'fShipId', 'pageTitle', 'metaDescription', 'metaKeywords'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $userId = Auth::user()->id;
            $seaId = decrypt($request->shipId);
            $rateData = RateReview::where('user_id', $userId)->where('user_sea_service_id', $seaId)->first();
            if (!$rateData) {
                $rateData = new RateReview;
            }
            $rateData->user_id = $userId;
            $rateData->user_sea_service_id = $seaId;
            if ($request->experience) {
                $rateData->experience = $request->experience;
            }
            if ($request->company_type) {
                switch ($request->company_type) {
                    case 'vessel':
                        $rateData->vessel_rate = $request->rating;
                        $rateData->vessel_comment = $request->comment;
                        break;
                    case 'company':
                        $rateData->company_rate = $request->rating;
                        $rateData->company_comment = $request->comment;
                        break;
                    case 'manning':
                        $rateData->manning_rate = $request->rating;
                        $rateData->manning_comment = $request->comment;
                        break;
                    default:
                        # code...
                        break;
                }
            }
            $rateData->save();
            return ['status' => 1, 'shipId' => $rateData->user_sea_service_id];
        } catch (Exception $e) {
            return ['status' => 2];
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getShipData($shipId)
    {
        try {
            $rankData = array_flatten(\CommonHelper::new_rank());
            $userId = Auth::user()->id;
            $shipId = decrypt($shipId);
            // dd($shipId);
            $shipData = UserSeaService::where('id', $shipId)->first();
            $rateReview = RateReview::where('user_id', $userId)->where('user_sea_service_id', $shipId)->first();
            $data['shipData'] = View::make('admin.rate_review.ship_data', compact('shipData', 'rateReview', 'rankData'))->render();
            return $data;
        } catch (Exception $e) {
            return ['status' => false];
        }
    }

    public function rateData()
    {
        $rateData = RateReview::get();
        $rankData = \CommonHelper::new_rank();
        return view('admin.rate_review.rate_data', compact('rateData', 'rankData'));
    }

    public function showdata(Request $request)
    {
        $pageTitle = "Onboard Experience – feedback, review & rating | Flanknot";
        $metaDescription = "At Flanknot, Seafarer User can share Job Profile and Onboard Experience during contract.
            At Flanknot, Seafarer User can share Star Rating and Review of the Vessel of contract, the Vessel Owner Company of contract, the Vessel Manning Company of contract.";
        $metaKeywords = "Onboard Profile, Experience, review, rating";
        if(isset($request['share-experience-onboard']) && !empty($request['share-experience-onboard'])){
            $userId = \CommonHelper::decodeKey($request['share-experience-onboard']);
            $user_data = $this->userService->getDataByUserId($userId);
            $firstName = isset($user_data->first_name) ? $user_data->first_name : '';
        }else{
            $userId = Auth::user()->id;
            $firstName = Auth::user()->first_name;
        }
        $shipName = UserSeaService::with(['getRateData'])->where('user_id', $userId)->where('to', '!=', null)->orderBy('from', 'DESC')->orderby('to', 'DESC')->get();
        $firstShipName = $shipName->first();
        $fShipId = '';
        if($firstShipName){
            encrypt($firstShipName->id);
        }
        $sailingContracts = [];
        $ranks = [];
        $shipTypes = [];
        foreach ($shipName as $Key => $name) {
            if(isset($name->getRateData->experience) && !empty($name->getRateData->experience)){
                array_push($sailingContracts, [
                    "id" => $name->id,
                    "name" => $name->ship_name,
                    "to" => $name->to,
                    'total_duration' => $name->total_duration . ' (' . date("Y", strtotime($name->to)) . ')',
                    'average' => $name->getRateData ? (($name->getRateData->vessel_rate + $name->getRateData->manning_rate + $name->getRateData->company_rate) / 3) : 0.0,
                ]);
                array_push($shipTypes, $name->ship_type);
                array_push($ranks, $name->rank_id);
            }else{
                unset($shipName[$Key]);
            }
        }

        $shipTypes = array_unique($shipTypes);
        $ranks = array_unique($ranks);
        $optionShipTypes = $optionRanks = [];
        foreach($shipTypes as $ship) {
            foreach(\CommonHelper::ship_type() as $key => $type) {
                if($key == $ship) {
                    array_push($optionShipTypes, ["id"=> $ship, "value" => $type]);
                    break;
                }
            }
        }
        foreach($ranks as $rank) {
            foreach(\CommonHelper::new_rank() as $key => $default_ranks) {
                foreach($default_ranks as $innerKey => $dRank) {
                    if($innerKey == $rank) {
                        array_push($optionRanks, ["id"=> $rank, "value" => $dRank]);
                        break;
                    }
                }
            }
        }
        usort($sailingContracts, function ($a, $b) {return strcmp($b['average'], $a['average']);});
        $userIdEncodeKey = \CommonHelper::encodeKey($userId);
        if(count($shipName) > 0){
            $isData = 1;
        }else{
            $isData = 0;
        }
        return view('admin.rate_review.showdata', compact('shipName', 'firstShipName', 'fShipId', 'sailingContracts','optionShipTypes','optionRanks', 'firstName', 'userIdEncodeKey', 'pageTitle', 'metaDescription', 'metaKeywords', 'isData'));
    }

    public function showDataLoadMore(Request $request)
    {
        if(isset($request['share_experience_onboard']) && !empty($request['share_experience_onboard'])){
            $userId = \CommonHelper::decodeKey($request['share_experience_onboard']);
            $user_data = $this->userService->getDataByUserId($userId);
            $firstName = isset($user_data->first_name) ? $user_data->first_name : '';
        }else{
            $userId = Auth::user()->id;
            $firstName = Auth::user()->first_name;
        }
        
        $shipName = UserSeaService::with(['getRateData'])->
            where('user_id', $userId)->
            where('to', '!=', null);
            ;
        if($request->has('id')){
            $shipName->where("id", "<", $request->get('id'));
        }
        if($request->has('ship_type')){
            $shipName->where("ship_type",$request->get("ship_type"));
        }
        if($request->has('rank_id')){
            $shipName->where("rank_id",$request->get("rank_id"));
        }
        $shipName->orderBy('from', 'DESC')-> orderby('to', 'DESC');
        $html = "";
        $lastId = 0;
        foreach ($shipName->limit(5)->get() as $row) {
            if (isset($row->getRateData->experience) && !empty($row->getRateData->experience)) {
                $html .= view('admin.rate_review.showdata-item', ['row' => $row])->render();
                $lastId = $row->id;
            }
        }
        $totalRecords = $shipName->count();
        if($html == ""){
            $html = '<div class="row text-center" style="margin-left: 5px;margin-top: 10px;" >
                      No records found...
                   </div>';
        }
        if($totalRecords > 5) {
            $html .= '
            <div class="show_more_main" id="load_more_main' . $lastId . '">
                <button id="' . $lastId . '" class="show_more btn btn-link" title="Load more data">Load more</button>
                <span class="loding" style="display: none;"><i class="fa fa-spin fa-spinner"></i></span>
            </div>
            ';
        }
        return response()->json(["totalRecords" => $totalRecords, "html" => $html]);
    }

}
