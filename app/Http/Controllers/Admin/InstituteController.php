<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\UserService;
use App\Services\BatchService;
use App\Services\InstituteService;
use App\Services\AdvertiseService;
use App\Http\Requests;
use App\AdvertiseDetails;
use App\User;
use App\AdvertiserCourseDiscountMapping;
use Auth;
use View;
use Route;

class InstituteController extends Controller
{	
	private $userService;
	private $batchService;
	private $instituteService;

    public function __construct()
    {
        $this->userService = new UserService();
        $this->batchService = new BatchService();
        $this->advertiseService = new AdvertiseService();
        $this->instituteService = new InstituteService();
    } 

    public function viewInstituteList(Request $request){

    	if(Auth::check()){
            $filter = $request->toArray();
            $state =  \App\State::all()->toArray();
            $options['with'] = ['institute_registration_detail.institute_locations.state','institute_registration_detail.institute_locations.city','institute_registration_detail.institute_locations.pincode.pincodes_states.state','institute_registration_detail.institute_locations.pincode.pincodes_cities.city','institute_registration_detail.institute_detail','personal_detail','institute_registration_detail'];

            if (isset($filter) && !empty($filter)) {
                $result = $this->userService->getAllInstituteData($options,$filter);
            }else {
                $result = $this->userService->getAllInstituteData($options);
            }
            
            //dd($result->toArray());
            $list_search = View::make("admin.partials.list_institute")->with(['data' => $result]);
            $template = $list_search->render();

            if($request->isXmlHttpRequest()){
                return response()->json(['template' => $template], 200);
            }else{
                return view('admin.institute.list_institute',['data' => $result,'state' => $state,'filter' => $filter]);
            }
        }else{
            return redirect()->route('home');
        }
    }

    public function viewAllBatchesLists(Request $request){

    	if(Auth::check()){
            $course_name = '';
            $filter = '';
            $filter = $request->toArray();

            $options['with'] = ['institute_registration_detail.institute_courses','institute_registration_detail.institute_detail','institute_registration_detail','institute_registration_detail.user'];

            /*if (isset($filter) && !empty($filter)) {
                $result = $this->batchService->getAllBatches($options,$filter);
            }else {
                $result = $this->batchService->getAllBatches($options);
            }*/
            
            if(isset($filter) && !empty($filter)) {
                $result = $this->instituteService->getAllCourseDetailsByInstituteId('',$options,$filter);
            }else{
                $result = $this->instituteService->getAllCourseDetailsByInstituteId('',$options);
            }
            $result_data = $result->toArray();
            
            foreach ($result_data['data'] as $key => $value) {

                $location_options['with'] = ['institute_registration_detail.institute_locations.state','institute_registration_detail.institute_locations.city','institute_registration_detail.institute_locations.pincode.pincodes_states.state','institute_registration_detail.institute_locations.pincode.pincodes_cities.city'];

                $location = $this->userService->getDataByUserId($value['institute_registration_detail']['user_id'], $location_options)->toArray();

                if(isset($location[0]['institute_registration_detail']['institute_locations']))
                $result_data['data'][$key]['institute_registration_detail']['institute_location'] = $location[0]['institute_registration_detail']['institute_locations'];

            }
            if(isset($filter['course_type'])){
	            $course_name = $this->instituteService->getCourseNameByCourseType($filter['course_type'])->toArray();
	        }
	        
            $institutes = \App\InstituteRegistration::all()->toArray();
            $courses = \App\Courses::all()->toArray();
            
            $batch_search = View::make("admin.partials.list_batches")->with(['data' => $result,'result_data' => $result_data,'institutes' => $institutes,'courses' => $courses]);
            $template = $batch_search->render();


            if($request->isXmlHttpRequest()){
                return response()->json(['template' => $template], 200);
            }else{
                return view('admin.institute.list_batches',['data' => $result,'result_data' => $result_data,'institutes' => $institutes,'filter' => $filter,'course_name' => $course_name,'courses' => $courses]);
            }
        }else{
            return redirect()->route('home');
        }
    }

    public function addInstitute(){
        return view('admin.institute.add_institute');
    }

    public function editInstitute($institute_id){

        if(isset($institute_id) && !empty($institute_id)) {
            
            $options['with'] = ['institute_registration_detail.institute_detail','institute_registration_detail','institute_registration_detail.institute_locations.state','institute_registration_detail.institute_locations.city','institute_registration_detail.institute_locations.pincode.pincodes_states.state','institute_registration_detail.institute_locations.pincode.pincodes_cities.city'];

            $result = $this->userService->getDataByUserId($institute_id, $options)->toArray();
            $name = Route::currentRouteName();
            $result['current_route'] = $name;

            return view('admin.institute.add_institute', ['user_data' => $result]);
        } else {
            return redirect()->route('home');
        }

    }

    public function updateInstitute(Requests\UpdateInstituteRegistrationRequest $updateInstituteRegistrationRequest,$institute_id){
        $instituteDetails = $updateInstituteRegistrationRequest->toArray();
        $logged_institute_data['id'] = $institute_id;
        $instituteDetails['added_by'] = 'admin';

        $result = $this->instituteService->store($instituteDetails, $logged_institute_data);

        if($result['status'] == 'success'){
            return response()->json(['status' => 'success' , 'message' => 'Your institute details has been store successfully.','redirect_url' => route('admin.view.institutes')],200);
        }else{
            return response()->json(['status' => 'failed' , 'message' => 'Error while adding data.'],400);
        }
    }

    public function storeInstitute(Requests\InstituteRegistrationRequest $updateInstituteRegistrationRequest){
        $instituteDetails = $updateInstituteRegistrationRequest->toArray();
        $instituteDetails['added_by'] = 'admin';
        
        $result = $this->instituteService->store($instituteDetails);

        if($result['status'] == 'success'){
            return response()->json(['status' => 'success' , 'message' => 'Your institute details has been store successfully.','redirect_url' => route('admin.view.institutes')],200);
        }else{
            return response()->json(['status' => 'failed' , 'message' => 'Error while adding data.'],400);
        }
    }

    public function storeLogo(Request $request){

        $array['image-x'] = $request['image-x'];
        $array['image-y'] = $request['image-y'];
        $array['image-x2'] = $request['image-x2'];
        $array['image-y2'] = $request['image-y2'];
        $array['image-w'] = $request['image-w'];
        $array['image-h'] = $request['image-h'];
        $array['crop-w'] = $request['crop-w'];
        $array['crop-h'] = $request['crop-h'];
        $array['profile_pic'] = $request['profile_pic'];
        $array['role'] = 'admin';
 
        $result = $this->instituteService->uploadLogo($array);

        if( $result['status'] == 'success' ) {
            return response()->json($result, 200);
        } else {
            return response()->json($result, 400);
        }
    }

    public function addInstisuteBatch($user_id = NULL){
        $course_type = '';
        $institutes = \App\InstituteRegistration::all()->toArray();

        if(isset($user_id) && !empty($user_id)){
            $registration_data = $this->instituteService->getDetailsByCompanyId($user_id)->toArray();
            $institute_id = $registration_data[0]['id'];
            
            $course_type = [];

            $options['with'] = ['institute_registration_detail.institute_locations.state','institute_registration_detail.institute_locations.city','institute_registration_detail.institute_locations.pincode.pincodes_states.state','institute_registration_detail.institute_locations.pincode.pincodes_cities.city','institute_registration_detail.institute_courses'];

            $location = $this->userService->getDataByUserId($user_id, $options)->toArray();
            
            if(isset($location[0]['institute_registration_detail']['institute_courses']) AND !empty($location[0]['institute_registration_detail']['institute_courses'])){
                foreach ($location[0]['institute_registration_detail']['institute_courses'] as $key => $value) {
                    if($value['status'] != 0){
                        $course_type[$value['course_type']] = \CommonHelper::institute_course_types()[$value['course_type']];
                    }
                }
            }
            
            return view('admin.institute.add_batch',['institutes' => $institutes,'course_type' => $course_type,'location' => $location,'institute_id' => $institute_id]);
        }
        return view('admin.institute.add_batch',['institutes' => $institutes]);
    }

    public function getInstituteLocationByInstituteId($institute_id){//Auth::loginUsingId(107);
        if (Auth::check()) {

            $institute_registration = $this->instituteService->getDetailsById($institute_id)->toArray();

            if(isset($institute_registration[0]['user_id']))
                $institute_id = $institute_registration[0]['user_id'];

            $options['with'] = ['institute_registration_detail.institute_locations.state','institute_registration_detail.institute_locations.city','institute_registration_detail.institute_locations.pincode.pincodes_states.state','institute_registration_detail.institute_locations.pincode.pincodes_cities.city','institute_registration_detail.institute_courses'];
            $result = $this->userService->getDataByUserId($institute_id, $options)->toArray();
            $course_type = '';
            if(isset($result[0]['institute_registration_detail']['institute_courses']) AND !empty($result[0]['institute_registration_detail']['institute_courses'])){
                foreach ($result[0]['institute_registration_detail']['institute_courses'] as $key => $value) {
                    if($value['status'] != 0)
                        $course_type[$value['course_type']] = \CommonHelper::institute_course_types()[$value['course_type']];
                }
            }

            
            if(isset($result[0]['institute_registration_detail']['institute_locations']) AND !empty($result[0]['institute_registration_detail']['institute_locations'])){
                return response()->json(['location' => $result[0]['institute_registration_detail']['institute_locations'],'course_type' => $course_type], 200);
            }
        } else {
            return redirect()->route('site.login');
        }
    }

     public function storeCourseBatches(Request $request){
        $data = $request->toArray();
        
        $result = $this->batchService->store($data);

        if($result['status'] == 'success') {
            $result['redirect_url'] = route('admin.view.institute.batches');
            return response()->json($result, 200);
        }
        else {
            return response()->json($result, 400);
        }
    }

    public function editCourseBatches($institute_id,$batch_id){

        $institutes = \App\InstituteRegistration::all()->toArray();
        $course_type = [];
        // $options['with'] = ['institute_locations.state','institute_locations.city','institute_locations.pincode.pincodes_states.state','institute_locations.pincode.pincodes_cities.city'];

        // $location = $this->userService->getDataByUserId($institute_id, $options)->toArray();
        
        // $result = $this->batchService->getDataByBatchId($batch_id)->toArray();

        $options['with'] = ['courses'];
        
        $result = $this->instituteService->getAllCourseDetailsByInstituteCourseId($batch_id, $options)->toArray();
        
        $past_batches[0] = '';
        $past_batches = $this->instituteService->getPastCourseDetailsByInstituteCourseId($batch_id, $options)->toArray();
        
        $options['with'] = ['institute_registration_detail.institute_locations.state','institute_registration_detail.institute_locations.city','institute_registration_detail.institute_locations.pincode.pincodes_states.state','institute_registration_detail.institute_locations.pincode.pincodes_cities.city','institute_registration_detail.institute_courses'];

        $location = $this->userService->getDataByUserId($institute_id, $options)->toArray();


        if(isset($location[0]['institute_registration_detail']['institute_courses']) AND !empty($location[0]['institute_registration_detail']['institute_courses'])){
            foreach ($location[0]['institute_registration_detail']['institute_courses'] as $key => $value) {
                if($value['status'] != 0)
                    $course_type[$value['course_type']] = \CommonHelper::institute_course_types()[$value['course_type']];
            }
        }
        
        if(isset($location[0]['institute_registration_detail']))
            $institute_id = $location[0]['institute_registration_detail']['id'];

        $name = Route::currentRouteName();
        $result[0]['current_route'] = $name;
        // dd($result[0]);
        if($result){
            return view('admin.institute.add_batch',['data' => $result[0],'location' => $location,'institutes' => $institutes,'course_type' => $course_type,'institute_id' => $institute_id]);
        }
    }

    public function updateCourseBatches(Request $request,$batch_id){
        $data = $request->toArray();
        $data['institute_course_batch_id'] = $batch_id;
        //$data['batch_id'] = $batch_id;
        
        $result = $this->batchService->store($data);

        if($result['status'] == 'success') {
            $result['redirect_url'] = route('admin.view.institute.batches');
            return response()->json($result, 200);
        }
        else {
            return response()->json($result, 400);
        }
    }

    public function courseApplicantListing(Request $request){
        if(Auth::check()){
            $filter = '';
            $filter = $request->toArray();

            $paginate = env('PAGINATE_LIMIT',10);
            $options['with'] = ['user.professional_detail','institute_details','batch_details','batch_details.course_details'];
            
            $data = $this->instituteService->getAllUserAppliedCourses('', $options, $paginate,$filter);

            $course_applicant_data = $data->toArray();
            // dd($course_applicant_data);
            $course_name = \App\InstituteCourses::all()->toArray();
            $all_courses = \App\Courses::all()->toArray();

            $batch_search = View::make("admin.partials.course_applicant_listing")->with(['data' => $course_applicant_data['data'] , 'search_data' => '', 'pagination_data' => $course_applicant_data    , 'pagination' => $data, 'course_list' => $course_name,'all_courses' => $all_courses]);
            $template = $batch_search->render();
            // \CommonHelper::dd($course_applicant_data);
            if($request->isXmlHttpRequest()){
                return response()->json(['template' => $template], 200);
            }else{
                return view('admin.institute.course_applicant_listing',['data' => $course_applicant_data['data'] , 'search_data' => '', 'pagination_data' => $course_applicant_data, 'pagination' => $data, 'course_list' => $course_name,'all_courses' => $all_courses,'filter' => $filter]);
            }

        }else{
           return response()->json(['status' => 'error', 'message' => 'Please login to continue.'], 400); 
        }

    }

    public function view($institute_id){

        if(Auth::check() && isset($institute_id)){
            $options['with'] = ['personal_detail'];
            $result['user'] = $this->userService->getDataByUserId($institute_id, $options)->toArray();
            
            $options['with'] = ['institute_registration_detail.institute_detail','institute_registration_detail','institute_registration_detail.institute_locations.state','institute_registration_detail.institute_locations.city','institute_registration_detail.institute_locations.pincode.pincodes_states.state','institute_registration_detail.institute_locations.pincode.pincodes_cities.city','institute_registration_detail.institute_courses.course_details'];
            $result['institute_data'] = $this->userService->getDataByUserId($institute_id, $options)->toArray();
            
            $institutes = \App\InstituteRegistration::all()->toArray();

            $filter['no_pagination'] = 1;

            $options['with'] = ['institute_batches.institute_location'];

            $registration_data = $this->instituteService->getDetailsByCompanyId($institute_id)->toArray();

            if(isset( $registration_data[0]['id']))
                $reg_id = $registration_data[0]['id'];
            else
                return redirect()->route('home');
           
            $data = $this->instituteService->getAllCourseDetailsByInstituteId($reg_id,$options,$filter);

            $result['institute_data'][0]['institute_registration_detail']['data_array'] = $data->toArray();
            $all_courses = \App\Courses::all()->toArray();

            foreach ($result['institute_data'][0]['institute_registration_detail']['data_array'] as $key => $value) {
                $location_options['with'] = ['institute_registration_detail.institute_locations.state','institute_registration_detail.institute_locations.city','institute_registration_detail.institute_locations.pincode.pincodes_states.state','institute_registration_detail.institute_locations.pincode.pincodes_cities.city'];

                $location = $this->userService->getDataByUserId($institute_id, $location_options)->toArray();
                
                $result['institute_data'][0]['institute_registration_detail']['data_array'][$key]['institute_location'] = $location[0]['institute_registration_detail']['institute_locations'];
            }

            $name = Route::currentRouteName();
            $result['current_route'] = $name;

            return view('admin.institute.view_institute',['data' => $result,'institutes' => $institutes,'all_courses' => $all_courses]);
        }else{
            return redirect()->route('home');
        }
    }

    public function viewCourses(){

        if(Auth::check()){
            $institute_id = Auth::User()->id;
        }
        $options = ['institute_registration_details','institute_registration_details.institute_details','institute_registration_details.course_type_details','institute_registration_details.courses','institute_registration_details.user'];
        $courses = \App\Courses::all()->toArray();
        $institutes = \App\InstituteRegistration::all()->toArray();

        $data = $this->instituteService->getAllCourseDetailsByInstituteId($institute_id,$options);
       
        $result = $data->toArray();
        return view('admin.institute.view_courses',['data' => $result,'pagination' => $data,'pagination_data' => $result,'courses' => $courses,'institutes' => $institutes]);
    }

    public function disableCourse($course_id){
        $result = $this->instituteService->disableCourseByCourseId($course_id);

        if($result['status'] == 'success') {
            return response()->json(['status' => 'success', 'message' => 'Course has been deactivated.','redirect_url' => route('home')], 200);
        } else {
            return response()->json(['status' => 'error', 'message' => 'There was some error while deactivating course.'], 400);
        }

    }

    public function enableCourse($course_id){
        $result = $this->instituteService->enableCourseByCourseId($course_id);

        if($result['status'] == 'success') {
            return response()->json(['status' => 'success', 'message' => 'Course has been activated.','redirect_url' => route('home')], 200);
        } else {
            return response()->json(['status' => 'error', 'message' => 'There was some error while activating course.'], 400);
        }

    }

    public function storeCourse(Request $request){

        $data = $request->toArray();
        
        $result = $this->instituteService->storeCourses($data);
        if($result['status'] == 'success') {
            return response()->json(['status' => 'success', 'message' => 'Course has beed added.'], 200);
        } else {
            return response()->json(['status' => 'error', 'message' => 'There was some error while adding course.'], 400);
        }
    }

    public function deleteCourse($course_id,$institute_id){

        $result = $this->instituteService->deleteCourse($course_id,$institute_id);
        if($result['status'] == 'success') {
            return response()->json(['status' => 'success', 'message' => 'Course has beed deleted successfully.'], 200);
        } else {
            return response()->json(['status' => 'error', 'message' => 'There was some error while deleting course.'], 400);
        }
    }

    public function addCourseDiscountMapping(Request $request){
        $course_type = '';
        $course_name = '';
        $institutes = \App\InstituteRegistration::all()->toArray();

        $filter = $request->toArray();
        $advertiser_list = $this->advertiseService->getAllAdvertisementsWithActiveAdvertisements()->toArray();

        $existing_course_discounts = $this->instituteService->getExistitngCourseDiscount($filter);

        $course_discount = View::make("admin.partials.course_discount_mapping")->with(['existing_course_discounts' => $existing_course_discounts]);
        $template = $course_discount->render();

        if($request->isXmlHttpRequest()){
            return response()->json(['template' => $template], 200);
        }

        if(isset($filter['course_type'])){
            $course_name = $this->instituteService->getCourseNameByCourseType($filter['course_type'])->toArray();
        }

        //dd($existing_course_discounts->toArray());
        return view('admin.institute.course_discount_mapping',['existing_course_discounts' => $existing_course_discounts,'institutes' => $institutes,'advertiser_list' => $advertiser_list,'filter' => $filter,'course_name' => $course_name]);
    }

    public function getInstituteByCourse(Request $request){

        $data = $request->toArray();

        if(isset($data['institute_id'])){
            if(empty($data['institute_id'])){
                $institute_list = '';
            }else{
                $institute_list = $this->instituteService->getInstituteListByCourse($data['course_type'],$data['course'],$data['institute_id'])->toArray();
            }
        }else{
            $institute_list = $this->instituteService->getInstituteListByCourse($data['course_type'],$data['course'])->toArray();
        }

        return response()->json(['status' => 'success', 'list' => $institute_list], 200);
    }

    public function storeCourseDiscountMapping(Request $request){

        $data = $request->toArray();
        $already_not_present = True;

        if(isset($data['existing_id']) && !empty($data['existing_id'])){
            AdvertiserCourseDiscountMapping::where('id',$data['existing_id'])->delete();
        }

        foreach ($data['institute_list'] as $key => $institute) {
            
            foreach ($data['advertise_list'] as $advertise) {
                
                $result = AdvertiserCourseDiscountMapping::where('batch_id',$institute)->where('advertise_id',$advertise)->first();

                if($result){
                    $already_not_present = False;
                }
            }
        }

        if($already_not_present){
            foreach ($data['institute_list'] as $key => $institute) {
                
                foreach ($data['advertise_list'] as $advertise) {
                    $adv_discount['batch_id'] = $institute;
                    $adv_discount['advertise_id'] = $advertise;
                    $adv_discount['discount'] = $data['discount'];

                    $result = AdvertiserCourseDiscountMapping::create($adv_discount);
                }
            }
        }

        $existing_course_discounts = AdvertiserCourseDiscountMapping::with('company_registration','batch_details.course_details.courses')->orderBy('id','desc')->paginate(10);

        //dd(123,$advertiser_list);

        $course_discount = View::make("admin.partials.course_discount_mapping")->with(['existing_course_discounts' => $existing_course_discounts]);
        $template = $course_discount->render();

        if(!$already_not_present){
            return response()->json(['template' => $template,'status' => 'failed','message' => 'Course discount already added.'], 400);
        }


        if($result){

            if($request->isXmlHttpRequest()){
                return response()->json(['template' => $template,'status' => 'success','message' => 'Course discount added successfully.'], 200);
            }

            return response()->json(['status' => 'success','message' => 'Course discount added successfully.'], 200);
        }
    }

    public function editCourseDiscountMapping(Request $request){
        $data = $request->toArray();
        $option_template = '';

        $result = AdvertiserCourseDiscountMapping::with('company_registration','batch_details.course_details.courses')->where('id',$data['id'])->first()->toArray();

        if(isset($result['batch_details']['course_details']['course_type'])){
            $course_list = $this->instituteService->getCourseNameByCourseType($result['batch_details']['course_details']['course_type'])->toArray();

            foreach ($course_list as $key => $value) {
                $option_template = $option_template."<option value='".$value['id']."'>".$value['course_name']."</option>";
            }
        }

        $data['course_option_template'] = $option_template;
        $data['course_type'] = $result['batch_details']['course_details']['course_type'];
        $data['course_id'] = $result['batch_details']['course_details']['course_id'];
        $data['discount'] = $result['discount'];
        $data['advertise_id'] = $result['advertise_id'];
        $data['batch_id'] = $result['batch_details']['id'];
        $data['institute_id'] = $result['batch_details']['course_details']['institute_id'];
        
        $data['institute_list'] = $this->instituteService->getInstituteListByCourse($data['course_type'],$data['course_id'])->toArray();

        //dd($institute_list);

        return response()->json(['data' => $data], 200);
    }

    public function deleteCourseDiscountMapping(Request $request){

        $data = $request->toArray();

        $result = AdvertiserCourseDiscountMapping::where('id',$data['discount_id'])->delete();

        if($result) {

            $existing_course_discounts = AdvertiserCourseDiscountMapping::with('company_registration','batch_details.course_details.courses')->orderBy('id','desc')->paginate(10);

            $course_discount = View::make("admin.partials.course_discount_mapping")->with(['existing_course_discounts' => $existing_course_discounts]);
            $template = $course_discount->render();

            return response()->json(['status' => 'success','template' => $template, 'message' => 'Course has been deleted successfully.'], 200);
        } else {
            return response()->json(['status' => 'error', 'message' => 'Course not found.'], 400);
        }
    }
}
