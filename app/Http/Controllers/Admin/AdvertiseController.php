<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Services\UserService;
use App\Services\CompanyService;
use App\Services\AdvertiseService;
use Auth;
use Route;
use View;

class AdvertiseController extends Controller
{
    private $userService;
	private $companyService;

    public function __construct()
    {
        $this->userService = new UserService();
        $this->companyService = New CompanyService();
        $this->advertiseService = New AdvertiseService();
    } 

    public function store(Request $request){
    	$data = $request->toArray();

    	if(isset($data['login_data']) && !empty($data['login_data'])){
            $login_data = explode('&', $data['login_data']);
    		foreach ($login_data as $key => $value) {
    			$array_value = explode('=', $value);
    			$login[$array_value['0']] = str_replace("+"," ",$array_value['1']);
    		}
        }
		$login['registered_as'] = 'advertiser';
        $login['added_by'] = 'admin';

        if(isset($data['company_data']) && !empty($data['company_data'])){
    		
            $d = str_replace('%5B','[', $data['company_data']);
            $d = str_replace('%5D',']', $d);
            $company_data = str_replace('%40','@', $d);

            $company_data = explode('&', $company_data);

    		foreach ($company_data as $key => $value) {
    			$company_array_value = explode('=', $value);
    			$company[$company_array_value['0']] = str_replace("+"," ",$company_array_value['1']);
    		}
        }
		$company['registered_as'] = 'advertiser';
        $company['added_by'] = 'admin';

        if(isset($login['email']))
		  $login['email'] = str_replace("%40","@",$login['email']);

		if(isset($login['uploaded-file-path'])){
			$login['uploaded-file-path'] = str_replace("%5C","/",$login['uploaded-file-path']);
			$login['uploaded-file-path'] = str_replace("%2F","",$login['uploaded-file-path']);
		}
        
       

		if(isset($login['user_id'])){
			$login_user['id'] = $login['user_id'];
			$result = $this->userService->store($login,$login_user);
		}else
    		$result = $this->userService->store($login);

    	if($result){
    		if(isset($result['user']['id'])){
    			$data['id'] = $result['user']['id'];
    		}else{
	    		$data = $result['user']->toArray();
    		}
    		if(isset($company['uploaded-file-path'])){
                $company['uploaded-file-path'] = str_replace("%5C","/",$company['uploaded-file-path']);
                $company['uploaded-file-path'] = str_replace("%2F","",$company['uploaded-file-path']);
            }

            if(isset($company['company_address'])){
                $company['company_address'] = str_replace("%5C","/",$company['company_address']);
                $company['company_address'] = str_replace("%2F","",$company['company_address']);
                $company['company_address'] = str_replace("%2C",",",$company['company_address']);
            }
            if(isset($company['product_description'])){
                $company['product_description'] = str_replace("%5C","/",$company['product_description']);
                $company['product_description'] = str_replace("%2F","",$company['product_description']);
                $company['product_description'] = str_replace("%2C",",",$company['product_description']);
            }
            if(isset($company['bussiness_description'])){
	    		$company['bussiness_description'] = str_replace("%5C","/",$company['bussiness_description']);
                $company['bussiness_description'] = str_replace("%2F","",$company['bussiness_description']);
				$company['bussiness_description'] = str_replace("%2C",",",$company['bussiness_description']);
			}
	    	
	    	$result = $this->companyService->store_company_details($company,$data['id']);
	       
            if($result){
                return response()->json(['status' => 'success' , 'message' => 'Your company details has been store successfully.','redirect_url' => route('admin.view.advertiser')],200);
            }else{
                return response()->json(['status' => 'failed' , 'message' => 'Error while adding data.'],400);
            }
    	}
    }

    public function edit($user_id){
    	if(Auth::check() && isset($user_id)){

            $options['with'] = ['personal_detail'];
            $result['user'] = $this->userService->getDataByUserId($user_id, $options)->toArray();
            

            $options['with'] = ['company_registration_detail.company_detail','company_registration_detail','company_registration_detail.company_locations.state','company_registration_detail.company_locations.city','company_registration_detail.company_locations.pincode.pincodes_states.state','company_registration_detail.company_locations.pincode.pincodes_cities.city'];

          	$result['company_data'] = $this->userService->getDataByUserId($user_id, $options)->toArray();
            
          	$name = Route::currentRouteName();
            $result['current_route'] = $name;

            return view('admin.advertiser.add_advertiser',['user_data' => $result]);
        }else{
            return redirect()->route('home');
        }
    }

    public function view($user_id){
        
    	if(Auth::check() && isset($user_id)){

            $options['with'] = ['personal_detail'];
            $result['user'] = $this->userService->getDataByUserId($user_id, $options)->toArray();
            

            $options['with'] = ['company_registration_detail.company_detail','company_registration_detail','company_registration_detail.company_locations.state','company_registration_detail.company_locations.city','company_registration_detail.company_locations.pincode.pincodes_states.state','company_registration_detail.company_locations.pincode.pincodes_cities.city'];
          	$result['company_data'] = $this->userService->getDataByUserId($user_id, $options)->toArray();
            
          	$name = Route::currentRouteName();
            $result['current_route'] = $name;

            return view('admin.advertiser.view_advertiser',['data' => $result]);
        }else{
            return redirect()->route('home');
        }
    }

    public function viewAdvertiserList(Request $request){
        if(Auth::check()){
            $state =  \App\State::all()->toArray();
            $filter = $request->toArray();

            $options['with'] = ['personal_detail','company_registration_detail.company_detail','company_registration_detail'];

            if (isset($filter) && !empty($filter)) {
                $result = $this->userService->getAllAdvertiserData($options,$filter);
            }else {
                $result = $this->userService->getAllAdvertiserData($options);
            }
            
            $job_search = View::make("admin.partials.advertiser_list")->with(['data' => $result]);
            $template = $job_search->render();
            
            if($request->isXmlHttpRequest()){
                return response()->json(['template' => $template], 200);
            }else{
                return view('admin.advertiser.list_advertiser',['data' => $result,'filter' => $filter,'state' => $state]);
            }
        }else{
            return redirect()->route('home');
        }
    }

    public function addAdvertise(){
        $state =  \App\State::all()->toArray();

        $options['with'] = ['personal_detail','company_registration_detail.company_detail','company_registration_detail'];

        $result = $this->userService->getAllAdvertiserDataWithoutFilter($options)->toArray();

        return view('admin.advertiser.add_advertise',['state' => $state,'advertiser_list' => $result]);
    }

    public function storeAdvertise(Request $request){
        if(Auth::check()){
            
            $data = $request->toArray();

            $result = $this->advertiseService->store($data);
            
            if($result['status'] == 'success'){
                return response()->json(['status' => 'success' , 'message' => 'Your advertisement details has been store successfully.','redirect_url' => route('admin.view.advertisements')],200);
            }else{
                return response()->json(['status' => 'failed' , 'message' => 'Error while adding data.'],400);
            }

        }else{
            return redirect()->route('home');
        }
    }

    public function enquiryList(){
        
        $paginate = env('PAGINATE_LIMIT',10);

        $result = $this->advertiseService->getAllAdvertiseEnquiries($paginate);

        return view('admin.advertiser.enquiry_list',['data' => $result]);
    }

    public function advertisementsList(Request $request){
        $paginate = env('PAGINATE_LIMIT',10);

        $filter = $request->toArray();

        if (isset($filter) && !empty($filter)) {
            $result = $this->advertiseService->getAllAdvertisementDetails($paginate,$filter);
        }else {
            $result = $this->advertiseService->getAllAdvertisementDetails($paginate);
        }
        
        $advertisements = View::make("admin.partials.advertisement_list")->with(['data' => $result]);
        $template = $advertisements->render();

        if($request->isXmlHttpRequest()){
            return response()->json(['template' => $template], 200);
        }else{
            return view('admin.advertiser.advetisement_list',['data' => $result,'filter' => $filter]);
        }
    }

    public function changeStatus(Request $request){
        $data = $request->toArray();

        $result = $this->advertiseService->changeStatus($data);

        if($result['status'] == 'success'){

            return response()->json(['status' => 'success' , 'message' => 'status changed successfully'],200);
        }else{
            return response()->json(['status' => 'failed' , 'message' => 'Error.'],400);
        }
    }
}
