<?php

namespace App\Http\Controllers\Admin;

use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Services\UserService;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class GraphController extends Controller
{
    private $userService;
    public function __construct()
    {
//        $this->middleware('auth');
        $this->userService = new UserService();

    }
    public function createGraphData($userId = null){
        if($userId == null){
            $userId = Auth::id();
        }
        return User::whereId($userId)->with([
                'sea_service_detail' => function ($query) {
                    $query->where('to', '!=', null);
                }
            ])->get()->map(function ($q) use($userId){
            $contract['current_rank']=$q->professional_detail->current_rank;
            $total_contract=0;
            $current_rank_contract = 0;
            $diff=0;
            $current_rank_diff=0;
            if(empty($q->sea_service_detail[0])){
                return $contract['is_graph'] = false;
            }
            $contract['is_graph'] = true;
            foreach ($q->sea_service_detail as $key=>$value){
                $total_contract+=1;
                $from = Carbon::createFromFormat('Y-m-d', $value->from);
                $to = Carbon::createFromFormat('Y-m-d', $value->to);
                if ($value->rank_id==$contract['current_rank']) {
                    $current_rank_contract += 1;
                    $current_rank_diff += $from->diffInDays($to)+1;
                }
                $diff+= $from->diffInDays($to)+1;
            }
            $contract['total_contract']=$total_contract;
            $contract['total_duration']=$diff;
            $contract['current_rank_contract']=$current_rank_contract;
            $contract['current_rank_duration']=$current_rank_diff;
            $userOptions['with'] = ['personal_detail','professional_detail','passport_detail','seaman_book_detail','coc_detail','gmdss_detail','wkfr_detail','personal_detail.state','personal_detail.city','personal_detail.pincode.pincodes_states.state','personal_detail.pincode.pincodes_cities.city','sea_service_detail','course_detail','cop_detail','coe_detail','document_permissions.requester.company_registration_detail','document_permissions.requester.institute_registration_detail','document_permissions.document_type.type'];
            $user_data = $this->userService->getDataByUserId($userId, $userOptions)->toArray();
            $currentUserRank = $user_data[0]['professional_detail']['current_rank'];
            $all_engine_type=$q->sea_service_detail->groupBy('engine_type');
            foreach ($all_engine_type as $key=>$item) {
                if($key!=''){
                    $contract['engine'][$key]=0;
                    foreach($item as $engine_type){
                        if($key == 'other'){
                            // $contract['engine'][$engine_type->other_engine_type];
                        }
                        $from = Carbon::createFromFormat('Y-m-d', $engine_type->from);
                        $to = Carbon::createFromFormat('Y-m-d', $engine_type->to);
                        $contract['engine'][$key]+= $from->diffInDays($to)+1;
                    }
                }
            }
//             dd($contract['engine']);
            if (!empty($contract['engine'])) {
                ksort($contract['engine']);
                $_sort_engines = array_keys($contract['engine']);
                $emptyEngineType=$contract['engine'];
                $newRank = [5,6,9,10,11,12,13,16,17,54];
                $userRankView = array_merge(range(9, 22), range(31, 48)); //ignored ranks
                array_push($userRankView,50,51,52,54);
                $_filtered_engine = array_merge(range(9, 22), range(35, 48)); //ignored ranks
                array_push($_filtered_engine,32,50,51,52,54);
//                 dd($_filtered_engine);
                if(!in_array($currentUserRank,$userRankView)){
                    foreach ($q->sea_service_detail->groupBy('engine_type', 'rank_id') as $all_rank_key => $all_rank_array) {
                        foreach ($all_rank_array as $all_rank_array_value) {
                            if (!in_array($all_rank_array_value->rank_id, $_filtered_engine)) {
                                $_sort_rank = array_search($all_rank_key, $_sort_engines);

                                $contract['all_engine'][$all_rank_array_value->rank_id][$_sort_rank]['all_days'] = (isset($contract['all_engine'][$all_rank_array_value->rank_id][$_sort_rank]['all_days']) ? $contract['all_engine'][$all_rank_array_value->rank_id][$_sort_rank]['all_days'] : 0);
                                $from = Carbon::createFromFormat('Y-m-d', $all_rank_array_value->from);
                                $to = Carbon::createFromFormat('Y-m-d', $all_rank_array_value->to);
                                $contract['all_engine'][$all_rank_array_value->rank_id][$_sort_rank]['all_days'] += $from->diffInDays($to) + 1;
                                $contract['all_engine'][$all_rank_array_value->rank_id][$_sort_rank]['engine_type'] = $all_rank_array_value->engine_type;
                            }
                        }
                    }
//                    if (!in_array($all_rank_array_value->rank_id, $_filtered_engine)) {
                    if (!empty($contract['all_engine'])) {
                        foreach ($contract['all_engine'] as $_ranks_key => $_ships_value) {
                            for ($_count = 0; $_count < count($_sort_engines); $_count++) {
                                if (empty($_ships_value[$_count])) {
                                    $contract['all_engine'][$_ranks_key][$_count]['all_days'] = 0;
                                    $contract['all_engine'][$_ranks_key][$_count]['engine_type'] = 0;
                                }
                            }

                        }
                    }
                    
//                    if(!in_array($all_rank_array_value->rank_id,$_filtered_engine)){
                    if (!empty($contract['all_engine'])) {
                        foreach ($contract['all_engine'] as $_ranks_key=>$_engine_value){
                            for ($_count = 0; $_count < count($_sort_engines); $_count++) {
                                if (!empty($_engine_value[$_count])) {
                                    $engineType = $contract['all_engine'][$_ranks_key][$_count]['engine_type'];
                                    unset($emptyEngineType[$engineType]);
                                }
                            }
                        }
                    }
                    if(!empty($emptyEngineType) && !empty($contract['all_engine'])){
                        $removeEngineKey = [];
                        $key = 0;
                        foreach ($contract['engine'] as $checkEngineKey => $checkEngineValue){
                            if (array_key_exists($checkEngineKey,$emptyEngineType)){
                                $removeEngineKey[] = $key;
                            }
                            $key++;
                        }
//                    dd($removeEngineKey);
                        foreach ($contract['all_engine'] as $_ranks_key=>$_engine_value){
                            foreach ($removeEngineKey as $removeEngineK => $removeEngineV){
                                unset($contract['all_engine'][$_ranks_key][$removeEngineV]);
                            }
                        }
                        foreach ($emptyEngineType as $emptyEngineTypeK => $emptyEngineTypeV){
                            unset($contract['engine'][$emptyEngineTypeK]);
                        }
                    }
                }
            }

//            dd($contract);
            $all_ship_type=$q->sea_service_detail->groupBy('ship_type');
            foreach ($all_ship_type as $key=>$item) {
                $contract['ships'][$key]=0;
                foreach ($item as $ship_type){
                    $from = Carbon::createFromFormat('Y-m-d', $ship_type->from);
                    $to = Carbon::createFromFormat('Y-m-d', $ship_type->to);
                    $contract['ships'][$key]+= $from->diffInDays($to)+1;
                }
            }
            ksort($contract['ships']);
            $contract['sea_career'] = $contract['ships'];
            $_sort_ships=array_keys($contract['ships']);
            $emptyShipId=$contract['ships'];
            $_filtered_rank=array_merge(range(18,23),range(31,52)); //ignored ranks
            if(!in_array($currentUserRank,$_filtered_rank)){
                foreach ($q->sea_service_detail->groupBy('ship_type','rank_id') as $all_rank_key => $all_rank_array){
                    foreach ($all_rank_array as $all_rank_array_value){
                        if(!in_array($all_rank_array_value->rank_id,$_filtered_rank)){
                            $_sort_rank = array_search($all_rank_key, $_sort_ships);

                            $contract['all_ship'][$all_rank_array_value->rank_id][$_sort_rank]['all_days'] = (isset($contract['all_ship'][$all_rank_array_value->rank_id][$_sort_rank]['all_days']) ? $contract['all_ship'][$all_rank_array_value->rank_id][$_sort_rank]['all_days'] : 0);
                            $from = Carbon::createFromFormat('Y-m-d', $all_rank_array_value->from);
                            $to = Carbon::createFromFormat('Y-m-d', $all_rank_array_value->to);
                            $contract['all_ship'][$all_rank_array_value->rank_id][$_sort_rank]['all_days'] += $from->diffInDays($to) + 1;
                            $contract['all_ship'][$all_rank_array_value->rank_id][$_sort_rank]['ship_id'] = $all_rank_array_value->ship_type;
                        }
                    }
                }
//                if(!in_array($all_rank_array_value->rank_id,$_filtered_rank)){
                if(!empty($contract['all_ship'])){
                    foreach ($contract['all_ship'] as $_ranks_key=>$_ships_value){
                        for($_count=0;$_count<count($_sort_ships);$_count++){
                            if (empty($_ships_value[$_count])){
                                $contract['all_ship'][$_ranks_key][$_count]['all_days']=0;
                                $contract['all_ship'][$_ranks_key][$_count]['ship_id']=0;
                            }
                        }

                    }
                }
//                if(!in_array($all_rank_array_value->rank_id,$_filtered_rank)){
                if(!empty($contract['all_ship'])){
                    foreach ($contract['all_ship'] as $_ranks_key=>$_ships_value){
                        for($_count=0;$_count<count($_sort_ships);$_count++){
                            if(!empty($contract['all_ship'][$_ranks_key][$_count]['ship_id'])){
                                $ship_id = $contract['all_ship'][$_ranks_key][$_count]['ship_id'];
                                unset($emptyShipId[$ship_id]);
                            }
                        }

                    }
                }
                if(!empty($emptyShipId) && !empty($contract['all_ship'])){
                    $removeShipKey = [];
                    $key = 0;
                    foreach ($contract['ships'] as $checkShipKey => $checkShipValue){
                        if (array_key_exists($checkShipKey,$emptyShipId)){
                            $removeShipKey[] = $key;
                        }
                        $key++;
                    }
                    $contractAllShip = $contract['all_ship'];
                    foreach ($contractAllShip as $_ranks_key=>$_ships_value){
                        foreach ($removeShipKey as $removeShipK => $removeShipV){
                            unset($contract['all_ship'][$_ranks_key][$removeShipV]);
                        }
                    }
                    foreach ($emptyShipId as $emptyShipIdK => $emptyShipIdKV){
                        unset($contract['ships'][$emptyShipIdK]);
                    }
                }
            }
            return $contract;
        })->first();
    }
    public function index(){
        $pageTitle = "Infographics – Sailing summary, Engine and Ship type experience as graphs |Flanknot ";
        $metaDescription = "At Flanknot, The Seafarer User experience is depicted as Graphs and charts.";
        $metaKeywords = "Sailing Summary, pie chart, ship type experience, engine type experience, bar graph";
        $user_id = Auth::id();
        $user_contract= $this->createGraphData();
        // dd($user_contract);
        return view('admin.infographic.graph',compact('user_contract', 'user_id', 'pageTitle', 'metaDescription', 'metaKeywords'));
    }
    
     public function shareGraph(Request $request){
        $pageTitle = "Infographics – Sailing summary, Engine and Ship type experience as graphs |Flanknot";
        $metaDescription = "At Flanknot, view Seafarer experience depicted as Graphs and charts.";
        $metaKeywords = "Sailing Summary, pie chart, ship type experience, engine type experience, bar graph";
        $user_id = \CommonHelper::decodeKey($request['graph']);
        if(!isset($user_id)){
            $user_id = '';
        }
        $user_contract= $this->createGraphData($user_id);
        return view('admin.infographic.graph',compact('user_contract', 'user_id', 'pageTitle', 'metaDescription', 'metaKeywords'));
    }
}
