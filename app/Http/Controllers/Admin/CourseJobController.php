<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Requests\CompanyDetailRequest;
use App\Http\Requests\CourseJobRequest;
use Crypt;
use Auth;
use Route;
use View;
use App\CourseJob;
use App\CompanyEmailAddress;
use App\SendedJob;


class CourseJobController extends Controller
{

    function __construct()
    {
        $this->modelObj = new CourseJob;
    }
    /**
     * Display a listing of the resource.
     * @author Chirag G
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        return view('admin.job.job.index');
    } 

    /**
     * Get Company List for datatable function
     * @author Chirag G
     * @return JSON Object
     **/
    public function get(Request $request)
    {
        return $this->modelObj->get($request);
    }

    public function create(){

        $encryptedId = "";
        return view('admin.job.job.addedit',compact('encryptedId'));
    }    

    public function edit($id){

        $courseJob = $this->modelObj->edit(Crypt::decrypt($id));
        $encryptedId = $id;
        return view('admin.job.job.addedit',compact('encryptedId','courseJob'));
    }

    public function store(CourseJobRequest $request){      
        return $this->modelObj->saveUpdateCourseJob($request);
    }

    public function update(CourseJobRequest $request, $id)
    {
        return $this->modelObj->saveUpdateCourseJob($request,$id);
    }

    public function destroy(Request $request, $id){

        $request['checkbox']=[$id];
        return $this->modelObj->destroy($request);   
    }

    public function deleteJob($id){      
        return (new CourseJob)->deleteCompanyJobContact($id);
    }

    public function getCompanyEmailAddress(Request $request){

        $input = $request->all();
        
        if (isset($input['course_job_id']) && !empty($input['course_job_id'])) {
            

            $data = $this->modelObj->where('id',$input['course_job_id'])->firstOrFail();
            $setAddress = $data->company_email_address_id;

        }else{

            $setAddress = null;
        }

        $emailAddress = CompanyEmailAddress::where('company_id',$request->company_id)->pluck('email','id')->toArray();

        return view('admin.job.job._get_company_address',compact('emailAddress','setAddress'));
    }

    /**
     * Display a listing of the resource.
     * @author Chirag G
     * @return \Illuminate\Http\Response
     */
    public function sendIndex()
    {   
        return view('admin.job.job._send_job');
    } 

    /**
     * Get Company List for datatable function
     * @author Chirag G
     * @return JSON Object
     **/
    public function sendGet(Request $request)
    {
        return $this->modelObj->sendGet($request);
    }

    /**
     * Display a listing of the resource.
     * @author Chirag G
     * @return \Illuminate\Http\Response
     */
    public function viewJobAll()
    {   
        return view('admin.job.job.view._all');
    } 

    /**
     * Get Company List for datatable function
     * @author Chirag G
     * @return JSON Object
     **/
    public function getViewAllJob(Request $request)
    {
        return $this->modelObj->getViewAllJob($request);
    }

    /**
     * Display a listing of the resource.
     * @author Chirag G
     * @return \Illuminate\Http\Response
     */
    public function viewJobCompany()
    {   
        return view('admin.job.job.view._company');
    } 

    /**
     * Get Company List for datatable function
     * @author Chirag G
     * @return JSON Object
     **/
    public function getViewCompanyJob(Request $request)
    {
        return $this->modelObj->getViewCompanyJob($request);
    }

    /**
     * Display a listing of the resource.
     * @author Chirag G
     * @return \Illuminate\Http\Response
     */
    public function viewJobUser()
    {   
        return view('admin.job.job.view._user');
    } 

    /**
     * Get Company List for datatable function
     * @author Chirag G
     * @return JSON Object
     **/
    public function getViewAllUser(Request $request)
    {
        return $this->modelObj->getViewAllUser($request);
    }
    /**
     * Get Company List for datatable function
     * @author Chirag G
     * @return JSON Object
     **/
    public function preview(Request $request)
    {
        $job = $this->modelObj->edit($request->course_job_id);        

        return view('admin.job.job._preview',compact('job'));
    }
    
    public function jobSendToUser(Request $request){
        
        return (new SendedJob)->jobSendToUser($request);
    }
    
}
