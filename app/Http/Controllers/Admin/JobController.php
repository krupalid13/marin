<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\CompanyService;
use App\Services\JobService;
use route;
use View;

class JobController extends Controller
{	
	private $companyService;

    function __construct()
    {
        $this->companyService = New CompanyService();
        $this->jobService = new JobService();
    }

    public function addCompanyJob(){
    	$result =  \App\CompanyRegistration::all()->where('type','!=','advertiser')->toArray();
    	return view('admin.company.add_company_jobs',['company_list' => $result]);
    }

    public function getShipDetailsByCompanyId($ship_id){
    	
    	$ship_data = $this->companyService->getCompanyShipDetailsById($ship_id)->toArray();
		
        if(isset($ship_data[0]['ship_type']) && !empty($ship_data[0]['ship_type'])){
            foreach ($ship_data[0]['ship_type'] as $key => $value) {
                $ships[] = $value['ship_type'];
                $ship_name[$value['id']] = $value['ship_name'];
            }
        }
        $options = [];
        $ship_names = [];
        if(isset($ships) && !empty($ships)){
	        foreach (\CommonHelper::ship_type() as $key => $value) {
	        	if(in_array($key, $ships)){
	        		$options[] = "<option value=$key>$value</option>";
	        	}
	        }
	    }

        if(isset($ship_name) && !empty($ship_name)){
            foreach ($ship_name as $key => $value) {
                $ship_names[] = "<option value=$key>$value</option>";
            }
        }
        
        if(isset($ships) && !empty($ships)){
        	return response()->json(['status' => 'success', 'message' => 'ship Details found.','options' => $options,'ship_names' => $ship_names], 200);
        }else{
        	return response()->json(['status' => 'success', 'message' => 'ship Details not found.'], 400);
        }
    }

    public function getShipDetailsByshipId($company_id,$ship_type){
        if(isset($company_id) && !empty($company_id)){
            $ship_data = $this->companyService->getCompanyShipDetailsById($company_id)->toArray();
            if(isset($ship_data[0]['ship_type']) && !empty($ship_data[0]['ship_type'])){
                foreach ($ship_data[0]['ship_type'] as $key => $value) {
                    if($value['ship_type'] == $ship_type){
                        $ship_details = $value; 
                    }
                }
            }
            
            if(isset($ship_details) && !empty($ship_details)){
                return response()->json(['status' => 'success', 'message' => 'ship Details found.','ship_details' => $ship_details], 200);
            }else{
                return response()->json(['status' => 'failed', 'message' => 'ship Details not found.'], 400);
            }
        }
    }

    public function store(Request $request){
    	
        if(isset($request) && !empty($request)){
        	$data = $request->toArray();
            $result = $this->jobService->store($data);
        }

        if($result['status'] == 'success') {
            return response()->json(['status' => 'success', 'message' => 'New job added.','redirect_url' => route('admin.view.company.jobs')], 200);
        } else {
            return response()->json(['status' => 'error', 'message' => 'There was some error while adding job.'], 400);
        }
    }

    public function viewALLJobs(Request $request){
        
        $filter = '';
        if(isset($request) && !empty($request))
            $filter = $request->toArray();

    	$paginate = env('PAGINATE_LIMIT',10);
        $data = $this->jobService->getJobDetailsByPaginate($paginate,$filter);

        $result = $data->toArray();
       
        $company_list =  \App\CompanyRegistration::all()->where('type','!=','advertiser')->toArray();

        $job_list =  \App\CompanyJob::orderBy('id','desc')->get()->toArray();
        

        $job_search = View::make("admin.partials.job_listing")->with(['data' => $result['data'],'paginate' => $data,'pagination_data'=>$result,'company_list' => $company_list,'job_list' => $job_list]);
        $template = $job_search->render();

        if($request->isXmlHttpRequest()){
            return response()->json(['template' => $template], 200);
        }else{
            return view('admin.company.job_listing', ['data' => $result['data'],'paginate' => $data,'pagination_data'=>$result,'company_list' => $company_list,'job_list' => $job_list]);
        }
    }

    public function editCompanyJob($company_id,$job_id){

    	if(isset($job_id) && !empty($job_id)){
            $ships = [];
            $job_data = $this->jobService->getJobDetailsByJobId($job_id)->toArray();
            $name = Route::currentRouteName();
            $job_data[0]['current_route'] = $name;
            $job_data[0]['job_id'] = $job_id;

            $ship_data = $this->companyService->getCompanyShipDetailsById($company_id)->toArray();
            $result =  \App\CompanyRegistration::all()->where('type','!=','advertiser')->toArray();

            if(isset($ship_data[0]['ship_type']) && !empty($ship_data[0]['ship_type'])){
                foreach ($ship_data[0]['ship_type'] as $key => $value) {
                    $ships[] = $value['ship_type'];
                    $job_data[0]['ship_type'] = $value['ship_type'];
                }
            }

            return view('admin.company.add_company_jobs' , ['job_data' => $job_data,'ship_data' => $ship_data,'ships' => $ships,'company_list' => $result]);
        }
    }

    public function updateCompanyJob(Request $request,$job_id){
    	
    	$job_data = $request->toArray();
    	$job_data['job_id'] = $job_id;
        unset($job_data['_token']);
        
        $result = $this->jobService->update($job_data);
            
        if($result['status'] == 'success') {
            return response()->json(['status' => 'success', 'message' => 'Job Details has been updated.','redirect_url' => route('admin.view.company.jobs')], 200);
        } else {
            return response()->json(['status' => 'error', 'message' => 'There was some error while adding job.'], 400);
        }
    }

    public function disableJob($job_id){
        $result = $this->jobService->disableJobByJobId($job_id);

        if($result['status'] == 'success') {
            return response()->json(['status' => 'success', 'message' => 'Job has been disabled.','redirect_url' => route('home')], 200);
        } else {
            return response()->json(['status' => 'error', 'message' => 'There was some error while disabling job.'], 400);
        }

    }

    public function enableJob($job_id){
        $result = $this->jobService->enableJobByJobId($job_id);

        if($result['status'] == 'success') {
            return response()->json(['status' => 'success', 'message' => 'Job has been enabled.','redirect_url' => route('home')], 200);
        } else {
            return response()->json(['status' => 'error', 'message' => 'There was some error while disabling job.'], 400);
        }

    }

    public function viewALLJobApplicant(Request $request){

        $filter = '';
        if(isset($request) && !empty($request))
            $filter = $request->toArray();

        $paginate = env('PAGINATE_LIMIT',10);
        $options['with'] = ['user.professional_detail','company_details','company_job'];

        $data = $this->jobService->getAllUserAppliedJobs('', $options, $paginate, $filter);

        $job_applicant_data = $data->toArray();

        $job_list =  \App\CompanyJob::orderBy('id','desc')->get()->toArray();

        $job_search = View::make("admin.partials.job_applicant_listing")->with(['data' => $job_applicant_data['data'] , 'search_data' => '', 'pagination_data' => $job_applicant_data, 'pagination' => $data,'job_list' => $job_list]);
        $template = $job_search->render();
        
        if($request->isXmlHttpRequest()){
            return response()->json(['template' => $template], 200);
        }else{
            return view('admin.company.job_applicant_listing',['data' => $job_applicant_data['data'] , 'search_data' => '', 'pagination_data' => $job_applicant_data, 'pagination' => $data,'job_list' => $job_list,'filter' => $filter]);
        }

    }
    
}
