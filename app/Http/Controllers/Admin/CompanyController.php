<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\CompanyService;
use App\Services\AdvertiseService;
use App\Services\UserService;
use App\Services\SubscriptionService;
use App\Services\DocumentService;
use App\Http\Requests;
use Auth;
use Route;
use View;
use Maatwebsite\Excel\Facades\Excel as Excel;
use App\Company;
use App\Http\Requests\CompanyDetailRequest;
use Crypt;
use App\CompanyEmailAddress;
use App\CompanyContacts;

class CompanyController extends Controller
{

    private $companyService;
	private $advertiseService;
    private $documentService;

    function __construct()
    {
        $this->companyService = New CompanyService();
        $this->advertiseService = New AdvertiseService();
        $this->userService = new UserService();
        $this->SubscriptionService = new SubscriptionService();
        $this->documentService = new DocumentService();
        $this->modelObj = new Company;
    }

    /*public function storeCompanyDetails(Requests\CompanyRegistrationRequest $companyRegistrationRequest){*/
    public function storeCompanyDetails(Request $companyRegistrationRequest, $type, $company_id = NULL){
    	$company_data = $companyRegistrationRequest->toArray();
    	$company_data['added_by'] = 'admin';
    	$company_data['registered_as'] = 'company';

        if($type == 'basic_details'){
    	   $result = $this->companyService->store($company_data);
        }
        if($type == 'location_details'){
            if(isset($company_id)){
                $company_result['id'] = $company_id;
                $registration_data = $this->companyService->getDetailsByCompanyId($company_id);
                
                $result = $this->companyService->store_location_ship_details($company_data,$registration_data);
                $free_subscription = $this->SubscriptionService->storeFreeSubscription('company',$company_id)->toArray();
            }else{
                return response()->json(['status' => 'failed' , 'message' => 'Error while adding data.'],400);
            }
        }

        if($result['status'] == 'success'){
            return response()->json(['status' => 'success' , 'message' => 'Your company details has been store successfully.','redirect_url' => route('admin.view.company'),'company_id' => isset($result['id']) ? $result['id'] : ''],200);
        }else{
            return response()->json(['status' => 'failed' , 'message' => 'Error while adding data.'],400);
        }
    }

    public function editcompany($company_id){
    	if (Auth::check()) {
            $options['with'] = ['company_registration_detail.company_detail','company_registration_detail','company_registration_detail.company_ship_details','company_registration_detail.company_locations.state','company_registration_detail.company_documents','company_registration_detail.company_locations.city','company_registration_detail.company_locations.pincode.pincodes_states.state','company_registration_detail.company_locations.pincode.pincodes_cities.city'];

            $result = $this->userService->getDataByUserId($company_id, $options)->toArray();

            $name = Route::currentRouteName();
            $result['current_route'] = $name;

            return view('admin.company.add_company', ['user_data' => $result]);
        } else {
            return redirect()->route('home');
        }
    }

    public function updateCompany(Request $updateCompanyRegistrationRequest, $type, $company_id){
    	$company_data = $updateCompanyRegistrationRequest->toArray();
        $logged_company_data = [];

        if (isset($company_id) && !empty($company_id)) {
            $logged_company_data[0]['id'] = $company_id;
            $logged_company_data['id'] = $company_id;

            if($type == 'basic_details'){
                $company_data['added_by'] = 'admin';
                $company_data['registered_as'] = 'company';
                $result = $this->companyService->store($company_data, $logged_company_data);
            }
            if($type == 'location_details'){
               $registration_data = $this->companyService->getDetailsByCompanyId($company_id);
	           $result = $this->companyService->store_location_ship_details($company_data,$registration_data);
            }
            
	        if($result['status'] == 'success'){
	            return response()->json(['status' => 'success' , 'message' => 'Your company details has been store successfully.','redirect_url' => route('admin.view.company'),'company_id' => isset($company_id) ? $company_id : ''],200);
	        }else{
	            return response()->json(['status' => 'failed' , 'message' => 'Error while adding data.'],400);
	        }
	    }
        else{
            return response()->json(['status' => 'failed' , 'message' => 'Error while adding data.'],400);
        }
    }

    public function storeProfilePic(Request $request){

    	$array['image-x'] = $request['image-x'];
        $array['image-y'] = $request['image-y'];
        $array['image-x2'] = $request['image-x2'];
        $array['image-y2'] = $request['image-y2'];
        $array['image-w'] = $request['image-w'];
        $array['image-h'] = $request['image-h'];
        $array['crop-w'] = $request['crop-w'];
        $array['crop-h'] = $request['crop-h'];
        $array['profile_pic'] = $request['profile_pic'];
        $array['role'] = 'admin';

        $result = $this->companyService->uploadLogo($array);

        if( $result['status'] == 'success' ) {
            return response()->json($result, 200);
        } else {
            return response()->json($result, 400);
        }
    }

    public function viewcompany(Request $request){

        $state =  \App\State::all()->toArray();

        $filter = $request->toArray();
        $options['with'] = ['company_registration_detail.company_detail','company_registration_detail','company_registration_detail.company_ship_details','company_registration_detail.company_locations.state','company_registration_detail.company_documents','company_registration_detail.company_locations.city','company_registration_detail.company_locations.pincode.pincodes_states.state','company_registration_detail.company_locations.pincode.pincodes_cities.city','company_registration_detail.associate_agent','company_registration_detail.appointed_agent'];

        if (isset($filter) && !empty($filter)) {
            $result = $this->userService->getAllCompanyDataByFilters($filter,$options);
        }else {
            $result = $this->userService->getAllCompanyData($options);
        }

        $job_search = View::make("admin.partials.company_list")->with(['company_data' => $result]);
        $template = $job_search->render();

        if($request->isXmlHttpRequest()){
            return response()->json(['template' => $template], 200);
        }else{
            return view('admin.company.list_company',['company_data' => $result,'state' => $state,'data' => $filter]);
        }

    }

    public function city_list($state_id){
        
        $city =  \App\City::where('state_id', $state_id)->get()->toArray();
        return $city;
    }

    public function view($company_id){

        $options['with'] = ['company_registration_detail.company_detail','company_registration_detail','company_registration_detail.company_ship_details','company_registration_detail.company_locations.state','company_registration_detail.company_documents','company_registration_detail.company_locations.city','company_registration_detail.company_locations.pincode.pincodes_states.state','company_registration_detail.company_locations.pincode.pincodes_cities.city','company_registration_detail.associate_agent','company_registration_detail.appointed_agent','company_registration_detail.company_team.location.state','company_registration_detail.company_team.location.city'];

        $result = $this->userService->getDataByUserId($company_id,$options)->toArray();
        $company_list =  \App\CompanyRegistration::all()->where('type','!=','advertiser')->toArray();
       
        if(isset($result[0]['company_registration_detail']['company_ship_details']) && !empty($result[0]['company_registration_detail']['company_ship_details'])){
            foreach ($result[0]['company_registration_detail']['company_ship_details'] as $key => $value) {
            
                if($value['scope_type'] == 'owner'){
                    $result[0]['owner_ship'][] = $value;
                }
                if($value['scope_type'] == 'manager'){
                    $result[0]['manager_ship'][] = $value;
                }
            }
        }

        return view('admin.company.view_company',['data' => $result,'company_list' => $company_list,'hide_company_name' => '1']);
    }

    public function addAdvertise(){
        $result = '';
        $company_list =  \App\CompanyRegistration::all()->where('type','!=','advertiser')->toArray();
        if (Auth::check()) {
            $user_id = Auth::user()->id;
            $options['with'] = ['company_registration_detail.advertisment_details'];
            $result = $this->userService->getDataByUserId($user_id, $options)->toArray();
        }
        
        return view('admin.company.add_advertise',['data' => $result,'company_list' => $company_list]);
    }

    public function storeAdvertise(Request $request){
        if (Auth::check()) {

            $data = $request->toArray();

            $result = $this->companyService->storeAdvertise($data,$data['company_id']);
            if( $result['status'] == 'success' ) {
                if(isset($data['company_id']) && !empty($data['company_id'])){
                    return response()->json(['redirect_url' => route('admin.list.company.advertise')], 200);
                }
                return response()->json(['redirect_url' => route('site.show.company.details')], 200);
            } else {
                return response()->json($result, 400);
            }
        }else{
            return response()->json(['message' => 'Please Login'], 400);
        }
    }

    public function ListAdvertise(Request $request){

        $filter = $request->toArray();

        $advertisement_details = $this->advertiseService->getAllAdvertisementCompanyDetails();
        
        if(isset($advertisement_details) && !empty($advertisement_details) && count($advertisement_details) > 0){
            $advertisement_details = $advertisement_details->toArray();
        }else{
            $advertisement_details = '';
        }
        
        if(isset($filter) && !empty($filter)){
            $result = $this->companyService->getCompanyAdvertises($filter);
        }else{
            $result = $this->companyService->getCompanyAdvertises();
        }

        // dd($result->toArray());

        $advertisement = View::make("admin.partials.company_list_advertisements")->with(['data' => $result]);
        $template = $advertisement->render();

        if($request->isXmlHttpRequest()){
            return response()->json(['template' => $template], 200);
        }else{
            return view('admin.company.list_advertisements',['data' => $result,'advertisement_details' => $advertisement_details,'filter' => $filter]);
        }
    }

    public function downloadXL(Request $request){
        $filter = $request->toArray();
        $options['with'] = ['company_registration_detail','company_registration_detail.company_detail','company_registration_detail.company_ship_details','company_registration_detail.company_locations.state','company_registration_detail.company_documents','company_registration_detail.company_locations.city','company_registration_detail.company_locations.pincode.pincodes_states.state','company_registration_detail.company_locations.pincode.pincodes_cities.city'];

        if (isset($filter) && !empty($filter)) {
            $result = $this->userService->getAllCompanyDataByFiltersWithoutPagination($filter,$options);
        }else {
            $result = $this->userService->getAllCompanyDataByFiltersWithoutPagination('',$options);
        }
        
        Excel::create('Company Details', function($excel) use($result){

            $excel->sheet('New sheet', function($sheet) use($result) {

                $sheet->loadView('admin.company.company_excel',array('data' => $result));

            });

        })->download('xlsx');
    }

    public function ListDownloadedDocuments(){
        $options['with'] = ['requester.company_registration_detail','owner'];
        $list = $this->documentService->ListDownloadedDocuments($options);
        /*dd($list->toArray());*/
        return view('admin.company.list_downloaded_documents',['document_list' => $list]);
    }

    public function getVesselByVesselId(Request $request,$company_id){

        if(Auth::check()){
            $data = $request->toArray();
           
            $result = $this->companyService->getShipDetailsByshipId($data['vessel_id'],$company_id);

            if($result['status'] == 'success'){
                return response()->json(['result' => $result],200);
            }else{
                return response()->json(['result' => $result],400);
            }
        }
        else{
            return response()->json(['status' => 'failed' , 'message' => 'Error while adding data.'],400);
        }
    }

    public function storeShipDetails(Request $request,$company_id)
    {
        if(Auth::check()){
            $data = $request->toArray();
            $data['company_id'] = $company_id;

            $result = $this->companyService->storeShipDetails($data);

            $ship_details = $this->companyService->getShipDetails($data['company_id'],$data['scope_type'])->toArray();
            $template = '';

            if(isset($ship_details) && !empty($ship_details)){
                $vessels = View::make("site.partials.company_vessel_results")->with(['ship_details' => $ship_details]);
                $template = $vessels->render();
            }

            if($result['status'] == 'success'){
                return response()->json(['status' => 'success' , 'message' => 'Your ship details has been store successfully.', 'template' => $template],200);
            }else{
                return response()->json(['status' => 'failed' , 'message' => 'Error while adding data.'],400);
            }
        }
    }

    public function deleteVesselDetails(Request $request,$company_id){

        if(Auth::check()){
            $data = $request->toArray();
            
            $result = $this->companyService->deleteVesselDetails($data['vessel_id'],$company_id);

            if($result['status'] == 'success'){
                return response()->json($result, 200);
            }else{
                return response()->json($result, 400);
            }
        }
        else{
            return response()->json(['status' => 'failed' , 'message' => 'Error while adding data.'],400);
        }
    } 

    public function getAgentByAgentId(Request $request,$company_id){

        if(Auth::check()){
            $data = $request->toArray();
            
            $result = $this->companyService->getAgentDetailsByAgentId($data['agent_id']);

            if($result['status'] == 'success'){
                return response()->json(['result' => $result],200);
            }else{
                return response()->json(['result' => $result],400);
            }
        }
        else{
            return response()->json(['status' => 'failed' , 'message' => 'Error while adding data.'],400);
        }
    }

    public function storeAgents(Request $request,$company_id){

        if(Auth::check()){
            $data = $request->toArray();
            $data['company_id'] = $company_id;
            
            $result = $this->companyService->storeAgents($data);

            $agent_details = $this->companyService->getAgentsByAgentType($data['company_id'],$data['agent_type'])->toArray();
            $template = '';

            if(isset($agent_details) && !empty($agent_details)){
                $agents = View::make("site.partials.company_agent")->with(['data' => $agent_details]);
                $template = $agents->render();
            }

            if($result['status'] == 'success'){
                return response()->json(['status' => 'success' , 'message' => 'Your agent details has been store successfully.', 'template' => $template],200);
            }else{
                return response()->json(['status' => 'failed' , 'message' => 'Error while adding data.'],400);
            }
        }
    }

    public function deleteAgentDetails(Request $request,$company_id){

        if(Auth::check()){
            $data = $request->toArray();
            
            $result = $this->companyService->deleteAgentDetails($data['agent_id'],$company_id);

            if($result['status'] == 'success'){
                return response()->json($result, 200);
            }else{
                return response()->json($result, 400);
            }
        }
        else{
            return response()->json(['status' => 'failed' , 'message' => 'Error while adding data.'],400);
        }
    } 

    public function uploadAgentProfilePic(Request $request)
    {
        if(Auth::check()){
            $user = Auth::user()->toArray();
            $user_id = $user['id'];
        }

        $array['image-x'] = $request['image-x'];
        $array['image-y'] = $request['image-y'];
        $array['image-x2'] = $request['image-x2'];
        $array['image-y2'] = $request['image-y2'];
        $array['image-w'] = $request['image-w'];
        $array['image-h'] = $request['image-h'];
        $array['crop-w'] = $request['crop-w'];
        $array['crop-h'] = $request['crop-h'];
        $array['profile_pic'] = $request['profile_pic'];

        $result = $this->companyService->uploadAgentProfilePic($array);

        if( $result['status'] == 'success' ) {
            return response()->json($result, 200);
        } else {
            return response()->json($result, 400);
        }
    }

    public function updateTeamDetails(Request $request,$company_id)
    {
        if(Auth::check()){
            $data = $request->toArray();
            $result = $this->companyService->getTeamDataByTeamId($data['team_id']);

            if($result){
                return response()->json( ['status' => 'success' , 'message' => 'Data is successfully saved.','team_data' => $result[0]],200);
            }else{
                return response()->json( $result,400);
            }
        }
        else{
            return response()->json(['status' => 'failed' , 'message' => 'Error while adding data.'],400);
        }
    }

    public function uploadTeamProfilePic(Request $request)
    {
        if(Auth::check()){
            $user = Auth::user()->toArray();
            $user_id = $user['id'];
        }

        $array['image-x'] = $request['image-x'];
        $array['image-y'] = $request['image-y'];
        $array['image-x2'] = $request['image-x2'];
        $array['image-y2'] = $request['image-y2'];
        $array['image-w'] = $request['image-w'];
        $array['image-h'] = $request['image-h'];
        $array['crop-w'] = $request['crop-w'];
        $array['crop-h'] = $request['crop-h'];
        $array['profile_pic'] = $request['profile_pic'];

        $result = $this->companyService->uploadTeamProfilePic($array, $user_id);

        if( $result['status'] == 'success' ) {
            return response()->json($result, 200);
        } else {
            return response()->json($result, 400);
        }
    }

    public function storeTeamDetails(Request $request)
    {
        $data = $request->toArray();
        if($data['exiting_team_id']){
            $result = $this->companyService->updateTeamDetailsByTeamId($data);
        }
        else{
            $result = $this->companyService->storeTeamDetails($data);
        }

        if (Auth::check()) {
            $user_id = Auth::user()->id;
            $team_data_by_company_id = $this->companyService->getTeamDataByCompanyId($data['company_id'])->toArray();
        }

        $template = '';

        if(isset($result) && !empty($result)){
            $team_data = View::make("site.partials.company_team_details")->with(['user_data' => $team_data_by_company_id]);
            $template = $team_data->render();
        }

        if($result['status'] == 'success'){
            return response()->json(['status' => 'success' , 'message' => 'Your team details has been store successfully.', 'template' => $template],200);
        }else{
            return response()->json(['status' => 'failed' , 'message' => 'Error while adding data.'],400);
        }

    }

    public function deleteTeamDetails(Request $request,$company_id)
    {
        if(Auth::check()){
            $data = $request->toArray();
            $user = Auth::user()->toArray();
            $user_id = $user['id'];
            $team_id = $data['team_id'];

            $team_location_by_company_id = $this->companyService->getLocationDataByCompanyId($company_id)->toArray();

            $user_company_id = $team_location_by_company_id['0']['id'];

            if($user_company_id == $company_id){
                $result = $this->companyService->deleteTeamDetails($team_id,$company_id);
            }

            if($result){
                return response()->json(['status' => 'Success' , 'message' => 'Team details has been deleted successfully.'] ,200);
            }else{
                return response()->json(['status' => 'failed' , 'message' => 'Error while deleting the data.'],400);
            }
        }
        else{
            return response()->json(['status' => 'failed' , 'message' => 'Error while deleting the data.'],400);
        }
    }
    

    /**
     * Display a listing of the resource.
     * @author Chirag G
     * @return \Illuminate\Http\Response
     */
    public function getCompanyList()
    {   
        return view('admin.job.company.index');
    } 

    /**
     * Get Company List for datatable function
     * @author Chirag G
     * @return JSON Object
     **/
    public function getAjaxCompanyList(Request $request)
    {
        return $this->modelObj->getAjaxCompanyListDatatable($request);
    }

    public function createCompany(){

        $encryptedId = "";
        return view('admin.job.company.addedit',compact('encryptedId'));
    }    

    public function editcompanyDetail($id){

        $company = $this->modelObj->editcompanyDetail(Crypt::decrypt($id));
        $encryptedId = $id;
        return view('admin.job.company.addedit',compact('encryptedId','company'));
    }

    public function storeCompanyDetail(CompanyDetailRequest $request){       
        return $this->modelObj->saveUpdateCompany($request);
    }

    public function updateCompanyDetail(CompanyDetailRequest $request, $id)
    {
        return $this->modelObj->saveUpdateCompany($request,$id);
    }

    public function deleteCompanyDetail(Request $request, $id){       
        $request['checkbox']=[$id];
        return $this->modelObj->deleteAll($request);   
    }

    public function deleteCompanyEmailAddress($id){
        
        return (new CompanyEmailAddress)->deleteCompanyEmailAddress($id);
    }

    public function deleteCompanyContacts($id){
        
        return (new CompanyContacts)->deleteCompanyContact($id);
    }

    
}
