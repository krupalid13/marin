<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\PromotionalEmailService;
use App\PromotionalEmail;
use route;
use View;
use Illuminate\Support\Facades\Input;

class PromotionalEmailController extends Controller {

    private $promotionalEmailService;
    private $promotionalEmailObj;

    function __construct() {
        $this->promotionalEmailService = new PromotionalEmailService();
        $this->promotionalEmailObj = new PromotionalEmail;
    }

    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\Response
     */
    public function index() {
        return view('admin.email.index');
    }

    public function send() {
        return view('admin.email.send');
    }

    public function sendEmail() {
        $emails = Input::get('email');
        if (isset($emails[0]) && !empty($emails[0])) {
            $result = $this->promotionalEmailService->sendEmail($emails);
        }
//        return response()->json(['success' => true,'msg'=>$msg, 'status'=>1,'errors' => $errors]);;
        if ($result['status'] == 'success') {
            return response()->json(['success' => true, 'status' => 1, 'msg' => 'Email Send Successfully.', 'redirect_url' => route('admin.email')], 200);
        } else {
            return response()->json(['status' => 'Error', 'errors' => 'There was some error while sending email.'], 400);
        }
    }

    /**
     * Get email List for datatable function
     * @return JSON Object
     * */
    public function get(Request $request) {
        return $this->promotionalEmailObj->get($request);
    }

}
