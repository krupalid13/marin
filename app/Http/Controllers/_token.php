<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class WebsiteController extends Controller
{
    public function home()
	{
		return view('site.home');
	}

	public function institute()
	{
		return view('site.institute.institute');
	}
	public function services()
	{
		return view('site.service');
	}
	public function uploads(){
		return view ('site.institute.upload_file');
	}
	public function profileview()
	{
		return view ('site.user.my_profile');
	}
}
