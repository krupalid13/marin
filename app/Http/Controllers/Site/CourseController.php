<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\CourseService;
use Auth;

class CourseController extends Controller
{
    private $courseService;

    function __construct() {
        $this->courseService = new CourseService();
    }

    public function seafarerCourseApply(Request $request){
        if(Auth::check()){
            $user_id = Auth::User()->id;
           
            if(isset($request->course_id)){
                $course_application['course_id'] = $request->course_id;
                $course_application['institute_id'] = $request->institute_id;
                $course_application['user_id'] = $user_id;

                $result = $this->courseService->applyForCourse($course_application);
                if($result['status'] == 'success') {
                    return response()->json(['status' => 'success', 'message' => 'Course Applied Successfully.'], 200);
                } else {

                    if(isset($result['prev_applied'])){
                        return response()->json(['status' => 'error', 'message' => 'You have already applied for this course.'], 400);
                    }
                    return response()->json(['status' => 'error', 'message' => 'There was some error while applying for course.'], 400);
                }
            }else{
                return response()->json(['status' => 'error', 'message' => 'Error while applying for course.'], 400); 
            }
        }else{
           return response()->json(['status' => 'error', 'message' => 'Please login to continue.', 'redirect' => route('site.login')], 400); 
        }
    }
}
