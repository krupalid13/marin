<?php

namespace App\Http\Controllers\Site;

use App\Services\CandidateService;
use App\Services\UserService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\CompanyService;
use App\Services\DocumentService;
use App\Services\CompanyDownloadsService;
use App\Services\SendEmailService;
use App\Services\SubscriptionService;
use App\CompanyRegistration;

use App\Http\Requests;
use Auth;
use Route;
use View;
use URL;
use DB;
use Response;
use ZipArchive;
use RecursiveIteratorIterator;
use RecursiveDirectoryIterator;
use Redirect;
use Barryvdh\DomPDF\Facade as PDF;

class CompanyController extends Controller
{
    private $companyService;
    private $companyDownloadsService;
    private $userService;
    private $candidateService;
    private $sendEmailService;
    private $subscriptionService;
    private $documentService;

    function __construct()
    {
        $this->companyService = New CompanyService();
        $this->userService = New UserService();
        $this->candidateService = New CandidateService();
        $this->companyDownloadsService = New CompanyDownloadsService();
        $this->sendEmailService = new SendEmailService();
        $this->subscriptionService = new SubscriptionService();
        $this->documentService = new DocumentService();
    }

    public function registration()
    {
        if (Auth::check()) {
            return redirect()->route('home');
        }
        return view('site.registration.company_registration');
    }

    public function store(Requests\CompanyRegistrationRequest $companyRegistrationRequest)
    {
        $companyDetails = $companyRegistrationRequest->toArray();
        $logged_company_data = "";

        $companyDetails['otp'] = $this->userService->generateOTP();
        if (Auth::check()) {
            $logged_company_data = Auth::User()->toArray();
            $logged_company_data['otp'] = $companyDetails['otp'];
        }
        $result = $this->companyService->store($companyDetails, $logged_company_data);

        if($result['status'] == 'success'){
            return response()->json(['status' => 'success' , 'message' => 'Your company details has been store successfully.','redirect_url' => route('home')],200);
        }else{
            return response()->json(['status' => 'failed' , 'message' => 'Error while adding data.'],400);
        }
    }

    public function store_location_ship_details(Request $request){
        $data = $request->toArray();
        if(Auth::check()){
            $logged_company_data = $this->companyService->getDataByUserId(Auth::User()->id);

            if(!empty($logged_company_data) && count($logged_company_data) > 0)
                $logged_company_data = $logged_company_data->toArray();

            $result = $this->companyService->store_location_ship_details($data,$logged_company_data);
        }

        $parsed = parse_url( URL::previous());

        $subscriptions = $this->subscriptionService->getAllActiveSubscription('company');
        
        if($parsed['path'] == '/company/profile/edit'){
            $redirect_to = route('site.show.company.details');
        } elseif (!empty($subscriptions)) {
           $redirect_to = route('site.user.subscription');
        } else{
            $redirect_to = route('home',['auto_action' => 'welcome']);
        }

        if($result['status'] == 'success'){
            return response()->json(['status' => 'success' , 'message' => 'Your company location and ship details has been store successfully.','redirect_url' => $redirect_to],200);
        }else{
            return response()->json(['status' => 'failed' , 'message' => 'Error while adding data.'],400);
        }
    }

    public function view(){
        
        if (Auth::check()) {
            $user_id = Auth::user()->id;            
            $options['with'] = ['company_registration_detail.company_detail','company_registration_detail','company_registration_detail.company_ship_details','company_registration_detail.company_locations.state','company_registration_detail.company_documents','company_registration_detail.company_locations.city','company_registration_detail.company_locations.pincode.pincodes_states.state','company_registration_detail.company_locations.pincode.pincodes_cities.city','company_registration_detail.advertisment_company_details','company_registration_detail.next_company_jobs.user_applied_jobs','company_registration_detail.associate_agent','company_registration_detail.appointed_agent','company_registration_detail.company_locations.team_members'];

            $result = $this->userService->getDataByUserId($user_id, $options)->toArray();
             
            if(isset($result[0]['company_registration_detail']['advertisment_company_details']) && !empty($result[0]['company_registration_detail']['advertisment_company_details']))
            $result[0]['advertisment_company_details'] = end($result[0]['company_registration_detail']['advertisment_company_details']);

            if(isset($result[0]['company_registration_detail']['company_locations']) && !empty($result[0]['company_registration_detail']['company_locations'])){

                foreach ($result[0]['company_registration_detail']['company_locations'] as $key => $location) {
                    if($location['is_headoffice'] == '1'){
                        $result[0]['head_branch'][] = $location;
                    }else{
                        $result[0]['branch'][] = $location;
                    }
                }

            }
            
            if(isset($result[0]['company_registration_detail']['company_ship_details']) && !empty($result[0]['company_registration_detail']['company_ship_details'])){
                foreach ($result[0]['company_registration_detail']['company_ship_details'] as $key => $value) {
                
                    if($value['scope_type'] == 'owner'){
                        $result[0]['owner_ship'][] = $value;
                    }
                    if($value['scope_type'] == 'manager'){
                        $result[0]['manager_ship'][] = $value;
                    }
                }
            }

            return view('site.company.company_profile', ['data' => $result, 'user_id' => $user_id]);
        } else {
            return redirect()->route('home');
        
        }
    }

    public function viewProfile($id){
        if (isset($id)) {
            $user_id = '';
            $role = '';
            $available_user = \App\User::find($id);

            if(Auth::check()){
                $role = Auth::user()->registered_as;
                $user_id = Auth::user()->id;
            }

            if(isset($available_user) && !empty($available_user) && $available_user->registered_as == 'company'){
                $options['with'] = ['company_registration_detail.company_detail','company_registration_detail','company_registration_detail.company_ship_details','company_registration_detail.company_locations.state','company_registration_detail.company_documents','company_registration_detail.company_locations.city','company_registration_detail.company_locations.pincode.pincodes_states.state','company_registration_detail.company_locations.pincode.pincodes_cities.city','company_registration_detail.advertisment_company_details','company_registration_detail.next_company_jobs.user_applied_jobs','company_registration_detail.associate_agent','company_registration_detail.appointed_agent','company_registration_detail.company_locations.team_members'];
                $result = $this->userService->getDataByUserId($id, $options)->toArray();
               
                if(isset($result[0]['company_registration_detail']['advertisment_company_details']) && !empty($result[0]['company_registration_detail']['advertisment_company_details'])){
                    $result[0]['advertisment_company_details'] = end($result[0]['company_registration_detail']['advertisment_company_details']);
                }  

                if(isset($result[0]['company_registration_detail']['company_locations']) && !empty($result[0]['company_registration_detail']['company_locations'])){

                    foreach ($result[0]['company_registration_detail']['company_locations'] as $key => $location) {
                        if($location['is_headoffice'] == '1'){
                            $result[0]['head_branch'][] = $location;
                        }else{
                            $result[0]['branch'][] = $location;
                        }
                    }

                }

                if(isset($result[0]['company_registration_detail']['company_ship_details']) && !empty($result[0]['company_registration_detail']['company_ship_details'])){
                    foreach ($result[0]['company_registration_detail']['company_ship_details'] as $key => $value) {
                    
                        if($value['scope_type'] == 'owner'){
                            $result[0]['owner_ship'][] = $value;
                        }
                        if($value['scope_type'] == 'manager'){
                            $result[0]['manager_ship'][] = $value;
                        }
                    }
                }
                
                return view('site.company.company_profile_user', ['data' => $result,'user' => 'another','role' => $role, 'user_id' => $user_id]);
            }else{
                return redirect()->route('home');
            }

        } else {
            return redirect()->route('home');
        }
    }

    public function editProfile()
    {
        if (Auth::check()) {
            $user_id = Auth::user()->id;
            $options['with'] = ['company_registration_detail.company_detail','company_registration_detail','company_registration_detail.company_ship_details','company_registration_detail.company_locations.state','company_registration_detail.company_documents','company_registration_detail.company_locations.city','company_registration_detail.company_locations.pincode.pincodes_states.state','company_registration_detail.company_locations.pincode.pincodes_cities.city'];

            $result = $this->userService->getDataByUserId($user_id, $options)->toArray();

            $name = Route::currentRouteName();
            $result['current_route'] = $name;

            return view('site.registration.company_registration', ['user_data' => $result]);
        } else {
            return redirect()->route('home');
        }
        return view('site.user.profile');

    }

    public function updateCompanyDetails(Requests\UpdateCompanyRegistrationRequest $companyRegistrationRequest){
        $companyDetails = $companyRegistrationRequest->toArray();
        $logged_company_data = "";

        $companyDetails['otp'] = $this->userService->generateOTP();
        if (Auth::check()) {
            $logged_company_data = Auth::User()->toArray();
            $logged_company_data['otp'] = $companyDetails['otp'];
        }

        $result = $this->companyService->store($companyDetails, $logged_company_data);

        if($result['status'] == 'success'){
            return response()->json(['status' => 'success' , 'message' => 'Your company details has been store successfully.','redirect_url' => route('home')],200);
        }else{
            return response()->json(['status' => 'failed' , 'message' => 'Error while adding data.'],400);
        }
    }

    public function uploadLogo(Request $request){

        $array['image-x'] = $request['image-x'];
        $array['image-y'] = $request['image-y'];
        $array['image-x2'] = $request['image-x2'];
        $array['image-y2'] = $request['image-y2'];
        $array['image-w'] = $request['image-w'];
        $array['image-h'] = $request['image-h'];
        $array['crop-w'] = $request['crop-w'];
        $array['crop-h'] = $request['crop-h'];
        $array['profile_pic'] = $request['profile_pic'];

        if(isset($request['role']) && $request['role'] != 'undefined')
            $array['role'] = $request['role'];

        $result = $this->companyService->uploadLogo($array);

        if( $result['status'] == 'success' ) {
            return response()->json($result, 200);
        } else {
            return response()->json($result, 400);
        }
    }

    public function candidateSearch(Request $request){

        if(Auth::check()){
            $user = Auth::User()->toArray();
        }

        $filter = $request->toArray();
        //dd($filter);
        $paginate = env('PAGINATE_LIMIT',10);

        $result = $this->candidateService->getAllCandidateDataByFilters($filter,$paginate);

        $candidate_search = View::make("site.partials.candidate_search")->with(['data' => $result]);
        $template = $candidate_search->render();

        if($user['status'] == '1'){
            if($request->isXmlHttpRequest()){
            //dd($result);
                return response()->json(['template' => $template], 200);
            }else{
                return view('site.company.candidate_search' , ['data' => $result,'filter' => $filter]);
            }
        }else{
            return view('site.company.candidate_search',['not_activated' => '1']);
        }
    }

    public function candidateDownloadCV(Request $request, $candidate_id){

        if(Auth::check()){
            $role = Auth::user()->registered_as;
            $email = Auth::user()->email;
            $user = Auth::user()->toArray();

            $candidate = Auth::user()->find($candidate_id)->toArray();
            $user['candidate_name'] = $candidate['first_name'];
                        
            $user_id= Auth::user()->id;
            $my_subscription_all = $this->userService->mySubscriptionAllType($user_id, $role);
            $my_subscription = $my_subscription_all['my'];
            // $resume_download_allowed = $this->companyDownloadsService->checkCompanyResumeDownloads($request,$user_id,$candidate_id);

            // if((isset($resume_download_allowed['data']) && !empty($resume_download_allowed['data'])) OR $role == 'admin'){
                
                $options['with'] = ['personal_detail','professional_detail','passport_detail','seaman_book_detail','coc_detail','gmdss_detail','wkfr_detail','personal_detail.state','personal_detail.city','personal_detail.pincode.pincodes_states.state','personal_detail.pincode.pincodes_cities.city','sea_service_detail','course_detail','cop_detail'];

                $result = $this->userService->getDataByUserId($candidate_id,$options)->toArray();

                $courses = $this->userService->getAllCourses();

                $count = 0;
                foreach ($result[0]['course_detail'] as $key => $value) {
                    if($value['course_type'] == 'Value Added'){
                        $result[0]['value_added_course_detail'][$count] = $value;
                        $count++;
                        unset($result[0]['course_detail'][$key]);
                    }
                }

                // return view('site.user.seafarer_download_cv',['data' => $result[0],'all_courses' => $courses]);
                $pdf = PDF::loadHTML((string)view('site.user.seafarer_download_cv',['data' => $result[0],'all_courses' => $courses]));
                $directory = public_path().'/'.env('CANDIDATE_CV_PATH');
                if (!is_dir($directory)) {
                    mkdir($directory, 0777);
                }
                $file_path = public_path().DIRECTORY_SEPARATOR.'uploads'.DIRECTORY_SEPARATOR.'seafarer_download_cv'.DIRECTORY_SEPARATOR.$candidate_id."-".$result[0]['first_name'].'.pdf';
                // $file_path = str_replace('/', '\\', $file_path);
                
                $pdf->save($file_path);

                if($role != 'admin'){
                    $company_download_data = [];
                    $company_download_data['company_id'] = $my_subscription['active']['company_reg_id'];
                    $company_download_data['seafarer_id'] = $candidate_id;
                    $company_download_data['url'] = $request->path();
                    $company_download_data['type'] = config('feature.feature1');

                    $this->companyDownloadsService->store($company_download_data);
                    $user['company_email'] = '';
                    $options['with'] = ['company_registration_detail.company_detail'];
                    $company_detail = $this->userService->getDataByUserId($user_id,$options);

                    if(isset($company_detail) && !empty($company_detail)){
                        $company_details = $company_detail->toArray();

                        if(isset($company_details[0]['company_registration_detail']['company_detail']['company_email']) && !empty($company_details[0]['company_registration_detail']['company_detail']['company_email'])){
                            $user['company_email'] = $company_details[0]['company_registration_detail']['company_detail']['company_email'];
                        }
                    }

                    $this->sendEmailService->sendPdfToCompanyResumeDownload($user,$file_path);
                }


                //return $pdf->download($file_path);
                if ($role == 'admin') {
                   return response()->download($file_path);
                } else {
                    $file_path = url('/').'/uploads/seafarer_download_cv/'.$candidate_id."-".$result[0]['first_name'].'.pdf';
                    return response()->json(['file_path'=> $file_path] , 200);
                }
                


            // }else{
            //     return redirect()->back()->with(['message' => $resume_download_allowed['message'],'error_type' => 'resume_download_limited']);
            // }
        }
    }

    public function resumeList(){
        if(Auth::check()){
            $user_id = Auth::user()->id;

            $company_registration_data = $this->companyService->getDetailsByCompanyId($user_id)->toArray();
            $company_id = $company_registration_data[0]['id'];
            
            $result = $this->companyDownloadsService->getResumeDownloadList($company_id);
            //dd($result->toArray());
            $pagination_data = $result->toArray();
            $collection = collect($pagination_data['data']);

            $data = array_values($collection->groupBy('seafarer_id')->toArray());
                
            foreach ($data as $key => $value) {
                $details = \App\CompanyDownloads::where('seafarer_id',$value[0]['seafarer_id'])->where('company_id',$value[0]['company_id'])->orderBy('created_at','desc')->first()->toArray();

                $data[$key][0]['created_at'] = $details['created_at'];
            }
            
            return view('site.company.resume_downloaded_list' , ['data' => $data,'pagination' => $result,'pagination_data' => $pagination_data]);
        }
    }

    public function addAdvertise(){
        $result = '';
        if (Auth::check()) {
            $user_id = Auth::user()->id;
            $options['with'] = ['company_registration_detail.advertisment_company_details'];
            $result = $this->userService->getDataByUserId($user_id, $options)->toArray();
        }
        
        return view('site.company.add_advertise',['data' => $result]);
    }

    public function storeAdvertise(Request $request){

        if (Auth::check()) {
            $user_id = Auth::user()->id;

            $data = $request->toArray();
            $result = $this->companyService->storeAdvertise($data,$user_id);
            if( $result['status'] == 'success' ) {
                return response()->json(['redirect_url' => route('site.show.company.details')], 200);
            } else {
                return response()->json($result, 400);
            }
        }else{
            return response()->json(['message' => 'Please Login'], 400);
        }
    }

    public function getAllCompanyWithAavertisements(Request $request){

        $advertisements = new CompanyRegistration;
        $city = '';
        $state =  \App\State::all()->toArray();
        $filter = $request->toArray();
        
        if(isset($filter['company_name']) && !empty($filter['company_name'])){
            $advertisements = $advertisements->where('company_name','like','%'.$filter['company_name'].'%');
        }

        if(isset($filter['country']) && !empty($filter['country'])){
            $advertisements = $advertisements->whereHas('company_locations', function ($c) use ($filter) {
                        $c->where('country', $filter['country']);
                    });
        }

        if(isset($filter['state']) && !empty($filter['state'])){
            $advertisements = $advertisements->whereHas('company_locations', function ($c) use ($filter) {
                        $c->where('state_id', $filter['state']);
                    });
            $city =  \App\city::where('state_id',$filter['state'])->get()->toArray();
        }

        if(isset($filter['state_text']) && !empty($filter['state_text'])){
            $advertisements = $advertisements->whereHas('company_locations', function ($c) use ($filter) {
                        $c->where('state_text', $filter['state_text']);
                    });
        }

        if(isset($filter['city']) && !empty($filter['city'])){
            $advertisements = $advertisements->whereHas('company_locations', function ($c) use ($filter) {
                        $c->where('city_id', $filter['city']);
                    });
        }

        if(isset($filter['city_text']) && !empty($filter['city_text'])){
            $advertisements = $advertisements->whereHas('company_locations', function ($c) use ($filter) {
                        $c->where('city_text', $filter['city_text']);
                    });
        }

        $advertisements = $advertisements->whereHas('user_details', function ($c) {
            $c->where('status', '1');
        });

        $advertisements = $advertisements->where('type',NULL)->with('user_details','next_company_jobs','advertisment_company_details','company_detail','company_ship_details','company_locations.state','company_documents','company_locations.city','company_locations.pincode.pincodes_states.state','company_locations.pincode.pincodes_cities.city')
                    ->orderBy(DB::raw('ISNULL(is_featured_order), is_featured_order'), 'ASC')
                    ->paginate(24);
              
        return view('site.featured_company.featured_company',['advertisements' => $advertisements, 'filter' => $filter, 'state' => $state, 'city' => $city]);
    }

    public function getAllSeafarerPermissionRequest(Request $request){

        if (Auth::check()) {
            $user_id = Auth::user()->id;
            $filter = $request->toArray();

            $result = $this->documentService->getAllSeafarerPermissionRequest($user_id,$filter);
            
            // dd($result->toArray());
            return view('site.company.permission_requests',['pagination' => $result, 'pagination_data' => $result->toArray(), 'filter' => $filter]);

        }else{
            return response()->json(['message' => 'Please Login'], 400);
        }
    }

    public function downloadDocumentsByPermissionId(Request $request,$permission_id){

        if(isset($permission_id) && !empty($permission_id)){

            if (Auth::check()) {
                $user_id = Auth::user()->id;
                $requester_id = Auth::user()->id;
                $owner_id = '';

                $result = $this->documentService->checkPermissionBelongsToUser($permission_id,$user_id);
                
                if(isset($result) && count($result) > 0){
                    
                    $owner_id = $result['0']->owner_id;
                    
                    $permission_details = $this->documentService->getPermissionDetailsByPermissionId($permission_id);

                    $dir = env('DOCUMENT_STORAGE_PATH') . $permission_details['owner_id'];
                    
                    if(isset($permission_details['document_type']) && !empty($permission_details['document_type'])){
                        
                        foreach ($permission_details['document_type'] as $key => $doc) {
                            $permission_dir = $dir;

                            if(isset($doc['type']['type']) && !empty($doc['type']['type'])){
                                $permission_dir = $permission_dir . "/" . $doc['type']['type'];
                                $type = $doc['type']['type'];
                            }

                            if(isset($doc['type']['type_id']) && !empty($doc['type']['type_id'] && $doc['type']['type_id'] != '0')){
                                $permission_dir = $permission_dir . "/" . $doc['type']['type_id'];
                                $type = $type. " " . $doc['type']['type_id'];
                            }

                            $requested_files[] = $permission_dir;
                            $requested_type[] = $type;
                        }
                    }
                    $files = $requested_files;  

                    // $files = glob($dir . '/*');
                    $source_arr = $requested_files;
                    $zip_file = 'zip_download'.'/'.$permission_id.'_' . time() .'_zip.zip';
                    // Initialize archive object
                    $zip = new ZipArchive();
                    $zip->open($zip_file, ZipArchive::CREATE | ZipArchive::OVERWRITE);

                    foreach ($source_arr as $source)
                    {
                        if (!file_exists($source)) continue;
                        $source = str_replace('\\', '/', realpath($source));
                        
                        if (is_dir($source) === true)
                        {
                            $files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($source), RecursiveIteratorIterator::SELF_FIRST);

                            foreach ($files as $file)
                            {
                                $file = str_replace('\\', '/', realpath($file));

                                if (!is_dir($file) === true)
                                {
                                    
                                    // Get real and relative path for current file
                                    $filePath = $file;
                                    $relativePath = substr($filePath, strlen($zip_file) - 1);

                                    $path = explode($permission_details['owner_id'].'/', $relativePath);
                                    
                                    // Add current file to archive
                                    $zip->addFile($filePath, $path['1']);
                                    chmod($filePath,0777);
                                }
                            }
                        }
                        else if (is_file($source) === true)
                        {
                            $zip->addFromString(basename($source), file_get_contents($source));
                        }

                    }
                    
                    if(isset($requested_type) && !empty($requested_type)){
                        $this->userService->storeRequestedTypeDocumentsList($requested_type,$owner_id,$requester_id);
                    }

                    $zip->close();

                    header('Content-Description: File Transfer');
                    header('Content-Type: application/octet-stream');
                    header('Content-Disposition: attachment; filename='.basename($zip_file));
                    header('Content-Transfer-Encoding: binary');
                    header('Expires: 0');
                    header('Cache-Control: must-revalidate');
                    header('Pragma: public');
                    header('Content-Length: ' . filesize($zip_file));
                    readfile($zip_file);
                    unlink($zip_file);

                }else{
                    return Redirect::back()->withErrors(['You dont have permission to download this document.']);
                }

            }else{
                return Redirect::back()->withErrors(['Please Login.']);
            }

        }else{
            return Redirect::back()->withErrors(['Permission id required.']);

        }

    }

    public function viewScope(){

        if(Auth::check()){
            $user_id = Auth::user()->id;

            $options['with'] = ['company_registration_detail.company_detail','company_registration_detail','company_registration_detail.company_ship_details','company_registration_detail.company_locations.state','company_registration_detail.company_documents','company_registration_detail.company_locations.city','company_registration_detail.company_locations.pincode.pincodes_states.state','company_registration_detail.company_locations.pincode.pincodes_cities.city','company_registration_detail.advertisment_company_details','company_registration_detail.company_jobs','company_registration_detail.associate_agent','company_registration_detail.appointed_agent'];

            $result = $this->userService->getDataByUserId($user_id, $options)->toArray();

            if(isset($result[0]['company_registration_detail']['company_ship_details']) && !empty($result[0]['company_registration_detail']['company_ship_details'])){
                foreach ($result[0]['company_registration_detail']['company_ship_details'] as $key => $value) {
                
                    if($value['scope_type'] == 'owner'){
                        $result[0]['owner_ship'][] = $value;
                    }
                    if($value['scope_type'] == 'manager'){
                        $result[0]['manager_ship'][] = $value;
                    }
                }
            }
            
            return view('site.company.scope',['data' => $result]);
        }

    }

    public function storeShipDetails(Request $request)
    {
        if(Auth::check()){
            $data = $request->toArray();

            $logged_company_data = $this->companyService->getDataByUserId(Auth::User()->id);

            if(!empty($logged_company_data) && count($logged_company_data) > 0){
                $logged_company_data = $logged_company_data->toArray();
                $data['company_id'] = $logged_company_data[0]['id'];
            }

            $result = $this->companyService->storeShipDetails($data);

            $ship_details = $this->companyService->getShipDetails($data['company_id'],$data['scope_type'])->toArray();
            $template = '';

            if(isset($ship_details) && !empty($ship_details)){
                $vessels = View::make("site.partials.company_vessel_results")->with(['ship_details' => $ship_details]);
                $template = $vessels->render();
            }

            if($result['status'] == 'success'){
                return response()->json(['status' => 'success' , 'message' => 'Your ship details has been store successfully.', 'template' => $template],200);
            }else{
                return response()->json(['status' => 'failed' , 'message' => 'Error while adding data.'],400);
            }
        }
    }

    public function deleteVesselDetails(Request $request){

        if(Auth::check()){
            $data = $request->toArray();
            
            $logged_company_data = $this->companyService->getDataByUserId(Auth::User()->id);

            if(!empty($logged_company_data) && count($logged_company_data) > 0){
                $logged_company_data = $logged_company_data->toArray();
                $company_id = $logged_company_data[0]['id'];
            }
            
            $result = $this->companyService->deleteVesselDetails($data['vessel_id'],$company_id);

            if($result['status'] == 'success'){
                return response()->json($result, 200);
            }else{
                return response()->json($result, 400);
            }
        }
        else{
            return response()->json(['status' => 'failed' , 'message' => 'Error while adding data.'],400);
        }
    } 

    public function getVesselByVesselId(Request $request){

        if(Auth::check()){
            $data = $request->toArray();
            
            $logged_company_data = $this->companyService->getDataByUserId(Auth::User()->id);

            if(!empty($logged_company_data) && count($logged_company_data) > 0){
                $logged_company_data = $logged_company_data->toArray();
                $company_id = $logged_company_data[0]['id'];
            }
            
            $result = $this->companyService->getShipDetailsByshipId($data['vessel_id'],$company_id);

            if($result['status'] == 'success'){
                return response()->json(['result' => $result],200);
            }else{
                return response()->json(['result' => $result],400);
            }
        }
        else{
            return response()->json(['status' => 'failed' , 'message' => 'Error while adding data.'],400);
        }
    }

    public function getSeafarerCoursesByCourseType(Request $request){

        if(Auth::check()){
            $course_type = $request->course_type;
           
            if($course_type == 'normal'){
                $courses = \CommonHelper::courses();
                foreach ($courses as $key => $value) {
                    $courses[] = "<option value=$key>$value</option>";
                }
            }else{
                $courses = \CommonHelper::value_added_courses();
                foreach ($courses as $key => $value) {
                    $courses[] = "<option value=$key>$value</option>";
                }
            }
            
            if($course_type){
                return response()->json(['courses' => $courses],200);
            }else{
                return response()->json(['courses' => ''],400);
            }
        }
        else{
            return response()->json(['status' => 'failed' , 'message' => 'Error while adding data.'],400);
        }
    }

    public function storeAgents(Request $request){

        if(Auth::check()){
            $data = $request->toArray();

            $logged_company_data = $this->companyService->getDataByUserId(Auth::User()->id);

            if(!empty($logged_company_data) && count($logged_company_data) > 0){
                $logged_company_data = $logged_company_data->toArray();
                $data['company_id'] = $logged_company_data[0]['id'];
            }

            $result = $this->companyService->storeAgents($data);

            $agent_details = $this->companyService->getAgentsByAgentType($data['company_id'],$data['agent_type'])->toArray();
            $template = '';

            if(isset($agent_details) && !empty($agent_details)){
                $agents = View::make("site.partials.company_agent")->with(['data' => $agent_details]);
                $template = $agents->render();
            }

            if($result['status'] == 'success'){
                return response()->json(['status' => 'success' , 'message' => 'Your agent details has been store successfully.', 'template' => $template],200);
            }else{
                return response()->json(['status' => 'failed' , 'message' => 'Error while adding data.'],400);
            }
        }
    }

    public function deleteAgentDetails(Request $request){

        if(Auth::check()){
            $data = $request->toArray();
            
            $logged_company_data = $this->companyService->getDataByUserId(Auth::User()->id);

            if(!empty($logged_company_data) && count($logged_company_data) > 0){
                $logged_company_data = $logged_company_data->toArray();
                $company_id = $logged_company_data[0]['id'];
            }
            
            $result = $this->companyService->deleteAgentDetails($data['agent_id'],$company_id);

            if($result['status'] == 'success'){
                return response()->json($result, 200);
            }else{
                return response()->json($result, 400);
            }
        }
        else{
            return response()->json(['status' => 'failed' , 'message' => 'Error while adding data.'],400);
        }
    } 

    public function getAgentByAgentId(Request $request){

        if(Auth::check()){
            $data = $request->toArray();
            
            $logged_company_data = $this->companyService->getDataByUserId(Auth::User()->id);

            if(!empty($logged_company_data) && count($logged_company_data) > 0){
                $logged_company_data = $logged_company_data->toArray();
                $company_id = $logged_company_data[0]['id'];
            }
            
            $result = $this->companyService->getAgentDetailsByAgentId($data['agent_id']);

            if($result['status'] == 'success'){
                return response()->json(['result' => $result],200);
            }else{
                return response()->json(['result' => $result],400);
            }
        }
        else{
            return response()->json(['status' => 'failed' , 'message' => 'Error while adding data.'],400);
        }
    }

    public function uploadAgentProfilePic(Request $request)
    {
        
        $array['image-x'] = $request['image-x'];
        $array['image-y'] = $request['image-y'];
        $array['image-x2'] = $request['image-x2'];
        $array['image-y2'] = $request['image-y2'];
        $array['image-w'] = $request['image-w'];
        $array['image-h'] = $request['image-h'];
        $array['crop-w'] = $request['crop-w'];
        $array['crop-h'] = $request['crop-h'];
        $array['profile_pic'] = $request['profile_pic'];

        $result = $this->companyService->uploadAgentProfilePic($array);

        if( $result['status'] == 'success' ) {
            return response()->json($result, 200);
        } else {
            return response()->json($result, 400);
        }
    }

    public function viewTeamDetails()
    {
        if (Auth::check()) {
            $user_id = Auth::user()->id;

            $logged_company_data = $this->companyService->getDataByUserId(Auth::User()->id);

            if(!empty($logged_company_data) && count($logged_company_data) > 0){
                $logged_company_data = $logged_company_data->toArray();
                $data['company_id'] = $logged_company_data[0]['id'];
            }

            $team_data_by_company_id = $this->companyService->getTeamDataByCompanyId($data['company_id'])->toArray();

            $team_location_by_company_id = $this->companyService->getLocationDataByCompanyId($data['company_id'])->toArray();

            return view('site.company.company_team', ['user_data' => $team_data_by_company_id,'location_data' => $team_location_by_company_id]);
        } else {
            return redirect()->route('home');
        }
    }


    public function uploadTeamProfilePic(Request $request)
    {
        if(Auth::check()){
            $user = Auth::user()->toArray();
            $user_id = $user['id'];
        }

        $array['image-x'] = $request['image-x'];
        $array['image-y'] = $request['image-y'];
        $array['image-x2'] = $request['image-x2'];
        $array['image-y2'] = $request['image-y2'];
        $array['image-w'] = $request['image-w'];
        $array['image-h'] = $request['image-h'];
        $array['crop-w'] = $request['crop-w'];
        $array['crop-h'] = $request['crop-h'];
        $array['profile_pic'] = $request['profile_pic'];

        $result = $this->companyService->uploadTeamProfilePic($array, $user_id);

        if( $result['status'] == 'success' ) {
            return response()->json($result, 200);
        } else {
            return response()->json($result, 400);
        }
    }

    public function storeTeamDetails(Request $request)
    {
        $data = $request->toArray();
        if($data['exiting_team_id']){
            $result = $this->companyService->updateTeamDetailsByTeamId($data);
        }
        else{
            $result = $this->companyService->storeTeamDetails($data);
        }

        if (Auth::check()) {
            $user_id = Auth::user()->id;
            $team_data_by_company_id = $this->companyService->getTeamDataByCompanyId($data['company_id'])->toArray();
        }

        $template = '';

        if(isset($result) && !empty($result)){
            $team_data = View::make("site.partials.company_team_details")->with(['user_data' => $team_data_by_company_id]);
            $template = $team_data->render();
        }

        if($result['status'] == 'success'){
            return response()->json(['status' => 'success' , 'message' => 'Your team details has been store successfully.', 'template' => $template],200);
        }else{
            return response()->json(['status' => 'failed' , 'message' => 'Error while adding data.'],400);
        }

    }

    public function deleteTeamDetails(Request $request)
    {
        if(Auth::check()){
            $data = $request->toArray();
            $user = Auth::user()->toArray();
            $user_id = $user['id'];
            $team_id = $data['team_id'];

            $logged_company_data = $this->companyService->getDataByUserId(Auth::User()->id);

            if(!empty($logged_company_data) && count($logged_company_data) > 0){
                $logged_company_data = $logged_company_data->toArray();
                $data['company_id'] = $logged_company_data[0]['id'];
            }
            $company_id =  $data['company_id'];

            $team_location_by_company_id = $this->companyService->getLocationDataByCompanyId($data['company_id'])->toArray();

            $user_company_id = $team_location_by_company_id['0']['id'];

            if($user_company_id == $company_id){
                $result = $this->companyService->deleteTeamDetails($team_id,$company_id);
            }

            if($result){
                return response()->json(['status' => 'Success' , 'message' => 'Team details has been deleted successfully.'] ,200);
            }else{
                return response()->json(['status' => 'failed' , 'message' => 'Error while deleting the data.'],400);
            }
        }
        else{
            return response()->json(['status' => 'failed' , 'message' => 'Error while deleting the data.'],400);
        }
    }

    public function updateTeamDetails(Request $request)
    {
        if(Auth::check()){
            $data = $request->toArray();
            $result = $this->companyService->getTeamDataByTeamId($data['team_id']);

            if($result){
                return response()->json( ['status' => 'success' , 'message' => 'Data is successfully saved.','team_data' => $result[0]],200);
            }else{
                return response()->json( $result,400);
            }
        }
        else{
            return response()->json(['status' => 'failed' , 'message' => 'Error while adding data.'],400);
        }
    }
}