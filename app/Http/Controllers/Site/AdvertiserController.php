<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Services\UserService;
use App\Services\AdvertiseService;
use App\Services\CompanyService;
use App\AdvertiseDetails;
use App\AdvertisementsLocations;
use Auth;
use Route;
use View;
use DB;

class AdvertiserController extends Controller
{
	private $userService;
	private $companyService;
    private $advertiseService;

    public function __construct()
    {
        $this->userService = new UserService();
        $this->companyService = New CompanyService();
        $this->advertiseService = New AdvertiseService();
    } 

    public function registration(){
    	return view('site.advertiser.registration');
    }

    public function store(Request $request){
    	$data = $request->toArray();
    	
    	$login_data = explode('&', $data['login_data']);
		foreach ($login_data as $key => $value) {
			$array_value = explode('=', $value);
			$login[$array_value['0']] = str_replace("+"," ",$array_value['1']);
		}
		$login['registered_as'] = 'advertiser';

		$company_data = explode('&', $data['company_data']);

            $d = str_replace('%5B','[', $data['company_data']);
            $d = str_replace('%5D',']', $d);
            $company_data = str_replace('%40','@', $d);

            $company_data = explode('&', $company_data);

		  foreach ($company_data as $key => $value) {
			$company_array_value = explode('=', $value);
			$company[$company_array_value['0']] = str_replace("+"," ",$company_array_value['1']);
		  }
		$company['registered_as'] = 'advertiser';
		$login['email'] = str_replace("%40","@",$login['email']);

		if(isset($login['uploaded-file-path'])){
			$login['uploaded-file-path'] = str_replace("%5C","/",$login['uploaded-file-path']);
			$login['uploaded-file-path'] = str_replace("%2F","",$login['uploaded-file-path']);
		}
        $login['otp'] = $this->userService->generateOTP();
        $login['country'][0] = $company['country[0]'];

		if(isset($login['user_id'])){
            $login_user = \App\User::find($login['user_id'])->toArray();
			$result = $this->userService->store($login,$login_user);
		}else
    		$result = $this->userService->store($login);

    	if($result){
    		if(isset($result['user']['id'])){
    			$data['id'] = $result['user']['id'];
    		}else{
	    		$data = $result['user']->toArray();
    		}
    		if(isset($company['uploaded-file-path'])){
	    		$company['uploaded-file-path'] = str_replace("%5C","/",$company['uploaded-file-path']);
				$company['uploaded-file-path'] = str_replace("%2F","",$company['uploaded-file-path']);
			}

            if(isset($company['company_address'])){
                $company['company_address'] = str_replace("%5C","/",$company['company_address']);
                $company['company_address'] = str_replace("%2F","",$company['company_address']);
                $company['company_address'] = str_replace("%2C",",",$company['company_address']);
            }
            if(isset($company['product_description'])){
                $company['product_description'] = str_replace("%5C","/",$company['product_description']);
                $company['product_description'] = str_replace("%2F","",$company['product_description']);
                $company['product_description'] = str_replace("%2C",",",$company['product_description']);
            }
            if(isset($company['bussiness_description'])){
                $company['bussiness_description'] = str_replace("%5C","/",$company['bussiness_description']);
                $company['bussiness_description'] = str_replace("%2F","",$company['bussiness_description']);
                $company['bussiness_description'] = str_replace("%2C",",",$company['bussiness_description']);
            }
	    	
	    	$result = $this->companyService->store_company_details($company,$data['id']);
	    	return response()->json(['status' => 'success' , 'message' => 'Your company details has been store successfully.','redirect_url' => $result['redirect_url']],200);
    	}else{
            return response()->json(['status' => 'failed' , 'message' => 'Error while adding data.'],400);
        }

    }

    public function edit(){
    	if(Auth::check()){
    		$user_id = Auth::User()->id;

            $options['with'] = ['personal_detail'];
            $result['user'] = $this->userService->getDataByUserId($user_id, $options)->toArray();
            

            $options['with'] = ['company_registration_detail','company_registration_detail.company_locations.state','company_registration_detail.company_locations.city','company_registration_detail.company_locations.pincode.pincodes_states.state','company_registration_detail.company_locations.pincode.pincodes_cities.city'];
          	$result['company_data'] = $this->userService->getDataByUserId($user_id, $options)->toArray();

          	$name = Route::currentRouteName();
            $result['current_route'] = $name;

            return view('site.advertiser.registration',['user_data' => $result]);
        }else{
            return redirect()->route('home');
        }
    }

    public function viewAdvertiserProfile($id){
        if(!empty($id)){
            $user_id = $id;

            $options['with'] = ['personal_detail','company_registration_detail.advertisment_details'];
            $result['user'] = $this->userService->getDataByUserId($user_id, $options)->toArray();
            

            $options['with'] = ['company_registration_detail.company_detail','company_registration_detail'];
            $result['company_data'] = $this->userService->getDataByUserId($user_id, $options)->toArray();

            $name = Route::currentRouteName();
            $result['current_route'] = $name;

            return view('site.advertiser.advertiserProfile',['data' => $result,'user' => 'another_user']);

        }else{
            return redirect()->route('home');
        }
    }

    public function view(){
        /*Auth::loginUsingId('26');*/
    	if(Auth::check()){

            if(!empty($id)){
                $user_id = $id;
            }else{
    		    $user_id = Auth::User()->id;
            }
            
            $options['with'] = ['personal_detail','company_registration_detail.advertisment_details'];
            $result['user'] = $this->userService->getDataByUserId($user_id, $options)->toArray();

            $options['with'] = ['company_registration_detail.company_detail','company_registration_detail','company_registration_detail.company_locations'];
            $result['company_data'] = $this->userService->getDataByUserId($user_id, $options)->toArray();

            $name = Route::currentRouteName();
            $result['current_route'] = $name;
            return view('site.advertiser.profile',['data' => $result]);
        }else{
            return redirect()->route('home');
        }
    }

    public function addAdvertise(){
        if(Auth::check()){
            $user = Auth::User()->toArray();
        }

        $state =  \App\State::all()->toArray();
        $limits = $this->userService->checkSubscriptionAvailability($user['id'],$user['registered_as']);
        
        $advertise_status = '';

        if (!isset($limits['result'][config('feature.feature3')]['status'])) {
            $advertise_status = 'Your advertisement visiblity is over, dectivate another advertise or upgrade subscription';
        }

        if($user['status'] == '1'){
            return view('site.advertiser.add_advertise',['state' => $state,'advertise_status' => $advertise_status]);
        }else{
            return view('site.advertiser.add_advertise',['state' => $state,'not_activated' => '1','advertise_status'=>$advertise_status]);
        }
    }

    public function storeAdvertise(Request $request){

        if(Auth::check()){
            
            $data = $request->toArray();

            $result = $this->advertiseService->store($data);
            
            if($result['status'] == 'success'){
                return response()->json(['status' => 'success' , 'message' => 'Your advertisement details has been store successfully.','redirect_url' => route('site.advertiser.profile')],200);
            }else{
                return response()->json(['status' => 'failed' , 'message' => 'Error while adding data.'],400);
            }

        }else{
            return redirect()->route('home');
        }
    }

    public function get_advertisements(){

        $state = '';
        $city= '';
        $limit = '4';
        $type = '';
        if(Auth::check()){
            $user_id = Auth::User()->id;
            $options['with'] = ['personal_detail'];
            $user_details = $this->userService->getDataByUserId($user_id,$options)->toArray();

            if(isset($user_details[0]['personal_detail']['state_id']) && !empty($user_details[0]['personal_detail']['state_id'])){
                $state = $user_details[0]['personal_detail']['state_id'];
            }
            if(isset($user_details[0]['personal_detail']['city_id']) && !empty($user_details[0]['personal_detail']['city_id'])){
                $city = $user_details[0]['personal_detail']['city_id'];
            }
            $type = 'login';
        }else{
            
            $state = '';
            $city ='';
            
            $type = 'IP';
        }
        
        $result = $this->advertiseService->get_advertisements($limit,$state,$city,$type);
        
        if(count($result) > 0){
            $advertisements = View::make("site.partials.home_advertisement_box")->with(['data' => $result]);
            $template = $advertisements->render();
            
            return response()->json(['template' => $template], 200);
        }else{
            return response()->json(['status' => 'failed' , 'message' => 'No advertisements found.'],400);
        }
    }

    public function advertiseEnquiry(Request $request){

        if(isset($request) && !empty($request)){
            $data = $request->toArray();
            if(Auth::check()){
                $user_id = Auth::User()->id;
                $data['user_id'] = $user_id;
            }
            $result = $this->advertiseService->storeEnquiry($data);

            if($result['status'] == 'success'){
                return response()->json(['status' => 'success' , 'message' => 'We have received your enquiry.'],200);
            }else{
                return response()->json(['status' => 'failed' , 'message' => 'Error while adding data.'],400);
            }

        }
    }

    public function enquiryList(){

        if(Auth::check()){
            
            $user_id = Auth::User()->id;
            $company = $this->companyService->getDataByUserId($user_id);

            if(!empty($company)){
                $company_details = $company->toArray();
                $company_id = $company_details['0']['id'];
            }else{
                return redirect()->route('home');
            }
            
            $result = $this->advertiseService->getAdvertiseEnquiryByCompanyId($company_id);
            
            if(isset($result)){
                $paginate = $result;
                $result = $result->toArray();
            }

            return view('site.advertiser.enquiry_list',['data' => $result['data'],'link'=>$result,'paginate' => $paginate]);
        }else{
            return redirect()->route('home');
        }
    }

    public function changeAdvertisementStatus(Request $request){
        $data = $request->toArray();
        if (isset($data['id']) && !empty($data['id'])) {
            $user_id = Auth::user()->id;
            $company = $this->companyService->getDataByUserId($user_id)->toArray();
            $advertise = $this->advertiseService->getAdvertisementsById($data['id'])->toArray();
            if ($advertise['company_id'] != $company[0]['id'] || $advertise['status'] == 0) {
                return response()->json(['status' => 'failed'],400);
            }

            $result = $this->advertiseService->changeStatus($data);    
            if($result['status'] == 'success'){
                return response()->json(['status' => 'success'],200);
            }else{
                return response()->json(['status' => 'failed'],400);
            } 
        } else {
            return response()->json(['status' => 'failed'],400);
        }
    }

    public function getAllAdvertiserWithAavertisements(Request $request)
    {
        $city = '';
        $state =  \App\State::orderBy('name','asc')->get()->toArray();
        $filter = $request->toArray();

        $results =  new AdvertisementsLocations;
        $results = $results->select('advertise_id')->groupBy('advertise_id');
        

        if(isset($filter['company_name']) && !empty($filter['company_name'])){
            $results = $results->whereHas('advertisement_details.company_registration_details', function ($c) use ($filter) {
                        $c->where('company_name','like','%'.$filter['company_name'].'%');
                    });
        }

        if(isset($filter['state']) && !empty($filter['state'])){
            $results = $results->where('state_id',$filter['state']);
            $city =  \App\city::where('state_id',$filter['state'])->get()->toArray();
            
        }

        if(isset($filter['city']) && !empty($filter['city'])){
            $results = $results->where('city_id',$filter['city']);
        }
        

        $results = $results->with('advertisement_details.company_registration_details')->whereHas('advertisement_details',function ($query)    {
                    $query->where('status', 2);
                })->orderBY('id','desc')->paginate(9);
       
                    
        return view('site.advertiser.advertise_listing',['advertisements' => $results, 'filter' => $filter, 'state' => $state, 'city' => $city]);
    }
}
