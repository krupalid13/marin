<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\UserService;
use App\Services\InstituteService;
use App\Services\StateService;
use App\Services\CityService;
use App\Services\BatchService;
use App\Services\SubscriptionService;
use App\Services\DocumentService;
use App\Services\OrderService;
use App\Http\Requests;
use App\OrderStatusHistory;
use App\Services\SendEmailService;
use App\InstituteImageGallery;
use App\InstituteBatches;
use App\AdvertiserCourseDiscountMapping;
use Auth;
use Route;
use View;
use ZipArchive;
use RecursiveIteratorIterator;
use RecursiveDirectoryIterator;
use App\Events\BatchOrderNotification;
use PubNub\PNConfiguration;
use PubNub\PubNub;

class InstituteController extends Controller
{
    private $instituteService;
    private $userService;
    private $stateService;
    private $cityService;
    private $batchService;
    private $subscriptionService;
    private $documentService;
    private $orderService;
    private $sendEmailService;

    function __construct()
    {
        $this->instituteService = New InstituteService();
        $this->userService = New UserService();
        $this->stateService = New StateService();
        $this->cityService = New CityService();
        $this->batchService = New BatchService();
        $this->subscriptionService = New SubscriptionService();
        $this->documentService = new DocumentService();
        $this->orderService = new OrderService();
        $this->sendEmailService = new SendEmailService();
    }

    public function registration(){
        if (Auth::check()) {
            return redirect()->route('home');
        }
        return view('site.registration.institute_registration');
    }

    public function view($id=null){//Auth::loginUsingId(107);
        
        if(isset($id) && !empty($id)){
            $institute_id = $id;
        }else{
            $institute_id = Auth::user()->id;
        }

        $options['with'] = ['institute_registration_detail.institute_detail','institute_registration_detail','institute_registration_detail.institute_locations.state','institute_registration_detail.institute_locations.city','institute_registration_detail.institute_locations.pincode.pincodes_states.state','institute_registration_detail.institute_locations.pincode.pincodes_cities.city','institute_registration_detail.advertisment_details','institute_registration_detail.institute_notice','institute_registration_detail.institute_gallery','institute_registration_detail.institute_locations.location_contact'];
        $result = $this->userService->getDataByUserId($institute_id, $options)->toArray();

        $institute_course_type = [];
        $filter['no_pagination'] = 1;
        $filter['no_order_by'] = 1;
        $options['with'] = ['course_details','institute_batches.course_details'];

        $institute_courses = $this->instituteService->getAllCourseDetailsByInstituteId($result[0]['institute_registration_detail']['id'],$options,$filter);
        
        if(!empty($institute_courses)){
            foreach ($institute_courses as $key => $course) {
                $institute_course_type[$course['course_type']][] = $course->toArray();
            }
        }

        if(isset($id) && !empty($id)){
            return view('site.institute.institute', ['data' => $result,'institute_course_type' => $institute_course_type,'user' => 'another']);
        }
        return view('site.institute.institute', ['data' => $result,'institute_course_type' => $institute_course_type]);
        
    }

    public function editProfile()
    {
		
		
        if (Auth::check()) {
            $institute_id = Auth::user()->id;
            $options['with'] = ['institute_registration_detail.institute_detail','institute_registration_detail','institute_registration_detail.institute_locations.state','institute_registration_detail.institute_locations.city','institute_registration_detail.institute_locations.pincode.pincodes_states.state','institute_registration_detail.institute_locations.pincode.pincodes_cities.city','institute_registration_detail.institute_locations.location_contact'];

            $result = $this->userService->getDataByUserId($institute_id, $options)->toArray();
            $name = Route::currentRouteName();
            $result['current_route'] = $name;

            return view('site.registration.institute_registration', ['user_data' => $result]);
        } else {
            return redirect()->route('home');
        }

    }

    public function store(Requests\InstituteRegistrationRequest $instituteRegistrationRequest){
        
        $instituteDetails = $instituteRegistrationRequest->toArray();
        $instituteDetails['otp'] = $this->userService->generateOTP();
        $instituteDetails['registered_as'] = 'institute';
        $logged_institute_data = "";

        if (Auth::check()) {
            $logged_institute_data = Auth::User()->toArray();
        }

        $result = $this->instituteService->store($instituteDetails, $logged_institute_data);

        //temp hide
        /*$subscriptions = $this->subscriptionService->getAllActiveSubscription('institute');

        if (!empty($subscriptions)) {
           $reditect_url = route('site.user.subscription');
        } else {
            $reditect_url = route('home',['auto_action' => 'welcome']);
        }*/

        $reditect_url = route('home',['auto_action' => 'welcome']);

        if($result['status'] == 'success'){
            return response()->json(['status' => 'success' , 'message' => 'Your institute details has been store successfully.','redirect_url' => $reditect_url],200);
        }else{
            return response()->json(['status' => 'failed' , 'message' => 'Error while adding data.'],400);
        }
    }

    public function updateInstituteDetails(Requests\UpdateInstituteRegistrationRequest $updateInstituteRegistrationRequest){
        
        $instituteDetails = $updateInstituteRegistrationRequest->toArray();
        $instituteDetails['otp'] = $this->userService->generateOTP();
        $logged_institute_data = "";

        if (Auth::check()) {
            $logged_institute_data = Auth::User()->toArray();
        }

        $result = $this->instituteService->store($instituteDetails, $logged_institute_data);

        if($result['status'] == 'success'){
            return response()->json(['status' => 'success' , 'message' => 'Your institute details has been store successfully.','redirect_url' => route('site.show.institute.details')],200);
        }else{
            return response()->json(['status' => 'failed' , 'message' => 'Error while adding data.'],400);
        }
    }

    public function uploadLogo(Request $request){

        $array['image-x'] = $request['image-x'];
        $array['image-y'] = $request['image-y'];
        $array['image-x2'] = $request['image-x2'];
        $array['image-y2'] = $request['image-y2'];
        $array['image-w'] = $request['image-w'];
        $array['image-h'] = $request['image-h'];
        $array['crop-w'] = $request['crop-w'];
        $array['crop-h'] = $request['crop-h'];
        $array['profile_pic'] = $request['profile_pic'];
 
        $result = $this->instituteService->uploadLogo($array);

        if( $result['status'] == 'success' ) {
            return response()->json($result, 200);
        } else {
            return response()->json($result, 400);
        }
    }

    public function courseBatches(){

        if(Auth::check()){
            $user_id = Auth::user()->id;
            $registration_data = $this->instituteService->getDetailsByCompanyId($user_id)->toArray();
            $institute_id = $registration_data[0]['id'];

            $course_type = [];

            $options['with'] = ['institute_registration_detail.institute_locations.state','institute_registration_detail.institute_locations.city','institute_registration_detail.institute_locations.pincode.pincodes_states.state','institute_registration_detail.institute_locations.pincode.pincodes_cities.city','institute_registration_detail.institute_courses'];

            $location = $this->userService->getDataByUserId($user_id, $options)->toArray();

            if(isset($location[0]['institute_registration_detail']['institute_courses']) AND !empty($location[0]['institute_registration_detail']['institute_courses'])){
                foreach ($location[0]['institute_registration_detail']['institute_courses'] as $key => $value) {
                    if($value['status'] != 0)
                        $course_type[$value['course_type']] = \CommonHelper::institute_course_types()[$value['course_type']];
                }
            }

            return view('site.institute.add_batches',['location' => $location,'course_type' => $course_type]);
        }
    }

    public function getCourseNameByCourseType($course_id,$institute_id = null){

        if(empty($institute_id)) {
            if(Auth::check()){
                $user_id = Auth::user()->id;
                $registration_data = $this->instituteService->getDetailsByCompanyId($user_id)->toArray();
                if(isset($registration_data[0]['id'])) {
                    $institute_id = $registration_data[0]['id'];
                }
            }
        }

        if(isset($_GET['is_all']) && $_GET['is_all'] == true) {
            $institute_id = null;
        }

        $loadtype = null;
        if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
        {    
         $loadtype = 'ajax'; 
        }

        $result = $this->instituteService->getCourseNameByCourseType($course_id,$institute_id)->toArray();
        if( $institute_id != null )  {
            $result_data = array();

            foreach ($result as $r_key => $r_value) {

                 if(isset($r_value['course_details'])) {
                     $result_data[] =  $r_value['course_details'];
                 }else {
                     $result_data[] = $r_value;
                 }
            }
            $result = $result_data;
        }else {
            $result_data = array();

            foreach ($result as $r_key => $r_value) {
                $result_data[] = $r_value;
            }
            $result = $result_data;
        }
        

        return $result;
    }

    public function storeCourseBatches(Request $request){
        $data = $request->toArray();
        
        if(Auth::check()){
            $user_id = Auth::user()->id;
            $registration_data = $this->instituteService->getDetailsByCompanyId($user_id)->toArray();
            $data['institute_id'] = $registration_data[0]['id'];
        }
        
        $result = $this->batchService->store($data);

        if($result['status'] == 'success') {
            return response()->json($result, 200);
        }
        else {
            return response()->json($result, 400);
        }
    }

    public function updateCourseBatches(Request $request,$batch_id){
        $data = $request->toArray();
        $data['institute_course_batch_id'] = $batch_id;
        
        if(Auth::check()){
            $user_id = Auth::user()->id;
            $registration_data = $this->instituteService->getDetailsByCompanyId($user_id)->toArray();
            $data['institute_id'] = $registration_data[0]['id'];
        }
        
        $data['action_type'] = 'UPDATE';
        $result = $this->batchService->store($data);

        if($result['status'] == 'success') {
            return response()->json($result, 200);
        }
        else {
            return response()->json($result, 400);
        }
    }

    public function editCourseBatches($batch_id){

        $institute_id = Auth::user()->id;
        $options['with'] = ['courses'];

        if(Auth::check()){
            $user_id = Auth::user()->id;
            $registration_data = $this->instituteService->getDetailsByCompanyId($user_id)->toArray();
            if(isset($registration_data[0]['id'])) {
                $institute_reg_id = $registration_data[0]['id'];
            }
        }

        $result = $this->instituteService->getAllBatchesByInstituteId($institute_reg_id, $options, $batch_id)->toArray();
        
        $options['with'] = ['institute_registration_detail.institute_locations.state','institute_registration_detail.institute_locations.city','institute_registration_detail.institute_locations.pincode.pincodes_states.state','institute_registration_detail.institute_locations.pincode.pincodes_cities.city','institute_registration_detail.institute_courses'];

        $location = $this->userService->getDataByUserId($institute_id, $options)->toArray();
        if(isset($location[0]['institute_registration_detail']['institute_courses']) AND !empty($location[0]['institute_registration_detail']['institute_courses'])){
            foreach ($location[0]['institute_registration_detail']['institute_courses'] as $key => $value) {
                if($value['status'] != 0)
                    $course_type[$value['course_type']] = \CommonHelper::institute_course_types()[$value['course_type']];
            }
        }
        //print_r($result); exit;
        if(isset($result[0]['course_details']['course_type'])){
            $result[0]['courses'] = $this->getCourseNameByCourseType($result[0]['course_details']['course_type'],$institute_reg_id);
        }
        $name = Route::currentRouteName();
        $result[0]['current_route'] = $name;

        if($result){
            return view('site.institute.add_batches',['data' => $result[0],'location' => $location,'course_type' => $course_type]);
        }
    }

    public function getBatchByBatchAndInstituteId(Request $request) {

        $batch_id = $request->batch_id;
        $filter = array();
        if(Auth::check()){
            $user_id = Auth::user()->id;
            $registration_data = $this->instituteService->getDetailsByCompanyId($user_id)->toArray();
            $institute_id = $registration_data[0]['id'];
        }

        $institute_courses = $this->instituteService->getAllBatchesByInstituteId($institute_id,$filter,$batch_id,'no');
       
        
        if(!empty($institute_courses)) {
           if($institute_courses[0]['status'] == 1) {
                $institute_courses[0]['status'] = 'Active';
           }else {
                $institute_courses[0]['status'] = 'Inactive';
           }
           
           $course_types = \CommonHelper::institute_course_types();
           
           foreach ($course_types as $key => $value) {
               if( $institute_courses[0]['course_details']['course_type'] == $key ) {
                   $institute_courses[0]['course_details']['course_type'] = $value;
               }
           }

           $course_id_array = \DB::table('courses')->where('id', $institute_courses[0]['course_details']['course_id'])->get()->toArray();

           $institute_courses[0]['course_details']['course_name'] = $course_id_array[0]->course_name;
        }



        return response()->json(['data' => $institute_courses]);
    }

    public function batchListing(Request $request){
        // echo 'test';exit;
        $view_type = ( isset($_GET['display_type']) &&  $_GET['display_type'] == 'calendar' ) ? 'calendar' : 'list';

        $institute_id = 0;


        if(Auth::check()){
            $user_id = Auth::user()->id;
            $registration_data = $this->instituteService->getDetailsByCompanyId($user_id)->toArray();
            $institute_id = $registration_data[0]['id'];
        }

        $batch_type_list = \DB::table('institute_courses')->where('institute_id',$institute_id)->select('course_type')->distinct()->get()->toArray();

        $batch_type_list_new = array();
        foreach ($batch_type_list as $b_key => $b_value) {
           $batch_type_list_new[] = $b_value->course_type;
        }

        $course_type_list = array();
        $i = 1;
        foreach(\CommonHelper::institute_course_types() as $r_index => $rank) {
            if(in_array($r_index, $batch_type_list_new)) {
                $course_type_list[$i]['id'] =  $r_index;
                $course_type_list[$i]['name'] =  $rank;
            }
            $i++;
        }
        
        $options['with'] = ['institute_batches.institute_location'];

        $location_options['with'] = ['institute_registration_detail.institute_locations.state','institute_registration_detail.institute_locations.city','institute_registration_detail.institute_locations.pincode.pincodes_states.state','institute_registration_detail.institute_locations.pincode.pincodes_cities.city','institute_registration_detail.institute_courses'];

        $location = $this->userService->getDataByUserId($user_id, $location_options)->toArray();

        $filter = array();
        $course_name = '';

        
        if($view_type == 'calendar') {
            if( !isset($filter['course_type']) ) {
                $filter['course_type'] = isset($_GET['course_type']) ? $_GET['course_type'] : 0;
            }

            if( !isset($filter['course_name']) ) {
                $filter['course_name'] = isset($_GET['course_name']) ? $_GET['course_name'] : 0;
            }
        }

        if($view_type == 'list') {

            if(isset($request) && !empty($request)){
                $filter = $request->toArray();
                $institute_courses = $this->instituteService->getAllBatchesByInstituteId($institute_id,$filter);
            }else{
                $filter = $request->toArray();
                $institute_courses = $this->instituteService->getAllBatchesByInstituteId($institute_id,$filter);
            }

        }else {
            $institute_courses = $this->instituteService->getAllBatchesByInstituteId($institute_id,$filter,null,'no');
        }
        //dd($institute_courses->toArray());
        if(isset($filter['course_type'])){
            $course_name = array();
            $c_sname = $this->getCourseNameByCourseType($filter['course_type']);
            $result_data = array();
            foreach ($c_sname as $c_key => $c_value) {
                if(isset($c_value['course_details'])) {
                     $result_data[] =  $c_value['course_details'];
                 }else {
                     $result_data[] = $c_value;
                 }
            }
            $course_name = $result_data;
        }
        //dd($course_name);
        $filter['display_type'] = $view_type;


        $batch_search = View::make("site.partials.batch_listing")->with(['data' => $institute_courses,'filter' => $filter,'course_name' => $course_name,'all_location' => $location]);
        $template = $batch_search->render();

        if($request->isXmlHttpRequest()){
            return response()->json(['template' => $template], 200);
        }else{

            return view('site.institute.batch_listing',['data' => $institute_courses,'filter' => $filter,'course_name' => $course_name,'all_location' => $location, 'course_type_list' => $course_type_list]);
        }
    }

    public function disableBatch($batch_id){
        $result = $this->batchService->disableBatchByBatchId($batch_id);

        if($result['status'] == 'success') {
            return response()->json(['status' => 'success', 'message' => 'Batch has been disabled.','redirect_url' => route('home')], 200);
        } else {
            return response()->json(['status' => 'error', 'message' => 'There was some error while disabling batch.'], 400);
        }

    }

    public function enableBatch($batch_id){
        $result = $this->batchService->enableBatchByBatchId($batch_id);

        if($result['status'] == 'success') {
            return response()->json(['status' => 'success', 'message' => 'Batch has been enabled.','redirect_url' => route('home')], 200);
        } else {
            return response()->json(['status' => 'error', 'message' => 'There was some error while enabling batch.'], 400);
        }

    }

    public function disableCourse($course_id){
        $result = $this->instituteService->disableCourseByCourseId($course_id);

        if($result['status'] == 'success') {
            return response()->json(['status' => 'success', 'message' => 'Course has been deactivated.','redirect_url' => route('home')], 200);
        } else {
            return response()->json(['status' => 'error', 'message' => 'There was some error while deactivating course.'], 400);
        }

    }

    public function enableCourse($course_id){
        $result = $this->instituteService->enableCourseByCourseId($course_id);

        if($result['status'] == 'success') {
            return response()->json(['status' => 'success', 'message' => 'Course has been activated.','redirect_url' => route('home')], 200);
        } else {
            return response()->json(['status' => 'error', 'message' => 'There was some error while activating course.'], 400);
        }

    }

    public function seafarer_course_search(Request $request){

        $filter = '';
        $course_name = '';
        $user_id = '';
        $course_name = '';
        $role = '';

        if(Auth::User()){
            $role = Auth::User()->registered_as;
        }
        if(Auth::check()){
            $user_id = Auth::User()->id;
        }

        if(isset($request) && !empty($request)){
            $filter = $request->toArray();
            
            if(isset($filter['course_name'])){
                $course_name = $this->getCourseNameByCourseType($filter['course_type']);
            }
            $data = $this->batchService->getAllCourseBatchDetails($user_id,$filter);
        }else{
            $data = $this->batchService->getAllCourseBatchDetails($user_id);
        }

        $result = $data->toArray();
        // dd($result);
            
        $locations = $this->batchService->getAllActiveCourseLocations();
        // dd($locations->toArray());
        $courses = \App\Courses::all()->toArray();
        $institute_list = \App\InstituteRegistration::orderBy('institute_name')->get();

        $institute_search = View::make("site.partials.institutes_search")->with(['data' => $result['data'],'pagination' => $data,'pagination_data' => $result,'filter' => $filter,'course_name' => $course_name,'all_courses' => $courses,'role' => $role,'user_id' => $user_id,'institute_list' => $institute_list]);
        $template = $institute_search->render();
        
        //dd($result['data']);
        if($request->isXmlHttpRequest()){
            return response()->json(['template' => $template], 200);
            exit;
        }else{
            //return view('site.institutes_courses.institutes_courses',['course_type' => $institute_course_type,'course_by_type' => $course_by_type]);     

            //return view('site.institute.course_search',['data' => $result['data'],'pagination' => $data,'pagination_data' => $result,'filter' => $filter,'course_name' => $course_name,'all_courses' => $courses,'role' => $role,'user_id' => $user_id]);
            return view('site.institutes_courses.institutes_search',['data' => $result['data'],'pagination' => $data,'pagination_data' => $result,'filter' => $filter,'course_name' => $course_name,'all_courses' => $courses,'role' => $role,'user_id' => $user_id,'institute_list' => $institute_list,'locations' => $locations]);
        }   

    }

    public function homepage_course_listing(Request $request,$course_id=null){
        
        $institute_course_type = \CommonHelper::institute_course_types();
        $courses = \App\Courses::all()->toArray();
        $all_courses = '';
        $course_by_type = [];

        foreach ($courses as $key => $value) {
            if(array_key_exists($value['course_type'], $institute_course_type)){
                $course_by_type[$value['course_type']][] = $value; 
            }
        }

        if(!empty($course_id)){

           /* $courseModel = \App\InstituteCourses::where('course_type',$course_id)->where('status','1')->with('course_details');
        
            $courseModel = $courseModel->whereHas('institute_batches', function ($c){
                $c->where('start_date','>',date('Y-m-d'))->where('status','1');
            })->with(['institute_batches' => function ($c){
                $c->with(['institute_location'])->where('start_date','>',date('Y-m-d'))->where('status','1');
            }]);

            $course_type = $courseModel->get()->toArray();*/

            $all_courses = \App\Courses::where('course_type',$course_id)->where('status','1');

            $all_courses = $all_courses->with(['institute_courses.institute_batches' => function ($c){
                $c->with(['institute_location'])->where('start_date','>',date('Y-m-d'))->where('status','1');
            }]);

            $all_courses = $all_courses->get()->toArray();
        }

        return view('site.institutes_courses.institutes_courses',['course_type' => $institute_course_type,'course_by_type' => $course_by_type,'selected_courses_by_type' => $all_courses,'course_id' => $course_id]);   
    }

    public function homepage_institute_listing(){

        $institute_list = \App\InstituteRegistration::with('institute_courses')->orderBy('institute_name')->get();
        return view('site.institutes_courses.institutes_list',['institute_list' => $institute_list]); 
    }

    public function courseApplicantListing(Request $request){
        
        if(Auth::check()){
            $institute_id = Auth::User()->id;
            $filter = $request->toArray();
            
            $registration_data = $this->instituteService->getDetailsByCompanyId($institute_id)->toArray();
            $institute_reg_id = $registration_data[0]['id'];

            //last 0 for no pagination
            $batches = $this->instituteService->getAllBatchesByInstituteId($institute_reg_id,'','','no')->toArray();

            if(!empty($batches)){
                $batch_collection = collect($batches);
                $institute_batches = $batch_collection->pluck('id')->all();

                //get orders by institute batches
                $orders = $this->orderService->getOrderByInstituteBatches($institute_batches,$paginate = 1,$filter);
                //dd($orders->toArray(),$batches,$institute_batches);
            }

            $course_applicant_data = $orders->toArray();
            //dd($course_applicant_data);
            $course_name = \App\InstituteCourses::all()->toArray();
            return view('site.institute.course_applicant_listing',['data' => $course_applicant_data['data'] , 'search_data' => '', 'pagination_data' => $course_applicant_data, 'pagination' => $orders, 'course_list' => $course_name, 'filter' => $filter]);

        }else{
           return response()->json(['status' => 'error', 'message' => 'Please login to continue.'], 400); 
        }

    }

    public function courseListing(Request $request){
        if(Auth::check()){
            $user_id = Auth::User()->id;
            $registration_data = $this->instituteService->getDetailsByCompanyId($user_id)->toArray();
            $institute_id = $registration_data[0]['id'];
        }

        $filter = $request->toArray();

        $options['with'] = ['course_details','institute_batches.course_details'];

        $courses = \App\Courses::all()->toArray();
        $data = $this->instituteService->getAllCourseDetailsByInstituteId($institute_id,$options,$filter);

        $result = $data->toArray();
        
        return view('site.institute.course_listing',['data' => $result,'pagination' => $data,'pagination_data' => $result,'courses' => $courses,'filter' => $filter, 'institute_id'=> $institute_id]);
    }

    public function editCourse($course_id){

        $course_details = \App\InstituteCourses::where('id',$course_id)->with('course_details')->first();

        if(!empty($course_details)){
            $course_details = $course_details->toArray();
            $course_details['course_list'] = \App\Courses::where('course_type',$course_details['course_type'])->get()->toArray();

            return response()->json(['status' => 'success', 'course_details' => $course_details], 200);
        }else{
            return response()->json(['status' => 'error', 'message' => 'There was some error.'], 400);
        }
    }

    public function storeCourse(Request $request){

        $data = $request->toArray();
        if(Auth::check()){
            $user_id = Auth::User()->id;
            $registration_data = $this->instituteService->getDetailsByCompanyId($user_id)->toArray();
            $data['institute_id'] = $registration_data[0]['id'];
        }

        $result = $this->instituteService->storeCourses($data);
        if($result['status'] == 'success') {
            return response()->json(['status' => 'success', 'message' => 'Course has beed added.'], 200);
        } else {
            return response()->json(['status' => 'error', 'message' => 'There was some error while adding course.'], 400);
        }
    }

    public function deleteCourse($course_id){

        if(Auth::check()){
            $user_id = Auth::User()->id;
            $user_data = $this->instituteService->getDetailsByCompanyId($user_id)->toArray();
            $institute_id = $user_data[0]['id'];
        }

        $result = $this->instituteService->deleteCourse($course_id,$institute_id);
        if($result['status'] == 'success') {
            return response()->json(['status' => 'success', 'message' => 'Course has beed deleted successfully.'], 200);
        } else {
            return response()->json(['status' => 'error', 'message' => 'There was some error while deleting course.'], 400);
        }
    }

    public function getAllSeafarerPermissionRequest(Request $request){

        if (Auth::check()) {
            $user_id = Auth::user()->id;
            $filter = $request->toArray();

            $result = $this->documentService->getAllSeafarerPermissionRequest($user_id,$filter);

//             dd($result->toArray());
            return view('site.institute.permission_requests',['pagination' => $result, 'pagination_data' => $result->toArray(), 'filter' => $filter]);

        }else{
            return response()->json(['message' => 'Please Login'], 400);
        }
    }

    public function downloadDocumentsByPermissionId(Request $request,$permission_id){

        if(isset($permission_id) && !empty($permission_id)){

            if (Auth::check()) {
                $user_id = Auth::user()->id;
                $requester_id = Auth::user()->id;
                $owner_id = '';

                $result = $this->documentService->checkPermissionBelongsToUser($permission_id,$user_id);

                if(isset($result) && count($result) > 0){

                    $owner_id = $result['0']->owner_id;

                    $permission_details = $this->documentService->getPermissionDetailsByPermissionId($permission_id);

                    $dir = env('DOCUMENT_STORAGE_PATH') . $permission_details['owner_id'];

                    if(isset($permission_details['document_type']) && !empty($permission_details['document_type'])){

                        foreach ($permission_details['document_type'] as $key => $doc) {
                            $permission_dir = $dir;

                            if(isset($doc['type']['type']) && !empty($doc['type']['type'])){
                                $permission_dir = $permission_dir . "/" . $doc['type']['type'];
                                $type = $doc['type']['type'];
                            }

                            if(isset($doc['type']['type_id']) && !empty($doc['type']['type_id'] && $doc['type']['type_id'] != '0')){
                                $permission_dir = $permission_dir . "/" . $doc['type']['type_id'];
                                $type = $type. " " . $doc['type']['type_id'];
                            }

                            $requested_files[] = $permission_dir;
                            $requested_type[] = $type;
                        }
                    }
                    $files = $requested_files;

                    // $files = glob($dir . '/*');
                    $source_arr = $requested_files;
                    $zip_file = $permission_id.'_' . time() .'_zip';
                    // Initialize archive object
                    $zip = new ZipArchive();
                    $zip->open($zip_file, ZipArchive::CREATE | ZipArchive::OVERWRITE);

                    foreach ($source_arr as $source)
                    {
                        if (!file_exists($source)) continue;
                        $source = str_replace('\\', '/', realpath($source));

                        if (is_dir($source) === true)
                        {
                            $files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($source), RecursiveIteratorIterator::SELF_FIRST);

                            foreach ($files as $file)
                            {
                                $file = str_replace('\\', '/', realpath($file));

                                if (!is_dir($file) === true)
                                {

                                    // Get real and relative path for current file
                                    $filePath = $file;
                                    $relativePath = substr($filePath, strlen($zip_file) - 1);

                                    $path = explode($permission_details['owner_id'].'/', $relativePath);

                                    // Add current file to archive
                                    $zip->addFile($filePath, $path['1']);
                                }
                            }
                        }
                        else if (is_file($source) === true)
                        {
                            $zip->addFromString(basename($source), file_get_contents($source));
                        }

                    }

                    if(isset($requested_type) && !empty($requested_type)){
                        $this->userService->storeRequestedTypeDocumentsList($requested_type,$owner_id,$requester_id);
                    }

                    $zip->close();

                    header('Content-Description: File Transfer');
                    header('Content-Type: application/octet-stream');
                    header('Content-Disposition: attachment; filename='.basename($zip_file));
                    header('Content-Transfer-Encoding: binary');
                    header('Expires: 0');
                    header('Cache-Control: must-revalidate');
                    header('Pragma: public');
                    header('Content-Length: ' . filesize($zip_file));
                    readfile($zip_file);
                    unlink($zip_file);

                }else{
                    return Redirect::back()->withErrors(['You dont have permission to download this document.']);
                }

            }else{
                return Redirect::back()->withErrors(['Please Login.']);
            }

        }else{
            return Redirect::back()->withErrors(['Permission id required.']);

        }

    }

    public function getInstituteCourseDetailsById($course_id){
        return $this->instituteService->getInstituteCourseDetailsById($course_id);
    }

    public function getDataByBatchId(Request $request,$batch_id){
        $batch_details = $this->batchService->getDataByBatchId($batch_id)->toArray();

        if($request->isXmlHttpRequest()){
            return response()->json(['batch_details' => $batch_details[0]], 200);
        }else{

        }
    }

    public function seafarer_book_course(Request $request,$batch_id,$location_id){
        $advertisment_details = '';
        $batch_details = $this->batchService->getDataByBatchId($batch_id);
        //dd($batch_details->toArray());
        $user_id = '';
        $role = '';
        if (Auth::check()) {
            $user_id = Auth::user()->id;
            $role = Auth::user()->registered_as;
        }

        if(isset($batch_details[0]['status']) && $batch_details[0]['status'] == 0){
            return redirect()->route('home');
        }

        $courses = \App\Courses::all()->toArray();

        if(!isset($batch_details[0])){
            return redirect()->route('home')->with('Course not found.');
        }

        $prev_batch_count = '0';
        $make_payment = '';
        $order_id = '';
        $status = '';
        $prev_batch_details = [];
        $data = $request->toArray();
        $allow_split_payment = 0;

        if(isset($batch_details[0]['start_date'])){
            $start_date = $batch_details[0]['start_date'];
            $current_date = date("Y-m-d");

            //Calculate diff between batch start date and current date
            $date1 = date_create($start_date);
            $date2 = date_create($current_date);
            $diff = date_diff($date1,$date2);
            
            if($diff->days > 7){
                $allow_split_payment = 1;
            }
        }

        if(isset($data['order_id']) && !empty($data['order_id'])){
            if(!Auth::check()){
                $current_url = $request->fullUrl();
                return redirect()->route('site.login',['redirect_url' => $current_url]);
            }
        }

        if(Auth::check()){
            $user_id = Auth::user()->id;
            $prev_batch_details = $this->orderService->checkIfAppliedToBatch($batch_id,$user_id);
            
            if(isset($prev_batch_details) && !empty($prev_batch_details)){
                if(($prev_batch_details[0]['status'] == '4') || $prev_batch_details[0]['status'] == '6'){
                    $make_payment = 1;
                    $order_id = $prev_batch_details[0]['id'];
                    $order_details = $this->orderService->getOrderDetailById($order_id)->toArray();
                }
            }
            if(isset($prev_batch_details[0]['status']))
                $status = $prev_batch_details[0]['status'];

            $prev_batch_count = count($prev_batch_details);
        }
        //dd($prev_batch_details,77);
        $institute_course_id = $batch_details[0]['institute_course_id'];
        $same_batches = $this->batchService->getInstituteBatchedByInstituteCourseId($institute_course_id,$location_id)->toArray();
        $discount = '';

        if(isset($batch_id)){
            $advertiserModel = AdvertiserCourseDiscountMapping::where('batch_id',$batch_id);
            
            $advertiserModel = $advertiserModel->whereHas('company_registration.advertisment_details', function ($c) {
                $c->where('status', '2');
            })->with('company_registration.advertisment_details');
           
            $advertisment_details = $advertiserModel->get()->toArray();
            
            if(isset($advertisment_details) && !empty($advertisment_details)){
                $key = array_rand($advertisment_details,1);
                $advertisment_test = $advertisment_details;

                $advertisment_details = [];
                $advertisment_details = $advertisment_test[$key];
            }
            //dd($advertisment_details);
            if(isset($advertisment_details['discount']) && !empty($advertisment_details['discount'])){
                $discount = $advertisment_details['discount'];
            }
            
            if(isset($order_details) && !empty($order_details[0]['discount'])){
                $discount = $order_details[0]['discount'];
            }
            
        }
        
        $all_batches = $batch_details[0]['child_batches']->toArray();
        $batches_count['group'] = [];

        if(isset($all_batches) && !empty($all_batches)){
            foreach ($all_batches as $key => $value) {
                # code...
                if(isset($value['on_demand_batch_type']) && $value['on_demand_batch_type'] == '1'){
                    $all_batches[$key]['group'] = [
                        '1' => [],
                        '2' => [],
                        '3' => [],
                        '4' => [],
                    ];
                }
                
                if(isset($value['on_demand_batch_type']) && $value['on_demand_batch_type'] == '2'){
                    $all_batches[$key]['group'] = [
                        '1-2' => [],
                        '3-4' => [],
                    ];
                }

                if(isset($value['on_demand_batch_type']) && $value['on_demand_batch_type'] == '3'){
                    $all_batches[$key]['group'] = [
                        'monthly' => [],
                    ];
                }
                
                if(isset($value['orders']) && !empty($value['orders'])){

                    foreach ($value['orders'] as $key1 => $value1) {
                        $batches_count['group'][$value1['preffered_type']][] = $value1;
                    }
                   
                }

            }
        }
        
        return view('site.institute.course_blocking',['discount' => $discount,'advertisment_details' => $advertisment_details,'user_id' => $user_id,'data' => $batch_details[0],'all_courses' => $courses,'selected_location_id' => $location_id,'same_batches' => $same_batches,'make_payment' => $make_payment,'order_id' => $order_id,'prev_batch_count' => $prev_batch_count,'status' => $status,'prev_batch_details' => $prev_batch_details,'allow_split_payment' => $allow_split_payment,'batches_count' => $batches_count,'role' => $role]);
    }

    public function blockSeat(Request $request){
        if(Auth::check()){
            $role = Auth::User()->registered_as;

            if($role == 'seafarer'){
                $data = $request->toArray();
                $user_id = Auth::User()->id;

                $mob_verification = Auth::User()->is_mob_verified;

                if($mob_verification == 0){
                    return response()->json(['message' => 'Please verify your mobile number to proceed.','mob_verification_req' => 1], 400);
                }

                if(!empty($data['batch_id']) && !empty($data['location_id'])){
                    //check if already applied to batch and pending
                    $prev_batch_details = $this->orderService->checkIfAppliedToBatch($data['batch_id'],$user_id);
                    
                    if(count($prev_batch_details) > 0){
                        return response()->json(['message' => 'You already applied to this batch.'], 400);
                    }else{
                        $batch_details = $this->batchService->getDataByBatchId($data['batch_id']);
                        $user_details = $this->userService->getDataByUserId($user_id);
                        
                        if(!empty($batch_details)){
                            $batch['batch_id'] = $data['batch_id'];
                            $batch['user_id'] = $user_id ;
                            $discount = !empty($data['discount']) ? $data['discount'] : NULL;
    
                            $cgst = env('CGST_PERCENT');
                            $sgst = env('SGST_PERCENT');

                            if(!empty($discount)){
                                $batch['discount'] = $discount;
                                $cost = isset($batch_details[0]['cost']) ? $batch_details[0]['cost'] : '';
                                $batch['subscription_cost'] = $cost - ($cost*$discount/100);
                            }else{
                                $batch['subscription_cost'] = isset($batch_details[0]['cost']) ? $batch_details[0]['cost'] : '';
                            }

                            $cgst_cost = round(($batch['subscription_cost']*$cgst)/100);
                            $sgst_cost = round(($batch['subscription_cost']*$sgst)/100);

                            $batch['tax'] = json_encode(['cgst'=>env('CGST_PERCENT'),'sgst'=>env('SGST_PERCENT')]);

                            $batch['tax_amount'] = $cgst_cost+$sgst_cost;
                            $batch['total'] = $batch['subscription_cost']+$cgst_cost+$sgst_cost;
                            $batch['status'] = '0';
                            $batch['preffered_date'] = !empty($data['pref_date']) ?  date('Y-m-d', strtotime($data['pref_date'])) : NULL;
                            $batch['preffered_type'] = !empty($data['pref_type']) ? $data['pref_type'] : NULL;
                            
                            $batch['status'] = '0';

                            //dd($batch_details[0]->toArray());
                            $order = $this->orderService->storeOrder($batch);
                            //dd($batch);

                            //dd($order->toArray());
                            if(count($order) > 0){
                                $batch_details[0]['order_status'] = '0';
                                OrderStatusHistory::create(['order_id' => $order['id'],'user_id' => $user_id,'to_status' => 0]);
                                $this->sendEmailService->sendBatchBlockSeatEmailToInstitute($batch_details[0],$user_details);

                                //Notification
                                $institute = $batch_details['0']['course_details']['institute_registration_detail']['user_id'];

                                $options['with'] = ['user','batch_details.course_details.courses','batch_details.course_details.course_details','batch_details.course_details.institute_registration_detail','batch_details.batch_location'];
                                $order_details = $this->orderService->getOrderDetailById($order->id,$options);

                                $data['batch_details'] = $batch_details[0]->toArray();
                                $data['user_details'] = $user_details[0]->toArray();
                                $data['order_details'] = $order_details[0];

                                event(new BatchOrderNotification($user_id, $institute, 'batch_booked',$data));

                                $admins = \App\User::where('registered_as','admin')->get()->toArray();

                                foreach ($admins as $key => $admin) {
                                    event(new BatchOrderNotification($user_id, $admin['id'], 'batch_booked',$data));
                                }
                                
                                return response()->json(['status' => 'success' ,'message' => 'Seat book successfully.'], 200);
                            }else{
                                return response()->json(['message' => 'failed'], 400);
                            }    
                        }
                    }
                    
                }else{
                    return response()->json(['message' => 'Something went wrong. Please try again after some time.'], 400);
                }
            }else{
                return response()->json(['message' => 'You are not allowed to this. Please login as seafarer','not_allowed' => 1], 400);
            }
        }else{
            return response()->json(['message' => 'Please Login','login' => 1], 400);
        }
    }

    public function changeInstituteBatchOrderStatus(Request $request){
        $batch = $request->toArray();
        $batch_details = $this->batchService->getDataByBatchId($batch['batch_id'])->toArray();

        if(true /*isset($batch_details['0']) && isset($batch_details['0']['batch_type']) && $batch_details['0']['batch_type'] == 'confirmed' */){
            $order = $this->orderService->changeInstituteBatchOrderStatus($batch['order_id'],$batch['batch_id'],$batch['status']);
            if(count($order) > 0){

                $user_id = Auth::User()->id;
                $options['with'] = ['user','batch_details.course_details.courses','batch_details.course_details.course_details','batch_details.course_details.institute_registration_detail','batch_details.batch_location'];
                $order_details = $this->orderService->getOrderDetailById($batch['order_id'],$options);

                //4 is for approved status
                if($batch['status'] == '4'){
                    //dd($order_details->toArray());
                    OrderStatusHistory::create(['order_id' => $batch['order_id'],'user_id' => $user_id,'from_status' => '0','to_status' => $batch['status']]);
                    $this->sendEmailService->sendBatchApprovalEmailToUser($order_details[0]);

                    $user = $order_details[0]['user_id'];
                    $institute = $batch_details['0']['course_details']['institute_registration_detail']['user_id'];
                    
                    $data['batch_details'] = $batch_details[0];
                    $data['order_details'] = $order_details[0];

                    event(new BatchOrderNotification($institute, $user, 'batch_approved',$data));

                    $admins = \App\User::where('registered_as','admin')->get()->toArray();

                    foreach ($admins as $key => $admin) {
                        event(new BatchOrderNotification($institute, $admin['id'], 'batch_approved',$data));
                    }

                }
                
                if($batch['status'] == '5' && !empty($batch['reason'])){
                    OrderStatusHistory::create(['reason' => $batch['reason'],'order_id' => $batch['order_id'],'user_id' => $user_id,'from_status' => '0','to_status' => $batch['status']]);
                    $this->sendEmailService->sendBatchApprovalEmailToUser($order_details[0],$batch['reason']);

                    $user = $order_details[0]['user_id'];
                    $institute = $batch_details['0']['course_details']['institute_registration_detail']['user_id'];
                    
                    $data['batch_details'] = $batch_details[0];
                    $data['order_details'] = $order_details[0];

                    event(new BatchOrderNotification($institute, $user, 'batch_denied',$data));

                    $admins = \App\User::where('registered_as','admin')->get()->toArray();

                    foreach ($admins as $key => $admin) {
                        event(new BatchOrderNotification($institute, $admin['id'], 'batch_denied',$data));
                    }
                }

                $status = \CommonHelper::order_status()[$batch['status']];
                return response()->json(['status' => $status ,'message' => 'Status changed successfully.'], 200);
            }else{
                return response()->json(['message' => 'Order not found'], 400);
            }
        }else{
            return response()->json(['message' => 'Please confirm your batch first.'], 400);
        }
    }

    public function proceedToPayment(Request $request){

        if(Auth::check()){
            $user = Auth::user();
            $role = $user->registered_as;
            
            $payment_data = $request->toArray();
            $payment_data['user_id'] = $user->id;
            $payment_data['role'] = $role;

            if(isset($payment_data['order_id']) && !empty($payment_data['order_id'])){
                $order_id = $payment_data['order_id'];
                $order_details = $this->orderService->getOrderDetailById($order_id);

                $user_id = Auth::user()->id;
        
                if($order_details[0]['user_id'] != $user_id){
                    return response()->json(['message' => 'This order not belongs to you.'], 400);
                }
                //dd($order_details->toArray(),$payment_data);
                //To check order exists
                if(isset($order_details[0]) && !empty($order_details[0])){

                    //if($order_details[0]['status'] != '1'){
                    if($order_details[0]['status']){
                        $batch_details = $this->batchService->getDataByBatchId($payment_data['batch_id']);

                        if(!empty($batch_details->toArray())){
                            $batch_details = $batch_details->toArray();
                            $batch_data = $batch_details[0];

                            $cgst = env('CGST_PERCENT');
                            $sgst = env('SGST_PERCENT');

                            $cost = $batch_data['cost'];

                            if(isset($order_details[0]['discount']) && !empty($order_details[0]['discount'])){
                                $discount = $order_details[0]['discount'];
                                $discount_amt = round(($cost*$discount)/100);
                                $cost = $cost-$discount_amt;
                            }
                            $cgst_cost = round(($cost*$cgst)/100);
                            $sgst_cost = round(($cost*$sgst)/100);

                            $total = $cost+$cgst_cost+$sgst_cost;
                            //dd($payment_data);
                            if($batch_data['payment_type'] == 'split' && $payment_data['payment_by'] == 'split'){

                                $initial_amt = round(($total*$batch_data['initial_per'])/100);
                                $pending_amt = round(($total*$batch_data['pending_per'])/100);

                                $total_cost = $initial_amt;

                                if(isset($payment_data['pay']) && !empty($payment_data['pay']) && $payment_data['pay'] == 'split'){
                                    $total_cost = $pending_amt;
                                    $payuParams['split_payment'] = '2';
                                }

                                $payuParams['payment_type'] = 'split';
                            }else{
                                $total_cost = $total;
                                $payuParams['payment_type'] = 'single';
                            }

                            $payuParams['total'] = $total_cost;
                            $payuParams['order_id'] = $payment_data['order_id'];
                        }else{
                            return response()->json(['status' => 'failed','message' => 'Batch not found.'],400);
                        }
                    }else{
                        return response()->json(['status' => 'failed','message' => 'You have already made payment for this order.'],400);
                    }
                    
                }else{
                    return response()->json(['status' => 'failed','message' => 'Order not found.'],400);
                }
                
            }else{
                return response()->json(['status' => 'failed','message' => 'Order not found.'],400);
            }
            
            $result = $this->orderService->payUMoneyInstituteBatchParameters($payuParams);
            if($result['status'] == 'success'){
                //payumoney integration.
                return response()->json(['status' => 'success' , 'payumoney_form' => $result['payumoney_form']],200);
            }else{
                return response()->json(['status' => 'failed'],400);
            }
        }else{
            return redirect()->route('site.login');
        }
    }

    public function paymentGatewayResponse(Request $request,$order_id,$payment_type,$split_type=Null){
        
        $response = $request->toArray();
        $user_id = Auth::User()->id;
        $array = [
            'status' => $response['status'],
            'firstname' => $response['firstname'],
            'lastname' => $response['lastname'],
            'amount' => $response['amount'],
            'txnid' => $response['txnid'],
            'posted_hash' => $response['hash'],
            'key' => $response['key'],
            'productinfo' => $response['productinfo'],
            'email' => $response['email'],
            'phone' => $response['phone'],
            'mode' => $response['mode'],
            'transaction_id' => $response['mihpayid'],
            'salt' => env("PAYUMONEY_SALT"),
            'error' => $response['error'],
            'message' => $response['error_Message'],
            'bank_ref_num' => $response['bank_ref_num'],
            'unmappedstatus' => $response['unmappedstatus']
        ];

        $response = $this->orderService->orderPaymentGatewayResponse($array,$order_id,$payment_type,$split_type);

        //dd($array,$response);
        if($request['status'] == 'success'){

            $order_options['with'] = ['order_payments'];
            $order_details = $this->orderService->getOrderDetailById($order_id,$order_options);
            $batch_details = $this->batchService->getDataByBatchId($order_details[0]['batch_id']);
            $user_details = $this->userService->getDataByUserId($user_id);
            $batch_details[0]['order_details'] = $order_details->toArray();

            $update_batch_id = $order_details[0]['batch_id'];
            $available_size = ( $order_details[0]['available_size'] - 1 );
            \DB::table('institute_batches')->where('id', $update_batch_id)->update(['available_size' => $available_size]);

            if($payment_type == 'split'){
                if($split_type == 2){
                    OrderStatusHistory::create(['order_id' => $order_id,'user_id' => $user_id,'from_status' => '6','to_status' => '1']);

                    $institute = $batch_details['0']['course_details']['institute_registration_detail']['user_id'];

                    $data['batch_details'] = $batch_details[0]->toArray();
                    $data['user_details'] = $user_details[0]->toArray();
                    $data['order_details'] = $order_details[0];

                    event(new BatchOrderNotification($user_id, $institute, 'batch_confirmed',$data));

                    $admins = \App\User::where('registered_as','admin')->get()->toArray();

                    foreach ($admins as $key => $admin) {
                        event(new BatchOrderNotification($institute, $admin['id'], 'batch_confirmed',$data));
                    }
                }else{
                    OrderStatusHistory::create(['order_id' => $order_id,'user_id' => $user_id,'from_status' => '4','to_status' => '6']);

                    //Notification batch partially confirmed
                    $institute = $batch_details['0']['course_details']['institute_registration_detail']['user_id'];

                    $data['batch_details'] = $batch_details[0]->toArray();
                    $data['user_details'] = $user_details[0]->toArray();
                    $data['order_details'] = $order_details[0];

                    event(new BatchOrderNotification($user_id, $institute, 'batch_partially_confirmed',$data));

                    $admins = \App\User::where('registered_as','admin')->get()->toArray();

                    foreach ($admins as $key => $admin) {
                        event(new BatchOrderNotification($institute, $admin['id'], 'batch_partially_confirmed',$data));
                    }
                }
            }else{
                OrderStatusHistory::create(['order_id' => $order_id,'user_id' => $user_id,'from_status' => '4','to_status' => '1']);

                $institute = $batch_details['0']['course_details']['institute_registration_detail']['user_id'];

                $data['batch_details'] = $batch_details[0]->toArray();
                $data['user_details'] = $user_details[0]->toArray();
                $data['order_details'] = $order_details[0];

                event(new BatchOrderNotification($user_id, $institute, 'batch_confirmed',$data));

                $admins = \App\User::where('registered_as','admin')->get()->toArray();

                foreach ($admins as $key => $admin) {
                    event(new BatchOrderNotification($institute, $admin['id'], 'batch_confirmed',$data));
                }
            }

            $this->sendEmailService->sendBatchBlockSeatConfirmEmailToInstitute($batch_details[0],$user_details);
            $this->sendEmailService->sendBatchSeatConfirmEmailToSeafarer($batch_details[0],$user_details);

            return redirect()->route("site.view.batch.order",['order_id' => $order_id,'type' => $split_type]);
        }else{
            OrderStatusHistory::create(['order_id' => $order_id,'user_id' => $user_id,'from_status' => '4','to_status' => '2']);

            return redirect()->route("site.view.batch.order",$order_id);
        }
    }

    public function orderCancel(Request $request){

        if(Auth::check()){
            $user = Auth::user();
            $response = $request->toArray();
            $user_id = $user->id;

            $order_id = $response['order_id'];
            $batch_id = $response['batch_id'];
            
            $order_details = $this->orderService->getOrderDetailById($order_id);

            if(!empty($order_details) && $order_details[0]['status'] == '4'){
                $result = $this->orderService->cancelOrder($order_id,$user_id,$batch_id);
                
                if($result){
                    OrderStatusHistory::create(['order_id' => $order_id,'user_id' => $user_id,'from_Status' => 0,'to_status' => 3]);
                    $order_details = $this->orderService->getOrderDetailById($order_id);
                    $batch_details = $this->batchService->getDataByBatchId($order_details[0]['batch_id']);
                    $user_details = $this->userService->getDataByUserId($user_id);
                    
                    $this->sendEmailService->sendOrderCancelEmailToInstitute($batch_details[0],$user_details);

                    //Notification Order Cancelled
                    $institute = $batch_details['0']['course_details']['institute_registration_detail']['user_id'];

                    $data['batch_details'] = $batch_details[0]->toArray();
                    $data['user_details'] = $user_details[0]->toArray();
                    $data['order_details'] = $order_details[0];

                    event(new BatchOrderNotification($user_id, $institute, 'batch_cancelled',$data));

                    $admins = \App\User::where('registered_as','admin')->get()->toArray();

                    foreach ($admins as $key => $admin) {
                        event(new BatchOrderNotification($institute, $admin['id'], 'batch_cancelled',$data));
                    }

                    return response()->json(['status' => 'success' , 'message' => 'Order cancelled successfully.'],200);
                }else{
                    return response()->json(['status' => 'failed', 'message' => 'Order not found.'],400);
                }
            }else{
                return response()->json(['status' => 'failed', 'message' => 'You can not cancel order.'],400);
            }

        }else{
            return response()->json(['status' => 'failed', 'message' => 'Please login.'],400);
        }
    }

    public function addAdvertise(){
        $result = '';
        if (Auth::check()) {
            $user_id = Auth::user()->id;
            $options['with'] = ['institute_registration_detail.advertisment_details'];
            $result = $this->userService->getDataByUserId($user_id, $options)->toArray();
        }
        
        return view('site.institute.add_advertise',['data' => $result]);
    }

    public function storeAdvertise(Request $request){

        if (Auth::check()) {
            $user_id = Auth::user()->id;

            $data = $request->toArray();
            $result = $this->instituteService->storeAdvertise($data,$user_id);
            if( $result['status'] == 'success' ) {
                return response()->json(['redirect_url' => route('site.show.institute.details')], 200);
            } else {
                return response()->json($result, 400);
            }
        }else{
            return response()->json(['message' => 'Please Login'], 400);
        }
    }

    public function addNotice(){

        $result = '';
        if (Auth::check()) {
            $user_id = Auth::user()->id;
            $options['with'] = ['institute_registration_detail.institute_notice'];
            $result = $this->userService->getDataByUserId($user_id, $options)->toArray();
        }

        return view('site.institute.add_notice',['data' => $result]);
    }

    public function storeNotice(Request $request){
        if (Auth::check()) {
            $institute_id = Auth::user()->id;

            $data = $request->toArray();
            $data['institute_id'] = $institute_id;

            $data['start_date'] = date('Y-m-d', strtotime($data['start_date']));
            $data['end_date'] = date('Y-m-d', strtotime($data['end_date']));
            
            $result = $this->instituteService->storeNotice($data,$institute_id);
            if( $result['status'] == 'success' ) {
                return response()->json(['message' => 'Notice stored successsfully.','redirect_url' => route('site.show.institute.details')], 200);
            } else {
                return response()->json($result, 400);
            }
        }else{
            return response()->json(['message' => 'Please Login'], 400);
        }
    }

    public function addImageGallery(){
        $result = '';
        if (Auth::check()) {
            $institute_id = Auth::user()->id;
            $options['with'] = ['institute_registration_detail.institute_gallery'];
            $result = $this->userService->getDataByUserId($institute_id, $options)->toArray();
        }

        return view('site.institute.image_gallery',['data' => $result]);
    }

    public function storeImageGallery(Request $request){

        if (Auth::check()) {
            $data = $request->toArray();
            $institute_id = Auth::user()->id;

            $data['institute_id'] = $institute_id;

            $image_list = InstituteImageGallery::where('institute_id',$institute_id)->get();

            if(isset($image_list) && count($image_list) > 14){
                return response()->json(['message' => "You can upload maximum 15 images only."], 400);
            }

            $result = $this->instituteService->storeImageGallery($data,$institute_id);

            $options['with'] = ['institute_registration_detail.institute_gallery'];
            $images = $this->userService->getDataByUserId($institute_id, $options)->toArray();

            $image_gallery = View::make("site.partials.image_gallery")->with(['data' => $images]);
            $template = $image_gallery->render();

            if( $result['status'] == 'success' ) {
                if($request->isXmlHttpRequest()){
                    return response()->json(['message' => "Image added successfully.",'template' => $template], 200);
                }else{
                    return response()->json(['message' => "Image added successfully."], 200);
                }

            } else {
                return response()->json(['message' => "Error."], 400);
            }
        }else{
            return response()->json(['message' => 'Please Login'], 400);
        }
    }

    public function deleteImageGallery(Request $request){
        if (Auth::check()) {
            $data = $request->toArray();
            $institute_id = Auth::user()->id;

            $image_id = $data['image_id'];

            $result = InstituteImageGallery::where('institute_id',$institute_id)->where('id',$image_id)->delete();

            $options['with'] = ['institute_registration_detail.institute_gallery'];
            $images = $this->userService->getDataByUserId($institute_id, $options)->toArray();
            
            $image_gallery = View::make("site.partials.image_gallery")->with(['data' => $images]);
            $template = $image_gallery->render();

            if($result) {
                return response()->json(['message' => "Image deleted successfully.",'template' => $template], 200);
            } else {
                return response()->json(['message' => "Image not found."], 400);
            }
        }else{
            return response()->json(['message' => 'Please Login'], 400);
        }
    }

    public function onDemandBookings(Request $request){
        
        if(Auth::check()){
            $institute_id = Auth::User()->id;
            $filter = $request->toArray();
            
            $registration_data = $this->instituteService->getDetailsByCompanyId($institute_id)->toArray();
            $institute_reg_id = $registration_data[0]['id'];

            //last 0 for no pagination
            $batches = $this->instituteService->getAllOnDemandBatchesByInstituteId($institute_reg_id,'','','no');

            $all_batches = $batches->toArray();
            foreach ($all_batches['data'] as $key => $value) {
                # code...
                if(isset($value['on_demand_batch_type']) && $value['on_demand_batch_type'] == '1'){
                    $all_batches['data'][$key]['group'] = [
                        '1' => [],
                        '2' => [],
                        '3' => [],
                        '4' => [],
                    ];
                }
                
                if(isset($value['on_demand_batch_type']) && $value['on_demand_batch_type'] == '2'){
                    $all_batches['data'][$key]['group'] = [
                        '1-2' => [],
                        '3-4' => [],
                    ];
                }

                if(isset($value['on_demand_batch_type']) && $value['on_demand_batch_type'] == '3'){
                    $all_batches['data'][$key]['group'] = [
                        'monthly' => [],
                    ];
                }
                
                if(isset($value['orders']) && !empty($value['orders'])){

                    foreach ($value['orders'] as $key1 => $value1) {
                        $all_batches['data'][$key]['group'][$value1['preffered_type']][] = $value1;
                    }
                   
                }

            }

            //dd($all_batches);
            
            $course_name = \App\InstituteCourses::all()->toArray();
            return view('site.institute.on_demand_course_listing',['batches' => $all_batches, 'search_data' => '','pagination' => $batches, 'course_list' => $course_name, 'filter' => $filter]);

        }else{
           return response()->json(['status' => 'error', 'message' => 'Please login to continue.'], 400); 
        }

    }

    public function onDemandBatchConfirm(Request $request){

        if(Auth::check()){
            
            $data = $request->toArray();

            $batch_id = $data['batch_id'];
            $preffered_type = $data['selected_week'];

            $batch_data['start_date'] = date('Y-m-d',strtotime($data['dbdatepicker']));
            $batch_data['batch_type'] = 'confirmed';
            $batch_data['on_demand_batch_type'] = NULL;
            
            $batch_details = $this->batchService->getDataByBatchId($batch_id)->toArray();
            //dd(count($batch_details[0]['child_batches']));
            $batch_details[0]['preffered_type'] = $data['selected_week'];
            $batch_details[0]['parent_batch_id'] = $batch_id;
            $batch_details[0]['start_date'] = $batch_data['start_date'];
            $batch_details[0]['batch_type'] = 'confirmed';
            $batch_details[0]['course_start_date'] = $batch_data['start_date'];
            //dd($batch_details,$data);
            $result = InstituteBatches::create($batch_details[0]);

            if(isset($batch_details[0]['batch_location']) && !empty($batch_details[0]['batch_location'])){
                foreach ($batch_details[0]['batch_location'] as $key => $value) {
                    $location['batch_id'] = $result->id;
                    $location['location_id'] = $value['location_id'];
                    $location['status'] = '1';
                    $this->batchService->storeBatchLocation($location);
                }
            }

            if(isset($batch_details[0]['discount']) && !empty($batch_details[0]['discount'])){
                foreach ($batch_details[0]['discount'] as $key => $value) {
                    $discount['batch_id'] = $result->id;
                    $discount['advertise_id'] = $value['advertise_id'];
                    $discount['discount'] = $value['discount'];
                    AdvertiserCourseDiscountMapping::create($discount);
                }
            }
            //dd($batch_details[0],$result->toArray());

            if($preffered_type == ''){
                $result = $this->batchService->updateBatchDataByBatchId($batch_id,$batch_data);
            }else{
                //Update orders
                $order_data['batch_id'] = $result->id;
                $this->orderService->updateOrderByBatchIdAndPreffType($batch_id,$preffered_type,$order_data);

                if($preffered_type == 'Monthly'){
                    $this->batchService->disableBatchByBatchId($batch_id);
                }

                if($preffered_type == '3-4' || $preffered_type == '1-2'){
                    if(isset($batch_details[0]['child_batches']) && count($batch_details[0]['child_batches'])+1 > 1){
                        $this->batchService->disableBatchByBatchId($batch_id);
                    }
                }

                if($preffered_type == '1' || $preffered_type == '2' || $preffered_type == '3' || $preffered_type == '4'){
                    if(isset($batch_details[0]['child_batches']) && count($batch_details[0]['child_batches'])+1 > 3){
                        $this->batchService->disableBatchByBatchId($batch_id);
                    }
                }

                if($result){
                    return response()->json(['status' => 'success', 'message' => 'Batch confirmed successfully.'], 200); 
                }else{
                    return response()->json(['status' => 'error', 'message' => 'Batch not found.'], 400); 
                }
            }

        }else{
           return response()->json(['status' => 'error', 'message' => 'Please login to continue.'], 400); 
        }
    }

    public function getInstituteAdvertiseByBatchId(Request $request){

        $data = $request->toArray();

        $advertisment_details = AdvertiserCourseDiscountMapping::with('advertisement_details.company_registration_details')->where('batch_id',$request->batch_id)->get()->random(1)->toArray();
        //dd($advertisment_details);
        $image_path = asset(env('ADVERTISE_PATH').$advertisment_details['advertisement_details']['company_registration_details']['id'].'/'.$advertisment_details['advertisement_details']['img_path']);

        return response()->json(['status' => 'success', 'image_path' => $image_path], 200); 
    }
}
