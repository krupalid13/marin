<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\UserService;

class SearchCandidateController extends Controller
{
    private $userService;

    public function __construct()
    {
        $this->userService = new UserService();
    }
}
