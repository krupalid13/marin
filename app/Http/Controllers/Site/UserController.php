<?php

namespace App\Http\Controllers\Site;

use App\Courses;
use App\Services\SendEmailService;
use App\Services\OrderService;
use App\Services\UserProfessionalDetailsService;
use App\SharedDocsToOthers;
use App\UserLanguage;
use App\Jobs\SendEmailShareDocs;
use App\Repositories\ShareContactsRepository;
use App\User;
use App\UserCoursesCertificate;
use App\UserCoeDetails;
use App\UserPersonalDetail;
use App\UserDangerousCargoEndorsementDetail;
use App\UserSemanBookDetail;
use App\UserDocuments;
use App\UserGmdssDetail;
use App\UserProfessionalDetail;
use App\UserCocDetail;
use App\UserSeaService;
use App\UserDocumentsType;
use App\UserPassportDetail;
use App\UserWkfrDetail;
use App\EnginType;
use CommonHelper;
use S3;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\UserService;
use App\Services\DocumentService;
use Auth;
use phpDocumentor\Reflection\Types\Null_;
use App\Http\Requests;
use Route;
use View;
use File;
use Response;
use ZipArchive;
use RecursiveIteratorIterator;
use RecursiveDirectoryIterator;
use Redirect;
use Session;
use Mail;
use DB;
use PDF;
use App\Services\PincodeService;
use Datatables;
use App\ShareHistory;
use App\HistoryLog;
use App\PromotionalEmailLog;
use App\Repositories\UserRepository;

class UserController extends Controller
{
    private $userService;
    private $documentService;
    private $orderService;
	private $sendEmailService;
    private $pincodeService;
    private $userRepository;

    public function __construct()
    {
        $this->userService = new UserService();
        $this->orderService = new OrderService();
        $this->documentService = new DocumentService();
        $this->userProfessionalDetailsService = new UserProfessionalDetailsService();
        $this->userDangerousCargoEndorsementDetail = new UserDangerousCargoEndorsementDetail();
        $this->pincodeService = new PincodeService();
        $this->userRepository = new UserRepository();
        
    }

    public function registration(){
        if( Auth::check() ) {
            return redirect()->route('home');
        }
        $courses = $this->userService->getAllCourses();
       return view('site.registration',['all_courses' => $courses]);
    }
    
public function otpVerify(Request $request){
    // $otpData = $request->toArray();
    print_r($request);
    die;
}
     public function profile( Request $request, $user_id = null) {
        //Auth::loginUsingId(19);
        
        $user = '';
        $role = '';
        if(isset($user_id)){
            $user = 'another_user';
        }
        if(Auth::check() && !isset($user_id)){
            $user_id = Auth::user()->id;

        }elseif (isset($user_id) && $user_id != '') {
            # code...
        }
        
        if(Auth::check()){
            $role = Auth::user()->registered_as;
        }

        if(isset($user_id)){

            $options['with'] = ['personal_detail','professional_detail','passport_detail','seaman_book_detail','coc_detail','gmdss_detail','wkfr_detail','personal_detail.state','personal_detail.city','personal_detail.pincode.pincodes_states.state','personal_detail.pincode.pincodes_cities.city','sea_service_detail','course_detail','cop_detail','coe_detail','document_permissions.requester.company_registration_detail','document_permissions.requester.institute_registration_detail','document_permissions.document_type.type'];
            $result = $this->userService->getDataByUserId($user_id, $options)->toArray();
            if(isset($result[0]['professional_detail']['current_rank_exp'])){
                $rank_exp = explode(".", $result[0]['professional_detail']['current_rank_exp']);
                $result[0]['professional_detail']['years'] = $rank_exp[0];
                $result[0]['professional_detail']['months'] = ltrim($rank_exp[1],0);
            }
            
            $courses = $this->userService->getAllCourses();

            $user_documents = $this->userService->getUserDocuments($user_id);
            $uploaded_documents = [];
            // dd($user_documents);

            if(isset($result[0]['professional_detail']['current_rank']) && !empty($result[0]['professional_detail']['current_rank'])){
                $required_documents = \CommonHelper::rank_required_fields()[$result[0]['professional_detail']['current_rank']];
            }
            
            //if(isset($required_documents) && !empty($required_documents)){

                //Passport visa block
                if(isset($result[0]['passport_detail']) && !empty($result[0]['passport_detail'])){
                    $uploaded_documents['passport'] = $result[0]['passport_detail'];
                    $uploaded_documents['passport']['title'] = 'Passport / Visa';

                    if(isset($user_documents['passport']) && !empty($user_documents['passport'])){
                        $uploaded_documents['passport']['imp_doc'] = $user_documents['passport'];
                    }
                }
                if(isset($result[0]['passport_detail']['us_visa']) && $result[0]['passport_detail']['us_visa'] == '1'){
                    $uploaded_documents['visa'] = $result[0]['passport_detail'];
                    if(isset($result[0]['passport_detail']) && !empty($result[0]['passport_detail'])){
                        $uploaded_documents['visa']['hide'] = 1;
                        $uploaded_documents['visa']['title'] = 'Passport / Visa';
                    }

                    if(isset($user_documents['visa']) && !empty($user_documents['visa'])){
                        $uploaded_documents['visa']['imp_doc'] = $user_documents['visa'];
                    }
                }
                
                //CDC
                if(isset($result[0]['seaman_book_detail']) && !empty($result[0]['seaman_book_detail'])){
                    $uploaded_documents['cdc'] = $result[0]['seaman_book_detail'];

                    if(isset($uploaded_documents['cdc']) && !empty($uploaded_documents['cdc'])){
                        foreach ($uploaded_documents['cdc'] as $key => $value) {
                            if(isset($user_documents['cdc'][$key+1]['user_documents']) && !empty($user_documents['cdc'][$key+1]['user_documents'])){
                                $uploaded_documents['cdc'][$key]['title'] = 'CDC';

                                if(isset($user_documents['cdc'][$key+1]) && !empty($user_documents['cdc'][$key+1])){
                                    $uploaded_documents['cdc'][$key]['imp_doc'] = $user_documents['cdc'][$key+1];
                                }
                            }
                        }
                    }
                }
                
                //Coc Coe Block
                //if(in_array('COC', $required_documents)){
                    $uploaded_documents['coc'] = $result[0]['coc_detail'];
                    
                    if(isset($uploaded_documents['coc']) && !empty($uploaded_documents['coc'])){
                        foreach ($uploaded_documents['coc'] as $key => $value) {
                            if(isset($user_documents['coc'][$key+1]['user_documents']) && !empty($user_documents['coc'][$key+1]['user_documents'])){
                                $uploaded_documents['coc'][$key]['title'] = 'COC / COE';

                                if(isset($user_documents['coc'][$key+1]) && !empty($user_documents['coc'][$key+1]))
                                    $uploaded_documents['coc'][$key]['imp_doc'] = $user_documents['coc'][$key+1];
                            }
                        }
                    }
                //}

                //if(in_array('COE', $required_documents) || in_array('COE-Optional', $required_documents)){
                    $uploaded_documents['coe'] = $result[0]['coe_detail'];

                    if(in_array('COC', $required_documents)){
                        foreach ($uploaded_documents['coe'] as $key => $value) {
                            if(isset($user_documents['coe'][$key+1]['user_documents']) && !empty($user_documents['coe'][$key+1]['user_documents'])){
                                $uploaded_documents['coe'][$key]['hide'] = 1;
                                $uploaded_documents['coe'][$key]['title'] = 'COC / COE';

                                if(isset($user_documents['coe'][$key+1]) && !empty($user_documents['coe'][$key+1]))
                                    $uploaded_documents['coe'][$key]['imp_doc'] = $user_documents['coe'][$key+1];
                            }
                        }
                    }
                //}

                //GMDSS watch keeping
                //if(in_array('GMDSS', $required_documents) || in_array('GMDSS-Optional', $required_documents)){
                    $uploaded_documents['gmdss'] = $result[0]['gmdss_detail'];
                    $uploaded_documents['gmdss']['title'] = 'GMDSS / WK';

                    if(isset($user_documents['gmdss']) && !empty($user_documents['gmdss'])){
                        $uploaded_documents['gmdss']['imp_doc'] = $user_documents['gmdss'];
                    }
                //}

                //if(in_array('WATCH_KEEPING-Optional', $required_documents)){
                    $uploaded_documents['watch keeping'] = $result[0]['wkfr_detail'];
                    $uploaded_documents['watch keeping']['title'] = 'GMDSS / WK';

                    if(in_array('GMDSS', $required_documents) || in_array('GMDSS-Optional', $required_documents)){
                        $uploaded_documents['watch keeping']['hide'] = 1;

                        if(isset($user_documents['watch keeping']) && !empty($user_documents['watch keeping'])){
                            $uploaded_documents['watch keeping']['imp_doc'] = $user_documents['watch keeping'];
                        }

                    }
               // }

                //courses
                if(isset($result[0]['course_detail']) && !empty($result[0]['course_detail'])){
                    $uploaded_documents['courses'] = $result[0]['course_detail'];

                    foreach ($uploaded_documents['courses'] as $key => $value) {
                        if(isset($user_documents['courses'][$key+1]['user_documents']) && !empty($user_documents['courses'][$key+1]['user_documents'])){
                            $uploaded_documents['courses'][$key]['title'] = 'PreSea / PostSea Course Certificates';

                            if(isset($user_documents['courses'][$key+1]) && !empty($user_documents['courses'][$key+1]))
                                $uploaded_documents['courses'][$key]['imp_doc'] = $user_documents['courses'][$key+1];
                        }
                    }
                }
                
                //sea service
                if(isset($result[0]['sea_service_detail']) && !empty($result[0]['sea_service_detail'])){
                    $uploaded_documents['sea service'] = $result[0]['sea_service_detail'];

                    foreach ($result[0]['sea_service_detail'] as $key => $value) {
                        if(isset($user_documents['sea service'][$key+1]['user_documents']) && !empty($user_documents['sea service'][$key+1]['user_documents'])){
                            $uploaded_documents['sea service'][$key]['title'] = 'Sea Service Letters';

                            if(isset($user_documents['sea service'][$key+1]) && !empty($user_documents['sea service'][$key+1])){
                                $uploaded_documents['sea service'][$key]['imp_doc'] = $user_documents['sea service'][$key+1];
                            }
                        }
                    }
                }

                //yf vaccination
                if(isset($result[0]['wkfr_detail']) && $result[0]['wkfr_detail']['yellow_fever'] == '1'){
                    $uploaded_documents['yellow fever'] = $result[0]['wkfr_detail'];
                    if(isset($user_documents['yellow fever'][0]['user_documents']) && !empty($user_documents['yellow fever'][0]['user_documents'])){
                        $uploaded_documents['yellow fever']['title'] = 'Medicals / YF Vaccination';

                        if(isset($user_documents['yellow fever']) && !empty($user_documents['yellow fever'])){
                            $uploaded_documents['yellow fever']['imp_doc'] = $user_documents['yellow fever'];
                        }
                    }
                }
            //}

            $count = 0;
            if(isset($result[0]['course_detail'])){
                foreach ($result[0]['course_detail'] as $key => $value) {
                    if($value['course_type'] == 'Value Added'){
                        $result[0]['value_added_course_detail'][$count] = $value;
                        $count++;
                        unset($result[0]['course_detail'][$key]);
                    }
                }
            }
            // dd($courses);

            if(count($result) > 0 && ($result[0]['registered_as'] == null || $result[0]['registered_as'] != 'advertiser')){

//                set var for rank
                $show_resume = CommonHelper::getResumeNumber();
                return view('site.user.my_profile1',['data' => $result,'user' => $user, 'all_courses' => $courses,'role' => $role,'user_documents' => $user_documents,'user_id' => $user_id, 'documents' => $uploaded_documents, 'show_resume' => $show_resume]);
            }else{
                return view('errors.404');
            }

            return view('site.user.profile');
        }
    }

    public function profile1($user_id = null)
    {

        //Auth::loginUsingId(19);

        $user = '';
        $role = '';
        if(isset($user_id)){
            $user = 'another_user';
        }
        if(Auth::check() && !isset($user_id)){
            $user_id = Auth::user()->id;

        }elseif (isset($user_id) && $user_id != '') {
            # code...
        }
        
        if(Auth::check()){
            $role = Auth::user()->registered_as;
        }

        if(isset($user_id)){

            $options['with'] = ['personal_detail','professional_detail','passport_detail','seaman_book_detail','coc_detail','gmdss_detail','wkfr_detail','personal_detail.state','personal_detail.city','personal_detail.pincode.pincodes_states.state','personal_detail.pincode.pincodes_cities.city','sea_service_detail','course_detail','cop_detail','coe_detail','document_permissions.requester.company_registration_detail','document_permissions.requester.institute_registration_detail','document_permissions.document_type.type'];
            $result = $this->userService->getDataByUserId($user_id, $options)->toArray();
            if(isset($result[0]['professional_detail']['current_rank_exp'])){
                $rank_exp = explode(".", $result[0]['professional_detail']['current_rank_exp']);
                $result[0]['professional_detail']['years'] = $rank_exp[0];
                $result[0]['professional_detail']['months'] = ltrim($rank_exp[1],0);
            }
            
            $courses = $this->userService->getAllCourses();

            $user_documents = $this->userService->getUserDocuments($user_id);
            $uploaded_documents = [];
            // dd($user_documents);

            if(isset($result[0]['professional_detail']['current_rank']) && !empty($result[0]['professional_detail']['current_rank'])){
                $required_documents = \CommonHelper::rank_required_fields()[$result[0]['professional_detail']['current_rank']];
            }
            
            if(isset($required_documents) && !empty($required_documents)){

                //Passport visa block
                if(isset($result[0]['passport_detail']) && !empty($result[0]['passport_detail'])){
                    $uploaded_documents['passport'] = $result[0]['passport_detail'];
                    $uploaded_documents['passport']['title'] = 'Passport / Visa';

                    if(isset($user_documents['passport']) && !empty($user_documents['passport'])){
                        $uploaded_documents['passport']['imp_doc'] = $user_documents['passport'];
                    }
                }
                if(isset($result[0]['passport_detail']['us_visa']) && $result[0]['passport_detail']['us_visa'] == '1'){
                    $uploaded_documents['visa'] = $result[0]['passport_detail'];
                    if(isset($result[0]['passport_detail']) && !empty($result[0]['passport_detail'])){
                        $uploaded_documents['visa']['hide'] = 1;
                        $uploaded_documents['visa']['title'] = 'Passport / Visa';
                    }

                    if(isset($user_documents['visa']) && !empty($user_documents['visa'])){
                        $uploaded_documents['visa']['imp_doc'] = $user_documents['visa'];
                    }
                }
                
                //CDC
                if(isset($result[0]['seaman_book_detail']) && !empty($result[0]['seaman_book_detail'])){
                    $uploaded_documents['cdc'] = $result[0]['seaman_book_detail'];

                    if(isset($uploaded_documents['cdc']) && !empty($uploaded_documents['cdc'])){
                        foreach ($uploaded_documents['cdc'] as $key => $value) {
                            if(isset($user_documents['cdc'][$key+1]['user_documents']) && !empty($user_documents['cdc'][$key+1]['user_documents'])){
                                $uploaded_documents['cdc'][$key]['title'] = 'CDC';

                                if(isset($user_documents['cdc'][$key+1]) && !empty($user_documents['cdc'][$key+1])){
                                    $uploaded_documents['cdc'][$key]['imp_doc'] = $user_documents['cdc'][$key+1];
                                }
                            }
                        }
                    }
                }
                
                //Coc Coe Block
                if(in_array('COC', $required_documents)){
                    $uploaded_documents['coc'] = $result[0]['coc_detail'];
                    
                    if(isset($uploaded_documents['coc']) && !empty($uploaded_documents['coc'])){
                        foreach ($uploaded_documents['coc'] as $key => $value) {
                            if(isset($user_documents['coc'][$key+1]['user_documents']) && !empty($user_documents['coc'][$key+1]['user_documents'])){
                                $uploaded_documents['coc'][$key]['title'] = 'COC / COE';

                                if(isset($user_documents['coc'][$key+1]) && !empty($user_documents['coc'][$key+1]))
                                    $uploaded_documents['coc'][$key]['imp_doc'] = $user_documents['coc'][$key+1];
                            }
                        }
                    }
                }

                if(in_array('COE', $required_documents) || in_array('COE-Optional', $required_documents)){
                    $uploaded_documents['coe'] = $result[0]['coe_detail'];

                    if(in_array('COC', $required_documents)){
                        foreach ($uploaded_documents['coe'] as $key => $value) {
                            if(isset($user_documents['coe'][$key+1]['user_documents']) && !empty($user_documents['coe'][$key+1]['user_documents'])){
                                $uploaded_documents['coe'][$key]['hide'] = 1;
                                $uploaded_documents['coe'][$key]['title'] = 'COC / COE';

                                if(isset($user_documents['coe'][$key+1]) && !empty($user_documents['coe'][$key+1]))
                                    $uploaded_documents['coe'][$key]['imp_doc'] = $user_documents['coe'][$key+1];
                            }
                        }
                    }
                }

                //GMDSS watch keeping
                if(in_array('GMDSS', $required_documents) || in_array('GMDSS-Optional', $required_documents)){
                    $uploaded_documents['gmdss'] = $result[0]['gmdss_detail'];
                    $uploaded_documents['gmdss']['title'] = 'GMDSS / WK';

                    if(isset($user_documents['gmdss']) && !empty($user_documents['gmdss'])){
                        $uploaded_documents['gmdss']['imp_doc'] = $user_documents['gmdss'];
                    }
                }

                if(in_array('WATCH_KEEPING-Optional', $required_documents)){
                    $uploaded_documents['watch keeping'] = $result[0]['wkfr_detail'];
                    $uploaded_documents['watch keeping']['title'] = 'GMDSS / WK';

                    if(in_array('GMDSS', $required_documents) || in_array('GMDSS-Optional', $required_documents)){
                        $uploaded_documents['watch keeping']['hide'] = 1;

                        if(isset($user_documents['watch keeping']) && !empty($user_documents['watch keeping'])){
                            $uploaded_documents['watch keeping']['imp_doc'] = $user_documents['watch keeping'];
                        }

                    }
                }

                //courses
                if(isset($result[0]['course_detail']) && !empty($result[0]['course_detail'])){
                    $uploaded_documents['courses'] = $result[0]['course_detail'];

                    foreach ($uploaded_documents['courses'] as $key => $value) {
                        if(isset($user_documents['courses'][$key+1]['user_documents']) && !empty($user_documents['courses'][$key+1]['user_documents'])){
                            $uploaded_documents['courses'][$key]['title'] = 'PreSea / PostSea Course Certificates';

                            if(isset($user_documents['courses'][$key+1]) && !empty($user_documents['courses'][$key+1]))
                                $uploaded_documents['courses'][$key]['imp_doc'] = $user_documents['courses'][$key+1];
                        }
                    }
                }
                
                //sea service
                if(isset($result[0]['sea_service_detail']) && !empty($result[0]['sea_service_detail'])){
                    $uploaded_documents['sea service'] = $result[0]['sea_service_detail'];

                    foreach ($result[0]['sea_service_detail'] as $key => $value) {
                        if(isset($user_documents['sea service'][$key+1]['user_documents']) && !empty($user_documents['sea service'][$key+1]['user_documents'])){
                            $uploaded_documents['sea service'][$key]['title'] = 'Sea Service Letters';

                            if(isset($user_documents['sea service'][$key+1]) && !empty($user_documents['sea service'][$key+1])){
                                $uploaded_documents['sea service'][$key]['imp_doc'] = $user_documents['sea service'][$key+1];
                            }
                        }
                    }
                }

                //yf vaccination
                if(isset($result[0]['wkfr_detail']) && $result[0]['wkfr_detail']['yellow_fever'] == '1'){
                    $uploaded_documents['yellow fever'] = $result[0]['wkfr_detail'];
                    if(isset($user_documents['yellow fever'][0]['user_documents']) && !empty($user_documents['yellow fever'][0]['user_documents'])){
                        $uploaded_documents['yellow fever']['title'] = 'Medicals / YF Vaccination';

                        if(isset($user_documents['yellow fever']) && !empty($user_documents['yellow fever'])){
                            $uploaded_documents['yellow fever']['imp_doc'] = $user_documents['yellow fever'];
                        }
                    }
                }
            }

            $count = 0;
            if(isset($result[0]['course_detail'])){
                foreach ($result[0]['course_detail'] as $key => $value) {
                    if($value['course_type'] == 'Value Added'){
                        $result[0]['value_added_course_detail'][$count] = $value;
                        $count++;
                        unset($result[0]['course_detail'][$key]);
                    }
                }
            }
            // dd($courses);

            if(count($result) > 0 && ($result[0]['registered_as'] == null || $result[0]['registered_as'] != 'advertiser')){
                return view('site.user.my_profile1',['data' => $result,'user' => $user, 'all_courses' => $courses,'role' => $role,'user_documents' => $user_documents,'user_id' => $user_id, 'documents' => $uploaded_documents]);
            }else{
                return view('errors.404');
            }
           
            return view('site.user.profile');
        }
    }

    public function editProfile() {
        if(Auth::check()) {
            $pageTitle = "Edit Profile - Adding personal and professional information to create your Profile, Resume, Infographic and more | Flanknot";
            $metaDescription = "At Flanknot, Seafarer can create and update their Personal, contact, Professional, Service & Course details.";
            $metaKeywords = "Residential address, gender, contact, Current Rank, Applied Rank, Expected Salary, Passport, Visa, Indos, CDC, Continuous discharge Certificate, Seaman book information, WK, Watchkeeping Certificate, COP, Certificate of Proficiency, COC, Certificate of Competency, COE, Certificate of Endorsement, DCE, Dangerous Cargo Endorsement, GMDSS, Global Maritime Distress and Safety System, FRAMO, DP, Dynamic positioning, Sea Service, vessel name, owner company, manning company, vessel flag, vessel type, vessel GRT, vessel BHP, vessel engine type, sing on date and sign off date, Course details, basic type courses, Advance type courses, Offshore related courses, Tanker related courses, Refresher and Upgradation courses, Management courses, Academic details, ILO Medical and Screening Medical details, Yellow fever vaccination, Cholera vaccination, Hepatitis B vaccination, D&T vaccination, covid vaccination, Character Certificate, PCC, PAN, Aadhar, Bank details. Uploading seafarer documents.";
            $user_id = Auth::user()->id;
            $options['with'] = ['userDangerousCargoEndorsementDetail', 'personal_detail', 'professional_detail', 'passport_detail', 'seaman_book_detail', 'coc_detail', 'gmdss_detail', 'wkfr_detail', 'personal_detail.state', 'personal_detail.city', 'personal_detail.pincode.pincodes_states.state', 'personal_detail.pincode.pincodes_cities.city','sea_service_detail','course_detail','cop_detail','coe_detail','languagesknown','visas'];
            $result = $this->userService->getDataByUserId($user_id, $options)->toArray();
           
            if (isset($result[0]['professional_detail']['current_rank_exp']) && !empty($result[0]['professional_detail']['current_rank_exp'])){
                $rank_exp = explode(".", $result[0]['professional_detail']['current_rank_exp']);
                $result[0]['professional_detail']['years'] = $rank_exp[0];
                $result[0]['professional_detail']['months'] = $rank_exp[1];
            }

            $courses = $this->userService->getAllCourses();
            $user_documents = $this->userService->getUserDocuments($user_id);
          
            $count = 0;
//            dd(\CommonHelper::courses_by_table());
            foreach ($result[0]['course_detail'] as $key => $value) {
                if($value['course_type'] == 'Value Added'){
                    $result[0]['value_added_course_detail'][$count] = $value;
                    $count++;
                    unset($result[0]['course_detail'][$key]);
                }
            }
            
            $name = Route::currentRouteName();
            $result['current_route'] = $name;
            if(count($result) > 0 && ($result[0]['registered_as'] == null || $result[0]['registered_as'] == 'seafarer')){
                $percentageMb = 0;
                $usedMb = 0;
                if(!empty(Auth::user()->used_kb)){
                    $usedMb = Auth::user()->used_kb / 1024;
                    $maxKb = Auth::user()->max_kb / 1024;
                    $percentageMb = ($usedMb * 100) / $maxKb; 
                }
                return view('site.registration',['user_data' => $result,'usedMb'=>$usedMb,'percentageMb'=>$percentageMb,'all_courses' => $courses, 'user_documents' => $user_documents, 'pageTitle' => $pageTitle, 'metaDescription' => $metaDescription, 'metaKeywords' => $metaKeywords]);
            }else{
                return view('errors.404');
            }
            
        }else{
            return redirect()->route('home');
        }
        return view('site.user.profile');
    }

    public function storeDetails(Requests\SeafarerRegistrationRequest $seafarerRegistrationRequest){

        $userData = $seafarerRegistrationRequest->toArray();
        $userData['otp'] = $this->userService->generateOTP();
        $userData['registered_as'] = 'seafarer';
        $logged_user_data = NULL;
        if(Auth::check()){
            $logged_user_data = Auth::user()->toArray();
        }
        $result = $this->userService->store($userData,$logged_user_data);

        if($result['status'] == 'success'){
            return response()->json(['status' => 'success' , 'message' => 'Your basic details has been store successfully.', 'update_mobile' => '0'],200);
        }else{
            return response()->json(['status' => 'failed' , 'message' => 'Error while adding data.'],400);
        }
    }

    public function updateProfileDetails(Requests\SeafarerProfileRegistrationRequest $seafarerProfileRegistrationRequest){
        $userData = $seafarerProfileRegistrationRequest->toArray();      
        $logged_user_data = Auth::user()->toArray();
        $result = $this->userService->store($userData,$logged_user_data);
        if(Auth::check()){
            $logged_user_data = Auth::user()->toArray();
        }
        $user = $this->userService->getDataByUserId($logged_user_data['id'])->toArray();
        if($result['status'] == 'success'){
            return response()->json(['status' => 'success' , 'update_email' => $user[0]['is_email_verified'] , 'update_mobile' => $user[0]['is_mob_verified'] , 'message' => 'Your basic details has been store successfully.','update'=>$result['user']['update']],200);
        }else{
            return response()->json(['status' => 'failed' , 'message' => 'Error while adding data.'],400);
        }
    }

	
    public function storeDocuments(Requests\SeafererDocumentDetails $seafarerDocumentDetails){
        $userData = $seafarerDocumentDetails->toArray();
        $result = $this->userService->storeDocuments($userData);

        $userId = Auth::user()->id;
        $dceDetails = $this->userDangerousCargoEndorsementDetail->whereUserId($userId)->first();
        $dceStatus = isset($dceDetails) && !empty($dceDetails->status) ? json_decode($dceDetails->status) : null;
        $dceRadio = isset($userData['dce_radio']) ? $userData['dce_radio'] : 0;
        if (!empty($dceDetails) && $dceStatus->status = 1 && $userData['dce_radio'] == 0) {
            $types = ['oil', 'lequefied_gas', 'chemical', 'all'];
//            foreach ($types as $deeType) {
//                $decDcoumentsTypes = UserDocumentsType::whereUserId($userId)->whereType('dce')->where('type_id', $deeType)->with('user_documents')->get();
//                if (!empty($decDcoumentsTypes)) {
//                    $this->userService->destroyCdcDocuments($decDcoumentsTypes);
//                }
//            }
            $this->destroyUserDocumentsnew(['dce'], $userId);
            $this->userDangerousCargoEndorsementDetail->destroy($userId);
        }else if($dceRadio == 0){
            $types = ['oil', 'lequefied_gas', 'chemical', 'all'];
            $this->destroyUserDocumentsnew(['dce'], $userId);
            $this->userDangerousCargoEndorsementDetail->destroy($userId);
        }else{
            $deeTypes = ['oil', 'lequefied_gas', 'chemical', 'all'];
            $dceAdded = isset($userData['dce']) ? $userData['dce'] : [];
            if(!empty($dceAdded)){
                foreach ($dceAdded as $dceAdd){
                    if (($dcekey = array_search($dceAdd, $deeTypes)) !== false) {
                        unset($deeTypes[$dcekey]);
                    }
                }
            }
            if(!empty($deeTypes)){
                foreach ($deeTypes as $deeType) {
                    $decDcoumentsTypes = UserDocumentsType::whereUserId($userId)->whereType('dce')->where('type_id', $deeType)->with('user_documents')->get();
                    if(!empty($decDcoumentsTypes)){
                        $this->userService->destroyCdcDocuments($decDcoumentsTypes);
                    }
                }
            }
        }
        if(isset($userData['wk_cop']) && $userData['wk_cop'] == ''){
            $wkCopDocuments = UserDocumentsType::whereUserId($userId)->whereIn('type',['wk_cop'])->with('user_documents')->get();
            if(!empty($wkCopDocuments)){
                $this->userService->destroyCdcDocuments($wkCopDocuments);
            }
            $this->userService->destroyCdcDocuments($wkCopDocuments);
            UserDocumentsType::whereUserId($userId)->whereIn('type',['wk_cop'])->delete();
            $userWkCop=UserWkfrDetail::whereUserId($userId);
            $userWkCop->update([
                'watch_keeping' => "",
                'type' => "",
                'wkfr_number' => "",
                'issue_date' => null
            ]);
        }
        
        if(isset($userData['current_rank_id']) && !empty($userData['current_rank_id']) && (in_array('DCE-Optional', \CommonHelper::rank_required_fields()[$userData['current_rank_id']]) || in_array('DCE', \CommonHelper::rank_required_fields()[$userData['current_rank_id']])) && ($dceRadio == 1)) {
            $checkData = UserDangerousCargoEndorsementDetail::whereUserId($userId)->first();
            if($checkData != null){
                $dceStatus = json_decode($checkData->status);
                $oldTypes = $dceStatus->type;
                $newTypes = $userData['dce'];
                $differentTypes = array_diff($oldTypes, $newTypes);
                $this->destroyUserDocuments($differentTypes, $userId);
            }
            $dangerousCargoEndorsement = $this->userDangerousCargoEndorsementDetail->store($userData);
        }

        $userData = User::whereId($userId)->first();
        if($result['status'] == 'success'){
            return response()->json(['user_data' => $userData,'status' => 'success' , 'existing_user' => $result['user']['is_mob_verified'] , 'message' => 'Your document details has been store successfully.','redirect_url' => route('home')],200);
        }else{
            return response()->json(['status' => 'failed' , 'message' => 'Error while adding data.'],400);
        }
    }

    public static function destroyUserDocumentsnew($types, $userId) {
        $userDocuments = UserDocumentsType::whereUserId($userId)->whereIn('type', $types)->with('user_documents')->get();
       
        if (count($userDocuments) > 0) {
            $totalSize = 0;
            foreach ($userDocuments as $key => $value) {
               /* if (count($value->user_documents) > 0) {
                    foreach (File::allFiles(public_path('uploads/user_documents/' . $userId . '/' . $value->type . '/' . $value->type_id)) as $file) {
                        $totalSize += $file->getSize();
                    }
                }
                $folderPath = public_path('uploads/user_documents/' . $userId . '/' . $value->type . '/' . $value->type_id);
                if (File::exists($folderPath)) {
                    File::deleteDirectory($folderPath);
                }*/

                $userDocuments = ($value->user_documents)->pluck('id');
                if (count($userDocuments) > 0) {
                    foreach($value->user_documents as $key => $documentId) {
                        $img_id = $documentId['id'];
                        $img_name = $documentId['document_path'];
                        $user_id = $userId;
                        $type = $value->type;
                        $type_id = $value->type_id;
                        $documentService = new DocumentService();
                        $documentService->deleteDocument($img_id, $img_name, $user_id, $type, $type_id);
                    }
                }
                UserDocumentsType::whereId($value->id)->delete();
            }

            /*$oldUsedKb = Auth::user()->used_kb;
            User::whereId($userId)->update([
                'used_kb' => (int)($oldUsedKb - ((int)($totalSize / 1024)))
            ]);*/
        }

        return true;
    }
    public static function destroyUserDocuments($types, $userId) {
        $userDocuments = UserDocumentsType::whereUserId($userId)->whereIn('type_id', $types)->with('user_documents')->get();

        if (count($userDocuments) > 0) {
            $totalSize = 0;
            foreach ($userDocuments as $key => $value) {
                if (count($value->user_documents) > 0) {
                    foreach (File::allFiles(public_path('uploads/user_documents/' . $userId . '/' . $value->type . '/' . $value->type_id)) as $file) {
                        $totalSize += $file->getSize();
                    }
                }
                $folderPath = public_path('uploads/user_documents/' . $userId . '/' . $value->type . '/' . $value->type_id);
                if (File::exists($folderPath)) {
                    File::deleteDirectory($folderPath);
                }

                $userDocuments = ($value->user_documents)->pluck('id');
                if (count($userDocuments) > 0) {
                    foreach($userDocuments as $key => $documentId) {
                        UserDocuments::whereId($documentId)->delete();
                    }
                }
                UserDocumentsType::whereId($value->id)->delete();
            }

            $oldUsedKb = Auth::user()->used_kb;
            User::whereId($userId)->update([
                'used_kb' => (int)($oldUsedKb - ((int)($totalSize / 1024)))
            ]);
        }

        return true;
    }
    public function verifyMobile(Request $request){

        $data = $request->toArray();
        $data['user_id'] = Auth::user()->id;
        $data['mobile'] = Auth::user()->mobile;

        $result = $this->userService->verifyMobile($data);

        if($result['status'] == 'success'){
            return response()->json(['status' => 'Success' , 'message' => 'Your mobile number has been verified.'],200);
            //return redirect()->route('home')->with(['status' => 'mobile_verification_success' , 'message' => 'Your mobile has been verified successfully.']);
        }elseif($result['status'] == 'failed'){
            return response()->json(['status' => 'failed' , 'message' => 'Invalid otp provided.'],400);
        }else{
            return response()->json(['status' => 'error' , 'message' => 'Error while verifying mobile number. Please try again.'],400);
        }
    }

    public function resendEmail(){
        $data = Auth::user();
        $result = $this->userService->resendEmail($data);

        if($result['status'] == 'success'){
            return response()->json(['status' => 'Success' , 'message' => 'Email has been sent successfully.'],200);
        }else{
            return response()->json(['status' => 'failed' , 'message' => 'Error in Email sending.'],400);
        }
    }

    public function resendOtp(Request $request){
        $data['user_id'] = Auth::user()->id;

        $mobile = Auth::user()->mobile;
        $data['otp'] = rand(100000,999999);

        $result = $this->userService->resendOtp($data['user_id'],$data['otp'],$mobile);

        if($result['status'] == 'success'){
            return response()->json(['status' => 'Success' , 'message' => 'OTP has been send successfully.'],200);
        }else{
            return response()->json(['status' => 'failed' , 'message' => 'Error in OTP generation.'],400);
        }
    }

    public function logout(){
        Auth::logout();
        return redirect()->route('home');
    }

    public function verifyEmail($email){
        
        $data['email'] = $email;
        $result = $this->userService->verifyEmail($data['email']);

        if($result['status'] == 'another_user'){
            return redirect()->route($result['route_name'], ['email_verification' => 'another_user']);
        }
        elseif($result['status'] == 'invalid_user'){
            return redirect()->route($result['route_name']);
        }
        elseif($result['status'] == 'user_login'){
            return redirect()->route($result['login'], ['route_name' => $result['route_name'], 'param' => $result['param']]);
        }
        elseif($result['status'] == 'success'){
            return redirect()->route('home',['route_name' => $result['route_name'],'auto_action' => 'verified']);
        }
        else{
            return redirect()->route('home')->with(['status' => 'error' , 'message' => 'Error in email verification']);
        }
    }

    public function changeAvailability(Request $request){
        $user_id = Auth::User()->id;
        $userdata = $request->toArray();
        
        $data['applied_rank'] = $userdata['applied_rank'];
        $data['availability'] = date('Y-m-d',strtotime($userdata['date_avaibility']));
        $result = $this->userProfessionalDetailsService->updateByUserId($data,$user_id);
        
        if($result) {
            return response()->json(['message' => 'Date of availability has been updated.','redirect_url' => route('home')], 200);
        } else {
            return response()->json(['message' => 'failed'], 400);
        }
    }

    public function uploadUserProfile(Request $request){
        $array['image-x'] = $request['image-x'];
        $array['image-y'] = $request['image-y'];
        $array['image-x2'] = $request['image-x2'];
        $array['image-y2'] = $request['image-y2'];
        $array['image-w'] = $request['image-w'];
        $array['image-h'] = $request['image-h'];
        $array['crop-w'] = $request['crop-w'];
        $array['crop-h'] = $request['crop-h'];
        $array['profile_pic'] = $request['profile_pic'];
        if(isset($request['role']))
            $array['role'] = $request['role'];
            $fileSize =filesize($request['profile_pic']);
            if($fileSize > 0){
                $fileSize = $fileSize/1024;
                $checkImageSize = CommonHelper::checkImageSize($fileSize);
                if($checkImageSize['status']){
                    $result = $this->userService->uploadProfile($array);
                    if($result['status'] == 'success' ) {
                        $user = User::find(Auth::user()->id);
                        $user->used_kb = $user->used_kb + $fileSize;
                        $user->Save();
                        return response()->json($result, 200);
                    } else {
                        return response()->json($result, 400);
                    }
                }else{  
                    Session::flash('file-size-error','Your storage currently full you have no more space.');
                    $result['status'] = false;
                    return response()->json($result,201);
                }
            }else{
                $result['status'] = false;
                return response()->json($result);
            }
            
    }
    
    public function deleteUserProfilePic(){
        if (Auth::check()) {
            $users = Auth::user();

            if(!empty($users->profile_pic)){
                $existProfilePic = 'public/images/uploads/seafarer_profile_pic/'.$users->id.'/'.$users->profile_pic;
                $headers = get_headers(env('AWS_URL') . $existProfilePic);
                // if(File::exists($existProfilePic)){
                if($headers[0] == 'HTTP/1.1 200 OK'){
                    $fileSize = str_replace('Content-Length: ', '', $headers[9]) / 1024;
                    $fileSize = $fileSize / 1024;
                    $s3 = new S3(env('AWS_ACCESS_KEY_ID'), env('AWS_SECRET_ACCESS_KEY'), false, 's3.amazonaws.com',env('AWS_DEFAULT_REGION'));
                    if ($s3->deleteObject(env('AWS_BUCKET'), $existProfilePic)) {
        
                    }
                    $user = User::find($users->id);
                    $user->profile_pic = null;
                    $user->used_kb = abs($users->used_kb - $fileSize);
                    $user->save();
                }
            }

            return response()->json(['status' => 'success' , 'message' => 'Profile pic deleted successfully'],200);
        }else{
            return response()->json(['status' => 'failed' , 'message' => 'Error while deleting data.'],400);
        }
    }

    public function displayProfile(){
        if (Auth::check()) {
            $user_id = Auth::user()->id;
            $options['with'] = ['personal_detail','professional_detail','passport_detail','seaman_book_detail','coc_detail','gmdss_detail','wkfr_detail'];

            $result = $this->userService->getDataByUserId($user_id, $options)->toArray();

            return view('site.registration.seafarer_profile_registration',['data' => $result]);
        }else{
            //route to login page
        }
    }

    public function storeServiceDetails(Request $request){
        if(Auth::check()){
            $data = $request->toArray();
            $user = Auth::user()->toArray();
            $user_id = $user['id'];
            
            $result = $this->userService->storeServiceDetails($data,$user_id);

            if($result['status'] == 'success'){
                return response()->json(['status' => 'success' , 'message' => 'Your service details has been added successfully.'],200);
            }else{
                return response()->json(['status' => 'failed' , 'message' => 'Error while adding data.'],400);
            }
        }
        else{
            return response()->json(['status' => 'failed' , 'message' => 'Error while adding data.'],400);
        }
    }
    
    public function saveShareContact(Request $request){
        if(Auth::check()){
            $data = $request->toArray();
            $user = Auth::user()->toArray();
            $user_id = $user['id'];
            $data['user_id'] = $user_id;
            $result = $this->userService->saveShareContact($data);
            
            if($result['status'] == 'exist'){
                return response()->json(['status' => 'failed' , 'message' => 'Email already exist.'],400);
            }
            if($result['status'] == 'success'){
                return response()->json(['status' => 'success' , 'message' => 'Your contact has been added successfully.'],200);
            }else{
                return response()->json(['status' => 'failed' , 'message' => 'Error while adding data.'],400);
            }
        }
        else{
            return response()->json(['status' => 'failed' , 'message' => 'Error while adding data.'],400);
        }
    }
    
    public function deleteShareContact(Request $request){
        if(Auth::check()){
            $data = array();
            $data['is_delete'] = 1;
            $shareContactId = $request['share_contact_id'];
            $user = Auth::user()->toArray();
            $user_id = $user['id'];
            $result = $this->userService->deleteShareContact($data, $shareContactId, $user_id);
            if($result['status'] == 'success'){
                return response()->json(['status' => 'success' , 'message' => 'Contact deleted successfully'],200);
            }else{
                return response()->json(['status' => 'failed' , 'message' => 'Error while deleting data.'],400);
            }
        }
        else{
            return response()->json(['status' => 'failed' , 'message' => 'Error while deleting data.'],400);
        }
    }
    
    public function getShareContact(){
        $user = Auth::user()->toArray();
        $user_id = $user['id'];
        $shareContactData = $this->userService->getShareContact($user_id);
        return Datatables::of($shareContactData)
                        ->editColumn('group', function($shareContact) {
                            if($shareContact->group == 1){
                                return "Person";
                            }else{
                                return "Company";
                            }
                        })
                        ->editColumn('action', function($shareContact){
                            $button = '';
                            $button .= '<button class="btn btn-primary edit-share-contact" data-id="'.$shareContact->id.'"><i class="fa fa-pencil"></i></button> ';
                            $button .= '<button class="btn btn-danger delete-share-contact" data-id="'.$shareContact->id.'"><i class="fa fa-trash"></i></button>';
                            return $button;
                        })
                        ->make(true);
    }
    
    public function getShareHistoryResume(){
        $user = Auth::user()->toArray();
        $user_id = $user['id'];
        $shareHistoryData = $this->userService->getShareHistory($user_id, 1);
        return Datatables::of($shareHistoryData)
                        ->addColumn('shared_to', function($row) {
                            if(!empty($row->share_contact)){
                                $displayName = "";
                                if($row->share_contact->group == 1) {
                                    $displayName = $row->share_contact->p_name;
                                } else {
                                    $displayName = $row->share_contact->name . ($row->share_contact->p_name ? "<br><small ckass=''>(" . $row->share_contact->p_name . ")</small>" : "");
                                }
                                return $displayName;
                            }else{
                                return "";
                            }
                        })
                        ->editColumn('created_at', function($row) {
                            return date("d-m-Y H:i:s", strtotime($row->created_at));
                        })
                        ->editColumn('history_log', function($rows){
                            $history_log = $rows['history_log'];
                            for($counter = 0; $counter < count($rows['history_log']); $counter++) {
                                $created_at = $history_log[$counter]['created_at'];
                                $history_log[$counter]['created'] = date("d-m-Y H:i:s", strtotime($created_at));
                                if($history_log[$counter]['description'] == 'Your Resume has been approved, kindly contact me' || $history_log[$counter]['description'] == "Thank you, Your Resume has been received") {
                                    $history_log[$counter]['description'] = "QR : " . $history_log[$counter]['description'];
                                }
                            }
                            return json_decode($history_log, JSON_PRETTY_PRINT);
                        })
                        ->addColumn('group', function($row) {
                            if (!empty($row->share_contact)) {
                                if ($row->share_contact->group == 1) {
                                    return "Person";
                                } else {
                                    return "Company";
                                }
                            } else {
                                return "";
                            }
                        })
                        ->addColumn('validity', function($row) {
                            $data = "Active";
                            if ((strtotime($row['created_at']) + (3600 * 24 * 60)) < time()) {
                                $data = "Expired";
                            }
                            return $data;
                        })
                        ->editColumn('latest_qr', function($rows){
                            $history_log = $rows['history_log'];
                            return count($history_log) > 0 ? date("d-m-Y H:i:s", strtotime($history_log[0]->created_at)) : "";
                        })
                        ->editColumn('action', function($row){
                            $button = '';
                            if(!empty($row['history_log'][0])){
                                if($row['is_new'] == 1 || (count($row['history_log']) && $row['history_log'][0]['is_new'] == 1)){
                                    $button .= '<button class="btn btn-primary view-share-history notification" data-id="' . $row['id'] . '"><i class="fa fa-eye"></i><span class="badge">New</span></button> ';
                                }else{
                                    $button .= '<button class="btn btn-primary view-share-history notification" data-id="' . $row['id'] . '"><i class="fa fa-eye"></i></button> ';
                                }
                            }
                            $button .= '<button class="btn btn-danger delete_doc notification pull-right" data-id="' . $row['id'] . '"><i class="fa fa-trash"></i></button> ';
                            return $button;
                        })
                        ->make(true);
    }
    
    public function getShareHistoryDocument(){
        $user = Auth::user()->toArray();
        $user_id = $user['id'];
        $shareHistoryData = $this->userService->getShareHistory($user_id, 3);
        return Datatables::of($shareHistoryData)
                        ->addColumn('shared_to', function($row) {
                            if(!empty($row->share_contact)){
                                $displayName = "";
                                if($row->share_contact->group == 1) {
                                    $displayName = $row->share_contact->p_name;
                                } else {
                                    $displayName = $row->share_contact->name . ($row->share_contact->p_name ? "<br><small ckass=''>(" . $row->share_contact->p_name . ")</small>" : "");
                                }
                                return $displayName;
                            }else{
                                return "";
                            }
                        })
                        ->addColumn('group', function($row) {
                            if (!empty($row->share_contact)) {
                                if ($row->share_contact->group == 1) {
                                    return "Person";
                                } else {
                                    return "Company";
                                }
                            } else {
                                return "";
                            }
                        })
                        ->addColumn('validity', function($row) {
                            $data = "Active";
                            if (!$row['document'] || ($row['document'] && ($row['document']->valid_time + (3600 * 48)) < time())) {
                                $data = "Expired";
                            }
                            return $data;
                        })
                        ->editColumn('history_log', function($rows){
                            $history_log = $rows['history_log'];
                            for($counter = 0; $counter < count($rows['history_log']); $counter++) {
                                $created_at = $history_log[$counter]['created_at'];
                                $history_log[$counter]['created'] = date("d-m-Y H:i:s", strtotime($created_at));
                            }
                            return json_decode($history_log, JSON_PRETTY_PRINT);
                        })
                        ->editColumn('latest_qr', function($rows){
                            $history_log = $rows['history_log'];
                            return count($history_log) > 0 ? date("d-m-Y H:i:s", strtotime($history_log[0]->created_at)) : "";
                        })
                        ->editColumn('created_at', function($row){
                            return date('d-m-Y H:i:s', strtotime($row->created_at));
                        })
                        ->editColumn('action', function($row){
                            $button = '';
                                if($row['is_new'] == 1 || (count($row['history_log']) && $row['history_log'][0]['is_new'] == 1)){
                                    $button .= '<button class="btn btn-primary view-share-history notification" data-id="' . $row['id'] . '"><i class="fa fa-eye"></i><span class="badge">New</span></button> ';
                                }else{
                                    $button .= '<button class="btn btn-primary view-share-history notification" data-id="' . $row['id'] . '"><i class="fa fa-eye"></i></button> ';
                                }
                                $button .= '<button class="btn btn-danger delete_doc notification pull-right" data-id="' . $row['id'] . '"><i class="fa fa-trash"></i></button> ';
                            return $button;
                        })
                        ->editColumn('document', function($row){
                            $data = '';
                                if(!empty($row['document'])){
                                    $data = json_decode($row['document']->data_display, JSON_PRETTY_PRINT);
                                    if($data && isset($data['child'])) {
                                        foreach($data['child'] as $key => $values){
                                            if($key == 'sea_service_article'){
                                                foreach($values as $key_child => $value) {
                                                    $string = str_replace('article', '', $value);
                                                    $fromDate = substr($string, 0, 10);
                                                    $toDate = (strlen($string) >= 20) ? date('d-m-Y', strtotime(substr($string, 10))) : "On Board";
                                                    $data['child'][$key][$key_child] = date('d-m-Y', strtotime($fromDate)) . ' to ' . $toDate;
                                                }
                                            } else if($key == 'sea_service_contract'){
                                                foreach($values as $key_child => $value) {
                                                    $string = str_replace('contract', '', $value);
                                                    $fromDate = substr($string, 0, 10);
                                                    $toDate = (strlen($string) >= 20) ? date('d-m-Y', strtotime(substr($string, 10))) : "On Board";
                                                    $data['child'][$key][$key_child] = date('d-m-Y', strtotime($fromDate)) . ' to ' . $toDate;
                                                }
                                            } else if($key == 'sea_service'){
                                                foreach($values as $key_child => $value) {
                                                    $string = $value;
                                                    $fromDate = substr($string, 0, 10);
                                                    $toDate = (strlen($string) >= 20) ? date('d-m-Y', strtotime(substr($string, 10))) : "On Board";
                                                    $data['child'][$key][$key_child] = date('d-m-Y', strtotime($fromDate)) . ' to ' . $toDate;
                                                }
                                            } else if($key == 'courses'){
                                                $courses = Auth::user()->course_detail->toArray();
                                                foreach($values as $key_child => $value) {
                                                    foreach($courses as $key_course => $course) {
                                                        if($value == $course['course_id']){
                                                            $arr = CommonHelper::course_name_by_course_type($course['course_type']);
                                                            foreach ($arr as $val) {
                                                                if ($course['course_id'] == $val->id) {
                                                                    $data['child'][$key][$key_child] = str_replace(")", "", str_replace("(", "", substr($val->course_name, strpos($val->course_name,"("))));
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            } else if($key == 'coe'){
                                                $coes = Auth::user()->coe_detail->toArray();
                                                foreach($values as $key_child => $value) {
                                                    foreach($coes as $key_coe => $coe) {
                                                        if($value == $coe['coe_number']. $coe['coe_grade']) {
                                                            $data['child'][$key][$key_child] = (isset($coe['coe_grade']) ? $coe['coe_grade'] : '') . ' ' . ((isset($coe['coe']) && $coe['coe'] != '') ? CommonHelper::countries()[$coe['coe']] : '');
                                                        }
                                                    }
                                                }
                                            } else if($key == 'coc'){
                                                $cocs = Auth::user()->coc_detail->toArray();
                                                foreach($values as $key_child => $value) {
                                                    foreach($cocs as $key_coc => $coc) {
                                                        if($value == $coc['coc_number']) {
                                                            $data['child'][$key][$key_child] = $coc['coc_grade'] . ' ' . CommonHelper::countries()[$coc['coc']];
                                                        }
                                                    }
                                                }
                                            } else if($key == 'dce'){
                                                foreach($values as $key_child => $value) {
                                                    if ($value == 'all') {
                                                        $data['child'][$key][$key_child] = 'All : Oil & Chemical & Liquified Gas';
                                                    } else if ($value == 'lequefied_gas') {
                                                        $data['child'][$key][$key_child] = 'liquified_gas';
                                                    } else {
                                                        $data['child'][$key][$key_child] = $value;
                                                    }
                                                }
                                            } else if($key == 'cdc'){
                                                $cdcs = Auth::user()->seaman_book_detail;
                                                foreach($values as $key_child => $value) {
                                                    foreach($cdcs as $cdc) {
                                                        if($value === $cdc['cdc_issue_date'].$cdc['cdc_expiry_date']) {
                                                            $data['child'][$key][$key_child] = CommonHelper::countries()[$cdc['cdc']];
                                                            break;
                                                        } else {
                                                            $data['child'][$key][$key_child] = 'cdc';
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            return $data;
                        })
                        ->make(true);
    }

    public function requestNewLink(Request $request) {
        if(Auth::check()){
            $user = Auth::user()->toArray();
            $_only_bank = false;

            $history_log = HistoryLog::findOrFail($request->history_id);
            $history_log->description = str_replace("Pending", ucfirst($request->status) . 'ed', $history_log->description);
            $history_log->save();
            if($request->status =='accept') {

                $shareHis = ShareHistory::findOrFail($request->share_history_id);
                $sharedDocsToOthers = SharedDocsToOthers::where("share_history_id", $request->share_history_id)->first();

                $store = new SharedDocsToOthers();
                $token = str_random(25);
                $store->user_id = $user['id'];
                $store->token = $token;
                $store->valid_time = time();
                $store->data = $sharedDocsToOthers->data;
                $store->receiver_email = $sharedDocsToOthers->receiver_email;

                $shareContactId = 0;
                $this->shareContactRepository = new ShareContactsRepository();
                $shareContactexist = $this->shareContactRepository->checkShareContactExist($sharedDocsToOthers->receiver_email, $user['id']);
                if (!empty($shareContactexist[0])) {
                    $shareContactId = $shareContactexist[0]['id'];
                } else {
                    $shareContactData = array();
                    $shareContactData['user_id'] = $user['id'];
                    $shareContactData['email'] = $sharedDocsToOthers->receiver_email;
                    $shareContactData['type'] = 1;
                    $this->shareContactRepository = new ShareContactsRepository();
                    $saveContact = $this->shareContactRepository->store($shareContactData);
                    $shareContactId = $saveContact->id;
                }

                $shareHistory = new ShareHistory();
                $shareHistory->roll_type = 3;
                $shareHistory->user_id = $user['id'];
                $shareHistory->is_allow_qr = 0;
                $shareHistory->contact_id = $shareContactId;
                $shareHistory->email = $sharedDocsToOthers->receiver_email;
                $shareHistory->save();
                $shareHistoryId = $shareHistory->id;

                $_bank_details = [];
                $data = json_decode($store->data);

                $parent = !empty($data->parent) ? $data->parent : [];
                $child = !empty($data->child) ? $data->child : null;
                $sharedData['parent'] = (array) $parent;

                if (isset($data_arr['parent'][0]) && ($data_arr['parent'][0] == 'bank_details')) {
                    $_only_bank = true;
                }

                if (in_array('bank_details', $sharedData)) {
                    $__user_details = UserPersonalDetail::where('user_id', $user['id'])->first();
                    $_bank_details['bank_name'] = $__user_details['bank_name'];
                    $_bank_details['account_holder_name'] = $__user_details['account_holder_name'];
                    $_bank_details['account_no'] = $__user_details['account_no'];
                    $_bank_details['ifsc_code'] = $__user_details['ifsc_code'];
                    $_bank_details['branch_address'] = $__user_details['branch_address'];
                    $_bank_details['aadhaar'] = $__user_details['aadhaar'];
                    $_bank_details['pancard'] = $__user_details['pancard'];
                }

                $_full_name = $user['first_name'] . ' ' . $user['last_name'];

                if ($child) {
                    foreach ($child as $key => $row) {
                        if ($key == 'courses') {
                            foreach ($child->courses as $value) {
                                $courses = \App\Courses::where('id', $value)->value('course_name');
                                $sharedData['parent'][] = 'Course' . ' - ' . str_replace(")", "", str_replace("(", "", substr($courses, strpos($courses, '('))));
                            }
                        } elseif ($key == 'sea_service') {
                            foreach ($row as $value) {
                                // dump(strlen($value));
                                if (strlen($value) == 10) {
                                    $seaData = $value . ' On Board';
                                } else {
                                    $seaData = substr_replace($value, " / ", 10, -strlen($value));
                                }
                                $sharedData['parent'][] = 'Service' . ' - ' . $seaData;
                            }
                        } elseif ($key == 'sea_service_article') {

                            foreach ($row as $value) {
                                $string = str_replace('article', '', $value);
                                $fromDate = substr($string, 0, 10);
                                $toDate = (strlen($string) >= 20) ? date('d-m-Y', strtotime(substr($string, 10))) : "On Board";

                                $sharedData['parent'][] = 'Article' . ' - ' . date('d-m-Y', strtotime($fromDate)) . ' to ' . $toDate;
                            }
                        } elseif ($key == 'sea_service_contract') {

                            foreach ($row as $value) {
                                $string = str_replace('contract', '', $value);
                                $fromDate = substr($string, 0, 10);
                                $toDate = (strlen($string) >= 20) ? date('d-m-Y', strtotime(substr($string, 10))) : "On Board";

                                $sharedData['parent'][] = 'Contract' . ' - ' . date('d-m-Y', strtotime($fromDate)) . ' to ' . $toDate;
                            }
                        } elseif ($key == 'coe') {
                            $coes = Auth::user()->coe_detail->toArray();
                            foreach($row as $key_child => $value) {
                                foreach($coes as $key_coe => $coe) {
                                    if($value == $coe['coe_number']. $coe['coe_grade']) {
                                        $sharedData['parent'][] = "Coe - " . ((isset($coe['coe']) && $coe['coe'] != '') ? CommonHelper::countries()[$coe['coe']] : '');
                                    }
                                }
                            }
                        } elseif ($key == 'coc') {
                            $cocs = Auth::user()->coc_detail->toArray();
                            foreach($row as $key_child => $value) {
                                foreach($cocs as $key_coc => $coc) {
                                    if($value == $coc['coc_number']) {
                                        $sharedData['parent'][] = "Coc - " . CommonHelper::countries()[$coc['coc']];
                                    }
                                }
                            }
                        } elseif ($key == 'cdc') {
                            $cdcs = Auth::user()->seaman_book_detail;
                            foreach($row as $key_child => $value) {
                                foreach($cdcs as $cdc) {
                                    if($value == $cdc['cdc_issue_date'].$cdc['cdc_expiry_date']) {
                                        $sharedData['parent'][] = "Cdc - ". CommonHelper::countries()[$cdc['cdc']];
                                        break;
                                    } else {
                                        $sharedData['parent'][] = 'Cdc';
                                    }
                                }
                            }
                        } elseif ($key == 'dce') {
                            foreach ($row as $value) {
                                if ($value == 'all') {
                                    $data = 'Oil + Chemical + Liquefied Gas';
                                } else {
                                    $data = "Dce - " . ucwords(strtolower(str_replace('_', ' ', str_replace("lequefied_gas", "liquified_gas", $value))));
                                }
                                $sharedData['parent'][] = $data;
                            }
                        } else {
                            $sharedData['parent'][] = $key;
                        }
                    }
                }

                $sharedData = array_filter($sharedData);
                $sharedData = !empty($sharedData['parent']) ? $sharedData['parent'] : [];
                $userCocDetail = \App\UserCocDetail::whereUserId($user['id'])->first();
                $userWkfrDetail = \App\UserWkfrDetail::whereUserId($user['id'])->first();
                $mail_data = [
                    'email' => $sharedDocsToOthers->receiver_email,
                    'subject' => '' . $_full_name . ' has shared Documents with you.',
                    'token' => $store->token,
                    'valid_time' => $store->valid_time,
                    'name' => $_full_name,
                    'bank_details' => $_bank_details,
                    'only_bank' => $_only_bank,
                    'sharedData' => $sharedData,
                    'userCocDetail' => $userCocDetail,
                    'userWkfrDetail' => $userWkfrDetail,
                ];
                dispatch(new SendEmailShareDocs($mail_data));

                $store->share_history_id = $shareHistoryId;
                $store->save();

                // $historyLog = new HistoryLog();
                // $historyLog->is_new = "1";
                // $historyLog->description = "Requested Link: <a href='" . route('documents.sharedData',["token"=>$token]) . "' target='_blank'>Click Here</a>";
                // $historyLog->share_history_id = $request->share_history_id;
                // $historyLog->save();

            }
            return response()->json([
                "status" => "success",
                "message" => "New link successfully shared",
            ]);
        }
    }
    public function deleteSharedDocuments(Request $request) {
        if(Auth::check()) {
            $shareHistory = ShareHistory::findOrFail($request->id);
            $shareHistory->delete();
            return response()->json([
                'status' => "success",
                "message" => "Successfully deleted."
            ]);
        }
    }
    public function deleteSharedResume(Request $request) {
        if(Auth::check()) {
            $shareHistory = ShareHistory::findOrFail($request->id);
            $shareHistory->delete();
            return response()->json([
                'status' => "success",
                "message" => "Successfully deleted."
            ]);
        }
    }
    public function viewShareHistory(Request $request){
        if(Auth::check()){
            $data = $request->toArray();
            $user = Auth::user()->toArray();
            $user_id = $user['id'];
            $shareHistoryId = $data['share_histoyr_id'];
            if($shareHistoryId){
                $whereShareHistory = array();
                $whereShareHistory['user_id'] = $user_id;
                $whereShareHistory['id'] = $shareHistoryId;
                $result = ShareHistory::where($whereShareHistory)->update(['is_new'=>0]);
                if($result){
                    $whereHistoryLog = array();
                    $whereHistoryLog['share_history_id'] = $shareHistoryId;
                    HistoryLog::where($whereHistoryLog)->update(['is_new'=>0]);
                }
            }
            return response('Updated', 200);
        }
    }
    
    public function getShareContactList(){
        $user = Auth::user()->toArray();
        $user_id = $user['id'];
        $shareContactData = $this->userService->getShareContactList($user_id);
        return $shareContactData;
    }

    public function storeGeneralDetails(Request $request){
        if(Auth::check()){
            $data = $request->toArray();
            $user = Auth::user()->toArray();
            $user_id = $user['id'];
            
            $result = $this->userService->storeGeneralDetails($data,$user_id);

            if($result['status'] == 'success'){
                return response()->json(['status' => 'success' , 'message' => 'Your general details has been added successfully.'],200);
            }else{
                return response()->json(['status' => 'failed' , 'message' => 'Error while adding data.'],400);
            }
        }
        else{
            return response()->json(['status' => 'failed' , 'message' => 'Error while adding data.'],400);
        }
    }

    public function storeSpecificServiceDetails(Request $request){
        if(Auth::check()){
            $data = $request->toArray();
            $user = Auth::user()->toArray();
            $user_id = $user['id'];
            if(!empty($data['date_from'][0]) && !empty($data['date_to'][0])){
                $dateFrom = date('Y-m-d',strtotime($data['date_from'][0]));
                $dateTo = date('Y-m-d',strtotime($data['date_to'][0]));
                if($dateFrom > $dateTo){
                    return response()->json(['status' => 'failed' , 'message' => 'Sign off date must be greater than or equal to sign on date'],400);
                }
            }
            $result = $this->userService->storeSpecificServiceDetails($data,$user_id);

            if($result['status'] == 'sea_service_from_present'){
                return response()->json(['status' => 'failed' , 'message' => 'Sea service already added between given sign on date.'],400);
            } 

            if($result['status'] == 'sea_service_to_present'){
                return response()->json(['status' => 'failed' , 'message' => 'Sea service already added between given sign off date.'],400);
            }

            if($result['status'] == 'sea_service_to_sign_off'){
                return response()->json(['status' => 'failed' , 'message' => 'You are still on board in previous entry, change Sign off Date.'],400);
            }
            
            if($result['status'] == 'two_onboard_not_allow'){
                return response()->json(['status' => 'failed' , 'message' => 'You are still on board please exit sign of date'],400);
            }
            
            if($result['status'] == 'sign_on_and_sign_of_less_then_to_on_board'){
                return response()->json(['status' => 'failed' , 'message' => 'You are still on board, Please From date an To date must be less than from on board sign on date'],400);
            }
            
            if($result['status'] == 'sea_service_exists'){
                return response()->json(['status' => 'failed' , 'message' => 'From date must be greater than previous sea service sign off date'],400);
            }

            $services = $this->userService->getSeaServiceDetailsByUserId($user_id);
			
//			$i=count($services);
            $service_count = \DB::table('user_documents_type')->where('user_id', $user_id)->where('type', 'sea_service')->where('type_id', $data['date_from'][0].$data['date_to'][0])->count();
            if(!$service_count)
            {
                DB::table('user_documents_type')->insert(['user_id'=>$user_id,'type_id'=>$data['date_from'][0].$data['date_to'][0],'order'=>0,'type' => 'sea_service']);
            }
            if(isset($data['exiting_service_id']) && !empty($data['exiting_service_id'])){
                $sea_service = View::make("site.partials.seafarer_service_details")->with(['services' => $services]);
                $template = $sea_service->render();
                
                return response()->json(['status' => 'success' , 'message' => 'Your service details has been updated successfully.', 'template' => $template, 'update' => '1'],200);
            }else{
                $sea_service = View::make("site.partials.seafarer_service_details")->with(['services' => $services]);
            }

            $template = $sea_service->render();
            $engineType = EnginType::where('user_id',Auth::user()->id)->orWhereNull('user_id')->pluck('name','id')->toArray();
            if($result['status'] == 'success'){
                return response()->json(['status' => 'success' , 'message' => 'Your service details has been added successfully.', 'template' => $template,'engineType'=>$engineType],200);
            }else{
                return response()->json(['status' => 'failed' , 'message' => 'Error while adding data.'],400);
            }
        }
        else{
            return response()->json(['status' => 'failed' , 'message' => 'Error while adding data.'],400);
        }
    }

    public function deleteServiceDetails(Request $request){
        if(Auth::check()){
            $data = $request->toArray();
            $user = Auth::user()->toArray();
            $user_id = $user['id'];
            $serviceId = $data['service_id'];
            $user_document_type = UserDocumentsType::whereTypeId($data['from'].$data['to'])->whereType('sea_service')->whereUserId(Auth::id())->first();
            if(isset($user_document_type)){
                UserDocuments::whereUserDocumentId($user_document_type['id'])->delete();
                UserDocumentsType::whereTypeId($data['from'].$data['to'])->whereType('sea_service')->whereUserId(Auth::id())->delete();
            }

            $result = $this->userService->deleteServiceDetails($data['service_id'],$user_id);
            if($result['status'] == 'success'){
                $user_documents = $this->userService->getUserDocuments($user_id);
                if (isset($user_documents['sea_service_contract'][$serviceId . 'contract']['user_documents']) && !empty($user_documents['sea_service_contract'][$serviceId . 'contract']['user_documents'])) {
                    $sea_service_contract = $user_documents['sea_service_contract'][$serviceId . 'contract']['user_documents'];
                    foreach ($sea_service_contract as $seaServiceContract) {
                        $img_id = $seaServiceContract['id'];
                        $img_name = $seaServiceContract['document_path'];
                        $type = 'sea_service_contract';
                        $type_id = $serviceId.'contract';
                        $this->userService->deleteDocument($img_id, $img_name, $user_id, $type, $type_id);
                    }
                }
                if (isset($user_documents['sea_service_article'][$serviceId . 'article']['user_documents']) && !empty($user_documents['sea_service_article'][$serviceId . 'article']['user_documents'])) {
                    $sea_service_article = $user_documents['sea_service_article'][$serviceId . 'article']['user_documents'];
                    foreach ($sea_service_article as $seaServiceArticle) {
                        $img_id = $seaServiceArticle['id'];
                        $img_name = $seaServiceArticle['document_path'];
                        $type = 'sea_service_article';
                        $type_id = $serviceId.'article';
                        $this->userService->deleteDocument($img_id, $img_name, $user_id, $type, $type_id);
                    }
                }
                if (isset($user_documents['sea_service'][$serviceId]['user_documents']) && !empty($user_documents['sea_service'][$serviceId]['user_documents'])) {
                    $sea_service = $user_documents['sea_service'][$serviceId]['user_documents'];
                    foreach ($sea_service as $seaService) {
                        $img_id = $seaService['id'];
                        $img_name = $seaService['document_path'];
                        $type = 'sea_service';
                        $type_id = $serviceId;
                        $this->userService->deleteDocument($img_id, $img_name, $user_id, $type, $type_id);
                    }
                }
            }
            if($result['status'] == 'success'){
                return response()->json( $result,200);
            }else{
                return response()->json( $result,400);
            }
        }
        else{
            return response()->json(['status' => 'failed' , 'message' => 'Error while adding data.'],400);
        }
    }   

    public function getSeaServiceDetails(Request $request){
        if(Auth::check()){
            $data = $request->toArray();
            $user = Auth::user()->toArray();
            $user_id = $user['id'];
            
            $result = $this->userService->getSeaServiceDetails($data['service_id'],$user_id);

            if($result['status'] == 'success'){
                return response()->json( $result,200);
            }else{
                return response()->json( $result,400);
            }
        }
        else{
            return response()->json(['status' => 'failed' , 'message' => 'Error while adding data.'],400);
        }
    }   

    public function storeCourseDetails(Request $request){
        
        if(Auth::check()){
            $data = $request->toArray();
            $user = Auth::user()->toArray();
            $user_id = Auth::user()->id;
            
            $result = $this->userService->storeCourseDetails($data,$user_id);

            if($result['status'] == 'success'){
                return response()->json(['status' => 'success' , 'message' => 'Your Course details has been store successfully.','redirect_url' => route('user.profile'), 'update_email' => $user['is_email_verified'] , 'update_mobile' => $user['is_mob_verified']],200);
            }else{
                return response()->json(['status' => 'failed' , 'message' => 'Error while adding data.'],400);
            }
        }
        else{
            return response()->json(['status' => 'failed' , 'message' => 'Error while adding data.'],400);
        }
    }

    public function checkEmail(Request $request){
        $data = $request->toArray();

        //dd($data);
        $result = $this->userService->checkEmail($data);
        return $result;
    }

    public function checkMobile(Request $request){
        $data = $request->toArray();

        //dd($data);
        $result = $this->userService->checkMobile($data);
        return $result;
    }

    public function updateAvailability($id){

        if(Auth::check()){
            $logged_id = Auth::User()->id;

            if($logged_id == $id){
                $result = $this->userProfessionalDetailsService->getDetailsByUserId($id)->toArray();

                $availability = $result[0]['availability'];
                $current_rank = $result[0]['current_rank'];
                
                $current_route = Route::currentRouteName();

                if($current_route == 'user.update.availability'){
                    return view('site.user.seafarer_update_availability',['availability' => $availability,'current_rank' => $current_rank,'redirect_url' => route('home')]);
                }else{
                    return view('site.user.seafarer_update_availability',['availability' => $availability,'current_rank' => $current_rank]);
                }
            }else{
                return redirect()->route('home', ['update_availabilty' => 'another_user']);
            }
        }
        
    }

    public function checkSubscriptionAvailability(Request $request){
        if(Auth::check()){
            $user = Auth::User();
            $user_id = $user->id;
            $user_role = $user->registered_as;
            $result = $this->userService->checkSubscriptionAvailability($user_id,$user_role);
            if ($result['status'] == 'success') {
                return response()->json(['status' => 'success' , 'result' => $result],200);
            } else {
                return response()->json(['status' => 'error' , 'message' => $result['error']['message']],400);
            }
        }
    }

    public function getUserShippingCompanyWithId(Request $request){
        if(Auth::check()){
            $text = '';

            if(isset($request->user_id) && !empty($request->user_id) && !is_null($request->user_id) && $request->user_id != 'undefined'){
                $user_id = $request->user_id;
            }else{
                $user = Auth::User();
                $user_id = $user->id;    
            }

            $result = $this->userService->getUserShippingCompanyWithId($user_id,$request->term);

            return $result;
        }
    }
    
    public function getUserShippingManningWithId(Request $request){
        if(Auth::check()){
            $text = '';

            if(isset($request->user_id) && !empty($request->user_id) && !is_null($request->user_id) && $request->user_id != 'undefined'){
                $user_id = $request->user_id;
            }else{
                $user = Auth::User();
                $user_id = $user->id;    
            }

            $result = $this->userService->getUserShippingManningWithId($user_id,$request->term);

            return $result;
        }
    }

    public function getUserShipName(Request $request){
        if(Auth::check()){
            $text = '';
            $shipping_company = '';

            if(isset($request->user_id) && !empty($request->user_id) && !is_null($request->user_id) && $request->user_id != 'undefined'){
                $user_id = $request->user_id;
            }else{
                $user = Auth::User();
                $user_id = $user->id;    
            }

            $text = $request->term;
            $shipping_company = $request->shipping_company;

            $result = $this->userService->getUserShipName($user_id,$text,$shipping_company);

            return $result;
        }
    }

    public function getCourseIssueBy(Request $request){
        if(Auth::check()){
            $text = '';
            $shipping_company = '';

            if(isset($request->user_id) && !empty($request->user_id) && !is_null($request->user_id) && $request->user_id != 'undefined'){
                $user_id = $request->user_id;
            }else{
                $user = Auth::User();
                $user_id = $user->id;    
            }

            $text = $request->term;

            $result = $this->userService->getCourseIssueBy($user_id,$text);

            return $result;
        }
    }

    public function ShareFile($user_id = null)
    {
		
		
        //Auth::loginUsingId(19);

        $user = '';
        $role = '';
        if(isset($user_id)){
            $user = 'another_user';
        }
        if(Auth::check() && !isset($user_id)){
            $user_id = Auth::user()->id;

        }elseif (isset($user_id) && $user_id != '') {
            # code...
        }
        
        if(Auth::check()){
            $role = Auth::user()->registered_as;
        }

        if(isset($user_id)){

            $options['with'] = ['personal_detail','professional_detail','passport_detail','seaman_book_detail','coc_detail','gmdss_detail','wkfr_detail','personal_detail.state','personal_detail.city','personal_detail.pincode.pincodes_states.state','personal_detail.pincode.pincodes_cities.city','sea_service_detail','course_detail','cop_detail','coe_detail','document_permissions.requester.company_registration_detail','document_permissions.requester.institute_registration_detail','document_permissions.document_type.type'];
            $result = $this->userService->getDataByUserId($user_id, $options)->toArray();
            if(isset($result[0]['professional_detail']['current_rank_exp'])){
                $rank_exp = explode(".", $result[0]['professional_detail']['current_rank_exp']);
                $result[0]['professional_detail']['years'] = $rank_exp[0];
                $result[0]['professional_detail']['months'] = ltrim($rank_exp[1],0);
            }
            
            $courses = $this->userService->getAllCourses();

            $user_documents = $this->userService->getUserDocuments($user_id);
            $uploaded_documents = [];
            // dd($user_documents);

            if(isset($result[0]['professional_detail']['current_rank']) && !empty($result[0]['professional_detail']['current_rank'])){
                $required_documents = \CommonHelper::rank_required_fields()[$result[0]['professional_detail']['current_rank']];
            }
            
            //if(isset($required_documents) && !empty($required_documents)){

                //Passport visa block
                if(isset($result[0]['passport_detail']) && !empty($result[0]['passport_detail'])){
                    $uploaded_documents['passport'] = $result[0]['passport_detail'];
                    $uploaded_documents['passport']['title'] = 'Passport / Visa';

                    if(isset($user_documents['passport']) && !empty($user_documents['passport'])){
                        $uploaded_documents['passport']['imp_doc'] = $user_documents['passport'];
                    }
                }
                if(isset($result[0]['passport_detail']['us_visa']) && $result[0]['passport_detail']['us_visa'] == '1'){
                    $uploaded_documents['visa'] = $result[0]['passport_detail'];
                    if(isset($result[0]['passport_detail']) && !empty($result[0]['passport_detail'])){
                        $uploaded_documents['visa']['hide'] = 1;
                        $uploaded_documents['visa']['title'] = 'Passport / Visa';
                    }

                    if(isset($user_documents['visa']) && !empty($user_documents['visa'])){
                        $uploaded_documents['visa']['imp_doc'] = $user_documents['visa'];
                    }
                }
                
                //CDC
                if(isset($result[0]['seaman_book_detail']) && !empty($result[0]['seaman_book_detail'])){
                    $uploaded_documents['cdc'] = $result[0]['seaman_book_detail'];

                    if(isset($uploaded_documents['cdc']) && !empty($uploaded_documents['cdc'])){
                        foreach ($uploaded_documents['cdc'] as $key => $value) {
                            if(isset($user_documents['cdc'][$key+1]['user_documents']) && !empty($user_documents['cdc'][$key+1]['user_documents'])){
                                $uploaded_documents['cdc'][$key]['title'] = 'CDC';

                                if(isset($user_documents['cdc'][$key+1]) && !empty($user_documents['cdc'][$key+1])){
                                    $uploaded_documents['cdc'][$key]['imp_doc'] = $user_documents['cdc'][$key+1];
                                }
                            }
                        }
                    }
                }
                
                //Coc Coe Block
                //if(in_array('COC', $required_documents)){
                    $uploaded_documents['coc'] = $result[0]['coc_detail'];
                    
                    if(isset($uploaded_documents['coc']) && !empty($uploaded_documents['coc'])){
                        foreach ($uploaded_documents['coc'] as $key => $value) {
                            if(isset($user_documents['coc'][$key+1]['user_documents']) && !empty($user_documents['coc'][$key+1]['user_documents'])){
                                $uploaded_documents['coc'][$key]['title'] = 'COC / COE';

                                if(isset($user_documents['coc'][$key+1]) && !empty($user_documents['coc'][$key+1]))
                                    $uploaded_documents['coc'][$key]['imp_doc'] = $user_documents['coc'][$key+1];
                            }
                        }
                    }
                //}

                //if(in_array('COE', $required_documents) || in_array('COE-Optional', $required_documents)){
                    $uploaded_documents['coe'] = $result[0]['coe_detail'];

                    if(in_array('COC', $required_documents)){
                        foreach ($uploaded_documents['coe'] as $key => $value) {
                            if(isset($user_documents['coe'][$key+1]['user_documents']) && !empty($user_documents['coe'][$key+1]['user_documents'])){
                                $uploaded_documents['coe'][$key]['hide'] = 1;
                                $uploaded_documents['coe'][$key]['title'] = 'COC / COE';

                                if(isset($user_documents['coe'][$key+1]) && !empty($user_documents['coe'][$key+1]))
                                    $uploaded_documents['coe'][$key]['imp_doc'] = $user_documents['coe'][$key+1];
                            }
                        }
                    }
                //}

                //GMDSS watch keeping
                //if(in_array('GMDSS', $required_documents) || in_array('GMDSS-Optional', $required_documents)){
                    $uploaded_documents['gmdss'] = $result[0]['gmdss_detail'];
                    $uploaded_documents['gmdss']['title'] = 'GMDSS / WK';

                    if(isset($user_documents['gmdss']) && !empty($user_documents['gmdss'])){
                        $uploaded_documents['gmdss']['imp_doc'] = $user_documents['gmdss'];
                    }
                //}

                //if(in_array('WATCH_KEEPING-Optional', $required_documents)){
                    $uploaded_documents['watch keeping'] = $result[0]['wkfr_detail'];
                    $uploaded_documents['watch keeping']['title'] = 'GMDSS / WK';

                    if(in_array('GMDSS', $required_documents) || in_array('GMDSS-Optional', $required_documents)){
                        $uploaded_documents['watch keeping']['hide'] = 1;

                        if(isset($user_documents['watch keeping']) && !empty($user_documents['watch keeping'])){
                            $uploaded_documents['watch keeping']['imp_doc'] = $user_documents['watch keeping'];
                        }

                    }
               // }

                //courses
                if(isset($result[0]['course_detail']) && !empty($result[0]['course_detail'])){
                    $uploaded_documents['courses'] = $result[0]['course_detail'];

                    foreach ($uploaded_documents['courses'] as $key => $value) {
                        if(isset($user_documents['courses'][$key+1]['user_documents']) && !empty($user_documents['courses'][$key+1]['user_documents'])){
                            $uploaded_documents['courses'][$key]['title'] = 'PreSea / PostSea Course Certificates';

                            if(isset($user_documents['courses'][$key+1]) && !empty($user_documents['courses'][$key+1]))
                                $uploaded_documents['courses'][$key]['imp_doc'] = $user_documents['courses'][$key+1];
                        }
                    }
                }
                
                //sea service
                if(isset($result[0]['sea_service_detail']) && !empty($result[0]['sea_service_detail'])){
                    $uploaded_documents['sea service'] = $result[0]['sea_service_detail'];

                    foreach ($result[0]['sea_service_detail'] as $key => $value) {
                        if(isset($user_documents['sea service'][$key+1]['user_documents']) && !empty($user_documents['sea service'][$key+1]['user_documents'])){
                            $uploaded_documents['sea service'][$key]['title'] = 'Sea Service Letters';

                            if(isset($user_documents['sea service'][$key+1]) && !empty($user_documents['sea service'][$key+1])){
                                $uploaded_documents['sea service'][$key]['imp_doc'] = $user_documents['sea service'][$key+1];
                            }
                        }
                    }
                }

                //yf vaccination
                if(isset($result[0]['wkfr_detail']) && $result[0]['wkfr_detail']['yellow_fever'] == '1'){
                    $uploaded_documents['yellow fever'] = $result[0]['wkfr_detail'];
                    if(isset($user_documents['yellow fever'][0]['user_documents']) && !empty($user_documents['yellow fever'][0]['user_documents'])){
                        $uploaded_documents['yellow fever']['title'] = 'Medicals / YF Vaccination';

                        if(isset($user_documents['yellow fever']) && !empty($user_documents['yellow fever'])){
                            $uploaded_documents['yellow fever']['imp_doc'] = $user_documents['yellow fever'];
                        }
                    }
                }
            //}

            $count = 0;
            if(isset($result[0]['course_detail'])){
                foreach ($result[0]['course_detail'] as $key => $value) {
                    if($value['course_type'] == 'Value Added'){
                        $result[0]['value_added_course_detail'][$count] = $value;
                        $count++;
                        unset($result[0]['course_detail'][$key]);
                    }
                }
            }
            // dd($courses);

            if(count($result) > 0 && ($result[0]['registered_as'] == null || $result[0]['registered_as'] != 'advertiser')){
				
                return view('site.user.seafarer_share_doc',['data' => $result,'user' => $user, 'all_courses' => $courses,'role' => $role,'user_documents' => $user_documents,'user_id' => $user_id, 'documents' => $uploaded_documents]);
            }else{
                return view('errors.404');
            }

            return view('site.user.profile');
        }
    }



    public function DownloadCV(){

        if(Auth::check()){
                        
            $user_id= Auth::user()->id;
                
            $options['with'] = ['personal_detail','professional_detail','passport_detail','seaman_book_detail','coc_detail','gmdss_detail','wkfr_detail','personal_detail.state','personal_detail.city','personal_detail.pincode.pincodes_states.state','personal_detail.pincode.pincodes_cities.city','sea_service_detail','course_detail','cop_detail'];

            $result = $this->userService->getDataByUserId($user_id,$options)->toArray();

            $courses = $this->userService->getAllCourses();

            $count = 0;
            foreach ($result[0]['course_detail'] as $key => $value) {
                if($value['course_type'] == 'Value Added'){
                    $result[0]['value_added_course_detail'][$count] = $value;
                    $count++;
                    unset($result[0]['course_detail'][$key]);
                }
            }

            // return view('site.user.seafarer_download_cv',['data' => $result[0],'all_courses' => $courses]);
            $pdf = PDF::loadHTML((string)view('site.user.seafarer_download_cv',['data' => $result[0],'all_courses' => $courses]));
            $directory = public_path().'/'.env('CANDIDATE_CV_PATH');
            
            if (!is_dir($directory)) {
                mkdir($directory, 0777);
            }
            $file_path = public_path().DIRECTORY_SEPARATOR.'uploads'.DIRECTORY_SEPARATOR.'seafarer_download_cv'.DIRECTORY_SEPARATOR.$user_id."-".$result[0]['first_name'].'.pdf';
            // $file_path = str_replace('/', '\\', $file_path);
            
            $pdf->save($file_path);

            $file_path = url('/').'/uploads/seafarer_download_cv/'.$user_id."-".$result[0]['first_name'].'.pdf';
            
            return response()->json(['file_path'=> $file_path] , 200);
                    
        }

    }


    public function DownloadFile(){

        if(Auth::check()){

            $user_id= Auth::user()->id;

            $result = [];
            $result['path'] = '';

            $pdf = PDF::loadHTML((string)view('site.user.download-file',['data' => $result['path']]));
            $directory = public_path().'/'.env('CANDIDATE_CV_PATH');
            if (!is_dir($directory)) {
                mkdir($directory, 0777);
            }
            $file_path = public_path().DIRECTORY_SEPARATOR.'uploads'.DIRECTORY_SEPARATOR.'seafarer_download_cv'.DIRECTORY_SEPARATOR.$user_id."-".$result[0]['first_name'].'.pdf';
            // $file_path = str_replace('/', '\\', $file_path);

            $pdf->save($file_path);

            $file_path = url('/').'/uploads/seafarer_download_cv/'.$user_id."-".$result[0]['first_name'].'.pdf';

            return response()->json(['file_path'=> $file_path] , 200);

        }

    }


    public function storeSeafarerCourseDetails(Request $request){

        if(Auth::check()){
                        
            $user_id = Auth::user()->id;

            $data = $request->toArray();
			
            if($data['existing_course_id'] == ''){
                $countdata = DB::table('user_courses_certificates')->where('user_id',$user_id)->where('course_id',$data['certificate_name'])->count();
                if($countdata > 0){
                    return response()->json(['status' => 'error' , 'message' => 'Certificate Name is already exists.'],400);
                }
            }

//            if($data['existing_course_id'] != ''){
//                $countdata = DB::table('user_courses_certificates')->where('user_id',$user_id)->where('course_id',$data['certificate_name'])->where('id','!=',$data['existing_course_id'])->count();
//                if($countdata > 0){
//                    return response()->json(['status' => 'error' , 'message' => 'Certificate Name is already exists.'],400);
//                }
//            }
            $result = $this->userService->storeSingleCourseDetail($data,$user_id);

            //return response()->json(['status' => 'error' , 'message' => 'already exist.'],400);

            //$course_type = $data['seafarer_course_type'] == 'normal' ? 'Normal' : 'Value Added';
            $course_details = $this->userService->getCourseDetailsByUserId($user_id);
			$i=count($course_details);
//            $course_count = \DB::table('user_documents_type')->where('user_id', $user_id)->where('type', 'courses')->count();
            if($data['existing_course_id'] == ''){
                $course_count = \DB::table('user_documents_type')->where('user_id', $user_id)->where('type', 'courses')->where('type_id', $data['certificate_name'])->count();
                if($course_count == 0){
                    DB::table('user_documents_type')->insert(           ['user_id'=>$user_id,'type_id'=>$data['certificate_name'],'order'=>0,'type' => 'courses', 'status'=>1]); //add status for default public
                }
            }
//            if(!$course_count)
//            {
//			}
			
            $courses = View::make("site.partials.seafarer_course_details")->with(['course_details' => $course_details]);
            $template = $courses->render();

            if ($result['status'] == 'success') {

                if($data['existing_course_id']!='' && $data['existing_course_id']!=''){
                    return response()->json(['status' => 'success' , 'message' => 'Course has been updated successfully.', 'courses' => $template],200);
                }
                return response()->json(['status' => 'success' , 'message' => 'Course has been added successfully.', 'courses' => $template],200);
            } else {
                return response()->json(['status' => 'error' , 'message' => $result['error']['message']],400);
            }
        }
    }

    public function deleteSeafarerCourseDetails(Request $request){

        if(Auth::check()){
            $data = $request->toArray();
            $user = Auth::user()->toArray();
            $course_doc_id=$data['course_id'] ?? 0;
            $course_id=$data['id'];
            $user_id = $user['id'];
            $user_document_type = UserDocumentsType::whereTypeId($course_doc_id)->whereType('courses')->whereUserId(Auth::id())->first();
            if(isset($user_document_type)){
                $userDocuments = UserDocuments::whereUserDocumentId($user_document_type['id'])->get();
                if (isset($userDocuments) && count($userDocuments) > 0) {
                    foreach($userDocuments as $key => $documentId) {
                        $img_id = $documentId['id'];
                        $img_name = $documentId['document_path'];
                        $type = "courses";
                        $type_id = $course_doc_id;
                        $documentService = new DocumentService();
                        $documentService->deleteDocument($img_id, $img_name, $user_id, $type, $type_id);
                    }
                }
                UserDocuments::whereUserDocumentId($user_document_type['id'])->delete();
                UserDocumentsType::whereTypeId($course_doc_id)->whereType('courses')->whereUserId(Auth::id())->delete();
            }
            $result = $this->userService->deleteSeafarerCourseByCourseId($course_id,$user_id);
            return response()->json($result,200);

            if($result['status'] == 'success'){
                return response()->json($result,200);
            }else{
                return response()->json($result,400);
            }
        }
        else{
            return response()->json(['status' => 'failed' , 'message' => 'Error while adding data.'],400);
        }
    }

    public function getSeafarerCourseByCourseId(Request $request){

        if(Auth::check()){
            $data = $request->toArray();
            $user = Auth::user()->toArray();
            $user_id = $user['id'];
            
            $result = $this->userService->getSeafarerCourseByCourseId($data['course_id'],$user_id);
            
            $course_type = $result['result']['course_type'];

            if($course_type == 'Normal'){
                $coursess = \CommonHelper::courses();
                foreach ($coursess as $key => $value) {
                    $courses[] = "<option value=$key>$value</option>";
                }
            }else{
                $coursess = \CommonHelper::value_added_courses();
                foreach ($coursess as $key => $value) {
                    $courses[] = "<option value=$key>$value</option>";
                }
            }

            if($result['status'] == 'success'){
                return response()->json(['result' => $result,'courses' => $courses],200);
            }else{
                return response()->json(['result' => $result,'courses' => $courses],400);
            }
        }
        else{
            return response()->json(['status' => 'failed' , 'message' => 'Error while adding data.'],400);
        }
    }

    public function getSeafarerCoursesByCourseType(Request $request){

        if(Auth::check()){
            $course_type = $request->course_type;
           
            if($course_type == 'normal'){
                $courses = \CommonHelper::courses();
                foreach ($courses as $key => $value) {
                    $courses[] = "<option value=$key>$value</option>";
                }
            }else{
                $courses = \CommonHelper::value_added_courses();
                foreach ($courses as $key => $value) {
                    $courses[] = "<option value=$key>$value</option>";
                }
            }
            
            if($course_type){
                return response()->json(['courses' => $courses],200);
            }else{
                return response()->json(['courses' => ''],400);
            }
        }
        else{
            return response()->json(['status' => 'failed' , 'message' => 'Error while adding data.'],400);
        }
    }

    public function getUpload()
    {
        return view('pages.upload');
    }

    /*public function dropzone1(Request $request)
    {
        $photo = $request->toArray();
        
        if(Auth::check()){
            $user = Auth::user()->toArray();
            $user_id = $user['id'];
            $type_id = 0;

            if(isset($photo['type_id']) && !empty($photo['type_id'])){
                $type_id = $photo['type_id'];
            }

            $result = $this->userService->storeUserDocuments($photo['file'], $photo['type'], $type_id, $user_id);
            
            if($result['status'] == 'success'){
                return response()->json(['message' => 'Document stored successfully.','filename' => $result['filename'],'id' => $result['id'], 'type' => $photo['type'], 'type_id' => $type_id],200);
            }else{
                return response()->json(['message' => 'Error'],400);
            }
        }else{
            return response()->json(['message' => 'please login'],400);
        }
    }*/
	
	
	public function dropzone(Request $request)
    {
        $photo = $request->toArray();

        if(Auth::check()){
            $user = Auth::user()->toArray();
            $user_id = $user['id'];
            $type_id = 0;

            if(isset($photo['type_id']) && !empty($photo['type_id'])){
                $type_id = $photo['type_id'];
            }
            $fileSize = filesize($photo['file']);
            if($fileSize > 0){
                $fileSize = $fileSize/1024;
                $checkImageSize = CommonHelper::checkImageSize($fileSize);
                
                if($checkImageSize['status']){

                    $result = $this->userService->storeUserDocuments($photo['file'], $photo['type'], $type_id, $user_id);
                    if($result['status'] == 'success'){
                        //return response()->json(['message' => 'Document stored successfully.','filename' => $result['filename'],'id' => $result['id'], 'type' => $photo['type'], 'type_id' => $type_id],200);
                        //echo '<img src="' . env('APP_URL').'/'.$result['filename'] . '">';
                        $user = User::find(Auth::user()->id);
                        $user->used_kb = $user->used_kb + $fileSize;
                        if($user->save()){
//                             echo $photo['type'];
                             return response()->json(['message' => 'Your document uploaded successfully.', 'type' => $photo['type']],200);
//                            exit();
                        }
                        
                    }
                    else{
                        return response()->json(['message' => 'Error'],400);
                    }
                }else{
                    Session::flash('file-size-error','Your storage currently full you have no more space.');
                    return response()->json(['message' => 'Storage! Your storage currently full you have no more space.'],201);
                }
            }
        }else{
            return response()->json(['message' => 'please login'],400);
        }

    }

    public function deleteUpload(Request $request)
    {
        $image = $request->toArray();
        if(Auth::check()){
            $user = Auth::user()->toArray();
            $user_id  = $user['id'];
            $img_name = $image['img_name'];
            $img_id   = $image['id'];
            $docs = UserDocuments::find($img_id);
            $docsType = UserDocumentsType::where('id',$docs->user_document_id)->first();
            // $type = 'photo';
            $type = $docsType->type;
            $typeId = $docsType->type_id;
            // dump(isset($typeId));
            // dump($typeId != 0);
            // dump($typeId !== 0);
            // dd("fgldfjoigdigjdf");

            if(isset($typeId) && $typeId !== 0) {
                // dd("sdfjsdifjisodjf");
                $type_id = $typeId;
            } else {
                $type_id = 0;
            }
                // dd($type_id);
            $response = $this->userService->deleteDocument($img_id,$img_name,$user_id,$type,$type_id);

            if($response || $response == '0'){
                return response()->json(['message' => 'Document deleted successfully.'],200);
            }else{
                return response()->json(['message' => 'Document not found.'],400);
            }
        }
        else{
            return response()->json(['message' => 'please login'],400);
        }
    }
    public function getUserDocumentsList(Request $request){

        if(Auth::check()){
            $user = Auth::user()->toArray();
            $uploaded_documents = [];

            if(isset($request->user_id) && !empty($request->user_id)){
                $user_id = $request->user_id;
            }else{
                $user_id = $user['id'];
            }
            
            $options['with'] = ['userDangerousCargoEndorsementDetail', 'personal_detail','professional_detail','passport_detail','seaman_book_detail','coc_detail','gmdss_detail','wkfr_detail','personal_detail.state','personal_detail.city','personal_detail.pincode.pincodes_states.state','personal_detail.pincode.pincodes_cities.city','sea_service_detail','course_detail','cop_detail','coe_detail'];

            $result = $this->userService->getDataByUserId($user_id, $options)->toArray();
            if (isset($result[0]['user_dangerous_cargo_endorsement_detail'])) {
                $uploaded_documents['user_dangerous_cargo_endorsement_detail'] = $result[0]['user_dangerous_cargo_endorsement_detail'];
            }
            if(isset($result[0]['profile_pic'])){
				$uploaded_documents['profile_pic'] = $result[0]['profile_pic'];
			}
			
			if(isset($result[0]['professional_detail'])){
				$uploaded_documents['professional_detail'] = $result[0]['professional_detail'];
			}
			
			if(isset($result[0]['personal_detail'])){
				$uploaded_documents['personal_detail'] = $result[0]['personal_detail'];
			}

            if(isset($result[0]['professional_detail']['current_rank']) && !empty($result[0]['professional_detail']['current_rank'])){
                $required_documents = \CommonHelper::rank_required_fields()[$result[0]['professional_detail']['current_rank']];
            }
            
            //if(isset($required_documents) && !empty($required_documents)){

                //Passport visa block
                if(isset($result[0]['passport_detail']) && !empty($result[0]['passport_detail'])){
                    $uploaded_documents['passport'] = $result[0]['passport_detail'];
                    $uploaded_documents['passport']['title'] = 'Passport / Visa';
                }
                if(isset($result[0]['passport_detail']['us_visa']) && $result[0]['passport_detail']['us_visa'] == '1'){
                    $uploaded_documents['visa'] = $result[0]['passport_detail'];
                    if(isset($result[0]['passport_detail']) && !empty($result[0]['passport_detail'])){
                        $uploaded_documents['visa']['hide'] = 1;
                        $uploaded_documents['visa']['title'] = 'Passport / Visa';
                    }
                }

                //CDC
                if(isset($result[0]['seaman_book_detail']) && !empty($result[0]['seaman_book_detail'])){
                    $uploaded_documents['cdc'] = $result[0]['seaman_book_detail'];

                    if(isset($uploaded_documents['cdc']) && !empty($uploaded_documents['cdc'])){
                        foreach ($uploaded_documents['cdc'] as $key => $value) {
                            $uploaded_documents['cdc'][$key]['title'] = 'CDC';
                        }
                    }
                }
                
                //Coc Coe Block
               // if(in_array('COC', $required_documents)){
                //    dd($result);
                    $uploaded_documents['coc'] = $result[0]['coc_detail'];
                    
                    if(isset($uploaded_documents['coc']) && !empty($uploaded_documents['coc'])){
                        foreach ($uploaded_documents['coc'] as $key => $value) {
                            $uploaded_documents['coc'][$key]['title'] = 'COC / COE';
                        }
                    }
               // }

                //if(in_array('COE', $required_documents) || in_array('COE-Optional', $required_documents)){
                    $uploaded_documents['coe'] = $result[0]['coe_detail'];

                    if(in_array('COC', $required_documents)){
                        foreach ($uploaded_documents['coe'] as $key => $value) {
                            //$uploaded_documents['coe'][$key]['hide'] = 1;
                            $uploaded_documents['coe'][$key]['title'] = 'COC / COE';
                        }
                    }
                //}
                   
                //GMDSS watch keeping
                //if(in_array('GMDSS', $required_documents) || in_array('GMDSS-Optional', $required_documents)){
                    $uploaded_documents['gmdss'] = $result[0]['gmdss_detail'];
                    $uploaded_documents['gmdss']['title'] = 'GMDSS / WK';
               // }

               // if(in_array('WATCH_KEEPING-Optional', $required_documents)){
                    $uploaded_documents['watch keeping'] = $result[0]['wkfr_detail'];
                    $uploaded_documents['watch keeping']['title'] = 'GMDSS / WK';

                    //if(in_array('GMDSS', $required_documents) || in_array('GMDSS-Optional', $required_documents)){
                        //$uploaded_documents['watch keeping']['hide'] = 1;
                    //}
                //}

                //courses
                if(isset($result[0]['course_detail']) && !empty($result[0]['course_detail'])){

                    usort($result[0]['course_detail'], function ($a, $b)
                    {
                        $t1 = strtotime($a['created_at']);
                        $t2 = strtotime($b['created_at']);
                        return $t1 - $t2;
                    });

                    $uploaded_documents['courses'] = $result[0]['course_detail'];
                    foreach ($uploaded_documents['courses'] as $key => $value) {
                        $uploaded_documents['courses'][$key]['title'] = 'PreSea / PostSea Course Certificates';
                    }
                }
                
                //sea service
                
                if(isset($result[0]['sea_service_detail']) && !empty($result[0]['sea_service_detail'])){
                    $uploaded_documents['sea service'] = $result[0]['sea_service_detail'];

                    foreach ($result[0]['sea_service_detail'] as $key => $value) {
                        $uploaded_documents['sea service'][$key]['title'] = 'Sea Service Letters';
                    }
                }

                //yf vaccination
                if(isset($result[0]['wkfr_detail']) && $result[0]['wkfr_detail']['yellow_fever'] == '1'){
                    $uploaded_documents['yellow fever'] = $result[0]['wkfr_detail'];
                    $uploaded_documents['yellow fever']['title'] = 'Medicals / YF Vaccination';
                }
				
				
            //}
              

            $user_documents = $this->userService->getUserDocuments($user_id);
//            $user_documents = UserDocumentsType::where('user_id',$user_id)->with('user_documents')->get();
            if(isset($request->profile_status) && $request->profile_status == 'view_profile'){
                //$documents = View::make("site.partials.upload_documents_profile_dropzone")->with(['data' => $result[0], 'documents' => $uploaded_documents, 'user_uploaded_documents' => $user_documents]);
				$documents = View::make("site.partials.upload_documents_profile")->with(['data' => $result[0],'user_id'=>$user_id, 'documents' => $uploaded_documents, 'user_uploaded_documents' => $user_documents]);
            }else{
//                return $user_documents;
//                return $uploaded_documents;
               //$documents = View::make("site.partials.upload_documents_dropzone")->with(['data' => $result[0], 'documents' => $uploaded_documents, 'user_uploaded_documents' => $user_documents]);
				 $documents = View::make("site.partials.upload_documents_edit_profile")->with(['data' => $result[0],'user_id'=>$user_id, 'documents' => $uploaded_documents, 'user_uploaded_documents' => $user_documents]);
            }
            $template = $documents->render();

            if($result){
                return response()->json(['result' => $result, 'template' => $template,'message' => 'data found.'],200);
            }else{
                return response()->json(['result' => $result,'message' => 'Data not found'],400);
            }
        }
        else{
            return response()->json(['status' => 'failed' , 'message' => 'Error.'],400);
        }

    }

    public function getDocumentPath($user_id,$type=NULL,$option=NULL){

        if(!empty($user_id)){
            $requester_id = '';
            if(Auth::check()){
                $user = Auth::user()->toArray();
                $requester_id = $user['id'];
            }

            $public_docs = $this->documentService->getUserDocumentsList($user_id,$type,$option);

            if(isset($public_docs) && count($public_docs) > 0){

                $dir = 'public/uploads/user_documents/' . $user_id;
                
                if(isset($public_docs) && !empty($public_docs)){
                    
                    foreach ($public_docs as $key => $doc) {
                        $permission_dir = $dir;
                        
                        if(isset($doc['type']) && !empty($doc['type'])){
                            $permission_dir = $permission_dir . "/" . $doc['type'];
                            $type = $doc['type'];
                        }

                        if(isset($doc['type_id']) && !empty($doc['type'] && $doc['type_id'] != '0')){
                            $permission_dir = $permission_dir . "/" . $doc['type_id'];
                            $type = $type. " " . $doc['type_id'];
                        }

                        $requested_files[] = $permission_dir;
                        $requested_type[] = $type;
                    }
                }
                
                $files = $requested_files;  

                // $files = glob($dir . '/*');
                $source_arr = $requested_files;
				
                $zip_file = public_path().'/zip_download'.'/'.$user_id.'_' . time() .'_zip.zip';
               
                // Initialize archive object
                $zip = new ZipArchive();
                $zip->open($zip_file, ZipArchive::CREATE | ZipArchive::OVERWRITE);

                foreach ($source_arr as $source)
                {
                    if (!file_exists($source)) continue;
                    $source = str_replace('\\', '/', realpath($source));
                    
                    if (is_dir($source) === true)
                    {
                        $files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($source), RecursiveIteratorIterator::SELF_FIRST);

                        foreach ($files as $file)
                        {
                            $file = str_replace('\\', '/', realpath($file));

                            if (!is_dir($file) === true)
                            {
                                
                                // Get real and relative path for current file
                                $filePath = $file;
								//added by ashish GD(05-09-2019)
                                //echo $relativePath = substr($filePath, strlen($zip_file) - 1);
								$relativePath =$filePath;

                                $path = explode($user_id.'/', $relativePath);
								
                                // Add current file to archive
                                $zip->addFile($filePath, $path['1']);
                                chmod($filePath,0777);
                            }
                        }
                    }
                    else if (is_file($source) === true)
                    {
                        $zip->addFromString(basename($source), file_get_contents($source));
                    }

                }

                if(isset($requested_type) && !empty($requested_type)){
                    $this->userService->storeRequestedTypeDocumentsList($requested_type,$user_id,$requester_id);
                }
               
                $zip->close();

                header('Content-Description: File Transfer');
                header('Content-Type: application/octet-stream');
                header('Content-Disposition: attachment; filename='.basename($zip_file));
                header('Content-Transfer-Encoding: binary');
                header('Expires: 0');
                header('Cache-Control: must-revalidate');
                header('Pragma: public');
                header('Content-Length: ' . filesize($zip_file));
                readfile($zip_file);
                unlink($zip_file);

            }
            else{
                return Redirect::back()->withErrors(['You dont have permission to download this document.']);
            }
        }
        else
        {
            return Redirect::back()->withErrors(['Document not found.']);
        }

    }

    public function storeRequestedUserDocumentsList(Request $request){

        if(Auth::check()) {
            $user = Auth::user()->toArray();
            $user_id = $user['id'];
        }

        $data = $request->toArray();
        $data['requester_id'] = $user_id;
        $data['status'] = 0;
        $result = $this->userService->storeRequestedUserDocumentsList($data);

        if($result['status'] == 'success'){
            return response()->json(['status' => 'success' , 'message' => 'Your request has been sent successfully.' ],200);
        }else{
            return response()->json(['status' => 'failed' , 'message' => 'Selected documents are already requested.'],400);
        }
    }

    public function updateRequestedDocumentStatus(Request $request){
        $status_data = $request->toArray();
        $result = $this->userService->updateRequestedDocumentStatus($status_data);

        if($result){
            if($status_data['status'] == '1'){
                return response()->json(['status' => 'success' , 'message' => 'Request is accepted.' ,'status_value' => 'Accepted'],200);
            }
            else{
                return response()->json(['status' => 'success' , 'message' => 'Request is rejected.' ,'status_value' => 'Rejected'],200);
            }
        }else{
            return response()->json(['status' => 'failed' , 'message' => 'Your request is failed. Please try again.'],400);
        }
    }

    public function storeDocumentsPermission(Request $request){

        if(Auth::check()) {
            $user = Auth::user()->toArray();
            $user_id = $user['id'];

            $permissions = $request->toArray();

            $result = $this->userService->storeDocumentsPermissions($permissions,$user_id);

            if($result){
                return response()->json(['status' => 'success' , 'message' => 'Your documents has been added successfully.', 'update_email' => $user['is_email_verified'] , 'update_mobile' => $user['is_mob_verified'] ],200);
            }else{
                return response()->json(['status' => 'failed' , 'message' => 'Your request is failed. Please try again'],400);
            }
        }
    }

    public function changeUserDocumentStatus(Request $request){

        if(Auth::check()) {
            $user = Auth::user()->toArray();
            $user_id = $user['id'];

            $document_details = $request->toArray();

            $result = $this->userService->changeUserDocumentStatus($document_details,$user_id);

            if($result){
                return response()->json(['status' => 'success' , 'message' => 'Document status changed successfully.'],200);
            }else{
                return response()->json(['status' => 'failed' , 'message' => 'Please upload documents first.'],400);
            }
        }else{
            return response()->json(['status' => 'failed' , 'message' => 'Please login.'],400);
        }
    
    }

    public function getStateByCountryId($country_id){
        $city =  \App\State::orderBy('name','asc')->get()->toArray();
        return $city;
    }

    public function viewDocumentPath($user_id,$type = NULL,$type_id= NULL,$file_name = NULL){
        if(!empty($user_id)) {
            if (Auth::check()) {
                $user = Auth::user()->toArray();
                $login_id = $user['id'];

                if($user_id == $login_id){
                    $path = 'public/uploads/user_documents/'.$user_id;

                    if(isset($type) && !empty($type)){
                        $path = $path . "/" . $type;
                    }

                    if(isset($type_id) && !empty($type_id) && $type_id != '0'){
                        $path = $path . "/" . $type_id;
                    }

                    if(isset($file_name) && !empty($file_name)){
                        $path = $path . "/" . $file_name;
                    }

                    if(!File::exists($path)) {
                        return response()->json(['message' => 'Document not found.'], 400);
                    }

                    $file = File::get($path);
                    $type = File::mimeType($path);
                    $response = Response::make($file, 200);
                    $response->header("Content-Type", $type);
                    return $response;
                }
            }
        }

    }

  public function sendEmail($blade_file,$data, $array, $title, $subject, $type = null)
    {
        $data['host'] = 'FLANKNOT';
        $data['home'] = route('home');
        


        if(isset($array['first_name']) && !empty($array['first_name'])) {
            $name = $array['first_name'];
        } else {
            $name = 'Hello';
        }
        
        try {
            Mail::send($blade_file,['template' => $data], function ($message) use ($data, $array, $type, $title, $subject, $name) 
            {
                $message->from(env('MAIL_FROM_ADDRESS','donot-reply@flanknot.com'), $title);
                //$m->to('sachin@yopmail.com', $name)->subject($subject);
                $message->to($array['email'], $name)->subject($subject);

                if(isset($data['send_pdf']) && !empty($data['send_pdf'])){
                    $message->attach($data['file_path'], array(
                        'mime' => 'application/pdf')
                    );
                    if(isset($array['company_email']) && !empty($array['company_email'])){
                        $message->cc($array['email']);
                    }
                }
            });

            $status_arr = ['status' => 'success'];
            return $status_arr;


        } catch(Exception $e) {
            $status_arr = ['status' => 'failed', 'message' => $e->getMessage()];
            return $status_arr;
        }

    }


public function preemail(){

 $userData['otp'] = '456278';
		$get_mob_data= $this->userService->resendOtp($userData['otp'],'9971660364');
      //echo "<pre>"; print_r($get_mob_data); die;
	  /*$user_data=array();
	  
	   $user_data['is_email_verified'] = 0;
                $user['update_email'] = 1;
				 $user_data['email'] = 'jeet.super@gmail.com';
				 //print_r($user_data); die;
                //$this->sendEmailService->sendVerificationEmailToUser($user_data);
	    $user_data['link'] = route('site.user.verify.email', base64_encode($user_data['email']));

        $blade_file = 'emails.seafarerEmailVerification';
        $title = 'Course4Sea.com: Verify your email';
        $subject = 'Course4Sea.com: Verify your email';
        $message = 'Please verify your email by clicking';
        $data = $user_data;
        
        return $this->sendEmail($blade_file, $data, $user_data, $title, $subject, null);
		*/
	  
	  return response()->json([$get_mob_data],200);
}





 public function preRegistration(Request $request){
       
        $userData = $request->toArray();
        $userData['otp'] = $this->userService->generateOTP();
        $userData['registered_as'] = 'seafarer';
        $userData['pre_registration'] = '1';
        $logged_user_data = NULL;
       
        $result = $this->userService->store($userData,$logged_user_data);
		$users = DB::table('users')
                     ->select('id')
                     ->where('email', '=', $userData['email'])
                     ->get();
		$user_id=$users[0]->id;
		
		DB::table('user_documents_type')->insert(array(
		array('user_id' => $user_id, 'type' =>'coc'),
		array('user_id' => $user_id, 'type' =>'coe'),
		array('user_id' => $user_id, 'type' =>'cdc'),
		array('user_id' => $user_id, 'type' =>'course'),
		array('user_id' => $user_id, 'type' =>'photo'),
		array('user_id' => $user_id, 'type' =>'wk_cop'),
		array('user_id' => $user_id, 'type' =>'bank_doc'),
		array('user_id' => $user_id, 'type' =>'pancard'),
		array('user_id' => $user_id, 'type' =>'ilo_medical'),
		array('user_id' => $user_id, 'type' =>'passbook'),
		array('user_id' => $user_id, 'type' =>'yellow_fever'),
		array('user_id' => $user_id, 'type' =>'us_visa'),
		array('user_id' => $user_id, 'type' =>'aadhaar'),
		array('user_id' => $user_id, 'type' =>'medical'),
		array('user_id' => $user_id, 'type' =>'gm_dss'),
		array('user_id' => $user_id, 'type' =>'sea_services'),
		array('user_id' => $user_id, 'type' =>'indos'),
		array('user_id' => $user_id, 'type' =>'passport'),
		array('user_id' => $user_id, 'type' =>'gmdss'),
		array('user_id' => $user_id, 'type' =>'sid'),
         )
		);
	    
		if(isset($request->user_referral_code) && !empty($request->user_referral_code)){
               $record = $this->userRepository->getReferalCode($request->user_referral_code);
                if(!empty($record)){
                   
                    $referraldata = [
                        'affiliate_user_id' => $record->id,
                        'affiliate_referral_code' => $record->user_affiliate_code,
                        'affiliate_referral_by' => 1
                    ];
                    $updatedRecord = $this->userRepository->addReferalDetails($user_id,$referraldata);
                    $affiliate_data = [
                        'user_id' => $record->id,
                        'referral_user_id' => \Auth::id(),
                        'type' => 1
                    ];
                    $this->userRepository->addRewardsData($affiliate_data);
                } 
           
        }
       
        $userData['link'] = route('site.user.verify.email', base64_encode($userData['email']));

        $blade_file = 'emails.seafarerEmailVerification';
        $title = 'FLANKNOT: Verify your email';
        $subject = 'FLANKNOT: Verify your email';
        $message = 'Please verify your email by clicking';
        $data = $userData;
        $this->sendEmail($blade_file, $data, $userData, $title, $subject, null);
	
        if($result['status'] == 'success'){
            return response()->json([$result],200);
            // return response()->json(['status' => 'success' , 'message' => 'Your basic details has been store successfully.', 'update_mobile' => '0', 'block_seat' => 1],200);
        }else{
            return response()->json(['status' => 'failed' , 'message' => 'Error while adding data.'],400);
        }
    }

    //seafarer pre-registration while institute batch booking
    public function preRegistration1(){
            
        $userData['otp'] = '456278';
		echo $this->userService->resendOtp($userData['otp'],'9971660364');
        die;
        $userData = $request->toArray();
        $userData['otp'] = $this->userService->generateOTP();
		echo $this->userService->resendOtp($userData['otp'],$userData['mobile']);
		return response()->json(['status' => 'failed' , 'message' => 'Error while adding data.'],400);
		die;
        $userData['registered_as'] = 'seafarer';
        $userData['pre_registration'] = '1';
        $logged_user_data = NULL;
        
        $result = $this->userService->store($userData,$logged_user_data);
       
        if($result['status'] == 'success'){
                
            return response()->json([$result],200);
            // return response()->json(['status' => 'success' , 'message' => 'Your basic details has been store successfully.', 'update_mobile' => '0', 'block_seat' => 1],200);
        }else{
            return response()->json(['status' => 'failed' , 'message' => 'Error while adding data.'],400);
        }
    }

    public function myBookings(Request $request){
        $pageTitle = "Course booking – Course booking order list | Flanknot";
        $metaDescription = "The Order of all courses booked at Flanknot.
            At Flanknot, The Seafarer User can track progress from the User booking up to the course conducted.";
        $metaKeywords = "Course booking, list of course orders, course booking tracking";
        if(Auth::check()){
            $user_id = Auth::User()->id;
            $options['with'] = ['order_payments','order_history.user','batch_details.course_details.courses','batch_details.course_details.course_details','batch_details.course_details.institute_registration_detail','batch_details.course_details.institute_registration_detail.user','batch_details.batch_location'];
            $orders = $this->orderService->getOrderDetailByUserId($user_id,$options);

            if($orders){
                //$orders = $orders->toArray();
            }
            //dd($orders);
        }
        return view('site.user.my_bookings',['data' => $orders, 'pageTitle' => $pageTitle, 'metaDescription' => $metaDescription, 'metaKeywords' => $metaKeywords]);
    }

    public function wkUpdate(Request $request) {
        $data=$request->toArray();
	    if (isset($data['wk_cop'])){
	        $result = UserWkfrDetail::whereUserId(Auth::id())->update(['wk_cop'=>$data['wk_cop']]);
	        if ($result) return response("Updated", 200);
        }
        return response('Not Updated', 400);
    }

    public function userProfile(Request $request){
        $user = '';
        $role = '';
        $pageTitle = "Seafarer Profile - A user profile for Seafarers | Flanknot";
        $metaDescription = "Flanknot is the best place to find Complete Seafarer data.
            Professional Detail, Contact Information, Personality details, Seafarer traveling document data, Sailing document data, Sea Service experience detail, Course Certificate data and more.";
        $metaKeywords = "Flanknot user-profile";
        if(isset($user_id)){
            $user = 'another_user';
        }
        if(Auth::check() && !isset($user_id)){
            $user_id = Auth::user()->id;

        }elseif (isset($user_id) && $user_id != '') {
            # code...
        }
        
        if(Auth::check()){
            $role = Auth::user()->registered_as;
        }

        if(isset($user_id)){

            $options['with'] = ['userDangerousCargoEndorsementDetail', 'visas', 'personal_detail','professional_detail','passport_detail','seaman_book_detail','coc_detail','gmdss_detail','wkfr_detail','personal_detail.state','personal_detail.city','personal_detail.pincode.pincodes_states.state','personal_detail.pincode.pincodes_cities.city','sea_service_detail','course_detail','cop_detail','coe_detail','document_permissions.requester.company_registration_detail','document_permissions.requester.institute_registration_detail','document_permissions.document_type.type'];
            $result = $this->userService->getDataByUserId($user_id, $options)->toArray();
            if(isset($result[0]['professional_detail']['current_rank_exp'])){
                $rank_exp = explode(".", $result[0]['professional_detail']['current_rank_exp']);
                $result[0]['professional_detail']['years'] = $rank_exp[0];
                $result[0]['professional_detail']['months'] = ltrim($rank_exp[1],0);
            }
            
            $courses = $this->userService->getAllCourses();

            $user_documents = $this->userService->getUserDocuments($user_id);
            $uploaded_documents = [];
            // dd($user_documents);

            if(isset($result[0]['professional_detail']['current_rank']) && !empty($result[0]['professional_detail']['current_rank'])){
                $required_documents = \CommonHelper::rank_required_fields()[$result[0]['professional_detail']['current_rank']];
            }
            
            //if(isset($required_documents) && !empty($required_documents)){

                //Passport visa block
                if(isset($result[0]['passport_detail']) && !empty($result[0]['passport_detail'])){
                    $uploaded_documents['passport'] = $result[0]['passport_detail'];
                    $uploaded_documents['passport']['title'] = 'Passport / Visa';

                    if(isset($user_documents['passport']) && !empty($user_documents['passport'])){
                        $uploaded_documents['passport']['imp_doc'] = $user_documents['passport'];
                    }
                }
                if(isset($result[0]['passport_detail']['us_visa']) && $result[0]['passport_detail']['us_visa'] == '1'){
                    $uploaded_documents['visa'] = $result[0]['passport_detail'];
                    if(isset($result[0]['passport_detail']) && !empty($result[0]['passport_detail'])){
                        $uploaded_documents['visa']['hide'] = 1;
                        $uploaded_documents['visa']['title'] = 'Passport / Visa';
                    }

                    if(isset($user_documents['visa']) && !empty($user_documents['visa'])){
                        $uploaded_documents['visa']['imp_doc'] = $user_documents['visa'];
                    }
                }
                
                //CDC
                if(isset($result[0]['seaman_book_detail']) && !empty($result[0]['seaman_book_detail'])){
                    $uploaded_documents['cdc'] = $result[0]['seaman_book_detail'];

                    if(isset($uploaded_documents['cdc']) && !empty($uploaded_documents['cdc'])){
                        foreach ($uploaded_documents['cdc'] as $key => $value) {
                            if(isset($user_documents['cdc'][$key+1]['user_documents']) && !empty($user_documents['cdc'][$key+1]['user_documents'])){
                                $uploaded_documents['cdc'][$key]['title'] = 'CDC';

                                if(isset($user_documents['cdc'][$key+1]) && !empty($user_documents['cdc'][$key+1])){
                                    $uploaded_documents['cdc'][$key]['imp_doc'] = $user_documents['cdc'][$key+1];
                                }
                            }
                        }
                    }
                }
                
                //Coc Coe Block
                //if(in_array('COC', $required_documents)){
                    $uploaded_documents['coc'] = $result[0]['coc_detail'];
                    
                    if(isset($uploaded_documents['coc']) && !empty($uploaded_documents['coc'])){
                        foreach ($uploaded_documents['coc'] as $key => $value) {
                            if(isset($user_documents['coc'][$key+1]['user_documents']) && !empty($user_documents['coc'][$key+1]['user_documents'])){
                                $uploaded_documents['coc'][$key]['title'] = 'COC / COE';

                                if(isset($user_documents['coc'][$key+1]) && !empty($user_documents['coc'][$key+1]))
                                    $uploaded_documents['coc'][$key]['imp_doc'] = $user_documents['coc'][$key+1];
                            }
                        }
                    }
                //}

                //if(in_array('COE', $required_documents) || in_array('COE-Optional', $required_documents)){
                    $uploaded_documents['coe'] = $result[0]['coe_detail'];

                    if(in_array('COC', $required_documents)){
                        foreach ($uploaded_documents['coe'] as $key => $value) {
                            if(isset($user_documents['coe'][$key+1]['user_documents']) && !empty($user_documents['coe'][$key+1]['user_documents'])){
                                $uploaded_documents['coe'][$key]['hide'] = 1;
                                $uploaded_documents['coe'][$key]['title'] = 'COC / COE';

                                if(isset($user_documents['coe'][$key+1]) && !empty($user_documents['coe'][$key+1]))
                                    $uploaded_documents['coe'][$key]['imp_doc'] = $user_documents['coe'][$key+1];
                            }
                        }
                    }
                //}

                //GMDSS watch keeping
                //if(in_array('GMDSS', $required_documents) || in_array('GMDSS-Optional', $required_documents)){
                    $uploaded_documents['gmdss'] = $result[0]['gmdss_detail'];
                    $uploaded_documents['gmdss']['title'] = 'GMDSS / WK';

                    if(isset($user_documents['gmdss']) && !empty($user_documents['gmdss'])){
                        $uploaded_documents['gmdss']['imp_doc'] = $user_documents['gmdss'];
                    }
                //}

                //if(in_array('WATCH_KEEPING-Optional', $required_documents)){
                    $uploaded_documents['watch keeping'] = $result[0]['wkfr_detail'];
                    $uploaded_documents['watch keeping']['title'] = 'GMDSS / WK';

                    if(in_array('GMDSS', $required_documents) || in_array('GMDSS-Optional', $required_documents)){
                        $uploaded_documents['watch keeping']['hide'] = 1;

                        if(isset($user_documents['watch keeping']) && !empty($user_documents['watch keeping'])){
                            $uploaded_documents['watch keeping']['imp_doc'] = $user_documents['watch keeping'];
                        }

                    }
               // }

                //courses
                if(isset($result[0]['course_detail']) && !empty($result[0]['course_detail'])){
                    $uploaded_documents['courses'] = $result[0]['course_detail'];

                    foreach ($uploaded_documents['courses'] as $key => $value) {
                        if(isset($user_documents['courses'][$key+1]['user_documents']) && !empty($user_documents['courses'][$key+1]['user_documents'])){
                            $uploaded_documents['courses'][$key]['title'] = 'PreSea / PostSea Course Certificates';

                            if(isset($user_documents['courses'][$key+1]) && !empty($user_documents['courses'][$key+1]))
                                $uploaded_documents['courses'][$key]['imp_doc'] = $user_documents['courses'][$key+1];
                        }
                    }
                }
                
                //sea service
                if(isset($result[0]['sea_service_detail']) && !empty($result[0]['sea_service_detail'])){
                    $uploaded_documents['sea service'] = $result[0]['sea_service_detail'];

                    foreach ($result[0]['sea_service_detail'] as $key => $value) {
                        if(isset($user_documents['sea service'][$key+1]['user_documents']) && !empty($user_documents['sea service'][$key+1]['user_documents'])){
                            $uploaded_documents['sea service'][$key]['title'] = 'Sea Service Letters';

                            if(isset($user_documents['sea service'][$key+1]) && !empty($user_documents['sea service'][$key+1])){
                                $uploaded_documents['sea service'][$key]['imp_doc'] = $user_documents['sea service'][$key+1];
                            }
                        }
                    }
                }

                //yf vaccination
                if(isset($result[0]['wkfr_detail']) && $result[0]['wkfr_detail']['yellow_fever'] == '1'){
                    $uploaded_documents['yellow fever'] = $result[0]['wkfr_detail'];
                    if(isset($user_documents['yellow fever'][0]['user_documents']) && !empty($user_documents['yellow fever'][0]['user_documents'])){
                        $uploaded_documents['yellow fever']['title'] = 'Medicals / YF Vaccination';

                        if(isset($user_documents['yellow fever']) && !empty($user_documents['yellow fever'])){
                            $uploaded_documents['yellow fever']['imp_doc'] = $user_documents['yellow fever'];
                        }
                    }
                }
            //}

            $count = 0;
            if(isset($result[0]['course_detail'])){
                foreach ($result[0]['course_detail'] as $key => $value) {
                    if($value['course_type'] == 'Value Added'){
                        $result[0]['value_added_course_detail'][$count] = $value;
                        $count++;
                        unset($result[0]['course_detail'][$key]);
                    }
                }
            }
            $userId = Auth::user()->id;
            $courses = Courses::pluck('course_name','id')->toArray();
            $userCourses = UserCoursesCertificate::select('*')->whereUserId($userId)->groupBy('course_id')->get();
            if(count($result) > 0 && ($result[0]['registered_as'] == null || $result[0]['registered_as'] != 'advertiser')){
                // set var for rank
                $userProData = UserProfessionalDetail::whereUserId($userId)->first();
                $userPersonalData = UserPersonalDetail::whereUserId($userId)->first();
                $userPassData = UserPassportDetail::whereUserId($userId)->first();
                $userCocDetail = UserCocDetail::whereUserId($userId)->first();
                $userCoeDetail = UserCoeDetails::whereUserId($userId)->first();
                $userWkfrDetail = UserWkfrDetail::whereUserId($userId)->first();

                $userGmdssDetail = UserGmdssDetail::whereUserId($userId)->first();
                $userSemanBookDetail = UserSemanBookDetail::whereUserId($userId)->first();
                $userDceDetail = UserDangerousCargoEndorsementDetail::whereUserId($userId)->first();

                $show_resume = CommonHelper::getResumeNumber();
                $rank = array_flatten(\CommonHelper::new_rank());
                $country = \CommonHelper::countries();
                $shipData = \CommonHelper::ship_type();
                $shipType = UserSeaService::select('*')->whereUserId($userId)->groupBy('ship_type')->get();
                $pass = $userPassData->pass_number ? 'Passport' : null;
                $vissaPass = count($result[0]['visas']) > 0 ? 'Visa' : null;
                $indos = !empty($userWkfrDetail->indos_number) ? 'INDOS' : null;
                $coc = !empty($userCocDetail->coc_number) ? 'COC' : null;
                $coe = !empty($userCoeDetail->coe_number) ? 'COE' : null;
                $gmd = !empty($userGmdssDetail->gmdss_number) ? 'GMDSS' : null;
                $userDceDetail = !empty($userDceDetail) ? 'DCE' : null;
                $semanBook = !empty($userSemanBookDetail->cdc_number) ? 'CDC': null;
                $SID = !empty($userProData->sid_number) ? 'SID': null;
                $sailingCertificate = $userWkfrDetail['wk_cop'] && $userWkfrDetail['type'] ? 'Sailing Certificate': null;
                if($userWkfrDetail && $userWkfrDetail['wk_cop'] == 'wk') {
                    $sailingCertificate = 'WK'; 
                } else if($userWkfrDetail && $userWkfrDetail['wk_cop'] == 'cop') {
                    $sailingCertificate = 'COP'; 
                }
                $medical = ($userWkfrDetail['ilo_medical'] == 1 || $userWkfrDetail['typescreening_test'] == 1) ? 'MEDICAL': null;
                $vaccination = ($userWkfrDetail['yellow_fever'] == 1 || $userWkfrDetail['cholera'] == 1 || $userWkfrDetail['hepatitis_b'] == 1 || $userWkfrDetail['hepatitis_c'] == 1 || $userWkfrDetail['diphtheria'] == 1 || $userWkfrDetail['covid'] == 1) ? 'VACCINATION': null;
                $docsArray = [$pass,$vissaPass, $medical, $vaccination,$indos,$semanBook,$coc,$coe,$sailingCertificate,$gmd, $userDceDetail, $SID];
                $docsArray = array_filter($docsArray);
                $docsType = null;
                $percentageMb = 0;
                $usedMb = 0;
                if(!empty(Auth::user()->used_kb)){
                    $usedMb = Auth::user()->used_kb / 1024;
                    $maxKb = Auth::user()->max_kb / 1024;
                    $percentageMb = ($usedMb * 100) / $maxKb; 
                }
                if($request->ajax()){

                    $medicalArray = [];
                    if($result[0]['wkfr_detail']['ilo_medical'] == 1) {
                        array_push($medicalArray, [
                            'country' => $result[0]['wkfr_detail']['ilo_country'],
                            "type" => "ILO Medical",
                            "issue_on" => (isset($result[0]['wkfr_detail']['ilo_issue_date']) ? date('d-m-Y',strtotime($result[0]['wkfr_detail']['ilo_issue_date'])) : '-')
                        ]);
                    }
                    if($result[0]['wkfr_detail']['screening_test'] == 1) {
                        array_push($medicalArray, [
                            'country' => $result[0]['wkfr_detail']['screening_test_county'],
                            "type" => "Screening Test",
                            "issue_on" => (isset($result[0]['wkfr_detail']['screening_test_date']) ? date('d-m-Y',strtotime($result[0]['wkfr_detail']['screening_test_date'])) : '-')
                        ]);
                    }

                    
                    $vaccineArray = [];
                    if($result[0]['wkfr_detail']['yellow_fever'] == 1) {
                        array_push($vaccineArray, [
                            'country' => $result[0]['wkfr_detail']['yf_country'],
                            "type" => "Yellow Fever",
                            "issue_on" => (isset($result[0]['wkfr_detail']['yf_issue_date']) ? date('d-m-Y',strtotime($result[0]['wkfr_detail']['yf_issue_date'])) : '-')
                        ]);
                    }
                    if($result[0]['wkfr_detail']['cholera'] == 1) {
                        array_push($vaccineArray, [
                            'country' => $result[0]['wkfr_detail']['cholera_country'],
                            "type" => "Cholera",
                            "issue_on" => (isset($result[0]['wkfr_detail']['cholera_issue_date']) ? date('d-m-Y',strtotime($result[0]['wkfr_detail']['cholera_issue_date'])) : '-')
                        ]);
                    }

                    if($result[0]['wkfr_detail']['hepatitis_b'] == 1) {
                        array_push($vaccineArray, [
                            'country' => $result[0]['wkfr_detail']['hepatitis_b_country'],
                            "type" => "Hepatitis B",
                            "issue_on" => (isset($result[0]['wkfr_detail']['hepatitis_b_issue_date']) ? date('d-m-Y',strtotime($result[0]['wkfr_detail']['hepatitis_b_issue_date'])) : '-')
                        ]);
                    }
                    if($result[0]['wkfr_detail']['hepatitis_c'] == 1) {
                        array_push($vaccineArray, [
                            'country' => $result[0]['wkfr_detail']['hepatitis_c_country'],
                            "type" => "Hepatitis C",
                            "issue_on" => (isset($result[0]['wkfr_detail']['hepatitis_c_issue_date']) ? date('d-m-Y',strtotime($result[0]['wkfr_detail']['hepatitis_c_issue_date'])) : '-')
                        ]);
                    }
                    if($result[0]['wkfr_detail']['diphtheria'] == 1) {
                        array_push($vaccineArray, [
                            'country' => $result[0]['wkfr_detail']['diphtheria_country'],
                            "type" => "Diphtheria and tetanus",
                            "issue_on" => (isset($result[0]['wkfr_detail']['diphtheria_issue_date']) ? date('d-m-Y',strtotime($result[0]['wkfr_detail']['diphtheria_issue_date'])) : '-')
                        ]);
                    }
                    if($result[0]['wkfr_detail']['covid'] == 1) {
                        array_push($vaccineArray, [
                            'country' => $result[0]['wkfr_detail']['covid_country'],
                            "type" => "Covid19",
                            "issue_on" => (isset($result[0]['wkfr_detail']['covid_issue_date']) ? date('d-m-Y',strtotime($result[0]['wkfr_detail']['covid_issue_date'])) : '-')
                        ]);
                    }
                    $courseId = $request->courseId;
                    $shipTypeData = UserSeaService::whereUserId($userId)->orderBy('from', 'DESC')->orderBy('to', 'DESC');
                    $courseData = UserCoursesCertificate::whereUserId($userId);
                    if($request->shipId){
                        if($request->shipId != 'all'){
                            $shipTypeData = $shipTypeData->whereShipType($request->shipId);
                        }
                        $view = 'ship_type_data';
                        $type = 1;
                    }
                    if($request->courseId){
                        if($request->courseId != 'all'){
                            $courseData = $courseData->whereCourseId($request->courseId);
                        }

                        $view = 'courses_data';
                        $type = 2;
                    }
                    if($request->docsId || $request->docsId == '0') {
                        $view = 'document_data';
                        $type = 3;
                        $docsType = $request->docsId;
                    }
                    $shipTypeData = $shipTypeData->get();
                    $result[0]['sea_service_detail'] = $shipTypeData;
                    $courseData = $courseData->get();
                    $result[0]['course_detail'] = $courseData;
                    $data['type'] = $type;
                    $data['shipData'] = View::make('site.user.'.$view,[
                        'data' => $result,'usedMb'=>$usedMb,'userSemanBookDetail'=>$userSemanBookDetail,
                        'sailingCertificate'=>$sailingCertificate,'userCocDetail'=>$userCocDetail,
                        'coursesName'=>$courses,'docsType'=>$docsType,'courseData'=>$courseData,
                        'userWkfrDetail'=>$userWkfrDetail,'userCourses'=>$userCourses,
                        'country'=>$country,'shipData'=>$shipData,'userProData'=>$userProData,'courseId' => $courseId, 'medicalArray' =>$medicalArray,'vaccineArray'=>$vaccineArray])->render();
                    return $data;
                }
                return view('site.user.profile2',['data' => $result,'percentageMb'=>$percentageMb,'usedMb'=>$usedMb,'user' => $user,'coursesName'=>$courses,'docsArray'=>$docsArray,'userCourses'=>$userCourses,'country'=>$country,'userPassData'=>$userPassData,'shipData'=>$shipData,'shipType'=>$shipType,'rank'=>$rank,'userPersonalData'=>$userPersonalData,'userProData'=>$userProData, 'all_courses' => $courses,'role' => $role,'user_documents' => $user_documents,'user_id' => $user_id, 'documents' => $uploaded_documents, 'show_resume' => $show_resume, 'pageTitle' => $pageTitle, 'metaDescription' => $metaDescription, 'metaKeywords' => $metaKeywords]);
            }else{
                return view('errors.404');
            }
        return view('site.user.profile2');
        }   
    }

    public function shareUserProfile(Request $request,$user_id = null){
        $pageTitle = "Seafarer Profile - User profile | Flanknot";
        $metaDescription = "At Flanknot view Seafarer user Profile.
            Professional Detail, Contact Information, Personality details, Seafarer traveling document data, Sailing document data, Sea Service experience detail, Course Certificate data and more.";
        $metaKeywords = "Flanknot user-profile, resume, user document, onboard experience, infographic";
        if(isset($request->share_user_id) && !empty($request->share_user_id)){
            $user_id = \CommonHelper::decodeKey($request->share_user_id);
        }
        //$user_id = \CommonHelper::decodeKey($request['profile']);
        $user = '';
        $role = '';
        if(\Request::route()->getName() == "demo-share-profile"){
            $user_id = 239;
            if (isset($request->token) && !empty($request->token)) {
                $token = \CommonHelper::decodeKey($request->token);
                if (!empty($token)) {
                    $PromotionalEmailLog = new PromotionalEmailLog();
                    $PromotionalEmailLog->promotional_email_id = $token;
                    $PromotionalEmailLog->created_at = date('Y-m-d H:i:s');
                    $PromotionalEmailLog->save();
                }
            }

        }

        if(isset($user_id)){
            $user = 'another_user';
        }
        
//        if(Auth::check()){
//            $role = Auth::user()->registered_as;
//        }

        if(isset($user_id)){

            $options['with'] = ['userDangerousCargoEndorsementDetail', 'visas', 'personal_detail','professional_detail','passport_detail','seaman_book_detail','coc_detail','gmdss_detail','wkfr_detail','personal_detail.state','personal_detail.city','personal_detail.pincode.pincodes_states.state','personal_detail.pincode.pincodes_cities.city','sea_service_detail','course_detail','cop_detail','coe_detail','document_permissions.requester.company_registration_detail','document_permissions.requester.institute_registration_detail','document_permissions.document_type.type'];
            $result = $this->userService->getDataByUserId($user_id, $options)->toArray();
//            dd($result);
            if(isset($result[0]['professional_detail']['current_rank_exp'])){
                $rank_exp = explode(".", $result[0]['professional_detail']['current_rank_exp']);
                $result[0]['professional_detail']['years'] = $rank_exp[0];
                $result[0]['professional_detail']['months'] = ltrim($rank_exp[1],0);
            }
            if(isset($result[0]['registered_as'])){
                $role = $result[0]['registered_as'];
            }
            
            $courses = $this->userService->getAllCourses();

            $user_documents = $this->userService->getUserDocuments($user_id);
            $uploaded_documents = [];
            // dd($user_documents);
            $required_documents = [];

            if(isset($result[0]['professional_detail']['current_rank']) && !empty($result[0]['professional_detail']['current_rank'])){
                $required_documents = \CommonHelper::rank_required_fields()[$result[0]['professional_detail']['current_rank']];
            }
            
            //if(isset($required_documents) && !empty($required_documents)){

                //Passport visa block
                if(isset($result[0]['passport_detail']) && !empty($result[0]['passport_detail'])){
                    $uploaded_documents['passport'] = $result[0]['passport_detail'];
                    $uploaded_documents['passport']['title'] = 'Passport / Visa';

                    if(isset($user_documents['passport']) && !empty($user_documents['passport'])){
                        $uploaded_documents['passport']['imp_doc'] = $user_documents['passport'];
                    }
                }
                if(isset($result[0]['passport_detail']['us_visa']) && $result[0]['passport_detail']['us_visa'] == '1'){
                    $uploaded_documents['visa'] = $result[0]['passport_detail'];
                    if(isset($result[0]['passport_detail']) && !empty($result[0]['passport_detail'])){
                        $uploaded_documents['visa']['hide'] = 1;
                        $uploaded_documents['visa']['title'] = 'Passport / Visa';
                    }

                    if(isset($user_documents['visa']) && !empty($user_documents['visa'])){
                        $uploaded_documents['visa']['imp_doc'] = $user_documents['visa'];
                    }
                }
                
                //CDC
                if(isset($result[0]['seaman_book_detail']) && !empty($result[0]['seaman_book_detail'])){
                    $uploaded_documents['cdc'] = $result[0]['seaman_book_detail'];

                    if(isset($uploaded_documents['cdc']) && !empty($uploaded_documents['cdc'])){
                        foreach ($uploaded_documents['cdc'] as $key => $value) {
                            if(isset($user_documents['cdc'][$key+1]['user_documents']) && !empty($user_documents['cdc'][$key+1]['user_documents'])){
                                $uploaded_documents['cdc'][$key]['title'] = 'CDC';

                                if(isset($user_documents['cdc'][$key+1]) && !empty($user_documents['cdc'][$key+1])){
                                    $uploaded_documents['cdc'][$key]['imp_doc'] = $user_documents['cdc'][$key+1];
                                }
                            }
                        }
                    }
                }
                
                //Coc Coe Block
                //if(in_array('COC', $required_documents)){
                    $uploaded_documents['coc'] = $result[0]['coc_detail'];
                    
                    if(isset($uploaded_documents['coc']) && !empty($uploaded_documents['coc'])){
                        foreach ($uploaded_documents['coc'] as $key => $value) {
                            if(isset($user_documents['coc'][$key+1]['user_documents']) && !empty($user_documents['coc'][$key+1]['user_documents'])){
                                $uploaded_documents['coc'][$key]['title'] = 'COC / COE';

                                if(isset($user_documents['coc'][$key+1]) && !empty($user_documents['coc'][$key+1]))
                                    $uploaded_documents['coc'][$key]['imp_doc'] = $user_documents['coc'][$key+1];
                            }
                        }
                    }
                //}

                //if(in_array('COE', $required_documents) || in_array('COE-Optional', $required_documents)){
                    $uploaded_documents['coe'] = $result[0]['coe_detail'];

                    if(in_array('COC', $required_documents)){
                        foreach ($uploaded_documents['coe'] as $key => $value) {
                            if(isset($user_documents['coe'][$key+1]['user_documents']) && !empty($user_documents['coe'][$key+1]['user_documents'])){
                                $uploaded_documents['coe'][$key]['hide'] = 1;
                                $uploaded_documents['coe'][$key]['title'] = 'COC / COE';

                                if(isset($user_documents['coe'][$key+1]) && !empty($user_documents['coe'][$key+1]))
                                    $uploaded_documents['coe'][$key]['imp_doc'] = $user_documents['coe'][$key+1];
                            }
                        }
                    }
                //}

                //GMDSS watch keeping
                //if(in_array('GMDSS', $required_documents) || in_array('GMDSS-Optional', $required_documents)){
                    $uploaded_documents['gmdss'] = $result[0]['gmdss_detail'];
                    $uploaded_documents['gmdss']['title'] = 'GMDSS / WK';

                    if(isset($user_documents['gmdss']) && !empty($user_documents['gmdss'])){
                        $uploaded_documents['gmdss']['imp_doc'] = $user_documents['gmdss'];
                    }
                //}

                //if(in_array('WATCH_KEEPING-Optional', $required_documents)){
                    $uploaded_documents['watch keeping'] = $result[0]['wkfr_detail'];
                    $uploaded_documents['watch keeping']['title'] = 'GMDSS / WK';

                    if(in_array('GMDSS', $required_documents) || in_array('GMDSS-Optional', $required_documents)){
                        $uploaded_documents['watch keeping']['hide'] = 1;

                        if(isset($user_documents['watch keeping']) && !empty($user_documents['watch keeping'])){
                            $uploaded_documents['watch keeping']['imp_doc'] = $user_documents['watch keeping'];
                        }

                    }
               // }

                //courses
                if(isset($result[0]['course_detail']) && !empty($result[0]['course_detail'])){
                    $uploaded_documents['courses'] = $result[0]['course_detail'];

                    foreach ($uploaded_documents['courses'] as $key => $value) {
                        if(isset($user_documents['courses'][$key+1]['user_documents']) && !empty($user_documents['courses'][$key+1]['user_documents'])){
                            $uploaded_documents['courses'][$key]['title'] = 'PreSea / PostSea Course Certificates';

                            if(isset($user_documents['courses'][$key+1]) && !empty($user_documents['courses'][$key+1]))
                                $uploaded_documents['courses'][$key]['imp_doc'] = $user_documents['courses'][$key+1];
                        }
                    }
                }
                
                //sea service
                if(isset($result[0]['sea_service_detail']) && !empty($result[0]['sea_service_detail'])){
                    $uploaded_documents['sea service'] = $result[0]['sea_service_detail'];

                    foreach ($result[0]['sea_service_detail'] as $key => $value) {
                        if(isset($user_documents['sea service'][$key+1]['user_documents']) && !empty($user_documents['sea service'][$key+1]['user_documents'])){
                            $uploaded_documents['sea service'][$key]['title'] = 'Sea Service Letters';

                            if(isset($user_documents['sea service'][$key+1]) && !empty($user_documents['sea service'][$key+1])){
                                $uploaded_documents['sea service'][$key]['imp_doc'] = $user_documents['sea service'][$key+1];
                            }
                        }
                    }
                }

                //yf vaccination
                if(isset($result[0]['wkfr_detail']) && $result[0]['wkfr_detail']['yellow_fever'] == '1'){
                    $uploaded_documents['yellow fever'] = $result[0]['wkfr_detail'];
                    if(isset($user_documents['yellow fever'][0]['user_documents']) && !empty($user_documents['yellow fever'][0]['user_documents'])){
                        $uploaded_documents['yellow fever']['title'] = 'Medicals / YF Vaccination';

                        if(isset($user_documents['yellow fever']) && !empty($user_documents['yellow fever'])){
                            $uploaded_documents['yellow fever']['imp_doc'] = $user_documents['yellow fever'];
                        }
                    }
                }
            //}

            $count = 0;
            if(isset($result[0]['course_detail'])){
                foreach ($result[0]['course_detail'] as $key => $value) {
                    if($value['course_type'] == 'Value Added'){
                        $result[0]['value_added_course_detail'][$count] = $value;
                        $count++;
                        unset($result[0]['course_detail'][$key]);
                    }
                }
            }
            $userId = $user_id;
            $courses = Courses::pluck('course_name','id')->toArray();
            $userCourses = UserCoursesCertificate::select('*')->whereUserId($userId)->groupBy('course_id')->get();
            if(count($result) > 0 && ($result[0]['registered_as'] == null || $result[0]['registered_as'] != 'advertiser')){
                // set var for rank
                $userProData = UserProfessionalDetail::whereUserId($userId)->first();
                $userPersonalData = UserPersonalDetail::whereUserId($userId)->first();
                $userPassData = UserPassportDetail::whereUserId($userId)->first();
                $userCocDetail = UserCocDetail::whereUserId($userId)->first();
                $userCoeDetail = UserCoeDetails::whereUserId($userId)->first();
                $userWkfrDetail = UserWkfrDetail::whereUserId($userId)->first();
                $userGmdssDetail = UserGmdssDetail::whereUserId($userId)->first();
                $userSemanBookDetail = UserSemanBookDetail::whereUserId($userId)->first();
                $userDceDetail = UserDangerousCargoEndorsementDetail::whereUserId($userId)->first();

                $show_resume = CommonHelper::getResumeNumber($result[0]['professional_detail']['current_rank']);
                $rank = array_flatten(\CommonHelper::new_rank());
                $country = \CommonHelper::countries();
                $shipData = \CommonHelper::ship_type();
                $shipType = UserSeaService::select('*')->whereUserId($userId)->groupBy('ship_type')->get();
                $pass = $userPassData->pass_number ? 'Passport' : null;
                $vissaPass = count($result[0]['visas']) > 0 ? 'Visa' : null;
                $indos = !empty($userWkfrDetail->indos_number) ? 'INDOS' : null;
                $coc = !empty($userCocDetail->coc_number) ? 'COC' : null;
                $coe = !empty($userCoeDetail->coe_number) ? 'COE' : null;
                $gmd = !empty($userGmdssDetail->gmdss_number) ? 'GMDSS' : null;
                $userDceDetail = !empty($userDceDetail) ? 'DCE' : null;
                $semanBook = !empty($userSemanBookDetail->cdc_number) ? 'CDC': null;
                $sailingCertificate = $userWkfrDetail['wk_cop'] && $userWkfrDetail['type'] ? 'Sailing Certificate': null;
                if($userWkfrDetail && $userWkfrDetail['wk_cop'] == 'wk') {
                    $sailingCertificate = 'WK'; 
                } else if($userWkfrDetail && $userWkfrDetail['wk_cop'] == 'cop') {
                    $sailingCertificate = 'COP'; 
                }
                $SID = !empty($userProData->sid_number) ? 'SID': null;
                $medical = ($userWkfrDetail['ilo_medical'] == 1 || $userWkfrDetail['typescreening_test'] == 1) ? 'MEDICAL': null;
                $vaccination = ($userWkfrDetail['yellow_fever'] == 1 || $userWkfrDetail['cholera'] == 1 || $userWkfrDetail['hepatitis_b'] == 1 || $userWkfrDetail['hepatitis_c'] == 1 || $userWkfrDetail['diphtheria'] == 1 || $userWkfrDetail['covid'] == 1) ? 'VACCINATION': null;
                $docsArray = [$pass,$vissaPass,$medical, $vaccination,$indos,$semanBook,$coc,$coe,$sailingCertificate,$gmd, $userDceDetail, $SID];
                $docsArray = array_filter($docsArray);
                $docsType = null;
                $percentageMb = 0;
                $usedMb = 0;
//            dd($result[0]);
                if(!empty($result[0]['used_kb'])){
                    $usedMb = $result[0]['used_kb'] / 1024;
                    $maxKb = $result[0]['max_kb'] / 1024;
                    $percentageMb = ($usedMb * 100) / $maxKb; 
                }
                if($request->ajax()){

                    $medicalArray = [];
                    if($result[0]['wkfr_detail']['ilo_medical'] == 1) {
                        array_push($medicalArray, [
                            'country' => $result[0]['wkfr_detail']['ilo_country'],
                            "type" => "ILO Medical",
                            "issue_on" => (isset($result[0]['wkfr_detail']['ilo_issue_date']) ? date('d-m-Y',strtotime($result[0]['wkfr_detail']['ilo_issue_date'])) : '-')
                        ]);
                    }
                    if($result[0]['wkfr_detail']['screening_test'] == 1) {
                        array_push($medicalArray, [
                            'country' => $result[0]['wkfr_detail']['screening_test_county'],
                            "type" => "Screening Test",
                            "issue_on" => (isset($result[0]['wkfr_detail']['screening_test_date']) ? date('d-m-Y',strtotime($result[0]['wkfr_detail']['screening_test_date'])) : '-')
                        ]);
                    }

                    
                    $vaccineArray = [];
                    if($result[0]['wkfr_detail']['yellow_fever'] == 1) {
                        array_push($vaccineArray, [
                            'country' => $result[0]['wkfr_detail']['yf_country'],
                            "type" => "Yellow Fever",
                            "issue_on" => (isset($result[0]['wkfr_detail']['yf_issue_date']) ? date('d-m-Y',strtotime($result[0]['wkfr_detail']['yf_issue_date'])) : '-')
                        ]);
                    }
                    if($result[0]['wkfr_detail']['cholera'] == 1) {
                        array_push($vaccineArray, [
                            'country' => $result[0]['wkfr_detail']['cholera_country'],
                            "type" => "Cholera",
                            "issue_on" => (isset($result[0]['wkfr_detail']['cholera_issue_date']) ? date('d-m-Y',strtotime($result[0]['wkfr_detail']['cholera_issue_date'])) : '-')
                        ]);
                    }

                    if($result[0]['wkfr_detail']['hepatitis_b'] == 1) {
                        array_push($vaccineArray, [
                            'country' => $result[0]['wkfr_detail']['hepatitis_b_country'],
                            "type" => "Hepatitis B",
                            "issue_on" => (isset($result[0]['wkfr_detail']['hepatitis_b_issue_date']) ? date('d-m-Y',strtotime($result[0]['wkfr_detail']['hepatitis_b_issue_date'])) : '-')
                        ]);
                    }
                    if($result[0]['wkfr_detail']['hepatitis_c'] == 1) {
                        array_push($vaccineArray, [
                            'country' => $result[0]['wkfr_detail']['hepatitis_c_country'],
                            "type" => "Hepatitis C",
                            "issue_on" => (isset($result[0]['wkfr_detail']['hepatitis_c_issue_date']) ? date('d-m-Y',strtotime($result[0]['wkfr_detail']['hepatitis_c_issue_date'])) : '-')
                        ]);
                    }
                    if($result[0]['wkfr_detail']['diphtheria'] == 1) {
                        array_push($vaccineArray, [
                            'country' => $result[0]['wkfr_detail']['diphtheria_country'],
                            "type" => "Diphtheria and tetanus",
                            "issue_on" => (isset($result[0]['wkfr_detail']['diphtheria_issue_date']) ? date('d-m-Y',strtotime($result[0]['wkfr_detail']['diphtheria_issue_date'])) : '-')
                        ]);
                    }
                    if($result[0]['wkfr_detail']['covid'] == 1) {
                        array_push($vaccineArray, [
                            'country' => $result[0]['wkfr_detail']['covid_country'],
                            "type" => "Covid19",
                            "issue_on" => (isset($result[0]['wkfr_detail']['covid_issue_date']) ? date('d-m-Y',strtotime($result[0]['wkfr_detail']['covid_issue_date'])) : '-')
                        ]);
                    }

                    $courseId = $request->courseId;
                    $shipTypeData = UserSeaService::whereUserId($userId)->orderBy('from', 'DESC')->orderBy('to', 'DESC');
                    $courseData = UserCoursesCertificate::whereUserId($userId);
                    if($request->shipId){
                        if($request->shipId != 'all'){
                            $shipTypeData = $shipTypeData->whereShipType($request->shipId);
                        }
                        $view = 'ship_type_data';
                        $type = 1;
                    }
                    if($request->courseId){
                        if($request->courseId != 'all'){
                            $courseData = $courseData->whereCourseId($request->courseId);
                        }

                        $view = 'courses_data';
                        $type = 2;
                    }
                    if($request->docsId || $request->docsId == '0') {
                        $view = 'document_data';
                        $type = 3;
                        $docsType = $request->docsId;
                    }
                    $shipTypeData = $shipTypeData->get();
                    $result[0]['sea_service_detail'] = $shipTypeData;
                    $courseData = $courseData->get();
                    $result[0]['course_detail'] = $courseData;
                    $data['type'] = $type;
                    $data['shipData'] = View::make('site.user.'.$view,[
                        'data' => $result,'usedMb'=>$usedMb,'userSemanBookDetail'=>$userSemanBookDetail,
                        'sailingCertificate'=>$sailingCertificate,'userCocDetail'=>$userCocDetail,
                        'coursesName'=>$courses,'docsType'=>$docsType,'courseData'=>$courseData,
                        'userWkfrDetail'=>$userWkfrDetail,'userCourses'=>$userCourses,
                        'country'=>$country,'shipData'=>$shipData,'userProData'=>$userProData,'courseId' => $courseId, 'vaccineArray' => $vaccineArray, 'medicalArray' => $medicalArray])->render();
                    return $data;
                }
                return view('site.user.share_profile2',['data' => $result,'percentageMb'=>$percentageMb,'usedMb'=>$usedMb,'user' => $user,'coursesName'=>$courses,'docsArray'=>$docsArray,'userCourses'=>$userCourses,'country'=>$country,'userPassData'=>$userPassData,'shipData'=>$shipData,'shipType'=>$shipType,'rank'=>$rank,'userPersonalData'=>$userPersonalData,'userProData'=>$userProData, 'all_courses' => $courses,'role' => $role,'user_documents' => $user_documents,'user_id' => $user_id, 'documents' => $uploaded_documents, 'show_resume' => $show_resume, 'share_user_id' => $request['profile'], 'pageTitle' => $pageTitle, 'metaDescription' => $metaDescription, 'metaKeywords' => $metaKeywords]);
            }else{
                return view('errors.404');
            }
            return view('site.user.share_profile2', ['pageTitle' => $pageTitle, 'metaDescription' => $metaDescription, 'metaKeywords' => $metaKeywords]);
        }   
    }

    public function getUserInfo(Request $request){
        $userId = Auth::user()->id;
        $userData = User::whereId($userId)->first();
        $userPersonalData = UserPersonalDetail::whereUserId($userId)->first();
        $userProData = UserProfessionalDetail::whereUserId($userId)->first();
        $userCocDetail = UserCocDetail::whereUserId($userId)->get();
        $userWkfrDetail = UserWkfrDetail::whereUserId($userId)->first();
        $mStatus = \CommonHelper::marital_status();
        $coverallSize = \CommonHelper::coverall_size();
        $safetyShoeSize = \CommonHelper::safety_shoe_size();
        $bloodType = \CommonHelper::blood_type();
        $languages = UserLanguage::where("user_id", $userId)->get()->toArray();

        $myLanguages = [];
        for($counter = 0; $counter < count($languages); $counter++) {
            $canSpeakRead = [];
            if($languages[$counter]['can_read'] == 1) {
                array_push($canSpeakRead, "Read");
            } 
            if($languages[$counter]['can_speak'] == 1) {
                array_push($canSpeakRead, "Speak");
            }

            array_push($myLanguages, "<span class='known-language'>" . $languages[$counter]['language'] ."</span> <span style='color:#aaaaaa;font-size:13px;'>" .  (implode(" | ", $canSpeakRead)) ."</span>");
        }
        $knownLanguages =  "";
        if(count($languages) > 0) {
            $knownLanguages = implode(', ', $myLanguages);
        }
        $rank = array_flatten(\CommonHelper::new_rank());
        $type = $request->user_type;
        $userLocationCity = '';
        $userLocationState = '';
        $userLocationPostalCode = '';
        $userLocationCountry = '';
        $pincodeData = [];
        $options['with'] = ['personal_detail.state','personal_detail.city','personal_detail.pincode.pincodes_states.state','personal_detail.pincode.pincodes_cities.city'];
        $UserLocationData = $this->userService->getDataByUserId($userId, $options)->toArray();
        if(isset($UserLocationData[0]['personal_detail']) && !empty($UserLocationData[0]['personal_detail'])){
            if(isset($UserLocationData[0]['personal_detail']['state']['name']) && !empty($UserLocationData[0]['personal_detail']['state']['name'])){
                $userLocationState = $UserLocationData[0]['personal_detail']['state']['name'];
            }
            if(isset($UserLocationData[0]['personal_detail']['city']['name']) && !empty($UserLocationData[0]['personal_detail']['city']['name'])){
                $userLocationCity = $UserLocationData[0]['personal_detail']['city']['name'];
            }
            if(isset($UserLocationData[0]['personal_detail']['pincode']['code']) && !empty($UserLocationData[0]['personal_detail']['pincode']['code'])){
                $userLocationPostalCode = $UserLocationData[0]['personal_detail']['pincode']['code'];
            }
            if(isset($UserLocationData[0]['personal_detail']['country']) && !empty($UserLocationData[0]['personal_detail']['country'])){
                $countryId = $UserLocationData[0]['personal_detail']['country'];
                $country = \CommonHelper::countries();
                if(isset($country[$countryId]) && !empty($country[$countryId])){
                    $userLocationCountry = $country[$countryId];
                }
            }
        }
        $data['userInfo'] = View::make('site.user.user_info',compact('userData', 'knownLanguages','safetyShoeSize', 'bloodType','coverallSize','userProData','rank','type','userPersonalData','userWkfrDetail','userCocDetail','mStatus', 'userLocationCity', 'userLocationState', 'userLocationPostalCode', 'userLocationCountry'))->render();
        return $data;
    }
    
    public function getShareUserInfo(Request $request){
        $userId = "";
        if(isset($request->share_user_id) && !empty($request->share_user_id)){
            $userId = $request->share_user_id;
        }
        $userData = User::whereId($userId)->first();
        $userPersonalData = UserPersonalDetail::whereUserId($userId)->first();
        $userProData = UserProfessionalDetail::whereUserId($userId)->first();
        $userCocDetail = UserCocDetail::whereUserId($userId)->first();
        $userWkfrDetail = UserWkfrDetail::whereUserId($userId)->first();
        $mStatus = \CommonHelper::marital_status();
        $coverallSize = \CommonHelper::coverall_size();
        $safetyShoeSize = \CommonHelper::safety_shoe_size();
        $rank = array_flatten(\CommonHelper::new_rank());
        $type = $request->user_type;

        $bloodType = \CommonHelper::blood_type();
        $languages = UserLanguage::where("user_id", $userId)->get()->toArray();

        $myLanguages = [];
        for($counter = 0; $counter < count($languages); $counter++) {
            $canSpeakRead = [];
            if($languages[$counter]['can_read'] == 1) {
                array_push($canSpeakRead, "Read");
            } 
            if($languages[$counter]['can_speak'] == 1) {
                array_push($canSpeakRead, "Speak");
            }

            array_push($myLanguages, "<span class='known-language'>" . $languages[$counter]['language'] ."</span> <span style='color:#aaaaaa;font-size:13px;'>" .  (implode(" | ", $canSpeakRead)) ."</span>");
        }
        $knownLanguages =  "";
        if(count($languages) > 0) {
            $knownLanguages = implode(', ', $myLanguages);
        }

        $userLocationCity = '';
        $userLocationState = '';
        $userLocationPostalCode = '';
        $userLocationCountry = '';
        $pincodeData = [];
        $options['with'] = ['personal_detail.state','personal_detail.city','personal_detail.pincode.pincodes_states.state','personal_detail.pincode.pincodes_cities.city'];
        $UserLocationData = $this->userService->getDataByUserId($userId, $options)->toArray();
        if(isset($UserLocationData[0]['personal_detail']) && !empty($UserLocationData[0]['personal_detail'])){
            if(isset($UserLocationData[0]['personal_detail']['state']['name']) && !empty($UserLocationData[0]['personal_detail']['state']['name'])){
                $userLocationState = $UserLocationData[0]['personal_detail']['state']['name'];
            }
            if(isset($UserLocationData[0]['personal_detail']['city']['name']) && !empty($UserLocationData[0]['personal_detail']['city']['name'])){
                $userLocationCity = $UserLocationData[0]['personal_detail']['city']['name'];
            }
            if(isset($UserLocationData[0]['personal_detail']['pincode']['code']) && !empty($UserLocationData[0]['personal_detail']['pincode']['code'])){
                $userLocationPostalCode = $UserLocationData[0]['personal_detail']['pincode']['code'];
            }
            if(isset($UserLocationData[0]['personal_detail']['country']) && !empty($UserLocationData[0]['personal_detail']['country'])){
                $countryId = $UserLocationData[0]['personal_detail']['country'];
                $country = \CommonHelper::countries();
                if(isset($country[$countryId]) && !empty($country[$countryId])){
                    $userLocationCountry = $country[$countryId];
                }
            }
        }
        $data['userInfo'] = View::make('site.user.user_info',compact('userData','safetyShoeSize','coverallSize','userProData','rank','type','userPersonalData','userWkfrDetail','userCocDetail','mStatus', 'userLocationCity', 'userLocationState', 'userLocationPostalCode', 'userLocationCountry','bloodType','knownLanguages'))->render();
        return $data;
    }
    
    public function doNotShowAgain(Request $request) {
        if ($request->ajax()) {
            if (Auth::check()) {
                $userId = Auth::user()->id;
                $userData = array('is_do_not_show' => 1);
                $userData = User::whereId($userId)->update($userData);
                return response()->json(['status' => 'success' , 'message' => 'Data update successfully'],200);
            }
        }
    }
    
    public function testEmail() {
        $userData = array();
        $userData['email'] = 'balkrishnajatin@gmail.com';
        $blade_file = 'emails.testEmal';
        $title = 'FLANKNOT: Test Template';
        $subject = 'FLANKNOT: Test Template';
        $message = 'Test';
        $data = $userData;
        $check = $this->sendEmail($blade_file, $data, $userData, $title, $subject, null);
        dd($check);
    }

}
