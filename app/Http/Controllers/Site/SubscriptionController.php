<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Services\SubscriptionService;
use App\Services\CartProductService;
use App\Services\CompanyService;
use App\Services\InstituteService;
use App\Services\UserService;
use Auth;

use Illuminate\Support\Facades\Artisan;

class SubscriptionController extends Controller
{
    private $subscriptionService;
    private $CartProductService;
    private $companyService;
    private $instituteService;
    private $userService;

    public function __construct() {
        $this->subscriptionService = new SubscriptionService();
        $this->CartProductService = new CartProductService();
        $this->companyService = new CompanyService();
        $this->instituteService = new InstituteService();
        $this->userService = new UserService();
    }

     public function subscription1() {
        if(Auth::check()) {
            $renewable_subscription_id = '';

            $user_data = Auth::User()->toArray();
            $user_role = $user_data['registered_as'];

            $subscription = $this->subscriptionService->getAllSubscriptionDetails($user_role)->toArray();

            return view('site.subscription.subscription1', ['subscriptions' => $subscription, 'free_trial' => false, 'renewable_subscription_id' => $renewable_subscription_id]);
        }
    }

    public function activateFreeSubscription(Request $request){
        if(Auth::check()){
            $data = $request->toArray();
            $user_id = Auth::User()->id;
            $user_role = Auth::User()->registered_as;
            $added_subscription = false;

            if ($user_role == 'admin') {
                $user_id = $request['user_id'];
                $user = $this->userService->getDataByUserId($user_id);
                $user_role = $user->registered_as;
            } 

            $my_subscription_all = $this->userService->mySubscriptionAllType($user_id, $user_role);
            $my_subscription = $my_subscription_all['my'];

            if (empty($my_subscription['active']) && empty($my_subscription['expired']) && empty($my_subscription['up_coming'])) {
                $added_subscription = $this->userService->saveAnyFreeSubscription($user_id,$user_role);
            }
                



            // $subscription_data['user_id'] = $user_id;
            // $subscription_data['subscription_id'] = $data['subscription_id'];
            // $subscription_data['status'] = '1';

            // $subscribed_plan_details = $this->subscriptionService->getAllSubscriptionDetailsBySubscriptionId($data['subscription_id']);

            // if($subscribed_plan_details)
            //     $subscribed_plan = $subscribed_plan_details->toArray();

            // $subscription_data['duration'] = $subscribed_plan[0]['duration'];
            // $subscription_data['valid_from'] = date('Y-m-d');
            // $subscription_data['valid_to'] = date('Y-m-d', strtotime('+'.$subscribed_plan[0]['duration'].' months'));

            // $subscribed_data = $this->subscriptionService->storeSubscription($subscription_data);
            if($added_subscription){
                return response()->json(['status' => 'success' , 'message' => 'Your free subscription has been activated.'],200);
            }else{
                return response()->json(['status' => 'failed' , 'message' => 'Your are already subscribed.'],400);
            }
        }else{
            return redirect()->route('site.login');
        }
    }

    public function subscriptionList(){
        if(Auth::check()){
            $user_data = Auth::User()->toArray();
            $user_id = Auth::User()->toArray();
            $user_role = $user_data['registered_as'];

            $my_subscription_all = $this->userService->mySubscriptionAllType($user_id, $user_role);
            $my_subscription = $my_subscription_all['my'];
            $data = $my_subscription_all['all'];

            // $subscription = $this->subscriptionService->getSubscriptionsByUserId($user_id);
            // $subscription_data = [];
            // if (!empty($subscription)) {
            //     $subscription_data = $subscription->toArray();
            // }

            return view('site.subscription.mysubscription',['data' => $data,'my_subscription'=>$my_subscription]);
        }
    }

    public function subscription() {

        if(Auth::check() && Auth::User()->registered_as != 'admin') {

            $renewable_subscription_id = '';
            $user_data = Auth::User()->toArray();
            $user_id = Auth::User()->toArray();
            $user_role = $user_data['registered_as'];

            $my_subscription_all = $this->userService->mySubscriptionAllType($user_id, $user_role);
            $my_subscription = $my_subscription_all['my'];

            $subscription = $this->subscriptionService->getAllActiveSubscription($user_role)->toArray();

            return view('site.subscription.subscription', ['subscriptions' => $subscription,'my_subscription'=>$my_subscription]);
        } else {
            $subscription = $this->subscriptionService->getAllActiveSubscription()->toArray();
            $all_subscription = [];
            foreach ($subscription as $key => $sub_value) {
                if ($sub_value['type'] == 'company') {
                    $all_subscription['company'][$key] = $sub_value;
                } elseif ($sub_value['type'] == 'institute') {
                    $all_subscription['institute'][$key] = $sub_value;
                } elseif ($sub_value['type'] == 'advertiser') {
                    $all_subscription['advertiser'][$key] = $sub_value;
                } elseif ($sub_value['type'] == 'seafarer') {
                    $all_subscription['seafarer'][$key] = $sub_value;
                }
            }
            return view('site.subscription.subscription', ['subscriptions' => $all_subscription]);
        }
    }
}