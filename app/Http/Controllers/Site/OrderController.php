<?php

namespace App\Http\Controllers\Site;

use App\Services\OrderService;
use App\Services\OrderPaymentService;
use App\Services\SubscriptionService;
use App\Services\CompanyService;
use App\Services\InstituteService;
use App\Services\UserService;
use Auth;
use Illuminate\Http\Request;
class OrderController
{
    private $orderService;
    private $orderPaymentService;
    private $instituteService;
    private $userService;

    function __construct()
    {
        $this->orderService = new OrderService();
        $this->orderPaymentService = new OrderPaymentService();
        $this->subscriptionService = new SubscriptionService();
        $this->companyService = new CompanyService();
        $this->instituteService = new InstituteService();
        $this->userService = new UserService();
    }

    public function proceedToPayment(Request $request){
        if(Auth::check()){
            $user = Auth::user();
            $role = $user->registered_as;
            $data['user_id'] = $user->id;
            $data['subscription_id'] = $request['subscription_id'];
            $data['subscription_date'] = $request['subscription_date'];
            if ($data['subscription_date'] != 'now') {
                $data['subscription_date'] = 'after';
            }
            $data['role'] = $role;

            // check present and upcoming subscription if any
            $my_subscription_all = $this->userService->mySubscriptionAllType($user->id, $role);
            $my_subscription = $my_subscription_all['my'];

            if (isset($my_subscription['active']['subscription_details']['duration_title']) && strtolower($my_subscription['active']['subscription_details']['duration_title']) == 'free') {
                $data['subscription_date'] = 'now';
            }

            if (!empty($my_subscription['up_coming'])) {
                return response()->json(['status' => 'failed','message'=>'You already have 1 active & 1 upcoming subscription, try later when there is no upcoming subscription'],400);
            }

            $result = $this->orderService->proceedToPayment($data);
            if($result['status'] == 'success'){
                //payumoney integration.
                return response()->json(['status' => 'success' , 'payumoney_form' => $result['payumoney_form']],200);
            }else{
                return response()->json(['status' => 'failed'],400);
            }
        }else{
            return redirect()->route('site.login');
        }
    }

    public function paymentGatewayResponse(Request $request, $order_id, $subscription_date){
        $array = [
            'status' => $request['status'],
            'firstname' => $request['firstname'],
            'lastname' => $request['lastname'],
            'amount' => $request['amount'],
            'txnid' => $request['txnid'],
            'posted_hash' => $request['hash'],
            'key' => $request['key'],
            'productinfo' => $request['productinfo'],
            'email' => $request['email'],
            'phone' => $request['phone'],
            'mode' => $request['mode'],
            'transaction_id' => $request['mihpayid'],
            'salt' => env("PAYUMONEY_SALT"),
            'error' => $request['error'],
            'message' => $request['error_Message'],
            'bank_ref_num' => $request['bank_ref_num'],
            'unmappedstatus' => $request['unmappedstatus']
          ];
        
        $response = $this->orderService->orderPaymentGatewayResponse($array,$order_id);

        if($request['status'] == 'success'){
            $user_obj = Auth::User();
            $user_id = $user_obj->id;
            $role = $user_obj->registered_as;

            $order = $this->orderService->getOrderDetailById($order_id)->toArray();

            $subscription = $this->subscriptionService->getAllSubscriptionDetailsBySubscriptionId($order[0]['subscription_id'])->toArray();
            $subscription_json = json_encode($subscription[0]);

            $my_subscription_all = $this->userService->mySubscriptionAllType($user_id, $role);
            $my_subscription = $my_subscription_all['my'];

            $new_sub_data = [];
            $new_sub_data['order_id'] = $order_id;
            $new_sub_data['subscription_details'] = $subscription_json;
            $new_sub_data['status'] = 1;
            if ($subscription_date == 'after' && !empty($my_subscription['active'])) {
                $new_sub_data['valid_from'] = date('Y-m-d 00:00:00', strtotime('+1 day', strtotime($my_subscription['active']['valid_to'])));
                $new_sub_data['valid_to'] = date('Y-m-d 23:59:59', strtotime('+'.(($subscription[0]['duration'] * 30) -1 ).' days', strtotime($new_sub_data['valid_from'])));
            } elseif ($subscription_date == 'now' && !empty($my_subscription['active'])) {
                $new_sub_data['valid_from'] = date('Y-m-d 00:00:00');
                $new_sub_data['valid_to'] = date('Y-m-d 23:59:59', strtotime('+'.(($subscription[0]['duration'] * 30) -1 ).' days'));
                // change previous status to 2(half stop)
                $str_arr = [];
                $str_arr['id'] = $my_subscription['active']['id'];
                $str_arr['status'] = 2;
                $ch_status = $this->subscriptionService->saveEditAnySubscription($str_arr,$role);
            } else {
                $new_sub_data['valid_from'] = date('Y-m-d 00:00:00');
                $new_sub_data['valid_to'] = date('Y-m-d 23:59:59', strtotime('+'.(($subscription[0]['duration'] * 30) -1 ).' days'));
            }

            // extra column as per role

            if ($role == 'company') {
                $company = $this->companyService->getDetailsByCompanyId($user_id)->toArray();
                $new_sub_data['company_reg_id'] = $company[0]['id'];
            } elseif ($role == 'institute') {
                $institute = $this->instituteService->getDetailsByCompanyId($user_id)->toArray();
                $new_sub_data['institute_reg_id'] = $institute[0]['id'];
            } elseif ($role == 'advertiser') {
                $advertiser = $this->companyService->getDetailsByCompanyId($user_id)->toArray();
                $new_sub_data['company_reg_id'] = $advertiser[0]['id'];
            } elseif ($role == 'seafarer') {
                $new_sub_data['user_id'] = $user_id;
            }

            $added_subscription = $this->subscriptionService->saveEditAnySubscription($new_sub_data,$role);

            // $active_subscriptions = $this->subscriptionService->getSubscriptionsByUserIdWithActiveSubscription($user_id)->toArray();

            // $options['with'] = ['order_payment','user_subscription_detail'];
            // $order_details = $this->orderService->getOrderDetailById($order_id,$options)->toArray();
            
            // if(count($active_subscriptions) > 0 && empty($active_subscriptions[0]['valid_from']) && empty($active_subscriptions[0]['order_id'])){
            //     $this->subscriptionService->deleteSubscriptionById($active_subscriptions[0]['id']);
            //     $new_subscriptions['valid_from'] = date('Y-m-d h:i:s');
            //     $new_subscriptions['valid_to'] = date('Y-m-d h:i:s', strtotime('+'.$order_details[0]['user_subscription_detail']['duration'].' months'));
            //     $new_subscriptions['status'] = 1;
            // } 
            // else if(count($active_subscriptions) > 0){
            //    $new_subscriptions['status'] = 3;
            // }else{
            //     $new_subscriptions['valid_from'] = date('Y-m-d h:i:s');
            //     $new_subscriptions['valid_to'] = date('Y-m-d h:i:s', strtotime('+'.$order_details[0]['user_subscription_detail']['duration'].' months'));
            //     $new_subscriptions['status'] = 1;
            // }
            // $this->subscriptionService->updateSubscriptionByOrderId($new_subscriptions, $order_details[0]['user_subscription_detail']['order_id']);
        } 

        //echo "Thank You";
        return redirect()->route("site.view.order",$order_id);
    }

    public function viewOrder($order_id){
        if (Auth::check()) {

        $role = Auth::user()->registered_as;
        $options['with'] = ['order_payment','company_subscription','institute_subscription','advertiser_subscription','seafarer_subscription','subscription'];
        $order_details = $this->orderService->getOrderDetailById($order_id,$options)->toArray();
            if ($order_details[0]['user_id'] == Auth::User()->id || Auth::User()->registered_as == 'admin') {
                
                if($order_details[0]['order_payment']['message']){
                    $order_details[0]['order_payment']['message'] = json_decode($order_details[0]['order_payment']['message'], true);
                }
                
                if ($order_details[0]['company_subscription']['subscription_details']) {
                    $order_details[0]['company_subscription']['subscription_details'] = json_decode($order_details[0]['company_subscription']['subscription_details'],true);
                    $order_details[0]['my_subscription'] = $order_details[0]['company_subscription'];
                } elseif ($order_details[0]['institute_subscription']['subscription_details']) {
                   $order_details[0]['institute_subscription']['subscription_details'] = json_decode($order_details[0]['institute_subscription']['subscription_details'],true);
                    $order_details[0]['my_subscription'] = $order_details[0]['institute_subscription'];
                } elseif ($order_details[0]['advertiser_subscription']['subscription_details']) {
                   $order_details[0]['advertiser_subscription']['subscription_details'] = json_decode($order_details[0]['advertiser_subscription']['subscription_details'],true);
                    $order_details[0]['my_subscription'] = $order_details[0]['advertiser_subscription'];
                } elseif ($order_details[0]['seafarer_subscription']['subscription_details']) {
                   $order_details[0]['seafarer_subscription']['subscription_details'] = json_decode($order_details[0]['seafarer_subscription']['subscription_details'],true);
                    $order_details[0]['my_subscription'] = $order_details[0]['seafarer_subscription'];
                }
                return view('site.order.order_status',['order_details' => $order_details]);

            } else {
                return redirect()->back()->with(['message' => 'order not found']);
            }

        } else {
            return redirect()->route('site.login');
        }
    }

    //Payment type values are 1 and 2.
    //1 for initial payment
    //2 for pending payment
    public function viewBatchOrder($order_id,$payment_type=NULL){
        if (Auth::check()) {
            $role = Auth::user()->registered_as;
            $options['with'] = ['order_payments','batch_details.course_details.courses','batch_details.course_details.institute_details','batch_details.course_details.institute_registration_detail'];
            $order_details = $this->orderService->getOrderDetailById($order_id,$options)->toArray();
            //dd($order_details);
            if (!empty($order_details) && ($order_details[0]['user_id'] == Auth::User()->id || Auth::User()->registered_as == 'admin')) {
                
                if($order_details[0]['payment_type'] == 'split'){
                    if(isset($payment_type) && !empty($payment_type)){

                    }else{
                        $payment_type = 1;
                    }
                }

                if($order_details[0]['order_payments'][0]['message']){
                    $order_details[0]['order_payments']['message'][0] = json_decode($order_details[0]['order_payments'][0]['message'], true);
                }
                
                //dd($order_details[0]);
                return view('site.order.batch_order_status',['order_details' => $order_details,'payment_type' => $payment_type]);

            } else {
                return redirect()->back()->with(['message' => 'order not found']);
            }

        } else {
            return redirect()->route('site.login');
        }
    }
}