<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Services\JobService;
use App\Services\UserService;
use App\Services\CompanyService;
use Auth;
use Route;
use View;
use Carbon\Carbon;
use App\ShareHistory;
use CommonHelper;

class JobController extends Controller
{
    private $jobService;
    private $userService;

    function __construct() {
        $this->jobService = new JobService();
        $this->userService = new UserService();
        $this->companyService = new CompanyService();
    }

    public function addJob(){

        if(Auth::check()){
            $user = Auth::User()->toArray();
        }else{
            return redirect()->route('home');
        }
        $ships = [];
        $logged_company_data = $this->companyService->getDataByUserId($user['id'])->toArray();

        $ship_data = $this->companyService->getCompanyShipDetailsById($logged_company_data[0]['id'])->toArray();
        
        if(isset($ship_data[0]['ship_type']) && !empty($ship_data[0]['ship_type'])){
            foreach ($ship_data[0]['ship_type'] as $key => $value) {
                $ships[] = $value['ship_type'];
            }
        }
        if($user['status'] == '1'){
            return view('site.company.company_add_jobs',['ship_data' => $ship_data,'ships' => $ships]);
        }else{
            return view('site.company.company_add_jobs',['not_activated' => '1']);
        }
    }

    public function dataByCompanyShipType($ship_type){
        if(Auth::check()){
            $user = Auth::User()->toArray();
            $logged_company_data = $this->companyService->getDataByUserId($user['id'])->toArray();

            $ship_data = $this->companyService->getCompanyShipDetailsById($logged_company_data[0]['id'])->toArray();
            if(isset($ship_data[0]['ship_type']) && !empty($ship_data[0]['ship_type'])){
                foreach ($ship_data[0]['ship_type'] as $key => $value) {
                    if($value['ship_type'] == $ship_type){
                        $ship_details = $value; 
                    }
                }
            }
            
            if(isset($ship_details) && !empty($ship_details)){
                return response()->json(['status' => 'success', 'message' => 'ship Details found.','ship_details' => $ship_details], 200);
            }else{
                return response()->json(['status' => 'failed', 'message' => 'ship Details not found.'], 400);
            }
        }
    }

    public function editJob($job_id){
        if(isset($job_id) && !empty($job_id)){

            $job_data = $this->jobService->getJobDetailsByJobId($job_id)->toArray();
            $name = Route::currentRouteName();
            $job_data[0]['current_route'] = $name;
            $job_data[0]['job_id'] = $job_id;

            if(Auth::check()){
                $user = Auth::User()->toArray();
                $logged_company_data = $this->companyService->getDataByUserId($user['id'])->toArray();
            }

            $ship_data = $this->companyService->getCompanyShipDetailsById($logged_company_data[0]['id'])->toArray();
            
            if(isset($ship_data[0]['ship_type']) && !empty($ship_data[0]['ship_type'])){
                foreach ($ship_data[0]['ship_type'] as $key => $value) {
                    $ships[] = $value['ship_type'];
                }
            }

            return view('site.company.company_add_jobs' , ['job_data' => $job_data,'ship_data' => $ship_data,'ships' => $ships]);
        }
    }

    public function updateJob(Request $request){
        $job_data = $request->toArray();

        unset($job_data['_token']);
        
        $existing_job_data = $this->jobService->getJobDetailsByJobId($job_data['job_id'])->toArray();
        
        if(Auth::check()) {
            $user = Auth::User()->toArray();
            $logged_company_data = $this->companyService->getDataByUserId($user['id'])->toArray();

            if($existing_job_data[0]['company_id'] == $logged_company_data[0]['id']){
                $result = $this->jobService->update($job_data);
            }else{
                return response()->json(['status' => 'error', 'message' => 'Job is not belongs to this company.'], 400);
            }
        }

        if($result['status'] == 'success') {
            return response()->json(['status' => 'success', 'message' => 'Job Details has been updated.','redirect_url' => route('site.company.list.jobs')], 200);
        } else {
            return response()->json(['status' => 'error', 'message' => 'There was some error while adding job.'], 400);
        }
    }

    public function store(Request $request){
        $job_data = $request->toArray();

        if(Auth::check()){
            $user = Auth::User()->toArray();
            $logged_company_data = $this->companyService->getDataByUserId($user['id'])->toArray();

            $job_data['company_id'] = $logged_company_data[0]['id'];
            $result = $this->jobService->store($job_data);
        }

        if($result['status'] == 'success') {
            return response()->json(['status' => 'success', 'message' => 'New job added.','redirect_url' => route('site.company.list.jobs')], 200);
        } else {
            return response()->json(['status' => 'error', 'message' => 'There was some error while adding job.'], 400);
        }
    }

    public function jobListing(){
        if(Auth::check()){
            $user = Auth::User()->toArray();
            $logged_company_data = $this->companyService->getDataByUserId($user['id'])->toArray();
            
            $data = $this->jobService->getJobDetailsByCompanyIdByPaginate($logged_company_data[0]['id']);
            $result = $data->toArray();

            return view('site.company.company_job_listing', ['data' => $result['data'],'paginate' => $data,'pagination_data'=>$result]);
        }else{
            return redirect()->route('home');
        }

    }

    public function disableJob($job_id){
        $result = $this->jobService->disableJobByJobId($job_id);

        if($result['status'] == 'success') {
            return response()->json(['status' => 'success', 'message' => 'Job has been disabled.','redirect_url' => route('home')], 200);
        } else {
            return response()->json(['status' => 'error', 'message' => 'There was some error while disabling job.'], 400);
        }

    }

    public function enableJob($job_id){
        $result = $this->jobService->enableJobByJobId($job_id);

        if($result['status'] == 'success') {
            return response()->json(['status' => 'success', 'message' => 'Job has been enabled.','redirect_url' => route('home')], 200);
        } else {
            return response()->json(['status' => 'error', 'message' => 'There was some error while disabling job.'], 400);
        }

    }

    public function seafarer_job_search(Request $request){

        $user_id = '';

        $search_data = $request->toArray();
        
        if(isset($search_data) && isset($search_data['param'])){

            if(Auth::check()){
                $search_data = base64_decode($search_data['param']);
                $search_data = parse_url($search_data);
                parse_str($search_data['path'], $search_data);  
            }else{
                $current_url = $request->fullUrl();
                return redirect()->route('site.login',['redirect_url' => $current_url]);
            }
            //echo $query['email'];
        }
        $paginate = env('PAGINATE_LIMIT',10);
        $options['with_condition'] = ['user_applied_jobs'];
        $options['with'] = ['company_registration_details.company_detail','company_registration_details','company_registration_details.user_details','job_nationality'];
        $role= '';
        
        if(Auth::User()){
            $user_id = Auth::User()->id;
            $role = Auth::User()->registered_as;
        }

        $data = $this->jobService->getAllJobDetails($search_data,$paginate,$options,$user_id);
        
        $job_data = $data->toArray();
        
        $job_search = View::make("site.partials.seafarer_job_search")->with(['data' => $job_data['data'] , 'search_data' => $search_data, 'pagination_data' => $job_data , 'pagination' => $data,'role' => $role]);
        $template = $job_search->render();

        if($request->isXmlHttpRequest()){
            return response()->json(['template' => $template], 200);
        }else{
            return view('site.user.job_search',['data' => $job_data['data'] , 'search_data' => $search_data, 'pagination_data' => $job_data , 'pagination' => $data,'role' => $role,'user_id' => $user_id]);
        }
        
    }

    public function seafarerJobApply(Request $request){
        if(Auth::check()){
            $user_id = Auth::User()->id;
           
            if(isset($request->job_id)){
                $job_application['job_id'] = $request->job_id;
                $job_application['company_id'] = $request->company_id;
                $job_application['user_id'] = $user_id;

                $result = $this->jobService->applyForJob($job_application);
                if($result['status'] == 'success') {
                    if(isset($result['job_apply'])){
                        return response()->json(['status' => 'error', 'message' => 'You already applied for this job.'], 400);
                    }
                    return response()->json(['status' => 'success', 'message' => 'Job applied successfully.'], 200);
                } else {
                    return response()->json(['status' => 'error', 'message' => 'There was some error while applying job.'], 400);
                }
            }else{
                return response()->json(['status' => 'error', 'message' => 'Error while applying for job.'], 400); 
            }
        }else{
           return response()->json(['status' => 'error', 'message' => 'Please login to continue.', 'redirect' => route('site.login')], 400); 
        }
    }

    public function jobApplicantListing(Request $request){
        if(Auth::check()){
            $user = Auth::User()->id;
            $logged_company_data = $this->companyService->getDataByUserId($user)->toArray();
            
            $company_id = $logged_company_data[0]['id'];

            $paginate = env('PAGINATE_LIMIT',10);
            $options['with'] = ['user.professional_detail','company_details','company_job'];

            $data = $this->jobService->getAllUserAppliedJobs($company_id, $options, $paginate);

            $job_applicant_data = $data->toArray();

            return view('site.company.job_applicant_listing',['data' => $job_applicant_data['data'] , 'search_data' => '', 'pagination_data' => $job_applicant_data    , 'pagination' => $data]);

        }else{
           return response()->json(['status' => 'error', 'message' => 'Please login to continue.'], 400); 
        }
    }

    public function shipNameByCompanyShipType($ship_type_id,$company_id=null){
        if(Auth::check()){
            
            if(isset($company_id) && !empty($company_id)){
                $company_id = $company_id;
            }else{
                $company_id = Auth::User()->id;
            }
            
            $ship_data = $this->companyService->getCompanyShipsByShipTypeId($company_id,$ship_type_id)->toArray();
            
            if(count($ship_data) > 0){
                return response()->json(['status' => 'success', 'result' => $ship_data], 200);
            }else{
                return response()->json(['status' => 'error', 'message' => 'No Results Found'], 400);
            }
        }
    } 

    public function dataByCompanyShipId($ship_id){
        if(Auth::check()){
            $user = Auth::User()->toArray();
            $ship_data = $this->companyService->dataByCompanyShipId($ship_id)->toArray();

            /*if(isset($ship_data[0]['ship_type']) && !empty($ship_data[0]['ship_type'])){
                foreach ($ship_data[0] as $key => $value) {
                    if($value['ship_type'] == $ship_type){
                        $ship_details = $value; 
                    }
                }
            }*/
            
            if(isset($ship_data) && !empty($ship_data)){
                return response()->json(['status' => 'success', 'message' => 'ship Details found.','ship_details' => $ship_data[0]], 200);
            }else{
                return response()->json(['status' => 'failed', 'message' => 'ship Details not found.'], 400);
            }
        }
    }

    public function getNewJobs(){

        if (Auth::check()) {
            $pageTitle = "Job Notification – Apply to Seagoing Jobs | Flanknot";
            $metaDescription = "At Flanknot, Seafarer Users receives accurate and targeted Job Notification based on the Seafarer Users Rank, Nationality, Sailing Certificate, documents and Sailing Certificate.
                All Flanknot Seafarer User has an option to apply to the Job application received.";
            $metaKeywords = "Job notification, Apply Jobs, Not Interested in Job, Source of Job, Company name, company email, company contact numbers, Rank, Job Requirement";
            $authDetail = Auth::user();
            $options['with'] = ['seaman_book_detail'];
            $seamanBookDetail = $this->userService->getDataByUserId($authDetail->id, $options)->toArray();
            $newUnreadJobCount = \App\SendedJob::where('user_id',$authDetail->id)
                                ->where('status',2)->update(['status'=>1]);

            $getAllJob = \App\SendedJob::with(
                                        [
                                            'courseJob',
                                            'courseJob.company',
                                            'courseJob.companyEmailAddress',
                                            'courseJob.jobStatus',
                                            'courseJob.jobMultiplePassportCountry',
                                            'user',
                                        ])
                                        ->where('user_id',$authDetail->id)
                                        ->whereHas('courseJob',function($query){
                                            
                                            $query->where('valid_till_date','>=',Carbon::now());
                                        })
                                        ->has('user')
                                        ->orderBy('id','DESC')
                                        ->paginate('10');
                                        // dd($getAllJob);
                                        
            return view('site.job_section.job._get_new_job', compact('authDetail','getAllJob', 'seamanBookDetail', 'pageTitle', 'metaDescription', 'metaKeywords'));

        }else{

            return redirect()->route('home');            
        }        
    }

    public function appliedJob(Request $request){

        $input = $request->all();

        $checkSendJob = \App\SendedJob::with(['courseJob','courseJob.companyEmailAddress'])
                                        ->where('id',\Crypt::decrypt($input['send_job_id']))->firstOrFail();
        
        if ($checkSendJob->applied == 2) {
            
            $checkSendJob->applied = 1;
            $checkSendJob->applied_date = Carbon::now();
            $checkSendJob->save();   

            $data = Auth::user();

            $shareHistory = new ShareHistory();
            $shareHistory->roll_type = 4; // Job
            $shareHistory->sended_job_id = $checkSendJob->id; 
            $shareHistory->course_job_id = $checkSendJob->course_job_id; 
            $shareHistory->user_id = $data->id;
            $shareHistory->is_allow_qr = 1;
            $shareHistory->email = $checkSendJob->courseJob->companyEmailAddress->email;
            $shareHistory->save();
            $shareHistoryId = $shareHistory->id;


            $shareHistoryIdKey = CommonHelper::encodeKey($shareHistoryId);

            // 'share-pdf?resume='.$shareHistoryIdKey

            // if (env('ENABLE_MAIL') == true) {
                
                $result = $this->userService->sendUserJobAppliedMailToCompany($data,$checkSendJob->course_job_id,$shareHistoryIdKey);
            // }

            $msg = "You have successfully applied for job.";
            flashMessage('success',$msg);

        }else{

            $msg = "You have already applied for this job.";
            flashMessage('danger',$msg);
        }

        return response()->json(['success' => true,'msg'=>$msg, 'status'=>1]);
      
    }   
}
