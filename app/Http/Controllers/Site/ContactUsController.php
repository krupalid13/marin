<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Services\ContactUsService;

class ContactUsController extends Controller
{
    private $contactUsService;

    function __construct() {
        $this->contactUsService = new ContactUsService();
    }

    public function store(Request $request){
        $contact_us_data = $request->toArray();
        $data = $this->contactUsService->store($contact_us_data);
        if($data['id'] != ''){
            return response()->json(['status' => 'success' , 'message' => 'Thank you for your enquiry. We will contact you soon.'],200);
        }else{
            return response()->json(['status' => 'failed' , 'message' => 'Error.'],400);
        }
    }
}
