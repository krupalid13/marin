<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\UserService;
use App\UserProfessionalDetail;
use App\User;
use App\Services\SendEmailService;
use Auth;

class LoginController extends Controller
{

	private $userService;

	function __construct(){
        $this->userService = new UserService();
        $this->SendEmailService = new SendEmailService();
    }

    public function login(Request $request)
    {
       
     $data = $request->toArray();
        
        //$result = $this->userService->checkUserLogin($login_data);
        $og_redirect_url = '';
        $redirect_url = '';
        if(isset($data['redirect_url']) && !empty($data['redirect_url']))
        {
            $data['redirect_url'] = str_replace('|', '&', $data['redirect_url']);
            $url = explode('?',$data['redirect_url']);
            $params = explode('&',$url[1]);
            $user_id_params = explode('=',$params[0],2);

            $data1 = base64_decode($user_id_params[1]);

            $data1 = parse_url($data1);
            parse_str($data1['path'], $data1);

            if(isset($data1) && !empty($data1['user_id'])){
                $url_user_id = $data1['user_id'];
            }
            $og_redirect_url = $data['redirect_url'];
        }
        
        if(isset($data['remember']) && $data['remember'] == 'on')
        {
            $data['remember'] = true;
        }else{
            $data['remember'] = false;
        }
        
        if(Auth::attempt(['email' => $data['email'], 'password' => $data['password'],'is_email_verified'=>1], $data['remember'])) 
        {
            // Authentication passed...
            $user_id = Auth::User()->id;
            $role = Auth::User()->registered_as;
            
            if(isset($role) && !empty($role) && $role == 'seafarer')
            {
                $profession_details = UserProfessionalDetail::find($user_id);

                if(isset($profession_details) && !empty($profession_details)){
                    $profession_details = $profession_details->toArray();

                    if(isset($profession_details['availability']) && !empty($profession_details['availability'])){
                        $now = time(); // or your date as well
                        $your_date = strtotime($profession_details['availability']);
                        $datediff = $now - $your_date;
                        $day_diff = floor($datediff / (60 * 60 * 24));
                        if($day_diff >= 15 && $your_date < $now){
                            $redirect_url = route('home',['availability' => '1']);
                        }else{
                            $redirect_url = route('user.profile');
                        }
                    }
                    else{
                        $redirect_url = route('user.profile');
                    }
                }
                else{
                        $redirect_url = route('user.profile');
                    }
            }else{
                $redirect_url = route('user.profile');
            }
                
            $this->userService->storeLastLoggedOndate($user_id);
            $result['status'] = 'success';
        }else{
            $result['status'] = 'Failed';
        }

        if($request->isXmlHttpRequest()){
	        if($result['status'] == 'success') {
	            return response()->json(['status' => 'success', 'message' => 'successfully login.','redirect_url' => $redirect_url], 200);
	        } else {
                $user = User::where('email',$data['email'])->where('is_email_verified',0)->first();
                if($user){
                    $this->SendEmailService->sendVerificationEmailToUser($user);
                    return response()->json(['status' => 'error', 'message' => 'Your email address is not verified.Please verify email address to continue using app'], 400);
                }
	            return response()->json(['status' => 'error', 'message' => 'Invalid credentials'], 400);
	        }
	    }else{
	    	if($result['status'] == 'success') {
                if(!empty($og_redirect_url)){
                    if(isset($url_user_id) && !empty($url_user_id)){
                        if($user_id == $url_user_id){
                            $redirect_url = $og_redirect_url;
                        }
                        else{
                           return redirect()->route('home', ['job_apply' => 'another_user']);
                        }
                    }else{
                        $redirect_url = $og_redirect_url;
                    }
                    
                }
                
	            return redirect()->intended($redirect_url);
	        }else{
                    $errMsg = 'Invalid credentials';
                    $userExist = User::where('email',$data['email'])->where('is_email_verified',0)->first();
                    if($userExist){
                        $this->SendEmailService->sendVerificationEmailToUser($userExist);
                        $errMsg = 'Your email address is not verified.Please verify email address to continue using app';
                    }
                    if(!empty($og_redirect_url)){
                        $redirect_url = $og_redirect_url;
                        return redirect()->route('site.login',['redirect_url' => $redirect_url])->with('error',$errMsg);
                    }   
                    else
                    {
                        return redirect()->route('site.login')->with('error',$errMsg);
                    }
	        }
	    }
    }

    public function doLogin(Request $request){
        $redirect_url = '';
        $pageTitle = "Seafarer - Sign In | Flanknot";
        $metaDescription = "To use Flanknot Sign in with your registered email id. Verify your email id to continue using Flanknot.";
        $metaKeywords = "Seafarers, Maritime Institute, Shipping Companies";
        if(isset($_GET['redirect_url']) && !empty($_GET['redirect_url'])){
            $redirect_url = $_GET['redirect_url'];
        }
       
    	return view('site.login.login',['redirect_url' => $redirect_url, 'pageTitle' => $pageTitle, 'metaDescription' => $metaDescription, 'metaKeywords' => $metaKeywords]);
    }

    public function resetPasswordView()
    {
        return view('site.login.reset_password');
    }

    public function resetPassword(Request $request){

        $data = $request->toArray();
        $admin_data = $this->userService->getAdminDataByEmail($data)->toArray();

        if(count($admin_data) > 0){
            $admin_data = $admin_data[0];
            $this->sendEmailService->sendResetPasswordEmail($admin_data);
            return response()->json(['status' => 'success' , 'message' => 'We have sent you email on your mentioned email.'],200);
        }else{
            return response()->json(['status' => 'failure' , 'message' => 'Invalid email id provided.'],400);
        }
    }

    public function setNewPassword(Request $request,$email){
        $data['email'] =  base64_decode($email);
        $admin_data = $this->userService->getAdminDataByEmail($data)->toArray();
        if(count($admin_data) > 0){
            return view('admin.login.change_password',['email' => $email]);
        }
    }
}
