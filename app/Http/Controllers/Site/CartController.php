<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Services\SubscriptionService;
use App\Services\CartProductService;
use Auth;

class CartController extends Controller
{
    private $SubscriptionService;
    private $CartProductService;

    function __construct() {
        $this->SubscriptionService = new SubscriptionService();
        $this->CartProductService = new CartProductService();
    }

    public function addToCart(Requests\SubscriptionRequest $subscriptionRequest) {
        if( Auth::check()) {
            $user = Auth::user();
            $user_id = $user->id;
            $subscription_id = $subscriptionRequest['subscription_id'];
            $data['subscription_id'] = $subscription_id;
            $data['user_id'] = $user_id;
            $data['quantity'] = isset($subscriptionRequest['quantity']) ?  $subscriptionRequest['quantity'] > 1 ? 1 : $subscriptionRequest['quantity'] : 1;

            $subscription_plan_details = $this->SubscriptionService->getAllSubscriptionDetailsBySubscriptionId($data['subscription_id'])->toArray();
            $data['price'] = $subscription_plan_details[0]['amount'];

            $result = $this->CartProductService->addSubscriptionToCart($data);

            if($result['status'] == 'success') {
                return response()->json(['status' => 'success', 'message' => 'Subscription has been added to the cart'], 200);
            } else {
                return response()->json(['status' => 'error', 'message' => 'There was some error while adding the subscription to your cart. Please try again'], 400);
            }
        } else {
            return redirect()->route('login')->with(['subscription_login' => 'Please login to get the subscription']);
        }
    }

    public function viewCart()
    {
        if( Auth::check())
        {
            $user = Auth::user()->toArray();
            $user_cart_details = $this->CartProductService->getCartDetails($user)->toArray();

            return view('site.cart.cart' , ['cart_details' => $user_cart_details]);
        }else {
            return redirect()->route('login')->with(['subscription_login' => 'Please login to continue.']);
        }
    }

    public function deleteFromCart(Request $request, $cart_id){

        $result = $this->CartProductService->deleteFromCart($cart_id);

        if($result['status'] == 'success') {
            return response()->json(['status' => 'success', 'message' => 'Subscription has been removed from the cart' , 'cart_total' => $result['cart_total']], 200);
        } else {
            return response()->json(['status' => 'error', 'message' => 'There was some error while removing the subscription from your cart. Please try again'], 400);
        }
    }

    public function checkout(){
        if( Auth::check())
        {
            $user = Auth::user()->toArray();
            $user_cart_details = $this->CartProductService->getCartDetails($user)->toArray();

            return view('site.checkout.checkout' , ['cart_details' => $user_cart_details]);
        }else {
            return redirect()->route('login')->with(['subscription_login' => 'Please login to continue.']);
        }
    }
}
