<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\UserService;
use App\Http\Controllers\Controller;
use Barryvdh\DomPDF\Facade as PDF;
use Auth;
use DB;
use App\Services\SendEmailService;
use App\Services\SendSmsService;
use App\Services\JobService;
use App\Services\BatchService;
use App\Services\OrderService;
use App\Services\InstituteService;
use App\UserProfessionalDetail;
use App\AdvertiseDetails;
use App\CompanyRegistration;
use App\SeamanBookEnquiry;
use App\InstituteRegistration;
use App\User;
use App\Inquiry;
use App\Events\BatchOrderNotification;
use PubNub\PNConfiguration;
use PubNub\PubNub;
use Mail;
use ZanySoft\Zip\Zip;
    use League\Flysystem\Filesystem;
    use S3;
    use Illuminate\Support\Facades\Storage;
    use League\Flysystem\ZipArchive\ZipArchiveAdapter;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    private $sendEmailService;
    public function __construct()
    {
        $this->userService = New UserService();
        $this->jobService = New JobService();
        $this->batchService = New BatchService();
        $this->orderService = New OrderService();
        $this->instituteService = New InstituteService();
        $this->sendEmailService = new SendEmailService();
        $this->sendSmsService = new SendSmsService();
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function home()
    {
        $pageTitle = "FLANKNOT - Seafarers, Maritime Institutes, Shipping Companies & more";
        $metaDescription = "At Flanknot, Seafarers can Create Informative Profiles, Dynamic Resumes with Free Storage to upload Documents and can Share these details with QR Acknowledgements.Seafarers can self-assess their onboard work and can also give vital feedbacks.Flanknot provides the lowest Prices and the highest Discounts on Courses booking.";
        $metaKeywords = "Seafarer, Maritime Institute, Shipping Companies, Seafarer Profile, Seafarer Resume, Documents, Passport, Visa, Indos, CDC, Continuous Discharge Certificate,  Seaman book, WK, watchkeeping certificate,  COP, Certificate of Proficiency,  COC, Certificate of Competency information, COE, Certificate of Endorsement, DCE, Dangerous Cargo Endorsement, GMDSS, Global Maritime Distress and Safety System,  FRAMO, DP, Dynamic positioning, Sea Service, vessel name, owner company, manning company, vessel flag, vessel type, vessel GRT, vessel BHP, vessel engine type, sing on date and sign off date, Course, Basic Modular Courses, Advance Modular Courses, Add-On Courses, Competency Courses, Preparatory Courses, Simulator Courses, Offshore related courses, Tanker related courses, Refresher and Upgradation courses, Management courses, Academic details, ILO Medical and Screening Medical details, Yellow fever vaccination details , Cholera vaccination details , Hepatitis B details, D&T vaccination details, covid vaccination details. Character Certificate details, PAN details, Aadhar details, salary details. Uploading seafarer documents";
        $institute_course_type = \CommonHelper::institute_course_types();
        $courses = \App\Courses::all()->toArray();
        $all_courses = '';
        $course_by_type = [];
        $institutes_list = User::where('registered_as', 'institute')->whereNotNull('profile_pic')->select('profile_pic','first_name', 'id')->get()->toArray();
        foreach ($courses as $key => $value) {
            if(array_key_exists($value['course_type'], $institute_course_type)){
                $course_by_type[$value['course_type']][] = $value; 
            }
        }
//        return view('site.home.home',['course_type' => $institute_course_type,'course_by_type' => $course_by_type,'selected_courses_by_type' => $all_courses,'institutes_list' => $institutes_list]);   
//        return view('site.home.home');   
        return view('site.home.home-new-theme', compact('pageTitle', 'metaDescription', 'metaKeywords'));   
    }
    
    public function register()
    {
        $pageTitle = "Seafarer - Registration for all Sailing Ranks | Flanknot";
        $metaDescription = "To join Flanknot, Register using your existing Email id. Select your Rank, Nationality and Enter your Passport Details.";
        $metaKeywords = "Seafarers, Rank, Deck Officers, Master, Master DPO, Master NCV, Master Handler, Master Inland, Master Yatch, Master Home Trade, Master Skipper, Chief Officer, Chief Officer DPO, Chief Officer NCV, Chief Officer Inland, Second Officer, Second Officer DPO,  Third Officer, Third Officer DPO, Junior Officer, NWKO, NWKO NCV, Deck Cadet, R. O. Handler, Radio Officer Handler, Radio Officer, OOW Watch, Engineers, Chief Engineer, Chief Engineer NCV, Second Engineer, Second Engineer NCV, Third Engineer, Fourth Engineer, Junior Engineer, Electro Technical Officer, Electrical Officer, Tr. Electrical Officer, Engine Cadet, Trainee Marine Engineer, TME, Engine Driver, Bosun, Able Seaman, Ordinary Seaman, Pumpman, Welder, Fitter, Crane Operator, Motarman, Oiler, Wiper, G.P Rating, Chief Cook, Cook, 2nd Cook, General Steward, Messman, GS, Technician, Roustabout, Rigger, Labour";
        $institute_course_type = \CommonHelper::institute_course_types();
        $courses = \App\Courses::all()->toArray();
        $all_courses = '';
        $course_by_type = [];
        $institutes_list = User::where('registered_as', 'institute')->whereNotNull('profile_pic')->select('profile_pic','first_name', 'id')->get()->toArray();
        foreach ($courses as $key => $value) {
            if(array_key_exists($value['course_type'], $institute_course_type)){
                $course_by_type[$value['course_type']][] = $value; 
            }
        }
        return view('site.registration.user_register', compact('pageTitle', 'metaDescription', 'metaKeywords'));
    }

    public function services(Request $request)
    {
        return view('site.service.service',['data' => $request->toArray()]);
    }

    public function aboutUs()
    {
        $pageTitle = "About Flanknot - Mission & Vision | Flanknot";
        $metaDescription = "About Flanknot, Mission, Vision, Process, Team, Core Values.";
        $metaKeywords = "Shipping companies, manning companies, enquiry";
        return view('site.about_us.about_us_new-theme', compact('pageTitle', 'metaDescription', 'metaKeywords'));
    }
    public function pricing()
    {
        return view('site.pricing.pricing');
    }


    public function disclaimer()
    {
        $pageTitle = "Flanknot - Disclaimer | Flanknot";
        $metaDescription = "Flanknot Disclaimer Policy";
        $metaKeywords = "Flanknot disclaimer";
        return view('site.disclaimer.disclaimer-new-theme', compact('pageTitle', 'metaDescription', 'metaKeywords'));
    }

    public function faq()
    {
        $pageTitle = "Flanknot FAQ’s - frequently asked questions | Flanknot ";
        $metaDescription = "Doubts regarding Flanknot answered.
            The most frequently asked questions and the answers to these questions.";
        $metaKeywords = "Flanknot FAQ";
        return view('site.FAQ.faq-new-theme', compact('pageTitle', 'metaDescription', 'metaKeywords'));
    }

    public function tnc()
    {
        $pageTitle = "Flanknot - Terms & Conditions | Flanknot";
        $metaDescription = "Flanknot Terms and Condition Policy";
        $metaKeywords = "Flanknot terms & condition";
        return view('site.t&c.t&c-new-theme', compact('pageTitle', 'metaDescription', 'metaKeywords'));
    }
    
    public function report()
    {
        $pageTitle = "Flanknot - Report | Flanknot";
        $metaDescription = "Flanknot Report";
        $metaKeywords = "Flanknot report";
        return view('site.report', compact('pageTitle', 'metaDescription', 'metaKeywords'));
    }
    
    public function privacy_policy()
    {
        $pageTitle = "Flanknot - Privacy Policy | Flanknot";
        $metaDescription = "Flanknot Privacy Policy";
        $metaKeywords = "Flanknot Privacy-policy";
        return view('site.privacy_policy.privacy_policy-new-theme', compact('pageTitle', 'metaDescription', 'metaKeywords'));
    }

    public function cookie_policy()
    {
        $pageTitle = "Flanknot - Cookie Policy | Flanknot";
        $metaDescription = "Flanknot Cookie Policy";
        $metaKeywords = "Flanknot cookie-policy";
        return view('site.cookie_policy.cookie_policy', compact('pageTitle', 'metaDescription', 'metaKeywords'));
    }
    public function company_inquiry()
    {
        $pageTitle = "Shipping Companies - Enquiry section to contact and know more | Flanknot";
        $metaDescription = "At Flanknot, Shipping companies can create a self-informative Profile. At Flanknot, Shipping companies can connect with Seafarers across the Globe and use the useful features on Flanknot.";
        $metaKeywords = "Seafarer jobs, Shipping Companies, ship jobs";
        return view('site.company_inquiry.company_inquiry', compact('pageTitle', 'metaDescription', 'metaKeywords'));   
    }
    public function institutes_inquiry()
    {
        $pageTitle = "Maritime Institute - Enquiry section to contact and know more | Flanknot";
        $metaDescription = "At Flanknot, Maritime Institutes gets to publicize its firm and endorse its courses.
            Flanknot provides specifically designed profile for Shipping Institutes that showcases the Institute in the best way and enables users to user to connect to them.
            Maritime Institutes can join Flanknot and put up their course lists along with batch timings and all other required details for bookings.";
        $metaKeywords = "Maritime Institutes, enquiry";
        return view('site.institutes_inquiry.institutes_inquiry', compact('pageTitle', 'metaDescription', 'metaKeywords'));   
    }
    public function contact_us()
    {
        $pageTitle = "Contact Details - for Help, Grievance, Share Idea | Flanknot";
        $metaDescription = "Contact Flanknot for Help regarding understanding or Navigation of the website.
            Share any new ideas or suggestions at Flanknot and get rewarded. Contact Flanknot if any queries, Grievance or compliance issues.";
        $metaKeywords = "Flanknot contact, Help, Idea, suggestions, queries";
        return view('site.contact_us.contact_us', compact('pageTitle', 'metaDescription', 'metaKeywords'));   
    }
    
    public function how_to() {
        $pageTitle = "How To – To use | Flanknot";
        $metaDescription = "How to Use Flanknot.com?
How to Edit Profile?
How to Upload Documents?
How to Create Resume?
How to view My Documents?
How to Download Documents?
How to Apply Jobs?
How to Save Contact?
How to Share Resume?
How to Share Documents?
How to Share Profile?
How to Receive Quick Response?
How to Know if Documents were shared correctly?";
        $metaKeywords = "Edit Profile, Upload Documents, Resume, My Documents, Dowload Documents, Job Notification, Contacts, Share Resume, Share Documents, Share Profile, Share History Resume, Share History Documents";
        return view('site.how_to', compact('pageTitle', 'metaDescription', 'metaKeywords'));
    }

    public function mySubcription()
    {
        return view('site.home.subcription');
    }

    public function jobApplicationListing()
    {
        return view('site.home.job_application_listing');
    }

    public function addJobPost()
    {
        return view('site.home.add_job_post');
    }

    public function candidateSearch()
    {
        return view('site.home.candidate_search');
    }

    public function searchListing()
    {
        return view('site.home.search_listing');
    }

    public function seamanBookEnquiry(Request $request)
    {
        $enquiry = SeamanBookEnquiry::create($request->toArray());

        if($enquiry){
            return response()->json(['status' => 'success' , 'message' => 'We have received your enquiry.We will contact you soon.'],200);
        }else{
            return response()->json(['status' => 'failed' , 'message' => 'Error while adding data.'],400);
        }

    }
    
    public function post_inquiry(Request $request)
    {
        $request = $request->toArray();
        unset($request['_token']);
        $data = DB::table('inquiries')->insert($request);

        if($request['type'] == 1){
            $type = 'Company';           
        }else{
            $type = 'Institute';
        } 
        $subject  = 'Inquiry from '.$request['title'];
        if($data){
            $inquiryEmailContent = view('emails.newInquiryEmail');
            $inquiryEmailContent = str_replace("{name}", $request['name'], $inquiryEmailContent);
            $inquiryEmailContent = str_replace("{type}", $type, $inquiryEmailContent);
            $inquiryEmailContent = str_replace("{institute_name}", $request['title'], $inquiryEmailContent);
            $inquiryEmailContent = str_replace("{website}", $request['website'], $inquiryEmailContent);
            $inquiryEmailContent = str_replace("{email}", $request['email'], $inquiryEmailContent);
            $inquiryEmailContent = str_replace("{number}", $request['number'], $inquiryEmailContent);

            $data = ['template' => $inquiryEmailContent, 'subject' => $subject,'users' => ['flankknot@gmail.com','balkrishnajatin@gmail.com' ] ];
           
            Mail::send([], [], function ($message) use ($data) {
                $message->from(env('MAIL_FROM_ADDRESS','donot-reply@flanknot.com'));
                foreach($data['users'] as $user){
                        $message->to($user);
                }                               
                $message->subject($data['subject']);
                $message->setBody($data['template'], 'text/html'); // for HTML rich messages
            });
            return response()->json(['status' => 'success' , 'message' => 'We have received your inquiry.We will contact you soon.'],200);
        }else{
            return response()->json(['status' => 'failed' , 'message' => 'Error while adding data.'],400);
        }

    }
    
    public function send_report(Request $request)
    {
        $request = $request->toArray();
        unset($request['_token']);
        $data = DB::table('reports')->insert($request);
        if($data){
            return response()->json(['status' => 'success' , 'message' => 'We have received your report.We will contact you soon.'],200);
        }else{
            return response()->json(['status' => 'failed' , 'message' => 'Error while adding data.'],400);
        }

    }

    public function dropzone(){
        return view('site.test');
    }

    public function test(){
        return view('site.institute.course_blocking');
        if(isset($id) && !empty($id)){
            $institute_id = $id;
        }else{
            $institute_id = Auth::user()->id;
        }

        $options['with'] = ['institute_registration_detail.institute_detail','institute_registration_detail','institute_registration_detail.institute_locations.state','institute_registration_detail.institute_locations.city','institute_registration_detail.institute_locations.pincode.pincodes_states.state','institute_registration_detail.institute_locations.pincode.pincodes_cities.city','institute_registration_detail.advertisment_details','institute_registration_detail.institute_notice','institute_registration_detail.institute_gallery'];
        $result = $this->userService->getDataByUserId($institute_id, $options)->toArray();

        $institute_course_type = [];
        $filter['no_pagination'] = 1;
        $options['with'] = ['course_details','institute_batches.course_details'];

        $institute_courses = $this->instituteService->getAllCourseDetailsByInstituteId($result[0]['institute_registration_detail']['id'],$options,$filter);
        
        if(!empty($institute_courses)){
            foreach ($institute_courses as $key => $course) {
                $institute_course_type[$course['course_type']][] = $course->toArray();
            }
        }

        if(isset($id) && !empty($id)){
            return view('site.institute.new_institute_profile', ['data' => $result,'institute_course_type' => $institute_course_type,'user' => 'another']);
        }
        return view('site.institute.institute', ['data' => $result,'institute_course_type' => $institute_course_type]);
        
        $admin_id = \App\User::where('registered_as','admin')->first();dd($admin_id->id);

        event(new BatchOrderNotification(3, 2, 'batch_booked','test'));

        dd(123);
        // $batch_details = $this->batchService->getDataByBatchId(158);
        // $user_details = $this->userService->getDataByUserId(136);
        // $options['with'] = ['order_payments'];
        // $batch_details[0]['order_details'] = $this->orderService->getOrderDetailById(91,$options)->toArray();
        // //dd($batch_details[0]->toArray());
        // //$this->sendEmailService->sendBatchBlockSeatConfirmEmailToInstitute($batch_details[0],$user_details);
        // $this->sendEmailService->sendBatchSeatConfirmEmailToSeafarer($batch_details[0],$user_details);
        // dd(123);

    }

    public function test12()
    {
        $result = $this->userService->getAllSeafarersByCurrentDayAvailability()->toArray();

        foreach ($result as $key => $data) {
            $location = '';

            if(isset($data['user_personal_details']) && !empty($data['user_personal_details'])){
                $location = $data['user_personal_details'];
            }
            // dd($data);
            $result[$key]['job_details'] = $this->jobService->getJobDetailsByRankAndLocation($data['applied_rank'],$data['user_id'])->toArray();
            if(empty($result[$key]['job_details'])){
                unset($result[$key]);
            }else{
                $result[$key]['host'] = $_SERVER['HTTP_HOST'];
                $result[$key]['home'] = route('home');
                $result[$key]['route'] = route('site.seafarer.job.search');
                // dd($result[$key]);
                //$this->sendEmailService->sendJobsToSeafarer($result[$key]);
                return view('emails.seafarerJobAvailability',['template' => $result[$key]]);
            }
        }
        
        dd($result);
        
    }

    public function test2(){
        //from current day to next 2 day
        $days = 2;
        $result = $this->userService->getAllSeafarersExpiringAvailabilityBeforeGivenDays($days)->toArray();
        
        if(isset($result) && !empty($result)){
            foreach ($result as $key => $value) {
                
                if(isset($value['availability']) && !empty($value['availability'])){
                    $current_date = date('Y-m-d');
                    $to_date = date('Y-m-d', strtotime('+'.$days.'days'));

                    $date2=date_create($value['availability']);
                    $date1=date_create($current_date);
                    $diff=date_diff($date1,$date2);
                    $result[$key]['days'] = $diff->format("%a");

                    //$this->sendEmailService->sendAvalailityEmailToSeafarer($result[$key]);
                }
            }
        }
        //dd($result);
        $data['route'] = route('user.update.availability',$array['user_id']);
        //return view('emails.seafarerUpdateAvailability',['template' => $result[2]]);
    }

    public function test1()
    {
        //jobs and there matching seafarers

        $options['with'] = ['company_registration_details'];

        $result = $this->jobService->getAllJobDetails('','',$options,'')->toArray();

        if(isset($result) && !empty($result)){
            foreach ($result as $key => $value) {
                $result[$key]['job_matching_seafarers'] = $this->userService->getSeafarerByRankAndAvailability($value['rank'],$value['date_of_joining'])->toArray();
                
            }
        }

        if(isset($result) && !empty($result)){
            foreach ($result as $key => $value) {
                if(isset($value['job_matching_seafarers']) && !empty($value['job_matching_seafarers'])){
                    $d = date('d-m-Y',strtotime($value['date_of_joining']));
                    $now = date('d-m-Y');
                    
                    $result[$key]['route'] = route('site.company.candidate.search',['rank' => $value['rank'],'availability_from' => $now,'availability_to' => $d]);
                    $result[$key]['seafarer_profile_route'] = route('user.view.profile');
                    //dd($result[$key]);
                    //$this->sendEmailService->sendCandidatesToCompany($result[$key]);
                    //return view('emails.companyMatchedJobSeafarers',['template' => $result[$key]]);

                }
            }
        }
        //dd($result[3]);
    }

    public function test_availability(){
        $users_list = $this->userService->getUserWithGivenAvilability();

        if(isset($users_list) && !empty($users_list)){

            foreach ($users_list as $day => $users) {
                
                if(isset($users) && !empty($users)){
                    foreach ($users as $user) {
                        $this->sendEmailService->sendExpireAvalailityEmailToSeafarer($user,$day);
                    }
                }
            }
        }

    }
    
    public function test4(){
        $foo = new \App\Services\OrderService();

        $options = ['batch_details.batch_location','user']; 
        $orders = $foo->getOrderDetailByStatus(6,$options );

        foreach ($orders as $key => $value) {
            $batch_start_date = $value['batch_details']['start_date'];

            $start  = date_create(date('Y-m-d',strtotime($batch_start_date)));
            $end    = date_create(); // Current time and date
            $diff   = date_diff($start,$end);

            if($diff->days > 25){
                unset($orders[$key]);
            }else{
                $day = $diff->days;
                $user = $value['user']->toArray();
                $batch_details = $value['batch_details']->toArray();
                $this->sendEmailService->sendFullPaymentNotification($user,$batch_details);
            }
        }
        
        dd($diff->days,122);
    }

    public function institutesCourses(){
        return view('site.institutes_courses.institutes_courses');
    }

    public function institutesSearchResult(){
        return view('site.institutes_courses.institutes_search');
    }

    public function institutesDetails(){
        return view('site.institutes_courses.institutes_details');
    }

    public function test123(){
        return view('pass');
    }
    
    public function testZip(){
        
        
//        \Storage::disk('local')->put(
//    '1623939386-pic.png', 
//    \Storage::disk('s3')->get('public/images/uploads/seafarer_profile_pic/184/1621272262-pic.PNG'));
        
//        $zip = Zip::create(env('DOCUMENT_STORAGE_PATH') . "package6.zip");
//        $zip->add("s3://" . env('AWS_BUCKET') .  "public/images/uploads/seafarer_profile_pic/184/1621272262-pic.PNG", "Something.png");
//        
////        $file = file_get_contents("https://flanknot-demo.s3.ap-south-1.amazonaws.com/public/uploads/user_documents/184/cdc/720/1623939386-pic.png");
//        $zip->add("https://flanknot-demo.s3.ap-south-1.amazonaws.com/public/uploads/user_documents/184/cdc/720/1623939386-pic.png");
//        $zip->close();
//        
//        dd($zip);
        
        
        
//        $aws = Aws::factory('/path/to/my_config.json');
//        $result = $client->getObject(array(
//            'Bucket' => $bucket,
//            'Key'    => 'data.txt'
//        ));
//
//
//        // see laravel's config/filesystem.php for the source disk
        $source_disk = 's3';
        $source_path = 'public/images/uploads/seafarer_profile_pic/184/';

        $file_names = Storage::disk($source_disk)->files($source_path);
//dd($file_names);
        $zip = new Filesystem(new ZipArchiveAdapter(public_path('archive123.zip')));
//        $zip = new Filesystem(new ZipArchiveAdapter('/var/www/html/marine/archive12.zip'));

        foreach($file_names as $file_name){
            $file_content = Storage::disk($source_disk)->get($file_name);
            $file_name = str_replace("public/images/uploads/seafarer_profile_pic/184/","",$file_name);
            $zip->put($file_name, $file_content);
        }
        
        $zip->getAdapter()->getArchive()->close();
        
        dd($zip);
//
//
//
//
//
//        $user_id = 1;
//        $name = "testZip";
//        $source_disk = 's3';
//        $source_path = '';
//        $file_names = Storage::disk($source_disk)->files($source_path);
//        $zip = new Filesystem(new ZipArchiveAdapter(public_path('archive.zip')));
//
//        foreach($file_names as $file_name){
//            $file_content = Storage::disk($source_disk)->get($file_name);
//            $zip->put($file_name, $file_content);
//        }
//
//        $zip->getAdapter()->getArchive()->close();
//
//        return redirect('archive.zip');
//        $destination_path = env('DOCUMENT_STORAGE_PATH');
//        $filename = "package.zip";
        $zip = Zip::create(env('DOCUMENT_STORAGE_PATH') . "package3.zip");
//        $zip->add('public/images/homepage/banner/crew-img2.PNG');
        $zip->add('https://flanknot-demo.s3.ap-south-1.amazonaws.com/public/uploads/user_documents/147/photo/1621248756-pic.jpg', '1621248756-pic.jpg'); // adding a file in zip.
//        $zip->add("s3://" . env('AWS_BUCKET') .  "/public/uploads/user_documents/147/photo/1621248756-pic.jpg");
        $zip->close();
        dd($zip);
        $zipPath = env('DOCUMENT_STORAGE_PATH') . "package1.zip";
//        dd($zip);
        $s3 = new S3(env('AWS_ACCESS_KEY_ID'), env('AWS_SECRET_ACCESS_KEY'), false, 's3.amazonaws.com',env('AWS_DEFAULT_REGION'));
        $moveable_file = str_replace(["/","\\"], [DIRECTORY_SEPARATOR,DIRECTORY_SEPARATOR], $zipPath);
        $s3->putObjectFile($moveable_file, env('AWS_BUCKET'), $moveable_file, S3::ACL_PUBLIC_READ);
        dd("done");
        
//        worked
//        $zip = Zip::create(env("AWS_URL").env('DOCUMENT_STORAGE_PATH') . 'your_zipfile.zip');
//        $zip->add('https://flankknot.com/public/images/homepage/banner/crew-img4.PNG'); // adding an directory.
//        $zip->close();
        dd($zip);
        $zip = Zip::create(env('DOCUMENT_STORAGE_PATH') . $name . '.zip', true);
        $zip->add('public/images/homepage/banner/crew-img2.PNG');
        dd($zip);
        $full_path = env('DOCUMENT_STORAGE_PATH') . $user_id;
        foreach ($data as $path) {
        }
        $zip->close();
        return env('DOCUMENT_STORAGE_PATH') . $user_id . '/' . $name . '.zip';
    }

}
