<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Auth;
use Illuminate\Http\Request;

class SeafarerProfileRegistrationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {   
        // print_r($request->all());die;
        if(isset($request) && isset($request['user_id']) && !empty($request['user_id'])){
            $user_id = $request['user_id'];
            $req = $request->toArray();
        }else{
            $user_id = Auth::user()->id;
        }

        if(isset($req['firstname']) && isset($req['email'])){
            return [
                'firstname' => 'required|max:255',
                /*'lastname' => 'required|max:255|alpha',*/
                'email' => 'required|email|unique:users,email,'.$user_id,
                'gender' => 'required',
                'mobile' => 'required|min:10|unique:users,mobile,'.$user_id,
                //'kin_number' => 'required|min:10',
                'country' => 'required',
                'state' => 'required',
                'city' => 'required',
                'current_rank' => 'required',
                'years' => 'required',
                'months' => 'required',
            ];
        }else{
            return [];
        }
    }
}
