<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use Requests;

class SeafererDocumentDetails extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {   
        $data = $request->toArray();
        
        $required_field_for_rank = \CommonHelper::rank_required_fields()[$data['current_rank_id']];
        
        $temp_rules = [
            'passcountry' => 'required',
            'passno' => 'required',
            'passplace' => 'required',
            'passdateofissue' => 'required|date',
            'passdateofexp' => 'required|date',
            'cdccountry.*' => 'required',
            'cdcno.*' => 'required',
            'cdc_exp.*' => 'required|date',
            
        ];

        foreach ($required_field_for_rank as $key => $value) {

            $field_arr = explode('-', $value);

            if(count($field_arr) > 1) {
                //optional
                
            } else{
                //required
                if ($value == 'COC') {
                    $temp_rules['grade.*'] ='required';
                    $temp_rules['coc_country.*'] = 'required';
                    $temp_rules['coc_no.*'] = 'required';
                    $temp_rules['coc_exp.*'] = 'required';
                }
                if ($value == 'COP') {
                    $temp_rules['cop_no.*'] = 'required';
                    $temp_rules['cop_issue.*'] = 'required';
                    $temp_rules['cop_grade.*'] = 'required';
                    $temp_rules['cop_exp.*'] = 'required';
                }
                if ($value == 'GMDSS') {
                    $temp_rules['gmdss_country'] = 'required';
                    $temp_rules['gmdss_no'] = 'required';
                    $temp_rules['gmdss_doe'] = 'required|date';
                }
                if ($value == 'WATCH_KEEPING') {
                    $temp_rules['watch_country'] = 'required';
                    $temp_rules['deckengine'] = 'required';
                    $temp_rules['watch_no'] = 'required';
                    $temp_rules['watchissud'] = 'required|date';
                }
            }
        }
        //dd($temp_rules);
        $rules = $temp_rules;

        return $rules;
    }
}
