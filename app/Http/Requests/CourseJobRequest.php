<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use Crypt;

class CourseJobRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * All validation messages function
     *
     * @return array
     **/
    public function messages()
    {
        return [
            'company_id.required'          => "Please select company name.",
            'company_email_address_id.required'             => "Please select company email address.",
            'rank_id.required'             => "Please select job post rank.",
            'title.required'             => "Job title is required.",
            'valid_till_date.required'             => "Please select date.",

        ];
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        $input = $request->all();
        switch ($this->method()) {
            case 'GET':
            case 'DELETE':
                {
                    return [];
                }
            case 'POST':
                {
                    $data =  [
                        'company_id' => 'required',
                        'company_email_address_id'          => 'required',
                        'rank_id'          => 'required',
                        'title'          => 'required',
                        'valid_till_date'          => 'required',
                    ];

                    return $data;
                }
            case 'PUT':{

                    $data =  [
                        'company_id' => 'required',
                        'company_email_address_id'          => 'required',
                        'rank_id'          => 'required',
                        'title'          => 'required',
                        'valid_till_date'          => 'required',
                    ];

                    return $data;

            }
            case 'PATCH':
            default:break;
        }
    }
}
