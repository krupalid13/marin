<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use Crypt;

class CompanyDetailRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * All validation messages function
     *
     * @return array
     **/
    public function messages()
    {
        return [
            'email.required'          => "Company email field is required.",
            'email.email'             => "Please enter a valid email address.",
            'name.check_company_detail_unique_name' => "This company name is already used by someone other.",
            'email.check_company_detail_unique_email' => "This email address is already used.",
            'email_group.*' => "Please add valid addition email address.",
            'update_email_group.*' => "Please add valid addition email address."
          
        ];
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        $input = $request->all();
        $id = (isset($input['id']) && !empty($input['id'])) ? Crypt::decrypt($input['id']) : '';
        switch ($this->method()) {
            case 'GET':
            case 'DELETE':
                {
                    return [];
                }
            case 'POST':
                {
                    $data =  [
                        'name' => 'required|CheckCompanyDetailUniqueName:'.$id.'',
                        'email'          => 'required|email|CheckCompanyDetailUniqueEmail:'.$id.'',
                        'address'          => 'required',
                        'type'          => 'required',
                    ];

                    if (isset($input['email_group']) && !empty($input['email_group'])) {
                        
                        $data['email_group.*'] = 'required|email'; 
                    }

                   

                    return $data;
                }
            case 'PUT':{

                $data = [
                    
                        'name' => 'required|CheckCompanyDetailUniqueName:'.$id.'',
                        'email'          => 'required|email|CheckCompanyDetailUniqueEmail:'.$id.'',
                        'address'          => 'required',
                        'type'          => 'required'                      

                    ];

                    if (isset($input['email_group']) && !empty($input['email_group'])) {
                        
                        $data['email_group.*'] = 'required|email'; 
                    }

                    if (isset($input['update_email_group']) && !empty($input['update_email_group'])) {
                        
                        $data['update_email_group.*'] = 'required|email'; 
                    }

                    return $data;

            }
            case 'PATCH':
            default:break;
        }
    }
}
