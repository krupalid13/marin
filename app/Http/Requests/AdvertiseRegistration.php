<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use Auth;

class AdvertiseRegistration extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {   
        $data = $request->toArray();
        $login_data = explode('&', $data['login_data']);
        foreach ($login_data as $key => $value) {
            $array_value = explode('=', $value);
            $login[$array_value['0']] = str_replace("+"," ",$array_value['1']);
        }

        $company_data = explode('&', $data['company_data']);
        foreach ($company_data as $key => $value) {
            $company_array_value = explode('=', $value);
            $company[$company_array_value['0']] = str_replace("+"," ",$company_array_value['1']);
        }
        $company['registered_as'] = 'advertiser';
        $login['email'] = str_replace("%40","@",$login['email']);

        
        if(Auth::check()){
            $user_id = Auth::user()->id;
        }
        $request['login_data'] = $login;
        $request['company_data'] = $company;
        

        return [
            'login_data.*.firstname' => 'required|max:255',
            'company_name' => 'required|max:255',
            'company_address' => 'required|max:255',
            'bussiness_description' => 'required',
            'login_data.*.email' => 'required|email|unique:users,email,'.$user_id,
            'login_data.*.mobile' => 'required|min:10|unique:users,mobile,'.$user_id,
        ];
       
    }
}
