<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CompanyRegistrationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        
        return [
            'email' => 'required|email',
            'email' => Rule::unique('company_registration'),
            'password' => 'required|min:6',
            'contact_person' => 'required|max:255',
            'contact_person_number' => 'required|min:10',
            //'contact_person_email' => 'required|email',
            'company_name' => 'required|max:255',
            'company_type' => 'required',
            'company_description' => 'required',
            'company_email' => 'required|email',
            'company_contact_number' => 'min:10',
            'fax' => 'min:6',
            //'pincode_id' => 'required',
            //'pincode_name' => 'required',
            //'country' => 'required',
            //'address' => 'required',
            //'company_ships' => 'required',
        ];
    }
}
