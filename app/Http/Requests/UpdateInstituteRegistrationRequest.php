<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Auth;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;

class UpdateInstituteRegistrationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        $institute_id = '';

        if(isset($request) && isset($request['institute_id']) && !empty($request['institute_id'])){
            $institute_id = $request['institute_id'];
        }else{
            $institute_id = Auth::user()->id;
        }
        
        return [
            'email' => 'required|email|unique:users,email,'.$institute_id,
            'contact_person' => 'required|max:255',
            'contact_person_number' => 'required|min:10|unique:users,mobile,'.$institute_id,
            'institute_name' => 'required|max:255',
            'institute_alias' => 'required|max:20',
            'institute_description' => 'required',
            /*'institute_email' => 'required|email',
            'institute_contact_number' => 'required|min:10',*/
            'fax' => 'min:6',
            'country' => 'required',
            'state' => 'required',
            'city' => 'required',
            'address' => 'required',
        ];
    }
}
