<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Auth;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;

class UpdateCompanyRegistrationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {   
        $company_id = '';
        if(isset($request) && isset($request['company_id']) && !empty($request['company_id'])){
            $company_id = $request['company_id'];
        }else{
            $company_id = Auth::user()->id;
        }
        
        return [
            'email' => 'required|email|unique:users,email,'.$company_id,
            'contact_person' => 'required|max:255',
            'contact_person_number' => 'required|min:10',
            //'contact_person_email' => 'required|email',
            'company_name' => 'required|max:255',
            'company_type' => 'required',
            'company_description' => 'required',
            'company_email' => 'required|email',
            'company_contact_number' => 'min:10',
            'fax' => 'min:6',
        ];
    }

    public function messages()
    {
        return [
            'email.unique' => 'Contact person email has already been taken.',
        ];
    }
}
