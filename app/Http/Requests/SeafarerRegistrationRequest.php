<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Auth;

class SeafarerRegistrationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user_id = '';
        if(Auth::check())
            $user_id = Auth::user()->id;

        return [
            'firstname' => 'required|max:255',
            /*'lastname' => 'required|max:255|alpha',*/
            'email' => 'required|email|unique:users,email,'.$user_id,
            'password' => 'required|min:6',
            'cpassword' => 'required|min:6',
            'gender' => 'required',
            'mobile' => 'required|min:10|unique:users,mobile,'.$user_id,
            'kin_number' => 'required|min:10',
            'country' => 'required',
            'state' => 'required',
            'city' => 'required',
            'current_rank' => 'required',
            'years' => 'required',
            'months' => 'required',
        ];
    }
}
