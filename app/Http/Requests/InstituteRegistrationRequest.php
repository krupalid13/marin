<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class InstituteRegistrationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email|unique:institute_registration,email',
            'password' => 'required|min:6',
            'contact_person' => 'required|max:255',
            'contact_person_number' => 'required|min:10|unique:users,mobile',
            'institute_name' => 'required|max:255',
            'institute_description' => 'required',
            /*'institute_email' => 'required|email',
            'institute_contact_number' => 'required|min:10',*/
            'fax' => 'min:6',
            'country' => 'required',
            'state' => 'required',
            'city' => 'required',
            'address' => 'required',
        ];
    }
}
