<?php

namespace App\Http\Middleware;

use Closure;
use App\Services\UserService;
use Auth;


class CheckFeatureMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function __construct() {
        $this->userService = new UserService();
    }

    public function handle($request, Closure $next)
    {   
        $action_name = $request->route()->getActionName();
            $a = explode('@', $action_name);
            $action_name = $a[count($a) - 1];
            $new_action = 'feature.'.$action_name;
            $b = config($new_action);
            $c = 'feature.'.$b;
            $feature = config($c);
            if (empty($feature)) {
                return response()->json(['status' => 'failed' , 'message' => 'This feature is not registered, please contact admin for any enquiry.'],422);
            }

        if (Auth::check()) {
            $user = Auth::User();
            $user_role = $user->registered_as;
            if ($user_role != 'admin') {

                $user_id = $user->id;

                $my_subscription_all = $this->userService->mySubscriptionAllType($user_id, $user_role);
                if (empty($my_subscription_all['my']['active'])) {

                    return redirect()->route('site.user.subscription')->with(['message' => 'Your dont have any active subscription, please renew or buy to proceed']);
                }



                $result = $this->userService->checkSubscriptionAvailability($user_id,$user_role);
                if (!$result['result'][$feature]['status']) {
                    return response()->json(['status' => 'failed' , 'message' => 'Your '.\CommonHelper::get_feature_name_to_show($feature).' feature limit is over.'],422);
                }
             
            }  


           return $next($request);
        }
        return redirect()->route('site.login');
        
    }
}
