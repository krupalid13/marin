<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class CheckRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $role)
    {   
        if(Auth::User()){
            $user = Auth::User()->toArray();
            
            if(isset($user) && ($role == $user['registered_as'] || $user['registered_as'] == 'admin'))
                return $next($request);
            else
                abort(404);
        }else{
            if($role == 'admin'){
                return redirect()->guest(route('admin.login'));
            }
            return redirect()->guest(route('site.login'));
        }
    }
}
