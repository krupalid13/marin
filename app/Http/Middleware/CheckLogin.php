<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class CheckLogin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (!Auth::check()) {  

           if($request->ajax() || $request->wantsJson())
           {
               $this->formatResponseService = new FormatResponseService();
               return $this->formatResponseService->respondNotLoggedIn();
           }  

           return redirect()->route('site.login');
        }

        return $next($request);
    }
}
