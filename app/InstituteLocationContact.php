<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InstituteLocationContact extends Model
{
    protected $table = 'institute_location_contacts';

    protected $fillable = ['location_id', 'contact_name', 'contact_name', 'contact_number', 'designation'];
}
