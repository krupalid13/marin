<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdvertiserCourseDiscountMapping extends Model
{
    protected $table = 'advertiser_course_discount_mappings';

	protected $fillable = ['batch_id','advertise_id','discount'];

	public function company_registration(){
        return $this->hasOne('App\CompanyRegistration','id','advertise_id');
    }

    public function batch_details(){
        return $this->hasOne('App\InstituteBatches','id','batch_id');
    }

}
