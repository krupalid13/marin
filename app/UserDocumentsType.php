<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserDocumentsType extends Model
{
    protected $table = 'user_documents_type';

    protected $fillable = ['user_id', 'type', 'type_id', 'order', 'status'];

    public function user_documents(){
        return $this->hasMany('App\UserDocuments','user_document_id','id')->orderBy('id');
    }
}
