<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DocumentPermissions extends Model
{
    protected $table = 'document_permissions';

    protected $fillable = ['requester_id','owner_id','status'];

    public function requester(){
        return $this->belongsTo('App\User' ,'requester_id' ,'id');
    }

    public function owner(){
        return $this->belongsTo('App\User' ,'owner_id' ,'id');
    }

    public function document_type(){
        return $this->hasMany('App\DocumentPermissionRequests' ,'document_permission_id' ,'id');
    }

}
