<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyLocationDetails extends Model
{
    protected $table = 'company_locations';

    protected $fillable = ['company_id', 'country', 'state_id', 'city_id', 'pincode_id','pincode_text', 'address','state_text','city_text','is_headoffice','telephone'];

    public function state(){
        return $this->belongsTo('App\State');
    }

    public function city(){
        return $this->belongsTo('App\City');
    }

    public function pincode(){
        return $this->belongsTo('App\Pincode');
    }

    public function team_members(){
        return $this->hasMany('App\CompanyTeamDetails','location_id','id');
    }

}

