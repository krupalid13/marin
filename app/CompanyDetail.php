<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyDetail extends Model
{
    protected $table = 'company_details';

    protected $fillable = ['company_id', 'country','state_id', 'city_id', 'zip', 'pincode_id', 'state_text', 'city_text', 'fax', 'address', 'company_description', 'company_type','company_email','company_contact_number','rpsl_no','advertisement'];

    public function state(){
        return $this->belongsTo('App\State');
    }

    public function city(){
        return $this->belongsTo('App\City');
    }

    public function pincode(){
        return $this->belongsTo('App\Pincode');
    }

    public function ship_type(){
        return $this->hasMany('App\CompanyShipDetails','company_id','company_id');
    }
}
