<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserDocuments extends Model
{
    protected $table = 'user_documents';

    protected $fillable = ['user_document_id', 'document_path'];
}
