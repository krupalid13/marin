<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EndorsementList extends Model
{
    protected $table = 'endorsement_list';

    protected $fillable = ['name', 'type'];
}
