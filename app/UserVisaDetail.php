<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserVisaDetail extends Model
{
    protected $table = 'user_visa_detail';
    protected $fillable = ['user_id', 'country_id', 'visa_type', 'visa_expiry_date'];
  
}
