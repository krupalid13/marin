<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Crypt;
use App\CourseJob;

class JobMultipleShipType extends Model
{
    protected $table = 'job_multiple_ship_type';
}
