<?php namespace App;
use Illuminate\Database\Eloquent\Model;

class PincodeArea extends Model{

	protected $table = 'pincodes_cities';

	protected $fillable = ['pincode_id','area_id'];

	public function pincode(){
		return $this->belongsTo('App\Pincode');
	}

	public function area(){
		return $this->belongsTo('App\Area');
	}
	
}

?>