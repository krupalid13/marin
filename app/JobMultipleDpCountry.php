<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Yajra\Datatables\Datatables;
use Crypt;
use App\CourseJob;

class JobMultipleDpCountry extends Model
{
    protected $table = 'job_multiple_dp_countries';
}
