<?php

namespace App\Listeners;

use App\Events\BatchOrderNotification;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Services\NotificationService;
use App\Jobs\SendNotification;

class BatchOrderNotifications
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        $this->notificationService = new NotificationService();
    }

    /**
     * Handle the event.
     *
     * @param  BatchOrderNotification  $event
     * @return void
     */
    public function onStoreNotification($event)
    {
        $data['from_user_id'] = $event->from_id;
        $data['to_user_id'] = $event->to_id;
        $data['type'] = $event->type;
        $data['payload'] = json_encode($event->data);

        $notification = $this->notificationService->createOrUpdate($data);

        dispatch(new SendNotification($data['to_user_id'],$notification->id));
    }
}
