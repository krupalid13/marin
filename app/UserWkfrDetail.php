<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserWkfrDetail extends Model
{
    protected $table = 'user_wkfr_details';

    protected $fillable = ['user_id', 'watch_keeping', 'type', 'wkfr_number', 'indos_number', 'issue_date', 'yellow_fever', 'yf_issue_date', 'wk_cop','ilo_medical','ilo_issue_date','ilo_country','screening_test','screening_test_date','screening_test_county','blood_type'];
}
