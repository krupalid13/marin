<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyDocuments extends Model
{
    protected $table = 'company_documents';

    protected $fillable = ['company_id','pancard','incorporation_certificate','rpsl_certificate','pancard_number','incorporation_number'];
}
