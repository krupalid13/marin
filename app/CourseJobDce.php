<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Yajra\Datatables\Datatables;
use Crypt;
use App\Dce;

class CourseJobDce extends Model
{
    protected $table = 'course_job_dce';

    public function dce(){

        return $this->belongsTo('App\Dce','dce_id','id');
    }
}
