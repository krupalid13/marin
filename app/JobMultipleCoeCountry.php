<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Yajra\Datatables\Datatables;
use Crypt;
use App\CourseJob;

class JobMultipleCoeCountry extends Model
{
    protected $table = 'job_multiple_coe_countries';
}
