<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserDocumentDownloads extends Model
{
    protected $table = 'user_document_downloads';

    protected $fillable = ['requester_id', 'owner_id', 'documents_list'];

    public function document_permissions(){
        return $this->hasMany('App\DocumentPermissions','owner_id','owner_id')->orderBy('id','desc');
    }

    public function requester(){
        return $this->belongsTo('App\User' ,'requester_id' ,'id');
    }

    public function owner(){
        return $this->belongsTo('App\User' ,'owner_id' ,'id');
    }
}
