<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Yajra\Datatables\Datatables;

class PromotionalEmail extends Model {
    /**
     * get Email List Datatable function
     * @return void
     * @author
     * */

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email', 'email_type', 'updated_at'
    ];

    public function get($request) {
        if ($request->method() == 'GET') {

            $sql = self::select("promotional_emails.*", 'users.id as user_id')
                    ->leftJoin('users', 'promotional_emails.email', '=', 'users.email')
                    ->with('promotionalEmailLog', 'promotionalEmailLogCount');

            return Datatables::of($sql)
                            ->addColumn('last_view', function($row) {
                                $lastView = "";
                                if (!empty($row->promotionalEmailLog)) {
                                    $lastView = $row->promotionalEmailLog->created_at;
                                } 
                                return $lastView;
                            })
                            ->addColumn('view_count', function($row) {
                                $lastView = "";
                                if (!empty($row->promotionalEmailLogCount)) {
                                    $lastView = count($row->promotionalEmailLogCount);
                                } 
                                return $lastView;
                            })
                            ->addColumn('user_register', function($row) {
                                $userRegister = "";
                                if (!empty($row->user_id)) {
                                    $userRegister = '<i class="fa fa-check" aria-hidden="true"></i>';
                                }else{
                                    $userRegister = '<i class="fa fa-times" aria-hidden="true"></i>';
                                } 
                                return $userRegister;
                            })
                            ->make(true);
        }
    }

    public function promotionalEmailLog() {
        return $this->hasOne('App\PromotionalEmailLog', 'promotional_email_id', 'promotional_email_id')->orderBy('promotional_email_log_id', 'DESC');
    }
    
    public function promotionalEmailLogCount() {
        return $this->hasMany('App\PromotionalEmailLog', 'promotional_email_id', 'promotional_email_id');
    }

}
