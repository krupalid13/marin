<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyAgents extends Model
{
    protected $table = 'company_agents';

    protected $fillable = ['logo','from_company','from_company_id','to_company','to_company_id','location','agent_type','status'];
}
