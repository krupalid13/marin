<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class order extends Model
{
    protected $table = 'orders';

    protected $fillable = ['user_id', 'subscription_id', 'subscription_cost', 'cost','total', 'tax','tax_amount','status','batch_id','payment_type','preffered_type','preffered_date','discount'];

    public function seafarer_subscription_detail(){
        return $this->hasOne('App\SeafarerSubscription');
    }

    public function user_subscription_detail(){
        return $this->hasOne('App\UserSubscription');
    }

    public function order_payment(){
        return $this->hasOne('App\OrderPayment');
    }

    public function order_payments(){
        return $this->hasMany('App\OrderPayment')->orderBy('id','desc');
    }

    public function subscription(){
        return $this->belongsTo('App\Subscription');
    }

    public function company_subscription(){
        return $this->hasOne('App\CompanySubscription');
    }

    public function institute_subscription(){
        return $this->hasOne('App\InstituteSubscription');
    }

    public function advertiser_subscription(){
        return $this->hasOne('App\AdvertiserSubscription');
    }

    public function seafarer_subscription(){
        return $this->hasOne('App\SeafarerSubscription');
    }

    public function user(){
        return $this->hasOne('App\User','id','user_id');
    }

    public function batch_details(){
        return $this->hasOne('App\InstituteBatches','id','batch_id');
    }

    public function order_history(){
        return $this->hasMany('App\OrderStatusHistory','order_id','id');
    }
}
