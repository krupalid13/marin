<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserHomeShip extends Model
{
    protected $table = 'user_home_ships';
    protected $fillable = ['type', 'typevalue'];
}
