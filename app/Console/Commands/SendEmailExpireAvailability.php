<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Services\UserService;
use App\Services\SendEmailService;
use App\Jobs\SendEmailExpireAvailabilityToSeafarer;

class SendEmailExpireAvailability extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send_email_expire_availability';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Email to seafarer to those whose date of availability is expired';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->userService = New UserService();
        $this->sendEmailService = new SendEmailService();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public static function handle()
    {
        $userService = New UserService();
        $users_list = $userService->getUserWithGivenAvilability();

        if(isset($users_list) && !empty($users_list)){

            foreach ($users_list as $day => $users) {
                
                if(isset($users) && !empty($users)){
                    foreach ($users as $user) {
                        //$this->sendEmailService->sendExpireAvalailityEmailToSeafarer($user,$day);
                        dispatch(new SendEmailExpireAvailabilityToSeafarer($user,$day));
                    }
                }
            }
        }
    }
}
