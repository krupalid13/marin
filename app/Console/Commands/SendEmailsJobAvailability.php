<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Services\UserService;
use App\Services\JobService;
use App\Services\SendEmailService;
use App\Jobs\SendEmailJobAvailability;

class SendEmailsJobAvailability extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send_emails_job_availability';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Job email to seafarer';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->userService = New UserService();
        $this->jobService = New JobService();
        $this->sendEmailService = new SendEmailService();
    }

    /**
     * Execute the console command.
     *  
     * @return mixed
     */
    public function handle()
    {
        $result = $this->userService->getAllSeafarersByCurrentDayAvailability()->toArray();

        if( count($result) > 0 ) {
            foreach ($result as $key => $data){

                $location = '';
                $rank = $data['applied_rank'];
                if(isset($data['user_personal_details']) && !empty($data['user_personal_details'])){
                    $location = $data['user_personal_details'];
                }
                $result[$key]['job_details'] = $this->jobService->getJobDetailsByRankAndLocation($rank)->toArray();

                if(empty($result[$key]['job_details'])){
                    unset($result[$key]);
                }else{
                    $result[$key]['home'] = route('home');
                    $result[$key]['route'] = route('site.seafarer.job.search');
                    dispatch(new SendEmailJobAvailability($result[$key]));
                }
            }

        }
    }
}
