<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Jobs\SendEmailSeafarerForFullPaymentJob;

class SendEmailSeafarerForFullPayment extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send_emails_to_seafarer_batch_full_payment';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send email to seafarer for full payment of institute batch fee';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public static function handle()
    {
        $foo = new \App\Services\OrderService();

        $options = ['batch_details.batch_location','user']; 
        $orders = $foo->getOrderDetailByStatus(6,$options);

        foreach ($orders as $key => $value) {
            $batch_start_date = $value['batch_details']['start_date'];

            $start  = date_create(date('Y-m-d',strtotime($batch_start_date)));
            $end    = date_create(); // Current time and date
            $diff   = date_diff($start,$end);

            if($diff->days > 25){
                unset($orders[$key]);
            }else{
                $day = $diff->days;
                $user = $value['user']->toArray();
                $batch_details = $value['batch_details']->toArray();
                //$this->sendEmailService->sendFullPaymentNotification($user,$batch_details);
                dispatch(new SendEmailSeafarerForFullPaymentJob($user,$batch_details));
            }
        }
    }
}
