<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Services\UserService;
use App\Services\JobService;
use App\Services\SendEmailService;
use App\Jobs\SendEmailCompanyAccordingJob;

class SendEmailsCompanyAccordingJob extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send_emails_company_candidate_according_job';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send candidate email to companies according to job details';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */


    public static function handle()
    {   
        $jobService = New JobService();
        $userService = New UserService();

        $t=date('d-m-Y');
        $day = strtolower(date("d",strtotime($t)));

        $no = $day;
        $count = 0;

        do {
            $count++;
            $no = $no - 7;
        } while ($no > 0);

        $options['with'] = ['company_registration_details','company_registration_details.active_subscription'];
        $result = $jobService->getAllJobDetails('','',$options,'')->toArray();

        if(isset($result) && !empty($result)){
            foreach ($result as $key => $value) {
                $result[$key]['job_matching_seafarers'] = $userService->getSeafarerByRankAndAvailability($value['rank'],$value['date_of_joining'])->toArray();
                
            }
        }

        if(isset($result) && !empty($result)){
            foreach ($result as $key => $value) {

                if (isset($value['company_registration_details']['active_subscription']) && !empty($value['company_registration_details']['active_subscription'])) {
                    $act_sub = end($value['company_registration_details']['active_subscription']);
                    $act_sub_val = json_decode($act_sub['subscription_details'],true);

                    $limits = $userService->getAllFeaturesCount($value['company_id'], 'company',$act_sub_val['subscription_features'],$act_sub['valid_from'],$act_sub['valid_to']);

                    if (!empty($limits) && isset($limits[config('feature.feature2')]) && $limits[config('feature.feature2')]['available'] > 0) {

                        $feature_count = $limits[config('feature.feature2')]['feature']['count'];

                        if ($limits[config('feature.feature2')]['feature']['duration_type'] == 'month') {
                            $day_to_send_arr = [];
                            if ($feature_count == 1) {
                                $day_to_send_arr = [1];
                            } elseif ($feature_count == 2) {
                                $day_to_send_arr = [2,4];
                            } elseif ($feature_count == 3) {
                                $day_to_send_arr = [1,2,3];
                            } elseif ($feature_count == 4) {
                                $day_to_send_arr = [1,2,3,4];
                            } elseif ($feature_count == 5) {
                                $day_to_send_arr = [1,2,3,4,5];
                            } elseif ($feature_count == 'all') {
                                $day_to_send_arr = [1,2,3,4,5];
                            }


                            if (in_array($count, $day_to_send_arr)) {
                                if(isset($value['job_matching_seafarers']) && !empty($value['job_matching_seafarers'])){
                                    $d = date('d-m-Y',strtotime($value['date_of_joining']));
                                    $now = date('d-m-Y');
                                    
                                    $result[$key]['route'] = route('site.company.candidate.search',['rank' => $value['rank'],'availability_from' => $now,'availability_to' => $d]);
                                    $result[$key]['seafarer_profile_route'] = route('user.view.profile');
                                    
                                    dispatch(new SendEmailCompanyAccordingJob($result[$key]));

                                    $store_data = [];
                                    $store_data['parent_id'] = $value['company_id'];
                                    $store_data['registered_as'] = 'company';

                                    $userService->storeEmailFeature($store_data);
                                }
                            }

                           
                        } else {
                            if(isset($value['job_matching_seafarers']) && !empty($value['job_matching_seafarers'])){
                                $d = date('d-m-Y',strtotime($value['date_of_joining']));
                                $now = date('d-m-Y');
                                
                                $result[$key]['route'] = route('site.company.candidate.search',['rank' => $value['rank'],'availability_from' => $now,'availability_to' => $d]);
                                $result[$key]['seafarer_profile_route'] = route('user.view.profile');

                                dispatch(new SendEmailCompanyAccordingJob($result[$key]));

                                $store_data = [];
                                $store_data['parent_id'] = $value['company_id'];
                                $store_data['registered_as'] = 'company';

                                $userService->storeEmailFeature($store_data);
                            }
                        }

                        
                    }

                }

            }
        }
    }
}
