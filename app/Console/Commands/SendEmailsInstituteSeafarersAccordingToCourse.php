<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Services\UserService;
use App\Services\JobService;
use App\Services\InstituteService;
use App\Services\SendEmailService;
use App\Jobs\SendEmailInstituteSeafarersAccordingToCourse;

class SendEmailsInstituteSeafarersAccordingToCourse extends Command
{   
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send_emails_institute_seafarer_according_to_course';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send seafarer email to institute according to expiry course';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
       
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public static function handle()
    {   
        $instituteService = new InstituteService();
        $userService = New UserService();
        $sendEmailService = new SendEmailService();

        $t=date('d-m-Y');
        $day = strtolower(date("d",strtotime($t)));

        $no = $day;
        $count = 0;

        do {
            $count++;
            $no = $no - 7;
        } while ($no > 0);

        $options['with'] = ['courses','institute_details','institute_registration_details','institute_registration_details.active_subscription','user','institute_batches.institute_location.location'];
        $result = $instituteService->getAllInstitutesCourses($options)->toArray();

        if($result){
            foreach ($result as $key => $value) {
                if(empty($value['institute_batches'])){
                    unset($result[$key]);
                }
                else{
                    foreach ($value['institute_batches'] as $k => $v) {
                        $course_details = $instituteService->getInstitutesCoursesByInstituteCourseId($v['institute_course_id'])->toArray();
                        
                        $city_ids = array_values(collect($value['institute_batches'][$k]['institute_location'])->pluck('location.city_id')->toArray());

                        $options['with'] = ['user','professional_detail'];
                        $result[$key]['institute_batches'][$k]['seafarers'] = $instituteService->getSeafarerWithCourseId($course_details,$city_ids,$options)->toArray();
                    }
                }
            }
        }

        foreach ($result as $key => $value) {
            if (isset($value['institute_registration_details']['active_subscription']) && !empty($value['institute_registration_details']['active_subscription'])) {

                $act_sub = end($value['institute_registration_details']['active_subscription']);
                $act_sub_val = json_decode($act_sub['subscription_details'],true);

                $limits = $userService->getAllFeaturesCount($value['institute_registration_details']['id'], 'institute',$act_sub_val['subscription_features'],$act_sub['valid_from'],$act_sub['valid_to']);

                if (!empty($limits) && isset($limits[config('feature.feature2')]) && $limits[config('feature.feature2')]['available'] > 0) {

                        $feature_count = $limits[config('feature.feature2')]['feature']['count'];

                        if ($limits[config('feature.feature2')]['feature']['duration_type'] == 'month') {
                            $day_to_send_arr = [];
                            if ($feature_count == 1) {
                                $day_to_send_arr = [1];
                            } elseif ($feature_count == 2) {
                                $day_to_send_arr = [2,4];
                            } elseif ($feature_count == 3) {
                                $day_to_send_arr = [1,2,3];
                            } elseif ($feature_count == 4) {
                                $day_to_send_arr = [1,2,3,4];
                            } elseif ($feature_count == 5) {
                                $day_to_send_arr = [1,2,3,4,5];
                            } elseif ($feature_count == 'all') {
                                $day_to_send_arr = [1,2,3,4,5];
                            }

                            if (in_array($count, $day_to_send_arr)) {

                                foreach ($value['institute_batches'] as $k => $v) {
                                    if(!empty($v['seafarers'])){
                                        $v['course_type'] = $result[$key]['course_type'];
                                        $v['institute_course_type'] = \CommonHelper::institute_course_types();
                                        $v['course_name'] = $result[$key]['courses'][0]['course_name'];
                                        $v['home'] = route('home');
                                        $v['seafarer_profile_route'] = route('user.view.profile');
                                        $v['institute_registration_details'] = $value['institute_registration_details'];
                                        $v['user_details'] = $result[$key]['user'];
                                        dispatch(new SendEmailInstituteSeafarersAccordingToCourse($v));

                                        $store_data = [];
                                        $store_data['parent_id'] = $value['institute_registration_details']['id'];
                                        $store_data['registered_as'] = 'institute';

                                        $userService->storeEmailFeature($store_data);
                                        }
                                }
                            }



                        } else {

                            foreach ($value['institute_batches'] as $k => $v) {
                                if(!empty($v['seafarers'])){
                                    $v['course_type'] = $result[$key]['course_type'];
                                    $v['institute_course_type'] = \CommonHelper::institute_course_types();
                                    $v['course_name'] = $result[$key]['courses'][0]['course_name'];
                                    $v['home'] = route('home');
                                    $v['seafarer_profile_route'] = route('user.view.profile');
                                    $v['institute_registration_details'] = $value['institute_registration_details'];
                                    $v['user_details'] = $result[$key]['user'];
                                    dispatch(new SendEmailInstituteSeafarersAccordingToCourse($v));

                                    $store_data = [];
                                    $store_data['parent_id'] = $value['institute_registration_details']['id'];
                                    $store_data['registered_as'] = 'institute';

                                    $userService->storeEmailFeature($store_data);
                                }
                            }
                        }
                }
            }
        }
    }
}
