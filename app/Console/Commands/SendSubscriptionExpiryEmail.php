<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Jobs\SendSubscriptionExpiryEmailJob;

class SendSubscriptionExpiryEmail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send_subscription_expiry_email';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'send subscription expiry email';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $days_arr = [];
        $days_arr[0] = 15;
        $days_arr[1] = 3;
        foreach ($days_arr as $index => $day) {

            dispatch(new SendSubscriptionExpiryEmailJob($day));

        }
    }
}
