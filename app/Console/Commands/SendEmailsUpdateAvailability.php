<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Services\UserService;
use App\Services\JobService;
use App\Services\SendEmailService;
use App\Jobs\SendEmailUpdateAvailability;

class SendEmailsUpdateAvailability extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send_email_update_availability';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Email to seafarer to update availability.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->userService = New UserService();
        $this->jobService = New JobService();
        $this->sendEmailService = new SendEmailService();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $days = 2;
        $result = $this->userService->getAllSeafarersExpiringAvailabilityBeforeGivenDays($days)->toArray();
        
        if(isset($result) && !empty($result) && count($result) > 0){
            foreach ($result as $key => $value) {
                
                if(isset($value['availability']) && !empty($value['availability'])){
                    $current_date = date('Y-m-d');
                    $to_date = date('Y-m-d', strtotime('+'.$days.'days'));

                    $date2=date_create($value['availability']);
                    $date1=date_create($current_date);
                    $diff=date_diff($date1,$date2);
                    $result[$key]['days'] = $diff->format("%a");
                    
                    dispatch(new SendEmailUpdateAvailability($result[$key]));
                }
            }
        }
    }
}
