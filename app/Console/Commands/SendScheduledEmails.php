<?php

namespace App\Console\Commands;

use App\Jobs\SendScheduledEmail;

use Illuminate\Console\Command;
use App\Services\ScheduleService;

use Carbon\Carbon;

class SendScheduledEmails extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send_scheduled_emails';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send scheduled emails after every 5 minutes';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    private $scheduleService;

    public function __construct()
    {
        parent::__construct();
        $this->scheduleService = New ScheduleService();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {   
        // $next_five_minutes = Carbon::now()->addminute(6);
        $next_five_minutes = Carbon::now()->addday(1);
        $now = Carbon::now();
        $scheduled_emails = $this->scheduleService->getScheduledEmailByCurrentDateTime($now, $next_five_minutes)->toArray();
        
        if( count($scheduled_emails) > 0 ) {
            foreach($this->scheduled_emails as $scheduled_email) {
                dispatch(new SendScheduledEmail($scheduled_email));
            }
        }
    }
}
