<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserPersonalDetail extends Model
{
    protected $table = 'user_personal_details';

    protected $fillable = ['user_id', 'gender', 'marital_status', 'country', 'state_id', 'city_id', 'pincode_text', 'pincode_id', 'state_text', 'city_text','place_of_birth', 'present_add', 'permanent_add','dob','nearest_place','height','weight','kin_number','kin_name','kin_relation','nationality','kin_alternate_no','landline','landline_code','permanent_add2'];

    public function state(){
        return $this->belongsTo('App\State');
    }

    public function city(){
        return $this->belongsTo('App\City');
    }

    public function pincode(){
        return $this->belongsTo('App\Pincode');
    }

}
