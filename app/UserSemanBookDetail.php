<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserSemanBookDetail extends Model
{
    protected $table = 'user_seman_book_details';

    protected $fillable = ['user_id', 'cdc', 'cdc_number', 'cdc_issue_date', 'cdc_expiry_date', 'cdc_verification_date', 'other_cdc', 'other_cdc_number', 'other_cdc_expiry_date','status','sequence'];
}
