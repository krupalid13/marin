<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserCopDetail extends Model
{
    protected $table = 'user_cop_details';

    protected $fillable = ['user_id', 'cop_number', 'cop_grade', 'cop_issue_date', 'cop_exp_date'];
}
