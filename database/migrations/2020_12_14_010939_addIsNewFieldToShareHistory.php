<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIsNewFieldToShareHistory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('share_histories', function (Blueprint $table) {
            //
            $table->tinyInteger('is_new')->default(0)->after('is_allow_qr');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('share_histories', function (Blueprint $table) {
            //
            $table->dropColumn('is_new');
        });
    }
}
