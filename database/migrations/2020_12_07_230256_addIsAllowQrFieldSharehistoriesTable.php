<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIsAllowQrFieldSharehistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('share_histories', function (Blueprint $table) {
            //
            $table->tinyInteger('is_allow_qr')->default(0)->after('ip');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('share_histories', function (Blueprint $table) {
            //
            $table->dropColumn('is_allow_qr');
        });
    }
}
