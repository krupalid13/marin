<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddKincodeToUserpersonalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_personal_details', function (Blueprint $table) {          
             $table->string('kin_number_code')->nullble()->after('kin_number');
             $table->string('kin_alternate_number_code')->nullble()->after('kin_alternate_no');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_personal_details', function (Blueprint $table) {
            //
            $table->dropColumn('kin_number_code');
            $table->dropColumn('kin_alternate_number_code');
        });
    }
}
