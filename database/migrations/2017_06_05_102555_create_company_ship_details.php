<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyShipDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_ship_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('company_id')->nullable();
            $table->string('ship_type')->nullable();
            $table->string('ship_flag')->nullable();
            $table->string('grt')->nullable();
            $table->string('engine_type')->nullable();
            $table->string('bhp')->nullable();
            $table->string('voyage')->nullable();
            $table->string('built_year')->nullable();
            $table->string('p_i_cover')->nullable();
            $table->string('p_i_cover_company_name')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
