<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewFieldsToShareContactTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('share_contacts', function (Blueprint $table) {
            //
            $table->string('title')->nullable()->after('alternate_no');
            $table->string('job_title')->nullable()->after('title');
            $table->string('mobile_code')->nullable()->after('job_title');
            $table->integer('type')->nullable()->comment('1 => Work, 2 => Personal')->after('mobile_code');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('share_contacts', function (Blueprint $table) {
            //
            $table->dropColumn('title');
            $table->dropColumn('job_title');
            $table->dropColumn('mobile_code');
            $table->dropColumn('type');
        });
    }
}
