<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserPersonalDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_personal_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('marital_status')->nullable();
            $table->date('dob')->nullable();
            $table->string('country')->nullable();
            $table->string('state_id')->nullable();
            $table->string('city_id')->nullable();
            $table->string('pincode_text')->nullable();
            $table->string('pincode_id')->nullable();
            $table->string('state_text')->nullable();
            $table->string('city_text')->nullable();
            $table->string('place_of_birth')->nullable();
            $table->text('present_add')->nullable();
            $table->text('permanent_add')->nullable();
            $table->integer('height')->nullable();
            $table->integer('weight')->nullable();
            $table->string('nearest_place')->nullable();
            $table->string('kin_name')->nullable();
            $table->string('kin_relation')->nullable();
            $table->string('kin_number')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_personal_details');
    }
}
