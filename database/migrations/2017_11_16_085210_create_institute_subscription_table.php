<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInstituteSubscriptionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('institute_subscriptions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('institute_reg_id')->nullable();
            $table->integer('order_id')->nullable();
            $table->text('subscription_details')->nullable();
            $table->tinyInteger('status')->nullable()->default(0);
            $table->date('valid_from')->nullable();
            $table->date('valid_to')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('institute_subscriptions');
    }
}
