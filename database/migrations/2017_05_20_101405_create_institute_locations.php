vb<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInstituteLocations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('institute_locations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('institute_id')->nullable();
            $table->string('country')->nullable();
            $table->string('state_id')->nullable();
            $table->string('city_id')->nullable();
            $table->string('pincode_text')->nullable();
            $table->string('pincode_id')->nullable();
            $table->string('state_text')->nullable();
            $table->string('city_text')->nullable();
            $table->text('address')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('institute_locations');
    }
}
