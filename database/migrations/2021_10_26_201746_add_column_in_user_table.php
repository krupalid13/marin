<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnInUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->integer('affiliate_user_id')->after('is_do_not_show');
            $table->string('affiliate_referral_code')->nullable();
            $table->integer('affiliate_referral_by');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
             $table->dropColumn('affiliate_user_id');
              $table->dropColumn('affiliate_referral_code');
               $table->dropColumn('affiliate_referral_by');
        });
    }
}
