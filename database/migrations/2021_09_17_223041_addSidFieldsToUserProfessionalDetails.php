<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSidFieldsToUserProfessionalDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_professional_details', function (Blueprint $table) {
            //
            $table->string('sid_number')->nullable();
            $table->date('sid_issue_date')->nullable();
            $table->date('sid_expire_date')->nullable();
            $table->string('issue_place')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_professional_details', function (Blueprint $table) {
            //
            $table->dropColumn('sid_number');
            $table->dropColumn('sid_issue_date');
            $table->dropColumn('sid_expire_date');
            $table->dropColumn('issue_place');
        });
    }
}
