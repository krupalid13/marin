<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserSemanBookDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_seman_book_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('cdc')->nullable();
            $table->string('cdc_number')->nullable();
            $table->date('cdc_issue_date')->nullable();
            $table->date('cdc_expiry_date')->nullable();
            $table->string('other_cdc')->nullable();
            $table->string('other_cdc_number')->nullable();
            $table->date('other_cdc_expiry_date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_seman_book_details');
    }
}
