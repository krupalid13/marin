<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddGmdssNumberAndGmdssExpiryDateToUserGmdssDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_gmdss_details', function (Blueprint $table) {
            $table->string('gmdss_endorsement_number')->nullable();
            $table->date('gmdss_endorsement_expiry_date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_gmdss_details', function (Blueprint $table) {
            //
        });
    }
}
