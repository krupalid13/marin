<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyJobs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_jobs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('company_id');
            $table->string('job_title')->nullable();
            $table->string('job_category')->nullable();
            $table->string('rank')->nullable();
            $table->string('ship_type')->nullable();
            $table->integer('sal_from')->nullable();
            $table->integer('sal_to')->nullable();
            $table->string('valid_from')->nullable();
            $table->string('valid_to')->nullable();
            $table->string('date_of_joining')->nullable();
            $table->string('nationality')->nullable();
            $table->string('min_rank_exp')->nullable();
            $table->text('job_description')->nullable();
            $table->tinyInteger('status')->default(0);
            $table->string('grt')->nullable();
            $table->string('bhp')->nullable();
            $table->string('engine_type')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company_jobs');
    }
}
