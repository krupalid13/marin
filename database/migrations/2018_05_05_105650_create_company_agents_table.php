<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyAgentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_agents', function (Blueprint $table) {
            $table->increments('id');
            $table->string('logo')->nullable();
            $table->string('from_company')->nullable();
            $table->integer('from_company_id')->nullable();
            $table->string('to_company')->nullable();
            $table->integer('to_company_id')->nullable();
            $table->string('location')->nullable();
            $table->string('agent_type')->nullable();
            $table->tinyInteger('status')->default('0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company_agents');
    }
}
