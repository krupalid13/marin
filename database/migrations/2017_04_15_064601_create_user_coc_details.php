<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserCocDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_coc_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('coc')->nullable();
            $table->string('coc_number')->nullable();
            $table->date('coc_expiry_date')->nullable();
            $table->string('coc_grade')->nullable();
            $table->string('other_coc')->nullable();
            $table->string('other_coc_number')->nullable();
            $table->date('other_coc_expiry_date')->nullable();
            $table->string('other_coc_grade')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_coc_details');
    }
}
