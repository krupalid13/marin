<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserGmdssDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_gmdss_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('gmdss')->nullable();
            $table->string('gmdss_number')->nullable();
            $table->date('gmdss_expiry_date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_gmdss_details');
    }
}
