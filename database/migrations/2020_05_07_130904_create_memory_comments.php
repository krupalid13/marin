<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMemoryComments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('memory_comments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->unsigned();
            $table->bigInteger('memory_id')->unsigned()->nullable(true);
            $table->foreign('memory_id')->references('id')->on('memories')->onDelete('cascade');
            $table->string('comment')->nullable(true);
            $table->string('reply')->nullable(true);
            $table->bigInteger('comment_id')->nullable(true)->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('memory_comments');
    }
}
