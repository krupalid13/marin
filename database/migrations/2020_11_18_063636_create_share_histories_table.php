<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShareHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('share_histories', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('roll_type')->nullabel()->comment('1 = Resume, 2 = Profile, 3 = Document');
            $table->integer('user_id')->nullabel();
            $table->integer('contact_id')->nullabel();
            $table->string('email')->nullabel();
            $table->dateTime('open_at')->nullabel();
            $table->integer('response_id')->nullabel();
            $table->string('response')->nullabel();
            $table->dateTime('response_at')->nullabel();
            $table->string('ip')->nullabel();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('share_histories');
    }
}
