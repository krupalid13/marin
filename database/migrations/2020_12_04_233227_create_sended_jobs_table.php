<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSendedJobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sended_jobs', function (Blueprint $table) {
            $table->bigInteger('id',20);
            
            $table->integer('user_id')->length(10)->nullable();
            $table->bigInteger('course_job_id')->nullable();
            $table->foreign('course_job_id','course_job_sended_foren_key')
                ->references('id')->on('course_jobs')
                ->onDelete('cascade')
                ->onUpdate('no action');

            $table->enum('applied',[1,2])->default(2);
            $table->date('applied_date')->nullable();
            $table->enum('status',[1,2])->default(2);
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sended_jobs');
    }
}
