<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserSeaService extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('user_sea_services');
        Schema::create('user_sea_services', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('rank_id')->nullable();
            $table->string('company_name')->nullable();
            $table->string('ship_name')->nullable();
            $table->integer('ship_flag')->nullable();
            $table->integer('ship_type')->nullable();
            $table->string('manning_by')->nullable();
            $table->string('grt')->nullable();
            $table->string('nrt')->nullable();
            $table->string('engine_type')->nullable();
            $table->integer('bhp')->nullable();
            $table->date('from')->nullable();
            $table->date('to')->nullable();
            $table->string('total_duration')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_sea_services');
    }
}
