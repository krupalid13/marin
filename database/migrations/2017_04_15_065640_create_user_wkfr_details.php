<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserWkfrDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_wkfr_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('watch_keeping')->nullable();
            $table->string('type')->nullable();
            $table->string('wkfr_number')->nullable();
            $table->string('indos_number')->nullable();
            $table->date('issue_date')->nullable();
            $table->tinyInteger('yellow_fever')->nullable()->default(0);
            $table->date('yf_issue_date')->nullable();
            $table->tinyInteger('status')->default('0');
            $table->string('wk_cop')->default('wk')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_wkfr_details');
    }
}
