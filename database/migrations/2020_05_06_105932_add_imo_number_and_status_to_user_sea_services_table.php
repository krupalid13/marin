<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddImoNumberAndStatusToUserSeaServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_sea_services', function (Blueprint $table) {
            $table->string('imo_number')->nullable(true)->after('total_duration');
            $table->tinyInteger('status')->default('0')->after('imo_number')->comment('0=Not Connected, 1=Connected');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_sea_services', function (Blueprint $table) {
            $table->dropColumn(['imo_number','status']);
        });
    }
}
