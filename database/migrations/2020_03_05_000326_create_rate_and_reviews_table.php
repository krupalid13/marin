<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRateAndReviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rate_and_reviews', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            // $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->unsignedInteger('user_sea_service_id');
            // $table->foreign('user_sea_service_id')->references('id')->on('user_sea_services')->onDelete('cascade');
            $table->double('vessel_rate',5,1)->nullable();
            $table->longText('vessel_comment')->nullable();
            $table->double('company_rate',5,1)->nullable();
            $table->longText('company_comment')->nullable();
            $table->double('manning_rate',5,1)->nullable();
            $table->longText('manning_comment')->nullable();
            $table->longText('experience')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rate_and_reviews');
    }
}
