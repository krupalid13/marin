<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMultiplePassportCountryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('multiple_passport_country', function (Blueprint $table) {
            $table->bigInteger('id',20);
            $table->bigInteger('course_job_id')->nullable();
            $table->foreign('course_job_id','course_job_passport_country_id_foren_key')
                ->references('id')->on('course_jobs')
                ->onDelete('cascade')
                ->onUpdate('no action');
            $table->integer('passport_country_id')->length(11)->nullable();
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('multiple_passport_country');
    }
}
