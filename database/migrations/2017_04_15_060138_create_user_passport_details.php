<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserPassportDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_passport_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('pass_country')->nullable();
            $table->string('pass_number')->nullable();
            $table->string('place_of_issue')->nullable();
            $table->date('pass_expiry_date')->nullable();
            $table->tinyInteger('us_visa')->nullable()->default(0);
            $table->date('us_visa_expiry_date')->nullable();
            $table->tinyInteger('indian_pcc')->nullable()->default(0);
            $table->date('indian_pcc_issue_date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_passport_details');
    }
}
