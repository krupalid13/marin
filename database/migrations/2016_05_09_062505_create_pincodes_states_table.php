<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePincodesStatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pincodes_states', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('pincode_id')->index()->unsigned()->nullable();
            $table->foreign('pincode_id')->references('id')->on('pincodes')->onDelete('set null');

            $table->integer('state_id')->index()->unsigned()->nullable();
            $table->foreign('state_id')->references('id')->on('states')->onDelete('set null');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pincodes_states');
    }
}
