<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddShareHistoryIdInSharedDocsToOthers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('shared_docs_to_others', function (Blueprint $table) {
            //
            $table->integer('share_history_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('shared_docs_to_others', function (Blueprint $table) {
            //
            $table->dropColumn('share_history_id');
        });
    }
}
