<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('dob')->nullable();
            $table->string('gender')->nullable();
            $table->string('email')->unique()->nullable();
            $table->string('password')->nullable();
            $table->string('mobile')->unique()->nullable();
            $table->integer('otp')->nullable();
            $table->tinyInteger('status')->default(0);
            $table->string('profile_pic')->nullable();
            $table->string('registered_as')->nullable();
            $table->tinyInteger('is_mob_verified')->default(0);
            $table->tinyInteger('is_email_verified')->default(0);
            $table->string('added_by')->nullable();
            $table->tinyInteger('welcome_email')->default(0);
            $table->tinyInteger('welcome_sms')->default(0);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
