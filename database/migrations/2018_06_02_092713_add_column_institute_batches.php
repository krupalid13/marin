<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnInstituteBatches extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('institute_batches', function (Blueprint $table) {
            $table->integer('reserved_seats')->nullable();
            $table->string('payment_type')->nullable();
            $table->integer('initial_per')->nullable();
            $table->integer('pending_per')->nullable();
            $table->integer('initial_amt')->nullable();
            $table->integer('pending_amt')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
