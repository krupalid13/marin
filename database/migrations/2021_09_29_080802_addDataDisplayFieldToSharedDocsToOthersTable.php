<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDataDisplayFieldToSharedDocsToOthersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('shared_docs_to_others', function (Blueprint $table) {
            //
            $table->text('data_display')->nullable()->after('data');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('shared_docs_to_others', function (Blueprint $table) {
            //
            $table->dropColumn('data_display');
        });
    }
}
