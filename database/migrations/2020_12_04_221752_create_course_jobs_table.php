<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCourseJobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('course_jobs', function (Blueprint $table) {
            $table->bigInteger('id',20);
            $table->string('unique_code', 255)->nullable();
            
            $table->bigInteger('company_email_address_id')->nullable();
            $table->foreign('company_email_address_id','company_email_address_foren_key')
                ->references('id')->on('company_email_address')
                ->onDelete('cascade')
                ->onUpdate('no action');

            $table->bigInteger('company_id')->nullable();
            $table->foreign('company_id','company_id_foren_key')
                ->references('id')->on('companies')
                ->onDelete('cascade')
                ->onUpdate('no action');

            $table->integer('rank_id')->length(11)->nullable();
            $table->text('image')->nullable();
            $table->text('title')->nullable();
            $table->text('message')->nullable();
            $table->date('valid_till_date')->nullable();
            $table->text('source')->nullable();
            $table->integer('job_status_id')->length(11)->nullable();
            $table->integer('passport_country_id')->length(11)->nullable();
            $table->integer('rank_experience_year')->length(11)->nullable();
            $table->integer('rank_experience_month')->length(11)->nullable();
            $table->integer('salary_type')->length(11)->nullable();
            $table->float('minimum_wages')->nullable();
            $table->float('maximum_wages')->nullable();
            $table->string('coc',255)->nullable();
            $table->integer('coc_country_id')->length(11)->nullable();
            $table->string('coe',255)->nullable();
            $table->integer('coe_country_id')->length(11)->nullable();
            $table->string('gmdss_country_id',255)->nullable();
            $table->string('cop',255)->nullable();
            $table->integer('cop_country_id')->length(11)->nullable();
            $table->string('dp',255)->nullable();
            $table->integer('dp_country_id')->length(11)->nullable();
            $table->integer('course_id')->length(11)->nullable();
            $table->integer('coc_experience_year')->length(11)->nullable();
            $table->integer('coc_experience_month')->length(11)->nullable();
            $table->integer('coe_experience_year')->length(11)->nullable();
            $table->integer('coe_experience_month')->length(11)->nullable();
            $table->integer('dp_experience_year')->length(11)->nullable();
            $table->integer('dp_experience_month')->length(11)->nullable();
            $table->integer('gmdss_experience_year')->length(11)->nullable();
            $table->integer('gmdss_experience_month')->length(11)->nullable();
            $table->integer('cop_experience_year')->length(11)->nullable();
            $table->integer('cop_experience_month')->length(11)->nullable();
            $table->string('experience_ship_type',255)->nullable();
            $table->string('experience_engine_type',255)->nullable();
            $table->integer('certification')->length(11)->nullable();
            $table->string('vaccination',255)->nullable();
            $table->timestamps();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('course_jobs');
    }
}
