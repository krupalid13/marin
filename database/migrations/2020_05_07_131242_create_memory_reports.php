<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMemoryReports extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('memory_reports', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->unsigned();
            $table->bigInteger('memory_id')->unsigned();
            $table->foreign('memory_id')->references('id')->on('memories')->onDelete('cascade');
            $table->tinyInteger('report')->comment('1=Mature Content, 2=Sexually Explicit, 3=Spam, 4=Abusive or Offensive, 5=Not the vessel');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('memory_reports');
    }
}
