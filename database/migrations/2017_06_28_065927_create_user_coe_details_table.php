<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserCoeDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_coe_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('coe')->nullable();
            $table->string('coe_number')->nullable();
            $table->string('coe_grade')->nullable();
            $table->date('coe_expiry_date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_coe_details');
    }
}
