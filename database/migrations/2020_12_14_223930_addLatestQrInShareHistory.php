<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLatestQrInShareHistory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('share_histories', function (Blueprint $table) {
            //
            $table->dateTime('latest_qr')->nullable()->after('is_new');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('share_histories', function (Blueprint $table) {
            //
            $table->dropColumn('latest_qr');
        });
    }
}
