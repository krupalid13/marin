<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyEmailAddressTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_email_address', function (Blueprint $table) {
            $table->bigInteger('id',20);
            $table->bigInteger('company_id')->nullable();
            $table->foreign('company_id','company_foren_key')
                ->references('id')->on('companies')
                ->onDelete('cascade')
                ->onUpdate('no action');
            $table->string('email', 255)->nullable();
            $table->enum('is_default',[1,2])->default(2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company_email_address');
    }
}
