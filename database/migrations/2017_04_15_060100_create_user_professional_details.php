<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserProfessionalDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_professional_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->nullable();
            $table->integer('current_rank')->nullable();
            $table->decimal('current_rank_exp',11,2);
            $table->integer('applied_rank')->nullable();
            $table->date('availability')->nullable();
            $table->string('last_salary')->nullable();
            $table->string('name_of_school')->nullable();
            $table->string('school_from')->nullable();
            $table->string('school_to')->nullable();
            $table->string('school_qualification')->nullable();
            $table->string('institute_name')->nullable();
            $table->string('institute_from')->nullable();
            $table->string('institute_to')->nullable();
            $table->string('institute_degree')->nullable();
            $table->string('currency')->nullable();
            $table->longText('other_exp')->nullable();
            $table->text('about_me')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_professional_details');
    }
}
