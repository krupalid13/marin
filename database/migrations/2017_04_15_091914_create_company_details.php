<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('company_id');
            $table->string('country')->nullable();
            $table->string('state_id')->nullable();
            $table->string('city_id')->nullable();
            $table->string('zip')->nullable();
            $table->string('pincode_id')->nullable();
            $table->string('state_text')->nullable();
            $table->string('city_text')->nullable();
            $table->string('fax')->nullable();
            $table->string('address')->nullable();
            $table->integer('company_type')->nullable();
            $table->text('company_description')->nullable();
            $table->string('company_email')->nullable();
            $table->string('company_contact_number')->nullable();
            $table->string('rpsl_no')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company_details');
    }
}
