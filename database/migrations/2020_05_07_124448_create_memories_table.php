<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMemoriesTable extends Migration {
    public function up() {
        Schema::create('memories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('sea_service_id')->unsigned();
            $table->string('image');
            $table->float('image_size', 7, 2);
            $table->string('title')->nullable(true);
            $table->string('description')->nullable(true);
            $table->timestamps();
        });
    }

    public function down() {
        Schema::dropIfExists('memories');
    }
}
