<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInstituteBatchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('institute_batches', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('institute_id')->nullable();
            $table->string('course_type')->nullable();
            $table->string('course_name')->nullable();
            $table->date('start_date')->nullable();
            $table->integer('duration')->nullable();
            $table->integer('cost')->nullable();
            $table->integer('size')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('institute_batches');
    }
}
