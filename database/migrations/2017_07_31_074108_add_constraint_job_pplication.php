<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddConstraintJobPplication extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('job_application', function (Blueprint $table) {

            $table->integer('job_id')->index()->unsigned()->nullable()->change();
            $table->integer('company_id')->index()->unsigned()->nullable()->change();
            $table->integer('user_id')->index()->unsigned()->nullable()->change();

            $table->foreign('job_id')->references('id')->on('company_jobs');
            $table->foreign('company_id')->references('id')->on('company_registration');
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
