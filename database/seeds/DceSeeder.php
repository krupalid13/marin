<?php

use Illuminate\Database\Seeder;
use App\Dce;

class DceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = getDceList();

        if (!empty($data)) {
        	
        	foreach ($data as $key => $value) {
        		
        		$obj = new Dce;
        		$obj->name = $value;
        		$obj->save();
        	}
        }
	}
}
