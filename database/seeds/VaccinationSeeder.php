<?php

use Illuminate\Database\Seeder;
use App\Vaccination;

class VaccinationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = getVaccinationList();

        if (!empty($data)) {
        	
        	foreach ($data as $key => $value) {
        		
        		$obj = new Vaccination;
        		$obj->name = $value;
        		$obj->save();
        	}
        }
    }
}
