<?php

use Illuminate\Database\Seeder;
use App\JobStatus;

class JobStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = getJobStatusList();

        if (!empty($data)) {
        	
        	foreach ($data as $key => $value) {
        		
        		$obj = new JobStatus;
        		$obj->name = $value;
        		$obj->status = 1;
        		$obj->save();
        	}
        }
    }
}
