$(document).ready(function () {    
    if ($(".upload_files").is(":visible")) {
    	$('.slider-for').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            fade: true,
            asNavFor: '.slider-nav'
          });
          $('.slider-nav').slick({
            slidesToShow: 5,
            slidesToScroll: 1,
            asNavFor: '.slider-for',
            arrows:false,
            dots: false,
            centerMode: true,
            focusOnSelect: true
          });
    }
});