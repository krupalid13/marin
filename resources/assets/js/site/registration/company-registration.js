$(document).ready(function() {

    var check = false;
    var ship_check = false;
    var block_index = 1;
    var location_block_index = 1;
    var ship_block_index = 1;

    jQuery.validator.addMethod("complete_url", function(val, elem) {
        if (val.length == 0) { return true; }

        /*if(!/^(https?|ftp):\/\//i.test(val)) {
            val = 'http://'+val; // set both the value
            $(elem).val(val); // also update the form element
        }*/
        return /^(www\.)[A-Za-z0-9_-]+\.+[A-Za-z0-9.\/%&=\?_:;-]+$/.test(val);
    },"Enter valid URL");

    $("#company-details").on('click',function(e){
        if($("#company-details").hasClass('page-class')){
            $("#company-locations").addClass('page-done page-active');
            $("#company-details").removeClass('page-done page-active');
            $("#company-registration-form").show();
            $("#company-registration-location-details").hide();
            if($("#company-details").hasClass('company-details-tab')){
                $("#company-details-circle").css("background-color","red");
                $("#company-details-circle").css("border","red");
                $(".page-name").css("color","red");
                $(".page-active .page-name").css("color","#47c4f0");
                $("#company-locations-circle").css("background-color","#47c4f0");
            }
        }
       /* $("#imp-documents-circle").css("background-color","red");
        $("#imp-documents-circle").css("border","red");
        $("#basic-details-circle").css("background-color","#47c4f0");
        $(".page-name").css("color","#47c4f0");
        $(".page-active .page-name").css("color","red");*/
    });

    $("#company-locations").on('click',function(e){
        if($("#company-details").hasClass('page-class')){
            //$("#company-details").removeClass('page-done page-active');
            $("#company-locations").addClass('page-done page-active');
            $("#company-registration-form").hide();
            $("#company-registration-location-details").removeClass('hide');
            $("#company-registration-location-details").show();
            check_rpsl();
            if($("#company-details").hasClass('company-details-tab')){
                $("#company-locations-circle").css("background-color","red");
                $("#company-locations-circle").css("border","red");
                $("#company-details-circle").css("background-color","#47c4f0");
                $(".page-name").css("color","#47c4f0");
                $(".page-active .page-name").css("color","red");
                $("#company-details .page-name").css("color","#47c4f0");

            }
        }
        /*$("#basic-details-circle").css("background-color","red");
        $("#basic-details-circle").css("background-color","#47c4f0");
        $(".page-name").css("color","#47c4f0");
        $(".page-active .page-name").css("color","red");*/
    });
    var regex = /^[a-zA-Z ]*$/;
    $('#company-registration-form').validate({
        
        focusInvalid: false,
        invalidHandler: function(form, validator) {

            if (!validator.numberOfInvalids())
                return;

            $('html, body').animate({
                scrollTop: $(validator.errorList[0].element).offset().top + (-200)
            }, 200);

        },

        rules: {
            email: {
                required: true,
                email: true,
                remote: {
                    url: $("#api-check-email").val(),
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content'),
                    },
                    type: "post",
                    async: false,
                    data: {
                        id: function(){ return $("input[name='company_id']").val()},
                        email: function(){ return $("#email").val(); }
                    }
                }
            },
            password: {
                minlength: 6,
                required: true
            },
            cpassword: {
                required: true,
                minlength: 5,
                equalTo: "#password"
            },
            contact_person_number: {
                required: true,
                number: true,
                minlength: 10,
                remote: {
                    url: $("#api-check-mobile").val(),
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content'),
                    },
                    type: "post",
                    async: false,
                    data: {
                        id: function(){ return $("input[name='company_id']").val()},
                        mobile: function(){ return $("#contact_person_number").val(); }
                    }
                }
            },
            contact_person: {
                required: true,
                number: false
            },
            contact_person_email: {
                required: true,
                email: true
            },
            company_name: {
                minlength: 2,
                required: true
            },
            company_type: {
                required: true
            },
            company_description: {
                required: true
            },
            company_email: {
                required: true,
                email: true
            },
            company_contact_number: {
                number: true
            },
            website: {
                complete_url: true
            },
            fax : {
                minlength: 6
            },
            rpsl_no : {
                required: true
            },
            agree : {
                required: true
            }
        },
        messages: {
            email: {
                required: "We need your email address to contact you",
                email: "Your email address must be in the format of name@domain.com",
                remote: "Email already exist"
            },
            contact_person_email: {
                required: "We need your email address to contact you",
                email: "Your email address must be in the format of name@domain.com"
            },
            cpassword: {
                equalTo: "Confirm password is not matching with password "
            },
            rank_exp: {
                number: "Please enter experience in numbers"
            },
            contact_person_number: {
                number: "Please enter valid Mobile number",
                remote: "Mobile already exist"
            },
            mobile: {
                number: "Please enter valid Mobile number",
                remote: "Mobile already exist"
            },
            agree: {
                required: "Please agree to the terms and conditions."
            },
        },
    });

    $("#company-registration-location-details").validate({
        focusInvalid: false,
        invalidHandler: function(form, validator) {

            if (!validator.numberOfInvalids())
                return;

            $('html, body').animate({
                scrollTop: $(validator.errorList[0].element).offset().top + (-200)
            }, 200);

        },
        rules : {
            incorporation_number : {
                required: true
            }/*,
            pancard_number : {
                required: true
            }*/
        }
    });

    function check_rpsl(){
        if($("#company_type").val() == 1){
            $("#rpsl-file-container").removeClass('hide');
        }else{
            $("#rpsl-file-container").addClass('hide');
        }

    }

    $("#company-details-submit").on('click',function(e){
        e.preventDefault();
        $('#company-registration-form').valid();
        var l = Ladda.create(this);
        if($('#company-registration-form').valid()){
            l.start();
            $.ajax({
                type: "POST",
                url: $("#company-registration-form").attr('action'),
                data: $("#company-registration-form").serialize(),
                statusCode: {
                    200:function (response) {
                        //window.location.href = response.redirect_url;
                        $(window).scrollTop(0);
                        $(".alert-box").addClass('hide');
                        $("#company-details").addClass('page-class');
                        $("#company-locations").addClass('page-done page-active page-class').removeClass('disabled cursor-not-allowed');
                        $("#company-registration-form").hide();
                        $("#company-registration-location-details").removeClass('hide');
                        $("#company-registration-location-details").show();
                        check_rpsl();
                        if($("#company-details").hasClass('company-details-tab')){
                            $("#company-locations-circle").css("background-color","red");
                            $("#company-locations-circle").css("border","red");
                            $("#company-details-circle").css("background-color","#47c4f0");
                            $(".page-name").css("color","#47c4f0");
                            $(".page-active .page-name").css("color","red");
                            $("#company-details .page-name").css("color","#47c4f0");
                        }
                        
                        l.stop();
                    },
                    400:function (response) {
                        l.stop();

                    },
                    422:function (response) {

                        for (var prop in response.responseJSON){
                            if(prop == "mobile"){
                                $("#mobile-error").removeClass('hide');
                                $("#mobile-error").text(response.responseJSON['mobile'][0]);
                                $("#mobile").focus();
                            }
                            if(prop == "email"){
                                $("#email-error").removeClass('hide');
                                $("#email-error").text(response.responseJSON['email'][0]);
                                $("#email").focus();
                            }
                        }
                        l.stop();
                        
                        var foo = response.responseJSON;
                        var text = '';
                        for (var prop in foo) {
                            text += foo[prop][0]+ '<br />';
                        }
                        $(".alert-box").show();
                        $(".alert-box").html(text);

                        $('html,body').animate({
                            scrollTop: ($("#alert-box").offset().top - 100) + 'px'},
                        'slow');
                        l.stop();
                    }
                }
            });
        }
    });

    /*$.validator.addMethod("pRequired", $.validator.methods.required,
        "Please upload pancard copy");*/

    $.validator.addMethod("iRequired", $.validator.methods.required,
        "Please upload incorporation certificate");

    /*$.validator.addMethod("rRequired", $.validator.methods.required,
        "Please upload RPSL certificate");*/


    /*$.validator.addClassRules({
        pancard_required: {
            pRequired: true
        }
    });*/

    $.validator.addClassRules({
        incorporation_required: {
            iRequired: true
        }
    });

    /*$.validator.addClassRules({
        rpsl_required: {
            rRequired: true
        }
    });*/

    $("#company-locations-submit-button").on('click',function(e){

         e.preventDefault();
        $("[name^='country']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select country"
                }
            });
        });

        $("[name^='pincode']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please enter pincode"
                }
            } );
        });

        $("[name^='state']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select state"
                }
            } );
        });

        $("[name^='state_text']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select state"
                }
            } );
        });

        $("[name^='city']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select city"
                }
            } );
        });

        $("[name^='city_text']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select city"
                }
            } );
        });

        $("[name^='address']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please enter address"
                }
            } );
        });

        $("[name^='ship_type']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select ship type"
                }
            } );
        });

        $("[name^='ship_name']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please enter ship name"
                }
            } );
        });

        $("[name^='telephone']").each(function(){
            $(this).rules("add", {
                number: true,
                messages: {
                    required: "Please enter numbers only"
                }
            } );
        });

        /*$("[name^='ship_flag']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select ship flag"
                }
            } );
        });*/

        $("[name^='grt']").each(function(){
            $(this).rules("add", {
                required: true,
                number: true,
                messages: {
                    required: "Please enter GRT"
                }
            } );
        });

        $("[name^='engine_type']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select engine type"
                }
            } );
        });

        $("[name^='bhp']").each(function(){
            $(this).rules("add", {
                required: true,
                number: true,
                messages: {
                    required: "Please enter bhp"
                }
            } );
        });

        /*$("[name^='built-year']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select year of built"
                }
            } );
        });*/

        $("[name^='p_i_cover_company_name']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please enter company name"
                }
            } );
        });


        if($("#company-registration-location-details").valid()){
            
            var l = Ladda.create(this);
            var data = new FormData();
            var other_fields = $("#company-registration-location-details").serializeArray();
            $.each(other_fields, function(key,input){
                data.append(input.name,input.value);
            });
            $("input[type='file']").each(function(index,element){
                if($(this)[0].files.length > 0){
                    var temp_file_anme = $(this).attr('name');
                    var temp_arr = temp_file_anme.split('_1');
                    var file_name = temp_arr[0];
                    data.append(file_name,$(this)[0].files[0]);
                }
            });
            l.start();
            $.ajax({
                type: "POST",
                url: $("#company-registration-location-details").attr('action'),
                data: data,
                contentType: false,
                processData: false,
                statusCode: {
                    200:function (response) {
                        window.location.href = response.redirect_url;
                        
                        $('#company-registration-form').hide();
                        $("#company-registration-location-details").removeClass('hide');
                        $("#company-locations").addClass('page-done page-active');
                        
                        l.stop();
                    },
                    400:function (response) {
                        l.stop();

                    },
                    422:function (response) {

                        var foo = response.responseJSON;
                        var text = '';
                        for (var prop in foo) {
                            text += foo[prop][0]+ '<br />';
                        }
                        $(".alert-box").show();
                        $(".alert-box").html(text);
                        l.stop();

                    }
                }
            });
        }

    });

    $(document).on('change','.is_headoffice',function (e) {
        e.preventDefault();

        if( $(this).val() == 1) {
            if( $('input[type="radio"][value="1"][class="is_headoffice"]:checked').length > 1 ) {

                var name_attr_value = $(this).attr('name');
                $('input[type="radio"][class="is_headoffice"]').prop('checked',false);
                $('input[type="radio"][value="0"][class="is_headoffice"]').prop('checked',true);
                $('[name="'+name_attr_value+'"][value="0"]').prop('checked',false);
                $('[name="'+name_attr_value+'"][value="1"]').prop('checked',true);
            }
        }

    });

    $(document).on('click','.add-location-button',function (e) {

        var form_id = $(this).attr('data-form-id');

        var html = $('#locations-template').clone();

        html.attr('block-index', location_block_index);
            html.find("input[type='text']").val("");
            html.find("textarea").val("");
            html.find("select").val("");

            if($(".total_location_block_index").length > 0 && check == false){
                block_index = parseInt($(".total_location_block_index").last().val())+1;
                check = true;
            }
            
            var location_index = block_index + 1;
            var new_location = parseInt($(".location-name").length) + 1;
            html.find(".location-name").text('Company Location '+ new_location);

            var prev_index = location_block_index - 1;
            html.find('.state-block').removeClass('state-block-'+prev_index);
            html.find('.city-block').removeClass('city-block-'+prev_index);
            html.find('.pincode-block').removeClass('pincode-block-'+prev_index);
            html.find('.address-block').removeClass('address-block-'+prev_index);
            html.find('.telephone-block').removeClass('telephone-block-'+prev_index);

            //var block_index = parseInt($('#location-details-form .country').last().attr('block-index'))+1;
            html.find('.state-block').addClass('state-block-'+block_index);
            html.find('.city-block').addClass('city-block-'+block_index);
            html.find('.telephone-block').addClass('telephone-block-'+block_index);
            html.find('.pincode-block').addClass('pincode-block-'+block_index);
            html.find('.address-block').addClass('address-block-'+block_index);
            html.find('.pincode-loader').addClass('pincode-loader-'+block_index);
            html.find('.pincode-loader').removeClass('pincode-loader-0');
            //html.find('input[type="radio"]').removeAttr('checked');
            html.find('input[type="radio"][value="1"]').prop('checked', false);
            html.find('input[type="radio"][value="0"]').prop('checked', true);

            html.find(".country").attr('block-index',block_index);
            html.find(".country").attr('name','country['+block_index+']');
            html.find(".state").attr('name','state['+block_index+']');
            html.find(".city").attr('name','city['+block_index+']');

            html.find(".state_text").attr('name','state_text['+block_index+']');
            html.find(".city_text").attr('name','city_text['+block_index+']');

            html.find(".address").attr('name','address['+block_index+']');
            html.find(".telephone").attr('name','telephone['+block_index+']');

            html.find(".pincode-id").attr('name','pincode_id['+block_index+']');

            html.find(".pincode").attr('name','pincode['+block_index+']');
            html.find(".pincode").attr('block-index',block_index);
            html.find(".pincode").attr('data-form-id',form_id);

            html.find(".address").attr('name','address['+block_index+']');
            html.find(".address-block").attr('address-index',block_index);

            html.find(".is_headoffice").attr('name','is_headoffice['+block_index+']');

            html.find(".close-button").addClass('close-button-'+block_index).removeClass('close-button-0').removeClass('hide');
            html.find(".close-button").attr('id','close-button-'+block_index);

            html.find(".add-location-button").addClass('location-button-'+block_index).removeClass('location-button-0').removeClass('hide');

            //$(".add-location-button").addClass('hide');

            html.find('.add-remove-container').append('<button type="button" class="btn btn-warning remove-institute-address-button m-l-15" style="margin-top:21px;">Remove Address</button>');
           
            $("#add-more-location-row").before(html);
            // $(".add-location-template").append(html);


            location_block_index++;
            block_index++;

    });

    $(document).on('click','.close-button',function (e) {
        $(this).closest('#locations-template').remove();

        $('.add-location-button').last().removeClass('hide');
        // $(".location-button-"+last_block_index).removeClass('hide');
         
        setTimeout(function(){ 
            $(".location-name").each(function(index,element){
                var index = index + 1;
                $(this).text('Company Location '+index);
            }); 
        }, 50);
    });

    $(document).on('click','.add-ship-button',function (e) {

        var form_id = $(this).attr('data-form-id');
        var html = $('#ships-template').clone();

        html.find("input[type='text']").val("");
        html.find("select").val("");
        html.find('input[type="radio"][value="0"]').prop('checked', true);
        
        if($(".total_block_index").length > 0 && ship_check == false){
            ship_block_index = parseInt($(".total_block_index").last().val())+1;
            ship_check = true;
        }

        var ship_text_index = ship_block_index + 1;
        html.find(".ship-name").text('Ship Details '+ship_text_index);

        html.find(".ship_type").attr('name','ship_type['+ship_block_index+']');
        html.find(".ship_name").attr('name','ship_name['+ship_block_index+']');
        html.find(".ship_flag").attr('name','ship_flag['+ship_block_index+']');
        html.find(".grt").attr('name','grt['+ship_block_index+']');
        html.find(".engine_type").attr('name','engine_type['+ship_block_index+']');
        html.find(".bhp").attr('name','bhp['+ship_block_index+']');
        html.find(".built-year").attr('name','built-year['+ship_block_index+']');
        html.find(".voyage").attr('name','voyage['+ship_block_index+']');
        html.find(".p-i-cover").attr('name','p-i-cover['+ship_block_index+']');
        html.find(".p_i_cover_company_name").attr('name','p_i_cover_company_name['+ship_block_index+']');

        var prev_index = ship_block_index - 1;
        html.find("#pi_cover_index").removeClass('pi_cover_index_0');
        html.find("#pi_cover_index").addClass('pi_cover_index_'+ship_block_index).addClass('hide');
        html.find(".p-i-cover").attr('block-index',ship_block_index);

        html.find(".ship-close-button").addClass('ship-close-button-'+ship_block_index).removeClass('ship-close-button-0').removeClass('hide');
        html.find(".ship-close-button").attr('id','ship-close-button-'+ship_block_index);
        html.append('<hr>');
        $("#add-more-ships-row").before(html);
        //$('.add-ship-template').append(html);
        ship_block_index++;
    });

    $(document).on('change','.p-i-cover',function (e) {
        e.preventDefault();
        var block_index = $(this).attr('block-index');

        if( $(this).val() == 1) {
            $('.pi_cover_index_'+block_index).removeClass('hide');
        }else{
            $('.pi_cover_index_'+block_index).addClass('hide');
        }

    });

    $(document).on('click','.ship-close-button',function (e) {
        $(this).closest('#ships-template').remove();
    });

    $("#company_type").on('change', function(e){
        if($(this).val() == 1){
            $(".rpsl_number_textbox").removeClass('hide');
        }else{
            $(".rpsl_number_textbox").addClass('hide');
        }
    });

    $(".edit_link").on('click',function(e){
        window.location.href = $(this).attr('href')+'#'+$(this).attr('data-form');
    });

   $(window).load(function() {
        var x = location.hash;
        if( x != ''){
            var res = x.split("#");
            if(res[1] == 'locations'){
                $("#company-details").addClass('page-class');
                $("#company-locations").addClass('page-done page-active page-class').removeClass('disabled cursor-not-allowed');
                $("#company-registration-form").hide();
                $("#company-registration-location-details").removeClass('hide');
                $("#company-registration-location-details").show();
                if($("#company-details").hasClass('company-details-tab')){
                    $("#company-locations-circle").css("background-color","red");
                    $("#company-locations-circle").css("border","red");
                    $("#company-details-circle").css("background-color","#47c4f0");
                    $(".page-name").css("color","#47c4f0");
                    $(".page-active .page-name").css("color","red");
                    $("#company-details .page-name").css("color","#47c4f0");
                }
            }
        }
   });

   $('#add-advertise-form').validate({
        focusInvalid: false,
        invalidHandler: function(form, validator) {

            if (!validator.numberOfInvalids())
                return;

            $('html, body').animate({
                scrollTop: $(validator.errorList[0].element).offset().top + (-200)
            }, 200);

        },
        rules: {
            company_id: {
                required: true,
            },
            advertise_img: {
                required: true
            },
        }
   });

    $("#submitAdvertiseDetailButton").on('click',function(e){
        e.preventDefault();
        var l = Ladda.create(this);

        if($('#add-advertise-form').valid()){ 

            var data = new FormData();
            $("input[type='file']").each(function(index,element){
                data.append('img_obj',$(".fileupload-exists input[type='file']")[0].files[0]);

                if($('#company_id').length > 0){
                    data.append('company_id',$('#company_id').val());
                }
                if($('#role').length > 0){
                    data.append('role',$('#role').val());
                }
            });

            l.start();
            $.ajax({
                type: "POST",
                url: $("#add-advertise-form").attr('action'),
                data: data,
                contentType : false,
                processData: false,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                },
                statusCode: {
                    200:function (response) {
                        window.location.href = response.redirect_url;
                        window.scrollTo(0,0);
                        l.stop();
                    },
                    400:function (response) {
                        l.stop();

                    }
                }
            });

        }
    });

    $("#logo-pic").on('click',function(e){
        $("#advModal").modal('show');
    });

    $(".advertise-file-uplaod").change(function(e) {
        var file, img;
        var ext = $(".advertise-file-uplaod").val().split('.').pop().toLowerCase();

        if ((file = this.files[0])) {
            img = new Image();
            var ValidImageTypes = ["gif", "jpeg", "png", "jpg"];

            if($.inArray(ext, ValidImageTypes) < 0) {
                $(".advertise-file-uplaod").val('');
                $('.fileupload-new img').attr('src','/images/no-image-advertisements.png');
                $('.fileupload-preview img').attr('src','/images/no-image-advertisements.png');
                $("#advertise_upload_error").text('Please upload image file only.');
            }else{
                 $("#advertise_upload_error").text('');
            }
            
            img.onerror = function() {
                $(".advertise-file-uplaod").val('');
                $('.fileupload-new img').attr('src','/images/no-image-advertisements.png');
                $('.fileupload-preview img').attr('src','/images/no-image-advertisements.png');
                $('.fileupload-preview.fileupload-exists.thumbnail').empty();
                $('.fileupload-preview.fileupload-exists.thumbnail').append('<img src="/images/no-image-advertisements.png" alt="">');
                $("#advertise_upload_error").text('Please upload image file.');
                
            };
            //img.src = _URL.createObjectURL(file);
        }
    });

    $(".remove_advertise_btn").on('click', function(e){
        $("#advertise_upload_error").text('');
        $(".advertise-file-uplaod").val('');
        $('.fileupload-new img').attr('src','/images/no-image-advertisements.png');
        $('.fileupload-preview img').attr('src','/images/no-image-advertisements.png');
    });

    /*$(".company_subscription").on('click',function(e){
        e.preventDefault();
        var id = $("#company_subscription").data('id');
        var company_id = $("#company_subscription").data('company');
        var role = $("#company_subscription").data('role');
        var url = $("#api-admin-get-user-subscription").val();

        $(".full-cnt-loader").removeClass('hidden');

        $.ajax({
            type: "GET",
            url: url,
            data: {
                id: id,
                role: role,
                company_id: company_id,
            },
            statusCode: {
                200:function (response) {
                    $(".full-cnt-loader").addClass('hidden');
                    $(".data").html(response.template);
                },
                400:function (response) {
                    $(".data").text(response.message);
                    $(".full-cnt-loader").addClass('hidden');
                }
            }
        });
        
    });
*/

    $('#myTab2 a').not(".fa-edit").click(function (e) {
        e.preventDefault();

        var url = $(this).attr("data-url");
        var href = this.hash;
        var pane = $(this);
        
        // ajax load from data-url
        $(href).load(url,function(result){
            pane.tab('show');
        });
    });

    $(document).on('click','.pagination li a',function (e) {

        var url = $(this).attr("href");

        var id = $("#company_subscription").data('id');
        var company_id = $("#company_subscription").data('company');
        var role = $("#company_subscription").data('role');
        url = url+'&company_id='+company_id+'&role='+role+'&id='+id;

        e.preventDefault();

        var href = "#company_subscription";
        var pane = $(this);

        // ajax load from data-url

        $(href).load(url,function(result){
            $("#company_subscription").tab('show');
        });

    });

    $(document).on('click','.view-sub-modal', function (e) {
        var modal_id = $(this).data('modal-id');
        $(".sub-"+modal_id).modal('show');
    });

    $(document).on('click','.scope-radio', function (e) {
        var section = $(this).data('section');   
        
        if($(this).val() == 1){
            $("."+section+"-section").removeClass('hide');
        }else{
            $("."+section+"-section").addClass('hide');
        }
    });

    $(document).on('click','.user_type', function (e) {

        if($(this).val() == 'owners'){
            $(".owner-section").removeClass('hide');
            $(".manager-section").addClass('hide');
        }else{
            $(".manager-section").removeClass('hide');
            $(".owner-section").addClass('hide');
        }
    });

    $('#owner-vessel').validate({
        rules: {
            ship_type: {
                required: true,
            },
            ship_flag: {
                required: true
            },
            engine_type: {
                required: true
            },
            grt: {
                number: true,
            },
            bhp: {
                number: true,
            }, 
        }
    });

    $('#manager-vessel').validate({
        rules: {
            ship_type: {
                required: true,
            },
            ship_flag: {
                required: true
            },
            engine_type: {
                required: true
            },
            grt: {
                number: true,
            },
            bhp: {
                number: true,
            }, 
        }
    });

    $(document).on('click','.save-vessel-details', function (e) {
        
        var l = Ladda.create(this);
        var form = $(this).data('form');
        
        if($('#'+form).valid()){
            l.start();
            $.ajax({
                type: "POST",
                url: $('#'+form).attr('action'),
                data: $('#'+form).serialize(),
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content'),
                },
                statusCode: {
                    200:function (response) {       
                        toastr.success(response.message, 'Success');

                        $('#'+form).trigger("reset");
                        $("."+form).empty();
                        $("."+form).append(response.template);
                        l.stop();
                    },
                    400:function (response) {
                        l.stop();

                    },
                    422:function (response) {
                        l.stop();
                    }
                }
            });
        }
    });

    $(document).on('click','.vessel-close-button', function (e) {  
        
        var vessel_id = $(this).attr('data-vessel-id');
        var scope = $(this).attr('data-scope');


        if(typeof($("#api-admin-company-delete-vessel-by-vessel-id").val()) != 'undefined'){
            var temp_url = $("#api-admin-company-delete-vessel-by-vessel-id").val();
            var url_arr = temp_url.split("%");
            var url = url_arr[0] + parseInt($('.company_id').val());
        }else{
            var url = $("#api-company-delete-vessel-by-vessel-id").val();
        }

        
        var form = "#"+scope+"-vessel";

        $.ajax({
            type: "POST",
            url: url,
            data: {
                'vessel_id' : vessel_id,
                '_token' : $("input[name='_token']").val()
            },
            statusCode: {
                200:function (response) {
                    
                    toastr.success(response.message, 'Success');

                    $(".vessel_section_"+vessel_id).remove();
                    
                    if($(form+" .vessel-section .vessel-name").length == '0'){
                        $(form).append("<div class='row no-data-found'>"+
                            "<div class='col-xs-12 text-center'>"+
                                "<div class='discription'>"+
                                    "<span class='content-head'>No Data Found</span>"+
                                "</div>"+
                            "</div>"+
                        "</div>");
                    }else{
                         $(form+" .vessel-section .vessel-name").each(function(index,element){
                            var index = index + 1;
                            $(this).text('Vessel '+index);
                        });
                    }
                    
                },
                400:function (response) {
                    toastr.error(response.responseJSON.message, 'Error');
                }
            }
         });
    });

    $(document).on('click','.vessel-edit-button',function (e) {

        var vessel_id = $(this).attr('data-vessel-id');
        var scope = $(this).attr('data-scope');
        $('.existing_vessel_id').val(vessel_id);

        var form = "#"+scope+"-vessel";
        window.scrollTo(0,1000);
        
        if(typeof($("#api-admin-company-get-vessel-by-vessel-id").val()) != 'undefined'){
            var temp_url = $("#api-admin-company-get-vessel-by-vessel-id").val();
            var url_arr = temp_url.split("%");
            var url = url_arr[0] + parseInt($('.company_id').val());
        }else{
            url = $("#api-company-get-vessel-by-vessel-id").val();
        }

        $.ajax({
            type: "GET",
            url: url,
            data: {
                'vessel_id' : vessel_id,
                '_token' : $("input[name='_token']").val()
            },
            statusCode: {
                200:function (response) {
                    if(typeof(response.result.result.bhp) != 'object'){
                        $(form+" .bhp").val(response.result.result.bhp);
                    }

                    if(typeof(response.result.result.grt) != 'object'){
                        $(form+" .grt").val(response.result.result.grt);
                    }

                    if(typeof(response.result.result.ship_name) != 'object'){
                        $(form+" .ship_name").val(response.result.result.ship_name);
                    }

                    if(typeof(response.result.result.ship_type) != 'object'){
                        $(form+" .ship_type").val(response.result.result.ship_type);
                    }
                    $('select[name="ship_type"]').select('refresh');

                    if(typeof(response.result.result.ship_flag) != 'object'){
                        $(form+" .ship_flag").val(response.result.result.ship_flag);
                    }
                    $('select[name="ship_flag"]').select('refresh');

                    if(typeof(response.result.result.engine_type) != 'object'){
                        $(form+" .engine_type").val(response.result.result.engine_type);
                    }
                    $('select[name="ship_type"]').select('refresh');

                    $(form+" input[name=p-i-cover][value=" + response.result.result.p_i_cover + "]").prop('checked', true);

                },
                400:function (response) {
                   toastr.error(response.responseJSON.message, 'Error');
                }
            }
        });
    });
    
    $('#appointed_agent').validate({
        rules: {
            company_name: {
                required: true,
            },
            location: {
                required: true
            },
        }
    });

    $('#associate_agent').validate({
        rules: {
            company_name: {
                required: true,
            },
            location: {
                required: true
            },
        }
    });

    $(document).on('click','.save-agent-details', function (e) {
        
        var l = Ladda.create(this);
        var form = $(this).data('form');
        
        if($('#'+form).valid()){
            l.start();
            $.ajax({
                type: "POST",
                url: $('#'+form).attr('action'),
                data: $('#'+form).serialize(),
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content'),
                },
                statusCode: {
                    200:function (response) {       
                        toastr.success(response.message, 'Success');

                        $('#'+form).trigger("reset");
                        $("."+form).empty();
                        $("."+form).append(response.template);
                        $('#'+form+" #preview").attr('src','');
                        $('#'+form+" .existing_agent_id").val('');
                        $('#'+form+" .profile_pic_text").show();
                        l.stop();
                    },
                    400:function (response) {
                        l.stop();

                    },
                    422:function (response) {
                        l.stop();
                    }
                }
            });
        }
    });

    $(document).on('click','.agent-close-button', function (e) {  
        
        var agent_id = $(this).attr('data-agent-id');
        var scope = $(this).attr('data-scope');

        if(typeof($("#api-admin-company-delete-agent-by-agent-id").val()) != 'undefined'){
            var temp_url = $("#api-admin-company-delete-agent-by-agent-id").val();
            var url_arr = temp_url.split("%");
            var url = url_arr[0] + parseInt($('.company_id').val());
        }else{
            var url = $("#api-company-delete-agent-by-agent-id").val();
        }

        $(".agent_section_"+agent_id).remove();
        var form = "#"+scope+"_agent";
        var form_class = "."+scope+"_agent";

        $.ajax({
            type: "POST",
            url: url,
            data: {
                'agent_id' : agent_id,
                '_token' : $("input[name='_token']").val()
            },
            statusCode: {
                200:function (response) {
                    
                    toastr.success(response.message, 'Success');

                    $(".agent_section_"+agent_id).remove();
                    
                    if($(form+" "+form_class+" .vessel-section .agent-name").length == '0'){
                        $(form).append("<div class='row no-data-found'>"+
                            "<div class='col-xs-12 text-center'>"+
                                "<div class='discription'>"+
                                    "<span class='content-head'>No Data Found</span>"+
                                "</div>"+
                            "</div>"+
                        "</div>");
                    }else{
                         $(form+" "+form_class+" .vessel-section .agent-name").each(function(index,element){
                            var index = index + 1;
                            $(this).text('Agent '+index);
                        });
                    }
                    
                },
                400:function (response) {
                    toastr.error(response.responseJSON.message, 'Error');
                }
            }
         });
    });

    $(document).on('click','.agent-edit-button',function (e) {

        var agent_id = $(this).attr('data-agent-id');
        var scope = $(this).attr('data-scope');
        $('.existing_agent_id').val(agent_id);

        var form = "#"+scope+"_agent";
        
        if(typeof($("#api-admin-company-get-agent-by-agent-id").val()) != 'undefined'){
            var temp_url = $("#api-admin-company-get-agent-by-agent-id").val();
            var url_arr = temp_url.split("%");
            var url = url_arr[0] + parseInt($('.company_id').val());
        }else{
            var url = $("#api-company-get-agent-by-agent-id").val();
        }

        $.ajax({
            type: "GET",
            url: url,
            data: {
                'agent_id' : agent_id,
                '_token' : $("input[name='_token']").val()
            },
            statusCode: {
                200:function (response) {  
                    $('html, body').animate({
                        scrollTop: ($('#'+scope+'_agent').offset().top-150)
                    },500);

                    if(typeof(response.result.result.from_company) == 'string'){
                        $(form+" .company_name").val(response.result.result.from_company);
                    }

                    if(typeof(response.result.result.to_company) == 'string'){
                        $(form+" .company_name").val(response.result.result.to_company);
                    }

                    if(typeof(response.result.result.location) == 'string'){
                        $(form+" .location").val(response.result.result.location);
                    }

                    if(response.result.result.logo){
                        $(form+' input[name="uploaded-file-name"]').val(response.result.result.logo);
                        var path = $(form+' .path').val();
                        path = "/" +path + "/" + agent_id + "/" + response.result.result.logo;
                        $(form+" .profile_pic_text").hide();
                        $(form+" #preview").attr('src',path);
                    }

                },
                400:function (response) {
                   toastr.error(response.responseJSON.message, 'Error');
                }
            }
        });
    });

    $('#team-section-form').validate({
        rules: {
            agent_name: {
                minlength: 2,
                required: true
            },
            location_id : {
                required: true
            },
            agent_designation : {
                required: true
            }
        },
    });

    $("#company-team-details-submit").on('click',function(e){
        e.preventDefault();

        $('#team-section-form').valid();
        var l = Ladda.create(this);
        if($('#team-section-form').valid()){
            l.start();
            $.ajax({
                type: "POST",
                url: $("#team-section-form").attr('action'),
                data: $("#team-section-form").serialize(),
                statusCode: {
                    200:function (response) {
                        $("#preview").attr('src','');
                        $(".exiting_team_id").val('');
                        $(".profile_pic_text").show();
                        toastr.success(response.message, 'Success');
                        $("#team-section-form").trigger("reset");
                        $(".company_team_data").empty();
                        $(".company_team_data").append(response.template);
                        l.stop();
                    },
                    400:function (response) {
                        toastr.error(response.responseJSON.message, 'Error');
                        l.stop();
                    }
                }
            });
        }
    });

    $(document).on('click','.company-team-close-button',function(e){
        e.preventDefault();
        var l = Ladda.create(this);
        l.start();

        var team_id = $(this).attr('data-team-id');
        var close_btn_id = $(this).attr('data-id');
        var company_id = $(this).attr('data-company-id');

        if(typeof($("#api-admin-company-delete-team-by-team-id").val()) != 'undefined'){
            var temp_url = $("#api-admin-company-delete-team-by-team-id").val();
            var url_arr = temp_url.split("%");
            var url = url_arr[0] + parseInt($('.company_id').val());
        }else{
            var url = $("#api-company-delete-team-by-team-id").val();
        }

        $.ajax({
            type: "POST",
            url: url,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content'),
            },
            data: {'team_id' : team_id,'company_id' : company_id},
            statusCode: {
                200:function (response) {
                    toastr.success(response.message, 'Success');
                    $(".company_team_show_data_"+close_btn_id).remove();
                    l.stop();

                    if($(".team-name").length != '0'){
                        $(".team-name").each(function(index,element){
                            var index = index + 1;
                            $(this).text('Team Agent '+index);
                        });
                    }else{
                        $(".company_team_show_data").append("<div class='row no-data-found'>"+
                            "<div class='col-xs-12 text-center'>"+
                                "<div class='discription'>"+
                                    "<span class='content-head'>No Data Found</span>"+
                                "</div>"+
                            "</div>"+
                        "</div>");
                    }
                },
                400:function (response) {
                    toastr.error(response.responseJSON.message, 'Error');
                    l.stop();
                }
            }
        });
    });

    $(document).on('click','.company-team-edit-button',function (e) {
        e.preventDefault();

        var index = $(this).attr('data-index-id');
        var team_id = $(this).attr('data-team-id');
        
        window.scrollTo(0,0);

        $(".team-name").each(function(index,element){
            var index = index + 1;
            $(this).text('Team Agent '+index);
        });

        if(typeof($("#api-admin-company-update-team-by-team-id").val()) != 'undefined'){
            var temp_url = $("#api-admin-company-update-team-by-team-id").val();
            var url_arr = temp_url.split("%");
            var url = url_arr[0] + parseInt($('.company_id').val());
        }else{
            var url = $("#api-company-update-team-by-team-id").val();
        }


        $.ajax({
            type: "POST",
            url: url,
            data: {
                'team_id' : team_id,
                '_token' : $("input[name='_token']").val()
            },
            statusCode: {
                200:function (response) {

                    $(".exiting_team_id").val(team_id);
                    if(response.team_data.agent_name.length > 0){
                        $(".agent_name").val(response.team_data.agent_name);
                    }
                    if(response.team_data.agent_designation.length > 0){
                        $(".agent_designation").val(response.team_data.agent_designation);
                    }
                    if(response.team_data.location_id){
                        $(".location_id").val(response.team_data.location_id);
                    }

                    if(response.team_data.profile_pic){
                        $('input[name="uploaded-file-name"]').val(response.team_data.profile_pic);
                        var path = $(".path").val();
                        path = "/" +path + "/" + team_id + "/" + response.team_data.profile_pic;
                        $(".profile_pic_text").hide();
                        $("#preview").attr('src',path);
                    }

                },
                400:function (response) {
                    toastr.error(response.responseJSON.message, 'Error');
                }
            }
        });
    });

});