$(document).ready(function() {
    var location_block_index = 1; // variable used while adding new location block for institute.
    var initialize_pincode_maxlength_validation = false;
    var check = false;
    var date = moment().format('DD-MM-YYYY');
    
    if($('.batch-datepicker').length > 0){
        $('.batch-datepicker').datepicker({
            format: "dd-mm-yyyy",
            startDate: date,
            autoClose: true,
            startView: 0
            }).on('changeDate', function(ev) {
                if(ev.viewMode === 0){
                    $(this).closest('.batch-datepicker').datepicker('hide');
                }
        });
    }
    
    $('.batch-datepicker-month').datepicker({
        format: "dd-mm-yyyy",
        startDate: date,
        autoClose: true,
        startView: 1
        }).on('changeMonth', function(ev) {
            console.log(456);
            if(ev.viewMode === 1){
                var month = new Date(ev.date).getMonth() + 1;
                var year = String(ev.date).split(" ")[3];
                
                $(this).datepicker('update', new Date(ev.date.getFullYear(), ev.date.getMonth() + 1, 0));
                $(this).datepicker('hide');
            }
    });

    jQuery.validator.addMethod("complete_url", function(val, elem) {
        if (val.length == 0) { return true; }
        return /^(www\.)[A-Za-z0-9_-]+\.+[A-Za-z0-9.\/%&=\?_:;-]+$/.test(val);
    },"Enter valid URL");


    $(document).on('change',".get_institute_list",function(e){
        var course_type = $('#course_type').val();
        var course = $('.get_institute_list').val();
        
        $.ajax({
            type: "GET",
            url: $("#api-institute-get-by-course").val(),
            data: {
                course_type : course_type,
                course : course,
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            },
            statusCode: {
                200:function (response) {
                    var options,inst_options;
                    $('.select2-select').select2('destroy');
                    $('.inst_option').remove();

                    $.each(response.list, function(key,input){
                        if(typeof input.institute_courses != 'undefined'){
                            $.each(input.institute_courses, function(key1,input1){
                                if(typeof input1.institute_batches != 'undefined'){
                                    options = options + '<option value='+input.id+'>'+input.institute_name+'</option>'
                                }
                            });
                        }
                    });

                    $('.institute_list_by_course').append(options);

                    $('.select2-select').select2({
                        closeOnSelect: false
                    });
                },
                400:function (response) {
                    toastr.error(response.responseJSON.message, 'Error');
                }
            }
        });

    });

    $(document).on('change',".institute_list_by_course",function(e){
        var course_type = $('#course_type').val();
        var course = $('.get_institute_list').val();
        var institute_id = $(this).val();
        
        $.ajax({
            type: "GET",
            url: $("#api-institute-get-by-course").val(),
            data: {
                course_type : course_type,
                course : course,
                institute_id : institute_id,
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            },
            statusCode: {
                200:function (response) {
                    var options;

                    $('.select2-select').select2('destroy');
                    $('.inst_option').remove();

                    $.each(response.list, function(key,input){
                        if(typeof input.institute_courses != 'undefined'){
                            $.each(input.institute_courses, function(key1,input1){
                                if(typeof input1.institute_batches != 'undefined'){
                                    $.each(input1.institute_batches, function(key2,input2){
                                        var d_date = input2.start_date.split("-");
                                        options = options + '<option class="inst_option" value='+input2.id+'>'+input.institute_name+' '+d_date[2]+'-'+d_date[1]+'-'+d_date[0]+'</option>'
                                    });
                                }
                            });
                        }
                    });

                    $('.institute_list').append(options);

                    $('.select2-select').select2({
                        closeOnSelect: false
                    });
                },
                400:function (response) {
                    toastr.error(response.responseJSON.message, 'Error');
                }
            }
        });

    });

    
    $('#add-course-discount-batch').validate({
        focusInvalid: false,
        invalidHandler: function(form, validator) {

            if (!validator.numberOfInvalids())
                return;

            $('html, body').animate({
                scrollTop: $(validator.errorList[0].element).offset().top + (-200)
            }, 200);

        },
        rules: {
            course_type: {
                required: true
            },
            course_name: {
                required: true
            },
            institute_list: {
                required: true
            },
            advertise_list: {
                required: true
            },
            institute_list_by_course: {
                required: true
            },
            discount: {
                required: true,
                number: true,
            },
        }
    }); 

    $(document).on('click',"#add-institute-batch-discount",function(e){

        if($('#add-course-discount-batch').valid()){
            e.preventDefault();
            var l = Ladda.create(this);

            if($('#add-course-discount-batch').valid()){

                l.start();
                $('.discount_result').html('');
                $('.full-cnt-loader').removeClass('hidden');
                $.ajax({
                    url: $("#add-course-discount-batch").attr('action'),
                    type: "POST",
                    data: $("#add-course-discount-batch").serialize(),
                    statusCode: {
                        200:function (response) {
                            $('.full-cnt-loader').addClass('hidden');
                            $('.discount_result').html(response.template);
                            toastr.success(response.message, 'Success');
                            l.stop();
                            $('#add-course-discount-batch')[0].reset();
                            $('.advertise_list').select2("val","");
                            $('.institute_list').select2("val","");
                            //window.location.reload();
                        },
                        400:function (response) {
                            $('.full-cnt-loader').addClass('hidden');
                            $('.discount_result').html(response.responseJSON.template);
                            toastr.error(response.responseJSON.message, 'Error');
                            l.stop();
                        }
                    }
                });

            }
        }

    });

    $(document).on('click','.discount_edit_section',function (e) {
        var id = $(this).data('id');

        $(".institute_list_by_course option").remove();

        $.ajax({
            url: $("#api-edit-course-discount-batch").val(),
            type: "GET",
            data: {
                id : id,
            },
            statusCode: {
                200:function (response) {
                    var inst_options;

                    $.each(response.data.institute_list, function(key,input){
                        inst_options = inst_options + '<option value='+input.id+'>'+input.institute_name+'</option>'
                    });

                    $('.institute_list_by_course').append(inst_options);

                    if(typeof(response.data.institute_id) != 'undefined'){
                        $(".institute_list_by_course").val(response.data.institute_id);
                    }

                    if(typeof(response.data.id) != 'undefined'){
                        $(".existing_id").val(response.data.id);
                    }

                    if(typeof(response.data.course_type) != 'undefined'){
                        $("#course_type").val(response.data.course_type);
                    }

                    if(typeof(response.data.course_option_template) != 'undefined'){
                        $('.get_institute_list').append(response.data.course_option_template);
                    }

                    if(typeof(response.data.course_id) != 'undefined'){
                        $(".get_institute_list").val(response.data.course_id);
                    }

                    if(typeof(response.data.discount) != 'undefined'){
                        $(".discount").val(response.data.discount);
                    }

                    if(typeof(response.data.advertise_id) != 'undefined'){
                        $(".advertise_list").val(response.data.advertise_id).trigger('change');
                    }

                    $('.select2-select').select2('destroy');

                    var options;

                    $.each(response.data.institute_list, function(key,input){
                        if(typeof input.institute_courses != 'undefined'){
                            $.each(input.institute_courses, function(key1,input1){
                                if(typeof input1.institute_batches != 'undefined'){
                                    $.each(input1.institute_batches, function(key2,input2){
                                        options = options + '<option class="inst_option" value='+input2.id+'>'+input.institute_name+' '+input2.start_date+'</option>'
                                    });
                                }
                            });
                        }
                    });

                    $('.institute_list').append(options);

                    $('.select2-select').select2({
                        closeOnSelect: false
                    });

                    if(typeof(response.data.batch_id) != 'undefined'){
                        $(".institute_list").val(response.data.batch_id).trigger('change');
                    }

                    window.scrollTo(0,0);
                },
                400:function (response) {

                }
            }
        });
    });

    $(document).on('click','.add-institute-location-button',function (e) {

        var form_id = $(this).attr('data-form-id');

        var html = $('#locations-template').clone();

        html.attr('block-index', location_block_index);
            html.find("input[type='text']").val("");
            html.find("textarea").val("");
            html.find("select").val("");
            html.find(".location_id").remove();

            if($(".total_location_block_index").length > 0 && check == false){
                block_index = parseInt($(".total_location_block_index").last().val())+1;
                check = true;
            }

            var location_index = block_index + 1;
            html.find(".location-name").text('Location '+location_index);

            var prev_index = location_block_index - 1;
            html.find('.state-block').removeClass('state-block-'+prev_index);
            html.find('.city-block').removeClass('city-block-'+prev_index);
            html.find('.pincode-block').removeClass('pincode-block-'+prev_index);
            html.find('.address-block').removeClass('address-block-'+prev_index);

            //var block_index = parseInt($('#location-details-form .country').last().attr('block-index'))+1;
            html.find('.state-block').addClass('state-block-'+block_index);
            html.find('.city-block').addClass('city-block-'+block_index);
            html.find('.pincode-block').addClass('pincode-block-'+block_index);
            html.find('.address-block').addClass('address-block-'+block_index);
            html.find('.pincode-loader').addClass('pincode-loader-'+block_index);
            html.find('.pincode-loader').removeClass('pincode-loader-0');
            //html.find('input[type="radio"]').removeAttr('checked');
            html.find('input[type="radio"][value="1"]').prop('checked', false);
            html.find('input[type="radio"][value="0"]').prop('checked', true);

            html.find(".country").attr('block-index',block_index);
            html.find(".country").attr('name','country['+block_index+']');
            html.find(".state").attr('name','state['+block_index+']');
            html.find(".city").attr('name','city['+block_index+']');

            html.find(".state_text").attr('name','state_text['+block_index+']');
            html.find(".city_text").attr('name','city_text['+block_index+']');

            html.find(".address").attr('name','address['+block_index+']');

            html.find(".pincode-id").attr('name','pincode_id['+block_index+']');

            html.find(".pincode").attr('name','pincode['+block_index+']');
            html.find(".pincode").attr('block-index',block_index);
            html.find(".pincode").attr('data-form-id',form_id);

            html.find(".address").attr('name','address['+block_index+']');
            html.find(".address-block").attr('address-index',block_index);

            html.find(".is_headoffice").attr('name','is_headoffice['+block_index+']');
            html.find(".branch_type").attr('name','branch_type['+block_index+']');

            html.find(".loc_institute_email").attr('name','loc_institute_email['+block_index+']');
            html.find(".loc_phone_number").attr('name','loc_phone_number['+block_index+']');
            html.find(".loc_contact_person").attr('name','loc_contact_person['+block_index+']');
            html.find(".loc_contact_designation").attr('name','loc_contact_designation['+block_index+']');
            html.find(".loc_contact_phone").attr('name','loc_contact_phone['+block_index+']');

            html.find(".close-button").addClass('close-button-'+block_index).removeClass('close-button-0').removeClass('hide');
            html.find(".close-button").attr('id','close-button-'+block_index);

            html.find(".add-location-button").addClass('location-button-'+block_index).removeClass('location-button-0').removeClass('hide');

            //$(".add-location-button").addClass('hide');

            html.find('.add-remove-container').append('<button type="button" class="btn btn-warning remove-institute-address-button m-l-15" style="margin-top:21px;">Remove Address</button>');
            $("#add-more-location-row").before(html);
            // $(".add-location-template").append(html);


            location_block_index++;
            block_index++;

    });

    $(document).on('click','.close-button',function (e) {
        
        $(this).closest('#locations-template').remove();

        $(".location-name").each(function(index,element){
            var index = index + 1;
            $(this).text('Location '+index);
        });
    });

    $(document).on('change', '.country', function () {
       var block_index = $(this).attr('block-index');
       if( $(this).val() == india_value ) {
           $(".state-block-"+block_index).find('.fields-for-india').removeClass('hide');
           $(".state-block-"+block_index).find('.fields-not-for-india').addClass('hide');

           $(".city-block-"+block_index).find('.fields-for-india').removeClass('hide');
           $(".city-block-"+block_index).find('.fields-not-for-india').addClass('hide');

           $("[name='pincode["+block_index+"]']").rules("add", {
               maxlength: 6,
               messages: {
                   maxlength: "Please provide valid 6 digit pincode"
               }
           });

           $("[name='pincode["+block_index+"]']").addClass('pin_code_fetch_input');
       } else {
           $(".state-block-"+block_index).find('.fields-for-india').addClass('hide');
           $(".state-block-"+block_index).find('.fields-not-for-india').removeClass('hide');

           $(".city-block-"+block_index).find('.fields-for-india').addClass('hide');
           $(".city-block-"+block_index).find('.fields-not-for-india').removeClass('hide');

           $("[name='pincode["+block_index+"]']").removeClass('pin_code_fetch_input');
           
           $("[name='pincode["+block_index+"]']").rules('remove','maxlength');


       }
    });

    $('#institute-registration-form').validate({
        focusInvalid: false,
        invalidHandler: function(form, validator) {

            if (!validator.numberOfInvalids())
                return;

            $('html, body').animate({
                scrollTop: $(validator.errorList[0].element).offset().top + (-200)
            }, 200);

        },
        rules: {
            email: {
                required: true,
                email: true,
                remote: {
                    url: $("#api-check-email").val(),
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content'),
                    },
                    type: "post",
                    async: false,
                    data: {
                        id: function(){ return $("#institute_id").val() },
                        email: function(){ return $("#email").val(); }
                    }
                }
            },
            password: {
                minlength: 6,
                required: true
            },
            cpassword: {
                required: true,
                minlength: 6,
                equalTo: "#password"
            },
            contact_person_number: {
                required: true,
                number: true,
                minlength: 10,
                remote: {
                    url: $("#api-check-mobile").val(),
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content'),
                    },
                    type: "post",
                    async: false,
                    data: {
                        id: function(){ return $("#institute_id").val()},
                        mobile: function(){ return $("#contact_person_number").val(); }
                    }
                }
            },
            contact_person: {
                minlength: 2,
                required: true
            },
            contact_person_email: {
                required: true,
                email: true
            },
            institute_name: {
                minlength: 2,
                required: true
            },
            institute_description: {
                required: true
            },
            institute_email: {
                required: true,
                email: true
            },
            institute_contact_number: {
                required: true,
                number: true,
                minlength: 10
            },
            fax : {
                minlength: 6
            },
            pincode: {
                required: true,
                number: true,
                minlength: 5
            },
            address: {
                required: true
            },
            website: {
                complete_url: true
            },
            agree : {
                required: true
            }
        },
        messages: {
            email: {
                required: "We need your email address to contact you",
                email: "Your email address must be in the format of name@domain.com",
                remote: "Email already exist"
            },
            contact_person_email: {
                required: "We need your email address to contact you",
                email: "Your email address must be in the format of name@domain.com"
            },
            cpassword: {
                equalTo: "Confirm password is not matching with password "
            },
            mobile: {
                number: "Please enter valid Mobile number"
            },
            contact_person_number: {
                number: "Please enter valid Mobile number",
                remote: "Mobile already exist"
            },
            agree: {
                required: "Please agree to the terms and conditions."
            }
        },
    });

    $("#institute-details-submit").on('click',function(e){
        e.preventDefault();
        var l = Ladda.create(this);

        $("[name^='country']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select country"
                }
            });
        });

        $("[name^='pincode']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please enter pincode"
                }
            } );
        });

        $("[name^='state']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select state"
                }
            } );
        });

        $("[name^='state_text']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select state"
                }
            } );
        });

        $("[name^='city']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select city"
                }
            } );
        });

        $("[name^='city_text']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select city"
                }
            } );
        });

        $("[name^='address']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please enter address"
                }
            } );
        });

        $("[name^='branch_type']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select branch type"
                }
            } );
        });

        $("[name^='loc_contact_phone']").each(function(){
            $(this).rules("add", {
                number: true,
            } );
        });

        $("[name^='loc_phone_number']").each(function(){
            $(this).rules("add", {
                number: true,
            } );
        });

        $("[name^='loc_institute_email']").each(function(){
            $(this).rules("add", {
                email: true,
            } );
        });

        $('#institute-registration-form').valid();
        if($('#institute-registration-form').valid()){
            l.start();
            $.ajax({
                url: $("#institute-registration-form").attr('action'),
                type: "POST",
                data: $("#institute-registration-form").serialize(),
                statusCode: {
                    200:function (response) {
                        // if($("#current-route-name").val() == 'site.institute.registration'){
                        //     window.location.href = $("#api-auto-welcome-route").val();
                        // }else{
                        //     window.location.href = response.redirect_url;
                        // }
                        window.location.href = response.redirect_url;
                        l.stop();
                    },
                    400:function (response) {
                        l.stop();
                    },
                    422:function (response) {

                        var foo = response.responseJSON;
                        var text = '';
                        for (var prop in foo) {
                            text += foo[prop][0]+ '<br />';
                        }
                        $(".alert-box").show();
                        $(".alert-box").html(text);
                        l.stop();

                    }
                }
            });
        }
    });

    // add rule to the first pincode field
    if( initialize_pincode_maxlength_validation == 1 || initialize_pincode_maxlength_validation == true ) {
        $.each(pincode_block_indexes, function (index, data) {
            $("[name='pincode["+data+"]").rules("add", {
                maxlength: 6,
                messages: {
                    maxlength: "Please provide valid 6 digit pincode"
                }
            });
        });
    }

    $(".institute_course_name").on('change', function(e){
        if($(this).val() == 'other'){
            $("#other_course_name").removeClass('hide');
        }else{
            $("#other_course_name").addClass('hide');
        }
    });

    $(document).on('change',"#course_type",function(e){
        e.preventDefault();
        
        var id = $(this).val();
        var temp_url = $("#api-institute-course-name-by-course-types").val();
        var url_arr = temp_url.split("%");
        var url = url_arr[0] + parseInt($(this).val());
        
        $(".other_course_name").val('');
        $("#other_course_name").addClass('hide');

        if($('#institute_id').length > 0){
            url = url +'/'+parseInt($('#institute_id').val());
        }

        pathname = window.location.pathname;
        is_all = false;
        if(pathname == '/institute/course/batches/add') {
            is_all = false; 
        }else if(pathname == '/') {
            is_all = true;
        }
        
        if(id.length > 0){
            $.ajax({
                type: "get",
                url: url + '?is_all='+is_all,
                statusCode: {
                    200:function (response) {
                        $(".institute_course_name option").remove();
                        if(response.length > 0){
                            $(".institute_course_name").append("<option value=''>Select Course Name</option>");
                            $.each(response,function(index,value){
                                if($('#institute_id').length > 0){
                                    $(".institute_course_name").append('<option value='+value.id+'>'+value.course_name+'</option>');
                                }else
                                    $(".institute_course_name").append('<option value='+value.id+'>'+value.course_name+'</option>');

                            });
                        }else{
                            $(".institute_course_name").append("<option value=''>Select Course Name</option>");
                        }
                        //$(".institute_course_name").append("<option value='other'>Other</option>");
                    }
                }
            });
        }else{
            $(".institute_course_name option").remove();
            $(".institute_course_name").append("<option value=''>Select Course Name</option>");
        }
    });

    $(document).on('change',".course_type_filter",function(e){
        e.preventDefault();
        
        var id = $(this).val();
        var temp_url = $("#api-institute-course-name-by-course-types").val();
        var url_arr =temp_url.split("%");
        var url = url_arr[0] + parseInt($(this).val());
        
        $(".other_course_name").val('');
        $("#other_course_name").addClass('hide');

        if($('#institute_id').length > 0){
            url = url +'/'+parseInt($('#institute_id').val());
        }
        
        if(id.length > 0){
            $.ajax({
                type: "get",
                url: url,
                statusCode: {
                    200:function (response) {
                        $(".institute_course_name option").remove();
                        $(".institute_course_name").val('');
                        $(".institute_course_name").append('<option value="">Select Course Name</option>'); 
                        if(response.length > 0){
                            $.each(response,function(index,value){
                                if($('#institute_id').length > 0){
                                    $(".institute_course_name").append('<option value='+value.id+'>'+value.course_details.course_name+'</option>');
                                }else
                                    $(".institute_course_name").append('<option value='+value.id+'>'+value.course_name+'</option>');
                            });
                        }else{
                            $(".institute_course_name").append("<option value=''>Select Course Name</option>");
                        }
                    }
                }
            });
        }else{
            $(".institute_course_name option").remove();
            $(".institute_course_name").append("<option value=''>Select Course Name</option>");
        }
    });

    $(".institute_name").on('change', function(e){
        e.preventDefault();

        var temp_url = $("#api-institute-location-by-institute-id").val();
        var url_arr = temp_url.split("%");
        var url = url_arr[0] + parseInt($(this).val());

        $.ajax({
            type: "get",
            url: url,
            statusCode: {
                200:function (response) {
                    $(".institute_locations option").remove();
                    $(".institute_locations").val('').trigger('change');
                    if(response.location){
                        $.each(response.location,function(index,value){
                            if(value.country == '95'){
                                $(".select2-select").append('<option value='+value.id+'>'+value.city.name+' , '+value.state.name+'</option>');
                            }
                        });
                    }
                    $("#course_type option").remove();
                    $("#course_type").append('<option value=>Select Course Type</option>');
                    if(response.course_type){
                        $.each(response.course_type,function(index,value){
                            $("#course_type").append('<option value='+index+'>'+value+'</option>');
                        });
                    }
                }
            }
        });
    });

    $(".course_type").on('change', function(e){
        e.preventDefault();
        
        var temp_url = $("#api-institute-course-name-by-course-types").val();
        var url_arr =temp_url.split("%");
        var url = url_arr[0] + parseInt($(this).val());

        $(".other_course_name").val('');
        $("#other_course_name").addClass('hide');

        $.ajax({
            type: "get",
            url: url,
            statusCode: {
                200:function (response) {
                    $(".institute_course_name option").remove();
                    $(".institute_course_name").append('<option value="">Select Course Name</option>');
                    if(response.length > 0){
                        $.each(response,function(index,value){
                            $(".institute_course_name").append('<option value='+value.id+'>'+value.course_name+'</option>');
                        });
                    }
                    //$(".institute_course_name").append('<option value=other>Other</option>');
                }
            }
        });
    });

    $(document).on('change',".course_type_other",function(e){
        e.preventDefault();
        
        var temp_url = $("#api-institute-course-name-by-course-types").val();
        var url_arr =temp_url.split("%");
        var url = url_arr[0] + parseInt($(this).val());


        $(".other_course_name").val('');
        $("#other_course_name").addClass('hide');

        pathname = window.location.pathname;
        is_all = false;
        if(pathname == '/institute/courses') {
            is_all = true;
        }else if(pathname == '/') {
            is_all = true;
        }

        $.ajax({
            type: "get",
            url: url + '?is_all='+is_all,
            statusCode: {
                200:function (response) {
                    $(".institute_course_name option").remove();
                    $(".institute_course_name").append('<option value="">Select Course Name</option>');
                    if(response.length > 0){
                        $.each(response,function(index,value){
                            $(".institute_course_name").append('<option value='+value.id+'>'+value.course_name+'</option>');
                        });
                    }
                    $(".institute_course_name").append('<option value=other>Other</option>');
                }
            }
        });
    });

    $('#add-institute-batch').validate({
        focusInvalid: false,
        invalidHandler: function(form, validator) {

            if (!validator.numberOfInvalids())
                return;

            $('html, body').animate({
                scrollTop: $(validator.errorList[0].element).offset().top + (-200)
            }, 200);

        },
        rules: {
            institute_id: {
                required: true
            },
            course_type: {
                required: true
            },
            course_name: {
                required: true
            },
            other_course_name: {
                required: true
            },
            start_date: {
                required: true
            },
            duration: {
                required: true,
                number: true
            },
            cost: {
                required: true,
                number: true
            },
            size: {
                required: true,
                number: true
            },
            eligibility: {
                required: true,
            },
            documents_req: {
                required: true,
            },
        }
    });

    $('#add-institute-course').validate({
        focusInvalid: false,
        invalidHandler: function(form, validator) {

            if (!validator.numberOfInvalids())
                return;

            $('html, body').animate({
                scrollTop: $(validator.errorList[0].element).offset().top + (-200)
            }, 200);

        },
        rules: {
            course_type: {
                required: true
            },
            course_name: {
                required: true
            },
            other_course_name: {
                required: true
            },
            institute_id: {
                required: true
            },
            duration: {
                required: true,
                number: true
            },
            cost: {
                required: true,
                number: true
            },
            size: {
                required: true,
                number: true
            },
            eligibility: {
                required: true,
            },
            documents_req: {
                required: true,
            },
            certificate_validity:{
                number: true,
            },
            approved_by:{
                required: true,
            },
        }
    });

    $(document).on('click',".addCourseModal_btn",function(e){
        $('.job-discription-card').find("input[type=text], textarea, select").val("");
        $('.job-discription-card').find(".course-select2-select").select2("val", "");
    });

    $(document).on('click',"#addinstitutecoursebtn",function(e){
        e.preventDefault();
        var l = Ladda.create(this);

        if($('#add-institute-course').valid()){

            l.start();
            $.ajax({
                url: $("#add-institute-course").attr('action'),
                type: "POST",
                data: $("#add-institute-course").serialize(),
                statusCode: {
                    200:function (response) {
                        toastr.success(response.message, 'Success');

                        window.location.reload();
                        l.stop();
                    },
                    400:function (response) {
                        l.stop();
                    }
                }
            });

        }

    });

    $(document).on('click',"#addinstitutebatchbtn",function(e){
        e.preventDefault();
        var l = Ladda.create(this);

        $("[name^='location']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select location"
                }
            });
        });

        $("[name^='start_date']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select start date"
                }
            });
        });

        $("[name^='duration']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please enter duration"
                }
            });
        });

        $("[name^='cost']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please enter cost"
                }
            });
        });

        $("[name^='size']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please enter size"
                }
            });
        });

        $("[name^='initial_per']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please enter initial percentage"
                }
            });
        });

        $("[name^='pending_per']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please enter pending percentage"
                }
            });
        });

        $("[name^='batch_type_on_demand']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please enter batch type on demand"
                }
            });
        });
        
        $("[name^='reserved_seats']").each(function(){
            $(this).rules("add", {
                required: true,
                max: $(this).closest('.size').val(),
                messages: {
                    required: "Please enter reserved seats",
                    max: "Please enter value less than batch size"
                }
            });
        });

        if($('#add-institute-batch').valid()){

            l.start();
            $.ajax({
                url: $("#add-institute-batch").attr('action'),
                type: "POST",
                data: $("#add-institute-batch").serialize(),
                statusCode: {
                    200:function (response) {
                        window.location.href = response.redirect_url;
                        l.stop();
                    },
                    400:function (response) {
                        l.stop();
                    }
                }
            });

        }

    });

    $('.courseEditButton').on('click', function(e){
        e.preventDefault();
        var course_id = ($(this).data('id'));

        $('#addCourseModal').modal('show');

        url = './course/edit/'+course_id;

        $.ajax({
            type: "POST",
            url: url,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            },
            statusCode: {
                200:function (response) {

                    if(typeof(response.course_details) != 'undefined'){
                        var data = response.course_details;
                        $('.course_id').val(data.id);
                        $('.course_type_other').val(data.course_type);
                        $('.size').val(data.size);
                        $('.eligibility').val(data.eligibility);
                        $('.certificate_validity').val(data.certificate_validity);
                        $('.key_topics').val(data.key_topics);
                        $('.description').val(data.description);
                        $('.cost').val(data.cost);
                        $('.duration').val(data.duration);
                        $('.approved_by').val(data.approved_by);

                        $(".institute_course_name option").remove();
                        $(".institute_course_name").val('');
                        $(".institute_course_name").append('<option value="">Select Course Name</option>'); 

                        $.each(data.course_list,function(index,value){
                            $(".institute_course_name").append('<option value='+value.id+'>'+value.course_name+'</option>');
                        });

                        $(".institute_course_name").val(data.course_id);

                        var documents = data.documents_req.split(',');
                        $(".course-select2-select").select2('val',documents);
                    }
                },
                400:function (response) {
                    toastr.error(response.message, 'Error');
                }

            }
        });

    });

    $('.courseDisableButton').on('click', function(e){
        e.preventDefault();
        var job_id = ($(this).data('id'));
        var status = ($(this).data('status'));

        if(status == 'enable'){
            url = './course/enable/'+job_id;
            $(".disable-"+job_id).removeClass('hide');
            $(".enable-"+job_id).addClass('hide');
        }else{
            url = './course/disable/'+job_id;
            $(".enable-"+job_id).removeClass('hide');
            $(".disable-"+job_id).addClass('hide');
        }
        $.ajax({
            type: "POST",
            url: url,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            },
            statusCode: {
                200:function (response) {
                    toastr.success(response.message, 'Success');
                },
                400:function (response) {
                    toastr.error(response.message, 'Error');
                }

            }
        });

    });

    $(document).on('click',".courseDeleteButton",function(e){
        e.preventDefault();
        var course_id = ($(this).data('id'));
        var temp_url = $("#api-institute-delete-course").val();

        if(temp_url){
            var temp_arr = temp_url.split('%');
            var url = temp_arr[0]+course_id;
        }else{
            temp_url =$("#api-admin-institute-delete-course").val();
            var institute_id = $("#institute_id").val();
            var temp_arr = temp_url.split('%');
            var url = temp_arr[0]+course_id+'/'+institute_id;
        }

        if (confirm('Are you sure ? You want to delete course ?')) {
            $.ajax({
                type: "POST",
                url: url,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                },
                statusCode: {
                    200:function (response) {
                        toastr.success(response.message, 'Success');

                        setTimeout(function(){
                            window.location.reload();
                        },500);
                        
                    },
                    400:function (response) {
                        toastr.error(response.message, 'Error');
                    }

                }
            });
        }

    });

    $(document).on('click',".discount_delete_button",function(e){
        e.preventDefault();
        var discount = $(this).data('id');

        if (confirm('Are you sure ? You want to delete discount ?')) {

            $('.discount_result').html('');
            $('.full-cnt-loader').removeClass('hidden');

            $.ajax({
                type: "POST",
                url: $("#api-delete-course-discount-batch").val(),
                data : {
                    discount_id : discount,
                },
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                },
                statusCode: {
                    200:function (response) {
                        $('.full-cnt-loader').addClass('hidden');
                        $('.discount_result').html(response.template);
                        toastr.success(response.message, 'Success');
                    },
                    400:function (response) {
                        toastr.error(response.message, 'Error');
                    }

                }
            });
        }

    });
    
    $(document).on('click',".batchDisableButton",function(e){
        e.preventDefault();
        var job_id = ($(this).data('id'));
        var status = ($(this).data('status'));

        if(status == 'enable'){
            url = './batch/enable/'+job_id;
            $(".disable-"+job_id).removeClass('hide');
            $(".enable-"+job_id).addClass('hide');
        }else{
            url = './batch/disable/'+job_id;
            $(".enable-"+job_id).removeClass('hide');
            $(".disable-"+job_id).addClass('hide');
        }

        $.ajax({
            type: "POST",
            url: url,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            },
            statusCode: {
                200:function (response) {
                    toastr.success(response.message, 'Success');
                },
                400:function (response) {
                    toastr.error(response.message, 'Error');
                }

            }
        });

    });


    $(document).on('click',".course-apply-button",function(e){

        e.preventDefault();
        var l = Ladda.create(this);
        l.start();

        var batch_id = $(this).attr('data-course-id');
        var btn_id = $(this).attr('data-btn-id');
        var institute_id = $(this).attr('data-institute-id');
        var url = $("#api-seafarer-course-apply-route").val();

        $.ajax({
            url: url,
            type: "POST",
            data: {
                'institute_id' : institute_id,
                'course_id' : batch_id,
                '_token' : $("input[name='_token']").val()
            },
            statusCode: {
                200:function (response) {
                    /*$("#course-apply-button-"+btn_id).hide();
                    $("#applied-for-course-"+btn_id).removeClass('hide');*/
                    toastr.success(response.message, 'Success');
                    l.stop();
                },
                400:function (response) {
                    l.stop();

                    toastr.error(response.responseJSON['message'], 'Error');

                    if(typeof response.responseJSON['redirect'] != 'undefined' && response.responseJSON['redirect'].length > 0){
                       
                        setTimeout(function(){
                            window.location.href = response.responseJSON['redirect'];
                        },500);
                    }
                }
            }
        });
    });

    $(document).on('change','.is_headoffice',function (e) {
        e.preventDefault();

        if( $(this).val() == 1) {
            if( $('input[type="radio"][value="1"][class="is_headoffice"]:checked').length > 1 ) {

                var name_attr_value = $(this).attr('name');
                $('input[type="radio"][class="is_headoffice"]').prop('checked',false);
                $('input[type="radio"][value="0"][class="is_headoffice"]').prop('checked',true);
                $('[name="'+name_attr_value+'"][value="0"]').prop('checked',false);
                $('[name="'+name_attr_value+'"][value="1"]').prop('checked',true);
            }
        }

    });

    $(document).on('change','.institute-state', function(e){
        e.preventDefault();
        var temp_url = $("#api-admin-state-list").val();
        var url_arr = temp_url.split("%");
        var url = url_arr[0] + parseInt($(this).val());
        var state_index = $(this).data('index');
        $('select[name="city"]').val('').trigger('change');
        $('select[name="city"] option').remove();

        $.ajax({
                type: "get",
                url: url,
                statusCode: {
                    200:function (response) {
                        $("#city option").remove();
                        $('select[name="city"]').append("<option value>Select City</option>");
                        $.each(response,function(index,value){
                            $('select[name="city"]').append('<option value='+value.id+'>'+value.name+'</option>');
                        }); 
                    },
                    400:function (response) {
                       $('select[name="city"]').append("<option value>Select City</option>");
                    },
                    422:function (response) {
                        $('select[name="city"]').append("<option value>Select City</option>");
                    }
                }
            });
    });
    var index = 0;
    var check = false;
    $(document).on('click','.add-course-batch-button',function (e) {

        var html = $('#batch-template').clone();

        html.find("input[type='text']").val("");
        html.find("select").val("");
        html.find("label.error").addClass('hide');

        if($(".total_block_index").length > 0 && check == false){
            index = parseInt($(".total_block_index").last().val())+1;
            check = true;
        }
let last_index = (  $(".total_block_index").length == 1) ? 0 : ($(".total_block_index").length  - 1)

        var text_index = index + 1;
        html.find(".batch-name").text('Batch '+text_index);

        html.find(".start_date").attr('name','start_date['+index+']');


        html.find(".duration").attr({
            'name' : 'duration['+index+']',
            'id' : 'cb_duration_'+index
        });
        let cb_duration = (last_index == 0 ) ? $('#cb_duration').val() : $('#cb_duration_'+last_index).val();

        html.find(".cost").attr({
            'name' : 'cost['+index+']',
            'id' : 'cb_cost_'+index
        });

        let cb_cost = (last_index == 0 ) ? $('#cb_cost').val() : $('#cb_cost_'+last_index).val();

        html.find(".size").attr({
            'name' : 'size['+index+']',
            'id' : 'cb_batch_size_'+index
        });

        let cb_batch_size = (last_index == 0 ) ? $('#cb_batch_size').val() : $('#cb_batch_size_'+last_index).val();


        html.find(".location").attr('name','location['+index+'][]');
        html.find(".reserved_seats").attr('name','reserved_seats['+index+']');
        html.find(".initial_per").attr('name','initial_per['+index+']');
        html.find(".initial_per").attr('data-id',index);

        html.find(".pending_per").attr('name','pending_per['+index+']');
        html.find(".pending_per").attr('data-id',index);

        html.find(".initial_amt").attr('name','initial_amt['+index+']');
        html.find(".pending_amt").attr('name','pending_amt['+index+']');
        html.find(".payment_type").attr('name','payment_type['+index+']');
        html.find(".payment_type").attr('data-id',index);
        html.find(".cost").attr('data-id',index);
       
        html.find(".payment_details_0").removeClass('payment_details_0').addClass('payment_details_'+index);
        html.find(".batch_type_details_0").removeClass('batch_type_details_0').addClass('batch_type_details_'+index);

        html.find(".discount").attr('name','discount['+index+']');
        //html.find(".batch_type").attr('name','batch_type['+index+']');
        //html.find(".batch_type_on_demand").attr('name','batch_type_on_demand['+index+']');
        html.find(".discount_per").attr('name','discount_per['+index+']');
        //console.log(index,index);
        html.find(".batch_type_on_demand").attr('name','batch_type_on_demand['+index+']');
        html.find(".batch_type").attr('name','batch_type['+index+']');
        html.find(".batch_type").attr('data-id',index);


        html.find(".batch_id").remove();
        html.find(".select2-choices").remove();
        html.find(".select2-container").remove();

        var prev_index = index - 1;
        html.find(".batch-close-button").addClass('close-button-'+index).removeClass('close-button-0').removeClass('hide');
        html.find(".batch-close-button").attr('id','close-button-'+index);
        html.append('<hr>');
        $("#add-more-batch-row").before(html);
        
        $('#cb_duration_'+index).val(cb_duration);
        $('#cb_cost_'+index).val(cb_cost);
        $('#cb_batch_size_'+index).val(cb_batch_size);
        //$('.add-ship-template').append(html);
        index++;

        $('.select2-select').select2({
            maximumSelectionSize: 1
        }).on('select2-opening', function(e) {
            if ($(this).select2('val').length > 0) {
                e.preventDefault();
            }
        });

        $('.issue-datepicker').datepicker({
            format: "dd-mm-yyyy",
            endDate: date,
            autoClose: true,
            startView: 2
            }).on('changeDate', function(ev) {
                if(ev.viewMode === 0){
                    $('.issue-datepicker').datepicker('hide');
                }
        });

        $('.batch-datepicker').datepicker({
            format: "dd-mm-yyyy",
            startDate: date,
            autoClose: true,
            startView: 0
            }).on('changeDate', function(ev) {
                if(ev.viewMode === 0){
                    $('.batch-datepicker').datepicker('hide');
                }
        });

    });

    $(document).on('click','.batch-close-button',function (e) {
        $(this).closest('#batch-template').remove();

        $(".batch-name").each(function(index,element){
            var index = index + 1;
            $(this).text('Batch '+index);
        });
    });

    $(document).on('click','.date_slide',function (e) {
        var id = $(this).attr('data-id');
        var key = $(this).attr('data-key');

        $(".details-key-"+key).addClass('hide');
        
        $(".details-"+id).removeClass('hide');
        
        $(".date_slide_"+key).removeClass('select');
        $(this).addClass('select');

        if($(".course-apply-button").length > 0){
            $("#course-apply-button-"+key).attr('data-course-id',id);
        }
    });

    $(document).on('init','.batches',function (e) {
        $(".batches").removeClass("dont-break");
    });

    $(".batch_details").on('click',function(e){

        $('.batches').slick('unslick');
        if(!$('.batches').hasClass('slick-initialized')){
            setTimeout(function(){
                        
                $('.batches').slick({
                    arrows: true,
                    autoplay: false,
                    centerMode: false,
                    slidesToShow: 6,
                    slidesToScroll: 1,
                    dots: false,
                    infinite: false,
                    lazyLoad: 'ondemand',
                    prevArrow: '<span class="navArrow_prev navArrow" style="left: 0;position: absolute;top: 50%;transform: translateY(-6px);"><i class="fa fa-chevron-left"></i></span>',
                    nextArrow: '<span class="navArrow_next navArrow" style="right: 0;position: absolute;top: 50%;transform: translateY(-6px);"><i class="fa fa-chevron-right"></i></span>',
                    responsive: [
                        {
                            breakpoint: 1024,
                            settings: {
                                slidesToShow: 2,
                                slidesToScroll: 1,
                                dots: false
                            }
                        },
                        {
                            breakpoint: 600,
                            settings: {
                                initialSlide: 1,
                                slidesToShow: 1,
                                slidesToScroll: 1,
                                centerMode: true
                            }
                        }
                    ]
                });
            },500);
        }
    });

    $('.batches').slick({
        arrows: true,
        autoplay: false,
        centerMode: false,
        slidesToShow: 6,
        slidesToScroll: 1,
        dots: false,
        infinite: false,
        lazyLoad: 'ondemand',
        prevArrow: '<span class="navArrow_prev navArrow" style="left: 0;position: absolute;top: 50%;transform: translateY(-6px);"><i class="fa fa-chevron-left"></i></span>',
        nextArrow: '<span class="navArrow_next navArrow" style="right: 0;position: absolute;top: 50%;transform: translateY(-6px);"><i class="fa fa-chevron-right"></i></span>',
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1,
                    dots: false
                }
            },
            {
                breakpoint: 600,
                settings: {
                    initialSlide: 1,
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    centerMode: true
                }
            }
        ]
    });

   $('#myTab2 a').not(".fa-edit").click(function (e) {
        e.preventDefault();

        var url = $(this).attr("data-url");
        var href = this.hash;
        var pane = $(this);
        
        // ajax load from data-url
        $(href).load(url,function(result){
            pane.tab('show');
        });
    });

    $(document).on('click','.pagination li a',function (e) {

        var url = $(this).attr("href");

        var id = $("#institute_subscription").data('id');
        var institute_id = $("#institute_subscription").data('institute');
        var role = $("#institute_subscription").data('role');
        url = url+'&institute_id='+institute_id+'&role='+role+'&id='+id;

        e.preventDefault();

        var href = "#institute_subscription";
        var pane = $(this);

        // ajax load from data-url

        $(href).load(url,function(result){
            $("#institute_subscription").tab('show');
        });

    });

    /*$(".institute_subscription").on('click',function(e){
        e.preventDefault();
        var id = $("#institute_subscription").data('id');
        var institute_id = $("#institute_subscription").data('institute');
        var role = $("#institute_subscription").data('role');
        var url = $("#api-admin-get-user-subscription").val();

        $(".full-cnt-loader").removeClass('hidden');
        $.ajax({
            type: "GET",
            url: url,
            data: {
                id: id,
                role: role,
                institute_id: institute_id,
            },
            statusCode: {
                200:function (response) {
                    $(".full-cnt-loader").addClass('hidden');
                    $(".data").html(response.template);
                },
                400:function (response) {
                    $(".data").text(response.message);
                    $(".full-cnt-loader").addClass('hidden');
                }
            }
        });
        
    });*/

    $(document).on('click','.view-sub-modal',function (e) {
        var modal_id = $(this).data('modal-id');
        $(".sub-"+modal_id).modal('show');
    });

    $(document).on('click','.payment_type',function (e) {

        var id = $(this).data('id');
            if($(this).val() == 'split'){
            $('.payment_details_'+id).removeClass('hide');
        }else{
            $('.payment_details_'+id).addClass('hide');
        }
    });

    $(document).on('click','.batch_type',function (e) {

        var id = $(this).data('id');
        $(".batch-datepicker-month").datepicker('destroy');
        $(".batch-datepicker").datepicker('destroy');
        $("input[name='start_date["+id+"]']").val('');
        
        if($(this).val() == 'confirmed'){
            $('.batch_type_details_'+id).addClass('hide');
            $("input[name='start_date["+id+"]']").addClass('batch-datepicker').removeClass('batch-datepicker-month');

            $('.batch-datepicker').datepicker({
                format: "dd-mm-yyyy",
                startDate: date,
                autoClose: true,
                startView: 0
                }).on('changeDate', function(ev) {
                    if(ev.viewMode === 0){
                        $('.batch-datepicker').datepicker('hide');
                    }
            });

            $('#add-batch').show();

        }else{
            $('.batch_type_details_'+id).removeClass('hide');
            $("input[name='start_date["+id+"]']").addClass('batch-datepicker-month').removeClass('batch-datepicker');

            $('.batch-datepicker-month').datepicker({
                format: "dd-mm-yyyy",
                startDate: date,
                autoClose: true,
                startView: 1
                }).on('changeMonth', function(ev) {
                    console.log(123);
                    if(ev.viewMode === 1){
                        var month = new Date(ev.date).getMonth() + 1;
                        var year = String(ev.date).split(" ")[3];
                        
                        $(this).datepicker('update', new Date(ev.date.getFullYear(), ev.date.getMonth() + 1, 0));
                        $(this).datepicker('hide');
                    }
            });

            $('#add-batch').hide();
        }

        $(".batch-datepicker-month").datepicker('refresh');
        $(".batch-datepicker").datepicker('refresh');
    });

    $(document).on('change','.per_amt',function (e) {
        var id = $(this).data('id');
        calculatePayment($(this).data('type'),id);
    });

    $(document).on('change','.cost',function (e) {
        var id = $(this).data('id');
        $("input[name='initial_per["+id+"]']").val('');
        $("input[name='initial_amt["+id+"]']").val('');
        $("input[name='pending_per["+id+"]']").val('');
        $("input[name='pending_amt["+id+"]']").val('');
    });

    function calculatePayment(type,id){
        
        $("[name^='cost']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please type cost"
                }
            });
        });

        var cost = $("input[name='cost["+id+"]']").val();
       
        var init_payment = $("input[name='initial_per["+id+"]']").val();;
        var pending_payment = $("input[name='pending_per["+id+"]']").val();

        if(type == 'pending'){
            $("input[name='pending_amt["+id+"]']").val(cost * pending_payment/100);
            var init_per = 100 - pending_payment;
            $("input[name='initial_per["+id+"]']").val(init_per);
            $("input[name='initial_amt["+id+"]']").val(cost * init_per/100);
        }
        else{
            $("input[name='initial_amt["+id+"]']").val(cost * init_payment/100);
            var pending_per = 100 - init_payment;
            $("input[name='pending_per["+id+"]']").val(pending_per);
            $("input[name='pending_amt["+id+"]']").val(cost * pending_per/100);
        }
    }
    
    $(document).on('change','.institute_course_name',function (e) {
        setDefaultBatchValues();
    });

    function setDefaultBatchValues(){
        var course_id = $(".institute_course_name").val();

        var temp_url = $("#api-site-institute-course-details-by-id").val();
        var url_arr = temp_url.split("%");
        var url = url_arr[0] + parseInt(course_id);

        $.ajax({
            type: "get",
            url: url,
            statusCode: {
                200:function (response) {
                   
                    if(response.cost){
                        $(".cost").val(response.cost);
                    }else{
                        $(".cost").val('');
                    }

                    if(response.size){
                        $(".size").val(response.size);
                    }else{
                        $(".size").val('');
                    }

                    if(response.duration){
                        $(".duration").val(response.duration);
                    }else{
                        $(".duration").val('');
                    }

                    $(".initial_per").val('');
                    $(".pending_per").val('');
                    $(".initial_amt").val('');
                    $(".pending_amt").val('');

                },
                400:function (response) {
                    
                }
            }
        });
    }

    $(document).on('click','.course_toggle',function (e) {

        var url = $(this).attr("href");

        var id = $("#institute_subscription").data('id');
        var institute_id = $("#institute_subscription").data('institute');
        var role = $("#institute_subscription").data('role');
        url = url+'&institute_id='+institute_id+'&role='+role+'&id='+id;

        e.preventDefault();

        var href = "#institute_subscription";
        var pane = $(this);

        // ajax load from data-url

        $(href).load(url,function(result){
            $("#institute_subscription").tab('show');
        });

    });

    $('#add-advertise-form').validate({
        focusInvalid: false,
        invalidHandler: function(form, validator) {

            if (!validator.numberOfInvalids())
                return;

            $('html, body').animate({
                scrollTop: $(validator.errorList[0].element).offset().top + (-200)
            }, 200);

        },
        rules: {
            company_id: {
                required: true,
            },
            advertise_img: {
                required: true
            },
        }
   });

    $("#submitAdvertiseDetailButton").on('click',function(e){
        
        e.preventDefault();
        var l = Ladda.create(this);

        if($('#add-advertise-form').valid()){ 

            var data = new FormData();
            $("input[type='file']").each(function(index,element){
                data.append('img_obj',$(".fileupload-exists input[type='file']")[0].files[0]);

                if($('#company_id').length > 0){
                    data.append('company_id',$('#company_id').val());
                }
                if($('#role').length > 0){
                    data.append('role',$('#role').val());
                }
            });

            l.start();
            $.ajax({
                type: "POST",
                url: $("#add-advertise-form").attr('action'),
                data: data,
                contentType : false,
                processData: false,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                },
                statusCode: {
                    200:function (response) {
                        window.location.href = response.redirect_url;
                        window.scrollTo(0,0);
                        l.stop();
                    },
                    400:function (response) {
                        l.stop();

                    }
                }
            });

        }
    });

    $("#logo-pic").on('click',function(e){
        $("#advModal").modal('show');
    });

    $('#add-institute-notice').validate({
        focusInvalid: false,
        invalidHandler: function(form, validator) {

            if (!validator.numberOfInvalids())
                return;

            $('html, body').animate({
                scrollTop: $(validator.errorList[0].element).offset().top + (-200)
            }, 200);

        },
        rules: {
            start_date: {
                required: true
            },
            end_date: {
                required: true
            },
            notice: {
                required: true
            },
        }
    });

    $(document).on('click',"#addinstitutenoticebtn",function(e){
        e.preventDefault();
        var l = Ladda.create(this);

        if($('#add-institute-notice').valid()){

            l.start();
            $.ajax({
                url: $("#add-institute-notice").attr('action'),
                type: "POST",
                data: $("#add-institute-notice").serialize(),
                statusCode: {
                    200:function (response) {
                        toastr.success(response.message, 'Success');
                        l.stop();
                        window.location.href = response.redirect_url;
                    },
                    400:function (response) {
                        l.stop();
                    }
                }
            });

        }

    });


    $('#img-gallery').slick({
        infinite: false,
        speed: 300,
        slidesToShow: 3,
        slidesToScroll: 3,
        arrows: true,
        prevArrow: '<span class="navArrow_prev navArrow" style="padding:0 10px;left: 0;position: absolute;top: 50%;transform: translateY(-6px);"><i class="fa fa-chevron-left"></i></span>',
        nextArrow: '<span class="navArrow_next navArrow" style="padding:0 10px;right: 0;position: absolute;top: 50%;transform: translateY(-6px);"><i class="fa fa-chevron-right"></i></span>',
        responsive: [
            {
              breakpoint: 1500,
              settings: {
                slidesToShow: 2,
                slidesToScroll: 2,
                infinite: true,
              }
            },
            {
              breakpoint: 600,
              settings: {
                slidesToShow: 2,
                slidesToScroll: 2
              }
            },
            {
              breakpoint: 480,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1
              }
            }
            // You can unslick at a given breakpoint now by adding:
            // settings: "unslick"
            // instead of a settings object
        ]
    });
    
    $('#add-image-gallery-form').validate({
        rules: {
            advertise_img: {
                required: true
            },
        }
    });

    $(document).on('click',"#saveImageGalleryButton",function(e){
        e.preventDefault();
        var l = Ladda.create(this);

        if($("input[type='file']").val() == ''){
            $("#advertise_upload_error").css('display','block');
            $("#advertise_upload_error").text('Please upload image file.');
            return true;
        }

        if($('#add-image-gallery-form').valid()){

            var data = new FormData();
            $("input[type='file']").each(function(index,element){
                data.append('img_obj',$(".fileupload-exists input[type='file']")[0].files[0]);

                if($('#company_id').length > 0){
                    data.append('company_id',$('#company_id').val());
                }
                if($('#role').length > 0){
                    data.append('role',$('#role').val());
                }
            });

            l.start();
            $('.loader_overlay').removeClass('hide');
            $.ajax({
                url: $("#add-image-gallery-form").attr('action'),
                type: "POST",
                data: data,
                contentType : false,
                processData: false,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                },
                statusCode: {
                    200:function (response) {

                        if(response.template){
                            $(".image-section").html('');
                            $(".image-section").append(response.template);
                            $('.loader_overlay').addClass('hide');
                            $("input[type='file']").val('')

                            $("#advertise_upload_error").text('');
                            $("#advertise-file-uplaod").val('');
                            $('.fileupload-new img').attr('src','/images/no-image-advertisements.png');
                            $('.fileupload-preview img').attr('src','/images/no-image-advertisements.png');
                        }
                        toastr.success(response.message, 'Success');
                        l.stop();
                    },
                    400:function (response) {
                        toastr.error(response.responseJSON.message, 'Error');
                        $('.loader_overlay').addClass('hide');
                        l.stop();
                    }
                }
            });

        }

    });

    $(".advertise-file-uplaod").change(function(e) {
        var file, img;
        var ext = $(".advertise-file-uplaod").val().split('.').pop().toLowerCase();

        if ((file = this.files[0])) {
            img = new Image();
            var ValidImageTypes = ["jpeg", "png", "jpg"];

            if($.inArray(ext, ValidImageTypes) < 0) {
                $(".advertise-file-uplaod").val('');
                $('.fileupload-new img').attr('src','/images/no-image-advertisements.png');
                $('.fileupload-preview img').attr('src','/images/no-image-advertisements.png');
                $("#advertise_upload_error").text('Please upload image file only.');
            }else{
                 $("#advertise_upload_error").text('');
            }
            
            img.onerror = function() {
                $(".advertise-file-uplaod").val('');
                $('.fileupload-new img').attr('src','/images/no-image-advertisements.png');
                $('.fileupload-preview img').attr('src','/images/no-image-advertisements.png');
                $('.fileupload-preview.fileupload-exists.thumbnail').empty();
                $('.fileupload-preview.fileupload-exists.thumbnail').append('<img src="/images/no-image-advertisements.png" alt="">');
                $("#advertise_upload_error").text('Please upload image file.');
                
            };
            //img.src = _URL.createObjectURL(file);
        }
    });

    $(".remove_advertise_btn").on('click', function(e){
        $(".advertise_upload #advertise_upload_error").show();
        $("#advertise_upload_error").text('');
        $("#advertise-file-uplaod").val('');
        $('.fileupload-new img').attr('src','/images/no-image-advertisements.png');
        $('.fileupload-preview img').attr('src','/images/no-image-advertisements.png');
    });

    $(document).on('click',".gallery-close-icon",function(e){

        var dat = $(this);
        if (confirm('Are you sure you want to delete this image ?')) {
            $('.loader_overlay').removeClass('hide');
            $.ajax({
                type: "POST",
                url: $("#api-institute-delete-image-gallery").val(),
                data: {
                    image_id : $(this).data('id'),
                },
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                },
                statusCode: {
                    200:function (response) {
                        dat.closest(".gallery-image").remove();
                        if(response.template){
                            $(".image-section").html('');
                            $(".image-section").append(response.template);
                            $('.loader_overlay').addClass('hide');
                        }
                        toastr.success(response.message, 'Success');
                    },
                    400:function (response) {
                        $('.loader_overlay').addClass('hide');
                        toastr.error(response.responseJSON.message, 'Error');
                    }
                }
            });
        }
    });    

});