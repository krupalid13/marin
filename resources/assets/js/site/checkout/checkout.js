$(document).ready(function() {

    $("#proceed-to-payment").on('click', function(e){
        e.preventDefault();

        var url = $("#api-proceed-to-payment-route").val();

        $.ajax({
            url : url,
            method : "POST",
            data : {
                '_token' : $("input[name='_token']").val(),
            },
            statusCode: {
                200 : function(response) {
                    $("#payumoney-form").empty();
                    $("#payumoney-form").append(response.payumoney_form);
                    $("#payumoney_form").submit();
                },
                400 : function(response) {
                }
            }
        });
    });
});