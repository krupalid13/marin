$(document).ready(function() {

    $('.form-forgot').validate({

        rules: {
            email: {
                required: true,
                email: true
            },
            messages: {
                email: {
                    required: "We need your email address to contact you",
                    email: "Your email address must be in the format of name@domain.com"
                },
            },
        }
    });

    $("#reset-password-submit-button").on('click',function(e){
        e.preventDefault();
        var l = Ladda.create(this);
            if($('.form-forgot').valid()) {
                l.start();
                $.ajax({
                    type: "POST",
                    url: $(".form-forgot").attr('action'),
                    data: $(".form-forgot").serialize(),
                    statusCode: {
                        200: function (response) {
                            toastr.success(response.message, 'success');
                            $(".go-back").trigger( "click" );
                            l.stop();
                        },
                        400: function (response) {
                            toastr.error(response.responseJSON.message, 'error');
                            l.stop();

                        },
                        422: function (response) {

                        }
                    }
                });
            }
    });

    $('.form-change-password').validate({

        rules: {
            password: {
                minlength: 6,
                required: true
            },
            cpassword: {
                required: true,
                minlength: 5,
                equalTo: "#password"
            },
            messages: {
                email: {
                    required: "We need your email address to contact you",
                    email: "Your email address must be in the format of name@domain.com"
                },
                cpassword: {
                    equalTo: "Confirm password is not matching with password "
                }
            },
        }
    });

    $("#admin-change-password-button").on('click',function(e){
        e.preventDefault();
        var l = Ladda.create(this);
        if($('.form-change-password').valid()) {
            l.start();
            $.ajax({
                type: "POST",
                url: $(".form-change-password").attr('action'),
                data: $(".form-change-password").serialize(),
                statusCode: {
                    200: function (response) {
                        toastr.success(response.message, 'success');
                        window.location.href = response.redirect_url;
                        l.stop();
                    },
                    400: function (response) {
                        toastr.error(response.responseJSON.message, 'error');
                        l.stop();

                    },
                    422: function (response) {

                    }
                }
            });
        }
    });
});