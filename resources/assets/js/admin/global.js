$(document).ready(function(){
    
    $.ajaxSetup({
        headers:
        { 'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content') }
    });

    var searchAjaxCall = "undefined";
    date = moment().format('DD-MM-YYYY');

    birth_date = moment().subtract(17, 'years').format('DD-MM-YYYY');

    $(document).on('change', '.country', function () {
       var block_index = $(this).attr('block-index');
       if( $(this).val() == india_value ) {
           $(".state-block-"+block_index).find('.fields-for-india').removeClass('hide');
           $(".state-block-"+block_index).find('.fields-not-for-india').addClass('hide');

           $(".city-block-"+block_index).find('.fields-for-india').removeClass('hide');
           $(".city-block-"+block_index).find('.fields-not-for-india').addClass('hide');

           $("[name='pincode["+block_index+"]']").rules("add", {
               maxlength: 6,
               messages: {
                   maxlength: "Please provide valid 6 digit pincode"
               }
           });

           $("[name='pincode["+block_index+"]']").addClass('pin_code_fetch_input');
       } else {
           $(".state-block-"+block_index).find('.fields-for-india').addClass('hide');
           $(".state-block-"+block_index).find('.fields-not-for-india').removeClass('hide');

           $(".city-block-"+block_index).find('.fields-for-india').addClass('hide');
           $(".city-block-"+block_index).find('.fields-not-for-india').removeClass('hide');

           $("[name='pincode["+block_index+"]']").removeClass('pin_code_fetch_input');
           
           $("[name='pincode["+block_index+"]']").rules('remove','maxlength');


       }
    });


    $('.issue-datepicker').datepicker({
        format: "dd-mm-yyyy",
        endDate: date,
        autoClose: true,
        startView: 2
    }).on('changeDate', function(ev) {
        if(ev.viewMode === 0){
            $('.issue-datepicker').datepicker('hide');
        }
    });

    $('.birth-date-datepicker').datepicker({
        format: "dd-mm-yyyy",
        endDate: birth_date,
        autoClose: true,
        startView: 2
    }).on('changeDate', function(ev) {
        if(ev.viewMode === 0){
            $('.birth-date-datepicker').datepicker('hide');
        }
    });

    $('.datepicker').datepicker({
        format: "dd-mm-yyyy",
        startDate: date,
        autoClose: true,
        startView: 2
    }).on('changeDate', function(ev) {
        if(ev.viewMode === 0){
            $('.datepicker').datepicker('hide');
        }
    });

    
    $('.m-y-datepicker').datepicker({
        format: "mm-yyyy",
        startDate: date,
        autoClose: true,
        startView: 2,
        minViewMode: "months"
    }).on('changeDate', function(ev) {
        if(ev.viewMode === 1){
            $('.m-y-datepicker').datepicker('hide');
        }
    });
    
    var loadDetails = false;
    $(document).on("keyup", ".pin_code_fetch_input", function(){
        var value = $(this).val();
        var form_id = $(this).data("form-id");
        var block_index = $(this).attr('block-index');

        $(".pincode-error-message").remove();

        if($.isNumeric(value) && value.length == 6){
            while(loadDetails == false) {
                $(".pincode-loader-"+block_index).removeClass("hide");
                var url = $("#api-pincode-related-data").val();
                var pincode = value;
                loadDetails = true;
                $.ajax({
                    url : url,
                    method : "GET",
                    data : {
                        pincode : pincode
                    },
                    statusCode : {
                        200 : function(response) {

                            if(response.result.length > 0) {
                                $("#" + form_id).find(".pincode-block-"+block_index+" .pincode-id").val(response.result[0].id);

                                $(".pincode-loader-"+block_index).addClass("hide");
                                // state
                                var states_list = response.result[0].pincodes_states;
                                $("#" + form_id).find("select[name='state["+block_index+"]'] option").remove();
                               
                                if(states_list.length == 1) {

                                    for(var s = 0; s < states_list.length; s++) {
                                        var s_id = states_list[s].state.id;
                                        str = states_list[s].state.name.toLowerCase().replace(/\b[a-z]/g, function(letter) {
                                            return letter.toUpperCase();
                                        });
                                        var s_name = str;
                                        $("#" + form_id).find("select[name='state["+block_index+"]']").append("<option value='"+s_id+"' selected>"+s_name+"</option>");
                                    }

                                } else {

                                    $("#" + form_id).find("select[name='state["+block_index+"]']").append("<option value='' selected> Select a state </option>");
                                    for(var s = 0; s < states_list.length; s++) {
                                        var s_id = states_list[s].state.id;
                                        str = states_list[s].state.name.toLowerCase().replace(/\b[a-z]/g, function(letter) {
                                            return letter.toUpperCase();
                                        });
                                        var s_name = str;
                                        $("#" + form_id).find("select[name='state["+block_index+"]']").append("<option value='"+s_id+"'>"+s_name+"</option>");
                                    }

                                }

                                // city
                                var cities_list = response.result[0].pincodes_cities;
                                $("#" + form_id).find("select[name='city["+block_index+"]'] option").remove();

                                if(cities_list.length == 1) {

                                    for(var c = 0; c < cities_list.length; c++) {
                                        var c_id = cities_list[c].city.id;
                                        str = cities_list[c].city.name.toLowerCase().replace(/\b[a-z]/g, function(letter) {
                                            return letter.toUpperCase();
                                        });
                                        var c_name = str;
                                        $("#" + form_id).find("select[name='city["+block_index+"]']").append("<option value='"+c_id+"' selected>"+c_name+"</option>");
                                    }

                                } else {

                                    $("#" + form_id).find("select[name='city["+block_index+"]']").append("<option value='' selected> Select a city </option>");

                                    for(var c = 0; c < cities_list.length; c++) {
                                        var c_id = cities_list[c].city.id;
                                        str = cities_list[c].city.name.toLowerCase().replace(/\b[a-z]/g, function(letter) {
                                            return letter.toUpperCase();
                                        });
                                        var c_name = str;
                                        $("#" + form_id).find("select[name='city["+block_index+"]']").append("<option value='"+c_id+"'>"+c_name+"</option>");
                                    }

                                }

                            } else {

                                // state
                                $("#" + form_id).find("select[name='state["+block_index+"]'] option").remove();
                                $("#" + form_id).find("select[name='state["+block_index+"]']").append("<option value='' selected disabled> Select a state </option>");

                                // city
                                $("#" + form_id).find("select[name='city["+block_index+"]'] option").remove();
                                $("#" + form_id).find("select[name='city["+block_index+"]']").append("<option value='' selected disabled> Select a city </option>");

                                // area
                                // $("#" + form_id).find("select[name='area["+block_index+"]'] option").remove();
                                // $("#" + form_id).find("select[name='area["+block_index+"]']").append("<option value='' selected disabled> Select a area </option>");
                            }
                        },
                        400 :  function(response) {
                            $(".pincode-loader-"+block_index).addClass("hide");
                            $("#"+form_id +" .pin_code_fetch_input").closest("div").append("<div class='pincode-error-message'>Invalid pincode. Please enter a correct pin code.</div>");
                            // state
                            $("#"+form_id).find("select[name='state["+block_index+"]'] option").remove();
                            $("#"+form_id).find("select[name='state["+block_index+"]']").append("<option value=''>Select a state</option>");

                            // city
                            $("#"+form_id).find("select[name='city["+block_index+"]'] option").remove();
                            $("#"+form_id).find("select[name='city["+block_index+"]']").append("<option value=''>Select a city</option>");

                            // area
                            // $("#"+form_id).find("select[name='area["+block_index+"]'] option").remove();
                            // $("#"+form_id).find("select[name='area["+block_index+"]']").append("<option value=''>Select a area</option>");

                        }
                    }
                });
            }
        } else if($.isNumeric(value) && value.length < 6) {
            loadDetails = false;
            $(".pincode-loader"+block_index).addClass("hide");

        }
    });

    $(document).on('submit', '.listing-search-form', function(e){
        e.preventDefault();
        var formId = $(this).closest('form');
        changeSearchState(formId);
    });

    $(document).on('submit', '.filter-search-form', function(e){
        e.preventDefault();
        var formId = $(".listing-search-form");
        var filtersformId = $(this);
        changeSearchState(formId,filtersformId);
    });


    $(document).on('click', 'ul.pagination a', function(e) {
        if (typeof dont_triger_history == 'undefined') {
            e.preventDefault();
            var url = $(this).attr('href');
            var page = getUrlParameter('page',url);
            setGetParameter('page',page);
        }
    });

    /* uSed by all listing search and filtering pages in the admin panel */
    function changeSearchState(formId,filtersformId)
    {   
        var formData = formId.serializeArray();
        if(typeof filtersformId == "undefined")
        {
        var params = '?'+$.param(formData);
        
        }
        else
        {
        var filtersformData = filtersformId.serializeArray();

        var params = '?'+$.param(formData)+'&'+$.param(filtersformData);
        
        }    
        History.pushState(params, null, params);
    }

    /* uSed by all listing search and filtering pages in the admin panel */
    function fetchSearchResults(url, successCallback, errorCallback)
    {
        /*$.each(filtersformData, function( index, value ) {
            console.log($("#"+filtersformData[index]['name']) ,filtersformData[index][value]);
            //console.log(filtersformData[index]['name']);
        });*/

        if(searchAjaxCall != "undefined")
        {
            searchAjaxCall.abort();
            searchAjaxCall = "undefined";
        }

        $('#search-results-main-container').addClass('hide');
        $('.full-cnt-loader').removeClass('hidden');

        searchAjaxCall = $.ajax({
            url: url,
            dataType: 'json',
            statusCode: {
                200: function(response) {

                    if (typeof response.data != 'undefined' && response.data.status == 'success') {
                       
                        $('#search-results-main-container').removeClass('hide');
                        $('.full-cnt-loader').addClass('hidden');

                        $('#search-results-main-container').html(response.template);
                        

                        if(typeof successCallback != "undefined")
                        {
                            successCallback();
                        }

                    }
                }
            },
            error: function(error, response) {

                if(typeof errorCallback != "undefined")
                {
                    errorCallback();
                }
            }
        });
    }

    var getUrlParameter = function getUrlParameter(sParam,url) {
        var sPageURL = url,
            sURLVariables = sPageURL.split('?'),
            sParameterName,
            i;

        
        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');

            if (sParameterName[0] === sParam) {
                return sParameterName[1] === undefined ? true : sParameterName[1];
            }
        }
    }

    function setGetParameter(paramName, paramValue)
    {
        var url = document.URL;
        var hash = location.hash;
        url = url.replace(hash, '');
        if (url.indexOf(paramName + "=") >= 0)
        {
            var prefix = url.substring(0, url.indexOf(paramName));
            var suffix = url.substring(url.indexOf(paramName));
            suffix = suffix.substring(suffix.indexOf("=") + 1);
            suffix = (suffix.indexOf("&") >= 0) ? suffix.substring(suffix.indexOf("&")) : "";
            url = prefix + paramName + "=" + paramValue + suffix;
        }
        else
        {
        if (url.indexOf("?") < 0)
            url += "?" + paramName + "=" + paramValue;
        else
            url += "&" + paramName + "=" + paramValue;
        }
        var params = url + hash;
        History.pushState(params, null, params);
    }

    $("#state").on('change', function(e){
        e.preventDefault();
        var temp_url = $("#api-admin-state-list").val();
        var url_arr =temp_url.split("%");
        var url = url_arr[0] + parseInt($(this).val());

        $.ajax({
            type: "get",
            url: url,
            statusCode: {
                200:function (response) {
                    $("#city option").remove();
                    $("#city").append("<option value=''>Select City</option>");
                    $.each(response,function(index,value){
                        $("#city").append('<option value='+value.id+'>'+value.name+'</option>');
                    }); 
                },
                400:function (response) {
                   
                },
                422:function (response) {

                }
            }
        });
    });

    $(document).on('click',"#download_excel",function(e){

        $type = $(this).data('type');

        if($type == 'company'){
            var url = $("#api-admin-company-download-excel").val();
            url = url+'?'+$(".filter-search-form").serialize();
        }
        if($type == 'seafarer'){
            var url = $("#api-admin-seafarer-download-excel").val();
            url = url+'?'+$(".filter-search-form").serialize();
        }
        
        $("#download_excel").attr('href',url);
        $("#download_excel").attr('target','_blank');

        //$("#download_excel").click();
        /*var newWindow = window.open("","");
        newWindow.location.href = url;
        window.close();*/
        /*$.ajax({
            type: "get",
            url: url,
            data: $(".filter-search-form").serialize(),
            statusCode: {
                200:function (response) {
                    
                },
                400:function (response) {
                   
                }
            }
        });*/
    });

    $(".notification_bell").on('click',function(e){
        
        $(".overlay_section").addClass('menu_overlay');
        if ($('.notification-main-view').css('display') == 'block')
        {
            $('.notification-main-view').css('display','none');
        }else
        {
            $('.notification-main-view').css('display','block');
        }
    });

    $(".overlay_section").on('click',function(e){
        $(this).removeClass('menu_overlay');
        $('.notification-main-view').css('display','none');
    });

    $.ajax({
        type: "GET",
        url: $("#api-get-user-notification").val(),
        headers: {
            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
        },
        statusCode: {
            200:function (response) {
                if(typeof(response.template) != 'undefined'){
                    if(response.template.length > 0){
                        $('.notification-content-view').append(response.template);
                    }

                    if(typeof(response.notification_count) != 'undefined'){
                        $('.notification-count').attr('data-notification',response.notification_count);

                        if(response.notification_count != '0'){
                            $('.notification-count').html(response.notification_count);
                            $('.notification-count').removeClass('hide');
                        }

                    }
                }
            },
            400:function (response) {
               
            }
        }
    });

    if(typeof(user_id) != 'undefined' && user_id != ''){

        var pubnub = new PubNub({
            subscribeKey: subscribeKey,
            publishKey: publishKey,
            ssl: true
        });

        pubnub.addListener({
            status: function(statusEvent) {

                if (statusEvent.category === "PNConnectedCategory") {
                    var payload = {
                        my: 'payload'
                    };
                    pubnub.publish(
                        { 
                            message: payload
                        }, 
                        function (status) {
                            // handle publish response
                        }
                    );
                }
            },
            message: function(data) {
                // handle data
                if(typeof(data.message) != 'undefined'){

                    if(typeof(data.message.notification_id) != 'undefined'){
                        var notification_id = data.message.notification_id;

                        $.ajax({
                            type: "POST",
                            url: $("#api-upload-user-notification").val(),
                            data: {
                                notification_id : notification_id,
                            },
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                            },
                            statusCode: {
                                200:function (response) {
                                   if(typeof(response.template) != 'undefined'){
                                        $('.no-notification').remove();
                                        $('.notification-content-view').prepend(response.template);
                                        $('.notification-count').removeClass('hide');

                                        var notification_count = $('.notification-count').data('notification') + 1;
                                        $('.notification-count').attr('data-notification',notification_count);
                                        $('.notification-count').html(notification_count);
                                    }

                                },
                                400:function (response) {
                                   

                                }
                            }
                        });
                    }
                }
            },
            presence: function(presenceEvent) {
                // handle presence
            }
        });

        var listen_to = user_id+'_Notification';

        pubnub.subscribe({
            channels: [listen_to],
        });
    }

    $(document).on('click','.notification',function (e) {
        var notification_id = $(this).data('notification');
        var url = $(this).data('url');

        $(this).removeClass('unread');

        $.ajax({
            type: "POST",
            url: $("#api-set-user-notification-read").val(),
            data: {
                notification_id : notification_id,
                url : url,
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            },
            statusCode: {
                200:function (response) {
                    var curr_notification = $('.notification-count').attr('data-notification');
                    console.log(curr_notification);

                    if(curr_notification != 1){
                        curr_notification = curr_notification - 1;
                        $('.notification-count').attr('data-notification',curr_notification);
                        $('.notification-count').html(curr_notification);
                    }else{
                        $('.notification-count').attr('data-notification',0);
                        $('.notification-count').html(0);
                        $('.notification-count').addClass('hide');
                    }
                    //window.location.href = url;
                },
                400:function (response) {

                }
            }
        });

    });

});
