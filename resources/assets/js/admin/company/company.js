$(document).ready(function() {

    function check_rpsl(){
        if($("#company_type").val() == 1){
            $("#rpsl-file-container").removeClass('hide');
        }else{
            $("#rpsl-file-container").addClass('hide');
        }

    }

	$(document).on('click',".company-next-btn",function(e){
        e.preventDefault();
        check_rpsl();

        $("[name^='country']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select country"
                }
            });
        });

        $("[name^='pincode']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please enter pincode"
                }
            } );
        });

        $("[name^='state']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select state"
                }
            } );
        });

        $("[name^='state_text']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select state"
                }
            } );
        });

        $("[name^='city']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select city"
                }
            } );
        });

        $("[name^='city_text']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select city"
                }
            } );
        });

        $("[name^='address']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please enter address"
                }
            } );
        });

        $("[name^='ship_type']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select ship type"
                }
            } );
        });

        $("[name^='telephone']").each(function(){
            $(this).rules("add", {
                number: true,
                messages: {
                    required: "Please enter numbers only"
                }
            } );
        });

        /*$("[name^='ship_flag']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select ship flag"
                }
            } );
        });*/

        $("[name^='grt']").each(function(){
            $(this).rules("add", {
                required: true,
                number: true,
                messages: {
                    required: "Please enter GRT"
                }
            } );
        });

        /*$("[name^='engine_type']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select engine type"
                }
            } );
        });*/

        $("[name^='bhp']").each(function(){
            $(this).rules("add", {
                required: true,
                number: true,
                messages: {
                    required: "Please enter bhp"
                }
            } );
        });

        /*$("[name^='built-year']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select year of built"
                }
            } );
        });*/

        $("[name^='p_i_cover_company_name']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please enter company name"
                }
            } );
        });

        $("#add-company-form").valid();
        
    });

    $('#add-company-form').validate({
    	rules: {
            email: {
                required: true,
                email: true,
                remote: {
                    url: $("#api-check-email").val(),
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content'),
                    },
                    type: "post",
                    async: false,
                    data: {
                        id: function(){ return $("input[name='company_id']").val()},
                        email: function(){ return $("#email").val(); }
                    }
                }
            },
            password: {
                minlength: 6,
                required: true
            },
            cpassword: {
                required: true,
                minlength: 5,
                equalTo: "#password"
            },
            contact_person_number: {
                required: true,
                number: true,
                minlength: 10,
                remote: {
                    url: $("#api-check-mobile").val(),
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content'),
                    },
                    type: "post",
                    async: false,
                    data: {
                        id: function(){ return $("input[name='company_id']").val()},
                        mobile: function(){ return $("#contact_person_number").val(); }
                    }
                }
            },
            contact_person: {
                required: true,
                number: false
            },
            contact_person_email: {
                required: true,
                email: true
            },
            company_name: {
                minlength: 2,
                required: true
            },
            company_type: {
                required: true
            },
            company_description: {
                required: true
            },
            company_email: {
                required: true,
                email: true
            },
            company_contact_number: {
                number: true
            },
            website: {
                complete_url: true
            },
            fax : {
                minlength: 6
            },
            rpsl_no : {
                required: true
            },
            incorporation_number : {
                required: true
            }/*,
            pancard_number : {
                required: true
            }*/
        },
        messages: {
            email: {
                required: "We need your email address to contact you",
                email: "Your email address must be in the format of name@domain.com",
                remote: "Email already exist"
            },
            contact_person_email: {
                required: "We need your email address to contact you",
                email: "Your email address must be in the format of name@domain.com"
            },
            contact_person_number: {
                remote: "Mobile already exist"
            },
            cpassword: {
                equalTo: "Confirm password is not matching with password "
            },
            rank_exp: {
                number: "Please enter experience in numbers"
            },
            mobile: {
                number: "Please enter valid Mobile number"
            }
        },
   	});

    $('#add-company-finish-button').on('click', function(){
        
        if($("#add-company-form").valid()){
                
                var l = Ladda.create(this);
                var data = new FormData();
                var other_fields = $("#add-company-form").serializeArray();
                $.each(other_fields, function(key,input){
                    data.append(input.name,input.value);
                });
                $("input[type='file']").each(function(index,element){
                    if($(this)[0].files.length > 0){
                        var temp_file_anme = $(this).attr('name');
                        var temp_arr = temp_file_anme.split('_1');
                        var file_name = temp_arr[0];
                        data.append(file_name,$(this)[0].files[0]);
                    }
                });
                l.start();
                $.ajax({
                    type: "POST",
                    url: $("#add-company-form").attr('action'),
                    data: data,
                    contentType: false,
                    processData: false,
                    statusCode: {
                        200:function (response) {
                            window.location.href = response.redirect_url;
                            l.stop();
                        },
                        400:function (response) {
                            l.stop();

                        },
                        422:function (response) {
                            $("#step-2").hide();
                            $("#step-1").show();
                            var foo = response.responseJSON;
                            var text = '';
                            for (var prop in foo) {
                                text += foo[prop][0]+ '<br />';
                            }
                            $(".alert-box").show();
                            $(".alert-box").html(text);
                            l.stop();

                        }
                    }
                });
            }
        });

    $("#company_type").on('change', function(e){
        if($(this).val() == 1){
            $(".rpsl_number_textbox").removeClass('hide');
        }else{
            $(".rpsl_number_textbox").addClass('hide');
        }
    });

});