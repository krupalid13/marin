var FormWizard = function () {
	"use strict";
    var wizardContent = $('#wizard');
    var wizardForm = $('#add-company-form');
    var numberOfSteps = $('.swMain > ul > li').length;
    var enable = false;
    if($("#edit").length > 0){
        enable = true;
    }
    var initWizard = function () {
        // function to initiate Wizard Form
        wizardContent.smartWizard({
            selected: 0,
            keyNavigation: false,
            onLeaveStep: leaveAStepCallback,
            onShowStep: onShowStep,
            enableAllSteps: enable,
        });
        var numberOfSteps = 0;
        animateBar();
        initValidator();
    };
    var animateBar = function (val) {
        if ((typeof val == 'undefined') || val == "") {
            val = 1;
        };
        
        var valueNow = Math.floor(100 / numberOfSteps * val);
        $('.step-bar').css('width', valueNow + '%');
    };
    var validateCheckRadio = function (val) {
        $("input[type='radio'], input[type='checkbox']").on('ifChecked', function(event) {
			$(this).parent().closest(".has-error").removeClass("has-error").addClass("has-success").find(".help-block").remove().end().find('.symbol').addClass('ok');
		});
    };    
    var initValidator = function () {
        $.validator.addMethod("cardExpiry", function () {
            //if all values are selected
            if ($("#card_expiry_mm").val() != "" && $("#card_expiry_yyyy").val() != "") {
                return true;
            } else {
                return false;
            }
        }, 'Please select a month and year');
        $.validator.setDefaults({
            errorElement: "span", // contain the error msg in a span tag
            errorClass: 'help-block',
            errorPlacement: function (error, element) { // render error placement for each input type
                if (element.attr("type") == "radio" || element.attr("type") == "checkbox") { // for chosen elements, need to insert the error after the chosen container
                    error.insertAfter($(element).closest('.form-group').children('div').children().last());
                } else if (element.attr("name") == "card_expiry_mm" || element.attr("name") == "card_expiry_yyyy") {
                    error.appendTo($(element).closest('.form-group').children('div'));
                } else {
                    error.insertAfter(element);
                    // for other inputs, just perform default behavior
                }
            },
            ignore: ':hidden',
        //     rules: {
        //     firstname: {
        //         alpha: true,
        //         required: true
        //     },
        //     /*lastname: {
        //         lettersonly: true,
        //         minlength: 2,
        //         required: true
        //     },*/
        //     email: {
        //         required: true,
        //         email: true
        //     },
        //     password: {
        //         minlength: 6,
        //         required: true
        //     },
        //     cpassword: {
        //         required: true,
        //         minlength: 6,
        //         equalTo: "#password"
        //     },
        //     gender: {
        //         required: true
        //     },
        //     pincode: {
        //         required: true,
        //         number: true,
        //         minlength: 5
        //     },
        //     dob: {
        //         required: true,
        //     },
        //     mobile: {
        //         required: true,
        //         number: true,
        //         minlength: 10
        //     },
        //     '[country[0]': {
        //         required: true
        //     },
        //     'pincode[0]': {
        //         required: true
        //     },
        //     'state_name[0]': {
        //         required: true
        //     },
        //     'state[0]': {
        //         required: true
        //     },
        //     'city[0]': {
        //         required: true
        //     },
        //     'city_text[0]': {
        //         required: true
        //     },
        //     rank: {
        //         required: true
        //     },
        //     years: {
        //         required: true
        //     },
        //     months: {
        //         required: true
        //     }
        // },
        // messages: {
        //     firstname: {
        //         alpha: "Please enter only alphabets",
        //         required: "Please specify your full name"
        //     },
        //     lastname: {
        //         required: "Please specify your last name",
        //         lettersonly: "Please enter characters only"
        //     },
        //     email: {
        //         required: "We need your email address to contact you",
        //         email: "Your email address must be in the format of name@domain.com"
        //     },
        //     cpassword: {
        //         equalTo: "Confirm password is not matching with password "
        //     },
        //     gender: "Please check a gender!",
        //     rank_exp: {
        //         number: "Please enter experience in numbers"
        //     },
        //     mobile: {
        //         number: "Please enter valid Mobile number"
        //     }
        // },
            highlight: function (element) {
                $(element).closest('.help-block').removeClass('valid');
                // display OK icon
                $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
                // add the Bootstrap error class to the control group
            },
            unhighlight: function (element) { // revert the change done by hightlight
                $(element).closest('.form-group').removeClass('has-error');
                // set error class to the control group
            },
            success: function (label, element) {
                label.addClass('help-block valid');
                // mark the current input as valid and display OK icon
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
            }
        });
    };
    var displayConfirm = function () {
        $('.display-value', wizardForm).each(function () {
            var input = $('[name="' + $(this).attr("data-display") + '"]', wizardForm);
            if (input.attr("type") == "text" || input.attr("type") == "email" || input.is("textarea")) {
                $(this).html(input.val());
            } else if (input.is("select")) {
                $(this).html(input.find('option:selected').text());
            } else if (input.is(":radio") || input.is(":checkbox")) {

                $(this).html(input.filter(":checked").closest('label').text());
            } else if ($(this).attr("data-display") == 'card_expiry') {
                $(this).html($('[name="card_expiry_mm"]', wizardForm).val() + '/' + $('[name="card_expiry_yyyy"]', wizardForm).val());
            }
        });
    };

    function check_rpsl(){
        if($("#company_type").val() == 1){
            $("#rpsl-file-container").removeClass('hide');
        }else{
            $("#rpsl-file-container").addClass('hide');
        }
    }
    function check_form(form_id,type,l){
        var formdata = new FormData();
        var action = wizardForm.attr('action');

         $("[name^='country']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select country"
                }
            });
        });

        $("[name^='pincode']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please enter pincode"
                }
            } );
        });

        $("[name^='state']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select state"
                }
            } );
        });

        $("[name^='state_text']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select state"
                }
            } );
        });

        $("[name^='city']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select city"
                }
            } );
        });

        $("[name^='city_text']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select city"
                }
            } );
        });

        $("[name^='address']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please enter address"
                }
            } );
        });

        $("[name^='ship_type']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select ship type"
                }
            } );
        });

        $("[name^='telephone']").each(function(){
            $(this).rules("add", {
                number: true,
                messages: {
                    required: "Please enter numbers only"
                }
            } );
        });

        /*$("[name^='ship_flag']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select ship flag"
                }
            } );
        });*/

        $("[name^='grt']").each(function(){
            $(this).rules("add", {
                required: true,
                number: true,
                messages: {
                    required: "Please enter GRT"
                }
            } );
        });

        $("[name^='engine_type']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select engine type"
                }
            } );
        });

        $("[name^='bhp']").each(function(){
            $(this).rules("add", {
                required: true,
                number: true,
                messages: {
                    required: "Please enter bhp"
                }
            } );
        });

        /*$("[name^='built-year']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select year of built"
                }
            } );
        });*/

        $("[name^='p_i_cover_company_name']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please enter company name"
                }
            } );
        });

        if(wizardForm.valid()) {
            check_rpsl();
            $('#'+form_id+' input').each(function(ele, input) {
                formdata.append(input.name,input.value);
            });
            $('#'+form_id+' select').each(function(ele, input) {
                formdata.append(input.name,input.value);
            });
            $('#'+form_id+' input[type="radio"]:checked').each(function(ele, input) {
                formdata.append(input.name,input.value);
            });
            $('#'+form_id+' textarea').each(function(ele, input) {
                formdata.append(input.name,input.value);
            });

            if(action == 'update'){
                var temp_url = $("#api-admin-store-company-step-update-route").val();
                var url = temp_url.split('%');
                
                url = url[0]+type+'/'+$("#company_id").val();
            }
            if(action == 'store'){
                var temp_url = $("#api-admin-store-company-step-route").val();
                var url = temp_url.split('%');
                if($("#company_id").val())
                    url = url[0]+type+'/'+$("#company_id").val();
                else
                     url = url[0]+type;
            }
            

            $("input[type='file']").each(function(index,element){
                if($(this)[0].files.length > 0){
                    var temp_file_anme = $(this).attr('name');
                    var temp_arr = temp_file_anme.split('_1');
                    var file_name = temp_arr[0];
                    formdata.append(file_name,$(this)[0].files[0]);
                }
            });

            l.start();
            $.ajax({
                type: "POST",
                url: url,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                },
                data: formdata,
                contentType: false,
                processData: false,
                statusCode: {
                    200:function (response) {
                        if(response.company_id){
                            $('#company_id').val(response.company_id);
                        }
                        if(form_id == 'step-2'){
                            window.location.href = response.redirect_url;
                        }
                        wizardContent.smartWizard("goForward");
                        l.stop();
                    },
                    400:function (response) {
                        l.stop();
                    },
                    422:function (response) {
                        for (var prop in response.responseJSON){
                            if(prop == "email"){
                                $("#email-box").addClass('has-error');
                                $("#email-error").removeClass('hide');
                                $("#email-error").text(response.responseJSON['email'][0]);
                            }
                            if(prop == "mobile"){
                                $("#mobile-box").addClass('has-error');
                                $("#mobile-error").removeClass('hide');
                                $("#mobile-error").text(response.responseJSON['mobile'][0]);
                            }
                        }
                    }
                }
            });
            return true;
            //wizardContent.smartWizard("goForward");
        }else{
            return false;
        }
    }

    var onShowStep = function (obj, context) {
    	if(context.toStep == numberOfSteps){
    		$('.anchor').children("li:nth-child(" + context.toStep + ")").children("a").removeClass('wait');
            displayConfirm();
    	}
        $(".next-step").unbind("click").click(function (e) {
            e.preventDefault();
            var data = new FormData();
            var form_id = $(this).attr('data-form');
            var type = $(this).attr('data-type');
            var l = Ladda.create(this);
            check_form(form_id,type,l);

        });
        $(".back-step").unbind("click").click(function (e) {
            e.preventDefault();
            wizardContent.smartWizard("goBackward");
        });
        $(".finish-step").unbind("click").click(function (e) {
            e.preventDefault();
            onFinish(obj, context);
        });
        window.scrollTo(0,0);
    };
    var leaveAStepCallback = function (obj, context) {
        return validateSteps(context.fromStep, context.toStep);
        // return false to stay on step and true to continue navigation
    };
    var onFinish = function (obj, context) {
        if (validateAllSteps()) {
            alert('form submit function');
            $('.anchor').children("li").last().children("a").removeClass('wait').removeClass('selected').addClass('done').children('.stepNumber').addClass('animated tada');
            //wizardForm.submit();
        }
    };
    var validateSteps = function (stepnumber, nextstep) {
        var isStepValid = false;
        
        
        if (numberOfSteps >= nextstep && nextstep > stepnumber) {
        	
            // cache the form element selector
            if (wizardForm.valid()) { // validate the form
                wizardForm.validate().focusInvalid();
                for (var i=stepnumber; i<=nextstep; i++){
        		$('.anchor').children("li:nth-child(" + i + ")").not("li:nth-child(" + nextstep + ")").children("a").removeClass('wait').addClass('done').children('.stepNumber').addClass('animated tada');
        		}
                //focus the invalid fields
                animateBar(nextstep);
                isStepValid = true;
                return true;
            };
        } else if (nextstep < stepnumber) {
        	for (i=nextstep; i<=stepnumber; i++){
        		$('.anchor').children("li:nth-child(" + i + ")").children("a").addClass('wait').children('.stepNumber').removeClass('animated tada');
        	}
            
            animateBar(nextstep);
            return true;
        } 
    };
    var validateAllSteps = function () {
        var isStepValid = true;
        // all step validation logic
        return isStepValid;
    };
    return {
        init: function () {
            initWizard();
            validateCheckRadio();
        }
    };
}();