$(document).on('ready', function(){
	$(document).on('click', '.view-more-cities-button', function(e){
		$("#view-more-cities-modal").modal('show');
		$("#view-more-cities-modal .city-list-content").text($(this).data('cities'));
	});

	$(document).on('click', '.delete-schedule-button', function(e){
		var schedule_id = $(this).data('scheduled-id');
		var el = $(this);
		const modal = new Promise(function(resolve, reject){
	       $('#confirm-reject-modal').modal('show');
	       $('#confirm-reject-modal .modal-heading').html('Delete schedule confirm');
	       $('#confirm-reject-modal .modal-body-content').html('This action will delete the schedule. Are you sure you want to continue?');
	       $('#confirm-reject-modal #modal-confirm-btn').click(function(){
	           resolve(1);
	       });
	       $('#confirm-reject-modal #modal-reject-btn').click(function(){
	           reject(0);
	       });
        }).then(function(val){
        	$('#confirm-reject-modal').modal('hide');
        	$.ajax({
				url : delete_schedule_url,
				method : "POST",
				data : {
					id : schedule_id
				},
				statusCode : {
					200 : function(response) {
						var parent_el = $('#schedules-listing-container').find('tbody tr');
						if (parent_el.length == 1) {
							$('#schedules-listing-container').html('<div>NO schedule to show</div>');
							$('.search-count-content').remove();
						} else {
							el.closest('tr').remove();
						}
					},
					400 : function(response) {
						
					}
				}
				 
			});
	    }).catch(function(err){
	    	$('#confirm-reject-modal').modal('hide');
	    	
	    });

	});




});