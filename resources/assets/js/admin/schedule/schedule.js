$(document).on('ready', function(){

	$('#add-schedule-form').validate({
        rules : {
            'date' : 'required',
            'time' : 'required',
            'country' : 'required',
            'msg_email' : 'required',
        },
        messages : {
            'schedule_date' : 'Please provide the date of schedule',
            'schedule_time' : "Please provide the time to schedule",
            'country' : 'Please select country',
            'msg_email' : 'Please enter message',
        },
    });


	$(document).on('ifClicked', '.city-checkbox', function(){

        $("#city-required-block").addClass('hide');

    	var sub_category = $(this);
    	var state_id = sub_category.closest('.city-select').attr('id');
    	var new_value = state_id.replace('state-id-', '');
		var category_checkbox = $('.state-location-container .state-select .checkbox').find(".state-checkbox[value='"+new_value+"']");
    	setTimeout(function () {
        	if (sub_category.is(':checked')) {
        		category_checkbox.iCheck('check');
        		$('.state-location-container').animate({
    		        scrollTop: category_checkbox.offset().top - $('.state-location-container').offset().top + $('.state-location-container').scrollTop() - 10
    		    }, 300);
        	} else {
        		var check_count = 0;
    			var all_sub_category = sub_category.closest('.city-select').find('.checkbox .city-checkbox').is(':checked');
    			if(!all_sub_category){
    				category_checkbox.iCheck('uncheck');
    			}
        	}

            if( $(".city-checkbox:checked").length == 0 ) {
                $("#city-required-block").removeClass('hide');
                $("#state-required-block").removeClass('hide');
            } else {
                $("#state-required-block").addClass('hide');
            }
    	}, 200);
	});

	$(document).on('change', '#email-country', function(){
    	var country_val  = $(this).val();
		if (country_val == 95) {
			$('.state-city-container').css('display','block');
		} else {
			$('.state-city-container').css('display','none');
		}
    });

    $(document).on('ifChanged', '.select-all-state', function(){

        $("#state-required-block").addClass('hide');

    	var select_all = $(this);
		if (select_all.is(':checked')) {
			$('.email-location-checkbox').iCheck('check'); 
		} else {
			$('.email-location-checkbox').iCheck('uncheck');
		}
        if( $(".state-checkbox:checked").length == 0 ) {
            $("#state-required-block").removeClass('hide');
            $("#city-required-block").removeClass('hide');
        } else {
            $("#city-required-block").addClass('hide');
        }
    });

    $(document).on('ifClicked', '.state-checkbox', function(){

        $("#state-required-block").addClass('hide');

		var state = $(this);
		var state_id = state.val();
    	var city = 'state-id-'+state_id;
    	setTimeout(function () {
            if (state.is(':checked')) {
            	$('#'+city+' .checkbox .email-location-checkbox').iCheck('check');

    		    $('.city-location-container').animate({
    		        scrollTop: $('#'+city).offset().top - $('.city-location-container').offset().top + $('.city-location-container').scrollTop()
    		    }, 300);
            } else {
            	$('#'+city+' .checkbox .email-location-checkbox').iCheck('uncheck');
            }
            if( $(".state-checkbox:checked").length == 0 ) {
                $("#state-required-block").removeClass('hide');
                $("#city-required-block").removeClass('hide');
            } else {
                $("#city-required-block").addClass('hide');
            }
    	}, 200);
    });


});