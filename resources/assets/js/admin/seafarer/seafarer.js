$(document).ready(function() {

    $("#seafarerResetButton").on('click', function(){
        $(':input','.filter-search-form')
            .not(':button, :submit, :reset, :hidden')
            .val('')
            .removeAttr('checked')
            .removeAttr('selected');
    });

    $("#us").on('change', function(e){
        var us_check = $(this).val();
        if(us_check == '1'){
            $('.us-block').removeClass('hide');
        }else{
            $('.us-block').addClass('hide');
        }
    });

    $("input[name='pcc']").on('change', function(e){
        var pcc_check = $(this).val();
        if(pcc_check == '1'){
            $('.pcc-block').removeClass('hide');
        }else{
            $('.pcc-block').addClass('hide');
        }
    });

    $("input[name='yellowfever']").on('change', function(e){
        var yellowfever_check = $(this).val();
        if(yellowfever_check == '1'){
            $('.yellowfever-block').removeClass('hide');
        }else{
            $('.yellowfever-block').addClass('hide');
        }
    });

    $("#current_rank").on('change', function(e){
        
        $("#current_rank_id").val($(this).val());
        var fields = required_fields[$(this).val()];
        
        $(".required_fields").addClass('hide');
        //$(".required_fields input").val('');

        for(var i=0; i<fields.length; i++) {
            var field_arr = fields[i].split('-');
            if(field_arr.length > 1) {
                $("#"+field_arr[0]+"-details").removeClass('hide');
                $("#"+field_arr[0]+"-details span").addClass('hide' );
            } else{
                $("#"+fields[i]+"-details").removeClass('hide');
            }
        }
    });
    
  $(".change-status-user").on('click',function(e){
      var status = $(this).attr('data-status');
      var id = $(this).attr('data-id');
      var url = $('#api-admin-seafarer-change-status').val();

      $.ajax({
              type: "POST",
              url: url,
              headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
              },
              data: {
                status: status,
                id: id,
              },
              statusCode: {
                  200:function (response) {
                    if(status == 'active'){
                      $('#deactive-'+id).removeClass('hide');
                      $('#active-'+id).addClass('hide');
                    }else{
                      $('#active-'+id).removeClass('hide');
                      $('#deactive-'+id).addClass('hide');
                    }
                    toastr.success(response.message, 'Success');
                  },
                  400:function (response) {
                    toastr.error(response.message, 'Error');
                  }
              }
          });
  });

	$('#add-seafarer-form').validate({
		rules: {
            firstname: {
                alpha: true,
                required: true
            },
            /*lastname: {
                lettersonly: true,
                minlength: 2,
                required: true
            },*/
            email: {
                required: true,
                email: true,
                remote: {
                    url: $("#api-check-email").val(),
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content'),
                    },
                    type: "post",
                    async: false,
                    data: {
                        id: function(){ return $("input[name='user_id']").val()},
                        email: function(){ return $("#email").val(); }
                    }
                }
            },
            password: {
                minlength: 6,
                required: true
            },
            cpassword: {
                required: true,
                minlength: 6,
                equalTo: "#password"
            },
            gender: {
                required: true
            },
            pincode: {
                required: true,
                number: true,
                minlength: 5
            },
            dob: {
                required: true,
            },
            mobile: {
                required: true,
                number: true,
                minlength: 10,
                remote: {
                    url: $("#api-check-mobile").val(),
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content'),
                    },
                    type: "post",
                    async: false,
                    data: {
                        id: function(){ return $("input[name='user_id']").val()},
                        mobile: function(){ return $("#mobile").val(); }
                    }
                },
                notEqualTo: "#kin_number",
            },
            '[country[0]': {
                required: true
            },
            'pincode[0]': {
                required: true
            },
            'state_name[0]': {
                required: true
            },
            'state[0]': {
                required: true
            },
            'city[0]': {
                required: true
            },
            'city_text[0]': {
                required: true
            },
            current_rank: {
                required: true
            },
            years: {
                required: true
            },
            months: {
                required: true
            },
            passcountry: {
                required: true
            },
            passno: {
                required: true
            },
            passplace: {
                required: true
            },
            passdateofissue: {
                required: true
            },
            passdateofexp: {
                required: true
            },
            yf_issue_date: {
                required: true
            },
            us: {
                required: true
            },
            usvalid: {
                required: true
            },
            pccdate: {
                required: true
            },
            cdccountry: {
                required: true
            },
            cdcno: {
                required: true
            },
            cdc_exp: {
                required: true
            },
            ocdccountry: {
                required: true
            },
            ocdcno: {
                required: true
            },
            ocdc_exp: {
                required: true
            },
            coc_no: {
                required: true
            },
            coc_exp: {
                required: true
            },
            coc_country: {
                required: true
            },
            grade: {
                required: true
            },

            ococ_no: {
                required: true
            },
            ococ_exp: {
                required: true
            },
            ococ_country: {
                required: true
            },
            ograde: {
                required: true
            },
            /*gmdss_country: {
                required: true
            },
            gmdss_no: {
                required: true
            },
            gmdss_doe: {
                required: true
            },*/
            place_of_birth: {
                required: true
            },
            nationality: {
                required: true,
            },
            marital_status: {
                required: true,
            },
            height: {
                required: true,
                number: true,
            },
            weight: {
                required: true,
                number: true,
            },
            kin_name: {
                required: true,
                alpha: true
            },
            kin_relation: {
                required: true
            },
            kin_number: {
                required: true,
                number: true,
                minlength: 10,
                notEqualTo: "#mobile",
            },
            kin_alternate_no: {
                number: true,
            },
            permananent_address: {
                required: true
            },
            landline: {
                number: true
            },
            applied_rank: {
                required: true
            },
            date_avaibility: {
                required: true
            },
            last_wages: {
                required: true,
                number: true
            },
            /*name_of_school: {
                required: true
            },
            school_from: {
                required: true
            },
            school_to: {
                required: true
            },
            school_qualification: {
                required: true
            },
            institute_name: {
                required: true
            },
            institute_from: {
                required: true
            },
            institute_to: {
                required: true
            },
            institute_degree: {
                required: true
            }*/
        },
        messages: {
            firstname: {
                alpha: "Please enter only alphabets",
                required: "Please specify your full name"
            },
            lastname: {
                required: "Please specify your last name",
                lettersonly: "Please enter characters only"
            },
            email: {
                required: "We need your email address to contact you",
                email: "Your email address must be in the format of name@domain.com",
                remote: "Email already exist"
            },
            cpassword: {
                equalTo: "Confirm password is not matching with password "
            },
            gender: "Please check a gender!",
            rank_exp: {
                number: "Please enter experience in numbers"
            },
            mobile: {
                number: "Please enter valid Mobile number",
                remote: "Mobile already exist"
            },
            passcountry: "Please select passport country",
            passno: "Please enter Passport Number",
        }
	});

	$(document).on('click',".seafarer-next-btn",function(e){
        e.preventDefault();

        var optional = new Array();
        var current_rank = $("#current_rank").val();
        if(current_rank){
            var fields = required_fields[current_rank];

            for(var i=0; i<fields.length; i++) {
                var field_arr = fields[i].split('-');
                if(field_arr.length > 1) {
                    optional.push(field_arr[0]);
                }
            }
        }

        $("[name^='cdccountry']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select country"
                }
            });
        });

        $("[name^='cdcno']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please enter number"
                }
            });
        });

        $("[name^='cdc_issue']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select issue date"
                }
            });
        });

        $("[name^='cdc_exp']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select expiry date"
                }
            });
        });

        /*$("[name^='coecountry']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select country"
                }
            });
        });

        $("[name^='coeno']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please enter number"
                }
            });
        });*/

        /*$("[name^='coe_grade']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please enter grade"
                }
            });
        });

        $("[name^='coe_exp']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select expiry date"
                }
            });
        });

        $("[name^='cop_issue']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select issue date"
                }
            });
        });

        $("[name^='cop_exp']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select expiry date"
                }
            });
        });*/

        /*$("[name^='cop_no']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please enter number"
                }
            });
        }); 

        $("[name^='cop_grade']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please enter grade"
                }
            });
        });*/

        if(jQuery.inArray('WATCH_KEEPING',optional) == -1){

            $("[name='watch_country']").each(function(){
                $(this).rules("add", {
                    required: true
                });
            });

            $("[name='deckengine']").each(function(){
                $(this).rules("add", {
                    required: true
                });
            });
            $("[name='watch_no']").each(function(){
                $(this).rules("add", {
                    required: true
                });
            });
            $("[name='watchissud']").each(function(){
                $(this).rules("add", {
                    required: true
                });
            });
        }

        if(jQuery.inArray('COC',optional) == -1){
            $("[name^='coc_country']").each(function(){
                $(this).rules("add", {
                    required: true,
                    messages: {
                        required: "Please enter country"
                    }
                });
            });

            $("[name^='coc_no']").each(function(){
                $(this).rules("add", {
                    required: true,
                    messages: {
                        required: "Please enter number"
                    }
                });
            });

            $("[name^='grade']").each(function(){
                $(this).rules("add", {
                    required: true,
                    messages: {
                        required: "Please enter grade"
                    }
                });
            }); 

            $("[name^='coc_exp']").each(function(){
                $(this).rules("add", {
                    required: true,
                    messages: {
                        required: "Please enter expiry date"
                    }
                });
            });
        }

        /*$("[name^='value_added_certificate_name']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select certificate name"
                }
            });
        });

        $("[name^='value_added_date_of_issue']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select issue date"
                }
            });
        });

        $("[name^='value_added_issue_by']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select issue by"
                }
            });
        });

        $("[name^='value_added_issuing_authority']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select issuing authority"
                }
            });
        });

        $("[name^='value_added_place_of_issue']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select place of issue"
                }
            });
        });

        $("[name^='value_added_certificate_no']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please enter certificate number"
                }
            });
        });*/

        /*$("[name^='certificate_name']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select certificate name"
                }
            });
        });

        $("[name^='date_of_issue']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select issue date"
                }
            });
        });*/

        /*$("[name^='date_of_expiry']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select expiry date"
                }
            });
        });*/

        /*$("[name^='issue_by']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select issue by"
                }
            });
        });*/

        $("[name^='issuing_authority']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select issuing authority"
                }
            });
        });

        $("[name^='place_of_issue']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select place of issue"
                }
            });
        });

        /*$("[name^='certificate_no']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please enter certificate number"
                }
            });
        });*/

        $("[name^='shipping_company']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please enter shipping company name"
                }
            });
        });

        $("[name^='name_of_ship']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please enter ship name"
                }
            });
        });

        $("[name^='ship_type']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select ship type"
                }
            });
        });

        $("[name^='rank']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select rank"
                }
            });
        });

        $("[name^='grt']").each(function(){
            $(this).rules("add", {
                number: true,
                messages: {
                    number: "Please enter grt in numbers"
                }
            });
        });

        $("[name^='bhp']").each(function(){
            $(this).rules("add", {
                number: true,
                messages: {
                    number: "Please enter bhp in numbers"
                }
            });
        });

        /*$("[name^='engine_type']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select engine type",
                }
            });
        });*/

        $("[name^='date_from']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select sign on date"
                }
            });
        });

        $("[name^='date_to']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select sign off date"
                }
            });
        });

       /* $("[name^='ship_flag']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select ship flag"
                }
            });
        });*/

        $("[name^='course_date_of_issue']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select date of issue"
                }
            });
        });

        
        $("[name^='value_added_course_date_of_issue']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select date of issue"
                }
            });
        });

        $('#add-seafarer-form').valid();
    });

    $('#add-seafarer-finish-button').on('click', function(){

      if($('#add-seafarer-form').valid()){
        var l = Ladda.create(this);
        l.start();
        $.ajax({
            type: "POST",
            url: $("#add-seafarer-form").attr('action'),
            data: $("#add-seafarer-form").serialize(),
            statusCode: {
                200:function (response) {
                    toastr.success(response.message, 'Success');
                    l.stop();
                    window.location.href = response.redirect_url;
                },
                400:function (response) {
                    toastr.error(response.message, 'Error');
                    l.stop();
                },
                422:function (response) {
                    l.stop();
                    $("#step-4").hide();
                    $("#step-1").show();
                    for (var prop in response.responseJSON){
                        if(prop == "email"){
                            $("#email-box").addClass('has-error');
                            $("#email-error").removeClass('hide');
                            $("#email-error").text(response.responseJSON['email'][0]);
                        }
                        if(prop == "mobile"){
                            $("#mobile-box").addClass('has-error');
                            $("#mobile-error").removeClass('hide');
                            $("#mobile-error").text(response.responseJSON['mobile'][0]);
                        }
                    }
                    l.stop();
                }
            }
        });

      }

    });

    $(document).on('click','.resend_welcome_email',function (e) {
        $.ajax({
            type: "POST",
            url: $("#api-admin-send-welcome-email").val(),
            data: {
                id: $(this).data('id'),
            },
            statusCode: {
                200:function (response) {
                    toastr.success(response.message, 'Success');
                },
                400:function (response) {
                    toastr.error(response.responseJSON.message, 'Error');
                },
                422:function (response) {
                    
                }
            }
        });

    });

});