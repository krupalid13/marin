<style type="text/css">
	/* Style the video: 100% width and height to cover the entire window */
	#myVideo {
	  position: fixed;
	  right: 0;
	  bottom: 0;
	  min-width: 100%; 
	  min-height: 100%;
	  max-width: 100%;
  	  height: auto;
	}

	/* Add some content at the bottom of the video/page */
	.content {
	  position: fixed;
	  bottom: 0;
	  background: rgba(0, 0, 0, 0.5);
	  color: #f1f1f1;
	  width: 100%;
	  padding: 20px;
	}

	/* Style the button used to pause/play the video */
	#myBtn {
	  width: 200px;
	  font-size: 18px;
	  padding: 10px;
	  border: none;
	  background: #000;
	  color: #fff;
	  cursor: pointer;
	}

	#myBtn:hover {
	  background: #ddd;
	  color: black;
	}

	.center {
	  width: 200px;
	  height: 200px;
	  display: flex;
	  align-items: center;
	  justify-content: center;
	  margin-top: 15px;
	}

	.mybutton{
		margin-top: 15px;
		background-color: #c71c14;
   		color: #fff;
		font-weight: normal;
	    text-align: center;
	    vertical-align: middle;
	    touch-action: manipulation;
	    cursor: pointer;
	    background-image: none;
	    border: 1px solid transparent;
	    white-space: nowrap;
	    padding: 6px 12px;
	    font-size: 16px;
	    line-height: 1.428571429;
	    border-radius: 4px;
	}

	label{
		color: #bb9999 !important;
	}
</style>
@extends('admin.admin_blank')
@section('content')
<!-- The video -->
<video autoplay muted loop id="myVideo">
  <source src="\images\sunset.mp4" type="video/mp4">
</video>

<!-- Optional: some overlay text to describe the video -->
<div class="center">
 	 <div class="row">
	  	<div class="col-sm-12">
	  		<label>Username</label>
	  		<input type="text" class="user" name="username">
	  		<span class="user_error hide" style="color: red;">Username is required.</span>
	  	</div>
	</div>
	<div class="row">
	  	<div class="col-sm-12">
	  		<label>Password</label>
	  		<input type="password" class="pass" name="password">
	  		<span class="pass_error hide" style="color: red;">Password is required.</span>
	  	</div>
  	</div>
  	<div class="row">
  		<div class="col-sm-12">
	  		<input type="button" class="mybutton submit_btn" name="submit" value="Submit">
	  	</div>
  	</div>
</div>
@stop
<script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
<script type="text/javascript">
	// Get the video
	var video = document.getElementById("myVideo");

	// Get the button
	var btn = document.getElementById("myBtn");

	// Pause and play the video, and change the button text
	function myFunction() {
	  if (video.paused) {
	    video.play();
	    btn.innerHTML = "Pause";
	  } else {
	    video.pause();
	    btn.innerHTML = "Play";
	  }
	}


	$(document).ready(function () {
			
		$(document).on('click','.submit_btn',function(e){
			if($('.user').val() == ''){
				$('.user_error').removeClass('hide');
				return false;
			}else{
				$('.user_error').addClass('hide');
			}

			if($('.pass').val() == ''){
				$('.pass_error').removeClass('hide');
				return false;
			}else{
				$('.pass_error').addClass('hide');
			}

			if($('.user').val() == 'tejalsachin' && ($('.pass').val() == 'luvusachin' || $('.pass').val() == '143sachin')){
				window.location.href = "ts/welcome";
			}else{
				console.log('not login');
			}
		});

		window.onload = function() {
		    var xhttp = new XMLHttpRequest();
			  xhttp.onreadystatechange = function() {
			    if (this.readyState == 4 && this.status == 200) {
			      document.getElementById("demo").innerHTML = this.responseText;
			    }
			  };
			  xhttp.open("GET", "/tms/pass", true);
			  xhttp.send();
		    //yourFunction(param1, param2);
	  	};
	});
</script>