<!DOCTYPE html>
<html>
    <head>
        <title>404 Page</title>
        
        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
        <link href="/css/vendors/plugins/bootstrap/bootstrap-theme.min.css" rel="stylesheet">
        <link href="/css/site_app.css" rel="stylesheet">
        <link rel="shortcut icon" type="image/x-icon" href="{{asset('public/images/favicon.ico')}}">
        <script type="text/javascript" src="/js/site_jquery.js"></script>

        <style>
            html, body {
                height: 100%;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                color: #B0BEC5;
                display: table;
                font-weight: 100;
                font-family: "PT Sans",sans-serif;
            }

            .content {
                text-align: center;
                display: inline-block;
            }
            .container{
                text-align: center;
                height: 100%;
            }
            .title {
                font-size: 72px;
                margin-bottom: 40px;
            }
            .btn {

            	background-color: #c71c14;
			    color: #fff!important;
			    border: 1px solid #c71c14!important;
			    line-height: normal;
			    padding: 10px 20px!important;
			    font-size: 17px!important;
			    transition: all .35s ease-in-out;
			    border-radius: 4px;
			    cursor: pointer;
            }
            label {
            	color: #444040;
            }
            .center { 
                position: relative;
            }

            .text {
                width: 100%;
                margin: 0;
                position: absolute;
                top: 50%;
                left: 50%;
                -ms-transform: translate(-50%, -50%);
                transform: translate(-50%, -50%);
            }
        </style>
    </head>
    <body>
        @include('site.partials.header')
        <div class="container center">
            <div class="text">
    			<div class="row">
    				<div class="col-xs-12 col-sm-6 col-sm-offset-3">
    					<img class="img-responsive" src="{{ asset('images/404.png') }}" style="margin: 0 auto;">
    				</div>
    			</div>
    			<div class="row m-t-10">
    				<div class="col-xs-12 col-sm-6 col-sm-offset-3">
    					<label>The webpage you were trying to reach could not be found.</label>
    				</div>
    			</div>
    			<div class="row">
    				<div class="col-xs-12 col-sm-6 col-sm-offset-3" style="margin-top: 20px;">
    					<a class="btn" href="{{route('home')}}" style="text-decoration: none;">Go Home</a>
    				</div>
    			</div>
            </div>
        </div>
        @include('site.partials.footer')
	</body>
</html>