<!DOCTYPE html>
<html>

<head>
    <title>Gone.</title>

    <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{ asset('public/assets/css/style.css') }}" />
    <link href="{{ URL::asset('public/assets/css/site_app.css') }}" rel="stylesheet" />
    @stack('styles')
    <script type="text/javascript" src="{{ URL::asset('public/assets/js/site_jquery.js') }}"></script>
    <link rel="stylesheet" href="{{ asset('public/assets/vendors/toastr/toastr.min.css') }}" />
    <script type="text/javascript" src="{{ URL::asset('public/assets/js/site_plugins.js') }}"></script>
    <script src="{{ asset('public/assets/vendors/toastr/toastr.min.js') }}"></script>
    <link rel="shortcut icon" type="image/x-icon" href="{{asset('public/images/favicon.ico')}}">
    <style>
        html,
        body {
            height: 100%;
        }

        body {
            margin: 0;
            padding: 0;
            width: 100%;
            color: #0637c5;
            display: table;
            font-weight: 100;
            font-family: 'Lato', sans-serif;
        }

        .container {
            text-align: center;
            display: table-cell;
            width: 100%;
            vertical-align: middle;
        }

        .content {
            text-align: center;
            display: inline-block;
        }

        .title {
            font-size: 72px;
            margin-bottom: 40px;
        }

        .image-preview {
            width: 15%;
        }

        h1 {
            padding-left: 16px;
            padding-right: 16px;
            font-size: 20px;
            margin-bottom: 40px
        }

        h1 {
            color: rgb(69,69,69);
            font-family: Arial, Helvetica, sans-serif;
        }

        .btn-primary {
            padding-left: 64px;
            padding-right: 64px;
        }

    </style>
    @php
        $codes = explode('###', $exception->getMessage());
    @endphp
    <script>
        $(document).ready(function() {
            toastr.options = {
                "closeButton": false,
                "debug": false,
                "newestOnTop": true,
                "progressBar": true,
                "positionClass": "toast-top-center",
                "preventDuplicates": true,
                "onclick": null,
                "showDuration": "5000",
                "hideDuration": "5000",
                "timeOut": "5000",
                "extendedTimeOut": "5000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            }
            @if ($codes[2] && $codes[2] == 'true')
                toastr.success("You have requested for a new link.");
            @endif
        })

    </script>
    @stack('scripts')
</head>

<body>
    <div class="container">
        <div class="content">
            <form method="POST">
                {{ csrf_field() }}
                <img class="image-preview" src="{{ asset('public/assets/images/document-link-expired.png') }}" />
                <h1>{!! $codes[0] !!}</h1>
                <input type="hidden" name="share_token" value="{{ $codes[1] }}" />
                @if ($codes[2] && $codes[2] == 'false')
                    <!--<button class="btn btn-primary">Request for a New Link</button>-->
                @endif
            </form>
        </div>
    </div>
</body>

</html>
