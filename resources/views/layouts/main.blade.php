<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="_token" content="{{ csrf_token() }}">
    <title>
    @if(isset($pageTitle) && !empty($pageTitle))
        {!! $pageTitle !!}
    @else
        {{env('APP_NAME')}}
    @endif
    </title>
    <meta name="description" content="{!! ($metaDescription) ?? '' !!}">
    <meta name="keywords" content="{!! ($metaKeywords) ?? '' !!}">
    <link rel="stylesheet" href="{{asset('public/assets/css/style.css')}}"/>
    <link href="{{ URL:: asset('public/assets/css/site_app.css')}}" rel="stylesheet"/>
    @stack('styles')
    <script type="text/javascript" src="{{ URL:: asset('public/assets/js/site_jquery.js')}}"></script>
    <link rel="stylesheet" href="{{asset('public/assets/vendors/toastr/toastr.min.css')}}"/>
    <link rel="shortcut icon" type="image/x-icon" href="{{asset('public/images/favicon.ico')}}">
</head>
<body>
<div class="se-pre-con"></div>
@include('site.partials.header')
@yield('content')
<footer class="container">
    <div class="row">
        <div class="col-lg-12 text-center p-10">
            <div class="copyryt">Copyright @ {{\Carbon\Carbon::now()->year}} | <a
                        href="{{env('APP_URL')}}">{{env('APP_NAME')}}</a> | All Rights Reserved | Terms and Conditions
            </div>
        </div>
    </div>
</footer>
<script type="text/javascript" src="{{ URL:: asset('public/assets/js/site_plugins.js')}}"></script>
<script src="{{asset('public/assets/vendors/toastr/toastr.min.js')}}"></script>

<script>
    $(document).ready(function(){
    toastr.options = {
        "closeButton": false,
        "debug": false,
        "newestOnTop": true,
        "progressBar": true,
        "positionClass": "toast-top-center",
        "preventDuplicates": true,
        "onclick": null,
        "showDuration": "5000",
        "hideDuration": "5000",
        "timeOut": "5000",
        "extendedTimeOut": "5000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    }
    @if(session('success'))
        toastr.success("{{ session('success') }}");
        @php session()->forget('success'); @endphp
    @endif

    @if(session('error'))
        toastr.error("{{ session('error') }}");
        @php session()->forget('error'); @endphp
    @endif

    @if(count($errors)>0)
        @foreach($errors->all() as $error)
            toastr.error('{{ $error }}');
        @endforeach
    @endif

    @if(session('info'))
        toastr.info("{{ session('info') }}");
        @php session()->forget('info'); @endphp
    @endif
})
        $(window).load(function() {
            $(".se-pre-con").fadeOut("slow");
        });
</script>
@stack('scripts')
</body>
</html>