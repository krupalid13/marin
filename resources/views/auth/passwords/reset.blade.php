<?php
    isset($_GET['set_password']) ? $title = 'Set Password' : $title = 'Reset Password';
?>
@extends('site.index')
@section('content')

<div class="login_home_page content-section-wrapper">
    <div class="section container">
        <div class="row">
            <div class="col-xs-12">
                <div class="login_page login-box">
                    <div id="login-popup" class="white-popup signin-sigup-default no-margin" role="dialog" >
                        <div class="signin-signup-heading">
                            <div class="signin-signup new-sigin-link active login_page"><a class="open-popup-link">{{$title}}</a></div>
                        </div>
                        <form id="loginForm" class="form-horizontal" role="form" method="POST" action="{{ url('/password/reset') }}">
                            {{ csrf_field() }}
                            <div class="new-login-content">
                                <div class="small-12 medium-12 columns">
                                    <div class="new-login-form-container">
                                        <input type="hidden" name="token" value="{{ $token }}">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <b>Please enter your registered email address.</b>
                                            </div>
                                        </div>
                                        <div class="row margin-vert-5 {{ $errors->has('email') ? ' has-error' : '' }}">
                                            <div class="col-sm-4 padding-top-5">
                                                <label class="mar-b-10"><span class="label-icon">
                                                    <i class="fa fa-envelope" aria-hidden="true"></i></span>
                                                    <span class="label-text mandatory">Email</span>
                                                </label>
                                            </div>

                                            <div class="col-md-8">
                                                <input id="email" type="email" class="form-control" name="email" value="{{ $email or old('email') }}" required autofocus placeholder="Enter email address">

                                                @if ($errors->has('email'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('email') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="row margin-vert-5 {{ $errors->has('password') ? ' has-error' : '' }}">
                                            <div class="col-sm-4 padding-top-5">
                                                <label class="mar-b-10">
                                                    <span class="label-icon">
                                                        <i class="fa fa-lock fa-lg" aria-hidden="true"></i>
                                                    </span>
                                                    <span class="label-text mandatory">Password</span>
                                                </label>
                                            </div>

                                            <div class="col-md-8">
                                                <input id="password" type="password" class="form-control" name="password" required placeholder="Enter your password">

                                                @if ($errors->has('password'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('password') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="row margin-vert-5 {{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                            <div class="col-sm-4 padding-top-5">
                                                <label class="mar-b-10">
                                                    <span class="label-icon">
                                                        <i class="fa fa-lock fa-lg" aria-hidden="true"></i>
                                                    </span>
                                                    <span class="label-text mandatory">Confirm Password</span>
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required placeholder="Enter your password">

                                                @if ($errors->has('password_confirmation'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="row margin-vert-5">
                                            <div class="col-sm-12">
                                                <button type="submit" data-style='zoom-in' class="btn-1 light-blue new-sign-up-btn fullwidth ladda-button m-t-15">{{$title}}</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@stop
@section('js_script')
    <script type="text/javascript">
        $("#loginForm").validate({
            rules: {
                email: {
                    required: true,
                    email: true,
                },
                password: {
                    minlength: 6,
                    required: true,
                },
                password_confirmation: {
                    minlength: 6,
                    required: true,
                    equalTo: "#password",
                }
            },
            messages: {
                password_confirmation: {
                    equalTo: "Confirm password is not matching with password "
                },
            }
        });
    </script>
@stop