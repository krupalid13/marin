@extends('emails.default_layout')

@section('email_content')
	<table class="" width="100%" style="max-width:600px;"> 
        <tbody>
            <tr> 
                <td align="left" valign="top" class="" style="color:#2c2c2c;display:block;line-height:20px;font-weight:300;margin:0 auto;clear:both;background-color:#fff;padding:20px" bgcolor="#F9F9F9"> 
                    <p style="padding:0;margin:0;font-size:16px;font-weight:bold;font-size:13px"> 
                        Hi {{isset($template['company_registration_details']['company_name']) ? $template['company_registration_details']['company_name'] : ''}}, 
                    </p>
                    <br> 
                    <p style="padding:0;margin:0;font-size:16px;font-size:13px"> We have found matching candidates according to job details.</p>
                    <br>

                    <?php
                    	$i = 0;
						$maxiterations = 5;
                    ?>

                	@foreach($template['job_matching_seafarers'] as $seafarer)
                		<table width="100%" cellspacing="5" style="border:1px solid #ddd;margin-bottom: 7px;">
                    		@if ($i < $maxiterations)
                    		<?php
                    			$img_path = '';
                    			if(isset($seafarer['profile_pic']) AND !empty(isset($seafarer['profile_pic']))){
                    				$img_path = $seafarer['profile_pic'];
                    			}
                    		?>
                    		
							<tr style="border:1px solid #ddd;">
						    	<td height="150px" width="150px" style="text-align: center;padding-right: 5px;">
						    		<a href="{{isset($template['seafarer_profile_route']) ? $template['seafarer_profile_route'].'/'.$seafarer['id'] : ''}}" target="_blank">
							    		@if(!empty($img_path))
						    				<img class="profile-pic" src="{{asset(isset($seafarer['profile_pic']) ? env('SEAFARER_PROFILE_PATH').$seafarer['id'].'/'.$seafarer['profile_pic'] : 'images/user_default_image.png')}}">
							    			
							    		@else
							    			<img class="profile-pic" src="{{ asset('images/user_default_image.png') }}" alt="" height="150px" width="150px">
							    		@endif
						    		</a>
						    	</td>
						    	<td width="40%" style="padding-left: 10px;">
						    		<div class="company-name" style="margin-bottom: 10px;">
				    					{{ isset($seafarer['first_name']) ? $seafarer['first_name'] : ''}}
				    				</div>
				                    <div class="col-sm-6" style="font-size: 12px;">
			    						Applied Rank: 
			    						@foreach(\CommonHelper::new_rank() as $index => $category)
									    	@foreach($category as $r_index => $rank)
	                                        	{{ !empty($seafarer['professional_detail']['applied_rank']) ? $seafarer['professional_detail']['applied_rank'] == $r_index ? $rank : '' : ''}}
	                                        @endforeach
	                                    @endforeach

			    					</div>
							    	<div class="other-discription row">
				    					<div class="col-sm-6" style="font-size: 12px;">
				    						Current Rank: 
				    						@foreach(\CommonHelper::new_rank() as $index => $category)
										    	@foreach($category as $r_index => $rank)
		                                        	{{ !empty($seafarer['professional_detail']['current_rank']) ? $seafarer['professional_detail']['current_rank'] == $r_index ? $rank : '' : ''}}
		                                        @endforeach
		                                    @endforeach
				    					</div>
				    				</div>
				    				<?php
                                        if(isset($seafarer['professional_detail']['current_rank_exp'])){
                                            $rank_exp = explode(".",$seafarer['professional_detail']['current_rank_exp']);
                                            $rank_exp[1] = number_format($rank_exp[1]);
                                        }
				    				?>
			    					<div class="other-discription row">
				    					<div class="col-sm-6" style="font-size: 12px;">
				    					Current Rank Exp: 
				    					{{ isset($seafarer['professional_detail']['current_rank_exp']) ? $rank_exp[0] ." Years ". $rank_exp[1].' Months' : ''}}
				    					</div>
				    				</div>
				    				
						    	</td>
						    	<td style="padding-top: 30px;">
			    					<div class="col-sm-6" style="font-size: 12px;">
				    					Country: 
				    						@foreach( \CommonHelper::countries() as $c_index => $country)
                                                {{ isset($seafarer['personal_detail']['country']) ? $seafarer['personal_detail']['country'] == $c_index ? $country : '' : ''}}
                                            @endforeach
				                    </div>
				                    <div class="col-sm-6" style="font-size: 12px;">
				                    	State : 
			                                {{ isset($seafarer['personal_detail']['state_id']) ? ucfirst(strtolower($seafarer['personal_detail']['pincode']['pincodes_states'][0]['state']['name'])) : (isset($seafarer['personal_detail']['state_text']) ? ucfirst(strtolower($seafarer['personal_detail']['state_text'])) : '')}}
				                    </div>
			    					<div class="col-sm-6" style="font-size: 12px;">
			    						City: 
			                                {{ isset($seafarer['personal_detail']['city_id']) ? $seafarer['personal_detail']['pincode']['pincodes_cities'][0]['city']['name'] : (isset($seafarer['personal_detail']['city_text']) ? $seafarer['personal_detail']['city_text'] : '')}}
			    					</div>
			    					<div class="col-sm-6" style="font-size: 12px;">
			    						Postal Code: 
			                                {{ isset($seafarer['personal_detail']['pincode_text']) ? $seafarer['personal_detail']['pincode_text'] : ''}}
			    					</div>
						    	</td>
						    </tr>
						    
						    <?php $i++; ?>
						   	@endif
						</table>
		    			@endforeach
		    			
		    			@if(isset($template['job_matching_seafarers']) AND count($template['job_matching_seafarers']) > 5)
		    			<table width="100%">
							<tr>
				    			<td colspan="3" style="padding-top: 15px;">
				    				<p style="padding:0;margin:0;font-size:16px;font-size:13px"> To view more candidate please click on below button.</p>
	                    			<br>
				    				<a class="btn" style="border-radius: 4px;background-color: #c71c14 !important;color: #fff !important;border: 1px solid #c71c14 !important;text-decoration: none;line-height: normal;padding: 8px 20px !important;
				                        font-size: 16px !important;" href="{{isset($template['route']) ? $template['route'] : ''}}" target="_blank">
				                        View More Candidate
				                    </a>
				    			</td>
				    		</tr>
				    	</table>
			    		@endif
					
                </td> 
            </tr>
         </tbody>
    </table>
@stop