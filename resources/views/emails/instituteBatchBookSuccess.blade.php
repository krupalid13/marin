@extends('emails.default_layout')

@section('email_content')
	<table class="" width="100%" cellspacing="0" cellpadding="0" style="max-width:600px;"> 
        <tbody>
            <tr> 
                <td align="left" valign="top" class="" style="color:#2c2c2c;display:block;line-height:20px;font-weight:300;margin:0 auto;clear:both;background-color:#fff;padding:20px" bgcolor="#F9F9F9"> 
                    <p style="padding:0;margin:0;font-size:16px;font-weight:bold;font-size:13px"> 
                        Hi {{isset($template['seafarer']['0']['first_name']) ? ucwords($template['seafarer']['0']['first_name']) : ''}},
                    </p>
                    <br> 

                    <?php 
                        if(isset($template['order_details'][0]['status']) && $template['order_details'][0]['status'] == '6'){
                            $pay_type = 'partial';
                        }else{
                            $pay_type = 'full';
                        }

                        $start_date = isset($template['start_date']) ? $template['start_date'] : '';
                        $expiry_date = date('d-m-Y', strtotime('-7 days', strtotime($start_date))); 
                    ?>
                    <p style="padding:0;margin:0;color:#565656;font-size:13px"> Thank you for the {{$pay_type}} payment againt your booking.</p>

                    @if(isset($template['order_details'][0]['status']) && $template['order_details'][0]['status'] == '6')
                        <p style="padding:0;margin:0;color:#565656;font-size:13px"> Please make the pending payment before {{$expiry_date}}. If you fail to make the payment before the {{$expiry_date}} you booking will be cancelled.</p>
                    @endif

                    @if(isset($template['order_details'][0]['status']) && $template['order_details'][0]['status'] == '1')
                        <p style="padding:0;margin:0;color:#565656;font-size:13px"> Your seat booking has been confirmed.</p>
                    @endif

                    <br> 
                    <p style="padding:0;margin:0;color:#565656;font-size:13px"> <b>Batch details are as follows:-</b></p>
                    <h5>Course Type : <strong>
                        @foreach(\CommonHelper::institute_course_types() as $r_index => $course_type)
                            {{ !empty($template['course_details']['course_details']['course_type']) ? $template['course_details']['course_details']['course_type'] == $r_index ? $course_type : '' : ''}}
                        @endforeach
                    </strong></h5>

                    <h5>Course Name : <strong>{{isset($template['course_details']['course_details']['course_name']) ? $template['course_details']['course_details']['course_name'] : '-'}}</strong></h5>

                    <h5>Start Date : <strong>{{isset($template['start_date']) ? date('d-m-Y',strtotime($template['start_date'])) : '-'}}</strong></h5>
                    
                    @if(isset($template['order_details'][0]['status']) && $template['order_details'][0]['status'] == '6')
                        <h5>Payment Made : <strong> Rs. {{isset($template['order_details'][0]['order_payments'][0]['amount']) ? $template['order_details'][0]['order_payments'][0]['amount'] : '-'}}</strong></h5>

                        <h5>Pending Payment : <strong> Rs. {{ isset($template['order_details'][0]['total']) && isset($template['order_details'][0]['order_payments'][0]['amount']) ? $template['order_details'][0]['total'] - $template['order_details'][0]['order_payments'][0]['amount'] : '-'}}</strong></h5>
                    @endif

                    @if(isset($template['order_details'][0]['status']) && $template['order_details'][0]['status'] == '1')
                        <h5>Payment Made : <strong> Rs. {{isset($template['order_details'][0]['order_payments'][0]['amount']) ? $template['order_details'][0]['order_payments'][0]['amount'] : '-'}}</strong></h5>

                        @if(isset($template['order_details'][0]['order_payments'][1]['amount']) && $template['order_details'][0]['order_payments'][1]['amount'])
                            <h5>Total Payment Made : <strong> Rs. {{$template['order_details'][0]['order_payments'][0]['amount'] + $template['order_details'][0]['order_payments'][1]['amount']}}</strong></h5>
                        @endif
                    @endif
                </td>
            </tr>
         </tbody>
    </table>
@stop