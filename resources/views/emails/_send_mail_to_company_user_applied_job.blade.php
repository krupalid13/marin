<html>
<head>
	<title></title>
	<style>
		p{
			margin: 0
		}
.container {
    margin: auto 40px;
    padding-bottom: 8px;
}
.row {
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -ms-flex-wrap: wrap;
    flex-wrap: wrap;
    margin-right: -15px;
    margin-left: -15px;
}
.text-center{
	text-align: center;
}
@media (max-width: 991px){
.row{
	padding: 20px;
}
}
@media (min-width: 992px){
.col-lg-4 {
    -webkit-box-flex: 0;
    -ms-flex: 0 0 33.333333%;
    flex: 0 0 33.333333%;
    max-width: 33.333333%;
}

.col-lg-8 {
    -webkit-box-flex: 0;
    -ms-flex: 0 0 66.666667%;
    flex: 0 0 66.666667%;
    max-width: 66.666667%;
}
.columns{
	columns: 2;
}
.width-40{
	width: 40%;
}
}
</style>
</head>
<body>
@php
	$user=\App\User::where('id',$data['id'])->first();
	$c_rank=$user->professional_detail()->first()->current_rank;

	$job = \App\CourseJob::with(['company'])->where('id',$data['course_job_id'])->first();
@endphp
<div style="background-color: #f5f7f8;width: 100%;padding: 0">

	<p style="background-color: #00008B;color:#fff;padding:10px 0;font-size:30px;margin-left:30px !importanant">{{$user['first_name'].' '.$user['last_name']}} has Applied for an {{$job->title}}</p>
<div style="background-color: #f5f7f8">

	<div class="container">
		<a href="#"><img src="{{asset('/public/assets/images/coursealogo.png')}}" alt="logo" border="0"></a>
	<div style=" background-color: #fff;-webkit-box-shadow: 0px 0px 17px -5px rgba(0,0,0,0.69);
-moz-box-shadow: 0px 0px 17px -5px rgba(0,0,0,0.69);
box-shadow: 0px 0px 17px -5px rgba(0,0,0,0.69);border: solid 2px #eee
" class="">
		<div class="row">
			<div class="col-lg-4 text-center" style="">
				<div style="border-right: solid 1px #ccc;margin: 20px 0">
					<div style="padding: 20px;">
					<a href="#"><img style="max-height: 150px;width: auto;border-radius: 80px;" src="{{asset(env('SEAFARER_PROFILE_PATH').$user->id.DIRECTORY_SEPARATOR.$user->profile_pic)}}" alt="user-png-icon-male-user-icon-512" border="0"></a>
				     <a style="display: block;
				    background-color: #c71c14;
				    width: fit-content;
				    margin: auto;
				    padding: 5px 30px;
				    border-radius: 8px;
				    color: #fff;
				    line-height: 1;
				    text-decoration: none;
				    margin-top: 20px" href="{{route('share-pdf')}}?resume={{$data['share_encrypted_id']}}">View <br> Resume</a> 
					</div>
				</div>
			</div>
			<div class="col-lg-8">
				<div style="padding: 18px ">
				<ul style="list-style: none;padding: 0;line-height: 25px;">
					<li><b>Seafarer Name: </b> {{$user['first_name'].' '.$user['last_name']}}</li>
					<li>
						<b>Current Rank: </b>  
						@foreach(\CommonHelper::new_rank() as $index => $category)
							@foreach($category as $r_index => $rank)
								{{ isset($c_rank) ? $c_rank == $r_index ? $rank : '' : ''}}
							@endforeach
						@endforeach
					</li>
					<li><b>Grade: </b> JKJKJ</li>
					<li><b>Nationality: </b> India</li>
				</ul>
                                    <p style="padding-left: 15px; color: #c71c14;" class="danger"><b>This link is valid for 7 days Only.</b></p>
			</div>	
			</div>
		</div>
	</div>
	
	
	<div style="background-color: #fedc45; padding: 5px 20px; margin-bottom: 8px; margin-top: 30px">
		<p class="text-center">MORE ABOUT course4sea</p>
		<ul class="columns" style="line-height: 35px;">
			<li>Create your profile and upload your documents.</li>
			<li>View profiles of Maritime Institutes and Shipping companies.</li>
			<li>Get Job notifications</li>
			<li>Share your profile / resume / documents.</li>
			<li>Find courses at the lowest price.</li>
			<li>Get alerts about your expiring documents.</li>
		</ul>
		<a style="display: block;
				    background-color: #00a63f;
				    width: fit-content;
				    margin: auto;
				    padding: 10px 20px;
				    color: #fff;
				    line-height: 1;
				    text-decoration: none;
				    margin-top: 20px;
				    text-align: center;" href="#">visit <br> course4sea.com</a>
	</div>
	</div>
</div>
</div>
</body>
</html>