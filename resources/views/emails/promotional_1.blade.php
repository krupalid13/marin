<!DOCTYPE html>
<html lang="en" xmlns:o="urn:schemas-microsoft-com:office:office">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width,initial-scale=1">
        <meta name="x-apple-disable-message-reformatting">
        <title></title>
        <!--[if mso]>
  <style>
    table {border-collapse:collapse;border-spacing:0;border:none;margin:0;}
    div, td {padding:0;}
    div {margin:0 !important;}
        </style>
  <noscript>
    <xml>
      <o:OfficeDocumentSettings>
        <o:PixelsPerInch>96</o:PixelsPerInch>
      </o:OfficeDocumentSettings>
    </xml>
  </noscript>
  <![endif]-->
        <style>
            table, td, div, h1, p {
                font-family: Arial, sans-serif;
            }
            @media screen and (max-width: 530px) {
                .unsub {
                    display: block;
                    padding: 8px;
                    margin-top: 14px;
                    border-radius: 6px;
                    background-color: #555555;
                    text-decoration: none !important;
                    font-weight: bold;
                }
                .col-lge {
                    max-width: 100% !important;
                }
            }
            @media screen and (min-width: 531px) {
                .col-sml {
                    max-width: 27% !important;
                }
                .col-lge {
                    max-width: 73% !important;
                }
            }
            @media (min-width: 992px){
                .columns{
                    columns: 2;
                }
            }
        </style>
    </head>
    <body style="margin:0;padding:0;word-spacing:normal;background-color:#939297;">
        <div role="article" aria-roledescription="email" lang="en" style="text-size-adjust:100%;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;background-color:#f5f7f8;">
            <table role="presentation" style="width:100%;border:none;border-spacing:0;">
                <tr>
                    <td align="center" style="padding:0;">
                        <!--[if mso]>
                        <table role="presentation" align="center" style="width:600px;">
                        <tr>
                        <td>
                        <![endif]-->
                        <table role="presentation" style="width:94%;max-width:600px;border:none;border-spacing:0;text-align:left;font-family:Arial,sans-serif;font-size:16px;line-height:22px;color:#363636;">
                            <tr>
                                <td style="padding:40px 30px 0px 30px;text-align:center;font-size:24px;font-weight:bold;">
                                    <a href="{{asset(route('home'))}}" style="text-decoration:none;"><img src="https://flanknot.com/public/assets/images/logo-grey-bg.png" width="165" alt="Flanknot" style="width:80%;max-width:165px;height:auto;border:none;text-decoration:none;color:#ffffff;"></a>
                                    <h1 style="margin-top:0;margin-bottom:16px;font-size:16px;line-height:32px;font-weight:bold;letter-spacing:-0.02em;text-align:center;">Maritime Community</h1>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding:30px;background-color:#ffffff;">

                                    <p style="margin:0;">At Flanknot get <span style="color:#04225d;font-weight:bold;">Free Amazon Cloud Storage & Securities</span> to upload and organize all your Documents and Certificates.</p>
                                    <p style="color:#04225d;font-weight:bold;">Easy to Access & Easy to Share</p>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding:0;font-size:24px;line-height:28px;font-weight:bold;">
                                    <a href="{{asset(route('home'))}}" style="text-decoration:none;"><img src="https://flanknot.com/public/assets/images/email_upload_document.png" width="600" alt="" style="width:100%;height:auto;display:block;border:none;text-decoration:none;color:#363636;"></a>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding:35px 30px 11px 30px;font-size:0;background-color:#ffffff;border-bottom:1px solid #f0f0f5;border-color:rgba(201,201,207,.35);">
                                    <!--[if mso]>
                                    <table role="presentation" width="100%">
                                    <tr>
                                    <td style="width:145px;" align="left" valign="top">
                                    <![endif]-->
                                    <div class="col-sml" style="display:inline-block;width:100%;max-width:145px;vertical-align:top;text-align:left;font-family:Arial,sans-serif;font-size:14px;color:#363636;">
                                        <img src="https://flanknot.com/public/assets/images/user-profile-round.png" width="115" alt="" style="width:80%;max-width:115px;margin-bottom:20px;">
                                    </div>
                                    <!--[if mso]>
                                    </td>
                                    <td style="width:395px;padding-bottom:20px;" valign="top">
                                    <![endif]-->
                                    <div class="col-lge" style="display:inline-block;width:100%;max-width:395px;vertical-align:top;padding-bottom:20px;font-family:Arial,sans-serif;font-size:16px;line-height:22px;color:#363636;">
                                        <p style="margin-top:0;margin-bottom:12px;color:#04225d;font-weight:bold;">Informative Profile</p>
                                        <p style="margin-top:0;margin-bottom:18px;">A seafarer specific profile that can display all your professional  details in a systematic manner.</p>
                                        <p style="margin:0;"><a href="{{asset(route('demo-share-profile'))}}?token={{$data['token']}}" style="background: #ff3884; text-decoration: none; padding: 10px 25px; color: #ffffff; border-radius: 4px; display:inline-block; mso-padding-alt:0;text-underline-color:#ff3884"><!--[if mso]><i style="letter-spacing: 25px;mso-font-width:-100%;mso-text-raise:20pt">&nbsp;</i><![endif]--><span style="mso-text-raise:10pt;font-weight:bold;">View Profile</span><!--[if mso]><i style="letter-spacing: 25px;mso-font-width:-100%">&nbsp;</i><![endif]--></a></p>
                                    </div>
                                    <!--[if mso]>
                                    </td>
                                    </tr>
                                    </table>
                                    <![endif]-->
                                </td>
                            </tr>
                            <tr>
                                <td style="padding:30px;font-size:24px;line-height:28px;font-weight:bold;background-color:#ffffff;border-bottom:1px solid #f0f0f5;border-color:rgba(201,201,207,.35);">
                                    <a href="{{asset(route('home'))}}" style="text-decoration:none;"><img src="https://flanknot.com/public/assets/images/you_are_hired.png" width="540" alt="" style="width:100%;height:auto;border:none;text-decoration:none;color:#363636;"></a>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding:30px;background-color:#ffffff;border-bottom:1px solid #f0f0f5;border-color:rgba(201,201,207,.35);">
                                    <p style="margin:0;color:#04225d;font-weight:bold;">Dynamic Resume</p>
                                    <p style="">Use Flanknot to Share your Resume with companies. Be notified when your Resume was viewed or downloaded. </p>
                                    <p style="">Get<span style="color:#04225d;font-weight:bold;"> Quick Response</span> from companies.</p>
                                    <p>
                                        <a href="{{asset(route('register'))}}" style="background: #ff3884; text-decoration: none; padding: 10px 25px; color: #ffffff; border-radius: 4px; display:inline-block; mso-padding-alt:0;text-underline-color:#ff3884"><!--[if mso]><i style="letter-spacing: 25px;mso-font-width:-100%;mso-text-raise:20pt">&nbsp;</i><![endif]--><span style="mso-text-raise:10pt;font-weight:bold;">Register with Flanknot</span><!--[if mso]><i style="letter-spacing: 25px;mso-font-width:-100%">&nbsp;</i><![endif]--></a>
                                        <a href="{{asset(route('site.how-to'))}}" style="background: #ff3884; text-decoration: none; padding: 10px 25px; color: #ffffff; border-radius: 4px; display:inline-block; mso-padding-alt:0;text-underline-color:#ff3884"><!--[if mso]><i style="letter-spacing: 25px;mso-font-width:-100%;mso-text-raise:20pt">&nbsp;</i><![endif]--><span style="mso-text-raise:10pt;font-weight:bold;">How to ?</span><!--[if mso]><i style="letter-spacing: 25px;mso-font-width:-100%">&nbsp;</i><![endif]--></a>
                                    </p>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding:0px 30px 0px 30px;background-color:#ffffff;">
                                    <p class="text-center" style="font-size:20px;color:#042059;font-weight:bold">Whats on Flanknot?</p>
                                    <ul class="columns" style="line-height: 35px;padding: 0;line-height: 25px;">
                                        <li>Informative Profiles</li>
                                        <li>Quick Response</li>
                                        <li>Interactive Community</li>
                                        <li>Dynamic Resume</li>
                                        <li>Job Notifications</li>
                                        <li>Analytical Data</li>
                                        <li>Documents Organised</li>
                                        <li>Course Alert</li>
                                        <li>Assessment & Reviews</li>
                                        <li>Free Cloud Storage</li>
                                        <li>Cheapest Course Price</li>
                                        <li>Circular Notifications</li>
                                    </ul>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding:0;background-color:#ffffff; text-align: center;">
                                    <p>
                                        <a href="{{asset(route('home'))}}" style="background: #ff3884; text-decoration: none; padding: 10px 25px; color: #ffffff; border-radius: 4px; display:inline-block; mso-padding-alt:0;text-underline-color:#ff3884"><!--[if mso]><i style="letter-spacing: 25px;mso-font-width:-100%;mso-text-raise:20pt">&nbsp;</i><![endif]--><span style="mso-text-raise:10pt;font-weight:bold;">visit flanknot.com</span><!--[if mso]><i style="letter-spacing: 25px;mso-font-width:-100%">&nbsp;</i><![endif]-->
                                        </a>
                                    </p>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding:30px;text-align:center;font-size:12px;background-color:#404040;color:#cccccc;">
                                    <p style="margin:0;font-size:14px;line-height:20px;">
                                        <a class="unsub" href="{{asset(route('home'))}}" style="color:#cccccc;text-decoration:none;">
                                            Flanknot | Copyright {{ \Carbon\Carbon::now()->format('Y')}}
                                        </a>
                                    </p>
                                </td>
                            </tr>
                        </table>
                        <!--[if mso]>
                        </td>
                        </tr>
                        </table>
                        <![endif]-->
                    </td>
                </tr>
            </table>
        </div>
    </body>
</html>