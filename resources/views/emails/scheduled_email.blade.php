@extends('emails.default_layout')

@section('email_content')
	<table class="" width="100%" cellspacing="0" cellpadding="0" style="max-width:600px;"> 
        <tbody>
            <tr> 
                <td align="left" valign="top" class="" style="color:#2c2c2c;display:block;line-height:20px;font-weight:300;margin:0 auto;clear:both;background-color:#fff;padding:20px" bgcolor="#F9F9F9"> 
                    <p style="padding:0;margin:0;font-size:16px;font-weight:bold;font-size:13px"> 
                        Hi @{{user_name}}, 
                    </p>
                    <br> 
                    <p style="padding:0;margin:0;color:#565656;font-size:13px"> @{{message}}</p>
                </td> 
            </tr>
         </tbody>
    </table>
@stop