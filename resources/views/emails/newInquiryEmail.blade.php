@extends('emails.default_layout')

@section('email_content')
<table class="" width="100%" cellspacing="0" cellpadding="0" style="max-width:600px;"> 
    <tbody>
        <tr> 
            <td align="left" valign="top" class="" style="color:#2c2c2c;display:block;line-height:20px;font-weight:300;margin:0 auto;clear:both;background-color:#fff;padding:20px" bgcolor="#F9F9F9"> 
                <p style="padding:0;margin:0;font-size:16px;font-weight:bold;font-size:13px"> 
                    Hi Admin, 
                </p>
                <br> 
                <p style="padding:0;margin:0;color:#565656;font-size:13px"> You have new inquiry.</p>
                <p style="padding-top:5px;margin:0;color:#565656;font-size:13px"> Name : {name} <br>
                 {type} name : {institute_name} <br>
                 Website : {website} <br>
                 Email : {email} <br>
                 Number : {number}</p> 
            </td> 
        </tr>
     </tbody>
</table>

@stop