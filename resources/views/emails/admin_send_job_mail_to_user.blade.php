<html>
<head>
	<title></title>
        <link rel="shortcut icon" type="image/x-icon" href="{{asset('public/images/favicon.ico')}}">
	<style>
		p{
			margin: 0
		}
.container {
    margin: auto 40px;
    padding-bottom: 8px;
}
.row {
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -ms-flex-wrap: wrap;
    flex-wrap: wrap;
    margin-right: -15px;
    margin-left: -15px;
}
.text-center{
	text-align: center;
}
@media (max-width: 991px){
.row{
	padding: 20px;
}
}
@media (min-width: 992px){
.col-lg-4 {
    -webkit-box-flex: 0;
    -ms-flex: 0 0 33.333333%;
    flex: 0 0 33.333333%;
    max-width: 33.333333%;
}

.col-lg-8 {
    -webkit-box-flex: 0;
    -ms-flex: 0 0 66.666667%;
    flex: 0 0 66.666667%;
    max-width: 66.666667%;
}
.columns{
	columns: 2;
}
.width-40{
	width: 40%;
}
}
</style>
</head>
<body>
@php
	$user=\App\User::where('id',$data['id'])->first();
	$job = \App\CourseJob::with(['company'])->where('id',$data['course_job_id'])->first();

@endphp
<div style="background-color: #f5f7f8;width: 100%;padding: 0">

	<p style="background-color: #00a63f;text-align: center;color: #fff;padding:  10px 0">{{$data['subject']}}</p>
	<h4 style="margin-left:50px">HI {{$user['first_name'].' '.$user['last_name']}}</h4>
<div style="background-color: #f5f7f8">

	<div class="container">
		<a href="#"><img src="{{asset('/public/assets/images/coursealogo.png')}}" alt="logo" border="0"></a>
	<div style=" background-color: #fff;-webkit-box-shadow: 0px 0px 17px -5px rgba(0,0,0,0.69);
-moz-box-shadow: 0px 0px 17px -5px rgba(0,0,0,0.69);
box-shadow: 0px 0px 17px -5px rgba(0,0,0,0.69);border: solid 2px #eee
" class="">
		<div class="row">
			<div class="col-lg-8" style="padding-left:40px">
				<div style="padding: 18px ">
				<ul style="list-style: none;padding: 0;line-height: 25px;">
					<h3>We Have Found a New job Opening that suits your profile.</h3>
					<li><b>Company Name: </b> {{$job->company->name}}</li>
					<li><b>Title: </b> {{$job->title}}</li>
				</ul>
				<a href="javascript:void(0)" style="background-color:green;padding:12px;text-decoration:none;color:white;margin-left:150px">View Job Dettail</a>
			</div>	
			</div>
		</div>
	</div>
	
	
	<div style="background-color: #fedc45; padding: 5px 20px; margin-bottom: 8px; margin-top: 30px">
		<p class="text-center">MORE ABOUT course4sea</p>
		<ul class="columns" style="line-height: 35px;">
			<li>Create your profile and upload your documents.</li>
			<li>View profiles of Maritime Institutes and Shipping companies.</li>
			<li>Get Job notifications</li>
			<li>Share your profile / resume / documents.</li>
			<li>Find courses at the lowest price.</li>
			<li>Get alerts about your expiring documents.</li>
		</ul>
		<a style="display: block;
				    background-color: #00a63f;
				    width: fit-content;
				    margin: auto;
				    padding: 10px 20px;
				    color: #fff;
				    line-height: 1;
				    text-decoration: none;
				    margin-top: 20px;
				    text-align: center;" href="#">visit <br> course4sea.com</a>
	</div>
	</div>
</div>
</div>
</body>
</html>