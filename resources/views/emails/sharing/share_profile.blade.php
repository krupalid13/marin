<!DOCTYPE html>
<html>
    <head>
        <title></title>
        <style>
            p{
                margin: 0
            }
            .container {
                margin: auto 40px;
                padding-bottom: 8px;
            }
            .row {
                display: -webkit-box;
                display: -ms-flexbox;
                display: flex;
                margin-right: -15px;
                margin-left: -15px;
                justify-content: center;
            }
            .text-center{
                text-align: center;
            }
            .col-lg-6 {

                width: 50%;
                padding: 0 20px;
                /*position: relative;*/
                /*min-height: 1px;*/
                /*padding-right: 15px;*/
                /*padding-left: 15px;*/
            }
            @media (max-width: 991px){
                .row{
                    padding: 20px;
                }
            }
            @media only screen and (max-width:479px) {
                body {
                    width:auto!important
                }
                table {
                    width:100%!important
                }
                td[class=full_width] {
                    width:100%!important
                }
            }
            @media (min-width: 992px){
                .col-lg-4 {
                    -webkit-box-flex: 0;
                    -ms-flex: 0 0 33.333333%;
                    flex: 0 0 33.333333%;
                    max-width: 33.333333%;
                }

                .col-lg-8 {
                    -webkit-box-flex: 0;
                    -ms-flex: 0 0 66.666667%;
                    flex: 0 0 66.666667%;
                    max-width: 66.666667%;
                }

                .columns{
                    columns: 3;
                }
                .width-40{
                    width: 40%;
                }
            }
        </style>
    </head>
    <body>
        @php
        $user=Auth::user();
        $c_rank=$user->professional_detail()->first()->current_rank;
        $profile_image = "";
        $gender = "M";
        if(Auth::check()){
        if(Auth::user()->gender == 'F'){
        $gender = 'F';
        }
        }
        if(isset(Auth::user()->profile_pic) && !empty(Auth::user()->profile_pic)){
        $profile_image = 'https://flanknot-demo.s3.ap-south-1.amazonaws.com/public/images/uploads/seafarer_profile_pic/'.Auth::user()->id.'/'.Auth::user()->profile_pic;
        }else if($gender == 'M'){
        $profile_image = asset('public/assets/images/default-user-male.png');
        }else{
        $profile_image = asset('public/assets/images/defualt-user-female.png');
        }
        @endphp
        <div style="background-color: #f5f7f8;width: 100%;padding: 0">

            <p style="background-color: #00a63f;text-align: center;color: #fff;padding:  10px 0">{{$data['subject']}}</p>
            <div style="background-color: #f5f7f8">

                <div class="container">
                    <a href="{{asset(route('home'))}}" target="_blank">
                        <img src="{{asset('public/assets/images/logo-grey-bg.png')}}" style="outline: none; height:40px; text-decoration: none; -ms-interpolation-mode: bicubic; max-width: 160px; clear: both; display: block;padding: 10px;">
                    </a>
                    <div style=" background-color: #fff;-webkit-box-shadow: 0px 0px 17px -5px rgba(0,0,0,0.69); -moz-box-shadow: 0px 0px 17px -5px rgba(0,0,0,0.69); box-shadow: 0px 0px 17px -5px rgba(0,0,0,0.69);border: solid 2px #eee" class="">
                        <div class="row">

                            <div style="padding: 18px ;text-align: center;width:100%!important;">
                                <ul style="list-style: none;padding: 0;line-height: 25px;">
                                    <li>
                                        <a href="#"><img style="max-height: 150px;width: auto;border-radius: 80px;" src="{{$profile_image}}" alt="user-png-icon-male-user-icon-512" border="0"></a>
                                    </li>
                                    <li style="font-size:18px;"><b> {{$user['first_name'].' '.$user['PPMname'].' '.$user['PPLname']}}</b></li>
                                    <!--						<li>
                                                                                            <b>Current Rank: </b>  
                                                                                            @foreach(\CommonHelper::new_rank() as $index => $category)
                                                                                                    @foreach($category as $r_index => $rank)
                                                                                                            {{ isset($c_rank) ? $c_rank == $r_index ? $rank : '' : ''}}
                                                                                                    @endforeach 
                                                                                            @endforeach
                                                                                    </li>-->
                                    <!--{{-- <li><b>Grade: </b> {{($data['userWkfrDetail']['wk_cop'] && $data['userWkfrDetail']['type'] ? strtoupper($data['userWkfrDetail']['wk_cop']).' '.$data['userWkfrDetail']['type'] : $data['userWkfrDetail']['coc_grade']) }}</li> --}}-->
                                    <!--<li><b>Nationality: </b> {{\CommonHelper::countries()[$user->personal_detail()->first()->country]}}</li>-->
                                    <li>
                                        <a style="display: block;background-color: #c71c14;width: fit-content;margin: auto;padding: 5px 30px;border-radius: 8px;color: #fff;line-height: 1;text-decoration: none;margin-top: 20px" href="{{route('share-profile', ['id' => $user['id']])}}">
                                            View <br> Profile</a>
                                    </li>

                                </ul>
                        <!--<p style="padding-left: 20px; margin-top: 80px"><b>Find attachment of my resume</b></p>-->
                            </div>	
                        </div>
                    </div>

<!--                    <div style="border: solid 2px #063253;text-align: center; padding-bottom: 30px; margin: 30px 4% 0 4%">
                        <p style="text-align: center;margin-top: 9px;margin-bottom: 20px"><b>What's on my profile !</b></p>
                        <div class="row">
                            <div class="col-lg-6">
                                <div style="border: solid 1px #063253; margin: 10px">
                                    <img src="{{asset('public/assets/images/my-resume.png')}}" stype="width: 33%; padding-top: 20px;"/>
                                    <p style="color: #063253; text-align: center;padding-top: 100px;padding-bottom: 10px; border-bottom: solid 2px #063253"> My Resume</p>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div style="border: solid 1px #063253; margin: 10px">
                                    <img src="{{asset('public/assets/images/my-documents.png')}}" stype="width: 33%; padding-top: 20px;"/>
                                    <p style="color: #063253; text-align: center;padding-top: 100px;padding-bottom: 10px; border-bottom: solid 2px #063253">My Documents</p>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div style="border: solid 1px #063253; margin: 10px; width: 95%;">
                                    <img src="{{asset('public/assets/images/skill-infographic.png')}}"style="padding-top: 20px; width: 33%;" />
                                    <p style="color: #063253; text-align: center;padding-top: 100px;padding-bottom: 10px; border-bottom: solid 2px #063253">My Statistics</p>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div style="border: solid 1px #063253; margin: 10px">
                                    <img src="{{asset('public/assets/images/onboard-experience.png')}}" style="width: 33%; padding-top: 20px;" />
                                    <p style="color: #063253; text-align: center;padding-top: 100px;padding-bottom: 10px; border-bottom: solid 2px #063253">My Feedback</p>
                                </div>
                            </div>
                        </div>
                    </div>-->
                    
                    <div style="border: solid 2px #063253;text-align: center; padding-bottom: 30px; margin: 30px 4% 0 4%">
                        <p style="text-align: center;margin-top: 9px;margin-bottom: 20px"><b>What's on my profile !</b></p>
                        <table width="100%">
                            <tr>
                                <td >
                                    <div style="margin: 10px">
                                        <img src="{{asset('public/assets/images/my-resume.png')}}" style="width: 90%; padding-top: 20px;"/>
                                        <p style="color: #063253; text-align: center;padding-top: 5px;padding-bottom: 5px; border-bottom: solid 2px #063253"> Resume</p>
                                    </div>
                                </td>
                                <td>
                                    <div style="margin: 10px">
                                        <img src="{{asset('public/assets/images/my-documents.png')}}" style="width: 90%; padding-top: 20px;"/>
                                        <p style="color: #063253; text-align: center;padding-top: 5px;padding-bottom: 5px; border-bottom: solid 2px #063253">Documents</p>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="full_width" >
                                    <div style="margin: 10px">
                                        <img src="{{asset('public/assets/images/skill-infographic.png')}}" style="width: 90%; padding-top: 20px;" />
                                        <p style="color: #063253; text-align: center;padding-top: 5px;padding-bottom: 5px; border-bottom: solid 2px #063253">Statistics</p>
                                    </div>
                                </td>
                                <td class="full_width" >
                                    <div style="margin: 10px">
                                        <img src="{{asset('public/assets/images/onboard-experience.png')}}" style="width: 90%; padding-top: 20px;" />
                                        <p style="color: #063253; text-align: center;padding-top: 5px;padding-bottom: 5px; border-bottom: solid 2px #063253">Feedback</p>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>


                    <div style="background-color: #fedc45; padding: 5px 10px; margin-bottom: 8px; margin-top: 30px">
                        <p class="text-center" style="font-size:20px;color:#042059;font-weight:bold">Whats on Flanknot?</p>
                        <ul class="columns" style="line-height: 35px;">
                            <li>Informative Profiles</li>
                            <li>Quick Response</li>
                            <li>Interactive Community</li>
                            <li>Dynamic Resume</li>
                            <li>Job Notifiactions</li>
                            <li>Analytical Data</li>
                            <li>Documents Organised</li>
                            <li>Course Alert</li>
                            <li>Assessment & Reviews</li>
                            <li>Free Cloud Storage</li>
                            <li>Cheapest Course Price</li>
                            <li>Circular Notifications</li>
                        </ul>
                        <a style="display: block;
                           background-color: #2e6da4;
                           width: fit-content;
                           margin: auto;
                           padding: 10px 20px;
                           color: #fff;
                           line-height: 1;
                           text-decoration: none;
                           margin-top: 20px;
                           text-align: center;" href="{{asset(route('home'))}}">visit <br> flanknot.com</a>
                    </div>
                    <div style="background-color: #00a63f;text-align: center; padding-top: 10px;padding-bottom: 10px;" align="center">
                        <a href="{{asset(route('home'))}}" style="text-align: center; color: #fff !important; font-size: 12px; letter-spacing: 0.5px; font-weight: 500; text-decoration: none; font-family: Helvetica, Arial, sans-serif; line-height: 19px; margin: 0; padding: 0;" align="center" target="_blank">
                            Flanknot || Copyright {{ \Carbon\Carbon::now()->format('Y')}}
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>