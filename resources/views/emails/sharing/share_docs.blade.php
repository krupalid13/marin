<!DOCTYPE html>
<html>
    <head>
        <title></title>
        <style>
            p{
                margin: 0
            }
            .container {
                margin: auto 40px;
                padding-bottom: 8px;
            }
            .row {
                display: -webkit-box;
                display: -ms-flexbox;
                display: flex;
                -ms-flex-wrap: wrap;
                flex-wrap: wrap;
                margin-right: -15px;
                margin-left: -15px;
            }
            .text-center{
                text-align: center;
            }

            .shared-documents {
                width: 100%;
            }
            .shared-documents li {
                width: 77%;
            }
            @media (max-width: 991px){
                .row{
                    padding: 20px;
                }
            }
            @media (min-width: 992px){
                .col-lg-4 {
                    -webkit-box-flex: 0;
                    -ms-flex: 0 0 33.333333%;
                    flex: 0 0 33.333333%;
                    max-width: 33.333333%;
                }

                .col-lg-8 {
                    -webkit-box-flex: 0;
                    -ms-flex: 0 0 66.666667%;
                    flex: 0 0 66.666667%;
                    max-width: 66.666667%;
                }
                .columns{
                    columns: 3;
                }
                .width-100{
                    width: 100%;
                }

                .bank-details-columns {
                    columns: 2;
                    width: 100%;
                }
            }
        </style>
    </head>
    <body>
        @php
        $user=Auth::user();
        $c_rank=$user->professional_detail()->first()->current_rank;
        $profile_image = "";
        $gender = "M";
        if(Auth::check()){
        if(Auth::user()->gender == 'F'){
        $gender = 'F';
        }
        }
        if(isset(Auth::user()->profile_pic) && !empty(Auth::user()->profile_pic)){
        $profile_image = 'https://flanknot-demo.s3.ap-south-1.amazonaws.com/public/images/uploads/seafarer_profile_pic/'.Auth::user()->id.'/'.Auth::user()->profile_pic;
        }else if($gender == 'M'){
        $profile_image = asset('public/assets/images/default-user-male.png');
        }else{
        $profile_image = asset('public/assets/images/defualt-user-female.png');
        }
        @endphp
        <div style="background-color: #f5f7f8;width: 100%;padding: 0">
            <p style="background-color: #00a63f;text-align: center;color: #fff;padding:  10px 0">{{$data['subject']}}</p>
            <div style="background-color: #f5f7f8">
                <div class="container">
                    <a href="{{asset(route('home'))}}" target="_blank">
                        <img src="{{asset('public/assets/images/logo-grey-bg.png')}}" style="outline: none; height:40px; text-decoration: none; -ms-interpolation-mode: bicubic; max-width: 160px; clear: both; display: block;padding: 10px;">
                    </a>
                    <div style=" background-color: #fff;-webkit-box-shadow: 0px 0px 17px -5px rgba(0,0,0,0.69);
                         -moz-box-shadow: 0px 0px 17px -5px rgba(0,0,0,0.69);
                         box-shadow: 0px 0px 17px -5px rgba(0,0,0,0.69);border: solid 2px #eee
                         " class="">
                        <div class="row">
                            <div style="padding: 18px ;text-align: center;width:100%!important;">
                                <ul style="list-style: none;padding: 0;line-height: 25px;">
                                    <li><a href="#"><img style="height: 150px;width: auto;border-radius: 80px;" src="{{$profile_image}}" alt="user-png-icon-male-user-icon-512" border="0"></a></li>
                                    <li style="font-size:18px;"><b> {{$user['first_name'].' '.$user['PPMname'].' '.$user['PPLname']}}</b></li>
<!--                                    <li>
                                        <b>Current Rank: </b>  
                                        @foreach(\CommonHelper::new_rank() as $index => $category)
                                        @foreach($category as $r_index => $rank)
                                        {{ isset($c_rank) ? $c_rank == $r_index ? $rank : '' : ''}}
                                        @endforeach 
                                        @endforeach
                                    </li>-->
                                    <!-- <li><b>Grade: </b> {{($data['userWkfrDetail']['wk_cop'] && $data['userWkfrDetail']['type'] ? strtoupper($data['userWkfrDetail']['wk_cop']).' '.$data['userWkfrDetail']['type'] : $data['userWkfrDetail']['coc_grade']) }}</li>-->
                                    <!--<li><b>Nationality: </b> {{\CommonHelper::countries()[$user->personal_detail()->first()->country]}}</li>-->
                                    <li>
                                        <a style="display: block;
                                       background-color: #c71c14;
                                       width: fit-content;
                                       margin: auto;
                                       padding: 5px 10px;
                                       border-radius: 8px;
                                       color: #fff;
                                       text-decoration: none;
                                       margin-top: 20px" href="{{route('documents.sharedData', $data['token'])}}">View <br> Document</a>
                                    </li>
<!--                                    <li>
                                        <p style="color: red; margin-top: 10px">For Security Reasons, the link is valid for 48 hrs only.</p>
                                    </li>-->
                                </ul>
                            </div>
                        </div>
                    </div>
                    @php
                    $style= "border: solid 1px #042059; margin-top: 40px;";
                    if(empty($data['bank_details'])){
                    $style= "border: solid 1px #042059; margin-top: 40px;margin-bottom: 40px";
                    }   
                    @endphp
                    <div style="{{$style}}">
                        <p style="background-color: #042059;text-align: center;color: #fff;padding:  10px 0; margin: 0">List of Document Shared</p>
                        <ul class="columns width-100 shared-documents" style="color: #042059;line-height: 22px;">
                            @foreach($data['sharedData'] as $key => $row)
                            @if($row == 'yellow_fever' || $row == 'cholera' || $row == 'hepatitis_b' || $row == 'hepatitis_c' || $row == 'diphtheria')
                            <li>{{"Vaccination - " . ucwords(str_replace("_", " ", $row))}}</li>
                            @elseif($row == 'covid')
                            <li>{{"Vaccination - Covid19"}}</li>
                            @elseif($row == 'screening_test')
                            <li>{{"Medical Screening Test"}}</li>
                            @else 
                            <li>{{ucwords(str_replace("_", " ", $row))}}</li>
                            @endif
                            @endforeach
                        </ul>
                    </div>
                    @if(!empty($data['bank_details']))
                    <div style="border: solid 1px #042059;margin-bottom: 40px">
                        <p style="background-color: #042059;text-align: center;color: #fff;padding:  10px 0; margin: 0">Bank Details</p>
                        <ul class="bank-details-columns width-100" style="color: #042059;line-height: 35px;">
                            <li>Name - {{$data['bank_details']['account_holder_name']}}</li>
                            <li>Account No - {{$data['bank_details']['account_no']}}</li>
                            <li>Branch Address - {{$data['bank_details']['branch_address']}}</li>
                            <li>Bank Name - {{$data['bank_details']['bank_name']}}</li>
                            <li>IFSC Code - {{$data['bank_details']['ifsc_code']}}</li>
                        </ul>
                        <hr>
                        <ul  style="color: #042059;" class="bank-details-columns">
                            <li>Pancard - {{$data['bank_details']['pancard']}} &nbsp;  &nbsp; </li>
                            <li>Aadhar Card - {{$data['bank_details']['aadhaar']}}</li>
                        </ul>
                    </div>
                    @endif

                    <div style="background-color: #fedc45; padding: 5px 10px; margin-bottom: 8px">
                        <p class="text-center" style="font-size:20px;color:#042059;font-weight:bold">Whats on Flanknot?</p>
                        <ul class="columns" style="line-height: 35px;">
                            <li>Informative Profiles</li>
                            <li>Quick Response</li>
                            <li>Interactive Community</li>
                            <li>Dynamic Resume</li>
                            <li>Job Notifiactions</li>
                            <li>Analytical Data</li>
                            <li>Documents Organised</li>
                            <li>Course Alert</li>
                            <li>Assessment & Reviews</li>
                            <li>Free Cloud Storage</li>
                            <li>Cheapest Course Price</li>
                            <li>Circular Notifications</li>
                        </ul>
                        <a style="display: block;
                           background-color: #2e6da4;
                           width: fit-content;
                           margin: auto;
                           padding: 10px 20px;
                           color: #fff;
                           line-height: 1;
                           text-decoration: none;
                           margin-top: 20px;
                           text-align: center;" href="{{asset(route('home'))}}">visit <br> flanknot.com</a>
                    </div>
                    <div style="background-color: #00a63f;text-align: center; padding-top: 10px;padding-bottom: 10px;" align="center">
                        <a href="{{asset(route('home'))}}" style="text-align: center; color: #fff !important; font-size: 12px; letter-spacing: 0.5px; font-weight: 500; text-decoration: none; font-family: Helvetica, Arial, sans-serif; line-height: 19px; margin: 0; padding: 0;" align="center" target="_blank">
                            Flanknot || Copyright {{ \Carbon\Carbon::now()->format('Y')}}
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
