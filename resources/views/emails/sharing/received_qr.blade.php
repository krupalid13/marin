<html>
    <head>
        <title></title>
        <style>
            p{
                margin: 0
            }
            .container {
                margin: auto 40px;
                padding-bottom: 8px;
            }
            .row {
                display: -webkit-box;
                display: -ms-flexbox;
                display: flex;
                -ms-flex-wrap: wrap;
                flex-wrap: wrap;
                margin-right: -15px;
                margin-left: -15px;
            }
            .text-center{
                text-align: center;
            }
            @media (max-width: 991px){
                .row{
                    padding: 20px;
                }
            }
            @media (min-width: 992px){
                .col-lg-4 {
                    -webkit-box-flex: 0;
                    -ms-flex: 0 0 33.333333%;
                    flex: 0 0 33.333333%;
                    max-width: 33.333333%;
                }

                .col-lg-8 {
                    -webkit-box-flex: 0;
                    -ms-flex: 0 0 66.666667%;
                    flex: 0 0 66.666667%;
                    max-width: 66.666667%;
                }
                .columns{
                    columns: 3;
                }
                .width-40{
                    width: 40%;
                }
            }
        </style>
    </head>
    <body>
        <div style="background-color: #f5f7f8;width: 100%;padding: 0">

            <p style="background-color: #00a63f;text-align: center;color: #fff;padding:  10px 0">{{$data['subject']}}</p>
            <div style="background-color: #f5f7f8">

                <div class="container">
                    <a href="{{asset(route('home'))}}" target="_blank">
                        <img src="{{asset('public/assets/images/logo-grey-bg.png')}}" style="outline: none; height:40px; text-decoration: none; -ms-interpolation-mode: bicubic; max-width: 160px; clear: both; display: block;padding: 10px;">
                    </a>

                    <div style=" background-color: #fff;-webkit-box-shadow: 0px 0px 17px -5px rgba(0,0,0,0.69);
                         -moz-box-shadow: 0px 0px 17px -5px rgba(0,0,0,0.69);
                         box-shadow: 0px 0px 17px -5px rgba(0,0,0,0.69);border: solid 2px #eee
                         " class="">
                        @php
                        $user=$data['user'];
                        $profile_image = "";
                        $gender = "M";
                        if($user->gender == 'F'){
                        $gender = 'F';
                        }
                        if(isset($user->profile_pic) && !empty($user->profile_pic)){
                        $profile_image = 'https://flanknot-demo.s3.ap-south-1.amazonaws.com/public/images/uploads/seafarer_profile_pic/'.$user->id.'/'.$user->profile_pic;
                        }else if($gender == 'M'){
                        $profile_image = asset('public/assets/images/default-user-male.png');
                        }else{
                        $profile_image = asset('public/assets/images/defualt-user-female.png');
                        }
                        @endphp
                        <div class="row">
                            <div style="padding: 18px ;text-align: center;width:100%!important;">
                                <ul style="list-style: none;padding: 0;line-height: 25px;">
                                    <li>
                                        <small style="font-size: 12px;">The sender has checked out your resume. To view your share history, click on My Share History <br>
                                            Do not forward this email. It contains links which allow direct login to your Flanknot.com account.</small>
                                        <hr>
                                        <br>
                                        <br>
                                        <br>
                                    </li>
                                    <li style="font-size:18px;"><b>Quick Response </b></li>
                                    <li style="font-size:16px;">"{{$data['qr_resp']}}"</li>
                                    <li>
                                        <a style="display: block;
                                           background-color: #c71c14;
                                           width: fit-content;
                                           margin: auto;
                                           padding: 5px 30px;
                                           border-radius: 8px;
                                           color: #fff;
                                           line-height: 1;
                                           text-decoration: none;
                                           margin-top: 20px" href="{{URL::to($data['qr_link'])}}">My Share History</a> 
                                    </li>
                                </ul>
                                <!--<p style="padding-left: 15px; color: #c71c14;" class="danger"><b>This link is valid for 90 days Only.</b></p>-->
                            </div>	
                        </div>
                    </div>


                    <div style="background-color: #fedc45; padding: 5px 10px; margin-bottom: 8px; margin-top: 30px">
                        <p class="text-center" style="font-size:20px;color:#042059;font-weight:bold">Whats on Flanknot?</p>
                        <ul class="columns" style="line-height: 35px;">
                            <li>Informative Profiles</li>
                            <li>Quick Response</li>
                            <li>Interactive Community</li>
                            <li>Dynamic Resume</li>
                            <li>Job Notifiactions</li>
                            <li>Analytical Data</li>
                            <li>Documents Organised</li>
                            <li>Course Alert</li>
                            <li>Assessment & Reviews</li>
                            <li>Free Cloud Storage</li>
                            <li>Cheapest Course Price</li>
                            <li>Circular Notifications</li>
                        </ul>
                        <a style="display: block;
                           background-color: #2e6da4;
                           width: fit-content;
                           margin: auto;
                           padding: 10px 20px;
                           color: #fff;
                           line-height: 1;
                           text-decoration: none;
                           margin-top: 20px;
                           text-align: center;" href="{{asset(route('home'))}}">visit <br> flanknot.com</a>
                    </div>
                    <div style="background-color: #00a63f;text-align: center; padding-top: 10px;padding-bottom: 10px;" align="center">
                        <a href="{{asset(route('home'))}}" style="text-align: center; color: #fff !important; font-size: 12px; letter-spacing: 0.5px; font-weight: 500; text-decoration: none; font-family: Helvetica, Arial, sans-serif; line-height: 19px; margin: 0; padding: 0;" align="center" target="_blank">
                            Flanknot || Copyright {{ \Carbon\Carbon::now()->format('Y')}}
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>