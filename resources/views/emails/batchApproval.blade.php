@extends('emails.default_layout')

@section('email_content')
	<table class="" width="100%" cellspacing="0" cellpadding="0" style="max-width:600px;"> 
        <tbody>
            <tr> 
                <td align="left" valign="top" class="" style="color:#2c2c2c;display:block;line-height:20px;font-weight:300;margin:0 auto;clear:both;background-color:#fff;padding:20px" bgcolor="#F9F9F9"> 
                    <p style="padding:0;margin:0;font-size:16px;font-weight:bold;font-size:13px"> 
                        Hi {{isset($template['user']['first_name']) ? ucfirst($template['user']['first_name']) : ''}},
                    </p>
                    
                    @if(isset($template['status']) && $template['status'] == '4')
                        <p style="padding:0;margin:0;color:#565656;font-size:13px"> Institute has approved your booking.</p>
                    @else
                        <p style="padding:0;margin:0;color:#565656;font-size:13px"> Institute has denied your booking. 
                        @if(isset($template['reason']) && !empty($template['reason']))
                            Reason for batch deniel is {{$template['reason']}}.
                        @endif
                    @endif

                    <br> 
                    <p style="padding:0;margin:0;color:#565656;font-size:13px"><b>Batch details are as follows:-</b></p>

                    <h5>Institute Name : <strong>{{isset($template['batch_details']['course_details']['institute_registration_detail']['institute_name']) ? $template['batch_details']['course_details']['institute_registration_detail']['institute_name'] : ''}}</strong></h5>
                    <h5>Course Type : <strong>
                        @foreach(\CommonHelper::institute_course_types() as $r_index => $course_type)
                            {{ !empty($template['batch_details']['course_details']['course_details']['course_type']) ? $template['batch_details']['course_details']['course_details']['course_type'] == $r_index ? $course_type : '' : ''}}
                        @endforeach
                    </strong></h5>
                    <h5>Course Name : <strong>{{isset($template['batch_details']['course_details']['course_details']['course_name']) ? $template['batch_details']['course_details']['course_details']['course_name'] : '-'}}</strong></h5>
                    <h5>Batch Cost : <strong>Rs {{isset($template['subscription_cost']) ? $template['subscription_cost'] : ''}}</strong></h5>
                    <h5>Tax : <strong>Rs {{isset($template['tax_amount']) ? $template['tax_amount'] : ''}}</strong></h5>
                    <h5>Total : <strong>Rs {{isset($template['total']) ? $template['total'] : ''}}</strong></h5>

                    @if($template['status'] == '4')
                    <p style="display: flex;align-items: center;">
                        <h5 style="margin-bottom: 15px;">
                            To make payment please click on below button.
                        </h5>

                        <a style="border-radius: 4px;margin: 20px 20px 20px 0px;background-color: #c71c14 !important;color: #fff !important;border: none;text-decoration: none;line-height: normal;padding: 8px 15px !important;
                            font-size: 16px !important;" target="_blank" href="{{$template['link']}}">Make Payment</a>
                    </p>
                    @endif
                </td>
            </tr>
         </tbody>
    </table>
@stop