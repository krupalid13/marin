@extends('emails.default_layout')

@section('email_content')
	<table class="" width="100%" cellspacing="0" cellpadding="0" style="max-width:600px;"> 
        <tbody>
            <tr> 
                <td align="left" valign="top" class="" style="color:#2c2c2c;display:block;line-height:20px;font-weight:300;margin:0 auto;clear:both;background-color:#fff;padding:20px" bgcolor="#F9F9F9"> 
                    <p style="padding:0;margin:0;font-size:16px;font-weight:bold;font-size:13px"> 
                        Hi {{isset($template['course_details']['institute_registration_detail']['user']['first_name']) ? $template['course_details']['institute_registration_detail']['user']['first_name'] : ''}},
                    </p>
                    <br> 

                    <p style="padding:0;margin:0;color:#565656;font-size:13px"> Seafarer has cancelled the order of your batch.</p>

                    <p style="padding:0;margin:0;color:#565656;font-size:13px"> Seafarer details are as follows:-</p>
                    <h5>Name : <strong>{{isset($template['seafarer'][0]['first_name']) ? $template['seafarer'][0]['first_name'] : '-'}}</strong></h5>
                    <h5>Email : <strong>{{isset($template['seafarer'][0]['email']) ? $template['seafarer'][0]['email'] : '-'}}</strong></h5>
                    <h5>Mobile : <strong>{{isset($template['seafarer'][0]['mobile']) ? $template['seafarer'][0]['mobile'] : '-'}}</strong></h5>

                    <br> 
                    <p style="padding:0;margin:0;color:#565656;font-size:13px"> Batch details are as follows:-</p>
                    <h5>Course Type : <strong>
                        @foreach(\CommonHelper::institute_course_types() as $r_index => $course_type)
                            {{ !empty($template['course_details']['course_details']['course_type']) ? $template['course_details']['course_details']['course_type'] == $r_index ? $course_type : '' : ''}}
                        @endforeach
                    </strong></h5>
                    <h5>Course Name : <strong>{{isset($template['course_details']['course_details']['course_name']) ? $template['course_details']['course_details']['course_name'] : '-'}}</strong></h5>
                    <h5>Batch Cost : <strong>Rs {{isset($template['cost']) ? $template['cost'] : '-'}}</strong></h5>
                    <h5>Start Date : <strong>{{isset($template['start_date']) ? date('d-m-Y',strtotime($template['start_date'])) : '-'}}</strong></h5>
                    
                </td>
            </tr>
         </tbody>
    </table>
@stop