@extends('emails.default_layout')

@section('email_content')
	<table class="" width="100%" cellspacing="0" cellpadding="0" style="max-width:600px;"> 
        <tbody>
            <tr> 
                <td align="left" valign="top" class="" style="color:#2c2c2c;display:block;line-height:20px;font-weight:300;margin:0 auto;clear:both;background-color:#fff;padding:20px" bgcolor="#F9F9F9"> 
                    <p style="padding:0;margin:0;font-size:16px;font-weight:bold;font-size:13px"> 
                        Hi {{isset($template['first_name']) ? $template['first_name'] : ''}}, 
                    </p>
                    <br> 
                    <p style="padding:0;margin:0;color:#565656;font-size:13px"> Greeting from Consultanseas! Thank you for registering with us.</p>
                    <p style="padding-top:5px;margin:0;color:#565656;font-size:13px"> To view your profile please click on below button.</p> <br>
                    <a class="btn" style="border-radius: 4px;background-color: #c71c14 !important;color: #fff !important;border: 1px solid #c71c14 !important;text-decoration: none;line-height: normal;padding: 8px 20px !important;
                        font-size: 16px !important;" href="{{isset($template['profile']) ? $template['profile'] : ''}}" target="_blank">
                        View Profile
                    </a>
                </td> 
            </tr>
         </tbody>
    </table>
@stop