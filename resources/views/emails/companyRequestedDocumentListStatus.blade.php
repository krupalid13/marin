@extends('emails.default_layout')

@section('email_content')
    <table class="" width="100%" cellspacing="0" cellpadding="0" style="max-width:600px;">
        <tbody>
        <tr>
            <td align="left" valign="top" class="" style="color:#2c2c2c;display:block;line-height:20px;font-weight:300;margin:0 auto;clear:both;background-color:#fff;padding:20px" bgcolor="#F9F9F9">
                <p style="padding:0;margin:0;font-size:16px;font-weight:bold;font-size:13px">
                    Hi {{isset($template['first_name']) ? ucwords($template['first_name']) : ''}},
                </p>

                <br>

                <h3>Status of requested documents.</h3>
                <h5>Seafarer Name : <strong>{{ucwords($template['first_name'])}}</strong></h5>
                <h5>Seafarer Email Id : <strong>{{$template['seafarer_email_id']}}</strong></h5>
                <h5>Document Types : <strong>{{strtoupper($template['type'])}}</strong></h5>

                <br>

                <p style="padding:0;margin:0;color:#565656;font-size:13px">
                    {{$template['message']}}
                </p>

                @if($template['status'] == '1')
                    <p style="display: flex;align-items: center;">
                        <h5 style="margin-bottom: 15px;">
                            To download documents please click on "Download Documents" button.
                        </h5>

                        <a style="border-radius: 4px;margin: 20px 20px 20px 0px;background-color: #47c4f0 !important;color: #fff !important;border: none;text-decoration: none;line-height: normal;padding: 8px 15px !important;
                            font-size: 16px !important;" href="{{$template['link']}}">Download Documents</a>
                    </p>
                @endif
            </td>
        </tr>
        </tbody>
    </table>
@stop