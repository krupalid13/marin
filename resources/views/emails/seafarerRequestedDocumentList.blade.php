@extends('emails.default_layout')

@section('email_content')
    <table class="" width="100%" cellspacing="0" cellpadding="0" style="max-width:600px;">
        <tbody>
        <tr>
            <td align="left" valign="top" class="" style="color:#2c2c2c;display:block;line-height:20px;font-weight:300;margin:0 auto;clear:both;background-color:#fff;padding:20px" bgcolor="#F9F9F9">
                <p style="padding:0;margin:0;font-size:16px;font-weight:bold;font-size:13px">
                    Hi {{isset($template['first_name']) ? ucwords($template['first_name']) : ''}},
                </p>

                <br>

                @if($template['registered_as'] == 'institute')
                    <h3>Requested documents by institute.</h3>
                    <h5>Institute Name : <strong>{{ucwords($template['requester_name'])}}</strong></h5>
                    <h5>Institute Email Id : <strong>{{$template['requester_email']}}</strong></h5>
                    <h5>Document Types : <strong>{{strtoupper($template['type'])}}</strong></h5>
                @endif

                @if($template['registered_as'] == 'company')
                    <h3>Requested documents by company.</h3>
                    <h5>Comapny Name : <strong>{{ucwords($template['company_name'])}}</strong></h5>
                    <h5>Comapny Email Id : <strong>{{$template['company_email_id']}}</strong></h5>
                    <h5>Document Types : <strong>{{strtoupper($template['type'])}}</strong></h5>
                @endif

                <br>

                <p style="padding:0;margin:0;color:#565656;font-size:13px">
                    Please click on "Accept" button to give the permission or "Reject" to deny the permission to see the documents.
                </p>
                <p style="display: flex;align-items: center;">
                    <a class="btn" style="border-radius: 4px;margin: 20px 20px 20px 0px;background-color: #47c4f0 !important;color: #fff !important;border: none;text-decoration: none;line-height: normal;padding: 8px 20px !important;
                        font-size: 16px !important;" target="_blank" href="{{$template['link']}}">
                        Accept
                    </a>
                    <a class="btn" style="border-radius: 4px;margin: 20px 20px 20px 0px;background-color: #c71c14 !important;color: #fff !important;border: none;text-decoration: none;line-height: normal;padding: 8px 20px !important;
                        font-size: 16px !important;" href="{{$template['link']}}" target="_blank">
                        Reject
                    </a>
                </p>
            </td>
        </tr>
        </tbody>
    </table>
@stop