@extends('emails.default_layout')

@section('email_content')
	<table class="" width="100%" style="max-width:600px;"> 
        <tbody>
            <tr> 
                <td align="left" valign="top" class="" style="color:#2c2c2c;display:block;line-height:20px;font-weight:300;margin:0 auto;clear:both;background-color:#fff;padding:20px" bgcolor="#F9F9F9"> 
                    <p style="padding:0;margin:0;font-size:16px;font-weight:bold;font-size:13px"> 
                        Hi {{isset($template['user_details']['first_name']) ? $template['user_details']['first_name'] : ''}}, 
                    </p>
                    <br> 
                    <p style="padding:0;margin:0;font-size:16px;font-size:13px"> We have found jobs for you.</p>
                    <br>

                    <?php
                    	$i = 0;
						$maxiterations = 5;
                    ?>
                	@foreach($template['job_details'] as $jobs)
                		<table width="100%" cellspacing="5" style="border:1px solid #ddd;margin-bottom: 7px;">
                    		@if ($i < $maxiterations)
                    		<?php
                    			$img_path = '';
                    			if(isset($jobs['company_registration_details']['user']['profile_pic']) AND !empty(isset($jobs['company_registration_details']['user']['profile_pic']))){
                    				$img_path = $jobs['company_registration_details']['user']['profile_pic'];
                    			}
                    		?>
							<tr style="border:1px solid #ddd;">
						    	<td width="150px" style="text-align: center;padding-right: 5px;">

						    		@if(!empty($img_path))
						    			<img class="profile-pic" src="{{asset(isset($jobs['company_registration_details']['user']['profile_pic']) ? env('COMPANY_LOGO_PATH').$jobs['company_registration_details']['user']['id'].'/'.$jobs['company_registration_details']['user']['profile_pic'] : 'images/user_default_image.png')}}">
						    		@else
						    			<img class="profile-pic" src="{{ asset('images/user_default_image.png') }}" alt="" height="150px" width="150px">
						    		@endif
						    	</td>
						    	<td width="40%" style="padding-left: 10px;">
						    		<div class="company-name" style="margin-bottom: 10px;">
				    					{{ isset($jobs['company_registration_details']['company_name']) ? ucfirst($jobs['company_registration_details']['company_name']) : ''}}
				    				</div>
						    		<div class="col-sm-6" style="font-size: 12px;">
			    						Rank:
			                            @foreach(\CommonHelper::new_rank() as $index => $category)
                                            @foreach($category as $r_index => $rank)
                                                {{ !empty($jobs['rank']) ? $jobs['rank'] == $r_index ? $rank : '' : ''}}
                                            @endforeach
                                        @endforeach
			    					</div>
			    					<div class="col-sm-6" style="font-size: 12px;">
				    					Nationality: 
				    						@foreach(\CommonHelper::countries() as $n_index => $nationality)
				                                {{ !empty($jobs['nationality']) ? $jobs['nationality'] == $n_index ? $nationality : '' : ''}}
				                            @endforeach
				                            {{ isset($jobs['nationality']) ? $jobs['nationality'] == '0' ? 'All' : '' : ''  }}
			                        </div>
				                    <div class="col-sm-6" style="font-size: 12px;">
			    						Date of Joining: {{ isset($jobs['date_of_joining']) ? $jobs['date_of_joining'] : '-'}}
			    					</div>
			    					<div class="other-discription row">
				    					<div class="col-sm-6" style="font-size: 12px;">
				    					Minimum Rank Exp: {{ isset($jobs['min_rank_exp']) ? $jobs['min_rank_exp'] : ''}} Years
				    					</div>
				    				</div>
							    	
				    				
						    	</td>
						    	<td style="padding-top: 10px;">
			    					<div class="col-sm-6" style="font-size: 12px;">
				    					Ship Type: 
				    						@foreach(\CommonHelper::ship_type() as $s_index => $ship_type)
				                                {{ !empty($jobs['ship_type']) ? $jobs['ship_type'] == $s_index ? $ship_type : '' : ''}}
				                            @endforeach
				                    </div>
				                    <div class="col-sm-6" style="font-size: 12px;">
				                    	BHP: 
			                                {{ !empty($jobs['bhp']) ? $jobs['bhp'] : ''}}
				                    </div>
			    					<div class="col-sm-6" style="font-size: 12px;">
			    						GRT: 
			                                {{ !empty($jobs['grt']) ? $jobs['grt'] : ''}}
			    					</div>
							    	<!-- <td colspan="2">
								    	<div class="other-discription row">
					    					<div class="col-sm-6" style="font-size: 12px;">
					    					Job Description: {{ isset($jobs['job_description']) ? $jobs['job_description'] : ''}}
					    					</div>
					    				</div>
					    			</td> -->
						    	</td>

						    </tr>
						    <tr>
						    	<td></td>
						    	<td colspan="2" style="padding-left: 10px;">
						    		<div class="other-discription row">
				    					<div class="col-sm-6" style="font-size: 12px;">
				    					Job Description: {{ isset($jobs['job_description']) ? $jobs['job_description'] : ''}}
				    					</div>
				    				</div>
				    			</td>
				    		</tr>
						    <tr>
						    	<td></td>
						    	<td colspan="2" style="text-align: center;padding: 5px;">
						    		
						    		<a class="btn" style="border-radius: 4px;background-color: #c71c14 !important;color: #fff !important;border: 1px solid #c71c14 !important;text-decoration: none;line-height: normal;padding: 6px 145px !important;
				                        font-size: 14px !important;" href="{{isset($template['route']) ? $template['route'].'?param='.base64_encode('user_id='.$template['user_id'].'&rank='.$template['applied_rank'].'&job_id='.$jobs['id'].'&company_id='.$jobs['company_id'].'&auto_apply=true') : ''}}" target="_blank">
				                        Apply Now
				                    </a>
						    	</td>
						    </tr>
						    <?php $i++; ?>
						   	@endif
						</table>
		    			@endforeach
		    			
		    			@if(isset($template['job_details']) AND count($template['job_details']) > 5)
		    			<table width="100%">
							<tr>
				    			<td colspan="3" style="padding-top: 15px;">
				    				<p style="padding:0;margin:0;font-size:16px;font-size:13px"> To view more jobs please click on below button.</p>
	                    			<br>
				    				<a class="btn" style="border-radius: 4px;background-color: #c71c14 !important;color: #fff !important;border: 1px solid #c71c14 !important;text-decoration: none;line-height: normal;padding: 8px 20px !important;
				                        font-size: 16px !important;" href="{{isset($template['route']) ? $template['route'] : ''}}" target="_blank">
				                        View More Jobs
				                    </a>
				    			</td>
				    		</tr>
				    	</table>
			    		@endif
					
                </td> 
            </tr>
         </tbody>
    </table>
@stop