@extends('emails.default_layout')

@section('email_content')
	<table class="" width="100%" style="max-width:600px;"> 
        <tbody>
            <tr> 
                <td align="left" valign="top" class="" style="color:#2c2c2c;display:block;line-height:20px;font-weight:300;margin:0 auto;clear:both;background-color:#fff;padding:20px" bgcolor="#F9F9F9"> 
                    <p style="padding:0;margin:0;font-size:16px;font-weight:bold;font-size:13px"> 
                        Hi {{isset($template['institute_registration_details']['institute_name']) ? $template['institute_registration_details']['institute_name'] : ''}}, 
                    </p>
                    <br> 
                    <p style="padding:0;margin:0;font-size:13px"> We have found matching seafarer according to course batch.</p>
                    <br>
                    <p style="margin:0;font-size:13px">
                    	Batch Details are as follows:-
                    	<table width="100%" cellspacing="0" cellpadding="0" style="padding:0;margin:0;font-size:16px;font-size:13px">
	                    	<tr>
		                    	<td style="padding-top: 5px;padding-bottom: 5px;">
		                    	Course Type: 
		                        @foreach($template['institute_course_type'] as $index => $category)
		                            {{ isset($template['course_type']) ? $template['course_type'] == $index ? $category : '' : ''}}
		                        @endforeach
								</td>							
	                        	<td style="padding-top: 5px;padding-bottom: 5px;">Course Name: 
	                        	{{ isset($template['course_name']) ? $template['course_name'] : ''}}
	                        	</td>

	                        </tr>
	                    </table>
                    </p>
                    <?php
                    	$i = 0;
						$maxiterations = 5;
                    ?>
                	
            		@foreach($template['seafarers'] as $seafarer_details)
                		<table width="100%" cellspacing="5" style="border:1px solid #ddd;margin-bottom: 7px;">
                    		@if ($i < $maxiterations)
                    		<?php
                    			$img_path = '';
                    			if(isset($seafarer_details['user']['profile_pic']) AND !empty(isset($seafarer_details['user']['profile_pic']))){
                    				$img_path = $seafarer_details['user']['profile_pic'];
                    			}

                    			if(isset($template['seafarer_profile_route']) && isset($seafarer_details['user']))
                    				$path = $template['seafarer_profile_route'].'/'.$seafarer_details['user']['id'];
                    		?>
                    		
							<tr style="border:1px solid #ddd;">
						    	<td height="150px" width="150px" style="text-align: center;padding-right: 5px;">
						    		<a href="{{isset($path) ? $path : ''}}" target="_blank">
							    		@if(!empty($img_path))
						    				<img class="profile-pic" src="{{asset(isset($seafarer_details['user']['profile_pic']) ? env('SEAFARER_PROFILE_PATH').$seafarer_details['user_id'].'/'.$seafarer_details['user']['profile_pic'] : 'images/user_default_image.png')}}">
							    			
							    		@else
							    			<img class="profile-pic" src="{{ asset('images/user_default_image.png') }}" alt="" height="150px" width="150px">
							    		@endif
						    		</a>
						    	</td>
						    	<td width="40%" style="padding-left: 10px;">
						    		<div class="company-name" style="margin-bottom: 10px;">
				    					{{ isset($seafarer_details['user']['first_name']) ? $seafarer_details['user']['first_name'] : ''}}
				    				</div>
				                    <div class="col-sm-6" style="font-size: 12px;">
			    						Applied Rank: 
			    						@foreach(\CommonHelper::new_rank() as $index => $category)
									    	@foreach($category as $r_index => $rank)
	                                        	{{ !empty($seafarer_details['professional_detail']['applied_rank']) ? $seafarer_details['professional_detail']['applied_rank'] == $r_index ? $rank : '' : ''}}
	                                        @endforeach
	                                    @endforeach

			    					</div>
							    	<div class="other-discription row">
				    					<div class="col-sm-6" style="font-size: 12px;">
				    						Current Rank: 
				    						@foreach(\CommonHelper::new_rank() as $index => $category)
										    	@foreach($category as $r_index => $rank)
		                                        	{{ !empty($seafarer_details['professional_detail']['current_rank']) ? $seafarer_details['professional_detail']['current_rank'] == $r_index ? $rank : '' : ''}}
		                                        @endforeach
		                                    @endforeach
				    					</div>
				    				</div>
				    				<?php
                                        if(isset($seafarer_details['professional_detail']['current_rank_exp'])){
                                            $rank_exp = explode(".",$seafarer_details['professional_detail']['current_rank_exp']);
                                            $rank_exp[1] = number_format($rank_exp[1]);
                                        }
				    				?>
			    					<div class="other-discription row">
				    					<div class="col-sm-6" style="font-size: 12px;">
				    					Current Rank Exp: 
				    					{{ isset($seafarer_details['professional_detail']['current_rank_exp']) ? $rank_exp[0] ." Years ". $rank_exp[1].' Months' : ''}}
				    					</div>
				    				</div>
				    				
						    	</td>
						    	<td style="padding-top: 50px;">
			    					<div class="col-sm-6" style="font-size: 12px;">
				    					Country: 
				    						@foreach( \CommonHelper::countries() as $c_index => $country)
                                                {{ isset($seafarer_details['user_personal_detail']['country']) ? $seafarer_details['user_personal_detail']['country'] == $c_index ? $country : '' : ''}}
                                            @endforeach
				                    </div>
				                    <div class="col-sm-6" style="font-size: 12px;">
				                    	State : 
			                                {{ isset($seafarer_details['user_personal_detail']['state_id']) ? ucfirst(strtolower($seafarer_details['user_personal_detail']['pincode']['pincodes_states'][0]['state']['name'])) : (isset($seafarer_details['user_personal_detail']['state_text']) ? ucfirst(strtolower($seafarer_details['user_personal_detail']['state_text'])) : '')}}
				                    </div>
			    					<div class="col-sm-6" style="font-size: 12px;">
			    						City: 
			                                {{ isset($seafarer_details['user_personal_detail']['city_id']) ? $seafarer_details['user_personal_detail']['pincode']['pincodes_cities'][0]['city']['name'] : (isset($seafarer_details['user_personal_detail']['city_text']) ? $seafarer_details['user_personal_detail']['city_text'] : '')}}
			    					</div>
			    					<div class="col-sm-6" style="font-size: 12px;">
			    						Postal Code: 
			                                {{ isset($seafarer_details['user_personal_detail']['pincode_text']) ? $seafarer_details['user_personal_detail']['pincode_text'] : ''}}
			    					</div>
			    					<div class="col-sm-6" style="font-size: 12px;">
			    						Date Of Expiry: 
			                                {{ isset($seafarer_details['expiry_date']) ? date('d-m-Y',strtotime($seafarer_details['expiry_date'])) : '-'}}
			    					</div>
						    	</td>
						    </tr>
						    
						    <?php $i++; ?>
						   	@endif
						</table>
					@endforeach					
                </td> 
            </tr>
         </tbody>
    </table>
@stop