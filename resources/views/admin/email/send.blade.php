@extends('admin.index')
@section('style_section')
@endsection
@section('content')
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-white">
                <div class="panel-body">
                    {{
	                      Form::open([
	                      'id'=>'sendEmail',
	                      'class'=>'FromSubmit form-horizontal',
	                      'url'=>route('admin.email.sendEmail'),
	                      'data-redirect_url'=>route('admin.email'),
	                      'name'=>'Company',
	                      'enctype' =>"multipart/form-data"
	                      ])
                    }}


                    {{ csrf_field() }}

                    <h2 >
                        Send Emails
                    </h2>
                    <hr>
                    <div class="email_list">
                        <div class="form-group">
                            <label class="col-sm-3 control-label">
                                Email <span class="symbol required"></span>
                            </label>
                            <div class="col-sm-6">
                                <input placeholder="Enter email" class="form-control" name="email[]" type="email">
                            </div>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-12 ">
                            <div class="col-md-offset-3">
                                <button type="button" class="btn btn-primary add-more-email">Add More Email</button>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-offset-3">
                                    <button type="submit" class="btn btn-primary">Send</button>
                                    <a href="{{URL(route('admin.email'))}}" type="button" class="btn btn-danger">Cancel</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>		

</div>
<div class="form-group collapse add-more-email-div">
    <label class="col-sm-3 control-label">
        Email <span class="symbol required"></span>
    </label>
    <div class="col-sm-6">
        <input placeholder="Enter email" class="form-control" name="email[]" type="email">
    </div>
</div>
@stop

@section('js_script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/additional-methods.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-jgrowl/1.4.7/jquery.jgrowl.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-jgrowl/1.4.7/jquery.jgrowl.css" />

<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<script>
$(document).ready(function () {
    $("#sendEmail").validate({
        rules: {
            'email[]': {
                required: true,
                email: true
            },
        },
        messages: {
            'email': {required: "Please enter email"},
        },
        errorPlacement: function (error, element) {
            $(element).closest('div').append(error);
        }
    });
});

$(document).ready(function () {

    $(".add-more-email").click(function () {
        var html = $(".add-more-email-div").clone();
        console.log(html);
        html
                .removeClass('add-more-email-div')
                .removeClass('collapse')
                .end();
        $(".email_list").after(html);
    });


    $("body").on("click", ".remove", function () {
        $(this).parent().parents(".form-group").remove();
    });


});




</script>

@stop

