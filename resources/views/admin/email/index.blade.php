@extends('admin.index')

@section('style_section')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css">

<style>
    div#padding_to_add {
        padding: 25px;
    }
</style>
@endsection

@section('content')
<div class="container">
    <div class="row-fluid">
        <div class="row" id="padding_to_add">
            <div class="col-12">
                <div class="pull-right">
                    <a class="btn btn-primary" href="{{ route('admin.email.send') }}" title="Send Emails">Send Emails</a>
                </div>
            </div>
        </div>
        <br>
        <div class="datatable-container">

            {{ Form::open(
		          array(
		          'id'                => 'table_form',
		          'name'              => 'table_form',
		          'class'             => 'ajaxFormSubmitDatatable',
		          'method'              => 'POST'
		          ))
            }}

            <table class="table table-bordered table-hover" id="dataTableBuilder" width="100%">
                <thead>
                <th>Email</th>
                <th>Send Time</th>
                <th>Profile View</th>
                <th>Total Profile view</th>
                <th>User Register</th>
                </thead>
                <tbody></tbody>
                <tfoot></tfoot>
            </table>
            </form>
        </div>
    </div>
</div>

</div>

@stop

@section('js_script')
<script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
<script>
jQuery(document).ready(function () {
    FormWizard.init();
});
</script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>

function RESET_FORM()
{
    $('#search-frm').trigger('reset');
    table.draw();
    return false;
}
$(document).on("click", "button[type='submit']", function () {
    table.draw();
    return false;
});
var table = $('#dataTableBuilder').DataTable({
    bProcessing: true,
    bServerSide: true,
    processing: true,
    serverSide: true,
    searching: true,
    order: [[ 1, "desc" ]],
    ajax: {
        url: "{{ route('admin.get.emails') }}?datatable=no",
        data: function (d) {
            d.id = $('input[name=id]').val();
            d.title = $('input[name=title]').val();
        }
    },
    columns: [
        {data: 'email'},
        {data: 'created_at'},
        {data: 'last_view', searchable: false, orderable: false},
        {data: 'view_count', searchable: false, orderable: false},
        {data: 'user_register', searchable: false, orderable: false}
    ]
});
</script>
@stop