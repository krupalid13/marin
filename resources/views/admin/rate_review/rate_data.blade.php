@extends('site.index')
@section('page-level-style')
<style>

@media only screen and (min-width: 768px) {

.rowClass {
    margin-top: 100px;
}
.masonry { /* Masonry container */
    column-count: 2;
    column-gap: 0.81em;
}

.card { /* Masonry bricks or child elements */
    /*background-color: #eee;*/
    display: inline-block;
    margin: 0 0 1em;
    width: 100%;
}


}

@media only screen and (max-width: 768px) {

.masonry { /* Masonry container */
    column-count: 1;
    /*column-gap: 0.81em;*/
}
.card {
    margin: 0 auto;
    width: 80%;
}
}


    .card {
        margin: 10px;
        border: 1px solid black;
        border-radius: 0 !important;
    }

    nav.navbar.navbar-default.z-depth-1, .cs-footer {
        display: none;
    }

    figure.profile-picture {
        background-position: center center;
        background-size: cover;
        border: 5px #efefef solid;
        border-radius: 50%;
        bottom: -50px;
        box-shadow: inset 1px 1px 3px rgba(0, 0, 0, 0.2), 1px 1px 4px rgba(0, 0, 0, 0.3);
        height: 100px;
        width: 100px;
        float: left;
        margin-right: 15px;
    }

    .col-md-6 {
        border: 1px solid black;
    }

    .name-title {
        float: left;
        display: inline-grid;
        font-weight: 700;
        margin-top: 10px;
    }

    .name-title span {
        font-weight: 300;
    }

    .section {
        width: 100%;
        display: inline-table;
        padding: 12px;
        margin-top: 5px;
    }

    .inner-row {
        width: 100%;
        display: inline-table;
    }

    .name-time {
        margin-top: 10px;
        margin-bottom: 0px;
    }

    .content {
        width: 100%;
        display: inherit;
    }

    .line {
        border-top: 1px solid black;
    }

    .review-line {
        float: left;
        margin-right: 10px;
    }
    .font-word{
        width: 569px;
        overflow: hidden;
        text-overflow: ellipsis;
        word-break: break-word;
        font-size: 13px;
    }
    .ship-rate{
        margin: -5px;
    }
    .review-comment{
        font-size: 13px;
        word-break: break-word;
    }
</style>
@stop
@section('content')
    <div class="container">
        <div class="row rowClass">
            <div class="row masonry">
                @foreach($rateData as $row)
                    <div class="card">
                        <div class="section">
                            <div class="inner-row">
                                <figure class="profile-picture" style="{{'background-image: url('.url('public/images/uploads/seafarer_profile_pic/'.$row->user_id.'/'.$row->getUserData['profile_pic']).')'}}">
                                </figure>
                                <p class="name-title">
                                    {{$row->getUserData['first_name'].' '.$row->getUserData['last_name']}}
                                    <span>Chief engineer</span>
                                </p>
                            </div>
                            <div class="inner-row">
                                <p class="name-time"><b>Vessel:</b> {{$row->getShipData['ship_name']}}&nbsp;&nbsp;&nbsp;<b>For Period:</b>
                                    {{\Carbon\Carbon::parse($row->getShipData['from'])->format('d-m-Y')}} TO
                                    {{\Carbon\Carbon::parse($row->getShipData['to'])->format('d-m-Y')}}</p>
                            </div>
                        </div>
                        <div class="section line">
                            <div class="content">
                               <div class="font-word"> <b>Exp:</b> &nbsp;{{$row->experience}}</div>
                            </div>
                        </div>
                        <div class="section line">
                            @if($row->vessel_rate)
                                <div class="content">
                                    <div class="review-line"><b>Vessel:</b></div>
                                    <div class="review-line">{{$row->getShipData['ship_name']}}</div>
                                    <div class="{{'vesselShipRate'.$row->id}} review-line ship-rate"></div>
                                    <div class="review-line review-comment">&nbsp;"{{$row->vessel_comment}}"</div>
                                    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.3.2/jquery.rateyo.min.css">
                                    <script src="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.3.2/jquery.rateyo.min.js"></script>
                                    <script type="text/javascript">
                                        var shipId ="{{$row->id}}";
                                        $(".vesselShipRate"+shipId).rateYo({
                                            rating: "{{$row->vessel_rate}}",
                                            readOnly: true
                                        });
                                    </script>
                                </div>
                            @endif
                            @if($row->company_rate)
                                <div class="content">
                                    <div class="review-line"><b>Owener:</b></div>
                                    <div class="review-line">{{$row->getShipData['company_name']}}</div>
                                    <div class="{{'ownerShipRate'.$row->id}} review-line ship-rate"></div>
                                    <div class="review-line review-comment">&nbsp;"{{$row->company_comment}}"</div>
                                    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.3.2/jquery.rateyo.min.css">
                                    <script src="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.3.2/jquery.rateyo.min.js"></script>
                                    <script type="text/javascript">
                                        var shipId ="{{$row->id}}";
                                        $(".ownerShipRate"+shipId).rateYo({
                                            rating: "{{$row->company_rate}}",
                                            readOnly: true
                                        });
                                    </script>
                                </div>
                            @endif
                            @if($row->manning_rate)
                                <div class="content">
                                    <div class="review-line"><b>Manning:</b></div>
                                    <div class="review-line">{{$row->getShipData['manning_by']}}</div>
                                    <div class="{{'manningShipRate'.$row->id}} review-line ship-rate"></div>
                                    <div class="review-line review-comment">&nbsp;"{{$row->manning_comment}}"</div>
                                    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.3.2/jquery.rateyo.min.css">
                                    <script src="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.3.2/jquery.rateyo.min.js"></script>
                                    <script type="text/javascript">
                                        var shipId ="{{$row->id}}";
                                        $(".manningShipRate"+shipId).rateYo({
                                            rating: "{{$row->manning_rate}}",
                                            readOnly: true
                                        });
                                    </script>
                                </div>
                            @endif
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@stop
@section('js_script')
@stop

