<div id="show_more_main{{$row->id}}" class="col-md-12 col-sm-12 col-xs-12" style="background-color: #ffffff;    padding: 10px;    margin-top: 30px;    margin-left: 5px;    border: 1px solid #b7b7b7;">
   <div class="col-sm-3  col-xs-12 borderright" style="border-right: 1px solid #cecece;     min-height: 130px;">
      <div class="form-group">
         <div style="display: block; width:100%">
            <label class="input-label rank-label" for="mobile" style="color:#043559; font-size: 13px; margin:0px;  width: 100%;"><i class="fa fa-tag"></i> Rank :</label>
            <span class="rank-text" style="color: #1b1b1b !important; margin-top: -2px;  font-size: 13px;  float: left;">
               @foreach(\CommonHelper::new_rank() as $index => $category)
                  @foreach($category as $r_index => $rank)
                     {{ isset($row->rank_id) ? $row->rank_id == $r_index ? $rank : '' : ''}}
                  @endforeach
            @endforeach
            </span>
         </div>
         <div style="display: block; width:100%">
            <label class="input-label ship-type-label" for="mobile" style="color:#043559; font-size: 13px; margin-top:24px; width: 100%;" > <i class="fa fa-ship"></i> Ship Type:</label>
            <span class="ship-type-text" style="color: #1b1b1b !important; margin-top: -2px;  font-size: 13px;  float: left;">
               @foreach( \CommonHelper::ship_type() as $c_index => $type)
                  {{ isset($row->ship_type) ? $row->ship_type == $c_index ? $type : '' : ''}}
            @endforeach
            </span>
         </div>
         <div style="display: block; width:100%">
            <label class="input-label period-label" for="mobile" style="color:#043559; font-size: 13px; margin-top:24px; width: 100%;" ><i class="fa fa-calendar"></i> Period :</label>
            <span class="period-text" style="color: #1b1b1b !important; margin-top: -2px;  font-size: 13px;  float: left;margin-bottom:24px">{{ isset($row->from) ? date('d-m-Y',strtotime($row->from)) : ''}} To {{ isset($row->to) ? date('d-m-Y',strtotime($row->to)) : ''}}</span>
         </div>
      </div>
   </div>
   <div class="col-sm-6  col-xs-12 borderright" style="border-right: 1px solid #cecece;     min-height: 130px;margin-top: 10px;margin-bottom: 10px;">
      
      <!------------------ Job profile --------------------->
      <div class="form-group jobprofile{{ $row->id }}">
         <label class="input-label" for="mobile" style="color:#043559; font-size: 13px; margin:0px;  width: 100%;"><i class="fa fa-comments" aria-hidden="true"></i> Job Profile :</label>
         
         <span style="color: #1b1b1b !important; width: 100%; ;  font-size: 14px;   float: left; margin-top:24px">"{{ $row->getRateData->experience ?? '-' }}"</span>
         
      </div>
      <!------------------ Job profile --------------------->


      <div class="form-group vessel{{ $row->id }}" style="display: none;">
         <span class="input-label" for="mobile" style="color:#043559; font-size: 13px; font-weight: 600; margin:0px;  width: 100%;margin-top:24px"><i class="fa fa-anchor" aria-hidden="true"></i> Vessel Name: </span> {{ $row->ship_name }}
         
         <span style="color: #1b1b1b !important; width: 100%;margin-top:24px;  font-size: 15px;   float: left; ">"{{ $row->getRateData->vessel_comment ?? '-' }}"</span>
         <br>
         <!--vessel_rate-->
         <div style="width: 100%; float: left; margin-top:24px">
         <span class="getclass scorevessel{{ $row->id }}" data-id="{{ $row->id }}" data-type="vessel" data-score="{{ $row->getRateData->vessel_rate ?? 0.0 }}">
            
         </span>
         
         
         </div>
      </div>


      <div class="form-group owner{{ $row->id }}" style="display: none;">
         <span class="input-label" for="mobile" style="color:#043559; font-size: 13px; font-weight: 600; width: 100%;margin-top:24px"><i class="fa fa-building-o" aria-hidden="true"></i>  Owner Company: </span> {{ $row->company_name }}
         
         <span style="color: #1b1b1b !important; width: 100%; margin-top:24px;  font-size: 15px;   float: left; ">"{{ $row->getRateData->company_comment ?? '-' }}"</span>
         <br>
         <!--vessel_rate-->
         <div style="width: 100%; float: left; margin-top:24px">
            <span class="getclass scoreowner{{ $row->id }}" data-id="{{ $row->id }}" data-type="owner" data-score="{{ $row->getRateData->company_rate ?? 0.0 }}">
            
         </span>
         
         
         </div>
      </div>

      <div class="form-group manning{{ $row->id }}" style="display: none;" >
         <span class="input-label" for="mobile" style="color:#043559; font-size: 13px; font-weight: 600; margin:0px;  width: 100%;"><i class="fa fa-building-o" aria-hidden="true"></i>  Manning Company: </span> {{ $row->manning_by }}
         
         <span style="color: #1b1b1b !important; width: 100%; margin-top:24px;  font-size: 15px;   float: left; ">"{{ $row->getRateData->manning_comment ?? '-' }}"</span>
         <br>
         <!--manning_rate-->
         <div style="width: 100%; float: left; margin-top:24px">
            <span class="getclass scoremanning{{ $row->id }}" data-id="{{ $row->id }}" data-type="manning" data-score="{{ $row->getRateData->manning_rate ?? 0.0 }}">
            
         </span>
         
         
         </div>
      </div>


   </div>
   <div class="col-sm-3  col-xs-12" style="min-height: 130px;">
      <div class="form-group">
         <label class="btns{{ $row->id }} btn btn-primary btn-xs btn-block active-button clickon" for="mobile" data-type="jobprofile" data-id="{{ $row->id }}" style="font-size: 13px;"><i class="fa fa-comments"></i> Job Profile</label>
         <label class="btns{{ $row->id }} btn btn-primary btn-xs btn-block clickon" for="mobile" data-type="vessel" data-id="{{ $row->id }}" style="font-size: 13px;margin-top:24px"><i class="fa fa-anchor"></i> Vessel Name</label>
         {{-- <span class="clickon" data-type="vessel"style="font-size: 13px;" data-id="{{ $row->id }}">{{ $row->ship_name }}</span><br> --}}

         <label class="btns{{ $row->id }} btn btn-primary btn-xs btn-block clickon mt-2" for="mobile" data-type="owner" data-id="{{ $row->id }}" style="font-size: 13px;margin-top:24px" ><i class="fa fa-building-o" aria-hidden="true"></i> Owner Company</label>
         {{-- <span data-type="owner" style="font-size: 13px;" data-id="{{ $row->id }}" class="clickon">{{ $row->company_name }} </span><br> --}}
         <label data-type="manning" data-id="{{ $row->id }}"  class="btns{{ $row->id }} btn btn-primary btn-xs btn-block clickon " for="mobile" style="font-size: 13px;margin-top:24px" ><i class="fa fa-building-o" aria-hidden="true"></i>  Manning Company</label>
         {{-- <span  data-type="manning" style="font-size: 13px;" data-id="{{ $row->id }}" class="clickon">{{ $row->manning_by }} </span><br> --}}
      </div>
   </div>
</div>
            