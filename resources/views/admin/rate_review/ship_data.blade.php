<div class="col-md-9">
    <p class="sea">
        <b>For sea service</b>: {{\Carbon\Carbon::parse($shipData->from)->format('d-m-Y')}} To {{\Carbon\Carbon::parse($shipData->to)->format('d-m-Y')}}
    </p>
    <div class="row box">
        <div class="box-header">
            <p><strong>Vessel: </strong>{{$shipData->ship_name}}&nbsp;&nbsp;
                <strong>Rank: </strong>@foreach(CommonHelper::new_rank() as $index => $category)
                @foreach($category as $r_index => $rank)
                    {{ isset($shipData->rank_id) ? $shipData->rank_id == $r_index ? $rank : '' : ''}}
                @endforeach
            @endforeach</p>
        </div>
        <h5 class="head-border float-left mx-3">My Onboard Job Profile</h5>
        <!--                <div class="about-border"></div>-->
        <div class="input-group" id="lname">
            {{Form::textarea('exterience',!empty($rateReview) && $rateReview->experience ? $rateReview->experience : null,['class'=>'form-control custom-control experience','rows'=>3,'placeholder'=>'You can write about your Onboard profile, duties, job experience, working hrs, special moment and relation with rest of the officers and crew onboard…etc.','style'=>'resize:none'])}}
            <button class="rightButton float-right button button-primary save-experience"><i class="fa fa-check">&nbsp;</i></button>
            <span class="rate-error exterience-error-msg d-none">Please enter your experience.</span>
        </div>
        @if(!empty($rateReview) && $rateReview->experience)
            <div class="row boxRaw" style="padding-bottom: 0px;">
                <div class="center-border center rate-review-text">Rate and review</div>
            </div>
            <div class="row boxRaw">
                @if($shipData->ship_name)
                    {{-- vassel box --}}
                    <div class="column ship-item">
                        <span class="color-gray">Vessel</span>
                        <div class="card">
                            {{-- <i class="fa fa-pencil"></i> --}}
                            <div class="card-header ship-name">
                                {{$shipData->ship_name}}
                            </div>
                            <div class="card-body">
                                <div id="vessel_rate"></div>
                            </div>
                            @if(!empty($rateReview) && $rateReview->vessel_rate || $rateReview->vessel_comment)
                                <div class="comment-text">*{{$rateReview->vessel_comment}}*</div>
                            @else
                                <button data-toggle="modal" data-modalname="Vessel" data-target="#rateting-modal" data-type="vessel" data-name="{{$shipData->ship_name}}" class="speech-bubble comment-data" >Comment</button>
                            @endif
                        </div>
                    </div>
                @endif
                @if($shipData->company_name)
                    {{-- owner box --}}
                    <div class="column ship-item">
                        <span class="color-gray">Owner Company</span>
                        <div class="card">
                            {{-- <i class="fa fa-pencil"></i> --}}
                            <div class="card-header">
                                {{$shipData->company_name}}
                            </div>
                            <div class="card-body">
                                <div id="company_rate"></div>
                            </div>
                            @if(!empty($rateReview) && $rateReview->company_rate || $rateReview->company_comment)
                                <span class="comment-text">* {{$rateReview->company_comment}} *</span>
                            @else
                                <button data-toggle="modal" data-modalname="Owner" data-target="#rateting-modal" data-type="company" data-name="{{$shipData->company_name}}" class="speech-bubble comment-data">Comment</button>
                            @endif
                        </div>
                    </div>
                @endif
                @if($shipData->manning_by)
                    {{-- manning box --}}
                    <div class="column  ship-item">
                        <span class="color-gray">Manning Company</span>
                        <div class="card">
                            {{-- <i class="fa fa-pencil"></i> --}}
                            <div class="card-header">
                                {{$shipData->manning_by}}
                            </div>
                            <div class="card-body">
                                <div id="manning_rate"></div>
                            </div>
                            @if(!empty($rateReview) && $rateReview->manning_rate || $rateReview->manning_comment)
                                <span class="comment-text">* {{$rateReview->manning_comment}} *</span>
                            @else
                                <button data-toggle="modal" data-modalname="Manning" data-target="#rateting-modal" data-type="manning" data-name="{{$shipData->manning_by}}" class="speech-bubble comment-data">Comment</button>
                            @endif
                        </div>
                    </div>
                @endif
            </div>
        @endif
    </div>
</div>
<script type="text/javascript">
$("#vessel_rate").rateYo({
    rating: "{{$rateReview && $rateReview->vessel_rate ? $rateReview->vessel_rate : 0.0}}",
    readOnly: true,
    starWidth: "20px"
});
$("#company_rate").rateYo({
    rating: "{{$rateReview && $rateReview->company_rate ? $rateReview->company_rate : 0.0}}",
    readOnly: true,
    starWidth: "20px"
});
$("#manning_rate").rateYo({
    rating: "{{$rateReview && $rateReview->manning_rate ? $rateReview->manning_rate : 0.0}}",
    readOnly: true,
    starWidth: "20px"
});

</script>