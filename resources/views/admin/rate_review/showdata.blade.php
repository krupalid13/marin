@extends('site.index')
@section('page-level-style')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.3.2/jquery.rateyo.min.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" rel="stylesheet" />
    <style>
        .active-button {
            background: #FFF;
            color: #043559;
        }

        :root {
            --star-size: 60px;
            --star-color: #fff;
            --star-background: #fc0;
        }

        .Stars {
            --percent: calc(var(--rating) / 5 * 100%);

            display: inline-block;
            font-size: var(--star-size);
            font-family: Times; // make sure ★ appears correctly
            line-height: 1;

            &::before {
                content: '★★★★★';
                letter-spacing: 3px;
                background: linear-gradient(90deg, var(--star-background) var(--percent), var(--star-color) var(--percent));
                -webkit-background-clip: text;
                -webkit-text-fill-color: transparent;
            }
        }

        .jq-ry-container {
            padding: 0px !important;
        }



        .checked {
            color: orange;
        }

        .clickon {
            cursor: pointer;
        }

        td {
            text-align: left;
            color: #4e4e4e;
            font-size: 13px;
        }

        @media all and (max-width: 1200px) and (min-width: 900px) {
            .col-sm-3 {
                padding-left: 5px !important;
                padding-right: 5px !important;
            }

        }
        .filter-container {}
        .rank-label, .rank-text, .ship-type-label, .ship-type-text, .period-label, .period-text {}
        @media all and (max-width: 748px) {
            .borderright {
                border: none !important;
            }
            .filter-container {
                margin-top:10px;
            }
            .rank-label, .ship-type-label, .period-label {
                width: auto !important;
            }

            .rank-text, .ship-type-text, .period-text {
                float: none !important;
            }

        }

        @media all and (max-width: 992px) {
            .paddingleftzero {
                padding-left: 5px !important;
            }
        }

        .show_more_main {
            margin: 10px auto;
            text-align: center;
            padding: 20px;
            display: block;
            width: 100%;
        }

        .show_more {
            font-weight: bold;
            font-size: 16px;
            margin-top: 20px;
        }
        #top-rated-sailing-contracts td {
            padding: 16px 0 0 0;
        }
    </style>
@stop
@section('content')

    <div class="my-review" style="padding: 55px 0 80px 0;">
        <h4 class="text-center h4 heading" style="margin-top:15px;">My OnBoard Experience</h4>
        <div class="container" style="margin-top: 30px;">
            @if(!empty($firstShipName) && $isData == 1)
                <div class="col-md-4 col-sm-12 col-xs-12 paddingleftzero" style="padding-right: 0px; float: right;">

                    <div class="col-md-12  col-sm-12 col-xs-12"
                        style="background-color: #ffffff;padding: 0px;margin-top: 10px;border: 1px solid #b7b7b7;">
                        <div class="col-md-12  col-sm-12 col-xs-12"
                            style="padding: 10px 0px;    text-align: center;    background: #ededed;">
                            My Top Rated Sailing Contracts
                        </div>
                        <div class="col-md-12  col-sm-12 col-xs-12">
                            <table id="top-rated-sailing-contracts" style="width: 100%; margin:15px 0px;">
                                <tr>
                                    <th>Vessel</th>
                                    <th>Sailing Days</th>
                                    <th>My Rating</th>
                                </tr>
                                @php $i = 1; @endphp
                                @if (!empty($sailingContracts))
                                    @foreach ($sailingContracts as $row)
                                        <tr class="{{$i > 5 ? 'contract-sailing' : ''}}" id="row_{{ $i }}"
                                            style="{{ $i > 5 ? 'display:none' : '' }}">
                                            <td>{{ $i }}. {{ $row['name'] }} </td>
                                            <td>{{ $row['total_duration'] }}</td>
                                            <td>
                                                <span class="score">

                                                    <div class="score-wrap" style="float: left;">
                                                        <span class="getclass scorecontract-rating{{ $row['id'] }}"
                                                            data-id="{{ $row['id'] }}" data-type="contract-rating"
                                                            data-score="{{ $row['average'] ?? 0.0 }}">
                                                    </div>
                                                </span>
                                            </td>
                                        </tr>
                                        @php $i++; @endphp
                                    @endforeach
                                @endif

                            </table>
                            @if (count($shipName) > 5)
                                <a href="javascript:;" class="pull-right view-all-contract-rating">View All</a>
                                <a href="javascript:;" style="display: none" class="pull-right view-less-contract-rating">View Less</a>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-12  col-sm-12 col-xs-12" style="padding:0px;">
                        <table style="width: 100%; margin:5px 0px;">
                            <tr>
                                <td style="color: blue; font-size: 11px;">My Rating is the avg of rating given to the vessel,
                                    Owner and Manning company.</td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="col-md-8 col-sm-12 col-xs-12" style="padding: 0px;">
                   <div class="row" style="margin-left: 5px;margin-top: 10px;" >
                      <div class="col-md-3 filter-container" style="padding-left:0;">
                         <h5>View Listing By: </h5>
                      </div>
                      <div class="col-md-4 filter-container" style="padding-left:0">
                         <select class="form-control" name="rank" id="rank">
                            <option value=""> - Select Rank - </option>
                            @foreach($optionRanks as $key => $optionRank)
                               <option value="{{$optionRank['id']}}">{{$optionRank['value']}}</option>
                            @endforeach
                         </select>
                      </div>
                      <div class="col-md-4 filter-container" style="padding-left:0">
                         <select class="form-control" name="ship_type" id="ship_type">
                            <option value=""> - Select Ship Type - </option>
                            @foreach($optionShipTypes as $key => $shipType)
                               <option value="{{$shipType['id']}}">{{$shipType['value']}}</option>
                            @endforeach
                         </select>
                      </div>
                   </div>
                   <div id="no-record" class="row text-center" style="margin-left: 5px;margin-top: 10px;display:none" >
                      No records found...
                   </div>
                    @if (!empty($shipName))
                        @php
                            $i = 1;
                            $lastId = 0;
                        @endphp
                        <div id="show-data-list">
                            @foreach ($shipName as $row)
                                @if ($i <= 5)
                                    {{-- @include('admin.rate_review.showdata-item',['row'=> $row]) --}}
                                    @php $lastId = $row->id @endphp
                                @endif
                                @php $i++; @endphp
                            @endforeach
                        </div>
                        @if (count($shipName) > 5)
                            <br />
                            {{-- <div class="show_more_main" id="show_more_main{{ $lastId }}">
                                <button id="{{ $lastId }}" class="show_more btn btn-link" title="Load more data">Load more</button>
                                <span class="loding" style="display: none;"><i class="fa fa-spin fa-spinner"></i></span>
                            </div> --}}
                        @endif
                    @endif

                </div>
            @else
                @if(\Route::getCurrentRoute()->getPath() == "share-experience-onboard")
                    <div class="col-md-12">
                        <img class="img-responsive center-block graph-img" src="{{ URL:: asset('public/images/no_feedback_or_review_given.png')}}">
                    </div>
                @else
                    <div class="mt-30 panel panel-bricky">
                        <div class="panel-heading text-center">
                            <h4 class="h4">
                                <h4 class="h4">Add <a href="{{URL('rate-review')}}">onboard feedback</a> to activate this feature.</h4>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <img class="img-responsive center-block graph-img" src="{{ URL:: asset('public/images/onboard_experience.png')}}">
                    </div>
                @endif
            @endif    
        </div>
    </div>
@stop
@section('js_script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.3.2/jquery.rateyo.min.js"></script>
    <script type="text/javascript"></script>

    <script type="text/javascript">
        function loadStars() {
            $(".getclass").each(function() {
                var id = $(this).data("id");
                var type = $(this).data("type");
                var score = $(this).data("score");

                $(".score" + type + id).rateYo({
                    rating: score,
                    readOnly: true,
                    starWidth: "18px"
                });
            });
        }
        $(document).ready(function() {
            loadMoreData('', '');
            $(document).on('click', '.show_more', function() {
                var ID = $(this).attr('id');
                loadMoreData(ID, true);
            });
        });
        $('#rank').on('change', function() {
            var ID = $('.show_more').attr('id');
            loadMoreData(ID, false);
        });
        $('#ship_type').on('change', function() {
            var ID = $('.show_more').attr('id');
            loadMoreData(ID, false);
        })

        function loadMoreData(id, isLoadMore) {
            if (isLoadMore) {
                $('.show_more').hide();
                $('.loding').show();
            } else {
               $("#show-data-list").html("<div class='text-center'><i class='fa fa-spin fa-spinner'></i></div>")
            }

            $.ajax({
                type: 'GET',
                url: '{{ route('experienceShowDataLoadMore') }}',
                datatype: "json",
                data: {
                    id: id,
                    rank_id: $("#rank").val(),
                    ship_type: $("#ship_type").val(),
                    share_experience_onboard: '{{$userIdEncodeKey}}'
                },
                success: function(response) {
                   $('#load_more_main' + id).remove();
                    if (isLoadMore) {
                        $('#show-data-list').append(response.html);
                        $('.show_more').show();
                        $('.loding').hide();
                    } else {
                        $('#show-data-list').html(response.html);
                    }
                    if(response.totalRecords == 0) {
                        $("#no-record").show();
                    } else {
                        $("#no-record").hide();
                    }
                    loadStars();
                }
            });
        }
        $(function() {
            loadStars();
            $('#show-data-list').on("click", ".clickon", function() {
                var type = $(this).data('type');
                var id = $(this).data('id');

                $('.jobprofile' + id).css('display', 'none');
                $('.owner' + id).css('display', 'none');
                $('.manning' + id).css('display', 'none');
                $('.vessel' + id).css('display', 'none');
                $('.' + type + id).css('display', 'block');
                $('.btns' + id).removeClass('active-button');
                $(this).addClass('active-button');
            })
            $('.view-all-contract-rating').on("click", function() {
                $('.contract-sailing').css('display', '');
                $(this).hide();
                $('.view-less-contract-rating').show();
            })
            $('.view-less-contract-rating').on("click", function() {
                $('.contract-sailing').css('display', 'none');
                $(this).hide();
                $('.view-all-contract-rating').show();
            })
        })

    </script>
@stop
