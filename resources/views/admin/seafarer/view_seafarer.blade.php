@extends('admin.index')

@section('content')

<?php
	$india_value = array_search('India',\CommonHelper::countries());
	if(isset($data[0]['professional_detail']['current_rank']))
		$required_fields = \CommonHelper::rank_required_fields()[$data[0]['professional_detail']['current_rank']];
	
?>

<div id="view-company-page">

	<input type="hidden" name="india_value" value="{{$india_value}}" required>

	<div class="container main-content">
		<div class="toolbar row">
			<div class="col-sm-6 hidden-xs">
				<div class="page-header">
					<h1>Seafarer <small>View Seafarer Details</small></h1>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-md-12 m_t_25">
				<div class="tabbable">
					<ul id="myTab2" class="nav nav-tabs">
						<li class="active">
							<a href="#basic_details" data-toggle="tab">
								Basic Details
							</a>
						</li>
						<li>
							<a href="#imp_documents" data-toggle="tab">
								Important Documents
							</a>
						</li>
						<li>
							<a href="#sea_service_details" data-toggle="tab">
								Sea Service Details
							</a>
						</li>
						<li>
							<a href="#certificate_details" data-toggle="tab">
								Certificate Details
							</a>
						</li>
						<li>
                            <a href="#seafarer_subscription" data-url="{{route('admin.get.user.subscription',['page' => 1,'role' => 'seafarer','seafarer_id' => isset($data[0]['id']) ? $data[0]['id'] : ''])}}">
                                Subscription
                            </a>
                        </li>
                        <li>
							<a href="#uploaded_documents" data-toggle="tab">
								Uploaded Documents
							</a>
						</li>
						<li class="pull-right">
							<a type="button" class="fa fa-edit" href="{{Route('admin.edit.seafarer',$data[0]['id'])}}" seafarer-index="{{$data[0]['id']}}">
								Edit
							</a>
						</li>
						<li class="pull-right">
							<a type="button" class="fa fa-download" href="{{route('site.company.candidate.download-cv',$data[0]['id'])}}">
								Download CV
							</a>
						</li>
					</ul>
					<div class="tab-content panel-body">

						<?php
                            $image_path = '';
                            if(isset($data[0]['profile_pic'])){
                                $image_path = env('SEAFARER_PROFILE_PATH')."".$data[0]['id']."/".$data[0]['profile_pic'];
                            }
                        ?>
						<div class="tab-pane fade in active content" id="basic_details">
							<div class="row text-center" style="display: flex;justify-content: center;">
								<div class="col-lg-3 col-md-5 col-sm-5 company_profile_pic" style="border-radius: 50%;">

								@if(isset($image_path) && !empty($image_path))
	                				<img class="seafarer_profile_pic" src="/{{ isset($image_path) ? $image_path : 'images/user_default_image.png'}}" >
	                			@else
	                				<img class="seafarer_profile_pic" src="/images/user_default_image.png" >
	                			@endif

								</div>
							</div>
							<div class="panel-heading">
								<h4 class="panel-title">ACCOUNT CREDENTIALS</h4>
							</div>

							<div class="row">

								<label class="col-sm-3 control-label">
									Full Name:
								</label>
								<div class="col-sm-3">
									{{ isset($data[0]['first_name']) ? $data[0]['first_name'] : ''}}
								</div>
								<label class="col-sm-3 control-label">
									Email: 
									@if($data[0]['is_email_verified'] == '1')
										<i class="fa fa-check-circle green f-15" aria-hidden="true" title="Email Verified"></i>
									@else
										<i class="fa fa-minus-circle red f-15" aria-hidden="true" title="Email Unverified"></i>
									@endif
								</label>
	                           	<div class="col-sm-3">
	                           		{{ isset($data[0]['email']) ? $data[0]['email'] : ''}}
	                            </div>
							</div>

							<div class="row">
								<label class="col-sm-3 control-label">
									Date Of Birth:
								</label>
								<div class="col-sm-3">
									{{ isset($data[0]['personal_detail']['dob'])? date('d-m-Y',strtotime($data[0]['personal_detail']['dob'])) : '-' }}
								</div>
							
							
								<label class="col-sm-3 control-label">
									Gender: 
								</label>
								<div class="col-sm-3">
									{{ !empty($data[0]['gender']) ?  $data[0]['gender'] == 'M'? 'Male' : 'Female' : 'Male' }}
								</div>
							</div>

							<div class="row">
								<label class="col-sm-3 control-label">
									Place Of Birth: 
								</label>
								<div class="col-sm-3">
									{{ isset($data[0]['personal_detail']['place_of_birth'])? $data[0]['personal_detail']['place_of_birth'] : '-' }}
								</div>
							
								<label class="col-sm-3 control-label">
									Marital Status: 
								</label>
								<div class="col-sm-3">
									@foreach(\CommonHelper::marital_status() as $m_index => $status)
                                       {{ !empty($data[0]['personal_detail']['marital_status']) ? $data[0]['personal_detail']['marital_status'] == $m_index ? $status : '' : '-'}}
                                	@endforeach
								</div>
							</div>

							<div class="row">
								<label class="col-sm-3 control-label">
									Height: 
								</label>
								<div class="col-sm-3">
									{{ isset($data[0]['personal_detail']['height'])? $data[0]['personal_detail']['height'] : '-' }} Cms
								</div>
							
								<label class="col-sm-3 control-label">
									Weight: 
								</label>
								<div class="col-sm-3">
									{{ isset($data[0]['personal_detail']['weight'])? $data[0]['personal_detail']['weight'] : '-' }} Kg
								</div>
							</div>

							<div class="row">
								<label class="col-sm-3 control-label">
									Mobile: 
									@if($data[0]['is_mob_verified'] == '1')
					     				<i class="fa fa-check-circle green f-15" aria-hidden="true" title="Mobile Unverified"></i>
					     			@else
										<i class="fa fa-minus-circle red f-15" aria-hidden="true" title="Mobile Unverified"></i>
					     			@endif
								</label>
								<div class="col-sm-3">
									{{ isset($data[0]['mobile'])? $data[0]['mobile'] : '-' }}
								</div>
							</div>

							<div class="row">
							
								<label class="col-sm-3 control-label">
									Last Wages Drawn:
								</label>
								<div class="col-sm-3">
									{{isset($data['professional_detail']['currency']) ? $data['professional_detail']['currency'] == 'dollar' ? '$' : 'Rs' : 'Rs'}}
									{{ isset($data[0]['professional_detail']['last_salary'])? $data[0]['professional_detail']['last_salary'] : '' }} <b>Per Month</b>
								</div>
								<label class="col-sm-3 control-label">
									Nearest Airport/Railway Station: 
								</label>
								<div class="col-sm-3">
									{{ !empty($data[0]['personal_detail']['nearest_place'])? $data[0]['personal_detail']['nearest_place'] : '-' }}
								</div>
							</div>

							<div class="panel-heading">
								<h4 class="panel-title">NEXT OF KIN</h4>
							</div>

							<div class="row">
								<label class="col-sm-3 control-label">
									Kin Name:
								</label>
								<div class="col-sm-3">
									{{ isset($data[0]['personal_detail']['kin_name'])? $data[0]['personal_detail']['kin_name'] : '' }}
								</div>
							
								<label class="col-sm-3 control-label">
									Kin Relation:
								</label>
								<div class="col-sm-3">
									{{ isset($data[0]['personal_detail']['kin_relation'])? $data[0]['personal_detail']['kin_relation'] : '' }}
								</div>
							</div>

							<div class="row">
								<label class="col-sm-3 control-label">
									Kin Phone Number:
								</label>
								<div class="col-sm-3">
									{{ isset($data[0]['personal_detail']['kin_number'])? $data[0]['personal_detail']['kin_number'] : '-' }}
								</div>

								<label class="col-sm-3 control-label">
									Alternate Number:
								</label>
								<div class="col-sm-3">
								{{ isset($data[0]['personal_detail']['kin_alternate_no'])? $data[0]['personal_detail']['kin_alternate_no'] : '-'}}
								</div>
							</div>

							<div class="panel-heading">
								<h4 class="panel-title">LOCATION DETAILS</h4>
							</div>

							 <div class="row">
								<label class="col-sm-3 control-label">
									Country:
								</label>
								<div class="col-sm-3">
									@foreach( \CommonHelper::countries() as $c_index => $country)
                                        {{ isset($data[0]['personal_detail']['country']) ? $data[0]['personal_detail']['country'] == $c_index ? $country : '' : ''}}
                                    @endforeach
								</div>
							
								<label class="col-sm-3 control-label">
									State:
								</label>
								<div class="col-sm-3">
									{{ isset($data[0]['personal_detail']['state_id']) ? $data[0]['personal_detail']['pincode']['pincodes_states'][0]['state']['name'] : (isset($data[0]['personal_detail']['state_text']) ? $data[0]['personal_detail']['state_text'] : '')}}
								</div>
							</div>

							<div class="row">
								<label class="col-sm-3 control-label">
									City:
								</label>
								<div class="col-sm-3">
									{{ isset($data[0]['personal_detail']['city_id']) ? $data[0]['personal_detail']['pincode']['pincodes_cities'][0]['city']['name'] : (isset($data[0]['personal_detail']['city_text']) ? $data[0]['personal_detail']['city_text'] : '')}}
								</div>
							
								<label class="col-sm-3 control-label">
									Pin Code:
								</label>
								<div class="col-sm-3">
									{{ isset($data[0]['personal_detail']['pincode_text']) ? $data[0]['personal_detail']['pincode_text'] : ''}}
								</div>
							</div>

							<div class="panel-heading">
								<h4 class="panel-title">PROFFESSIONAL DETAILS</h4>
							</div>

							<div class="row">
								<label class="col-sm-3 control-label">
									Current Rank:
								</label>
								<div class="col-sm-3">
									@foreach(\CommonHelper::new_rank() as $index => $category)
								    	@foreach($category as $r_index => $rank)
	                                    	{{ !empty($data[0]['professional_detail']['current_rank']) ? $data[0]['professional_detail']['current_rank'] == $r_index ? $rank : '' : ''}}
	                                    @endforeach
	                                @endforeach
								</div>
							
								<label class="col-sm-3 control-label">
									Current Rank Experience:
								</label>
								<div class="col-sm-3">
									{{ isset($data[0]['professional_detail']['years']) ? $data[0]['professional_detail']['years'] : '0'}} Years {{ isset($data[0]['professional_detail']['months']) && ($data[0]['professional_detail']['months'] != '') ? $data[0]['professional_detail']['months'] : '0' }} Months
								</div>
							</div>

							<div class="row">
								<label class="col-sm-3 control-label">
									Rank Applied For:
								</label>
								<div class="col-sm-3">

									@foreach(\CommonHelper::new_rank() as $index => $category)
								    	@foreach($category as $r_index => $rank)
	                                    	{{ !empty($data[0]['professional_detail']['applied_rank']) ? $data[0]['professional_detail']['applied_rank'] == $r_index ? $rank : '' : ''}}
	                                    @endforeach
	                                @endforeach
								</div>
							
								<label class="col-sm-3 control-label">
									Date of Availability:
								</label>
								<div class="col-sm-3">
									{{ isset($data[0]['professional_detail']['availability']) ? date('d-m-Y',strtotime($data[0]['professional_detail']['availability'])) : ''}}
								</div>
							</div>
			
						</div>
					
						<div class="tab-pane fade" id="imp_documents">

							<div class="row">
								<div class="col-sm-12">
									<div class="panel-heading">
										<h4 class="panel-title">PASSPORT DETAILS</h4>
									</div>
									<div class="sub-details-container">
										<div class="content-container">
											<div class="row">
												<div class="col-xs-12 col-sm-6 col-md-3">
													<div class="discription">
														<label class="control-label">Passport Country:</label>
														<span class="content">
														@foreach( \CommonHelper::countries() as $c_index => $country)
				                                            {{ isset($data[0]['passport_detail']['pass_country']) ? $data[0]['passport_detail']['pass_country'] == $c_index ? $country : '' : ''}}
				                                        @endforeach
														</span>
													</div>
												</div>
												<div class="col-xs-12 col-sm-6 col-md-3">
													<div class="discription">
														<label class="control-label">Passport Number:</label>
														<span class="content">{{ isset($data[0]['passport_detail']['pass_number']) ? $data[0]['passport_detail']['pass_number'] : '' }}</span>
													</div>
												</div>
												<div class="col-xs-12 col-sm-6 col-md-3">
													<div class="discription">
														<label class="control-label">Place Of Issue:</label>
														<span class="content">{{ isset($data[0]['passport_detail']['place_of_issue']) ? $data[0]['passport_detail']['place_of_issue'] : '' }}</span>
													</div>
												</div>
												<div class="col-xs-12 col-sm-6 col-md-3">
													<div class="discription">
														<label class="control-label">Expiry Date:</label>
														<span class="content">{{ isset($data[0]['passport_detail']['pass_expiry_date'])? date('d-m-Y',strtotime($data[0]['passport_detail']['pass_expiry_date'])) : '' }}</span>
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-xs-12 col-sm-6 col-md-3">
													<div class="discription">
														<label class="control-label">US Visa:</label>
														<span class="content">
															{{ isset($data[0]['passport_detail']['us_visa']) ? $data[0]['passport_detail']['us_visa'] == 1 ? 'Yes' : 'No' : 'No'}}
														</span>
													</div>
												</div>
												@if(isset($data[0]['passport_detail']['us_visa']) && ($data[0]['passport_detail']['us_visa'] == 1))
												<div class="col-xs-12 col-sm-6 col-md-3">
													<div class="discription">
														<label class="control-label">Expiry Date:</label>
														<span class="content">
															{{ isset($data[0]['passport_detail']['us_visa_expiry_date']) ? date('d-m-Y',strtotime($data[0]['passport_detail']['us_visa_expiry_date'])) : ''}}
														</span>
													</div>
												</div>
												@endif
												<div class="col-xs-12 col-sm-6 col-md-3">
													<div class="discription">
														<label class="control-label">Indian PCC:</label>
														<span class="content">
															{{ isset($data[0]['passport_detail']['indian_pcc']) ? $data[0]['passport_detail']['indian_pcc'] == '1'? 'Yes' : 'No' : 'No'}}
														</span>
													</div>
												</div>

												@if(isset($data[0]['passport_detail']['indian_pcc']) && ($data[0]['passport_detail']['indian_pcc'] == 1))
													<div class="col-xs-12 col-sm-6 col-md-3">
														<div class="discription">
															<label class="control-label">Indian PCC Issue Date:</label>
															<span class="content">
																{{ isset($data[0]['passport_detail']['indian_pcc_issue_date']) ? date('d-m-Y',strtotime($data[0]['passport_detail']['indian_pcc_issue_date'])) : ''}}
															</span>
														</div>
													</div>
												@endif

												
											</div>
											<div class="row">
												<div class="col-xs-12 col-sm-6 col-md-3">
													<div class="discription">
														<label class="control-label">INDOS Number:</label>
														<span class="content">
															{{ isset($data[0]['wkfr_detail']['indos_number']) && !empty($data[0]['wkfr_detail']['indos_number']) ? $data[0]['wkfr_detail']['indos_number'] : '-'}}
														</span>
													</div>
												</div>
												<div class="col-xs-12 col-sm-6 col-md-3">
													<div class="discription">
														<label class="control-label">Yellow Fever Vaccination:</label>
														<span class="content">
															{{ isset($data[0]['wkfr_detail']['yellow_fever']) ? $data[0]['wkfr_detail']['yellow_fever'] == '1'? 'Yes' : 'No' : 'NO' }}
														</span>
													</div>
												</div>

												@if(isset($data[0]['wkfr_detail']['yellow_fever']) && ($data[0]['wkfr_detail']['yellow_fever'] == 1))
													<div class="col-xs-12 col-sm-6 col-md-6">
														<div class="discription">
															<label class="control-label">Yellow Fever Issue Date:</label>
															<span class="content">
																{{ isset($data[0]['wkfr_detail']['yf_issue_date']) ? date('d-m-Y',strtotime($data[0]['wkfr_detail']['yf_issue_date'])) : ''}}
															</span>
														</div>
													</div>
												@endif

												@if($data[0]['passport_detail']['fromo'] == 1 || ($data[0]['passport_detail']['fromo'] == 0))
													<div class="col-xs-12 col-sm-6 col-md-3">
														<div class="discription">
															<label class="control-label">Experience Of Framo:</label>
															<span class="content">
																{{ isset($data[0]['passport_detail']['fromo']) ? $data[0]['passport_detail']['fromo'] == '1'? 'Yes' : 'No' : 'NO' }}
															</span>
														</div>
													</div>
												@endif
											</div>
											<hr>
										</div>
									</div>
								</div>
							</div>
							@if(isset($data[0]['seaman_book_detail']) AND !empty($data[0]['seaman_book_detail']))
							<div class="row">
								<div class="col-sm-12">
									<div class="sub-details-container">
										<div class="content-container">
												<div class="panel-heading">
													<h4 class="panel-title">SEAMAN BOOK DETAILS</h4>
												</div>
											
												@foreach($data[0]['seaman_book_detail'] as $index => $cdc_data)
													<div class="discription">
														<span class="content-head">CDC Details {{$index+1}}
															@if($cdc_data['status'] == '1')
											     				<i class="fa fa-check-circle green f-15" aria-hidden="true" title="CDC Verified"></i>
											     			@endif
														</span>
													</div>
													<div class="row">
														<div class="col-xs-12 col-sm-6 col-md-3">
															<div class="discription">
																<label class="control-label">CDC:</label>
																<span class="content">
																	@foreach( \CommonHelper::countries() as $c_index => $country)
							                                            {{ isset($cdc_data['cdc']) ? $cdc_data['cdc'] == $c_index ? $country : '' : ''}}
							                                        @endforeach
						                                        </span>
															</div>
														</div>
														<div class="col-xs-12 col-sm-6 col-md-3">
															<div class="discription">
																<label class="control-label">Number:</label>
																<span class="content">
																	{{isset($cdc_data['cdc_number']) ? $cdc_data['cdc_number'] : ''}}
																</span>
															</div>
														</div>
														<div class="col-xs-12 col-sm-6 col-md-3">
															<div class="discription">
																<label class="control-label">Issue Date:</label>
																<span class="content">
																	{{isset($cdc_data['cdc_issue_date']) ? date('d-m-Y',strtotime($cdc_data['cdc_issue_date'])) : ''}}
																</span>
															</div>
														</div>
														<div class="col-xs-12 col-sm-6 col-md-3">
															<div class="discription">
																<label class="control-label">Expiry Date:</label>
																<span class="content">
																	{{isset($cdc_data['cdc_expiry_date']) ? date('d-m-Y',strtotime($cdc_data['cdc_expiry_date'])) : ''}}
																</span>
															</div>
														</div>
													</div>
													<hr>
												@endforeach
										</div>
									</div>
								</div>
							</div>
							@endif

							@if(isset($data[0]['cop_detail']['cop_number']) AND !empty($data[0]['cop_detail']['cop_number']))
							<div class="row">
								<div class="col-sm-12">
									<div class="sub-details-container">
										<div class="content-container">
												<div class="panel-heading">
													<h4 class="panel-title">CERTIFICATE OF PROFICIENCY</h4>
												</div>
												@foreach($data[0]['cop_detail'] as $index => $cop_data)
													<div class="discription">
														<span class="content-head">COP Details {{$index+1}}</span>
													</div>
													<div class="row">
														<div class="col-xs-12 col-sm-6 col-md-3">
															<div class="discription">
																<label class="control-label">Number:</label>
																<span class="content">
																	{{ isset($cop_data['cop_number']) ? $cop_data['cop_number'] : ''}}
																</span>
															</div>
														</div>
														<div class="col-xs-12 col-sm-6 col-md-3">
															<div class="discription">
																<label class="control-label">Grade:</label>
																<span class="content">
																	{{ isset($cop_data['cop_grade']) ? $cop_data['cop_grade'] : '' }}
																</span>
															</div>
														</div>
														<div class="col-xs-12 col-sm-6 col-md-3">
															<div class="discription">
																<label class="control-label">Issue Date:</label>
																<span class="content">
																	{{ isset($cop_data['cop_issue_date']) ? date('d-m-Y',strtotime($cop_data['cop_issue_date'])) : '' }}
																</span>
															</div>
														</div>
														<div class="col-xs-12 col-sm-6 col-md-3">
															<div class="discription">
																<label class="control-label">Expiry Date:</label>
																<span class="content">
																	{{ isset($cop_data['cop_exp_date']) ? date('d-m-Y',strtotime($cop_data['cop_exp_date'])) : '' }}
																</span>
															</div>
														</div>
													</div>
													<hr>
												@endforeach
										</div>
									</div>
								</div>
							</div>
							@endif
							
							@if(isset($data[0]['coc_detail']) AND !empty($data[0]['coc_detail']) AND (in_array('COC',$required_fields) OR in_array('COC-Optional',$required_fields)))
							<div class="row">
								<div class="col-sm-12">
									<div class="sub-details-container">
										<div class="content-container">
											<div class="row">
												<div class="col-sm-12">
													<div class="title">CERTIFICATE OF COMPENTENCY</div>
												</div>
											</div>
												@foreach($data[0]['coc_detail'] as $index => $coc_data)
													<div class="discription">
														<span class="content-head">COC Details {{$index+1}}
															@if($coc_data['status'] == '1')
											     				<i class="fa fa-check-circle green f-15" aria-hidden="true" title="COC Verified"></i>
											     			@endif
														</span>
													</div>
													<div class="row">
														<div class="col-xs-12 col-sm-6 col-md-2">
															<div class="discription">
																<label class="control-label">COC:</label>
																<span class="content">
																	@foreach( \CommonHelper::countries() as $c_index => $country)
						                                                {{ isset($coc_data['coc']) ? $coc_data['coc'] == $c_index ? $country : '' : ''}}
						                                            @endforeach
						                                        </span>
															</div>
														</div>
														<div class="col-xs-12 col-sm-6 col-md-2">
															<div class="discription">
																<label class="control-label">Number:</label>
																<span class="content">
																	{{ (isset($coc_data['coc_number']) AND !empty($coc_data['coc_number'])) ? $coc_data['coc_number'] : '-'}}
																</span>
															</div>
														</div>
														<div class="col-xs-12 col-sm-6 col-md-2">
															<div class="discription">
																<label class="control-label">Grade:</label>
																<span class="content">
																	{{ (isset($coc_data['coc_grade']) AND !empty($coc_data['coc_grade'])) ? $coc_data['coc_grade'] : '-' }}
																</span>
															</div>
														</div>
														<div class="col-xs-12 col-sm-6 col-md-3">
															<div class="discription">
																<label class="control-label">Verification Date:</label>
																	<span class="content">
																	{{ !is_null($coc_data['coc_verification_date']) ? date('d-m-Y',strtotime($coc_data['coc_verification_date'])) : '-' }}
																</span>
															</div>
														</div>
														<div class="col-xs-12 col-sm-6 col-md-3">
															<div class="discription">
																<label class="control-label">Expiry Date:</label>
																<span class="content">
																	{{ (isset($coc_data['coc_expiry_date']) AND !empty($coc_data['coc_expiry_date'])) ? date('d-m-Y',strtotime($coc_data['coc_expiry_date'])) : '-' }}
																</span>
															</div>
														</div>
													</div>
													<hr>
												@endforeach
										</div>
									</div>
								</div>
							</div>
							@endif

							@if(isset($data[0]['coe_detail']) AND !empty($data[0]['coe_detail']) && (in_array('COE',$required_fields) OR in_array('COE-Optional',$required_fields)))
							<div class="row">
								<div class="col-sm-12">
									<div class="sub-details-container">
										<div class="content-container">
											<div class="row">
												<div class="col-sm-12">
													<div class="title">CERTIFICATE OF ENDORSEMENT</div>
												</div>
											</div>
												@foreach($data[0]['coe_detail'] as $index => $coe_data)
													<div class="discription">
														<span class="content-head">COE Details {{$index+1}}</span>
													</div>
													<div class="row">
														<div class="col-xs-12 col-sm-6 col-md-2">
															<div class="discription">
																<label class="control-label">COE:</label>
																<span class="content">
																	@foreach( \CommonHelper::countries() as $c_index => $country)
						                                                {{ isset($coe_data['coe']) ? $coe_data['coe'] == $c_index ? $country : '' : ''}}
						                                            @endforeach
						                                        </span>
															</div>
														</div>
														<div class="col-xs-12 col-sm-6 col-md-2">
															<div class="discription">
																<label class="control-label">Number:</label>
																<span class="content">
																	{{ (isset($coe_data['coe_number']) AND !empty($coe_data['coe_number'])) ? $coe_data['coe_number'] : '-'}}
																</span>
															</div>
														</div>
														<div class="col-xs-12 col-sm-6 col-md-2">
															<div class="discription">
																<label class="control-label">Grade:</label>
																<span class="content">
																	{{ (isset($coe_data['coe_grade']) AND !empty($coe_data['coe_grade'])) ? $coe_data['coe_grade'] : '-' }}
																</span>
															</div>
														</div>
														<div class="col-xs-12 col-sm-6 col-md-3">
															<div class="discription">
																<label class="control-label">Verification Date:</label>
																	<span class="content">
																	{{ !is_null($coe_data['coe_verification_date']) ? date('d-m-Y',strtotime($coe_data['coe_verification_date'])) : '-' }}
																</span>
															</div>
														</div>
														<div class="col-xs-12 col-sm-6 col-md-3">
															<div class="discription">
																<label class="control-label">Expiry Date:</label>
																<span class="content">
																	{{ (isset($coe_data['coe_expiry_date']) AND !empty($coe_data['coe_expiry_date'])) ? date('d-m-Y',strtotime($coe_data['coe_expiry_date'])) : '-' }}
																</span>
															</div>
														</div>
													</div>
													<hr>
												@endforeach
										</div>
									</div>
								</div>
							</div>
							@endif
							
							@if(isset($data[0]['gmdss_detail']) AND !empty($data[0]['gmdss_detail']) && (in_array('GMDSS',$required_fields) OR in_array('GMDSS-Optional',$required_fields)) && 
							((!empty($data[0]['gmdss_detail']['gmdss'])) || (!empty($data[0]['gmdss_detail']['gmdss_number'])) || (!empty($data[0]['gmdss_detail']['gmdss_expiry_date']))))
								<div class="row">
									<div class="col-sm-12">
										<div class="sub-details-container">
											<div class="content-container">
												<div class="panel-heading">
													<h4 class="panel-title">GMDSS DETAILS</h4>
												</div>
												<div class="row">
													<div class="col-xs-12 col-sm-6 col-md-4">
														<div class="discription">
															<label class="control-label">GMDSS:</label>
															<span class="content">
																@foreach( \CommonHelper::countries() as $c_index => $country)
					                                                {{ isset($data[0]['gmdss_detail']['gmdss']) ? $data[0]['gmdss_detail']['gmdss'] == $c_index ? $country : '' : ''}}
					                                            @endforeach
															</span>
														</div>
													</div>
													<div class="col-xs-12 col-sm-6 col-md-4">
														<div class="discription">
															<label class="control-label">Number:</label>
															<span class="content">
																{{ (isset($data[0]['gmdss_detail']['gmdss_number']) AND !empty($data[0]['gmdss_detail']['gmdss_number'])) ? $data[0]['gmdss_detail']['gmdss_number'] : '-'}}
															</span>
														</div>
													</div>
													<div class="col-xs-12 col-sm-6 col-md-4">
														<div class="discription">
															<label class="control-label">Expiry Date:</label>
															<span class="content">
																{{ (isset($data[0]['gmdss_detail']['gmdss_expiry_date']) AND !empty($data[0]['gmdss_detail']['gmdss_expiry_date'])) ? date('d-m-Y',strtotime($data[0]['gmdss_detail']['gmdss_expiry_date'])) : '-' }}
															</span>
														</div>
													</div>
													<div class="col-xs-12 col-sm-6 col-md-4 gmdss_endorsement {{isset($data[0]['gmdss_detail']['gmdss']) && $data[0]['gmdss_detail']['gmdss'] == $india_value ? '' : 'hide'}}">
														<div class="discription">
															<label class="content-head">GMDSS Endorsement Number:</label>
															<span class="content">
																{{ isset($data[0]['gmdss_detail']['gmdss_endorsement_number']) && !empty($data[0]['gmdss_detail']['gmdss_endorsement_number']) ? $data[0]['gmdss_detail']['gmdss_endorsement_number'] : '-'}}
															</span>
														</div>
													</div>
													<div class="col-xs-12 col-sm-6 col-md-4 gmdss_valid_till {{isset($data[0]['gmdss_detail']['gmdss']) && $data[0]['gmdss_detail']['gmdss'] == $india_value ? '' : 'hide'}}">
														<div class="discription">
															<label class="content-head">Valid Till:</label>
															<span class="content">
																{{ isset($data[0]['gmdss_detail']['gmdss_endorsement_expiry_date']) && !empty($data[0]['gmdss_detail']['gmdss_endorsement_expiry_date']) ? date('d-m-Y',strtotime($data[0]['gmdss_detail']['gmdss_endorsement_expiry_date'])) : '-'}}
															</span>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							@endif
		
							@if(isset($data[0]['wkfr_detail']['watch_keeping']) && !empty($data[0]['wkfr_detail']['watch_keeping']))
								<div class="row">
									<div class="col-sm-12">
										<div class="sub-details-container">
											<div class="content-container">
												<div class="panel-heading">
													<h4 class="panel-title">WATCH KEEPING FOR RATING</h4>
												</div>
												<div class="row">
													<div class="col-xs-12 col-sm-6 col-md-3">
														<div class="discription">
															<label class="control-label">Watch Keeping:</label>
															<span class="content">
																@foreach( \CommonHelper::countries() as $c_index => $country)
					                                                {{ isset($data[0]['wkfr_detail']['watch_keeping']) ? $data[0]['wkfr_detail']['watch_keeping'] == $c_index ? $country : '' : ''}}
					                                            @endforeach
					                                        </span>
														</div>
													</div>
													<div class="col-xs-12 col-sm-6 col-md-3">
														<div class="discription">
															<label class="control-label">Deck / Engine:</label>
															<span class="content">
																{{ isset($data[0]['wkfr_detail']['type']) ? $data[0]['wkfr_detail']['type'] == 'Deck' ? 'Deck' : 'Engine' : 'Engine'}}
															</span>
														</div>
													</div>
													<div class="col-xs-12 col-sm-6 col-md-3">
														<div class="discription">
															<label class="control-label">Number:</label>
															<span class="content">
																{{ isset($data[0]['wkfr_detail']['wkfr_number']) && !empty($data[0]['wkfr_detail']['wkfr_number']) ? $data[0]['wkfr_detail']['wkfr_number'] : '-'}}
															</span>
														</div>
													</div>
													<div class="col-xs-12 col-sm-6 col-md-3">
														<div class="discription">
															<label class="control-label">Issue Date:</label>
															<span class="content">
																{{ isset($data[0]['wkfr_detail']['issue_date']) && !empty($data[0]['wkfr_detail']['issue_date']) ? date('d-m-Y',strtotime($data[0]['wkfr_detail']['issue_date'])) : '-'}}
															</span>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							@endif
						</div>
						<div class="tab-pane fade" id="certificate_details">

							<div class="row">
								<div class="col-sm-12">
									<div class="panel-heading">
										<h4 class="panel-title">ACADEMICS AND PROFESSIONAL QUALIFICATION</h4>
									</div>
									<div class="col-sm-4 p-0">
										<label class="input-label" for="name_of_school">Name of School / College:</label>
										<span class="content">
				                    		{{ isset($data[0]['professional_detail']['name_of_school']) && !empty($data[0]['professional_detail']['name_of_school']) ? $data[0]['professional_detail']['name_of_school'] : '-'}}
				                    	</span>
					                </div>
					                <div class="col-sm-4 p-0">
										<label class="input-label" for="school_to">Pass Out Year:</label>
				                    	<span class="content">
				                    		@if(isset($data[0]['professional_detail']['school_to']) && !empty($data[0]['professional_detail']['school_to']))
												@for($i = '1980' ; $i < 2018 ; $i++)
													{{isset( $data[0]['professional_detail']['school_to']) ? $i == $data[0]['professional_detail']['school_to'] ? $i : '' : ''}}
												@endfor
											@else
												-
											@endif
										</span>
					                </div>
					                <div class="col-sm-4 p-0">
										<label class="input-label" for="school_qualification">Highest Qualification:</label>
				                    	<span class="content">
				                    		{{ isset($data[0]['professional_detail']['school_qualification']) && !empty($data[0]['professional_detail']['school_qualification']) ? $data[0]['professional_detail']['school_qualification'] : '-'}}
				                    	</span>
					                </div>
								</div>
							</div>
							<hr>
							<div class="row">
								<div class="col-sm-12">
									<div class="panel-heading">
										<h4 class="panel-title">PRE SEA TRAINING / APPRENTICE SHIP</h4>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-12">
									<div class="col-sm-4 p-0">
										<label class="input-label" for="institute_name">Name of Institute / College:</label>
										<span class="content">
					                    	{{ isset($data[0]['professional_detail']['institute_name']) &&!empty($data[0]['professional_detail']['institute_name']) ? $data[0]['professional_detail']['institute_name'] : '-'}}
					                    </span>
					                </div>
					                <div class="col-sm-4 p-0">
											<label class="input-label" for="institute_to">Pass Out Year:</label>
					                    	<span class="content">
					                    		@if(isset($data[0]['professional_detail']['institute_to']) && !empty($data[0]['professional_detail']['institute_to']))
													@for($i = '1980' ; $i < 2018 ; $i++)
														{{isset($data[0]['professional_detail']['institute_to']) ? $i == $data[0]['professional_detail']['institute_to'] ? $i : '' : ''}}
													@endfor
												@else
													-
												@endif
											</span>
					                </div>
					                <div class="col-sm-4 p-0">
										<label class="input-label" for="institute_degree">Type of Degree:</label>
										<span class="content">
					                    	{{ isset($data[0]['professional_detail']['institute_degree']) && !empty($data[0]['professional_detail']['institute_degree']) ? $data[0]['professional_detail']['institute_degree'] : '-'}}
					                    </span>
					                </div>
								</div>
							</div>
							<hr>
							@if(isset($data[0]['course_detail']) AND !empty($data[0]['course_detail']))
							<div class="row">
								<div class="col-sm-12">
									<div class="panel-heading">
										<h4 class="panel-title">COURSE DETAILS</h4>
									</div>
									<?php $course = 1; ?>
									<div class="sub-details-container">
										@foreach($data[0]['course_detail'] as $index => $courses)
											<div class="content-container">
												<div class="row">
													<div class="col-sm-12">
														<div class="title">CERTIFICATE {{$course}}</div>
													</div>
												</div>
												<div class="row">
													<div class="col-xs-12 col-sm-6 col-md-4">
														<div class="discription">
															<label class="control-label">Certificate Name:</label>
															<span class="content">
															@foreach(\CommonHelper::courses() as $index => $category)
					                                            {{ isset($courses['course_id']) ? $courses['course_id'] == $index ? $category : '' : ''}}
					                                        @endforeach
															</span>
														</div>
													</div>
													<div class="col-xs-12 col-sm-6 col-md-4">
														<div class="discription">
															<label class="control-label">Certificate Number:</label>
															<span class="content">
																{{ isset($courses['certification_number']) && !empty($courses['certification_number']) ? $courses['certification_number'] : '-'}}
															</span>
														</div>
													</div>
													<div class="col-xs-12 col-sm-6 col-md-4">
														<div class="discription">
															<label class="control-label">Date Of Issue:</label>
															<span class="content">
																{{ isset($courses['issue_date']) && !empty($courses['issue_date']) ? date('d-m-Y',strtotime($courses['issue_date'])) : '-'}}
															</span>
														</div>
													</div>
													<div class="col-xs-12 col-sm-6 col-md-4">
														<div class="discription">
															<label class="control-label">Date Of Expiry:</label>
															<span class="content">
																{{ isset($courses['expiry_date']) && !empty($courses['expiry_date']) ? date('d-m-Y',strtotime($courses['expiry_date'])) : '-'}}
															</span>
														</div>
													</div>
													<div class="col-xs-12 col-sm-6 col-md-4">
														<div class="discription">
															<label class="control-label">Issued By:</label>
															<span class="content">
															{{ isset($courses['issue_by']) && !empty($courses['issue_by']) ? $courses['issue_by'] : '-'}}
															</span>
														</div>
													</div>
													<!-- <div class="col-xs-12 col-sm-6 col-md-4">
														<div class="discription">
															<label class="control-label">Issuing Authority:</label>
															<span class="content">
															@foreach( \CommonHelper::issuing_authority() as $c_index => $authority)
																{{ isset($courses['issuing_authority']) ? $courses['issuing_authority'] == $c_index ? $authority : '' : ''}}
															@endforeach
															</span>
														</div>
													</div> -->
												</div>
												<hr>
											</div>
											<?php $course++; ?>
										@endforeach
									</div>
								</div>
							</div>
							@else
								<div class="">
									No Data Found.
								</div>	
							@endif

							@if(isset($data[0]['value_added_course_detail']) AND !empty($data[0]['value_added_course_detail']))
							<div class="row">
								<div class="col-sm-12">
									<div class="panel-heading">
										<h4 class="panel-title">ADD-ON COURSE DETAILS</h4>
									</div>
									<div class="sub-details-container">
										@foreach($data[0]['value_added_course_detail'] as $index => $courses)
											<div class="content-container">
												<div class="row">
													<div class="col-sm-12">
														<div class="title">CERTIFICATE {{$index+1}}</div>
													</div>
												</div>
												<div class="row">
													<div class="col-xs-12 col-sm-6 col-md-4">
														<div class="discription">
															<label class="control-label">Certificate Name:</label>
															<span class="content">
															@foreach(\CommonHelper::value_added_courses() as $index => $category)
					                                            {{ isset($courses['course_id']) ? $courses['course_id'] == $index ? $category : '' : ''}}
					                                        @endforeach
															</span>
														</div>
													</div>
													<div class="col-xs-12 col-sm-6 col-md-4">
														<div class="discription">
															<label class="control-label">Certificate Number:</label>
															<span class="content">
																{{ isset($courses['certification_number']) ? $courses['certification_number'] : '-'}}
															</span>
														</div>
													</div>
													<div class="col-xs-12 col-sm-6 col-md-4">
														<div class="discription">
															<label class="control-label">Date Of Issue:</label>
															<span class="content">
																{{ isset($courses['issue_date']) ? date('d-m-Y',strtotime($courses['issue_date'])) : '-'}}
															</span>
														</div>
													</div>
													<div class="col-xs-12 col-sm-6 col-md-4">
														<div class="discription">
															<label class="control-label">Date Of Expiry:</label>
															<span class="content">
																{{ isset($courses['expiry_date']) ? date('d-m-Y',strtotime($courses['expiry_date'])) : '-'}}
															</span>
														</div>
													</div>
													<div class="col-xs-12 col-sm-6 col-md-4">
														<div class="discription">
															<label class="control-label">Issued By:</label>
															<span class="content">
															{{ isset($courses['issue_by']) ? $courses['issue_by'] : '-'}}
															</span>
														</div>
													</div>
													<!-- <div class="col-xs-12 col-sm-6 col-md-4">
														<div class="discription">
															<label class="control-label">Issuing Authority:</label>
															<span class="content">
															@foreach( \CommonHelper::issuing_authority() as $c_index => $authority)
																{{ isset($courses['issuing_authority']) ? $courses['issuing_authority'] == $c_index ? $authority : '' : ''}}
															@endforeach
															</span>
														</div>
													</div> -->
												</div>
												<hr>
											</div>
										@endforeach
									</div>
								</div>
							</div>	
							@endif

							@if(!empty($data[0]['professional_detail']['other_exp']))
								<div class="row">
									<div class="col-sm-12">
										<div class="panel-heading">
											<h4 class="panel-title">OTHER EXPERIENCE</h4>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12">
										{{$data[0]['professional_detail']['other_exp']}}
									</div>
								</div>
							@endif
						</div>
						<div class="tab-pane fade" id="sea_service_details">

							@if(isset($data[0]['sea_service_detail']) AND !empty($data[0]['sea_service_detail']))
							<div class="row">
								<div class="col-sm-12">
									<div class="panel-heading">
										<h4 class="panel-title">SEA SERVICE DETAILS</h4>
									</div>
									<div class="sub-details-container">
										@foreach($data[0]['sea_service_detail'] as $index => $services)
											<div class="content-container">
												<div class="row">
													<div class="col-sm-12">
														<div class="title">SERVICE {{$index+1}}</div>
													</div>
												</div>
												<div class="row">
													<div class="col-xs-12 col-sm-6 col-md-4">
														<div class="discription">
															<label class="control-label">Shipping Company:</label>
															<span class="content">
															{{ isset($services['company_name']) ? $services['company_name'] : ''}}
															</span>
														</div>
													</div>
													<div class="col-xs-12 col-sm-6 col-md-4">
														<div class="discription">
															<label class="control-label">Name Of Ship:</label>
															<span class="content">
																{{ isset($services['ship_name']) ? $services['ship_name'] : ''}}
															</span>
														</div>
													</div>
													<div class="col-xs-12 col-sm-6 col-md-4">
														<div class="discription">
															<label class="control-label">Ship Type:</label>
															<span class="content">
															@foreach( \CommonHelper::ship_type() as $c_index => $type)
																{{ isset($services['ship_type']) ? $services['ship_type'] == $c_index ? $type : '' : ''}}
															@endforeach
															</span>
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-xs-12 col-sm-6 col-md-4">
														<div class="discription">
															<label class="control-label">Rank:</label>
															<span class="content">
															@foreach(\CommonHelper::new_rank() as $index => $category)
														    	@foreach($category as $r_index => $rank)
						                                        	{{ isset($services['rank_id']) ? $services['rank_id'] == $r_index ? $rank : '' : ''}}
						                                        @endforeach
						                                    @endforeach
															</span>
														</div>
													</div>
													<div class="col-xs-12 col-sm-6 col-md-4">
														<div class="discription">
															<label class="control-label">GRT:</label>
															<span class="content">
																{{ isset($services['grt']) && !empty($services['grt']) ? $services['grt'] : '-'}}
															</span>
														</div>
													</div>
													<div class="col-xs-12 col-sm-6 col-md-4">
														<div class="discription">
															<label class="control-label">BHP:</label>
															<span class="content">
																{{ isset($services['bhp']) && !empty($services['bhp']) ? $services['bhp'] : '-'}}
															</span>
														</div>
													</div>
												</div>

												<div class="row">
													<div class="col-xs-12 col-sm-6 col-md-4">
														<div class="discription">
															<label class="control-label">Engine Type:</label>
															<span class="content">
															@if(isset($services['engine_type']) && !empty($services['engine_type']))
																@foreach( \CommonHelper::engine_type() as $c_index => $type)
																	{{ isset($services['engine_type']) ? $services['engine_type'] == $c_index ? $type == 'Other' ? $services['other_engine_type']: $type  : '' : ''}}
																@endforeach
															@else
																-
															@endif
														</select>
															</span>
														</div>
													</div>
													<div class="col-xs-12 col-sm-6 col-md-4">
														<div class="discription">
															<label class="control-label">Sign On Date:</label>
															<span class="content">
																{{ isset($services['from']) ? date('d-m-Y',strtotime($services['from'])) : '-'}}
															</span>
														</div>
													</div>
													<div class="col-xs-12 col-sm-6 col-md-4">
														<div class="discription">
															<label class="control-label">Sign Off Date:</label>
															<span class="content">
																{{ isset($services['to']) ? date('d-m-Y',strtotime($services['to'])) : '-'}}
															</span>
														</div>
													</div>
													<div class="col-xs-12 col-sm-6 col-md-4">
														<div class="discription">
															<label class="control-label">Ship Flag:</label>
															<span class="content">
																@if(isset($services['ship_flag']) && !empty($services['ship_flag']))
																	@foreach( \CommonHelper::countries() as $c_index => $name)
																		{{ isset($services['ship_flag']) ? $services['ship_flag'] == $c_index ? $name : '' : ''}}
																	@endforeach
																@else
																	-
																@endif
																</select>
															</span>
														</div>
													</div>
												</div>
												<hr>
											</div>
										@endforeach
									</div>
								</div>
							</div>
							
							@else
								<div class="">
									No Data Found.
								</div>
							@endif
						</div>
						
						<div id="seafarer_subscription" class="tab-pane fade" data-id="{{ isset($data[0]['id']) ? $data[0]['id'] : ''}}" data-role='seafarer' data-seafarer="{{ isset($data[0]['id']) ? $data[0]['id'] : ''}}">
                            <div class="row panel-heading">
                                <div class="col-sm-6">
                                    <h4 class="panel-title">SUBSCRIPTION DETAILS</h4>
                                </div>
                            </div>
                            <div class="full-cnt-loader hidden">
                                <div class="" style="text-align: center;padding-top:20px;padding-bottom:20px; ">
                                    <i class="fa fa-spinner fa-spin fa-4" aria-hidden="true" style="font-size: 7em;"></i>
                                </div>
                            </div>
                            <div class="data">
                                No Subscriptions Found.
                            </div>
                            
                        </div>

                        <div id="uploaded_documents" class="tab-pane fade">
                            <div class="row panel-heading">
                                <div class="col-sm-6">
                                    <h4 class="panel-title">UPLOADED DOCUMENTS</h4>
                                </div>
                            </div>
                            @if(isset($user_documents) && empty($user_documents))
								<div class="row">
									<div class="col-xs-12 text-center">
										<div class="discription">
											<span class="content-head">No Data Found</span>
										</div>
									</div>
								</div>
							@else
								<div class="sub-details-container m-t-0 m-b-0">
	                                @foreach($user_documents as $name => $value)
										<div class="content-container">
											@if(isset($value[0]) && !empty($value[0]))

												<?php
													$docs = $value[0];
													$document_path = route('site.user.get.documents.path',['user_id' => $docs['user_id'],'type' => $name]);
												?>

												<div class="row">
													<div class="col-xs-12 m-t-15 upload-title">
														<div class="title">{{strtoupper($name)}}
														</div>
													</div>
												</div>

												<div class="user_documents_download m-b-15 m-t-15">
													<div class="row">
														<div class="col-xs-12">
															<div class="view_documents">
																@foreach($value as $document)
																	
																	@foreach($document['user_documents'] as $user_docs)
																		<?php
																			$extension = '';
																			if(isset($user_docs['document_path']) && !empty($user_docs['document_path'])){
																				$extension = last(explode('.',$user_docs['document_path']));
																			}

																			if($extension == 'jpg' || $extension == 'jpeg'){
																				$logo = '/images/jpg.jpg';
																			}
																			else if($extension == 'pdf'){
																				$logo = '/images/pdf.png';
																			}
																			else if($extension == 'gif'){
																				$logo = '/images/gif.png';
																			}
																			else if($extension == 'png'){
																				$logo = '/images/png.png';
																			}
																			else if($extension == 'doc' || $extension == 'docx'){
																				$logo = '/images/doc.png';
																			}else {
																				$logo = '/images/file.png';
																			}
																		?>
																		<a href="#">
																			<img src="{{asset($logo)}}" alt="" width="100" height="100">
																		</a>
																	@endforeach
																	
																	@if($document['status'] == '0')
																		<div class="document_type pull-right">Document Type : <b>Private</b></div>
																	@else
																		<div class="document_type pull-right">Document Type : <b>Public</b></div>
																	@endif
																	
																@endforeach
															</div>
														</div>
													</div>
												</div>

											@else

												<?php
													$show_download = true;
													foreach($value as $index => $data){
														$doc_user = $value[$index]['user_id'];
														if($data['status'] == '0' && $show_download){
															$show_download = false;
														}
													}
													$document_path = route('site.user.get.documents.path',['user_id' => $doc_user,'type' => $name]);
												?>

												<div class="row">
													<div class="col-xs-12 m-t-15 upload-title">
														<div class="title">{{strtoupper($name)}}
														</div>
													</div>
												</div>

												@foreach($value as $index => $data)
													<?php
														$type_document_path = route('site.user.get.documents.path',['user_id' => $data['user_id'],'type' => $name,'type_id' => $data['type_id']]);
													?>
													<div class="user_documents_download m-b-15">
														<div class="row">
															<div class="col-xs-12">
																<div class="view_documents">
																	<div class="col-xs-12 m-t-15">
																		<div class="title">{{strtoupper($name)}} {{$index}}</div>
																	</div>
	                                                                
																	@if($data['status'] == '0')
																		<div class="document_type pull-right">Document Type : <b>Private</b></div>
																	@else
																		<div class="document_type pull-right">Document Type : <b>Public</b></div>
																	@endif
																	

                                                                    @foreach($data['user_documents'] as $document)

                                                                    	<?php
																			$extension = '';
																			if(isset($document['document_path']) && !empty($document['document_path'])){
																				$extension = last(explode('.',$document['document_path']));
																			}

																			if($extension == 'jpg' || $extension == 'jpeg'){
												        						$logo = '/images/jpg.jpg';
												        					}
												        					else if($extension == 'pdf'){
												        						$logo = '/images/pdf.png';
												        					}
												        					else if($extension == 'gif'){
												        						$logo = '/images/gif.png';
												        					}
												        					else if($extension == 'png'){
												        						$logo = '/images/png.png';
												        					}
												        					else if($extension == 'doc' || $extension == 'docx'){
												        						$logo = '/images/doc.png';
												        					}else {
												        						$logo = '/images/file.png';
												        					}
												        				?>
                                                                        <a href="#">
                                                                            <img src="{{asset($logo)}}" alt="" width="100" height="100">
                                                                        </a>
                                                                    @endforeach
	                                                                
	                                                            </div>
															</div>
														</div>
													</div>
												@endforeach
											@endif
										</div>
									@endforeach
								</div>
							@endif
                        </div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@stop
@section('js_script')
    <script type="text/javascript" src="/js/site/registration.js"></script>
@stop