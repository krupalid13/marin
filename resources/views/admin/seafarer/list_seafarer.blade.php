@extends('admin.index')

@section('content')

<div class="main-wrapper">
	<div class="main-container1">
				<!-- start: PAGE -->
				<div class="main-content">
					<!-- start: PANEL CONFIGURATION MODAL FORM -->
					<div class="modal fade" id="panel-config" tabindex="-1" role="dialog" aria-hidden="true">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
										&times;
									</button>
									<h4 class="modal-title">Panel Configuration</h4>
								</div>
								<div class="modal-body">
									Here will be a configuration form
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-default" data-dismiss="modal">
										Close
									</button>
									<button type="button" class="btn btn-primary">
										Save changes
									</button>
								</div>
							</div>
							<!-- /.modal-content -->
						</div>
						<!-- /.modal-dialog -->
					</div>
					<!-- /.modal -->
					<!-- end: SPANEL CONFIGURATION MODAL FORM -->
					<div class="container">
						<!-- start: PAGE HEADER -->
						<!-- start: TOOLBAR -->
						<div class="toolbar row">
							<div class="col-sm-6 hidden-xs">
								<div class="page-header">
									<h1>Seafarer <small>A list of all seafarers</small></h1>
								</div>
							</div>
						</div>
						<!-- end: TOOLBAR -->
						<!-- end: PAGE HEADER -->
						<!-- start: BREADCRUMB -->
						<form class="filter-search-form m_l_10">
							<div class="row m_t_25">
								<div class="col-md-12">
									<div class="panel panel-white">
										<div class="panel-body">
											<div class="row">
												<div class="col-sm-3">
					                                <div class="form-group">
					                                    <label>Seafarer Name </label>
					                                    <input type="text" class="form-control" name="first_name" id="first_name" value="{{isset($job_data[0]['first_name']) ? $job_data[0]['first_name'] : ''}}" placeholder="Enter Seafarer Name">
					                                </div>
					                            </div>    
												<div class="col-sm-3">
					                                <div class="form-group">
					                                    <label>Rank </label>
					                                    <select id="rank" name="rank" class="form-control">
					                                        <option value=>Select Rank</option>

					                                        @foreach(\CommonHelper::new_rank() as $rank_index => $category)
					                                            <optgroup label="{{$rank_index}}">
					                                            @foreach($category as $r_index => $rank)
					                                                <option value="{{$r_index}}" {{ isset($job_data[0]['rank']) ? $job_data[0]['rank'] == $r_index ? 'selected' : '' : ''}}>{{$rank}} </option>
					                                            @endforeach
					                                        @endforeach
					                                    </select>
					                                </div>
					                            </div>
					                            <div class="col-sm-3">
					                                <div class="form-group">
					                                    <label>Ship Type</label>
					                                    <select class="form-control" name="ship_type" id="ship_type">
					                                        <option value=>Select ship type</option>
					                                        @foreach(\CommonHelper::ship_type() as $r_index => $ship_type)
					                                            <option value="{{$r_index}}" {{ !empty($job_data[0]['ship_type']) ? $job_data[0]['ship_type'] == $r_index ? 'selected' : '' : ''}}>{{$ship_type}} </option>
					                                        @endforeach
					                                    </select>
					                                </div>
					                            </div>
					                            <div class="col-sm-3">
					                                <div class="form-group">
					                                    <label>Rank Experience</label>
					                                    <div class="row">
					                                        <div class="col-sm-6">
					                                            <select class="form-control" id="rank_exp_from" name="rank_exp_from">
					                                                  <option value=''>From</option>
					                                                  <option value='1'>1</option>
					                                                  <option value='2'>2</option>
					                                                  <option value='3'>3</option>
					                                                  <option value='4'>4</option>
					                                                  <option value='5'>5</option>
					                                            </select>
					                                        </div>
					                                        <div class="col-sm-6">
					                                            <select class="form-control" id="rank_exp_to" name="rank_exp_to">
					                                                  <option value=''>To</option>
					                                                  <option value='1'>1</option>
					                                                  <option value='2'>2</option>
					                                                  <option value='3'>3</option>
					                                                  <option value='4'>4</option>
					                                                  <option value='5'>5</option>
					                                            </select>
					                                        </div>
					                                    </div>
					                                </div>
					                            </div>
					                            <div class="col-sm-3">
					                                <div class="form-group">
					                                    <label>COC</label>
					                                    <select class="form-control" id="coc" name="coc">
					                                        <option value="">Select COC</option>
					                                        @foreach( \CommonHelper::countries() as $c_index => $country)
					                                            <option value="{{ $c_index }}"> {{ $country }}</option>
					                                        @endforeach
					                                    </select>
					                                </div>
					                            </div>


					                            <div class="col-sm-3">
					                                <div class="form-group">
					                                    <label>CDC</label>
					                                    <select class="form-control" id="cdc" name="cdc">
					                                        <option value="">Select CDC</option>
					                                        @foreach( \CommonHelper::countries() as $c_index => $country)
					                                            <option value="{{ $c_index }}"> {{ $country }}</option>
					                                        @endforeach
					                                    </select>   
					                                </div>
					                            </div>

					                            <div class="col-sm-3">
					                                <div class="form-group">
					                                    <label>Watch Keeping</label>
					                                    <select class="form-control" id="watch_keeping" name="watch_keeping">
					                                        <option value="">Select Watch Keeping</option>
					                                        @foreach( \CommonHelper::countries() as $c_index => $country)
					                                            <option value="{{ $c_index }}"> {{ $country }}</option>
					                                        @endforeach
					                                    </select>   
					                                </div>
	                            				</div>

	                            				<div class="col-sm-3">
					                                <div class="form-group">
					                                    <label>Last Logged On</label>
					                                	<div class="row">
						                                    <div class="col-sm-6">
						                                    	<input type="text" name="last_logged_from" class="form-control issue-datepicker" placeholder="dd-mm-yyyy" value="{{isset($filter['last_logged_from']) ? $filter['last_logged_from'] : ''}}">
						                                    </div>
						                                    <div class="col-sm-6">
						                                    	<input type="text" name="last_logged_to" class="form-control issue-datepicker" placeholder="dd-mm-yyyy" value="{{isset($filter['last_logged_to']) ? $filter['last_logged_to'] : ''}}">
						                                    </div>
						                                </div>
					                                </div>
	                            				</div>
	                            			</div>
	                            			<div class="row">
	                            				<div class="col-sm-3">
					                                <div class="form-group">
					                                    <label>User Verified</label>
					                                  	<select class="form-control" id="user_verified" name="user_verified">
				                                            <option value="">All</option>
				                                            <option value="mob_v" {{isset($filter['user_verified']) ? ($filter['user_verified']) == 'mob_v' ? "selected=selected" : '' : ''}}>Mobile Verified</option>
				                                            <option value="email_v" {{isset($filter['user_verified']) ? ($filter['user_verified']) == 'email_v' ? "selected=selected" : '' : ''}}>Email Verified</option>
				                                            <option value="both" {{isset($filter['user_verified']) ? ($filter['user_verified']) == 'both' ? "selected=selected" : '' : ''}}>Mobile & Email Verified</option>
				                                            <option value="no_v" {{isset($filter['user_verified']) ? ($filter['user_verified']) == 'no_v' ? "selected=selected" : '' : ''}}>No Verification</option>
					                                    </select>   
					                                </div>
	                            				</div>
	                            				<div class="col-sm-3">
					                                <div class="form-group">
					                                    <label>CDC Status</label>
					                                  	<select class="form-control" id="cdc_status" name="cdc_status">
				                                            <option value="">All</option>
				                                            <option value="cdc_v" {{isset($filter['cdc_status']) ? ($filter['cdc_status']) == 'cdc_v' ? "selected=selected" : '' : ''}}>All CDC Verified</option>
				                                            <option value="cdc_uv" {{isset($filter['cdc_status']) ? ($filter['cdc_status']) == 'cdc_uv' ? "selected=selected" : '' : ''}}>All CDC Unverified</option>
				                                           <!--  <option value="atleast_cdc_v" {{isset($filter['cdc_status']) ? ($filter['cdc_status']) == 'atleast_cdc_v' ? "selected=selected" : '' : ''}}>Atleast One CDC Verified</option> -->
				                                            <option value="atleast_cdc_uv" {{isset($filter['cdc_status']) ? ($filter['cdc_status']) == 'atleast_cdc_uv' ? "selected=selected" : '' : ''}}>Atleast One Unverified</option>
					                                    </select>   
					                                </div>
	                            				</div>
	                            				<div class="col-sm-3">
					                                <div class="form-group">
					                                    <label>COC Status</label>
					                                  	<select class="form-control" id="coc_status" name="coc_status">
				                                            <option value="">All</option>
				                                           	<option value="coc_v" {{isset($filter['coc_status']) ? ($filter['coc_status']) == 'coc_v' ? "selected=selected" : '' : ''}}>All COC Verified</option>
				                                            <option value="coc_uv" {{isset($filter['coc_status']) ? ($filter['coc_status']) == 'coc_uv' ? "selected=selected" : '' : ''}}>All COC Unverified</option>
				                                            <!-- <option value="atleast_coc_v" {{isset($filter['coc_status']) ? ($filter['coc_status']) == 'atleast_coc_v' ? "selected=selected" : '' : ''}}>Atleast One COC Verified</option> -->
				                                            <option value="atleast_coc_uv" {{isset($filter['coc_status']) ? ($filter['coc_status']) == 'atleast_coc_uv' ? "selected=selected" : '' : ''}}>Atleast One Unverified</option>
					                                    </select>   
					                                </div>
	                            				</div>
	                            			</div>
	                            			<div class="row">
	                            				<div class="col-xs-12 col-sm-3">
													<div class="search-count-content m_t_25">
														<a type="button" id="download_excel" class="btn btn-success" data-type='seafarer'>Download Excel</a>
													</div>
												</div>
					                            <div class="col-sm-9">
					                                <div class="section_cta text-right m_t_25 job-btn-container">
					                                    <button type="button" class="btn btn-danger coss-inverse-btn search-reset-btn" id="seafarerResetButton">Reset</button>
					                                    <button type="submit" data-style="zoom-in" class="btn btn-info coss-primary-btn search-post-btn ladda-button candidate-filter-search-button" id="candidate-filter-search-button"><i class="fa fa-search search-icon" aria-hidden="true"></i>Search</button>
					                                </div>
					                            </div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</form>
						<!-- end: BREADCRUMB -->
						<!-- start: PAGE CONTENT -->
						<div class="row search-results-main-container" >
							
							<div class="col-md-12">
								<div class="panel panel-white">
									<div class="panel-body">
									@if(isset($seafarer_data['data']) && !empty($seafarer_data['data']))

										<div class="panel-heading">
											<div class="row">
												<div class="col-xs-12 col-sm-8 paginator-content  pull-right no-margin">
													<ul id="top-user-paginator" class="pagination pagination-blue">
														{!! $pagination->render() !!}
													</ul>
												</div>
												<div class="col-xs-12 col-sm-4">
													<div class="pull-left search-count-content">
														Showing {{$seafarer_data['from']}} - {{$seafarer_data['to']}} of {{$seafarer_data['total']}} Results
													</div>
												</div>
											</div>
										</div>
										
										<div class="full-cnt-loader hidden">
											<div class="" style="text-align: center;padding-top:20px;padding-bottom:20px; ">
											    <i class="fa fa-spinner fa-spin fa-4" aria-hidden="true" style="font-size: 7em;"></i>
										    </div>
										</div>
										<div class="list-container">
										@foreach($seafarer_data['data'] as $data)
											<div class="seafarer-listing-container">
												<div class="panel">
													<div class="panel-heading border-light partition-blue">
														<h5 class="panel-title text-bold header-link-text p-5-5">
																<a href="{{Route('admin.view.seafarer.id',$data['id'])}}">
																	{{isset($data['first_name']) ? $data['first_name'] : ''}}
																</a>
														</h5>
														<ul class="panel-heading-tabs border-light">
															<li class="panel-tools">
																<div class="dropdown">
																	<a data-toggle="dropdown" class="btn btn-xs dropdown-toggle btn-transparent-grey">
																		<i class="fa fa-cog"></i>
																	</a>
																	<ul class="dropdown-menu dropdown-light pull-right" role="menu">
																		<li>
																			<a href="{{Route('admin.view.seafarer.id',$data['id'])}}" data-type="user">
																				View
																			</a>
																		</li>
																		<li>
																			<a href="{{Route('admin.edit.seafarer',$data['id'])}}">
																				Edit
																			</a>
																		</li>
																		<li>
																			<a class="change-status-user cursor-pointer {{ $data['status'] == '0' ? 'hide' : ''}}" data-status='deactive' data-id={{$data['id']}} id="deactive-{{$data['id']}}">
																				Deactivate
																			</a>
																		</li>
																		<li>
																			<a class="change-status-user cursor-pointer {{ $data['status'] != '0' ? 'hide' : ''}}"" data-status='active' data-id={{$data['id']}} id="active-{{$data['id']}}">
																				Activate
																			</a>
																		</li>
																		<li>
																			<a class="resend_welcome_email cursor-pointer" data-id={{$data['id']}}>
																				Resend Welcome Email
																			</a>
																		</li>
																	</ul>
																</div>
															</li>
														</ul>
														
													</div>

													<div class="panel-body">
														<div class="row">
															<div class="col-lg-3 col-md-5 col-sm-5" id="user-profile-pic">
																<a>
																	<div class="user-profile-pic-thumb make-user-profile-pic-block">
					                                					<img src="/{{ isset($data['profile_pic']) ? env('SEAFARER_PROFILE_PATH')."/".$data['id']."/".$data['profile_pic'] : 'images/user_default_image.png'}}" style="height: 200px;width: 200px;">
					                                				</div>
						                            			</a>
															</div>
															<div class="col-lg-9 col-md-7 col-sm-7">
																<div class="row">
																	<div class="col-sm-4 sm-t-20">
																		<div class="row">
																			<div class="col-xs-12">
																				<div class="header-tag">
																					Basic Information
																				</div>
																			</div>
																		</div>
																		<div id="user-basic-details">
																			<div class="row">
																				<div class="col-xs-6 col-sm-7">
																					<dl>
																						<dt> Name : </dt>
																						<dd class="margin-bottom-5px">
																							{{isset($data['first_name']) ? $data['first_name'] : ''}}
																						</dd>
																						<dt> Email : 
																							@if($data['is_email_verified'] == '1')
																								<i class="fa fa-check-circle green f-15" aria-hidden="true" title="Email Verified"></i>
																							@else
																								<i class="fa fa-minus-circle red f-15" aria-hidden="true" title="Email Unverified"></i>
																							@endif
																						</dt>
																						<dd class="margin-bottom-5px make-word-wrap-break">
																							{{isset($data['email']) ? $data['email'] : ''}}
																			     		</dd>
																			     		<dt> Mobile : 
																			     			@if($data['is_mob_verified'] == '1')
																			     				<i class="fa fa-check-circle green f-15" aria-hidden="true" title="Mobile Verified"></i>
																			     			@else
																								<i class="fa fa-minus-circle red f-15" aria-hidden="true" title="Mobile Unverified"></i>
																			     			@endif
																			     		</dt>
																						<dd class="margin-bottom-5px">
																							{{isset($data['mobile']) ? $data['mobile'] : ''}}
																			     		</dd>
																			     		<dt>
																			     		<dt> Last Logged On : </dt>
																						<dd class="margin-bottom-5px w-170">
																							{{isset($data['last_logged_on']) ? date('d-m-Y H:i a', strtotime($data['last_logged_on'])) : '-'}}
																			     		</dd>
																			     		<dt>
																					</dl>
																				</div>
																			</div>
																		</div>
																	</div>
																	<?php
						                                                if(isset($data['professional_detail']['current_rank_exp'])){
						                                                    $rank_exp = explode(".",$data['professional_detail']['current_rank_exp']);
						                                                    $rank_exp[1] = number_format($rank_exp[1]);
						                                                }
						                                            ?>
																	<div class="col-sm-4">
																		<div class="header-tag">
																			Current Rank Information
																		</div>
																		<div id="user-professional-details">
																			<dl>
																				<dt> Current Rank :</dt>
																				<dd class="margin-bottom-5px">
																					@if(!empty($data['professional_detail']['current_rank']))
									                                                    @foreach(\CommonHelper::new_rank() as $rank_index => $category)
												                                            @foreach($category as $r_index => $rank)
												                                                {{ !empty($data['professional_detail']['current_rank']) ? $data['professional_detail']['current_rank'] == $r_index ? $rank : '' : ''}}
												                                            @endforeach
												                                        @endforeach
												                                    @else
												                                    	-
												                                    @endif
																				</dd>
																				<dt> Current Rank Experience :</dt>
																				<dd class="margin-bottom-5px">
																					@if(isset($data['professional_detail']['current_rank_exp']) && !empty($data['professional_detail']['current_rank_exp']))
																						{{ isset($data['professional_detail']['current_rank_exp']) ? $rank_exp[0] ." Years ". $rank_exp[1].' Months' : ''}}
																					@else
																						-
																					@endif
																				</dd>
																				<dt> Date Of Availability :</dt>
																				<dd class="margin-bottom-5px">
																					{{ isset($data['professional_detail']['availability']) ? date('d-m-Y', strtotime($data['professional_detail']['availability']))  : '-'}}
																				</dd>
																				<dt> Registered On :</dt>
																				<dd class="margin-bottom-5px">
																					{{ isset($data['created_at']) && !empty($data['created_at']) ? date('d-m-Y H:i:s', strtotime($data['created_at']))  : '-'}}
																				</dd>
																			</dl>
																		</div>
																	</div>
																	<div class="col-sm-4">
																			<div class="header-tag">
																				Passport Information
																			</div>
																			<div id="user-professional-details">
																				<dl>
																					<dt> Passport Number :</dt>
																					<dd class="margin-bottom-5px">
																						{{isset($data['passport_detail']['pass_number']) && !empty($data['passport_detail']['pass_number']) ? $data['passport_detail']['pass_number'] : '-'}} 
																						
																					</dd>
																					<dt> Country :</dt>
																					<dd class="margin-bottom-5px">
																						@if(isset($data['passport_detail']['pass_country']) && !empty($data['passport_detail']['pass_country']))
																							@foreach( \CommonHelper::countries() as $c_index => $country)
										                                                        {{ !empty($data['passport_detail']['pass_country']) ? $data['passport_detail']['pass_country'] == $c_index ? $country : '' : ''  }}
										                                                    @endforeach
										                                                @else
										                                                	-
										                                                @endif
																					</dd>
																					<dt> Place Of Issue : </dt>
																					<dd class="margin-bottom-5px">
																						{{ isset($data['passport_detail']['place_of_issue']) && !empty($data['passport_detail']['place_of_issue']) ? $data['passport_detail']['place_of_issue'] : '-'}}  
																					</dd>
																				</dl>
																			</div>
																		</div>
																	
																</div>
															</div>

														</div>
													</div>
												</div>

											</div>
										@endforeach
										</div>

										<div class="panel-heading">
											<div class="row">
												<div class="col-xs-12 col-sm-8 paginator-content  pull-right no-margin">
													<ul id="top-user-paginator" class="pagination pagination-blue">
														{!! $pagination->render() !!}
													</ul>
												</div>
												<div class="col-xs-12 col-sm-4">
													<div class="search-count-content">
														Showing {{$seafarer_data['from']}} - {{$seafarer_data['to']}} of {{$seafarer_data['total']}} Results
													</div>
												</div>
											</div>
										</div>
									@else
										<div class="no-results-found">No results found. Try again with different search criteria.</div>
									@endif
									</div>
								</div>
							</div>
						</div>
						<!-- end: PAGE CONTENT-->
					</div>
					<div class="subviews">
						<div class="subviews-container"></div>
					</div>
				</div>
				<!-- end: PAGE -->
			</div>
</div>

	<script type="text/javascript">
        
        var searchAjaxCall = "undefined";

        $(document).ready(function(){

            //init history plugin to perform search without page refresh
            // Establish Variables
            History = window.History; // Note: We are using a capital H instead of a lower h

            // Bind to State Change
            // Note: We are using statechange instead of popstate
			
            History.Adapter.bind(window,'statechange', function() {

                // Log the State
                var State = History.getState(); // Note: We are using History.getState() instead of event.state

                //start search (defined)
                fetchSearchResults(State.url, 
                                    function() {
                                        
                                    }, function() {

                                    });

            });

            

        });

        function fetchSearchResults(url, successCallback, errorCallback)
	    {
	    	
	    	var temp_current_state_url = url.split('?');
            if(temp_current_state_url.length > 1 && temp_current_state_url[1] != ''){
                var parameters = temp_current_state_url[1].split('&');

                var dropdown_elements = ['rank','ship_type','rank_exp_from','rank_exp_to','coc','cdc','watch_keeping','user_verified','cdc_status','coc_status'];

                for(var i=0; i<parameters.length; i++){

                    var param_array = parameters[i].split('=');
                    if($.inArray(param_array[0],dropdown_elements) > -1){
                        $('select[name="'+param_array[0]+'"]').val(param_array[1]);
                    }else if($.inArray(param_array[0],['availability_from','availability_to','first_name','last_logged']) > -1){
                        $('input[name="'+param_array[0]+'"]').val(param_array[1]);
                    }

                }
            } else {
             	$(':input','.filter-search-form')
	                .not(':button, :submit, :reset, :hidden')
	                .val('')
	                .removeAttr('checked')
	                .removeAttr('selected');
	             }

	        if(searchAjaxCall != "undefined")
	        {
	            searchAjaxCall.abort();
	            searchAjaxCall = "undefined";
	        }

	        $('.list-container').addClass('hide');
        	$('.full-cnt-loader').removeClass('hidden');

	        searchAjaxCall = $.ajax({
	            url: url,
	            cache: false,
	            dataType: 'json',
	            statusCode: {
	                200: function(response) {
	                    if (typeof response.template != 'undefined') {
	                       	$('.list-container').removeClass('hide');
                        	$('.full-cnt-loader').addClass('hidden');

	                        $('.search-results-main-container').html(response.template);

	                        if(typeof successCallback != "undefined")
	                        {
	                            successCallback();
	                        }

	                    }
	                }
	            },
	            error: function(error, response) {

	                if(typeof errorCallback != "undefined")
	                {
	                    errorCallback();
	                }
	            }
	        });
	    }
    </script>

@stop