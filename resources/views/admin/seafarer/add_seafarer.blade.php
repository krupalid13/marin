@extends('admin.index')

@section('content')

<?php
	$initialize_pincode_maxlength_validation = false;
	$india_value = array_search('India',\CommonHelper::countries());
	$required_fields_for_selected_rank[] = '';

	$first_tab_class = 'page-done page-active';
	$tab_class = 'details-tab';
	$reg_flow = 'registration-flow';
	$disabled_class = 'disabled';
	if(Route::currentRouteName() == 'site.seafarer.edit.profile') {
		$first_tab_class = '';
		$tab_class = 'edit-details-tab';
		$reg_flow = '';
		$disabled_class = '';
	}
	$required_fields = \CommonHelper::rank_required_fields();

	if(isset($user_data[0]['professional_detail']['current_rank'])){
		$required_fields_for_selected_rank = \CommonHelper::rank_required_fields()[$user_data[0]['professional_detail']['current_rank']];
	}
	$required_fields_name[] = '';
	if(isset($required_fields_for_selected_rank)){
		foreach ($required_fields_for_selected_rank as $key => $value) {
			$rank_name = explode('-', $value);
			$required_fields_name[$key] = $rank_name[0];
		}
	}
?>

<div class="container">
	<div class="row">
		<div class="col-sm-12">
			<!-- start: FORM WIZARD PANEL -->
			<div class="panel panel-white">
				<div class="panel-body">
					<input type="hidden" name="india_value" value="{{$india_value}}" required>
					<form id="add-seafarer-form" class="smart-wizard form-horizontal" action="{{ isset($user_data['current_route']) ? 'update' : 'store'}}" 
					method="post">
						{{ csrf_field() }}
						@if(isset($user_data['current_route']))
							<input type="hidden" id="edit">
						@endif
						<div id="wizard" class="swMain">
							<ul>
								<li>
									<a href="#step-1">
										<div class="stepNumber">
											1
										</div>
										<span class="stepDesc"> Step 1
											<br />
											<small>Basic Details</small> </span>
									</a>
								</li>
								<li>
									<a href="#step-2">
										<div class="stepNumber">
											2
										</div>
										<span class="stepDesc"> Step 2
											<br />
											<small>Important Documents</small> </span>
									</a>
								</li>
								<li>
									<a href="#step-3">
										<div class="stepNumber">
											3
										</div>
										<span class="stepDesc"> Step 3
											<br />
											<small>Sea Service Details</small> </span>
									</a>
								</li>
								<li>
									<a href="#step-4">
										<div class="stepNumber">
											4
										</div>
										<span class="stepDesc"> Step 4
											<br />
											<small>Certificate Details</small> </span>
									</a>
								</li>
							</ul>
							<div class="progress progress-xs transparent-black no-radius active">
								<div aria-valuemax="100" aria-valuemin="0" role="progressbar" class="progress-bar partition-green step-bar">
									<span class="sr-only"> 0% Complete (success)</span>
								</div>
							</div>
							<div id="step-1">
								<input type="hidden" name="user_id" id="user_id" value="{{isset($user_data[0]['id']) ? $user_data[0]['id'] : ''}}">
								<h2 class="StepTitle">Login Details</h2>
								<div class="panel-heading">
									<h4 class="panel-title">ACCOUNT CREDENTIALS</h4>
								</div>
									<div class="form-group">
										<input type="hidden" name="image-x" required>
                                        <input type="hidden" name="image-y" required>
                                        <input type="hidden" name="image-x2" required>
                                        <input type="hidden" name="image-y2" required>
                                        <input type="hidden" name="crop-w" required>
                                        <input type="hidden" name="crop-h" required>
                                        <input type="hidden" name="image-w" required>
                                        <input type="hidden" name="image-h" required>
                                        <input type="hidden" name="uploaded-file-name" required>
                                        <input type="hidden" name="uploaded-file-path" required>
										
										<div class="row no-margin">
                                            <div class="col-sm-8 col-sm-offset-2">
		                                        <div class="upload-photo-container">
													<div class="image-content">
														<div class="registration-profile-image" style="height: 100%;width: 100%;">
															<?php
																$image_path = '';
																if(isset($user_data[0]['profile_pic'])){
																	$image_path = "/".env('SEAFARER_PROFILE_PATH')."".$user_data[0]['id']."/".$user_data[0]['profile_pic'];
																}
															?>
															@if(empty($image_path))
																<div class="icon profile_pic_text p-t-30"><i class="fa fa-camera" aria-hidden="true"></i></div>
																<div class="image-text profile_pic_text">Upload Profile <br> Picture</div>
															@endif
															
															<input type="file" style="z-index: 9;" name="profile_pic" id="profile-pic" class="cursor-pointer" onChange="profilePicSelectHandler('add-seafarer-form', 'profile_pic', 'pic', 'seafarer')">

															@if(!empty($image_path))
															 	<img id="preview" style="border-radius: 50%;width: 100%;" src="{{ $image_path }}">
			                                                @else                
															 	<img id="preview" style="border-radius: 50%;">
			                                                @endif
														</div>
													</div>
												</div>
												<span class="f-14">Note: Photo has to be professional photo.<br> Image size should be less than 5 MB.</span>
											</div>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label">
											Email <span class="symbol required"></span>
										</label>
										<div class="col-sm-4">
											<input type="email" class="form-control" id="email" name="email" placeholder="Type your email address" value="{{ isset($user_data[0]['email']) ? $user_data[0]['email'] : ''}}">
				                    		<span class="hide" id="email-error" style="color: red"></span>
										</div>
										<label class="col-sm-2 control-label">
											Full name <span class="symbol required"></span>
										</label>
										<div class="col-sm-4">
											<input type="text" class="form-control" id="firstname" name="firstname" value="{{ isset($user_data[0]['first_name']) ? $user_data[0]['first_name'] : ''}}" placeholder="Type your full name">
										</div>
									</div>
									@if(!isset($user_data['current_route']))
										<div class="form-group">
											<label class="col-sm-2 control-label">
												Password <span class="symbol required"></span>
											</label>
											<div class="col-sm-4">
												 <input type="password" class="form-control" id="password" name="password" placeholder="Type your password">
											</div>
											<label class="col-sm-2 control-label">
												Confirm Password <span class="symbol required"></span>
											</label>
											<div class="col-sm-4">
												<input type="password" class="form-control" id="cpassword" name="cpassword" placeholder="Type password again">
											</div>
										</div>
									@endif
									
									<div class="form-group">
										<label class="col-sm-2 control-label">
											Date Of Birth <span class="symbol required"></span>
										</label>
										<div class="col-sm-4">
											<input type="text" class="form-control birth-date-datepicker" id="dob" name="dob" value="{{ isset($user_data[0]['personal_detail']['dob']) ? date('d-m-Y',strtotime($user_data[0]['personal_detail']['dob'])) : ''}}" placeholder="dd-mm-yyyy">
										</div>
										<label class="col-sm-2 control-label">
											Place Of Birth <span class="symbol required"></span>
										</label>
										<div class="col-sm-4">
						                    <input type="text" class="form-control" id="place_of_birth" name="place_of_birth" value="{{ isset($user_data[0]['personal_detail']['place_of_birth']) ? $user_data[0]['personal_detail']['place_of_birth'] : ''}}" placeholder="Type city Name">
						                </div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label">
											Nationality <span class="symbol required"></span>
										</label>
										<div class="col-sm-4">
											<select id='nationality' name="nationality" class="form-control">
											<option value=''>Select Your nationality</option>
											@foreach( \CommonHelper::countries() as $c_index => $country)
													<option value="{{ $c_index }}" {{ isset($user_data[0]['personal_detail']['nationality']) ? $user_data[0]['personal_detail']['nationality'] == $c_index ? 'selected' : '' : ''}}> {{ $country }}</option>
											@endforeach
										</select>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label">
											Gender <span class="symbol required"></span>
										</label>
										<div class="col-sm-4">
											<label class="radio-inline">
											  <input type="radio" name="gender" value="M" 
											  {{ !empty($user_data[0]['gender']) ?  $user_data[0]['gender'] == 'M'? 'checked' : '' : 'checked' }}> Male </label>
											<label class="radio-inline">
											  <input type="radio" name="gender" value="F" 
											  {{ !empty($user_data[0]['gender']) ?  $user_data[0]['gender'] == 'F'? 'checked' : '' : '' }}> Female
											</label>
										</div>

										<label class="col-sm-2 control-label">
											Marital Status <span class="symbol required"></span>
										</label>
										<div class="col-sm-4">
										 <select id="marital_status" name="marital_status" class="form-control select-rank-exp">
						                    <option value="">Select</option>
						                    	@foreach(\CommonHelper::marital_status() as $m_index => $status)
			                                        <option value="{{$m_index}}" {{ !empty($user_data[0]['personal_detail']['marital_status']) ? $user_data[0]['personal_detail']['marital_status'] == $m_index ? 'selected' : '' : ''}}>{{$status}} 
			                                        </option>
			                                	@endforeach
		                                </select>
		                               </div>
									</div>
									<div class="form-group">
						                <label class="col-sm-2 control-label">
											Height (cm) <span class="symbol required"></span>
										</label>
										<div class="col-sm-4">
					    	                <input type="text" class="form-control" id="height" name="height" value="{{ isset($user_data[0]['personal_detail']['height']) ? $user_data[0]['personal_detail']['height'] : ''}}" placeholder="Height in cms">
						                 </div>

										<label class="col-sm-2 control-label">
											Weight (kg) <span class="symbol required"></span>
										</label>
										<div class="col-sm-4">
					    	                <input type="text" class="form-control" id="weight" name="weight" value="{{ isset($user_data[0]['personal_detail']['weight']) ? $user_data[0]['personal_detail']['weight'] : ''}}" placeholder="Weight in kgs">
						                 </div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label">
											Mobile Number <span class="symbol required"></span>
										</label>
										<div class="col-sm-4">
											<input type="text" class="form-control" id="mobile" name="mobile" value="{{ isset($user_data[0]['mobile']) ? $user_data[0]['mobile'] : ''}}" placeholder="Type your mobile number">
											<span class="hide" id="mobile-error" style="color: red"></span>
										</div>
								
										<label class="col-xs-12 col-sm-2 control-label">
											Landline Number
										</label>
										<div class="col-xs-4 col-sm-1">
											<input type="text" class="form-control" id="landline_code" name="landline_code" value="{{ isset($user_data[0]['personal_detail']['landline_code']) ? $user_data[0]['personal_detail']['landline_code'] : ''}}" placeholder="code" maxlength="5" style="text-align: center;">
										</div>
										<div class="col-xs-8 col-sm-3" style="margin-left: -15px;">
											<input type="text" class="form-control" id="landline" name="landline" value="{{ isset($user_data[0]['personal_detail']['landline']) ? $user_data[0]['personal_detail']['landline'] : ''}}" placeholder="Type your landline number">
										</div>
									</div>

									<div class="panel-heading">
										<h4 class="panel-title">LOCATION DETAILS</h4>
									</div>

									<div class="form-group">
										<label class="col-sm-2 control-label">
											Country <span class="symbol required"></span>
										</label>
										<div class="col-sm-4">
											<select id='country' name="country[0]" class="form-control country" block-index="0">
											<option value=''>Select Your Country</option>
											@foreach( \CommonHelper::countries() as $c_index => $country)
												@if(isset($user_data[0]['personal_detail']['country']))
													<option value="{{ $c_index }}" {{ isset($user_data[0]['personal_detail']['country']) ? $user_data[0]['personal_detail']['country'] == $c_index ? 'selected' : '' : ''}}> {{ $country }}</option>
												@else
													<option value="{{ $c_index }}" {{ $india_value == $c_index ? 'selected' : ''}}> {{ $country }}</option>
												@endif
											@endforeach
										</select>
										</div>
									
										<label class="col-sm-2 control-label">
											Postal Code <span class="symbol required"></span>
										</label>
										<div class="col-sm-4 pincode-block-0">
											<input type="hidden" class="pincode-id" name="pincode_id[0]" value="">
											
											<i class="fa fa-spin fa-refresh select-loader hide pincode-loader-0"></i>
											<input type="text" block-index="0" id='pincode' data-form-id="add-seafarer-form" class="form-control pincode pin_code_fetch_input" name="pincode[0]" value="{{ isset($user_data[0]['personal_detail']['pincode_text']) ? $user_data[0]['personal_detail']['pincode_text'] : ''}}" placeholder="Type your pincode">
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label">
											State <span class="symbol required"></span>
										</label>
										<div class="col-sm-4 state-block state-block-0">
											@if(isset($user_data[0]['personal_detail']))
											@if($user_data[0]['personal_detail']['country'] == $india_value )
												<select id="state" name="state[0]" class="form-control state fields-for-india">
													<option value="">Select Your State</option>
													@foreach($user_data[0]['personal_detail']['pincode']['pincodes_states'] as $pincode_states)
														<option value="{{$pincode_states['state_id']}}" {{$pincode_states['state_id'] == $user_data[0]['personal_detail']['state_id'] ? 'selected' : ''}}>{{ucwords(strtolower($pincode_states['state']['name']))}}</option>
													@endforeach
												</select>
												<input type="text" id="state" name="state_name[0]" class="form-control state_text hide fields-not-for-india" placeholder="Enter State Name" value="">

											@else
												<select id="state" name="state[0]" class="form-control state hide fields-for-india">
													<option value="">Select Your State</option>
												</select>
												<input type="text" id="state" name="state_name[0]" class="form-control state_text fields-not-for-india" placeholder="Enter State Name" value="{{ isset($user_data[0]['personal_detail']['state_text']) ? $user_data[0]['personal_detail']['state_text'] : ''}}">
											@endif
										@else
											<select id="state" name="state[0]" class="form-control state fields-for-india">
												<option value="">Select Your State</option>
											</select>
											<input type="text" id="state" name="state_name[0]" class="form-control state_text hide fields-not-for-india" placeholder="Enter State Name" value="{{ isset($user_data[0]['personal_detail']['state_text']) ? $user_data[0]['personal_detail']['state_text'] : ''}}">
										@endif
										</div>
									
										<label class="col-sm-2 control-label">
											City <span class="symbol required"></span>
										</label>
										<div class="col-sm-4 city-block city-block-0">
											@if(isset($user_data[0]['personal_detail']['country']))
											@if( $user_data[0]['personal_detail']['country'] == $india_value )
												<select id="city" name="city[0]" class="form-control city fields-for-india">
													<option value="">Select Your City</option>
													@foreach($user_data[0]['personal_detail']['pincode']['pincodes_cities'] as $pincode_city)
														<option value="{{$pincode_city['city_id']}}" {{$pincode_city['city_id'] == $user_data[0]['personal_detail']['city_id'] ? 'selected' : ''}}>{{ucwords(strtolower($pincode_city['city']['name']))}}</option>
													@endforeach
												</select>
												<input type="text" name="city_text[0]" class="form-control city_text hide fields-not-for-india" placeholder="Enter City Name" value="">

											@else
												<select id="city" name="city[0]" class="form-control city hide fields-for-india">
													<option value="">Select Your City</option>
												</select>

												{{--<input type="text" name="city_text[0]" class="form-control city_text fields-not-for-india" placeholder="Enter City Name" value="">--}}
												<input type="text" name="city_text[0]" class="form-control city_text fields-not-for-india" placeholder="Enter City Name" value="{{ isset($user_data[0]['personal_detail']['city_text']) ? $user_data[0]['personal_detail']['city_text'] : ''}}">
											@endif
										@else
											<select id="city" name="city[0]" class="form-control city fields-for-india">
												<option value="">Select Your City</option>
											</select>

											{{--<input type="text" name="city_text[0]" class="form-control city_text fields-not-for-india" placeholder="Enter City Name" value="">--}}
											<input type="text" name="city_text[0]" class="form-control city_text hide fields-not-for-india" placeholder="Enter City Name" value="{{ isset($user_data[0]['personal_detail']['city_text']) ? $user_data[0]['personal_detail']['city_text'] : ''}}">
										@endif
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label">
											Address Line 1<span class="symbol required"></span>
										</label>
										<div class="col-sm-4">
											<input type="text" class="form-control" id="permananent_address" name="permananent_address" value="{{ isset($user_data[0]['personal_detail']['permanent_add']) ? $user_data[0]['personal_detail']['permanent_add'] : ''}}" placeholder="Type your address">
										</div>
										
										<label class="col-sm-2 control-label">
											Address Line 2</span>
										</label>

										<div class="col-sm-4">
					                    	<input type="text" class="form-control" id="permananent_address2" name="permananent_address2" value="{{ isset($user_data[0]['personal_detail']['permanent_add2']) ? $user_data[0]['personal_detail']['permanent_add2'] : ''}}" placeholder="Type your address">
					                	</div>
					                </div>
									<div class="form-group">
										<label class="col-sm-2 control-label">
											Nearest Airport/Railway Station</span>
										</label>
										<div class="col-sm-4">
											 <input type="text" class="form-control" id="nearest_place" name="nearest_place" value="{{ isset($user_data[0]['personal_detail']['nearest_place']) ? $user_data[0]['personal_detail']['nearest_place'] : ''}}" placeholder="Type your Nearest Airport or Railway Station">
										</div>
									</div>

									<div class="panel-heading">
										<h4 class="panel-title">NEXT OF KIN</h4>
									</div>
									<div class="form-group">
						                <label class="col-sm-2 control-label" for="kin_name">Name<span class="symbol required"></span></label>
										<div class="col-sm-4">
						                    <input type="text" class="form-control" id="kin_name" name="kin_name" value="{{ isset($user_data[0]['personal_detail']['kin_name']) ? $user_data[0]['personal_detail']['kin_name'] : ''}}" placeholder="Type your kin full name">
						                </div>
									
						                <label class="col-sm-2 control-label" for="kin_relation">Relation<span class="symbol required"></span></label>
										<div class="col-sm-4">
						                    <input type="text" class="form-control" id="kin_relation" name="kin_relation" value="{{ isset($user_data[0]['personal_detail']['kin_relation']) ? $user_data[0]['personal_detail']['kin_relation'] : ''}}" placeholder="Type your relation with kin">
						                </div>
									</div>
									<div class="form-group">
						                <label class="col-sm-2 control-label" for="kin_number">Phone Number<span class="symbol required"></span></label>
										<div class="col-sm-4">
					    	                <input type="text" class="form-control" id="kin_number" name="kin_number" value="{{ isset($user_data[0]['personal_detail']['kin_number']) ? $user_data[0]['personal_detail']['kin_number'] : ''}}" placeholder="Type kin phone number">
						                 </div>
										<label class="col-sm-2 control-label">
											Alternate Number</span>
										</label>
										<div class="col-sm-4">
											<input type="text" class="form-control" id="kin_alternate_no" name="kin_alternate_no" value="{{ isset($user_data[0]['personal_detail']['kin_alternate_no']) ? $user_data[0]['personal_detail']['kin_alternate_no'] : ''}}" placeholder="Type your alternate number">
										</div>
									</div>

									<div class="panel-heading">
										<h4 class="panel-title">PROFFESSIONAL DETAILS</h4>
									</div>

									<div class="form-group">
										<label class="col-sm-2 control-label">
											Current Rank <span class="symbol required"></span>
										</label>
										<div class="col-sm-4">
											<select id="current_rank" name="current_rank" class="form-control">
											  	<option value="">Select Your Rank</option>
											    @foreach(\CommonHelper::new_rank() as $index => $category)
											    	<optgroup label="{{$index}}"></optgroup>
											    	@foreach($category as $r_index => $rank)
			                                        	<option value="{{$r_index}}" {{ !empty($user_data[0]['professional_detail']) ? $user_data[0]['professional_detail']['current_rank'] == $r_index ? 'selected' : '' : ''}}>{{$rank}} </option>
			                                        @endforeach
			                                    @endforeach
											</select>
										</div>
										<label class="col-sm-2 control-label">
											Current Rank Experience <span class="symbol required"></span>
										</label>
										<div class="col-sm-4 no-padding">
											<div class="col-xs-6 col-md-4">
						                    	<select id="years" name="years" class="form-control select-rank-exp">
						                    	 	<option value="">Years</option>
							                    	 @foreach(\CommonHelper::years() as $r_index => $rank)
				                                        <option value="{{$r_index}}" {{ !empty($user_data[0]['professional_detail']) ? $user_data[0]['professional_detail']['years'] == $r_index ? 'selected' : '' : ''}}>{{$rank}} 
				                                        </option>
				                                    @endforeach
							                    </select>
											</div>
											<div class="col-xs-6 col-md-4">
							                    <select id="months" name="months" class="form-control select-rank-exp">
								                    <option value="">Months</option>
								                     @foreach(\CommonHelper::months() as $r_index => $rank)
					                                        <option value="{{$r_index}}" {{ !empty($user_data[0]['professional_detail']) ? $user_data[0]['professional_detail']['months'] == $r_index ? 'selected' : '' : ''}}>{{$rank}} 
					                                        </option>
					                                @endforeach
			                                </select>
			                                </div>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label">
											Rank Applied For<span class="symbol required"></span>
										</label>
										<div class="col-sm-4">
											<select id="applied_rank" name="applied_rank" class="form-control">
											  	<option value="">Select Your Rank</option>
											    @foreach(\CommonHelper::new_rank() as $index => $category)
											    	<optgroup label="{{$index}}"></optgroup>
											    	@foreach($category as $r_index => $rank)
			                                        	<option value="{{$r_index}}" {{ !empty($user_data[0]['professional_detail']['applied_rank']) ? $user_data[0]['professional_detail']['applied_rank'] == $r_index ? 'selected' : '' : ''}}>{{$rank}} </option>
			                                        @endforeach
			                                    @endforeach
											</select>
										</div>
										<label class="col-sm-2 control-label">
											Date of Availability <span class="symbol required"></span>
										</label>
										<div class="col-sm-4">
											<input type="text" class="form-control datepicker" id="date_avaibility" name="date_avaibility" value="{{ isset($user_data[0]['professional_detail']['availability']) ? date('d-m-Y',strtotime($user_data[0]['professional_detail']['availability'])) : ''}}" placeholder="dd-mm-yyyy">
										</div>
									</div>
									<?php 
										$dollar = '';
										$rupees = '';
										if(isset($user_data[0]['professional_detail']['currency'])){
											if($user_data[0]['professional_detail']['currency'] == 'dollar'){
												$dollar = 'active';
											}else{
												$rupees = 'active';
											}
										}
										else{
											$rupees = 'active';
										}
									?>
									<div class="form-group">
										<label class="col-xs-12 col-sm-2 control-label" >
											Last Wages Drawn <span class="symbol required"></span>
										</label>
										<div class="col-sm-4">
											<div class="col-xs-4 col-sm-3 no-padding">
												<div class="btn-group p-t-5" id="status" data-toggle="buttons">
									              	<label class="btn btn-default btn-on btn-xs {{$dollar}}">
									              		<input type="radio" value="dollar" name="wages_currency" checked="checked"><i class="fa fa-dollar fa-lg"></i>
									              	</label>
									              	<label class="btn btn-default btn-xs btn-off {{$rupees}}">
									              	<input type="radio" value="rupees" name="wages_currency">
									              		<i class="fa fa-inr fa-lg"></i>
								              		</label>
									            </div>
											</div>
											<div class="col-xs-4 col-sm-5 no-padding">
												<input type="text" class="form-control" id="last_wages" name="last_wages" value="{{ isset($user_data[0]['professional_detail']['last_salary']) ? $user_data[0]['professional_detail']['last_salary'] : ''}}" placeholder="Type your last wages drawn">
											</div>
											<div class="col-xs-4 col-sm-4" style="padding: 10px 10px;">
												Per Month
											</div>
										</div>
									</div>
									
									
								<div class="form-group">
									<div class="col-sm-2 col-sm-offset-10">
										<button type="button" data-style="zoom-in" class="btn btn-blue next-step btn-block seafarer-next-btn ladda-button" data-form="step-1" data-type='basic_details'>
											Save & Next <i class="fa fa-arrow-circle-right"></i>
										</button>
									</div>
								</div>
							</div>
							<div id="step-2">
								<h2 class="StepTitle">Important Documents</h2>
								<input type="hidden" id="current_rank_id" name="current_rank_id" value="{{isset($user_data[0]['professional_detail']['current_rank']) ? $user_data[0]['professional_detail']['current_rank'] : ''}}">
								<div class="panel-heading">
									<h4 class="panel-title">PASSPORT DETAILS</h4>
								</div>
								<div class="form-group">
									<div class="row no-margin">
										<label class="col-sm-2 control-label">
											Passport No <span class="symbol required"></span>
										</label>
										<div class="col-sm-4">
											<input type="text" class="form-control" id="passno" name="passno" value="{{ isset($user_data[0]['passport_detail']['pass_number']) ? $user_data[0]['passport_detail']['pass_number'] : ''}}" placeholder="Type your passport number">
										</div>
										<label class="col-sm-2 control-label">
											Date Of Issue <span class="symbol required"></span>
										</label>
										<div class="col-sm-4">
											<input type="text" class="form-control issue-datepicker" id="passdateofissue" name="passdateofissue" value="{{ isset($user_data[0]['passport_detail']['pass_issue_date']) ? date('d-m-Y',strtotime($user_data[0]['passport_detail']['pass_issue_date'])) : ''}}" placeholder="dd-mm-yyyy">
										</div>
									</div>
								</div>

								<div class="form-group">
									<div class="row no-margin">
										<label class="col-sm-2 control-label">
											Date Of Expiry <span class="symbol required"></span>
										</label>
										<div class="col-sm-4">
											<input type="text" class="form-control datepicker" id="passdateofexp" name="passdateofexp" value="{{ isset($user_data[0]['passport_detail']['pass_expiry_date']) ? date('d-m-Y',strtotime($user_data[0]['passport_detail']['pass_expiry_date'])) : ''}}" placeholder="dd-mm-yyyy">
										</div>
										<label class="col-sm-2 control-label">
											Place Of Issue <span class="symbol required"></span>
										</label>
										<div class="col-sm-4">
											<input type="text" class="form-control" id="passplace" name="passplace" value="{{ isset($user_data[0]['passport_detail']['place_of_issue']) ? $user_data[0]['passport_detail']['place_of_issue'] : ''}}" placeholder="Type name">
										</div>
									</div>
								</div>

								<div class="form-group">
									<div class="row no-margin">
										<label class="col-sm-2 control-label">
											Passport Country <span class="symbol required"></span>
										</label>
										<div class="col-sm-4">
											<select class="form-control" id="passcountry" name="passcountry">
											  	<option value="">Select Passport Country</option>
			                                    @foreach( \CommonHelper::countries() as $c_index => $country)
			                                        <option value="{{ $c_index }}" {{ !empty($user_data[0]['passport_detail']) ? $user_data[0]['passport_detail']['pass_country'] == $c_index ? 'selected' : '' : ''  }}> {{ $country }}</option>
			                                    @endforeach
											</select>
										</div>
									</div>
								</div>

								<div class="form-group">
									<div class="row no-margin">
										<label class="col-sm-2 control-label">
											US Visa <span class="symbol required"></span>
										</label>
										<div class="col-sm-4">
		                                    <label class="radio-inline">
											  	<input type="radio" name="us" value="1" {{ !empty($user_data[0]['passport_detail']) ? $user_data[0]['passport_detail']['us_visa'] == 1 ? 'checked' : '' : ''}}> Yes</label>
											<label class="radio-inline">
											  	<input type="radio" name="us" value="0" {{ !empty($user_data[0]['passport_detail']) ? $user_data[0]['passport_detail']['us_visa'] == 0 ? 'checked' : '' : 'checked'}}> No</label>
										</div>
										@if(isset($user_data[0]['passport_detail']['us_visa']) && ($user_data[0]['passport_detail']['us_visa'] == 1))
											<label class="col-sm-2 control-label us-block">
												US Visa Expiry Date <span class="symbol required"></span>
											</label>
											<div class="col-sm-4">
												<input type="text" class="form-control datepicker us-block" id="usvalid" name="usvalid" value="{{ isset($user_data[0]['passport_detail']['us_visa_expiry_date']) ? date('d-m-Y',strtotime($user_data[0]['passport_detail']['us_visa_expiry_date'])) : ''}}" placeholder="dd-mm-yyyy">
											</div>
										@else
											<label class="col-sm-2 control-label us-block hide">
											US Visa Expiry Date <span class="symbol required"></span>
											</label>
											<div class="col-sm-4">
												<input type="text" class="form-control datepicker us-block hide" id="usvalid" name="usvalid" value="" placeholder="dd-mm-yyyy">
											</div>
										@endif
									</div>
								</div>

								<div class="form-group">
									<div class="row no-margin">
										<label class="col-sm-2 control-label">
											Indian PCC <span class="symbol required"></span>
										</label>
										<div class="col-sm-4">
											<label class="radio-inline">
											  	<input type="radio" name="pcc" value="1" {{ isset($user_data[0]['passport_detail']['indian_pcc']) ? $user_data[0]['passport_detail']['indian_pcc'] == 1 ? 'checked' : '' : 'checked'}}> Yes</label>
											<label class="radio-inline">
											  	<input type="radio" name="pcc" value="0" {{ isset($user_data[0]['passport_detail']['indian_pcc']) ? $user_data[0]['passport_detail']['indian_pcc'] == 0 ? 'checked' : '1' : '2'}}> No</label>
										</div>

										@if(isset($user_data[0]['passport_detail']['indian_pcc']) && ($user_data[0]['passport_detail']['indian_pcc'] == 0))
											<label class="col-sm-2 control-label pcc-block hide">
												Indian PCC Issue Date <span class="symbol required"></span>
											</label>
											<div class="col-sm-4">
												<input type="text" class="form-control issue-datepicker pcc-block hide" id="pccdate" name="pccdate" value="" placeholder="dd-mm-yyyy">
											</div>
										@else
											<label class="col-sm-2 control-label pcc-block">
											Indian PCC Issue Date <span class="symbol required"></span>
											</label>
											<div class="col-sm-4">
												<input type="text" class="form-control issue-datepicker pcc-block" id="pccdate" name="pccdate" value="{{ isset($user_data[0]['passport_detail']['indian_pcc_issue_date']) ? date('d-m-Y',strtotime($user_data[0]['passport_detail']['indian_pcc_issue_date'])) : ''}}" placeholder="dd-mm-yyyy">
											</div>
										@endif
									</div>
								</div>

								<div class="form-group">
									<div class="row no-margin">
										<label class="col-sm-2 control-label">
											Yellow Fever Vaccination? <span class="symbol required"></span>
										</label>
										<div class="col-sm-4">
											<label class="radio-inline">
												<input type="radio" name="yellowfever" id="yellowfever" value="1" {{!empty($user_data[0]['wkfr_detail']) ? $user_data[0]['wkfr_detail']['yellow_fever'] == '1'? 'checked' : '' : 'checked' }}> Yes
												</label>
												<label class="radio-inline">
												  <input type="radio" name="yellowfever" id="yellowfever" value="0" {{!empty($user_data[0]['wkfr_detail']) ? $user_data[0]['wkfr_detail']['yellow_fever'] == '0'? 'checked' : '' : '' }}> No
											</label>
										</div>
										@if(isset($user_data[0]['wkfr_detail']['yellow_fever']) && ($user_data[0]['wkfr_detail']['yellow_fever'] == 0))
							                <label class="col-sm-2 control-label yellowfever-block hide" for="yf_issue_date">Yellow Fever Issue Date<span class="symbol required"></span></label>
											<div class="col-sm-4">
							                	<input type="text" class="form-control issue-datepicker yellowfever-block hide" id="yf_issue_date" name="yf_issue_date" value="{{ isset($user_data[0]['wkfr_detail']['yf_issue_date']) ? date('d-m-Y',strtotime($user_data[0]['wkfr_detail']['yf_issue_date'])) : ''}}" placeholder="dd-mm-yyyy">
							                </div>
							            @else
							                <label class="col-sm-2 control-label yellowfever-block" for="yf_issue_date">Yellow Fever Issue Date<span class="symbol required"></span></label>
						                	<div class="col-sm-4">
						                    	<input type="text" class="form-control issue-datepicker yellowfever-block" id="yf_issue_date" name="yf_issue_date" value="{{ isset($user_data[0]['wkfr_detail']['yf_issue_date']) ? date('d-m-Y',strtotime($user_data[0]['wkfr_detail']['yf_issue_date'])) : ''}}" placeholder="dd-mm-yyyy">
						                    </div>
						                    
								        @endif
								    </div>
								</div>

								<?php 

									if(in_array('FROMO', $required_fields_for_selected_rank))
									{
										$hide_fromo = '';
									}
									else{
										$hide_fromo = 'hide';
									}
								?>

								<div class="form-group {{$hide_fromo}}" id="FROMO-details">
									<div class="row no-margin">
										<label class="col-sm-2 control-label">
											Experience Of Framo<span class="symbol required"></span>
										</label>
										<div class="col-sm-4">
											<label class="radio-inline">
											  	<input type="radio" name="fromo" value="1" {{!empty($user_data[0]['passport_detail']['fromo']) ? $user_data[0]['passport_detail']['fromo'] == '1'? 'checked' : '' : '' }}> Yes
											</label>
											<label class="radio-inline">
											  	<input type="radio" name="fromo" value="0" {{!empty($user_data[0]['passport_detail']['fromo']) ? $user_data[0]['passport_detail']['fromo'] == '0'? 'checked' : '' : 'checked' }}> No
											</label>
										</div>
									</div>
								</div>

								<div class="form-group">
									<div class="row no-margin">
										<label class="col-sm-2 control-label">
											Indos Number </span>
										</label>
										<div class="col-sm-4">
											<input type="text" class="form-control" id="indosno" name="indosno" value="{{ isset($user_data[0]['wkfr_detail']['indos_number']) ? $user_data[0]['wkfr_detail']['indos_number'] : ''}}" placeholder="Type the number">
										</div>
									</div>
								</div>

								<div class="panel-heading">
									<h4 class="panel-title">SEAMAN BOOK DETAIL</h4>
								</div>
								
								@if(isset($user_data[0]['seaman_book_detail']) && count($user_data[0]['seaman_book_detail']) > 0)
									@foreach($user_data[0]['seaman_book_detail'] as $index => $data)
										<?php 
											$cdc_verified = '';
											$cdc_unverified = '';
											if(isset($data['status'])){
												if($data['status'] == '1'){
													$cdc_verified = 'active';
												}else{
													$cdc_unverified = 'active';
												}
											}
											else{
												$cdc_unverified = 'active';
											}
										?>
										<div id="cdc-template" class="multiple-info">
										<div class="row">
											<div class="col-xs-12">
												<div class="col-xs-4 col-sm-2 no-padding">
													<div class="input-label p-t-10 cdc-name cdc-name-0 pull-left">CDC Details {{$index+1}}</div>
												</div>
												<div class="col-xs-8 col-sm-10 no-padding">
													@if($index > 0)
														<div class="pull-right close-button cdc-close-button p-t-10 p-l-5 cdc-close-button-0" data-type='cdc'>
				                                        <i class="fa fa-times" aria-hidden="true"></i>
				                                    </div>
			                                        @else
														<div class="pull-right close-button cdc-close-button hide p-t-10 p-l-5 cdc-close-button-0" data-type='cdc'>
				                                        <i class="fa fa-times" aria-hidden="true"></i>
				                                    </div>
			                                        @endif
													<div class="pull-right cdc-status-tab" style="width: 150px;">
														<div class="col-xs-6 no-padding no-margin">
															<label class="control-label">Is Verified</span></label>
														</div>
														<div class="col-xs-6 no-padding no-margin">
														<select class="form-control" name="cdc_status[{{$index}}]">
														  	<option value="1" {{ !empty($data['status']) ? $data['status'] == '1' ? 'selected' : '' : ''  }}>
														  	Yes
														  	</option>
														  	<option value="0" {{ !empty($data['status']) ? $data['status'] == '0' ? 'selected' : '' : 'selected'  }}>No</option>
						                                   
														</select>
														</div>
										            </div>
												</div>
											</div>
										</div>
										<div class="row form-group">
											<input type="hidden" class="total-count-cdc">
											<div class="col-sm-3">
												<div>
								                    <label class="input-label" for="number">CDC Number<span class="symbol required"></span></label>
								                    <input type="text" class="form-control cdcno" name="cdcno[{{$index}}]" value="{{ isset($data['cdc_number']) ? $data['cdc_number'] : ''}}" placeholder="Type alphanumeric number">
								                 </div>
											</div>
											<div class="col-sm-3">
												<div>
													<label class="input-label">CDC Issue Date<span class="symbol required"></span></label>
													<input type="text" class="form-control issue-datepicker cdc_issue"  name="cdc_issue[{{$index}}]" value="{{ isset($data['cdc_issue_date']) ? date('d-m-Y',strtotime($data['cdc_issue_date'])) : ''}}" placeholder="dd-mm-yyyy">
												</div>
											</div>
											<div class="col-sm-3">
												<div>
													<label class="input-label">CDC Expiry Date<span class="symbol required"></span></label>
													<input type="text" class="form-control datepicker cdc_exp"  name="cdc_exp[{{$index}}]" value="{{ isset($data['cdc_expiry_date']) ? date('d-m-Y',strtotime($data['cdc_expiry_date'])) : ''}}" placeholder="dd-mm-yyyy">
												</div>
											</div>
											<div class="col-sm-3">
												<div>
													<label class="input-label">Issuing Authority<span class="symbol required"></span></label>
													<select class="form-control cdccountry" name="cdccountry[{{$index}}]">
													  	<option value="">Select Country</option>
					                                    @foreach( \CommonHelper::countries() as $c_index => $country)
					                                        <option value="{{ $c_index }}" {{ !empty($data['cdc']) ? $data['cdc'] == $c_index ? 'selected' : '' : ''  }}> {{ $country }}</option>
					                                    @endforeach
													</select>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-3">
												<div>
													<label class="input-label" for="cdc_ver">CDC Verification Date<span class="symbol required"></span></label>
													<input type="text" class="form-control issue-datepicker cdc_ver" name="cdc_ver[{{$index}}]" value="{{ isset($data['cdc_verification_date']) ? date('d-m-Y',strtotime($data['cdc_verification_date'])) : ''}}" placeholder="dd-mm-yyyy">
												</div>
											</div>
										</div>
										<hr>
										</div>
									@endforeach
								@else
									<div id="cdc-template">
										<div class="row no-margin">
											<div class="col-xs-12">
												<div class="input-label p-t-10 cdc-name cdc-name-0 pull-left">CDC Details 1</div>
												<div class="pull-right close-button cdc-close-button hide p-t-10 cdc-close-button-0" data-type='cdc'>
			                                        <i class="fa fa-times" aria-hidden="true"></i>
			                                    </div>
											</div>
										</div>
										<div class="row form-group no-margin">
											<input type="hidden" class="total-count-cdc">
											<div class="col-sm-3">
												<div>
								                    <label class="input-label" for="number">CDC Number<span class="symbol required"></span></label>
								                    <input type="text" class="form-control cdcno" name="cdcno[0]" value="{{ isset($user_data[0]['seaman_book_detail']['cdc_number']) ? $user_data[0]['seaman_book_detail']['cdc_number'] : ''}}" placeholder="Type alphanumeric number">
								                 </div>
											</div>
											<div class="col-sm-3">
												<div>
													<label class="input-label">CDC Issue Date<span class="symbol required"></span></label>
													<input type="text" class="form-control issue-datepicker cdc_issue"  name="cdc_issue[0]" value="{{ isset($data['cdc_issue_date']) ? date('d-m-Y',strtotime($data['cdc_issue_date'])) : ''}}" placeholder="dd-mm-yyyy">
												</div>
											</div>
											<div class="col-sm-3">
												<div>
													<label class="input-label">CDC Expiry Date<span class="symbol required"></span></label>
													<input type="text" class="form-control datepicker cdc_exp"  name="cdc_exp[0]" value="{{ isset($user_data[0]['seaman_book_detail']['cdc_expiry_date']) ? date('d-m-Y',strtotime($user_data[0]['seaman_book_detail']['cdc_expiry_date'])) : ''}}" placeholder="dd-mm-yyyy">
												</div>
											</div>
											<div class="col-sm-3">
												<div>
													<label class="input-label">Issuing Authority<span class="symbol required"></span></label>
													<select class="form-control cdccountry" name="cdccountry[0]">
													  	<option value="">Select Country</option>
					                                    @foreach( \CommonHelper::countries() as $c_index => $country)
					                                        <option value="{{ $c_index }}" {{ !empty($user_data[0]['seaman_book_detail']) ? $user_data[0]['seaman_book_detail']['cdc'] == $c_index ? 'selected' : '' : ''  }}> {{ $country }}</option>
					                                    @endforeach
													</select>
												</div>
											</div>
										</div>
										<div class="row">©∫
											<div class="col-sm-3">
												<div>
													<label class="input-label" for="cdc_ver">CDC Verification Date<span class="symbol required"></span></label>
													<input type="text" class="form-control issue-datepicker cdc_ver" name="cdc_ver[0]" value="{{ isset($data['cdc_verification_date']) ? date('d-m-Y',strtotime($data['cdc_verification_date'])) : ''}}" placeholder="dd-mm-yyyy">
												</div>
											</div>
										</div>
										<hr>
									</div>
								@endif
								<div class="row no-margin" id="add-more-cdc-row">
		                            <div class="col-sm-12">
		                                <button type="button" data-form-id="seafarer-certificates-form" class="btn add-more-button add-cdc-button pull-right" id="add-cdc">Add CDC</button>
		                            </div>
		                        </div>
								
								<?php 

									if(in_array('COP-Optional', $required_fields_for_selected_rank)){
										$hide_cop = 'hide';
									}
									else{
										$hide_cop = '';
									}
									if(in_array('COP', $required_fields_name)){
										$div_class = '';
									}else{
										$div_class = 'hide';
									}
								?>

								<div id='COP-details' class="{{$div_class}}">
									<div class="panel-heading">
										<h4 class="panel-title">CERTIFICATE OF PROFICIENCY</h4>
									</div>
							
									@if(isset($user_data[0]['cop_detail']) && count($user_data[0]['cop_detail']) > 0)
										@foreach($user_data[0]['cop_detail'] as $index => $data)
											<div id="cop-template" class="multiple-info">
												<div class="row">
													<input type="hidden" class="total-count-cop">
													<div class="col-xs-12">
														<div class="input-label p-t-10 cop-name pull-left">COP Details {{$index+1}}</div>
														@if($index > 0)
															<div class="pull-right close-button cop-close-button p-t-10 cop-close-button-0" data-type='cop'>
					                                        <i class="fa fa-times" aria-hidden="true"></i>
					                                    	</div>
				                                        @else
															<div class="pull-right close-button cop-close-button hide p-t-10 cop-close-button-0" data-type='cop'>
					                                        <i class="fa fa-times" aria-hidden="true"></i>
					                                    	</div>
				                                        @endif
													</div>
												</div>
												<div class="row">
													<div class="col-sm-3">
														<div>
										                    <label class="input-label" for="cop_no">COP Number<span class="symbol required {{$hide_cop}}"></span></label>
										                    <input type="text" class="form-control cop_no" name="cop_no[{{$index}}]" value="{{ isset($data['cop_number']) ? $data['cop_number'] : ''}}" placeholder="Type alphanumeric number">
										                 </div>
													</div>
													<div class="col-sm-3">
														<div>
										                    <label class="input-label" for="grade">COP Grade<span class="symbol required {{$hide_cop}}"></span></label>
										                    <input type="text" class="form-control cop_grade" name="cop_grade[{{$index}}]" value="{{ isset($data['cop_grade']) ? $data['cop_grade'] : ''}}" placeholder="Type your grade">
										                 </div>
													</div>
													<div class="col-sm-3">
														<div>
										                    <label class="input-label" for="cop_exp">Issue Date<span class="symbol required {{$hide_cop}}"></span></label>
										                    <input type="text" class="form-control issue-datepicker cop_issue" name="cop_issue[{{$index}}]" value="{{ isset($data['cop_issue_date']) ? date('d-m-Y',strtotime($data['cop_issue_date'])) : ''}}" placeholder="dd-mm-yyyy">
										                 </div>
													</div>
													<div class="col-sm-3">
														<div>
										                    <label class="input-label" for="cop_exp">Expiry Date<span class="symbol required {{$hide_cop}}"></span></label>
										                    <input type="text" class="form-control datepicker cop_exp" name="cop_exp[{{$index}}]" value="{{ isset($data['cop_exp_date']) ? date('d-m-Y',strtotime($data['cop_exp_date'])) : ''}}" placeholder="dd-mm-yyyy">
										                 </div>
													</div>
												</div>
												<hr>
											</div>
										@endforeach
									@else
										<div id="cop-template">
												<div class="row no-margin">
													<input type="hidden" class="total-count-cop">
													<div class="col-xs-12">
														<div class="input-label p-t-10 cop-name pull-left">COP Details 1</div>
														<div class="pull-right close-button cop-close-button hide p-t-10 cop-close-button-0" data-type='cop'>
					                                        <i class="fa fa-times" aria-hidden="true"></i>
					                                    </div>
													</div>
												</div>
												<div class="row no-margin">
													<div class="col-sm-3">
														<div>
										                    <label class="input-label" for="cop_no">COP Number<span class="symbol required {{$hide_cop}}"></span></label>
										                    <input type="text" class="form-control cop_no" name="cop_no[0]" value="{{ isset($user_data[0]['cop_detail']['cop_number']) ? $user_data[0]['cop_detail']['cop_number'] : ''}}" placeholder="Type alphanumeric number">
										                 </div>
													</div>
													<div class="col-sm-3">
														<div>
										                    <label class="input-label" for="grade">COP Grade<span class="symbol required {{$hide_cop}}"></span></label>
										                    <input type="text" class="form-control cop_grade" name="cop_grade[0]" value="{{ isset($user_data[0]['cop_detail']['cop_grade']) ? $user_data[0]['cop_detail']['cop_grade'] : ''}}" placeholder="Type your grade">
										                 </div>
													</div>
													<div class="col-sm-3">
														<div>
										                    <label class="input-label" for="cop_exp">Issue Date<span class="symbol required {{$hide_cop}}"></span></label>
										                    <input type="text" class="form-control issue-datepicker cop_issue" name="cop_issue[0]" value="{{ isset($user_data[0]['cop_detail']['cop_issue_date']) ? date('d-m-Y',strtotime($user_data[0]['cop_detail']['cop_issue_date'])) : ''}}" placeholder="dd-mm-yyyy">
										                 </div>
													</div>
													<div class="col-sm-3">
														<div>
										                    <label class="input-label" for="cop_exp">Expiry Date<span class="symbol required {{$hide_cop}}"></span></label>
										                    <input type="text" class="form-control datepicker cop_exp" name="cop_exp[0]" value="{{ isset($user_data[0]['cop_detail']['cop_exp_date']) ? date('d-m-Y',strtotime($user_data[0]['cop_detail']['cop_exp_date'])) : ''}}" placeholder="dd-mm-yyyy">
										                 </div>
													</div>
												</div>
												<hr>
										</div>
									@endif
									<div class="row no-margin" id="add-more-cop-row">
			                            <div class="col-sm-12">
			                                <button type="button" data-form-id="seafarer-certificates-form" class="btn add-more-button add-cop-button pull-right" id="add-cop">Add COP</button>
			                            </div>
			                        </div>
			                    </div>
								
								<?php 
							
									if(in_array('COC-Optional', $required_fields_for_selected_rank)){
										$hide_coc = 'hide';
									}
									else{
										$hide_coc = '';
									}
									if(in_array('COC', $required_fields_name)){
										$div_class_coc = '';
									}else{
										$div_class_coc = 'hide';
									}
								?>
								<div id='COC-details' class="required_fields {{$div_class_coc}}">
			                        <div class="panel-heading">
										<h4 class="panel-title">CERTIFICATE OF COMPENTENCY</h4>
									</div>

									@if(isset($user_data[0]['coc_detail']) && count($user_data[0]['coc_detail']) > 0)
										@foreach($user_data[0]['coc_detail'] as $index => $data)
											<?php 
												$coc_verified = '';
												$coc_unverified = '';
												if(isset($data['status'])){
													if($data['status'] == '1'){
														$coc_verified = 'active';
													}else{
														$coc_unverified = 'active';
													}
												}
												else{
													$coc_unverified = 'active';
												}
											?>
											<div id="coc-template" class="multiple-info"> 
												<div class="row">
													<input type="hidden" class="total-count-coc">
													<div class="col-xs-12">
														<div class="input-label p-t-10 coc-name pull-left">COC Details {{$index+1}}</div>
														@if($index > 0)
															<div class="pull-right close-button coc-close-button p-t-10 p-l-5 coc-close-button-0" data-type='coc'>
						                                        <i class="fa fa-times" aria-hidden="true"></i>
						                                    </div>
			                                            @else
															<div class="pull-right close-button coc-close-button hide p-t-10 p-l-5 coc-close-button-0" data-type='coc'>
						                                        <i class="fa fa-times" aria-hidden="true"></i>
						                                    </div>
			                                            @endif
			                                            <div class="pull-right coc-status-tab" style="width: 150px;">
															<div class="col-xs-6 no-padding no-margin">
																<label class="control-label">Is Verified</span></label>
															</div>
															<div class="col-xs-6 no-padding no-margin">
															<select class="form-control" name="coc_status[{{$index}}]">
															  	<option value="1" {{ !empty($data['status']) ? $data['status'] == '1' ? 'selected' : '' : ''  }}>
															  	Yes
															  	</option>
															  	<option value="0" {{ !empty($data['status']) ? $data['status'] == '0' ? 'selected' : '' : 'selected'  }}>No</option>
							                                   
															</select>
															</div>
											            </div>
													</div>
												</div>
												<div class="row form-group">
													<div class="col-sm-3">
														<div>
															<label class="input-label">COC<span class="symbol required {{$hide_coc}}"></span></label>
															<select class="form-control coc_country" name="coc_country[{{$index}}]">
															  	<option value="">Select COC Country</option>
							                                    @foreach( \CommonHelper::countries() as $c_index => $country)
							                                        <option value="{{ $c_index }}" {{ !empty($data['coc']) ? $data['coc'] == $c_index ? 'selected' : '' : ''  }}> {{ $country }}</option>
							                                    @endforeach
															</select>
														</div>
													</div>
													<div class="col-sm-3">
														<div>
										                    <label class="input-label" for="coc_no">COC Number<span class="symbol required {{$hide_coc}}"></span></label>
										                    <input type="text" class="form-control coc_no" name="coc_no[{{$index}}]" value="{{ isset($data['coc_number']) ? $data['coc_number'] : ''}}" placeholder="Type alphanumeric number">
										                 </div>
													</div>
													<div class="col-sm-3">
														<div>
										                    <label class="input-label" for="grade">COC Grade<span class="symbol required {{$hide_coc}}"></span></label>
										                    <input type="text" class="form-control grade" name="grade[{{$index}}]" value="{{ isset($data['coc_grade']) ? $data['coc_grade'] : ''}}" placeholder="Type your grade">
										                 </div>
													</div>
													<div class="col-sm-3">
														<div>
										                    <label class="input-label" for="coc_exp">COC Expiry Date<span class="symbol required {{$hide_coc}}"></span></label>
										                    <input type="text" class="form-control datepicker coc_exp" name="coc_exp[{{$index}}]" value="{{ isset($data['coc_expiry_date']) ? date('d-m-Y',strtotime($data['coc_expiry_date'])) : ''}}" placeholder="dd-mm-yyyy">
										                 </div>
													</div>
												</div>
												<div class="row">
													<div class="col-sm-3">
														<div>
															<label class="input-label" for="coc_ver">COC Verification Date<span class="symbol required {{$hide_coc}}"></span></label>
															<input type="text" class="form-control issue-datepicker coc_ver" name="coc_ver[{{$index}}]" value="{{ isset($data['coc_verification_date']) ? date('d-m-Y',strtotime($data['coc_verification_date'])) : ''}}" placeholder="dd-mm-yyyy">
														</div>
													</div>
												</div>
												<hr>
											</div>
										@endforeach
									@else
										<div id="coc-template" class="multiple-info">
											<div class="row">
												<input type="hidden" class="total-count-coc">
												<div class="col-xs-12">
													<div class="input-label p-t-10 coc-name pull-left">COC Details 1</div>
													<div class="pull-right close-button coc-close-button hide p-t-10 coc-close-button-0" data-type='coc'>
				                                        <i class="fa fa-times" aria-hidden="true"></i>
				                                    </div>
												</div>
											</div>
											<div class="row form-group">
												<div class="col-sm-3">
													<div>
														<label class="input-label">COC<span class="symbol required {{$hide_coc}}"></span></label>
														<select class="form-control coc_country" name="coc_country[0]">
														  	<option value="">Select COC Country</option>
						                                    @foreach( \CommonHelper::countries() as $c_index => $country)
						                                        <option value="{{ $c_index }}" {{ !empty($user_data[0]['coc_detail']['coc']) ? $user_data[0]['coc_detail']['coc'] == $c_index ? 'selected' : '' : ''  }}> {{ $country }}</option>
						                                    @endforeach
														</select>
													</div>
												</div>
												<div class="col-sm-3">
													<div>
									                    <label class="input-label" for="coc_no">COC Number<span class="symbol required {{$hide_coc}}"></span></label>
									                    <input type="text" class="form-control coc_no" name="coc_no[0]" value="{{ isset($user_data[0]['coc_detail']['coc_number']) ? $user_data[0]['coc_detail']['coc_number'] : ''}}" placeholder="Type alphanumeric number">
									                 </div>
												</div>
												<div class="col-sm-3">
													<div>
									                    <label class="input-label" for="grade">COC Grade<span class="symbol required {{$hide_coc}}"></span></label>
									                    <input type="text" class="form-control grade" name="grade[0]" value="{{ isset($user_data[0]['coc_detail']['coc_grade']) ? $user_data[0]['coc_detail']['coc_grade'] : ''}}" placeholder="Type your grade">
									                 </div>
												</div>
												<div class="col-sm-3">
													<div>
									                    <label class="input-label" for="coc_exp">COC Expiry Date<span class="symbol required {{$hide_coc}}"></span></label>
									                    <input type="text" class="form-control datepicker coc_exp" name="coc_exp[0]" value="{{ isset($user_data[0]['coc_detail']['coc_expiry_date']) ? date('d-m-Y',strtotime($user_data[0]['coc_detail']['coc_expiry_date'])) : ''}}" placeholder="dd-mm-yyyy">
									                 </div>
												</div>
											</div>
											<div class="row">
												<div class="col-sm-3">
													<div>
														<label class="input-label" for="coc_ver">COC Verification Date<span class="symbol required {{$hide_coc}}"></span></label>
														<input type="text" class="form-control issue-datepicker coc_ver" name="coc_ver[0}]" value="{{ isset($data['coc_verification_date']) ? date('d-m-Y',strtotime($data['coc_verification_date'])) : ''}}" placeholder="dd-mm-yyyy">
													</div>
												</div>
											</div>
											<hr>
										</div>
									@endif
									<div class="row no-margin" id="add-more-coc-row">
			                            <div class="col-sm-12">
			                                <button type="button" data-form-id="seafarer-certificates-form" class="btn add-more-button add-coc-button pull-right" id="add-coc">Add COC</button>
			                            </div>
			                        </div>
			                    </div>

			                    <?php 
									if(in_array('COE', $required_fields_name)){
										$div_class_coe = '';
									}else{
										$div_class_coe = 'hide';
									}
								?>
								
								<div id='COE-details' class="required_fields {{$div_class_coe}}">
			                        <div class="panel-heading">
										<h4 class="panel-title">CERTIFICATE OF ENDORSEMENT</h4>
									</div>
									
									@if(isset($user_data[0]['coe_detail']) && count($user_data[0]['coe_detail']) > 0)
										@foreach($user_data[0]['coe_detail'] as $index => $data)
											<div id="coe-template">
											<div class="row no-margin">
												<div class="col-xs-12">
													<div class="input-label p-t-10 coe-name coe-name-0 pull-left">COE Details {{$index+1}}</div>
													@if($index > 0)
														<div class="pull-right close-button coe-close-button p-t-10 coe-close-button-0" data-type='coe'>
				                                        <i class="fa fa-times" aria-hidden="true"></i>
				                                    </div>
			                                        @else
														<div class="pull-right close-button coe-close-button hide p-t-10 coe-close-button-0" data-type='coe'>
				                                        <i class="fa fa-times" aria-hidden="true"></i>
				                                    </div>
			                                        @endif
												</div>
											</div>
											<div class="row no-margin">
												<input type="hidden" class="total-count-coe">
												<div class="col-sm-3">
													<div>
														<label class="input-label">COE</span></label>
														<select class="form-control coecountry" name="coecountry[{{$index}}]">
														  	<option value="">Select COE Country</option>
						                                    @foreach( \CommonHelper::countries() as $c_index => $country)
						                                        <option value="{{ $c_index }}" {{ !empty($data['coe']) ? $data['coe'] == $c_index ? 'selected' : '' : ''  }}> {{ $country }}</option>
						                                    @endforeach
														</select>
													</div>
												</div>
												<div class="col-sm-3">
													<div>
									                    <label class="input-label" for="number">COE Number</span></label>
									                    <input type="text" class="form-control coeno" name="coeno[{{$index}}]" value="{{ isset($data['coe_number']) ? $data['coe_number'] : ''}}" placeholder="Type alphanumeric number">
									                 </div>
												</div>
												<div class="col-sm-3">
													<div>
									                    <label class="input-label" for="grade">COE Grade</span></label>
									                    <input type="text" class="form-control coe_grade" name="coe_grade[{{$index}}]" value="{{ isset($data['coe_grade']) ? $data['coe_grade'] : ''}}" placeholder="Type your grade">
									                 </div>
												</div>
												<div class="col-sm-3">
													<div>
														<label class="input-label">COE Expiry Date</span></label>
														<input type="text" class="form-control datepicker coe_exp"  name="coe_exp[{{$index}}]" value="{{ isset($data['coe_expiry_date']) ? date('d-m-Y',strtotime($data['coe_expiry_date'])) : ''}}" placeholder="dd-mm-yyyy">
													</div>
												</div>
											</div>
											<hr>
											</div>
										@endforeach
									@else
										<div id="coe-template">
											<div class="row no-margin">
												<div class="col-xs-12">
													<div class="input-label p-t-10 coe-name coe-name-0 pull-left">COE Details 1</div>
													<div class="pull-right close-button coe-close-button hide p-t-10 coe-close-button-0" data-type='coe'>
				                                        <i class="fa fa-times" aria-hidden="true"></i>
				                                    </div>
												</div>
											</div>
											<div class="row no-margin">
												<input type="hidden" class="total-count-coe">
												<div class="col-sm-3">
													<div>
														<label class="input-label">COE</span></label>
														<select class="form-control coecountry" name="coecountry[0]">
														  	<option value="">Select COE Country</option>
						                                    @foreach( \CommonHelper::countries() as $c_index => $country)
						                                        <option value="{{ $c_index }}" {{ !empty($user_data[0]['coe_detail']) ? $user_data[0]['coe_detail']['coe'] == $c_index ? 'selected' : '' : ''  }}> {{ $country }}</option>
						                                    @endforeach
														</select>
													</div>
												</div>
												<div class="col-sm-3">
													<div>
									                    <label class="input-label" for="number">COE Number</span></label>
									                    <input type="text" class="form-control coeno" name="coeno[0]" value="{{ isset($user_data[0]['coe_detail']['coe_number']) ? $user_data[0]['coe_detail']['coe_number'] : ''}}" placeholder="Type alphanumeric number">
									                 </div>
												</div>
												<div class="col-sm-3">
													<div>
									                    <label class="input-label" for="grade">COE Grade</span></label>
									                    <input type="text" class="form-control coe_grade" name="coe_grade[0]" value="{{ isset($user_data[0]['coe_detail']['coe_grade']) ? $user_data[0]['coe_detail']['coe_grade'] : ''}}" placeholder="Type your grade">
									                 </div>
												</div>
												<div class="col-sm-3">
													<div>
														<label class="input-label">COE Expiry Date</span></label>
														<input type="text" class="form-control datepicker coe_exp"  name="coe_exp[0]" value="{{ isset($user_data[0]['coe_detail']['coe_expiry_date']) ? date('d-m-Y',strtotime($user_data[0]['coe_detail']['coe_expiry_date'])) : ''}}" placeholder="dd-mm-yyyy">
													</div>
												</div>
											</div>
											<hr>
										</div>
									@endif
									<div class="row no-margin" id="add-more-coe-row">
			                            <div class="col-sm-12">
			                                <button type="button" data-form-id="seafarer-certificates-form" class="btn add-more-button add-coe-button pull-right" id="add-coe">Add COE</button>
			                            </div>
			                        </div>
			                    </div>
								
								<?php 
									if(in_array('GMDSS', $required_fields_name)){
										$div_class_gmdss = '';
									}else{
										$div_class_gmdss = 'hide';
									}

									if(in_array('GMDSS-Optional', $required_fields_for_selected_rank)){
										$hide_gmdss = 'hide';
									}
									else{
										$hide_gmdss = '';
									}
								?>

								<div id='GMDSS-details' class="required_fields {{$div_class_gmdss}}">
			                        <div class="panel-heading">
										<h4 class="panel-title">GMDSS DETAILS</h4>
									</div>

									<div class="m-l-r-0 m-b-15">
										<div class="row no-margin">
											<div class="col-sm-4">
												<div>
													<label class="input-label">GMDSS<span class="symbol required {{$hide_gmdss}}"></span></label>
													<select class="form-control" id="gmdss_country" name="gmdss_country">
														<option value="">Select GMDSS Country</option>
														@foreach( \CommonHelper::countries() as $c_index => $country)
															<option value="{{ $c_index }}" {{ !empty($user_data[0]['gmdss_detail']) ? $user_data[0]['gmdss_detail']['gmdss'] == $c_index ? 'selected' : '' : ''  }}> {{ $country }}</option>
														@endforeach
													</select>
												</div>
											</div>
											<div class="col-sm-4">
												<div>
													<label class="input-label" for="gmdss_no">GMDSS Number<span class="symbol required {{$hide_gmdss}}"></span></label>
													<input type="text" class="form-control" id="gmdss_no" name="gmdss_no" value="{{ isset($user_data[0]['gmdss_detail']['gmdss_number']) ? $user_data[0]['gmdss_detail']['gmdss_number'] : ''}}" placeholder="Type alphanumeric number">
												</div>
											</div>
											<div class="col-sm-4">
												<div>
													<label class="input-label" for="gmdss_doe">GMDSS Expiry Date<span class="symbol required {{$hide_gmdss}}"></span></label>
													<input type="text" class="form-control datepicker" id="gmdss_doe" name="gmdss_doe" value="{{ isset($user_data[0]['gmdss_detail']['gmdss_expiry_date']) ? date('d-m-Y',strtotime($user_data[0]['gmdss_detail']['gmdss_expiry_date'])) : ''}}" placeholder="dd-mm-yyyy">
												</div>
											</div>
										</div>
									</div>

									<div class="panel-heading gmdss_endorsement {{isset($user_data[0]['gmdss_detail']['gmdss']) && $user_data[0]['gmdss_detail']['gmdss'] == $india_value ? '' : 'hide'}}">
										<h4 class="panel-title">GMDSS ENDORSEMENT</h4>
									</div>
									<div class="m-l-r-0 m-b-15">
										<div class="row no-margin">
											<div class="col-sm-4 gmdss_endorsement {{isset($user_data[0]['gmdss_detail']['gmdss']) && $user_data[0]['gmdss_detail']['gmdss'] == $india_value ? '' : 'hide'}}">
												<div>
													<label class="input-label" for="gmdss_endorsement">GMDSS Endorsement Number</label>
													<input type="text" class="form-control" name="gmdss_endorsement" value="{{ isset($user_data[0]['gmdss_detail']['gmdss_endorsement_number']) ? $user_data[0]['gmdss_detail']['gmdss_endorsement_number'] : ''}}" placeholder="Please Enter Number">
												</div>
											</div>
											<div class="col-sm-4 gmdss_valid_till {{isset($user_data[0]['gmdss_detail']['gmdss']) && $user_data[0]['gmdss_detail']['gmdss'] == $india_value ? '' : 'hide'}}">
												<div>
													<label class="input-label" for="gmdss_valid_till">Valid Till</label>
													<input type="text" class="form-control datepicker" name="gmdss_valid_till" value="{{ isset($user_data[0]['gmdss_detail']['gmdss_endorsement_expiry_date']) ? date('d-m-Y',strtotime($user_data[0]['gmdss_detail']['gmdss_endorsement_expiry_date'])) : ''}}" placeholder="dd-mm-yyyy">
												</div>
											</div>
										</div>
									</div>
								</div>

								<?php 
									$hide_wk = '';
									if(!in_array('WATCH_KEEPING-Optional', $required_fields_for_selected_rank)){
										$hide_wk = '';
									}
									else{
										$hide_wk = 'hide';
									}

									if(in_array('WATCH_KEEPING', $required_fields_name)){
										$div_class_wk = '';
									}else{
										$div_class_wk = 'hide';
									}
								?>
								
								<div id='WATCH_KEEPING-details' class="{{$div_class_wk}}">
									<div class="panel-heading">
										<h4 class="panel-title">WATCH KEEPING FOR RATING</h4>
										<div class="pull-right coc-status-tab" style="width: 150px;margin-top: -15px;">
											<div class="col-xs-6 no-padding no-margin">
												<label class="control-label">Is Verified</span></label>
											</div>
											<div class="col-xs-6 no-padding no-margin">
												<select class="form-control" name="wk_status">
												  	<option value="1" {{ !empty($user_data[0]['wkfr_detail']['status']) ? $user_data[0]['wkfr_detail']['status'] == '1' ? 'selected' : '' : ''  }}>
												  	Yes
												  	</option>
												  	<option value="0" {{ !empty($user_data[0]['wkfr_detail']['status']) ? $user_data[0]['wkfr_detail']['status'] == '0' ? 'selected' : '' : 'selected'}}>No</option>
												</select>
											</div>
							            </div>
									</div>

									<div class="row no-margin">
										<div class="col-sm-3">
											<div>
												<label class="input-label">Country<span class="symbol required {{$hide_wk}}"></span></label>
												<select class="form-control" id="watch_country" name="watch_country">
												  <option value="">Select watch keeping Country</option>
				                                    @foreach( \CommonHelper::countries() as $c_index => $country)
				                                        <option value="{{ $c_index }}" {{ !empty($user_data[0]['wkfr_detail']) ? $user_data[0]['wkfr_detail']['watch_keeping'] == $c_index ? 'selected' : '' : ''  }}> {{ $country }}</option>
				                                    @endforeach
												</select>
											</div>
										</div>
										<div class="col-sm-3">
											<div>
							                    <label class="input-label" for="watchissud">Issue Date<span class="symbol required {{$hide_wk}}"></span></label>
							                    <input type="text" class="form-control issue-datepicker" id="watchissud" name="watchissud" value="{{ isset($user_data[0]['wkfr_detail']['issue_date']) ? date('d-m-Y',strtotime($user_data[0]['wkfr_detail']['issue_date'])) : ''}}" placeholder="dd-mm-yyyy">
							                 </div>
										</div>
										<div class="col-sm-3">
											<div>
							                    <label class="input-label" for="watch_no">Number<span class="symbol required {{$hide_wk}}"></span></label>
							                    <input type="text" class="form-control" id="watch_no" name="watch_no" value="{{ isset($user_data[0]['wkfr_detail']['wkfr_number']) ? $user_data[0]['wkfr_detail']['wkfr_number'] : ''}}" placeholder="Type number">
							                 </div>
										</div>
										<div class="col-sm-3">
											<div>
												<label class="input-label" for="deckengine">Deck/Engine<span class="symbol required {{$hide_wk}}"></span></label>
												<select id="deckengine" name="deckengine" class="form-control selectpicker">
			                                        <option value="" label="Select">Select</option>
			                                        <option value="Deck" {{ !empty($user_data[0]['wkfr_detail']) ? $user_data[0]['wkfr_detail']['type'] == 'Deck' ? 'selected' : '' : ''}}>Deck</option>
			                                        <option value="Engine" {{ !empty($user_data[0]['wkfr_detail']) ? $user_data[0]['wkfr_detail']['type'] == 'Engine'? 'selected' : '' : ''}}>Engine</option>
			                                    </select>
											</div>
										</div>
									</div>
								</div>
						
								<div class="form-group m-t-15">
									<div class="col-sm-2 col-sm-offset-3 no-margin">
										<button class="btn btn-light-grey back-step btn-block">
											<i class="fa fa-circle-arrow-left"></i> Back
										</button>
									</div>
									<div class="col-sm-2 col-sm-offset-3 pull-right">
										<button type="button" data-style="zoom-in" class="btn btn-blue next-step btn-block document-details-submit seafarer-next-btn ladda-button" data-form="step-2" data-type='imp_documents'>
											Save & Next <i class="fa fa-arrow-circle-right"></i>
										</button>
									</div>
								</div>

							</div>
							<div id="step-4">

								<div class="row">
									<div class="col-sm-12">
										<div class="heading">
											ACADEMICS AND PROFESSIONAL QUALIFICATION
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="col-sm-4">
										<div>
											<label class="input-label" for="name_of_school">Name of School / College</label>
					                    	<input type="text" class="form-control" id="name_of_school" name="name_of_school" value="{{ isset($user_data[0]['professional_detail']['name_of_school']) ? $user_data[0]['professional_detail']['name_of_school'] : ''}}" placeholder="Type your School/College name">
					                    </div>
					                </div>
					               <!--  <div class="col-sm-3">
										<div>
											<label class="input-label" for="school_from">From</label>
					                    	<select id="school_from" name="school_from" class="form-control">
												<option value="">Select Year From</option>
												@for($i = '1980' ; $i < 2018 ; $i++)
													<option value="{{$i}}" {{isset( $user_data[0]['professional_detail']['school_from']) ? $i == $user_data[0]['professional_detail']['school_from'] ? 'selected' : '' : ''}}>{{$i}}</option>
												@endfor
											</select>
					                    </div>
					                </div> -->
					                <div class="col-sm-4">
										<div>
											<label class="input-label" for="school_to">Pass Out Year</label>
					                    	<select id="school_to" name="school_to" class="form-control">
												<option value="">Select Year</option>
												@for($i = '1980' ; $i < 2018 ; $i++)
													<option value="{{$i}}" {{isset( $user_data[0]['professional_detail']['school_to']) ? $i == $user_data[0]['professional_detail']['school_to'] ? 'selected' : '' : ''}}>{{$i}}</option>
												@endfor
											</select>
					                    </div>
					                </div>
					                <div class="col-sm-4">
										<div>
											<label class="input-label" for="school_qualification">Highest Qualification</label>
					                    	<input type="text" class="form-control" id="school_qualification" name="school_qualification" value="{{ isset($user_data[0]['professional_detail']['school_qualification']) ? $user_data[0]['professional_detail']['school_qualification'] : ''}}" placeholder="Type your highest qualification">
					                    </div>
					                </div>
								</div>
								<div class="row">
									<div class="col-sm-12 m-t-15">
										<div class="heading">
											PRE SEA TRAINING / APPRENTICE SHIP
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="col-sm-4">
										<div>
											<label class="input-label" for="institute_name">Name of Institute / College</label>
					                    	<input type="text" class="form-control" id="institute_name" name="institute_name" value="{{ isset($user_data[0]['professional_detail']['institute_name']) ? $user_data[0]['professional_detail']['institute_name'] : ''}}" placeholder="Type your Institute name">
					                    </div>
					                </div>
					                <!-- <div class="col-sm-3">
										<div>
											<label class="input-label" for="institute_from">From</label>
					                    	<select id="institute_from" name="institute_from" class="form-control">
												<option value="">Select Year From</option>
												@for($i = '1980' ; $i < 2018 ; $i++)
													<option value="{{$i}}" {{isset($user_data[0]['professional_detail']['institute_from']) ? $i == $user_data[0]['professional_detail']['institute_from'] ? 'selected' : '' : ''}}>{{$i}}</option>
												@endfor
											</select>
					                    </div>
					                </div> -->
					                <div class="col-sm-4">
										<div>
											<label class="input-label" for="institute_to">Pass Out Year</label>
					                    	<select id="institute_to" name="institute_to" class="form-control">
												<option value="">Select Year</option>
												@for($i = '1980' ; $i < 2018 ; $i++)
													<option value="{{$i}}" {{isset($user_data[0]['professional_detail']['institute_to']) ? $i == $user_data[0]['professional_detail']['institute_to'] ? 'selected' : '' : ''}}>{{$i}}</option>
												@endfor
											</select>
					                    </div>
					                </div>
					                <div class="col-sm-4">
					                	<div>
											<label class="input-label" for="institute_degree">Type of Degree</label>
					                    	<input type="text" class="form-control" id="institute_degree" name="institute_degree" value="{{ isset($user_data[0]['professional_detail']['institute_degree']) ? $user_data[0]['professional_detail']['institute_degree'] : ''}}" placeholder="Type your degree name">
					                    </div>
					                </div>
								</div>

								@if(Route::currentRouteName() != 'admin.add.seafarer')
									<h2 class="StepTitle">Courses Details</h2>
									
									<div id="seafarer_courses_section">
										<input type="hidden" class="existing_course_id" name="existing_course_id">
										<div class="row no-margin">
						                    <div class="form-group">
						                    	<div class="col-sm-4">
								                    <label class="input-label" for="certificate_name">Course Type<span class="symbol required"></span></label>
							    	                <select name="seafarer_course_type" id="seafarer_course_type" class="form-control search-select seafarer_course_type" block-index="0">
														<option value=''>Select Your Course Type</option>
														<option value="normal"> Normal Course</option>												
														<option value="value_added"> Value Added</option>
													</select>
								                 </div>
						                    
												<div class="col-sm-4">
								                    <label class="input-label" for="certificate_name">Certificate Name<span class="symbol required"></span></label>
							    	                <select name="certificate_name" class="form-control search-select certificate_name" block-index="0">
														<option value=''>Select Your Certificate</option>
														@foreach( \CommonHelper::courses() as $c_index => $course)
															<option value="{{ $c_index }}" class="seafarer_courses normal_course hide"> {{ $course }}</option>
														@endforeach
														@foreach( \CommonHelper::value_added_courses() as $v_index => $v_course)
															<option value="{{ $v_index }}" class="seafarer_courses value_added_course hide"> {{ $v_course }}</option>
														@endforeach
													</select>
								                 </div>
											
												<div class="col-sm-4">
													<label class="input-labelf_issue">Certificate Number</label>
													<input type="text" class="form-control certificate_no" name="certificate_no" value="{{ isset($courses['certification_number']) ? $courses['certification_number'] : ''}}" placeholder="Type your certificate number">
												</div>
											</div>
										</div>
										<div class="row no-margin">
											<div class="form-group">
												<div class="col-sm-4">
								                    <label class="input-label_issue">Date Of Issue</span><span class="symbol required"></label>
							    	                <input type="text" class="form-control issue-datepicker date_of_issue" name="date_of_issue" value="{{ isset($courses['issue_date']) ? date('d-m-Y',strtotime($courses['issue_date'])) : ''}}" placeholder="dd-mm-yyyy">
								                 </div>
											
												<div class="col-sm-4">
								                    <label class="input-label_expiry">Date Of Expiry</label>
							    	                <input type="text" class="form-control datepicker date_of_expiry" name="date_of_expiry" value="{{ isset($courses['expiry_date']) ? date('d-m-Y',strtotime($courses['expiry_date'])) : ''}}" placeholder="dd-mm-yyyy">
							    	                <span class="f-14 note-clr-gray">Note: Leave blank for unlimited expiry date</span>
								                 </div>
											
												<div class="col-sm-4">
													<label class="input-labelf_issue">Issued By</span></label>
													<input type="text" class="form-control issue_by" name="issue_by" value="{{ isset($courses['issue_by']) ? $courses['issue_by'] : ''}}" placeholder="Type your issued by institute">
												</div>
											</div>
										</div>
									</div>

									<div class="row">
			                            <div class="col-sm-12">
			                                <button type="button" data-form-id="seafarer-certificates-form" class="btn add-seafarer-course-button add-more-button ladda-button pull-right m-b-15 " data-style="zoom-in">Add Certificate</button>
			                            </div>
			                        </div>
			                    @endif

		                        <h2 class="StepTitle">Courses</h2>
		                        @if(Route::currentRouteName() == 'admin.add.seafarer')
		                        	<?php
										$courses = \CommonHelper::courses();
									?>

									<div class="row no-margin">
											<div class="form-group">
												<div class="col-sm-4 col-xs-8">		
													<label>Course Name</label>
												</div>
												<div class="col-sm-2 col-xs-4">
													<label>Date Of Issue</label>
												</div>
												<div class="col-sm-4 col-xs-8">		
													<label>Course Name</label>
												</div>
												<div class="col-sm-2 col-xs-4">
													<label>Date Of Issue</label>
												</div>
											</div>
									</div>
									@for($i=1;  $i <= (count($courses)); $i++)
										<div class="row no-margin">
											<div class="form-group">
												<div class="col-sm-4 col-xs-8 p-t-10">		
													<input type="checkbox" name="checkbox" class="course_checkbox" data-id="{{$i}}">
													<label class="f-14">
														{{$courses[$i]}}
													</label>
												</div>
												<div class="col-sm-2 col-xs-4">
													<div class="hide date_of_issue_{{$i}}" data-check="off">
														<input type="text" name="course_date_of_issue[{{$i}}]" class="form-control issue-datepicker" placeholder="dd-mm-yyyy">
													</div> 
												</div>
												<?php
													$i++;
												?>
												@if($i < count($courses))
													<div class="col-sm-4 col-xs-8 p-t-10">		
														<input type="checkbox" name="checkbox" class="course_checkbox" data-id="{{$i}}">
														<label class="f-14">
															{{$courses[$i]}}
														</label>
													</div>
													<div class="col-sm-2 col-xs-4">
														<div class="hide date_of_issue_{{$i}}" data-check="off">
															<input type="text" name="course_date_of_issue[{{$i}}]" class="form-control issue-datepicker" placeholder="dd-mm-yyyy">
														</div> 
													</div>
												@endif
											</div>
										</div>
									@endfor
								@else
			                        <?php $course_count= '0';?>
		                        	<div class="normal_course_section">
										@if(isset($user_data[0]['course_detail']) && !empty($user_data[0]['course_detail']))
											<?php $normal_course_count = 1; ?>
											@foreach($user_data[0]['course_detail'] as $index => $courses)
												<div class="row course_detail_row course_detail_row_{{$courses['id']}}" data-course-id={{$courses['id']}}>
													<div class="col-sm-12">
														<div class="sub-details-container add-more-section">
															<div class="content-container">
																<div class="row">
																	<div class="col-sm-12">
																		<div class="title m-b-5 display-flex-center">
																			<div class="normal-course-name">
																				Certificate {{$normal_course_count}}
																			</div>
																			<div class="sea-service-buttons">
																				<div class="course-edit-button" data-id={{$courses['id']}}>
										                                    	    <i class="fa fa-edit" aria-hidden="true" title="edit"></i>
										                                    	</div>
																				<div class="course-close-button" data-id={{$courses['id']}}>
									                                    	    <i class="fa fa-times" aria-hidden="true" title="delete"></i>
										                                    	</div>
																			</div>
																			
								                                    	</div>
																	</div>
																</div>
															</div>
															<div class="row sea_service_details_section">
																<div class="col-xs-12 col-sm-6 col-md-4">
																	<div class="discription">
																		<span class="content-head">Certificate Name:</span>
																		<span class="content">
																			@foreach( \CommonHelper::courses() as $c_index => $course)
																				{{ isset($courses['course_id']) ? $courses['course_id'] == $c_index ? $course : '' : ''}}
																			@endforeach
								                                        </span>
																	</div>
																</div>
															
																<div class="col-xs-12 col-sm-6 col-md-4">
																	<div class="discription">
																		<span class="content-head">Certificate Number:</span>
																		<span class="content">
																			{{ isset($courses['certification_number']) ? $courses['certification_number'] : '-'}}
								                                        </span>
																	</div>
																</div>
															
																<div class="col-xs-12 col-sm-6 col-md-4">
																	<div class="discription">
																		<span class="content-head">Date Of Issue:</span>
																		<span class="content">
																			{{ isset($courses['issue_date']) ? date('d-m-Y',strtotime($courses['issue_date'])) : '-'}}
								                                        </span>
																	</div>
																</div>

																<div class="col-xs-12 col-sm-6 col-md-4">
																	<div class="discription">
																		<span class="content-head">Date Of Expiry:</span>
																		<span class="content">
																			{{ isset($courses['expiry_date']) && !empty($courses['expiry_date']) ? date('d-m-Y',strtotime($courses['expiry_date'])) : '-'}}
								                                        </span>
																	</div>
																</div>

																<div class="col-xs-12 col-sm-6 col-md-4">
																	<div class="discription">
																		<span class="content-head">Issued By:</span>
																		<span class="content">
																			{{ isset($courses['issue_by']) && !empty($courses['issue_by']) ? $courses['issue_by'] : '-'}}
								                                        </span>
																	</div>
																</div>

															</div>
														</div>
													</div>
												</div>
												<?php $normal_course_count++; ?>
											@endforeach
										@else
											<div class="row">
												<div class="col-xs-12 text-center">
													<div class="discription">
														<span class="content-head">No Data Found</span>
													</div>
												</div>
											</div>
										@endif
									</div>
								@endif

								<h2 class="StepTitle">Add-on Courses</h2>

								<?php
									$value_added_courses = \CommonHelper::value_added_courses();
								?>
								@if(Route::currentRouteName() == 'admin.add.seafarer')
									<div class="row no-margin">
										<div class="form-group">
											<div class="col-sm-4 col-xs-8">		
												<label>Course Name</label>
											</div>
											<div class="col-sm-2 col-xs-4">
												<label>Date Of Issue</label>
											</div>
											<div class="col-sm-4 col-xs-8">		
												<label>Course Name</label>
											</div>
											<div class="col-sm-2 col-xs-4">
												<label>Date Of Issue</label>
											</div>
										</div>
									</div>
									@for($i=1;  $i <= (count($value_added_courses)); $i++)
										<div class="row no-margin">
											<div class="form-group">
												<div class="col-sm-4 col-xs-8 p-t-10">		
													<input type="checkbox" name="checkbox" class="value_added_course_checkbox" data-id="{{$i}}">
													<label class="f-14">
														{{ $value_added_courses[$i] }}
													</label>
												</div>
												<div class="col-sm-2 col-xs-4">
													<div class="hide value_added_date_of_issue_{{$i}}" data-check="off">
														<input type="text" name="value_added_course_date_of_issue[{{$i}}]" class="form-control issue-datepicker" placeholder="dd-mm-yyyy">
													</div> 
												</div>
												<?php
													$i++;
												?>
												@if($i < count($value_added_courses))
													<div class="col-sm-4 col-xs-8 p-t-10">		
														<input type="checkbox" name="checkbox" class="value_added_course_checkbox" data-id="{{$i}}">
														<label class="f-14">
															{{ $value_added_courses[$i] }}
														</label>
													</div>
													<div class="col-sm-2 col-xs-4">
														<div class="hide value_added_date_of_issue_{{$i}}" data-check="off">
															<input type="text" name="value_added_course_date_of_issue[{{$i}}]" class="form-control issue-datepicker" placeholder="dd-mm-yyyy">
														</div> 
													</div>
												@endif
											</div>
										</div>
									@endfor
								@else
									<div class="value_added_course_section">
										@if(isset($user_data[0]['value_added_course_detail']) && !empty($user_data[0]['value_added_course_detail']))
											<?php $value_added_course_count = 1; ?>
											@foreach($user_data[0]['value_added_course_detail'] as $index => $courses)
												<div class="row course_detail_row course_detail_row_{{$courses['id']}}" data-course-id="{{$courses['id']}}">
													<div class="col-sm-12">
														<div class="sub-details-container add-more-section">
															<div class="content-container">
																<div class="row">
																	<div class="col-sm-12">
																		<div class="title m-b-5 display-flex-center">
																			<div class="value-added-course-name">
																				Certificate {{$value_added_course_count}}
																			</div>
																			<div class="sea-service-buttons">
																				<div class="course-edit-button" data-id={{$courses['id']}}>
										                                    	    <i class="fa fa-edit" aria-hidden="true" title="edit"></i>
										                                    	</div>
																				<div class="course-close-button" data-id={{$courses['id']}}>
									                                    	    <i class="fa fa-times" aria-hidden="true" title="delete"></i>
										                                    	</div>
																			</div>
																			
								                                    	</div>
																	</div>
																</div>
															</div>
															<div class="row sea_service_details_section">
																<div class="col-xs-12 col-sm-6 col-md-4">
																	<div class="discription">
																		<span class="content-head">Certificate Name:</span>
																		<span class="content">
																			@foreach( \CommonHelper::value_added_courses() as $c_index => $course)
																				{{ isset($courses['course_id']) ? $courses['course_id'] == $c_index  ? $course : '' : ''}}
																			@endforeach
								                                        </span>
																	</div>
																</div>
															
																<div class="col-xs-12 col-sm-6 col-md-4">
																	<div class="discription">
																		<span class="content-head">Certificate Number:</span>
																		<span class="content">
																			{{ isset($courses['certification_number']) && !empty($courses['certification_number']) ? $courses['certification_number'] : '-'}}
								                                        </span>
																	</div>
																</div>
															
																<div class="col-xs-12 col-sm-6 col-md-4">
																	<div class="discription">
																		<span class="content-head">Date Of Issue:</span>
																		<span class="content">
																			{{ isset($courses['issue_date']) && !empty($courses['issue_date']) ? date('d-m-Y',strtotime($courses['issue_date'])) : '-'}}
								                                        </span>
																	</div>
																</div>

																<div class="col-xs-12 col-sm-6 col-md-4">
																	<div class="discription">
																		<span class="content-head">Date Of Expiry:</span>
																		<span class="content">
																			{{ isset($courses['expiry_date']) && !empty($courses['expiry_date']) ? date('d-m-Y',strtotime($courses['expiry_date'])) : '-'}}
								                                        </span>
																	</div>
																</div>

																<div class="col-xs-12 col-sm-6 col-md-4">
																	<div class="discription">
																		<span class="content-head">Issued By:</span>
																		<span class="content">
																			{{ isset($courses['issue_by']) && !empty($courses['issue_by']) ? $courses['issue_by'] : '-'}}
								                                        </span>
																	</div>
																</div>

															</div>
														</div>
													</div>
												</div>
												<?php $value_added_course_count++; ?>
											@endforeach
										@else
											<div class="row">
												<div class="col-xs-12 text-center">
													<div class="discription">
														<span class="content-head">No Data Found</span>
													</div>
												</div>
											</div>
										@endif
									</div>
								@endif

		                        <h2 class="StepTitle">Other Experience</h2>
		                        <div class="form-group">
									<div class="col-sm-12">
										<textarea class="form-control" name="other_exp" rows="5" placeholder="Type your other experience">{{ isset($user_data[0]['professional_detail']['other_exp']) ? $user_data[0]['professional_detail']['other_exp'] : ''}}</textarea>
									</div>
								</div>

								<div class="form-group">
									<div class="col-sm-2 col-sm-offset-3 no-margin">
										<button class="btn btn-light-grey back-step btn-block">
											<i class="fa fa-circle-arrow-left"></i> Back
										</button>
									</div>
									<div class="col-sm-2 col-sm-offset-3">
										<button class="btn btn-light-grey forth-step-skip btn-block">
											<i class="fa fa-circle-arrow-left"></i> Skip
										</button>
									</div>
									
									<div class="col-sm-3 pull-right">
										<button type="button" data-style="zoom-in" class="btn btn-success btn-block next-step pull-right seafarer-next-btn ladda-button" data-form="step-4" data-type='course_details'>
											Finish & View Profile <i class="fa fa-arrow-circle-right"></i>
										</button>
									</div>
									<div class="col-sm-2 pull-right">
										<button type="button" data-style="zoom-in" class="btn btn-success btn-block next-step pull-right seafarer-next-btn ladda-button" data-redirect="finish" data-form="step-4" data-type='course_details'>
											Finish <i class="fa fa-arrow-circle-right"></i>
										</button>
									</div>
								</div>
							</div>
							<div id="step-3">
								<h2 class="StepTitle">Sea Service Details</h2>
								<input type="hidden" name="exiting_service_id" class="exiting_service_id">
								<div id="sea-service-template" class="sea-service-template multiple-info">
									<input type="hidden" class="service_total_block_index">
		                            <div class="form-group">
		                                <div class="col-xs-12">
		                                    <div class="input-label p-t-10 pull-left">Sea Service 1</div>
		                                    <div class="pull-right sea-service-close-button hide sea-service-close-button-0">
		                                        <i class="fa fa-times" aria-hidden="true"></i>
		                                    </div>
		                                </div>
		                            </div>
		                            <div class="form-group">
										<div class="col-md-4">
											<div>
												<label class="input-label" for="shipping_company">Shipping Company<span class="symbol required"></span></label>
												<input type="text" class="form-control shipping_company" name="shipping_company[0]" value="" placeholder="Type shipping company name">
											</div>
										</div>
										<div class="col-md-4">
											<div>
												<label class="input-label" for="name_of_ship">Name Of Ship<span class="symbol required"></span></label>
												<input type="text" class="form-control name_of_ship" id="name_of_ship" name="name_of_ship[0]" value="" placeholder="Type your ship name">
											</div>
										</div>
										<div class="col-sm-4">
											<div>
							                    <label class="input-label" for="ship_type">Ship Type<span class="symbol required"></span></label>
						    	                <select name="ship_type[0]" class="form-control ship_type" block-index="0">
													<option value=''>Select Your ship type</option>
													@foreach( \CommonHelper::ship_type() as $c_index => $type)
														<option value="{{ $c_index }}" {{ isset($user_data[0]['personal_detail']['type']) ? $user_data[0]['personal_detail']['type'] == $c_index ? 'selected' : '' : ''}}> {{ $type }}</option>
													@endforeach
												</select>
							                 </div>
										</div>

		                            </div>
									<div class="form-group">
										
										<div class="col-sm-4">
											<div>
												<label class="input-label" for="grt">GRT</span></label>
												<input type="text" class="form-control grt" name="grt[0]" value="" placeholder="Type your grt">
											</div>
										</div>
										<div class="col-sm-4">
											<div>
							                    <label class="input-label" for="bhp[0]">BHP</span></label>
						    	                <input type="text" class="form-control bhp" name="bhp[0]" value="" placeholder="Type your bhp">
							                 </div>
										</div>
										<div class="col-sm-4">
											<div>
							                    <label class="input-label" for="rank">Rank<span class="symbol required"></span></label>
							                    <select name="rank[0]" class="form-control rank" block-index="0">
							                    <option value=''>Select Rank</option>
													@foreach(\CommonHelper::new_rank() as $index => $category)
												    	<optgroup label="{{$index}}">
												    	@foreach($category as $r_index => $rank)
				                                        	<option value="{{$r_index}}" {{ isset($user_data[0]['personal_detail']['type']) ? $user_data[0]['personal_detail']['type'] == $r_index ? 'selected' : '' : ''}}>{{$rank}} </option>
				                                        @endforeach
				                                    @endforeach
												</select>
							                 </div>
										</div>
									</div>

									<div class="form-group">
										<div class="col-sm-4">
											<div>
												<label class="input-label" for="date_from">Sign On Date<span class="symbol required"></span></label>
												<input type="text" class="form-control issue-datepicker date_from" name="date_from[0]" value="" placeholder="dd-mm-yyyy">
											</div>
										</div>
										<div class="col-sm-4">
											<div>
												<label class="input-label" for="date_to">Sign Off Date<span class="symbol required"></span></label>
												<input type="text" class="form-control issue-datepicker date_to" name="date_to[0]" value="" placeholder="dd-mm-yyyy">
											</div>
										</div>
										<div class="col-sm-4">
											<div>
							                    <label class="input-label">Ship Flag</span></label>
						    	                <select name="ship_flag[0]" class="form-control ship_flag" block-index="0">
													<option value=''>Select Ship Flag</option>
													@foreach( \CommonHelper::countries() as $c_index => $name)
														<option value="{{ $c_index }}"> {{ $name }}</option>
													@endforeach
												</select>
							                 </div>
										</div>
									</div>
									<div class="form-group">
										<div class="col-sm-4">
						                    <label class="input-label" for="engine_type[0]">Engine Type</span></label>
					    	                <select name="engine_type[0]" class="form-control engine_type check_engine_type" block-index="0">
												<option value=''>Select Your engine type</option>
												@foreach( \CommonHelper::engine_type() as $c_index => $type)
													<option value="{{ $c_index }}" {{ isset($user_data[0]['personal_detail']['type']) ? $user_data[0]['personal_detail']['type'] == $c_index ? 'selected' : '' : ''}}> {{ $type }}</option>
												@endforeach
											</select>
										</div>
										<div class="col-sm-4 hide other_engine_type-0" id="other_engine_type">
											<div>
							                    <label class="input-label" for="other_engine_type">Other Engine Type<span class="symbol required"></span></label>
						    	                <input type="text" class="form-control other_engine_type" name="other_engine_type[0]" placeholder="Type your engine type">
							                 </div>
										</div>
									</div>
									<hr>
								</div>

								<div class="form-group" id="add-more-service-row">
		                            <div class="col-sm-12">
		                                <button type="button" data-form-id="seafarer-sea-service-form-form" class="btn add-sea-service-button pull-right add-more-button ladda-button" data-style="zoom-in">Add Sea Service</button>
		                            </div>
		                        </div>

								<div>
									<div class="row">
										<div class="col-sm-12">
											<div class="heading">
												SEA SERVICES
											</div>
										</div>
									</div>
									<div class="sea_service_detail_section">
									@if(isset($user_data[0]['sea_service_detail']) && !empty($user_data[0]['sea_service_detail']))
										<?php $show_skip_button = 'hide'; ?>
										@foreach($user_data[0]['sea_service_detail'] as $index => $services)
											<div class="row sea_service_detail_row_{{$services['id']}}">
												<div class="col-sm-12">
													<div class="sub-details-container add-more-section">
														<div class="content-container">
															<div class="row">
																<div class="col-sm-12">
																	<div class="title m-b-5 display-flex-center">
																		<div class="sea-service-name">
																			Sea Service {{$index+1}}
																		</div>
								                                    	<div class="sea-service-buttons">
																			<div class="sea-service-edit-button" data-index-id={{$index}} data-service-id={{$services['id']}}>
									                                    	    <i class="fa fa-edit" aria-hidden="true" title="edit"></i>
									                                    	</div>

																			<div class="sea-service-close-button" data-id={{$services['id']}} data-service-id={{$services['id']}}>
								                                    	    <i class="fa fa-times" aria-hidden="true" title="delete"></i>
									                                    	</div>
																		</div>
							                                    	</div>
																</div>
															</div>
														</div>
														<div class="row sea_service_details_section">
															<div class="col-xs-12 col-sm-6 col-md-4">
																<div class="discription">
																	<span class="content-head">Shipping Company:</span>
																	<span class="content">
																		{{ isset($services['company_name']) ? $services['company_name'] : ''}}
							                                        </span>
																</div>
															</div>
															<div class="col-xs-12 col-sm-6 col-md-4">
																<div class="discription">
																	<span class="content-head">Name Of Ship:</span>
																	<span class="content">
																		{{ isset($services['ship_name']) ? $services['ship_name'] : '-'}}
																	</span>
																</div>
															</div>
															<div class="col-xs-12 col-sm-6 col-md-4">
																<div class="discription">
																	<span class="content-head">Ship Type:</span>
																	<span class="content">
																	@foreach( \CommonHelper::ship_type() as $c_index => $type)
																		{{ isset($services['ship_type']) ? $services['ship_type'] == $c_index ? $type : '' : ''}}
																	@endforeach
																	</span>
																</div>
															</div>
															<div class="col-xs-12 col-sm-6 col-md-4">
																<div class="discription">
																	<span class="content-head">Rank:</span>
																	<span class="content">
																	@foreach(\CommonHelper::new_rank() as $index => $category)
																    	@foreach($category as $r_index => $rank)
								                                        	{{ isset($services['rank_id']) ? $services['rank_id'] == $r_index ? $rank : '' : ''}}
								                                        @endforeach
								                                    @endforeach
																	</span>
																</div>
															</div>
															<div class="col-xs-12 col-sm-6 col-md-4">
																<div class="discription">
																	<span class="content-head">GRT:</span>
																	<span class="content">
																		{{ !empty($services['grt']) ? $services['grt'] : '-'}}
																	</span>
																</div>
															</div>
															<div class="col-xs-12 col-sm-6 col-md-4">
																<div class="discription">
																	<span class="content-head">BHP:</span>
																	<span class="content">
																		{{ !empty($services['bhp']) ? $services['bhp'] : '-'}}
																	</span>
																</div>
															</div>
															<div class="col-xs-12 col-sm-6 col-md-4">
																<div class="discription">
																	<span class="content-head">Engine Type:</span>
																	<span class="content">
																	@if(isset($services['engine_type']) && !empty($services['engine_type']))
																		@if(isset($services['engine_type']) && $services['engine_type'] == 'other')
																			{{ isset($services['other_engine_type']) ? $services['other_engine_type'] : '-'}}
																		@else
																			@foreach( \CommonHelper::engine_type() as $c_index => $type)
																				{{ isset($services['engine_type']) ? $services['engine_type'] == $c_index ? $type  : '' : ''}}
																			@endforeach
																		@endif
																	@else
																		-
																	@endif
																</select>
																	</span>
																</div>
															</div>
															
															<div class="col-xs-12 col-sm-6 col-md-4">
																<div class="discription">
																	<span class="content-head">Sign On Date:</span>
																	<span class="content">
																		{{ isset($services['from']) ? date('d-m-Y',strtotime($services['from'])) : ''}}
																	</span>
																</div>
															</div>
															<div class="col-xs-12 col-sm-6 col-md-4">
																<div class="discription">
																	<span class="content-head">Sign Off Date:</span>
																	<span class="content">
																		{{ isset($services['to']) ? date('d-m-Y',strtotime($services['to'])) : ''}}
																	</span>
																</div>
															</div>
															<div class="col-xs-12 col-sm-6 col-md-4">
																<div class="discription">
																	<span class="content-head">Ship Flag:</span>
																	<span class="content">
																		@if(isset($services['ship_flag']) && !empty($services['ship_flag']))
																			@foreach( \CommonHelper::countries() as $c_index => $name)
																				{{ isset($services['ship_flag']) ? $services['ship_flag'] == $c_index ? $name : '' : ''}}
																			@endforeach
																		@else
																			-
																		@endif
																	</span>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										@endforeach
									@else
										<?php $show_skip_button = ''; ?>
										<div class="row no-data-found">
											<div class="col-xs-12 text-center">
												<div class="discription">
													<span class="content-head">No Data Found</span>
												</div>
											</div>
										</div>
									@endif
									</div>
								</div>

								<div class="form-group m-t-15">
									<div class="col-sm-2 col-sm-offset-3 no-margin">
										<button class="btn btn-light-grey back-step btn-block">
											<i class="fa fa-circle-arrow-left"></i> Back
										</button>
									</div>
									<div class="col-sm-2 pull-right">
										<button type="button" data-style="zoom-in" class="btn btn-blue btn-block ladda-button three-step-skip">
											Next <i class="fa fa-arrow-circle-right"></i>
										</button>
									</div>
									<div class="col-sm-2 col-sm-offset-3 no-margin pull-right">
										<button class="btn btn-light-grey three-step-skip btn-block {{$show_skip_button}}">
											<i class="fa fa-circle-arrow-left"></i> Skip
										</button>
									</div>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

@stop

@section('js_script')
	<script>
		var required_fields = <?php echo json_encode($required_fields) ?>;
	</script>
	<script type="text/javascript" src="/js/site/registration.js"></script>
	<script>
		jQuery(document).ready(function() {
			SeafarerFormWizard.init();
		});
	</script>
@stop