<html>
    <tr>
        <td align="center"><b>Sr. No</b></td>
        <td align="center"><b>Name</b></td>
        <td align="center"><b>Phone</b></td>
        <td align="center"><b>Email</b></td>
        <td align="center"><b>Current Rank</b></td>
        <td align="center"><b>Applied Rank</b></td>
        <td align="center"><b>Date Of Birth</b></td>
        <td align="center"><b>Age</b></td>
        <td align="center"><b>Location</b></td>
        <td align="center"><b>CDC Type</b></td>
        <td align="center"><b>COC Type</b></td>
        <td align="center"><b>Ship Type</b></td>
    </tr>
    @foreach($data as $index => $seafarer)
        <tr>
            <td>{{$index+1}}</td>
            <td>{{isset($seafarer['first_name']) ? $seafarer['first_name'] : '-'}}</td>
            <td>{{isset($seafarer['mobile']) ? $seafarer['mobile'] : '-'}}</td>
            <td>{{isset($seafarer['email']) ? $seafarer['email'] : '-'}}</td>
            <td>
                @foreach(\CommonHelper::new_rank() as $rank_index => $category)
                    @foreach($category as $r_index => $rank)
                        {{ isset($seafarer['professional_detail']['current_rank']) ? $seafarer['professional_detail']['current_rank'] == $r_index ? $rank : '' : ''}}
                    @endforeach
                @endforeach
            </td>
            <td>
                @foreach(\CommonHelper::new_rank() as $rank_index => $category)
                    @foreach($category as $r_index => $rank)
                        {{ isset($seafarer['professional_detail']['applied_rank']) ? $seafarer['professional_detail']['applied_rank'] == $r_index ? $rank : '' : ''}}
                    @endforeach
                @endforeach
            </td>
            <td>
                {{ isset($seafarer['personal_detail']['dob'])? date('d-m-Y',strtotime($seafarer['personal_detail']['dob'])) : '-' }}
            </td>
            <td>
                <?php
                    if(isset($seafarer['personal_detail']['dob']))
                        $age = date_diff(date_create($seafarer['personal_detail']['dob']), date_create('today'))->y;

                ?>
                {{ isset($age) ? $age : '-' }}
            </td>
            <td>

                @if(isset($seafarer['personal_detail']['city_id']) OR isset($seafarer['personal_detail']['city_text']))
                    {{ isset($seafarer['personal_detail']['city_id']) ? $seafarer['personal_detail']['pincode']['pincodes_cities'][0]['city']['name'] : (isset($seafarer['personal_detail']['city_text']) ? $seafarer['personal_detail']['city_text'] : '')}},

                    {{ isset($seafarer['personal_detail']['state_id']) ? $seafarer['personal_detail']['pincode']['pincodes_states'][0]['state']['name'] : (isset($seafarer['personal_detail']['state_text']) ? $seafarer['personal_detail']['state_text'] : '')}},

                    @foreach( \CommonHelper::countries() as $c_index => $country)
                        {{ isset($seafarer['personal_detail']['country']) ? $seafarer['personal_detail']['country'] == $c_index ? $country : '' : ''}}
                    @endforeach

                    {{ isset($seafarer['personal_detail']['pincode_text']) ? $seafarer['personal_detail']['pincode_text'] : ''}}.
                @else
                    -
                @endif
            </td>
            <td>
                @if(isset($seafarer['seaman_book_detail']) AND !empty($seafarer['seaman_book_detail']))
                    <?php
                        $count = count($seafarer['seaman_book_detail'])-1;
                        $ind = 0;
                    ?>
                    @foreach($seafarer['seaman_book_detail'] as $cdc)
                        @foreach( \CommonHelper::countries() as $c_index => $country)
                            {{ isset($cdc['cdc']) ? $cdc['cdc'] == $c_index ? $country : '' : ''}}
                            @if($cdc['cdc'] == $c_index AND ( $count != $ind))
                                ,
                                <?php $ind++ ?>
                            @endif
                        @endforeach
                    @endforeach
                @else
                    -
                @endif
            </td>
            <td>
                @if(isset($seafarer['coc_detail']) AND !empty($seafarer['coc_detail']))
                    <?php
                        $count = count($seafarer['coc_detail'])-1;
                        $ind = 0;
                    ?>
                    @foreach($seafarer['coc_detail'] as $coc)
                        @foreach( \CommonHelper::countries() as $c_index => $country)
                            {{ isset($coc['coc']) ? $coc['coc'] == $c_index ? $country : '' : ''}}
                            @if($coc['coc'] == $c_index AND ( $count != $ind))
                                ,
                                <?php $ind++ ?>
                            @endif
                        @endforeach
                    @endforeach
                @else
                    -
                @endif
            </td>
            <td>
                @if(isset($seafarer['sea_service_detail']) AND !empty($seafarer['sea_service_detail']))
                    <?php
                        $count = count($seafarer['sea_service_detail'])-1;
                        $ind = 0;
                    ?>
                    @foreach($seafarer['sea_service_detail'] as $services)
                        @foreach( \CommonHelper::ship_type() as $c_index => $type)
                            {{ isset($services['ship_type']) ? $services['ship_type'] == $c_index ? $type : '' : ''}}
                            @if($services['ship_type'] == $c_index AND ( $count != $ind))
                                ,
                                <?php $ind++ ?>
                            @endif
                        @endforeach
                    @endforeach
                @else
                    -
                @endif
            </td>
        </tr>
    @endforeach
</html>