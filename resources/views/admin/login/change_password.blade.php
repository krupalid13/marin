@extends('admin.index')
@section('content')

    <div class="row">
        <div class="main-login col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-4 col-md-offset-4">
            <div class="logo">
                <img src="/images/coss-logo.png">
            </div>
            <!-- start: LOGIN BOX -->
            <div class="box-login">
                <h3>Change your password</h3>
                <p>
                    Please enter your password and confirm password.
                </p>
                <form class="form-change-password" action="{{ route('admin.change.password') }}" method="post">
                    {{csrf_field()}}
                    <div class="errorHandler alert alert-danger no-display">
                        <i class="fa fa-remove-sign"></i> You have some form errors. Please check below.
                    </div>
                    <fieldset>
                        <input type="hidden" name="email" id="email" value="{{$email}}">
                        <div class="form-group">
								<span class="input-icon">
									<input type="password" class="form-control" name="password" id="password" placeholder="Password">
									<i class="fa fa-lock"></i> </span>
                        </div>
                        <div class="form-group form-actions">
								<span class="input-icon">
									<input type="password" class="form-control password" name="cpassword" id="cpassword" placeholder="Confirm Password">
									<i class="fa fa-lock"></i>
                                </span>
                        </div>
                        @if(Session::has('error'))
                            <div class="alert alert-danger">
                                {{ Session::get('error')}}
                            </div>
                        @endif
                        <div class="form-actions">
                            <button type="submit" class="btn btn-green pull-right ladda-button" id="admin-change-password-button">
                                Change Password <i class="fa fa-arrow-circle-right"></i>
                            </button>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
    </div>

@stop