<!DOCTYPE html>
<html lang="en">
<?php
$india_value = array_search('India',\CommonHelper::countries());
if(isset($data[0]['professional_detail']['current_rank'])){
    $required_fields = \CommonHelper::rank_required_fields()[$data[0]['professional_detail']['current_rank']];
}
if(isset($data[0]['document_permissions']) && !empty($data[0]['document_permissions'])){
    $document_permissions = $data[0]['document_permissions'];
}
if(Auth::check()){
    $registered_as = Auth::user()->registered_as;
}
$gender = "M";
if(isset($data[0]['gender']) && $data[0]['gender'] == 'F'){
    $gender = 'F';
}
?>
<head>
    @include('layouts/pdf/header')
    <style>
        thead {
            display: table-header-group;
        }
        tr {
            page-break-before: always;
            page-break-after: always;
            page-break-inside: avoid ;
        }
    </style>
    <style type="text/css">
        @page {
            margin: 0px;
        }
        body {
            margin: 0px;
        }
        * {
            font-family: Verdana, Arial, sans-serif;
        }
        a {
            color: #fff;
            text-decoration: none;
        }

        tfoot tr td {
            font-weight: bold;
            font-size:12px;
        }
        .user-profile-pic{
            border: solid 1px #d4cdcd;
            height: 165px;
            width: 165px;
        }
        .res-pad-10{
            margin: 10px 0 10px;
        }
        .pad-8{
            padding: 8px;
        }
        .th-back{
            background-color: #eaeaea;
        }
    </style>
    <meta charset="UTF-8">
    <title>Resume</title>
</head>
<body>
    @include('admin/quick_response')
<section class="container">
    <div class="row form-group">
        <div class="col-12">
            <div class="_flex">
                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 ">
                    <div class="card-body top-borderradius">
                        @if(isset($data[0]['profile_pic']))
                            <img class="img-responsive img-circle center-block user-profile-pic" src="{{url('public/images/uploads/seafarer_profile_pic/')}}/{{$data[0]['id']}}/{{$data[0]['profile_pic']}}" alt="image">
                        @else
                            @if($gender == 'M')
                                <img class="img-responsive img-circle center-block user-profile-pic" src="{{url('public/assets/images/default-user-male.png')}}" alt="image">
                            @else
                                <img class="img-responsive img-circle center-block user-profile-pic" src="{{url('public/assets/images/defualt-user-female.png')}}" alt="image">
                            @endif
                        @endif
                    </div>
                </div>
                <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9 ">
                    <h3 class="font-trebuchet">
                        {{ isset($data[0]['first_name']) ? ucwords($data[0]['first_name']) : ''}}
                    </h3>
                    <p class="font-trebuchet">
                        @foreach(\CommonHelper::new_rank() as $index => $category)
                            @foreach($category as $r_index => $rank)
                                {{ !empty($data[0]['professional_detail']['current_rank']) ? $data[0]['professional_detail']['current_rank'] == $r_index ? $rank : '' : ''}}
                            @endforeach
                        @endforeach
                    </p>
                    @if(!empty($data[0]['professional_detail']['availability']))
                        <p class="font-trebuchet">
                            Availability : 
                                {{isset($data[0]['professional_detail'])?((Carbon\Carbon::now())->lessThan(Carbon\Carbon::parse($data[0]['professional_detail']['availability']))?date('d-m-Y',strtotime($data[0]['professional_detail']['availability'])):'Immediate'):''}}
                        </p>
                    @endif
                </div>
            </div>
        </div>
    </div>
    
    <div class="row">
        <div class="col-12">
            <div class="flex pt-10">
                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 border-grey">
                    <p class="text-center wordwrap pt-m-8 res-pad-10"><i class="text-primary fa fa-phone" aria-hidden="true"></i> {{ isset($data[0]['mobile']) ? $data[0]['mobile'] : '' }}
                        @if(isset($data[0]['personal_detail']['landline']) && !empty($data[0]['personal_detail']['landline']))
                            / {{$data[0]['personal_detail']['landline']}}
                        @endif
                    </p>
                </div>
                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 border-grey">
                    <p class="text-center wordwrap pt-m-8 res-pad-10"><i class="text-primary fa fa-at" aria-hidden="true"></i> {{ isset($data[0]['email']) ? $data[0]['email'] : ''}}</p>
                </div>
                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 border-grey">
                    <p class="text-center wordwrap res-pad-10"><i class="text-primary fa fa-home" aria-hidden="true"></i> {{ isset($data[0]['personal_detail']['city_id']) ? $data[0]['personal_detail']['pincode']['pincodes_cities'][0]['city']['name'] : (isset($data[0]['personal_detail']['city_text']) ? $data[0]['personal_detail']['city_text'] : '')}} / {{ isset($data[0]['personal_detail']['state_id']) ? $data[0]['personal_detail']['pincode']['pincodes_states'][0]['state']['name'] : (isset($data[0]['personal_detail']['state_text']) ? $data[0]['personal_detail']['state_text'] : '')}} / @if(isset($data[0]['passport_detail']['pass_country']) && !empty($data[0]['passport_detail']['pass_country']))
                            @foreach( \CommonHelper::countries() as $c_index => $country)
                                {{ isset($data[0]['passport_detail']['pass_country']) ? $data[0]['passport_detail']['pass_country'] == $c_index ? $country : '' : ''}}
                            @endforeach
                        @else
                            -
                        @endif
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="container">
    <div>
        <h4 class="h4 pt-20 pb-10 border-bottom d-ib"><i class="text-primary fa fa-{{$gender == 'M' ? 'male' : 'female'}}" aria-hidden="true"></i> PERSONAL DETAILS</h4>
    </div>
    <table class="table table-bordered text-center" style="border-collapse: collapse;">
        <thead>
            <tr class="th-back">
                <th class="border-grey text-center">Nationality</th>
                <th class="border-grey text-center">DOB</th>
                <th class="border-grey text-center">Current Rank</th>
                <th class="border-grey text-center">Experience</th>
                <th class="border-grey text-center">Applied Rank</th>
            </tr>
        </thead>
        <tboady>
            <tr>
                <td class="border-grey">
                    @if (isset($data[0]['personal_detail']['nationality']))
                        {{\CommonHelper::countries()[$data[0]['personal_detail']['nationality']]}}
                    @endif
                </td>
                <td class="border-grey">
                    {{ isset($data[0]['personal_detail']) ? date('d-m-Y',strtotime($data[0]['personal_detail']['dob'])) : ''}}
                </td>
                <td class="border-grey">
                    @foreach(\CommonHelper::new_rank() as $index => $category)
                        @foreach($category as $r_index => $rank)
                            {{ !empty($data[0]['professional_detail']['current_rank']) ? $data[0]['professional_detail']['current_rank'] == $r_index ? $rank : '' : ''}}
                        @endforeach
                    @endforeach
                </td>
                <td class="border-grey">
                    {{ isset($data[0]['professional_detail']['years']) ? $data[0]['professional_detail']['years'] : '0'}} Y {{ isset($data[0]['professional_detail']['months']) && ($data[0]['professional_detail']['months'] != '') ? $data[0]['professional_detail']['months'] : '0' }} M
                </td>
                <td class="border-grey">
                    @foreach(\CommonHelper::new_rank() as $index => $category)
                        @foreach($category as $r_index => $rank)
                            {{ !empty($data[0]['professional_detail']['applied_rank']) ? $data[0]['professional_detail']['applied_rank'] == $r_index ? $rank : '' : ''}}
                        @endforeach
                    @endforeach
                </td>
            </tr>
        </tboady>
    </table>
</section>
<section class="container">
    <div>
        <h4 class="h4 pb-10 border-bottom pt-20 d-ib"><i class="text-primary fa fa-id-badge" aria-hidden="true"></i> MY PROFILE</h4>
    </div>
    <p>
        {{$data[0]['professional_detail']['about_me']}}
    </p>
    <br />
    <p>
        
        <a href="{{URL::to('/user-profile')}}" class="btn btn-success">
            <i class="fa fa-user-circle" aria-hidden="true"></i> Check out my profile
        </a>
    </p>
</section>
<section class="container">
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
            <div>
                <h4 class="h4 pb-10 border-bottom pt-20 d-ib"> <i class="text-primary fa fa-suitcase" aria-hidden="true"></i> TRAVELLING DOCUMENT</h4>
            </div>
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>Document</th>
                    <th>Document Nos</th>
                    <th>POI</th>
                    <th>DOE</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>
                        Passport :  @foreach( \CommonHelper::countries() as $c_index => $country)
                            {{ isset($data[0]['passport_detail']) ? $data[0]['passport_detail']['pass_country'] == $c_index ? $country : '' : ''}}
                        @endforeach
                    </td>
                    <td>
                        {{ isset($data[0]['passport_detail']['pass_number']) ? $data[0]['passport_detail']['pass_number'] : '-' }}
                    </td>
                    <td>
                        {{ isset($data[0]['passport_detail']['place_of_issue']) ? $data[0]['passport_detail']['place_of_issue'] : '-' }}
                    </td>
                    <td>
                        {{ isset($data[0]['passport_detail']['pass_expiry_date']) ? date('d-m-Y', strtotime($data[0]['passport_detail']['pass_expiry_date'])) : '-' }}
                    </td>
                </tr>
                </tbody>
            </table>
            @if(isset($data[0]['passport_detail']['us_visa']) && !empty($data[0]['passport_detail']['us_visa']))
                <p class="pad-8">
                    {{ isset($data[0]['passport_detail']['us_visa']) ? ($data[0]['passport_detail']['us_visa']==1 ? 'Us Visa Date Of Expiry : '.(isset($data[0]['passport_detail']['us_visa_expiry_date']) ? date('d-m-Y',strtotime($data[0]['passport_detail']['us_visa_expiry_date'])) : '-'):''):'' }}
                </p>
            @endif
            @php
                $yf=(\App\UserWkfrDetail::whereUserId($user_id)->whereYellowFever(1)->pluck('yf_issue_date')->first());
                $ilo=(\App\UserWkfrDetail::whereUserId($user_id)->whereYellowFever(1)->pluck('ilo_issue_date')->first());
            @endphp
            @if(!empty($yf) || !empty($ilo))
                <p class="pad-8">
                    @if(!empty($yf))
                        {{($yf)?'YF Vaccination Validity : '.date("d-m-Y",strtotime($yf)):''}}
                    @endif
                    @if(!empty($ilo))
                    </br>
                        {{($ilo)?'ILO Medical Validity : '.date("d-m-Y",strtotime($ilo)):''}}
                    @endif
                </p>
            @endif
            
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
            <div>
                <h4 class="h4 pb-10 border-bottom pt-20 d-ib">
                    <i class="text-primary fa fa-life-ring" aria-hidden="true"></i> SAILING DOCUMENT
                </h4>
            </div>
            <div class="table-responsive">
                <table class="table table-responsive">
                    <thead>
                    <tr>
                        <th>Document</th>
                        <th>Document Nos</th>
                        <th>Valid upto</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(isset($data[0]['personal_detail']['nationality']) && $data[0]['personal_detail']['nationality'] == '95')
                        <tr>
                            <td>INDOS :</td>
                            <td>{{ !empty($data[0]['wkfr_detail']['indos_number']) ? $data[0]['wkfr_detail']['indos_number'] : '-'}}</td>
                            <td>---</td>
                        </tr>
                    @endif
                    @foreach($data[0]['seaman_book_detail'] as $index => $cdc_data)
                        <tr>
                            <td>CDC : @foreach( \CommonHelper::countries() as $c_index => $country)
                                    {{ isset($cdc_data['cdc']) ? $cdc_data['cdc'] == $c_index ? $country : '' : ''}}
                                @endforeach
                            </td>
                            <td>
                                {{isset($cdc_data['cdc_number']) ? $cdc_data['cdc_number'] : ''}}
                            </td>
                            <td>
                                {{isset($cdc_data['cdc_expiry_date']) ? date('d-m-Y',strtotime($cdc_data['cdc_expiry_date'])) : ''}}
                            </td>
                        </tr>
                    @endforeach
                    {{--                <td>CDC : @foreach( \CommonHelper::countries() as $c_index => $country)--}}
                    {{--                        {{ isset($cdc_data['cdc']) ? $cdc_data['cdc'] == $c_index ? $country : '' : ''}}--}}
                    {{--                    @endforeach</td>--}}
                    {{--                <td>{{isset($cdc_data['cdc_number']) ? $cdc_data['cdc_number'] : ''}}</td>--}}
                    {{--                <td>{{ !is_null($cdc_data['cdc_verification_date']) ? date('d-m-Y',strtotime($cdc_data['cdc_verification_date'])) : '-' }}</td>--}}
                    {{--            </tr>--}}
                    @if(!empty($data[0]) && !empty($data[0]['coc_detail'][0]) && $data[0]['coc_detail'][0]['coc'] != '')
                        @foreach($data[0]['coc_detail'] as $index => $coc_data)
                            <tr>
                                <td>COC : {{ !empty($coc_data['coc_grade']) ? $coc_data['coc_grade'] : '-' }}  @if(isset($coc_data['coc']) && !empty($coc_data['coc']))
                                        @foreach( \CommonHelper::countries() as $c_index => $country)
                                            {{ isset($coc_data['coc']) ? $coc_data['coc'] == $c_index ? $country : '' : ''}}
                                        @endforeach
                                    @else - @endif
                                </td>
                                <td>
                                    {{ !empty($coc_data['coc_number']) ? $coc_data['coc_number'] : '-'}}
                                </td>
                                <td>
                                    {{ !is_null($coc_data['coc_expiry_date']) ? date('d-m-Y',strtotime($coc_data['coc_expiry_date'])) : '-' }}
                                </td>
                            </tr>
                        @endforeach
                    @endif
                    @if(isset($required_fields))
                        @if(isset($data[0]['coe_detail']) AND !empty($data[0]['coe_detail']) && (in_array('COE',$required_fields) OR in_array('COE-Optional',$required_fields)) && (!empty($data[0]['coe_detail'][0]['coe']) || !empty($data[0]['coe_detail'][0]['coe_number']) || !empty($data[0]['coe_detail'][0]['coe_grade']) || !empty($data[0]['coe_detail'][0]['coe_expiry_date']) || !empty($data[0]['coe_detail'][0]['coe_verification_date'])))
                            @foreach($data[0]['coe_detail'] as $index => $coe_data)
                                <tr>
                                    <td>
                                        COE : {{ !empty($coe_data['coe_grade']) ? $coe_data['coe_grade'] : '-'}} @if(isset($coe_data['coe']) && !empty($coe_data['coe']))
                                            @foreach( \CommonHelper::countries() as $c_index => $country)
                                                {{ isset($coe_data['coe']) ? $coe_data['coe'] == $c_index ? $country : '' : ''}}
                                            @endforeach
                                        @else
                                            -
                                        @endif
                                    </td>
                                    <td>
                                        {{ !empty($coe_data['coe_number']) ? $coe_data['coe_number'] : '-'}}
                                    </td>
                                    <td>
                                        {{ !empty($coe_data['coe_expiry_date']) ? date('d-m-Y',strtotime($coe_data['coe_expiry_date'])) : '-' }}
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                    @endif
                    @if(isset($required_fields) && !empty($required_fields))
                        @if(isset($data[0]['gmdss_detail']) AND !empty($data[0]['gmdss_detail']) && (in_array('GMDSS',$required_fields) OR in_array('GMDSS-Optional',$required_fields)) && ((!empty($data[0]['gmdss_detail']['gmdss'])) || (!empty($data[0]['gmdss_detail']['gmdss_number'])) || (!empty($data[0]['gmdss_detail']['gmdss_expiry_date']))))
                            <tr>
                                <td>GMDSS : @foreach( \CommonHelper::countries() as $c_index => $country)
                                        {{ isset($data[0]['gmdss_detail']['gmdss']) ? $data[0]['gmdss_detail']['gmdss'] == $c_index ? $country : '' : ''}}
                                    @endforeach
                                </td>
                                <td>
                                    {{ isset($data[0]['gmdss_detail']['gmdss_number']) ? $data[0]['gmdss_detail']['gmdss_number'] : '-'}}
                                </td>

                                <td>
                                    {{ isset($data[0]['gmdss_detail']['gmdss_expiry_date']) ? date('d-m-Y',strtotime($data[0]['gmdss_detail']['gmdss_expiry_date'])) : '-' }}
                                </td>
                            </tr>
                            {{--                        <div class="col-sm-3 gmdss_endorsement {{isset($data[0]['gmdss_detail']['gmdss']) && $data[0]['gmdss_detail']['gmdss'] == $india_value ? '' : 'hide'}}">--}}
                            @if(isset($data[0]['gmdss_detail']['gmdss_endorsement_number']))
                                <tr>
                                    <td>
                                        GMDSS Endorsement
                                    </td>
                                    <td>{{ isset($data[0]['gmdss_detail']['gmdss_endorsement_number']) && !empty($data[0]['gmdss_detail']['gmdss_endorsement_number']) ? $data[0]['gmdss_detail']['gmdss_endorsement_number'] : '-'}}
                                        {{--                        <div class="col-sm-3 gmdss_valid_till {{isset($data[0]['gmdss_detail']['gmdss']) && $data[0]['gmdss_detail']['gmdss'] == $india_value ? '' : 'hide'}}">--}}
                                    </td>
                                    <td>
                                        {{ isset($data[0]['gmdss_detail']['gmdss_endorsement_expiry_date']) && !empty($data[0]['gmdss_detail']['gmdss_endorsement_expiry_date']) ? date('d-m-Y',strtotime($data[0]['gmdss_detail']['gmdss_endorsement_expiry_date'])) : '-'}}
                                    </td>
                                </tr>
                            @endif
                        @endif
                    @endif
                    @if(isset($required_fields))
                        @if(isset($data[0]['wkfr_detail']) AND !empty($data[0]['wkfr_detail']) && (in_array('WATCH_KEEPING-Optional',$required_fields)) && ((!empty($data[0]['wkfr_detail']['wkfr_number'])) || (!empty($data[0]['wkfr_detail']['type']))))
                            <tr>
                                <td>
                                    {{(\App\UserWkfrDetail::whereUserId($user_id)->first()->wk_cop) == 'wk' ? "WK" : "COP"}} : {{ isset($data[0]['wkfr_detail']['type']) ? $data[0]['wkfr_detail']['type'] : '-'}} :
                                    @if(isset($data[0]['wkfr_detail']['watch_keeping']))
                                        {{\CommonHelper::countries()[$data[0]['wkfr_detail']['watch_keeping']]}}
                                    @else
                                        -
                                @endif
                                <td>
                                    {{ !empty($data[0]['wkfr_detail']['wkfr_number']) ? $data[0]['wkfr_detail']['wkfr_number'] : '-'}}
                                </td>
                                <td>
                                    -
                                </td>
                            </tr>
                        @endif
                    @endif
                    @if (isset($data[0]['professional_detail']) && !empty($data[0]['professional_detail']['current_rank']) && (in_array('DCE-Optional', \CommonHelper::rank_required_fields()[$data[0]['professional_detail']['current_rank']]) || in_array('DCE', \CommonHelper::rank_required_fields()[$data[0]['professional_detail']['current_rank']])))
                        @php
                            $dceDetails = isset($data[0]['user_dangerous_cargo_endorsement_detail']) ? $data[0]['user_dangerous_cargo_endorsement_detail'] : null;
                            $dceStatus = !empty($dceDetails) ? json_decode($dceDetails['status']) : null;
                            $oilDetails = !empty($dceDetails) ? json_decode($dceDetails['oil']) : null;
                            $chemicalDetails = !empty($dceDetails) ? json_decode($dceDetails['chemical']) : null;
                            $lequefiedGasDetails = !empty($dceDetails) ? json_decode($dceDetails['lequefied_gas']) : null;
                            $allDetails = !empty($dceDetails) ? json_decode($dceDetails['all']) : null;
                        @endphp
                        @if(!empty($dceDetails) && !empty($dceStatus) && $dceStatus->status == 1)
                            @if(!empty($dceStatus->type) && in_array('oil', $dceStatus->type))
                                <tr>
                                    <td>
                                        DCE : Oil {{ !empty($oilDetails) && !empty($oilDetails->country)  ?  \CommonHelper::countries()[$oilDetails->country] : null }}
                                    </td>
                                    <td>
                                        @if (!empty($oilDetails) && isset($oilDetails->grade) && $oilDetails->grade != null)
                                            {{ $oilDetails->grade == 0 ? 'Level I : ' : 'Level II : ' }}
                                        @endif
                                        {{ !empty($oilDetails) && isset($oilDetails->number) && !empty($oilDetails->number) ? $oilDetails->number : null }}
                                    </td>

                                    <td>
                                        {{ !empty($oilDetails) &&  isset($oilDetails->date_of_expiry) && !empty($oilDetails->date_of_expiry) ? $oilDetails->date_of_expiry : null }}
                                    </td>
                                </tr>
                            @endif
                            @if(!empty($dceStatus->type) && in_array('chemical', $dceStatus->type))
                                <tr>
                                    <td>
                                        DCE : Chemical {{ !empty($chemicalDetails) && !empty($chemicalDetails->country)  ?  \CommonHelper::countries()[$chemicalDetails->country] : null }}
                                    </td>
                                    <td>
                                        @if (!empty($chemicalDetails) && isset($chemicalDetails->grade) && $chemicalDetails->grade != null)
                                            {{ $chemicalDetails->grade == 0 ? 'Level I : ' : 'Level II : ' }}
                                        @endif
                                        {{ !empty($chemicalDetails) && isset($chemicalDetails->number) && !empty($chemicalDetails->number) ? $chemicalDetails->number : null }}
                                    </td>
                                    <td>
                                        {{ !empty($chemicalDetails) &&  isset($chemicalDetails->date_of_expiry) &&  !empty($chemicalDetails->date_of_expiry) ? $chemicalDetails->date_of_expiry : null }}
                                    </td>
                                </tr>
                            @endif
                            @if(!empty($dceStatus->type) && in_array('lequefied_gas', $dceStatus->type))
                                <tr>
                                    <td>
                                        DCE : Liquefied Gas {{ !empty($lequefiedGasDetails) && !empty($lequefiedGasDetails->country)  ?  \CommonHelper::countries()[$lequefiedGasDetails->country] : null }}
                                    </td>
                                    <td>
                                        @if (!empty($lequefiedGasDetails) && isset($lequefiedGasDetails->grade) &&  $lequefiedGasDetails->grade != null)
                                            {{ $lequefiedGasDetails->grade == 0 ? 'Level I : ' : 'Level II : ' }}
                                        @endif
                                        {{ !empty($lequefiedGasDetails) && isset($lequefiedGasDetails->number) && !empty($lequefiedGasDetails->number) ? $lequefiedGasDetails->number : null }}
                                    </td>

                                    <td>
                                        {{ !empty($lequefiedGasDetails) && isset($lequefiedGasDetails->date_of_expiry) && !empty($lequefiedGasDetails->date_of_expiry) ? $lequefiedGasDetails->date_of_expiry : null }}
                                    </td>
                                </tr>
                            @endif
                            @if(!empty($dceStatus->type) && in_array('all', $dceStatus->type))
                                <tr>
                                    <td>
                                        DCE : Oil + Chemical + Liquefied Gas {{ !empty($allDetails) && !empty($allDetails->country)  ?  \CommonHelper::countries()[$allDetails->country] : null }}
                                    </td>
                                    <td>
                                        @if (!empty($allDetails) &&  isset($allDetails->grade) && $allDetails->grade != null)
                                            {{ $allDetails->grade == 0 ? 'Level I : ' : 'Level II : ' }}
                                        @endif
                                        {{ !empty($allDetails) && isset($allDetails->number) && !empty($allDetails->number) ? $allDetails->number : null }}
                                    </td>
                                    <td>
                                        {{ !empty($allDetails) && isset($allDetails->date_of_expiry) && !empty($allDetails->date_of_expiry) ? $allDetails->date_of_expiry : null }}
                                    </td>
                                </tr>
                            @endif
                        @endif
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>
<section class="container">
    <div >
        <h4 class="h4 pb-10 border-bottom pt-20 d-ib"><i class="text-primary fa fa-graduation-cap" aria-hidden="true"></i> COURSE & CERTIFICATION</h4>
    </div>
    <div class="table-responsive">
        <table class="table table-striped ">
            <thead>
            <tr>
                <th>Course Name</th>
                <th>Institute</th>
                <th>Certificate No</th>
                <th>DOI</th>
                <th>Valid Upto</th>
            </tr>
            </thead>
            <tbody>
            @if(isset($data[0]['course_detail']) AND !empty($data[0]['course_detail']))
                <?php $normal_course_count = 1; ?>
                @foreach($data[0]['course_detail'] as $index => $courses)
                    <tr>
                        <td>
                            {{ isset($courses['course_id']) ? ( \App\Courses::whereId($courses['course_id'])->pluck('course_name'))->first() : ''}}
                            @foreach( \CommonHelper::courses() as $c_index => $course)
                            @endforeach
                        </td>
                        <td>
                            {{ isset($courses['issue_by']) && !empty(isset($courses['issue_by'])) ? $courses['issue_by'] : '-'}}
                        </td>
                        <td>
                            {{ isset($courses['certification_number']) && !empty(isset($courses['certification_number'])) ? $courses['certification_number'] : '-'}}
                        </td>
                        <td>
                            {{ isset($courses['issue_date']) && !empty(isset($courses['issue_date'])) ? $courses['issue_date'] : '-'}}
                        </td>
                        <td style="min-width: 96px;">
                            {{ isset($courses['expiry_date']) && !empty(isset($courses['expiry_date'])) ? date('d-m-Y',strtotime($courses['expiry_date'])) : '-'}}
                        </td>
                        <?php $normal_course_count++; ?>
                    </tr>
                @endforeach
            @endif
            </tbody>
        </table>
    </div>
</section>
<section class="container">
    <div>
        <h4 class="h4 pb-10 border-bottom pt-20 d-ib"><i class="text-primary fa fa-ship" aria-hidden="true"></i> SEA SERVICE EXPERIENCE</h4>
        <hr />
    </div>
    {{--    @if($pdfFormat!=true)--}}
    {{--        <div class="table-responsive">--}}
    {{--    @endif--}}
    <table class="table table-striped table-bordered">
        <thead>
        <tr>
            <th><p class="text-center">Rank</p></th>
            <th>
                <p class="text-center">Ship</p>
                <hr/>
                <p class="text-center">Flag</p>
            </th>
            <th>
                <p class="text-center">Owner Company</p>
                <hr/>
                <p class="text-center">Manning By</p>
            </th>
            <th>
                <p class="text-center">Ship Type</p>
                <hr/>
                <p class="text-center">Engine Type</p>
            </th>
            <th>
                <p class="text-center">GRT</p>
                <hr/>
                <p class="text-center">BHP</p>
            </th>
            <th>
                <p class="text-center">Sign On</p>
                <hr/>
                <p class="text-center">Sign Off</p>
            </th>
        </tr>
        </thead>
        <tbody>
            
        @if(isset($data[0]['sea_service_detail']) AND !empty($data[0]['sea_service_detail']))
            @foreach($data[0]['sea_service_detail'] as $index => $services)
                <tr>
                    <td class="pos-relative">
                        <p class="text-center">
                            @foreach(\CommonHelper::new_rank() as $index => $category)
                                @foreach($category as $r_index => $rank)
                                    {{ isset($services['rank_id']) ? $services['rank_id'] == $r_index ? $rank : '' : ''}}
                                @endforeach
                            @endforeach
                        </p>
                    </td>
                    <td>
                        <p class="text-center">{{ isset($services['ship_name']) ? $services['ship_name'] : '-'}}</p>
                        <hr/>
                        <p class="text-center">@foreach( \CommonHelper::countries() as $c_index => $country)
                                {{ isset($services['ship_flag']) ? $services['ship_flag'] == $c_index ? $country : '' : ''}}
                            @endforeach</p>
                    </td>
                    <td>
                        <p class="text-center">{{ isset($services['company_name']) ? $services['company_name'] : '-'}}</p>
                        <hr/>
                        <p class="text-center">{{ isset($services['manning_by']) ? $services['manning_by'] : '-'}}</p>
                    </td>
                    <td>
                        <p class="text-center"> @foreach( \CommonHelper::ship_type() as $c_index => $type)
                                {{ isset($services['ship_type']) ? $services['ship_type'] == $c_index ? $type : '' : ''}}
                            @endforeach
                        </p>
                        <hr/>
                        <p class="text-center"> @if(isset($services['engine_type']) && !empty($services['engine_type']))
                                @if(isset($services['engine_type']) && $services['engine_type'] == 'other')
                                    {{ isset($services['other_engine_type']) ? $services['other_engine_type'] : '-'}}
                                @else
                                    @foreach( \CommonHelper::engine_type_by_user_id($user_id) as $c_index => $type)
                                        {{ isset($services['engine_type']) ? $services['engine_type'] == $c_index ? $type  : '' : ''}}
                                    @endforeach
                                @endif
                            @else
                                -
                            @endif
                        </p>
                    </td>
                    <td>
                        <p class="text-center">
                            {{ !empty($services['grt']) ? $services['grt'] : '-'}}
                        </p>
                        <hr/>
                        <p class="text-center">
                            {{ !empty($services['bhp']) ? $services['bhp'] : '-'}}
                        </p>
                    </td>
                    <td>
                        <p class="text-center">
                            {{ isset($services['from']) ? date('d-m-Y',strtotime($services['from'])) : ''}}
                        </p>
                        <hr/>
                        <p class="text-center">
                            {{ isset($services['to']) ? date('d-m-Y',strtotime($services['to'])) : 'On Board'}}
                        </p>
                    </td>
                </tr>
            @endforeach
        @endif
        </tbody>
    </table>
    {{--    @if($pdfFormat!=true)--}}
    {{--        </div>--}}
    {{--    @endif--}}
</section>
@if(isset($data[0]['professional_detail']['other_exp']))
    <section class="container">
        <div>
            <h4 class="h4 pb-10 border-bottom pt-20 d-ib"><i class="text-primary fa fa-wrench" aria-hidden="true"></i> OTHER EXPERIENCE</h4>
        </div>
        <p>{{$data[0]['professional_detail']['other_exp']}}</p>
        <br>
    </section>
@endif
@if($pdfFormat!=true)
    <section class="container">
        <a href="{{route('create-pdf',['PDF'=>CommonHelper::encodeKey(3)])}}" class="btn btn-primary">Get Resume</a>
        <a href="" class="btn btn-danger" data-toggle="modal" data-target="#quick-response-model">Quick Response</a>
    </section>
@endif
<footer class="container-fluid footer bg-primary" style="padding-top: 5px; margin-top:10px">
    <div class="row">
        <p class=" col-xs-10 col-sm-10 col-md-10 col-lg-10" style="margin:0">This resume has been created under the knowledge and details provided by
            @if(isset($data[0]['first_name']) && !empty($data[0]['first_name']))
                {{$data[0]['first_name']}}
            @endif
            @if(isset($data[0]['last_name']) && !empty($data[0]['last_name']))
                {{$data[0]['last_name']}}
            @endif
            at {{config('app.name')}}</p>
        <span class=" col-xs-2 col-sm-2 col-md-2 col-lg-2 text-right">{{\Carbon\Carbon::now()->format('M d Y')}}</span>
    </div>
</footer>
</body>
</html>