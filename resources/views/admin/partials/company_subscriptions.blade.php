<div class="col-md-12">
	<div class="panel panel-white">
		<div class="panel-body">
			<?php 
				$data_array = $company_data->toArray();
			?>
			@if(isset($data_array['data']) && !empty($data_array['data']))
			
				<div class="panel-heading">
					<div class="row">
						<div class="col-xs-12">
							<div class="col-xs-6 paginator-content  pull-right no-margin">
								<ul id="top-user-paginator" class="pagination">
									{!! $company_data->render() !!}
								</ul>
							</div>
							
							<div class="pull-left search-count-content">
								Showing {{$data_array['from']}} - {{$data_array['to']}} of {{$data_array['total']}} Results
							</div>
						</div>
					</div>
				</div>
							
					
			@foreach($data_array['data'] as $data)	
			<div class="seafarer-listing-container">
			<div class="panel">
			<div class="panel-heading border-light partition-blue">
			<h5 class="panel-title text-bold header-link-text p-5-5">
				<a href="">
					{{isset($data['company_registration_detail']['company_name']) ? $data['company_registration_detail']['company_name'] : ''}}
				</a>
			</h5>
			<ul class="panel-heading-tabs border-light">
			
			</ul>

			</div>
								
			<div class="panel-body">
			<div class="row">
				<div class="col-lg-3 col-md-5 col-sm-5" id="user-profile-pic" style="height: 200px;width: 200px; margin-left: 15px;">
					<a>
						<div class="company_profile_pic">
        					<img src="/{{ isset($data['profile_pic']) ? env('COMPANY_LOGO_PATH')."/".$data['id']."/".$data['profile_pic'] : 'images/user_default_image.png'}}" >
        				</div>
        			</a>
				</div>
				<div class="col-lg-9 col-md-7 col-sm-7">
					<div class="row">
					<div class="col-sm-4 sm-t-20">
						<div class="row">
							<div class="col-xs-12">
								<div class="header-tag">
									Basic Information
								</div>
							</div>
						</div>
						<div id="user-basic-details">
							<div class="row">
								<div class="col-xs-6 col-sm-7">
									<dl>
										<dt> Company Name : </dt>
										<dd class="margin-bottom-5px">
											{{isset($data['company_registration_detail']['company_name']) ? $data['company_registration_detail']['company_name'] : ''}}
										</dd>
										<dt> Email : </dt>
										<dd class="margin-bottom-5px make-word-wrap-break">
											{{isset($data['company_detail']['company_email']) ? $data['company_detail']['company_email'] : ''}}
							     		</dd>
							     		<dt> Contact Number : </dt>
										<dd class="margin-bottom-5px">
											{{isset($data['company_registration_detail']['contact_number']) ? $data['company_registration_detail']['contact_number'] : ''}}
							     		</dd>
									</dl>
								</div>
							</div>
						</div>
					</div>
				
				@foreach($data['company_subscriptions'] as $company_data1)

					@if($company_data1['status'] == 1)
						<div class="col-sm-4">
							<div id="user-professional-details">
								<div class="row">
									<div class="col-xs-12">
										<div class="header-tag">
											Subscription Details
										</div>
									</div>
								</div>
								<dl>
						     		<dt> Subscription ID : </dt>
										<dd class="margin-bottom-5px">
											{{isset($company_data1['subscription_id']) ? $company_data1['subscription_id'] : ''}}
							     		</dd>
						     		<dt>
									<dt> Subscription Title :</dt>
									<dd class="margin-bottom-5px">
										{{isset($company_data1['subscription_feature']['title']) ? $company_data1['subscription_feature']['title'] : ''}}
									</dd>
								</dl>
							</div>
						</div>

						<div class="col-sm-4">
							<div class="header-tag">
								Validity Details
							</div>
							<div id="user-professional-details">
								<dl>
									<dt> Valid From :</dt>
									<dd class="margin-bottom-5px">
										{{isset($company_data1['valid_from']) ? date('d-m-Y',strtotime($company_data1['valid_from'])) : ''}}
									</dd>
									<dt> Valid To :</dt>
									<dd class="margin-bottom-5px">
										{{isset($company_data1['valid_to']) ? date('d-m-Y',strtotime($company_data1['valid_to'])) : ''}}
									</dd>
									<dt> Status : </dt>
									<dd class="margin-bottom-5px">
										{{isset($company_data1['status']) ? $company_data1['status'] == '1' ? 'Active' : 'Deactive' : ''}}
									</dd>
								</dl>
							</div>
						</div>
					@endif
				@endforeach
				</div>
				</div>
								
			</div>
			</div>
			</div>
								
			</div>
			@endforeach
			@else
				<div class="no-results-found">No results found. Try again with different search criteria.</div>
			@endif

			@if(isset($data_array['data']) && !empty($data_array['data']))
				<div class="panel-heading">
					<div class="row">
						<div class="col-xs-12">
							<div class="col-xs-6 paginator-content  pull-right no-margin">
								<ul id="top-user-paginator" class="pagination">
									{!! $company_data->render() !!}
								</ul>
							</div>
							
							<div class="pull-left search-count-content">
								Showing {{$data_array['from']}} - {{$data_array['to']}} of {{$data_array['total']}} Results
							</div>
						</div>
					</div>
				</div>
			@endif
		</div>
	</div>
</div>