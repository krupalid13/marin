<div class="col-md-12">
	<div class="panel panel-white">
		<div class="panel-body">
		@if(isset($seafarer_data['data']) && !empty($seafarer_data['data']))

			<div class="panel-heading">
				<div class="row">
					<div class="col-xs-12 col-sm-8 paginator-content  pull-right no-margin">
						<ul id="top-user-paginator" class="pagination pagination-blue">
							{!! $pagination->render() !!}
						</ul>
					</div>
					<div class="col-xs-12 col-sm-4">
						<div class="pull-left search-count-content">
							Showing {{$seafarer_data['from']}} - {{$seafarer_data['to']}} of {{$seafarer_data['total']}} Results
						</div>
					</div>
				</div>
			</div>
			
			<div class="full-cnt-loader hidden">
				<div class="" style="text-align: center;padding-top:20px;padding-bottom:20px; ">
				    <i class="fa fa-spinner fa-spin fa-4" aria-hidden="true" style="font-size: 7em;"></i>
			    </div>
			</div>
			<div class="list-container">
			@foreach($seafarer_data['data'] as $data)
				<div class="seafarer-listing-container">
					<div class="panel">
						<div class="panel-heading border-light partition-blue">
							<h5 class="panel-title text-bold header-link-text p-5-5">
									<a href="{{Route('admin.view.seafarer.id',$data['id'])}}">
										{{isset($data['first_name']) ? $data['first_name'] : ''}}
									</a>
							</h5>
							<ul class="panel-heading-tabs border-light">
								<li class="panel-tools">
									<div class="dropdown">
										<a data-toggle="dropdown" class="btn btn-xs dropdown-toggle btn-transparent-grey">
											<i class="fa fa-cog"></i>
										</a>
										<ul class="dropdown-menu dropdown-light pull-right" role="menu">
											<li>
												<a href="{{Route('admin.view.seafarer.id',$data['id'])}}" data-type="user">
													View
												</a>
											</li>
											<li>
												<a href="{{Route('admin.edit.seafarer',$data['id'])}}">
													Edit
												</a>
											</li>
											<li>
												<a class="change-status-user cursor-pointer {{ $data['status'] == '0' ? 'hide' : ''}}" data-status='deactive' data-id={{$data['id']}} id="deactive-{{$data['id']}}">
													Deactivate
												</a>
											</li>
											<li>
												<a class="change-status-user cursor-pointer {{ $data['status'] != '0' ? 'hide' : ''}}"" data-status='active' data-id={{$data['id']}} id="active-{{$data['id']}}">
													Activate
												</a>
											</li>
											<li>
												<a class="resend_welcome_email cursor-pointer" data-id={{$data['id']}}>
													Resend Welcome Email
												</a>
											</li>
										</ul>
									</div>
								</li>
							</ul>
							
						</div>

						<div class="panel-body">
							<div class="row">
								<div class="col-lg-3 col-md-5 col-sm-5" id="user-profile-pic">
									<a>
										<div class="user-profile-pic-thumb make-user-profile-pic-block">
                        					<img src="/{{ isset($data['profile_pic']) ? env('SEAFARER_PROFILE_PATH')."/".$data['id']."/".$data['profile_pic'] : 'images/user_default_image.png'}}" style="height: 200px;width: 200px;">
                        				</div>
                        			</a>
								</div>
								<div class="col-lg-9 col-md-7 col-sm-7">
									<div class="row">
										<div class="col-sm-4 sm-t-20">
											<div class="row">
												<div class="col-xs-12">
													<div class="header-tag">
														Basic Information
													</div>
												</div>
											</div>
											<div id="user-basic-details">
												<div class="row">
													<div class="col-xs-6 col-sm-7">
														<dl>
															<dt> Name : </dt>
															<dd class="margin-bottom-5px">
																{{isset($data['first_name']) ? $data['first_name'] : ''}}
															</dd>
															<dt> Email : 
																@if($data['is_email_verified'] == '1')
																	<i class="fa fa-check-circle green f-15" aria-hidden="true" title="Email Verified"></i>
																@else
																	<i class="fa fa-minus-circle red f-15" aria-hidden="true" title="Email Unverified"></i>
																@endif
															</dt>
															<dd class="margin-bottom-5px make-word-wrap-break">
																{{isset($data['email']) ? $data['email'] : ''}}
												     		</dd>
												     		<dt> Mobile : 
												     			@if($data['is_mob_verified'] == '1')
												     				<i class="fa fa-check-circle green f-15" aria-hidden="true" title="Mobile Unverified"></i>
												     			@else
																	<i class="fa fa-minus-circle red f-15" aria-hidden="true" title="Mobile Unverified"></i>
												     			@endif
												     		</dt>
															<dd class="margin-bottom-5px">
																{{isset($data['mobile']) ? $data['mobile'] : ''}}
												     		</dd>
												     		<dt>
												     		<dt> Last Logged On : </dt>
															<dd class="margin-bottom-5px w-170">
																{{isset($data['last_logged_on']) ? date('d-m-Y H:i a', strtotime($data['last_logged_on'])) : '-'}}
												     		</dd>
												     		<dt>
														</dl>
													</div>
												</div>
											</div>
										</div>
										<?php
                                            if(isset($data['professional_detail']['current_rank_exp'])){
                                                $rank_exp = explode(".",$data['professional_detail']['current_rank_exp']);
                                                $rank_exp[1] = number_format($rank_exp[1]);
                                            }
                                        ?>
										<div class="col-sm-4">
											<div class="header-tag">
												Current Rank Information
											</div>
											<div id="user-professional-details">
												<dl>
													<dt> Current Rank :</dt>
														<dd class="margin-bottom-5px">
															@if(!empty($data['professional_detail']['current_rank']))
			                                                    @foreach(\CommonHelper::new_rank() as $rank_index => $category)
						                                            @foreach($category as $r_index => $rank)
						                                                {{ !empty($data['professional_detail']['current_rank']) ? $data['professional_detail']['current_rank'] == $r_index ? $rank : '' : ''}}
						                                            @endforeach
						                                        @endforeach
						                                    @else
						                                    	-
						                                    @endif
														</dd>
														<dt> Current Rank Experience :</dt>
														<dd class="margin-bottom-5px">
															@if(isset($data['professional_detail']['current_rank_exp']) && !empty($data['professional_detail']['current_rank_exp']))
																{{ isset($data['professional_detail']['current_rank_exp']) ? $rank_exp[0] ." Years ". $rank_exp[1].' Months' : ''}}
															@else
																-
															@endif
														</dd>
														<dt> Date Of Availability :</dt>
														<dd class="margin-bottom-5px">
															{{ isset($data['professional_detail']['availability']) ? date('d-m-Y', strtotime($data['professional_detail']['availability']))  : '-'}}
														</dd>
														<dt> Registered On :</dt>
														<dd class="margin-bottom-5px">
															{{ isset($data['created_at']) && !empty($data['created_at']) ? date('d-m-Y H:i:s', strtotime($data['created_at']))  : '-'}}
														</dd>
												</dl>
											</div>
										</div>
										<div class="col-sm-4">
												<div class="header-tag">
													Passport Information
												</div>
												<div id="user-professional-details">
													<dl>
														<dt> Passport Number :</dt>
														<dd class="margin-bottom-5px">
															{{isset($data['passport_detail']['pass_number']) && !empty($data['passport_detail']['pass_number']) ? $data['passport_detail']['pass_number'] : '-'}} 
															
														</dd>
														<dt> Country :</dt>
														<dd class="margin-bottom-5px">
															@if(isset($data['passport_detail']['pass_country']) && !empty($data['passport_detail']['pass_country']))
																@foreach( \CommonHelper::countries() as $c_index => $country)
			                                                        {{ !empty($data['passport_detail']['pass_country']) ? $data['passport_detail']['pass_country'] == $c_index ? $country : '' : ''  }}
			                                                    @endforeach
			                                                @else
			                                                	-
			                                                @endif
														</dd>
														<dt> Place Of Issue : </dt>
														<dd class="margin-bottom-5px">
															{{ isset($data['passport_detail']['place_of_issue']) && !empty($data['passport_detail']['place_of_issue']) ? $data['passport_detail']['place_of_issue'] : '-'}}  
														</dd>
													</dl>
												</div>
											</div>
										
									</div>
								</div>

							</div>
						</div>
					</div>

				</div>
			@endforeach
			</div>

			<div class="panel-heading">
				<div class="row">
					<div class="col-xs-12 col-sm-8 paginator-content  pull-right no-margin">
						<ul id="top-user-paginator" class="pagination pagination-blue">
							{!! $pagination->render() !!}
						</ul>
					</div>
					<div class="col-xs-12 col-sm-4">
						<div class="search-count-content">
							Showing {{$seafarer_data['from']}} - {{$seafarer_data['to']}} of {{$seafarer_data['total']}} Results
						</div>
					</div>
				</div>
			</div>
		@else
			<div class="no-results-found">No results found. Try again with different search criteria.</div>
		@endif
		</div>
	</div>
</div>