<div class="col-md-12">
	<div class="panel panel-white">
		<div class="panel-body">
			<?php 
				$data_array = $data->toArray();
				$pagination = $data;
			?>
			@if(isset($data_array['data']) && !empty($data_array['data']))
			
				<div class="panel-heading">
					<div class="row">
						<div class="col-xs-12">
							<div class="col-xs-6 paginator-content  pull-right no-margin">
								<ul id="top-user-paginator" class="pagination">
									{!! $data->render() !!}
								</ul>
							</div>
							
							<div class="pull-left search-count-content">
								Showing {{$data_array['from']}} - {{$data_array['to']}} of {{$data_array['total']}} Results
							</div>
						</div>
					</div>
				</div>
				
				<div class="full-cnt-loader hidden">
					<div class="" style="text-align: center;padding-top:20px;padding-bottom:20px; ">
					    <i class="fa fa-spinner fa-spin fa-4" aria-hidden="true" style="font-size: 7em;"></i>
				    </div>
				</div>
				<div class="list-container">

					@foreach($data_array['data'] as $data)	
					<div class="seafarer-listing-container">
						<div class="panel">
							<div class="panel-heading border-light partition-blue">
								<h5 class="panel-title text-bold header-link-text p-5-5">
									<a href="{{Route('admin.view.advertiser.id',$data['id'])}}">
										{{isset($data['first_name']) ? ucfirst($data['first_name']) : ''}}
									</a>
								</h5>
								<ul class="panel-heading-tabs border-light">
									<li class="panel-tools">
										<div class="dropdown">
											<a data-toggle="dropdown" class="btn btn-xs dropdown-toggle btn-transparent-grey">
												<i class="fa fa-cog"></i>
											</a>
											<ul class="dropdown-menu dropdown-light pull-right" role="menu">
												<li>
													<a href="{{Route('admin.view.advertiser.id',$data['id'])}}" data-type="user">
														View
													</a>
												</li>
												<li>
													<a href="{{Route('admin.edit.advertiser',$data['id'])}}">
														Edit
													</a>
												</li>
												<li>
													<a class="change-status-user cursor-pointer {{ $data['status'] == '0' ? 'hide' : ''}}" data-status='deactive' data-id={{$data['id']}} id="deactive-{{$data['id']}}">
														Deactivate
													</a>
												</li>
												<li>
													<a class="change-status-user cursor-pointer {{ $data['status'] != '0' ? 'hide' : ''}}" data-status='active' data-id="{{$data['id']}}" id="active-{{$data['id']}}">
														Activate
													</a>
												</li>
											</ul>
										</div>
									</li>
								</ul>

							</div>
											
							<div class="panel-body">
								<div class="row">
									<div class="col-lg-3 col-md-5 col-sm-5" id="user-profile-pic">
										<a>
											<div class="user-profile-pic-thumb make-user-profile-pic-block">
	                        					<img src="/{{ isset($data['profile_pic']) ? env('SEAFARER_PROFILE_PATH')."/".$data['id']."/".$data['profile_pic'] : 'images/user_default_image.png'}}" style="height: 200px;width: 200px;">
	                        				</div>
	                        			</a>
									</div>
									<div class="col-lg-9 col-md-7 col-sm-7">
										<div class="row">
											<div class="col-sm-6 sm-t-20">
												<div class="row">
													<div class="col-xs-12">
														<div class="header-tag">
															Basic Information
														</div>
													</div>
												</div>
												<div id="user-basic-details">
													<div class="row">
														<div class="col-xs-6 col-sm-7">
															<dl>
																<dt> Owner Name : </dt>
																<dd class="margin-bottom-5px">
																	{{isset($data['first_name']) ? ucfirst($data['first_name']) : ''}}
																</dd>
																<dt> Owner Mobile Number : 
																	@if($data['is_mob_verified'] == '1')
								                                        <i class="fa fa-check-circle green f-15" aria-hidden="true" title="Mobile Verified"></i>
								                                    @else
								                                        <i class="fa fa-minus-circle red f-15" aria-hidden="true" title="Mobile Unverified"></i>
								                                    @endif
																</dt>
																<dd class="margin-bottom-5px make-word-wrap-break">
																	{{isset($data['mobile']) ? $data['mobile'] : ''}}
													     		</dd>
													     		<dt> Owner Email Address :
													     			@if($data['is_email_verified'] == '1')
								                                        <i class="fa fa-check-circle green f-15" aria-hidden="true" title="Email Verified"></i>
								                                    @else
								                                        <i class="fa fa-minus-circle red f-15" aria-hidden="true" title="Email Unverified"></i>
								                                    @endif
													     		</dt>
																<dd class="margin-bottom-5px">
																	{{isset($data['email']) ? $data['email'] : ''}}
													     		</dd>
													     		<dt> Registered On :</dt>
   																<dd class="margin-bottom-5px">
																	{{ isset($data['created_at']) && !empty($data['created_at']) ? date('d-m-Y H:i:s', strtotime($data['created_at']))  : '-'}}
																</dd>
															</dl>
														</div>
													</div>
												</div>
											</div>
											
											<div class="col-sm-4">
												
												<div id="user-professional-details">
													<div class="row">
														<div class="col-xs-12">
															<div class="header-tag">
																Company Details
															</div>
														</div>
													</div>
													<dl>
											     		<dt> Company Name : </dt>
															<dd class="margin-bottom-5px">
																{{ isset($data['company_registration_detail']['company_name']) ? $data['company_registration_detail']['company_name'] : '' }}
												     		</dd>
											     		<dt>
										     		<dt>
													<dt> Member of any club or association :</dt>
													<dd class="margin-bottom-5px">
														@foreach( \CommonHelper::member_category() as $c_index => $category)
	                                                        {{ isset($data['company_registration_detail']['b_i_member']) ? $data['company_registration_detail']['b_i_member'] == $c_index ? $category : '' : ''}}
	                                                    @endforeach
													</dd>
													<dt> Bussiness Category :</dt>
													<dd class="margin-bottom-5px">
														@foreach(\CommonHelper::bussiness_category() as $index => $categories)
					                                        @foreach( $categories as $c_index => $category)
					                                            {{ isset($data['company_registration_detail']['bussiness_category']) ? $data['company_registration_detail']['bussiness_category'] == $c_index ? $category : '' : ''}}
					                                        @endforeach
					                                    @endforeach
													</dd>
													<dt> Bussiness Description :</dt>
													<dd class="margin-bottom-5px">
														{{isset($data['company_registration_detail']['bussiness_description']) ? $data['company_registration_detail']['bussiness_description'] : ''}}
													</dd>
												</dl>
											</div>
										</div>
									</div>
													
								</div>
							</div>
						</div>
										
					</div>
					@endforeach
				</div>
			@else
				<div class="no-results-found">No results found. Try again with different search criteria.</div>
			@endif

			@if(isset($data_array['data']) && !empty($data_array['data']))
										
				<div class="panel-heading">
					<div class="row">
						<div class="col-xs-12">
							<div class="col-xs-6 paginator-content  pull-right no-margin">
								<ul id="top-user-paginator" class="pagination">
									{!! $pagination->render() !!}
								</ul>
							</div>
							
							<div class="pull-left search-count-content">
								Showing {{$data_array['from']}} - {{$data_array['to']}} of {{$data_array['total']}} Results
							</div>
						</div>
					</div>
				</div>
			@endif
		</div>
	</div>
</div>