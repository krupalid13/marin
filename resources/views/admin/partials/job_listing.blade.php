<div class="col-md-12">
	<div class="panel panel-white">
		<div class="panel-body">

			@if(isset($data) && !empty($data))
				
				<div class="panel-heading">
					<div class="row">
						<div class="col-xs-12">
							<div class="col-xs-6 paginator-content  pull-right no-margin">
								<ul id="top-user-paginator" class="pagination">
									{!! $paginate->render() !!}
								</ul>
							</div>
							
							<div class="pull-left search-count-content">
								Showing {{$pagination_data['from']}} - {{$pagination_data['to']}} of {{$pagination_data['total']}} Results
							</div>
						</div>
					</div>
				</div>
				
				<div class="full-cnt-loader hidden">
					<div class="" style="text-align: center;padding-top:20px;padding-bottom:20px; ">
					    <i class="fa fa-spinner fa-spin fa-4" aria-hidden="true" style="font-size: 7em;"></i>
				    </div>
				</div>
				<div class="list-container">
					@foreach($data as $data)	
					<div class="seafarer-listing-container">
						<div class="panel">
							<div class="panel-heading border-light partition-blue">
								<h5 class="panel-title text-bold header-link-text p-5-5">
									<a href="">
										{{isset($data['company_registration_details']['company_name']) ? ucfirst($data['company_registration_details']['company_name']) : ''}}
									</a>
								</h5>

								<ul class="panel-heading-tabs border-light">
									<li class="panel-tools">
										<div class="dropdown">
											<a data-toggle="dropdown" class="btn btn-xs dropdown-toggle btn-transparent-grey">
												<i class="fa fa-cog"></i>
											</a>
											<ul class="dropdown-menu dropdown-light pull-right" role="menu">
												<!-- <li>
													<a href="{{Route('admin.view.advertiser.id',$data['id'])}}" data-type="user">
														View
													</a>
												</li> -->
												<li>
													<a href="{{Route('admin.edit.company.jobs',[$data['company_id'],$data['id']])}}">
														Edit
													</a>
												</li>
												<?php 
													$disable= 'hide';
													$enable= 'hide';
													if(isset($data) && $data['status'] == 1){
														$disable = '';
													}else{
														$enable = '';
													}
												?>
												<li>
													<a class="jobDisableButton disable-{{$data['id']}} {{$disable}}" data-status='disable' data-id={{$data['id']}} href="">
														<span>Disable</span>
													</a>
												</li>
												
												<li>
													<a class="jobDisableButton enable-{{$data['id']}} {{$enable}}" data-status='enable' data-id={{$data['id']}} href="">
														<span>Enable</span>
													</a>
												</li>
												
											</ul>
										</div>
									</li>
								</ul>

							</div>
											
							<div class="panel-body">
								<div class="row">
									
									<div class="col-lg-3 col-md-5 col-sm-5" id="user-profile-pic" style="height: 200px;width: 200px; margin-left: 15px;">
										<a>
											<div class="company_profile_pic">
			                					<img src="/{{ isset($data['company_registration_details']['user_details']['profile_pic']) ? env('COMPANY_LOGO_PATH')."/".$data['company_registration_details']['user_id']."/".$data['company_registration_details']['user_details']['profile_pic'] : 'images/user_default_image.png'}}" >
			                				</div>
                            			</a>
									</div>
									<div class="col-lg-9 col-md-7 col-sm-7">
										<div class="row">																
											<div class="col-sm-4">		
												<div id="user-professional-details">
													<div class="row">
														<div class="col-xs-12">
															<div class="header-tag">
																Company Details
															</div>
														</div>
													</div>
													<dl>
											     		<dt> Company Name : </dt>
															<dd class="margin-bottom-5px">
																{{ isset($data['company_registration_details']['company_name']) ? $data['company_registration_details']['company_name'] : '' }}
												     		</dd>
											     		<dt>
														<dt> Company Email :</dt>
														<dd class="margin-bottom-5px">
															{{isset($data['company_registration_details']['email']) ? $data['company_registration_details']['email'] : ''}}
														</dd>
														<dt> Contact Number :</dt>
														<dd class="margin-bottom-5px">
															{{isset($data['company_registration_details']['contact_number']) ? $data['company_registration_details']['contact_number'] : ''}}
														</dd>
													</dl>
												</div>
											</div>
											<div class="col-sm-4">		
												<div id="user-professional-details">
													<div class="row">
														<div class="col-xs-12">
															<div class="header-tag">
																Job Details
															</div>
														</div>
													</div>
													<dl>
														<dt> Job ID : </dt>
															<dd class="margin-bottom-5px">
																{{ isset($data['id']) ? $data['id'] : ''}}
												     		</dd>
											     		<dt>
											     		<dt> Rank : </dt>
															<dd class="margin-bottom-5px">
																@foreach(\CommonHelper::new_rank() as $rank_index => $category)
									                                @foreach($category as $r_index => $rank)
									                                    {{ isset($data['rank']) ? $data['rank'] == $r_index ? $rank : '' : ''}}
									                                @endforeach
									                            @endforeach
												     		</dd>
											     		<dt>
														<dt> Ship Type :</dt>
														<dd class="margin-bottom-5px">
															@foreach( \CommonHelper::ship_type() as $c_index => $ship_type)
					                                            {{ isset($data['ship_type']) ? $data['ship_type'] == $c_index ? $ship_type : '' : ''}}
					                                        @endforeach
														</dd>
														<dt> Date Of Joining :</dt>
														<dd class="margin-bottom-5px">
															{{ isset($data['date_of_joining']) && !empty($data['date_of_joining']) ? $data['date_of_joining'] : '-'}}
														</dd>
													</dl>
												</div>
											</div>
											<div class="col-sm-4">		
												<div id="user-professional-details">
													<div class="row">
														<div class="col-xs-12">
															<div class="header-tag m_t_20">
																 
															</div>
														</div>
													</div>
													<dl>
											     		<dt> BHP : </dt>
															<dd class="margin-bottom-5px">
																{{ isset($data['bhp']) ? $data['bhp'] : ''}}
												     		</dd>
											     		<dt>
														<dt> GRT :</dt>
														<dd class="margin-bottom-5px">
															{{ isset($data['grt']) ? $data['grt'] : ''}}
														</dd>
														<dt> Engine Type :</dt>
														<dd class="margin-bottom-5px">
															@foreach( \CommonHelper::engine_type() as $c_index => $engine_type)
																{{ !empty($data['engine_type']) ? $data['engine_type'] == $c_index ? $engine_type : '' : ''}}
															@endforeach
														</dd>
														<dt> Minimum Rank Experience :</dt>
														<dd class="margin-bottom-5px">
															{{ isset($data['min_rank_exp']) ? $data['min_rank_exp'] : ''}} Years
														</dd>
													</dl>
												</div>
											</div>
										</div>
									</div>
													
								</div>
							</div>
						</div>
										
					</div>
					@endforeach
				</div>
			@else
				<div class="no-results-found">No results found. Try again with different search criteria.</div>
			@endif

			@if(isset($data) && !empty($data))
				<div class="panel-heading">
					<div class="row">
						<div class="col-xs-12">
							<div class="col-xs-6 paginator-content  pull-right no-margin">
								<ul id="top-user-paginator" class="pagination">
									{!! $paginate->render() !!}
								</ul>
							</div>
							
							<div class="pull-left search-count-content">
								Showing {{$pagination_data['from']}} - {{$pagination_data['to']}} of {{$pagination_data['total']}} Results
							</div>
						</div>
					</div>
				</div>
			@endif

		</div>
	</div>
</div>