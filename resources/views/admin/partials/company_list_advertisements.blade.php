<div class="col-md-12">
	<div class="panel panel-white">
		<div class="panel-body">
			<?php 
				$data_array = $data->toArray();
				$pagination = $data;
			?>
			@if(isset($data_array['data']) && !empty($data_array['data']))
			
				<div class="panel-heading">
					<div class="row">
						<div class="col-xs-12">
							<div class="col-xs-6 paginator-content  pull-right no-margin">
								<ul id="top-user-paginator" class="pagination">
									{!! $data->render() !!}
								</ul>
							</div>
							
							<div class="pull-left search-count-content">
								Showing {{$data_array['from']}} - {{$data_array['to']}} of {{$data_array['total']}} Results
							</div>
						</div>
					</div>
				</div>
				
				<div class="full-cnt-loader hidden">
					<div class="" style="text-align: center;padding-top:20px;padding-bottom:20px; ">
					    <i class="fa fa-spinner fa-spin fa-4" aria-hidden="true" style="font-size: 7em;"></i>
				    </div>
				</div>
				<div class="list-container">
					@foreach($data_array['data'] as $data)	
					<div class="seafarer-listing-container">
						<div class="panel">
							<div class="panel-heading border-light partition-blue">
								<h5 class="panel-title text-bold header-link-text p-5-5">
									<a href="">
										{{isset($data['company_registration_details']['company_name']) ? ucfirst($data['company_registration_details']['company_name']) : ''}}
									</a>
								</h5>
								<ul class="panel-heading-tabs border-light">
									<li class="panel-tools">
										<div class="dropdown">
											<a data-toggle="dropdown" class="btn btn-xs dropdown-toggle btn-transparent-grey">
												<i class="fa fa-cog"></i>
											</a>
											<ul class="dropdown-menu dropdown-light pull-right" role="menu">
												<li>
													<a class="change-adv-status cursor-pointer {{ $data['status'] == '0' ? 'hide' : ''}}" data-status='deactive' data-id={{$data['id']}} id="deactive-{{$data['id']}}">
														Deactivate
													</a>
												</li>
												<li>
													<a class="change-adv-status cursor-pointer {{ $data['status'] != '0' ? 'hide' : ''}}"" data-status='active' data-id={{$data['id']}} id="active-{{$data['id']}}">
														Activate
													</a>
												</li>
											</ul>
										</div>
									</li>
								</ul>

							</div>
											
							<div class="panel-body">
								<div class="row">
									<div class="col-lg-4 col-md-5 col-sm-5" id="user-profile-pic">
										<a>
											<div class="user-profile-pic-thumb make-user-profile-pic-block">
                            					<img src="{{!empty($data['img_path']) ? "/".env('COMPANY_LOGO_PATH').$data['company_id']."/".$data['img_path'] : 'images/user_default_image.png'}}" avatar-holder-src="{{ asset('images/user_default_image.png') }}" height="150" width="250">
                            				</div>
                            			</a>
									</div>
									<div class="col-lg-8 col-md-7 col-sm-7">
										<div class="row">
											<div class="col-sm-6 sm-t-20">
												<div class="row">
													<div class="col-xs-12">
														<div class="header-tag">
															Company Details
														</div>
													</div>
												</div>
												<div id="user-basic-details">
													<div class="row">
														<div class="col-xs-6 col-sm-7">
															<dl>
													     		<dt> Company Name : </dt>
																	<dd class="margin-bottom-5px">
																		{{ isset($data['company_registration_details']['company_name']) ? $data['company_registration_details']['company_name'] : '' }}
													     		</dd>
												     		<dt>
															<dt> Email:</dt>
																<dd class="margin-bottom-5px">
																	{{ isset($data['company_registration_details']['email']) ? $data['company_registration_details']['email'] : '' }}
																</dd>
															<dt> Website :</dt>
															<dd class="margin-bottom-5px">
																<a href="{{ !empty($data['company_registration_details']['website']) ? 'http://'.$data['company_registration_details']['website'] : '-' }}" target="_blank">
																	{{ !empty($data['company_registration_details']['website']) ? $data['company_registration_details']['website'] : '-' }}
																</a>
															</dd>
														</dl>
														</div>
													</div>
												</div>
											</div>
											
											<div class="col-sm-4">
												
												<div id="user-professional-details">
													<div class="row">
														<div class="col-xs-12">
															<div class="header-tag">
																Contact Details
															</div>
														</div>
													</div>
													
													<dl>
														<dt> Contact Person : </dt>
														<dd class="margin-bottom-5px">
															{{isset($data['company_registration_details']['contact_person']) ? ucfirst($data['company_registration_details']['contact_person']) : ''}}
														</dd>
														<dt> Contact Number : </dt>
														<dd class="margin-bottom-5px make-word-wrap-break">
															{{isset($data['company_registration_details']['contact_number']) ? $data['company_registration_details']['contact_number'] : ''}}
											     		</dd>
											     		<dt> Contact Person Email : </dt>
														<dd class="margin-bottom-5px">
															{{isset($data['company_registration_details']['contact_email']) ? $data['company_registration_details']['contact_email'] : ''}}
											     		</dd>
													</dl>
											</div>
										</div>
									</div>
													
								</div>
							</div>
						</div>
										
					</div>
					@endforeach
				</div>

				@if(isset($pagination) && !empty($pagination))
			
					<div class="panel-heading">
						<div class="row">
							<div class="col-xs-12">
								<div class="col-xs-6 paginator-content  pull-right no-margin">
									<ul id="top-user-paginator" class="pagination">
										{!! $pagination->render() !!}
									</ul>
								</div>
								
								<div class="pull-left search-count-content">
									Showing {{$data_array['from']}} - {{$data_array['to']}} of {{$data_array['total']}} Results
								</div>
							</div>
						</div>
					</div>
				@endif
			@else
				<div class="no-results-found">No results found. Try again with different search criteria.</div>
			@endif
		</div>
	</div>
</div>