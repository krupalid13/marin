<?php 
	$data_array = $existing_course_discounts->toArray();
	$pagination = $existing_course_discounts;
?>
@if(isset($data_array['data']) && !empty($data_array['data']))
	<div class="list-container">
		<div class="panel-heading">
			<div class="row">
				<div class="col-xs-12">
					<div class="col-xs-6 paginator-content  pull-right no-margin">
						<ul id="top-user-paginator" class="pagination">
							{!! $existing_course_discounts->render() !!}
						</ul>
					</div>
					
					<div class="pull-left search-count-content">
						Showing {{$data_array['from']}} - {{$data_array['to']}} of {{$data_array['total']}} Results
					</div>
				</div>
			</div>
		</div>
		@foreach($data_array['data'] as $data)	
			<div class="seafarer-listing-container">
				<div class="panel">
					<div class="panel-heading border-light partition-blue">
						<h5 class="panel-title text-bold header-link-text p-5-5">
							{{ isset($data['batch_details']['course_details']['institute_registration_detail']['institute_name']) ? ucwords($data['batch_details']['course_details']['institute_registration_detail']['institute_name']) : '' }}
						</h5>
						<ul class="panel-heading-tabs border-light">
							<li class="panel-tools">
								<div class="dropdown">
									<a data-toggle="dropdown" class="btn btn-xs dropdown-toggle btn-transparent-grey">
										<i class="fa fa-cog"></i>
									</a>
									<ul class="dropdown-menu dropdown-light pull-right" role="menu">
										<li>
											<a class='discount_edit_section' data-id="{{$data['id']}}">
												Edit
											</a>
										</li>
										<li>
											<a class="discount_delete_button" data-id="{{$data['id']}}">
												Delete
											</a>
										</li>
									</ul>
								</div>
							</li>
						</ul>

					</div>
									
					<div class="panel-body">
						<div class="row">
							<div class="col-lg-12 col-md-7 col-sm-7">
								<div class="row">
									<div class="col-sm-4 sm-t-20">
										<div class="row">
											<div class="col-xs-12">
												<div class="header-tag">
													Course Information
												</div>
											</div>
										</div>
										<div id="user-basic-details">
											<div class="row">
												<div class="col-xs-6 col-sm-12">
													<dl>
														<dt> Course type : </dt>
														<dd class="margin-bottom-5px">
															@foreach(\CommonHelper::institute_course_types() as $r_index => $course_type)
	                                                            {{ !empty($data['batch_details']['course_details']['course_type']) ? $data['batch_details']['course_details']['course_type'] == $r_index ? $course_type : '' : ''}}
	                                                        @endforeach
														</dd>
											     		<dt> Course Name :</dt>
														<dd class="margin-bottom-5px">
															{{ isset($data['batch_details']['course_details']['courses'][0]['course_name']) && !empty($data['batch_details']['course_details']['courses'][0]['course_name']) ? $data['batch_details']['course_details']['courses'][0]['course_name']  : '-'}}
														</dd>
														<dt> Course Discount :</dt>
														<dd class="margin-bottom-5px">
															{{ isset($data['discount']) && !empty($data['discount']) ? $data['discount']  : '-'}}%
														</dd>
													</dl>
												</div>
											</div>
										</div>
									</div>
									
									<div class="col-sm-4">
										<div id="user-professional-details">
											<div class="row">
												<div class="col-xs-12">
													<div class="header-tag">
														Advertiser Details
													</div>
												</div>
											</div>
											<dl>
									     		<dt> Company Name : </dt>
													<dd class="margin-bottom-5px">
														{{ isset($data['company_registration']['company_name']) ? $data['company_registration']['company_name'] : '-' }}
									     			</dd>
								     			<dt>
								     			<dt> Company Email : </dt>
													<dd class="margin-bottom-5px">
														{{ isset($data['company_registration']['email_display']) ? $data['company_registration']['email_display'] : '-' }}
									     			</dd>
								     			<dt>
											</dl>
										</div>
									</div>

									<div class="col-sm-4">
										<div id="user-professional-details">
											<div class="row">
												<div class="col-xs-12">
													<div class="header-tag">
														Batch Details
													</div>
												</div>
											</div>
											<dl>
									     		<dt> Start Date : </dt>
													<dd class="margin-bottom-5px">
														{{ isset($data['batch_details']['start_date']) && !empty($data['batch_details']['start_date']) ? date('d-m-Y', strtotime($data['batch_details']['start_date']))  : '-'}}
									     			</dd>
								     			<dt>
								     			<dt> Cost : </dt>
													<dd class="margin-bottom-5px">
														<span class="fa fa-inr"></span>
														{{ isset($data['batch_details']['cost']) ? $data['batch_details']['cost'] : '-' }}
									     			</dd>
								     			<dt>
								     			<dt> Duration : </dt>
													<dd class="margin-bottom-5px">
														{{ isset($data['batch_details']['duration']) ? $data['batch_details']['duration'] : '-' }} Days
									     			</dd>
								     			<dt>
											</dl>
										</div>
									</div>
								</div>
							</div>
											
						</div>
					</div>
				</div>
								
			</div>
		@endforeach
		@if(isset($data_array['data']) && !empty($data_array['data']))

			<div class="panel-heading">
				<div class="row">
					<div class="col-xs-12">
						<div class="col-xs-6 paginator-content  pull-right no-margin">
							<ul id="top-user-paginator" class="pagination">
								{!! $pagination->render() !!}
							</ul>
						</div>
						
						<div class="pull-left search-count-content">
							Showing {{$data_array['from']}} - {{$data_array['to']}} of {{$data_array['total']}} Results
						</div>
					</div>
				</div>
			</div>
		@endif
	</div>
	@else
	<div class="no-results-found">No results found.</div>
	@endif