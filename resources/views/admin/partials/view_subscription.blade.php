<div class="my-subcription">
	<div class="panel-heading">
		<h4 class="panel-title">SUBSCRIPTION DETAILS</h4>
	</div>
	@if(isset($data['from']) AND !empty($data['from']))
		<div class="row">
			<div class="col-md-12" id="main-data">
			    <div class="subcription">
			    	<div class="row">
			            <div class="col-xm-12 col-sm-6 m-t-10">
			            	<span class="pagi pagi-up">
			                    <span class="search-count">
			                        Showing {{$data['from']}} - {{$data['to']}} of {{$data['total']}}
			                    </span>
			                </span>
			            </div>
			            <div class="col-xm-12 col-sm-6">
			                    <nav> 
			                        <ul class="pagination pagination-sm">
			                            {!! $pagination->render() !!}
			                        </ul> 
			                    </nav>
			            </div>
			        </div>
				    <div class="row">
			    		@foreach($data['data'] as $val)
			    		<?php 
			    			$val['subscription_details'] = json_decode($val['subscription_details'],true);
			    			$now =  date('Y-m-d h:i:s');
			    			$valid_from =  date('Y-m-d 00:00:00', strtotime($val['valid_from']));
			    			$valid_to =  date('Y-m-d 23:59:59', strtotime($val['valid_to']));
			    		 ?>
				    	<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
					    	 <div class="package-card">
					    	 	<div class="name content-container">
					    	 		
						 			<div class="content-section">
						 			@if($val['subscription_details']['duration_title'] != 'free')
					    	 			<div class="dropdown setting pull-right">
											<a data-toggle="dropdown" class="btn-xs dropdown-toggle">
												<i class="fa fa-cog"></i>
											</a>
											<ul class="dropdown-menu dropdown-light pull-right">
												<li class="view-sub-modal" data-modal-id={{$val['order_id']}}>
													<a>
														<span>Payment Details</span>
													</a>
												</li>
											</ul>
										</div>
									@endif
						    	 		<div class="title">Package Name:
						    	 		</div>
						    	 		<div class="content">{{$val['subscription_details']['title']}}
						    	 			@if($val['status'] == '1' && strtotime($now) >= strtotime($valid_from) && strtotime($now) <= strtotime($valid_to))
								 				<div class="status package-active">Active</div>
								 			@elseif($val['status'] == '1' && strtotime($now) < strtotime($valid_from))
								 				<div class="status package-expired">Upcoming</div>
							 				@elseif($val['status'] == '1' && strtotime($now) > strtotime($valid_to))
								 				<div class="status package-expired">Expired</div>
							 				@elseif($val['status'] == '2')
								 				<div class="status package-expired">Discontinued</div>
							 				@elseif($val['status'] == '0')
								 				<div class="status package-expired">Cancelled</div>
								 			@endif
						    	 		</div>
						 			</div>
					    	 	</div>
					    	 	<div class="discription content-container">
					    	 		<div class="content-section">
						    	 		<span class="title">Amount:</span>
						    	 		<span class="content"> <i class="fa fa-inr"></i> {{ !empty($val['order']['total']) ? $val['order']['total'] : '-'}} </span>
						 			</div>
						 			<div class="content-section">
						    	 		<span class="title">Start Date:</span>
						    	 		<span class="content"> {{ date('d-m-Y',strtotime($val['valid_from']))}} </span>
						 			</div>
						 			<div class="content-section">
						    	 		<span class="title">Expiry Date:</span>
						    	 		<span class="content">{{ date('d-m-Y',strtotime($val['valid_to']))}}</span>
						 			</div>
					    	 	</div>
					    	 	<!-- <div class="invoice content-container">
					    	 		<div class="content-section">
						    	 		<span class="title">Invoice:</span>
						    	 		<span class="content"><a class="invoice-download" href="#"><img class="invoice-img" src="{{asset('images/home/pdf_img.jpg')}}" alt="">Download</a></span>
						 			</div>
					    	 	</div> -->
					    	 	<!-- <div class="dates content-container">
					    	 		<div class="content-section">
						    	 		<div class="title">Payment Date</div>
						    	 		<div class="content"> - </div>
						 			</div>
						 			<div class="content-section">
						    	 		<div class="title">Renewal Date</div>
						    	 		<div class="content">{{ date('d-m-Y',strtotime($val['valid_to']))}}</div>
						 			</div>
					    	 	</div> -->
					    	 </div>
					    	 <div class="subcription-modal sub-{{$val['order_id']}} modal fade" role="dialog">
						        <div class="modal-dialog">

						            <!-- Modal content-->
						            <div class="modal-content">
						              <div class="modal-header">
						                <button type="button" class="close" data-dismiss="modal">&times;</button>
						                <h4 class="modal-title">Subscription Details</h4>
						              </div>
						              <div class="modal-body">
					             		<div class="row">
					             			<div class="col-sm-4">
					             				<p><b>TxnID</b></p>
					             				<p>
					             					{{isset($val['order']['order_payment']['txnid']) ? $val['order']['order_payment']['txnid'] : '-'}}
					             				</p>
					             			</div>
					             			<div class="col-sm-4">
					             				<p><b>Transaction ID</b></p>
					             				<p>
					             					{{isset($val['order']['order_payment']['transaction_id']) ? $val['order']['order_payment']['transaction_id'] : '-'}}
					             				</p>
					             			</div>
					             			<div class="col-sm-4">
					             				<p><b>Bank Reference Number</b></p>
					             				<p>
					             					{{isset($val['order']['order_payment']['bank_ref_num']) ? $val['order']['order_payment']['bank_ref_num'] : '-'}}
					             				</p>
					             			</div>
					             			<div class="col-sm-4">
					             				<p><b>Mode</b></p>
					             				<p>
					             				<?php 
					             					$mode = isset($val['order']['order_payment']['mode']) ? $val['order']['order_payment']['mode'] : '';
					             					
					             					if(!empty($mode))
					             						$mode_name = \CommonHelper::paymentModes()[$mode];
					             				?>
					             					{{isset($mode_name) ? $mode_name : '-'}}
					             				</p>
					             			</div>
					             			<?php 
				      						$tax = json_decode($val['order']['tax'],true);
				      						$cgst = $tax['cgst'];
				      						$cgst_cost = ($val['order']['subscription_cost']*$cgst)/100;
				      						$sgst = $tax['sgst'];
				      						$sgst_cost = ($val['order']['subscription_cost']*$sgst)/100;
				      						$inr_symbol = "<i class='fa fa-inr'></i>";
				      					 ?>
					             			<div class="col-sm-4">
					             				<p><b>Amount</b></p>
					             				<p>
					             					<i class="fa fa-inr"></i>
					             					{{isset($val['order']['total']) ? $val['order']['total'] : '-'}}
					             					@if($val['subscription_details']['amount'] != 0)
					             					(including tax) <a href="#" data-toggle="tooltip" data-placement="top" title="Cgst({{$cgst}}%):Rs-{{$cgst_cost}} Sgst({{$sgst}}%):Rs-{{$sgst_cost}}"><i class="fa fa-question-circle" aria-hidden="true" style="color:black"></i></a>
					             					@endif
					             				</p>
					             			</div>
					             		</div>
						              </div>
						              <div class="modal-footer">
						                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
						              </div>
						            </div>

						        </div>
						    </div>
					    	  
				    	</div>
				    	@endforeach
				    </div>
				    <div class="row">
			            <div class="col-xm-12 col-sm-6 m-t-10">
			            	<span class="pagi pagi-up">
			                    <span class="search-count">
			                        Showing {{$data['from']}} - {{$data['to']}} of {{$data['total']}}
			                    </span>
			                </span>
			            </div>
			            <div class="col-xm-12 col-sm-6">
			                    <nav> 
			                        <ul class="pagination pagination-sm">
			                            {!! $pagination->render() !!}
			                        </ul> 
			                    </nav>
			            </div>
			        </div>
			    </div>
			</div>
		</div>
	@else
		<div class="row">
			<div class="col-xs-12">
				No Subscriptions Found.
			</div>
		</div>
	@endif
</div>
<script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip(); 
});
</script>

