<a class="closedbar inner hidden-sm hidden-xs open" href="#">
</a>
<nav id="pageslide-left" class="pageslide inner">
    <div class="navbar-content">
        <!-- start: SIDEBAR -->
        <div class="main-navigation left-wrapper transition-left">
            <ul class="main-navigation-menu">
                <li>
                    <a href="javascript:void(0)"><i class="fa fa-desktop"></i> <span class="title"> Seafarers </span><i class="icon-arrow"></i> </a>
                    <ul class="sub-menu">
                        <li>
                            <a href="{{Route('admin.view.seafarer')}}">
                                <span class="title"> View All </span>
                            </a>
                        </li>
                        <li>
                            <a href="{{Route('admin.add.seafarer')}}">
                                <span class="title"> Add Seafarer </span>
                            </a>
                        </li>
                        <li>
                            <a href="{{Route('admin.seafarer.requested.document.list')}}">
                                <span class="title"> Requested Documents List </span>
                            </a>
                        </li>
                    </ul>
                </li>

                <li>
                    <a href="javascript:void(0)"><i class="fa fa-desktop"></i> <span class="title"> Companies </span><i class="icon-arrow"></i> </a>
                    <ul class="sub-menu">
                        <li>
                            <a href="{{Route('admin.add.company')}}">
                                <span class="title"> Add Company </span>
                            </a>
                        </li>
                        <li>
                            <a href="{{Route('admin.view.company')}}">
                                <span class="title"> View All Companies</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{Route('admin.add.company.jobs')}}">
                                <span class="title"> Add Company Jobs</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{Route('admin.view.company.jobs')}}">
                                <span class="title"> View All Jobs</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{Route('admin.view.job.applicant')}}">
                                <span class="title"> View All Applicants</span>
                            </a>
                        </li>
                        <!-- <li>
                                <a href="{{Route('admin.view.company.subscriptions')}}">
                                        <span class="title"> Subscriptions</span>
                                </a>
                        </li> -->
                        <li>
                            <a href="{{Route('admin.add.company.advertise')}}">
                                <span class="title"> Add Advertisement </span>
                            </a>
                        </li>
                        <li>
                            <a href="{{Route('admin.list.company.advertise')}}">
                                <span class="title"> View All Advertisements </span>
                            </a>
                        </li>
                        <li>
                            <a href="{{Route('admin.list.downloaded.document')}}">
                                <span class="title"> Document Downloaded List </span>
                            </a>
                        </li>
                    </ul>
                </li>

                <li>
                    <a href="javascript:void(0)"><i class="fa fa-desktop"></i> <span class="title"> Advertisers </span><i class="icon-arrow"></i> </a>
                    <ul class="sub-menu">
                        <li>
                            <a href="{{Route('admin.add.advertiser')}}">
                                <span class="title"> Add Advertiser </span>
                            </a>
                        </li>
                        <li>
                            <a href="{{Route('admin.view.advertiser')}}">
                                <span class="title"> View All Advertisers </span>
                            </a>
                        </li>
                        <li>
                            <a href="{{Route('admin.add.advertise')}}">
                                <span class="title"> Add Advertisement </span>
                            </a>
                        </li>
                        <li>
                            <a href="{{Route('admin.view.advertisements')}}">
                                <span class="title"> View All Advertisements </span>
                            </a>
                        </li>
                        <li>
                            <a href="{{Route('admin.advertisements.list.enquiries')}}">
                                <span class="title"> View All Enquiries </span>
                            </a>
                        </li>
                    </ul>
                </li>

                <li>
                    <a href="javascript:void(0)"><i class="fa fa-desktop"></i> <span class="title"> Institutes </span><i class="icon-arrow"></i> </a>
                    <ul class="sub-menu">
                        <li>
                            <a href="{{Route('admin.add.institute')}}">
                                <span class="title"> Add Institute </span>
                            </a>
                        </li>
                        <li>
                            <a href="{{Route('admin.view.institutes')}}">
                                <span class="title"> View All Institutes </span>
                            </a>
                        </li>
                        <!-- <li>
                                <a href="{{Route('admin.institute.view.course')}}">
                                        <span class="title"> Courses </span>
                                </a>
                        </li> -->
                        <li>
                            <a href="{{Route('admin.add.course.batch')}}">
                                <span class="title"> Add Batch </span>
                            </a>
                        </li>
                        <li>
                            <a href="{{Route('admin.view.institute.batches')}}">
                                <span class="title"> View All Batches </span>
                            </a>
                        </li>
                        <li>
                            <a href="{{Route('admin.institute.list.course.applicant')}}">
                                <span class="title"> Course Applicant Listing </span>
                            </a>
                        </li>
                        <li>
                            <a href="{{Route('admin.institute.add.course.discount.mapping')}}">
                                <span class="title"> Course Discount Mapping </span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="javascript:void(0)"><i class="fa fa-desktop"></i> <span class="title"> Enquiries </span><i class="icon-arrow"></i> </a>
                    <ul class="sub-menu">
                        <li>
                            <a href="{{Route('admin.contact.us.enquiries')}}">
                                <span class="title"> Contact </span>
                            </a>
                        </li>
                        <li>
                            <a href="{{Route('admin.seaman.book.enquiries')}}">
                                <span class="title"> Seaman Book </span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="javascript:void(0)"><i class="fa fa-desktop"></i> <span class="title"> Scheduled email <!-- & sms --> </span><i class="icon-arrow"></i> </a>
                    <ul class="sub-menu">
                        <li>
                            <a href="{{route('admin.schedule')}}">
                                <span class="title"> Add Schedule </span>
                            </a>
                        </li>
                        <li>
                            <a href="{{route('admin.schedule.list')}}">
                                <span class="title">List Schedules </span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="{{route('admin.notifications')}}"><i class="fa fa-desktop"></i>
                        <span class="title"> Notifications </span>
                    </a>
                </li>
                <li>
                    <a href="javascript:void(0)"><i class="fa fa-desktop"></i> <span class="title"> Job </span><i class="icon-arrow"></i> </a>
                    <ul class="sub-menu">
                        <li>
                            <a href="{{Route('admin.getCompanyList')}}">
                                <span class="title"> Company </span>
                            </a>
                        </li>
                        <li>
                            <a href="{{Route('admin.j.index')}}">
                                <span class="title"> Jobs </span>
                            </a>
                        </li>
                        <li>
                            <a href="{{Route('admin.j.sendIndex')}}">
                                <span class="title"> Send Job </span>
                            </a>
                        </li>
                        <li>
                            <a href="{{Route('admin.j.viewJobAll')}}">
                                <span class="title"> View Job </span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="javascript:void(0)"><i class="fa fa-desktop"></i> <span class="title"> Marketing Emails </span><i class="icon-arrow"></i> </a>
                    <ul class="sub-menu">
                        <li>
                            <a href="{{Route('admin.email')}}">
                                <span class="title"> Promotional Emails </span>
                            </a>
                        </li>
                    </ul>
                </li>
                 <li>
                    <a href="{{route('admin.affiliate')}}"><i class="fa fa-desktop"></i>
                        <span class="title"> Affiliate </span>
                    </a>
                </li>
            </ul>
            <!-- end: MAIN NAVIGATION MENU -->
        </div>
        <!-- end: SIDEBAR -->
    </div>
</nav>