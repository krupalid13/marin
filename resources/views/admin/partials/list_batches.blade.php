<div class="col-md-12">
					<div class="panel panel-white">
						<div class="panel-body">

							<?php 
								$data_array = $data->toArray();
								$pagination = $data;
							?>
							@if(isset($result_data['data']) && !empty($result_data['data']))
							
								<div class="panel-heading">
									<div class="row">
										<div class="col-xs-12">
											<div class="col-xs-6 paginator-content  pull-right no-margin">
												<ul id="top-user-paginator" class="pagination">
													{!! $data->render() !!}
												</ul>
											</div>
											
											<div class="pull-left search-count-content">
												Showing {{$data_array['from']}} - {{$data_array['to']}} of {{$data_array['total']}} Results
											</div>
										</div>
									</div>
								</div>
											
							<div class="full-cnt-loader hidden">
							    <div class="" style="text-align: center;padding-top:20px;padding-bottom:20px; ">
							        <i class="fa fa-spinner fa-spin fa-4" aria-hidden="true" style="font-size: 7em;"></i>
							    </div>
							</div>		
							@foreach($result_data['data'] as $index1 => $data)	
							<div class="seafarer-listing-container">
							<div class="panel">
								<div class="panel-heading border-light partition-blue">
									<h5 class="panel-title text-bold header-link-text p-5-5">
										{{isset($data['institute_registration_detail']['institute_name']) ? $data['institute_registration_detail']['institute_name'].' -' : ''}}

										@if(isset($data['institute_registration_detail']['institute_courses']) AND !empty($data['institute_registration_detail']['institute_courses']))
		                                    @foreach($courses as $index => $course)
                                                {{ !empty($data['course_id']) ? $data['course_id'] == $course['id'] ? $course['course_name'] : '' : ''}}
                                            @endforeach
		                                @endif
									</h5>
									
									<ul class="panel-heading-tabs border-light">
									<li class="panel-tools">
										<div class="dropdown">
											<a data-toggle="dropdown" class="btn btn-xs dropdown-toggle btn-transparent-grey">
												<i class="fa fa-cog"></i>
											</a>
											<ul class="dropdown-menu dropdown-light pull-right" role="menu">
												<!-- <li>
													<a href="{{Route('admin.view.company.id',$data['id'])}}" data-type="user">
														View
													</a>
												</li> -->
												<li>
													<a href="{{Route('admin.institute.edit.course.batch',[$data['institute_registration_detail']['user_id'],$data['id']])}}">
														Edit
													</a>
												</li>
											</ul>
										</div>
									</li>
									</ul>

								</div>
												
							<div class="panel-body">
							<div class="row">
							<div class="col-lg-3 col-md-5 col-sm-5" id="user-profile-pic" style="height: 200px;width: 200px; margin-left: 15px;">
								<a>	
									<div class="company_profile_pic">
	                					<img src="/{{ isset($data['institute_registration_detail']['user']['profile_pic']) ? env('INSTITUTE_LOGO_PATH')."/".$data['institute_registration_detail']['user']['id']."/".$data['institute_registration_detail']['user']['profile_pic'] : 'images/user_default_image.png'}}" >
	                				</div>
                    			</a>
							</div>
							<div class="col-lg-9 col-md-7 col-sm-7">
								<div class="row">
									<div class="col-sm-4 sm-t-20">
										<div class="row">
											<div class="col-xs-12">
												<div class="header-tag">
													Institute Details
												</div>
											</div>
										</div>
										<div id="user-basic-details">
											<div class="row">
												<div class="col-xs-6 col-sm-7">
													<dl>

														<dt> Institute Name : </dt>
														<dd class="margin-bottom-5px">
															{{isset($data['institute_registration_detail']['institute_name']) ? $data['institute_registration_detail']     ['institute_name'] : ''}}
														</dd>
														<dt> Institute Email : </dt>
														<dd class="margin-bottom-5px make-word-wrap-break">
															{{isset($data['institute_registration_detail']     ['email']) ? $data['institute_registration_detail']     ['email'] : ''}}
											     		</dd>
											     		<dt> Contact Person : </dt>
														<dd class="margin-bottom-5px">
															{{isset($data['institute_registration_detail']     ['contact_person']) ? $data['institute_registration_detail']     ['contact_person'] : ''}}
											     		</dd>
											     		<dt> Contact Number : </dt>
														<dd class="margin-bottom-5px">
															{{isset($data['institute_registration_detail']     ['contact_number']) ? $data['institute_registration_detail']     ['contact_number'] : ''}}
											     		</dd>
													</dl>
												</div>
											</div>
										</div>
									</div>
									
									<div class="col-sm-8">
										
										<div id="user-professional-details">
											<div class="row">
												<div class="col-xs-12">
													<div class="header-tag">
														Course Details
													</div>
												</div>
											</div>
											<dl>
									     		<dt> Course Type : </dt>
													<dd class="margin-bottom-5px">
														@foreach(\CommonHelper::institute_course_types() as $index => $category)
					                                        {{ isset($data['institute_registration_detail']['institute_courses'][0]['course_type']) ? $data['institute_registration_detail']['institute_courses'][0]['course_type'] == $index ? $category : '' : ''}}
					                                    @endforeach
										     		</dd>
									     		<dt>
									     		<?php
                                                    $courses = \App\Courses::all()->toArray();
                                                ?>
												<dt> Course Name :</dt>
												<dd class="margin-bottom-5px">
													@if(isset($data['institute_registration_detail']['institute_courses']) AND !empty($data['institute_registration_detail']['institute_courses']))
					                                    @foreach($courses as $index => $course)
                                                            {{ !empty($data['course_id']) ? $data['course_id'] == $course['id'] ? $course['course_name'] : '' : ''}}
                                                        @endforeach
					                                @endif
					                                <!-- {{ isset($data['institute_registration_detail']['institute_courses'][0]['course_name']) ? $data['institute_registration_detail']['institute_courses'][0]['course_name'] : '' }} -->
												</dd>
											</dl>
										</div>
									
											<div id="user-professional-details">
												<?php

				                                    $collection = collect($data['institute_batches']);
				                                    
				                                    $batches = array_values($collection->sortBy('start_date')->toArray());
				                                    $course['institute_batches'] = $batches;
				                                    $Select = 'select';
				                                    // dd($course['institute_batches']);
				                                ?>
				                                <dl>
				                                    <dt>Batches Date: </dt>
				                                    <span class="ans">
				                                        @if(isset($course['institute_batches'][0]))
				                                            <div class="batches dont-break height-equalizer-wrapper dont-break p-t-10">
				                                           
				                                                @foreach($course['institute_batches'] as $key => $value)
				                                                    <div class="date_slide date_slide_{{$value['institute_course_id']}} z-depth-1 {{ $key == 0 ? $Select : ''}}" data-id="{{$value['id']}}" data-key="{{$value['institute_course_id']}}">
				                                                        {{isset($value['start_date']) ? date('d',strtotime($value['start_date'])) : ''}}
				                                                        {{isset($value['start_date']) ? date('M',strtotime($value['start_date'])) : ''}}
				                                                    </div>
				                                                @endforeach
				                                            </div>
				                                        @else
				                                            No batches found.
				                                        @endif
				                                    </span>
				                                </dl>

				                                @foreach($course['institute_batches'] as $key => $value)
		                                            <div class="row details details-key-{{$value['institute_course_id']}} details-{{$value['id']}} {{ $key == 0 ? '' : 'hide'}}">
		                                                <div class="col-sm-6">
		                                                    <dl>
		                                                        <dt>Start Date: </dt>
		                                                        <span class="ans">
		                                                            {{isset($value['start_date']) ? date('d-m-Y',strtotime($value['start_date'])) : ''}}
		                                                        </span>
		                                                    </dl>
		                                                    <dl>
		                                                        <dt>Duration:</dt> 
		                                                        <span class="ans">
		                                                            {{isset($value['duration']) ? $value['duration'] : ''}} Days
		                                                        </span>
		                                                    </dl>
		                                                </div>
		                                                <div class="col-sm-6">
		                                                    <dl>
		                                                        <dt>Cost:</dt> 
		                                                        <span class="ans">
		                                                            <i class="fa fa-inr"></i>
		                                                            {{isset($value['cost']) ? $value['cost'] : ''}}
		                                                        </span>
		                                                    </dl>
		                                                    <dl>
		                                                        <dt>Size:</dt> 
		                                                        <span class="ans">
		                                                            {{isset($value['size']) ? $value['size'] : ''}}
		                                                        </span>
		                                                    </dl>
		                                                </div>

		                                                <?php
		                                                    $location_ids = '';
		                                                    $locations = '';

		                                                    if(isset($value['institute_location']) && !empty($value['institute_location'])){
		                                                        $location_ids = array_values(collect($value['institute_location'])->pluck('location_id')->toArray());
		                                                    }

		                                                    if(isset($result_data['data'][$index1]))
		                                                    	$value1 = $result_data['data'][$index1];

		                                                    $comma = 0;
		                                                    // dd($value1,$location_ids);
		                                                ?>

		                                                <div class="col-sm-12">
		                                                    <dl>
		                                                        <dt>Location:</dt>
		                                                        <span class="ans">
		                                                            @if(isset($value1['institute_registration_detail']['institute_location']))
		                                                                @foreach($value1['institute_registration_detail']['institute_location'] as $index => $data1)
		                                                                    @if($data1['country'] == '95')
		                                                                        @if(!empty($location_ids) && in_array($data1['id'],$location_ids))
		                                                                        	@if($comma == 1)
		                                                                        		,
		                                                                        	@endif

		                                                                            {{$data1['city']['name']}} - {{ucfirst(strtolower($data1['state']['name']))}}
		                                                                            <?php $comma = 1 ?>
		                                                                        @endif
		                                                                    @endif
		                                                                @endforeach
		                                                            @endif
		                                                        </span>
		                                                    </dl>
		                                                </div>
		                                            </div>
	                                            @endforeach
												<!-- <dl>
													<dt> Batch Id :</dt>
													<dd class="margin-bottom-5px">
														{{isset($data['id']) ? $data['id'] : ''}}
													</dd>
													<dt> Batch Duration :</dt>
													<dd class="margin-bottom-5px">
														{{isset($data['duration']) ? $data['duration'] : ''}} Days
													</dd>
													<dt> Batch Size :</dt>
													<dd class="margin-bottom-5px">
														{{isset($data['size']) ? $data['size'] : ''}}
													</dd>
												</dl> -->
											</div>
									</div>
									
								</div>
							</div>
												
							</div>
							</div>
							</div>
												
							</div>
							@endforeach
							@else
								<div class="no-results-found">No results found. Try again with different search criteria.</div>
							@endif

							@if(isset($data_array['data']) && !empty($data_array['data']))

								<div class="panel-heading">
									<div class="row">
										<div class="col-xs-12">
											<div class="col-xs-6 paginator-content  pull-right no-margin">
												<ul id="top-user-paginator" class="pagination">
													{!! $pagination->render() !!}
												</ul>
											</div>
											
											<div class="pull-left search-count-content">
												Showing {{$data_array['from']}} - {{$data_array['to']}} of {{$data_array['total']}} Results
											</div>
										</div>
									</div>
								</div>
							@endif
						</div>
					</div>
				</div>