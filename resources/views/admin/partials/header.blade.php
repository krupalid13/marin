<header class="topbar navbar navbar-inverse navbar-fixed-top inner">
				<!-- start: TOPBAR CONTAINER -->
	<div class="container">
		<div class="navbar-header">
			<a class="sb-toggle-left hidden-md hidden-lg" href="#main-navbar">
				<i class="fa fa-bars"></i>
			</a>
			<!-- start: LOGO -->
			<div class="navbar-brand navbar-left dashboard-title">
				Consultanseas Admin Panel
			</div>
			<!-- end: LOGO -->
		</div>
		<div class="topbar-tools">
			<!-- start: TOP NAVIGATION MENU -->
			<ul class="nav navbar-right">
				<!-- start: USER DROPDOWN -->
				<li class="dropdown current-user">
					<a data-toggle="dropdown" data-hover="dropdown" class="dropdown-toggle" data-close-others="true" href="#">
						<img src="/images/user_default_image.png" class="img-circle" alt="" style="height: 25px;"> <span class="username hidden-xs"> {{ isset(Auth::user()->first_name) ? Auth::user()->first_name : 'Admin'}}</span> <i class="fa fa-caret-down "></i>
					</a>
					<ul class="dropdown-menu dropdown-dark dropdown-admin-section">
						<li>
							<a href="{{route('admin.logout')}}">
								Log Out
							</a>
						</li>
					</ul>
				</li>
				<!-- end: USER DROPDOWN -->
				<li class="hidden-xs hidden-sm hidden-md notification-bell-section">
                    <div class="overlay_section"></div>
                    <a class="pointer notification_bell">
                        <div class="notification-count hide" data-notification="0"></div>
                        <i class="fa fa-bell"></i>
                        <div class="notification_wrapper" style="position: relative;">
                            <div class="dropdown-menu animated notification-main-view pointer flipInY">
                                <div class="notification-title">
                                    Notifications
                                </div>
                                <div class="notification-content-view" id="notification">
                                </div>
                            </div>
                        </div>
                    </a>
                </li>
			</ul>
			<!-- end: TOP NAVIGATION MENU -->
		</div>
	</div>
	<!-- end: TOPBAR CONTAINER -->
</header>