<form id="add-job-form" class="smart-wizard form-horizontal" method="post" action="{{ isset($job_data[0]['current_route']) ? route('admin.update.company.job',$job_data[0]['id']) : route('admin.store.company.job')}}">
    <div id="wizard" class="swMain">
        {{ csrf_field() }}
        <div class="stepContainer">
            <div id="step-1">
                @if(isset($hide_company_name) AND !empty($hide_company_name))
                    <input type="hidden" class="company_id" name="company_id" value="{{isset($data[0]['company_registration_detail']['id']) ? $data[0]['company_registration_detail']['id'] : ''}}">
                @else
                <div class="form-group">
                    <label class="col-sm-3 control-label">
                        Company Name <span class="symbol required"></span>
                    </label>
                    <div class="col-sm-7">
                        <select id="company_id" name="company_id" class="form-control search-select">
                            <option value=''>Select Company</option>
                            @if(isset($company_list) AND !empty($company_list))
                                @foreach($company_list as $index => $list)
                                    <option value="{{isset($list['id']) ? $list['id'] : ''}}" {{ isset($job_data[0]['company_id']) ? $job_data[0]['company_id'] == $list['id'] ? 'selected' : '' : ''}} {{ isset($data[0]['company_registration_detail']['id']) ? $data[0]['company_registration_detail']['id'] == $list['id'] ? 'selected' : '' : ''}}>{{$list['company_name']}}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
                @endif 

                <div class="form-group">
                    <label class="col-sm-3 control-label">
                        Ship Name<span class="symbol required"></span>
                    </label>
                    <div class="col-sm-7">
                        <select id="ship_detail_id" name="ship_detail_id" class="form-control ship_name_change search-select">
                            <option value=''>Select Ship Name</option>
                            @if(isset($ship_data[0]['ship_type']))
                                @foreach($ship_data[0]['ship_type'] as $c_index => $ship_type)
                                    <option value="{{ $ship_type['id'] }}" {{ !empty($job_data[0]['ship_detail_id']) ? $job_data[0]['ship_detail_id'] == $ship_type['id'] ? 'selected' : '' : ''  }}> {{ $ship_type['ship_name'] }}</option>
                                @endforeach
                            @endif
                            @if(isset($data[0]['company_registration_detail']['company_ship_details']))
                                @foreach($data[0]['company_registration_detail']['company_ship_details'] as $c_index => $ship_type)
                                    <option value="{{ $ship_type['id'] }}" {{ !empty($job_data[0]['ship_detail_id']) ? $job_data[0]['ship_detail_id'] == $ship_type['id'] ? 'selected' : '' : ''  }}> {{ $ship_type['ship_name'] }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">
                        Job Category<span class="symbol required"></span>
                    </label>
                    <div class="col-sm-7">
                        <select id="job_category" name="job_category" class="form-control search-select">
                            <option value=''>Select Category</option>
                            @foreach(\CommonHelper::seafarer_job_category() as $index => $category)
                                <option value="{{$index}}" {{ isset($job_data[0]['job_category']) ? $job_data[0]['job_category'] == $index ? 'selected' : '' : ''}}>{{$category}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">
                        Rank<span class="symbol required"></span>
                    </label>
                    <div class="col-sm-7">
                        <select id="rank" name="rank" class="form-control search-select">
                            <option value=''>Select</option>
                            @foreach(\CommonHelper::new_rank() as $rank_index => $category)
                                <optgroup label="{{$rank_index}}">
                                @foreach($category as $r_index => $rank)
                                    <option value="{{$r_index}}" {{ isset($job_data[0]['rank']) ? $job_data[0]['rank'] == $r_index ? 'selected' : '' : ''}}>{{$rank}} </option>
                                @endforeach
                            @endforeach
                        </select>
                    </div>
                </div>
                <?php

                    if(isset($data[0]['company_registration_detail']['company_ship_details']) && !empty($data[0]['company_registration_detail']['company_ship_details'])){
                        foreach ($data[0]['company_registration_detail']['company_ship_details'] as $key => $value) {
                            $ships[] = $value['ship_type'];
                        }
                    }
                    
                    /*if(isset($ships) && !empty($ships)){
                        foreach (\CommonHelper::ship_type() as $key => $value) {
                            if(in_array($key, $data)){
                                $options = "<option value=$key>$value</option>";
                            }
                        }
                    }*/
                ?>

                <div class="form-group">
                    <label class="col-sm-3 control-label">
                        Ship Type<span class="symbol required"></span>
                    </label>
                    <div class="col-sm-7">
                        <select id="ship_type" class="form-control ship_type_change_name search-select" disabled="disabled">
                            <option value=''>Select Ship Type</option>
                            @foreach( \CommonHelper::ship_type() as $c_index => $ship_type)
                                <!-- @if(isset($ships) && !empty($ships))
                                    @if(in_array($c_index,$ships))
                                        <option value="{{ $c_index }}" {{ !empty($job_data[0]['ship_type']) ? $job_data[0]['ship_type'] == $c_index ? 'selected' : '' : ''  }}> {{ $ship_type }}</option>
                                    @endif
                                @endif -->
                                <option value="{{ $c_index }}" {{ !empty($job_data[0]['ship_type']) ? $job_data[0]['ship_type'] == $c_index ? 'selected' : '' : ''  }}> {{ $ship_type }}</option>
                            @endforeach
                        </select>
                        <input type="hidden" name="ship_type" class="ship_type" value="{{isset($job_data[0]['ship_type']) ? $job_data[0]['ship_type'] : ''}}">
                    </div>
                </div>

                <?php
                    $nationality_id = '';
                    if(isset($job_data[0]['job_nationality'] ) && !empty($job_data[0]['job_nationality'] )){
                        $nationality_id = array_values(collect($job_data[0]['job_nationality'] )->pluck('nationality')->toArray());
                    }

                ?>

                <div class="form-group">
                    <label class="col-sm-3 control-label">
                        Nationality<span class="symbol required"></span>
                    </label>
                    <div class="col-sm-7">
                        <select multiple="multiple" name="nationality[]" class="form-control location select2-select" style="height: auto;padding: 0px;" placeholder="Select Nationality">
                            <option value="">Select Nationality</option>
                            @foreach(\CommonHelper::countries() as $c_index => $country)
                                @if(!empty($nationality_id) && in_array($c_index,$nationality_id))
                                    <option value="{{ $c_index }}" selected="selected"> {{ $country }}</option>
                                @else
                                    <option value="{{ $c_index }}"> {{ $country }}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">
                        Valid From<span class="symbol required"></span>
                    </label>
                    <div class="col-sm-7">
                        <input type="text" class="form-control datepicker" id="valid_from" name="valid_from" value="{{ isset($job_data[0]['valid_from']) ? $job_data[0]['valid_from'] : '' }}" placeholder="dd-mm-yyyy">
                    </div>
                </div>
                <?php 
                    if(isset($job_data[0]['date_of_joining']) && !empty($job_data[0]['date_of_joining'])){
                        $parts = explode('-', $job_data[0]['date_of_joining']);
                        $month = $parts[1];
                        $year = $parts[2];
                        
                        $last_day = cal_days_in_month(CAL_GREGORIAN,$month,$year);
                        $job_data[0]['date_of_joining'] = $month."-".$year;
                    }
                ?>
                <div class="form-group">
                    <label class="col-sm-3 control-label">
                        Date Of Joining
                    </label>
                    <div class="col-sm-7">
                        <input type="text" class="form-control m-y-datepicker" id="date_of_joining" name="date_of_joining" value="{{ isset($job_data[0]['date_of_joining']) ? $job_data[0]['date_of_joining'] : '' }}" placeholder="mm-yyyy">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">
                        Approx GRT
                    </label>
                    <div class="col-sm-7">
                        <input type="text" class="form-control grt" placeholder="Please enter grt" value="{{ isset($job_data[0]['grt']) ? $job_data[0]['grt'] : '' }}" disabled="disabled">
                        <input type="hidden" class="grt" name="grt" value="{{ isset($job_data[0]['grt']) ? $job_data[0]['grt'] : '' }}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">
                       Approx BHP
                    </label>
                    <div class="col-sm-7">
                        <input type="text" class="form-control bhp" placeholder="Please enter bhp" id="bhp" name="bhp" value="{{ isset($job_data[0]['bhp']) ? $job_data[0]['bhp'] : '' }}" disabled="disabled">
                        <input type="hidden" class="bhp" name="bhp" value="{{ isset($job_data[0]['bhp']) ? $job_data[0]['bhp'] : '' }}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">
                        Engine Type<span class="symbol required"></span>
                    </label>
                    <div class="col-sm-7">
                        <select name="engine_type" class="form-control search-select engine_type" block-index="0" disabled="disabled">
                            <option value="">Select Engine Type</option>
                            @foreach( \CommonHelper::engine_type() as $c_index => $engine_type)
                                <option value="{{ $c_index }}" {{ isset($job_data[0]['engine_type']) ? $job_data[0]['engine_type'] == $c_index ? 'selected' : '' : ''}}> {{ $engine_type }}</option>
                            @endforeach
                        </select>
                        <input type="hidden" class="engine_type" name="engine_type" value="{{ isset($job_data[0]['engine_type']) ? $job_data[0]['engine_type'] : '' }}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">
                        Minimum Rank Exp<span class="symbol required"></span>
                    </label>
                    <div class="col-sm-7">
                        <input type="text" class="form-control" placeholder="Please enter minimum rank experience in years" id="min_rank_exp" name="min_rank_exp" value="{{ isset($job_data[0]['min_rank_exp']) ? $job_data[0]['min_rank_exp'] : '' }}">
                    </div>
                </div>
                <?php
                    $dollar = '';
                    $rupees = '';
                    if(isset($job_data[0]['wages_currency'])){
                        if($job_data[0]['wages_currency'] == 'dollar'){
                            $dollar = 'active';
                        }else{
                            $rupees = 'active';
                        }
                    }else{
                        $rupees = 'active';
                    }
                ?>
                <div class="form-group">
                    <label class="col-sm-3 control-label">
                        Wages Offered
                    </label>
                    <div class="col-sm-7">
                       <div class="col-xs-2 m-t-5 p-0">
                            <div class="btn-group" id="wages_currency" data-toggle="buttons">
                                <label class="btn btn-default btn-on btn-xs {{$dollar}}">
                                    <input type="radio" value="dollar" name="wages_currency" checked="checked">
                                    <i class="fa fa-dollar fa-lg"></i>
                                </label>
                                <label class="btn btn-default btn-xs btn-off {{$rupees}}">
                                    <input type="radio" value="rupees" name="wages_currency">
                                    <i class="fa fa-inr fa-lg"></i>
                                </label>
                            </div>
                        </div>
                        <div class="col-xs-6 p-l-0">
                            <input type="text" class="form-control" placeholder="Please enter wages offered" name="wages_offered" value="{{ isset($job_data[0]['wages_offered']) ? $job_data[0]['wages_offered'] : '' }}">
                        </div>
                        <div class="col-xs-4 p-10-0">
                            Per Month
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">
                        Job Description
                    </label>
                    <div class="col-sm-7">
                        <textarea class="form-control" placeholder="Please enter job description" id="job_description" name="job_description" rows="5">{{isset($job_data[0]['job_description']) ? $job_data[0]['job_description']:''}}</textarea>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-2 col-sm-offset-10">
                        <button type="button" data-style="zoom-in" class="btn btn-blue ladda-button btn-block submitCompanyJobsButton" id="submitCompanyJobsButton">
                            Save <i class="fa fa-arrow-circle-right"></i>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>