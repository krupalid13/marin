<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Consultanseas</title>
        <!-- start: META -->
        <meta charset="utf-8" />
        <!--[if IE]><meta http-equiv='X-UA-Compatible' content="IE=edge,IE=9,IE=8,chrome=1" /><![endif]-->
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="black">
        <meta content="" name="description" />
        <meta content="" name="author" />
        <link rel="shortcut icon" type="image/x-icon" href="{{asset('public/images/favicon.ico')}}">
        <link rel="stylesheet" href="{{asset('public/assets/css/vendors/plugins/bootstrap/bootstrap.min.css')}}">
        <link rel="stylesheet" href="{{asset('public/assets/css/vendors/plugins/fontawesome/font-awesome.min.scss')}}">
        <link rel="stylesheet" href="{{asset('public/assets/css/vendors/plugins/animate/animate.min.css')}}">
        <link rel="stylesheet" href="{{asset('public/assets/css/vendors/plugins/iCheck/all.css')}}">
        <!-- <link rel="stylesheet" href="/css/vendors/plugins/ladda-bootstrap/ladda.min.css"> -->
        <!-- <link rel="stylesheet" href="/css/vendors/plugins/ladda-bootstrap/ladda-themeless.min.css"> -->

        <!-- <link rel="stylesheet" href="/css/vendors/plugins/toastr/toastr.min.css"> -->


        <link rel="stylesheet" href="{{asset('public/assets/css/vendors/plugins/css/styles.css')}}">
        <link rel="stylesheet" href="{{asset('public/assets/css/vendors/plugins/css/styles-responsive.css')}}">
        <link rel="stylesheet" href="{{asset('public/assets/css/vendors/plugins.css')}}">
        <link rel="stylesheet" href="{{asset('public/assets/css/vendors/themes/theme-style9.css')}}">

        <link rel="stylesheet" href="{{asset('public/assets/css/admin_app.css')}}">
        <script type="text/javascript" src="{{asset('public/assets/js/site_jquery.js')}}"></script>

    </head>

    <body class="login">
        <div class="login">
            @yield('content')
        </div>

        <script type="text/javascript" src="{{asset('public/assets/js/admin_plugins.js')}}"></script>
        <script type="text/javascript" src="{{asset('public/assets/js/admin_app.js')}}"></script>

        <script>
            jQuery(document).ready(function() {
                Main.init();
                Login.init();
            });
        </script>
    </body>
</html>