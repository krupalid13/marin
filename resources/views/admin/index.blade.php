<!DOCTYPE html>
<html lang="en">
    <head>
        <?php $site_version = date('Y-m-d'); ?>
        <title>Consultanseas</title>
        <!-- start: META -->
        <meta charset="utf-8" />
        <!--[if IE]><meta http-equiv='X-UA-Compatible' content="IE=edge,IE=9,IE=8,chrome=1" /><![endif]-->
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="black">
        <meta content="" name="description" />
        <meta content="" name="author" />
        <meta name="_token" content="{{ csrf_token() }}">
        <link rel="shortcut icon" type="image/x-icon" href="{{asset('public/images/favicon.ico')}}">
        <link href='https://fonts.googleapis.com/css?family=Raleway:400,300,500,600,700,200,100,800' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="{{URL::to('public/assets/css/vendors/plugins/bootstrap/bootstrap.min.css')}}">
        <link rel="stylesheet" href="{{URL::to('public/assets/css/vendors/plugins/fontawesome/font-awesome.min.scss')}}">
        <link rel="stylesheet" href="{{URL::to('public/assets/css/vendors/plugins/iCheck/all.css')}}">
        <link rel="stylesheet" href="{{URL::to('public/assets/css/vendors/plugins/perfect-scrollbar/perfect-scrollbar.css')}}">
        <link rel="stylesheet" href="{{URL::to('public/assets/css/vendors/plugins/animate/animate.min.css')}}">
        <link rel="stylesheet" href="{{URL::to('public/assets/css/vendors/plugins/bootstrap-datepicker/bootstrap-datepicker.css')}}">
        <link rel="stylesheet" href="{{URL::to('public/assets/css/vendors/plugins/Jcrop/jquery.Jcrop.min.css')}}">
        
        
        
        <link rel="stylesheet" href="{{URL::to('public/assets/css/vendors/plugins/ladda-bootstrap/ladda.min.css')}}">
        <link rel="stylesheet" href="{{URL::to('public/assets/css/vendors/plugins/ladda-bootstrap/ladda-themeless.min.css')}}">

       @hasSection('IS_SELECT2')
        <link rel="stylesheet" href="{{URL::to('public/assets/css/vendors/plugins/select2/select2.css')}}">
        @endif  
        <link rel="stylesheet" href="{{URL::to('public/assets/css/vendors/plugins.css')}}">
        <link rel="stylesheet" href="{{URL::to('public/assets/css/vendors/plugins/toastr/toastr.min.css')}}">
        <link rel="stylesheet" href="{{URL::to('public/assets/css/vendors/plugins/bootstrap-fileupload/bootstrap-fileupload.min.css')}}">
        <link rel="stylesheet" href="{{URL::to('public/assets/css/vendors/styles.css')}}">
        <link rel="stylesheet" href="{{URL::to('public/assets/css/vendors/plugins/bootstrap-timepicker/bootstrap-timepicker.min.css')}}">
        <link rel="stylesheet" href="{{URL::to('public/assets/css/vendors/styles.css')}}">
        <link rel="stylesheet" href="{{URL::to('public/assets/css/vendors/styles-responsive.css')}}">
        <link rel="stylesheet" href="{{URL::to('public/assets/css/vendors/themes/theme-default.css" type="text/css" id="skin_color')}}">


        <link rel="stylesheet" href="{{URL::to('public/assets/css/admin_app.css')}}?v=<?php echo $site_version; ?>">
        <script type="text/javascript" src="{{URL::to('public/js/site_jquery.js')}}?v=<?php echo $site_version; ?>"></script>
        <!-- <script src="https://cdn.pubnub.com/sdk/javascript/pubnub.4.21.2.js"></script> -->
        @yield('style_section')
    </head>
    <body>
    <div class="se-pre-con"></div>
        <input type="hidden" id="api-check-email" value="{{ route('admin.check.email') }}">
        <input type="hidden" id="api-check-mobile" value="{{ route('admin.check.mobile') }}">
        <input type="hidden" id="api-pincode-related-data" value="{{ route('admin.pincode.get.states_cities') }}">
        <input type="hidden" id="api-admin-company-profile-path" value="{{ route('admin.company.store.profilePic') }}">
        <input type="hidden" id="api-admin-seafarer-profile-path" value="{{ route('admin.seafarer.store.profilePic') }}">
        <input type="hidden" id="api-admin-seafarer-change-status" value="{{ route('admin.change.seafarer.status') }}">
        <input type="hidden" id="api-admin-state-list" value="{{ route('admin.state.list','%test') }}">
        <input type="hidden" id="api-admin-store-advertiser-form-route" value="{{ route('admin.store.advertiser') }}">
        <input type="hidden" id="api-admin-add-seafarer" value="{{ route('admin.add.seafarer') }}">
        <input type="hidden" id="api-admin-advertisements-change-status" value="{{ route('admin.change.advertisements.status') }}">

        <input type="hidden" id="api-admin-get-company-ships" value="{{ route('admin.get.company.ships',['%test']) }}">
        <input type="hidden" id="api-admin-company-data-ships" value="{{ route('admin.company.related.ships',['%test','%test']) }}">

        <input type="hidden" id="api-admin-store-seafarer-step-route" value="{{ route('admin.store.seafarer',['%test','%test']) }}">
        
        <input type="hidden" id="api-admin-store-seafarer-step-update-route" value="{{ route('admin.update.seafarer',['%test','%test']) }}">

        <input type="hidden" id="api-admin-store-company-step-route" value="{{route('admin.store.company',['%test','%test']) }}">

        <input type="hidden" id="api-admin-store-company-step-update-route" value="{{ route('admin.update.company',['%test','%test']) }}">

        <input type="hidden" id="api-institute-course-name-by-course-types" value="{{ route('site.institute.get.course.name','%test') }}">
        <input type="hidden" id="api-admin-institute-logo" value="{{ route('admin.institute.store.logo') }}">
        
        <input type="hidden" id="api-institute-location-by-institute-id" value="{{ route('admin.get.institute.location.id',['%test']) }}">

        <input type="hidden" id="api-admin-company-data-ship-name" value="{{ route('site.company.details.ship.name.by.ship.id',['%test','%test']) }}">

        <input type="hidden" id="api-admin-company-data-ships-by-ship-type" value="{{ route('site.company.details.related.ships.by.ship.type','%test') }}">

        <input type="hidden" id="api-admin-seafarer-download-excel" value="{{ route('admin.download.excel') }}">

        <input type="hidden" id="api-admin-company-download-excel" value="{{ route('admin.download.company.excel') }}">

        <input type="hidden" id="api-admin-view-seafarer" value="{{ route('admin.view.seafarer') }}">
        <input type="hidden" id="api-admin-get-user-subscription" value="{{ route('admin.get.user.subscription') }}">

        <input type="hidden" id="api-admin-seafarer-store-single-service-details" value="{{ route('admin.user.store.single.service.details','%test') }}">

        <input type="hidden" id="api-admin-seafarer-delete-service-by-service-id" value="{{ route('admin.user.delete.service.details') }}">
        <input type="hidden" id="api-admin-seafarer-get-service-by-service-id" value="{{ route('admin.user.get.service.details') }}">

        <input type="hidden" id="api-admin-institute-delete-course" value="{{ route('admin.institute.delete.course',['%test','%test']) }}">

        <input type="hidden" id="api-admin-seafarer-store-course" value="{{ route('admin.user.store.seafarer.course.details') }}">
        <input type="hidden" id="api-admin-seafarer-delete-course" value="{{ route('admin.user.seafarer.delete.details') }}">
        <input type="hidden" id="api-admin-seafarer-get-course-by-course-id" value="{{ route('admin.user.seafarer.get.course.details') }}">
        <input type="hidden" id="api-admin-get-course-by-course-type" value="{{ route('admin.user.seafarer.get.course.by.course.type') }}">
        <input type="hidden" id="api-admin-send-welcome-email" value="{{ route('admin.send.seafarer.welcome.email') }}">

        <input type="hidden" id="api-admin-company-get-vessel-by-vessel-id" value="{{ route('admin.company.get.vessel.details','%test') }}">
        <input type="hidden" id="api-admin-company-delete-vessel-by-vessel-id" value="{{ route('admin.company.delete.vessel.details','%test') }}">

        <input type="hidden" id="api-admin-company-get-agent-by-agent-id" value="{{ route('admin.company.get.agent.details','%test') }}">
        <input type="hidden" id="api-admin-company-delete-agent-by-agent-id" value="{{ route('admin.company.delete.agent.details','%test') }}">

        <input type="hidden" id="api-agent-team-profile-pic" value="{{ route('admin.company.store.agent.profilePic') }}">

        <input type="hidden" id="api-admin-company-update-team-by-team-id" value="{{ route('admin.company.update.team.details','%test') }}">
        <input type="hidden" id="api-admin-company-delete-team-by-team-id" value="{{ route('admin.company.delete.team.details','%test') }}">

        <input type="hidden" id="api-company-team-profile-pic" value="{{ route('admin.company.team.store.profilePic') }}">

        <input type="hidden" id="api-institute-get-by-course" value="{{ route('admin.institute.get.institute.list.by.course') }}">
         
        <input type="hidden" id="api-edit-course-discount-batch" value="{{ route('admin.institute.edit.course.discount.mapping') }}">

        <input type="hidden" id="api-delete-course-discount-batch" value="{{ route('admin.institute.delete.course.discount.mapping') }}">

        <!-- //Notification -->
        <input type="hidden" id="api-get-user-notification" value="{{ route('admin.get.user.notification') }}">

        <input type="hidden" id="api-upload-user-notification" value="{{ route('admin.upload.user.notification') }}">

        <input type="hidden" id="api-set-user-notification-read" value="{{ route('admin.set.user.notification.read') }}">

        @include('admin.partials.sliding_bar')
        @include('common.profile-pic-modal')
        <div id="confirm-reject-modal" class="modal fade" role="dialog">
          <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title modal-heading">List of all cities</h4>
              </div>
              <div class="modal-body">
                <p class="modal-body-content">Some text in the modal.</p>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-red" id="modal-reject-btn">Close</button>
                <button type="button" class="btn btn-info" id="modal-confirm-btn">Confirm</button>
              </div>
            </div>

          </div>
        </div>

        <div class="main-wrapper">
            @include('admin.partials.header')

            @include('admin.partials.side_nav_bar')

            <div class="main-container inner">
                <!-- start: PAGE -->
                <div class="main-content">
                    @yield('content')
                </div>
            </div>

        </div>

        <?php
            $india_value = array_search('India',\CommonHelper::countries());
            $user_id = '';

            if(Auth::check()){
                $user_id = Auth::User()->id;
            }
        ?>

        <script type="text/javascript">

        // subscription feature variable start
        var check_company_subscription_url = "{{route('site.checkSubscriptionAvailability')}}";
        var feature_1 = "{{config('feature.feature1')}}";
        var feature_2 = "{{config('feature.feature2')}}";
        var feature_3 = "{{config('feature.feature3')}}";
        var user_id = "{{$user_id}}";
        var subscribeKey = "{{env('PUBNUB_SUBSCRIBE_KEY')}}";
        var publishKey = "{{env('PUBNUB_PUBLISH_KEY')}}";
        // subscription feature variable end

    </script>

        <script>
            var india_value = '{{$india_value}}';
            var admin_site = 'true';
        </script>

    <script type="text/javascript" src="{{URL::to('public/assets/js/admin_plugins.js?v='.$site_version)}}"></script>
    <script type="text/javascript" src="{{URL::to('public/assets/js/admin_app.js?v='.$site_version)}}"></script>

    <script>
        jQuery(document).ready(function() {
            Main.init();
            Login.init();
        });
        $(window).load(function() {
            $(".se-pre-con").fadeOut("slow");
        });
    </script>

    @include('admin.partials.flashmessage')
    
    <script src='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-growl/1.0.0/jquery.bootstrap-growl.min.js'></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-jgrowl/1.4.7/jquery.jgrowl.css" />
    <script type="text/javascript" src="{{URL::to('public/js/common.js')}}"></script>

    @yield('js_script')



    </body>
</html>