@extends('admin.index')

@section('content')

    <div class="container advertise-container">
        <div class="toolbar row">
            <div class="col-sm-12">
                <div class="page-header">
                    <h1>Advertise<small>Add Advertisements</small></h1>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 m-t-25">
                <!-- start: FORM WIZARD PANEL -->
                <div class="panel panel-white">
                    <div class="panel-body">
                        <input type="hidden" name="role" id="role" value="admin">
                        
                            <div id="" class="swMain">
                                <div id="step-1">

                                <form role="form" class="form-horizontal" id='add-advertise-form' action="{{route('admin.company.advertise.store')}}">
                                    <input type="hidden" id="role" value="admin">
                                    {{ csrf_field() }}
                                    <div class="form-group">
                                        <div class="row no-margin">
                                            <div class="col-sm-7">
                                                <label class="col-sm-4 control-label">
                                                    Company Name <span class="symbol required"></span>
                                                </label>

                                                <div class="col-sm-8">
                                                    <select id="company_id" name="company_id" class="form-control">
                                                        <option value=''>Select Company</option>
                                                        @if(isset($company_list) AND !empty($company_list))
                                                            @foreach($company_list as $index => $list)
                                                                <option value="{{isset($list['id']) ? $list['id'] : ''}}" {{ isset($job_data[0]['job_category']) ? $job_data[0]['job_category'] == $index ? 'selected' : '' : ''}}>{{$list['company_name']}}</option>
                                                            @endforeach
                                                        @endif
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row no-margin">
                                            <div class="col-sm-7">
                                                <label class="col-sm-4 control-label">
                                                    Image Upload <span class="symbol required"></span>
                                                </label>
                                                <br>
                                                <div class="col-sm-8 fileupload fileupload-new" data-provides="fileupload">
                                                    <div class="fileupload-new thumbnail">
                                                        <img src="{{ asset('images/no-image-advertisements.png') }}" alt=""/>
                                                    </div>
                                                    <div class="fileupload-preview fileupload-exists thumbnail" id='advertise_img' name='advertise_img'></div>
                                                    <span class="help-block" style="color: #737373;"></span>
                                                    <div class="row">
                                                        <div class="col-xs-12">
                                                            <i class="fa fa-info-circle"></i> Note: Please upload .jpeg .png .gif file only.<br>(Latest Ad image will be displayed.)
                                                        </div>
                                                    </div>
                                                    <div class="row m-t-10">
                                                        <div class="col-xs-12">
                                                            <span class="btn btn-light-grey btn-file" style="height: 35px;">
                                                                <span class="fileupload-new">
                                                                    <i class="fa fa-picture-o"></i> Select image</span>
                                                                <span class="fileupload-exists">
                                                                    <i class="fa fa-picture-o"></i> Change</span>
                                                                <input type="file" class="advertise-file-uplaod" name="advertise_upload">
                                                            </span>
                                                            <a href="#" class="btn fileupload-exists btn-light-grey remove_advertise_btn" data-dismiss="fileupload">
                                                                <i class="fa fa-times"></i> Remove
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div>
                                                        <label for="advertise_upload" class="error" id="advertise_upload_error"></label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row no-margin">
                                            <div class="col-sm-2 col-sm-offset-10">
                                                <button type="button" data-style="zoom-in" class="btn btn-blue btn-block ladda-button" id="submitAdvertiseDetailButton">
                                                    Save <i class="fa fa-arrow-circle-right"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('js_script')
    <script type="text/javascript" src="/js/site/company-registration.js"></script>
@stop