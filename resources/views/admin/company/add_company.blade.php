@extends('admin.index')

@section('content')

<?php
	$initialize_pincode_maxlength_validation = false;
	$india_value = array_search('India',\CommonHelper::countries());
?>

<div class="container">
	<div class="row">
		<div class="col-sm-12">
			<!-- start: FORM WIZARD PANEL -->
			<div class="panel panel-white">
				<div class="panel-body">
					<form id="add-company-form" class="smart-wizard form-horizontal" action="{{ isset($user_data['current_route']) ? 'update' : 'store'}}" 
					method="post">
						{{ csrf_field() }}
						@if(isset($user_data['current_route']))
							<input type="hidden" id="edit">
						@endif
						<div id="wizard" class="swMain">
							<ul>
								<li>
									<a href="#step-1">
										<div class="stepNumber">
											1
										</div>
										<span class="stepDesc"> Step 1
											<br />
											<small>Company Details</small> </span>
									</a>
								</li>
								<li>
									<a href="#step-2">
										<div class="stepNumber">
											2
										</div>
										<span class="stepDesc"> Step 2
											<br />
											<small>Company Locations</small> </span>
									</a>
								</li>
							</ul>
							<div class="progress progress-xs transparent-black no-radius active">
								<div aria-valuemax="100" aria-valuemin="0" role="progressbar" class="progress-bar partition-green step-bar">
									<span class="sr-only"> 0% Complete (success)</span>
								</div>		
							</div>
							<div id="step-1">
								<h2 class="StepTitle">Login Details</h2>
								<div class="panel-heading">
									<h4 class="panel-title">ACCOUNT CREDENTIALS</h4>
								</div>
								<div class="alert alert-danger alert-box" id="alert-box-documents" style="display: none">
		                            <ul>
		                                @foreach ($errors->all() as $error)
		                                    <li>{{ $error}}</li>
		                                @endforeach
		                            </ul>
		                    	</div>
		                    	<input type="hidden" name="company_id" id="company_id" value="{{ isset($user_data[0]['id']) ? $user_data[0]['id'] : ''}}">
	                                <div class="form-group">
											<label class="col-sm-3 control-label">
												Contact Person Email <span class="symbol required"></span>
											</label>
	                                       	<div class="col-sm-7">
		                                        <input type="email" class="form-control" id="email" name="email" placeholder="Type your email address"
		                                               value="{{ isset($user_data[0]['email']) ? $user_data[0]['email'] : ''}}">
		                                        <span class="hide" id="email-error" style="color: red"></span>
		                                    </div>
	                                    </div>
									@if(!isset($user_data['current_route']))
										<div class="form-group">
											<label class="col-sm-3 control-label">
												Password <span class="symbol required"></span>
											</label>
											<div class="col-sm-7">
												<input type="password" class="form-control" id="password" name="password" placeholder="Type your password">
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-3 control-label">
												Confirm Password <span class="symbol required"></span>
											</label>
											<div class="col-sm-7">
												<input type="password" class="form-control" id="cpassword" name="cpassword" placeholder="Type password again">
											</div>
										</div>
									@endif
									<div class="form-group">
										<label class="col-sm-3 control-label">
											Contact Person Name <span class="symbol required"></span>
										</label>
										<div class="col-sm-7">
											<input type="text" class="form-control" id="contact_person" name="contact_person" placeholder="Type your contact person name" value="{{ isset($user_data[0]['company_registration_detail']['contact_person']) ? $user_data[0]['company_registration_detail']['contact_person'] : ''}}">
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label">
											Contact Person Mobile <span class="symbol required"></span>
										</label>
										<div class="col-sm-7">
											<input type="text" class="form-control" id="contact_person_number" name="contact_person_number" placeholder="Type your contact person number" value="{{ isset($user_data[0]['company_registration_detail']['contact_number']) ? $user_data[0]['company_registration_detail']['contact_number'] : ''}}">
										</div>
									</div>
									<div class="panel-heading">
										<h4 class="panel-title">COMPANY DETAILS</h4>
									</div>
									<div class="form-group company-registration">
										<input type="hidden" name="image-x" required>
                                        <input type="hidden" name="image-y" required>
                                        <input type="hidden" name="image-x2" required>
                                        <input type="hidden" name="image-y2" required>
                                        <input type="hidden" name="crop-w" required>
                                        <input type="hidden" name="crop-h" required>
                                        <input type="hidden" name="image-w" required>
                                        <input type="hidden" name="image-h" required>
                                        <input type="hidden" name="uploaded-file-name" required>
                                        <input type="hidden" name="uploaded-file-path" required>
										
										<div class="row no-margin">
                                            <div class="col-sm-7 col-sm-offset-3">
		                                        <div class="upload-photo-container">
				                                    <div class="image-content">
				                                        <div class="registration-profile-image">
				                                            <?php
				                                            $image_path = '';
				                                            if(isset($user_data[0]['profile_pic'])){
				                                                $image_path = env('COMPANY_LOGO_PATH')."".$user_data[0]['id']."/".$user_data[0]['profile_pic'];
				                                            }
				                                            ?>
				                                            @if(empty($image_path))
				                                                <div class="icon profile_pic_text p-t-30"><i class="fa fa-camera" aria-hidden="true"></i></div>
				                                                <div class="image-text profile_pic_text">Upload Logo <br> Picture</div>
				                                            @endif

				                                            <input type="file" name="profile_pic" id="profile-pic" class="cursor-pointer" onChange="profilePicSelectHandler('add-company-form', 'profile_pic', 'logo' , 'company')">

				                                            @if(!empty($image_path))
				                                                <img id="preview" class="preview" src="/{{ $image_path }}">
				                                            @else
				                                                <img id="preview">
				                                            @endif
				                                        </div>
				                                    </div>
				                                </div>
				                            </div>
				                        </div>
									</div>
									
									<div class="form-group">
										<label class="col-sm-3 control-label">
											Company Name <span class="symbol required"></span>
										</label>
										<div class="col-sm-7">
											<input type="text" class="form-control" id="company_name" name="company_name" placeholder="Type company name" value="{{ isset($user_data[0]['company_registration_detail']['company_name']) ? $user_data[0]['company_registration_detail']['company_name'] : ''}}">
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label">
											Company Email ID <span class="symbol required"></span>
										</label>
										<div class="col-sm-7">
											<input type="text" class="form-control" id="company_email" name="company_email" placeholder="Type company email" value="{{ isset($user_data[0]['company_registration_detail']['company_detail']['company_email']) ? $user_data[0]['company_registration_detail']['company_detail']['company_email'] : ''}}">
										</div>
									</div>
									<div class="form-group pincode-block-0">
										<label class="col-sm-3 control-label">
											Company Phone Number
										</label>
										<div class="col-sm-7">
											<input type="text" class="form-control" id="cmpany_contact_number" name="company_contact_number" placeholder="Type your company phone number" value="{{ isset($user_data[0]['company_registration_detail']['company_detail']['company_contact_number']) ? $user_data[0]['company_registration_detail']['company_detail']['company_contact_number'] : ''}}">
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label">
											Company Website
										</label>
										<div class="col-sm-7">
											<div class="input-group">
												<span class="input-group-addon">http://</span>
	                                                <input type="text" class="form-control" id="website" name="website" placeholder="Type your website address" value="{{ isset($user_data[0]['company_registration_detail']['website']) ? $user_data[0]['company_registration_detail']['website'] : ''}}"></span>
	                                            <label for="website" class="error"></label>
	                                        </div>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label">
											Company Fax
										</label>
										<div class="col-sm-7 ">
											<input type="text" class="form-control" id="fax" name="fax" placeholder="Type your fax number" value="{{ isset($user_data[0]['company_registration_detail']['company_detail']['fax']) ? $user_data[0]['company_registration_detail']['company_detail']['fax'] : ''}}">
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label">
											Company Type <span class="symbol required"></span>
										</label>
										<div class="col-sm-7">
											<select name="company_type" id="company_type" class="form-control">
                                                <option value="">Select Company Type</option>
                                                @foreach( \CommonHelper::company_type() as $c_index => $company_type)
                                                    <option value="{{ $c_index }}" {{ isset($user_data[0]['company_registration_detail']['company_detail']['company_type']) ? $user_data[0]['company_registration_detail']['company_detail']['company_type'] == $c_index ? 'selected' : '' : ''}}> {{ $company_type }}</option>
                                                @endforeach
                                            </select>
										</div>
									</div>

									@if(isset($user_data[0]['company_registration_detail']['company_detail']['company_type']) && $user_data[0]['company_registration_detail']['company_detail']['company_type'] == 1)
										<div class="form-group rpsl_number_textbox">
											<label class="col-sm-3 control-label">
												RPSL Number <span class="symbol required"></span>
											</label>
											<div class="col-sm-7">
												<input type="text" class="form-control" id="rpsl_no" name="rpsl_no" placeholder="Type your rpsl number" value="{{ isset($user_data[0]['company_registration_detail']['company_detail']['rpsl_no']) ? $user_data[0]['company_registration_detail']['company_detail']['rpsl_no'] : ''}}">
											</div>
										</div>
                                    @else
                                        <div class="form-group rpsl_number_textbox hide">
                                        <label class="col-sm-3 control-label">
												RPSL Number <span class="symbol required"></span>
											</label>
	                                        <div class="col-sm-7">
	                                            <input type="text" class="form-control" id="rpsl_no" name="rpsl_no" placeholder="Type your rpsl number" value="{{ isset($user_data[0]['company_registration_detail']['company_detail']['rpsl_no']) ? $user_data[0]['company_registration_detail']['company_detail']['rpsl_no'] : ''}}">
	                                        </div>
                                    </div>
                                    @endif

									<div class="form-group">
										<label class="col-sm-3 control-label">
											Description <span class="symbol required"></span>
										</label>
										<div class="col-sm-7">
											<textarea class="form-control" id="company_description" name="company_description" rows="5" placeholder="Type your company description">{{ isset($user_data[0]['company_registration_detail']['company_detail']['company_description']) ? $user_data[0]['company_registration_detail']['company_detail']['company_description'] : ''}}</textarea>
			                                </div>
										</div>
								<div class="form-group">
									<div class="col-sm-2 col-sm-offset-10">
										<button type="button" data-style="zoom-in" class="btn btn-blue next-step btn-block company-next-btn ladda-button" data-form="step-1" data-type='basic_details'>
											Next <i class="fa fa-arrow-circle-right"></i>
										</button>
									</div>
								</div>
							</div>

							<div id="step-2" class="content">
								<div class="panel-heading">
									<h4 class="panel-title">LOCATION DETAILS</h4>
								</div>
								<input type="hidden" name="user_id" value="{{isset($user_data[0]['id']) ? $user_data[0]['id'] : ''}}">
								
								<div>
									 @if(isset($user_data[0]['company_registration_detail']['company_locations']) && !empty($user_data[0]['company_registration_detail']['company_locations']))
	                                    @foreach($user_data[0]['company_registration_detail']['company_locations'] as $index => $value)
	                                    <input type="hidden" class="total_location_block_index" value={{$index}}>
	                                    <input type="hidden" name="existing_location_id[{{$index}}]" value="{{$value['id']}}">
	                                    <div id="locations-template" class="locations-template multiple-info">
	                                        <div class="row">
	                                            <div class="col-xs-12">
	                                                <div class="input-label p-t-10 location-name pull-left">Company Location {{$index+1}}</div>
	                                                @if($index > 0)
	                                                    <div class="pull-right p-t-10 close-button close-button-{{$index}}">
	                                                        <i class="fa fa-times" aria-hidden="true"></i>
	                                                    </div>
	                                                @else
	                                                    <div class="pull-right p-t-10 close-button hide close-button-{{$index}}">
	                                                        <i class="fa fa-times" aria-hidden="true"></i>
	                                                    </div>
	                                                @endif
	                                            </div>
	                                        </div>
	                                        <div class="form-group">
	                                            <div class="col-sm-3">
	                                                <div>
	                                                    <label class="input-label">Country<span class="symbol required"></span></label>
	                                                    <select id='country' name="country[{{$index}}]" class="form-control country" block-index="{{$index}}">
	                                                        <option value="">Select Your Country</option>
	                                                        @foreach( \CommonHelper::countries() as $c_index => $country)
	                                                            <option value="{{ $c_index }}" {{ isset($value['country']) ? $value['country'] == $c_index ? 'selected' : '' : ''}}> {{ $country }}</option>
	                                                        @endforeach
	                                                    </select>
	                                                </div>
	                                            </div>
	                                            <div class="col-sm-3">
	                                                <div class="pincode-block pincode-block-{{$index}}">
	                                                    <label class="input-label" for="pin_code">Postal Code<span class="symbol required"></span></label>
	                                                    <div>
	                                                        <input type="hidden" class="pincode-id" name="pincode_id[{{$index}}]" value="{{ isset($value['pincode_id']) ? $value['pincode_id'] : ''}}">
	                                                        
	                                                        <i class="fa fa-spin fa-refresh select-loader hide pincode-loader pincode-loader-{{$index}}"></i>
	                                                        <input type="text" block-index="{{$index}}" data-form-id="add-company-form" class="form-control pincode pin_code_fetch_input" name="pincode[{{$index}}]" value="{{ isset($value['pincode_text']) ? $value['pincode_text'] : ''}}" placeholder="Type your pincode">
	                                                    </div>
	                                                </div>
	                                            </div>
	                                            <div class="col-sm-3">
	                                                <div class="state-block state-block-{{$index}}">
	                                                    <label class="input-label">State<span class="symbol required"></span></label>
	                                                    @if(isset($value))
	                                                        @if($value['country'] == $india_value )
	                                                        <select id="state" name="state[{{$index}}]" class="form-control state fields-for-india">
	                                                            <option value="">Select Your State</option>
	                                                            @foreach($value['pincode']['pincodes_states'] as $pincode_states)
	                                                                <option value="{{$pincode_states['state_id']}}" {{$pincode_states['state_id'] == $value['state_id'] ? 'selected' : ''}}>{{ucwords(strtolower($pincode_states['state']['name']))}}</option>
	                                                            @endforeach
	                                                        </select>
	                                                        <input type="text" id="state" name="state_name[{{$index}}]" class="form-control state_text hide fields-not-for-india" placeholder="Enter State Name" value="">

	                                                        @else
	                                                            <select id="state" name="state[{{$index}}]" class="form-control state hide fields-for-india">
	                                                                <option value="">Select Your State</option>
	                                                            </select>
	                                                            <input type="text" name="state_text[{{$index}}]" class="form-control state_text fields-not-for-india" placeholder="Enter State Name" value="{{ isset($value['state_text']) ? $value['state_text'] : ''}}">
	                                                        @endif
	                                                    @else
	                                                        <select id="state" name="state[{{$index}}]" class="form-control state hidden fields-for-india">
	                                                            <option value="">Select Your State</option>
	                                                        </select>
	                                                        <input type="text" id="state" name="state_text[{{$index}}]" class="form-control state_text fields-not-for-india" placeholder="Enter State Name" value="{{ isset($value['state_text']) ? $value['state_text'] : ''}}">
	                                                    @endif
	                                                </div>
	                                            </div>
	                                            <div class="col-sm-3">
	                                                <div class="city-block city-block-{{$index}}">
	                                                    <label class="input-label">City<span class="symbol required"></span></label>

	                                                    @if(isset($value['country']))
	                                                        @if( $value['country'] == $india_value )
	                                                            <select id="city" name="city[{{$index}}]" class="form-control city fields-for-india">
	                                                                <option value="">Select Your City</option>
	                                                                @foreach($value['pincode']['pincodes_cities'] as $pincode_city)
	                                                                    <option value="{{$pincode_city['city_id']}}" {{$pincode_city['city_id'] == $value['city_id'] ? 'selected' : ''}}>{{ucwords(strtolower($pincode_city['city']['name']))}}</option>
	                                                                @endforeach
	                                                            </select>
	                                                            <input type="text" name="city_text[{{$index}}]" class="form-control city_text hide fields-not-for-india" placeholder="Enter City Name" value="">

	                                                        @else
	                                                            <select id="city" name="city[{{$index}}]" class="form-control city hide fields-for-india">
	                                                                <option value="">Select Your City</option>
	                                                            </select>

	                                                            {{--<input type="text" name="city_text[{{$index}}]" class="form-control city_text fields-not-for-india" placeholder="Enter City Name" value="">--}}
	                                                            <input type="text" name="city_text[{{$index}}]" class="form-control city_text fields-not-for-india" placeholder="Enter City Name" value="{{ isset($value['city_text']) ? $value['city_text'] : ''}}">
	                                                        @endif
	                                                    @else
	                                                        <select id="city" name="city[{{$index}}]" class="form-control city hide fields-for-india">
	                                                            <option value="">Select Your City</option>
	                                                        </select>

	                                                        {{--<input type="text" name="city_text[{{$index}}]" class="form-control city_text fields-not-for-india" placeholder="Enter City Name" value="">--}}
	                                                        <input type="text" name="city_text[{{$index}}]" class="form-control city_text fields-not-for-india" placeholder="Enter City Name" value="{{ isset($value['city_text']) ? $value['city_text'] : ''}}">
	                                                    @endif
	                                                </div>
	                                            </div>
	                                        </div>
	                                        <div class="form-group">
	                                            <div class="col-sm-3">
	                                                <div class="address-block address-block-{{$index}}">
	                                                    <label class="input-label" for="address">Address<span class="symbol required"></span></label>
	                                                    <textarea class="form-control address" id="address" name="address[{{$index}}]" placeholder="Type your address" rows="5">{{ isset($value['address']) ? $value['address'] : ''}}</textarea>
	                                                </div>
	                                            </div>
	                                            <div class="col-sm-3">
	                                                <div class="row">
	                                                    <div class="col-xs-6">
	                                                        <div>
	                                                            <label class="input-label" for="is_headoffice">Is Headoffice<span class="symbol required"></span></label>
	                                                           <div class="row">
		                                                    		<div class="col-sm-6">
			                                                            <label class="radio-inline">
			                                                                <input type="radio" class="is_headoffice" name="is_headoffice[{{$index}}]" value="1" {{ !empty($value['is_headoffice']) ? $value['is_headoffice'] == 1 ? 'checked' : '' : ''}}> Yes
			                                                            </label>
			                                                        </div>
			                                                        <div class="col-sm-3">
			                                                            <label class="radio-inline">
			                                                                <input type="radio" class="is_headoffice" name="is_headoffice[{{$index}}]" value="0" {{ !empty($value['is_headoffice']) ? $value['is_headoffice'] == 0 ? 'checked' : '' : 'checked'}}> No
			                                                            </label>
			                                                        </div>
		                                                        </div>
	                                                        </div>
	                                                    </div>
	                                                </div>
	                                            </div>
                                                <div class="col-sm-3">
                                                    <div class="telephone-block telephone-block-{{$index}}">
                                                        <label class="input-label" for="telephone">
                                                            Telephone
                                                        </label>
                                                        <input type="text" name="telephone[{{$index}}]" class="form-control telephone" placeholder="Enter Telephone Number" value="{{ isset($value['telephone']) ? $value['telephone'] : ''}}">
                                                    </div>
                                                </div>
	                                        </div>
	                                    	<hr>
	                                    </div>
	                                    @endforeach
	                                @else
	                                    <div id="locations-template" class="locations-template multiple-info">
	                                        <div class="row">
	                                            <div class="col-xs-12">
	                                                <div class="input-label p-t-10 location-name pull-left">Company Location 1</div>
	                                                <div class="pull-right close-button hide close-button-0">
	                                                    <i class="fa fa-times" aria-hidden="true"></i>
	                                                </div>
	                                            </div>
	                                        </div>
	                                        <div class="form-group">
	                                            <div class="col-sm-3">
	                                                <div>
	                                                    <label class="input-label">Country<span class="symbol required"></span></label>
	                                                    <select name="country[0]" id='country' class="form-control country" block-index="0">
	                                                        <option value="">Select Your Country</option>
	                                                        @foreach( \CommonHelper::countries() as $c_index => $country)
	                                                            @if(isset($value['country']))
	                                                                <option value="{{ $c_index }}" {{ isset($value['country']) ? $value['country'] == $c_index ? 'selected' : '' : ''}}> {{ $country }}</option>
	                                                            @else
	                                                                <option value="{{ $c_index }}" {{ $india_value == $c_index ? 'selected' : ''}}> {{ $country }}</option>
	                                                            @endif
	                                                        @endforeach
	                                                    </select>
	                                                </div>
	                                            </div>
	                                            <div class="col-sm-3">
	                                                <div class="pincode-block pincode-block-0">
	                                                    <label class="input-label" for="pin_code">Postal Code<span class="symbol required"></span></label>
	                                                    <div>
	                                                        <input type="hidden" class="pincode-id" name="pincode_id[0]" value="{{ isset($value['pincode_id']) ? $value['pincode_id'] : ''}}">
	                                                        
	                                                        <i class="fa fa-spin fa-refresh select-loader hide pincode-loader pincode-loader-0"></i>
	                                                        <input type="text" block-index="0" data-form-id="add-company-form" class="form-control pincode pin_code_fetch_input" name="pincode[0]" value="{{ isset($value['pincode_text']) ? $value['pincode_text'] : ''}}" placeholder="Type your pincode">
	                                                    </div>
	                                                </div>
	                                            </div>
	                                            <div class="col-sm-3">
	                                                <div class="state-block state-block-0">
	                                                    <label class="input-label">State<span class="symbol required"></span></label>
	                                                    @if(isset($value))
	                                                        @if($value['country'] == $india_value )
	                                                        <select name="state[0]" class="form-control state fields-for-india">
	                                                            <option value="">Select Your State</option>
	                                                            @foreach($value['pincode']['pincodes_states'] as $pincode_states)
	                                                                <option value="{{$pincode_states['state_id']}}" {{$pincode_states['state_id'] == $value['state_id'] ? 'selected' : ''}}>{{ucwords(strtolower($pincode_states['state']['name']))}}</option>
	                                                            @endforeach
	                                                        </select>
	                                                        <input type="text" id="state" name="state_name[0]" class="form-control state_text hide fields-not-for-india" placeholder="Enter State Name" value="">

	                                                        @else
	                                                            <select name="state[0]" class="form-control state hide fields-for-india">
	                                                                <option value="">Select Your State</option>
	                                                            </select>
	                                                            <input type="text" name="state_text[0]" class="form-control state_text fields-not-for-india" placeholder="Enter State Name" value="{{ isset($value['state_text']) ? $value['state_text'] : ''}}">
	                                                        @endif
	                                                    @else
	                                                        <select name="state[0]" class="form-control state fields-for-india">
	                                                            <option value="">Select Your State</option>
	                                                        </select>
	                                                        <input type="text" id="state" name="state_text[0]" class="form-control hide state_text fields-not-for-india" placeholder="Enter State Name" value="{{ isset($value['state_text']) ? $value['state_text'] : ''}}">
	                                                    @endif
	                                                </div>
	                                            </div>
	                                            <div class="col-sm-3">
	                                                <div class="city-block city-block-0">
	                                                    <label class="input-label">City<span class="symbol required"></span></label>

	                                                    @if(isset($value['country']))
	                                                        @if( $value['country'] == $india_value )
	                                                            <select name="city[0]" class="form-control city fields-for-india">
	                                                                <option value="">Select Your City</option>
	                                                                @foreach($value['pincode']['pincodes_cities'] as $pincode_city)
	                                                                    <option value="{{$pincode_city['city_id']}}" {{$pincode_city['city_id'] == $value['city_id'] ? 'selected' : ''}}>{{ucwords(strtolower($pincode_city['city']['name']))}}</option>
	                                                                @endforeach
	                                                            </select>
	                                                            <input type="text" name="city_text[0]" class="form-control city_text hide fields-not-for-india" placeholder="Enter City Name" value="">

	                                                        @else
	                                                            <select name="city[0]" class="form-control city hide fields-for-india">
	                                                                <option value="">Select Your City</option>
	                                                            </select>

	                                                            {{--<input type="text" name="city_text[0]" class="form-control city_text fields-not-for-india" placeholder="Enter City Name" value="">--}}
	                                                            <input type="text" name="city_text[0]" class="form-control city_text fields-not-for-india" placeholder="Enter City Name" value="{{ isset($value['city_text']) ? $value['city_text'] : ''}}">
	                                                        @endif
	                                                    @else
	                                                        <select name="city[0]" class="form-control city fields-for-india">
	                                                            <option value="">Select Your City</option>
	                                                        </select>

	                                                        {{--<input type="text" name="city_text[0]" class="form-control city_text fields-not-for-india" placeholder="Enter City Name" value="">--}}
	                                                        <input type="text" name="city_text[0]" class="form-control city_text hide fields-not-for-india" placeholder="Enter City Name" value="{{ isset($value['city_text']) ? $value['city_text'] : ''}}">
	                                                    @endif
	                                                </div>
	                                            </div>

	                                        </div>
	                                        <div class="form-group">
	                                            <div class="col-sm-3">
	                                                <div class="address-block address-block-0">
	                                                    <label class="input-label" for="address">Address<span class="symbol required"></span></label>
	                                                    <textarea class="form-control address" name="address[0]" placeholder="Type your address" rows="5">{{ isset($value['address']) ? $value['address'] : ''}}</textarea>
	                                                </div>
	                                            </div>
	                                            <div class="col-sm-3">
	                                                <div class="row">
	                                                    <div class="col-xs-12">
                                                            <label class="input-label" for="is_headoffice">Is Headoffice<span class="symbol required"></span></label>
                                                            <div class="row">
		                                                    	<div class="col-sm-3">
		                                                            <label class="radio-inline">
		                                                                <input type="radio" class="is_headoffice" name="is_headoffice[0]" value="1" {{ !empty($value['is_headoffice']) ? $value['is_headoffice'] == 1 ? 'checked' : '' : ''}}> Yes
		                                                            </label>
		                                                        </div>
		                                                       	<div class="col-sm-3">
		                                                            <label class="radio-inline">
		                                                                <input type="radio" class="is_headoffice" name="is_headoffice[0]" value="0" {{ !empty($value['is_headoffice']) ? $value['is_headoffice'] == 0 ? 'checked' : '' : 'checked'}}> No
		                                                            </label>
		                                                        </div>
	                                                        </div>
	                                                    </div>
	                                                </div>
	                                            </div>
	                                            <div class="col-sm-3">
                                                    <div class="telephone-block telephone-block-0">
                                                        <label class="input-label" for="telephone">
                                                            Telephone
                                                        </label>
                                                        <input type="text" name="telephone[0]" class="form-control telephone" placeholder="Enter Telephone Number" value="{{ isset($value['telephone']) ? $value['telephone'] : ''}}">
                                                    </div>
                                                </div>
	                                        </div>
	                                    </div>
	                                @endif
	                            </div>

                                <div class="row no-margin" id="add-more-location-row">
                                    <div class="col-sm-12">
                                        <button type="button" data-form-id="add-company-form" class="btn add-location-button location-button-0 add-more-button pull-right" id="add-location">Add Location</button>
                                    </div>
                                </div>
								
								
								<!-- <div class="panel-heading">
									<h4 class="panel-title">SHIP DETAILS</h4>
								</div>
								@if(isset($user_data[0]['company_registration_detail']['company_ship_details']) && !empty($user_data[0]['company_registration_detail']['company_ship_details']))
                                    @foreach($user_data[0]['company_registration_detail']['company_ship_details'] as $index => $value)
                                        <input type="hidden" class="total_block_index" value={{$index}}>
                                        <div id="ships-template" class="ships-template multiple-info">
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <div class="input-label p-t-10 ship-name pull-left">Ship Details {{$index+1}}</div>
                                                    @if($index > 0)
                                                        <div class="pull-right p-t-10 ship-close-button ship-close-button-{{$index}}">
                                                            <i class="fa fa-times" aria-hidden="true"></i>
                                                        </div>
                                                    @else
                                                        <div class="pull-right p-t-10 ship-close-button hide ship-close-button-{{$index}}">
                                                            <i class="fa fa-times" aria-hidden="true"></i>
                                                        </div>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="form-group">
                                            	<div class="col-sm-4">
                                                    <div class="">
                                                        <label class="input-label" for="ship_name">Ship Name<span class="symbol required"></span></label>
                                                        <input type="text" name="ship_name[{{$index}}]" class="form-control ship_name" value="{{ isset($value['ship_name']) ? $value['ship_name'] : ''}}" placeholder="Type your ship name">
                                                    </div>
                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="">
                                                        <label class="input-label" for="no-of-ships">Type Of Ships<span class="symbol required"></span></label>
                                                        <select name="ship_type[{{$index}}]" class="form-control ship_type">
                                                            <option value="">Select Ship Type</option>
                                                            @foreach( \CommonHelper::ship_type() as $c_index => $ship_type)
                                                                <option value="{{ $c_index }}" {{ isset($value['ship_type']) ? $value['ship_type'] == $c_index ? 'selected' : '' : ''}}> {{ $ship_type }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="">
                                                        <label class="input-label" for="no-of-ships">Flag Of Ship</label>
                                                        <select name="ship_flag[{{$index}}]" class="form-control ship_flag" block-index="0">
                                                                <option value="">Select ship flag</option>
                                                                @foreach( \CommonHelper::countries() as $c_index => $ship_flag)
                                                                    <option value="{{ $c_index }}" {{ isset($value['ship_flag']) ? $value['ship_flag'] == $c_index ? 'selected' : '' : ''}}> {{ $ship_flag }}</option>
                                                                @endforeach
                                                        </select>
                                                    </div>
                                                </div>

                                                
                                            </div>
                                            <div class="form-group">
                                            	<div class="col-sm-4">
                                                    <div class="">
                                                        <label class="input-label" for="grt">GRT<span class="symbol required"></span></label>
                                                        <input type="text" class="form-control grt" name="grt[{{$index}}]" value="{{ isset($value['grt']) ? $value['grt'] : ''}}" placeholder="Type your GRT">
                                                    </div>
                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="">
                                                        <label class="input-label" for="engine_type">Engine Type<span class="symbol required"></span></label>
                                                        <select name="engine_type[{{$index}}]" class="form-control engine_type" block-index="0">
                                                            <option value="">Select Engine Type</option>
                                                            @foreach( \CommonHelper::engine_type() as $c_index => $engine_type)
                                                                <option value="{{ $c_index }}" {{ isset($value['engine_type']) ? $value['engine_type'] == $c_index ? 'selected' : '' : ''}}> {{ $engine_type }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                          
                                                <div class="col-sm-4">
                                                    <div class="">
                                                        <label class="input-label" for="no-of-ships">BHP<span class="symbol required"></span></label>
                                                        <input type="text" class="form-control bhp" name="bhp[{{$index}}]" value="{{ isset($value['bhp']) ? $value['bhp'] : ''}}" placeholder="Type your bhp">
                                                    </div>
                                                </div>
                                                
                                            </div>

                                            <div class="form-group">
                                            	<div class="col-sm-4">
                                                    <div class="">
                                                        <label class="input-label" for="no-of-ships">Voyage<span class="symbol required"></span></label>
                                                        <div>
	                                                        <label class="radio-inline">
	                                                            <input type="radio" class="voyage" name="voyage[{{$index}}]" value="1" {{ $value['voyage'] == 1 ? 'checked' : '' }}> Costal
	                                                        </label>
	                                                        <label class="radio-inline">
	                                                            <input type="radio" class="voyage" name="voyage[{{$index}}]" value="0" {{ $value['voyage'] == 0 ? 'checked' : '' }}> Foreign Going
	                                                        </label>
	                                                    </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="">
                                                        <label class="input-label" for="built-year">Year Of Built</span></label>
                                                        <select name="built-year[{{$index}}]" class="form-control built-year">
                                                            <option value="">Select built year</option>
                                                            @foreach( \CommonHelper::year_of_built() as $c_index => $built_year)
                                                                <option value="{{ $c_index }}" {{ isset($value['built_year']) ? $value['built_year'] == $c_index ? 'selected' : '' : ''}}> {{ $built_year }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="">
                                                        <label class="input-label" for="p-i-cover">P & I cover</label>
                                                        <div>
	                                                        <label class="radio-inline">
	                                                            <input type="radio" block-index='{{$index}}' class="p-i-cover" name="p-i-cover[{{$index}}]" value="1" {{ $value['p_i_cover'] == 1 ? 'checked' : ''}}> Yes
	                                                        </label>
	                                                        <label class="radio-inline">
	                                                            <input type="radio" block-index='{{$index}}' class="p-i-cover" name="p-i-cover[{{$index}}]" value="0" {{ $value['p_i_cover'] == 0 ? 'checked' : ''}}> No
	                                                        </label>
	                                                    </div>
                                                    </div>
                                                </div>
                                                @if(isset($value['p_i_cover']) && $value['p_i_cover'] == 1)
                                                <div class="col-sm-4">
                                                    <div class="pi_cover_index_{{$index}}" id="pi_cover_index">
                                                        <label class="input-label" for="p_i_cover_company_name">P & I Cover Company name<span class="symbol required"></span></label>
                                                        <input type="text" class="form-control p_i_cover_company_name" name="p_i_cover_company_name[{{$index}}]" value="{{$value['p_i_cover_company_name']}}" placeholder="Type your PI cover company name">
                                                    </div>
                                                </div>
                                                @else
                                                    <div class="col-sm-4">
                                                    <div class="hide pi_cover_index_{{$index}}" id="pi_cover_index">
                                                        <label class="input-label" for="p_i_cover_company_name">P & I Cover Company name<span class="symbol required"></span></label>
                                                        <input type="text" class="form-control p_i_cover_company_name" name="p_i_cover_company_name[{{$index}}]" placeholder="Type your PI cover company name">
                                                    </div>
                                                </div>
                                                @endif
                                            </div>
                                        </div>
                                        <hr>
                                    @endforeach
                                @else
                                <div id="ships-template" class="ships-template multiple-info">
                                    <div class="form-group">
                                        <div class="col-xs-12">
                                            <div class="input-label p-t-10 ship-name pull-left">Ship Details 1</div>
                                            <div class="pull-right ship-close-button hide ship-close-button-0">
                                                <i class="fa fa-times" aria-hidden="true"></i>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-4">
                                            <div class="">
                                                <label class="input-label" for="ship_name">Ship Name<span class="symbol required"></span></label>
                                                <input type="text" name="ship_name[0]" class="form-control ship_name" placeholder="Type your ship name">
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="">
                                                <label class="input-label" for="no-of-ships">Type Of Ships<span class="symbol required"></span></label>
                                                <select name="ship_type[0]" class="form-control ship_type">
                                                    <option value="">Select Ship Type</option>
                                                    @foreach( \CommonHelper::ship_type() as $c_index => $ship_type)
                                                        <option value="{{ $c_index }}" {{ isset($user_data[0]['company_registration_detail']['company_detail']['ship_type']) ? $user_data[0]['company_registration_detail']['company_detail']['ship_type'] == $c_index ? 'selected' : '' : ''}}> {{ $ship_type }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="">
                                                <label class="input-label" for="no-of-ships">Flag Of Ship</label>
                                                <select name="ship_flag[0]" class="form-control ship_flag" block-index="0">
                                                        <option value="">Select ship flag</option>
                                                        @foreach( \CommonHelper::countries() as $c_index => $country)
                                                            <option value="{{ $c_index }}" {{ isset($user_data[0]['company_registration_detail']['company_detail']['country']) ? $user_data[0]['company_registration_detail']['company_detail']['country'] == $c_index ? 'selected' : '' : ''}}> {{ $country }}</option>
                                                        @endforeach
                                                </select>
                                            </div>
                                        </div>

                                        
                                    </div>
                                    <div class="form-group">
                                    	<div class="col-sm-4">
                                            <div class="">
                                                <label class="input-label" for="grt">GRT<span class="symbol required"></span></label>
                                                <input type="text" class="form-control grt" name="grt[0]" placeholder="Type your GRT">
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="">
                                                <label class="input-label" for="engine_type">Engine Type</label>
                                                <select name="engine_type[0]" class="form-control engine_type" block-index="0">
                                                    <option value="">Select engine type</option>
                                                    @foreach( \CommonHelper::engine_type() as $c_index => $engine_type)
                                                        <option value="{{ $c_index }}"> {{ $engine_type }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                  
                                        <div class="col-sm-4">
                                            <div class="">
                                                <label class="input-label" for="no-of-ships">BHP<span class="symbol required"></span></label>
                                                <input type="text" class="form-control bhp" name="bhp[0]" value="{{ isset($user_data[0]['company_registration_detail']['company_detail']['ship_type']) ? $user_data[0]['company_registration_detail']['company_detail']['ship_type'] : ''}}" placeholder="Type your bhp">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                    	
                                        <div class="col-sm-4">
                                            <div class="">
                                                <label class="input-label" for="no-of-ships">Voyage<span class="symbol required"></span></label>
                                                <div>
                                                    <label class="radio-inline">
                                                        <input type="radio" class="voyage" name="voyage[0]" value="1" {{ !empty($user_data[0]['passport_detail']) ? $user_data[0]['passport_detail']['is_headoffice'] == 1 ? 'checked' : '' : ''}}> Costal
                                                    </label>
                                                    <label class="radio-inline">
                                                        <input type="radio" class="voyage" name="voyage[0]" value="0" {{ !empty($user_data[0]['is_headoffice']) ? $user_data[0]['passport_detail']['is_headoffice'] == 0 ? 'checked' : '' : 'checked'}}> Foreign Going
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="">
                                                <label class="input-label" for="built-year">Year Of Built<span class="symbol required"></span></label>
                                                <select name="built-year[0]" class="form-control built-year">
                                                    <option value="">Select built year</option>
                                                    @foreach( \CommonHelper::year_of_built() as $c_index => $built_year)
                                                        <option value="{{ $c_index }}" {{ isset($value['built_year']) ? $value['built_year'] == $c_index ? 'selected' : '' : ''}}> {{ $built_year }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="">
                                                <label class="input-label" for="p-i-cover">P & I cover<span class="symbol required"></span></label>
                                                <div>
                                                    <label class="radio-inline">
                                                        <input type="radio" block-index='0' class="p-i-cover" name="p-i-cover[0]" value="1" {{ !empty($user_data[0]['passport_detail']) ? $user_data[0]['passport_detail']['is_headoffice'] == 1 ? 'checked' : '' : ''}}> Yes
                                                    </label>
                                                    <label class="radio-inline">
                                                        <input type="radio" block-index='0' class="p-i-cover" name="p-i-cover[0]" value="0" {{ !empty($user_data[0]['is_headoffice']) ? $user_data[0]['passport_detail']['is_headoffice'] == 0 ? 'checked' : '' : 'checked'}}> No
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="hide pi_cover_index_0" id="pi_cover_index">
                                                <label class="input-label" for="p_i_cover_company_name">P & I Cover Company name<span class="symbol required"></span></label>
                                                <input type="text" class="form-control p_i_cover_company_name" name="p_i_cover_company_name[0]">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                            	@endif

                                <div class="form-group no-margin" id="add-more-ships-row">
	                                    <div class="col-sm-12">
	                                        <button type="button" data-form-id="company-registration-location-details" class="btn add-ship-button ship-button-0 pull-right add-more-button" id="add-ship">Add Ship</button>
	                                    </div>
	                                </div>
								</div> -->
								
								<div id="documents-form">
		                           <div class="user-details-container">
		                               <div class="panel-heading">
											<h4 class="panel-title">DOCUMENTS</h4>
										</div>
										<div class="row panel-body">
											<div class="col-md-4">
												<label class="input-label" for="incorporation_number">Incorporation Number<span class="symbol required"></span></label>
                                                <input type="text" class="form-control incorporation_number" name="incorporation_number" placeholder="Type your incorporation mumber" value="{{!empty($user_data[0]['company_registration_detail']['company_documents']['incorporation_number']) ? $user_data[0]['company_registration_detail']['company_documents']['incorporation_number'] : ''}}">
											</div>
											<div class="col-md-4">
												<label class="input-label" for="pancard_number">Pancard Number</label>
                                                <input type="text" class="form-control pancard_number" name="pancard_number" placeholder="Type your pancard mumber" value="{{!empty($user_data[0]['company_registration_detail']['company_documents']['pancard_number']) ? $user_data[0]['company_registration_detail']['company_documents']['pancard_number'] : ''}}">
											</div>
										</div>
		                                <div class="row panel-body">
		                                   <div class="col-sm-4">
		                                       <div class="">
		                                            <div class="input-label" for="incorporation_certificate">Incorporation Certificate<span class="symbol required"></span></div>
		                                            <div class="col-sm-12">
		                                                <div data-provides="fileupload" class="fileupload {{!empty($user_data[0]['company_registration_detail']['company_documents']['incorporation_certificate']) ? 'fileupload-exists' : 'fileupload-new' }}">
		                                                    <div class="row">
		                                                        <div class="col-md-12">
		                                                            <span class="fileupload-preview upload-file-name">
		                                                            	@if(isset($user_data[0]['company_registration_detail']['company_documents']['incorporation_certificate']))
                                                                    <a href="/{{env('COMPANY_DOC_PATH')}}/{{$user_data[0]['company_registration_detail']['id']}}/{{isset($user_data[0]['company_registration_detail']['company_documents']['incorporation_certificate']) ? $user_data[0]['company_registration_detail']['company_documents']['incorporation_certificate'] : '-'}}" target="_blank">View Document</a>
                                                                @endif
		                                                            </span>
		                                                        </div>
		                                                        <span class="btn btn-file upload-button-color" style="border-color: #cf1b00;">
		                                                        <i class="fa fa-upload red-text-upload"></i> 
		                                                        <span class="fileupload-new red-text-upload">Upload </span>
		                                                        <span class="fileupload-exists red-text-upload">Change</span>
		                                                            <input type="file" class="{{isset($user_data['current_route']) ? !empty($user_data[0]['company_registration_detail']['company_documents']['incorporation_certificate']) ? '' : 'incorporation_required' : 'incorporation_required'}}"
		                                                            name="{{!empty($user_data[0]['company_registration_detail']['company_documents']['incorporation_certificate']) ? 'incorporation_certificate':'incorporation_certificate_1'}}">
		                                                        </span>
		                                                    </div>
		                                                </div>
		                                            </div>
		                                            <label for="incorporation_certificate" class="error"></label>
		                                            <label for="incorporation_certificate_1" class="error"></label>
		                                       </div>
		                                   </div>

		                                   <div class="col-sm-4">
		                                       <div class="">
		                                            <div class="input-label" for="pancard">Company/Owner Pancard copy</div>
		                                            <div class="col-sm-12">
		                                                <div data-provides="fileupload" class="fileupload {{!empty($user_data[0]['company_registration_detail']['company_documents']['pancard']) ? 'fileupload-exists' : 'fileupload-new' }}">
		                                                    <div class="row">
		                                                        <div class="col-md-12">
		                                                            <span class="fileupload-preview upload-file-name">
		                                                            @if(isset($user_data[0]['company_registration_detail']['company_documents']['pancard']))
		                                                                <a href="/{{env('COMPANY_DOC_PATH')}}/{{$user_data[0]['company_registration_detail']['id']}}/{{isset($user_data[0]['company_registration_detail']['company_documents']['pancard']) ? $user_data[0]['company_registration_detail']['company_documents']['pancard'] : ''}}" target="_blank">View Document</a>
		                                                            @endif
		                                                            </span>
		                                                        </div>
		                                                        <span class="btn btn-file" style="border-color: #cf1b00;">
		                                                        <i class="fa fa-upload red-text-upload"></i>
		                                                        <span class="fileupload-new red-text-upload">Upload</span>
		                                                        <span class="fileupload-exists red-text-upload">Change</span>
		                                                            <input type="file" class="{{isset($user_data['current_route']) ? !empty($user_data[0]['company_registration_detail']['company_documents']['pancard']) ? '' : 'pancard_required' : 'pancard_required'}}" 
		                                                            name="{{!empty($user_data[0]['company_registration_detail']['company_documents']['pancard']) ? 'pancard' : 'pancard_1'}}">
		                                                        </span>
		                                                    </div>
		                                                </div>
		                                            </div>
		                                            <label for="pancard" class="error"></label>
		                                            <label for="pancard_1" class="error"></label>
		                                       </div>
		                                   </div>

		                                   <?php
		                                   		if(isset($user_data[0]['company_registration_detail']['company_detail']['company_type']) && $user_data[0]['company_registration_detail']['company_detail']['company_type'] == 1){
		                                   			$hide = '';
		                                   		}else{
		                                   			$hide = 'hide';
		                                   		}
		                                   ?>
		                                   <div class="col-sm-4 {{$hide}}" id="rpsl-file-container">
		                                       <div class="">
		                                            <div class="input-label" for="rpsl_certificate">RPSL Certificate</div>
		                                           	<div class="col-sm-12">
		                                                <div data-provides="fileupload" class="fileupload {{!empty($user_data[0]['company_registration_detail']['company_documents']['rpsl_certificate']) ? 'fileupload-exists' : 'fileupload-new' }}">
		                                                    <div class="row">
		                                                        <div class="col-md-12">
		                                                            <span class="fileupload-preview upload-file-name" >
		                                                            @if(isset($user_data[0]['company_registration_detail']['company_documents']['rpsl_certificate']))
		                                                                <a href="/{{env('COMPANY_DOC_PATH')}}/{{$user_data[0]['company_registration_detail']['id']}}/{{isset($user_data[0]['company_registration_detail']['company_documents']['rpsl_certificate']) ? $user_data[0]['company_registration_detail']['company_documents']['rpsl_certificate'] : '-'}}" target="_blank">View Document</a>
		                                                            @endif
		                                                            </span>
		                                                        </div>
		                                                        <span class="btn btn-file" style="border-color: #cf1b00;">
		                                                        <i class="fa fa-upload red-text-upload"></i>
		                                                        <span class="fileupload-new red-text-upload">Upload</span>
		                                                        <span class="fileupload-exists red-text-upload">Change</span>
		                                                            <input type="file" 
		                                                            name="{{!empty($user_data[0]['company_registration_detail']['company_documents']['rpsl_certificate']) ? 'rpsl_certificate' : 'rpsl_certificate_1'}}" class="{{isset($user_data['current_route']) ? !empty($user_data[0]['company_registration_detail']['company_documents']['rpsl_certificate']) ? '' : 'rpsl_required' : 'rpsl_required'}}">
		                                                        </span>
		                                                    </div>
		                                                </div>
		                                            </div>
		                                            <label for="rpsl_certificate" class="error"></label>
		                                            <label for="rpsl_certificate_1" class="error"></label>
		                                       </div>
		                                   </div>
		                               </div>
		                           </div>
		                       </div>

								<div class="form-group">
									<div class="col-sm-2 col-sm-offset-8 pull-right">
										<button type="button" data-style="zoom-in" class="btn btn-success btn-block pull-right ladda-button next-step" data-form="step-2" data-type='location_details'>
											Finish <i class="fa fa-arrow-circle-right"></i>
										</button>
									</div>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

@stop

@section('js_script')
	<script type="text/javascript" src="/js/site/company-registration.js"></script>
	<script>
		jQuery(document).ready(function() {
			FormWizard.init();
		});
		// $('#wizard').smartWizard({enableAllSteps: true});
	</script>
@stop