@extends('admin.index')

@section('content')

<div class="main-wrapper">
	<div class="main-container1">
				<!-- start: PAGE -->
				<div class="main-content">
					<div class="container">
						<!-- start: PAGE HEADER -->
						<!-- start: TOOLBAR -->
						<div class="toolbar row">
							<div class="col-sm-6 hidden-xs">
								<div class="page-header">
									<h1>Job Applicants<small>A list of all job applicants</small></h1>
								</div>
							</div>
						</div>

						<form class="filter-search-form m_l_10">
							<div class="row m_t_25">
								<div class="col-md-12">
									<div class="panel panel-white">
										<div class="panel-body">
											<div class="row">
												<div class="col-sm-3">
					                                <div class="form-group">
					                                    <label>Job ID </label>
					                                    <input type="text" class="form-control" name="job_id" id="job_id" value="{{isset($filter['job_id']) ? $filter['job_id'] : ''}}" placeholder="Enter job id">
					                                </div>
					                            </div>
					                            <div class="col-sm-3">
					                                <div class="form-group">
					                                    <label>Seafarer Name </label>
					                                    <input type="text" class="form-control" name="seafarer_name" id="seafarer_name" value="{{isset($filter['seafarer_name']) ? $filter['seafarer_name'] : ''}}" placeholder="Enter seafarer name">
					                                </div>
					                            </div>
					                            <div class="col-sm-3">
					                                <div class="form-group">
					                                    <label>Email </label>
					                                    <input type="text" class="form-control" name="email" id="email" value="{{isset($filter['email']) ? $filter['email'] : ''}}" placeholder="Enter email address">
					                                </div>
					                            </div>
					                            <div class="col-sm-3">
					                                <div class="form-group">
					                                    <label>Mobile</label>
					                                    <input type="text" class="form-control" name="mobile" id="mobile" value="{{isset($filter['mobile']) ? $filter['mobile'] : ''}}" placeholder="Enter mobile number">
					                                </div>
					                            </div>
					                        </div>    
					                        <div class="row">
					                            <div class="col-sm-12">
					                                <div class="section_cta text-right m_t_25 job-btn-container">
					                                    <button type="button" class="btn btn-danger coss-inverse-btn search-reset-btn" id="seafarerResetButton">Reset</button>
					                                    <button type="submit" data-style="zoom-in" class="btn btn-info coss-primary-btn search-post-btn ladda-button candidate-filter-search-button" id="candidate-filter-search-button"><i class="fa fa-search search-icon" aria-hidden="true"></i>Search</button>
					                                </div>
					                            </div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</form>

						 <div class="row search-results-main-container m-t-25">
							<div class="col-md-12">
								<div class="panel panel-white">
									<div class="panel-body">
										@if(isset($data) && !empty($data))
											
											<div class="panel-heading">
												<div class="row">
													<div class="col-xs-12">
														<div class="col-xs-6 paginator-content  pull-right no-margin">
															<ul id="top-user-paginator" class="pagination">
																{!! $pagination->render() !!}
															</ul>
														</div>
														
														<div class="pull-left search-count-content">
															Showing {{$pagination_data['from']}} - {{$pagination_data['to']}} of {{$pagination_data['total']}} Results
														</div>
													</div>
												</div>
											</div>
											
											<div class="full-cnt-loader hidden">
												<div class="" style="text-align: center;padding-top:20px;padding-bottom:20px; ">
												    <i class="fa fa-spinner fa-spin fa-4" aria-hidden="true" style="font-size: 7em;"></i>
											    </div>
											</div>

											@foreach($data as $data)
											<div class="seafarer-listing-container">
												<div class="panel">
													<div class="panel-heading border-light partition-blue">
														<h5 class="panel-title text-bold header-link-text p-5-5">
															<a href="{{Route('admin.view.seafarer.id',$data['user_id'])}}">
																{{ isset($data['user']['first_name']) ? $data['user']['first_name'] : ''}}
															</a>
														</h5>
														<ul class="panel-heading-tabs border-light">
															<li class="panel-tools">
																<div class="dropdown">
																	<a data-toggle="dropdown" class="btn btn-xs dropdown-toggle btn-transparent-grey">
																		<i class="fa fa-cog"></i>
																	</a>
																	<ul class="dropdown-menu dropdown-light pull-right" role="menu">
																		<!-- <li>
																			<a href="{{Route('admin.view.advertiser.id',$data['id'])}}" data-type="user">
																				View
																			</a>
																		</li> -->
																		<li>
																			<a href="{{Route('admin.view.seafarer.id',[$data['user']['id']])}}">
																				View Profile
																			</a>
																		</li>
																		
																		
																	</ul>
																</div>
															</li>
														</ul>

													</div>
																	
													<div class="panel-body">
														<div class="row">
															
															<div class="col-lg-3 col-md-5 col-sm-5" id="user-profile-pic" style="height: 200px;width: 200px; margin-left: 15px;">
																<a>
																	<div class="company_profile_pic">
									                					<img src="{{!empty($data['user']['profile_pic']) ? "/".env('SEAFARER_PROFILE_PATH').$data['user_id']."/".$data['user']['profile_pic'] : 'images/user_default_image.png'}}" avatar-holder-src="{{ asset('images/user_default_image.png') }}" class="job-applicant-user-profile-pic">
									                				</div>
						                            			</a>
															</div>
															<div class="col-lg-9 col-md-7 col-sm-7">
																<div class="row">																
																	<div class="col-sm-4">		
																		<div id="user-professional-details">
																			<div class="row">
																				<div class="col-xs-12">
																					<div class="header-tag">
																						Seafarer Details
																					</div>
																				</div>
																			</div>
																			<dl>
																	     		<dt> Seafarer Name : </dt>
																					<dd class="margin-bottom-5px">
																						{{ isset($data['user']['first_name']) ? $data['user']['first_name'] : ''}}
																		     		</dd>
																	     		<dt>
																				<dt> Seafarer Current Rank :</dt>
																				<dd class="margin-bottom-5px">
																					@foreach(\CommonHelper::new_rank() as $rank_index => $category)
														                                @foreach($category as $r_index => $rank)
														                                    {{ isset($data['user']['professional_detail']['current_rank']) ? $data['user']['professional_detail']['current_rank'] == $r_index ? $rank : '' : ''}}
														                                @endforeach
														                            @endforeach
																				</dd>
																				<?php
													                                if(isset($data['user']['professional_detail']['current_rank_exp'])){
													                                    $rank_exp = explode(".",$data['user']['professional_detail']['current_rank_exp']);
													                                    $rank_exp[1] = number_format($rank_exp[1]);
													                                }
													                            ?>
																				<dt> Rank Experience :</dt>
																				<dd class="margin-bottom-5px">
																					{{ isset($data['user']['professional_detail']['current_rank_exp']) ? $rank_exp[0] ." Years ". $rank_exp[1] . " Months" : ''}}
																				</dd>

																				<dt> Last Wages Drawn :</dt>
																				<dd class="margin-bottom-5px">
																					{{isset($data['user']['professional_detail']['currency']) ? $data['user']['professional_detail']['currency'] == 'dollar' ? '$' : 'Rs' : 'Rs'}}

																					{{ isset($data['user']['professional_detail']['last_salary']) ? $data['user']['professional_detail']['last_salary'] : '-'}} <b>Per Month</b>
																				</dd>
																			</dl>
																		</div>
																	</div>
																	<div class="col-sm-4">		
																		<div id="user-professional-details">
																			<div class="row">
																				<div class="col-xs-12">
																					<div class="header-tag">
																						Job Details
																					</div>
																				</div>
																			</div>
																			<dl>
																	     		<dt> Job Id : </dt>
																					<dd class="margin-bottom-5px">
																						{{ isset($data['job_id']) ? $data['job_id'] : ''}}
																		     		</dd>
																	     		<dt>
																				
																				<dt> Job Category :</dt>
																				<dd class="margin-bottom-5px">
																					@foreach(\CommonHelper::seafarer_job_category() as $index => $category)
												                                        {{ isset($data['company_job']['job_category']) ? $data['company_job']['job_category'] == $index ? $category : '' : ''}}
												                                    @endforeach
																				</dd>
																				<dt> Rank :</dt>
																				<dd class="margin-bottom-5px">
																					@foreach(\CommonHelper::new_rank() as $rank_index => $category)
														                                @foreach($category as $r_index => $rank)
														                                    {{ isset($data['company_job']['rank']) ? $data['company_job']['rank'] == $r_index ? $rank : '' : ''}}
														                                @endforeach
														                            @endforeach
																				</dd>
																			</dl>
																		</div>
																	</div>
																	<div class="col-sm-4">		
																		<div id="user-professional-details">
																			<div class="row">
																				<div class="col-xs-12">
																					<div class="header-tag m_t_20">
																						 
																					</div>
																				</div>
																			</div>
																			<dl>
																	     		<dt> Minimum Rank Experience : </dt>
																					<dd class="margin-bottom-5px">
																						{{ isset($data['company_job']['min_rank_exp']) ? $data['company_job']['min_rank_exp'] : ''}} yrs
																		     		</dd>
																	     		<dt>
																				<dt> Date Of Joining :</dt>
																				<dd class="margin-bottom-5px">
																					{{ isset($data['company_job']['date_of_joining']) && !empty($data['company_job']['date_of_joining']) ? date('d-m-Y',strtotime($data['company_job']['date_of_joining'])) : '-'}}
																				</dd>
																			</dl>
																		</div>
																	</div>
																</div>
															</div>
																			
														</div>
													</div>
												</div>
																
											</div>
											@endforeach
										@else
											<div class="no-results-found">No results found. Try again with different search criteria.</div>
										@endif

										@if(isset($data) && !empty($data))
											<div class="panel-heading">
												<div class="row">
													<div class="col-xs-12">
														<div class="col-xs-6 paginator-content  pull-right no-margin">
															<ul id="top-user-paginator" class="pagination">
																{!! $pagination->render() !!}
															</ul>
														</div>
														
														<div class="pull-left search-count-content">
															Showing {{$pagination_data['from']}} - {{$pagination_data['to']}} of {{$pagination_data['total']}} Results
														</div>
													</div>
												</div>
											</div>
										@endif
										
									</div>
								</div>
							</div>
						</div>
						<!-- end: PAGE CONTENT-->
					</div>
					<div class="subviews">
						<div class="subviews-container"></div>
					</div>
				</div>
				<!-- end: PAGE -->
			</div>
</div>

<script type="text/javascript">
        
    var searchAjaxCall = "undefined";

    $(document).ready(function(){
        History = window.History; // Note: We are using a capital H instead of a lower h
		
        History.Adapter.bind(window,'statechange', function() {

            var State = History.getState(); // Note: We are using History.getState() instead of event.state

            fetchSearchResults(State.url, 
            function() {
                
            }, function() {

            });

        }); 

    });

    function fetchSearchResults(url, successCallback, errorCallback)
    {
    	var temp_current_state_url = url.split('?');
        if(temp_current_state_url.length > 1 && temp_current_state_url[1] != ''){
            var parameters = temp_current_state_url[1].split('&');
            var dropdown_elements = [];

            for(var i=0; i<parameters.length; i++){

                var param_array = parameters[i].split('=');
                if($.inArray(param_array[0],dropdown_elements) > -1){
                    $('select[name="'+param_array[0]+'"]').val(param_array[1]);
                }else if($.inArray(param_array[0],['seafarer_name','job_id','email','mobile']) > -1){
                    $('input[name="'+param_array[0]+'"]').val(param_array[1]);
                }
            }
        } else {
         	$(':input','.filter-search-form')
                .not(':button, :submit, :reset, :hidden')
                .val('')
                .removeAttr('checked')
                .removeAttr('selected');
             }

        if(searchAjaxCall != "undefined")
        {
            searchAjaxCall.abort();
            searchAjaxCall = "undefined";
        }

        $('.list-container').addClass('hide');
        $('.seafarer-listing-container').addClass('hide');
        $('.full-cnt-loader').removeClass('hidden');

        searchAjaxCall = $.ajax({
            url: url,
            cache: false,
            dataType: 'json',
            statusCode: {
                200: function(response) {
					if (typeof response.template != 'undefined') {
                       	$('.list-container').removeClass('hide');
                       	$('.seafarer-listing-container').removeClass('hide');
                    	$('.full-cnt-loader').addClass('hidden');

                        $('.search-results-main-container').html(response.template);

                        if(typeof successCallback != "undefined")
                        {
                            successCallback();
                        }

                    }
                }
            },
            error: function(error, response) {

                if(typeof errorCallback != "undefined")
                {
                    errorCallback();
                }
            }
        });
    }
</script>

@stop
@section('js_script')
    <script type="text/javascript" src="/js/site/job.js"></script>
@stop