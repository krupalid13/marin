<html>
    <tr>
        <td align="center"><b>Sr. No</b></td>
        <td align="center"><b>Company Name</b></td>
        <td align="center"><b>Company Email ID</b></td>
        <td align="center"><b>Company Phone Number</b></td>
        <td align="center"><b>Contact Person Email</b></td>
        <td align="center"><b>Contact Person Name</b></td>
        <td align="center"><b>Contact Person Mobile</b></td>
        <td align="center"><b>Ship Count</b></td>
        <td align="center"><b>Ship Type</b></td>
        <td align="center"><b>Ship Flag</b></td>
        <td align="center"><b>GRT</b></td>
        <td align="center"><b>BHP</b></td>
    </tr>
    @foreach($data as $index => $company)
        <tr>
        	<td>{{$index+1}}</td>
        	<td>
        		{{ isset($company['company_registration_detail']['company_name']) ? $company['company_registration_detail']['company_name'] : '-'}}
        	</td>
        	<td>
        		{{ isset($company['company_registration_detail']['company_detail']['company_email']) ? $company['company_registration_detail']['company_detail']['company_email'] : '-'}}
        	</td>
        	<td>
        		{{ (isset($company['company_registration_detail']['company_detail']['company_contact_number']) AND !empty($company['company_registration_detail']['company_detail']['company_contact_number'])) ? $company['company_registration_detail']['company_detail']['company_contact_number'] : '-'}}
        	</td>
        	<td>
        		{{ isset($company['company_registration_detail']['email']) ? $company['company_registration_detail']['email'] : ''}}
        	</td>
        	<td>
        		{{ isset($company['company_registration_detail']['contact_person']) ? $company['company_registration_detail']['contact_person'] : ''}}
        	</td>
        	<td>
        		{{ isset($company['company_registration_detail']['contact_number']) ? $company['company_registration_detail']['contact_number'] : ''}}
        	</td>
        	<td>
        		{{ isset($company['company_registration_detail']['company_ship_details']) ? count($company['company_registration_detail']['company_ship_details']) : '0'}}
        	</td>
        	<td>
        		@if(isset($company['company_registration_detail']['company_ship_details']) AND !empty($company['company_registration_detail']['company_ship_details']))
        			<?php
                        $count = count($company['company_registration_detail']['company_ship_details'])-1;
                        $ind = 0;
                    ?>
                    @if(count($company['company_registration_detail']['company_ship_details']) > 0)
		        		@foreach($company['company_registration_detail']['company_ship_details'] as $index => $value)
		        			@foreach( \CommonHelper::ship_type() as $c_index => $ship_type)
	                            {{ isset($value['ship_type']) ? $value['ship_type'] == $c_index ? $ship_type : '' : ''}}
		                        @if($value['ship_type'] == $c_index AND ( $count != $ind))
			                        ,
			                        <?php $ind++ ?>
			                    @endif
	                        @endforeach
		        		@endforeach
		        	@else
		        		-
		        	@endif
	        	@endif
        	</td>
        	<td>
        		@if(isset($company['company_registration_detail']['company_ship_details']) AND !empty($company['company_registration_detail']['company_ship_details']))
        			<?php
                        $count = count($company['company_registration_detail']['company_ship_details'])-1;
                        $ind = 0;
                    ?>
                    @if(count($company['company_registration_detail']['company_ship_details']) > 0)
	        			@foreach($company['company_registration_detail']['company_ship_details'] as $index => $value)
		        			@foreach( \CommonHelper::countries() as $c_index => $ship_flag)
	                            {{ isset($value['ship_flag']) ? $value['ship_flag'] == $c_index ? $ship_flag : '' : ''}}
		                        @if($value['ship_flag'] == $c_index AND ( $count != $ind))
			                        ,
			                        <?php $ind++ ?>
			                    @endif
	                        @endforeach
		        		@endforeach
		        	@else
		        		-
		        	@endif
	        	@endif
        	</td>
        	<?php $comma = false;?>
        	<td align="left">
        		@if(isset($company['company_registration_detail']['company_ship_details']) AND !empty($company['company_registration_detail']['company_ship_details']))
        			@if(count($company['company_registration_detail']['company_ship_details']) > 0)
		        		@foreach($company['company_registration_detail']['company_ship_details'] as $index => $value)
		        			@if($comma)
	        					,
	        				@endif
	        				<?php $comma = false;?>
	                        {{ isset($value['grt']) ? $value['grt'] : ''}}
	                        <?php $comma = true;?>
		        		@endforeach
		        	@else
		        		-
		        	@endif
	        	@endif
        	</td>
        	<?php $comma = false;?>
        	<td align="left">
        		@if(isset($company['company_registration_detail']['company_ship_details']) AND !empty($company['company_registration_detail']['company_ship_details']))
        			@if(count($company['company_registration_detail']['company_ship_details']) > 0)
        				
		        		@foreach($company['company_registration_detail']['company_ship_details'] as $index => $value)
		        			@if($comma)
	        					,
	        				@endif
		        			<?php $comma = false;?>
	                        {{ isset($value['bhp']) ? $value['bhp'] : ''}}
	                        <?php $comma = true;?>
		        		@endforeach
	        		@else
		        		-
		        	@endif
	        	@endif
        	</td>
        </tr>
   	@endforeach
</html>