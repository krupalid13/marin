@extends('admin.index')

@section('content')
 <?php
    $india_value = array_search('India',\CommonHelper::countries());
?>

<div id="view-company-page">
	<div class="container main-content">
		<div class="toolbar row">
			<div class="col-sm-6 hidden-xs">
				<div class="page-header">
					<h1>Company <small>View Company Details</small></h1>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12 m_t_25">
				<div class="tabbable">
					<ul id="myTab2" class="nav nav-tabs">
						<li class="active">
							<a href="#company_details" data-toggle="tab">
								Company Details
							</a>
						</li>
						<li>
							<a href="#company_locations" data-toggle="tab">
								Company Locations
							</a>
						</li>
						<li>
							<a href="#team" data-toggle="tab">
								Team
							</a>
						</li>
                        <li>
							<a href="#scope" data-toggle="tab">
								Scope
							</a>
						</li>
						<li>
							<a href="#add_job" data-toggle="tab">
								Add Job
							</a>
						</li>
						<li>
							<a href="#company_subscription" data-toggle="tab" data-url="{{route('admin.get.user.subscription',['page' => 1,'role' => 'company','company_id' => isset($data[0]['id']) ? $data[0]['id'] : ''])}}">
                                Subscription
                            </a>
                        </li>
						<li class="pull-right">
							<a type="button" class="fa fa-edit" href="{{Route('admin.edit.company',$data[0]['id'])}}">
								Edit
							</a>
						</li>
					</ul>
					<div class="tab-content panel-body">
						<div class="tab-pane fade in active content" id="company_details">
							<?php
	                            $image_path = '';
	                            if(isset($data[0]['profile_pic'])){
	                                $image_path = env('COMPANY_LOGO_PATH')."".$data[0]['id']."/".$data[0]['profile_pic'];
	                            }
	                        ?>
							<div class="row text-center" style="display: flex;justify-content: center;">
								<div class="col-lg-3 col-md-5 col-sm-5 company_profile_pic">
	                				<img src="{{ asset(!empty($image_path) ? '/'.$image_path : 'images/user_default_image.png')}}" >
								</div>
							</div>
	                        <div class="panel-heading">
								<h4 class="panel-title">ACCOUNT CREDENTIALS</h4>
							</div>
							
							
							<div class="row">
								<label class="col-sm-3 control-label">
									Contact Person Email: 
									@if($data[0]['is_email_verified'] == '1')
										<i class="fa fa-check-circle green f-15" aria-hidden="true" title="Email Verified"></i>
									@else
										<i class="fa fa-minus-circle red f-15" aria-hidden="true" title="Email Unverified"></i>
									@endif
								</label>
	                           	<div class="col-sm-9">
	                           		{{ isset($data[0]['email']) ? $data[0]['email'] : ''}}
	                            </div>
                        	</div>
		                    
		                    <div class="row">
								<label class="col-sm-3 control-label">
									Contact Person Name:</span>
								</label>
								<div class="col-sm-9">
									{{ isset($data[0]['company_registration_detail']['contact_person']) ? $data[0]['company_registration_detail']['contact_person'] : ''}}
								</div>
							</div>
							<div class="row">
								<label class="col-sm-3 control-label">
									Contact Person Mobile:
									@if($data[0]['is_mob_verified'] == '1')
					     				<i class="fa fa-check-circle green f-15" aria-hidden="true" title="Mobile Verified"></i>
					     			@else
										<i class="fa fa-minus-circle red f-15" aria-hidden="true" title="Mobile Unverified"></i>
					     			@endif
								</span>
								</label>
								<div class="col-sm-9">
									{{ isset($data[0]['company_registration_detail']['contact_number']) ? $data[0]['company_registration_detail']['contact_number'] : ''}}
								</div>
							</div>

							<div class="panel-heading second-heading">
								<h4 class="panel-title">COMPANY DETAILS</h4>
							</div>
							
							<div class="row">
								<label class="col-sm-3 control-label">
									Company Name:</span>
								</label>
								<div class="col-sm-9">
									{{ isset($data[0]['company_registration_detail']['company_name']) ? $data[0]['company_registration_detail']['company_name'] : ''}}
								</div>
							</div>
							<div class="row">
								<label class="col-sm-3 control-label">
									Company Email ID:</span>
								</label>
								<div class="col-sm-9">
									{{ isset($data[0]['company_registration_detail']['company_detail']['company_email']) ? $data[0]['company_registration_detail']['company_detail']['company_email'] : ''}}
								</div>
							</div>
							<div class="row">
								<label class="col-sm-3 control-label">
									Company Phone Number:
								</label>
								<div class="col-sm-9">
									{{ !empty($data[0]['company_registration_detail']['company_detail']['company_contact_number']) ? $data[0]['company_registration_detail']['company_detail']['company_contact_number'] : '-'}}
								</div>
							</div>
							<div class="row">
								<label class="col-sm-3 control-label">
									Company Website:
								</label>
								<div class="col-sm-9">
									<div class="input-group">
										<a target="_blank" href="http://{{ isset($data[0]['company_registration_detail']['website']) ? $data[0]['company_registration_detail']['website'] : ''}}">{{ !empty($data[0]['company_registration_detail']['website']) ? $data[0]['company_registration_detail']['website'] : '-'}}</a>
                                        
                                    </div>
								</div>
							</div>
							<div class="row">
								<label class="col-sm-3 control-label">
									Company Fax:
								</label>
								<div class="col-sm-9">
									{{ !empty($data[0]['company_registration_detail']['company_detail']['fax']) ? $data[0]['company_registration_detail']['company_detail']['fax'] : '-'}}
								</div>
							</div>
							<div class="row">
								<label class="col-sm-3 control-label">
									Company Type:</span>
								</label>
								<div class="col-sm-9">
                                        @foreach( \CommonHelper::company_type() as $c_index => $company_type)
                                            {{ isset($data[0]['company_registration_detail']['company_detail']['company_type']) ? $data[0]['company_registration_detail']['company_detail']['company_type'] == $c_index ? $company_type : '' : ''}}
                                        @endforeach
                                    </select>
								</div>
							</div>

							@if(isset($data[0]['company_registration_detail']['company_detail']['company_type']) && $data[0]['company_registration_detail']['company_detail']['company_type'] == 1)
								<div class="row">
									<label class="col-sm-3 control-label">
										RPSL Number:</span>
									</label>
									<div class="col-sm-9">
										{{ isset($data[0]['company_registration_detail']['company_detail']['rpsl_no']) ? $data[0]['company_registration_detail']['company_detail']['rpsl_no'] : ''}}
									</div>
								</div>
                            @endif
							
							<div class="row">
								<label class="col-sm-3 control-label">
									Description:</span>
								</label>
								<div class="col-sm-9">
									{{ isset($data[0]['company_registration_detail']['company_detail']['company_description']) ? $data[0]['company_registration_detail']['company_detail']['company_description'] : ''}}
	                            </div>
							</div>
						</div>

						<div class="tab-pane fade" id="company_locations">
							<div class="sub-details-container">
                                <div class="content-container">
                                    <div class="row">
                                        <div class="col-sm-12">
                                        	<div class="panel-heading second-heading">
												<h4 class="panel-title">DOCUMENTS</h4>
											</div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-6 col-md-4">
                                            <div class="discription">
                                                <div class="content-head">Incorporation Certificate :</div>
                                                <div>
	                                                @if(isset($data[0]['company_registration_detail']['company_documents']['incorporation_certificate']))
	                                                    <a href="/{{env('COMPANY_DOC_PATH')}}/{{$data[0]['company_registration_detail']['id']}}/{{$data[0]['company_registration_detail']['company_documents']['incorporation_certificate']}}" target="_blank">View Document</a>
	                                                @else
	                                                	-
	                                                @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-6 col-md-4">
                                            <div class="discription">
                                                <div class="content-head">Company/Owner Pancard copy :</div>
                                                <div>
	                                                @if(isset($data[0]['company_registration_detail']['company_documents']['pancard']))
	                                                    <a href="/{{env('COMPANY_DOC_PATH')}}/{{$data[0]['company_registration_detail']['id']}}/{{$data[0]['company_registration_detail']['company_documents']['pancard']}}" target="_blank">View Document</a>
	                                                @else
	                                                	-
	                                                @endif
	                                             </div>
                                            </div>
                                        </div>
                                        @if($data[0]['company_registration_detail']['company_detail']['company_type'] == 1)
                                            <div class="col-xs-12 col-sm-6 col-md-4">
                                                <div class="discription">
                                                    <div class="content-head">RPSL Certificate :</div>
                                                   	<div>
	                                                    @if(isset($data[0]['company_registration_detail']['company_documents']['rpsl_certificate']))
	                                                        <a href="/{{env('COMPANY_DOC_PATH')}}/{{$data[0]['company_registration_detail']['id']}}/{{isset($data[0]['company_registration_detail']['company_documents']['rpsl_certificate']) ? $data[0]['company_registration_detail']['company_documents']['rpsl_certificate'] : '-'}}" target="_blank">View Document</a>
	                                                    @else
	                                                    	-
	                                                    @endif
	                                                </div>
                                                </div>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <!-- <div class="sub-details-container">
                                <div class="content-container">
                                    <div class="row">
                                        <div class="col-sm-12">
                                        	<div class="panel-heading shippng-details-heading">
                                        		<h4 class="panel-title">SHIP DETAILS</h4>
                                        	</div>
                                        </div>
                                    </div>
                                    @if(isset($data[0]['company_registration_detail']['company_ship_details']) && !empty($data[0]['company_registration_detail']['company_ship_details']))
                                        @foreach($data[0]['company_registration_detail']['company_ship_details'] as $index => $value)
                                            <div class="row" style="padding-top: 5px; padding-bottom: 5px">
                                                <div class="col-xs-12 col-sm-6 col-md-4">
                                                    <div class="discription" style="font-size: 17px">
                                                        <span class="content-head">Ship {{ $index+1 }}:</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-6 col-md-4">
                                                    <div class="discription">
                                                        <label class="control-label">Type Of Ships:</label>
                                                        <span class="content">
                                                            @foreach( \CommonHelper::ship_type() as $c_index => $ship_type)
                                                                {{ isset($value['ship_type']) ? $value['ship_type'] == $c_index ? $ship_type : '' : ''}}
                                                            @endforeach
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-6 col-md-4">
                                                    <div class="discription">
                                                        <label class="control-label">Flag Of Ship:</label>
                                                        <span class="content">
                                                            @if(isset($value['ship_flag']) && !empty($value['ship_flag']))
                                                                @foreach( \CommonHelper::countries() as $c_index => $ship_flag)
                                                                    {{ isset($value['ship_flag']) ? $value['ship_flag'] == $c_index ? $ship_flag : '' : ''}}
                                                                @endforeach
                                                            @else
                                                                -
                                                            @endif
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-6 col-md-4">
                                                    <div class="discription">
                                                        <label class="control-label">GRT:</label>
                                                        <span class="content">
                                                            {{ isset($value['grt']) ? $value['grt'] : ''}}
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-6 col-md-4">
                                                    <div class="discription">
                                                        <label class="control-label">Engine Type:</label>
                                                        <span class="content">
                                                            @if(isset($value['engine_type']) && !empty($value['engine_type']))
                                                                @foreach( \CommonHelper::engine_type() as $c_index => $engine_type)
                                                                    {{ isset($value['engine_type']) ? $value['engine_type'] == $c_index ? $engine_type : '' : ''}}
                                                                @endforeach
                                                            @else
                                                            	-
                                                            @endif
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-6 col-md-4">
                                                    <div class="discription">
                                                        <label class="control-label">BHP:</label>
                                                        <span class="content">
                                                            {{ isset($value['bhp']) ? $value['bhp'] : ''}}
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-6 col-md-4">
                                                    <div class="discription">
                                                        <label class="control-label">Voyage:</label>
                                                        <span class="content">
                                                             {{ isset($value['voyage']) ? $value['voyage'] == '0' ? 'Foreign Going' : 'Costal' : ''}}
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-6 col-md-4">
                                                    <div class="discription">
                                                        <label class="control-label">Year Of Built:</label>
                                                        <span class="content">
                                                           @foreach( \CommonHelper::year_of_built() as $c_index => $built_year)
                                                                {{ isset($value['built_year']) ? $value['built_year'] == $c_index ? $built_year : '' : ''}}
                                                            @endforeach
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-6 col-md-4">
                                                    <div class="discription">
                                                        <label class="control-label">P & I cover:</label>
                                                        <span class="content">
                                                            {{ isset($value['p_i_cover']) ? $value['p_i_cover'] == '1' ? 'Yes' : 'No' : ''}}
                                                        </span>
                                                    </div>
                                                </div>
                                                @if($value['p_i_cover'] == '1')
                                                <div class="col-xs-12 col-sm-6 col-md-4">
                                                    <div class="discription">
                                                        <label class="control-label">P & I Cover Company name:</label>
                                                        <span class="content">
                                                            {{ isset($value['p_i_cover_company_name']) ? $value['p_i_cover_company_name'] : ''}}
                                                        </span>
                                                    </div>
                                                </div>
                                                @endif
                                            </div>
                                            <hr>
                                        @endforeach
                                    @else
                                    	No Data Found.
                                    @endif
                                    
                                </div>
                            </div> -->
                            <div class="">
                                <div class="content-container">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="panel-heading second-heading">
	                                    		<h4 class="panel-title">LOCATION DETAILS</h4>
	                                    	</div>
                                        </div>
                                    </div>

                                    @if(isset($data[0]['company_registration_detail']['company_locations']) && !empty($data[0]['company_registration_detail']['company_locations']))
	                                    @foreach ($data[0]['company_registration_detail']['company_locations'] as $index => $company_location)
	                                        <div class="row" style="padding-top: 5px; padding-bottom: 5px">
	                                            <div class="col-xs-12 col-sm-6 col-md-4">
	                                                <div class="discription" style="font-size: 17px">
	                                                    <span class="content-head">Location {{ $index+1 }} :</span>
	                                                </div>
	                                            </div>
	                                        </div>
	                                        <div class="row">
	                                            <div class="col-xs-12 col-sm-6 col-md-4">
	                                                <div class="discription">
	                                                    <label class="control-label">Country:</label>
	                                                    <span class="content">
	                                                        @foreach( \CommonHelper::countries() as $c_index => $country)
	                                                            {{ $company_location['country'] == $c_index ? $country : '' }}
	                                                        @endforeach
	                                                    </span>
	                                                </div>
	                                            </div>
	                                            <div class="col-xs-12 col-sm-6 col-md-4">
	                                                <div class="discription">
	                                                    <label class="control-label">State:</label>
	                                                    <span class="content">
	                                                        {{ isset($company_location['state_id']) ? $company_location['pincode']['pincodes_states'][0]['state']['name'] : $company_location['state_text']}}
	                                                    </span>
	                                                </div>
	                                            </div>
	                                            <div class="col-xs-12 col-sm-6 col-md-4">
	                                                <div class="discription">
	                                                    <label class="control-label">City:</label>
	                                                    <span class="content">
	                                                        {{ isset($company_location['city_id']) ? $company_location['pincode']['pincodes_cities'][0]['city']['name'] : $company_location['city_text']}}
	                                                    </span>
	                                                </div>
	                                            </div>
	                                        </div>
	                                        <div class="row">
	                                            <div class="col-xs-12 col-sm-6 col-md-4">
	                                                <div class="discription">
	                                                    <label class="control-label">Pincode:</label>
	                                                    <span class="content">
	                                                        {{ isset($company_location['pincode_text']) ? $company_location['pincode_text'] : ''}}
	                                                    </span>
	                                                </div>
	                                            </div>
	                                            <div class="col-xs-12 col-sm-6 col-md-4">
                                                    <div class="discription">
                                                        <label class="content-label">Telephone:</label>
                                                        <span class="content">
                                                            {{ isset($company_location['telephone']) && !empty($company_location['telephone']) ? $company_location['telephone'] : '-'}}
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-6 col-md-4">
                                                    <div class="discription">
                                                        <label class="content-label">Is Headoffice:</label>
                                                        <span class="content">
                                                            {{ isset($company_location['is_headoffice']) && !empty($company_location['is_headoffice']) ? 'Yes' : 'No'}}
                                                        </span>
                                                    </div>
                                                </div>
	                                        </div>
	                                        <div class="row">
	                                            <div class="col-xs-12 col-sm-6 col-md-8">
	                                                <div class="discription">
	                                                    <label class="control-label">Address:</label>
	                                                    <span class="content">{{ $company_location['address'] }}</span>
	                                                </div>
	                                            </div>
	                                        </div>
	                                    <hr>
	                                    @endforeach
	                                @else
	                                	No Data Found.
	                               	@endif
                                </div>
                            </div>
						</div>
						
						<div class="tab-pane fade" id="add_job">
							@include('admin.partials.add_company_job')
						</div>

						<div id="company_subscription" class="tab-pane fade" data-id="{{ isset($data[0]['company_registration_detail']['id']) ? $data[0]['company_registration_detail']['id'] : ''}}" data-role='company' data-company="{{ isset($data[0]['id']) ? $data[0]['id'] : ''}}">
                            <div class="row panel-heading">
                                <div class="col-sm-6">
                                    <h4 class="panel-title">SUBSCRIPTION DETAILS</h4>
                                </div>
                            </div>
                            <div class="full-cnt-loader hidden">
                                <div class="" style="text-align: center;padding-top:20px;padding-bottom:20px; ">
                                    <i class="fa fa-spinner fa-spin fa-4" aria-hidden="true" style="font-size: 7em;"></i>
                                </div>
                            </div>
                            <div class="data">
                                No Subscriptions Found.
                            </div>
                            
                        </div>

                        <div class="tab-pane fade" id="team">
                        	<form class="team-section-form" id="team-section-form" method="post" action="{{ route('admin.company.team.details') }}">
			                    <div class="main-form-details sub-details-container">
			                        <div class="owner-section">
			                            <div class="team-details">
			                                <div class="row m-t-15 team-details-row">
			                                    <div class="col-xs-12 col-sm-3">
			                                        <div class="company_team_profile_pic" id="company_team_profile_pic">
			                                            <label class="input-label" for="pic">Profile Pic</label>
			                                            <div class="upload-photo-container">
			                                                {{ csrf_field() }}
			                                                <input type="hidden" name="image-x" required>
			                                                <input type="hidden" name="image-y" required>
			                                                <input type="hidden" name="image-x2" required>
			                                                <input type="hidden" name="image-y2" required>
			                                                <input type="hidden" name="crop-w" required>
			                                                <input type="hidden" name="crop-h" required>
			                                                <input type="hidden" name="image-w" required>
			                                                <input type="hidden" name="image-h" required>
			                                                <input type="hidden" name="uploaded-file-name" required>
			                                                <input type="hidden" name="uploaded-file-path" required>
			                                                <input type="hidden" class="path" value="{{env('COMPANY_TEAM_PROFILE_PIC_PATH')}}"></input>

			                                                <div class="image-content">
			                                                    <div class="profile-image registration-profile-image">
			                                                        <?php
			                                                            $image_path = '';
			                                                            if(isset($team['profile_pic'])){
			                                                                $image_path = env('COMPANY_TEAM_PROFILE_PIC_PATH')."".$team['id']."/".$team['profile_pic'];
			                                                            }
			                                                        ?>
			                                                        @if(empty($image_path))
			                                                            <div class="icon profile_pic_text"><i class="fa fa-camera" aria-hidden="true"></i></div>
			                                                            <div class="image-text profile_pic_text">Upload Profile <br> Picture</div>
			                                                        @endif

			                                                        <input type="file" name="profile_pic" id="profile-pic" class="cursor-pointer" onChange="profilePicSelectHandler('company_team_profile_pic', 'profile_pic', '' , 'company_team')">

			                                                        @if(!empty($image_path))
			                                                            <img id="preview" class="preview" src="/{{ $image_path }}">
			                                                        @else
			                                                            <img id="preview">
			                                                        @endif
			                                                    </div>
			                                                </div>
			                                            </div>
			                                        </div>
			                                    </div>
			                                    <div class="col-xs-12 col-sm-3">
			                                        <div class="form-group">
			                                            <label class="input-label" for="team_agent_name">Member Name<span class="symbol required"></span></label>
			                                            <input type="text" name="agent_name" class="form-control search-select agent_name" value="" placeholder="Type your name">
			                                        </div>
			                                    </div>
			                                    <div class="col-xs-12 col-sm-3">
			                                        <div class="form-group">
			                                            <label class="input-label" for="team_agent_designation">Designation<span class="symbol required"></span></label>
			                                            <input type="text" name="agent_designation" class="form-control search-select agent_designation" value="" placeholder="Type your designation">
			                                        </div>
			                                    </div>
			                                    <div class="col-xs-12 col-sm-3">
			                                        <div class="form-group">
			                                            <label class="input-label">Location<span class="symbol required"></span></label>
			                                            <select name="location_id" class="form-control search-select location_id">
			                                                <option value="">Select Location</option>
			                                                @if(isset($data[0]['company_registration_detail']['company_locations']) && !empty($data[0]['company_registration_detail']['company_locations']))
			                                                    @foreach($data[0]['company_registration_detail']['company_locations'] as $company_location_details)
			                                                        @if($company_location_details['country'] == $india_value)
			                                                            <option value="{{ $company_location_details['id'] }}" {{ isset($company_location_details['country']) ? $company_location_details['country'] : ''}}> {{ ucfirst(\CommonHelper::countries()[$company_location_details['country']]) }} {{ '/' }} {{ ucfirst($company_location_details['state']['name']) }} {{ '/' }} {{ ucfirst($company_location_details['city']['name']) }}</option>
			                                                        @else
			                                                            <option value="{{ $company_location_details['id'] }}" {{ isset($company_location_details['country']) ? $company_location_details['country'] : ''}}> {{ ucfirst(\CommonHelper::countries()[$company_location_details['country']]) }} {{ '/' }} {{ ucfirst($company_location_details['state_text']) }} {{ '/' }} {{ ucfirst($company_location_details['city_text']) }}</option>
			                                                        @endif
			                                                    @endforeach
			                                                @endif
			                                            </select>
			                                        </div>
			                                    </div>

			                                    <input type="hidden" name="company_id" value="{{$data[0]['company_registration_detail']['id']}}">
			                                    <input type="hidden" name="exiting_team_id" class="exiting_team_id">

			                                </div>
			                                <div class="row m-t-15">

			                                </div>
			                            </div>
			                        </div>
			                        <div class="row">
			                            <div class="col-xs-12 reg-form-btn-container text-right">
			                                <button type="button" data-style="zoom-in" class="reg-form-btn blue ladda-button company-team-details-submit p-0" id="company-team-details-submit" value="Save">Save</button>
			                            </div>
			                        </div>
			                    </div>

			                    <div class="company-team-details sub-details-container">
			                        <div class="row ship_details_title">
			                            <div class="col-xs-12">
			                                <div class="title">Team Details</div>
			                            </div>
			                        </div>
			                        <div class="company_team_data">
			                            @if(isset($data[0]['company_registration_detail']['company_team']) && !empty($data[0]['company_registration_detail']['company_team']))
			                                @foreach($data[0]['company_registration_detail']['company_team'] as $index => $team)
			                                    <div class="company_team_show_data">
			                                        <div class="company_team_show_data_{{ $team['id'] }}">
			                                            <div class="row team-row">
			                                                <div class="col-sm-6 team-title">
			                                                    <div class="description">
			                                                        <span class="content-head team-title team-name">Team Agent {{$index+1}}</span>
			                                                    </div>
			                                                </div>
			                                                <div class="col-xs-6">
			                                                    <div class="company-team-buttons">
			                                                        <div class="company-team-edit-button" data-index-id="{{$index}}" data-team-id="{{$team['id'] }}" data-company-id="{{$team['location']['company_id'] }}">
			                                                            <i class="fa fa-edit" aria-hidden="true" title="edit"></i>
			                                                        </div>
			                                                        <div class="company-team-close-button" data-id="{{ $team['id'] }}" data-team-id="{{ $team['id']}}" data-company-id="{{$team['location']['company_id']}}">
			                                                            <i class="fa fa-times" aria-hidden="true" title="delete"></i>
			                                                        </div>
			                                                    </div>
			                                                </div>
			                                            </div>
			                                            <?php
			                                                $image_path = '';
			                                                if(isset($team['profile_pic'])){
			                                                    $image_path = env('COMPANY_TEAM_PROFILE_PIC_PATH')."".$team['id']."/".$team['profile_pic'];
			                                                }
			                                            ?>
			                                            <div class="row">
			                                                <div class="col-sm-3">
			                                                    <div class="description">
			                                                        <span class="content">
			                                                            @if(!empty($image_path))
			                                                                <img id="preview" class="preview" src="/{{ $image_path }}">
			                                                            @else
			                                                                <img id="preview" class="preview" src="/images/user_default_image.png">
			                                                            @endif
			                                                        </span>
			                                                    </div>
			                                                </div>
			                                                <div class="col-sm-3">
			                                                    <div class="description">
			                                                        <span class="content-head">Member Name : </span>
			                                                        <span class="content">{{ !empty($team['agent_name']) ? $team['agent_name'] : '-' }}</span>
			                                                    </div>
			                                                </div>
			                                                <div class="col-sm-3">
			                                                    <div class="description">
			                                                        <span class="content-head">Designation : </span>
			                                                        <span class="content">{{ !empty($team['agent_designation']) ? $team['agent_designation'] : '-' }}</span>
			                                                    </div>
			                                                </div>
			                                                <div class="col-sm-3">
			                                                    <div class="description">
			                                                        <span class="content-head">Location : </span>
			                                                        <span class="content">
			                                                            @if(isset($team['location']['country']) && !empty($team['location']['country']))
			                                                                @if($team['location']['country'] == $india_value)
			                                                                    {{ ucfirst(\CommonHelper::countries()[$team['location']['country']]) }} {{ '/' }} {{ ucfirst($team['location']['state']['name']) }} {{ '/' }} {{ ucfirst($team['location']['city']['name']) }}
			                                                                @else
			                                                                    {{ ucfirst(\CommonHelper::countries()[$team['location']['country']]) }} {{ '/' }} {{ ucfirst($team['location']['state_text']) }} {{ '/' }} {{ ucfirst($team['location']['city_text']) }}
			                                                                @endif
			                                                            @else
			                                                                -
			                                                            @endif
			                                                        </span>
			                                                    </div>
			                                                </div>
			                                            </div>
			                                            <hr>
			                                        </div>
			                                    </div>
			                                @endforeach
			                            @else
			                                <div class="sub-details-container" style="box-shadow:none;margin:0px;">
			                                    <div class="content-container">
			                                        <div class="row">
			                                            <div class="col-xs-12 text-center">
			                                                <div class="discription">
			                                                    <span class="content-head">No Data Found.</span>
			                                                </div>
			                                            </div>
			                                        </div>
			                                    </div>
			                                </div>
			                            @endif
			                        </div>
			                    </div>
			                </form>
                        </div>

                        <div class="tab-pane fade" id="scope">
                        	<div class="scope-section">
			                    <ul class="nav nav-tabs user-tabs m-t-25">
			                        <li class="active">
			                            <a data-toggle="tab" href="#owners">
			                                <span>Owners</span>
			                            </a>
			                        </li>
			                        <li>
			                            <a data-toggle="tab" href="#managers">
			                                <span>Managers</span>
			                            </a>
			                        </li>
			                    </ul>
			                    <?php
			                        $owner_ship_yes = '';
			                        $owner_ship_no = '';
			                        $owner_section = 'hide';
			                        if(isset($data[0]['owner_ship']) && !empty($data[0]['owner_ship'])){
			                            $owner_ship_yes = 'checked';
			                            $owner_section = '';
			                        }else{
			                            $owner_ship_no = 'checked';
			                        }
			                    ?>
			                    <div class="tab-content">
			                        <div id="owners" class="tab-pane fade in active">
			                            <div class="owner-section">
			                            	
			                                <?php
			                                    $appointed_as_yes = '';
			                                    $appointed_as_no = '';
			                                    $appointed_as_section = 'hide';
			                                    if(isset($data[0]['company_registration_detail']['appointed_agent']) && !empty($data[0]['company_registration_detail']['appointed_agent'])){
			                                        $appointed_as_yes = 'checked';
			                                        $appointed_as_section = '';
			                                    }else{
			                                        $appointed_as_no = 'checked';
			                                    }
			                                ?>
			                                <div class="appointed-section display-section">
			                                    <div class="vessel-details">
			                                        <div class="row">
			                                            <div class="col-md-12">
			                                                <div class="scope-title">
			                                                    Our appointed managers / associates

			                                                    <span class="pull-right">
			                                                        <label class="radio-inline">
			                                                            <input type="radio" name="appointed" class="scope-radio" data-section="appointed-as" value="1" {{$appointed_as_yes}}>Yes</input>
			                                                        </label>
			                                                        <label class="radio-inline">
			                                                            <input type="radio" name="appointed" class="scope-radio" data-section="appointed-as" {{$appointed_as_no}} value="0">No</input>
			                                                        <label>
			                                                    </span>
			                                                </div>
			                                            </div>
			                                        </div>
			                                        <div class="m-t-15 appointed-as-section {{$appointed_as_section}}">
			                                            <form id="appointed_agent" method="post" action="{{route('admin.company.store.agent',$data[0]['company_registration_detail']['id'])}}">
			                                                <input class="hidden" name="agent_type" value="appointed"></input>
			                                                <input type="hidden" class="existing_agent_id" name="existing_agent_id" value="">
			                                                <div class="row">
			                                                    <div class="col-xs-12 col-sm-3">
			                                                        <div class="agent" id="agent">
			                                                            <label class="input-label" for="pic">Profile Pic</label>
			                                                            <div class="upload-photo-container">
			                                                                {{ csrf_field() }}
			                                                                <input type="hidden" name="image-x" required>
			                                                                <input type="hidden" name="image-y" required>
			                                                                <input type="hidden" name="image-x2" required>
			                                                                <input type="hidden" name="image-y2" required>
			                                                                <input type="hidden" name="crop-w" required>
			                                                                <input type="hidden" name="crop-h" required>
			                                                                <input type="hidden" name="image-w" required>
			                                                                <input type="hidden" name="image-h" required>
			                                                                <input type="hidden" name="uploaded-file-name" required>
			                                                                <input type="hidden" name="uploaded-file-path" required>
			                                                                <input type="hidden" class="path" value="{{env('COMPANY_TEAM_AGENT_PIC_PATH')}}"></input>

			                                                                <div class="image-content">
			                                                                    <div class="profile-image registration-profile-image">
			                                                                        
		                                                                            <div class="icon profile_pic_text"><i class="fa fa-camera" aria-hidden="true"></i></div>
		                                                                            <div class="image-text profile_pic_text">Upload Profile <br> Picture</div>

			                                                                        <input type="file" name="profile_pic" id="profile-pic" class="cursor-pointer" onChange="profilePicSelectHandler('appointed_agent', 'profile_pic', '' , 'agent_team')">

			                                                                        <img id="preview">
			                                                                    </div>
			                                                                </div>
			                                                            </div>
			                                                        </div>
			                                                    </div>
			                                                    <div class="col-sm-3">
			                                                        <label class="input-label" for="ship_name">Company Name<span class="symbol required"></span></label>
			                                                        <input type="text" name="company_name" class="form-control search-select company_name" value="{{ isset($value['company_name']) ? $value['company_name'] : ''}}" placeholder="Type your company name">
			                                                    </div>
			                                                    <div class="col-sm-3">
			                                                        <label class="input-label" for="location">Location<span class="symbol required"></span></label>
			                                                        <input type="text" name="location" class="form-control search-select location" value="{{ isset($value['location']) ? $value['location'] : ''}}" placeholder="Type your location">
			                                                    </div>
			                                                    <div class="col-sm-3 m-t-25">
			                                                        <button type="button" class="btn add-more-btn ladda-button save-agent-details" data-style="zoom-in" data-form="appointed_agent">Save</button>
			                                                    </div>
			                                                </div>
			                                                <div class="row m-t-15">
			                                                    <div class="col-sm-12">
			                                                        <div class="description">
			                                                            Agent Details
			                                                        </div>
			                                                    </div>
			                                                </div> 
			                                                <div class="appointed_agent">
			                                                    @if(isset($data[0]['company_registration_detail']['appointed_agent']) && !empty($data[0]['company_registration_detail']['appointed_agent']))
			                                                        @foreach($data[0]['company_registration_detail']['appointed_agent'] as $index => $agents)
			                                                        <div class="vessel-section agent_section_{{$agents['id']}}">
			                                                            <div class="row vessel-row">
			                                                                <div class="col-sm-12 vessel-title">
			                                                                    <div class="description">
			                                                                        <span class="content-head agent-name">Agent {{$index+1}}</span>
			                                                                        <div class="ship-buttons">
			                                                                            <div class="agent-edit-button" data-agent-id="{{$agents['id']}}" data-scope='appointed'>
			                                                                                <i class="fa fa-edit" aria-hidden="true" title="edit"></i>
			                                                                            </div>
			                                                                            <div class="agent-close-button" data-agent-id="{{$agents['id']}}" data-scope="appointed">
			                                                                                <i class="fa fa-times" aria-hidden="true" title="delete"></i>
			                                                                            </div>
			                                                                        </div>
			                                                                    </div>
			                                                                </div>
			                                                                <?php
			                                                                    $image_path = '';
			                                                                    if(isset($agents['logo'])){
			                                                                        $image_path = env('COMPANY_TEAM_AGENT_PIC_PATH')."".$agents['id']."/".$agents['logo'];
			                                                                    }
			                                                                ?>
			                                                                <div class="col-sm-3">
			                                                                    <div class="description">
			                                                                        <span class="content">
			                                                                            @if(!empty($image_path))
			                                                                                <img class="agent-preview" src="/{{ $image_path }}">
			                                                                            @else
			                                                                                <img class="agent-preview" src="/images/user_default_image.png">
			                                                                            @endif
			                                                                        </span>
			                                                                    </div>
			                                                                </div>
			                                                                <div class="col-sm-3">
			                                                                    <div class="description">
			                                                                        <span class="content-head">Company Name</span>
			                                                                        <span class="content">{{ !empty($agents['to_company']) ? $agents['to_company'] : '-'}}</span>
			                                                                    </div>
			                                                                </div>
			                                                                <div class="col-sm-3">
			                                                                    <div class="description">
			                                                                        <span class="content-head">Location</span>
			                                                                        <span class="content">{{ !empty($agents['location']) ? $agents['location'] : '-'}}</span>
			                                                                    </div>
			                                                                </div>
			                                                            </div>
			                                                            <hr>
			                                                        </div>                                            
			                                                        @endforeach
			                                                    @else
			                                                        <div class="row">
			                                                            <div class='col-xs-12 text-center'>
			                                                                <div class='discription'>
			                                                                    <span class='content-head'>No Data Found</span>
			                                                                </div>
			                                                            </div>
			                                                        </div>
			                                                    @endif
			                                                </div>
			                                            </form>
			                                        </div>
			                                    </div>
			                                </div>
			                                <div class="owner-to-section display-section">
			                                    <div class="row">
			                                        <div class="col-md-12">
			                                            <div class="scope-title">
			                                                We are owners to
			                                                <span class="pull-right">
			                                                    <label class="radio-inline">
			                                                        <input type="radio" name="owners_to" class="scope-radio" data-section="vessel" value="1" {{$owner_ship_yes}}>Yes</input>
			                                                    </label>
			                                                    <label class="radio-inline">
			                                                        <input type="radio" name="owners_to" class="scope-radio" data-section="vessel" {{$owner_ship_no}} value="0">No</input>
			                                                    <label>
			                                                </span>
			                                            </div>
			                                        </div>
			                                    </div>
			                                
			                                    <div class="vessel-details vessel-section {{$owner_section}} m-t-25">
			                                        <form id="owner-vessel" method="post" action="{{route('admin.company.store.ship.details',$data[0]['company_registration_detail']['id'])}}">
			                                            <div class="row">
			                                                <div class="col-md-12">
			                                                    <div class="scope-title">
			                                                        Enter the vessel details
			                                                    </div>
			                                                </div>
			                                            </div>
			                                            <div class="row m-t-15">
			                                                <input type="hidden" name="scope_type" value="owner">
			                                                <input type="hidden" class="existing_vessel_id" name="existing_vessel_id" value="">

			                                                <div class="col-sm-3">
			                                                    <label class="input-label" for="ship_name">Ship Name</label>
			                                                    <div class="form-group">
			                                                        <input type="text" name="ship_name" class="form-control search-select ship_name" placeholder="Type your ship name">
			                                                    </div>
			                                                </div>
			                                                <div class="col-sm-3">
			                                                    <label class="input-label">Type Of Ship<span class="symbol required"></span></label>
			                                                    <div class="form-group">
			                                                        <select name="ship_type" class="form-control search-select ship_type">
			                                                            <option value="">Select Ship Type</option>
			                                                            @foreach( \CommonHelper::ship_type() as $c_index => $ship_type)
			                                                                <option value="{{ $c_index }}"> {{ $ship_type }}</option>
			                                                            @endforeach
			                                                        </select>
			                                                    </div>
			                                                </div>
			                                                <div class="col-sm-3">
			                                                    <label class="input-label" for="no-of-ships">Flag Of Ship<span class="symbol required"></span></label>
			                                                    <div class="form-group">
			                                                        <select name="ship_flag" class="form-control search-select ship_flag">
			                                                                <option value="">Select Ship Flag</option>
			                                                                @foreach( \CommonHelper::countries() as $c_index => $ship_flag)
			                                                                    <option value="{{ $c_index }}"> {{ $ship_flag }}</option>
			                                                                @endforeach
			                                                        </select>
			                                                    </div>
			                                                </div>
			                                                <div class="col-sm-3">
			                                                    <div class="form-group">
			                                                        <label class="input-label" for="no-of-ships">Engine Type<span class="symbol required"></span></label>
			                                                        <select name="engine_type" class="form-control search-select engine_type">
			                                                            <option value="">Select Engine Type</option>
			                                                            @foreach( \CommonHelper::engine_type() as $c_index => $engine_type)
			                                                                <option value="{{ $c_index }}"> {{ $engine_type }}</option>
			                                                            @endforeach
			                                                        </select>
			                                                    </div>
			                                                </div>
			                                            </div>
			                                            <div class="row">
			                                                <div class="col-sm-3">
			                                                    <div class="form-group">
			                                                        <label class="input-label" for="grt">GRT</label>
			                                                        <input type="text" class="form-control grt" name="grt" placeholder="Type your GRT">
			                                                    </div>
			                                                </div>
			                                                <div class="col-sm-3">
			                                                    <div class="form-group">
			                                                        <label class="input-label" for="no-of-ships">BHP</label>
			                                                        <input type="text" class="form-control bhp" name="bhp" placeholder="Type your BHP">
			                                                    </div>
			                                                </div>
			                                                <div class="col-sm-3">
			                                                    <div class="form-group">
			                                                        <label class="input-label" for="p-i-cover">P & I cover</label>
			                                                        <div>
			                                                            <label class="radio-inline">
			                                                                <input type="radio" block-index='0' class="p-i-cover" name="p-i-cover" value="1"> Yes
			                                                            </label>
			                                                            <label class="radio-inline">
			                                                                <input type="radio" block-index='0' class="p-i-cover" name="p-i-cover" value="0" checked="checked"> No
			                                                            </label>
			                                                        </div>
			                                                    </div>
			                                                </div>
			                                                <div class="col-sm-3 m-t-25">
			                                                    <button type="button" class="btn add-more-btn ladda-button save-vessel-details" data-form="owner-vessel" data-style="zoom-in">Save</button>
			                                                </div>
			                                            </div>

			                                            <div class="row m-t-15">
			                                                <div class="col-sm-12">
			                                                    <div class="description">
			                                                        Vessel Details
			                                                    </div>
			                                                </div>
			                                            </div>
			                                            <div class="owner-vessel">
			                                                @if(isset($data[0]['owner_ship']) && !empty($data[0]['owner_ship']))    
			                                                    @foreach($data[0]['owner_ship'] as $index => $ship)
			                                                    <div class="vessel-section vessel_section_{{$ship['id']}}">
			                                                        <div class="row vessel-row">
			                                                            <div class="col-sm-12 vessel-title">
			                                                                <div class="description">
			                                                                    <span class="content-head vessel-name">Vessel {{$index+1}}</span>
			                                                                    <div class="ship-buttons">
			                                                                        <div class="vessel-edit-button" data-vessel-id="{{$ship['id']}}" data-scope='owner'>
			                                                                            <i class="fa fa-edit" aria-hidden="true" title="edit"></i>
			                                                                        </div>
			                                                                        <div class="vessel-close-button" data-vessel-id="{{$ship['id']}}" data-scope='owner'>
			                                                                            <i class="fa fa-times" aria-hidden="true" title="delete"></i>
			                                                                        </div>
			                                                                    </div>
			                                                                </div>
			                                                            </div>
			                                                            <div class="col-xs-6 col-sm-2">
			                                                                <div class="description">
			                                                                    <span class="content-head">Vessel Name</span>
			                                                                    <span class="content">{{ !empty($ship['ship_name']) ? $ship['ship_name'] : '-' }}</span>
			                                                                </div>
			                                                            </div>
			                                                            <div class="col-xs-6 col-sm-2">
			                                                                <div class="description">
			                                                                    <span class="content-head">Type</span>
			                                                                    <span class="content">
			                                                                        @foreach( \CommonHelper::ship_type() as $c_index => $ship_type)
			                                                                            {{ isset($ship['ship_type']) ? $ship['ship_type'] == $c_index ? $ship_type : '' : ''}}
			                                                                        @endforeach
			                                                                    </span>
			                                                                </div>
			                                                            </div>
			                                                            <div class="col-xs-6 col-sm-2">
			                                                                <div class="description">
			                                                                    <span class="content-head">Flag</span>
			                                                                    <span class="content">
			                                                                        @if(isset($ship['ship_flag']) && !empty($ship['ship_flag']))
			                                                                            @foreach( \CommonHelper::countries() as $c_index => $ship_flag)
			                                                                                {{ isset($ship['ship_flag']) ? $ship['ship_flag'] == $c_index ? $ship_flag : '' : ''}}
			                                                                            @endforeach
			                                                                        @else
			                                                                            -
			                                                                        @endif
			                                                                    </span>
			                                                                </div>
			                                                            </div>
			                                                            <div class="col-xs-6 col-sm-1">
			                                                                <div class="description">
			                                                                    <span class="content-head">GRT</span>
			                                                                    <span class="content">{{ !empty($ship['grt']) ? $ship['grt'] : '-' }}</span>
			                                                                </div>
			                                                            </div>
			                                                            <div class="col-xs-6 col-sm-1">
			                                                                <div class="description">
			                                                                    <span class="content-head">BHP</span>
			                                                                    <span class="content">{{ !empty($ship['bhp']) ? $ship['bhp'] : '-' }}</span>
			                                                                </div>
			                                                            </div>
			                                                            <div class="col-xs-6 col-sm-2">
			                                                                <div class="description">
			                                                                    <span class="content-head">Engine</span>
			                                                                    <span class="content">
			                                                                        @if(isset($ship['engine_type']) && !empty($ship['engine_type']))
			                                                                            @foreach( \CommonHelper::engine_type() as $c_index => $engine_type)
			                                                                                {{ isset($ship['engine_type']) ? $ship['engine_type'] == $c_index ? $engine_type : '' : ''}}
			                                                                            @endforeach
			                                                                        @else
			                                                                            -
			                                                                        @endif
			                                                                    </span>
			                                                                </div>
			                                                            </div>
			                                                            <div class="col-sm-2">
			                                                                <div class="description">
			                                                                    <span class="content-head">P & I</span>
			                                                                    <span class="content">
			                                                                        {{ isset($ship['p_i_cover']) ? $ship['p_i_cover'] == '1' ? 'Yes' : 'No' : ''}}
			                                                                    </span>
			                                                                </div>
			                                                            </div>
			                                                        </div>
			                                                        <hr>
			                                                    </div>
			                                                    @endforeach
			                                                @else
			                                                    <div class="row">
			                                                        <div class='col-xs-12 text-center'>
			                                                            <div class='discription'>
			                                                                <span class='content-head'>No Data Found</span>
			                                                            </div>
			                                                        </div>
			                                                    </div>
			                                                @endif
			                                            </div>
			                                        </form>
			                                    </div>
			                                </div>
			                            </div>
			                        </div>
			                        <?php
			                            $associate_as_yes = '';
			                            $associate_as_no = '';
			                            $associate_as_section = 'hide';
			                            if(isset($data[0]['company_registration_detail']['associate_agent']) && !empty($data[0]['company_registration_detail']['associate_agent'])){
			                                $associate_as_yes = 'checked';
			                                $associate_as_section = '';
			                            }else{
			                                $associate_as_no = 'checked';
			                            }
			                        ?>
			                        <div id="managers" class="tab-pane fade">
			                            <div class="manager-section">
			                                <div class="appointed-section display-section">
			                                    <div class="vessel-details">
			                                        <div class="row">
			                                            <div class="col-md-12">
			                                                <div class="scope-title">
			                                                    We are crew managers to
			                                                    <span class="pull-right">
			                                                        <label class="radio-inline">
			                                                            <input type="radio" name="crew" class="scope-radio" data-section="crew" value="1" {{$associate_as_yes}}>Yes</input>
			                                                        </label>
			                                                        <label class="radio-inline">
			                                                            <input type="radio" name="crew" class="scope-radio" data-section="crew" {{$associate_as_no}} value="0">No</input>
			                                                        <label>
			                                                    </span>
			                                                </div>
			                                            </div>
			                                        </div>
			                                        <div class="m-t-15 crew-section {{$associate_as_section}}">
			                                            <form id="associate_agent" method="post" action="{{route('admin.company.store.agent',$data[0]['company_registration_detail']['id'])}}">
			                                                <input class="hidden" name="agent_type" value="associate"></input>
			                                                <input type="hidden" class="existing_agent_id" name="existing_agent_id" value="">
			                                                <div class="row">
			                                                    <div class="col-xs-12 col-sm-3">
			                                                        <div class="agent" id="agent">
			                                                            <label class="input-label" for="pic">Profile Pic</label>
			                                                            <div class="upload-photo-container">
			                                                                {{ csrf_field() }}
			                                                                <input type="hidden" name="image-x" required>
			                                                                <input type="hidden" name="image-y" required>
			                                                                <input type="hidden" name="image-x2" required>
			                                                                <input type="hidden" name="image-y2" required>
			                                                                <input type="hidden" name="crop-w" required>
			                                                                <input type="hidden" name="crop-h" required>
			                                                                <input type="hidden" name="image-w" required>
			                                                                <input type="hidden" name="image-h" required>
			                                                                <input type="hidden" name="uploaded-file-name" required>
			                                                                <input type="hidden" name="uploaded-file-path" required>
			                                                                <input type="hidden" class="path" value="{{env('COMPANY_TEAM_AGENT_PIC_PATH')}}"></input>

			                                                                <div class="image-content">
			                                                                    <div class="profile-image registration-profile-image">
			                                                                        
		                                                                            <div class="icon profile_pic_text"><i class="fa fa-camera" aria-hidden="true"></i></div>
		                                                                            <div class="image-text profile_pic_text">Upload Profile <br> Picture</div>

			                                                                        <input type="file" name="profile_pic" id="profile-pic" class="cursor-pointer" onChange="profilePicSelectHandler('associate_agent', 'profile_pic', '' , 'agent_team')">

			                                                                        <img id="preview">
			                                                                        
			                                                                    </div>
			                                                                </div>
			                                                            </div>
			                                                        </div>
			                                                    </div>
			                                                    <div class="col-sm-3">
			                                                        <label class="input-label" for="ship_name">Company Name<span class="symbol required"></span></label>
			                                                        <input type="text" name="company_name" class="form-control search-select company_name" value="{{ isset($value['company_name']) ? $value['company_name'] : ''}}" placeholder="Type your company name">
			                                                    </div>
			                                                    <div class="col-sm-3">
			                                                        <label class="input-label" for="location">Location<span class="symbol required"></span></label>
			                                                        <input type="text" name="location" class="form-control search-select location" value="{{ isset($value['location']) ? $value['location'] : ''}}" placeholder="Type your location">
			                                                    </div>
			                                                    <div class="col-sm-3 m-t-25">
			                                                        <button type="button" class="btn add-more-btn ladda-button save-agent-details" data-style="zoom-in" data-form="associate_agent">Save</button>
			                                                    </div>
			                                                </div>
			                                                <div class="row m-t-15">
			                                                    <div class="col-sm-12">
			                                                        <div class="description">
			                                                            Agent Details
			                                                        </div>
			                                                    </div>
			                                                </div> 
			                                                <div class="associate_agent">
			                                                    @if(isset($data[0]['company_registration_detail']['associate_agent']) && !empty($data[0]['company_registration_detail']['associate_agent']))
			                                                        @foreach($data[0]['company_registration_detail']['associate_agent'] as $index => $agents)
			                                                        <div class="vessel-section agent_section_{{$agents['id']}}">
			                                                            <div class="row vessel-row">
			                                                                <div class="col-sm-12 vessel-title">
			                                                                    <div class="description">
			                                                                        <span class="content-head agent-name">Agent {{$index+1}}</span>
			                                                                        <div class="ship-buttons">
			                                                                            <div class="agent-edit-button" data-agent-id="{{$agents['id']}}" data-scope='associate'>
			                                                                                <i class="fa fa-edit" aria-hidden="true" title="edit"></i>
			                                                                            </div>
			                                                                            <div class="agent-close-button" data-agent-id="{{$agents['id']}}" data-scope="associate">
			                                                                                <i class="fa fa-times" aria-hidden="true" title="delete"></i>
			                                                                            </div>
			                                                                        </div>
			                                                                    </div>
			                                                                </div>
			                                                                <?php
			                                                                    $image_path = '';
			                                                                    if(isset($agents['logo'])){
			                                                                        $image_path = env('COMPANY_TEAM_AGENT_PIC_PATH')."".$agents['id']."/".$agents['logo'];
			                                                                    }
			                                                                ?>
			                                                                <div class="col-sm-3">
			                                                                    <div class="description">
			                                                                        <span class="content">
			                                                                            @if(!empty($image_path))
			                                                                                <img class="agent-preview" src="/{{ $image_path }}">
			                                                                            @else
			                                                                                <img class="agent-preview" src="/images/user_default_image.png">
			                                                                            @endif
			                                                                        </span>
			                                                                    </div>
			                                                                </div>
			                                                                <div class="col-sm-3">
			                                                                    <div class="description">
			                                                                        <span class="content-head">Company Name</span>
			                                                                        <span class="content">{{ !empty($agents['from_company']) ? $agents['from_company'] : '-'}}</span>
			                                                                    </div>
			                                                                </div>
			                                                                <div class="col-sm-3">
			                                                                    <div class="description">
			                                                                        <span class="content-head">Location</span>
			                                                                        <span class="content">{{ !empty($agents['location']) ? $agents['location'] : '-'}}</span>
			                                                                    </div>
			                                                                </div>
			                                                            </div>
			                                                            <hr>
			                                                        </div>
			                                                        @endforeach
			                                                    @else
			                                                        <div class="row">
			                                                            <div class='col-xs-12 text-center'>
			                                                                <div class='discription'>
			                                                                    <span class='content-head'>No Data Found</span>
			                                                                </div>
			                                                            </div>
			                                                        </div>
			                                                    @endif
			                                                    </div>
			                                                </div>
			                                            </form>
			                                        </div>
			                                    </div>
			                                </div>
			                                <?php
			                                    $manager_vessel_yes = '';
			                                    $manager_vessel_no = '';
			                                    $manager_vessel_section = 'hide';
			                                    if(isset($data[0]['manager_ship']) && !empty($data[0]['manager_ship'])){
			                                        $manager_vessel_yes = 'checked';
			                                        $manager_vessel_section = '';
			                                    }else{
			                                        $manager_vessel_no = 'checked';
			                                    }
			                                ?>
			                                <div class="manager-vessels-section display-section">
			                                    <div class="row">
			                                        <div class="col-md-12">
			                                            <div class="scope-title">
			                                                The vessels we manage

			                                                <span class="pull-right">
			                                                    <label class="radio-inline">
			                                                        <input type="radio" name="manager-vessel" class="scope-radio" data-section="manager-vessel" value="1" {{$manager_vessel_yes}}>Yes</input>
			                                                    </label>
			                                                    <label class="radio-inline">
			                                                        <input type="radio" name="manager-vessel" class="scope-radio" data-section="manager-vessel" {{$manager_vessel_no}} value="0">No</input>
			                                                    <label>
			                                                </span>
			                                            </div>
			                                        </div>
			                                    </div>
			                                
			                                    <div class="vessel-details manager-vessel-section m-t-25 {{$manager_vessel_section}}">
			                                        <form id="manager-vessel" method="post" action="{{route('admin.company.store.ship.details',$data[0]['company_registration_detail']['id'])}}">
			                                            <div class="row">
			                                                <div class="col-md-12">
			                                                    <div class="scope-title">
			                                                        Enter the vessel details
			                                                    </div>
			                                                </div>
			                                            </div>
			                                            <div class="row m-t-15">
			                                                <input type="hidden" name="scope_type" value="manager">
			                                                <input type="hidden" class="existing_vessel_id" name="existing_vessel_id" value="">

			                                                <div class="col-sm-3">
			                                                    <label class="input-label" for="ship_name">Ship Name</label>
			                                                    <div class="form-group">
			                                                        <input type="text" name="ship_name" class="form-control search-select ship_name" placeholder="Type your ship name">
			                                                    </div>
			                                                </div>
			                                                <div class="col-sm-3">
			                                                    <label class="input-label">Type Of Ship<span class="symbol required"></span></label>
			                                                    <div class="form-group">
			                                                        <select name="ship_type" class="form-control search-select ship_type">
			                                                            <option value="">Select Ship Type</option>
			                                                            @foreach( \CommonHelper::ship_type() as $c_index => $ship_type)
			                                                                <option value="{{ $c_index }}"> {{ $ship_type }}</option>
			                                                            @endforeach
			                                                        </select>
			                                                    </div>
			                                                </div>
			                                                <div class="col-sm-3">
			                                                    <label class="input-label" for="no-of-ships">Flag Of Ship<span class="symbol required"></span></label>
			                                                    <div class="form-group">
			                                                        <select name="ship_flag" class="form-control search-select ship_flag">
			                                                                <option value="">Select Ship Flag</option>
			                                                                @foreach( \CommonHelper::countries() as $c_index => $ship_flag)
			                                                                    <option value="{{ $c_index }}"> {{ $ship_flag }}</option>
			                                                                @endforeach
			                                                        </select>
			                                                    </div>
			                                                </div>
			                                                <div class="col-sm-3">
			                                                    <div class="form-group">
			                                                        <label class="input-label" for="no-of-ships">Engine Type<span class="symbol required"></span></label>
			                                                        <select name="engine_type" class="form-control search-select engine_type">
			                                                            <option value="">Select Engine Type</option>
			                                                            @foreach( \CommonHelper::engine_type() as $c_index => $engine_type)
			                                                                <option value="{{ $c_index }}"> {{ $engine_type }}</option>
			                                                            @endforeach
			                                                        </select>
			                                                    </div>
			                                                </div>
			                                            </div>
			                                            <div class="row">
			                                                <div class="col-sm-3">
			                                                    <div class="form-group">
			                                                        <label class="input-label" for="grt">GRT</label>
			                                                        <input type="text" class="form-control grt" name="grt" placeholder="Type your GRT">
			                                                    </div>
			                                                </div>
			                                                <div class="col-sm-3">
			                                                    <div class="form-group">
			                                                        <label class="input-label" for="no-of-ships">BHP</label>
			                                                        <input type="text" class="form-control bhp" name="bhp" placeholder="Type your BHP">
			                                                    </div>
			                                                </div>
			                                                <div class="col-sm-3">
			                                                    <div class="form-group">
			                                                        <label class="input-label" for="p-i-cover">P & I cover</label>
			                                                        <div>
			                                                            <label class="radio-inline">
			                                                                <input type="radio" block-index='0' class="p-i-cover" name="p-i-cover" value="1"> Yes
			                                                            </label>
			                                                            <label class="radio-inline">
			                                                                <input type="radio" block-index='0' class="p-i-cover" name="p-i-cover" value="0" checked="checked"> No
			                                                            </label>
			                                                        </div>
			                                                    </div>
			                                                </div>
			                                                <div class="col-sm-3 m-t-25">
			                                                    <button type="button" class="btn add-more-btn ladda-button save-vessel-details" data-form="manager-vessel" data-style="zoom-in">Save</button>
			                                                </div>
			                                            </div>

			                                            <div class="row m-t-15">
			                                                <div class="col-sm-12">
			                                                    <div class="description">
			                                                        Vessel Details
			                                                    </div>
			                                                </div>
			                                            </div>
			                                            <div class="manager-vessel">
			                                                @if(isset($data[0]['manager_ship']) && !empty($data[0]['manager_ship'])) 
			                                                    @foreach($data[0]['manager_ship']  as $index => $ship)
			                                                    <div class="vessel-section vessel_section_{{$ship['id']}}">
			                                                        <div class="row vessel-row">
			                                                            <div class="col-sm-12 vessel-title">
			                                                                <div class="description">
			                                                                    <span class="content-head vessel-name">Vessel {{$index+1}}</span>
			                                                                    <div class="ship-buttons">
			                                                                        <div class="vessel-edit-button" data-vessel-id="{{$ship['id']}}" data-scope="manager">
			                                                                            <i class="fa fa-edit" aria-hidden="true" title="edit"></i>
			                                                                        </div>
			                                                                        <div class="vessel-close-button" data-vessel-id="{{$ship['id']}}" data-scope="manager">
			                                                                            <i class="fa fa-times" aria-hidden="true" title="delete"></i>
			                                                                        </div>
			                                                                    </div>
			                                                                </div>
			                                                            </div>
			                                                            <div class="col-sm-2">
			                                                                <div class="description">
			                                                                    <span class="content-head">Vessel Name</span>
			                                                                    <span class="content">{{ !empty($ship['ship_name']) ? $ship['ship_name'] : '-' }}</span>
			                                                                </div>
			                                                            </div>
			                                                            <div class="col-sm-2">
			                                                                <div class="description">
			                                                                    <span class="content-head">Type</span>
			                                                                    <span class="content">
			                                                                        @foreach( \CommonHelper::ship_type() as $c_index => $ship_type)
			                                                                            {{ isset($ship['ship_type']) ? $ship['ship_type'] == $c_index ? $ship_type : '' : ''}}
			                                                                        @endforeach
			                                                                    </span>
			                                                                </div>
			                                                            </div>
			                                                            <div class="col-sm-2">
			                                                                <div class="description">
			                                                                    <span class="content-head">Flag</span>
			                                                                    <span class="content">
			                                                                        @if(isset($ship['ship_flag']) && !empty($ship['ship_flag']))
			                                                                            @foreach( \CommonHelper::countries() as $c_index => $ship_flag)
			                                                                                {{ isset($ship['ship_flag']) ? $ship['ship_flag'] == $c_index ? $ship_flag : '' : ''}}
			                                                                            @endforeach
			                                                                        @else
			                                                                            -
			                                                                        @endif
			                                                                    </span>
			                                                                </div>
			                                                            </div>
			                                                            <div class="col-sm-1">
			                                                                <div class="description">
			                                                                    <span class="content-head">GRT</span>
			                                                                    <span class="content">{{ !empty($ship['grt']) ? $ship['grt'] : '-' }}</span>
			                                                                </div>
			                                                            </div>
			                                                            <div class="col-sm-1">
			                                                                <div class="description">
			                                                                    <span class="content-head">BHP</span>
			                                                                    <span class="content">{{ !empty($ship['bhp']) ? $ship['bhp'] : '-' }}</span>
			                                                                </div>
			                                                            </div>
			                                                            <div class="col-sm-2">
			                                                                <div class="description">
			                                                                    <span class="content-head">Engine</span>
			                                                                    <span class="content">
			                                                                        @if(isset($ship['engine_type']) && !empty($ship['engine_type']))
			                                                                            @foreach( \CommonHelper::engine_type() as $c_index => $engine_type)
			                                                                                {{ isset($ship['engine_type']) ? $ship['engine_type'] == $c_index ? $engine_type : '' : ''}}
			                                                                            @endforeach
			                                                                        @else
			                                                                            -
			                                                                        @endif
			                                                                    </span>
			                                                                </div>
			                                                            </div>
			                                                            <div class="col-sm-2">
			                                                                <div class="description">
			                                                                    <span class="content-head">P & I</span>
			                                                                    <span class="content">
			                                                                        {{ isset($ship['p_i_cover']) ? $ship['p_i_cover'] == '1' ? 'Yes' : 'No' : ''}}
			                                                                    </span>
			                                                                </div>
			                                                            </div>
			                                                        </div>
			                                                        <hr>
			                                                    </div>
			                                                    @endforeach
			                                                @else
			                                                    <div class="row">
			                                                        <div class='col-xs-12 text-center'>
			                                                            <div class='discription'>
			                                                                <span class='content-head'>No Data Found</span>
			                                                            </div>
			                                                        </div>
			                                                    </div>
			                                                @endif
			                                            </div>
			                                        </form>
			                                    </div>
			                                </div>
			                            </div>
			                        </div>
			                    </div>
			                </div>
                        </div>

					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@stop
@section('js_script')
	<script type="text/javascript">
        $('.select2-select').select2({
            closeOnSelect: false
        });
    </script>
    <script type="text/javascript" src="/js/site/company-registration.js"></script>
    <script type="text/javascript" src="/js/site/job.js"></script>
@stop