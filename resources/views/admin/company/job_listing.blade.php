@extends('admin.index')

@section('content')

<div class="main-wrapper">
	<div class="main-container1">
				<!-- start: PAGE -->
				<div class="main-content">
					<div class="container">
						<!-- start: PAGE HEADER -->
						<!-- start: TOOLBAR -->
						<div class="toolbar row">
							<div class="col-sm-6 hidden-xs">
								<div class="page-header">
									<h1>Jobs <small>A list of all company jobs</small></h1>
								</div>
							</div>
						</div>

						<!-- filter -->
						<form class="filter-search-form m_l_10">
							<div class="row m_t_25">
								<div class="col-md-12">
									<div class="panel panel-white">
										<div class="panel-body">
											<div class="row">
												<div class="col-sm-3">
					                                <div class="form-group">
					                                    <label>Job ID </label>
					                                    <select id="job_id" name="job_id" class="form-control">
						                                    <option value=''>Select Job Id</option>
						                                    @if(isset($job_list) && !empty($job_list))
							                                    @foreach($job_list as $index => $list)
							                                        <option value="{{$list['id']}}" {{ isset($job_data[0]['job_category']) ? $job_data[0]['job_category'] == $index ? 'selected' : '' : ''}}>{{$list['id']}}</option>
							                                    @endforeach
							                                @endif
						                                </select>
					                                </div>
					                            </div>
					                            <div class="col-sm-3">
					                                <div class="form-group">
					                                    <label>Company Name </label>
					                                    <select name="company_id" class="form-control">
						                                    <option value=''>Select Company</option>
						                                    @if(isset($company_list) && !empty($company_list))
							                                    @foreach($company_list as $index => $list)
							                                        <option value="{{$list['id']}}" {{ isset($job_data[0]['job_category']) ? $job_data[0]['job_category'] == $list['id'] ? 'selected' : '' : ''}}>{{$list['company_name']}}</option>
							                                    @endforeach
							                                @endif
						                                </select>
					                                </div>
					                            </div>
					                            <div class="col-sm-3">
					                                <div class="form-group">
					                                    <label>Rank </label>
					                                    <select id="rank" name="rank" class="form-control">
						                                    <option value=''>Select Rank</option>
						                                    @foreach(\CommonHelper::new_rank() as $rank_index => $category)
						                                        <optgroup label="{{$rank_index}}">
						                                        @foreach($category as $r_index => $rank)
						                                            <option value="{{$r_index}}" {{ isset($job_data[0]['rank']) ? $job_data[0]['rank'] == $r_index ? 'selected' : '' : ''}}>{{$rank}} </option>
						                                        @endforeach
						                                    @endforeach
						                                </select>
					                                </div>
					                            </div>         
												<div class="col-sm-3">
					                                <div class="form-group">
					                                    <label>Ship Type </label>
                                                		<select id="ship_type" name="ship_type" class="form-control ship_type_change">
						                                    <option value=''>Select Ship Type</option>
						                                    @foreach( \CommonHelper::ship_type() as $c_index => $ship_type)
						                                        <option value="{{ $c_index }}" {{ !empty($job_data[0]['ship_type']) ? $job_data[0]['ship_type'] == $c_index ? 'selected' : '' : ''  }}> {{ $ship_type }}</option>
						                                    @endforeach
						                                </select>
					                                </div>
					                            </div>
					                        </div>
					                        <div class="row">
					                            <div class="col-sm-12">
					                                <div class="section_cta text-right m_t_25 job-btn-container">
					                                    <button type="button" class="btn btn-danger coss-inverse-btn search-reset-btn" id="seafarerResetButton">Reset</button>
					                                    <button type="submit" data-style="zoom-in" class="btn btn-info coss-primary-btn search-post-btn ladda-button candidate-filter-search-button" id="candidate-filter-search-button"><i class="fa fa-search search-icon" aria-hidden="true"></i>Search</button>
					                                </div>
					                            </div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</form>

						<div class="row search-results-main-container">
							<div class="col-md-12">
								<div class="panel panel-white">
									<div class="panel-body">

										@if(isset($data) && !empty($data))
											
											<div class="panel-heading">
												<div class="row">
													<div class="col-xs-12">
														<div class="col-xs-6 paginator-content  pull-right no-margin">
															<ul id="top-user-paginator" class="pagination">
																{!! $paginate->render() !!}
															</ul>
														</div>
														
														<div class="pull-left search-count-content">
															Showing {{$pagination_data['from']}} - {{$pagination_data['to']}} of {{$pagination_data['total']}} Results
														</div>
													</div>
												</div>
											</div>
											
											<div class="full-cnt-loader hidden">
												<div class="" style="text-align: center;padding-top:20px;padding-bottom:20px; ">
												    <i class="fa fa-spinner fa-spin fa-4" aria-hidden="true" style="font-size: 7em;"></i>
											    </div>
											</div>
											<div class="list-container">
												@foreach($data as $data)	
												<div class="seafarer-listing-container">
													<div class="panel">
														<div class="panel-heading border-light partition-blue">
															<h5 class="panel-title text-bold header-link-text p-5-5">
																<a href="">
																	{{isset($data['company_registration_details']['company_name']) ? ucfirst($data['company_registration_details']['company_name']) : ''}}
																</a>
															</h5>

															<ul class="panel-heading-tabs border-light">
																<li class="panel-tools">
																	<div class="dropdown">
																		<a data-toggle="dropdown" class="btn btn-xs dropdown-toggle btn-transparent-grey">
																			<i class="fa fa-cog"></i>
																		</a>
																		<ul class="dropdown-menu dropdown-light pull-right" role="menu">
																			<!-- <li>
																				<a href="{{Route('admin.view.advertiser.id',$data['id'])}}" data-type="user">
																					View
																				</a>
																			</li> -->
																			<li>
																				<a href="{{Route('admin.edit.company.jobs',[$data['company_id'],$data['id']])}}">
																					Edit
																				</a>
																			</li>
																			<?php 
																				$disable= 'hide';
																				$enable= 'hide';
																				if(isset($data) && $data['status'] == 1){
																					$disable = '';
																				}else{
																					$enable = '';
																				}
																			?>
																			<li>
																				<a class="jobDisableButton disable-{{$data['id']}} {{$disable}}" data-status='disable' data-id={{$data['id']}} href="">
																					<span>Disable</span>
																				</a>
																			</li>
																			
																			<li>
																				<a class="jobDisableButton enable-{{$data['id']}} {{$enable}}" data-status='enable' data-id={{$data['id']}} href="">
																					<span>Enable</span>
																				</a>
																			</li>
																			
																		</ul>
																	</div>
																</li>
															</ul>

														</div>
																		
														<div class="panel-body">
															<div class="row">
																
																<div class="col-lg-3 col-md-5 col-sm-5" id="user-profile-pic" style="height: 200px;width: 200px; margin-left: 15px;">
																	<a>
																		<div class="company_profile_pic">
										                					<img src="/{{ isset($data['company_registration_details']['user_details']['profile_pic']) ? env('COMPANY_LOGO_PATH')."/".$data['company_registration_details']['user_id']."/".$data['company_registration_details']['user_details']['profile_pic'] : 'images/user_default_image.png'}}" >
										                				</div>
							                            			</a>
																</div>
																<div class="col-lg-9 col-md-7 col-sm-7">
																	<div class="row">																
																		<div class="col-sm-4">		
																			<div id="user-professional-details">
																				<div class="row">
																					<div class="col-xs-12">
																						<div class="header-tag">
																							Company Details
																						</div>
																					</div>
																				</div>
																				<dl>
																		     		<dt> Company Name : </dt>
																						<dd class="margin-bottom-5px">
																							{{ isset($data['company_registration_details']['company_name']) ? $data['company_registration_details']['company_name'] : '' }}
																			     		</dd>
																		     		<dt>
																					<dt> Company Email :</dt>
																					<dd class="margin-bottom-5px">
																						{{isset($data['company_registration_details']['email']) ? $data['company_registration_details']['email'] : ''}}
																					</dd>
																					<dt> Contact Number :</dt>
																					<dd class="margin-bottom-5px">
																						{{isset($data['company_registration_details']['contact_number']) ? $data['company_registration_details']['contact_number'] : ''}}
																					</dd>
																				</dl>
																			</div>
																		</div>
																		<div class="col-sm-4">		
																			<div id="user-professional-details">
																				<div class="row">
																					<div class="col-xs-12">
																						<div class="header-tag">
																							Job Details
																						</div>
																					</div>
																				</div>
																				<dl>
																					<dt> Job ID : </dt>
																						<dd class="margin-bottom-5px">
																							{{ isset($data['id']) ? $data['id'] : ''}}
																			     		</dd>
																		     		<dt>
																		     		<dt> Rank : </dt>
																						<dd class="margin-bottom-5px">
																							@foreach(\CommonHelper::new_rank() as $rank_index => $category)
																                                @foreach($category as $r_index => $rank)
																                                    {{ isset($data['rank']) ? $data['rank'] == $r_index ? $rank : '' : ''}}
																                                @endforeach
																                            @endforeach
																			     		</dd>
																		     		<dt>
																					<dt> Ship Type :</dt>
																					<dd class="margin-bottom-5px">
																						@foreach( \CommonHelper::ship_type() as $c_index => $ship_type)
												                                            {{ isset($data['ship_type']) ? $data['ship_type'] == $c_index ? $ship_type : '' : ''}}
												                                        @endforeach
																					</dd>
																					<dt> Date Of Joining :</dt>
																					<dd class="margin-bottom-5px">
																						{{ isset($data['date_of_joining']) && !empty($data['date_of_joining']) ? $data['date_of_joining'] : '-'}}
																					</dd>
																				</dl>
																			</div>
																		</div>
																		<div class="col-sm-4">		
																			<div id="user-professional-details">
																				<div class="row">
																					<div class="col-xs-12">
																						<div class="header-tag m_t_20">
																							 
																						</div>
																					</div>
																				</div>
																				<dl>
																		     		<dt> BHP : </dt>
																						<dd class="margin-bottom-5px">
																							{{ isset($data['bhp']) ? $data['bhp'] : ''}}
																			     		</dd>
																		     		<dt>
																					<dt> GRT :</dt>
																					<dd class="margin-bottom-5px">
																						{{ isset($data['grt']) ? $data['grt'] : ''}}
																					</dd>
																					<dt> Engine Type :</dt>
																					<dd class="margin-bottom-5px">
																						@foreach( \CommonHelper::engine_type() as $c_index => $engine_type)
																							{{ !empty($data['engine_type']) ? $data['engine_type'] == $c_index ? $engine_type : '' : ''}}
																						@endforeach
																					</dd>
																					<dt> Minimum Rank Experience :</dt>
																					<dd class="margin-bottom-5px">
																						{{ isset($data['min_rank_exp']) ? $data['min_rank_exp'] : ''}} Years
																					</dd>
																				</dl>
																			</div>
																		</div>
																	</div>
																</div>
																				
															</div>
														</div>
													</div>
																	
												</div>
												@endforeach
											</div>
										@else
											<div class="no-results-found">No results found. Try again with different search criteria.</div>
										@endif

										@if(isset($data) && !empty($data))
											<div class="panel-heading">
												<div class="row">
													<div class="col-xs-12">
														<div class="col-xs-6 paginator-content  pull-right no-margin">
															<ul id="top-user-paginator" class="pagination">
																{!! $paginate->render() !!}
															</ul>
														</div>
														
														<div class="pull-left search-count-content">
															Showing {{$pagination_data['from']}} - {{$pagination_data['to']}} of {{$pagination_data['total']}} Results
														</div>
													</div>
												</div>
											</div>
										@endif

									</div>
								</div>
							</div>
						</div>
						<!-- end: PAGE CONTENT-->
					</div>
					<div class="subviews">
						<div class="subviews-container"></div>
					</div>
				</div>
				<!-- end: PAGE -->
			</div>
</div>

<script type="text/javascript">
        
        var searchAjaxCall = "undefined";

        $(document).ready(function(){
            History = window.History; // Note: We are using a capital H instead of a lower h
			
            History.Adapter.bind(window,'statechange', function() {

                var State = History.getState(); // Note: We are using History.getState() instead of event.state

                fetchSearchResults(State.url, 
                function() {
                    
                }, function() {

                });

            }); 

        });

        function fetchSearchResults(url, successCallback, errorCallback)
	    {
	    	
	    	var temp_current_state_url = url.split('?');
            if(temp_current_state_url.length > 1 && temp_current_state_url[1] != ''){
                var parameters = temp_current_state_url[1].split('&');

                var dropdown_elements = ['rank','ship_type','job_id','company_id'];

                for(var i=0; i<parameters.length; i++){

                    var param_array = parameters[i].split('=');
                    if($.inArray(param_array[0],dropdown_elements) > -1){
                        $('select[name="'+param_array[0]+'"]').val(param_array[1]);
                    }else if($.inArray(param_array[0],['availability_from','availability_to','first_name']) > -1){
                        $('input[name="'+param_array[0]+'"]').val(param_array[1]);
                    }

                }
            } else {
             	$(':input','#candidate-filter-form')
	                .not(':button, :submit, :reset, :hidden')
	                .val('')
	                .removeAttr('checked')
	                .removeAttr('selected');
	             }

	        if(searchAjaxCall != "undefined")
	        {
	            searchAjaxCall.abort();
	            searchAjaxCall = "undefined";
	        }

	        $('.list-container').addClass('hide');
	        $('.full-cnt-loader').removeClass('hidden');

	        searchAjaxCall = $.ajax({
	            url: url,
	            dataType: 'json',
	            cache:false,
	            statusCode: {
	                200: function(response) {
						$('.search-results-main-container').html(response.template);
	                    if (typeof response.data != 'undefined' && response.data.status == 'success') {
	                       	$('.list-container').removeClass('hide');
	                        $('#search-results-main-container').html(response.data.listing_html);
	                        $('.full-cnt-loader').addClass('hidden');

	                        if(typeof successCallback != "undefined")
	                        {
	                            successCallback();
	                        }

	                    }
	                }
	            },
	            error: function(error, response) {

	                if(typeof errorCallback != "undefined")
	                {
	                    errorCallback();
	                }
	            }
	        });
	    }
    </script>

@stop

@section('js_script')
    <script type="text/javascript" src="/js/site/job.js"></script>
@stop