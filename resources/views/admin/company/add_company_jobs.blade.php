@extends('admin.index')

@section('content')
<?php
    $initialize_pincode_maxlength_validation = false;
    $india_value = array_search('India',\CommonHelper::countries());
?>
    <div class="container">
        <div class="toolbar row">
            <div class="col-sm-12">
                <div class="page-header">
                    <h1>Jobs<small>Add Company Jobs</small></h1>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 m_t_25">
                <!-- start: FORM WIZARD PANEL -->
                <div class="panel panel-white">
                    <div class="panel-body">
                        @include('admin.partials.add_company_job')
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop
@section('js_script')    
    <script type="text/javascript">
        $('.select2-select').select2({
            closeOnSelect: false
        });
    </script>
    <script type="text/javascript" src="/js/site/job.js"></script>
@stop