@extends('admin.index')

@section('content')

<div class="main-wrapper">
    <div class="main-container1">
        <!-- start: PAGE -->
        <div class="main-content">
            <!-- /.modal -->
            <!-- end: SPANEL CONFIGURATION MODAL FORM -->
            <div class="container">
                <!-- start: PAGE HEADER -->
                <!-- start: TOOLBAR -->
                <div class="toolbar row">
                    <div class="col-sm-6 hidden-xs">
                        <div class="page-header">
                            <h1>Documents Downloaded List<small>A list all downloaded documents by company</small></h1>
                        </div>
                    </div>
                </div>

                <!-- filter -->
                 <div class="row search-results-main-container m-t-15">
                    <div class="col-md-12">
                        <div class="panel panel-white">
                            <div class="panel-body">
                                <?php 
                                    $data_array = $document_list->toArray();
                                ?>
                                @if(isset($data_array['data']) && !empty($data_array['data']))
                                
                                    <div class="panel-heading">
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-6 paginator-content  pull-right no-margin">
                                                <ul id="top-user-paginator" class="pagination">
                                                    {!! $document_list->render() !!}
                                                </ul>
                                            </div>
                                            
                                            <div class="col-xs-12 col-sm-6 pull-left search-count-content">
                                                Showing {{$data_array['from']}} - {{$data_array['to']}} of {{$data_array['total']}} Results
                                            </div>
                                        </div>
                                    </div>
                                                
                                <div class="full-cnt-loader hidden">
                                    <div class="" style="text-align: center;padding-top:20px;padding-bottom:20px; ">
                                        <i class="fa fa-spinner fa-spin fa-4" aria-hidden="true" style="font-size: 7em;"></i>
                                    </div>
                                </div>      

                                @foreach($data_array['data'] as $data)  
                                <div class="seafarer-listing-container">
                                <div class="panel">
                                <div class="panel-heading border-light partition-blue">
                                <h5 class="panel-title text-bold header-link-text p-5-5">
                                    <a href="{{Route('admin.view.company.id',$data['requester']['id'])}}">
                                        {{isset($data['requester']['company_registration_detail']['company_name']) ? $data['requester']['company_registration_detail']['company_name'] : ''}}
                                    </a>
                                </h5>
                                </div>
                                                    
                                <div class="panel-body">
                                <div class="row">
                                <div class="col-lg-3 col-md-5 col-sm-5" id="user-profile-pic" style="height: 200px;width: 200px; margin-left: 15px;">
                                    <a>
                                        <div class="company_profile_pic">
                                            <img src="/{{ isset($data['profile_pic']) ? env('COMPANY_LOGO_PATH')."/".$data['id']."/".$data['profile_pic'] : 'images/user_default_image.png'}}" >
                                        </div>
                                    </a>
                                </div>
                                <div class="col-lg-9 col-md-7 col-sm-7">
                                    <div class="row">
                                        <div class="col-sm-4 sm-t-20">
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <div class="header-tag">
                                                        Requester Information
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="user-basic-details">
                                                <div class="row">
                                                    <div class="col-xs-6 col-sm-12">
                                                        <dl>
                                                            <dt> Company Name : </dt>
                                                            <dd class="margin-bottom-5px">
                                                                {{isset($data['requester']['company_registration_detail']['company_name']) && !empty(isset($data['requester']['company_registration_detail']['company_name'])) ? $data['requester']['company_registration_detail']['company_name'] : '-'}}
                                                            </dd>
                                                            <dt> Email : </dt>
                                                            <dd class="margin-bottom-5px make-word-wrap-break">
                                                                {{isset($data['requester']['company_registration_detail']['email']) && !empty($data['requester']['company_registration_detail']['email']) ? $data['requester']['company_registration_detail']['email'] : '-'}}
                                                            </dd>
                                                            <dt> Contact Number : </dt>
                                                            <dd class="margin-bottom-5px">
                                                                {{isset($data['requester']['company_registration_detail']['contact_number']) && !empty(isset($data['requester']['company_registration_detail']['contact_number'])) ? $data['requester']['company_registration_detail']['contact_number'] : '-'}}
                                                            </dd>
                                                            <dt> Contact Person Name :</dt>
                                                            <dd class="margin-bottom-5px">
                                                                {{isset($data['requester']['company_registration_detail']['contact_person']) ? $data['requester']['company_registration_detail']['contact_person'] : '-'}} 
                                                                
                                                            </dd>
                                                            <dt> Contact Person Number :</dt>
                                                            <dd class="margin-bottom-5px">
                                                                {{isset($data['requester']['company_registration_detail']['contact_number']) ? $data['requester']['company_registration_detail']['contact_number'] : '-'}}
                                                            </dd>
                                                            <dt> Contact Person Email : </dt>
                                                            <dd class="margin-bottom-5px">
                                                                {{isset($data['requester']['company_registration_detail']['contact_email']) ? $data['requester']['company_registration_detail']['contact_email'] : '-'}}  
                                                            </dd>
                                                        </dl>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="col-sm-4">
                                            
                                            <div id="user-professional-details">
                                                <div class="row">
                                                    <div class="col-xs-12">
                                                        <div class="header-tag">
                                                            Document Downloaded Information
                                                        </div>
                                                    </div>
                                                </div>
                                                <dl>
                                                    <dt> Documents List : </dt>
                                                        <dd class="margin-bottom-5px">
                                                            {{ isset($data['documents_list']) ? strtoupper($data['documents_list']) : '-'}}
                                                        </dd>
                                                    <dt>
                                                </dl>
                                            </div>
                                        </div>

                                        <div class="col-sm-4">
                                                <div class="header-tag">
                                                    Seafarer Information
                                                </div>
                                                <div id="user-professional-details">
                                                    <dl>
                                                        <dt> Name :</dt>
                                                        <dd class="margin-bottom-5px">
                                                            {{isset($data['owner']['first_name']) ? $data['owner']['first_name'] : '-'}} 
                                                            
                                                        </dd>
                                                        <dt> Email : </dt>
                                                        <dd class="margin-bottom-5px">
                                                            {{isset($data['owner']['email']) ? $data['owner']['email'] : '-'}}  
                                                        </dd>
                                                        <dt> Number :</dt>
                                                        <dd class="margin-bottom-5px">
                                                            {{isset($data['owner']['mobile']) ? $data['owner']['mobile'] : '-'}}  
                                                        </dd>
                                                    </dl>
                                                </div>
                                        </div>
                                        
                                    </div>
                                </div>
                                                    
                                </div>
                                </div>
                                </div>
                                                    
                                </div>
                                @endforeach
                                @else
                                    <div class="no-results-found">No results found. Try again with different search criteria.</div>
                                @endif
                                @if(isset($data_array['data']) && !empty($data_array['data']))
                                
                                    <div class="panel-heading">
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-6 paginator-content  pull-right no-margin">
                                                <ul id="top-user-paginator" class="pagination">
                                                    {!! $document_list->render() !!}
                                                </ul>
                                            </div>
                                            
                                            <div class="col-xs-12 col-sm-6 pull-left search-count-content">
                                                Showing {{$data_array['from']}} - {{$data_array['to']}} of {{$data_array['total']}} Results
                                            </div>
                                        </div>
                                    </div>
                                @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end: PAGE CONTENT-->
            </div>
            <div class="subviews">
                <div class="subviews-container"></div>
            </div>
        </div>
        <!-- end: PAGE -->
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        History = window.History;

        History.Adapter.bind(window,'statechange', function() {

            var State = History.getState();

            fetchSearchResults(State.url);
        });
    });

    function fetchSearchResults(url)
    {
        window.location.href = url;
    }
</script>

@stop

