@extends('admin.index')

@section('content')

<div class="main-wrapper">
	<div class="main-container1">
				<!-- start: PAGE -->
				<div class="main-content">
					<!-- start: PANEL CONFIGURATION MODAL FORM -->
					<div class="modal fade" id="panel-config" tabindex="-1" role="dialog" aria-hidden="true">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
										&times;
									</button>
									<h4 class="modal-title">Panel Configuration</h4>
								</div>
								<div class="modal-body">
									Here will be a configuration form
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-default" data-dismiss="modal">
										Close
									</button>
									<button type="button" class="btn btn-primary">
										Save changes
									</button>
								</div>
							</div>
							<!-- /.modal-content -->
						</div>
						<!-- /.modal-dialog -->
					</div>
					<!-- /.modal -->
					<!-- end: SPANEL CONFIGURATION MODAL FORM -->
					<div class="container">
						<!-- start: PAGE HEADER -->
						<!-- start: TOOLBAR -->
						<div class="toolbar row">
							<div class="col-sm-6 hidden-xs">
								<div class="page-header">
									<h1>Company <small>A list of all companies</small></h1>
								</div>
							</div>
						</div>

						<!-- filter -->
						<form class="filter-search-form m_l_10">
							<div class="row m_t_25">
								<div class="col-md-12">
									<div class="panel panel-white">
										<div class="panel-body">
											<div class="row">
												<div class="col-sm-3">
					                                <div class="form-group">
					                                    <label>Company Name </label>
					                                    <input type="text" class="form-control" name="company_name" id="company_name" value="{{isset($data['company_name']) ? $data['company_name'] : ''}}" placeholder="Enter Company Name">
					                                </div>
					                            </div>    
												<div class="col-sm-3">
					                                <div class="form-group">
					                                    <label>Company Type </label>
					                                    <select id="company_type" name="company_type" class="form-control">
					                                        <option value=>Select Type</option>

					                                            @foreach(\CommonHelper::company_type() as $r_index => $type)
					                                                <option value="{{$r_index}}" {{ isset($data['company_type']) ? $data['company_type'] == $r_index ? 'selected' : '' : ''}}>{{$type}} </option>
					                                            @endforeach
					                                    </select>
					                                </div>
					                            </div>
					                            <div class="col-sm-3">
					                                <div class="form-group">
					                                    <label>State </label>
					                                    <select id="state" name="state" class="form-control">
					                                        <option value=''>Select State</option>
						                                    @foreach($state as $s_index => $state_list)
				                                                <option value="{{$state_list['id']}}" {{ isset($data['state']) ? $data['state'] == $state_list['id'] ? 'selected' : '' : ''}}>{{$state_list['name']}} </option>
				                                            @endforeach
				                                        </select>
					                                </div>
					                            </div>
					                            <div class="col-sm-3">
					                                <div class="form-group">
					                                    <label>City </label>
					                                    <select id="city" name="city" class="form-control">
					                                        <option value=>Select City</option>
				                                        </select>
					                                </div>
					                            </div>
					                        </div>
					                        <div class="row">
					                        	<div class="col-xs-12 col-sm-3">
													<div class="search-count-content m_t_25">
														<a type="button" id="download_excel" class="btn btn-success" data-type='company'>Download Excel</a>
													</div>
												</div>
					                            <div class="col-sm-9">
					                                <div class="section_cta text-right m_t_25 job-btn-container">
					                                    <button type="button" class="btn btn-danger coss-inverse-btn search-reset-btn" id="seafarerResetButton">Reset</button>
					                                    <button type="submit" data-style="zoom-in" class="btn btn-info coss-primary-btn search-post-btn ladda-button candidate-filter-search-button" id="candidate-filter-search-button"><i class="fa fa-search search-icon" aria-hidden="true"></i>Search</button>
					                                </div>
					                            </div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</form>

						 <div class="row search-results-main-container" >
							<div class="col-md-12">
								<div class="panel panel-white">
									<div class="panel-body">
										<?php 
											$data_array = $company_data->toArray();
										?>
										@if(isset($data_array['data']) && !empty($data_array['data']))
										
											<div class="panel-heading">
												<div class="row">
													<div class="col-xs-12 col-sm-6 paginator-content  pull-right no-margin">
														<ul id="top-user-paginator" class="pagination">
															{!! $company_data->render() !!}
														</ul>
													</div>
													
													<div class="col-xs-12 col-sm-6 pull-left search-count-content">
														Showing {{$data_array['from']}} - {{$data_array['to']}} of {{$data_array['total']}} Results
													</div>
												</div>
											</div>
														
										<div class="full-cnt-loader hidden">
										    <div class="" style="text-align: center;padding-top:20px;padding-bottom:20px; ">
										        <i class="fa fa-spinner fa-spin fa-4" aria-hidden="true" style="font-size: 7em;"></i>
										    </div>
										</div>		

										@foreach($data_array['data'] as $data)	
										<div class="seafarer-listing-container">
										<div class="panel">
										<div class="panel-heading border-light partition-blue">
										<h5 class="panel-title text-bold header-link-text p-5-5">
											<a href="{{Route('admin.view.company.id',$data['id'])}}">
												{{isset($data['company_registration_detail']['company_name']) ? $data['company_registration_detail']['company_name'] : ''}}
											</a>
										</h5>
										<ul class="panel-heading-tabs border-light">
										<li class="panel-tools">
											<div class="dropdown">
												<a data-toggle="dropdown" class="btn btn-xs dropdown-toggle btn-transparent-grey">
													<i class="fa fa-cog"></i>
												</a>
												<ul class="dropdown-menu dropdown-light pull-right" role="menu">
													<li>
														<a href="{{Route('admin.view.company.id',$data['id'])}}" data-type="user">
															View
														</a>
													</li>
													<li>
														<a href="{{Route('admin.edit.company',$data['id'])}}">
															Edit
														</a>
													</li>
													<li>
														<a class="change-status-user cursor-pointer {{ $data['status'] == '0' ? 'hide' : ''}}" data-status='deactive' data-id={{$data['id']}} id="deactive-{{$data['id']}}">
															Deactivate
														</a>
													</li>
													<li>
														<a class="change-status-user cursor-pointer {{ $data['status'] != '0' ? 'hide' : ''}}"" data-status='active' data-id={{$data['id']}} id="active-{{$data['id']}}">
															Activate
														</a>
													</li>
												</ul>
											</div>
										</li>
										</ul>

										</div>
															
										<div class="panel-body">
										<div class="row">
										<div class="col-lg-3 col-md-5 col-sm-5" id="user-profile-pic" style="height: 200px;width: 200px; margin-left: 15px;">
											<a>
												<div class="company_profile_pic">
				                					<img src="/{{ isset($data['profile_pic']) ? env('COMPANY_LOGO_PATH')."/".$data['id']."/".$data['profile_pic'] : 'images/user_default_image.png'}}" >
				                				</div>
	                            			</a>
										</div>
										<div class="col-lg-9 col-md-7 col-sm-7">
											<div class="row">
												<div class="col-sm-4 sm-t-20">
													<div class="row">
														<div class="col-xs-12">
															<div class="header-tag">
																Basic Information
															</div>
														</div>
													</div>
													<div id="user-basic-details">
														<div class="row">
															<div class="col-xs-6 col-sm-12">
																<dl>
																	<dt> Company Name : </dt>
																	<dd class="margin-bottom-5px">
																		{{isset($data['company_registration_detail']['company_name']) && !empty(isset($data['company_registration_detail']['company_name'])) ? $data['company_registration_detail']['company_name'] : '-'}}
																	</dd>
																	<dt> Email : 
																		@if($data['is_email_verified'] == '1')
																			<i class="fa fa-check-circle green f-15" aria-hidden="true" title="Email Verified"></i>
																		@else
																			<i class="fa fa-minus-circle red f-15" aria-hidden="true" title="Email Unverified"></i>
																		@endif
																	</dt>
																	<dd class="margin-bottom-5px make-word-wrap-break">
																		{{isset($data['company_registration_detail']['company_detail']['company_email']) && !empty($data['company_registration_detail']['company_detail']['company_email']) ? $data['company_registration_detail']['company_detail']['company_email'] : '-'}}
														     		</dd>
														     		<dt> Contact Number : 
														     			@if($data['is_mob_verified'] == '1')
														     				<i class="fa fa-check-circle green f-15" aria-hidden="true" title="Mobile Verified"></i>
														     			@else
																			<i class="fa fa-minus-circle red f-15" aria-hidden="true" title="Mobile Unverified"></i>
														     			@endif
														     		</dt>
																	<dd class="margin-bottom-5px">
																		{{isset($data['company_registration_detail']['contact_number']) && !empty(isset($data['company_registration_detail']['contact_number'])) ? $data['company_registration_detail']['contact_number'] : '-'}}
														     		</dd>
														     		<dt> Registered On :</dt>
																	<dd class="margin-bottom-5px">
																		{{ isset($data['created_at']) && !empty($data['created_at']) ? date('d-m-Y H:i:s', strtotime($data['created_at']))  : '-'}}
																	</dd>
																</dl>
															</div>
														</div>
													</div>
												</div>
												
												<div class="col-sm-4">
													
													<div id="user-professional-details">
														<div class="row">
															<div class="col-xs-12">
																<div class="header-tag">
																	Company Details
																</div>
															</div>
														</div>
														<dl>
												     		<dt> Company Type : </dt>
																<dd class="margin-bottom-5px">
																	@foreach( \CommonHelper::company_type() as $c_index => $type)
																		{{ !empty($data['company_registration_detail']['company_detail']['company_type']) ? $data['company_registration_detail']['company_detail']['company_type'] == $c_index ? $type : '' : ''  }}
																	@endforeach
													     		</dd>
												     		<dt>
															<dt> Company Description :</dt>
															<dd class="margin-bottom-5px">
																{{ isset($data['company_registration_detail']['company_detail']['company_description']) ? substr($data['company_registration_detail']['company_detail']['company_description'],0,140)." ..." : '-'}}
															</dd>
														</dl>
													</div>
												</div>

												<div class="col-sm-4">
														<div class="header-tag">
															Contact Details
														</div>
														<div id="user-professional-details">
															<dl>
																<dt> Contact Person Name :</dt>
																<dd class="margin-bottom-5px">
																	{{isset($data['company_registration_detail']['contact_person']) ? $data['company_registration_detail']['contact_person'] : '-'}} 
																	
																</dd>
																<dt> Contact Person Number :</dt>
																<dd class="margin-bottom-5px">
																	{{isset($data['company_registration_detail']['contact_number']) ? $data['company_registration_detail']['contact_number'] : '-'}}  
																</dd>
																<dt> Contact Person Email : </dt>
																<dd class="margin-bottom-5px">
																	{{isset($data['company_registration_detail']['contact_email']) ? $data['company_registration_detail']['contact_email'] : '-'}}  
																</dd>
															</dl>
														</div>
												</div>
												
											</div>
										</div>
															
										</div>
										</div>
										</div>
															
										</div>
										@endforeach
										@else
											<div class="no-results-found">No results found. Try again with different search criteria.</div>
										@endif
										@if(isset($data_array['data']) && !empty($data_array['data']))
										
											<div class="panel-heading">
												<div class="row">
													<div class="col-xs-12 col-sm-6 paginator-content  pull-right no-margin">
														<ul id="top-user-paginator" class="pagination">
															{!! $company_data->render() !!}
														</ul>
													</div>
													
													<div class="col-xs-12 col-sm-6 pull-left search-count-content">
														Showing {{$data_array['from']}} - {{$data_array['to']}} of {{$data_array['total']}} Results
													</div>
												</div>
											</div>
										@endif
										</div>
									</div>
								</div>
							</div>
						</div>
						<!-- end: PAGE CONTENT-->
					</div>
					<div class="subviews">
						<div class="subviews-container"></div>
					</div>
				</div>
				<!-- end: PAGE -->
			</div>
</div>

	<script type="text/javascript">
        
        var searchAjaxCall = "undefined";

        $(document).ready(function(){

            //init history plugin to perform search without page refresh
            // Establish Variables
            History = window.History; // Note: We are using a capital H instead of a lower h

            // Bind to State Change
            // Note: We are using statechange instead of popstate
			
            History.Adapter.bind(window,'statechange', function() {

                // Log the State
                var State = History.getState(); // Note: We are using History.getState() instead of event.state

                //start search (defined)
                fetchSearchResults(State.url, 
                                    function() {
                                        
                                    }, function() {

                                    });

            });

            

        });

        function fetchSearchResults(url, successCallback, errorCallback)
	    {
	    	
	    	var temp_current_state_url = url.split('?');
            if(temp_current_state_url.length > 1 && temp_current_state_url[1] != ''){
                var parameters = temp_current_state_url[1].split('&');

                var dropdown_elements = ['company_type','state','city'];

                for(var i=0; i<parameters.length; i++){

                    var param_array = parameters[i].split('=');
                    if($.inArray(param_array[0],dropdown_elements) > -1){
                        $('select[name="'+param_array[0]+'"]').val(param_array[1]);
                    }else if($.inArray(param_array[0],['availability_from','availability_to','first_name']) > -1){
                        $('input[name="'+param_array[0]+'"]').val(param_array[1]);
                    }

                }
            } else {
             	$(':input','#candidate-filter-form')
	                .not(':button, :submit, :reset, :hidden')
	                .val('')
	                .removeAttr('checked')
	                .removeAttr('selected');
	             }

	        if(searchAjaxCall != "undefined")
	        {
	            searchAjaxCall.abort();
	            searchAjaxCall = "undefined";
	        }

	        $('.seafarer-listing-container').addClass('hide');
	        $('.full-cnt-loader').removeClass('hidden');


	        searchAjaxCall = $.ajax({
	            url: url,
	            cache: false,
	            dataType: 'json',
	            statusCode: {
	                200: function(response) {
						
	                    if (typeof response.template != 'undefined') {
	                       
                        	$('.seafarer-listing-container').removeClass('hide');
	                        $('.search-results-main-container').html(response.template);
	                        $('.full-cnt-loader').addClass('hidden');

	                        $('.search-results-main-container').html(response.template);

	                        if(typeof successCallback != "undefined")
	                        {
	                            successCallback();
	                        }

	                    }
	                }
	            },
	            error: function(error, response) {

	                if(typeof errorCallback != "undefined")
	                {
	                    errorCallback();
	                }
	            }
	        });
	    }
    </script>

@stop