@extends('admin.index')

@section('content')

<div class="main-wrapper">
	<div class="main-container1">
				<!-- start: PAGE -->
				<div class="main-content">
					<!-- start: PANEL CONFIGURATION MODAL FORM -->
					<div class="modal fade" id="panel-config" tabindex="-1" role="dialog" aria-hidden="true">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
										&times;
									</button>
									<h4 class="modal-title">Panel Configuration</h4>
								</div>
								<div class="modal-body">
									Here will be a configuration form
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-default" data-dismiss="modal">
										Close
									</button>
									<button type="button" class="btn btn-primary">
										Save changes
									</button>
								</div>
							</div>
							<!-- /.modal-content -->
						</div>
						<!-- /.modal-dialog -->
					</div>
					<!-- /.modal -->
					<!-- end: SPANEL CONFIGURATION MODAL FORM -->
					<div class="container">
						<!-- start: PAGE HEADER -->
						<!-- start: TOOLBAR -->
						<div class="toolbar row">
							<div class="col-sm-6 hidden-xs">
								<div class="page-header">
									<h1>Company <small>A list of all companies</small></h1>
								</div>
							</div>
						</div>

						<!-- filter -->
						<form class="filter-search-form m_l_10">
							<div class="row m_t_25">
								<div class="col-md-12">
									<div class="panel panel-white">
										<div class="panel-body">
											<div class="row">
												<div class="col-sm-3">
					                                <div class="form-group">
					                                    <label>Subscription Plan </label>
					                                    <select id="subscription_id" name="subscription_id" class="form-control">
					                                        <option value=>Select Plan</option>

					                                            @foreach($all_subscriptions as $r_index => $type)
					                                                <option value="{{$type['id']}}" {{ isset($type['title']) ? $type['title'] == $r_index ? 'selected' : '' : ''}}>{{$type['title']}} </option>
					                                            @endforeach
					                                    </select>
					                                </div>
					                            </div>
												<div class="col-sm-3">
					                                <div class="form-group">
					                                    <label>Company Name </label>
					                                    <input type="text" class="form-control" name="company_name" id="company_name" value="{{isset($data['company_name']) ? $data['company_name'] : ''}}">
					                                </div>
					                            </div>
					                            <div class="col-sm-3">
					                                <div class="form-group">
					                                    <label>Valid From </label>
					                                    <input type="text" class="form-control issue-datepicker" name="valid_from" id="valid_from" value="{{isset($data['valid_from']) ? $data['valid_from'] : ''}}" placeholder="dd-mm-yyyy">
					                                </div>
					                            </div>
					                            <div class="col-sm-3">
					                                <div class="form-group">
					                                    <label>Valid To </label>
					                                    <input type="text" class="form-control datepicker" name="valid_to" id="valid_to" value="{{isset($data['valid_to']) ? $data['valid_to'] : ''}}" placeholder="dd-mm-yyyy">
					                                </div>
					                            </div>
												
					                        </div>
					                        <div class="row">
					                            <div class="col-sm-12">
					                                <div class="section_cta text-right m_t_25 job-btn-container">
					                                    <button type="button" class="btn btn-danger coss-inverse-btn search-reset-btn" id="seafarerResetButton">Reset</button>
					                                    <button type="submit" data-style="zoom-in" class="btn btn-info coss-primary-btn search-post-btn ladda-button candidate-filter-search-button" id="candidate-filter-search-button"><i class="fa fa-search search-icon" aria-hidden="true"></i>Search</button>
					                                </div>
					                            </div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</form>

						 <div class="row search-results-main-container" >
							<div class="col-md-12">
								<div class="panel panel-white">
									<div class="panel-body">
										<?php 
											$data_array = $company_data->toArray();
										?>
										
										@if(isset($data_array['data']) && !empty($data_array['data']))
										
											<div class="panel-heading">
												<div class="row">
													<div class="col-xs-12">
														<div class="col-xs-6 paginator-content pull-right no-margin">
															<ul id="top-user-paginator" class="pagination">
																{!! $company_data->render() !!}
															</ul>
														</div>
														
														<div class="pull-left search-count-content">
															Showing {{$data_array['from']}} - {{$data_array['to']}} of {{$data_array['total']}} Results
														</div>
													</div>
												</div>
											</div>
														
												
										@foreach($data_array['data'] as $data)	
										<div class="seafarer-listing-container">
										<div class="panel">
										<div class="panel-heading border-light partition-blue">
										<h5 class="panel-title text-bold header-link-text p-5-5">
											<a href="">
												{{isset($data['company_registration_detail']['company_name']) ? $data['company_registration_detail']['company_name'] : ''}}
											</a>
										</h5>
										<ul class="panel-heading-tabs border-light">
										<!-- <li class="panel-tools">
											<div class="dropdown">
												<a data-toggle="dropdown" class="btn btn-xs dropdown-toggle btn-transparent-grey">
													<i class="fa fa-cog"></i>
												</a>
												<ul class="dropdown-menu dropdown-light pull-right" role="menu">
													<li>
														<a href="{{Route('admin.view.company.id',$data['id'])}}" data-type="user">
															View
														</a>
													</li>
													<li>
														<a href="{{Route('admin.edit.company',$data['id'])}}">
															Edit
														</a>
													</li>
													<li>
														<a class="change-status-user cursor-pointer {{ $data['status'] == '0' ? 'hide' : ''}}" data-status='deactive' data-id={{$data['id']}} id="deactive-{{$data['id']}}">
															Deactivate
														</a>
													</li>
													<li>
														<a class="change-status-user cursor-pointer {{ $data['status'] != '0' ? 'hide' : ''}}"" data-status='active' data-id={{$data['id']}} id="active-{{$data['id']}}">
															Activate
														</a>
													</li>
												</ul>
											</div>
										</li> -->
										</ul>

										</div>
															
										<div class="panel-body">
										<div class="row">
											<div class="col-lg-3 col-md-5 col-sm-5" id="user-profile-pic" style="height: 200px;width: 200px; margin-left: 15px;">
												<a>
													<div class="company_profile_pic">
					                					<img src="/{{ isset($data['profile_pic']) ? env('COMPANY_LOGO_PATH')."/".$data['id']."/".$data['profile_pic'] : 'images/user_default_image.png'}}" >
					                				</div>
		                            			</a>
											</div>
											<div class="col-lg-9 col-md-7 col-sm-7">
												<div class="row">
												<div class="col-sm-4 sm-t-20">
													<div class="row">
														<div class="col-xs-12">
															<div class="header-tag">
																Basic Information
															</div>
														</div>
													</div>
													<div id="user-basic-details">
														<div class="row">
															<div class="col-xs-6 col-sm-7">
																<dl>
																	<dt> Company Name : </dt>
																	<dd class="margin-bottom-5px">
																		{{isset($data['company_registration_detail']['company_name']) ? $data['company_registration_detail']['company_name'] : ''}}
																	</dd>
																	<dt> Email : </dt>
																	<dd class="margin-bottom-5px make-word-wrap-break">
																		{{isset($data['company_detail']['company_email']) ? $data['company_detail']['company_email'] : ''}}
														     		</dd>
														     		<dt> Contact Number : </dt>
																	<dd class="margin-bottom-5px">
																		{{isset($data['company_registration_detail']['contact_number']) ? $data['company_registration_detail']['contact_number'] : ''}}
														     		</dd>
																</dl>
															</div>
														</div>
													</div>
												</div>
											
											@foreach($data['company_subscriptions'] as $company_data1)

												@if($company_data1['status'] == 1)
													<div class="col-sm-4">
														<div id="user-professional-details">
															<div class="row">
																<div class="col-xs-12">
																	<div class="header-tag">
																		Subscription Details
																	</div>
																</div>
															</div>
															<dl>
													     		<dt> Subscription ID : </dt>
																	<dd class="margin-bottom-5px">
																		{{isset($company_data1['subscription_id']) ? $company_data1['subscription_id'] : ''}}
														     		</dd>
													     		<dt>
																<dt> Subscription Title :</dt>
																<dd class="margin-bottom-5px">
																	{{isset($company_data1['subscription_feature']['title']) ? $company_data1['subscription_feature']['title'] : ''}}
																</dd>
															</dl>
														</div>
													</div>

													<div class="col-sm-4">
														<div class="header-tag">
															Validity Details
														</div>
														<div id="user-professional-details">
															<dl>
																<dt> Valid From :</dt>
																<dd class="margin-bottom-5px">
																	{{isset($company_data1['valid_from']) ? date('d-m-Y',strtotime($company_data1['valid_from'])) : ''}}
																</dd>
																<dt> Valid To :</dt>
																<dd class="margin-bottom-5px">
																	{{isset($company_data1['valid_to']) ? date('d-m-Y',strtotime($company_data1['valid_to'])) : ''}}
																</dd>
																<dt> Status : </dt>
																<dd class="margin-bottom-5px">
																	{{isset($company_data1['status']) ? $company_data1['status'] == '1' ? 'Active' : 'Deactive' : ''}}
																</dd>
															</dl>
														</div>
													</div>
												@endif
											@endforeach
											</div>
											</div>
															
										</div>
										</div>
										</div>
															
										</div>
										@endforeach
										@else
											<div class="no-results-found">No results found. Try again with different search criteria.</div>
										@endif
										
										@if(isset($data_array['data']) && !empty($data_array['data']))
										
											<div class="panel-heading">
												<div class="row">
													<div class="col-xs-12">
														<div class="col-xs-6 paginator-content pull-right no-margin">
															<ul id="top-user-paginator" class="pagination">
																{!! $company_data->render() !!}
															</ul>
														</div>
														
														<div class="pull-left search-count-content">
															Showing {{$data_array['from']}} - {{$data_array['to']}} of {{$data_array['total']}} Results
														</div>
													</div>
												</div>
											</div>
										@endif
									</div>
								</div>
							</div>
						</div>
						<!-- end: PAGE CONTENT-->
					</div>
					<div class="subviews">
						<div class="subviews-container"></div>
					</div>
				</div>
				<!-- end: PAGE -->
			</div>
</div>

	<script type="text/javascript">
        
        var searchAjaxCall = "undefined";

        $(document).ready(function(){

            //init history plugin to perform search without page refresh
            // Establish Variables
            History = window.History; // Note: We are using a capital H instead of a lower h

            // Bind to State Change
            // Note: We are using statechange instead of popstate
			
            History.Adapter.bind(window,'statechange', function() {

                // Log the State
                var State = History.getState(); // Note: We are using History.getState() instead of event.state

                //start search (defined)
                fetchSearchResults(State.url, 
                                    function() {
                                        
                                    }, function() {

                                    });

            });

            

        });

        function fetchSearchResults(url, successCallback, errorCallback)
	    {
	    	
	    	var temp_current_state_url = url.split('?');
            if(temp_current_state_url.length > 1 && temp_current_state_url[1] != ''){
                var parameters = temp_current_state_url[1].split('&');

                var dropdown_elements = ['subscription_id'];

                for(var i=0; i<parameters.length; i++){

                    var param_array = parameters[i].split('=');
                    if($.inArray(param_array[0],dropdown_elements) > -1){
                        $('select[name="'+param_array[0]+'"]').val(param_array[1]);
                    }else if($.inArray(param_array[0],['company_name','valid_from','valid_to']) > -1){
                        $('input[name="'+param_array[0]+'"]').val(param_array[1]);
                    }

                }
            } else {
             	$(':input','#candidate-filter-form')
	                .not(':button, :submit, :reset, :hidden')
	                .val('')
	                .removeAttr('checked')
	                .removeAttr('selected');
	             }

	        if(searchAjaxCall != "undefined")
	        {
	            searchAjaxCall.abort();
	            searchAjaxCall = "undefined";
	        }

	        $('.full-cnt-loader').removeClass('hidden');

	        searchAjaxCall = $.ajax({
	            url: url,
	            dataType: 'json',
	            statusCode: {
	                200: function(response) {
						$('.search-results-main-container').html(response.template);
	                    if (typeof response.data != 'undefined' && response.data.status == 'success') {
	                       
	                        $('#search-results-main-container').html(response.data.listing_html);
	                        $('.full-cnt-loader').addClass('hidden');

	                        if(typeof successCallback != "undefined")
	                        {
	                            successCallback();
	                        }

	                    }
	                }
	            },
	            error: function(error, response) {

	                if(typeof errorCallback != "undefined")
	                {
	                    errorCallback();
	                }
	            }
	        });
	    }
    </script>

@stop