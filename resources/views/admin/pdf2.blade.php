<!DOCTYPE html>
<html lang="en">
<?php
$india_value = array_search('India',\CommonHelper::countries());
if(isset($data[0]['professional_detail']['current_rank'])){
    $required_fields = \CommonHelper::rank_required_fields()[$data[0]['professional_detail']['current_rank']];
}
if(isset($data[0]['document_permissions']) && !empty($data[0]['document_permissions'])){
    $document_permissions = $data[0]['document_permissions'];
}
if(Auth::check()){
    $registered_as = Auth::user()->registered_as;
}
$gender = "M";
if($data[0]['gender'] == 'F'){
    $gender = 'F';
}
?>
<head>
    @include('layouts/pdf/header')
    <style>
        thead { display: table-header-group }
    tfoot { display: table-row-group }
    tr { page-break-inside: avoid }
    .well {
            margin-bottom: 8px;
            /* background-color: #f5f5f5; */
            border: none;
            border-top: 1px solid #e3e3e3;
            border-bottom: 1px solid #e3e3e3;
        }
        .font-12{
            color: #898989 !important;
        }
        #chart-area{
            font-size: 15px !important;
        }
        .m-0{
            margin: 0px !important;
        }
        .ml-2{
            margin-left: 18px !important;
        }
        .pad-8{
            padding: 8px;
        }
        .user-profile-pic{
            border: solid 1px #d4cdcd;
            height: 165px;
            width: 165px;
        }
        @media screen and (max-width: 760px) {
            .btn-success {
                padding: 1px 2px;
                font-size: 9px;
                line-height: 1.4;
                border-radius: 3px;
            }
            .fa-user-circle {
                display: none;
            }
        }
        @media screen and (max-width: 992px) {
            .col-sm-7, .col-sm-5 {
                Width:100%;
                flex-direction: row;
            }
        }
        </style>
    <meta charset="UTF-8">
    <title>
    @if(isset($pageTitle) && !empty($pageTitle))
        {!! $pageTitle !!}
    @else
        {{env('APP_NAME')}}
    @endif
    </title>
    <meta name="description" content="{!! ($metaDescription) ?? '' !!}">
    <meta name="keywords" content="{!! ($metaKeywords) ?? '' !!}">
    <link rel="shortcut icon" type="image/x-icon" href="{{asset('public/images/favicon.ico')}}">
</head>
<body>
    @if((isset($is_resume) && $is_resume == true) || $pdfFormat == true)
        <div class="container">
            <div class="row bg-primary" style="display:flex" >
                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 bg-primary pt-20">
                    <div class="card-body top-borderradius text-center">
                        @if(isset($data[0]['photo']) && isset($data[0]['photo']['document_path']))
                            <img class="img-responsive img-circle center-block user-profile-pic" src="{{env('AWS_URL') . 'public/uploads/user_documents'}}/{{$data[0]['id']}}/photo/{{$data[0]['photo']['document_path']}}" alt="image">
                        @else
                            @if($gender == 'M')
                                <img class="img-responsive img-circle center-block user-profile-pic" src="{{url('public/assets/images/default-user-male.png')}}" alt="image">
                            @else
                                <img class="img-responsive img-circle center-block user-profile-pic" src="{{url('public/assets/images/defualt-user-female.png')}}" alt="image">
                            @endif
                        @endif
                    </div>
                    <h3 class="h3 font-trebuchet text-center">{{ isset($data[0]['title']) ? ucwords($data[0]['title']) : ''}} {{ isset($data[0]['first_name']) ? ucwords($data[0]['first_name']) : ''}} {{ isset($data[0]['PPMname']) ? ucwords($data[0]['PPMname']) : ''}} {{ isset($data[0]['PPLname']) ? ucwords($data[0]['PPLname']) : ''}}</h3>
                    <p class="font-trebuchet text-center">@foreach(\CommonHelper::new_rank() as $index => $category)
                            @foreach($category as $r_index => $rank)
                                {{ !empty($data[0]['professional_detail']['current_rank']) ? $data[0]['professional_detail']['current_rank'] == $r_index ? $rank : '' : ''}}
                            @endforeach
                        @endforeach
                    </p>
                    <hr />
                    <p class="font-trebuchet">Contact 1 : 
                        @if(isset($data[0]['country_code']) && !empty($data[0]['country_code']))
                            +{{ isset($data[0]['country_code']) ? $data[0]['country_code'] : '' }} 
                        @endif
                        {{ isset($data[0]['mobile']) ? $data[0]['mobile'] : '' }} </p>
                    @if(isset($data[0]['personal_detail']['landline']) && !empty($data[0]['personal_detail']['landline']))
                    <p class="font-trebuchet">Contact 2 : {{$data[0]['personal_detail']['landline']}} </p>
                    @endif
                    
                    <p class="font-trebuchet" style="word-break: break-all;">Email : {{ isset($data[0]['email']) ? $data[0]['email'] : ''}}</p>
                    <p class="font-trebuchet">Loc : {{ isset($data[0]['personal_detail']['city_id']) ? $data[0]['personal_detail']['pincode']['pincodes_cities'][0]['city']['name'] : (isset($data[0]['personal_detail']['city_text']) ? $data[0]['personal_detail']['city_text'] : '')}} / {{ isset($data[0]['personal_detail']['state_id']) ? $data[0]['personal_detail']['pincode']['pincodes_states'][0]['state']['name'] : (isset($data[0]['personal_detail']['state_text']) ? $data[0]['personal_detail']['state_text'] : '')}} / @if(isset($data[0]['passport_detail']['pass_country']) && !empty($data[0]['passport_detail']['pass_country']))
                            @foreach( \CommonHelper::countries() as $c_index => $country)
                                {{ isset($data[0]['passport_detail']['pass_country']) ? $data[0]['passport_detail']['pass_country'] == $c_index ? $country : '' : ''}}
                            @endforeach
                        @else
                            -
                        @endif
                    </p>
                     @if(isset($data[0]['personal_detail']['nearest_place']) && !empty($data[0]['personal_detail']['nearest_place']))
                        Nearest Airport : {{ $data[0]['personal_detail']['nearest_place']}}
                    @endif
                    <br />
                    <br />
                    <h4 class="h4 pt-20"><i class="fa fa-{{($gender=='M')?'male':(($gender=='F')?'female':'')}}" aria-hidden="true"></i> PERSONAL DETAILS</h4>
                    <hr />
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">Nationality : @foreach( \CommonHelper::countries() as $c_index => $country)
                                {{ isset($data[0]['personal_detail']) ? $data[0]['personal_detail']['nationality'] == $c_index ? $country : '' : ''}}
                            @endforeach
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">DOB : {{ isset($data[0]['personal_detail']) ? date('d-m-Y',strtotime($data[0]['personal_detail']['dob'])) : ''}}</div>
                        @if(isset($data[0]['personal_detail']['place_of_birth']) && !empty($data[0]['personal_detail']['place_of_birth']))
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">POB : {{ $data[0]['personal_detail']['place_of_birth']}}</div>
                        @endif
                        @if(isset($data[0]['gender']) && !empty($data[0]['gender']))
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">Gender : {{ $data[0]['gender'] == 'F' ? "Female" : "Male"}}</div>
                        @endif
                    </div>
                    <br />
                    <br />
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">Current Rank : 
                            @foreach(\CommonHelper::new_rank() as $index => $category)
                                @foreach($category as $r_index => $rank)
                                    {{ !empty($data[0]['professional_detail']['current_rank']) ? $data[0]['professional_detail']['current_rank'] == $r_index ? $rank : '' : ''}}
                                @endforeach
                            @endforeach
                        </div>
                        @php
                        $dataCocDe = "";
                        if(isset($data[0]['coc_detail'][0]['coc_grade'])){
                            $dataCocDe = $data[0]['coc_detail'][0]['coc_grade'];
                        }
                        @endphp
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">Grade : 
                            {{($data[0]['wkfr_detail']['wk_cop'] && $data[0]['wkfr_detail']['type'] ? strtoupper($data['wkfr_detail'][0]['wk_cop']).' '.$data[0]['wkfr_detail']['type'] : $dataCocDe) }}
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">Experience : {{ isset($data[0]['professional_detail']['years']) ? $data[0]['professional_detail']['years'] : '0'}} Y {{ isset($data[0]['professional_detail']['months']) && ($data[0]['professional_detail']['months'] != '') ? $data[0]['professional_detail']['months'] : '0' }} M</div>
                    </div>
                    <br />
                    <br />
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">Applied Rank : @foreach(\CommonHelper::new_rank() as $index => $category)
                                @foreach($category as $r_index => $rank)
                                    {{ !empty($data[0]['professional_detail']['applied_rank']) ? $data[0]['professional_detail']['applied_rank'] == $r_index ? $rank : '' : ''}}
                                @endforeach
                            @endforeach
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">Availability : {{isset($data[0]['professional_detail'])?((Carbon\Carbon::now())->lessThan(Carbon\Carbon::parse($data[0]['professional_detail']['availability']))?date('d-m-Y',strtotime($data[0]['professional_detail']['availability'])):'Immediate'):''}}
                        </div>
                    </div>
                    <br />
                    <br />
                    <h4 class="h4 pt-20"><i class="fa fa-id-badge" aria-hidden="true"></i> MY PROFILE</h4>
                    <hr />
                    <p>
                        {{$data[0]['professional_detail']['about_me']}}
                    </p>
                    <br />
                    <p>
                        <a href="{{route('share-profile', ['id' => $user_id])}}" class="btn btn-success">
                            <i class="fa fa-user-circle" aria-hidden="true"></i> Check out my profile
                        </a>
                    </p>

                </div>
                <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8 bg-white">
                        <h4 class="h4 pt-20"> <i class="text-primary fa fa-suitcase" aria-hidden="true"></i> TRAVELLING DOCUMENT</h4>
                        <hr class="hr" />
                        {{--<p>
                            Passport No : {{ isset($data[0]['passport_detail']['pass_number']) ? $data[0]['passport_detail']['pass_number'] : '-' }} - @foreach( \CommonHelper::countries() as $c_index => $country)
                                {{ isset($data[0]['passport_detail']) ? $data[0]['passport_detail']['pass_country'] == $c_index ? $country : '' : ''}}
                            @endforeach
                        </p>
                        <div class="row">
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6"><p>Place of Issue : {{ isset($data[0]['passport_detail']['place_of_issue']) ? $data[0]['passport_detail']['place_of_issue'] : '-' }}</p>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                <p>DOE : {{ isset($data[0]['passport_detail']['pass_issue_date']) ? date('d-m-Y', strtotime($data[0]['passport_detail']['pass_issue_date'])) : '-' }}</p>
                            </div>
                        </div>
                        {{ isset($data[0]['passport_detail']['us_visa']) ? ($data[0]['passport_detail']['us_visa']==1 ? 'Us Visa Date Of Expiry : '.(isset($data[0]['passport_detail']['us_visa_expiry_date']) ? date('d-m-Y',strtotime($data[0]['passport_detail']['us_visa_expiry_date'])) : '-'):''):'' }}
                        @php
                            $yf=(\App\UserWkfrDetail::whereUserId($user_id)->whereYellowFever(1)->pluck('yf_issue_date')->first())
                        @endphp
                        <p>{{($yf)?'YF Vaccination validity : '.date("d-m-Y",strtotime($yf)):''}}</p>--}}
                    <div class="table-responsive">
                        <table class="table table-striped">
                            {{-- <thead>
                                <tr>
                                    <th>Document</th>
                                    <th>Document Nos</th>
                                    <th>POI</th>
                                    <th>DOE</th>
                                </tr>
                            </thead> --}}
                            <tbody>
                                <tr>
                                    <td>
                                        <b>Passport :  </b>
                                        @foreach( \CommonHelper::countries() as $c_index => $country)
                                            {{ isset($data[0]['passport_detail']) ? $data[0]['passport_detail']['pass_country'] == $c_index ? $country : '' : ''}}
                                        @endforeach
                                    </td>
                                    <td>
                                        <b>Nos :  </b>{{ isset($data[0]['passport_detail']['pass_number']) ? $data[0]['passport_detail']['pass_number'] : '-' }}
                                    </td>
                                    <td>
                                        <b>DOE :  </b>{{ isset($data[0]['passport_detail']['pass_expiry_date']) ? date('d-m-Y', strtotime($data[0]['passport_detail']['pass_expiry_date'])) : '-' }}
                                    </td>
                                    <td>
                                        <b>POI :  </b>{{ isset($data[0]['passport_detail']['place_of_issue']) ? $data[0]['passport_detail']['place_of_issue'] : '-' }}
                                    </td>
                                </tr>
                                @if(isset($data[0]['visas']) && count($data[0]['visas']) > 0)
                                    @foreach($data[0]['visas'] as $visa)
                                        <tr>
                                            <td>
                                                <b>Visa :  </b>
                                                @foreach( \CommonHelper::countries() as $c_index => $country)
                                                {{ isset($visa['country_id']) ? $visa['country_id'] == $c_index ? $country : '' : ''}}
                                                @endforeach
                                            </td>
                                            <td>
                                                <b>Type :  </b> {{$visa['visa_type']}}
                                            </td>
                                            <td>
                                                <b>DOE :  </b>{{(isset($visa['visa_expiry_date']) ? date('d-m-Y',strtotime($visa['visa_expiry_date'])) : '-')}}
                                            </td>
                                            <td>
                                                
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                    {{-- <div class="row">
                    @if(isset($data[0]['passport_detail']['us_visa']))
                        <div class="col-sm-6">
                            <p class="pad-8">
                                {{$data[0]['passport_detail']['us_visa']==1 ? 'Us Visa Date Of Expiry : '.(isset($data[0]['passport_detail']['us_visa_expiry_date']) ? date('d-m-Y',strtotime($data[0]['passport_detail']['us_visa_expiry_date'])) : '-'):''}}
                            </p>
                        </div>
                    @endif
                    </div> --}}
                        {{-- @php
                            $yf=(\App\UserWkfrDetail::whereUserId($user_id)->whereYellowFever(1)->pluck('yf_issue_date')->first());
                            $ilo=(\App\UserWkfrDetail::whereUserId($user_id)->whereYellowFever(1)->pluck('ilo_issue_date')->first());
                        @endphp
                        @if(!empty($yf) || !empty($ilo))
                            <div class="row pad-8">
                                @if(!empty($yf))
                                    <div class="col-sm-6">
                                        {{($yf)?'YF Vaccination Validity : '.date("d-m-Y",strtotime($yf)):''}}
                                    </div>
                                @endif
                                @if(!empty($ilo))
                                    <div class="col-sm-6">
                                        {{($ilo)?'ILO Medical Validity : '.date("d-m-Y",strtotime($ilo)):''}}
                                    </div>
                                @endif
                            </div>
                        @endif --}}
                        @if(isset($data[0]['wkfr_detail']))
                            @php
                                $vaccines = [];
                                if($data[0]['wkfr_detail']['yellow_fever'] == 1){
                                    array_push($vaccines, "Yellow Fever");
                                } 
                                if($data[0]['wkfr_detail']['cholera'] == 1){
                                    array_push($vaccines, "Cholera");
                                }
                                if($data[0]['wkfr_detail']['hepatitis_b'] == 1){
                                    array_push($vaccines, "Hepatitis B");
                                }
                                if($data[0]['wkfr_detail']['hepatitis_c'] == 1){
                                    array_push($vaccines, "Hepatitis C");
                                }
                                if($data[0]['wkfr_detail']['diphtheria'] == 1){
                                    array_push($vaccines, "Diphtheria");
                                }
                                if($data[0]['wkfr_detail']['covid'] == 1){
                                    array_push($vaccines, "Covid19");
                                }
                            @endphp
                            @if (count($vaccines) > 0)
                                <div class="row pad-8">
                                    <div class="col-sm-12">
                                        Vaccinated Against : {{ implode(" | ", $vaccines) }}
                                    </div>
                                </div>
                            @endif
                        @endif
                        <h4 class="h4 pt-20"><i class="text-primary fa fa-life-ring" aria-hidden="true"></i>
                            SAILING DOCUMENT</h4>
                        <hr class="hr" />
                        <div class="">
                            <table class="table ">
                                <thead>
                                <tr>
                                    <th>Document</th>
                                    <th>Document Nos</th>
                                    <th>Valid upto</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(isset($data[0]['personal_detail']['nationality']) && $data[0]['personal_detail']['nationality'] == '95')
                                    <tr>
                                        <td>INDOS :</td>
                                        <td>{{ !empty($data[0]['wkfr_detail']['indos_number']) ? $data[0]['wkfr_detail']['indos_number'] : '-'}}</td>
                                        <td>---</td>
                                    </tr>
                                @endif
                                @foreach($data[0]['seaman_book_detail'] as $index => $cdc_data)
                                    <tr>
                                        <td>CDC : @foreach( \CommonHelper::countries() as $c_index => $country)
                                                {{ isset($cdc_data['cdc']) ? $cdc_data['cdc'] == $c_index ? $country : '' : ''}}
                                            @endforeach
                                        </td>
                                        <td>
                                            {{isset($cdc_data['cdc_number']) ? $cdc_data['cdc_number'] : ''}}
                                        </td>
                                        <td>
                                            {{isset($cdc_data['cdc_expiry_date']) ? date('d-m-Y',strtotime($cdc_data['cdc_expiry_date'])) : ''}}
                                        </td>
                                    </tr>
                                @endforeach
                                {{--                <td>CDC : @foreach( \CommonHelper::countries() as $c_index => $country)--}}
                                {{--                        {{ isset($cdc_data['cdc']) ? $cdc_data['cdc'] == $c_index ? $country : '' : ''}}--}}
                                {{--                    @endforeach</td>--}}
                                {{--                <td>{{isset($cdc_data['cdc_number']) ? $cdc_data['cdc_number'] : ''}}</td>--}}
                                {{--                <td>{{ !is_null($cdc_data['cdc_verification_date']) ? date('d-m-Y',strtotime($cdc_data['cdc_verification_date'])) : '-' }}</td>--}}
                                {{--            </tr>--}}
                                @if(!empty($data[0]) && !empty($data[0]['coc_detail'][0]) && $data[0]['coc_detail'][0]['coc'] != '')
                                    @foreach($data[0]['coc_detail'] as $index => $coc_data)
                                    <tr>
                                        <td>COC : {{ !empty($coc_data['coc_grade']) ? $coc_data['coc_grade'] : '-' }}  @if(isset($coc_data['coc']) && !empty($coc_data['coc']))
                                                @foreach( \CommonHelper::countries() as $c_index => $country)
                                                    {{ isset($coc_data['coc']) ? $coc_data['coc'] == $c_index ? $country : '' : ''}}
                                                @endforeach
                                            @else - @endif
                                        </td>
                                        <td>
                                            {{ !empty($coc_data['coc_number']) ? $coc_data['coc_number'] : '-'}}
                                        </td>
                                        <td>
                                            {{ !is_null($coc_data['coc_expiry_date']) ? date('d-m-Y',strtotime($coc_data['coc_expiry_date'])) : '-' }}
                                        </td>
                                    </tr>
                                    @endforeach
                                @endif
                                @if(isset($required_fields))
                                    @if(isset($data[0]['coe_detail']) AND !empty($data[0]['coe_detail']) && (in_array('COE',$required_fields) OR in_array('COE-Optional',$required_fields)) && (!empty($data[0]['coe_detail'][0]['coe']) || !empty($data[0]['coe_detail'][0]['coe_number']) || !empty($data[0]['coe_detail'][0]['coe_grade']) || !empty($data[0]['coe_detail'][0]['coe_expiry_date']) || !empty($data[0]['coe_detail'][0]['coe_verification_date'])))
                                        @foreach($data[0]['coe_detail'] as $index => $coe_data)
                                            <tr>
                                                <td>
                                                    COE : {{ !empty($coe_data['coe_grade']) ? $coe_data['coe_grade'] : '-'}} @if(isset($coe_data['coe']) && !empty($coe_data['coe']))
                                                        @foreach( \CommonHelper::countries() as $c_index => $country)
                                                            {{ isset($coe_data['coe']) ? $coe_data['coe'] == $c_index ? $country : '' : ''}}
                                                        @endforeach
                                                    @else
                                                        -
                                                    @endif
                                                </td>
                                                <td>
                                                    {{ !empty($coe_data['coe_number']) ? $coe_data['coe_number'] : '-'}}
                                                </td>
                                                <td>
                                                    {{ !empty($coe_data['coe_expiry_date']) ? date('d-m-Y',strtotime($coe_data['coe_expiry_date'])) : '-' }}
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                @endif
                                @if(isset($required_fields) && !empty($required_fields))
                                    @if(isset($data[0]['gmdss_detail']) AND !empty($data[0]['gmdss_detail']) && (in_array('GMDSS',$required_fields) OR in_array('GMDSS-Optional',$required_fields)) && ((!empty($data[0]['gmdss_detail']['gmdss'])) || (!empty($data[0]['gmdss_detail']['gmdss_number'])) || (!empty($data[0]['gmdss_detail']['gmdss_expiry_date']))))
                                        <tr>
                                            <td>GMDSS : @foreach( \CommonHelper::countries() as $c_index => $country)
                                                    {{ isset($data[0]['gmdss_detail']['gmdss']) ? $data[0]['gmdss_detail']['gmdss'] == $c_index ? $country : '' : ''}}
                                                @endforeach
                                            </td>
                                            <td>
                                                {{ isset($data[0]['gmdss_detail']['gmdss_number']) ? $data[0]['gmdss_detail']['gmdss_number'] : '-'}}
                                            </td>

                                            <td>
                                                {{ isset($data[0]['gmdss_detail']['gmdss_expiry_date']) ? date('d-m-Y',strtotime($data[0]['gmdss_detail']['gmdss_expiry_date'])) : '-' }}
                                            </td>
                                        </tr>
                                        {{--                        <div class="col-sm-3 gmdss_endorsement {{isset($data[0]['gmdss_detail']['gmdss']) && $data[0]['gmdss_detail']['gmdss'] == $india_value ? '' : 'hide'}}">--}}
                                        @if(isset($data[0]['gmdss_detail']['gmdss_endorsement_number']))
                                            <tr>
                                                <td>
                                                    GMDSS Endorsement
                                                </td>
                                                <td>{{ isset($data[0]['gmdss_detail']['gmdss_endorsement_number']) && !empty($data[0]['gmdss_detail']['gmdss_endorsement_number']) ? $data[0]['gmdss_detail']['gmdss_endorsement_number'] : '-'}}
                                                    {{--                        <div class="col-sm-3 gmdss_valid_till {{isset($data[0]['gmdss_detail']['gmdss']) && $data[0]['gmdss_detail']['gmdss'] == $india_value ? '' : 'hide'}}">--}}
                                                </td>
                                                <td>
                                                    {{ isset($data[0]['gmdss_detail']['gmdss_endorsement_expiry_date']) && !empty($data[0]['gmdss_detail']['gmdss_endorsement_expiry_date']) ? date('d-m-Y',strtotime($data[0]['gmdss_detail']['gmdss_endorsement_expiry_date'])) : '-'}}
                                                </td>
                                            </tr>
                                        @endif
                                    @endif
                                @endif

                                @if (isset($data[0]['professional_detail']) && !empty($data[0]['professional_detail']['current_rank']) && (in_array('DCE-Optional', \CommonHelper::rank_required_fields()[$data[0]['professional_detail']['current_rank']]) || in_array('DCE', \CommonHelper::rank_required_fields()[$data[0]['professional_detail']['current_rank']])))
                                    @php
                                        $dceDetails = isset($data[0]['user_dangerous_cargo_endorsement_detail']) ? $data[0]['user_dangerous_cargo_endorsement_detail'] : null;
                                        $dceStatus = !empty($dceDetails) ? json_decode($dceDetails['status']) : null;
                                        $oilDetails = !empty($dceDetails) ? json_decode($dceDetails['oil']) : null;
                                        $chemicalDetails = !empty($dceDetails) ? json_decode($dceDetails['chemical']) : null;
                                        $lequefiedGasDetails = !empty($dceDetails) ? json_decode($dceDetails['lequefied_gas']) : null;
                                        $allDetails = !empty($dceDetails) ? json_decode($dceDetails['all']) : null;
                                    @endphp
                                    @if(!empty($dceDetails) && !empty($dceStatus) && $dceStatus->status == 1)
                                        @if(!empty($dceStatus->type) && in_array('oil', $dceStatus->type))
                                            <tr>
                                                <td>
                                                    DCE : Oil {{ !empty($oilDetails) && !empty($oilDetails->country)  ?  \CommonHelper::countries()[$oilDetails->country] : null }}
                                                </td>
                                                <td>
                                                    @if (!empty($oilDetails) && isset($oilDetails->grade) && $oilDetails->grade != null)
                                                        {{ $oilDetails->grade == 0 ? 'Level I : ' : 'Level II : ' }}
                                                    @endif
                                                    {{ !empty($oilDetails) && isset($oilDetails->number) && !empty($oilDetails->number) ? $oilDetails->number : null }}
                                                </td>

                                                <td>
                                                    {{ !empty($oilDetails) &&  isset($oilDetails->date_of_expiry) && !empty($oilDetails->date_of_expiry) ? $oilDetails->date_of_expiry : null }}
                                                </td>
                                            </tr>
                                        @endif
                                        @if(!empty($dceStatus->type) && in_array('chemical', $dceStatus->type))
                                            <tr>
                                                <td>
                                                    DCE : Chemical {{ !empty($chemicalDetails) && !empty($chemicalDetails->country)  ?  \CommonHelper::countries()[$chemicalDetails->country] : null }}
                                                </td>
                                                <td>
                                                    @if (!empty($chemicalDetails) && isset($chemicalDetails->grade) && $chemicalDetails->grade != null)
                                                        {{ $chemicalDetails->grade == 0 ? 'Level I : ' : 'Level II : ' }}
                                                    @endif
                                                    {{ !empty($chemicalDetails) && isset($chemicalDetails->number) && !empty($chemicalDetails->number) ? $chemicalDetails->number : null }}
                                                </td>
                                                <td>
                                                    {{ !empty($chemicalDetails) &&  isset($chemicalDetails->date_of_expiry) &&  !empty($chemicalDetails->date_of_expiry) ? $chemicalDetails->date_of_expiry : null }}
                                                </td>
                                            </tr>
                                        @endif
                                        @if(!empty($dceStatus->type) && in_array('lequefied_gas', $dceStatus->type))
                                            <tr>
                                                <td>
                                                    DCE : Liquefied Gas {{ !empty($lequefiedGasDetails) && !empty($lequefiedGasDetails->country)  ?  \CommonHelper::countries()[$lequefiedGasDetails->country] : null }}
                                                </td>
                                                <td>
                                                    @if (!empty($lequefiedGasDetails) && isset($lequefiedGasDetails->grade) &&  $lequefiedGasDetails->grade != null)
                                                        {{ $lequefiedGasDetails->grade == 0 ? 'Level I : ' : 'Level II : ' }}
                                                    @endif
                                                    {{ !empty($lequefiedGasDetails) && isset($lequefiedGasDetails->number) && !empty($lequefiedGasDetails->number) ? $lequefiedGasDetails->number : null }}
                                                </td>

                                                <td>
                                                    {{ !empty($lequefiedGasDetails) && isset($lequefiedGasDetails->date_of_expiry) && !empty($lequefiedGasDetails->date_of_expiry) ? $lequefiedGasDetails->date_of_expiry : null }}
                                                </td>
                                            </tr>
                                        @endif
                                        @if(!empty($dceStatus->type) && in_array('all', $dceStatus->type))
                                            <tr>
                                                <td>
                                                    DCE : Oil + Chemical + Liquefied Gas {{ !empty($allDetails) && !empty($allDetails->country)  ?  \CommonHelper::countries()[$allDetails->country] : null }}
                                                </td>
                                                <td>
                                                    @if (!empty($allDetails) &&  isset($allDetails->grade) && $allDetails->grade != null)
                                                        {{ $allDetails->grade == 0 ? 'Level I : ' : 'Level II : ' }}
                                                    @endif
                                                    {{ !empty($allDetails) && isset($allDetails->number) && !empty($allDetails->number) ? $allDetails->number : null }}
                                                </td>
                                                <td>
                                                    {{ !empty($allDetails) && isset($allDetails->date_of_expiry) && !empty($allDetails->date_of_expiry) ? $allDetails->date_of_expiry : null }}
                                                </td>
                                            </tr>
                                        @endif
                                    @endif
                                @endif


                                </tbody>
                            </table>
                        </div>
                        <h4 class="h4 pt-20"><i class="text-primary fa fa-bar-chart" aria-hidden="true"></i> SKILL & EXPERTISE</h4>
                        <hr class="hr m-0" />
                            <div class="row">
                                <div class="col-sm-7">
                                    <div class="" style="height:{{($pdfFormat!=true) ? '300':'360'}}px">
                                        <canvas id="chart-area" {{($pdfFormat!=true) ? '':'height=360'}} style="max-width:470px"></canvas>
                                    </div>
                                </div>
                                <div class="col-sm-5">
                                    {{-- <div class="pt-7 text-center">
                                        <strong class="text-center" style="color: #7F7F7F; font-family:Arial; font-size: 15px">Engine Experience (Days)</strong>
                                    </div> --}}
                                    @if(!empty($user_contract['engine']))
                                        @php
                                            foreach($user_contract['engine'] as $engine_key => $engine) {
                                                $ships[] = \CommonHelper::engine_type($user_id)[$engine_key];
                                            }
                                            $newRank =     [5,6,9,10,11,12,13,16,17,24,25,26,27,28,30,53,54];
                                            $applied_ranks=[5,6,9,10,11,12,13,16,17,24,25,26,27,28,30,53,54];
                                        @endphp
                                        <br>
                                        @if(!empty($user_contract['all_engine']))
                                            @foreach($user_contract['all_engine'] as $key => $all_engine_value)
                                                @php
                                                    if (!in_array($key,$applied_ranks)) continue;
                                                    ksort($all_engine_value);
                                                    $total_days=0;
                                                @endphp
                                                <div class="well">
                                                    @foreach($all_engine_value as $_key=>$_all_engine_value)
                                                        @if($_all_engine_value["all_days"] > 0)
                                                            @php
                                                                $total_days += $_all_engine_value["all_days"];
                                                            @endphp
                                                        @endif
                                                    @endforeach
                                                    @foreach(\CommonHelper::new_rank() as $index => $category)
                                                        @foreach($category as $key_rank => $rank)
                                                            @if($key == $key_rank)
                                                                <strong>{{$rank}}</strong> : <b>{{$total_days}} Day(s)</b>
                                                            @endif
                                                        @endforeach
                                                    @endforeach
                                                    <br>
                                                    @if(!in_array($key,$newRank))
                                                        <span class="font-12">
                                                            @foreach($all_engine_value as $_key=>$_all_engine_value)
                                                                @if($_all_engine_value["all_days"] > 0 && isset($ships[$_key]))
                                                                    {{$ships[$_key]}} - {{$_all_engine_value["all_days"]}} Day(s),&nbsp;
                                                                    @php
                                                                        $total_days += $_all_engine_value["all_days"];
                                                                    @endphp
                                                                @endif
                                                            @endforeach
                                                        </span>
                                                        <br>
                                                    @endif
                                                    {{-- <b>TOTAL : {{$total_days}} Days</b> --}}
                                                </div>
                                        {{-- <hr class="my-8"> --}}
                                        @endforeach
                                    @else
                                            <p class="text-center">
                                                ---
                                            </p>
                                        @endif
                                    @else
                                    -
                                    @endif
                                    @if($data[0]['passport_detail']['fromo'])
                                        <p class="ml-2">
                                            Experience in FRAMO
                                            {{-- Though {{$data[0]['first_name'].' '.$data[0]['last_name']}} has FRAMO experience. --}}
                                        </p>
                                    @endif
                                </div>

                            </div>
                    <script>
                        // var position = 'left';
                        var position = 'left';
                        if($( window ).width() < 992){
                            position = 'top';
                        }

                        @php
                            $count=count($user_contract['ships']);
                        @endphp
                            'use strict';
                        (function(setLineDash) {
                            CanvasRenderingContext2D.prototype.setLineDash = function() {
                                if(!arguments[0].length){
                                    arguments[0] = [1,0];
                                }
                                // Now, call the original method
                                return setLineDash.apply(this, arguments);
                            };
                        })(CanvasRenderingContext2D.prototype.setLineDash);
                        Function.prototype.bind = Function.prototype.bind || function (thisp) {
                            var fn = this;
                            return function () {
                                return fn.apply(thisp, arguments);
                            };
                        };
                        var config = {
                            type: 'pie',
                            data: {
                                datasets: [{
                                    data: [
                                        @foreach($user_contract['ships'] as $key=>$ship)
                                        {{$ship}},
                                        @endforeach
                                    ],
                                    backgroundColor: [
                                        @for( $i=0;$i<$count;$i++)
                                            '#{{str_pad(dechex(rand(0x000000, 0xFFFFFF)), 6, 0, STR_PAD_LEFT)}}',
                                        @endfor
                                    ]
                                }],
                                labels: [
                                        @foreach($user_contract['ships'] as $key=>$ship)
                                            '{{\CommonHelper::ship_type()[$key]}} : {{$ship}} D',
                                        @endforeach
                                ]
                            },
                            options: {
                                responsive: {{($pdfFormat!=true) ? 'true':'false'}},
                                maintainAspectRatio:false,
                                legend: {
                                    position:position,
                                    display: true,
                                },title: {
                                    display: true,
                                    text: 'Sailing summary (Days)',
                                    fontSize:13
                                }
                            }
                        };
                        window.onload = function() {
                            var ctx = document.getElementById('chart-area').getContext('2d');
                            window.myPie = new Chart(ctx, config);
                        }

                        $(window).on("resize", function () {
                            if($( window ).width() < 992){
                                position = 'top';
                                config.options.legend.position = 'top';
                            } else {
                                config.options.legend.position = 'left';
                            }
                            var ctx = document.getElementById('chart-area').getContext('2d');
                            window.myPie = new Chart(ctx, config);
                        });
                    </script>

                        <h4 class="h4 pt-20"><i class="text-primary fa fa-graduation-cap" aria-hidden="true"></i> COURSE & CERTIFICATION</h4>
                        <hr class="hr" />
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th>Course Name</th>
                                    <th>Valid Upto</th>
                                    <th>Institute</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(isset($data[0]['course_detail']) AND !empty($data[0]['course_detail']))
                                    <?php $normal_course_count = 1; ?>
                                    @foreach($data[0]['course_detail'] as $index => $courses)
                                        <tr>
                                            <td>
                                                {{ isset($courses['course_id']) ? ( \App\Courses::whereId($courses['course_id'])->pluck('course_name'))->first() : ''}}
                                                @foreach( \CommonHelper::courses() as $c_index => $course)
                                                @endforeach
                                            </td>
                                            <td style="min-width: 96px;">
                                                {{ isset($courses['expiry_date']) && !empty(isset($courses['expiry_date'])) ? date('d-m-Y',strtotime($courses['expiry_date'])) : '-'}}
                                            </td>
                                            <td style="min-width: 96px;">
                                                {{ isset($courses['issue_by']) && !empty(isset($courses['issue_by'])) ? $courses['issue_by'] : '-'}}
                                            </td>
                                            <?php $normal_course_count++; ?>
                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                        </div>

                </div>

                </div>
            </div>
        <br />
        <br />
        <section class="container ">
            <h4 class="h4 pt-20"><i class="text-primary fa fa-ship" aria-hidden="true"></i> SEA SERVICE EXPERIENCE</h4>
            <hr class="hr" />
            <div class="">
                <table class="table table-striped table-bordered">
                    <thead>
                    <tr>
                        <th><p class="text-center">Rank</p></th>
                        <th>
                            <p class="text-center">Ship</p>
                            <hr/>
                            <p class="text-center">Flag</p>
                        </th>
                        <th>
                            <p class="text-center">Owner Company</p>
                            <hr/>
                            <p class="text-center">Manning By</p>
                        </th>
                        <th>
                            <p class="text-center">Ship Type</p>
                            <hr/>
                            <p class="text-center">Engine Type</p>
                        </th>
                        <th>
                            <p class="text-center">GRT</p>
                            <hr/>
                            <p class="text-center">BHP</p>
                        </th>
                        <th>
                            <p class="text-center">Sign On</p>
                            <hr/>
                            <p class="text-center">Sign Off</p>
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(isset($data[0]['sea_service_detail']) AND !empty($data[0]['sea_service_detail']))
                        @foreach($data[0]['sea_service_detail'] as $index => $services)
                            <tr>
                                <td class="pos-relative">
                                    <p class="text-center">
                                        @foreach(\CommonHelper::new_rank() as $index => $category)
                                            @foreach($category as $r_index => $rank)
                                                {{ isset($services['rank_id']) ? $services['rank_id'] == $r_index ? $rank : '' : ''}}
                                            @endforeach
                                        @endforeach
                                    </p>
                                </td>
                                <td>
                                    <p class="text-center">{{ isset($services['ship_name']) ? $services['ship_name'] : '-'}}</p>
                                    <hr/>
                                    <p class="text-center">@foreach( \CommonHelper::countries() as $c_index => $country)
                                            {{ isset($services['ship_flag']) ? $services['ship_flag'] == $c_index ? $country : '' : ''}}
                                        @endforeach</p>
                                </td>
                                <td>
                                    <p class="text-center">{{ isset($services['company_name']) ? $services['company_name'] : '-'}}</p>
                                    <hr/>
                                    <p class="text-center">{{ isset($services['manning_by']) ? $services['manning_by'] : '-'}}</p>
                                </td>
                                <td>
                                    <p class="text-center"> @foreach( \CommonHelper::ship_type() as $c_index => $type)
                                            {{ isset($services['ship_type']) ? $services['ship_type'] == $c_index ? $type : '' : ''}}
                                        @endforeach
                                    </p>
                                    <hr/>
                                    <p class="text-center"> @if(isset($services['engine_type']) && !empty($services['engine_type']))
                                            @if(isset($services['engine_type']) && $services['engine_type'] == 'other')
                                                {{ isset($services['other_engine_type']) ? $services['other_engine_type'] : '-'}}
                                            @else
                                                @foreach( \CommonHelper::engine_type($user_id) as $c_index => $type)
                                                    {{ isset($services['engine_type']) ? $services['engine_type'] == $c_index ? $type  : '' : ''}}
                                                @endforeach
                                            @endif
                                        @else
                                            -
                                        @endif
                                    </p>
                                </td>
                                <td>
                                    <p class="text-center">
                                        {{ !empty($services['grt']) ? $services['grt'] : '-'}}
                                    </p>
                                    <hr/>
                                    <p class="text-center">
                                        {{ !empty($services['bhp']) ? $services['bhp'] : '-'}}
                                    </p>
                                </td>
                                <td>
                                    <p class="text-center">
                                        {{ isset($services['from']) ? date('d-m-Y',strtotime($services['from'])) : ''}}
                                    </p>
                                    <hr/>
                                    <p class="text-center">
                                        {{ isset($services['to']) ? date('d-m-Y',strtotime($services['to'])) : 'On Board'}}
                                    </p>
                                </td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
        </section>
        @if(isset($data[0]['professional_detail']['other_exp']) && !empty($data[0]['professional_detail']['other_exp']))
            <section class="container ">
                <h4 class="h4 pt-20"><i class="text-primary fa fa-industry" aria-hidden="true"></i> OTHER EXPERIENCE</h4>
                <hr class="hr" />
                <p>{{$data[0]['professional_detail']['other_exp']}}</p>
            </section>
        @endif
        @if($pdfFormat!=true && !request()->get('user'))
            <section class="container ">
                <br/><br/>
                <a onclick="DownloadFile()" href="javascript:;" id="download-pdf" class="btn btn-primary">Download as PDF</a>
            </section>
        @endif
        <footer class="container-fluid footer bg-primary" style="padding-top: 5px; margin-top:10px">
            <div class="row">
                <p class=" col-xs-10 col-sm-10 col-md-10 col-lg-10" style="margin:0">This Resume is auto generated using the information submitted by {{$data[0]['first_name']}} at Flanknot.com</p>
                <span class=" col-xs-2 col-sm-2 col-md-2 col-lg-2 text-right">{{\Carbon\Carbon::now()->format('M d Y')}}</span>
            </div>
        </footer>
    @else
        <div class="row">
            <div class="container m_h-100v">
                <h4 class="text-center h4 heading">Resume</h4>
                <div class="mt-30 panel panel-bricky">
                    <div class="panel-heading text-center">
                        <h4 class="h4">Kindly edit profile to view resume</h4>
                    </div>
                </div>
                <div class="col-md-12">
                    <img class="img-responsive center-block graph-img" src="{{ URL:: asset('public/images/checkout_my_resume.png')}}">
                </div>
            </div>
        </div>
    @endif
    <input type="hidden" id="pdfFirstName" value="{{ isset($data[0]['first_name']) ? ucwords($data[0]['first_name']) : ''}}">
</body>
@if($pdfFormat!=true)
<script type="text/javascript">
        
        function DownloadFile(url) {
            $("#download-pdf").html("<i class='fa fa-spin fa-spinner'></i> Downloading... Please Wait");
            //Create XMLHTTP Request.
            var req = new XMLHttpRequest();
            req.open("GET", "{{route('create-pdf',['PDF'=>CommonHelper::encodeKey(2)])}}", true);
            req.responseType = "blob";
            req.onload = function () {
                //Convert the Byte Data to BLOB object.
                var blob = new Blob([req.response], { type: "application/octetstream" });
                var today = new Date();
                var PdfDate = today.getFullYear()+'.'+(today.getMonth()+1)+'.'+today.getDate();
                var pdfTime = today.getHours() + "." + today.getMinutes() + "." + today.getSeconds();
                var pdfName = $('#pdfFirstName').val() + " " + PdfDate + " " + pdfTime + " Resume Flanknot.pdf";
                //Check the Browser type and download the File.
                var isIE = false || !!document.documentMode;
                if (isIE) {
                    window.navigator.msSaveBlob(blob, pdfName);
                } else {
                    var url = window.URL || window.webkitURL;
                    link = url.createObjectURL(blob);
                    var a = document.createElement("a");
                    a.setAttribute("download", pdfName);
                    a.setAttribute("href", link);
                    document.body.appendChild(a);
                    a.click();
                    document.body.removeChild(a);
                }
                $("#download-pdf").html("Download as PDF");
            };
            req.send();
        };
    </script>
    @endif
</html>