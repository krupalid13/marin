 @extends('admin.index')

@section('content')

<?php
	$initialize_pincode_maxlength_validation = false;
	$india_value = array_search('India',\CommonHelper::countries());
?>

<div class="container">
	<div class="toolbar row">
        <div class="col-sm-12">
            <div class="page-header">
                <h1>Course Discount Mapping<small>Add Discount</small></h1>
            </div>
        </div>
    </div>
    
	<div class="row">
		<div class="col-sm-12 m-t-25">
			<!-- start: FORM WIZARD PANEL -->
			<div class="panel panel-white">
				<div class="panel-body">
					<form id="add-course-discount-batch" class="smart-wizard form-horizontal" method="post" action="{{route('admin.institute.store.institute.list.by.course')}}">
		                {{ csrf_field() }}
		                <input type="hidden" class="existing_id" name="existing_id" value="">
		               	<div id="wizard1" class="swMain">
							<div id="step-1">
								<div class="alert alert-danger alert-box" id="alert-box-documents" style="display: none">
		                            <ul>
		                                @foreach ($errors->all() as $error)
		                                    <li>{{ $error}}</li>
		                                @endforeach
		                            </ul>
		                    	</div> 		                  
		                    	<div class="panel-heading p-l-0">
									<h4 class="panel-title">ADD COURSE DISCOUNT</h4>
								</div>

	                            <div class="form-group">
	                                <label class="col-sm-2 control-label" for="course_type">Course Type<span class="symbol required"></label>
		                            <div class="col-sm-4">
		                                <select id="course_type" name="course_type" class="form-control">
		                                    <option value=''>Select Course Type</option>
		                                    @foreach(\CommonHelper::institute_course_types() as $r_index => $type)
                                                <option value="{{$r_index}}">{{$type}} </option>
                                            @endforeach
		                                </select>
		                             </div>
		                        
	                                <label class="col-sm-2 control-label">Course Name<span class="symbol required"></label>
	                                <div class="col-sm-4">
		                                <select name="course_name" class="form-control institute_course_name get_institute_list">
		                                    <option value="">Select Course Name</option>
		                                </select>
		                            </div>
		                        </div>
		                        <div class="form-group">
	                                <label class="col-sm-2 control-label">Institute List<span class="symbol required"></label>
	                                <div class="col-sm-4">
		                                <select class="form-control institute_list_by_course" name="institute_list_by_course">
		                                	<option value="">Select Institute</option>
		                                </select>
		                            </div>
		                        
	                                <label class="col-sm-2 control-label">Institute Batch List<span class="symbol required"></label>
	                                <div class="col-sm-4">
		                                <select multiple="multiple" name="institute_list[]" class="form-control institute_list select2-select" style="height: auto;padding: 0px;" placeholder="Please select institute batch">
		                                </select>
		                            </div>
		                        </div>
		                        <div class="form-group">
	                                <label class="col-sm-2 control-label">Advertiser List<span class="symbol required"></label>
	                                <div class="col-sm-4">
		                                <select multiple="multiple" name="advertise_list[]" class="form-control advertise_list" style="height: auto;padding: 0px;" placeholder="Please select advertiser">
		                                	@if(isset($advertiser_list) && !empty($advertiser_list))
		                                        @foreach($advertiser_list as $index => $advertise)
		                                            <option value="{{$advertise['company_registration_detail']['id']}}">{{ucwords($advertise['company_registration_detail']['company_name'])}}</option>
		                                        @endforeach
		                                    @endif
		                                </select>
		                            </div>
		                        
	                                <label class="col-sm-2 control-label">Discount<span class="symbol required"></label>
	                                <div class="col-sm-4">
		                                <input type="text" class="form-control discount" name="discount" placeholder="Enter discount in %">
		                            </div>
		                        </div>
	                        		
		                        <div class="form-group">
                                    <div class="col-sm-2 col-sm-offset-10">
                                        <button type="button" data-style="zoom-in" class="btn btn-blue ladda-button btn-block add-institute-batch-discount" id="add-institute-batch-discount">
                                            Save <i class="fa fa-arrow-circle-right"></i>
                                        </button>
                                    </div>
                                </div>
		                    </div>
		                </div>
		            </form>
				</div>
			</div>
		</div>
	</div>

	<!-- filter -->
	<form class="filter-search-form m_l_10">
		<div class="row m_t_25">
			<div class="col-md-12">
				<div class="panel panel-white">
					<div class="panel-body">
						<div class="panel-heading p-l-0">
							<h4 class="panel-title">COURSE DISCOUNT FILTER</h4>
						</div>
						<div class="row">
							<div class="col-sm-3">
                                <div class="form-group">
                                    <label>Institute Name </label>
                                    <select id="institute_name" name="institute_name" class="form-control">
                                        <option value=>Select Institute Name</option>
		                                    @foreach($institutes as $r_index => $type)
                                                <option value="{{$type['id']}}" {{ isset($filter['institute_name']) ? $filter['institute_name'] == $type['id'] ? 'selected' : '' : ''}}>{{$type['institute_name']}} </option>
                                            @endforeach
                                    </select>
                                </div>
                            </div>    
							<div class="col-sm-3">
                                <div class="form-group">
                                    <label>Course Type </label>
                                    <select name="course_type" class="form-control course_type">
                                        <option value=>Select Course Type</option>
                                            @foreach(\CommonHelper::institute_course_types() as $r_index => $type)
                                                <option value="{{$r_index}}" {{ isset($filter['course_type']) ? $filter['course_type'] == $r_index ? 'selected' : '' : ''}}>{{$type}} </option>
                                            @endforeach
                                    </select>
                                </div>
                            </div>
                            
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label>Course Name </label>
                                    <select id="course_name" name="course_name" class="form-control institute_course_name">
                                        <option value=>Select Course Name</option>
                                        @if(isset($course_name) && !empty($course_name))
	                                        @foreach($course_name as $r_index => $type)
                                                <option value="{{$type['id']}}" {{ isset($filter['course_name']) ? $filter['course_name'] == $type['id'] ? 'selected' : '' : ''}}>{{$type['course_name']}} </option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                        
                            <div class="col-sm-3">
                                <div class="section_cta text-right m_t_25 job-btn-container">
                                    <button type="button" class="btn btn-danger coss-inverse-btn search-reset-btn" id="seafarerResetButton">Reset</button>
                                    <button type="submit" data-style="zoom-in" class="btn btn-info coss-primary-btn search-post-btn ladda-button candidate-filter-search-button" id="candidate-filter-search-button"><i class="fa fa-search search-icon" aria-hidden="true"></i>Search</button>
                                </div>
                            </div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</form>

	<div class="row search-results-main-container" >
		<div class="col-md-12">
			<div class="panel panel-white">
				<div class="panel-body">
					<!-- <div class="panel-heading1">
						<h4 class="panel-title p-l-15">COURSE DISCOUNT LISTING</h4>
					</div> -->
					<?php 
						$data_array = $existing_course_discounts->toArray();
						$pagination = $existing_course_discounts;
					?>
					<div class="full-cnt-loader hidden">
						<div class="" style="text-align: center;padding-top:20px;padding-bottom:20px; ">
						    <i class="fa fa-spinner fa-spin fa-4" aria-hidden="true" style="font-size: 7em;"></i>
					    </div>
					</div>
					<div class="discount_result">
						@if(isset($data_array['data']) && !empty($data_array['data']))
							<div class="list-container">
								<div class="panel-heading">
									<div class="row">
										<div class="col-xs-12">
											<div class="col-xs-6 paginator-content  pull-right no-margin">
												<ul id="top-user-paginator" class="pagination">
													{!! $existing_course_discounts->render() !!}
												</ul>
											</div>
											
											<div class="pull-left search-count-content">
												Showing {{$data_array['from']}} - {{$data_array['to']}} of {{$data_array['total']}} Results
											</div>
										</div>
									</div>
								</div>
								@foreach($data_array['data'] as $data)	
									<div class="seafarer-listing-container">
										<div class="panel">
											<div class="panel-heading border-light partition-blue">
												<h5 class="panel-title text-bold header-link-text p-5-5">
													{{ isset($data['batch_details']['course_details']['institute_registration_detail']['institute_name']) ? ucwords($data['batch_details']['course_details']['institute_registration_detail']['institute_name']) : '' }}
												</h5>
												<ul class="panel-heading-tabs border-light">
													<li class="panel-tools">
														<div class="dropdown">
															<a data-toggle="dropdown" class="btn btn-xs dropdown-toggle btn-transparent-grey">
																<i class="fa fa-cog"></i>
															</a>
															<ul class="dropdown-menu dropdown-light pull-right" role="menu">
																<li>
																	<a class='discount_edit_section' data-id="{{$data['id']}}">
																		Edit
																	</a>
																</li>
																<li>
																	<a class="discount_delete_button" data-id="{{$data['id']}}">
																		Delete
																	</a>
																</li>
															</ul>
														</div>
													</li>
												</ul>

											</div>
															
											<div class="panel-body">
												<div class="row">
													<div class="col-lg-12 col-md-7 col-sm-7">
														<div class="row">
															<div class="col-sm-4 sm-t-20">
																<div class="row">
																	<div class="col-xs-12">
																		<div class="header-tag">
																			Course Information
																		</div>
																	</div>
																</div>
																<div id="user-basic-details">
																	<div class="row">
																		<div class="col-xs-6 col-sm-12">
																			<dl>
																				<dt> Course type : </dt>
																				<dd class="margin-bottom-5px">
																					@foreach(\CommonHelper::institute_course_types() as $r_index => $course_type)
						                                                                {{ !empty($data['batch_details']['course_details']['course_type']) ? $data['batch_details']['course_details']['course_type'] == $r_index ? $course_type : '' : ''}}
						                                                            @endforeach
																				</dd>
																	     		<dt> Course Name :</dt>
																				<dd class="margin-bottom-5px">
																					{{ isset($data['batch_details']['course_details']['courses'][0]['course_name']) && !empty($data['batch_details']['course_details']['courses'][0]['course_name']) ? $data['batch_details']['course_details']['courses'][0]['course_name']  : '-'}}
																				</dd>
																				<dt> Course Discount :</dt>
																				<dd class="margin-bottom-5px">
																					{{ isset($data['discount']) && !empty($data['discount']) ? $data['discount']  : '-'}}%
																				</dd>
																			</dl>
																		</div>
																	</div>
																</div>
															</div>
															
															<div class="col-sm-4">
																<div id="user-professional-details">
																	<div class="row">
																		<div class="col-xs-12">
																			<div class="header-tag">
																				Advertiser Details
																			</div>
																		</div>
																	</div>
																	<dl>
															     		<dt> Company Name : </dt>
																			<dd class="margin-bottom-5px">
																				{{ isset($data['company_registration']['company_name']) ? $data['company_registration']['company_name'] : '-' }}
															     			</dd>
														     			<dt>
														     			<dt> Company Email : </dt>
																			<dd class="margin-bottom-5px">
																				{{ isset($data['company_registration']['email_display']) ? $data['company_registration']['email_display'] : '-' }}
															     			</dd>
														     			<dt>
																	</dl>
																</div>
															</div>

															<div class="col-sm-4">
																<div id="user-professional-details">
																	<div class="row">
																		<div class="col-xs-12">
																			<div class="header-tag">
																				Batch Details
																			</div>
																		</div>
																	</div>
																	<dl>
															     		<dt> Start Date : </dt>
																			<dd class="margin-bottom-5px">
																				{{ isset($data['batch_details']['start_date']) && !empty($data['batch_details']['start_date']) ? date('d-m-Y', strtotime($data['batch_details']['start_date']))  : '-'}}
															     			</dd>
														     			<dt>
														     			<dt> Cost : </dt>
																			<dd class="margin-bottom-5px">
																				<span class="fa fa-inr"></span>
																				{{ isset($data['batch_details']['cost']) ? $data['batch_details']['cost'] : '-' }}
															     			</dd>
														     			<dt>
														     			<dt> Duration : </dt>
																			<dd class="margin-bottom-5px">
																				{{ isset($data['batch_details']['duration']) ? $data['batch_details']['duration'] : '-' }} Days
															     			</dd>
														     			<dt>
																	</dl>
																</div>
															</div>
														</div>
													</div>
																	
												</div>
											</div>
										</div>
														
									</div>
								@endforeach
								@if(isset($data_array['data']) && !empty($data_array['data']))
							
									<div class="panel-heading">
										<div class="row">
											<div class="col-xs-12">
												<div class="col-xs-6 paginator-content  pull-right no-margin">
													<ul id="top-user-paginator" class="pagination">
														{!! $pagination->render() !!}
													</ul>
												</div>
												
												<div class="pull-left search-count-content">
													Showing {{$data_array['from']}} - {{$data_array['to']}} of {{$data_array['total']}} Results
												</div>
											</div>
										</div>
									</div>
								@endif
							</div>
						@else
							<div class="no-results-found">No results found.</div>
						@endif
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@stop

@section('js_script')
	<script type="text/javascript" src="/js/site/institute-registration.js"></script>

	<script type="text/javascript">
        
        var searchAjaxCall = "undefined";

        $(document).ready(function(){

            //init history plugin to perform search without page refresh
            // Establish Variables
            History = window.History; // Note: We are using a capital H instead of a lower h

            // Bind to State Change
            // Note: We are using statechange instead of popstate
			
            History.Adapter.bind(window,'statechange', function() {

                // Log the State
                var State = History.getState(); // Note: We are using History.getState() instead of event.state

                //start search (defined)
                fetchSearchResults(State.url, 
                                    function() {
                                        
                                    }, function() {

                                    });

            });

            

        });

        function fetchSearchResults(url, successCallback, errorCallback)
	    {
	    	
	    	var temp_current_state_url = url.split('?');
            if(temp_current_state_url.length > 1 && temp_current_state_url[1] != ''){
                var parameters = temp_current_state_url[1].split('&');

                var dropdown_elements = ['bussiness_category','bi_member'];

                for(var i=0; i<parameters.length; i++){

                    var param_array = parameters[i].split('=');
                    if($.inArray(param_array[0],dropdown_elements) > -1){
                        $('select[name="'+param_array[0]+'"]').val(param_array[1]);
                    }else if($.inArray(param_array[0],['first_name','company_name']) > -1){
                        $('input[name="'+param_array[0]+'"]').val(param_array[1]);
                    }

                }
            } else {
             	$(':input','.filter-search-form')
	                .not(':button, :submit, :reset, :hidden')
	                .val('')
	                .removeAttr('checked')
	                .removeAttr('selected');
	             }

	        if(searchAjaxCall != "undefined")
	        {
	            searchAjaxCall.abort();
	            searchAjaxCall = "undefined";
	        }

	        $('.list-container').addClass('hide');
        	$('.full-cnt-loader').removeClass('hidden');

	        searchAjaxCall = $.ajax({
	            url: url,
	            cache: false,
	            dataType: 'json',
	            statusCode: {
	                200: function(response) {
	                    if (typeof response.template != 'undefined') {
	                       
	                        $('.list-container').removeClass('hide');
                        	$('.full-cnt-loader').addClass('hidden');
                        	
							$('.discount_result').html(response.template);

	                        if(typeof successCallback != "undefined")
	                        {
	                            successCallback();
	                        }

	                    }
	                }
	            },
	            error: function(error, response) {

	                if(typeof errorCallback != "undefined")
	                {
	                    errorCallback();
	                }
	            }
	        });
	    }
    </script>
	<script type="text/javascript">
      
      $('.select2-select').select2({
         closeOnSelect: false
      });
      $('.advertise_list').select2({
         closeOnSelect: false
      });
    </script>
@stop