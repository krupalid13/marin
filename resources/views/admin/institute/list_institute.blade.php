@extends('admin.index')

@section('content')

<div class="main-wrapper">
	<div class="main-container1">
				<!-- start: PAGE -->
				<div class="main-content">
					
					<div class="container">
						<!-- start: PAGE HEADER -->
						<!-- start: TOOLBAR -->
						<div class="toolbar row">
							<div class="col-xs-12 col-sm-6">
								<div class="page-header">
									<h1>Institute <small>A list of all institutes</small></h1>
								</div>
							</div>
						</div>

						<!-- filter -->
						<form class="filter-search-form m_l_10">
							<div class="row m_t_25">
								<div class="col-md-12">
									<div class="panel panel-white">
										<div class="panel-body">
											<div class="row">
												<div class="col-sm-3">
					                                <div class="form-group">
					                                    <label>Institute Name </label>
					                                    <input type="text" class="form-control" name="institute_name" id="institute_name" value="{{isset($filter['institute_name']) ? $filter['institute_name'] : ''}}" placeholder="Enter Institute Name">
					                                </div>
					                            </div>    
					                            <div class="col-sm-3">
					                                <div class="form-group">
					                                    <label>State </label>
					                                    <select name="state" class="form-control state institute-state" data-index="0">
                                                            <option value=>Select State</option>
                                                                @foreach($state as $s_index => $state_list)
                                                                    <option value="{{$state_list['id']}}" {{ isset($filter['state']) ? $filter['state'] == $s_index ? 'selected' : '' : ''}}>{{$state_list['name']}} 
                                                                    </option>
                                                                @endforeach
                                                        </select>
					                                </div>
					                            </div>
					                            <div class="col-sm-3">
					                                <div class="form-group">
					                                    <label>City </label>
					                                    	<select name="city" id="city" class="form-control city" placeholder="Please select city">
					                                    	<option value=''> Select City</option>
					                                    </select>
					                                </div>
					                            </div>
												
					                            <div class="col-sm-3">
					                                <div class="section_cta text-right m_t_25 job-btn-container">
					                                    <button type="button" class="btn btn-danger coss-inverse-btn search-reset-btn" id="seafarerResetButton">Reset</button>
					                                    <button type="submit" data-style="zoom-in" class="btn btn-info coss-primary-btn search-post-btn ladda-button candidate-filter-search-button" id="candidate-filter-search-button"><i class="fa fa-search search-icon" aria-hidden="true"></i>Search</button>
					                                </div>
					                            </div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</form>

						 <div class="row search-results-main-container" >
							<div class="col-md-12">
								<div class="panel panel-white">
									<div class="panel-body">
										<?php 
											$data_array = $data->toArray();
											$pagination = $data;
										?>
										@if(isset($data_array['data']) && !empty($data_array['data']))
										
											<div class="panel-heading">
												<div class="row">
													<div class="col-xs-12">
														<div class="col-xs-6 paginator-content  pull-right no-margin">
															<ul id="top-user-paginator" class="pagination">
																{!! $data->render() !!}
															</ul>
														</div>
														
														<div class="pull-left search-count-content">
															Showing {{$data_array['from']}} - {{$data_array['to']}} of {{$data_array['total']}} Results
														</div>
													</div>
												</div>
											</div>
														
										<div class="full-cnt-loader hidden">
										    <div class="" style="text-align: center;padding-top:20px;padding-bottom:20px; ">
										        <i class="fa fa-spinner fa-spin fa-4" aria-hidden="true" style="font-size: 7em;"></i>
										    </div>
										</div>		
										@foreach($data_array['data'] as $data)	
										<div class="seafarer-listing-container">
										<div class="panel">
										<div class="panel-heading border-light partition-blue">
										<h5 class="panel-title text-bold header-link-text p-5-5">
											<a href="{{Route('admin.view.institute.id',$data['id'])}}">
												{{isset($data['institute_registration_detail']['institute_name']) ? $data['institute_registration_detail']['institute_name'] : ''}}
											</a>
										</h5>
										<ul class="panel-heading-tabs border-light">
										<li class="panel-tools">
											<div class="dropdown">
												<a data-toggle="dropdown" class="btn btn-xs dropdown-toggle btn-transparent-grey">
													<i class="fa fa-cog"></i>
												</a>
												<ul class="dropdown-menu dropdown-light pull-right" role="menu">
													<li>
														<a href="{{Route('admin.view.institute.id',$data['id'])}}" data-type="user">
															View
														</a>
													</li>
													<li>
														<a href="{{Route('admin.edit.institute',$data['id'])}}">
															Edit
														</a>
													</li>
													<li>
														<a class="change-status-user cursor-pointer {{ $data['status'] == '0' ? 'hide' : ''}}" data-status='deactive' data-id={{$data['id']}} id="deactive-{{$data['id']}}">
															Deactivate
														</a>
													</li>
													<li>
														<a class="change-status-user cursor-pointer {{ $data['status'] != '0' ? 'hide' : ''}}"" data-status='active' data-id={{$data['id']}} id="active-{{$data['id']}}">
															Activate
														</a>
													</li>
												</ul>
											</div>
										</li>
										</ul>

										</div>
															
										<div class="panel-body">
										<div class="row">
										<div class="col-lg-3 col-md-5 col-sm-5" id="user-profile-pic" style="height: 200px;width: 200px; margin-left: 15px;">
											<a>
												<div class="company_profile_pic">
				                					<img src="/{{ isset($data['profile_pic']) ? env('INSTITUTE_LOGO_PATH')."/".$data['id']."/".$data['profile_pic'] : 'images/user_default_image.png'}}" >
				                				</div>
	                            			</a>
										</div>
										<div class="col-lg-9 col-md-7 col-sm-7">
											<div class="row">
												<div class="col-sm-4 sm-t-20">
													<div class="row">
														<div class="col-xs-12">
															<div class="header-tag">
																Basic Information
															</div>
														</div>
													</div>
													
													<div id="user-basic-details">
														<div class="row">
															<div class="col-xs-6 col-sm-12">
																<dl>
																	<dt> Institute Name : </dt>
																		<dd class="margin-bottom-5px">
																			{{isset($data['institute_registration_detail']['institute_name']) ? $data['institute_registration_detail']['institute_name'] : ''}}
																		</dd>
																	<dt> Institute Email : </dt>
																		<dd class="margin-bottom-5px make-word-wrap-break">
																			{{isset($data['institute_registration_detail']['institute_detail']['institute_email']) ? $data['institute_registration_detail']['institute_detail']['institute_email'] : ''}}
															     		</dd>
														     		<dt> Institute Phone Number : </dt>
																		<dd class="margin-bottom-5px make-word-wrap-break">
																			{{isset($data['institute_registration_detail']['institute_detail']['institute_contact_number']) ? $data['institute_registration_detail']['institute_detail']['institute_contact_number'] : '-'}}
															     		</dd>
														     		<dt> Website : </dt>
																		<dd class="margin-bottom-5px">
																			<a href="{{isset($data['institute_registration_detail']['website']) ? 'http://'.$data['institute_registration_detail']['website'] : ''}}" target="_blank">
																				{{isset($data['institute_registration_detail']['website']) && !empty($data['institute_registration_detail']['website']) ? $data['institute_registration_detail']['website'] : '-'}}
																			</a>
															     		</dd>
														     		<dt>
																</dl>
															</div>
														</div>
													</div>
												</div>
												
												<div class="col-sm-4">
													
													<div id="user-professional-details">
														<div class="row">
															<div class="col-xs-12">
																<div class="header-tag">
																	
																</div>
															</div>
														</div>
														<dl class="m_t_20">
												     		<dt> Contact Person : </dt>
															<dd class="margin-bottom-5px">
																{{isset($data['institute_registration_detail']['contact_person']) ? $data['institute_registration_detail']['contact_person'] : '-'}}
												     		</dd>
												     		<dt> Contact Number : 
												     			@if($data['is_mob_verified'] == '1')
							                                        <i class="fa fa-check-circle green f-15" aria-hidden="true" title="Mobile Verified"></i>
							                                    @else
							                                        <i class="fa fa-minus-circle red f-15" aria-hidden="true" title="Mobile Unverified"></i>
							                                    @endif
												     		</dt>
															<dd class="margin-bottom-5px">
																{{isset($data['institute_registration_detail']['contact_number']) ? $data['institute_registration_detail']['contact_number'] : '-'}}
												     		</dd>
															<dt> Contact email :
																@if($data['is_email_verified'] == '1')
							                                        <i class="fa fa-check-circle green f-15" aria-hidden="true" title="Email Verified"></i>
							                                    @else
							                                        <i class="fa fa-minus-circle red f-15" aria-hidden="true" title="Email Unverified"></i>
							                                    @endif
															</dt>
															<dd class="margin-bottom-5px">
																{{isset($data['email']) ? $data['email'] : '-'}}
															</dd>
															<dt> Registered On :</dt>
															<dd class="margin-bottom-5px">
																{{ isset($data['created_at']) && !empty($data['created_at']) ? date('d-m-Y H:i:s', strtotime($data['created_at']))  : '-'}}
															</dd>
														</dl>
													</div>
												</div>
												
												@if(isset($data['institute_registration_detail']['institute_locations']) && !empty($data['institute_registration_detail']['institute_locations']))
													<div class="col-sm-4">
														<div class="header-tag">
															Location Details
														</div>
														<div id="user-professional-details">
															@foreach($data['institute_registration_detail']['institute_locations'] as $index => $location)
																@if($index < 2)
																<dt> Location {{$index+1}} :</dt>
																<dd class="margin-bottom-5px">
																	@if(isset($location['country']) && $location['country'] == '95')
																		{{isset($location['country']) ? $location['city']['name'].' , '. $location['state']['name'] : '-'}}
																	@endif
																</dd>
																<dt> Is Head Branch :</dt>
																<dd class="margin-bottom-5px">
																	{{isset($location['headbranch']) ? $location['headbranch'] == 1 ? 'Yes' : 'No' : 'No'}}
																</dd>
																@endif
															@endforeach
														</div>
													</div>
												@endif
												
											</div>
										</div>
															
										</div>
										</div>
										</div>
															
										</div>
										@endforeach

											@if(isset($pagination) && !empty($pagination))
											
												<div class="panel-heading">
													<div class="row">
														<div class="col-xs-12">
															<div class="col-xs-6 paginator-content  pull-right no-margin">
																<ul id="top-user-paginator" class="pagination">
																	{!! $pagination->render() !!}
																</ul>
															</div>
															
															<div class="pull-left search-count-content">
																Showing {{$data_array['from']}} - {{$data_array['to']}} of {{$data_array['total']}} Results
															</div>
														</div>
													</div>
												</div>
											@endif
										@else
											<div class="no-results-found">No results found. Try again with different search criteria.</div>
										@endif
									</div>
								</div>
							</div>
						</div>
						<!-- end: PAGE CONTENT-->
					</div>
					<div class="subviews">
						<div class="subviews-container"></div>
					</div>
				</div>
				<!-- end: PAGE -->
			</div>
</div>

	<script type="text/javascript">
        
        var searchAjaxCall = "undefined";

        $(document).ready(function(){

            //init history plugin to perform search without page refresh
            // Establish Variables
            History = window.History; // Note: We are using a capital H instead of a lower h

            // Bind to State Change
            // Note: We are using statechange instead of popstate
			
            History.Adapter.bind(window,'statechange', function() {

                // Log the State
                var State = History.getState(); // Note: We are using History.getState() instead of event.state

                //start search (defined)
                fetchSearchResults(State.url, 
                                    function() {
                                        
                                    }, function() {

                                    });

            });

            

        });

        function fetchSearchResults(url, successCallback, errorCallback)
	    {
	    	
	    	var temp_current_state_url = url.split('?');
            if(temp_current_state_url.length > 1 && temp_current_state_url[1] != ''){
                var parameters = temp_current_state_url[1].split('&');

                var dropdown_elements = ['company_type','state','city'];

                for(var i=0; i<parameters.length; i++){

                    var param_array = parameters[i].split('=');
                    if($.inArray(param_array[0],dropdown_elements) > -1){
                        $('select[name="'+param_array[0]+'"]').val(param_array[1]);
                    }else if($.inArray(param_array[0],['availability_from','availability_to','first_name']) > -1){
                        $('input[name="'+param_array[0]+'"]').val(param_array[1]);
                    }

                }
            } else {
             	$(':input','#candidate-filter-form')
	                .not(':button, :submit, :reset, :hidden')
	                .val('')
	                .removeAttr('checked')
	                .removeAttr('selected');
	             }

	        if(searchAjaxCall != "undefined")
	        {
	            searchAjaxCall.abort();
	            searchAjaxCall = "undefined";
	        }

	        $('.seafarer-listing-container').addClass('hide');
	        $('.full-cnt-loader').removeClass('hidden');

	        searchAjaxCall = $.ajax({
	            url: url,
	            cache: false,
	            dataType: 'json',
	            statusCode: {
	                200: function(response) {
						$('.search-results-main-container').html(response.template);
	                    if (typeof response.data != 'undefined' && response.data.status == 'success') {
	                       
                        	$('.seafarer-listing-container').removeClass('hide');
	                        $('.search-results-main-container').html(response.template);
	                        $('.full-cnt-loader').addClass('hidden');

	                        if(typeof successCallback != "undefined")
	                        {
	                            successCallback();
	                        }

	                    }
	                }
	            },
	            error: function(error, response) {

	                if(typeof errorCallback != "undefined")
	                {
	                    errorCallback();
	                }
	            }
	        });
	    }
    </script>

@stop
@section('js_script')
    <script type="text/javascript" src="/js/site/institute-registration.js"></script>
@stop