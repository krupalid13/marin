@extends('admin.index')

@section('content')

<?php
	$initialize_pincode_maxlength_validation = false;
	$india_value = array_search('India',\CommonHelper::countries());
?>

<div class="container">
	<div class="toolbar row">
        <div class="col-sm-12">
            <div class="page-header">
                <h1>Batch<small>Add Batch</small></h1>
            </div>
        </div>
    </div>
    
	<div class="row">
		<div class="col-sm-12 m-t-25">
			<!-- start: FORM WIZARD PANEL -->
			<div class="panel panel-white">
				<div class="panel-body">
					 <form id="add-institute-batch" class="smart-wizard form-horizontal" method="post" action="{{ isset($data['current_route']) ? route('admin.institute.update.course.batch',isset($data['id']) ? $data['id'] : '') : route('admin.institute.store.course.batch') }}">
		                {{ csrf_field() }}
		               <div id="wizard1" class="swMain">
							<div id="step-1">
								<h2 class="StepTitle">Add Course Batches</h2>
								<div class="alert alert-danger alert-box" id="alert-box-documents" style="display: none">
		                            <ul>
		                                @foreach ($errors->all() as $error)
		                                    <li>{{ $error}}</li>
		                                @endforeach
		                            </ul>
		                    	</div> 		                  
		                    	<div class="panel-heading">
									<h4 class="panel-title">COURSE DETAILS</h4>
								</div>
								
		                    	<div class="form-group">
	                                <label class="col-sm-3 control-label" for="institute_name">Institute Name<span class="symbol required"></label>
		                            <div class="col-sm-7">
		                                <select id="institute_id" name="institute_id" class="form-control institute_name">
		                                    <option value=''>Select Institute Name</option>
	                                    	@foreach($institutes as $index => $type)
		                                        <option value="{{$type['id']}}" {{ isset($institute_id	) ? $institute_id	 == $type['id'] ? 'selected' : '' : ''}}>{{$type['institute_name']}}</option>
		                                    @endforeach
		                                </select>
		                             </div>
		                        </div>

	                            <div class="form-group">
	                                <label class="col-sm-3 control-label" for="course_type">Course Type<span class="symbol required"></label>
		                            <div class="col-sm-7">
		                                <select id="course_type" name="course_type" class="form-control">
		                                    <option value=''>Select Course Type</option>
		                                    @if(isset($course_type) AND !empty($course_type))
			                                    @foreach($course_type as $index => $category)
			                                        <option value="{{$index}}" {{ isset($data['course_type']) ? $data['course_type'] == $index ? 'selected' : '' : ''}}>{{$category}}</option>
			                                    @endforeach
			                                @endif
		                                </select>
		                             </div>
		                        </div>
	                            <div class="form-group">
	                                <label class="col-sm-3 control-label">Course Name<span class="symbol required"></label>
	                                <div class="col-sm-7">
		                                <select name="course_name" class="form-control institute_course_name">
		                                    <option value="">Select Course Name</option>
		                                    @if(isset($data['courses']) && !empty($data['courses']))
		                                        @foreach($data['courses'] as $index => $category)
		                                            <option value="{{$data['id']}}" {{ isset($data['course_type']) ? $data['course_type'] == $category['course_type'] ? 'selected' : '' : ''}}>{{$category['course_name']}}</option>
		                                        @endforeach
		                                    @endif
		                                </select>
		                            </div>
		                        </div>

			                    <!-- <div class="row hide" id="other_course_name"> -->
			                    
	                            <div class="form-group hide" id="other_course_name">
	                                <label class="col-sm-3 control-label">Other Course Name<span class="symbol required"></label>
                                	<div class="col-sm-7">
	                                	<input type="text" class="form-control" class="other_course_name" name="other_course_name" placeholder="Type other course name">
	                                </div>
	                            </div>

	                            <div class="panel-heading">
									<h4 class="panel-title">BATCH DETAILS</h4>
								</div>

								<div id="future" class="tab-pane fade in active">
		                            @if(isset($data['institute_batches']) AND !empty($data['institute_batches']))
		                                @foreach($data['institute_batches'] as $key => $data)
		                                    <input type="hidden" class="total_block_index" value={{$key}}>
		                                    <div id="batch-template" style="padding-bottom: 10px;">
		                                        <input type="hidden" name="batch_id[{{$key}}]" value={{$data['id']}}>
		                                        <div class="">
			                                        <div class="col-xs-12">
			                                            <div class="control-label batch-name pull-left heading">Batch {{$key+1}}</div>
			                                            @if($key > 0)
			                                                <div class="pull-right batch-close-button close-button-0 p-t-10 heading">
			                                                    <i class="fa fa-times" aria-hidden="true"></i>
			                                                </div>
			                                            @else
			                                                <div class="pull-right batch-close-button hide close-button-0 p-t-10 heading">
			                                                    <i class="fa fa-times" aria-hidden="true"></i>
			                                                </div>
			                                            @endif
			                                        </div>
			                                    </div>

                                                <div class="row form-group no-margin">
	                                        		<div class="col-sm-3">
                                                    	<label class="control-label">Start Date<span class="symbol required"></label>
	                                                    <input type="text" class="form-control datepicker start_date" name="start_date[{{$key}}]" value="{{ isset($data['start_date']) ? date('d-m-Y',strtotime($data['start_date'])) : '' }}" placeholder="dd-mm-yyyy">
		                                            </div>

		                                            <div class="col-sm-3">
	                                                    <label class="control-label">Duration (Days)<span class="symbol required"></label>
	                                                    <input type="text" class="form-control duration" name="duration[{{$key}}]" value="{{ isset($data['duration']) ? $data['duration'] : '' }}" placeholder="Type your duration in days">
		                                            </div>
		                                       
		                                            <div class="col-sm-3">
	                                                    <label class="control-label" for="cost">Cost<span class="symbol required"></label>
	                                                    <input type="text" class="form-control cost" placeholder="Type your cost" name="cost[{{$key}}]" value="{{ isset($data['cost']) ? $data['cost'] : '' }}">
		                                            </div>
	                                                <div class="col-sm-3">
	                                                    <label class="control-label" for="size">Batch Size<span class="symbol required"></label>
	                                                    <input type="text" class="form-control size" placeholder="Type your batch size" name="size[{{$key}}]" value="{{ isset($data['size']) ? $data['size'] : '' }}">
		                                            </div>
		                                        </div>

		                                        <?php
		                                            $location_ids = '';

		                                            if(isset($data['institute_location']) && !empty($data['institute_location'])){
		                                                $location_ids = array_values(collect($data['institute_location'])->pluck('location_id')->toArray());
		                                                $locations = $data['institute_location'];
		                                            }
		                                            //dd($location[0]['institute_registration_detail']['institute_locations'],$data['institute_location'],$location_ids);
		                                        ?>
		                                        <div class="row form-group no-margin">
		                                            <div class="col-sm-6">
	                                                    <label class="control-label" for="location">Institute Location<span class="symbol required"></span></label>
	                                                    <select multiple="multiple" name="location[{{$key}}][]" class="form-control location select2-select" style="height: auto;    min-width: 480px;padding: 0px;" placeholder="Please select location">

	                                                    @if(isset($location[0]['institute_registration_detail']['institute_locations']))
	                                                        @foreach($location[0]['institute_registration_detail']['institute_locations'] as $index => $data)

	                                                            @if($data['country'] == '95')

	                                                                @if(!empty($location_ids) && in_array($data['id'],$location_ids))
	                                                                    <option value="{{$data['id']}}" selected="selected">{{$data['city']['name']}}, {{$data['state']['name']}}</option>
	                                                                    
	                                                                @else
	                                                                    <option value="{{$data['id']}}">{{$data['city']['name']}}, {{$data['state']['name']}}</option>
	                                                                @endif

	                                                            @endif
	                                                        @endforeach
	                                                    @endif
	                                                    </select>
	                                                    <label class="location_error hide">Please select location</label>
		                                            </div>
		                                        </div>
		                                    </div>
		                                @endforeach
		                            @else
		                                <div id="batch-template" style="padding-bottom: 10px;">
		                                	<div class="">
			                                    <div class="col-xs-12">
			                                        <div class="control-label batch-name pull-left heading">Batch 1</div>
			                                        <div class="pull-right batch-close-button hide close-button-0 p-t-10 heading">
			                                            <i class="fa fa-times" aria-hidden="true"></i>
			                                        </div>
			                                    </div>
			                                </div>
		                                    <input type="hidden" class="total_block_index" value="0">
		                                    
                                            <div class="form-group no-margin">
	                                        	<div class="col-sm-3">
	                                            	<label class="control-label" for="ship">Start Date<span class="symbol required"></label>
	                                                <input type="text" class="form-control datepicker start_date" name="start_date[0]" value="{{ isset($data['start_date']) ? date('d-m-Y',strtotime($data['start_date'])) : '' }}" placeholder="dd-mm-yyyy">
	                                             </div>
	                                        
                                            
	                                        	<div class="col-sm-3">
	                                                <label class="control-label">Duration (Days)<span class="symbol required"></label>
	                                                <input type="text" class="form-control duration" name="duration[0]" value="{{ isset($data['duration']) ? $data['duration'] : '' }}" placeholder="Type your duration in days">
	                                            </div>
	                                        
	                                        	<div class="col-sm-3">
	                                                <label class="control-label" for="cost">Cost<span class="symbol required"></label>
	                                                <input type="text" class="form-control cost" placeholder="Type your cost" name="cost[0]" value="{{ isset($data['cost']) ? $data['cost'] : '' }}">
	                                            </div>
	                                       
	                                        	<div class="col-sm-3">
	                                                <label class="control-label" for="size">Batch Size<span class="symbol required"></label>
	                                                <input type="text" class="form-control size" placeholder="Type your batch size" name="size[0]" value="{{ isset($data['size']) ? $data['size'] : '' }}">
	                                            </div>
	                                        </div>
		                                   
		                                    <?php
		                                        $location_ids = '';
		                                        if(isset($data['batch_location']) && !empty($data['batch_location'])){
		                                            $location_ids = array_values(collect($data['batch_location'])->pluck('location_id')->toArray());
		                                        }

		                                    ?>

		                                    <div class="form-group no-margin">
		                                        <div class="col-sm-6">
	                                                <label class="control-label" for="location">Institute Location<span class="symbol required"></span></label>
	                                                <select multiple="multiple" name="location[0][]" class="form-control location select2-select" style="height: auto;min-width: 480px;padding: 0px;" placeholder="Please select location">
		                                                @if(isset($location[0]['institute_registration_detail']['institute_locations']))
		                                                    @foreach($location[0]['institute_registration_detail']['institute_locations'] as $index => $data)
		                                                        @if($data['country'] == '95')

		                                                            @if(!empty($location_ids) && in_array($data['id'],$location_ids))
		                                                                <option value="{{$data['id']}}" selected="selected">{{$data['city']['name']}}, {{$data['state']['name']}}</option>
		                                                            @else
		                                                                <option value="{{$data['id']}}">{{$data['city']['name']}}, {{$data['state']['name']}}</option>
		                                                            @endif
		                                                        @endif
		                                                    @endforeach
		                                                @endif
	                                                </select>
	                                                <label class="location_error hide">Please select location</label>
	                                            </div>
		                                    </div>
		                                </div>
		                            @endif
		                            <hr>
		                            <div class="row" id="add-more-batch-row">
		                                <div class="col-sm-12">
		                                    <button type="button" data-form-id="seafarer-certificates-form" class="btn add-more-button add-course-batch-button" id="add-batch">
		                                    Add More Batch</button>
		                                </div>
		                            </div>
		                        </div>  
	                        		
		                        <div class="form-group">
                                    <div class="col-sm-2 col-sm-offset-10">
                                        <button type="button" data-style="zoom-in" class="btn btn-blue ladda-button btn-block advertise-details-submit" id="addinstitutebatchbtn">
                                            Save <i class="fa fa-arrow-circle-right"></i>
                                        </button>
                                    </div>
                                </div>
		                    </div>
		                </div>
		                </div>
		            </form>
				</div>
			</div>
		</div>
	</div>
</div>

@stop

@section('js_script')
	<script type="text/javascript" src="/js/site/institute-registration.js"></script>
	<script type="text/javascript">
      
      $('.select2-select').select2({
         closeOnSelect: false
      });
    </script>
@stop