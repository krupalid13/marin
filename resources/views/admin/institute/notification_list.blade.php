@extends('admin.index')

@section('content')

<div class="main-wrapper">
	<div class="main-container1">
				<!-- start: PAGE -->
				<div class="main-content">
					
					<div class="container">
						<!-- start: PAGE HEADER -->
						<!-- start: TOOLBAR -->
						<div class="toolbar row">
							<div class="col-xs-12 col-sm-6">
								<div class="page-header">
									<h1>Notifications <small>A list of all notifications</small></h1>
								</div>
							</div>
						</div>

						<div class="row search-results-main-container m_t_20">
							<div class="col-md-12">
								<div class="panel panel-white">
									<div class="panel-body">
										@if(isset($data) AND !empty($data))
											<?php $pagination_data = $data->toArray(); ?>
										
											<div class="panel-heading">
												<div class="row">
													<div class="col-xs-12">
														<div class="col-xs-6 paginator-content  pull-right no-margin">
															<ul id="top-user-paginator" class="pagination">
																{!! $data->render() !!}
															</ul>
														</div>
														
														<div class="pull-left search-count-content">
															Showing {{$pagination_data['from']}} - {{$pagination_data['to']}} of {{$pagination_data['total']}} Notifications
														</div>
													</div>
												</div>
											</div>
														
											<div class="full-cnt-loader hidden">
											    <div class="" style="text-align: center;padding-top:20px;padding-bottom:20px; ">
											        <i class="fa fa-spinner fa-spin fa-4" aria-hidden="true" style="font-size: 7em;"></i>
											    </div>
											</div>		
											<div class="seafarer-listing-container">
												<div class="panel-body">
													<div class="row">
														<div class="col-sm-12">
															<div class="notification-content-main-view">
															@foreach($notifications['data'] as $notification)
																<div class="row">
																	<div class="col-xs-12 col-sm-12">
																		<div class="notification-content">
																			{!!$notification['message']!!}
																		</div>
																	</div>
																</div>
															@endforeach
															</div>
														</div>
																		
													</div>
												</div>
																
											</div>

											@if(isset($pagination_data) && !empty($pagination_data))
											
												<div class="panel-heading">
													<div class="row">
														<div class="col-xs-12">
															<div class="col-xs-6 paginator-content  pull-right no-margin">
																<ul id="top-user-paginator" class="pagination">
																	{!! $data->render() !!}
																</ul>
															</div>
															
															<div class="pull-left search-count-content">
																Showing {{$pagination_data['from']}} - {{$pagination_data['to']}} of {{$pagination_data['total']}} Notifications
															</div>
														</div>
													</div>
												</div>
											@endif
										@else
											<div class="no-results-found">No results found. Try again with different search criteria.</div>
										@endif
									</div>
								</div>
							</div>
						</div>
						<!-- end: PAGE CONTENT-->
					</div>
					<div class="subviews">
						<div class="subviews-container"></div>
					</div>
				</div>
				<!-- end: PAGE -->
			</div>
</div>

@stop
@section('js_script')
    <script type="text/javascript" src="/js/site/institute-registration.js"></script>
@stop