@extends('admin.index')

@section('content')

<?php
	$initialize_pincode_maxlength_validation = false;
	$india_value = array_search('India',\CommonHelper::countries());
?>

<div class="container">
	<div class="toolbar row">
        <div class="col-sm-12">
            <div class="page-header">
                <h1>Institute<small>Add Institute</small></h1>
            </div>
        </div>
    </div>
	<div class="row">
		<div class="col-sm-12 m-t-25">
			<!-- start: FORM WIZARD PANEL -->
			<div class="panel panel-white">
				<div class="panel-body">
					<form id="institute-registration-form" class="smart-wizard form-horizontal" action="{{ isset($user_data['current_route']) ? route('admin.update.institute',$user_data[0]['id']) : route('admin.store.institute')}}" 
					method="post">
						{{ csrf_field() }}
						<div id="wizard1" class="swMain">
							<div id="step-1">
								<h2 class="StepTitle">Login Details</h2>
								<div class="panel-heading">
									<h4 class="panel-title">ACCOUNT CREDENTIALS</h4>
								</div>
								<div class="alert alert-danger alert-box" id="alert-box-documents" style="display: none">
		                            <ul>
		                                @foreach ($errors->all() as $error)
		                                    <li>{{ $error}}</li>
		                                @endforeach
		                            </ul>
		                    	</div>
		                    	<input type="hidden" name="institute_id" id="institute_id" value="{{ isset($user_data[0]['id']) ? $user_data[0]['id'] : ''}}">

	                                <div class="form-group">
										<label class="col-sm-3 control-label">
											Login Email Address <span class="symbol required"></span>
										</label>
                                       	<div class="col-sm-7">
	                                        <input type="email" class="form-control" id="email" name="email" placeholder="Type your email address"
	                                               value="{{ isset($user_data[0]['email']) ? $user_data[0]['email'] : ''}}">
	                                        <span class="hide" id="email-error" style="color: red"></span>
	                                    </div>
                                    </div>
									@if(!isset($user_data['current_route']))
										<div class="form-group">
											<label class="col-sm-3 control-label">
												Password <span class="symbol required"></span>
											</label>
											<div class="col-sm-7">
												<input type="password" class="form-control" id="password" name="password" placeholder="Type your password">
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-3 control-label">
												Confirm Password <span class="symbol required"></span>
											</label>
											<div class="col-sm-7">
												<input type="password" class="form-control" id="cpassword" name="cpassword" placeholder="Type password again">
											</div>
										</div>
									@endif
									<div class="form-group">
										<label class="col-sm-3 control-label">
											Contact Person Name <span class="symbol required"></span>
										</label>
										<div class="col-sm-7">
											<input type="text" class="form-control" id="contact_person" name="contact_person" placeholder="Type your contact person name" value="{{ isset($user_data[0]['institute_registration_detail']['contact_person']) ? $user_data[0]['institute_registration_detail']['contact_person'] : ''}}">
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label">
											Contact Person Number <span class="symbol required"></span>
										</label>
										<div class="col-sm-7">
											<input type="text" class="form-control" id="contact_person_number" name="contact_person_number" placeholder="Type your contact person number" value="{{ isset($user_data[0]['institute_registration_detail']['contact_number']) ? $user_data[0]['institute_registration_detail']['contact_number'] : ''}}">
										</div>
									</div>
									<div class="panel-heading">
										<h4 class="panel-title">INSTITUTE DETAILS</h4>
									</div>
									<div class="form-group company-registration">
										<input type="hidden" name="image-x" required>
                                        <input type="hidden" name="image-y" required>
                                        <input type="hidden" name="image-x2" required>
                                        <input type="hidden" name="image-y2" required>
                                        <input type="hidden" name="crop-w" required>
                                        <input type="hidden" name="crop-h" required>
                                        <input type="hidden" name="image-w" required>
                                        <input type="hidden" name="image-h" required>
                                        <input type="hidden" name="role" id="role" value="admin">
                                        <input type="hidden" name="uploaded-file-name" required>
                                        <input type="hidden" name="uploaded-file-path" required>
										
										<div class="row no-margin">
                                            <div class="col-sm-7 col-sm-offset-3">
		                                        <div class="upload-photo-container">
				                                    <div class="image-content">
				                                        <div class="registration-profile-image">
				                                            <?php
				                                            $image_path = '';
				                                            if(isset($user_data[0]['profile_pic'])){
				                                                $image_path = env('INSTITUTE_LOGO_PATH')."".$user_data[0]['id']."/".$user_data[0]['profile_pic'];
				                                            }
				                                            ?>
				                                            @if(empty($image_path))
				                                                <div class="icon profile_pic_text p-t-30"><i class="fa fa-camera" aria-hidden="true"></i></div>
				                                                <div class="image-text profile_pic_text">Upload Logo <br> Picture</div>
				                                            @endif

				                                            <input type="file" name="profile_pic" id="profile-pic" class="cursor-pointer" onChange="profilePicSelectHandler('institute-registration-form', 'profile_pic', 'logo' , 'institute')">

				                                            @if(!empty($image_path))
				                                                <img id="preview" class="preview" src="/{{ $image_path }}">
				                                            @else
				                                                <img id="preview">
				                                            @endif
				                                        </div>
				                                    </div>
				                                </div>
				                            </div>
				                        </div>
									</div>
									
									<div class="form-group">
										<label class="col-sm-3 control-label">
											Institute Name <span class="symbol required"></span>
										</label>
										<div class="col-sm-7">
											<input type="text" class="form-control" id="institute_name" name="institute_name" placeholder="Type institute name" value="{{ isset($user_data[0]['institute_registration_detail']['institute_name']) ? $user_data[0]['institute_registration_detail']['institute_name'] : ''}}">
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label">
											Description <span class="symbol required"></span>
										</label>
										<div class="col-sm-7">
											<input type="text" class="form-control" id="institute_description" name="institute_description" placeholder="Type your institute description" value="{{ isset($user_data[0]['institute_registration_detail']['institute_detail']['institute_description']) ? $user_data[0]['institute_registration_detail']['institute_detail']['institute_description'] : ''}}">
										</div>
									</div>
									<div class="form-group pincode-block-0">
										<label class="col-sm-3 control-label">
											Institute Email ID <span class="symbol required"></span>
										</label>
										<div class="col-sm-7">
											<input type="text" class="form-control" id="institute_email" name="institute_email" placeholder="Type institute email" value="{{ isset($user_data[0]['institute_registration_detail']['institute_detail']['institute_email']) ? $user_data[0]['institute_registration_detail']['institute_detail']['institute_email'] : ''}}">
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label">
											Institute Phone Number <span class="symbol required"></span>
										</label>
										<div class="col-sm-7">
											<input type="text" class="form-control" id="cmpany_contact_number" name="institute_contact_number" placeholder="Please enter mobile number" value="{{ isset($user_data[0]['institute_registration_detail']['institute_detail']['institute_contact_number']) ? $user_data[0]['institute_registration_detail']['institute_detail']['institute_contact_number'] : ''}}">
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label">
											Institute Website
										</label>
										<div class="col-sm-7 ">
											<div class="input-group">
												<span class="input-group-addon">http://</span>
													<input type="text" class="form-control" id="website" name="website" placeholder="Type your website address" value="{{ isset($user_data[0]['institute_registration_detail']['website']) ? $user_data[0]['institute_registration_detail']['website'] : ''}}">
													<label for="website" class="error"></label>
		                                        </div>
		                                    </div>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label">
											Institute Fax
										</label>
										<div class="col-sm-7">
											<input type="text" class="form-control" id="fax" name="fax" placeholder="Type your fax number" value="{{ isset($user_data[0]['institute_registration_detail']['institute_detail']['fax']) ? $user_data[0]['institute_registration_detail']['institute_detail']['fax'] : ''}}">
										</div>
									</div>

									<div class="panel-heading">
									<h4 class="panel-title">LOCATION DETAILS</h4>
								</div>
								<input type="hidden" name="user_id" value="{{isset($user_data[0]['id']) ? $user_data[0]['id'] : ''}}">
								
								<div>
									 @if(isset($user_data[0]['institute_registration_detail']['institute_locations']) && !empty($user_data[0]['institute_registration_detail']['institute_locations']))
	                                    @foreach($user_data[0]['institute_registration_detail']['institute_locations'] as $index => $value)
	                                    <input type="hidden" class="total_location_block_index" value={{$index}}>

	                                    <div id="locations-template" class="locations-template multiple-info">
	                                    	<input type="hidden" class="location_id" name="location_id[{{$index}}]" value={{$value['id']}}>
	                                        <div class="row">
	                                            <div class="col-xs-12">
	                                                <div class="input-label p-t-10 location-name pull-left">Location {{$index+1}}</div>
	                                                @if($index > 0)
	                                                    <div class="pull-right p-t-10 close-button close-button-{{$index}}">
	                                                        <i class="fa fa-times" aria-hidden="true"></i>
	                                                    </div>
	                                                @else
	                                                    <div class="pull-right p-t-10 close-button hide close-button-{{$index}}">
	                                                        <i class="fa fa-times" aria-hidden="true"></i>
	                                                    </div>
	                                                @endif
	                                            </div>
	                                        </div>
	                                        <div class="form-group">
	                                            <div class="col-sm-3">
	                                                <div>
	                                                    <label class="input-label">Country<span class="symbol required"></span></label>
	                                                    <select id='country' name="country[{{$index}}]" class="form-control country" block-index="{{$index}}">
	                                                        <option value="">Select Your Country</option>
	                                                        @foreach( \CommonHelper::countries() as $c_index => $country)
	                                                            <option value="{{ $c_index }}" {{ isset($value['country']) ? $value['country'] == $c_index ? 'selected' : '' : ''}}> {{ $country }}</option>
	                                                        @endforeach
	                                                    </select>
	                                                </div>
	                                            </div>
	                                            <div class="col-sm-3">
	                                                <div class="pincode-block pincode-block-{{$index}}">
	                                                    <label class="input-label" for="pin_code">Postal Code<span class="symbol required"></span></label>
	                                                    <div>
	                                                        <input type="hidden" class="pincode-id" name="pincode_id[{{$index}}]" value="{{ isset($value['pincode_id']) ? $value['pincode_id'] : ''}}">
	                                                        
	                                                        <i class="fa fa-spin fa-refresh select-loader hide pincode-loader pincode-loader-{{$index}}"></i>
	                                                        <input type="text" block-index="{{$index}}" data-form-id="institute-registration-form" class="form-control pincode pin_code_fetch_input" name="pincode[{{$index}}]" value="{{ isset($value['pincode_text']) ? $value['pincode_text'] : ''}}" placeholder="Type your pincode">
	                                                    </div>
	                                                </div>
	                                            </div>
	                                            <div class="col-sm-3">
	                                                <div class="state-block state-block-{{$index}}">
	                                                    <label class="input-label">State<span class="symbol required"></span></label>
	                                                    @if(isset($value))
	                                                        @if($value['country'] == $india_value )
	                                                        <select id="state" name="state[{{$index}}]" class="form-control state fields-for-india">
	                                                            <option value="">Select Your State</option>
	                                                            @foreach($value['pincode']['pincodes_states'] as $pincode_states)
	                                                                <option value="{{$pincode_states['state_id']}}" {{$pincode_states['state_id'] == $value['state_id'] ? 'selected' : ''}}>{{ucwords(strtolower($pincode_states['state']['name']))}}</option>
	                                                            @endforeach
	                                                        </select>
	                                                        <input type="text" id="state" name="state_name[{{$index}}]" class="form-control state_text hide fields-not-for-india" placeholder="Enter State Name" value="">

	                                                        @else
	                                                            <select id="state" name="state[{{$index}}]" class="form-control state hide fields-for-india">
	                                                                <option value="">Select Your State</option>
	                                                            </select>
	                                                            <input type="text" name="state_text[{{$index}}]" class="form-control state_text fields-not-for-india" placeholder="Enter State Name" value="{{ isset($value['state_text']) ? $value['state_text'] : ''}}">
	                                                        @endif
	                                                    @else
	                                                        <select id="state" name="state[{{$index}}]" class="form-control state hidden fields-for-india">
	                                                            <option value="">Select Your State</option>
	                                                        </select>
	                                                        <input type="text" id="state" name="state_text[{{$index}}]" class="form-control state_text fields-not-for-india" placeholder="Enter State Name" value="{{ isset($value['state_text']) ? $value['state_text'] : ''}}">
	                                                    @endif
	                                                </div>
	                                            </div>
	                                            <div class="col-sm-3">
	                                                <div class="city-block city-block-{{$index}}">
	                                                    <label class="input-label">City<span class="symbol required"></span></label>

	                                                    @if(isset($value['country']))
	                                                        @if( $value['country'] == $india_value )
	                                                            <select id="city" name="city[{{$index}}]" class="form-control city fields-for-india">
	                                                                <option value="">Select Your City</option>
	                                                                @foreach($value['pincode']['pincodes_cities'] as $pincode_city)
	                                                                    <option value="{{$pincode_city['city_id']}}" {{$pincode_city['city_id'] == $value['city_id'] ? 'selected' : ''}}>{{ucwords(strtolower($pincode_city['city']['name']))}}</option>
	                                                                @endforeach
	                                                            </select>
	                                                            <input type="text" name="city_text[{{$index}}]" class="form-control city_text hide fields-not-for-india" placeholder="Enter City Name" value="">

	                                                        @else
	                                                            <select id="city" name="city[{{$index}}]" class="form-control city hide fields-for-india">
	                                                                <option value="">Select Your City</option>
	                                                            </select>

	                                                            {{--<input type="text" name="city_text[{{$index}}]" class="form-control city_text fields-not-for-india" placeholder="Enter City Name" value="">--}}
	                                                            <input type="text" name="city_text[{{$index}}]" class="form-control city_text fields-not-for-india" placeholder="Enter City Name" value="{{ isset($value['city_text']) ? $value['city_text'] : ''}}">
	                                                        @endif
	                                                    @else
	                                                        <select id="city" name="city[{{$index}}]" class="form-control city hide fields-for-india">
	                                                            <option value="">Select Your City</option>
	                                                        </select>

	                                                        {{--<input type="text" name="city_text[{{$index}}]" class="form-control city_text fields-not-for-india" placeholder="Enter City Name" value="">--}}
	                                                        <input type="text" name="city_text[{{$index}}]" class="form-control city_text fields-not-for-india" placeholder="Enter City Name" value="{{ isset($value['city_text']) ? $value['city_text'] : ''}}">
	                                                    @endif
	                                                </div>
	                                            </div>
	                                        </div>
	                                        <div class="form-group">
	                                            <div class="col-sm-3">
	                                                <div class="address-block address-block-{{$index}}">
	                                                    <label class="input-label" for="address">Address<span class="symbol required"></span></label>
	                                                    <textarea class="form-control address" id="address" name="address[{{$index}}]" placeholder="Type your address" rows="5">{{ isset($value['address']) ? $value['address'] : ''}}</textarea>
	                                                </div>
	                                            </div>
	                                            <div class="col-sm-3">
	                                                <div class="row">
	                                                    <div class="col-xs-6">
	                                                        <div>
	                                                            <label class="input-label" for="is_headoffice">Is Headoffice<span class="symbol required"></span></label>
	                                                           <div class="row">
		                                                    		<div class="col-sm-6">
			                                                            <label class="radio-inline">
			                                                                <input type="radio" class="is_headoffice" name="is_headoffice[{{$index}}]" value="1" {{ !empty($value['headbranch']) ? $value['headbranch'] == 1 ? 'checked' : '' : ''}}> Yes
			                                                            </label>
			                                                        </div>
			                                                        <div class="col-sm-6">
			                                                            <label class="radio-inline">
			                                                                <input type="radio" class="is_headoffice" name="is_headoffice[{{$index}}]" value="0" {{ !empty($value['headbranch']) ? $value['headbranch'] == 0 ? 'checked' : '' : 'checked'}}> No
			                                                            </label>
			                                                        </div>
		                                                        </div>
	                                                        </div>
	                                                    </div>
	                                                </div>
	                                            </div>
	                                        </div>
	                                    	<hr>
	                                    </div>
	                                    @endforeach
	                                @else
	                                    <div id="locations-template" class="locations-template multiple-info">
	                                    	<input type="hidden" class="total_location_block_index" value='0'>
	                                        <div class="row form-group">
	                                            <div class="col-xs-12">
	                                                <div class="input-label p-t-10 location-name pull-left">Location 1</div>
	                                                <div class="pull-right close-button hide close-button-0">
	                                                    <i class="fa fa-times" aria-hidden="true"></i>
	                                                </div>
	                                            </div>
	                                        </div>
	                                        <div class="form-group">
	                                            <div class="col-sm-3">
	                                                <div>
	                                                    <label class="input-label">Country<span class="symbol required"></span></label>
	                                                    <select name="country[0]" id='country' class="form-control country" block-index="0">
	                                                        <option value="">Select Your Country</option>
	                                                        @foreach( \CommonHelper::countries() as $c_index => $country)
	                                                            @if(isset($value['country']))
	                                                                <option value="{{ $c_index }}" {{ isset($value['country']) ? $value['country'] == $c_index ? 'selected' : '' : ''}}> {{ $country }}</option>
	                                                            @else
	                                                                <option value="{{ $c_index }}" {{ $india_value == $c_index ? 'selected' : ''}}> {{ $country }}</option>
	                                                            @endif
	                                                        @endforeach
	                                                    </select>
	                                                </div>
	                                            </div>
	                                            <div class="col-sm-3">
	                                                <div class="pincode-block pincode-block-0">
	                                                    <label class="input-label" for="pin_code">Postal Code<span class="symbol required"></span></label>
	                                                    <div>
	                                                        <input type="hidden" class="pincode-id" name="pincode_id[0]" value="{{ isset($value['pincode_id']) ? $value['pincode_id'] : ''}}">
	                                                        
	                                                        <i class="fa fa-spin fa-refresh select-loader hide pincode-loader pincode-loader-0"></i>
	                                                        <input type="text" block-index="0" data-form-id="institute-registration-form" class="form-control pincode pin_code_fetch_input" name="pincode[0]" value="{{ isset($value['pincode_text']) ? $value['pincode_text'] : ''}}" placeholder="Type your pincode">
	                                                    </div>
	                                                </div>
	                                            </div>
	                                            <div class="col-sm-3">
	                                                <div class="state-block state-block-0">
	                                                    <label class="input-label">State<span class="symbol required"></span></label>
	                                                    @if(isset($value))
	                                                        @if($value['country'] == $india_value )
	                                                        <select name="state[0]" class="form-control state fields-for-india">
	                                                            <option value="">Select Your State</option>
	                                                            @foreach($value['pincode']['pincodes_states'] as $pincode_states)
	                                                                <option value="{{$pincode_states['state_id']}}" {{$pincode_states['state_id'] == $value['state_id'] ? 'selected' : ''}}>{{ucwords(strtolower($pincode_states['state']['name']))}}</option>
	                                                            @endforeach
	                                                        </select>
	                                                        <input type="text" id="state" name="state_name[0]" class="form-control state_text hide fields-not-for-india" placeholder="Enter State Name" value="">

	                                                        @else
	                                                            <select name="state[0]" class="form-control state hide fields-for-india">
	                                                                <option value="">Select Your State</option>
	                                                            </select>
	                                                            <input type="text" name="state_text[0]" class="form-control state_text fields-not-for-india" placeholder="Enter State Name" value="{{ isset($value['state_text']) ? $value['state_text'] : ''}}">
	                                                        @endif
	                                                    @else
	                                                        <select name="state[0]" class="form-control state fields-for-india">
	                                                            <option value="">Select Your State</option>
	                                                        </select>
	                                                        <input type="text" id="state" name="state_text[0]" class="form-control hide state_text fields-not-for-india" placeholder="Enter State Name" value="{{ isset($value['state_text']) ? $value['state_text'] : ''}}">
	                                                    @endif
	                                                </div>
	                                            </div>
	                                            <div class="col-sm-3">
	                                                <div class="city-block city-block-0">
	                                                    <label class="input-label">City<span class="symbol required"></span></label>

	                                                    @if(isset($value['country']))
	                                                        @if( $value['country'] == $india_value )
	                                                            <select name="city[0]" class="form-control city fields-for-india">
	                                                                <option value="">Select Your City</option>
	                                                                @foreach($value['pincode']['pincodes_cities'] as $pincode_city)
	                                                                    <option value="{{$pincode_city['city_id']}}" {{$pincode_city['city_id'] == $value['city_id'] ? 'selected' : ''}}>{{ucwords(strtolower($pincode_city['city']['name']))}}</option>
	                                                                @endforeach
	                                                            </select>
	                                                            <input type="text" name="city_text[0]" class="form-control city_text hide fields-not-for-india" placeholder="Enter City Name" value="">

	                                                        @else
	                                                            <select name="city[0]" class="form-control city hide fields-for-india">
	                                                                <option value="">Select Your City</option>
	                                                            </select>

	                                                            {{--<input type="text" name="city_text[0]" class="form-control city_text fields-not-for-india" placeholder="Enter City Name" value="">--}}
	                                                            <input type="text" name="city_text[0]" class="form-control city_text fields-not-for-india" placeholder="Enter City Name" value="{{ isset($value['city_text']) ? $value['city_text'] : ''}}">
	                                                        @endif
	                                                    @else
	                                                        <select name="city[0]" class="form-control city fields-for-india">
	                                                            <option value="">Select Your City</option>
	                                                        </select>

	                                                        {{--<input type="text" name="city_text[0]" class="form-control city_text fields-not-for-india" placeholder="Enter City Name" value="">--}}
	                                                        <input type="text" name="city_text[0]" class="form-control city_text hide fields-not-for-india" placeholder="Enter City Name" value="{{ isset($value['city_text']) ? $value['city_text'] : ''}}">
	                                                    @endif
	                                                </div>
	                                            </div>
	                                        </div>
	                                        <div class="form-group">
	                                            <div class="col-sm-3">
	                                                <div class="address-block address-block-0">
	                                                    <label class="input-label" for="address">Address<span class="symbol required"></span></label>
	                                                    <textarea class="form-control address" name="address[0]" placeholder="Type your address" rows="5">{{ isset($value['address']) ? $value['address'] : ''}}</textarea>
	                                                </div>
	                                            </div>
	                                            <div class="col-sm-3">
	                                                <div class="row">
	                                                    <div class="col-xs-12">
                                                            <label class="input-label" for="is_headoffice">Is Head Branch<span class="symbol required"></span></label>
                                                            <div class="row">
		                                                    	<div class="col-sm-3">
		                                                            <label class="radio-inline">
		                                                                <input type="radio" class="is_headoffice" name="is_headoffice[0]" value="1" {{ !empty($value['headbranch']) ? $value['headbranch'] == 1 ? 'checked' : '' : ''}}> Yes
		                                                            </label>
		                                                        </div>
		                                                       	<div class="col-sm-3">
		                                                            <label class="radio-inline">
		                                                                <input type="radio" class="is_headoffice" name="is_headoffice[0]" value="0" {{ !empty($value['headbranch']) ? $value['headbranch'] == 0 ? 'checked' : '' : 'checked'}}> No
		                                                            </label>
		                                                        </div>
	                                                        </div>
	                                                    </div>
	                                                </div>
	                                            </div>
	                                        </div>
	                                    </div>
	                                    <hr>
	                                @endif
	                            </div>

                                <div class="row no-margin m-b-25" id="add-more-location-row">
                                    <div class="col-sm-12">
                                        <button type="button" data-form-id="institute-registration-form" class="btn add-institute-location-button location-button-0 add-more-button pull-right" id="add-location">Add Location</button>
                                    </div>
                                </div>
								<div class="form-group">
									<div class="col-sm-2 col-sm-offset-10">
										<button type="button" data-style="zoom-in" class="btn btn-blue next-step btn-block company-next-btn ladda-button" id="institute-details-submit">
											Save <i class="fa fa-arrow-circle-right"></i>
										</button>
									</div>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

@stop

@section('js_script')
	<script type="text/javascript" src="/js/site/institute-registration.js"></script>
	<script>
		jQuery(document).ready(function() {
			FormWizard.init();
		});
	</script>
@stop