@extends('admin.index')

@section('content')

<div class="main-wrapper">
	<div class="main-container1">
				<!-- start: PAGE -->
				<div class="main-content">
					<div class="container">
						<!-- start: PAGE HEADER -->
						<!-- start: TOOLBAR -->
						<div class="toolbar row">
							<div class="col-sm-6 hidden-xs">
								<div class="page-header">
									<h1>Institute Courses<small>A list of all institute courses</small></h1>
								</div>

							</div>
							<div class="col-sm-6 hidden-xs">
								<div class="page-header">
									<button type="button" class="btn btn-success pull-right" data-toggle="modal" data-target="#addCourseModal" style="margin: 25px 0 0 0;">
			                            <i class="fa fa-plus-circle nav-icon" aria-hidden="true"></i>
				                            Add Course
			                        </button>
			                    </div>
							</div>
						</div>

						<div class="row search-results-main-container m-t-25">
							<div class="col-md-12">
								<div class="panel panel-white">
									<div class="panel-body">
										@if(isset($data) && !empty($data))
											
											<div class="panel-heading">
												<div class="row">
													<div class="col-xs-12">
														<div class="col-xs-6 paginator-content  pull-right no-margin">
															<ul id="top-user-paginator" class="pagination">
																{!! $pagination->render() !!}
															</ul>
														</div>
														
														<div class="pull-left search-count-content">
															Showing {{$data['from']}} - {{$data['to']}} of {{$data['total']}} Results
														</div>

													</div>
												</div>
											</div>
											
											<div class="full-cnt-loader hidden">
											    <div class="" style="text-align: center;padding-top:20px;padding-bottom:20px; ">
											        <i class="fa fa-spinner fa-spin fa-4" aria-hidden="true" style="font-size: 7em;"></i>
											    </div>
											</div>
											
											@foreach($data['data'] as $course)
											<?php 
                                                $disable= 'hide';
                                                $enable= 'hide';
                                                if(isset($course) && $course['status'] == 0){
                                                    $enable = '';
                                                }else{
                                                    $disable = '';
                                                }
                                            ?>
											<div class="seafarer-listing-container">
												<div class="panel">
													<div class="panel-heading border-light partition-blue">
														<h5 class="panel-title text-bold header-link-text p-5-5">
															<a href="{{route('admin.view.institute.id',$course['institute_id'])}}">
																{{ isset($course['institute_registration_details']['institute_name']) ? $course['institute_registration_details']['institute_name'] : ''}}
															</a>
														</h5>
														<ul class="panel-heading-tabs border-light">
															<li class="panel-tools">
																<div class="dropdown">
																	<a data-toggle="dropdown" class="btn btn-xs dropdown-toggle btn-transparent-grey">
																		<i class="fa fa-cog"></i>
																	</a>
																	<ul class="dropdown-menu dropdown-light pull-right" role="menu">
																		 <li>
				                                                            <a class="courseDisableButton disable-{{$course['id']}} {{$disable}}" data-status='disable' data-id="{{$course['id']}}">
				                                                                Deactivate
				                                                            </a>
				                                                        </li>
				                                                        
				                                                        <li>
				                                                            <a class="courseDisableButton enable-{{$course['id']}} {{$enable}}" data-status='enable' data-id="{{$course['id']}}">
				                                                                Activate
				                                                            </a>
				                                                        </li>
																		
																	</ul>
																</div>
															</li>
														</ul>

													</div>
																	
													<div class="panel-body">
														<div class="row">
															
															<div class="col-lg-3 col-md-5 col-sm-5" id="user-profile-pic" style="height: 200px;width: 200px; margin-left: 15px;">
																<a>
																	<div class="company_profile_pic">
									                					<img src="{{!empty($course['user']['profile_pic']) ? "/".env('INSTITUTE_LOGO_PATH').$course['institute_id']."/".$course['user']['profile_pic'] : asset('images/user_default_image.png') }}" avatar-holder-src="{{ asset('images/user_default_image.png') }}" class="job-applicant-user-profile-pic">
									                				</div>
						                            			</a>
															</div>
															<div class="col-lg-9 col-md-7 col-sm-7">
																<div class="row">																
																	<div class="col-sm-4">		
																		<div id="user-professional-details">
																			<div class="row">
																				<div class="col-xs-12">
																					<div class="header-tag">
																						Seafarer Details
																					</div>
																				</div>
																			</div>
																			<dl>
																	     		<dt> Institute Name : </dt>
																					<dd class="margin-bottom-5px">
																						{{ isset($course['institute_registration_details']['institute_name']) ? $course['institute_registration_details']['institute_name'] : ''}}
																		     		</dd>
																	     		<dt>
																				<dt> Course Type :</dt>
																				<dd class="margin-bottom-5px">
																					@foreach(\CommonHelper::institute_course_types() as $r_index => $course_type)
						                                                                {{ !empty($course['course_type']) ? $course['course_type'] == $r_index ? $course_type : '' : ''}}
						                                                            @endforeach
																				</dd>
																				
																				<dt> Course Name :</dt>
																				<dd class="margin-bottom-5px">
																					{{ isset($course['courses'][0]['course_name']) ? $course['courses'][0]['course_name'] : '-'}}
																				</dd>

																				<dt> Status :</dt>
																				<dd class="margin-bottom-5px">
																					<span class="{{$disable}} disable-{{$course['id']}}">
						                                                                Activate
						                                                            </span>
						                                                            <span class="{{$enable}} enable-{{$course['id']}}">
						                                                                Deactivate
						                                                            </span>
																				</dd>
																			</dl>
																		</div>
																	</div>
																</div>
															</div>
																			
														</div>
													</div>
												</div>
																
											</div>
											@endforeach
										@else
											<div class="no-results-found">No results found. Try again with different search criteria.</div>
										@endif

										@if(isset($data) && !empty($data))
											
											<div class="panel-heading">
												<div class="row">
													<div class="col-xs-12">
														<div class="col-xs-6 paginator-content  pull-right no-margin">
															<ul id="top-user-paginator" class="pagination">
																{!! $pagination->render() !!}
															</ul>
														</div>
														
														<div class="pull-left search-count-content">
															Showing {{$data['from']}} - {{$data['to']}} of {{$data['total']}} Results
														</div>
													</div>
												</div>
											</div>
										@endif
										
									</div>
								</div>
							</div>
						</div>
						<!-- end: PAGE CONTENT-->
					</div>
					<div class="subviews">
						<div class="subviews-container"></div>
					</div>
				</div>
				<!-- end: PAGE -->
			</div>
</div>

<div id="addCourseModal" class="modal fade" role="dialog">
    <form id="add-institute-course" method="post" action="{{ isset($data['current_route']) ? route('admin.institute.update.course.batches',$data['id']) : route('admin.institute.store.course') }}">
    {{ csrf_field() }}
        <div class="modal-dialog">

        <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add Course</h4>
                </div>
                <div class="modal-body">
                    <div class="job-discription-card">
                        <div class="row">
                        	<div class="col-sm-6">
                                <div class="form-group">
                                    <label class="input-label" for="institute_id">Institute Name<span class="symbol required"></label>
                                    <select id="institute_id" name="institute_id" class="form-control">
                                        <option value=''>Select Institute Name</option>
                                        @foreach($institutes as $index => $category)
                                            <option value="{{$category['institute_id']}}">{{$category['institute_name']}}</option>
                                        @endforeach
                                    </select>
                                 </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="input-label" for="course_type">Course Type<span class="symbol required"></label>
                                    <select id="course_type" name="course_type" class="form-control">
                                        <option value=''>Select Course Type</option>
                                        @foreach(\CommonHelper::institute_course_types() as $index => $category)
                                            <option value="{{$index}}" {{ isset($data['course_type']) ? $data['course_type'] == $index ? 'selected' : '' : ''}}>{{$category}}</option>
                                        @endforeach
                                    </select>
                                 </div>
                            </div>
                        
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="input-label">Course Name<span class="symbol required"></label>
                                    <select name="course_name" class="form-control institute_course_name">
                                        <option value="">Select Course Name</option>
                                        @if(isset($data['courses']) && !empty($data['courses']))

                                            @foreach($data['courses'] as $index => $category)
                                                <option value="{{$category['course_type']}}" {{ isset($data['course_type']) ? $data['course_type'] == $category['course_type'] ? 'selected' : '' : ''}}>{{$category['course_name']}}</option>
                                            @endforeach
                                            <option value="other">Other</option>
                                        @endif
                                    </select>
                                </div>
                            </div>

                        	<div class="col-sm-6 hide" id="other_course_name">
                                <div class="form-group">
                                    <label class="input-label">Other Course Name<span class="symbol required"></label>
                                    <input type="text" class="form-control" class="other_course_name" name="other_course_name" placeholder="Type other course name">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger ladda-button" data-style="zoom-in" id="addinstitutecoursebtn">Save</button>
                </div>
            </div>

        </div>
    </form>
</div>

<script type="text/javascript">
        
    var searchAjaxCall = "undefined";

    $(document).ready(function(){
        History = window.History; // Note: We are using a capital H instead of a lower h
		
        History.Adapter.bind(window,'statechange', function() {

            var State = History.getState(); // Note: We are using History.getState() instead of event.state

            fetchSearchResults(State.url, 
            function() {
                
            }, function() {

            });

        }); 

    });

    function fetchSearchResults(url, successCallback, errorCallback)
    {
    	
    	var temp_current_state_url = url.split('?');
        if(temp_current_state_url.length > 1 && temp_current_state_url[1] != ''){
            var parameters = temp_current_state_url[1].split('&');
            var dropdown_elements = ['course_type','course_name'];

            for(var i=0; i<parameters.length; i++){

                var param_array = parameters[i].split('=');
                if($.inArray(param_array[0],dropdown_elements) > -1){
                    $('select[name="'+param_array[0]+'"]').val(param_array[1]);
                }else if($.inArray(param_array[0],['seafarer_name','start_date']) > -1){
                    $('input[name="'+param_array[0]+'"]').val(param_array[1]);
                }
            }
        } else {
         	$(':input','#candidate-filter-form')
                .not(':button, :submit, :reset, :hidden')
                .val('')
                .removeAttr('checked')
                .removeAttr('selected');
             }

        if(searchAjaxCall != "undefined")
        {
            searchAjaxCall.abort();
            searchAjaxCall = "undefined";
        }

        $('.seafarer-listing-container').addClass('hide');
        $('.full-cnt-loader').removeClass('hidden');

        searchAjaxCall = $.ajax({
            url: url,
            dataType: 'json',
            statusCode: {
                200: function(response) {
					$('.search-results-main-container').html(response.template);
                    if (typeof response.data != 'undefined' && response.data.status == 'success') {
                       
                        $('.seafarer-listing-container').removeClass('hide');
                        $('.search-results-main-container').html(response.template);
                        $('.full-cnt-loader').addClass('hidden');

                        if(typeof successCallback != "undefined")
                        {
                            successCallback();
                        }

                    }
                }
            },
            error: function(error, response) {

                if(typeof errorCallback != "undefined")
                {
                    errorCallback();
                }
            }
        });
    }
</script>

@stop
@section('js_script')
    <script type="text/javascript" src="/js/site/institute-registration.js"></script>
@stop