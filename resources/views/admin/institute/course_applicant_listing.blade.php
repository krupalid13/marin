@extends('admin.index')

@section('content')

<div class="main-wrapper">
	<div class="main-container1">
				<!-- start: PAGE -->
				<div class="main-content">
					<div class="container">
						<!-- start: PAGE HEADER -->
						<!-- start: TOOLBAR -->
						<div class="toolbar row">
							<div class="col-sm-6 hidden-xs">
								<div class="page-header">
									<h1>Course Applicants<small>A list of all course applicants</small></h1>
								</div>
							</div>
						</div>

						<form class="filter-search-form m_l_10">
							<div class="row m_t_25">
								<div class="col-md-12">
									<div class="panel panel-white">
										<div class="panel-body">
											<div class="row">
					                            <div class="col-sm-3">
					                                <div class="form-group">
					                                    <label>Seafarer Name </label>
					                                    <input type="text" class="form-control" name="seafarer_name" id="seafarer_name" value="{{isset($data['seafarer_name']) ? $data['seafarer_name'] : ''}}" placeholder="Enter Seafarer Name">
					                                </div>
					                            </div>
												<div class="col-sm-3">
					                                <div class="form-group">
					                                    <label>Course Type </label>
					                                    <select name="course_type" class="form-control course_type_filter">
						                                    <option value=''>Select Course Type</option>
						                                    @foreach(\CommonHelper::institute_course_types() as $r_index => $rank)
					                                            <option value="{{$r_index}}" {{ isset($filter['course_type']) ? $filter['course_type'] == $r_index ? 'selected' : '' : ''}}>{{$rank}} </option>
					                                        @endforeach
						                                </select>
					                                </div>
					                            </div>
					                            <div class="col-sm-3">
					                                <div class="form-group">
					                                    <label>Course Name </label>
					                                    <select id="course_name" name="course_name" class="form-control institute_course_name">
						                                    <option value=''>Select Course Name</option>
						                                    @if(isset($course_name) && !empty($course_name))
							                                    @foreach($course_name as $r_index => $name)
							                                        <option value="{{$r_index}}" {{ isset($filter['course_name']) ? $filter['course_name'] == $name['course_type'] ? 'selected' : '' : ''}}>{{$name['course_title']}} </option>
							                                    @endforeach
							                                @endif
						                                </select>
					                                </div>
					                            </div>
					                            <div class="col-sm-3">
					                                <div class="form-group">
					                                    <label>Start Date </label>
					                                    <input type="text" name="start_date" class="form-control datepicker" value="{{isset($filter['start_date']) ? !empty($filter['start_date']) ? date('d-m-Y',strtotime($filter['start_date'])) : '' : ''}}" placeholder="dd-mm-yyyy">
					                                </div>
					                            </div>
					                            
					                            <div class="col-sm-12">
					                                <div class="section_cta text-right m_t_25 job-btn-container">
					                                    <button type="button" class="btn btn-danger coss-inverse-btn search-reset-btn" id="seafarerResetButton">Reset</button>
					                                    <button type="submit" data-style="zoom-in" class="btn btn-info coss-primary-btn search-post-btn ladda-button candidate-filter-search-button" id="candidate-filter-search-button"><i class="fa fa-search search-icon" aria-hidden="true"></i>Search</button>
					                                </div>
					                            </div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</form>

						 <div class="row search-results-main-container m-t-25">
							<div class="col-md-12">
								<div class="panel panel-white">
									<div class="panel-body">
										@if(isset($data) && !empty($data))
											
											<div class="panel-heading">
												<div class="row">
													<div class="col-xs-12">
														<div class="col-xs-6 paginator-content  pull-right no-margin">
															<ul id="top-user-paginator" class="pagination">
																{!! $pagination->render() !!}
															</ul>
														</div>
														
														<div class="pull-left search-count-content">
															Showing {{$pagination_data['from']}} - {{$pagination_data['to']}} of {{$pagination_data['total']}} Results
														</div>
													</div>
												</div>
											</div>
											
											<div class="full-cnt-loader hidden">
											    <div class="" style="text-align: center;padding-top:20px;padding-bottom:20px; ">
											        <i class="fa fa-spinner fa-spin fa-4" aria-hidden="true" style="font-size: 7em;"></i>
											    </div>
											</div>
			
											@foreach($data as $data)	
											<div class="seafarer-listing-container">
												<div class="panel">
													<div class="panel-heading border-light partition-blue">
														<h5 class="panel-title text-bold header-link-text p-5-5">
															<a href="">
																{{ isset($data['user']['first_name']) ? $data['user']['first_name'] : ''}}
															</a>
														</h5>
														<ul class="panel-heading-tabs border-light">
															<li class="panel-tools">
																<div class="dropdown">
																	<a data-toggle="dropdown" class="btn btn-xs dropdown-toggle btn-transparent-grey">
																		<i class="fa fa-cog"></i>
																	</a>
																	<ul class="dropdown-menu dropdown-light pull-right" role="menu">
																		<!-- <li>
																			<a href="{{Route('admin.view.advertiser.id',$data['id'])}}" data-type="user">
																				View
																			</a>
																		</li> -->
																		<li>
																			<a href="{{Route('admin.view.seafarer.id',[$data['user']['id']])}}">
																				View Profile
																			</a>
																		</li>
																		
																		
																	</ul>
																</div>
															</li>
														</ul>

													</div>
																	
													<div class="panel-body">
														<div class="row">
															
															<div class="col-lg-3 col-md-5 col-sm-5" id="user-profile-pic" style="height: 200px;width: 200px; margin-left: 15px;">
																<a>
																	<div class="company_profile_pic">
									                					<img src="{{!empty($data['user']['profile_pic']) ? "/".env('SEAFARER_PROFILE_PATH').$data['user_id']."/".$data['user']['profile_pic'] : 'images/user_default_image.png'}}" avatar-holder-src="{{ asset('images/user_default_image.png') }}" class="job-applicant-user-profile-pic">
									                				</div>
						                            			</a>
															</div>
															<div class="col-lg-9 col-md-7 col-sm-7">
																<div class="row">																
																	<div class="col-sm-4">		
																		<div id="user-professional-details">
																			<div class="row">
																				<div class="col-xs-12">
																					<div class="header-tag">
																						Seafarer Details
																					</div>
																				</div>
																			</div>
																			<dl>
																	     		<dt> Seafarer Name : </dt>
																					<dd class="margin-bottom-5px">
																						{{ isset($data['user']['first_name']) ? $data['user']['first_name'] : ''}}
																		     		</dd>
																	     		<dt>
																				<dt> Seafarer Current Rank :</dt>
																				<dd class="margin-bottom-5px">
																					@foreach(\CommonHelper::new_rank() as $rank_index => $category)
														                                @foreach($category as $r_index => $rank)
														                                    {{ isset($data['user']['professional_detail']['current_rank']) ? $data['user']['professional_detail']['current_rank'] == $r_index ? $rank : '' : ''}}
														                                @endforeach
														                            @endforeach
																				</dd>
																				<?php
													                                if(isset($data['user']['professional_detail']['current_rank_exp'])){
													                                    $rank_exp = explode(".",$data['user']['professional_detail']['current_rank_exp']);
													                                    $rank_exp[1] = number_format($rank_exp[1]);
													                                }
													                            ?>
																				<dt> Rank Experience :</dt>
																				<dd class="margin-bottom-5px">
																					{{ isset($data['user']['professional_detail']['current_rank_exp']) ? $rank_exp[0] ." Years ". $rank_exp[1] . " Months" : '-'}}
																				</dd>

																				<dt> Last Wages Drawn :</dt>
																				<dd class="margin-bottom-5px">
																					{{isset($data['user']['professional_detail']['currency']) ? $data['user']['professional_detail']['currency'] == 'dollar' ? '$' : 'Rs' : 'Rs'}}

																					{{ isset($data['user']['professional_detail']['last_salary']) ? $data['user']['professional_detail']['last_salary'] : '-'}} <b>Per Month</b>
																				</dd>
																			</dl>
																		</div>
																	</div>
																	<div class="col-sm-4">		
																		<div id="user-professional-details">
																			<div class="row">
																				<div class="col-xs-12">
																					<div class="header-tag">
																						Batch Details
																					</div>
																				</div>
																			</div>
																			<dl>
																	     		<dt> Batch Id : </dt>
																					<dd class="margin-bottom-5px">
																						{{ isset($data['id']) ? $data['id'] : ''}}
																		     		</dd>
																	     		<dt>
																				
																				<dt> Course Type :</dt>
																				<dd class="margin-bottom-5px">
																					@if(isset($data['batch_details']['course_details'][0]['course_type']) && !empty($data['batch_details']['course_details'][0]['course_type']))
																						@foreach(\CommonHelper::institute_course_types() as $r_index => $rank)
												                                            {{ isset($data['batch_details']['course_details'][0]['course_type']) ? $data['batch_details']['course_details'][0]['course_type'] == $r_index ? $rank : '' : ''}}
												                                        @endforeach
												                                    @else
												                                    	-
												                                    @endif
																				</dd>
																				<dt> Course Name :</dt>
																				<dd class="margin-bottom-5px">
																					@if(isset($data['batch_details']['course_details'][0]['course_id']) && !empty($data['batch_details']['course_details'][0]['course_id']))
												                                        @foreach($all_courses as $r_index => $course1)
												                                        	{{ isset($data['batch_details']['course_details'][0]['course_id']) ? $data['batch_details']['course_details'][0]['course_id'] == $course1['id'] ? $course1['course_name'] : '' : ''}}
												                                        @endforeach
											                                        @else
												                                    	-
												                                    @endif
																				</dd>
																			</dl>
																		</div>
																	</div>
																	<div class="col-sm-4">		
																		<div id="user-professional-details">
																			<div class="row">
																				<div class="col-xs-12">
																					<div class="header-tag m_t_20">
																						 
																					</div>
																				</div>
																			</div>
																			<dl>
																	     		<dt> Start Date : </dt>
																					<dd class="margin-bottom-5px">
																						{{ isset($data['batch_details']['start_date']) && !empty($data['batch_details']['start_date']) ? date('d-m-Y',strtotime($data['batch_details']['start_date'])) : '-'}}
																		     		</dd>
																	     		<dt>
																				<dt> Cost :</dt>
																				<dd class="margin-bottom-5px">
																					<i class="fa fa-inr fa-sm"></i> 
																					{{ isset($data['batch_details']['cost']) && !empty($data['batch_details']['cost']) ? $data['batch_details']['cost'] : '-'}}
																				</dd>
																			</dl>
																		</div>
																	</div>
																</div>
															</div>
																			
														</div>
													</div>
												</div>
																
											</div>
											@endforeach
												@if(isset($data) && !empty($data))
												
												<div class="panel-heading">
													<div class="row">
														<div class="col-xs-12">
															<div class="col-xs-6 paginator-content  pull-right no-margin">
																<ul id="top-user-paginator" class="pagination">
																	{!! $pagination->render() !!}
																</ul>
															</div>
															
															<div class="pull-left search-count-content">
																Showing {{$pagination_data['from']}} - {{$pagination_data['to']}} of {{$pagination_data['total']}} Results
															</div>
														</div>
													</div>
												</div>
											@endif
										@else
											<div class="no-results-found">No results found. Try again with different search criteria.</div>
										@endif
									</div>
								</div>
							</div>
						</div>
						<!-- end: PAGE CONTENT-->
					</div>
					<div class="subviews">
						<div class="subviews-container"></div>
					</div>
				</div>
				<!-- end: PAGE -->
			</div>
</div>

<script type="text/javascript">
        
    var searchAjaxCall = "undefined";

    $(document).ready(function(){
        History = window.History; // Note: We are using a capital H instead of a lower h
		
        History.Adapter.bind(window,'statechange', function() {

            var State = History.getState(); // Note: We are using History.getState() instead of event.state

            fetchSearchResults(State.url, 
            function() {
                
            }, function() {

            });

        }); 

    });

    function fetchSearchResults(url, successCallback, errorCallback)
    {
    	
    	var temp_current_state_url = url.split('?');
        if(temp_current_state_url.length > 1 && temp_current_state_url[1] != ''){
            var parameters = temp_current_state_url[1].split('&');
            var dropdown_elements = ['course_type','course_name'];

            for(var i=0; i<parameters.length; i++){

                var param_array = parameters[i].split('=');
                if($.inArray(param_array[0],dropdown_elements) > -1){
                    $('select[name="'+param_array[0]+'"]').val(param_array[1]);
                }else if($.inArray(param_array[0],['seafarer_name','start_date']) > -1){
                    $('input[name="'+param_array[0]+'"]').val(param_array[1]);
                }
            }
        } else {
         	$(':input','#candidate-filter-form')
                .not(':button, :submit, :reset, :hidden')
                .val('')
                .removeAttr('checked')
                .removeAttr('selected');
             }

        if(searchAjaxCall != "undefined")
        {
            searchAjaxCall.abort();
            searchAjaxCall = "undefined";
        }

        $('.seafarer-listing-container').addClass('hide');
        $('.full-cnt-loader').removeClass('hidden');

        searchAjaxCall = $.ajax({
            url: url,
            dataType: 'json',
            statusCode: {
                200: function(response) {
					$('.search-results-main-container').html(response.template);
                    if (typeof response.data != 'undefined' && response.data.status == 'success') {
                       
                        $('.seafarer-listing-container').removeClass('hide');
                        $('.search-results-main-container').html(response.template);
                        $('.full-cnt-loader').addClass('hidden');

                        if(typeof successCallback != "undefined")
                        {
                            successCallback();
                        }

                    }
                }
            },
            error: function(error, response) {

                if(typeof errorCallback != "undefined")
                {
                    errorCallback();
                }
            }
        });
    }
</script>

@stop
@section('js_script')
    <script type="text/javascript" src="/js/site/institute-registration.js"></script>
@stop