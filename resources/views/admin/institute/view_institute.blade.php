@extends('admin.index')

@section('content')
<div id="view-company-page">
	<div class="container main-content">
		<div class="toolbar row">
			<div class="col-sm-6 hidden-xs">
				<div class="page-header">
					<h1>Institute <small>View Institute Details</small></h1>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12 m_t_25">
				<div class="tabbable">
					<ul id="myTab2" class="nav nav-tabs">
						<li class="active">
							<a href="#institute_details" data-toggle="tab">
								Institute Details
							</a>
						</li>
                        <li>
                            <a href="#course_details" data-toggle="tab">
                                Course Details
                            </a>
                        </li>
                        <li>
                            <a href="#batch_details" class="batch_details" data-toggle="tab">
                                Batch Details
                            </a>
                        </li>
                        <li>
                            <a href="#institute_subscription" data-url="{{route('admin.get.user.subscription',['page' => 1,'role' => 'institute','institute_id' => isset($data['institute_data'][0]['id']) ? $data['institute_data'][0]['id'] : ''])}}">
                                Subscription
                            </a>
                        </li>
						<li class="pull-right">
							<a type="button" class="fa fa-edit" href="{{Route('admin.edit.institute',$data['user'][0]['id'])}}">
								Edit
							</a>
						</li>
					</ul>
					<div class="tab-content panel-body">
						<div class="tab-pane fade in active content" id="institute_details">
							<div class="panel-heading">
								<h4 class="panel-title">ACCOUNT CREDENTIALS</h4>
							</div>
                            <input class="hidden" id="institute_id" value="{{isset($data['institute_data'][0]['institute_registration_detail']['id']) ? $data['institute_data'][0]['institute_registration_detail']['id'] : ''}}"></input>
							<div class="row">
                                <label class="col-sm-3 control-label">
                                    Login Email Address:

                                    @if($data['user'][0]['is_email_verified'] == '1')
                                        <i class="fa fa-check-circle green f-15" aria-hidden="true" title="Email Verified"></i>
                                    @else
                                        <i class="fa fa-minus-circle red f-15" aria-hidden="true" title="Email Unverified"></i>
                                    @endif
                                </label>
                                <div class="col-sm-9">
                                	{{ isset($data['user'][0]['email']) ? $data['user'][0]['email'] : ''}}
                                </div>
                            </div>
                            <div class="row">
                                <label class="col-sm-3 control-label">
                                    Contact Person Name:
                                </label>
                                <div class="col-sm-9">
                                	{{ isset($data['user'][0]['first_name']) ? $data['user'][0]['first_name'] : ''}}
                                </div>
                            </div>

                            <div class="row">
                                <label class="col-sm-3 control-label">
                                    Contact Person Number:
                                    @if($data['user'][0]['is_mob_verified'] == '1')
                                        <i class="fa fa-check-circle green f-15" aria-hidden="true" title="Mobile Verified"></i>
                                    @else
                                        <i class="fa fa-minus-circle red f-15" aria-hidden="true" title="Mobile Unverified"></i>
                                    @endif
                                </label>
                                <div class="col-sm-9">
                                	{{ isset($data['institute_data'][0]['institute_registration_detail']['contact_number']) ? $data['institute_data'][0]['institute_registration_detail']['contact_number'] : ''}}
                                </div>
                            </div>

                            <div class="panel-heading">
								<h4 class="panel-title">INSTITUTE DETAILS</h4>
							</div>
                            
							<?php
	                            $institute_image_path = '';
	                            if(isset($data['institute_data'][0]['profile_pic'])){
	                                $institute_image_path = env('INSTITUTE_LOGO_PATH')."".$data['institute_data'][0]['id']."/".$data['institute_data'][0]['profile_pic'];
	                            }
	                        ?>
							<div class="row text-center" style="margin-left: 0px;">
								<div class="col-lg-3 col-md-5 col-sm-5 company_profile_pic">
	                				<img src="{{ !empty($institute_image_path) ? '/'.$institute_image_path : '/images/user_default_image.png'}}" >
								</div>
							</div>
							
							<div class="row">
                                <label class="col-sm-3 control-label">
                                    Institute Name:
                                </label>
                                <div class="col-sm-3">
                                    {{ isset($data['institute_data'][0]['institute_registration_detail']['institute_name']) ? $data['institute_data'][0]['institute_registration_detail']['institute_name'] : ''}}
                                </div>
                                <label class="col-sm-3 control-label">
                                    Institute Email ID:
                                </label>
                                <div class="col-sm-3">
                                    {{ isset($data['institute_data'][0]['institute_registration_detail']['email']) ? $data['institute_data'][0]['institute_registration_detail']['email'] : ''}}
                                </div>
                            </div>

                            <div class="row">
                                <label class="col-sm-3 control-label">
                                    Institute Phone Number:
                                </label>
                                <div class="col-sm-3">
                                    {{ isset($data['institute_data'][0]['institute_registration_detail']['institute_detail']['institute_contact_number']) ? $data['institute_data'][0]['institute_registration_detail']['institute_detail']['institute_contact_number'] : ''}}
                                </div>
                                <label class="col-sm-3 control-label">
                                    Institute Website:
                                </label>
                                <div class="col-sm-3">
                                    <a href="{{ !empty($data['institute_data'][0]['institute_registration_detail']['website']) ? 'http://'.$data['institute_data'][0]['institute_registration_detail']['website'] : ''}}" target="_blank">
                                        {{ !empty($data['institute_data'][0]['institute_registration_detail']['website']) ? $data['institute_data'][0]['institute_registration_detail']['website'] : '-'}}
                                    </a>
                                </div>
                            </div>

                            <div class="row">
                                <label class="col-sm-3 control-label">
                                    Institute Fax:
                                </label>
                                <div class="col-sm-3">
                                	{{ isset($data['institute_data'][0]['institute_registration_detail']['institute_detail']['fax']) ? $data['institute_data'][0]['institute_registration_detail']['institute_detail']['fax'] : '-'}}
                                </div>
                                <label class="col-sm-3 control-label">
                                    Description:
                                </label>
                                <div class="col-sm-3">
                                    {{ isset($data['institute_data'][0]['institute_registration_detail']['institute_detail']['institute_description']) ? $data['institute_data'][0]['institute_registration_detail']['institute_detail']['institute_description'] : ''}}
                                </div>
                            </div>
                            
                            <div class="panel-heading">
                                <h4 class="panel-title">LOCATION DETAILS</h4>
                            </div>
                            
                            @foreach($data['institute_data'][0]['institute_registration_detail']['institute_locations'] as $index => $location)
                                <div class="row">
                                    <div class="col-sm-3">
                                        Location {{$index+1}}
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-sm-3 control-label">
                                        Country:
                                    </label>
                                    <div class="col-sm-3">
                                        @foreach( \CommonHelper::countries() as $c_index => $country)
                                            {{ isset($location['country']) ? $location['country'] == $c_index ? $country : '' : ''}}
                                        @endforeach
                                    </div>
                                
                                    <label class="col-sm-3 control-label">
                                        State:
                                    </label>
                                    <div class="col-sm-3">
                                        {{ isset($location['state_id']) ? $location['pincode']['pincodes_states'][0]['state']['name'] : (isset($location['state_text']) ? $location['state_text'] : '')}}
                                    </div>
                                </div>

                                <div class="row">
                                    <label class="col-sm-3 control-label">
                                        City:
                                    </label>
                                    <div class="col-sm-3">
                                        {{ isset($location['city_id']) ? $location['pincode']['pincodes_cities'][0]['city']['name'] : (isset($location['city_text']) ? $location['city_text'] : '')}}
                                    </div>
                                
                                    <label class="col-sm-3 control-label">
                                        Pin Code:
                                    </label>
                                    <div class="col-sm-3">
                                        {{ isset($location['pincode_text']) ? $location['pincode_text'] : ''}}
                                    </div>
                                </div>

                                <div class="row">
                                    <label class="col-sm-3 control-label">
                                        Is Head Branch:
                                    </label>
                                    <div class="col-sm-3">
                                        {{ isset($location['headbranch']) ? $location['headbranch'] == '0' ? 'No' : 'Yes' : 'No'}}
                                    </div>
                                </div>
                            @endforeach

						</div>
                        <div id="course_details" class="tab-pane fade">
                            <div class="row panel-heading">
                                <div class="col-sm-6">
                                        <h4 class="panel-title">COURSE DETAILS</h4>
                                </div>
                                <div class="col-sm-6">
                                    <button type="button" class="btn btn-success pull-right" data-toggle="modal" data-target="#addCourseModal">
                                        <i class="fa fa-plus-circle nav-icon" aria-hidden="true"></i>
                                            Add Course
                                    </button>
                                </div>
                            </div>

                            @if(isset($data['institute_data'][0]['institute_registration_detail']['institute_courses']) AND !empty($data['institute_data'][0]['institute_registration_detail']['institute_courses']))
                                @foreach($data['institute_data'][0]['institute_registration_detail']['institute_courses'] as $index => $value)
                                    <div class="row">
                                        <label class="col-sm-3">
                                            Course {{$index+1}}
                                        </label>
                                    </div>
                                    <div class="row" style="padding-bottom: 8px;"> 
                                        <label class="col-sm-2 control-label">
                                            Course Name:
                                        </label> 
                                        <div class="col-sm-2">
                                            {{ isset($value['course_details']['course_name']) ? $value['course_details']['course_name'] : '-'}}
                                        </div>
                                        <label class="col-sm-2 control-label">
                                            Course Type:
                                        </label> 
                                        <div class="col-sm-2">
                                            @foreach(\CommonHelper::institute_course_types() as $r_index => $course_type)
                                                {{ isset($value['course_type']) ? $value['course_type'] == $r_index ? $course_type : '' : ''}}
                                            @endforeach
                                        </div>
                                        <?php 
                                            $disable= 'hide';
                                            $enable= 'hide';
                                            if(isset($value) && $value['status'] == 0){
                                                $enable = '';
                                            }else{
                                                $disable = '';
                                            }
                                        ?>
                                        <label class="col-sm-2 control-label">
                                            Status:
                                        </label> 
                                        <div class="col-sm-1">
                                            <span class="{{$disable}} disable-{{$value['id']}}">
                                                Activate
                                            </span>
                                            <span class="{{$enable}} enable-{{$value['id']}}">
                                                Deactivate
                                            </span>
                                        </div>
                                        <div class="col-sm-1">
                                            <div class="panel-heading">
                                                <ul class="panel-heading-tabs border-light">
                                                    <li class="panel-tools" style="padding: 0px;margin-top: -14px;">
                                                        <div class="dropdown">
                                                            <a data-toggle="dropdown" class="btn btn-xs dropdown-toggle btn-transparent-grey">
                                                                <i class="fa fa-cog"></i>
                                                            </a>
                                                            <ul class="dropdown-menu dropdown-light pull-right" role="menu">
                                                                 <li>
                                                                    <a class="courseDisableButton disable-{{$value['id']}} {{$disable}}" data-status='disable' data-id="{{$value['id']}}">
                                                                        Deactivate
                                                                    </a>
                                                                </li>
                                                                
                                                                <li>
                                                                    <a class="courseDisableButton enable-{{$value['id']}} {{$enable}}" data-status='enable' data-id="{{$value['id']}}">
                                                                        Activate
                                                                    </a>
                                                                </li>

                                                                <li>
                                                                    <a class="courseDeleteButton delete-{{$value['id']}}" data-id="{{$value['id']}}">
                                                                        Delete
                                                                    </a>
                                                                </li>
                                                                
                                                            </ul>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>

                                        </div>
                                    </div>
                                @endforeach
                            @else
                                No Courses Added Yet.
                            @endif
                        </div>
                        <div id="batch_details" class="tab-pane fade">
                            <div class="row panel-heading">
                                <div class="col-sm-6">
                                    <h4 class="panel-title">BATCH DETAILS</h4>
                                </div>
                                
                                <div class="col-sm-6">
                                    <a href="{{route('admin.add.course.batch',$data['user'][0]['id'])}}">
                                        <button type="button" class="btn btn-success pull-right">
                                            <i class="fa fa-plus-circle nav-icon" aria-hidden="true"></i>
                                                Add Batch
                                        </button>
                                    </a>
                                </div>
                            </div>
                            <?php
                                $institute_name = $data['institute_data'][0]['institute_registration_detail']['institute_name'];
                                $user_id = $data['institute_data'][0]['id'];
                            ?>
                            <div>
                                @if(isset($data['institute_data'][0]['institute_registration_detail']['data_array']) AND !empty($data['institute_data'][0]['institute_registration_detail']['data_array']))
                                    @foreach($data['institute_data'][0]['institute_registration_detail']['data_array'] as $institute)  
                                    <div class="seafarer-listing-container">
                                        <div class="panel">
                                            <div class="panel-heading border-light partition-blue" style="min-height: 50px !important;padding: 10px 15px;">
                                                <h5 class="panel-title text-bold header-link-text p-5-5">
                                                    <a href="">
                                                        {{isset($institute_name) ? $institute_name : ''}}
                                                        @foreach($all_courses as $index => $category)
                                                            {{ isset($institute['course_id']) ? $institute['course_id'] == $category['id'] ? '- '. $category['course_name'] : '' : ''}}
                                                        @endforeach
                                                    </a>
                                                </h5>
                                                
                                                <ul class="panel-heading-tabs border-light">
                                                <li class="panel-tools">
                                                    <div class="dropdown">
                                                        <a data-toggle="dropdown" class="btn btn-xs dropdown-toggle btn-transparent-grey">
                                                            <i class="fa fa-cog"></i>
                                                        </a>

                                                        <ul class="dropdown-menu dropdown-light pull-right" role="menu">
                                                            <!-- <li>
                                                                <a href="{{Route('admin.view.company.id',$institute['id'])}}" data-type="user">
                                                                    View
                                                                </a>
                                                            </li> -->
                                                            <li>
                                                                <a href="{{Route('admin.institute.edit.course.batch',[$user_id,$institute['id']])}}">
                                                                    Edit
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </li>
                                                </ul>

                                            </div>
                                                            
                                            <div class="panel-body">
                                                <div class="row">
                                                    
                                                    <div class="col-sm-12">
                                                        
                                                        <div id="user-professional-details">
                                                            <div class="row">
                                                                <div class="col-xs-12">
                                                                    <div class="header-tag">
                                                                        Course Details
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <dl class="col-sm-4">
                                                                <dt> Course Type : </dt>
                                                                    <dd class="margin-bottom-5px">
                                                                        @foreach(\CommonHelper::institute_course_types() as $index => $category)
                                                                            {{ isset($institute['course_type']) ? $institute['course_type'] == $index ? $category : '' : ''}}
                                                                        @endforeach
                                                                    </dd>
                                                                <dt>
                                                                <dt> Course Name :</dt>
                                                                <dd class="margin-bottom-5px">
                                                                    @foreach($all_courses as $index => $category)
                                                                        {{ isset($institute['course_id']) ? $institute['course_id'] == $category['id'] ? $category['course_name'] : '' : ''}}
                                                                    @endforeach
                                                                </dd>
                                                            </dl>
                                                            <dl class="col-sm-8">
                                                                <div id="user-professional-details">
                                                                    @if(isset($institute['institute_batches']) AND !empty($institute['institute_batches']))
                                                                        <?php

                                                                            $collection = collect($institute['institute_batches']);
                                                                            
                                                                            $batches = array_values($collection->sortBy('start_date')->toArray());
                                                                            $course['institute_batches'] = $batches;
                                                                            $Select = 'select';
                                                                            // dd($course['institute_batches']);
                                                                        ?>
                                                                        <dl>
                                                                            <dt>Batches Date: </dt>
                                                                            <span class="ans">
                                                                                @if(isset($institute['institute_batches'][0]))
                                                                                    <div class="batches height-equalizer-wrapper p-t-10">
                                                                                   
                                                                                        @foreach($institute['institute_batches'] as $key => $value)
                                                                                            <div class="date_slide date_slide_{{$value['institute_course_id']}} z-depth-1 {{ $key == 0 ? $Select : ''}}" data-id="{{$value['id']}}" data-key="{{$value['institute_course_id']}}">
                                                                                                {{isset($value['start_date']) ? date('d',strtotime($value['start_date'])) : ''}}
                                                                                                {{isset($value['start_date']) ? date('M',strtotime($value['start_date'])) : ''}}
                                                                                            </div>
                                                                                        @endforeach
                                                                                    </div>
                                                                                @else
                                                                                    No batches found.
                                                                                @endif
                                                                            </span>
                                                                        </dl>

                                                                        @foreach($institute['institute_batches'] as $key => $value)
                                                                            <div class="row details details-key-{{$value['institute_course_id']}} details-{{$value['id']}} {{ $key == 0 ? '' : 'hide'}}">
                                                                                <div class="col-sm-6">
                                                                                    <dl>
                                                                                        <dt>Start Date: </dt>
                                                                                        <span class="ans">
                                                                                            {{isset($value['start_date']) ? date('d-m-Y',strtotime($value['start_date'])) : ''}}
                                                                                        </span>
                                                                                    </dl>
                                                                                    <dl>
                                                                                        <dt>Duration:</dt> 
                                                                                        <span class="ans">
                                                                                            {{isset($value['duration']) ? $value['duration'] : ''}} Days
                                                                                        </span>
                                                                                    </dl>
                                                                                </div>
                                                                                <div class="col-sm-6">
                                                                                    <dl>
                                                                                        <dt>Cost:</dt> 
                                                                                        <span class="ans">
                                                                                            <i class="fa fa-inr"></i>
                                                                                            {{isset($value['cost']) ? $value['cost'] : ''}}
                                                                                        </span>
                                                                                    </dl>
                                                                                    <dl>
                                                                                        <dt>Size:</dt> 
                                                                                        <span class="ans">
                                                                                            {{isset($value['size']) ? $value['size'] : ''}}
                                                                                        </span>
                                                                                    </dl>
                                                                                </div>

                                                                                <?php
                                                                                    $location_ids = '';
                                                                                    $locations = '';
                                                                                    
                                                                                    if(isset($value['institute_location']) && !empty($value['institute_location'])){
                                                                                        $location_ids = array_values(collect($value['institute_location'])->pluck('location_id')->toArray());
                                                                                    }
                                                                                    
                                                                                    if(isset($institute))
                                                                                        $value1 = $institute;

                                                                                ?>

                                                                                <div class="col-sm-12">
                                                                                    <dl>
                                                                                        <dt>Location:</dt>
                                                                                        <span class="ans">
                                                                                            @if(isset($value1['institute_location']))
                                                                                                @foreach($value1['institute_location'] as $index => $data1)
                                                                                                    @if($data1['country'] == '95')
                                                                                                        @if(!empty($location_ids) && in_array($data1['id'],$location_ids))
                                                                                                            {{$data1['city']['name']}} - {{ucfirst(strtolower($data1['state']['name']))}},
                                                                                                        @endif

                                                                                                    @endif
                                                                                                @endforeach
                                                                                            @endif
                                                                                        </span>
                                                                                    </dl>
                                                                                </div>
                                                                            </div>
                                                                        @endforeach
                                                                    @else
                                                                        No batched found.
                                                                    @endif
                                                                </div>
                                                            </dl>
                                                        </div>
                                                    
                                                            
                                                    </div>
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                @else
                                        No Batches Added Yet.
                                @endif
                            </div>
                        </div>
                        
                        <div id="institute_subscription" class="tab-pane fade" data-id="{{ isset($data['institute_data'][0]['institute_registration_detail']['id']) ? $data['institute_data'][0]['institute_registration_detail']['id'] : ''}}" data-role='institute' data-institute="{{ isset($data['institute_data'][0]['id']) ? $data['institute_data'][0]['id'] : ''}}">
                            <div class="row panel-heading">
                                <div class="col-sm-6">
                                    <h4 class="panel-title">SUBSCRIPTION DETAILS</h4>
                                </div>
                            </div>
                            <div class="full-cnt-loader hidden">
                                <div class="" style="text-align: center;padding-top:20px;padding-bottom:20px; ">
                                    <i class="fa fa-spinner fa-spin fa-4" aria-hidden="true" style="font-size: 7em;"></i>
                                </div>
                            </div>
                            <div class="data">
                                No Subscriptions Found.
                            </div>
                            
                        </div>			
					</div>
                    
				</div>
			</div>
		</div>
	</div>
</div>

<div id="addCourseModal" class="modal fade" role="dialog">
    <form id="add-institute-course" method="post" action="{{ route('admin.institute.store.course') }}">
    {{ csrf_field() }}
        <div class="modal-dialog">

        <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add Course</h4>
                </div>

                <div class="modal-body">
                    <div class="job-discription-card">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="input-label" for="institute_id">Institute Name<span class="symbol required"></label>
                                    <select id="institute_id" name="institute_id" class="form-control">
                                        <option value=''>Select Institute Name</option>
                                        @foreach($institutes as $index => $category)
                                        
                                            <option value="{{$category['id']}}" {{isset($data['institute_data'][0]['id']) ? $data['institute_data'][0]['id'] == $category['user_id'] ? 'selected' : '' : ''}}>{{$category['institute_name']}}</option>
                                        @endforeach
                                    </select>
                                 </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="input-label" for="course_type">Course Type<span class="symbol required"></label>
                                    <select name="course_type" class="form-control course_type_other">
                                        <option value=''>Select Course Type</option>
                                        @foreach(\CommonHelper::institute_course_types() as $index => $category)
                                            <option value="{{$index}}" {{ isset($data['course_type']) ? $data['course_type'] == $index ? 'selected' : '' : ''}}>{{$category}}</option>
                                        @endforeach
                                    </select>
                                 </div>
                            </div>
                        
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="input-label">Course Name<span class="symbol required"></label>
                                    <select name="course_name" class="form-control institute_course_name">
                                        <option value="">Select Course Name</option>
                                        @if(isset($data['courses']) && !empty($data['courses']))

                                            @foreach($data['courses'] as $index => $category)
                                                <option value="{{$category['course_type']}}" {{ isset($data['course_type']) ? $data['course_type'] == $category['course_type'] ? 'selected' : '' : ''}}>{{$category['course_name']}}</option>
                                            @endforeach
                                            <option value="other">Other</option>
                                        @endif
                                    </select>
                                </div>
                            </div>

                            <div class="col-sm-6 hide" id="other_course_name">
                                <div class="form-group">
                                    <label class="input-label">Other Course Name<span class="symbol required"></label>
                                    <input type="text" class="form-control" class="other_course_name" name="other_course_name" placeholder="Type other course name">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger ladda-button" data-style="zoom-in" id="addinstitutecoursebtn">Save</button>
                </div>
            </div>

        </div>
    </form>
</div>
@stop
@section('js_script')
    <script type="text/javascript" src="/js/site/institute-registration.js"></script>
@stop


