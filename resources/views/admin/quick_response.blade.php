<form onkeydown="return event.key != 'Enter';" id="resume_form">
    <input type="hidden" name="_token" value="{{csrf_token()}}" />
    <div id="quick-response-model" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="form-group">
                        <h3>YEAH!</h3>
                    </div>
                    <div class="form-group">
                        <div class="radio">
                            <label>
                                <input type="radio" name="response_id" value="1" checked> Thank you, Your Resume has been received
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="radio">
                            <label>
                                <input type="radio" name="response_id" value="2"> Your Resume has been shortlisted, we shall notify you once approved
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="radio">
                            <label>
                                <input type="radio" name="response_id" value="3"> Your Resume has been approved, kindly contact me
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="radio">
                            <label>
                                <input type="radio" name="response_id" value="4"> There is an opening, kindly contact me
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="radio">
                            <label>
                                <input type="radio" name="response_id" value="11"> Kindly Share all relevant Documents and Certificates
                            </label>
                        </div>
                    </div>
                    <div id="contact_details_container" style="display: none;" class="form-group">
                        <div class="row">
                            <div class="col-md-5">
                                <input type="text" name="contact_name" id="contact_name" class="form-control" placeholder="Enter Your Contact Name" />
                            </div>
                            <div class="col-md-5">
                                <input type="text" name="contact_number" id="contact_number" class="form-control" placeholder="Enter Your Contact Number" />
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <h3>NAH!</h3>
                    </div>
                    <div class="form-group">
                        <div class="radio">
                            <label>
                                <input type="radio" name="response_id" value="7"> Kindly send your most updated resume
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="radio">
                            <label>
                                <input type="radio" name="response_id" value="8"> Sorry, but we have no openings at the moment, please try again after 6 months
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="radio">
                            <label>
                                <input type="radio" name="response_id" value="9"> Sorry, we do not hire your rank
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="radio">
                            <label>
                                <input type="radio" name="response_id" value="10"> Sorry, we can not hire your nationality
                            </label>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close
                    </button>
                    <button type="button" id="send_qr_message" onclick="Submit(event, 'resume')" data-endpoint="{{route('save-qr')}}" class="btn btn-primary">Send</button>
                </div>
            </div>

        </div>
    </div>
</form>