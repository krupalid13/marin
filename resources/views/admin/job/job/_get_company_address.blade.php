<div class="form-group">
<label class="col-sm-3 control-label">
	Select Email <span class="symbol required"></span>
</label>
	<div class="col-sm-6">
		{{ Form::select('company_email_address_id',$emailAddress,$setAddress,['placeholder'=>'select email address','id'=>'company_email_address_id','class'=>'form-control'])}}
	<span class="hide" id="company_email_address_id-error" style="color: red"></span>
</div>
</div>