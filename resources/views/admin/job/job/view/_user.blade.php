@extends('admin.index')

@section('style_section')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css">

<style>
	div#padding_to_add {
	    padding: 25px;
	}
</style>
<link rel="stylesheet" type="text/css" href="{{asset('public/assets/responsive/responsive.dataTables.min.css')}}">
@endsection

@section('content')
<?php 
	$countryData = \CommonHelper::countries();
    $dceData = \App\Dce::getDesDropdown();
    $allCourse = \App\Courses::getAllCourse();

    $requestAll = request()->all();

    $userName = null;
    $rankId = null;

    $jobCreatedTo = null; 
    $jobCreatedFrom = null; 

    $jobPostTo = null;
    $jobPostFrom = null;

    $applicationTo = null;
    $applicationFrom = null;

    $title = null;
    
    if(isset($requestAll['user_name']) && !empty($requestAll['user_name'])) {
    		
    	$userName = $requestAll['user_name'];
    }

    if(isset($requestAll['rank_id']) && !empty($requestAll['rank_id'])) {
    		
    	$rankId = $requestAll['rank_id'];
    }

    if(isset($requestAll['job_created_to']) && !empty($requestAll['job_created_to'])) {
    		
    	$jobCreatedTo = $requestAll['job_created_to'];
    }

    if(isset($requestAll['job_created_from']) && !empty($requestAll['job_created_from'])) {
    		
    	$jobCreatedFrom = $requestAll['job_created_from'];
    }
    
    if(isset($requestAll['job_post_to']) && !empty($requestAll['job_post_to'])) {
    		
    	$jobPostTo = $requestAll['job_post_to'];
    }

    if(isset($requestAll['job_post_from']) && !empty($requestAll['job_post_from'])) {
    		
    	$jobPostFrom = $requestAll['job_post_from'];
    }

    if(isset($requestAll['application_to']) && !empty($requestAll['application_to'])) {
    		
    	$applicationTo = $requestAll['application_to'];
    }

    if(isset($requestAll['application_from']) && !empty($requestAll['application_from'])) {
    		
    	$applicationFrom = $requestAll['application_from'];
    }

    if(isset($requestAll['title']) && !empty($requestAll['title'])) {
    		
    	$title = $requestAll['title'];
    }
?>
<div class="container">
	<div class="row-fluid">
		  <div style="text-align: center;">
              <h3><strong>Job Application</strong></h3>
          </div>
          	<form method="get" class="form-inline" role="form">
		          <div class="panel-body">
		                {{ csrf_field() }}
		                 
		                <a href="{{ route('admin.j.viewJobAll') }}" class="btn btn-primary">All</a>
		                <a href="{{ route('admin.j.viewJobCompany') }}" class="btn btn-primary">Company</a>
		                <a href="{{ route('admin.j.viewJobUser') }}" class="btn btn-info">User</a>
						<br><br>

						<div class="row">
							<div class="col-md-3">
								
								<div class="form-group">
				                      <label for="name">User</label><br>
				                      {{ Form::text('user_name',$userName,['id'=>'user_name','class'=>'form-control'])}}
				                </div>

							</div>

					        <div class="form-group">
			                      <label for="name">Rank</label><br>
			                      {{ Form::select('rank_id',\CommonHelper::new_rank(),$rankId,['placeholder'=>'select rank','id'=>'rank_id','class'=>'form-control'])}}
			                </div>
						</div>

		                <br><br>

		                <div class="row">
		                	<div class="col-md-5">
		                		
			               		<div class="form-group">
				                      <label for="name">Job Post between</label><br>
				                      {{ Form::date('job_post_to',$jobPostTo,['id'=>'job_post_to','class'=>'form-control'])}}
			  	                      {{ Form::date('job_post_from',$jobPostFrom,['id'=>'job_post_from','class'=>'form-control'])}}
				                </div>

		                	</div>

							<div class="form-group">
			                      <label for="name">Application between</label><br>
			                      {{ Form::date('application_to',$applicationTo,['id'=>'application_to','class'=>'form-control'])}}
		  	                      {{ Form::date('application_from',$applicationFrom,['id'=>'application_from','class'=>'form-control'])}}
			                </div>

		                </div>
		                <br><br>
		                <hr>

		                <div class="row">
		                	
		                	<div class="col-md-3">
		                		
								<div class="form-group">
				                      <label for="name">Job</label><br>
				                      {{ Form::text('title',$title,['id'=>'title','class'=>'form-control'])}}
			  	                </div>

		                	</div>

		                	<div class="col-md-3">
		                		
			                	<div class="form-group">
									<label style="text-align: center;"><br>
										@if(isset($requestAll['all_applied']) && $requestAll['all_applied'] == 1)
											<input type="radio" checked="true" class="all_applied" value="1" name="all_applied"> 
										@else
											<input type="radio" class="all_applied" value="1" name="all_applied">
										@endif
										All
										@if(isset($requestAll['all_applied']) && $requestAll['all_applied'] == 2)
											<input type="radio" checked="checked" class="all_applied" value="2" name="all_applied"> 
										@else
											<input type="radio" class="all_applied" value="2" name="all_applied"> 
										@endif
										Applied Only
									</label>
								</div>

		                	</div>

							<div class="form-group">
								<label style="text-align: center;"><br>
									@if(isset($requestAll['qr']) && $requestAll['qr'] == 1)
										<input type="radio" class="qr_check" checked="checked" value="1" name="qr"> 
									@else
										<input type="radio" class="qr_check" value="1" name="qr"> 
									@endif
									QR YES
									@if(isset($requestAll['qr']) && $requestAll['qr'] == 2)
										<input type="radio" checked="checked" class="qr_check" name="qr" value="2"> QR NO
									@else
										<input type="radio" class="qr_check" name="qr" value="2"> QR NO
									@endif
								</label>
							</div>

		                </div>


							<hr>
							<div class="form-group">
								<input type="submit" class="btn btn-primary" value="FILTER" />	
								<a href="{{route('admin.j.viewJobUser')}}" class="btn btn-info">Reset</a>
							</div>
							
				  </div>
  			</form>
  			<div style="text-align: center;">
              <h3><strong>Result</strong></h3>
          	</div>
          	<br>
          	<?php  

          		$totalCompany = getTotalCompany(); 
          		$totalJobCreated = getTotalJobCreated(); 
          		$totalPostSent = getTotalPostSent(); 
          		$totalJobApplication = getTotalJobApplication(); 
          		$totalUserApproached = getTotalUserApproached(); 

          	?>
          	<div class="row">
          		 <label style="margin-left: 10px">Total</label><br>
          		<div class="col-md-2">
          		    <div class="form-group">
						<label> Post received: </label> {{$totalPostSent}}
					</div>
				</div>

          		<div class="col-md-2">
          			<div class="form-group">
						<label> Job Applications: </label> {{$totalJobApplication}}
					</div>
				</div>
		
				<div class="col-md-2">
          		    <div class="form-group">
						<label> User approached: </label> {{$totalUserApproached}}
					</div>
				</div>

				<div class="col-md-2">
          		    <div class="form-group">
						<label> Job QR: </label> 19
					</div>
				</div>

				<div class="col-md-2">
          		    <div class="form-group">
						<label> Companies involved: </label> {{$totalCompany}}
					</div>
				</div>

          			
          	</div>
      	<br>
		<div class="datatable-container">

		         {{ Form::open(
		          array(
		          'id'                => 'table_form',
		          'name'              => 'table_form',
		          'class'             => 'ajaxFormSubmitDatatable',
		          'method'              => 'POST'
		          ))
		          }}

		            <table class="table table-bordered table-hover" id="dataTableBuilder" width="100%">
		                <thead>
		                	<th>Sr</th>
		                	<th>Job Id</th>
		                	<th>Status</th>
		                	<th>Company Name</th>
		                	<th>Job Post Date</th>
		                	<th>Rank</th>
		                	<th>Name</th>
		                	<th>Applied Date</th>
		                	<th>QR Date</th>
		                	<th>QR</th>
		                </thead>
		                <tbody></tbody>
		                <tfoot></tfoot>
		            </table>
		        </form>
		    </div>
		</div>
	</div>

</div>

@stop

@section('js_script')
<script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
<script>
	jQuery(document).ready(function() {
		FormWizard.init();
	});
</script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="{{asset('public/assets/responsive/dataTables.responsive.min.js')}}"></script>
<script>
    
   function RESET_FORM()
    {
        $('#search-frm').trigger('reset');
        table.draw();
        return false;
    }
    $(document).on("click","button[type='submit']",function(){
          table.draw();
          return false;
    });
    var table = $('#dataTableBuilder').DataTable({
        bProcessing: true,
        bServerSide: true,
        processing: true,
        serverSide: true,
        searching: true,
        responsive: true,
        ajax: {
            url: "{{ route('admin.j.getViewAllUser') }}?datatable=no",
            data: function (d) {
                d.id = $('input[name=id]').val();
                d.title   = $('input[name=title]').val();
                d.user_name   = $('#user_name').val();
                d.company_id   = $('#company_id').val();
                d.rank_id   = $('#rank_id').val();
                d.job_created_to   = $('#job_created_to').val();
                d.job_created_from   = $('#job_created_from').val();
                d.job_post_to   = $('#job_post_to').val();
                d.job_post_from   = $('#job_post_from').val();
                d.application_to   = $('#application_to').val();
                d.application_from   = $('#application_from').val();
                d.all_applied   = $('.all_applied:checked').val();
                d.qr_check   = $('.qr_check:checked').val();
            }
        },
        columns: [
            {data: 'id' },
            {data: 'courseJob.unique_code' },
            {data: 'courseJob.jobStatus.name' },
            {data: 'courseJob.company.name' },
            {data: 'created_at' },
            {data: 'courseJob.rank_id' },
            {data: 'courseJob.title' },
            {data: 'applied_date' },
            {data: 'shareData.latest_qr' },
            {data: 'shareData.history_log_latest.description' },
        ]
    });
</script>
@stop