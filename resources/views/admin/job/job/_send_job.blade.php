@extends('admin.index')

@section('style_section')
<style>
	div#padding_to_add {
	    padding: 25px;
	}
	.c_job_id span{
		margin-left: 70px
	} 
</style>
<link rel="stylesheet" type="text/css" href="{{asset('public/assets/responsive/jquery.dataTables.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('public/assets/responsive/responsive.dataTables.min.css')}}">
@endsection

@section('content')
<?php 

	$countryData = \CommonHelper::countries();
    $dceData = \App\Dce::getDesDropdown();
    $allCourse = \App\Courses::getAllCourse();
    $vaccination = \App\Vaccination::getVaccinationDropDown();
    $getJobList = \App\CourseJob::getCustomeJobDropDown();
    $engineTypeDropDown = \CommonHelper::engine_type_admin();
    $shipTypeDropDown = \CommonHelper::ship_type();

    $requestAll = request()->all();
    // dd($requestAll);
    $courseJobId  = null;
    $currentRank  = null;
    $rankExperienceYear = null;
    $rankExperienceMonth = null;
    $minimumWages = null;
    $maximumWages = null;
    $availability = null;
    $passCountry = null;
    $cdcCountry = null;
    $cocCountry = null;
    $coeCountry = null;
    $watchKeeping = null;
    $gmdssCountry = null;
    $shipType = null;
    $engineType = null;

    if (isset($requestAll['course_job_id']) && !empty($requestAll['course_job_id'])) {
    		
    	$courseJobId = $requestAll['course_job_id'];
    }    

    if (isset($requestAll['current_rank']) && !empty($requestAll['current_rank'])) {
    		
    	$currentRank = $requestAll['current_rank'];
    }
    
    if (isset($requestAll['rank_experience_year']) && !empty($requestAll['rank_experience_year'])) {
    		
    	$rankExperienceYear = $requestAll['rank_experience_year'];
    }

    if (isset($requestAll['rank_experience_month']) && !empty($requestAll['rank_experience_month'])) {
    		
    	$rankExperienceMonth = $requestAll['rank_experience_month'];
    }

    if (isset($requestAll['minimum_wages']) && !empty($requestAll['minimum_wages'])) {
    		
    	$minimumWages = $requestAll['minimum_wages'];
    }

    if (isset($requestAll['maximum_wages']) && !empty($requestAll['maximum_wages'])) {
    		
    	$maximumWages = $requestAll['maximum_wages'];
    }

    if (isset($requestAll['availability']) && !empty($requestAll['availability'])) {
    		
    	$availability = $requestAll['availability'];
    }

    if (isset($requestAll['pass_country']) && !empty($requestAll['pass_country'])) {
    		
    	$passCountry = $requestAll['pass_country'];
    }

    if (isset($requestAll['cdc']) && !empty($requestAll['cdc'])) {
    		
    	$cdcCountry = $requestAll['cdc'];
    }    

    if (isset($requestAll['coc']) && !empty($requestAll['coc'])) {
    		
    	$cocCountry = $requestAll['coc'];
    }    

    if (isset($requestAll['coe']) && !empty($requestAll['coe'])) {
    		
    	$coeCountry = $requestAll['coe'];
    }

    if (isset($requestAll['watch_keeping']) && !empty($requestAll['watch_keeping'])) {
    		
    	$watchKeeping = $requestAll['watch_keeping'];
    }

    if (isset($requestAll['gmdss']) && !empty($requestAll['gmdss'])) {
    		
    	$gmdssCountry = $requestAll['gmdss'];
    }

    if (isset($requestAll['experience_ship_type']) && !empty($requestAll['experience_ship_type'])) {
    		
    	$shipType = $requestAll['experience_ship_type'];
    }

    if (isset($requestAll['experience_engine_type']) && !empty($requestAll['experience_engine_type'])) {
    		
    	$engineType = $requestAll['experience_engine_type'];
    }

?>
<div class="container">
	<div class="row-fluid">
		  <div style="text-align: center;">
              <h3><strong>Post Job</strong></h3>
          </div>
        <form id="FilterJob" class="form-inline" method="get">
          <div class="panel-body">
                {{ csrf_field() }}
                  <div class="form-group c_job_id">
                      <label for="name">The Job</label>
                      {{ Form::select('course_job_id',[''=>'Select job']+$getJobList,$courseJobId,['id'=>'course_job_id','class'=>'form-control'])}}
                	<a href="javascript:void(0)" date-route="{{route('admin.j.preview')}}" class="btn btn-primary show_preview">Preview</a>
                	<a href="{{route('admin.j.sendIndex')}}" class="btn btn-info">Reset</a>
                  </div>
                  <input type="hidden" name="courseJobIdHidden" id="courseJobIdHidden" value="{{$courseJobId}}">
                <div>
		            <h3><strong>Seafarer Filter</strong> -</h3>
		        </div>
		        <div class="col-md-4">
			        <div class="form-group">
	                      <label for="name">Rank</label>
	                      {{ Form::select('current_rank',[''=>'select rank']+\CommonHelper::new_rank(),$currentRank,['id'=>'current_rank','class'=>'form-control'])}}
	                </div>
                </div>
		        <div class="col-md-2">
					
					<div class="form-group">
						@if(isset($requestAll['where_in']) && in_array('Ship',$requestAll['where_in']))
                	  		<input type="checkbox" value="Ship" class="where_in_ship" checked="checked" name="where_in[]"> On Ship
						@else
							<input type="checkbox" value="Ship" class="where_in_ship" name="where_in[]"> On Ship
						@endif
	                </div>

		        </div>

		        <div class="col-md-2">
	
	                <div class="form-group">
						@if(isset($requestAll['where_in']) && in_array('Home',$requestAll['where_in']))
							<input type="checkbox" value="Home" class="where_in_home" checked="checked" name="where_in[]"> At Home
						@else
	                	  <input type="checkbox" value="Home" class="where_in_home" name="where_in[]"> At Home
						@endif
	                </div>

		        </div>
    	        <div class="col-md-4">

	                <div class="form-group">
	                	@if(isset($requestAll['earlier']) && $requestAll['earlier'] == 1)
	                	<input type="checkbox" value="1" checked="checked" class="earlier" name="earlier" id="earlier"> 
	                	@else
	                	<input type="checkbox" value="1" class="earlier" name="earlier" id="earlier"> 
	                	@endif
	                	Also show earlier posted users
	                </div>

    	        </div>
    	        <hr><hr>
               	<div class="row">
	    	        <div class="col-md-3">
	    	        	
		                <div class="form-group">
							<label>
								Min Rank Experience
							</label>
							<br>
		               		{{ Form::select('rank_experience_year',\CommonHelper::years(),$rankExperienceYear,['placeholder'=>'Year','id'=>'rank_experience_year','class'=>'form-control'])}}

		               		{{ Form::select('rank_experience_month',\CommonHelper::months(),$rankExperienceMonth,['placeholder'=>'Month','id'=>'rank_experience_month','class'=>'form-control'])}}
						</div>
	    	        </div>
	    	        <div class="col-md-5">
	    	        	
		               <div class="form-group">
							<label style="text-align: center;">
								Wages
							</label>
							<br>
		               		
		               		{{ Form::text('minimum_wages',$minimumWages,['placeholder'=>'Minimum Wages','id'=>'minimum_wages','class'=>'form-control'])}}
							
							{{ Form::text('maximum_wages',$maximumWages,['placeholder'=>'Maximum Wages','id'=>'maximum_wages','class'=>'form-control'])}}

		                </div>
	    	        </div>
	            	
	               	<div class="col-md-3">
	               		
		        	    <div class="form-group">
							<label>
								Availability date
							</label>
							<br>
		                   	{{Form::date('availability',$availability,['id'=>'availability','class'=>'form-control'])}}
		                </div>
	               	</div>
                </div>
                <hr>
                <div class="row">
                	<div class="col-md-3">
	                	<div class="form-group">
							<label>Ch Cert </label>
							<br>
							@if(isset($requestAll['character_reference']) && $requestAll['character_reference'] == '1')
								<input type="radio" value="1" checked="checked" class="character_reference" name="character_reference"> YES
							@else
								<input type="radio" value="1" class="character_reference" name="character_reference"> YES
							@endif
							@if(isset($requestAll['character_reference']) && $requestAll['character_reference'] == '0')
								<input type="radio" value="0" checked="checked" class="character_reference" name="character_reference"> NO
							@else
								<input type="radio" value="0" class="character_reference" name="character_reference"> NO
							@endif
						</div>
					</div>
					<div class="col-md-5">
		                <div class="form-group">
							<label>
								Vaccination
							</label>
							<br>
		                   	{{ Form::select('vaccination',$vaccination,null,['multiple'=>true,'placeholder'=>'Select vaccination','id'=>'vaccination','class'=>'form-control'])}}
		                </div>	   
		            </div>						    
                </div> 	
                	

					<hr>
					<div class="row">
						<div class="col-md-4">
	                	
		                <div class="form-group">
							<label>
								Passport Country
							</label>
							<br>
		                   	<select id="pass_country" name="pass_country[]" class="form-control" multiple="true">
		                   		<option>Select Country</option>
		                   		@foreach($countryData as $key=>$v)
		                   			<?php 

		                   				$passCountrySelected = '';
		                   				if (!empty($passCountry)) {
		                   					
		                   					if (in_array($key,$passCountry)) {

		                   						$passCountrySelected = 'selected';
		                   					}
		                   				}

		                   			?>
		                   			<option value="{{$key}}" {{$passCountrySelected}}>{{$v}}</option>
		                   		@endforeach
		                   	</select>
		                </div>
					</div>
						<div class="col-md-5">
		                <div class="form-group">
							<label>
								CDC Country
							</label>
							<br>
							<select id="cdc" name="cdc[]" class="form-control" multiple="true">
			                   		<option>Select Country</option>
			                   		@foreach($countryData as $key=>$v)
			                   			<?php 

			                   				$cdcCountrySelected = '';
			                   				if (!empty($cdcCountry)) {
			                   					
			                   					if (in_array($key,$cdcCountry)) {

			                   						$cdcCountrySelected = 'selected';
			                   					}
			                   				}

			                   			?>
			                   			<option value="{{$key}}" {{$cdcCountrySelected}}>{{$v}}</option>
			                   		@endforeach
			                   	</select>
			            </div>				
			            </div>		
					</div>
	                

					<hr>
					<div class="row">
		                <div class="col-md-4">
		                	
			                <div class="form-group">
								<label>
									COC Country
								</label>
								<br>
								<select id="coc" name="coc[]" class="form-control" multiple="true">
			                   		<option>Select Country</option>
			                   		@foreach($countryData as $key=>$v)
			                   			<?php 

			                   				$cocCountrySelected = '';
			                   				if (!empty($cocCountry)) {
			                   					
			                   					if (in_array($key,$cocCountry)) {

			                   						$cocCountrySelected = 'selected';
			                   					}
			                   				}

			                   			?>
			                   			<option value="{{$key}}" {{$cocCountrySelected}}>{{$v}}</option>
			                   		@endforeach
			                   	</select>


			                </div>

		                </div>	                
						<div class="col-md-5">
							<div class="form-group">
								<label>
									COE Country
								</label>
								<br>
								<select id="coe" name="coe[]" class="form-control" multiple="true">
			                   		<option>Select Country</option>
			                   		@foreach($countryData as $key=>$v)
			                   			<?php 

			                   				$coeCountrySelected = '';
			                   				if (!empty($coeCountry)) {
			                   					
			                   					if (in_array($key,$coeCountry)) {

			                   						$coeCountrySelected = 'selected';
			                   					}
			                   				}

			                   			?>
			                   			<option value="{{$key}}" {{$coeCountrySelected}}>{{$v}}</option>
			                   		@endforeach
			                   	</select>


			                </div>
		                </div>
		            </div>    
	                <br><br>
	                <div class="row">
		                <div class="col-md-4">
		                	
							<div class="form-group">
								<label>
									@if(isset($requestAll['wk_cop']) && $requestAll['wk_cop'] == 'wk')
										<input type="radio" value="wk" checked="checked" class="wk_cop" name="wk_cop"> Wk
									@else
										<input type="radio" value="wk" class="wk_cop" name="wk_cop"> Wk
									@endif
									@if(isset($requestAll['wk_cop']) && $requestAll['wk_cop'] == 'cop')
										<input type="radio" value="cop" checked="checked" class="wk_cop" name="wk_cop"> COP
									@else
										<input type="radio" value="cop" class="wk_cop" name="wk_cop"> COP
									@endif
								</label>
								<br>
								<select id="watch_keeping" name="watch_keeping[]" class="form-control" multiple="true">
			                   		<option>Select Country</option>
			                   		@foreach($countryData as $key=>$v)
			                   			<?php 

			                   				$watchCountrySelected = '';
			                   				if (!empty($watchKeeping)) {
			                   					
			                   					if (in_array($key,$watchKeeping)) {

			                   						$watchCountrySelected = 'selected';
			                   					}
			                   				}

			                   			?>
			                   			<option value="{{$key}}" {{$watchCountrySelected}}>{{$v}}</option>
			                   		@endforeach
			                   	</select>

			               	</div>


		                </div>
		               	<div class="col-md-5">
							<div class="form-group">
								<label>
									GMDSS Country
								</label>
								<br>
								<select id="gmdss" name="gmdss[]" class="form-control" multiple="true">
			                   		<option>Select Country</option>
			                   		@foreach($countryData as $key=>$v)
			                   			<?php 

			                   				$gmssCountrySelected = '';
			                   				if (!empty($gmdssCountry)) {
			                   					
			                   					if (in_array($key,$gmdssCountry)) {

			                   						$gmssCountrySelected = 'selected';
			                   					}
			                   				}

			                   			?>
			                   			<option value="{{$key}}" {{$gmssCountrySelected}}>{{$v}}</option>
			                   		@endforeach
			                   	</select>

			                </div>
		                </div>		
		            </div>     
	                <hr>
					<div class="form-group">
						<label>
							Course
						</label>
						<br>
	                   	{{ Form::select('course_id[]',[''=>"Select course"]+ $allCourse,null,['multiple'=>true,'id'=>'course_id','class'=>'form-control'])}}
	                </div>
					<hr>
					<div class="col-md-3">
						
						<div class="form-group">
							<label>
								Experience Required on Ship Type
							</label>
							<br>

		                   	<select id="experience_ship_type" name="experience_ship_type[]" class="form-control" multiple="true">
		                   		<option>Ship type experience</option>
		                   		@foreach($shipTypeDropDown as $key=>$v)
		                   			<?php 

		                   				$shipTypeSelected = '';
		                   				if (!empty($shipType)) {
		                   					
		                   					if (in_array($key,$shipType)) {

		                   						$shipTypeSelected = 'selected';
		                   					}
		                   				}

		                   			?>
		                   			<option value="{{$key}}" {{$shipTypeSelected}}>{{$v}}</option>
		                   		@endforeach
		                   	</select>

		                </div>


					</div>

	                <div class="form-group">
						<label>
							Experience Required on Engine Type
						</label>
						<br>

						<select id="experience_engine_type" name="experience_engine_type[]" class="form-control" multiple="true">
	                   		<option>Engine type experience</option>
	                   		@foreach($engineTypeDropDown as $key=>$v)
	                   			<?php 

	                   				$engineTypeSelected = '';
	                   				if (!empty($engineType)) {
	                   					
	                   					if (in_array($key,$engineType)) {

	                   						$engineTypeSelected = 'selected';
	                   					}
	                   				}

	                   			?>
	                   			<option value="{{$key}}" {{$engineTypeSelected}}>{{$v}}</option>
	                   		@endforeach
	                   	</select>

	                </div>
					<br>
					<hr>


                	<div class="col-md-3">
		                <div class="form-group">
							<label>FRAMO</label>
							<br>
							@if(isset($requestAll['fromo']) && $requestAll['fromo'] == '1')
								<input type="radio" value="1" class="fromo" checked="checked" name="fromo"> YES
							@else
								<input type="radio" value="1" class="fromo" name="fromo"> YES
							@endif
							@if(isset($requestAll['fromo']) && $requestAll['fromo'] == '0')
								<input type="radio" value="0" class="fromo" checked="checked" name="fromo"> NO
							@else
								<input type="radio" value="0" class="fromo" name="fromo"> NO
							@endif
							
						</div>
                	</div>

					@if(isset($dceData) && !empty($dceData))
                	 	<div class="form-group">
							<label>
								DCE
							</label><br>
							@foreach($dceData as $key=>$v)
								{{ Form::checkbox('dce_id[]',$key,null,['class'=>''])}}
								{{$v}}
							@endforeach
						</div>
					@endif
					<hr>
					<div class="form-group">
						<button type="submit" class="btn btn-primary">APPLY FILTER</button>	
					</div>
			</div>
	  	</form>

	  	@if(isset($currentRank) || $currentRank !=null ||
	  		isset($requestAll['where_in']) || 
	  		isset($requestAll['where_in']) ||
	  		$rankExperienceYear !=null || $rankExperienceMonth !=null ||
	  		isset($availability) || 
	  		isset($requestAll['character_reference']) || 
	  		isset($requestAll['fromo']) ||
	  		isset($passCountry) ||
	  		isset($cdcCountry) ||
	  		isset($cocCountry) ||
	  		isset($coeCountry) ||
	  		isset($watchKeeping) ||
	  		isset($gmdssCountry) || 
	  		isset($minimumWages) ||
	  		isset($maximumWages) ||
	  		!empty($shipType) ||
	  		!empty($engineType)
	  		)

	      	<div class="" style="text-align: center;">
	            <h3><strong>Filter Result</strong></h3>
	        </div>
	        <div style="background-color:#8b91a0;color: white;padding: 5px">
	        	
	        	@if(isset($currentRank) && $currentRank !=null)
	        		Rank: {{getRankCommonFuncion($currentRank)}} /
	        	@endif
	        	@if(isset($requestAll['where_in']) && in_array('Home',$requestAll['where_in']))
	        		At Home /
	        	@endif
	        	@if(isset($requestAll['where_in']) && in_array('Ship',$requestAll['where_in']))
	        		On Ship /
	        	@endif
	        	@if($rankExperienceYear !=null && $rankExperienceMonth !=null)
	        		Min Rank Experience (Year/Month): {{$rankExperienceYear}}.{{$rankExperienceMonth}} /
	        	@elseif($rankExperienceYear !=null) 
	        		Min Rank Experience (Year): {{$rankExperienceYear}} /
	        	@elseif($rankExperienceMonth !=null) 
	        		Min Rank Experience (Month): {{$rankExperienceMonth}} /
	        	@endif
	        	@if(isset($requestAll['currency']) && !empty($requestAll['currency']))
	        		Currency:{{$requestAll['currency']}} /
	        	@endif

	        	@if(isset($maximumWages) || isset($minimumWages))

	        		Wages(min/max) / 
	        		@if(!empty($maximumWages)) 
	        			{{$maximumWages}}
	        		@else
	        			- 	
	        		@endif / 

	        		@if(!empty($minimumWages)) 
	        			{{$minimumWages}}
	        		@else
	        			- 	
	        		@endif

	        		/

	        	@endif

	        	@if(isset($availability) && !empty($availability))
	        		Availability date:{{$availability}} /
	        	@endif
	        	@if(isset($requestAll['character_reference']) && $requestAll['character_reference'] == '1')
	        		Ch Cert:Yes /
	        	@elseif(isset($requestAll['character_reference']) && $requestAll['character_reference'] == '0')	
	        		Ch Cert:No /
	        	@endif
	        	@if(isset($requestAll['fromo']) && $requestAll['fromo'] == '1')
	        		FRAMO:Yes /
	        	@elseif(isset($requestAll['fromo']) && $requestAll['fromo'] == '0')	
	        		FRAMO:No /
	        	@endif
	        	@if(isset($requestAll['fromo']) && $requestAll['fromo'] == 'wk')
	        		WK /
	        	@elseif(isset($requestAll['fromo']) && $requestAll['fromo'] == 'cop')
	        		COP /
	        	@endif
	        	
	        	@if(isset($passCountry) && !empty($passCountry))
	        		Passport Country : {{getMultipleCountryName($passCountry)}} /
	        	@endif

	        	@if(isset($cdcCountry) && !empty($cdcCountry))
	        		CDC Country : {{getMultipleCountryName($cdcCountry)}} /
	        	@endif

	        	@if(isset($cocCountry) && !empty($cocCountry))
	        		COC Country : {{getMultipleCountryName($cocCountry)}} /
	        	@endif


	        	@if(isset($coeCountry) && !empty($coeCountry))
	        		COE Country : {{getMultipleCountryName($coeCountry)}} /
	        	@endif


	        	@if(isset($watchKeeping) && !empty($watchKeeping))
	        		Wk COP Country : {{getMultipleCountryName($watchKeeping)}} /
	        	@endif

	        	@if(isset($gmdssCountry) && !empty($gmdssCountry))
	        		GMDSS Country : {{getMultipleCountryName($gmdssCountry)}} /
	        	@endif

	        	@if(!empty($shipType))
	        		Ship Type: 
	        		@foreach($shipType as $key=>$v)
                            @if(in_array($v,array_keys($shipTypeDropDown)))
                                {{ $shipTypeDropDown[$v] }},
                            @endif
                    @endforeach
                    /
	        	@endif

	        	@if(!empty($engineType))
	        		Engine Type: 
	        		@foreach($engineType as $key=>$v)
                            @if(in_array($v,array_keys($engineTypeDropDown)))
                                {{ $engineTypeDropDown[$v] }},
                            @endif
                    @endforeach
                    /
	        	@endif
	        	

	        </div>
			
			<hr>

	  	@endif

		<div class="table-toolbar">
          <div class="row">
            <div class="col-md-6">
              <div class="btn-group">
                <a class="btn btn-warning" style="margin-left: 15px" onclick="statusAll('table_form','{{route('admin.j.jobSendToUser')}}')">Send Job
                </a>
              </div>
            </div>
          </div>
        </div>
        <hr>
		<div class="datatable-container">

		         {{ Form::open(
		          array(
		          'id'                => 'table_form',
		          'name'              => 'table_form',
		          'class'             => 'ajaxFormSubmitDatatable',
		          'method'              => 'POST'
		          ))
		          }}

		            <table class="table table-bordered table-hover" id="dataTableBuilder" width="100%">
		                <thead>
		                	 <th class="table-checkbox">
				              <input type="checkbox" name="checkbox" value="1" id="select_all" />
				            </th>
		                	<th>Sr</th>
		                	<th>Rank</th>
		                	<th>FName</th>
		                	<th>LName</th>
		                	<th>Rank Exp</th>
		                	<th>Wages</th>
		                	<th>Last Job Id</th>
		                	<th>Post Date</th>
		                	<th>Applied Date</th>
		                	<th>QR Date</th>
		                	<th>QR</th>
		                	<th>View Resume</th>
		                	<th>View Profile</th>
		                </thead>
		                <tbody></tbody>
		                <tfoot></tfoot>
		            </table>
		        </form>
		    </div>
		</div>
	</div>

</div>
<div class="hide">
    <div id="modal_form" class="modal_box">
    </div>
</div>
@stop

@section('js_script')
<script src="{{asset('public/assets/responsive/jquery.dataTables.min.js')}}"></script>
<link rel="stylesheet" type="text/css" href="{{asset('public/assets/colorbox/colorbox.css')}}">	
<script src="{{asset('public/assets/colorbox/jquery.colorbox.min.js')}}"></script>
<script>
	jQuery(document).ready(function() {
		FormWizard.init();
	});
</script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-jgrowl/1.4.7/jquery.jgrowl.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-jgrowl/1.4.7/jquery.jgrowl.css" />

<script src="{{asset('public/assets/responsive/sweetalert.min.js')}}"></script>
<script src="{{asset('public/assets/responsive/dataTables.responsive.min.js')}}"></script>
<script>
   
	$(document).ready(function(){
	    
	    $("#FilterJob").validate({
	        rules: {
	           'course_job_id': { required: true },
	        },
	        messages:{
	           'course_job_id': { required:"Please select a job."},
	        },
	        errorPlacement: function(error, element) {
	        		
				if (element.attr("name") == "email") {
	                
	                $("#email_e").html(error);

	            }else{
          		
          			$(element).closest('div').append(error);

	            }
	        }
	    });
	});

   function RESET_FORM()
    {
        $('#search-frm').trigger('reset');
        table.draw();
        return false;
    }
    
    $(document).on("click","button[type='submit']",function(){
          // table.draw();
          // return false;
    });

    var table = $('#dataTableBuilder').DataTable({
        bProcessing: true,
        bServerSide: true,
        processing: true,
        serverSide: true,
        searching: true,
        responsive: true,
        ajax: {
            url: "{{ route('admin.j.sendGet') }}?datatable=no",
            data: function (d){
                d.id = $('input[name=id]').val();
                d.course_job_id   = $('#course_job_id').val();
                d.current_rank   = $('#current_rank').val();
                d.where_in_home   = $(".where_in_home:checked").val();
                d.where_in_ship  = $(".where_in_ship:checked").val();
                d.rank_experience_year   = $('#rank_experience_year').val();
                d.rank_experience_month   = $('#rank_experience_month').val();
                d.currency = $('input[name=currency]:checked').val();
                d.minimum_wages   = $('#minimum_wages').val();
                d.maximum_wages   = $('#maximum_wages').val();
                d.availability   = $('#availability').val();
                d.character_reference = $('.character_reference:checked').val();
                d.earlier = $('.earlier:checked').val();
                d.fromo = $('.fromo:checked').val();
                d.wk_cop = $('.wk_cop:checked').val();
                d.pass_country   = $('#pass_country').val();
                d.gmdss   = $('#gmdss').val();
                d.watch_keeping   = $('#watch_keeping').val();
                d.coe   = $('#coe').val();
                d.coc   = $('#coc').val();
                d.cdc   = $('#cdc').val();
                d.experience_ship_type   = $('#experience_ship_type').val();
                d.experience_engine_type   = $('#experience_engine_type').val();
            }
        },
        columns: [
          	{ data: 'id',orderable: false,searchable: false},
            {data: 'mobile',orderable: false,searchable: false },
            {data: 'professional_detail.current_rank',orderable: false,searchable: false },
            {data: 'first_name',orderable: false,searchable: false },
            {data: 'last_name',orderable: false,searchable: false },
            {data: 'professional_detail.current_rank_exp',orderable: false,searchable: false },
            {data: 'professional_detail.last_salary',orderable: false,searchable: false },
            {data: 'courseJob.unique_code',orderable: false,searchable: false },
            {data: 'last_job_post_date',orderable: false,searchable: false },
            {data: 'sended_jobs_id',orderable: false,searchable: false },
			{data: 'shareHistory.latest_qr' },
            {data: 'shareHistory.history_log_latest.description' },
            {data: 'view_resume',orderable: false,searchable: false },
            {data: 'view_profile',orderable: false,searchable: false },
        ]
    });

    $(document).on("click",".show_preview",function(){

    	var jobId = $("#course_job_id").val();
    	
    	if (jobId == '') {

    		swal("Job!", "You jave not selected any job!", "error");

    	}else{
	     
		        $("#modal_form").html("");

		        $.ajax({
		            url:"{{route('admin.j.preview')}}",
		            data:{"course_job_id":jobId,"_token":"{{csrf_token()}}"},
		            type:"POST",
		            success:function(data){
		            	$("#modal_form").html(data);
		                $.colorbox({
		                    href: '#modal_form',
		                    inline: true,
		                    width       : '90%',
		                    maxWidth    : '90%',
		                    maxHeight   : '90%',
		                    opacity     : '0.2',
		                    fixed       : true
		                });
		                //gebo_colorbox_single.init();
		            },
		            error:function(){

		            	console.log(":ww");
		            }
		        });

    	}

    });
</script>
@stop