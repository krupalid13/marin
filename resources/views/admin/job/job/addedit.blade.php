@extends('admin.index')

@section('IS_SELECT2') &nbsp; @endsection 
@section('style_section')
<style>
	.select2-container-multi.form-control{padding:0px !important; height:0px !important;display:table !important;}
	.select2-container.form-control{padding:0px !important; height:0px !important;display:table !important;}
	
	</style>
@endsection
@section('content')
<?php 
	
	$countryData = \CommonHelper::countries();
    $dceData = \App\Dce::getDesDropdown();
    $allCourse = \App\Courses::getAllCourse();
    $vaccination = \App\Vaccination::getVaccinationDropDown();


?>
<div class="container">

  	<div class="row">
		<div class="col-sm-12">
				<div class="panel panel-white">
				<div class="panel-body">
					@if(isset($courseJob))
	                    {{ Form::model($courseJob,
	                      array(
	                      'id'                => 'AddEditCompany',
	                      'class'             => 'FromSubmit form-horizontal',
	                      'data-redirect_url' => route('admin.j.index'),
	                      'url'               => route('admin.j.update', $encryptedId),
	                      'method'            => 'PUT',
	                      'enctype'           =>"multipart/form-data"
	                      ))
	                    }}
                  	@else
	                    {{
	                      Form::open([
	                      'id'=>'AddEditJob',
	                      'class'=>'FromSubmit form-horizontal',
	                      'url'=>route('admin.j.store'),
	                      'data-redirect_url'=>route('admin.j.index'),
	                      'name'=>'Company',
	                      'enctype' =>"multipart/form-data"
	                      ])
	                    }}
                  	@endif

                  		<input type="hidden" name="id" value="{{$encryptedId}}">
						{{ csrf_field() }}
						
						<div id="step-1">
								
							<h2 class="StepTitle">
							@if(isset($courseJob))
								Edit
							@else
								Add
							@endif
								Job
							</h2>
							<div class="panel-heading">
								<h4 class="panel-title">Job Details</h4>
							</div>

	                	    <div class="form-group">
								<label class="col-sm-3 control-label">
									Company <span class="symbol required"></span>
								</label>
	                           	<div class="col-sm-6">
	                           		{{ Form::select('company_id',[''=>'Select company for adding jobs']+\App\Company::getCompanyDropdown(),null,['id'=>'company_id','class'=>'form-control'])}}
									<span class="hide" id="company_id-error" style="color: red"></span>
	                            </div>
	                        </div>
	                        <div id="company_a"></div>
	                	    <hr>
	                        <span class="col-md-offset-2" style="font-size: 25px"><strong> 1: Detailing Job</strong></span>	
	                        <hr>
	                	    <div class="form-group">
								<label class="col-sm-3 control-label">
									Job Post for Rank <span class="symbol required"></span>
								</label>
	                           	<div class="col-sm-6">
	                           		{{ Form::select('rank_id',\CommonHelper::new_rank(),null,['placeholder'=>'select rank','id'=>'rank_id','class'=>'form-control'])}}
									<span class="hide" id="rank_id-error" style="color: red"></span>
	                            </div>
	                        </div>

	                	    <div class="form-group">
								<label class="col-sm-3 control-label">
									Upload Image
								</label>
	                           	<div class="col-sm-6">
	                           		{{ Form::file('image',null,['id'=>'image','class'=>'form-control'])}}
									<span class="hide" id="image-error" style="color: red"></span>
	                            </div>
	                        </div>

	                	    <div class="form-group">
								<label class="col-sm-3 control-label">
									Job Title <span class="symbol required"></span>
								</label>
	                           	<div class="col-sm-6">
	                           		{{ Form::text('title',null,['placeholder'=>'Type a title for Job','id'=>'title','class'=>'form-control'])}}
									<span class="hide" id="title-error" style="color: red"></span>
	                            </div>
	                        </div>

	                	    <div class="form-group">
								<label class="col-sm-3 control-label">
									About Job Msg
								</label>
	                           	<div class="col-sm-6">
	                           		{{ Form::textarea('message',null,['placeholder'=>'Describe the job Profile or type of candidate your searching','id'=>'message','class'=>'form-control'])}}
									<span class="hide" id="message-error" style="color: red"></span>
	                            </div>
	                        </div>

	                	    <div class="form-group">
								<label class="col-sm-3 control-label">
									Valid till date <span class="symbol required"></span>
								</label>
	                           	<div class="col-sm-6">
	                           		{{ Form::date('valid_till_date',null,['id'=>'valid_till_date','class'=>'form-control'])}}
									<span class="hide" id="valid_till_date-error" style="color: red"></span>
	                            </div>
	                        </div>

	                	    <div class="form-group">
								<label class="col-sm-3 control-label">
									Source
								</label>
	                           	<div class="col-sm-6">
	                           		{{ Form::text('source',null,['placeholder'=>'The source from where this requirement info was received','id'=>'source','class'=>'form-control'])}}
									<span class="hide" id="source-error" style="color: red"></span>
	                            </div>
	                        </div>

	                	    <div class="form-group">
								<label class="col-sm-3 control-label">
									Job status
								</label>
	                           	<div class="col-sm-6">
	                           		{{ Form::select('job_status_id',[''=>'Select the importances']+ \App\JobStatus::getJobStatusDropdown(),null,['id'=>'job_status_id','class'=>'form-control'])}}
									<span class="hide" id="job_status_id-error" style="color: red"></span>
	                            </div>
	                        </div>
	                         <hr>
	                        <span class="col-md-offset-2" style="font-size: 25px"><strong> 2: Additional Requirements</strong></span>	
	                        <hr>
	                	    <?php 

	                	    	if (isset($courseJob) && !$courseJob->jobMultiplePassportCountry->isEmpty()) {
	                	    		
	                	    		$defaultPassport = $courseJob->jobMultiplePassportCountry->pluck('passport_country_id')->toArray();
	                	    	}else{

	                	    		$defaultPassport = null;
	                	    	}
	                	    ?>
	                	   
	                	    <div class="form-group">
								<label class="col-sm-3 control-label">
									Passport Country
								</label>
	                           	<div class="col-sm-6">	                           	
	                           		<select class="form-control job-select2" multiple name="passport_country_id[]" id="passport_country_id" placeholder="Select Country">
					                  @foreach ($countryData as $key=>$val)

					                    <option value="{{$key}}" @if(!empty($defaultPassport) && in_array($key,$defaultPassport)) selected @endif>{{$val}}</option>
					                  @endforeach					                 
					                </select>
									<span class="hide" id="passport_country_id-error" style="color: red"></span>
	                            </div>
	                        </div>
                        
	                        <div class="form-group" style="margin-top: 26px;">
								<label class="col-sm-3 control-label">
									Expected Salary Range
								</label>
								<div class="col-sm-2">
									{{ Form::radio('salary_type',1,null,['class'=>''])}}
									USD
									{{ Form::radio('salary_type',2,null,['class'=>''])}}
									INR
								</div>
	                           	<div class="col-sm-2">
	                           		{{ Form::text('minimum_wages',null,['placeholder'=>'Minimum Wages','id'=>'minimum_wages','class'=>'form-control'])}}
									<span class="hide" id="minimum_wages-error" style="color: red"></span>
	                            </div>	                           	
	                            <div class="col-sm-2">
	                           		{{ Form::text('maximum_wages',null,['placeholder'=>'Maximum Wages','id'=>'maximum_wages','class'=>'form-control'])}}
									<span class="hide" id="maximum_wages-error" style="color: red"></span>
	                            </div>
	                        </div>
	                        <hr>
	                        <div class="form-group">
								<label class="col-sm-3 control-label">
									COC,Country & Min Exp 
								</label>
								<div class="col-sm-2">
	                           		{{ Form::text('coc',null,['placeholder'=>'Add all COC that will do','id'=>'rank_experience_year','class'=>'form-control'])}}
									<span class="hide" id="coc-error" style="color: red"></span>
	                            </div>	                           	
	                            <div class="col-sm-4">
	                            	 <?php 

			                	    	if (isset($courseJob) && !$courseJob->jobMultipleCocCountry->isEmpty()) {
			                	    		
			                	    		$defaultCoc = $courseJob->jobMultipleCocCountry->pluck('coc_country_id')->toArray();
			                	    	}else{

			                	    		$defaultCoc = null;
			                	    	}
			                	    ?>

			                	    <select class="form-control job-select2" multiple name="coc_country_id[]" id="coc_country_id" placeholder="Select Country">	
					                  @foreach ($countryData as $key=>$val)
					                    <option value="{{$key}}" @if(!empty($defaultCoc) && in_array($key,$defaultCoc)) selected @endif>{{$val}}</option>
					                  @endforeach					                 
					                </select>	                           		
									<span class="hide" id="coc_country_id-error" style="color: red"></span>
	                            </div>
								<div class="col-sm-2">
									  <select class="form-control"  name="coc_experience_year" id="coc_experience_year" placeholder="YY">
									  	<option disabled selected="true">YY</option>
					                  @foreach (\CommonHelper::years() as $key=>$val)
					                    <option value="{{$key}}" @if(isset($courseJob) && !empty($courseJob) && $courseJob->coc_experience_year == $key )  selected @endif>{{$val}}</option>
					                  @endforeach					                 
					                </select>
	                           	</div>	                           	
	                            <div class="col-sm-2">
	                            	 <select class="form-control"  name="coc_experience_month" id="coc_experience_month" placeholder="MM">
	                            	 	<option disabled selected="true">MM</option>
					                  @foreach (\CommonHelper::months() as $key=>$val)
					                    <option value="{{$key}}" @if(isset($courseJob) && !empty($courseJob) && $courseJob->coc_experience_month == $key )  selected @endif>{{$val}}</option>
					                  @endforeach					                 
					                </select>
	                           	</div>
		                    </div>
		                    <hr>
	                         <div class="form-group">
								<label class="col-sm-3 control-label">
									COE, Country & Min Exp
								</label>
								<div class="col-sm-2">
	                           		{{ Form::text('coe',null,['placeholder'=>'Add all COE that will do','id'=>'coe','class'=>'form-control'])}}
									<span class="hide" id="coc-error" style="color: red"></span>
	                            </div>	                           	
	                            <div class="col-sm-4">
	                            	<?php 

			                	    	if (isset($courseJob) && !$courseJob->jobMultipleCoeCountry->isEmpty()) {
			                	    		
			                	    		$defaultCoe = $courseJob->jobMultipleCoeCountry->pluck('coe_country_id')->toArray();
			                	    	}else{

			                	    		$defaultCoe = null;
			                	    	}
			                	    ?>
	                           		
	                           		  <select class="form-control job-select2" multiple name="coe_country_id[]" id="coe_country_id" placeholder=" Select Country">
					                  @foreach ($countryData as $key=>$val)
					                    <option value="{{$key}}" @if(!empty($defaultCoe) && in_array($key,$defaultCoe)) selected @endif>{{$val}}</option>
					                  @endforeach					                 
					                </select>
									<span class="hide" id="coe-error" style="color: red"></span>
	                            </div>

	                            <div class="col-sm-2">
	                            	 <select class="form-control"  name="coe_experience_year" id="coe_experience_year" placeholder="YY">
	                            	 	<option disabled selected="true">YY</option>
					                  @foreach (\CommonHelper::years() as $key=>$val)
					                    <option value="{{$key}}" @if(isset($courseJob) && !empty($courseJob) && $courseJob->coe_experience_year == $key )  selected @endif>{{$val}}</option>
					                  @endforeach					                 
					                </select>
	                           	</div>	                           	
	                            <div class="col-sm-2">
	                            	 <select class="form-control"  name="coe_experience_month" id="coe_experience_month" placeholder="MM">
	                            	 	<option disabled selected="true">MM</option>
					                  @foreach (\CommonHelper::months() as $key=>$val)
					                    <option value="{{$key}}" @if(isset($courseJob) && !empty($courseJob) && $courseJob->coe_experience_month == $key )  selected @endif>{{$val}}</option>
					                  @endforeach					                 
					                </select>
	                           	</div>

	                        </div>	  
	                        <hr>
	                        <div class="form-group">
								<label class="col-sm-3 control-label">
									DP, Country & Min Exp
								</label>
								<div class="col-sm-2">
	                           		{{ Form::text('dp',null,['placeholder'=>'Add DP','id'=>'coe','class'=>'form-control'])}}
									<span class="hide" id="dp-error" style="color: red"></span>
	                            </div>	                           	
	                            <div class="col-sm-4">
	                            	<?php 

			                	    	if (isset($courseJob) && !$courseJob->jobMultipleDpCountry->isEmpty()) {
			                	    		
			                	    		$defaultDp = $courseJob->jobMultipleDpCountry->pluck('dp_country_id')->toArray();
			                	    	}else{

			                	    		$defaultDp = null;
			                	    	}
			                	    ?>	                           		
	                           		<select class="form-control job-select2" multiple name="dp_country_id[]" id="dp_country_id" placeholder="Select Country">
					                  @foreach ($countryData as $key=>$val)
					                    <option value="{{$key}}" @if(!empty($defaultDp) && in_array($key,$defaultDp)) selected @endif>{{$val}}</option>
					                  @endforeach					                 
					                </select>
									<span class="hide" id="dp_country_id_error" style="color: red"></span>
	                            </div>

	                             <div class="col-sm-2">
	                             	 <select class="form-control"  name="dp_experience_year" id="dp_experience_year" placeholder="YY">
	                             	 	<option disabled selected="true">YY</option>
					                  @foreach (\CommonHelper::years() as $key=>$val)
					                    <option value="{{$key}}" @if(isset($courseJob) && !empty($courseJob) && $courseJob->dp_experience_year == $key )  selected @endif>{{$val}}</option>
					                  @endforeach					                 
					                </select>	                           		
	                           	</div>	                           	
	                            <div class="col-sm-2">
	                            	 <select class="form-control"  name="dp_experience_month" id="dp_experience_month" placeholder="MM">
	                            	 	<option disabled selected="true">MM</option>
					                  @foreach (\CommonHelper::months() as $key=>$val)
					                    <option value="{{$key}}" @if(isset($courseJob) && !empty($courseJob) && $courseJob->dp_experience_month == $key )  selected @endif>{{$val}}</option>
					                  @endforeach					                 
					                </select>
	                           		
	                           	</div>
	                        </div>	                         
	                        <hr>
	                        <div class="form-group">
								<label class="col-sm-3 control-label">
									GMDSS Country
								</label>
								<div class="col-sm-4">
									<?php 

			                	    	if (isset($courseJob) && !$courseJob->jobMultipleGmdssCountry->isEmpty()) {
			                	    		
			                	    		$defaultGmdss = $courseJob->jobMultipleGmdssCountry->pluck('gmdss_country_id')->toArray();
			                	    	}else{

			                	    		$defaultGmdss = null;
			                	    	}
			                	    ?>
			                	    <select class="form-control job-select2" multiple name="gmdss_country_id[]" id="gmdss_country_id" placeholder="Select Country">
					                  @foreach ($countryData as $key=>$val)
					                    <option value="{{$key}}" @if(!empty($defaultGmdss) && in_array($key,$defaultGmdss)) selected @endif>{{$val}}</option>
					                  @endforeach					                 
					                </select>	                           		
									<span class="hide" id="gmdss_country_id-error" style="color: red"></span>
	                            </div>
	                            <div class="col-sm-2">
	                            	 <select class="form-control"  name="gmdss_experience_year" id="gmdss_experience_year" placeholder="YY">
	                            	 	<option disabled selected="true">YY</option>
					                  @foreach (\CommonHelper::years() as $key=>$val)
					                    <option value="{{$key}}" @if(isset($courseJob) && !empty($courseJob) && $courseJob->gmdss_experience_year == $key )  selected @endif>{{$val}}</option>
					                  @endforeach					                 
					                </select>	
	                           	</div>	                           	
	                            <div class="col-sm-2">
	                            	 <select class="form-control"  name="gmdss_experience_month" id="gmdss_experience_month" placeholder="MM">
	                            	 	<option disabled selected="true">MM</option>
					                  @foreach (\CommonHelper::months() as $key=>$val)
					                    <option value="{{$key}}" @if(isset($courseJob) && !empty($courseJob) && $courseJob->gmdss_experience_month == $key )  selected @endif>{{$val}}</option>
					                  @endforeach					                 
					                </select>
	                          
	                           	</div>
	                        </div>
	                        <hr>
	                        <div class="form-group">
								<label class="col-sm-3 control-label">
									COP and Country
								</label>
								<div class="col-sm-2">
									{{ Form::radio('cop',1,null,['class'=>''])}}
									Wk
									{{ Form::radio('cop',2,null,['class'=>''])}}
									COP
								</div>
	                           	<div class="col-sm-4">
	                           		<?php 

			                	    	if (isset($courseJob) && !$courseJob->jobMultipleCopCountry->isEmpty()) {
			                	    		
			                	    		$defaultCop = $courseJob->jobMultipleCopCountry->pluck('cop_country_id')->toArray();
			                	    	}else{

			                	    		$defaultCop = null;
			                	    	}
			                	    ?>
			                	      <select class="form-control job-select2" multiple name="cop_country_id[]" id="cop_country_id" placeholder="Select Country">
					                  @foreach ($countryData as $key=>$val)
					                    <option value="{{$key}}" @if(!empty($defaultCop) && in_array($key,$defaultCop)) selected @endif>{{$val}}</option>
					                  @endforeach					                 
					                </select>
	                           		
									<span class="hide" id="cop_country_id-error" style="color: red"></span>
	                            </div>	    

	                            <div class="col-sm-2">
	                            	 <select class="form-control"  name="cop_experience_year" id="cop_experience_year" placeholder="YY">
	                            	 	<option disabled selected="true">YY</option>
					                  @foreach (\CommonHelper::years() as $key=>$val)
					                    <option value="{{$key}}" @if(isset($courseJob) && !empty($courseJob) && $courseJob->cop_experience_year == $key )  selected @endif>{{$val}}</option>
					                  @endforeach					                 
					                </select>	
	                           	</div>	                           	
	                            <div class="col-sm-2">
	                            	<select class="form-control"  name="cop_experience_month" id="cop_experience_month" placeholder="MM">

	                            		<option disabled selected="true">MM</option>
					                  @foreach (\CommonHelper::months() as $key=>$val)
					                    <option value="{{$key}}" @if(isset($courseJob) && !empty($courseJob) && $courseJob->cop_experience_month == $key )  selected @endif>{{$val}}</option>
					                  @endforeach					                 
					                </select>	                           		
	                           	</div>
	                        </div>
	                        <hr>
							@if(isset($dceData) && !empty($dceData))
                    	 	<div class="form-group">
								<label class="col-sm-3 control-label">
									DCE
								</label>
								<div class="col-sm-6">
									@foreach($dceData as $key=>$v)

										<?php 

											$setDce = null;

											if (isset($courseJob) && !$courseJob->courseJobDce->isEmpty()) {
												
												$dceArray = $courseJob->courseJobDce->pluck('dce_id')->toArray();
												
												if (in_array($key,$dceArray)) {

													$setDce = true;
												}
											}
										?>
										{{ Form::checkbox('dce_id[]',$key,$setDce,['class'=>''])}}
										{{$v}}
									@endforeach

								</div>
	                        </div>
							@endif

							<?php 

	                	    	if (isset($courseJob) && !$courseJob->jobMultipleCourse->isEmpty()) {
	                	    		
	                	    		$defaultCourse = $courseJob->jobMultipleCourse->pluck('course_id')->toArray();

	                	    	}else{

	                	    		$defaultCourse = null;
	                	    	}
	                	    ?>

	                         <div class="form-group">
								<label class="col-sm-3 control-label">
									Course Required
								</label>
								<div class="col-sm-6">
									<select class="form-control job-select2" multiple name="course_id[]" id="course_id" placeholder="Select Course">
					                  @foreach ($allCourse as $key=>$val)
					                    <option value="{{$key}}" @if(!empty($defaultCourse) && in_array($key,$defaultCourse)) selected @endif>{{$val}}</option>
					                  @endforeach					                 
					                </select>	                           		
									<span class="hide" id="course_id-error" style="color: red"></span>
	                            </div>
	                        </div>

							<?php 

	                	    	if (isset($courseJob) && !$courseJob->jobMultipleShipType->isEmpty()) {
	                	    		
	                	    		$defaultShipType = $courseJob->jobMultipleShipType->pluck('experience_ship_type_id')->toArray();
	                	    		
	                	    	}else{

	                	    		$defaultShipType = null;
	                	    	}
	                	    ?>

	                         <div class="form-group">
								<label class="col-sm-3 control-label">
									Experience Required on Ship Type
								</label>
								<div class="col-sm-6">
									<select class="form-control job-select2" multiple name="experience_ship_type[]" id="experience_ship_type" placeholder="Ship type experience">
					                  @foreach (\CommonHelper::ship_type() as $key=>$val)
					                    <option value="{{$key}}" @if(!empty($defaultShipType) && in_array($key,$defaultShipType)) selected @endif>{{$val}}</option>
					                  @endforeach					                 
					                </select>	
	                           	
									<span class="hide" id="experience_ship_type-error" style="color: red"></span>
	                            </div>
	                        </div>

							<?php 

	                	    	if (isset($courseJob) && !$courseJob->jobMultipleEngineType->isEmpty()) {
	                	    		
	                	    		$defaultEngineType = $courseJob->jobMultipleEngineType->pluck('experience_engine_type_id')->toArray();
	                	    		
	                	    	}else{

	                	    		$defaultEngineType = null;
	                	    	}
	                	    ?>

	                         <div class="form-group">
								<label class="col-sm-3 control-label">
									Experience Required on Engine Type 
								</label>
								<div class="col-sm-6">
									<select class="form-control job-select2" multiple name="experience_engine_type[]" id="experience_engine_type" placeholder="Engine type experience">
					                  @foreach (\CommonHelper::engine_type() as $key=>$val)
					                    <option value="{{$key}}" @if(!empty($defaultEngineType) && in_array($key,$defaultEngineType)) selected @endif>{{$val}}</option>
					                  @endforeach					                 
					                </select>	                           		
									<span class="hide" id="experience_engine_type-error" style="color: red"></span>
	                            </div>
	                        </div>

	                         <div class="form-group">
								<label class="col-sm-3 control-label">
									Character Certificate Required
								</label>
								<div class="col-sm-6">
									{{ Form::radio('certification',1,null,['class'=>''])}}
									YES
									{{ Form::radio('certification',2,null,['class'=>''])}}
									NO
								</div>
	                        </div>

	                         <div class="form-group">
								<label class="col-sm-3 control-label">
									Vaccination Required
								</label>
	                           	<div class="col-sm-6">
	                           		<select class="form-control job-select2" name="vaccination" id="vaccination" placeholder="Select vaccination">
					                  @foreach ($vaccination as $key=>$val)
					                    <option value="{{$key}}"  @if(isset($courseJob) && !empty($courseJob) && $courseJob->vaccination == $key )  selected @endif>{{$val}}</option>
					                  @endforeach					                 
					                </select>	
									<span class="hide" id="vaccination-error" style="color: red"></span>
	                            </div>
	                        </div>
							<div class="row">
			                      <div class="col-md-12">
			                        <div class="row">
			                          <div class="col-md-offset-3">
			                            <button type="submit" class="btn btn-primary">Save</button>
			                            <a href="{{route('admin.j.index')}}" type="button" class="btn btn-danger">Cancel</a>
			                          </div>
			                        </div>
			                      </div>
			                      <div class="col-md-6">
			                      </div>
		                    </div>

						</div>
					</form>
				</div>
			</div>
		</div>
	</div>		

</div>
<div class="copy hide">
	<div class="form-group">
		<label class="col-sm-3 control-label">
			Email <span class="symbol required"></span>
		</label>
		<div class="col-sm-6 control-group input-group">	
			{{ Form::text('email_group[]',null,['placeholder'=>'Email Address','class'=>'form-control'])}}
			 <div class="input-group-btn"> 
              	<a href="javascript:void(0)" class="btn btn-danger remove"><i class="glyphicon glyphicon-remove"></i></a>
            </div>
		</div>

	</div>
</div>
@stop

@section('js_script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/additional-methods.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-jgrowl/1.4.7/jquery.jgrowl.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-jgrowl/1.4.7/jquery.jgrowl.css" />

<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

 <script>
 	  $(document).ready(function(){ 	  		
	 	$('.job-select2').select2();

		    $("#AddEditJob").validate({
		        rules: {
		           'company_id': { required: true },
		           'company_email_address_id': { required: true },
		           'rank_id': { required: true },
		           'title': { required: true },
		           'valid_till_date': { required: true },
		           'image': { extension: "jpg|png|jpeg" },
		        },
		        messages:{
		           'company_id': { required:"Please select company name."},
		           'company_email_address_id': { required:"Please select company email address."},
		           'rank_id': { required:"Please select job post rank."},
		           'title': { required:"Job title is required."},
		           'valid_till_date': { required:"Please select date."},
		        },
		        errorPlacement: function(error, element) {
		        		
					if (element.attr("name") == "email") {
		                
		                $("#email_e").html(error);

		            }else{
	          		
	          			$(element).closest('div').append(error);

		            }
		        }
		    });
		});

 	  $(document).on("click",".delete_record", function(){

	    swal({
	      title: "Are you sure want to delete this record ?",
	      icon: "warning",
	      buttons: true,
	      dangerMode: true,
	    })
	    .then((willDelete) => {
	      if (willDelete) {
	        var formAction = $(this).data("route");
	        var thisVar = $(this);
	        $.ajax({
	            type: "DELETE",
	            url: formAction,
	            success: function (response) {
	                if(response.success == 1){
              	      
              	      thisVar.parent().parents(".form-group").remove();
	                  swal(response.msg, {
	                    icon: "success",
	                    });

					}else if(response.success == 2){
	                  
	                    flashMessage('danger', response.msg);
	                    swal(response.msg, {
	                    icon: "warning",
	                    });

	                }else{
	                	
	                	flashMessage('danger', response.msg);
	                    swal(response.msg, {
	                    icon: "warning",
	                    });
	                }
	            },
	            error: function (jqXhr) {
	          }
	        });
	        
	      } 
	    });
	});

	$(document).ready(function() {

	  $(".add-more").click(function(){ 
	      var html = $(".copy").html();
	      $(".after-add-more").after(html);
	  });


	  $("body").on("click",".remove",function(){ 
	      $(this).parent().parents(".form-group").remove();
	  });


	});

	$(document).on("change","#company_id",function(){

		changeCompany();
	});

    function changeCompany()
    {   

		var company_id = $("#company_id").val();

		@if(isset($courseJob))

			var course_job_id = "{{$courseJob->id}}";

		@else
			
			var course_job_id = '';

		@endif

    	$.ajax({
	       url:"{{route('admin.j.getCompanyEmailAddress')}}",
	       data:{"_token": "{{ csrf_token() }}",company_id:company_id,course_job_id:course_job_id},
	       success:function(data)
	       {
	        $('#company_a').html(data);
	       }
      	});
    }

    changeCompany();

 </script>

@stop

