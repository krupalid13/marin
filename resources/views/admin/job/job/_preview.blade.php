<style>
	.vcard .thumbnail{float:left}
	.vcard > ul{list-style:none; margin:10px 0 0 120px; overflow:hidden}
	.vcard > ul > li:first-child{border-top:1px dashed #dcdcdc}
	.vcard > ul > li{padding:8px; border-bottom:1px dashed #dcdcdc}
	.vcard .item-key{float:left; color:#888}
	.vcard .vcard-item{margin-left:120px}
	.vcard .v-heading{background:#F0F9FF; font-weight:700}
	.vcard .v-heading span{font-weight:100; font-size:11px; color:#666}
	.vcard .item-list-more,
	.vcard .thumbnail.item-list-more{display:none}

	div#modal_form {
	    background-color: white;
	    padding: 30px;
	}

	.span12.paddingvcard {
	    padding: 5px;
	}
</style>
<?php 

    $countryData = \CommonHelper::countries();
?>
<div class="row">
	<div class="col-md-12">
		<h3 class="heading">
            Job Detail
        </h3>
        <br>
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th colspan="2">Detailing Job</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>Company Name</td>
                    <td><b>{{$job->company->name}}</b></td>
                </tr>
                <tr>
                    <td>Company Email Address</td>
                    <td><b>{{$job->companyEmailAddress->email}}</b></td>
                </tr>
                <tr>
                    <?php 
                        $rankData = \CommonHelper::new_rank();
                    ?>
                    <td>Job Post for Rank</td>
                    <td>
                        @if($job->rank_id !=null)
                            <b>{{getRankCommonFuncion($job->rank_id)}}</b>
                        @else
                        -
                        @endif
                    </td>
                </tr>
                <tr>
                    <td>Job Title </td>
                    <td><b>{{$job->title}}</b></td>
                </tr>
                <tr>
                    <td>About Job Msg</td>
                    <td>
                    	<b>
                    	@if(!empty($job->message))
                    	{{$job->message}}
                    	@else
                    		-
                    	@endif
                		</b>
                	</td>
                </tr>
                <tr>
                    <td>Valid till date </td>
                    <td><b>
                    	@if(!empty($job->valid_till_date))
                    	{{$job->valid_till_date}}
                    	@else
                    		-
                    	@endif</b></td>
                </tr>
                <tr>
                    <td>Source</td>
                    <td><b>
                    	@if(!empty($job->source))
                    	{{$job->source}}
                    	@else
                    		-
                    	@endif
                    	</b>
                    </td>
                </tr>
                <tr>
                    <td>Job status</td>
                    <td>
                    	<b>
                    	@if(!empty($job->jobStatus))
                    	{{$job->jobStatus->name}}
                    	@else
                    		-
                    	@endif
                    	</b>
                    </td>
                </tr>
            </tbody>
        </table>
         <table class="table table-bordered">
            <thead>
                <tr>
                    <th colspan="2">Additional Requirements</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>Passport Country</td>
                    <td>
                        @if(isset($job->jobMultiplePassportCountry) && !$job->jobMultiplePassportCountry->isEmpty())
                            @foreach($job->jobMultiplePassportCountry as $key=>$v)
                                    @if($v->passport_country_id !=null)
                                        @if(in_array($v->passport_country_id,array_keys($countryData)))
                                            {{$countryData[$v->passport_country_id]}}
                                        @endif
                                        @if($job->jobMultiplePassportCountry->reverse()->keys()->first() != $key) 
                                           <b> | </b> 
                                        @endif
                                    @endif
                            @endforeach

                        @else
                        -
                        @endif

                    </td>
                </tr>
               <?php /* <tr>
                    <td>Minimum Rank Experience (Year/Month)</td>
                    <td>
                    	@if(!empty($job->rank_experience_year))
                    	{{$job->rank_experience_year}}
                    	@else
                    		-
                    	@endif / 
                    	@if(!empty($job->rank_experience_yearmonth))
                    	{{$job->rank_experience_month}}
                    	@else
                    		-
                    	@endif
                    </td>
                </tr> */ ?>
                <tr>
                    <td>Expected Salary Range </td>
                    <td>
                    	@if(!empty($job->salary_type))
                    		@if($job->salary_type == 1)
                    			USD
                    		@elseif($job->salary_type == 2)
                    			INR
                    		@endif
                    		| 	
                    	@endif

                    	(Minimum wages / Maximum wages) ( 
                    	@if(!empty($job->minimum_wages))
                    		{{$job->minimum_wages}}
                    	@else
                    		-
                    	@endif / 
                    	
                    	@if(!empty($job->maximum_wages))
                    		{{$job->maximum_wages}}
                    	@else
                    		-
                    	@endif
                    	) 
                    </td>
                </tr>
                <tr>
                    <td>COC</td>
                    <td>
                    	@if(!empty($job->coc))
                    		{{$job->coc}}
                    	@else
                    		-
                    	@endif
                    </td>
                </tr>
                <tr>
                    <td>COC Country</td> 
                    <td>
                        @if(isset($job->jobMultipleCocCountry) && !$job->jobMultipleCocCountry->isEmpty())
                            @foreach($job->jobMultipleCocCountry as $key=>$v)
                                    @if($v->coc_country_id !=null)
                                        @if(in_array($v->coc_country_id,array_keys($countryData)))
                                            {{$countryData[$v->coc_country_id]}}
                                        @endif
                                        @if($job->jobMultipleCocCountry->reverse()->keys()->first() != $key) 
                                           <b> | </b> 
                                        @endif
                                    @endif
                            @endforeach
                        @else
                        -
                        @endif    
                    </td>
                </tr>
                <tr>
                    <td>COC Min Experience</td> 
                    <td>
                        @if($job->coc_experience_year != 0)
                        {{$job->coc_experience_year}} Year
                        @endif
                        @if($job->coc_experience_month != 0)
                        {{$job->coc_experience_month}} Month
                        @endif
                    </td>
                </tr>
                 <tr>
                    <td>COE</td>
                    <td>
                        @if(!empty($job->coe))
                            {{$job->coe}}
                        @else
                            -
                        @endif
                    </td>
                </tr>
                <tr>
                    <td>COE Country</td> 
                    <td>
                        @if(isset($job->jobMultipleCoeCountry) && !$job->jobMultipleCoeCountry->isEmpty())
                            @foreach($job->jobMultipleCoeCountry as $key=>$v)
                                    @if($v->coe_country_id !=null)
                                        @if(in_array($v->coe_country_id,array_keys($countryData)))
                                            {{$countryData[$v->coe_country_id]}}
                                        @endif
                                        @if($job->jobMultipleCoeCountry->reverse()->keys()->first() != $key) 
                                           <b> | </b> 
                                        @endif
                                    @endif
                            @endforeach
                        @else
                        -
                        @endif    
                    </td>
                </tr>
                <tr>
                    <td>COE Min Experience</td> 
                    <td>
                        @if($job->coe_experience_year != 0)
                        {{$job->coe_experience_year}} Year
                        @endif
                        @if($job->coe_experience_month != 0)
                        {{$job->coe_experience_month}} Month
                        @endif
                    </td>
                </tr>
                <tr>
                    <td>DP</td>
                    <td>
                        @if(!empty($job->dp))
                            {{$job->dp}}
                        @else
                            -
                        @endif
                    </td>
                </tr>
                <tr>
                    <td>DP Country</td> 
                    <td>
                        @if(isset($job->jobMultipleDpCountry) && !$job->jobMultipleDpCountry->isEmpty())
                            @foreach($job->jobMultipleDpCountry as $key=>$v)
                                    @if($v->dp_country_id !=null)
                                        @if(in_array($v->dp_country_id,array_keys($countryData)))
                                            {{$countryData[$v->dp_country_id]}}
                                        @endif
                                        @if($job->jobMultipleDpCountry->reverse()->keys()->first() != $key) 
                                           <b> | </b> 
                                        @endif
                                    @endif
                            @endforeach
                        @else
                        -
                        @endif    
                    </td>
                </tr>
                <tr>
                    <td>DP Min Experience</td> 
                    <td>
                        @if($job->dp_experience_year != 0)
                        {{$job->dp_experience_year}} Year
                        @endif
                        @if($job->dp_experience_month != 0)
                        {{$job->dp_experience_month}} Month
                        @endif
                    </td>
                </tr>
               
                <tr>
                    <td>COP</td>
                    <td>
                        @if(!empty($job->cop))
                            @if($job->cop == 1)
                                WK
                            @else
                                COP
                            @endif
                        @else
                            -
                        @endif
                    </td>
                </tr>
                <tr>
                    <td>COP Country</td> 
                    <td>
                        @if(isset($job->jobMultipleCopCountry) && !$job->jobMultipleCopCountry->isEmpty())
                            @foreach($job->jobMultipleCopCountry as $key=>$v)
                                    @if($v->cop_country_id !=null)
                                        @if(in_array($v->cop_country_id,array_keys($countryData)))
                                            {{$countryData[$v->cop_country_id]}}
                                        @endif
                                        @if($job->jobMultipleCopCountry->reverse()->keys()->first() != $key) 
                                           <b> | </b> 
                                        @endif
                                    @endif
                            @endforeach
                        @else
                        -
                        @endif    
                    </td>
                </tr>
                <tr>
                    <td>COP Min Experience</td> 
                    <td>
                        @if($job->cop_experience_year != 0)
                        {{$job->cop_experience_year}} Year
                        @endif
                        @if($job->cop_experience_month != 0)
                        {{$job->cop_experience_month}} Month
                        @endif
                    </td>
                </tr>
                <tr>
                    <td>GMDSS Country</td> 
                    <td>
                        @if(isset($job->jobMultipleGmdssCountry) && !$job->jobMultipleGmdssCountry->isEmpty())
                            @foreach($job->jobMultipleGmdssCountry as $key=>$v)
                                    @if($v->gmdss_country_id !=null)
                                        @if(in_array($v->gmdss_country_id,array_keys($countryData)))
                                            {{$countryData[$v->gmdss_country_id]}}
                                        @endif
                                        @if($job->jobMultipleGmdssCountry->reverse()->keys()->first() != $key) 
                                           <b> | </b> 
                                        @endif
                                    @endif
                            @endforeach
                        @else
                        -
                        @endif    
                    </td>
                </tr>
                <tr>
                    <td>GMDSS Min Experience</td> 
                    <td>
                        @if($job->gmdss_experience_year != 0)
                        {{$job->gmdss_experience_year}} Year
                        @endif
                        @if($job->gmdss_experience_month != 0)
                        {{$job->gmdss_experience_month}} Month
                        @endif
                    </td>
                </tr>
                
                
                <tr>
                    <td>DCE</td>
                    <td>
                        @if(isset($job->courseJobDce) && !$job->courseJobDce->isEmpty())
                            @foreach($job->courseJobDce as $key=>$v)
                                    @if($v->dce !=null)
                                        {{ $v->dce->name }}
                                        @if($job->courseJobDce->reverse()->keys()->first() != $key) 
                                           <b> | </b> 
                                        @endif
                                    @endif
                            @endforeach

                        @else
                        -
                        @endif
                    </td>
                </tr>

                <tr>
                    <td>Course Required</td>
                    <td>
                        @if(isset($job->jobMultipleCourse) && !$job->jobMultipleCourse->isEmpty())
                            @foreach($job->jobMultipleCourse as $key=>$v)
                                    @if($v->course !=null)
                                        {{ $v->course->course_name }}
                                        @if($job->jobMultipleCourse->reverse()->keys()->first() != $key) 
                                           <b> | </b> 
                                        @endif
                                    @endif
                            @endforeach

                        @else
                        -
                        @endif
                    </td>
                </tr>
                <tr>
                    <td>Experience Required on Ship Type</td>
                    <td>
                        @if(isset($job->jobMultipleShipType) && !$job->jobMultipleShipType->isEmpty())
                            <?php $shipType = \CommonHelper::ship_type(); ?>
                            @foreach($job->jobMultipleShipType as $key=>$v)
                                    @if(in_array($v->experience_ship_type_id,array_keys($shipType)))
                                        {{ $shipType[$v->experience_ship_type_id] }}
                                        @if($job->jobMultipleShipType->reverse()->keys()->first() != $key) 
                                           <b> | </b> 
                                        @endif
                                    @endif
                            @endforeach

                        @else
                        -
                        @endif

                    </td>
                </tr>
                <tr>
                    <td>Experience Required on Engine Type</td>
                    <td> 
                        @if(isset($job->jobMultipleEngineType) && !$job->jobMultipleEngineType->isEmpty())
                            <?php $engineType = \CommonHelper::engine_type(); ?>
                            @foreach($job->jobMultipleEngineType as $key=>$v)
                                    @if(in_array($v->experience_engine_type_id,array_keys($engineType)))
                                        {{ $engineType[$v->experience_engine_type_id] }}
                                        @if($job->jobMultipleEngineType->reverse()->keys()->first() != $key) 
                                           <b> | </b> 
                                        @endif
                                    @endif
                            @endforeach

                        @else
                        -
                        @endif
                    </td>
                </tr>
                <tr>
                    <td>Character Certificate Required</td>
                    <td>
                    	@if(!empty($job->certification))
                    		@if($job->certification ==  1)
                    		Yes
                    		@else
                    		No
                    		@endif
                    	@else
                    		-
                    	@endif
                    </td>
                </tr>
                <tr>
                    <td>Vaccination Required</td>
                    <td>
                        @if(!empty($job->getVaccination))
                    		{{$job->getVaccination->name}}
                    	@else
                    		-
                    	@endif
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>