@extends('admin.index')

@section('style_section')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css">

<style>
	div#padding_to_add {
	    padding: 25px;
	}
</style>
@endsection

@section('content')
<div class="container">
	<div class="row-fluid">
		<div class="row" id="padding_to_add">
		    <div class="col-12">
		    	<div class="pull-right">
	                <a class="btn btn-primary" href="{{ route('admin.j.create') }}" title="Add Job">Create a Job</a>
	        	</div>
		    </div>
		</div>
		<br>
		<div class="datatable-container">

		         {{ Form::open(
		          array(
		          'id'                => 'table_form',
		          'name'              => 'table_form',
		          'class'             => 'ajaxFormSubmitDatatable',
		          'method'              => 'POST'
		          ))
		          }}

		            <table class="table table-bordered table-hover" id="dataTableBuilder" width="100%">
		                <thead>
		                	<th>Company Name</th>
		                	<th>Rank</th>
		                	<th>Job Id</th>
		                	<th>Job Title</th>
		                	<th>Validity</th>
		                	<th>Action</th>
		                </thead>
		                <tbody></tbody>
		                <tfoot></tfoot>
		            </table>
		        </form>
		    </div>
		</div>
	</div>

</div>

@stop

@section('js_script')
<script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
<script>
	jQuery(document).ready(function() {
		FormWizard.init();
	});
</script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
    
	$(document).on("click",".delete_record", function(){

	    swal({
	      title: "Are you sure want to delete this record ?",
	      icon: "warning",
	      buttons: true,
	      dangerMode: true,
	    })
	    .then((willDelete) => {
	      if (willDelete) {
	        var formAction = $(this).data("route");
	        $.ajax({
	            type: "DELETE",
	            url: formAction,
	            success: function (response) {
	                if(response.success == 1){
	                   $('.table').DataTable().draw(false);
	                  swal(response.msg, {
	                    icon: "success",
	                    });

	                }else{
	                    flashMessage('danger', response.msg);
	                    swal(response.msg, {
	                    icon: "warning",
	                    });
	                }
	            },
	            error: function (jqXhr) {
	          }
	        });
	        
	      } 
	    });
	});

   function RESET_FORM()
    {
        $('#search-frm').trigger('reset');
        table.draw();
        return false;
    }
    $(document).on("click","button[type='submit']",function(){
          table.draw();
          return false;
    });
    var table = $('#dataTableBuilder').DataTable({
        bProcessing: true,
        bServerSide: true,
        processing: true,
        serverSide: true,
        searching: true,
        ajax: {
            url: "{{ route('admin.j.get') }}?datatable=no",
            data: function (d) {
                d.id = $('input[name=id]').val();
                d.title   = $('input[name=title]').val();
            }
        },
        columns: [
            {data: 'created_at' },
            {data: 'rank_id' },
            {data: 'unique_code' },
            {data: 'title' },
            {data: 'valid_till_date' },
            {data: 'id' },
        ]
    });
</script>
@stop