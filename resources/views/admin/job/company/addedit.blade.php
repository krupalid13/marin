@extends('admin.index')

@section('style_section')

@endsection

@section('content')
<div class="container">

  	<div class="row">
		<div class="col-sm-12">
				<div class="panel panel-white">
				<div class="panel-body">
					@if(isset($company))
	                    {{ Form::model($company,
	                      array(
	                      'id'                => 'AddEditCompany',
	                      'class'             => 'companyForm form-horizontal',
	                      'data-redirect_url' => route('admin.getCompanyList'),
	                      'url'               => route('admin.updateCompany', $encryptedId),
	                      'method'            => 'PUT',
	                      'enctype'           =>"multipart/form-data"
	                      ))
	                    }}
                  	@else
	                    {{
	                      Form::open([
	                      'id'=>'AddEditCompany',
	                      'class'=>'companyForm form-horizontal',
	                      'url'=>route('admin.storeCompany'),
	                      'data-redirect_url'=>route('admin.getCompanyList'),
	                      'name'=>'Company',
	                      'enctype' =>"multipart/form-data"
	                      ])
	                    }}
                  	@endif

                  		<input type="hidden" name="id" value="{{$encryptedId}}">
						{{ csrf_field() }}
						
						<div id="step-1">
								
							<h2 class="StepTitle">
							@if(isset($company))
								Edit
							@else
								Add
							@endif
								Company
							</h2>
							<div class="panel-heading">
								<h4 class="panel-title">Company Details</h4>
							</div>

	                	    <div class="form-group">
								<label class="col-sm-3 control-label">
									Name <span class="symbol required"></span>
								</label>
	                           	<div class="col-sm-6">
	                           		{{ Form::text('name',null,['placeholder'=>'Type your company name','id'=>'name','class'=>'form-control'])}}
									<span class="hide" id="name-error" style="color: red"></span>
	                            </div>
	                        </div>

	                        <div class="form-group">
								<label class="col-sm-3 control-label">
									Type <span class="symbol required"></span>
								</label>
	                           	<div class="col-sm-6">
	                           		{{ Form::radio('type',1,null,['class'=>'type'])}}
	                           		Owner Company
	                           		{{ Form::radio('type',2,null,['class'=>'type'])}}
	                           		Manning Company
									<span class="hide" id="name-error" style="color: red"></span>
	                            </div>
	                        </div>
							
							<div class="form-group">
								<label class="col-sm-3 control-label">
									Official No
								</label>
								<div class="col-sm-6">
									{{ Form::text('official_no',null,['placeholder'=>'Official contact no','id'=>'official_no','class'=>'form-control allow_numeric_only'])}}
								</div>
							</div>

							<div class="form-group after-add-more-contact">
								<label class="col-sm-3 control-label">
									Contact Number 
								</label>
								<div class="col-sm-6 input-group control-group">	
									{{ Form::text('contact_group[]',null,['placeholder'=>'Contact Number','class'=>'form-control allow_numeric_only'])}}
									<div class="input-group-btn"> 
						            	<a href="javascript:void(0)" class="btn btn-success add-more-contact"><i class="glyphicon glyphicon-plus"></i></a>
						          </div>
								</div>
						       	 <div id="contact_number_e" class="col-md-12 col-md-offset-3"></div>
							</div>

							@if(isset($company) && !$company->companyContacts->isEmpty())
								@foreach($company->companyContacts as $key=>$v)
									<div class="form-group">
										<label class="col-sm-3 control-label">
											Contact Number 
										</label>
										<div class="col-sm-6 control-group input-group">	
											<input type="text" name="update_contact_group[{{$v->id}}]" value="{{$v->contact_number}}" placeholder="Contact Number"  class="form-control allow_numeric_only">
											 <div class="input-group-btn"> 
											 	<a href="javascript:void(0)" data-route="{{route('admin.deleteCompanyContact',\Crypt::encrypt($v->id))}}" class="btn btn-danger delete_contact_record"><i class="fa fa-trash" aria-hidden="true"></i></a>

								            </div>
										</div>
									</div>
								@endforeach
							@endif

							   <div class="form-group">
								<label class="col-sm-3 control-label">
									 Expiry date 
								</label>
	                           	<div class="col-sm-6">
	                           		{{ Form::date('expiry_date',null,['id'=>'expiry_date','class'=>'form-control'])}}
									
	                            </div>
	                        </div>

							<div class="form-group">
								<label class="col-sm-3 control-label">
									Website link
								</label>
								<div class="col-sm-6">
	                           		{{ Form::url('website',null,['placeholder'=>'Website link','id'=>'website','class'=>'form-control'])}}
	                           	</div>
							</div>

							<div class="form-group">
								<label class="col-sm-3 control-label">
									Address <span class="symbol required"></span>
								</label>
								<div class="col-sm-6">
									{{ Form::textarea('address',null,['placeholder'=>'Address','id'=>'address','class'=>'form-control'])}}
								</div>
							</div>	

							<div class="form-group">
								<label class="col-sm-3 control-label">
									Operating vessel type
								</label>
								<div class="col-sm-6">
									{{ Form::text('operating_vessel_type',null,['placeholder'=>'Operating vessel type','id'=>'Operating_vessel_type','class'=>'form-control'])}}
								</div>
							</div>							

							<div class="form-group after-add-more">
								<label class="col-sm-3 control-label">
									Email <span class="symbol required"></span>
								</label>
								<div class="col-sm-6 input-group control-group">	
									{{ Form::email('email',null,['placeholder'=>'Email Address','id'=>'email','class'=>'form-control'])}}
									<div class="input-group-btn"> 
						            	<a href="javascript:void(0)" class="btn btn-success add-more"><i class="glyphicon glyphicon-plus"></i></a>
						          </div>
								</div>
						       	 <div id="email_e" class="col-md-12 col-md-offset-3"></div>
							</div>
							@if(isset($company) && !$company->companyEmailAddress->isEmpty())
								@foreach($company->companyEmailAddress as $key=>$v)
									<div class="form-group">
										<label class="col-sm-3 control-label">
											Email <span class="symbol required"></span>
										</label>
										<div class="col-sm-6 control-group input-group">	
											<input type="email" name="update_email_group[{{$v->id}}]" value="{{$v->email}}" placeholder="Email Address" id="email" class="form-control">
											 <div class="input-group-btn"> 
											 	<a href="javascript:void(0)" data-route="{{route('admin.deleteCompanyEmailAddress',\Crypt::encrypt($v->id))}}" class="btn btn-danger delete_address_record"><i class="fa fa-trash" aria-hidden="true"></i></a>

								            </div>
										</div>
									</div>
								@endforeach
							@endif

							<div class="row">
			                      <div class="col-md-12">
			                        <div class="row">
			                          <div class="col-md-offset-3">
			                            <button type="submit" class="btn btn-primary">Save</button>
			                            <a href="{{route('admin.getCompanyList')}}" type="button" class="btn btn-danger">Cancel</a>
			                          </div>
			                        </div>
			                      </div>
			                      <div class="col-md-6">
			                      </div>
		                    </div>

						</div>
					</form>
				</div>
			</div>
		</div>
	</div>		

</div>
<div class="copy hide">
	<div class="form-group">
		<label class="col-sm-3 control-label">
			Email <span class="symbol required"></span>
		</label>
		<div class="col-sm-6 control-group input-group">	
			{{ Form::text('email_group[]',null,['placeholder'=>'Email Address','class'=>'form-control'])}}
			 <div class="input-group-btn"> 
              	<a href="javascript:void(0)" class="btn btn-danger remove"><i class="glyphicon glyphicon-remove"></i></a>
            </div>
		</div>

	</div>
</div>
<div class="contact_copy hide">
	<div class="form-group">
		<label class="col-sm-3 control-label">
			Contact Number 
		</label>
		<div class="col-sm-6 control-group input-group">	
			{{ Form::text('contact_group[]',null,['placeholder'=>'Contact Number','class'=>'form-control'])}}
			 <div class="input-group-btn"> 
              	<a href="javascript:void(0)" class="btn btn-danger remove_contact"><i class="glyphicon glyphicon-remove"></i></a>
            </div>
		</div>

	</div>
</div>
@stop

@section('js_script')

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/additional-methods.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-jgrowl/1.4.7/jquery.jgrowl.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-jgrowl/1.4.7/jquery.jgrowl.css" />

<script src="{{asset('public/assets/responsive/sweetalert.min.js')}}"></script>

 <script>
 	  $(document).ready(function(){
		    $("#AddEditCompany").validate({
		        rules: {
		           'name': { required: true,maxlength:70},
		           'official_no': { maxlength:20},
		           'address': { required: true},
		           'type': { required: true},
		           'operating_vessel_type': { maxlength:150},
		           'email': { required: true,email:true },
		           'url': { url:true },
		        },
		        messages:{
		           'name': { required:"Company name field is required."},
		           'address': { required:"Company address field is required."},
		           'type': { required:"Company type field is required."},
		           'email': { required:"Company email field is required."},
		        },
		        errorPlacement: function(error, element) {
		        		
					if (element.attr("name") == "email") {
		                
		                $("#email_e").html(error);

		            }else{
	          		
	          			$(element).closest('div').append(error);

		            }
		        }
		    });
		});

	$(document).ready(function() {

	  $(".add-more").click(function(){ 
	      var html = $(".copy").html();
	      $(".after-add-more").after(html);
	  });

	   $(".add-more-contact").click(function(){ 
	      var html = $(".contact_copy").html();
	      $(".after-add-more-contact").after(html);
	  });


	  $("body").on("click",".remove",function(){ 
	      $(this).parent().parents(".form-group").remove();
	  });

	  $("body").on("click",".remove_contact",function(){ 
	      $(this).parent().parents(".form-group").remove();
	  });


	});

	$(document).on("click",".delete_contact_record", function(){
		 swal({
	      title: "Are you sure want to delete this record ?",
	      icon: "warning",
	      buttons: true,
	      dangerMode: true,
	    })
	    .then((willDelete) => {
	      if (willDelete) {
	        var formAction = $(this).data("route");
	        var thisVar = $(this);
	        $.ajax({
	            type: "DELETE",
	            url: formAction,
	            success: function (response) {
	                if(response.success == 1){
              	      
              	      thisVar.parent().parents(".form-group").remove();
	                  swal(response.msg, {
	                    icon: "success",
	                    });

					}else if(response.success == 2){
	                  
	                    flashMessage('danger', response.msg);
	                    swal(response.msg, {
	                    icon: "warning",
	                    });

	                }else{
	                	
	                	flashMessage('danger', response.msg);
	                    swal(response.msg, {
	                    icon: "warning",
	                    });
	                }
	            },
	            error: function (jqXhr) {
	          }
	        });
	        
	      } 
	    });

	});
	$(document).on("click",".delete_address_record", function(){

	    swal({
	      title: "Are you sure want to delete this record ?",
	      icon: "warning",
	      buttons: true,
	      dangerMode: true,
	    })
	    .then((willDelete) => {
	      if (willDelete) {
	        var formAction = $(this).data("route");
	        var thisVar = $(this);
	        $.ajax({
	            type: "DELETE",
	            url: formAction,
	            success: function (response) {
	                if(response.success == 1){
              	      
              	      thisVar.parent().parents(".form-group").remove();
	                  swal(response.msg, {
	                    icon: "success",
	                    });

					}else if(response.success == 2){
	                  
	                    flashMessage('danger', response.msg);
	                    swal(response.msg, {
	                    icon: "warning",
	                    });

	                }else{
	                	
	                	flashMessage('danger', response.msg);
	                    swal(response.msg, {
	                    icon: "warning",
	                    });
	                }
	            },
	            error: function (jqXhr) {
	          }
	        });
	        
	      } 
	    });
	});

	$.ajaxSetup({
	  headers: {
	    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	  }
	});
	$('form.companyForm').submit(function (event) {

    var formId = $(this).attr('id');
    // return false;
    if ($('#'+formId).valid()) {

        var formAction = $(this).attr('action');
        var buttonText = $('#'+formId+' button[type="submit"]').text();
        var $btn = $('#'+formId+' button[type="submit"]').attr('disabled','disabled').html("Loading...");
        var redirectURL = $(this).data("redirect_url");
        $.ajax({
            type: "POST",
            url: formAction,
            data: new FormData(this),
            contentType: false,
            processData: false,
            enctype: 'multipart/form-data',
            success: function (response) {
                if (response.status == 'success' && response.msg !="") {
                    $('#'+formId+' button[type="submit"]').text(buttonText);
                    $('#'+formId+' button[type="submit"]').removeAttr('disabled','disabled');
                    toastr.success(response.msg, 'Success');
                    window.location=redirectURL;
                }else if (response.status == 'error' && response.msg !="") {
                     toastr.error(response.msg, 'Error');
                }
            },
            error: function (response) {
               toastr.error(response.msg, 'Error');
                $('#'+formId+' button[type="submit"]').text(buttonText);
                $('#'+formId+' button[type="submit"]').removeAttr('disabled','disabled');
            }
        });

        return false; 

    }else{

        return false;
    }
    
});
 </script>

@stop