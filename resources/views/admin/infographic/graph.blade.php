<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport"
              content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link href='http://fonts.googleapis.com/css?family=Raleway:400,300,500,600,700,200,100,800' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="{{URL:: asset('public/assets/css/vendors/plugins/bootstrap/bootstrap.min.css')}}">
        <link rel="stylesheet" href="{{URL:: asset('public/assets/css/vendors/plugins/fontawesome/font-awesome.min.scss')}}">
        <link rel="stylesheet" href="{{URL:: asset('public/assets/css/style.css')}}">
        <link rel="stylesheet" href="{{URL:: asset('public/assets/css/css/site_app.css')}}">

        <script src="{{URL:: asset('public/assets/js/admin/infograph/graph.js')}}"></script>
        <script type="text/javascript" src="{{URL :: asset('public/assets/js/site_jquery.js')}}"></script>
        <title>
            @if(isset($pageTitle) && !empty($pageTitle))
            {!! $pageTitle !!}
            @else
            {{env('APP_NAME')}}
            @endif
        </title>
        <link rel="shortcut icon" type="image/x-icon" href="{{asset('public/images/favicon.ico')}}">
        <meta name="description" content="{!! ($metaDescription) ?? '' !!}">
        <meta name="keywords" content="{!! ($metaKeywords) ?? '' !!}">
        <style>
            .graph-img{    border: 1px solid;
                           border-radius: 22px;
                           padding: 10px;}
            </style>
        </head>
        <body>
            @include('site.partials.header')
            @if($user_contract['is_graph']==true)
            <div class="container pt-10 mb-15 sea-career-graph">
            <div class="row border-grey add-more-section">
                <div class="text-center"><h4 style="background: #2a55a2;color: #fff;border-radius: 4px;padding: 10px;font-size: 16px;">Sea Career</h4></div>
                <div class="col-xs-12 col-sm-6">
                    <div class="text-center m-t-5">
                        <div class="d-ib p-5 border-grey w-210 bg-primary"> {{--h-184--}}
                            <h5 class="text-uppercase">Contract</h5>
                            <p class="font-15">
                                <b>{{$user_contract['total_contract']}}</b> Sails
                            </p>
                            <p class="font-15">
                                @foreach(\CommonHelper::new_rank() as $index => $category)
                                @foreach($category as $r_index => $rank)
                                {{ !empty($user_contract['current_rank']) ? $user_contract['current_rank'] == $r_index ? $rank : '' : ''}}
                                @endforeach
                                @endforeach : {{$user_contract['current_rank_contract']}} Sails
                            </p>
                            <p class="font-15">
                                Other : {{$user_contract['total_contract'] - $user_contract['current_rank_contract']}} Sails
                            </p>
                        </div>
                    </div>
                    <div class="text-center m-t-5 pb-10">
                        <div class="d-ib p-5 border-grey w-210 bg-primary"> {{--h-184--}}
                            <h5 class="text-uppercase">
                                Sea Time
                            </h5>
                            <p class="font-15">
                                <b>{{$user_contract['total_duration']}}</b> Days
                            </p>
                            <p class="font-15">
                                @foreach(\CommonHelper::new_rank() as $index => $category)
                                @foreach($category as $r_index => $rank)
                                {{ !empty($user_contract['current_rank']) ? $user_contract['current_rank'] == $r_index ? $rank : '' : ''}}
                                @endforeach
                                @endforeach : {{$user_contract['current_rank_duration']}} Days
                            </p>
                            <p class="font-15">
                                Other : {{$user_contract['total_duration'] - $user_contract['current_rank_duration']}} Days
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 pb-20">
                    {{--            <div id="canvas-holder" class="table-responsive">--}}
                    <canvas id="chart-area" width="569" height="405" style="max-height:404px; max-width:404px"></canvas>
                    {{--            </div>--}}
                </div>
            </div>
        </div>
        <script>
@php
        $count = count($user_contract['sea_career']);
@endphp
        var config = {
        type: 'pie',
                data: {

                datasets: [{
                data: [
                        @foreach($user_contract['sea_career'] as $key => $ship)
                {{$ship}},
                        @endforeach
                ],
                        backgroundColor: [
                                @for ($i = 0; $i < $count; $i++)
                                '#{{str_pad(dechex(rand(0x000000, 0xFFFFFF)), 6, 0, STR_PAD_LEFT)}}',
                                @endfor
                        ]
                }],
                        labels: [
                                @foreach($user_contract['sea_career'] as $key => $ship)
                                '{{\CommonHelper::ship_type()[$key]}}',
                                @endforeach
                        ]
                },
                options: {
                // responsive: true,
                legend: {
                position:'left',
                        display: true,
                },
                        title: {
                        display: true,
                                text: 'Sailing summary (Days)'
                        }
                }
        };
        </script>
        <br>
        <div class="container mb-15 border-grey">
            <div class="table-responsive">
                @if(!empty($user_contract['all_ship'])) <canvas id="bar_chart" style="min-height:350px" class="table"></canvas> @endif
            </div>
            <script>
                Chart.Tooltip.positioners.custom = function(elements, eventPosition) {
                var tooltip = this;
                return {
                x: 0,
                        y: 0
                };
                };
                var barChartData = {
                labels: [@foreach($user_contract['ships'] as $ship_key => $ship) '{{\CommonHelper::ship_type()[$ship_key]}}', @endforeach],
                        datasets: [@if (!empty($user_contract['all_ship']))@foreach($user_contract['all_ship'] as $key => $all_ship_value)
                                @php
                                asort($all_ship_value);
                        @endphp
                        {
                        label: @foreach(\CommonHelper::new_rank() as $index => $category)
                                @foreach($category as $key_rank => $rank)
                                @if ($key == $key_rank)
                                '{{$rank}}'
                                @endif
                                @endforeach
                                @endforeach,
                                backgroundColor: '#{{str_pad(dechex(rand(0x000000, 0xFFFFFF)), 6, 0, STR_PAD_LEFT)}}',
                                yAxisID: 'y-axis-2',
                                data: [
                                        @php
                                        ksort($all_ship_value);
                                @endphp
                                        @foreach($all_ship_value as $_key => $_all_ship_value)
                                        '{{$_all_ship_value["all_days"]}}',
                                        @endforeach
                                ]
                        },
                                @endforeach
                                @endif
                        ]

                };
                window.onload = function() {
                var ctx = document.getElementById('chart-area').getContext('2d');
                window.myPie = new Chart(ctx, config);
                @if (!empty($user_contract['all_ship']))
                        var chart_bar = document.getElementById('bar_chart').getContext('2d');
                window.myBar = new Chart(chart_bar, {
                type: 'bar',
                        data: barChartData,
                        options: {
                        tooltips: {
                        /*callbacks: {
                         label: function(tooltipItem) {
                         return Number(tooltipItem.yLabel) + " Day(s)";
                         }
                         },*/
                        mode: 'index',
                                intersect: true
                        },
                                responsive: true,
                                maintainAspectRatio: false,
                                title: {
                                display: true,
                                        text: 'Experience on Ship Type (Days) ',
                                        fontSize:20,
                                        fontColor:'#a52a2a',
                                },
                                scales: {
                                yAxes: [{
                                scaleLabel: {
                                display: true,
                                        labelString: 'Days',
                                        fontStyle: 'bold',
                                },
                                        type: 'linear',
                                        display: true,
                                        position: 'left',
                                        id: 'y-axis-2',
                                        gridLines: {
                                        drawOnChartArea: false
                                        }
                                }, ],
                                        xAxes: [{
                                        scaleLabel: {
                                        display: true,
                                                labelString: 'Ships',
                                                fontStyle: 'bold',
                                        },
                                                barPercentage: 0.44
                                        }],
                                }
                        }
                });
                @endif
                        @if (!empty($user_contract['all_engine']))
                        var ctx = document.getElementById('canvas').getContext('2d');
                window.myHorizontalBar = new Chart(ctx, {
                type: 'horizontalBar',
                        data: horizontalBarChartData,
                        options: {
                        /*tooltips: {
                         callbacks: {
                         label: function(tooltipItem) {
                         return Number(tooltipItem.xLabel) + " Day(s)";
                         }
                         }
                         }*/
                        elements: {
                        rectangle: {
                        borderWidth: 0.14,
                        }
                        },
                                responsive: true,
                                maintainAspectRatio:false,
                                scales: {
                                xAxes: [{
                                stacked: true,
                                        scaleLabel: {
                                        display: true,
                                                labelString: 'Days',
                                                fontStyle: 'bold',
                                        }
                                }],
                                        yAxes: [{
                                        stacked: true,
                                                barPercentage: 0.20,
                                                scaleLabel: {
                                                display: true,
                                                        labelString: 'Engine',
                                                        fontStyle: 'bold',
                                                }
                                        },
                                        ]
                                },
                                legend: {
                                position: 'right',
                                },
                                title: {
                                display: true,
                                        text: 'Experience on Engine Type (Days)',
                                        fontSize:20,
                                        fontColor:'#042c73',
                                }
                        }
                });
                @endif
                };
            </script>
        </div>
        @if(!empty($user_contract['all_engine']))
        <br>
        <section class="container border-grey" >
            <div class="table-responsive">
                <canvas id="canvas" style="min-height:250px" class="table"></canvas>
            </div>
            <script>
                var horizontalBarChartData = {
                labels: [@foreach($user_contract['engine'] as $engine_key => $engine) '{!! (\CommonHelper::engine_type($user_id)[$engine_key]) !!}', @endforeach],
                        datasets: [@if (!empty($user_contract['all_engine']))@foreach($user_contract['all_engine'] as $key => $all_engine_value)
                                @php
                                asort($all_engine_value);
                        @endphp
                        {
                        label: @foreach(\CommonHelper::new_rank() as $index => $category)
                                @foreach($category as $key_rank => $rank)
                                @if ($key == $key_rank)
                                '{{$rank}}'
                                @endif
                                @endforeach
                                @endforeach,
                                backgroundColor: '#{{str_pad(dechex(rand(0x000000, 0xFFFFFF)), 6, 0, STR_PAD_LEFT)}}',
                                borderWidth: 1,
                                // borderColor: 'red',
                                data: [
                                        @php
                                        ksort($all_engine_value);
                                @endphp
                                        @foreach($all_engine_value as $_key => $_all_engine_value)
                                        '{{$_all_engine_value["all_days"]}}',
                                        @endforeach
                                ]
                        },
                                @endforeach
                                @endif]

                };
            </script>
        </section>
        @endif
        @else
        @if(\Route::getCurrentRoute()->getPath() == "share-graph")
        <h4 class="text-center h4 heading">Skill Infographics</h4>
        <div class="col-md-12">
            <img class="img-responsive center-block graph-img" src="{{ URL:: asset('public/images/no_sea_service_added.png')}}">
        </div>
        @else
        <div class="row">
            <div class="container m_h-100v">
                <h4 class="text-center h4 heading">Skill Infographics</h4>
                <div class="mt-30 panel panel-bricky">
                    <div class="panel-heading text-center">
                        <h4 class="h4">Kindly <a href="{{route('site.seafarer.edit.profile')}}">Add</a> 
                            sea service to view a graphical representation of your experience.</h4>
                    </div>
                </div>
                <div class="col-md-12">
                    <img class="img-responsive center-block graph-img" src="{{ URL:: asset('public/images/infographic.png')}}">
                </div>
            </div>
        </div>
        @endif
        @endif

        <br><br><br><br>
        @include('site.partials.new_footer')
        <script type="text/javascript" src="{{ URL:: asset('public/assets/js/site_plugins.js')}}?v=123"></script>
        <script type="text/javascript" src="{{ URL:: asset('public/assets/js/site_app.js')}}?v=123"></script>
        <script type="text/javascript" src="{{ URL:: asset('public/assets/js/site/home.js')}}?v=123"></script>
    </body>
</html>