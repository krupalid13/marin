@extends('admin.index')
@section('style_section')
@endsection
@section('content')

@if(isset($data) && !empty($data))
  @php 
    $val = $data;
    $name = $val->name;
    $email = $val->email; 
    $redemption = $val->redemption;
    $commission = $val->commission;
    $phone_number = $val->phone_number;
    $id = $val->id;
    $affiliate_user_id = $val->user_id;
    $operation = 'Edit';
  @endphp
@else
  @php 
    $name = '';
    $email = '';
    $redemption = '';
    $commission = '';
    $phone_number = '';
    $operation = 'Add'; 
    $affiliate_user_id = '';
    $id = '';
  @endphp
@endif 

<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-white">
                <div class="panel-body">
                    <form method="POST" action="" accept-charset="UTF-8" id="add_affiliate" class=" form-horizontal" name="affiliate" novalidate="novalidate">
                    <input type="hidden" value="{{route('admin.affiliate.insert')}}" id="affiliate_store">
                     <input type="hidden" name="user_id" value="{{$affiliate_user_id}}" />  

                    <input type="hidden" value="{{$id}}" name="id" id="id">
                    {{ csrf_field() }}

                    <h2 >
                        Add Affiliate
                    </h2>
                    <hr>
                   
                        <div class="form-group">
                            <label class="col-sm-3 control-label">
                                Name <span class="symbol required"></span>
                            </label>
                            <div class="col-sm-6">
                                <input placeholder="Enter Name" class="form-control" value="{{$name}}" name="name" type="text">
                            </div>
                        </div>
                    
                   
                        <div class="form-group">
                            <label class="col-sm-3 control-label">
                                Email <span class="symbol required"></span>
                            </label>
                            <div class="col-sm-6">
                                <input placeholder="Enter email" class="form-control" name="email" type="email" value="{{$email}}" @if($email != "") readonly @endif>
                            </div>
                        </div>
                   
                   
                        <div class="form-group">
                            <label class="col-sm-3 control-label">
                                Phone Number 
                            </label>
                            <div class="col-sm-6">
                                <input placeholder="Enter Phone Number" class="form-control allow_numeric_only" name="phone_number" type="text" value="{{$phone_number}}">
                            </div>
                        </div>
                    
                        @if($redemption != "")
                        <div class="form-group">
                            <label class="col-sm-3 control-label">
                               Referral    
                            </label>
                            <div class="col-sm-6">
                                <input class="form-control" name="redemption" type="text" disabled  value="{{$redemption}}">
                                <span class="mandatory-field" style="font-size:13px;">This will auto generate from name</span>
                            </div>
                        </div>
                        @endif

                        <div class="form-group">
                            <label class="col-sm-3 control-label">
                               commission 
                            </label>
                            <div class="col-sm-6">
                                <input class="form-control allow_numeric_with_decimal" name="commission" type="text" value="{{$commission}}">
                            </div>
                        </div>
                   
                   
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-offset-3">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                    <a href="{{URL(route('admin.affiliate'))}}" type="button" class="btn btn-danger">Cancel</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>		

</div>
@stop

@section('js_script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/additional-methods.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-jgrowl/1.4.7/jquery.jgrowl.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-jgrowl/1.4.7/jquery.jgrowl.css" />

<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<script>
$(document).ready(function () {
   
$("#add_affiliate").validate({    
        rules: {
            name: "required",
            email: "required", 
        },
    submitHandler: function (form) { 
        var formData = new FormData($(form)[0]); 
        $.ajax({
                type: $(form).attr('method'),
                url: $('#affiliate_store').val(),
                headers: {
                   '_token' : $("input[name='_token']").val()
                },
                dataType: 'json',
                data: formData,
                contentType: false,
                processData: false,
                cache: false,
                beforeSend: function () {
                    $(form).find('button[type="submit"]').html('Submit').attr('disabled');
                },
                success: function (response) {
                    $(form).find('button[type="submit"]').html('Submit').removeAttr('disabled');
                    if(response.status == 'success'){
                        toastr.success(response.message, 'Success');
                        setTimeout(function () {
                            location.reload();
                        }, 1000);
                    }else{
                         toastr.error(response.message, 'Error');
                    }
                    
                },
                error: function(response) {
                    toastr.error(response.message, 'Error');
                    $(form).find('button[type="submit"]').html('Submit').removeAttr('disabled');
                },
            });
        },
    });
});

</script>

@stop

