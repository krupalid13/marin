                                  
                 <div class="card">
                  <div class="card-body">
                    <h5> Total Registered Users using your Referral code: {{$totalRecords}}</h5>
                    <h5> Total Users with Minimun Profile Requirement Completed: <span id="totalProfileRequirement"></span></h5>

                  </div>
                </div>

                <div class="card" style="float:right;margin-bottom:10px;">
                   <button class="btn btn-primary downloadExcel" href="#" title="Download to Excel">Export to Excel</button>
                </div>
                <input type="hidden" name="userId" value="{{$userId}}" id="userId">
                  <div class="datatable-container">
                    <table class="table table-bordered table-hover" width="100%">
                        <thead>
                            <th>UserName</th>
                            <th>Registered On</th>
                            <th>Registered Rank</th>
                            <th>Rank For Applied</th>
                            <th>Passport</th>
                            <th>CDC</th>
                            <th>Course</th>
                            <th>Passport </th>
                            <th>CDC</th>
                        </thead>
                        <tbody>
                          <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td colspan="2" class="text-center">Document</td>
                            <td></td>
                            <td colspan="2" class="text-center">Upload</td>
                          </tr>
                        
                          @php 
                          $totalCompleteUser = 0;
                          @endphp
                       @if(!empty($obj))
                     
                        @foreach($obj as $key=>$value)
                         @php $userComplete = 1;@endphp
                        @foreach($value as $userData)
                            <tr>
                                <td>{{$userData['first_name']}}&nbsp;{{$userData['PPMname'] }}&nbsp;{{ $userData['PPLname'] }}</td>
                                <td> {{ \Carbon\Carbon::parse($userData['created_at'])->format('Y-m-d')}} </td>
                                <td>
                                   @if(isset($userData['professional_detail']['current_rank']) && !empty($userData['professional_detail']['current_rank']))
                                    @php $rank = '';
                                        $newRankData = \CommonHelper::new_rank();
                                        foreach($newRankData as $newRankCate){
                                            if($rank != ''){
                                                break;
                                            }
                                            foreach($newRankCate as $newRankKey => $newRank){
                                                if($newRankKey == $userData['professional_detail']['current_rank']){
                                                    $rank = $newRank;
                                                    break;
                                                }
                                            }
                                        }
                                    @endphp
                                    
                                   {{$rank}}
                                 
                                  @endif 
                                </td>
                                <td>
                                  @if(isset($userData['professional_detail']['applied_rank']) && !empty($userData['professional_detail']['applied_rank']))
                                    @php $rank = '';
                                        $newRankData = \CommonHelper::new_rank();
                                        foreach($newRankData as $newRankCate){
                                            if($rank != ''){
                                                break;
                                            }
                                            foreach($newRankCate as $newRankKey => $newRank){
                                                if($newRankKey == $userData['professional_detail']['applied_rank']){
                                                    $rank = $newRank;
                                                    break;
                                                }
                                            }
                                        }
                                    @endphp
                                    
                                   {{$rank}}
                                  
                                  @endif
                                </td>
                               <td> 
                                @if(isset($userData['passport_detail']) && !empty($userData['passport_detail']))
                                    <i class="fa fa-check check-icon-color"></i>
                                  @else 
                                  @php $userComplete =0; @endphp
                                    <i class="fa fa-close cancel-icon-color"></i> 
                                  @endif
                                </td>
                               <td>
                                @if(isset($userData['seaman_book_detail']) && !empty($userData['seaman_book_detail']))
                                    <i class="fa fa-check check-icon-color"></i>
                                  @else 
                                  @php $userComplete =0; @endphp
                                    <i class="fa fa-close cancel-icon-color"></i> 
                                  @endif

                                </td>
                               <td> 
                                @if(isset($userData['course_detail']) && !empty($userData['course_detail']))
                                     <i class="fa fa-check check-icon-color"></i>
                                @else
                                @php $userComplete =0; @endphp
                                      <i class="fa fa-close cancel-icon-color"></i>
                                @endif
                                </td>
                              
                                @if(isset($userData['user_documents_type_with_document']) && !empty($userData['user_documents_type_with_document']) )
                                  @php
                                    $upload_documents_details = [];  
                                    $upload_documents = $userData['user_documents_type_with_document']; 
                                  @endphp
                                  @foreach ($upload_documents as $key => $value)  
                                          @if($value['type'] == 'passport')
                                             @if(isset($value['user_documents']) && !empty($value['user_documents']))  
                                                @php $upload_documents_details['passport'] = $value['type']; @endphp
                                              @endif   
                                          @elseif($value['type'] == 'cdc')  
                                           @if(isset($value['user_documents']) && !empty($value['user_documents'])) 
                                               @php $upload_documents_details['cdc'] = $value['type'];  @endphp   
                                            @endif            
                                     
                                      @endif
                                  @endforeach
                                  @endif                                
                                  <td>  
                                    @if(!empty($upload_documents_details) && isset($upload_documents_details['passport']) &&  $upload_documents_details['passport'] == 'passport')
                                      <i class="fa fa-check check-icon-color"></i>
                                    @else
                                    @php $userComplete =0; @endphp
                                         <i class="fa fa-close cancel-icon-color"></i>
                                  @endif
                                  </td>
                                  <td> 
                                    @if(!empty($upload_documents_details) && isset($upload_documents_details['cdc']) && $upload_documents_details['cdc'] == 'cdc')
                                        <i class="fa fa-check check-icon-color"></i>
                                      @else
                                      @php $userComplete =0; @endphp
                                         <i class="fa fa-close cancel-icon-color"></i>
                                      @endif
                                    </td>
                            </tr>
                            @if($userComplete == 1)
                            @php
                            $totalCompleteUser++;
                            @endphp
                            @endif
                          @endforeach  
                        @endforeach
                      @else                    
                      <tr>
                        <td colspan="7" class="text-center"><p> No data found</p></td>
                      </tr>
                      @endif
                      <input type="hidden" id="totalComplete" value="{{$totalCompleteUser}}"> 
                    </tbody>
                    <tfoot></tfoot>
                    </table>
            </div>