                <html>
                      <tr>
                           <td><strong>UserName</strong></td>
                            <td><strong>Registered On</strong></td>
                            <td><strong>Registered Rank</strong></td>
                            <td><strong>Rank For Applied</strong></td>
                            <td><strong>Passport</strong></td>
                            <td><strong>CDC</strong></td>
                            <td><strong>Course</strong></td>
                            <td><strong>Passport</strong></td>
                            <td><strong>CDC</strong></td>
                        </tr>
                         <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td colspan="2" align="center"><strong>Document<strong></td>
                            <td></td>
                            <td colspan="2" align="center"><strong>Upload<strong></td>
                          </tr>
                        
                       @if(!empty($obj))
                     
                        @foreach($obj as $key=>$value)
                          
                        @foreach($value as $userData)
                            <tr>
                                <td>{{$userData['first_name']}}&nbsp;{{$userData['PPMname'] }}&nbsp;{{ $userData['PPLname'] }}</td>
                                <td> {{ \Carbon\Carbon::parse($userData['created_at'])->format('Y-m-d')}} </td>
                                <td>
                                   @if(isset($userData['professional_detail']['current_rank']) && !empty($userData['professional_detail']['current_rank']))
                                    @php $rank = '';
                                        $newRankData = \CommonHelper::new_rank();
                                        foreach($newRankData as $newRankCate){
                                            if($rank != ''){
                                                break;
                                            }
                                            foreach($newRankCate as $newRankKey => $newRank){
                                                if($newRankKey == $userData['professional_detail']['current_rank']){
                                                    $rank = $newRank;
                                                    break;
                                                }
                                            }
                                        }
                                    @endphp
                                    
                                   {{$rank}}
                                  @endif
                                </td>
                                <td>
                                  @if(isset($userData['professional_detail']['applied_rank']) && !empty($userData['professional_detail']['applied_rank']))
                                    @php $rank = '';
                                        $newRankData = \CommonHelper::new_rank();
                                        foreach($newRankData as $newRankCate){
                                            if($rank != ''){
                                                break;
                                            }
                                            foreach($newRankCate as $newRankKey => $newRank){
                                                if($newRankKey == $userData['professional_detail']['applied_rank']){
                                                    $rank = $newRank;
                                                    break;
                                                }
                                            }
                                        }
                                    @endphp
                                    
                                   {{$rank}}
                                  @endif
                                </td>
                               <td> 
                                @if(isset($userData['passport_detail']) && !empty($userData['passport_detail']))
                                  Yes
                                  @else 
                                   No
                                  @endif
                                </td>
                               <td>
                                @if(isset($userData['seaman_book_detail']) && !empty($userData['seaman_book_detail']))
                                    Yes
                                  @else 
                                    No
                                  @endif
                                </td>
                               <td> 
                                @if(isset($userData['course_detail']) && !empty($userData['course_detail']))
                                    Yes
                                @else
                                      No
                                @endif
                                </td>
                              
                                @if(isset($userData['user_documents_type_with_document']) && !empty($userData['user_documents_type_with_document']) )
                                  @php
                                    $upload_documents_details = [];  
                                    $upload_documents = $userData['user_documents_type_with_document']; 
                                  @endphp
                                  @foreach ($upload_documents as $key => $value)  
                                          @if($value['type'] == 'passport')
                                             @if(isset($value['user_documents']) && !empty($value['user_documents']))  
                                                @php $upload_documents_details['passport'] = $value['type']; @endphp
                                              @endif   
                                          @elseif($value['type'] == 'cdc')  
                                           @if(isset($value['user_documents']) && !empty($value['user_documents'])) 
                                               @php $upload_documents_details['cdc'] = $value['type'];  @endphp   
                                            @endif            
                                     
                                      @endif
                                  @endforeach
                                  @endif                                
                                  <td>  
                                    @if(!empty($upload_documents_details) && isset($upload_documents_details['passport']) &&  $upload_documents_details['passport'] == 'passport')
                                      Yes
                                    @else
                                         No
                                  @endif
                                  </td>
                                  <td> 
                                    @if(!empty($upload_documents_details) && isset($upload_documents_details['cdc']) && $upload_documents_details['cdc'] == 'cdc')
                                       Yes
                                      @else
                                      No
                                      @endif
                                    </td>
                            </tr>
                          @endforeach  
                        @endforeach
                      @else
                      <tr>
                        <td colspan="7" class="text-center"><p> No data found</p></td>
                      </tr>
                      @endif 
                    
                    </html>