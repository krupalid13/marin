@extends('admin.index')

@section('style_section')
<link rel="stylesheet" type="text/css" href="{{asset('public/assets/responsive/jquery.dataTables.min.css')}}">

<style>
    div#padding_to_add {
        padding: 25px;
    }
    .check-icon-color{ color: #337ab7;}
    .cancel-icon-color{ color: #d43f3a;}
</style>
@endsection

@section('content')
<div class="container">
    <div class="row-fluid">
        <div class="row" id="padding_to_add">
          
            <div class="col-12">
                <div class="pull-right">
                    <a class="btn btn-primary" href="{{ route('admin.affiliate.add') }}" title="Add Affiliate">Add Affiliate</a>
                </div>
            </div>
        </div>
        <br>
        <div class="datatable-container">

                 {{ Form::open(
                  array(
                  'id'                => 'table_form',
                  'name'              => 'table_form',
                  'class'             => 'ajaxFormSubmitDatatable',
                  'method'              => 'POST'
                  ))
                  }}

                    <table class="table table-bordered table-hover" id="dataTableBuilder" width="100%">
                        <thead>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Phone Number</th>
                            <th>Referral</th>
                            <th>Action</th>
                        </thead>
                        <tbody></tbody>
                        <tfoot></tfoot>
                    </table>
                </form>
            </div>
        </div>
    </div>

</div>
<div class="modal fade bd-example-modal-xl" id="affiliate_modal" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title text-center" id="exampleModalLabel">Affiliate</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body table-responsive">
      </div>
      <div class="modal-footer">
        
      </div>
    </div>
  </div>
</div>

@stop

@section('js_script')
<script src="{{asset('public/assets/responsive/jquery.dataTables.min.js')}}"></script>
<script>
    jQuery(document).ready(function() {
        FormWizard.init();
    });
</script>

<script src="{{asset('public/assets/responsive/sweetalert.min.js')}}"></script>

<script>
    function viewData(id){       
        $.ajax({
                type: "POST",
                url: "{{route('user.getRewardsUserData')}}",
                data: {userid : id},
                success: function (response) {
                    console.log(response);
                    if(response.status == 'success'){
                      $('#affiliate_modal').modal('show');
                      $('#affiliate_modal').find('div.modal-body').html(response.html);
                      $('#totalProfileRequirement').text($('#totalComplete').val());
                      $('.table').DataTable().draw(true);
                            swal(response.msg, {
                            icon: "success",
                        });

                    }else if(response.status == 'error'){
                        flashMessage('danger', response.msg);
                        swal(response.msg, {
                        icon: "warning",
                        });

                    }else{

                        flashMessage('danger', response.msg);
                        swal(response.msg, {
                        icon: "warning",
                        });
                    }
                },
                error: function (jqXhr) {
              }
            });
       
    }

    $(document).on("click", ".downloadExcel", function() {
      var  userid =  $('#userId').val();
      window.location = '{{ route("download.affiliate.registeredUsers") }}?affiliate_user_id='+userid;
    });
    
    // $(document).on("click",".delete_record", function(){

    //     swal({
    //       title: "Are you sure want to delete this record ?",
    //       icon: "warning",
    //       buttons: true,
    //       dangerMode: true,
    //     })
    //     .then((willDelete) => {
    //       if (willDelete) {
    //         var formAction = $(this).data("route");
    //         $.ajax({
    //             type: "DELETE",
    //             url: formAction,
    //             success: function (response) {
    //                 if(response.status == 'success'){
                      
    //                    $('.table').DataTable().draw(true);
    //                     swal(response.msg, {
    //                     icon: "success",
    //                     });

    //                 }else if(response.status == 'error'){
                        
    //                     flashMessage('danger', response.msg);
    //                     swal(response.msg, {
    //                     icon: "warning",
    //                     });

    //                 }else{

    //                     flashMessage('danger', response.msg);
    //                     swal(response.msg, {
    //                     icon: "warning",
    //                     });
    //                 }
    //             },
    //             error: function (jqXhr) {
    //           }
    //         });
            
    //       } 
    //     });
    // });

    $(document).on("click", ".copy_link", function(e) { 
        e.preventDefault();  
        var thisObj = $(this);
        var copyText = thisObj.attr('data-redemption');
        document.addEventListener('copy', function(e) {
          e.clipboardData.setData('text/plain', copyText);
          e.preventDefault();
        }, true);
      document.execCommand('copy');  
      window.location.href = "{{route('register')}}?referral="+copyText;
     
    });

   function RESET_FORM()
    {
        $('#search-frm').trigger('reset');
        table.draw();
        return false;
    }   
    $(document).on("click","button[type='submit']",function(){
          table.draw();
          return false;
    });
    var table = $('#dataTableBuilder').DataTable({
        bProcessing: true,
        bServerSide: true,
        processing: true,
        serverSide: true,
        searching: true,
        ajax: {
            url: "{{ route('admin.getAjaxAffiliateList') }}?datatable=yes",
        },
        columns: [
            {data: 'name' },
            {data: 'email' },
            {data: 'phone_number' },
            {data: 'redemption' },
            {data: 'action',searchable: false, orderable: false },
        ]
    });
</script>
@stop