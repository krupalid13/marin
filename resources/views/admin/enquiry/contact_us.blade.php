@extends('admin.index')

@section('content')

<div class="main-wrapper">
	<div class="main-container1">
				<!-- start: PAGE -->
				<div class="main-content">
					<div class="container">
						<!-- start: PAGE HEADER -->
						<!-- start: TOOLBAR -->
						<div class="toolbar row">
							<div class="col-sm-6 hidden-xs">
								<div class="page-header">
									<h1>Contact Us Enquiries <small>A list of all contact us enquiries</small></h1>
								</div>
							</div>
						</div>

						<!-- filter -->

						 <div class="row search-results-main-container m_t_25" >
							<div class="col-md-12">
								<div class="panel panel-white">
									<div class="panel-body">
										<?php 
											$data_array = $data->toArray();
											$pagination = $data;
										?>
										@if(isset($data_array['data']) && !empty($data_array['data']))
										
											<div class="panel-heading">
												<div class="row">
													<div class="col-xs-12">
														<div class="col-xs-6 paginator-content  pull-right no-margin">
															<ul id="top-user-paginator" class="pagination">
																{!! $data->render() !!}
															</ul>
														</div>
														
														<div class="pull-left search-count-content">
															Showing {{$data_array['from']}} - {{$data_array['to']}} of {{$data_array['total']}} Results
														</div>
													</div>
												</div>
											</div>
												
											@foreach($data_array['data'] as $data)	
											<div class="seafarer-listing-container">
												<div class="panel">
													<div class="panel-heading border-light partition-blue">
														<h5 class="panel-title text-bold header-link-text p-5-5">
															{{isset($data['name']) ? ucfirst($data['name']) : ''}}
														</h5>

													</div>
																	
													<div class="panel-body">
														<div class="row">
															<div class="col-lg-12 col-md-12 col-sm-12">
																<div class="row">
																	<div class="col-sm-6 sm-t-20">
																		<div class="row">
																			<div class="col-xs-12">
																				<div class="header-tag">
																					Basic Information
																				</div>
																			</div>
																		</div>
																		<div id="user-basic-details">
																			<div class="row">
																				<div class="col-xs-12 col-sm-12">
																					<dl>
																						<dt> Name : </dt>
																						<dd class="margin-bottom-5px">
																							{{ isset($data['name']) ? $data['name'] : ''}}
																						</dd>
																						<dt>Phone : </dt>
																						<dd class="margin-bottom-5px make-word-wrap-break">
																							{{ isset($data['phone']) ? $data['phone'] : ''}}
																			     		</dd>
																			     		<dt> Email : </dt>
																						<dd class="margin-bottom-5px">
																							{{isset($data['email']) ? $data['email'] : ''}}
																			     		</dd>
																			     		<dt> Message : </dt>
																						<dd class="margin-bottom-5px">
																							{{isset($data['message']) ? $data['message'] : ''}}
																			     		</dd>
																					</dl>
																				</div>
																			</div>
																		</div>
																	</div>
																	
																	<div class="col-sm-6">
																		
																		<div id="user-professional-details">
																			<div class="row">
																				<div class="col-xs-12">
																					<div class="header-tag">
																						Enquiry Information
																					</div>
																				</div>
																			</div>
																			<dl>
																				<dt> Enquiry Date :</dt>
																				<dd class="margin-bottom-5px">
																					{{ isset($data['created_at']) ? date('d-m-Y',strtotime($data['created_at'])) : ''}}
																				</dd>
																				<dt> Enquiry Time :</dt>
																				<dd class="margin-bottom-5px">
																					{{ isset($data['created_at']) ? date('h:i a',strtotime($data['created_at'])) : ''}}
																				</dd>
																			</dl>
																		</div>
																	</div>
																</div>
															</div>
																			
														</div>
													</div>
												</div>
																
											</div>
											@endforeach
											@if(isset($pagination) && !empty($pagination))
										
												<div class="panel-heading">
													<div class="row">
														<div class="col-xs-12">
															<div class="col-xs-6 paginator-content  pull-right no-margin">
																<ul id="top-user-paginator" class="pagination">
																	{!! $pagination->render() !!}
																</ul>
															</div>
															
															<div class="pull-left search-count-content">
																Showing {{$data_array['from']}} - {{$data_array['to']}} of {{$data_array['total']}} Results
															</div>
														</div>
													</div>
												</div>
											@endif
										@else
											<div class="no-results-found">No results found.</div>
										@endif

										
									</div>
								</div>
							</div>
						</div>
						<!-- end: PAGE CONTENT-->
					</div>
					<div class="subviews">
						<div class="subviews-container"></div>
					</div>
				</div>
				<!-- end: PAGE -->
			</div>
</div>

	<script type="text/javascript">
        
        var searchAjaxCall = "undefined";

        $(document).ready(function(){

            //init history plugin to perform search without page refresh
            // Establish Variables
            History = window.History; // Note: We are using a capital H instead of a lower h

            // Bind to State Change
            // Note: We are using statechange instead of popstate
			
            History.Adapter.bind(window,'statechange', function() {

                // Log the State
                var State = History.getState(); // Note: We are using History.getState() instead of event.state

                //start search (defined)
                fetchSearchResults(State.url, 
                                    function() {
                                        
                                    }, function() {

                                    });

            });

            

        });

        function fetchSearchResults(url, successCallback, errorCallback)
	    {
	    	window.location.href = url;
	    }
    </script>

@stop