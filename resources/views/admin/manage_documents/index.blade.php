@php
$user_id=Auth::id();
@endphp
@extends('layouts.main')
@section('title','Share Documents')
@section('content')
<style>
    .modal-footer-p{text-align: left;}

span.email-ids, span.email-ids-profile {
    float: left;
    
    border: 1px solid #000;
    margin-right: 5px;
    padding-left: 10px;
    padding-right: 10px;
    margin-bottom: 5px;
    padding-top: 3px;
    padding-bottom: 3px;
    border-radius: 5px;
    margin-top: 5px;
    color: #000;
}
span.cancel-email, span.cancel-email-profile {
    border: 1px solid #000;
    width: 18px;
    display: block;
    float: right;
    text-align: center;
    margin-left: 20px;
    border-radius: 49%;
    height: 18px;
    line-height: 15px;
    margin-top: 1px;
    cursor: pointer;
    color: #000;
}
.emailclass.email-id-row input, .emailclass.email-id-row-profile input {
    border: 0px;
    outline: 0px;
    height: 30px;
    width: 100%;
}
.emailclass.email-id-row, .emailclass.email-id-row-profile {
    border: 1px solid #ccc;
}
</style>
<script>
    var baseUrl = {!! json_encode(url('/')) !!}
</script>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="{{asset('public/css/sharestyle.css')}}">
<script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
<!--<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />-->
<script src="https://kit.fontawesome.com/84c7f42d8e.js"></script>
<div class="share-page-container">
<div class="page-container share-page-container">
    <div class="page title">
        <h1><span>Share </span>your details</h1>
    </div>

    <div class="main-column-container">
        <div class="main-column-container-left">
            <nav class="navbox">
                <ul>
                    <li><a href="{{route('share.history.resume')}}" class="Share ">Share History</a></li>
                    <li><a href="{{route("job.getUserJobHistoryList")}}" class="abtus-page">Applied History</a></li>
                    <li><a href="{{route('share.contacts')}}">My Contacts</a></li>
                </ul>
            </nav>
        </div>

        <div class="main-column-container-center">
            <div class="col-1">  
                
                <div class="pannel-default-cursor pointer">
                    <div class="pannel-image">
                        <a href="javascript:;" data-toggle="modal" data-target="#resume_model"><img src="{{asset('public/assets/images/default/share-resume.png')}}" alt=""></a>
                    </div>
                    <div class="pannel-title">
                         <a href="javascript:;" data-toggle="modal" data-target="#resume_model"><p>Share Resume</p></a>
                    </div>
                </div>
                <form onkeydown="return event.key != 'Enter';" id="resume_form">
                    <input type="hidden" name="_token" value="{{csrf_token()}}" />
                    <div id="resume_model" class="modal fade" role="dialog">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="form-group">
                                            <div class="col-md-2">
                                                <label for="email" class="">Share Resume To:</label>
                                            </div>
                                            <div class="col-md-10">
                                                 <div class="form-group" style="margin-bottom: 5px;">
                                                    <div class="col-sm-12 emailclass email-id-row paddingzero" style="padding: 0px 5px!important; background: #fff; margin-top: 5px;">
                                                        <div class="to-all-mail">
    
                                                        </div>
                                                        <input type="text" name="to[]" class="enter-to-mail-id" id="to" placeholder="For New Email : Type email id and press enter" / style="font-size: 12px;">
                                                    </div>
                                                    <div class="emails">
    
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default"
                                            data-dismiss="modal">Close
                                    </button>&nbsp;
                                    <button type="button" onclick="Submit(event, 'resume')" data-endpoint="{{route('share.resume')}}" class="btn btn-primary form-group sendbutton"  disabled="">Send</button>
                                    <p class="modal-footer-p">
                                    <br>
                                        <span class="text-danger">(Keep this marked when sending your resume to a company) </span><br>
                                        <input type="checkbox" id="is_allow_qr" checked="true" value="1"> Allow the resume viewer to give a quick response
                                    </p>
                                </div>
                            </div>
    
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-2">
                <div class="pannel-default-cursor pointer">
                    <div class="pannel-image">
                        <a href="{{route('share.documents')}}"><img src="{{asset('public/assets/images/default/share-doc.jpg')}}" alt=""></a>
                    </div>
                    <div class="pannel-title">
                        <a href="{{route('share.documents')}}">
                            <p>Share Document</p>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-3">
                <div class="pannel-default-cursor pointer">
                    <div class="pannel-image">
                        <a data-toggle="modal" data-target="#profile_model"><img class="image-profile" src="{{asset('public/assets/images/default/share-profile.png')}}" alt=""></a>
                    </div>
                    <div class="pannel-title">
                        <a data-toggle="modal" data-target="#profile_model">
                            <p>Share Profile</p> 
                        </a>
                    </div>
                    <div id="profile_model" class="modal fade" role="dialog">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="form-group">
                                            <div class="col-md-3">
                                                <label for="email" class="p-t-7">Share Profile To:</label>
                                            </div>
                                            <div class="col-md-9">
                                                {{-- <input class="form-control" name="email" id="email_profile" type="text" placeholder="Enter E-Mail ID" required> --}}
                                                <div class="col-sm-12 emailclass email-id-row paddingzero" style="padding: 0px 5px!important; background: #fff; margin-top: 5px;">
                                                    <div class="to-all-mail-profile"></div>
                                                    <input type="text" name="to-profile[]" class="enter-to-mail-id-profile" placeholder="For New Email : Type email id and press enter" / style="font-size: 12px;">
                                                </div>
                                                <div class="emails-profile"></div>
                                            </div>
                                            <!--<div class="col-md-1"><i class="fa fa-envelope" style="color: #337ab7; margin-top: 6px; font-size: 24px;" aria-hidden="true"></i></div>-->
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default cancel-email-profile" data-dismiss="modal">Close </button>
                                    <button type="button" onclick="Submitdata(event, 'profile')" data-endpoint="{{route('share.profile')}}" class="btn btn-primary sendbutton-profile" disabled>Send</button>
                                </div>
                            </div>
        
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>
</div>
</div>
<!--
<div class="container mt-30">
    <section class="row">
        <div class="col-sm-4">
            <a href="javascript:void(0);" data-toggle="modal" data-target="#resume_model">
                <div class="panel panel-default cursor-pointer">
                    <div class="panel-heading">
                        <img class="img-responsive center-block" width="120"
                             src="{{asset('public/assets/images/default/share-resume.png')}}" alt="Resume"/>
                    </div>
                    <div class="panel-body text-center">Share Resume</div>
                </div>
            </a>
            <form onkeydown="return event.key != 'Enter';" id="resume_form">
                <input type="hidden" name="_token" value="{{csrf_token()}}" />
                <div id="resume_model" class="modal fade" role="dialog">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-body">
                                <div class="row">
                                    <div class="form-group">
                                        <div class="col-md-2">
                                            <label for="email" class="">Share Resume To:</label>
                                        </div>
                                        <div class="col-md-9">
                                             <div class="form-group" style="margin-bottom: 5px;">
                                                <div class="col-sm-12 emailclass email-id-row paddingzero" style="padding: 0px 5px!important; background: #fff; margin-top: 5px;">
                                                    <div class="to-all-mail">

                                                    </div>
                                                    <input type="text" name="to[]" class="enter-to-mail-id" id="to" placeholder="For New Email : Type email id and press enter" / style="font-size: 12px;">
                                                </div>
                                                <div class="emails">

                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-1"><i class="fa fa-envelope" style="color: #337ab7; margin-top: 6px; font-size: 24px;" aria-hidden="true"></i></div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default"
                                        data-dismiss="modal">Close
                                </button>&nbsp;
                                <button type="button" onclick="Submit(event, 'resume')" data-endpoint="{{route('share.resume')}}" class="btn btn-primary form-group sendbutton"  disabled="">Send</button>
                                <p class="modal-footer-p">
                                <br>
                                    <span class="text-danger">(Keep this marked when sending your resume to a company) </span><br>
                                    <input type="checkbox" id="is_allow_qr" checked="true" value="1"> Allow the resume viewer to give a quick response
                                </p>
                            </div>
                        </div>

                    </div>
                </div>
            </form>
        </div>

        <div class="col-sm-4">
            <a data-toggle="modal" data-target="#profile_model" href="javascript:void(0);">
                <div class="panel panel-default cursor-pointer">
                    <div class="panel-heading">
                        <img class="img-responsive center-block" width="120"
                             src="{{asset('public/assets/images/default/share-profile.png')}}" alt="Profile"/>
                    </div>
                    <div class="panel-body text-center">Share Profile</div>
                </div>
            </a>
            <div id="profile_model" class="modal fade" role="dialog">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-body">
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-md-3">
                                        <label for="email" class="p-t-7">Share Profile To:</label>
                                    </div>
                                    <div class="col-md-8">
                                        {{-- <input class="form-control" name="email" id="email_profile" type="text" placeholder="Enter E-Mail ID" required> --}}
                                        <div class="col-sm-12 emailclass email-id-row paddingzero" style="padding: 0px 5px!important; background: #fff; margin-top: 5px;">
                                            <div class="to-all-mail-profile"></div>
                                            <input type="text" name="to-profile[]" class="enter-to-mail-id-profile" placeholder="For New Email : Type email id and press enter" / style="font-size: 12px;">
                                        </div>
                                        <div class="emails-profile"></div>
                                    </div>
                                    <div class="col-md-1"><i class="fa fa-envelope" style="color: #337ab7; margin-top: 6px; font-size: 24px;" aria-hidden="true"></i></div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default cancel-email-profile" data-dismiss="modal">Close </button>
                            <button type="button" onclick="Submitdata(event, 'profile')" data-endpoint="{{route('share.profile')}}" class="btn btn-primary sendbutton-profile" disabled>Send</button>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="col-sm-4">
            <a href="{{route('share.documents')}}">
                <div class="panel panel-default" href="">
                    <div class="panel-heading">
                        <img class="img-responsive center-block" width="133"
                             src="{{asset('public/assets/images/default/share-doc.jpg')}}" alt="Documents"/>
                    </div>
                    <div class="panel-body text-center">Share Documents</div>
                </div>
            </a>
        </div>
    </section>

    <section class="row text-center">
        <a href="{{route('share.history.resume')}}"><i class="fa fa fa-history" aria-hidden="true"></i> Share History</a>&nbsp;&nbsp;&nbsp;&nbsp;
        <a href="{{route('share.contacts')}}"><i class="fa fa-user-o" aria-hidden="true"></i> Contacts</a>        
    </section>

    <div><strong>Job Opening</strong></div>
    <br>
    <?php 

         $authDetail = \Auth::user();
         
         $sql = \App\SendedJob::with(['courseJob','courseJob.company','user','shareData'])
                        ->where('user_id',$authDetail->id)
                        ->where('applied',1)
                        ->get();

    ?>

    <div class="container">
        <div class="row-fluid">
            <div class="datatable-container">
                    <table class="table table-bordered table-hover" id="myTable" width="100%">
                        <thead>
                            <th>Company Applied To</th>
                            <th>Job Tile</th>
                            <th>Job Post Date</th>
                            <th>Applied Date</th>
                            <th>Resume Viewed</th>
                            <th>Resume Downloaded</th>
                            <th>Response</th>
                        </thead>
                        <tbody>
                            @if(isset($sql) && !$sql->isEmpty())
                                @foreach($sql as $key=>$data)
                                    <tr>
                                        <td>
                                            @if($data->courseJob->company !=null)
                                                {{$data->courseJob->company->name}}
                                            @else
                                                -
                                            @endif
                                        </td>
                                        <td>{{$data->courseJob->title}}</td>
                                        <td>{{date('d M y',strtotime($data->created_at))}}</td>
                                        <td>
                                            @if($data->applied_date !=null)
                                                {{date('d M y',strtotime($data->applied_date))}}
                                            @else
                                                -
                                            @endif

                                        </td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>
                                            @if(!empty($data->shareData->latest_qr))
                                                {{date('d M Y',strtotime($data->shareData->latest_qr))}}
                                            @else
                                            -
                                            @endif
                                        </td>
                                    </tr>    
                                @endforeach
                            @endif
                        </tbody>

                    </table>
                </div>
            </div>
        </div>
    </div>

    <section class="row">
        <div class="col-sm-offset-3 col-sm-6 text-center">
            <h4>Share Your Documents and Details</h4>
            <img class="img-responsive center-block" src="{{asset('public/assets/images/default/sharing_docs.png')}}" alt="Sharing Documents" />
        </div>
    </section>
</div>-->
@php $farray = array();  @endphp
@if(!empty($share_contact))
    @foreach($share_contact as $shareContact)

    <?php
    $cname = ''; 
    if($shareContact->name != '') {
        $cname = $shareContact->name.' : ';
    }
    $p_name = ''; 
    if($shareContact->p_name != '') {
        $p_name = $shareContact->p_name.' : ';
    }
        $farray[] = $cname.$p_name.'('.$shareContact->email.')'; 
    ?>
    @endforeach
@endif
<input type="hidden" class="farray" value="{{ implode(',',$farray) }}">
@endsection
@push('scripts')
  
   <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<script !src="">

    function isEmail(email) {
      var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
      return regex.test(email);
    }
    function checkvalue(){
        var to = $('span.to-emails').toArray();
        var a = [];
        for ( var i = 0; i < to.length; i++ ) {
            a.push( to[ i ].innerHTML );
        }
        if(a.length == 0){
            $('.sendbutton').attr('disabled','disabled')
        }else{
            $('.sendbutton').removeAttr('disabled')
        }
    }
    function checkvalueProfile(){
        var to = $('span.to-emails-profile').toArray();
        console.log(to);
        var a = [];
        for ( var i = 0; i < to.length; i++ ) {
            a.push( to[ i ].innerHTML );
        }
        if(a.length == 0){
            $('.sendbutton-profile').attr('disabled','disabled')
        }else{
            $('.sendbutton-profile').removeAttr('disabled')
        }
    }

    $(document).ready(function(){
        $('body').on('click','.modal-content:not(.enter-to-mail-id)',function(){
            $('.emails').html('');
        })
        var farray = $('.farray').val();
            var array=farray.split(',');
         $(".enter-to-mail-id").keydown(function (e) {
                var getValue = $(this).val();
                filtered =  array.filter(function (str) { return str.toLowerCase().includes(getValue.toLowerCase()); });
                var html = '';
                filtered.forEach(function(entry) {
                   html += `<span class="clicktoappend" style="width: 100%; font-size: 12px;    cursor: pointer;    float: left; background: #f9f9f9;    border: 1px solid #d4d4d4;    padding: 5px;">`+entry+`</span>`
                });
            $('.emails').html(html);
            if (e.keyCode == 13 || e.keyCode == 9 || e.keyCode == 188 ||  e.keyCode == 186 ||  e.keyCode == 32) {
                
                if(isEmail(getValue)){
                     $('.to-all-mail').append('<span class="email-ids" ><span class="to-emails style="font-size: 12px; ">'+ getValue +'</span> <span class="cancel-email">x</span></span>');
                     $(this).val('');
                     $('.enter-to-mail-id').attr('value','');

                }else{
                    toastr.error('Please enter valid email.', 'Oh No!');
                }
                checkvalue()
            }
        });
         $('body').on('click','.clicktoappend',function(){
            var val = $(this).text();
            var to = $('span.to-emails').toArray();
            var a = [];
            for ( var i = 0; i < to.length; i++ ) {
                a.push( to[ i ].innerHTML );
            }

            if(!a.includes(val)){
                $('.to-all-mail').append('<span class="email-ids" ><span class="to-emails" style="font-size: 12px;">'+ val +'</span> <span class="cancel-email">x</span></span>');
                $('.emails').html('');
                $('.enter-to-mail-id').val('');
            }
            checkvalue()

         })
        $(document).on('click','.cancel-email',function(){

            $(this).parent().remove();
            checkvalue()
        });
       
       $(".enter-to-mail-id").focus(function(e) {
            console.log('in');
        }).blur(function(e) {
            var getValue = $(this).val();
            if(getValue){
                if(isEmail(getValue)){
                    $('.to-all-mail').append('<span class="email-ids" ><span class="to-emails style="font-size: 12px; ">'+ getValue +'</span> <span class="cancel-email">x</span></span>');
                    $(this).val('');
                    $('.enter-to-mail-id').attr('value','');
                    checkvalue()
               }
            }
        });
        
        $(".enter-to-mail-id-profile").focus(function(e) {
            console.log('in');
        }).blur(function(e) {
            var getValue = $(this).val();
            if(getValue){
                if(isEmail(getValue)){
                    $('.to-all-mail-profile').append('<span class="email-ids-profile"><span class="to-emails-profile" style="font-size: 12px; ">'+ getValue +'</span> <span class="cancel-email-profile">x</span></span>');
                     $(this).val('');
                     $('.enter-to-mail-id-profile').attr('value','');
                    checkvalueProfile()
               }
            }
        });
        
        
    })
    $(document).ready(function(){
           $(".myselect").select2();
    })
    function Submitdata(e, share) {
        e.preventDefault();
        var to = $('span.to-emails-profile').toArray();
        var a = [];
        for ( var i = 0; i < to.length; i++ ) {
            a.push( to[ i ].innerHTML );
        }
        if (a.length == 0) {
            toastr.error('Please select contact');
            return false;
        } else {
            const endpoint = e.target.dataset.endpoint;
            const token = $('input[name="_token"]').val();
            let is_allow_qr = 0;
            if($('#is_allow_qr').prop("checked") == true){
                is_allow_qr = 1;
            }
            $('.sendbutton-profile').html('Sending <i class="fa fa-spin fa-spinner" aria-hidden="true"></i>');
            $.post(endpoint, { email: a, _token: token, is_allow_qr: is_allow_qr}, (res) => {

                if (res.status == 200)
                    toastr.success(res.msg);
                else
                    toastr.error(res.msg);

                e.target.innerText = 'Send';
                $('.to-all-mail-profile').html('');
                checkvalueProfile();
            });
        }
    }

    function Submit(e, share) {
        e.preventDefault();
        var to = $('span.to-emails').toArray();
        var a = [];
        for ( var i = 0; i < to.length; i++ ) {
            a.push( to[ i ].innerHTML );
        }
        if (a.length == 0) {
            toastr.error('Please select contact');
            return false;
        } else {
            const endpoint = e.target.dataset.endpoint;
            const token = $('input[name="_token"]').val();
            let is_allow_qr = 0;
            if($('#is_allow_qr').prop("checked") == true){
                is_allow_qr = 1;
            }
            $('.sendbutton').html('Sending <i class="fa fa-spin fa-spinner" aria-hidden="true"></i>');
            $.post(endpoint, {
                email: a,
                _token: token,
                is_allow_qr: is_allow_qr,
            }, (res) => {
                if (res.status == 200){
                    toastr.success(res.msg);
                    e.target.innerText = 'Send';
                    $('.to-all-mail').html('');
                    checkvalue();
                }
                else{
                    toastr.error(res.msg);
                    e.target.innerText = 'Send';
                }
                
        });
        }
    }

    $(document).ready( function () {
        $('#myTable').DataTable();
    } );

    $(document).ready(function(){
        $('body').on('click','.modal-content:not(.enter-to-mail-id-profile)',function(){
            $('.emails-profile').html('');
        })
        var farray1 = $('.farray').val();
            var array1=farray1.split(',');
         $(".enter-to-mail-id-profile").keydown(function (e) {
                var getValue = $(this).val();
                filtered =  array1.filter(function (str) { return str.toLowerCase().includes(getValue.toLowerCase()); });
                var html = '';
                filtered.forEach(function(entry) {
                   html += `<span class="clicktoappend-profile" style="width: 100%; font-size: 12px;cursor: pointer;float: left; background: #f9f9f9;border: 1px solid #d4d4d4;padding: 5px;">`+entry+`</span>`
                });
            $('.emails-profile').html(html);
            if (e.keyCode == 13 || e.keyCode == 9 || e.keyCode == 188 ||  e.keyCode == 186 ||  e.keyCode == 32) {
                
                if(isEmail(getValue)){
                     $('.to-all-mail-profile').append('<span class="email-ids-profile"><span class="to-emails-profile" style="font-size: 12px; ">'+ getValue +'</span> <span class="cancel-email-profile">x</span></span>');
                     $(this).val('');
                     $('.enter-to-mail-id-profile').attr('value','');

                }else{
                    toastr.error('Please enter valid email.', 'Oh No!');
                }
                checkvalueProfile()
            }
        });
         $('body').on('click','.clicktoappend-profile',function(){
            var val = $(this).text();
            var to = $('span.to-emails-profile').toArray();
            var a = [];
            for ( var i = 0; i < to.length; i++ ) {
                a.push( to[ i ].innerHTML );
            }

            if(!a.includes(val)){
                $('.to-all-mail-profile').append('<span class="email-ids-profile" ><span class="to-emails-profile" style="font-size: 12px;">'+ val +'</span> <span class="cancel-email-profile">x</span></span>');
                $('.emails-profile').html('');
                $('.enter-to-mail-id-profile').val('');
            }
            checkvalueProfile();
         })
        $(document).on('click','.cancel-email-profile',function(){
            $(this).parent().remove();
            checkvalueProfile();
        });  
    })
    </script>
@endpush
