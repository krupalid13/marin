@php
    $user_id=Auth::id();
@endphp
@extends('layouts.main')
@section('title','My Documents')
@section('content')
<link rel="stylesheet" href="{{URL::to('public/assets/dist/css/lightbox.min.css')}}">
<style>
.image-box {
    display: inline-grid;
    text-align: center;
    /* margin: 10px; */
    margin: 10px 0px 20px 0px;
}
span.img-name {
    width: 115px;
    font-size: 12px;
}
.image-preview img {
  width: 100%;
  height: 530px;
  /* object-fit: cover; */
}

.image-preview {
    background: #262626;
    padding: 5%;
}

.col-md-4 a {
  text-decoration: none;
  display: inline-block;
  padding: 7px 12px;
}

.col-md-4 a:hover {
  background-color: #ddd;
  color: black;
}

.previous {
  background-color: rgba(0, 0, 0, 0.77);
  color: white;
}

.next {
  background-color: rgba(0, 0, 0, 0.77);
  color: white;
}

.round {
  border-radius: 50%;
}
.buttons{
    float: right;
    top: 4px;
    position: absolute;
    right: 58px;
    top: 42px;
}
.img-tag{
    cursor: pointer;
}
.img-text{
    margin-top: 10px;
}
.example-image-link:hover{
    box-shadow:0 5px 10px rgba(0, 0, 0, 0.3);
}
</style>
    <div class="container m_h-100v">
        <h4 class="text-center h4 heading">My Documents</h4>
        <div class="row table-row">
            <div class="col-md-12">
                @if($user_contract['is_docs']==true)
                    @php
                        $i=0;
                        $firstImage = null;
                        $firstImageTitle = null;
                    @endphp
                    @foreach($user_contract as $index => $data)
                        @if($index!='is_docs')
                            @php
                                $imgTitle = null ;
                                if($index=="school_qualification"){
                                    $imgTitle = strtoupper('Qualification');
                                }elseif($index=="institute_degree"){
                                    $imgTitle = strtoupper('Pre-sea');
                                }
                                else{
                                    $imgTitle = strtoupper(str_replace('_',' ',$index));
                                }
                            @endphp
                            @foreach($data as $_docs_type_id => $_docs_data)
                                @php
                                    $storage_path = 'public/uploads/user_documents/';
                                @endphp
                                @if(!empty($_docs_data[0]['user_documents']))
                                    @foreach($_docs_data[0]['user_documents'] as $_user_docs)
                                        @php
                                            $userDocTypeData = App\UserDocumentsType::where('id',$_user_docs->user_document_id)->first();
                                            if($userDocTypeData && $userDocTypeData->status == 1){
                                                $_future_changes=['passport','photo','character_certificate','institute_degree','character_reference','us_visa','indos','ilo_medical','yellow_fever','school_qualification','aadhaar','pancard','passbook','wk_cop','gmdss'];
                                                $common_url= url($storage_path.$user_id);
                                                if (in_array($index,$_future_changes)) $url = "/"  . $index . "/" . $_user_docs['document_path'];
                                                else $url = "/" . $index . "/" . $_docs_type_id . "/" . $_user_docs['document_path'];
                                                $imgData[$i]['img'] = $common_url.$url;
                                                $imgData[$i]['title'] = $imgTitle;
                                                // $imgData[$i]['name'] = (isset($_docs_data['heading'])?$_docs_data['heading'] : str_replace('_',' ',$index));
                                                $imgData[$i]['name'] = (isset($_docs_data['heading'])?$_docs_data['heading'] : str_replace('_',' ',$index));
                                                if ($_docs_data[0]['type'] == 'dce') {
                                                    $imgData[$i]['name'] = ucwords(strtolower($_docs_data[0]['type_id']));
                                                    if ($_docs_data[0]['type_id'] == 'all') {
                                                        $imgData[$i]['name'] = 'Oil + Chemical + Liquefied Gas';
                                                    }
                                                    if ($_docs_data[0]['type_id'] == 'lequefied_gas') {
                                                        $imgData[$i]['name'] = 'Liquefied Gas';
                                                    }
                                                }
                                                if($i == 0){
                                                    $firstImage = $common_url.$url;
                                                    $firstImageTitle = $imgTitle .' - '. $imgData[$i]['name'];
                                                }
                                                $i++;
                                            }
                                        @endphp
                                    @endforeach
                                @endif
                            
                            @endforeach
                        @endif
                    @endforeach
                    <table>
                        <tbody>
                            <span class="total-count"  data-count="{{count($imgData)}}"></span>
                            @foreach($imgData as $key=>$item)
                                <tr>
                                    <div class="image-box">
                                        <span class='img-name'>{{$item['title']}}</span>
                                        <a class="example-image-link" href="{{$item['img']}}" data-lightbox="example-set" data-title="{{$item['title'].' - '.$item['name']}}">
                                            <img class="example-image" src="{{$item['img']}}"  width="100px" height="100px" alt=""/>
                                        </a>
                                        {{-- <img src="{{$item['img']}}" width="100px" height="100px" hspace="10" vspace="10" class="img-tag img-{{$key}}" data-id={{$key}} data-name={{$item['img']}} data-title="{{$item['title'].' - '.$item['name']}}"> --}}
                                        <span class='img-name'>{{ucwords(str_replace('_',' ',$item['name'])) }}</span>
                                    </div>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                @else
                    <div class="mt-30 panel panel-bricky">
                        <div class="panel-heading text-center">
                            <h4 class="h4">Kindly <a href="{{route('site.seafarer.edit.profile')}}">Upload</a> atleast one
                                Document to use This Feature.</h4>
                        </div>
                    </div>

                @endif
            </div>
            {{-- <div class="col-md-4">
                <div class="buttons">
                    <a href="#" class="previous round" data-id="0" data-type="p">&#8249;</a>
                    <a href="#" class="next round" data-id="0" data-type="n">&#8250;</a>
                </div>
                <div class="image-preview">
                    <img src="{{$firstImage}}" class="display-image">
                </div>
                <div class='img-text'>{{$firstImageTitle}}</div>
            </div> --}}
        </div>
        {{-- <div class="panel panel-footer text-center">
            <button class="btn btn-azure" type="button" onclick="select_all()">Select All</button>
            <button class="btn btn-warning" type="reset">Clear All</button>
            <button class="btn btn-default" type="submit"> Download Zip File</button>
        </div> --}}
        <div class="clearfix"></div>
    </div>
@stop
@push('scripts')
<script src="{{URL::to('public/assets/dist/js/lightbox-plus-jquery.min.js')}}"></script>
    <script type="text/javascript">
        function select_all() {
            $('input[name="downloadable_docs[]"]').prop('checked', true);
        }
        $(document).on('click','.img-tag',function(){
            var name = $(this).data('name');
            var title = $(this).data('title');
            var id = $(this).data('id');
            $('.display-image').attr("src", name);
            $('.img-text').text(title);
            $('.round').data('id',id);
        });
        $(document).on('click','.round',function(){
            var type = $(this).data('type');
            var id = $(this).data('id');
            var count = $('.total-count').data('count');
            if(type == 'p'){
                id = parseInt(id) - 1;
            }else{
                id = parseInt(id) + 1;
            }
            if(parseInt(id) >= parseInt(count)){
                id = parseInt(count) - 1;
            }
            var imgUrl = $('.img-'+id).data('name');
            var title = $('.img-'+id).data('title');
            $('.display-image').attr("src", imgUrl);
            $('.img-text').text(title);
            if(id <= 0){
                id = 0;
            }
            $('.round').data('id',id);
        });
    </script>
@endpush