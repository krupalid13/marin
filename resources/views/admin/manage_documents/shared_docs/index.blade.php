@extends('layouts.main')
@section('title','Download Documents')
@section('content')
    <div class="container m_h-100v">
        <h4 class="text-center h4 heading">Download Documents</h4>
        <div class="row">
            <form action="{{route('shared.documents.zip',$token)}}" method="POST">
                <br/>
                <div class="panel panel-footer text-center">
                    <button class="btn btn-azure" type="button" onclick="select_all()">Select All</button>
                    <button class="btn btn-warning" type="reset" onclick="unselect_all()">Clear All</button>
                    <button class="btn btn-default" type="submit" disabled> Download Zip File</button>
                </div>
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    @foreach($only_shared_docs as $index => $data)
                        @if($index!='is_docs')
                            @php
                            $vaccination = ['yellow_fever', 'cholera', 'hepatitis_b', 'hepatitis_c', 'diphtheria'];
                            @endphp
                            <div class="panel">
                                <h5 class="h5 m-b-5">
                                    @if($index=="school_qualification")
                                        {{strtoupper('Qualification')}}
                                    @elseif($index=="institute_degree")
                                        {{strtoupper('Pre-sea')}}
                                    @elseif($index=="covid")
                                        {{strtoupper('Vaccination - Covid19')}}
                                    @elseif($index=="screening_test")
                                        {{strtoupper('Medical Screening Test')}}
                                    @elseif(in_array($index, $vaccination))
                                        {{strtoupper(str_replace('_',' ',"Vaccination - " . $index))}}
                                    @else
                                        {{strtoupper(str_replace('_',' ',$index))}}
                                    @endif
                                </h5>
                                <hr class="border-grey m-t-0">
                                
                                <div class="panel-body custom_row">
                                    @foreach($data as $_docs_type_id => $_docs_data)
                                        @php
                                            $storage_path = 'public/uploads/user_documents/';
                                        @endphp
                                        @if(!empty($_docs_data[0]['user_documents']))
                                            @foreach($_docs_data[0]['user_documents'] as $_user_docs)
                                                @php
                                                    $_future_changes=['passport','photo','character_certificate','institute_degree','character_reference','us_visa','indos','sid','ilo_medical','yellow_fever','screening_test', 'cholera', 'hepatitis_b', 'hepatitis_c', 'diphtheria', 'covid','school_qualification','aadhaar','pancard','passbook','wk_cop','gmdss'];
                                                    $common_url=env("AWS_URL") . $storage_path . $user_id;

                                                    if (in_array($index,$_future_changes)) $url = "/"  . $index . "/" . $_user_docs['document_path'];
                                                    else $url = "/" . $index . "/" . $_docs_type_id . "/" . $_user_docs['document_path'];
                                                @endphp
                                                <div class="custom_col">
                                                    <div class="panel panel-default">
                                                        <div class="panel-body text-center px-4">
                                                            <label class="cursor-pointer" for="{{$url}}">
                                                                <div class="avatar-upload">
                                                                    <a class="lightbox"
                                                                       href="#preview-{{$_user_docs['id']*98}}">
                                                                        <div class="avatar-view">
                                                                            <label for="imageUpload"></label>
                                                                        </div>
                                                                    </a>
                                                                    <div class="avatar-preview">
                                                                    	
                                                                        <h6 class="font-size-12">{{(isset($_docs_data['heading'])?$_docs_data['heading'] : str_replace('_',' ',$index))}}
                                                                        @php
                                                                    		if ($data['id'] == 'dce') {
                                                                    			if ($_docs_type_id == 'all') {
                                                                    				echo ' / Oil + Chemical + Liquified Gas';
                                                                    			} else {
                                                                    				echo ' / ' . ucwords(strtolower(str_replace('_', ' ', $_docs_type_id)));
                                                                    			}
                                                                    		}
                                                                    	@endphp
                                                                    	</h6>
                                                                        <div class="avatar-edit-img"
                                                                             id="{{$_user_docs['id']*89}}">
                                                                            <img src="{{$common_url.$url}}"
                                                                                 class="img-thumbnail" width="200"
                                                                                 style="max-height: 135px">
                                                                        </div>
                                                                        <div class="lightbox-target"
                                                                             id="preview-{{$_user_docs['id']*98}}">
                                                                            <img src="{{$common_url.$url}}"/>
                                                                            <a class="lightbox-close"
                                                                               href="#{{$_user_docs['id']*89}}"></a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </label>
                                                            <input class="" type="checkbox"
                                                                   name="downloadable_docs[]"
                                                                   id="{{$url}}"
                                                                   value="{{$url}}"/>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                        @endif
                                    @endforeach
                                </div>
                            </div>
                        @endif
                    @endforeach
                    <div class="panel panel-footer text-center">
                        <button class="btn btn-azure" type="button" onclick="select_all()">Select All</button>
                        <button class="btn btn-warning" type="reset" onclick="unselect_all()">Clear All</button>
                        <button class="btn btn-default" type="submit" disabled=""> Download Zip File</button>
                    </div>
                </form>
        </div>
        <div class="clearfix"></div>
    </div>
@stop
@push('scripts')
    <script>
        function select_all() {
            $('input[name="downloadable_docs[]"]').prop('checked', true);
            $('input[name="downloadable_docs[]"]').change();
        }
        function unselect_all() {
            $('input[name="downloadable_docs[]"]').prop('checked', false);
            $('input[name="downloadable_docs[]"]').change();
        }
        $('input[name="downloadable_docs[]"]').on('change', function () {
            console.log($('input[name="downloadable_docs[]"]:checked').length);
            if($('input[name="downloadable_docs[]"]:checked').length > 0){
                $('.btn-default').prop('disabled', false);
            } else {
                $('.btn-default').prop('disabled', true);
            }
        })
    </script>
@endpush