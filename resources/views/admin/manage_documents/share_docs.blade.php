@php
    $user_id=Auth::id();
    $bank_flag=true;
    $vaccination=true;
@endphp
@extends('layouts.main')
@section('content')
<style>
    .modal-footer-p{text-align: left;}

        span.email-ids {
    float: left;
    
    border: 1px solid #000;
    margin-right: 5px;
    padding-left: 10px;
    padding-right: 10px;
    margin-bottom: 5px;
    padding-top: 3px;
    padding-bottom: 3px;
    border-radius: 5px;
    margin-top: 5px;
    color: #000;
}
span.cancel-email {
    border: 1px solid #000;
    width: 18px;
    display: block;
    float: right;
    text-align: center;
    margin-left: 20px;
    border-radius: 49%;
    height: 18px;
    line-height: 15px;
    margin-top: 1px;
    cursor: pointer;
    color: #000;
}
.emailclass.email-id-row input {
    border: 0px;
    outline: 0px;
    height: 30px;
    width: 100%;
}
.emailclass.email-id-row {
    border: 1px solid #ccc;
}
</style>
    <div class="container m_h-100v">
        <h4 class="text-center h4 heading">Share Documents</h4>
        <div class="row">
            <br/>
            <div class="panel panel-footer text-center mt-2">
                <button class="btn btn-azure" type="button" onclick="select_all()">Select All</button>
                <button class="btn btn-warning" type="button" onclick="reset_all()">Clear All</button>
                <button class="btn btn-success btn_share" type="button" data-toggle="modal" data-target="#share"
                        disabled="disabled">Share
                </button>
            </div>
            @if($user_contract['is_docs']==true)
                <hr/>
                <form id="shareDocs" onkeydown="return event.key != 'Enter';">
                    <h5 class="text-uppercase">Share your Documents</h5>
                    <input type="hidden" name="_token" value="{{csrf_token()}}" />
                    @foreach($user_contract as $index => $data)
                        @if($index!='is_docs')
                            <div class="">
                                <!--@if( $bank_flag == true && ($index=='aadhaar' || $index=='character_reference' || $index=='pancard' || $index=='passbook'))
                                    @php
                                        $bank_flag=false;
                                    @endphp
                                    <hr/>
                                    <h5 class="text-uppercase">Share your identification and banking documents</h5>
                                    @if($personal_detail['aadhaar'] || $bank_detail['bank_name'] != "" || $bank_detail['account_holder_name'] != "" || $bank_detail['account_no'] != "" || $bank_detail['ifsc_code'] != "" || $bank_detail['branch_address'] != "")
                                    <ul class="m-b-5">
                                        <li>
                                            <label class="cursor-pointer" style="font-weight: 300" for="bank_details">
                                                <input class="bank_details" type="checkbox"
                                                       name="shareable_docs_parent[]"
                                                       id="bank_details"
                                                       value="bank_details"/> BANKS DETAILS
                                            </label>
                                            <a href="#" data-toggle="modal" data-target="#myModal">view</a>

                                            <div id="myModal" class="modal fade" role="dialog">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-body">
                                                            @php
                                                                $user=App\User::whereId($user_id)->first();
                                                            @endphp
                                                            <div class="row">
                                                                <div class="col-sm-6">
                                                                    Name
                                                                    : {{$bank_detail['account_holder_name']}}
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    Bank Name : {{$bank_detail['bank_name']}}
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    Acount Number : {{$bank_detail['account_no']}}
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    IFSC Code : {{$bank_detail['ifsc_code']}}
                                                                </div>
                                                                <div class="col-md-6">
                                                                    Branch Address : {{$bank_detail['branch_address']}}
                                                                </div>
                                                            </div>
                                                            <hr/>
                                                            <div class="row">
                                                                <div class="col-sm-6">
                                                                    Pan Card : {{$personal_detail['pancard']}}
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    Aadhaar No. : {{$personal_detail['aadhaar']}}
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-default"
                                                                    data-dismiss="modal">Close
                                                            </button>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                    @endif
                                @endif-->

                                @if( $vaccination == true && ($index=='ilo_medical' || $index=='yellow_fever' || $index=='screening_test' || $index=='cholera' || $index=='hepatitis_b' || $index=='hepatitis_c' || $index=='diphtheria' || $index=='covid'))
                                    @php
                                        $vaccination=false;
                                    @endphp
                                    <hr/>
                                    <h5 class="text-uppercase">Share your Medical Details</h5>
                                @endif

                                <ul class="m-b-5">
                                    <input id="{{$index}}" type="checkbox" name="shareable_docs_parent[]" value="{{$index}}">
                                    <label class="cursor-pointer" style="font-weight: 300" for="{{$index}}">
                                        @if($index=="school_qualification")
                                            {{strtoupper('Qualification')}}
                                        @elseif($index=="sea_service_contract")
                                            {{strtoupper('Contracts')}}
                                        @elseif($index=="screening_test")
                                            {{strtoupper('Medical Screening Test')}}
                                        @elseif($index=="covid")
                                            {{strtoupper('Vaccination - Covid19')}}
                                        @elseif($index=="diphtheria")
                                            {{strtoupper('Vaccination - Diphtheria & Tetanus')}}
                                        @elseif($index=="yellow_fever" || $index=="cholera" || $index=="hepatitis_b" || $index=="hepatitis_c")
                                            {{strtoupper('Vaccination - ' . str_replace('_',' ',$index))}}
                                        @elseif($index=="sea_service_article")
                                            {{strtoupper('Articles')}}
                                        @elseif($index=="passbook")
                                            {{strtoupper('Passbook / CHQ')}}
                                        @elseif($index=="passport")
                                            {{strtoupper('Passport ' . $data[0]['heading'])}}
                                        @elseif($index=="gmdss")
                                            {{strtoupper('GMDSS ' . $data[0]['heading'])}}
                                        @elseif($index=="wk_cop")
                                            {{strtoupper($data[0]['heading'])}}
                                        @elseif($index=="institute_degree")
                                            {{strtoupper('Pre-sea')}}
                                        @else
                                            {{strtoupper(str_replace('_',' ',$index))}}
                                        @endif
                                    </label>
                                    @foreach($data as $_docs_type_id => $_docs_data)
                                        @if($_docs_type_id!='id' && count($_docs_data[0]['user_documents'])>0)
                                            <li class="pl-25">
                                                <label class="cursor-pointer" style="font-weight: 300" for="{{$_docs_type_id}}">
                                                    <input class="{{$index}}" type="checkbox" name="shareable_docs_children[{{$index}}][]" id="{{$_docs_type_id}}" value="{{$_docs_type_id}}"/>

                                                    @if($index=="school_qualification")
                                                        {{strtoupper('Qualification')}}
                                                    @elseif($index=="institute_degree")
                                                        {{strtoupper('Pre-sea')}}
                                                    @else
                                                        {{-- {{ ($_docs_type_id == 'all') ?  'Oil + Chemical + Liquefied Gas' : ($_docs_type_id == 'lequefied_gas' ? 'Liquefied Gas' : strtoupper(str_replace('_',' ',$_docs_type_id))) }} --}}
                                                        @if($_docs_type_id == 'all')
                                                            {{strtoupper('Oil + Chemical + Liquefied Gas')}}
                                                        @elseif($_docs_type_id == 'lequefied_gas')
                                                            {{strtoupper('Liquefied Gas')}}
                                                        @elseif($_docs_type_id == 'lequefied_gas')
                                                            {{strtoupper('Liquefied Gas')}}
                                                        @elseif($index == 'courses')
                                                            {{($_docs_data['heading']) ?? ''}}
                                                        @elseif($index == 'sea_service')
                                                            {{($_docs_data['heading']) ?? ''}}
                                                        @elseif($index == 'cdc' || $index == 'sea_service_contract' || $index == 'sea_service_article' || $index == 'coc' || $index == 'coe')
                                                        @else
                                                            {{strtoupper(str_replace('_',' ',$_docs_type_id))}}
                                                        @endif
                                                    @endif

                                                    @if (
                                                        isset($_docs_data['heading']) && 
                                                        $index!="courses" && 
                                                        $index != 'sea_service' && 
                                                        $index != 'sea_service_article' && 
                                                        $index != 'sea_service_contract' && 
                                                        $index!="cdc" && 
                                                        $index!="coc" && 
                                                        $index!="coe")

                                                        {{ ' : ' . $_docs_data['heading'] }}
                                                    @elseif(isset($_docs_data['heading']) && $index=="courses" || $index == 'sea_service')
                                                    @elseif(isset($_docs_data['heading']) && ($index=="cdc" || $index == 'sea_service_contract' || $index == 'sea_service_article' || $index == 'coc' || $index == 'coe'))
                                                        {{($_docs_data['heading']) ?? ''}}
                                                    @else
                                                        @if ($index != 'dce' || $index != 'cdc')
                                                            {{ str_replace('_',' ',$index) }}
                                                        @endif
                                                    @endif
                                                </label>
                                            </li>
                                        @endif
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    @endforeach
                    @if($is_document_uploaded || $personal_detail['aadhaar'] != "" || $personal_detail['pancard'] != "" || $bank_detail['bank_name'] != "" || $bank_detail['account_holder_name'] != "" || $bank_detail['account_no'] != "" || $bank_detail['ifsc_code'] != "" || $bank_detail['branch_address'] != "")
                    <h5 class="text-uppercase">Share your identification and banking documents</h5>
                        @if($personal_detail['aadhaar'] != "" || $personal_detail['pancard'] != "" || $bank_detail['bank_name'] != "" || $bank_detail['account_holder_name'] != "" || $bank_detail['account_no'] != "" || $bank_detail['ifsc_code'] != "" || $bank_detail['branch_address'] != "")
                        <ul class="m-b-5">
                            <li>
                                <label class="cursor-pointer" style="font-weight: 300" for="bank_details">
                                    <input class="bank_details" type="checkbox"
                                        name="shareable_docs_parent[]"
                                        id="bank_details"
                                        value="bank_details"/> BANKS DETAILS
                                </label>
                                <a href="#" data-toggle="modal" data-target="#myModal">view</a>

                                <div id="myModal" class="modal fade" role="dialog">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-body">
                                                @php
                                                    $user=App\User::whereId($user_id)->first();
                                                @endphp
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        Name
                                                        : {{$bank_detail['account_holder_name']}}
                                                    </div>
                                                    <div class="col-sm-6">
                                                        Bank Name : {{$bank_detail['bank_name']}}
                                                    </div>
                                                    <div class="col-sm-6">
                                                        Acount Number : {{$bank_detail['account_no']}}
                                                    </div>
                                                    <div class="col-sm-6">
                                                        IFSC Code : {{$bank_detail['ifsc_code']}}
                                                    </div>
                                                    <div class="col-md-6">
                                                        Branch Address : {{$bank_detail['branch_address']}}
                                                    </div>
                                                </div>
                                                <hr/>
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        Pan Card : {{$personal_detail['pancard']}}
                                                    </div>
                                                    <div class="col-sm-6">
                                                        Aadhaar No. : {{$personal_detail['aadhaar']}}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default"
                                                        data-dismiss="modal">Close
                                                </button>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </li>
                        </ul>
                        @endif
                        @foreach($bank_identity as $index => $data)
                            <div class="">
                                <ul class="m-b-5">
                                    <input id="{{$index}}" type="checkbox" name="shareable_docs_parent[]" value="{{$index}}">
                                    <label class="cursor-pointer" style="font-weight: 300" for="{{$index}}">
                                        @if($index=="passbook")
                                            {{strtoupper('Passbook / CHQ')}}
                                        @else
                                            {{strtoupper(str_replace('_',' ',$index))}}
                                        @endif
                                    </label>
                                </ul>
                            </div>
                        @endforeach
                    @endif
                    <div class="panel panel-footer text-center">
                        <button class="btn btn-azure" type="button" onclick="select_all()">Select All</button>
                        <button class="btn btn-warning" type="button" onclick="reset_all()">Clear All</button>
                        <button class="btn btn-success btn_share" type="button" data-toggle="modal" data-target="#share"
                                disabled="disabled">Share
                        </button>

                        <div id="share" class="modal fade" role="dialog">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="form-group">
                                                <div class="col-md-2 text-left">
                                                    <label for="email" class="p-t-7">Share Document To:</label>
                                                </div>
                                                <div class="col-md-10">
                                                    <div class="form-group" style="margin-bottom: 5px;">
                                                        <div class="col-sm-12 emailclass email-id-row paddingzero" style="padding: 0px 5px!important; background: #fff; margin-top: 5px;">
                                                            <div class="to-all-mail"></div>
                                                            <input class="enter-to-mail-id" name="email[]" id="email" type="text" placeholder="For New Email : Type email id and press enter" style="font-size: 12px;" required>
                                                        </div>
                                                    </div>
                                                    <div class="emails text-left"></div>
                                                </div>
                                                <!--<div class="col-md-1"><i class="fa fa-envelope" style="color: #337ab7; margin-top: 6px; font-size: 24px;" aria-hidden="true"></i></div>-->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default"
                                                data-dismiss="modal">Close
                                        </button>
                                        <button type="button" onclick="Submit(event,)" data-endpoint="{{route('share.documents.post')}}"  disabled="" class="btn btn-primary sendbutton">Send</button>
                                        <p class="modal-footer-p text-left">
                                            <br>
                                            <span class="text-danger">This Document link will be valid for 48hrs </span><br>
                                        </p>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </form>
            @else
                <div class="mt-30 panel panel-bricky">
                    <div class="panel-heading text-center">
                        <h4 class="h4">Kindly <a href="{{route('site.seafarer.edit.profile')}}">Upload</a> atleast one
                            Document to use This Feature.</h4>
                    </div>
                </div>

            @endif
        </div>
        <div class="clearfix"></div>
    </div>
@stop
@php $farray = array(); @endphp
@if(!empty($share_contact))
    @foreach($share_contact as $shareContact)

    <?php
    $cname = ''; 
    if($shareContact->name != '') {
        $cname = $shareContact->name.' : ';
    }
    $p_name = ''; 
    if($shareContact->p_name != '') {
        $p_name = $shareContact->p_name.' : ';
    }
        $farray[] = $cname.$p_name.'('.$shareContact->email.')'; 
    ?>
    @endforeach
@endif
<input type="hidden" class="farray" value="{{ implode(',',$farray) }}">
@push('scripts')
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
    <script>
            function isEmail(email) {
      var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
      return regex.test(email);
    }
        function checkvalue(){
            var to = $('span.to-emails').toArray();
            var a = [];
            for ( var i = 0; i < to.length; i++ ) {
                a.push( to[ i ].innerHTML );
            }
            if(a.length == 0){
                $('.sendbutton').attr('disabled','disabled')
            }else{
                $('.sendbutton').removeAttr('disabled')
            }

        }
        const _checkboxs = () => {
            if ($('input:checked').length > 0) $('.btn_share').removeAttr('disabled');
            else $('.btn_share').attr('disabled', true);
        }

        function select_all() {
            $('input[type="checkbox"]').prop('checked', true);
            _checkboxs();
        }

        function reset_all() {
            $('form').trigger('reset');
            _checkboxs();
        }

        $(document).ready(() => {
            $('ul input').on('change', (e) => {
                var r = (e.target.value);
                if ($(`#${r}`)[0].checked) {
                    $(`.${r}`).prop('checked', true);
                } else $(`.${r}`).prop('checked', false);
                _checkboxs();
            });

        });
        $(document).ready(function(){
        $('body').on('click','.modal-content:not(.enter-to-mail-id)',function(){
            $('.emails').html('');
        })
        var farray = $('.farray').val();
            var array=farray.split(',');
         $(".enter-to-mail-id").keydown(function (e) {
                var getValue = $(this).val();
                filtered =  array.filter(function (str) { return str.toLowerCase().includes(getValue.toLowerCase()); });
                var html = '';
                filtered.forEach(function(entry) {
                   html += `<span class="clicktoappend" style="width: 100%; font-size: 12px;    cursor: pointer;    float: left;
    background: #f9f9f9;    border: 1px solid #d4d4d4;    padding: 5px;">`+entry+`</span>`
                });
            $('.emails').html(html);
            if (e.keyCode == 13 || e.keyCode == 9 || e.keyCode == 188 ||  e.keyCode == 186 ||  e.keyCode == 32) {
                
                
                if(isEmail(getValue)){
                    console.log('csdcdcscsdcscds');
                     $('.to-all-mail').append('<span class="email-ids" ><span class="to-emails style="font-size: 12px; ">'+ getValue +'</span> <span class="cancel-email">x</span></span>');
                     $(this).val('');
                     $('.enter-to-mail-id').attr('value','');

                }else{
                    toastr.error('Please enter valid email.', 'Oh No!');
                }
                checkvalue()
            }
        });
         $('body').on('click','.clicktoappend',function(){
            var val = $(this).text();
            var to = $('span.to-emails').toArray();
            var a = [];
            for ( var i = 0; i < to.length; i++ ) {
                a.push( to[ i ].innerHTML );
            }

            if(!a.includes(val)){
                $('.to-all-mail').append('<span class="email-ids" ><span class="to-emails" style="font-size: 12px;">'+ val +'</span> <span class="cancel-email">x</span></span>');
                $('.emails').html('');
                $('.enter-to-mail-id').val('');
            }
            checkvalue()

         })
         
         
         
        $(document).on('click','.cancel-email',function(){

            $(this).parent().remove();
            checkvalue()
        });
    })
    $(document).ready(function(){
           $(".myselect").select2();
           $(".enter-to-mail-id").focus(function(e) {
            console.log('in');
        }).blur(function(e) {
            var getValue = $(this).val();
            if(getValue){
                if(isEmail(getValue)){
                    $('.to-all-mail').append('<span class="email-ids" ><span class="to-emails style="font-size: 12px; ">'+ getValue +'</span> <span class="cancel-email">x</span></span>');
                    $(this).val('');
                    $('.enter-to-mail-id').attr('value','');
                    checkvalue()
               }
            }
        });
    })
            function Submit(e) {
                e.preventDefault();
                var to = $('span.to-emails').toArray();
                var a = [];
                for ( var i = 0; i < to.length; i++ ) {
                    a.push( to[ i ].innerHTML );
                }
                if (a.length == 0) {
                    toastr.error('Please select atleast one contact');
                    return false;
                } else {
                    const endpoint = e.target.dataset.endpoint;
                    const token = $('input[name="_token"]').val();
                    $('.sendbutton').html('Sending <i class="fa fa-spin fa-spinner" aria-hidden="true"></i>');

                    $.post(endpoint,$('#shareDocs').serialize()+'&_token='+token+'&to_emails='+a.join("###"), (res) => {
                        if (res.status == 200) toastr.success(res.msg);
                        else toastr.error(res.msg);
                        e.target.innerText = 'Send';
                        $('.to-all-mail').html('');
                        checkvalue();
                        // setTimeout(() => {
                        //     location.reload(true);
                        //     {{--location.href = {{route('share.index')}}--}}
                        // }, 3000)
                    });
                }
            }
    </script>

@endpush