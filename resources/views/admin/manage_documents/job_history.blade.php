@php
$user_id=Auth::id();
@endphp
@extends('layouts.main')
@section('content')
<style>
    .modal-footer-p{text-align: left;}

</style>
<script>
    var baseUrl = {!! json_encode(url('/')) !!}
</script>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.9/css/responsive.bootstrap.min.css">
<link rel="stylesheet" href="{{asset('public/css/sharestyle.css')}}">
<script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/responsive/2.2.9/js/dataTables.responsive.min.js"></script>
<!--<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />-->


<div class="container mt-30">


    <div class="text-center"><h4>Job Application History</h4></div>
    <br>
    <?php 

         $authDetail = \Auth::user();
         
         $sql = \App\SendedJob::with(['courseJob','courseJob.company','user','shareData'])
                        ->where('user_id',$authDetail->id)
                        ->where('applied',1)
                        ->get();

    ?>

    <div class="container">
        <div class="row-fluid">
            <div class="datatable-container">
                    <table class="table table-bordered table-hover" id="myTable" width="100%">
                        <thead>
                            <th>Applied Company</th>
                            <th>Applied Date</th>
                            <th>Response</th>
                            <th>Job Title</th>
                        </thead>
                        <tbody>
                            @if(isset($sql) && !$sql->isEmpty())
                                @foreach($sql as $key=>$data)
                                    <tr>
                                        <td>
                                            @if($data->courseJob->company !=null)
                                                {{$data->courseJob->company->name}}
                                            @else
                                                -
                                            @endif
                                        </td>
                                        <td>
                                            @if($data->applied_date !=null)
                                                {{date('d M y',strtotime($data->applied_date))}}
                                            @else
                                                -
                                            @endif

                                        </td>
                                        <td>
                                            @if(!empty($data->shareData->latest_qr))
                                                {{date('d M Y',strtotime($data->shareData->latest_qr))}}
                                            @else
                                            -
                                            @endif
                                        </td>
                                        <td>{{$data->courseJob->title}}</td>
                                    </tr>    
                                @endforeach
                            @endif
                        </tbody>

                    </table>
                </div>
            </div>
        </div>
    </div>

<!--    <section class="row">
        <div class="col-sm-offset-3 col-sm-6 text-center">
            <h4>Share Your Documents and Details</h4>
            <img class="img-responsive center-block" src="{{asset('public/assets/images/default/sharing_docs.png')}}" alt="Sharing Documents" />
        </div>
    </section>-->
</div>

@endsection
@push('scripts')
  
   <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<script !src="">

    $(document).ready( function () {
//        $('#myTable').DataTable();
        $('#myTable').DataTable( {
            responsive: true,
            "language": {
                "emptyTable": "No jobs applied"//Change your default empty table message
              }
        } );
    } );

    </script>
@endpush
