@php
$user_id=Auth::id();
@endphp
@extends('layouts.main')
@section('title','Share Contact')
@section('content')
<style>
    .contact-label {
        padding: 6px 12px;
    }
    .modal-title{     display: contents; }
</style>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.9/css/responsive.bootstrap.min.css">

<div class="container mt-30">
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Create Contact</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="create-contact-form" class="smart-wizard form-horizontal" action="" method="post">
                        {{ csrf_field() }}
                        <input type="hidden" name="share_contact_id" class="share_contact_id">
                        <div class="form-group row">
                            <label class="col-sm-1 col-form-label contact-label text-center">
                                <i class="fa fa-envelope-o fa-lg"></i>
                            </label>
                            <div class="col-sm-11">
                                <input type="email" class="form-control contact-input email" name="email" placeholder="Email" required="required">
                            </div>
                        </div>
                        <div class="form-group row text-center">
                            <label class="radio-inline">
                                <input type="radio" name="group" id="group_0" checked value="0">Company
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="group" id="group_1" value="1">Person 
                            </label>
                        </div>
                        <hr/>
                        <div class="form-group row company-name">
                            <label class="col-sm-1 col-form-label contact-label text-center">
                                <i class="fa fa-building-o fa-lg"></i>
                            </label>
                            <div class="col-sm-11">
                                <input type="text" class="form-control contact-input name" name="name" id="c_name" placeholder="Company Name" required="required">
                            </div>
                        </div>
                        <div class="form-group row more-field collapse">
                            <label class="col-sm-1 col-form-label contact-label text-center">
                                <i class="fa fa-user-o fa-lg"></i>
                            </label>
                            <div class="col-sm-3">
                                <select class="form-control title" name="title">
                                    <option value=""> Rank / Title </option>
                                    <option value="Mr."> Mr. </option>
                                    <option value="Miss."> Miss. </option>
                                    <option value="Mrs."> Mrs. </option>
                                    <option value="Ms."> Ms. </option>
                                    <option value="Capt."> Capt. </option>
                                    <option value="Ch.Eng."> Ch.Eng. </option>
                                    <option value="Dr."> Dr. </option>
                                </select>
                            </div>
                            <div class="col-sm-5">
                                <input type="text" class="form-control contact-input p_name" name="p_name" id="p_name" placeholder="Name of the user" required="required">
                            </div>
                            <div class="col-sm-3">
                                <input type="text" class="form-control contact-input job_title" name="job_title" placeholder="Job title">
                            </div>
                        </div>
                        <div class="form-group row more-field collapse">
                            <label class="col-sm-1 col-form-label contact-label text-center">
                                <i class="fa fa-phone fa-lg"></i>
                            </label>
                            <div class="col-sm-3">
                                <input type="number" class="form-control contact-input mobile_code" name="mobile_code" placeholder="Code">
                            </div>
                            <div class="col-sm-5">
                                <input type="number" class="form-control contact-input mobile_no" name="mobile_no" placeholder="Phone">
                            </div>
                            <div class="col-sm-3">
                                <select class="form-control type" name="type">
                                    <option value="1"> Work </option>
                                    <option value="2"> Personal </option>
                                </select>
                            </div>
                        </div>
                        <!--<button type="submit">submit</button>-->
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary pull-left more-field-btn"
                            >Add More Field
                    </button>
                    <button type="button" class="btn btn-default"
                            data-dismiss="modal">Cancel
                    </button>
                    <button type="button" data-form-id="create-contact-form" class="btn btn-dark-grey reset-btn-right ladda-button reset-contact-button" data-style="zoom-in">Reset
                    </button>
                    <button type="button" data-id="" data-url="{{URL::to('seafarer/save-share-coontact')}}" class="btn btn-primary save-contact"
                            >Save
                    </button>
                </div>
            </div>

        </div>
    </div>
    <section class="row form-group">
        <div class="col-md-4"></div>
        <div class="col-md-4 text-center">
            <b>My Contacts</b>
        </div>
        <div class="col-md-4">
            <button type="button" id="creat-contact" class="btn btn-primary pull-right"> Create Contact</button>
        </div>
        <div class="col-md-12">
            <hr>
        </div>
    </section>
    <section class="row">
        <table id="share-contact-table" class="display">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Group</th>
                    <th>Person Name</th>
                    <th>Email</th>
                    <th>Mobile</th>
                    <th>Action</th>
                </tr>
            </thead>
        </table>
    </section>
</div>
<input type="hidden" value="{{URL::to('seafarer/get-share-coontact')}}"  id="data-table-url">
<input type="hidden" value="{{URL::to('seafarer/delete-share-coontact')}}"  id="delete-contact-url">
@endsection
@push('scripts')
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/responsive/2.2.9/js/dataTables.responsive.min.js"></script>
<script>
let app = {
    dataTable: '',
    data: {}
}
$(document).ready(function () {
//    $('#myTable').DataTable();
    let dataTableUrl = $('#data-table-url').val();
    app.dataTable = $('#share-contact-table').DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        language: {
            "emptyTable": "No Contact Available"
        },
        ajax: {
            url: dataTableUrl,
            dataSrc: function (json) {
                $(json.data).each(function (key, value) {
                    app.data[value.id] = value;
                });
                return json.data
            }
        },
        columns: [
            {data: 'name', name: 'name'},
            {data: 'group', name: 'group'},
            {data: 'p_name', name: 'p_name'},
            {data: 'email', name: 'email'},
            {data: 'mobile_no', name: 'mobile_no'},
            {data: 'action', name: 'action'}
        ]
    });
})
        .on('click', '#creat-contact', function () {
            document.getElementById("create-contact-form").reset();
            $('.company-name').removeClass('collapse');
            $('#myModal').modal('show');
        })
        .on('click', '.more-field-btn', function () {
            $(".more-field").toggleClass("collapse");
            $(this).text(function (i, text) {
                return text === "Show Less Field" ? "Add More Field" : "Show Less Field";
            })
        })
        .on("click", ".save-contact", function (e) {
            let form = $('#create-contact-form');
//            var validator = $("#create-contact-form").validate();
//            validator.resetForm();
//            var validator = $( "#create-contact-form" ).validate();
//            form.destroy();
            if ($("input[name=group]:checked").val() == 1) {
                form.validate({
                    rules: {
                        p_name: "required",
                        c_name: {
                            required: false
                        }
                    }
                });
                $(".more-field").removeClass("collapse");
                $(".more-field-btn").text("Show Less Field");
//                $("#p_name").rules("remove", "required");
            } else {
                form.validate({
                    rules: {
                        c_name: "required",
                        p_name: {
                            required: false
                        }
                    }
                });
//                $("#c_name").rules("remove", "required");
            }
            if (!form.validate()) {
                return false;
            }
//                return false;

            let button = $(this);
            var data = $("#create-contact-form").serialize();

            if (form.valid()) {
                var l = Ladda.create(this);
                l.start();
                let url = $(this).attr('data-url');
                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    },
                    statusCode: {
                        200: function (response) {
                            toastr.success(response.message, 'Success');
                            $('#myModal').modal('hide');
                            app.dataTable.ajax.reload();

                            l.stop();
                        },
                        400: function (response) {
                            toastr.error(response.responseJSON.message, 'Error');
                            l.stop();
                        }
                    }
                });
            }
        })
        .on("submit", "#create-contact-form", function (e) {
            e.preventDefault();
            return false;
        })
        .on("click", ".edit-share-contact", function (e) {
            document.getElementById("create-contact-form").reset();
            $('.contact-input').find('.error').hide();
            $('.contact-input.error').removeClass('error');
            let id = $(this).attr('data-id');
            let data = app.data[id];
            $(".more-field").removeClass("collapse");
            $(".more-field-btn").text("Show Less Field");
            $('#create-contact-form')
                    .find('.share_contact_id')
                    .val(data.id)
                    .end()
                    .find('.email')
                    .val(data.email)
                    .end()
                    .find('.name')
                    .val(data.name)
                    .end()
                    .find('.title')
                    .val(data.title)
                    .end()
                    .find('.p_name')
                    .val(data.p_name)
                    .end()
                    .find('.job_title')
                    .val(data.job_title)
                    .end()
                    .find('.mobile_code')
                    .val(data.mobile_code)
                    .end()
                    .find('.mobile_no')
                    .val(data.mobile_no)
                    .end()
                    .find('.type')
                    .val(data.type)
                    .end();
            if (data.group == "Person") {
                $("#group_1").prop('checked', 'checked');
                $('.company-name').addClass('collapse');
                $(".more-field").removeClass("collapse");
                $(".more-field-btn").text("Show Less Field");
            }
            if (data.group == "Company") {
                $("#group_0").prop('checked', 'checked');
                $('.company-name').removeClass('collapse');
            }
            $('#myModal').modal('show');
        })
        .on("click", ".reset-contact-button", function (e) {
            document.getElementById("create-contact-form").reset();
        })
        .on("click", "input[name=group]", function (e) {
            if (this.value == 1) {
                $('.company-name').addClass('collapse');
                $(".more-field").removeClass("collapse");
                $(".more-field-btn").text("Show Less Field");
                $(".job_title").attr('placeholder', 'Relation');
            } else {
                $('.company-name').removeClass('collapse');
                $(".job_title").attr('placeholder', 'Job title');
            }
        })
        .on("click", ".delete-share-contact", function (e) {
            var r = confirm("Are you sure want to delete contact ?");
            if (r == true) {
                let url = $('#delete-contact-url').val();
                let shareContactId = $(this).attr('data-id');
                $.ajax({
                    type: "POST",
                    url: url,
                    data: {'share_contact_id': shareContactId},
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    },
                    statusCode: {
                        200: function (response) {
                            toastr.success(response.message, 'Success');
                            $('#myModal').modal('hide');
                            app.dataTable.ajax.reload();

                            l.stop();
                        },
                        400: function (response) {
                            toastr.error(response.responseJSON.message, 'Error');
                            l.stop();
                        }
                    }
                });
            }
        })






</script>
@endpush