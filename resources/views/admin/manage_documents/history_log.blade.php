@php
$user_id=Auth::id();
@endphp
@extends('layouts.main')
@section('title','Share History')
@section('content')
<style>
    .contact-label {
        padding: 6px 12px;
    }
    .modal-title{display: contents; }
    .view_history_div{    border: 1px solid #1a3d5c;
        border-radius: 10px;
        margin: 5px 5px 10px 5px;
        background: #f3ecec;
        padding: 0px;
    }
    .notification {
        text-decoration: none;
        position: relative;
        display: inline-block;
        border-radius: 2px;
    }
    .notification .badge {
        position: absolute;
        top: -10px;
        right: -25px;
        padding: 5px 10px;
        /* border-radius: 50%; */
        background-color: green;
        font-size: 5px;
        color: #ffffff;
        font-size: 10px;
    }
    .new-description{
        font-weight: bold;
    }
</style>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.9/css/responsive.bootstrap.min.css">

<div class="container mt-30">
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title"><span id="shared_to_title">Shared To</span> : Resume Shared Log</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default"
                            data-dismiss="modal">Close
                    </button>
                </div>
            </div>

        </div>
    </div>

    <section class="row form-group">
        <div class="col-md-4">
            <b style="display:inline-block; border-right: 2px black solid;padding-right: 10px">Resume History</b>
            <a style="padding:6px 12px;display:inline-block" href="{{route('share.history.document')}}">
                Document History
            </a>
        </div>
        <div class="col-md-5 text-center">
            <h4>Share History : Resume</h4>
        </div>
        <div class="col-md-3">
        </div>
        <div class="col-md-12">
            <hr>
        </div>
    </section>
    <section class="row">
        <table id="share-history-table" class="display">
            <thead>
                <tr>
                    <th>Shared To</th>
                    <th>Email</th>
                    <th>Group</th>
                    <th>Shared On</th>
                    <th>Validity</th>
                    <th>Last Echo</th>
                    <th>Detailed Info</th>
                </tr>
            </thead>
        </table>
    </section>
</div>
<div class="viewHistoryLogHtml collapse">
    <div class="row view_history_div">
        <div class="col-md-4">
            <h6 class="history_date"></h6>
        </div>
        <div class="col-md-8">
            <h6 class="history_discription"></h6>
        </div>
    </div>
</div>
<div class="emptyViewHistoryLogHtml collapse">
    <div class="row view_history_div">
        <h6 class="text-center">No response yet.</h6>
    </div>
</div>
<input type="hidden" value="{{URL::to('seafarer/get-share-history-resume')}}"  id="data-table-url">
<input type="hidden" value="{{URL::to('seafarer/view-share-histroy')}}"  id="view-share-histroy">
@endsection
@push('scripts')
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/responsive/2.2.9/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8/dist/sweetalert2.min.js"></script>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/sweetalert2@8/dist/sweetalert2.min.css">
<script>
let app = {
    dataTable: '',
    data: {}
}
$(document).ready(function () {
//    $('#myTable').DataTable();
    let dataTableUrl = $('#data-table-url').val();
    app.dataTable = $('#share-history-table').DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        language: {
            "emptyTable": "No Resume Shared"
        },
        ajax: {
            url: dataTableUrl,
            dataSrc: function (json) {
                $(json.data).each(function (key, value) {
                    app.data[value.id] = value;
                });
                return json.data
            }
        },
        "order": [[ 5, 'desc' ]],
        columns: [
            {data: 'shared_to', name: 'shared_to'},
            {data: 'email', name: 'email'},
            {data: 'group', name: 'group'},
            {data: 'created_at', name: 'created_at'},
            {data: 'validity', name: 'validity'},
            {data: 'latest_qr', name: 'latest_qr'},
            {data: 'action', name: 'action'}
        ]
    });
})
        .on("hidden.bs.modal", "#history_log", function (e) {
            $('#history_log').remove();
        })
        .on('click', '.view-share-history', function () {
            let dataId = $(this).attr('data-id');
            let shareHistoryData = app.data[dataId];
            var shared_to = shareHistoryData.email;
            if(shareHistoryData.share_contact &&  shareHistoryData.share_contact.name) {
                shared_to = shareHistoryData.share_contact.name;
            } else if(shareHistoryData.share_contact &&  shareHistoryData.share_contact.p_name) {
                shared_to = shareHistoryData.share_contact.p_name
            }
            
            $("#shared_to_title").html(shared_to);
            let historyData = app.data[dataId]['history_log'];
            let modal = $('#myModal').clone();
            modal
                    .attr('id', 'history_log')
                    .modal('show')
                    .end();
            $.each(historyData, function( index, value ) {
                let html = $('.viewHistoryLogHtml').clone();
                html
                        .removeClass('viewHistoryLogHtml')
                        .removeClass('collapse')
                        .find('.history_date')
                        .html(value.created)
                        .end()
                        .find('.history_discription')
                        .html(value.description)
                        .end()
                .appendTo(modal.find('.modal-body'))
                    .end();
                    if(value.is_new == 1){
                        html
                            .find('.history_discription')
                            .addClass('new-description')
                            .end()
                    }
            });
            if(historyData.length == 0){
                let html = $('.emptyViewHistoryLogHtml').clone();
                html
                        .removeClass('emptyViewHistoryLogHtml')
                        .removeClass('collapse')
                .appendTo(modal.find('.modal-body'))
                    .end();
            }else{
                let html = $('.viewHistoryLogHtml').clone();
                html
                        .removeClass('viewHistoryLogHtml')
                        .removeClass('collapse')
                        .find('.history_date')
                        .html(shareHistoryData.created_at)
                        .end()
                        .find('.history_discription')
                        .html('Resume shared')
                        .end()
                .appendTo(modal.find('.modal-body'))
                    .end();
                let viewUrl = $('#view-share-histroy').val();
                viewUrl = viewUrl + '?share_histoyr_id='+dataId;
                console.log(viewUrl);
                console.log(dataId);
                $.ajax({url: viewUrl, success: function(result){
                }});
            }
        })

        
        $(document).on('click', '.delete_doc', function () {
            var id = $(this).data("id");
            var current = $(this).data('current');
            var img_name = $(this).data("imagename");
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        url: '{{route('site.user.post.deleteSharedResume')}}',
                        type: "POST",
                        data: {"id": id},
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content'),
                        },
                        success: function (data) {
                            app.dataTable.ajax.reload();
                        }
                    });
                    Swal.fire(
                        'Deleted!',
                        'Your file has been deleted.',
                        'success'
                    )
                }
            });
        });
</script>
@endpush