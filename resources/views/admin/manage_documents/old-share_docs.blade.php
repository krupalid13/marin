@php
    $user_id=Auth::id();
    $bank_flag=true;
@endphp
@extends('layouts.main')
@section('title','Share Documents')
@section('content')
    <div class="container m_h-100v">
        <h4 class="text-center h4 heading">Shared Documents</h4>
        <div class="row">
            @if($user_contract['is_docs']==true)
                <hr/>
                <form id="shareDocs" onkeydown="return event.key != 'Enter';">
                    <h5 class="text-uppercase">Share your Documents</h5>
                    <input type="hidden" name="_token" value="{{csrf_token()}}" />
<!--@php var_dump($user_contract); @endphp	-->				
                    @foreach($user_contract as $index => $data)
                        @if($index!='is_docs')
                            <div class=""> 
                                @if( $bank_flag == true && $index=='aadhaar')
                                    @php
                                        $bank_flag=false;
                                    @endphp
                                    <hr/>
                                    <h5 class="text-uppercase">Share your banking details and documents</h5>
                                    <ul class="m-b-5">
                                        <li>
                                            <label class="cursor-pointer" style="font-weight: 300" for="bank_details">
                                                <input class="bank_details" type="checkbox"
                                                       name="shareable_docs_parent[]"
                                                       id="bank_details"
                                                       value="bank_details"/> Banks Details
                                            </label>
                                            <a href="#" data-toggle="modal" data-target="#myModal">view</a>

                                            <div id="myModal" class="modal fade" role="dialog">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-body">
                                                            @php
                                                                $user=App\User::whereId($user_id)->first();
                                                            @endphp
                                                            <div class="row">
                                                                <div class="col-sm-6">
                                                                    Name
                                                                    : {{$user['first_name'].' '.$user['last_name'] }}
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    Bank Name : {{$bank_detail['bank_name']}}
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    Acount Number : {{$bank_detail['account_no']}}
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    IFSC Code : {{$bank_detail['ifsc_code']}}
                                                                </div>
                                                                <div class="col-md-6">
                                                                    Branch Address : {{$bank_detail['branch_address']}}
                                                                </div>
                                                            </div>
                                                            <hr/>
                                                            <div class="row">
                                                                <div class="col-sm-6">
                                                                    Pan Card : {{$personal_detail['pancard']}}
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    Aadhaar No. : {{$personal_detail['aadhaar']}}
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-default"
                                                                    data-dismiss="modal">Close
                                                            </button>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                @endif
								@if($index!="us_visa")
                                <ul class="m-b-5">
                                    <input id="{{$index}}" type="checkbox" name="shareable_docs_parent[]"
                                           value="{{$index}}">
                                    <label class="cursor-pointer" style="font-weight: 300" for="{{$index}}"> 
                                        @if($index=="school_qualification")
                                            {{strtoupper('Qualification')}}
                                        @elseif($index=="institute_degree")
                                            {{strtoupper('Pre-sea')}}
                                        @else
                                            {{strtoupper(str_replace('_',' ',$index))}}
                                        @endif
										
                                    </label>
                                    @foreach($data as $_docs_type_id => $_docs_data)
                                        @if($_docs_type_id!='id' && count($_docs_data[0]['user_documents'])>0)
                                            <li class="pl-25">
                                                <label class="cursor-pointer" style="font-weight: 300" for="{{$_docs_type_id}}">
                                                    <input class="{{$index}}" type="checkbox" name="shareable_docs_children[{{$index}}][]" id="{{$_docs_type_id}}" value="{{$_docs_type_id}}"/>
                                                    @if($index=="school_qualification")
                                                        {{strtoupper('Qualification')}}
                                                    @elseif($index=="institute_degree")
                                                        {{strtoupper('Pre-sea')}}
                                                    @else
                                                        {{ ($_docs_type_id == 'all') ?  'Oil + Chemical + Liquefied Gas' : '' }}
                                                    @endif
                                                    @if (isset($_docs_data['heading']))
                                                        {{  $_docs_data['heading'] }}
                                                    @else
                                                        @if ($index != 'dce')
                                                            {{  str_replace('_',' ',$index) }}
                                                        @endif
                                                    @endif
                                                </label>
                                            </li>
                                        @endif
                                    @endforeach
                                </ul>
								@endif
                            </div>
                        @endif
                    @endforeach
                    <div class="panel panel-footer text-center">
                        <button class="btn btn-azure" type="button" onclick="select_all()">Select All</button>
                        <button class="btn btn-warning" type="button" onclick="reset_all()">Clear All</button>
                        <button class="btn btn-success btn_share" type="button" data-toggle="modal" data-target="#share"
                                disabled="disabled">Share
                        </button>

                        <div id="share" class="modal fade" role="dialog">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="form-group">
                                                <div class="col-md-3">
                                                    <label for="email" class="p-t-7">Share To:</label>
                                                </div>
                                                <div class="col-md-9">
                                                    <input class="form-control" name="email" id="email" type="email"
                                                           placeholder="Enter E-Mail ID" required>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default"
                                                data-dismiss="modal">Close
                                        </button>
                                        <button type="button" onclick="Submit(event,)" data-endpoint="{{route('share.documents.post')}}" class="btn btn-primary">Send</button>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </form>
            @else
                <div class="mt-30 panel panel-bricky">
                    <div class="panel-heading text-center">
                        <h4 class="h4">Kindly <a href="{{route('site.seafarer.edit.profile')}}">Upload</a> atleast one
                            Document to use This Feature.</h4>
                    </div>
                </div>

            @endif
        </div>
        <div class="clearfix"></div>
    </div>
@stop
@push('scripts')
    <script>
        const _checkboxs = () => {
            if ($('input:checked').length > 0) $('.btn_share').removeAttr('disabled');
            else $('.btn_share').attr('disabled', true);
        }

        function select_all() {
            $('input[type="checkbox"]').prop('checked', true);
            _checkboxs();
        }

        function reset_all() {
            $('form').trigger('reset');
            _checkboxs();
        }

        $(document).ready(() => {
            $('ul input').on('change', (e) => {
                var r = (e.target.value);
                if ($(`#${r}`)[0].checked) {
                    $(`.${r}`).prop('checked', true);
                } else $(`.${r}`).prop('checked', false);
                _checkboxs();
            });

        });
            function Submit(e) {
                e.preventDefault();
                const  email = $(`#email`).val();
                if(email == '' ||  !(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email))){
                    toastr.error('email not valid');
                    return false;
                } else {
                    const endpoint = e.target.dataset.endpoint;
                    const token = $('input[name="_token"]').val();
                    e.target.innerText = 'sending...';
                    $.post(endpoint,$('#shareDocs').serialize()+'&_token='+token, (res) => {
                        if (res.status == 200) toastr.success(res.msg);
                        else toastr.error(res.msg);
                        // setTimeout(() => {
                        //     location.reload(true);
                        //     {{--location.href = {{route('share.index')}}--}}
                        // }, 3000)
                    });
                }
            }
    </script>

@endpush