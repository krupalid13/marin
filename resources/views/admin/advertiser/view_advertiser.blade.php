@extends('admin.index')

@section('content')
<div id="view-company-page">
	<div class="container main-content">
		<div class="toolbar row">
			<div class="col-sm-6 hidden-xs">
				<div class="page-header">
					<h1>Advertiser <small>View Advertiser Details</small></h1>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12 m_t_25">
				<div class="tabbable">
					<ul id="myTab2" class="nav nav-tabs">
						<li class="active">
							<a href="#advertiser_details" data-toggle="tab">
								Advertiser Details
							</a>
						</li>
                        <li>
                            <a href="#advertiser_subscription" data-url="{{route('admin.get.user.subscription',['page' => 1,'role' => 'advertiser','advertiser_id' => isset($data['company_data'][0]['id']) ? $data['company_data'][0]['id'] : ''])}}" data-toggle="tab">
                                Subscription
                            </a>
                        </li>
						<li class="pull-right">
							<a type="button" class="fa fa-edit" href="{{Route('admin.edit.advertiser',$data['user'][0]['id'])}}">
								Edit
							</a>
						</li>
					</ul>
					<div class="tab-content panel-body">
						<div class="tab-pane fade in active content" id="advertiser_details">
							<div class="panel-heading">
								<h4 class="panel-title">ACCOUNT CREDENTIALS</h4>
							</div>
							<?php
                            	$image_path = '';
                            	if(isset($data['user'][0]['profile_pic'])){
                                $image_path = env('SEAFARER_PROFILE_PATH')."".$data['user'][0]['id']."/".$data['user'][0]['profile_pic'];
                            }
                            ?>
                            <div class="row text-center" style="display: flex;justify-content: center;">
                                <div class="col-lg-3 col-md-5 col-sm-5 company_profile_pic" style="border-radius: 50%;">
                                    <img class="seafarer_profile_pic" src="/{{ isset($image_path) ? $image_path : 'images/user_default_image.png'}}" >
                                </div>
                            </div>

							<div class="row">
                                <label class="col-sm-3 control-label">
                                    Owner Name:
                                </label>
                                <div class="col-sm-9">
                                	{{ isset($data['user'][0]['first_name']) ? $data['user'][0]['first_name'] : ''}}
                                </div>
                            </div>

                            <div class="row">
                                <label class="col-sm-3 control-label">
                                    Owner Mobile Number:
                                    @if($data['user'][0]['is_mob_verified'] == '1')
                                        <i class="fa fa-check-circle green f-15" aria-hidden="true" title="Mobile Verified"></i>
                                    @else
                                        <i class="fa fa-minus-circle red f-15" aria-hidden="true" title="Mobile Unverified"></i>
                                    @endif
                                </label>
                                <div class="col-sm-9">
                                	{{ isset($data['user'][0]['mobile']) ? $data['user'][0]['mobile'] : ''}}
                                </div>
                            </div>

                            <div class="row">
                                <label class="col-sm-3 control-label">
                                    Owner Email Address:
                                    @if($data['user'][0]['is_email_verified'] == '1')
                                        <i class="fa fa-check-circle green f-15" aria-hidden="true" title="Email Verified"></i>
                                    @else
                                        <i class="fa fa-minus-circle red f-15" aria-hidden="true" title="Email Unverified"></i>
                                    @endif
                                </label>
                                <div class="col-sm-9">
                                	{{ isset($data['user'][0]['email']) ? $data['user'][0]['email'] : ''}}
                                </div>
                            </div>

                            <div class="panel-heading">
								<h4 class="panel-title">COMPANY DETAILS</h4>
							</div>
							<?php
	                            $company_image_path = '';

	                            if(isset($data['company_data'][0]['company_registration_detail']['company_logo']) AND !empty($data['company_data'][0]['company_registration_detail']['company_logo'])){
	                                $company_image_path = env('COMPANY_LOGO_PATH')."".$data['company_data'][0]['id']."/".$data['company_data'][0]['company_registration_detail']['company_logo'];
	                            }
	                        ?>
							<div class="row text-center" style="display: flex;justify-content: center;">
								<div class="col-lg-3 col-md-5 col-sm-5 company_profile_pic">
	                				<img src="/{{ !empty($company_image_path) ? $company_image_path : 'images/user_default_image.png'}}" >
								</div>
							</div>
							
							<div class="row">
                                <label class="col-sm-3 control-label">
                                    Company Name:
                                </label>
                                <div class="col-sm-3">
                                	{{ isset($data['company_data'][0]['company_registration_detail']['company_name']) ? $data['company_data'][0]['company_registration_detail']['company_name'] : ''}}
                                </div>
                                <label class="col-sm-3 control-label">
                                    Company Website:
                                </label>
                                <div class="col-sm-3">
                                    {{ isset($data['company_data'][0]['company_registration_detail']['website']) && !empty($data['company_data'][0]['company_registration_detail']['website']) ? $data['company_data'][0]['company_registration_detail']['website'] : '-'}}
                                </div>
                            </div>
                            
                            <div class="row">
                                <label class="col-sm-3 control-label">
                                    Country:
                                </label>
                                <div class="col-sm-3">
                                    @foreach( \CommonHelper::countries() as $c_index => $country)
                                        {{ isset($data['company_data'][0]['company_registration_detail']['company_locations'][0]['country']) ? $data['company_data'][0]['company_registration_detail']['company_locations'][0]['country'] == $c_index ? $country : '' : ''}}
                                    @endforeach
                                </div>
                            
                                <label class="col-sm-3 control-label">
                                    State:
                                </label>
                                <div class="col-sm-3">
                                    {{ isset($data['company_data'][0]['company_registration_detail']['company_locations'][0]['state_id']) ? $data['company_data'][0]['company_registration_detail']['company_locations'][0]['pincode']['pincodes_states'][0]['state']['name'] : (isset($data['company_data'][0]['company_registration_detail']['company_locations'][0]['state_text']) ? $data['company_data'][0]['company_registration_detail']['company_locations'][0]['state_text'] : '')}}
                                </div>
                            </div>

                            <div class="row">
                                <label class="col-sm-3 control-label">
                                    City:
                                </label>
                                <div class="col-sm-3">
                                    {{ isset($data['company_data'][0]['company_registration_detail']['company_locations'][0]['city_id']) ? $data['company_data'][0]['company_registration_detail']['company_locations'][0]['pincode']['pincodes_cities'][0]['city']['name'] : (isset($data['company_data'][0]['company_registration_detail']['company_locations'][0]['city_text']) ? $data['company_data'][0]['company_registration_detail']['company_locations'][0]['city_text'] : '')}}
                                </div>
                            
                                <label class="col-sm-3 control-label">
                                    Pin Code:
                                </label>
                                <div class="col-sm-3">
                                    {{ isset($data['company_data'][0]['company_registration_detail']['company_locations'][0]['pincode_text']) ? $data['company_data'][0]['company_registration_detail']['company_locations'][0]['pincode_text'] : ''}}
                                </div>
                            </div>

                            <div class="row">
                                <label class="col-sm-3 control-label">
                                    Company Address:
                                </label>
                                <div class="col-sm-9">
                                	{{ isset($data['company_data'][0]['company_registration_detail']['address']) ? $data['company_data'][0]['company_registration_detail']['address'] : ''}}
                                </div>
                            </div>

                            <div class="row">
                                <label class="col-sm-3 control-label">
                                    Member of any club or association:
                                </label>
                                <div class="col-sm-3">
                                    @foreach( \CommonHelper::member_category() as $c_index => $category)
                                        {{ isset($data['company_data'][0]['company_registration_detail']['b_i_member']) ? $data['company_data'][0]['company_registration_detail']['b_i_member'] == $c_index ? $category : '' : ''}}
                                    @endforeach
                                    
                                </div>
                                <label class="col-sm-3 control-label">
                                    Bussiness Category:
                                </label>
                                <div class="col-sm-3">
                                    @foreach(\CommonHelper::bussiness_category() as $index => $categories)
                                        @foreach( $categories as $c_index => $category)
                                            {{ isset($data['company_data'][0]['company_registration_detail']['bussiness_category']) ? $data['company_data'][0]['company_registration_detail']['bussiness_category'] == $c_index ? $category : '' : ''}}
                                        @endforeach
                                    @endforeach
                                </div>
                            </div>
                            
                            <div class="row">
                                <label class="col-sm-3 control-label">
                                    Years In Bussiness:
                                </label>
                                <div class="col-sm-3">
                                	{{ isset($data['company_data'][0]['company_registration_detail']['buss_years']) ? $data['company_data'][0]['company_registration_detail']['buss_years'] : ''}}
                                    
                                </div>
                                <label class="col-sm-3 control-label">
                                    Product/Service Description:
                                </label>
                                <div class="col-sm-3">
                                        {{ isset($data['company_data'][0]['company_registration_detail']['product_description']) ? $data['company_data'][0]['company_registration_detail']['product_description'] : ''}}
                                </div>
                            </div>

                            <div class="row">
                                <label class="col-sm-3 control-label">
                                    Bussiness Description:
                                </label>
                                <div class="col-sm-9">
                                    {{ isset($data['company_data'][0]['company_registration_detail']['bussiness_description']) ? $data['company_data'][0]['company_registration_detail']['bussiness_description'] : ''}}
                                </div>
                            </div>
                            <div class="row">
                                <label class="col-sm-3 control-label">
                                   Email To Be Displayed:
                                </label>
                                <div class="col-sm-3">
                                    {{ isset($data['company_data'][0]['company_registration_detail']['email_display']) ? $data['company_data'][0]['company_registration_detail']['email_display'] : ''}}
                                </div>
                                <label class="col-sm-3 control-label">
                                   Contact Number To Be Displayed:
                                </label>
                                <div class="col-sm-3">
                                	{{ isset($data['company_data'][0]['company_registration_detail']['contact_display']) ? $data['company_data'][0]['company_registration_detail']['contact_display'] : ''}}
                                </div>
                            </div>
						</div>
                        
                        <div id="advertiser_subscription" class="tab-pane fade" data-id="{{ isset($data['company_data'][0]['company_registration_detail']['id']) ? $data['company_data'][0]['company_registration_detail']['id'] : ''}}" data-role='advertiser' data-advertiser="{{ isset($data['company_data'][0]['id']) ? $data['company_data'][0]['id'] : ''}}">
                            <div class="row panel-heading">
                                <div class="col-sm-6">
                                    <h4 class="panel-title">SUBSCRIPTION DETAILS</h4>
                                </div>
                            </div>
                            <div class="full-cnt-loader hidden">
                                <div class="" style="text-align: center;padding-top:20px;padding-bottom:20px; ">
                                    <i class="fa fa-spinner fa-spin fa-4" aria-hidden="true" style="font-size: 7em;"></i>
                                </div>
                            </div>
                            <div class="data">
                                No Subscriptions Found.
                            </div>
                            
                        </div>				
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@stop
@section('js_script')
    <script type="text/javascript" src="/js/site/advertise-registration.js"></script>
@stop