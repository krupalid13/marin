@extends('admin.index')

@section('content')

<div class="main-wrapper">
	<div class="main-container1">
				<!-- start: PAGE -->
				<div class="main-content">
					<div class="container">
						<!-- start: PAGE HEADER -->
						<!-- start: TOOLBAR -->
						<div class="toolbar row">
							<div class="col-sm-6 hidden-xs">
								<div class="page-header">
									<h1>Advertisements <small>A list of all advertisements</small></h1>
								</div>
							</div>
						</div>

						<!-- filter -->
						<form class="filter-search-form m_l_10">
							<div class="row m_t_25">
								<div class="col-md-12">
									<div class="panel panel-white">
										<div class="panel-body">
											<div class="row">
												<div class="col-sm-3">
					                                <div class="form-group">
					                                    <label>Owner Name </label>
					                                    <input type="text" class="form-control" name="first_name" id="first_name" value="{{isset($filter['first_name']) ? $filter['first_name'] : ''}}" placeholder="Enter Owner Name">
					                                </div>
					                            </div>
					                            <div class="col-sm-3">
					                                <div class="form-group">
					                                    <label>Company Name </label>
					                                    <input type="text" class="form-control" name="company_name" id="company_name" value="{{isset($filter['company_name']) ? $filter['company_name'] : ''}}" placeholder="Enter Company Name">
					                                </div>
					                            </div>
					                            <div class="col-sm-3">
					                                <div class="form-group">
					                                    <label>Bussiness Category </label>
					                                    <select name="bussiness_category" class="form-control">
		                                                    <option value="">Select Category</option>
		                                                    @foreach(\CommonHelper::bussiness_category() as $index => $categories)
                                                            	<optgroup label="{{$index}}">
                                                                @foreach($categories as $c_index => $category)
                                                                  <option value="{{ $c_index }}" {{ isset($filter['bussiness_category']) ? $filter['bussiness_category'] == $c_index ? 'selected' : '' : ''}}> {{ $category }}</option>
                                                                @endforeach
                                                        	@endforeach
		                                                </select>
					                                </div>
					                            </div>         
												
					                            <div class="col-sm-3">
					                                <div class="section_cta text-right m_t_25 job-btn-container">
					                                    <button type="button" class="btn btn-danger coss-inverse-btn search-reset-btn" id="seafarerResetButton">Reset</button>
					                                    <button type="submit" data-style="zoom-in" class="btn btn-info coss-primary-btn search-post-btn ladda-button candidate-filter-search-button" id="candidate-filter-search-button"><i class="fa fa-search search-icon" aria-hidden="true"></i>Search</button>
					                                </div>
					                            </div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</form>

						<div class="row search-results-main-container" >
							<div class="col-md-12">
								<div class="panel panel-white">
									<div class="panel-body">
										<?php 
											$data_array = $data->toArray();
											$pagination = $data;
										?>
										@if(isset($data_array['data']) && !empty($data_array['data']))
										
											<div class="panel-heading">
												<div class="row">
													<div class="col-xs-12">
														<div class="col-xs-6 paginator-content  pull-right no-margin">
															<ul id="top-user-paginator" class="pagination">
																{!! $data->render() !!}
															</ul>
														</div>
														
														<div class="pull-left search-count-content">
															Showing {{$data_array['from']}} - {{$data_array['to']}} of {{$data_array['total']}} Results
														</div>
													</div>
												</div>
											</div>
											
											<div class="full-cnt-loader hidden">
												<div class="" style="text-align: center;padding-top:20px;padding-bottom:20px; ">
												    <i class="fa fa-spinner fa-spin fa-4" aria-hidden="true" style="font-size: 7em;"></i>
											    </div>
											</div>
											<div class="list-container">
												@foreach($data_array['data'] as $data)	
												<div class="seafarer-listing-container">
													<div class="panel advertisement-card">
														<div class="panel-heading border-light partition-blue">
															<h5 class="panel-title text-bold header-link-text p-5-5">
																<a href="">
																	{{isset($data['company_registration_details']['user_details']['first_name']) ? ucfirst($data['company_registration_details']['user_details']['first_name']) : ''}}
																</a>
															</h5>
															
															<ul class="panel-heading-tabs border-light">
																<li class="panel-tools">
																	<div class="dropdown">
																		<a data-toggle="dropdown" class="btn btn-xs dropdown-toggle btn-transparent-grey">
																			<i class="fa fa-cog"></i>
																		</a>
																		<ul class="dropdown-menu dropdown-light pull-right" role="menu">
																			<li>
																				<a class="change-adv-status cursor-pointer {{ $data['status'] == '0' ? 'hide' : ''}}" data-status='deactive' data-id={{$data['id']}} id="deactive-{{$data['id']}}">
																					Deactivate
																				</a>
																			</li>
																			<li>
																				<a class="change-adv-status cursor-pointer {{ $data['status'] == '1' ? 'hide' : ''}}" data-status='active' data-id={{$data['id']}} id="active-{{$data['id']}}">
																					Activate
																				</a>
																			</li>
																			<!-- <li>
																				<a class="change-adv-status cursor-pointer {{ $data['status'] == '2' ? 'hide' : ''}}" data-status='live' data-id={{$data['id']}} id="live-{{$data['id']}}">
																					live
																				</a>
																			</li> -->
																		</ul>
																	</div>
																</li>
															</ul>

														</div>
																		
														<div class="panel-body">
															<div class="pull-right advertisement-label-container">
																@if($data['status'] == 0)
																	<div class="label label-danger">Admin Unapproved</div>
																@elseif($data['status'] == 1)
																	<div class="label label-info">Admin approved</div>
																@elseif($data['status'] == 2)
																	<div class="label label-success">Active</div>
																@endif
															</div>
															<div class="row">
																<div class="col-lg-4 col-md-5 col-sm-5" id="user-profile-pic">
																	<a>
																		<div class="user-profile-pic-thumb make-user-profile-pic-block">
						                                					<img src="{{!empty($data['img_path']) ? "/".env('ADVERTISE_PATH').$data['company_id']."/".$data['img_path'] : 'images/user_default_image.png'}}" avatar-holder-src="{{ asset('images/user_default_image.png') }}" style="max-width: 200px;">
						                                				</div>
							                            			</a>
																</div>
																<div class="col-lg-8 col-md-7 col-sm-7">
																	<div class="row">
																		<div class="col-sm-6 sm-t-20">
																			<div class="row">
																				<div class="col-xs-12">
																					<div class="header-tag">
																						Basic Information
																					</div>
																				</div>
																			</div>
																			<div id="user-basic-details">
																				<div class="row">
																					<div class="col-xs-6 col-sm-7">
																						<dl>
																							<dt> Advertiser Name : </dt>
																							<dd class="margin-bottom-5px">
																								{{isset($data['company_registration_details']['user_details']['first_name']) ? ucfirst($data['company_registration_details']['user_details']['first_name']) : ''}}
																							</dd>
																							<dt> Advertiser Mobile Number : </dt>
																							<dd class="margin-bottom-5px make-word-wrap-break">
																								{{isset($data['company_registration_details']['user_details']['mobile']) ? $data['company_registration_details']['user_details']['mobile'] : ''}}
																				     		</dd>
																				     		<dt> Advertiser Email Address : </dt>
																							<dd class="margin-bottom-5px">
																								{{isset($data['company_registration_details']['user_details']['email']) ? $data['company_registration_details']['user_details']['email'] : ''}}
																				     		</dd>
																						</dl>
																					</div>
																				</div>
																			</div>
																		</div>
																		
																		<div class="col-sm-4">
																			
																			<div id="user-professional-details">
																				<div class="row">
																					<div class="col-xs-12">
																						<div class="header-tag">
																							Company Details
																						</div>
																					</div>
																				</div>
																				<dl>
																		     		<dt> Company Name : </dt>
																						<dd class="margin-bottom-5px">
																							{{ isset($data['company_registration_details']['company_name']) ? $data['company_registration_details']['company_name'] : '' }}
																		     		</dd>
																	     		<dt>
																				<dt> Member of any club or association:</dt>
																					<dd class="margin-bottom-5px">
																						@foreach( \CommonHelper::member_category() as $c_index => $category)
																                            {{ isset($data['company_registration_details']['b_i_member']) ? $data['company_registration_details']['b_i_member'] == $c_index ? $category : '' : ''}}
																                        @endforeach
																					</dd>
																				<dt> Bussiness Category :</dt>
																				<dd class="margin-bottom-5px">
																					@foreach(\CommonHelper::bussiness_category() as $index => $categories)
																						@foreach( $categories as $c_index => $category)
												                                            {{ isset($data['company_registration_details']['bussiness_category']) ? $data['company_registration_details']['bussiness_category'] == $c_index ? $category : '' : ''}}
												                                        @endforeach
																					@endforeach
																				</dd>
																			</dl>
																		</div>
																	</div>
																</div>
																				
															</div>
														</div>
													</div>
																	
												</div>
												@endforeach
											</div>
											@if(isset($pagination) && !empty($pagination))
										
												<div class="panel-heading">
													<div class="row">
														<div class="col-xs-12">
															<div class="col-xs-6 paginator-content  pull-right no-margin">
																<ul id="top-user-paginator" class="pagination">
																	{!! $pagination->render() !!}
																</ul>
															</div>
															
															<div class="pull-left search-count-content">
																Showing {{$data_array['from']}} - {{$data_array['to']}} of {{$data_array['total']}} Results
															</div>
														</div>
													</div>
												</div>
											@endif
										@else
											<div class="no-results-found">No results found. Try again with different search criteria.</div>
										@endif
									</div>
								</div>
							</div>
						</div>
						<!-- end: PAGE CONTENT-->
					</div>
					<div class="subviews">
						<div class="subviews-container"></div>
					</div>
				</div>
				<!-- end: PAGE -->
			</div>
</div>

	<script type="text/javascript">
        
        var searchAjaxCall = "undefined";

        $(document).ready(function(){

            //init history plugin to perform search without page refresh
            // Establish Variables
            History = window.History; // Note: We are using a capital H instead of a lower h

            // Bind to State Change
            // Note: We are using statechange instead of popstate
			
            History.Adapter.bind(window,'statechange', function() {

                // Log the State
                var State = History.getState(); // Note: We are using History.getState() instead of event.state

                //start search (defined)
                fetchSearchResults(State.url, 
                                    function() {
                                        
                                    }, function() {

                                    });

            });

            

        });

        function fetchSearchResults(url, successCallback, errorCallback)
	    {
	    	
	    	var temp_current_state_url = url.split('?');
            if(temp_current_state_url.length > 1 && temp_current_state_url[1] != ''){
                var parameters = temp_current_state_url[1].split('&');

                var dropdown_elements = ['bussiness_category','bi_member'];

                for(var i=0; i<parameters.length; i++){

                    var param_array = parameters[i].split('=');
                    if($.inArray(param_array[0],dropdown_elements) > -1){
                        $('select[name="'+param_array[0]+'"]').val(param_array[1]);
                    }else if($.inArray(param_array[0],['first_name','company_name']) > -1){
                        $('input[name="'+param_array[0]+'"]').val(param_array[1]);
                    }

                }
            } else {
             	$(':input','#candidate-filter-form')
	                .not(':button, :submit, :reset, :hidden')
	                .val('')
	                .removeAttr('checked')
	                .removeAttr('selected');
	             }

	        if(searchAjaxCall != "undefined")
	        {
	            searchAjaxCall.abort();
	            searchAjaxCall = "undefined";
	        }

	        $('.list-container').addClass('hide');
        	$('.full-cnt-loader').removeClass('hidden');

	        searchAjaxCall = $.ajax({
	            url: url,
	            dataType: 'json',
	            statusCode: {
	                200: function(response) {
	                    if (typeof response.template != 'undefined') {
	                       
	                        $('.list-container').removeClass('hide');
                        	$('.full-cnt-loader').addClass('hidden');

							$('.search-results-main-container').html(response.template);

	                        if(typeof successCallback != "undefined")
	                        {
	                            successCallback();
	                        }

	                    }
	                }
	            },
	            error: function(error, response) {

	                if(typeof errorCallback != "undefined")
	                {
	                    errorCallback();
	                }
	            }
	        });
	    }
    </script>

@stop
@section('js_script')
    <script type="text/javascript" src="/js/site/advertise-registration.js"></script>
@stop