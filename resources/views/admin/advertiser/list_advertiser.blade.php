@extends('admin.index')

@section('content')

<div class="main-wrapper">
	<div class="main-container1">
				<!-- start: PAGE -->
				<div class="main-content">
					<div class="container">
						<!-- start: PAGE HEADER -->
						<!-- start: TOOLBAR -->
						<div class="toolbar row">
							<div class="col-sm-6 hidden-xs">
								<div class="page-header">
									<h1>Advertiser <small>A list of all Advertisers</small></h1>
								</div>
							</div>
						</div>

						<!-- filter -->
						<form class="filter-search-form m_l_10">
							<div class="row m_t_25">
								<div class="col-md-12">
									<div class="panel panel-white">
										<div class="panel-body">
											<div class="row">
												<div class="col-sm-3">
					                                <div class="form-group">
					                                    <label>Owner Name </label>
					                                    <input type="text" class="form-control" name="first_name" id="first_name" value="{{isset($filter['first_name']) ? $filter['first_name'] : ''}}" placeholder="Enter Owner Name">
					                                </div>
					                            </div>
					                            <div class="col-sm-3">
					                                <div class="form-group">
					                                    <label>Company Name </label>
					                                    <input type="text" class="form-control" name="company_name" id="company_name" value="{{isset($filter['company_name']) ? $filter['company_name'] : ''}}" placeholder="Enter Company Name">
					                                </div>
					                            </div>
					                            <div class="col-sm-3">
					                                <div class="form-group">
					                                    <label>Bussiness Category </label>
					                                    <select name="bussiness_category" class="form-control">
		                                                    <option value="">Select Category</option>
		                                                    @foreach( \CommonHelper::bussiness_category() as $index => $categories)
		                                                    	<optgroup label="{{$index}}">
		                                                        @foreach($categories as $c_index => $category)
																	<option value="{{ $c_index }}" {{ isset($filter['bussiness_category']) ? $filter['bussiness_category'] == $c_index ? 'selected' : '' : ''}}> {{ $category }}</option>
																@endforeach
		                                                    @endforeach
		                                                </select>
					                                </div>
					                            </div>
					                            <div class="col-sm-3">
					                                <div class="form-group">
					                                    <label>State </label>
					                                    <select name="state" class="form-control state advertisement-state-filter" data-index="0">
                                                            <option value=''>Select State</option>
                                                                @foreach($state as $s_index => $state_list)
                                                                    <option value="{{$state_list['id']}}" {{ isset($data['company_type']) ? $data['company_type'] == $s_index ? 'selected' : '' : ''}}>{{$state_list['name']}} 
                                                                    </option>
                                                                @endforeach
                                                        </select>
					                                </div>
					                            </div>
					                        </div>
					                        <div class="row">
					                        	
					                            <div class="col-sm-3">
					                                <div class="form-group">
					                                    <label>City </label>
					                                    	<select name="city" id="city" class="form-control city" placeholder="Please select city">
					                                    	<option value=''> Select City</option>
					                                    </select>
					                                </div>
					                            </div>   
					                            <div class="col-sm-9">
					                                <div class="section_cta text-right m_t_25 job-btn-container">
					                                    <button type="button" class="btn btn-danger coss-inverse-btn search-reset-btn" id="seafarerResetButton">Reset</button>
					                                    <button type="submit" data-style="zoom-in" class="btn btn-info coss-primary-btn search-post-btn ladda-button candidate-filter-search-button" id="candidate-filter-search-button"><i class="fa fa-search search-icon" aria-hidden="true"></i>Search</button>
					                                </div>
					                            </div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</form>

						<div class="row search-results-main-container" >
							<div class="col-md-12">
								<div class="panel panel-white">
									<div class="panel-body">
										<?php 
											$data_array = $data->toArray();
											$pagination = $data;
										?>
										
										@if(isset($data_array['data']) && !empty($data_array['data']))
										
											<div class="panel-heading">
												<div class="row">
													<div class="col-xs-12">
														<div class="col-xs-6 paginator-content  pull-right no-margin">
															<ul id="top-user-paginator" class="pagination">
																{!! $data->render() !!}
															</ul>
														</div>
														
														<div class="pull-left search-count-content">
															Showing {{$data_array['from']}} - {{$data_array['to']}} of {{$data_array['total']}} Results
														</div>
													</div>
												</div>
											</div>
											
											<div class="full-cnt-loader hidden">
												<div class="" style="text-align: center;padding-top:20px;padding-bottom:20px; ">
												    <i class="fa fa-spinner fa-spin fa-4" aria-hidden="true" style="font-size: 7em;"></i>
											    </div>
											</div>
											<div class="list-container">
											@foreach($data_array['data'] as $data)	
												<div class="seafarer-listing-container">
													<div class="panel">
														<div class="panel-heading border-light partition-blue">
															<h5 class="panel-title text-bold header-link-text p-5-5">
																<a href="{{Route('admin.view.advertiser.id',$data['id'])}}">
																	{{isset($data['first_name']) ? ucfirst($data['first_name']) : ''}}
																</a>
															</h5>
															<ul class="panel-heading-tabs border-light">
																<li class="panel-tools">
																	<div class="dropdown">
																		<a data-toggle="dropdown" class="btn btn-xs dropdown-toggle btn-transparent-grey">
																			<i class="fa fa-cog"></i>
																		</a>
																		<ul class="dropdown-menu dropdown-light pull-right" role="menu">
																			<li>
																				<a href="{{Route('admin.view.advertiser.id',$data['id'])}}" data-type="user">
																					View
																				</a>
																			</li>
																			<li>
																				<a href="{{Route('admin.edit.advertiser',$data['id'])}}">
																					Edit
																				</a>
																			</li>
																			<li>
																				<a class="change-status-user cursor-pointer {{ $data['status'] == '0' ? 'hide' : ''}}" data-status='deactive' data-id={{$data['id']}} id="deactive-{{$data['id']}}">
																					Deactivate
																				</a>
																			</li>
																			<li>
																				<a class="change-status-user cursor-pointer {{ $data['status'] != '0' ? 'hide' : ''}}" data-status='active' data-id={{$data['id']}} id="active-{{$data['id']}}">
																					Activate
																				</a>
																			</li>
																		</ul>
																	</div>
																</li>
															</ul>

														</div>
																		
														<div class="panel-body">
															<div class="row">
																<div class="col-lg-3 col-md-5 col-sm-5" id="user-profile-pic">
																	<a>
																		<div class="user-profile-pic-thumb make-user-profile-pic-block">
						                                					<img src="/{{ isset($data['profile_pic']) ? env('SEAFARER_PROFILE_PATH')."/".$data['id']."/".$data['profile_pic'] : 'images/user_default_image.png'}}" style="height: 200px;width: 200px;">
						                                				</div>
							                            			</a>
																</div>
																<div class="col-lg-9 col-md-7 col-sm-7">
																	<div class="row">
																		<div class="col-sm-6 sm-t-20">
																			<div class="row">
																				<div class="col-xs-12">
																					<div class="header-tag">
																						Basic Information
																					</div>
																				</div>
																			</div>
																			<div id="user-basic-details">
																				<div class="row">
																					<div class="col-xs-6 col-sm-7">
																						<dl>
																							<dt> Owner Name : </dt>
																							<dd class="margin-bottom-5px">
																								{{isset($data['first_name']) && !empty(isset($data['first_name'])) ? ucfirst($data['first_name']) : '-'}}
																							</dd>
																							<dt> Owner Mobile Number : 
																								@if($data['is_mob_verified'] == '1')
															                                        <i class="fa fa-check-circle green f-15" aria-hidden="true" title="Mobile Verified"></i>
															                                    @else
															                                        <i class="fa fa-minus-circle red f-15" aria-hidden="true" title="Mobile Unverified"></i>
															                                    @endif
																							</dt>
																							<dd class="margin-bottom-5px make-word-wrap-break">
																								{{isset($data['mobile']) && !empty(isset($data['mobile'])) ? $data['mobile'] : '-'}}
																				     		</dd>
																				     		<dt> Owner Email Address : 
																				     			@if($data['is_email_verified'] == '1')
															                                        <i class="fa fa-check-circle green f-15" aria-hidden="true" title="Email Verified"></i>
															                                    @else
															                                        <i class="fa fa-minus-circle red f-15" aria-hidden="true" title="Email Unverified"></i>
															                                    @endif
																				     		</dt>
																							<dd class="margin-bottom-5px">
																								{{isset($data['email']) && !empty(isset($data['email'])) ? $data['email'] : '-'}}
																				     		</dd>
																				     		<dt> Registered On :</dt>
																							<dd class="margin-bottom-5px">
																								{{ isset($data['created_at']) && !empty($data['created_at']) ? date('d-m-Y H:i:s', strtotime($data['created_at']))  : '-'}}
																							</dd>
																						</dl>
																					</div>
																				</div>
																			</div>
																		</div>
																		
																		<div class="col-sm-4">
																			
																			<div id="user-professional-details">
																				<div class="row">
																					<div class="col-xs-12">
																						<div class="header-tag">
																							Company Details
																						</div>
																					</div>
																				</div>
																				<dl>
																		     		<dt> Company Name : </dt>
																						<dd class="margin-bottom-5px">
																							{{ isset($data['company_registration_detail']['company_name']) ? $data['company_registration_detail']['company_name'] : '' }}
																		     		</dd>
																	     		<dt>
																				<dt> Member of any club or association :</dt>
																				<dd class="margin-bottom-5px">
																					@foreach( \CommonHelper::member_category() as $c_index => $category)
								                                                        {{ isset($data['company_registration_detail']['b_i_member']) ? $data['company_registration_detail']['b_i_member'] == $c_index ? $category : '' : ''}}
								                                                    @endforeach
																				</dd>
																				<dt> Bussiness Category :</dt>
																				<dd class="margin-bottom-5px">
								                                                    @foreach(\CommonHelper::bussiness_category() as $index => $categories)
																						@foreach( $categories as $c_index => $category)
												                                            {{ isset($data['company_registration_detail']['bussiness_category']) ? $data['company_registration_detail']['bussiness_category'] == $c_index ? $category : '' : ''}}
												                                        @endforeach
																					@endforeach
																				</dd>
																				<dt> Bussiness Description :</dt>
																				<dd class="margin-bottom-5px">
																					{{isset($data['company_registration_detail']['bussiness_description']) ? $data['company_registration_detail']['bussiness_description'] : ''}}
																				</dd>
																			</dl>
																		</div>
																	</div>
																</div>
																				
															</div>
														</div>
													</div>
																	
												</div>
											@endforeach
											</div>
											@if(isset($data_array['data']) && !empty($data_array['data']))
										
												<div class="panel-heading">
													<div class="row">
														<div class="col-xs-12">
															<div class="col-xs-6 paginator-content  pull-right no-margin">
																<ul id="top-user-paginator" class="pagination">
																	{!! $pagination->render() !!}
																</ul>
															</div>
															
															<div class="pull-left search-count-content">
																Showing {{$data_array['from']}} - {{$data_array['to']}} of {{$data_array['total']}} Results
															</div>
														</div>
													</div>
												</div>
											@endif
										@else
											<div class="no-results-found">No results found. Try again with different search criteria.</div>
										@endif
									</div>
								</div>
							</div>
						</div>
						<!-- end: PAGE CONTENT-->
					</div>
					<div class="subviews">
						<div class="subviews-container"></div>
					</div>
				</div>
				<!-- end: PAGE -->
			</div>
</div>

	<script type="text/javascript">
        
        var searchAjaxCall = "undefined";

        $(document).ready(function(){

            //init history plugin to perform search without page refresh
            // Establish Variables
            History = window.History; // Note: We are using a capital H instead of a lower h

            // Bind to State Change
            // Note: We are using statechange instead of popstate
			
            History.Adapter.bind(window,'statechange', function() {

                // Log the State
                var State = History.getState(); // Note: We are using History.getState() instead of event.state

                //start search (defined)
                fetchSearchResults(State.url, 
                                    function() {
                                        
                                    }, function() {

                                    });

            });

            

        });

        function fetchSearchResults(url, successCallback, errorCallback)
	    {
	    	
	    	var temp_current_state_url = url.split('?');
            if(temp_current_state_url.length > 1 && temp_current_state_url[1] != ''){
                var parameters = temp_current_state_url[1].split('&');

                var dropdown_elements = ['bussiness_category','bi_member'];

                for(var i=0; i<parameters.length; i++){

                    var param_array = parameters[i].split('=');
                    if($.inArray(param_array[0],dropdown_elements) > -1){
                        $('select[name="'+param_array[0]+'"]').val(param_array[1]);
                    }else if($.inArray(param_array[0],['first_name','company_name']) > -1){
                        $('input[name="'+param_array[0]+'"]').val(param_array[1]);
                    }

                }
            } else {
             	$(':input','.filter-search-form')
	                .not(':button, :submit, :reset, :hidden')
	                .val('')
	                .removeAttr('checked')
	                .removeAttr('selected');
	             }

	        if(searchAjaxCall != "undefined")
	        {
	            searchAjaxCall.abort();
	            searchAjaxCall = "undefined";
	        }

	        $('.list-container').addClass('hide');
        	$('.full-cnt-loader').removeClass('hidden');

	        searchAjaxCall = $.ajax({
	            url: url,
	            cache: false,
	            dataType: 'json',
	            statusCode: {
	                200: function(response) {
	                    if (typeof response.template != 'undefined') {
	                       
	                        $('.list-container').removeClass('hide');
                        	$('.full-cnt-loader').addClass('hidden');
                        	
							$('.search-results-main-container').html(response.template);

	                        if(typeof successCallback != "undefined")
	                        {
	                            successCallback();
	                        }

	                    }
	                }
	            },
	            error: function(error, response) {

	                if(typeof errorCallback != "undefined")
	                {
	                    errorCallback();
	                }
	            }
	        });
	    }
    </script>

@stop
@section('js_script')
    <script type="text/javascript" src="/js/site/advertise-registration.js"></script>
@stop