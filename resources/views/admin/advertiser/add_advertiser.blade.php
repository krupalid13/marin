@extends('admin.index')

@section('content')
<?php
    $initialize_pincode_maxlength_validation = false;
    $india_value = array_search('India',\CommonHelper::countries());
?>
    <div class="container">
        <div class="toolbar row">
            <div class="col-sm-12">
                <div class="page-header">
                    <h1>Advertiser<small>Add Advertiser</small></h1>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 m-t-25">
                <!-- start: FORM WIZARD PANEL -->
                <div class="panel panel-white">
                    <div class="panel-body">
                        <input type="hidden" name="role" id="role" value="admin">
                        
                            <div id="" class="swMain">
                                <div id="step-1">

                                <form id="advertise-login-form" class="smart-wizard form-horizontal" method="post">
                                        {{ csrf_field() }}
                                        <div class="panel-heading">
                                            <h4 class="panel-title">ACCOUNT CREDENTIALS</h4>
                                        </div>
                                        <div class="form-group">
                                            <input type="hidden" name="image-x" required>
                                            <input type="hidden" name="image-y" required>
                                            <input type="hidden" name="image-x2" required>
                                            <input type="hidden" name="image-y2" required>
                                            <input type="hidden" name="crop-w" required>
                                            <input type="hidden" name="crop-h" required>
                                            <input type="hidden" name="image-w" required>
                                            <input type="hidden" name="image-h" required>
                                            <input type="hidden" name="uploaded-file-name" required>
                                            <input type="hidden" name="uploaded-file-path" required>
                                            <input type="hidden" id="advertise_id" required value="{{isset($user_data['user'][0]['id']) ? $user_data['user'][0]['id'] : ''}}">

                                            <div class="row no-margin">
                                                <div class="col-sm-7 col-sm-offset-3">
                                                    <div class="upload-photo-container">
                                                        <div class="image-content">
                                                            <div class="registration-profile-image" style="height: 100%;width: 100%;">
                                                                <?php
                                                                $image_path = '';
                                                                if(isset($user_data['user'][0]['profile_pic'])){
                                                                    $image_path = "/".env('SEAFARER_PROFILE_PATH')."".$user_data['user'][0]['id']."/".$user_data['user'][0]['profile_pic'];
                                                                }
                                                                ?>
                                                                <!-- <div class="row text-center" style="display: flex;justify-content: center;">
                                                                    <div class="col-lg-3 col-md-5 col-sm-5 company_profile_pic" style="border-radius: 50%;">
                                                                        <img class="seafarer_profile_pic" src="/{{ isset($image_path) ? $image_path : 'images/user_default_image.png'}}" >
                                                                    </div>
                                                                </div> -->

                                                                @if(empty($image_path))
                                                                    <div class="icon profile_pic_text p-t-30"><i class="fa fa-camera" aria-hidden="true"></i></div>
                                                                    <div class="image-text profile_pic_text">Upload Profile <br> Picture</div>
                                                                @endif

                                                                <input type="file" style="z-index: 9;" name="profile_pic" id="profile-pic" class="cursor-pointer" onChange="profilePicSelectHandler('advertise-login-form', 'profile_pic', 'pic', 'seafarer')">

                                                                @if(!empty($image_path))
                                                                    <img id="preview" style="border-radius: 50%;width: 100%;" src="{{ $image_path }}">
                                                                @else
                                                                    <img id="preview" style="border-radius: 50%;">
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                            </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">
                                                Owner Name <span class="symbol required"></span>
                                            </label>
                                            <div class="col-sm-7">
                                                <input type="text" class="form-control" id="firstname" name="firstname" value="{{ isset($user_data['user'][0]['first_name']) ? $user_data['user'][0]['first_name'] : ''}}" placeholder="Type your full name">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">
                                                Owner Mobile Number <span class="symbol required"></span>
                                            </label>
                                            <div class="col-sm-7">
                                                <input type="text" class="form-control" id="mobile" name="mobile" value="{{ isset($user_data['user'][0]['mobile']) ? $user_data['user'][0]['mobile'] : ''}}" placeholder="Type your mobile number">
                                                <span class="hide" id="mobile-error" style="color: red"></span>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">
                                                Owner Email Address <span class="symbol required"></span>
                                            </label>
                                            <div class="col-sm-7">
                                                <input type="text" class="form-control" id="email" name="email" placeholder="Type your email address"
                                                       value="{{ isset($user_data['user'][0]['email']) ? $user_data['user'][0]['email'] : ''}}">
                                                <span class="hide" id="email-error" style="color: red"></span>
                                            </div>
                                        </div>

                                        @if(!isset($user_data['current_route']))
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">
                                                    Password <span class="symbol required"></span>
                                                </label>
                                                <div class="col-sm-7">
                                                    <input type="password" class="form-control" id="password" name="password" placeholder="Type your password">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">
                                                    Confirm Password <span class="symbol required"></span>
                                                </label>
                                                <div class="col-sm-7">
                                                    <input type="password" class="form-control" id="cpassword" name="cpassword" placeholder="Type password again">
                                                </div>
                                            </div>
                                        @else
                                            <input type="hidden" id='user_id' name='user_id' value='{{$user_data['user'][0]['id']}}'>
                                        @endif
                                </form>

                                <div class="panel-heading">
                                    <h4 class="panel-title">COMPANY DETAILS</h4>
                                </div>

                                <form id="advertise-company-form" class="smart-wizard form-horizontal" method="POST">
                                    <div class="form-group company-registration">
                                        <input type="hidden" name="image-x" required>
                                        <input type="hidden" name="image-y" required>
                                        <input type="hidden" name="image-x2" required>
                                        <input type="hidden" name="image-y2" required>
                                        <input type="hidden" name="crop-w" required>
                                        <input type="hidden" name="crop-h" required>
                                        <input type="hidden" name="image-w" required>
                                        <input type="hidden" name="image-h" required>
                                        <input type="hidden" name="role" value='advertiser'>
                                        <input type="hidden" name="uploaded-file-name" required>
                                        <input type="hidden" name="uploaded-file-path" required>

                                        <div id="company-details-form">
                                            <div class="row no-margin">
                                                <div class="col-sm-7 col-sm-offset-3">
                                                    <div class="user-details-container">
                                                        <div class="upload-photo-container">
                                                        {{ csrf_field() }}
                                                            <div class="image-content">
                                                                <div class="registration-profile-image">
                                                                    <?php
                                                                    $image_path = '';
                                                                    if(isset($user_data['company_data'][0]['company_registration_detail']['company_logo'])){
                                                                        $image_path = env('COMPANY_LOGO_PATH')."".$user_data['company_data'][0]['id']."/".$user_data['company_data'][0]['company_registration_detail']['company_logo'];
                                                                    }
                                                                    ?>
                                                                    @if(empty($image_path))
                                                                        <div class="icon profile_pic_text"><i class="fa fa-camera" aria-hidden="true"></i></div>
                                                                        <div class="image-text profile_pic_text">Upload Logo <br> Picture</div>
                                                                    @endif

                                                                    <input type="file" name="profile_pic" id="profile-pic" class="cursor-pointer" onChange="profilePicSelectHandler('advertise-company-form', 'profile_pic', 'logo' , 'company')">

                                                                    @if(!empty($image_path))
                                                                        <img id="preview" class="preview" src="/{{ $image_path }}">
                                                                    @else
                                                                        <img id="preview">
                                                                    @endif
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">
                                                Company Name <span class="symbol required"></span>
                                            </label>
                                            <div class="col-sm-7">
                                                <input type="text" class="form-control" id="company_name" name="company_name" placeholder="Type company name" value="{{ isset($user_data['company_data'][0]['company_registration_detail']['company_name']) ? $user_data['company_data'][0]['company_registration_detail']['company_name'] : ''}}">
                                            </div>
                                        </div>
                                        
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">
                                                Company Website
                                            </label>
                                            <div class="col-sm-7">
                                                <div class="input-group">
                                                    <span class="input-group-addon">http://</span>
                                                        <input type="text" class="form-control" id="website" name="website" placeholder="Type your website address" value="{{ isset($user_data['company_data'][0]['company_registration_detail']['website']) ? $user_data['company_data'][0]['company_registration_detail']['website'] : ''}}"></span>
                                                </div>
                                                <label for="website" class="error"></label>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">
                                                Country <span class="symbol required"></span>
                                            </label>
                                            <div class="col-sm-7">
                                                <select id='country' name="country[0]" class="form-control country" block-index="0">
                                                <option value=''>Select Your Country</option>
                                                @foreach( \CommonHelper::countries() as $c_index => $country)
                                                    @if(isset($user_data['company_data'][0]['company_registration_detail']['company_locations'][0]['country']))
                                                        <option value="{{ $c_index }}" {{ isset($user_data['company_data'][0]['company_registration_detail']['company_locations'][0]['country']) ? $user_data['company_data'][0]['company_registration_detail']['company_locations'][0]['country'] == $c_index ? 'selected' : '' : ''}}> {{ $country }}</option>
                                                    @else
                                                        <option value="{{ $c_index }}" {{ $india_value == $c_index ? 'selected' : ''}}> {{ $country }}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                            </div>
                                        </div>
                                        
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">
                                            Postal Code <span class="symbol required"></span>
                                        </label>
                                        <div class="col-sm-7 pincode-block-0">
                                            <input type="hidden" class="pincode-id" name="pincode_id[0]" value="{{ isset($user_data['company_data'][0]['company_registration_detail']['company_locations'][0]['pincode_id']) ? $user_data['company_data'][0]['company_registration_detail']['company_locations'][0]['pincode_id'] : ''}}">
                                            
                                            <i class="fa fa-spin fa-refresh select-loader hide pincode-loader-0"></i>
                                            <input type="text" block-index="0" id='pincode' data-form-id="advertise-company-form" class="form-control pincode pin_code_fetch_input" name="pincode[0]" value="{{ isset($user_data['company_data'][0]['company_registration_detail']['company_locations'][0]['pincode_text']) ? $user_data['company_data'][0]['company_registration_detail']['company_locations'][0]['pincode_text'] : ''}}" placeholder="Type your pincode">
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">
                                            State <span class="symbol required"></span>
                                        </label>
                                        <div class="col-sm-7 state-block state-block-0">
                                            @if(isset($user_data['company_data'][0]['company_registration_detail']['company_locations'][0]))
                                            @if($user_data['company_data'][0]['company_registration_detail']['company_locations'][0]['country'] == $india_value )
                                                <select id="state" name="state[0]" class="form-control state fields-for-india">
                                                    <option value="">Select Your State</option>
                                                    @foreach($user_data['company_data'][0]['company_registration_detail']['company_locations'][0]['pincode']['pincodes_states'] as $pincode_states)
                                                        <option value="{{$pincode_states['state_id']}}" {{$pincode_states['state_id'] == $user_data['company_data'][0]['company_registration_detail']['company_locations'][0]['state_id'] ? 'selected' : ''}}>{{ucwords(strtolower($pincode_states['state']['name']))}}</option>
                                                    @endforeach
                                                </select>
                                                <input type="text" id="state" name="state_name[0]" class="form-control state_text hide fields-not-for-india" placeholder="Enter State Name" value="">

                                            @else
                                                <select id="state" name="state[0]" class="form-control state hide fields-for-india">
                                                    <option value="">Select Your State</option>
                                                </select>
                                                <input type="text" id="state" name="state_name[0]" class="form-control state_text fields-not-for-india" placeholder="Enter State Name" value="{{ isset($user_data['company_data'][0]['company_registration_detail']['company_locations'][0]['state_text']) ? $user_data['company_data'][0]['company_registration_detail']['company_locations'][0]['state_text'] : ''}}">
                                            @endif
                                        @else
                                            <select id="state" name="state[0]" class="form-control state fields-for-india">
                                                <option value="">Select Your State</option>
                                            </select>
                                            <input type="text" id="state" name="state_name[0]" class="form-control state_text hide fields-not-for-india" placeholder="Enter State Name" value="{{ isset($user_data['company_data'][0]['company_registration_detail']['company_locations'][0]['state_text']) ? $user_data['company_data'][0]['company_registration_detail']['company_locations'][0]['state_text'] : ''}}">
                                        @endif
                                        </div>
                                    </div>
                                     <div class="form-group">
                                    
                                        <label class="col-sm-3 control-label">
                                            City <span class="symbol required"></span>
                                        </label>
                                        <div class="col-sm-7 city-block city-block-0">
                                            @if(isset($user_data['company_data'][0]['company_registration_detail']['company_locations'][0]['country']))
                                            @if( $user_data['company_data'][0]['company_registration_detail']['company_locations'][0]['country'] == $india_value )
                                                <select id="city" name="city[0]" class="form-control city fields-for-india">
                                                    <option value="">Select Your City</option>
                                                    @foreach($user_data['company_data'][0]['company_registration_detail']['company_locations'][0]['pincode']['pincodes_cities'] as $pincode_city)
                                                        <option value="{{$pincode_city['city_id']}}" {{$pincode_city['city_id'] == $user_data['company_data'][0]['company_registration_detail']['company_locations'][0]['city_id'] ? 'selected' : ''}}>{{ucwords(strtolower($pincode_city['city']['name']))}}</option>
                                                    @endforeach
                                                </select>
                                                <input type="text" name="city_text[0]" class="form-control city_text hide fields-not-for-india" placeholder="Enter City Name" value="">

                                            @else
                                                <select id="city" name="city[0]" class="form-control city hide fields-for-india">
                                                    <option value="">Select Your City</option>
                                                </select>

                                                {{--<input type="text" name="city_text[0]" class="form-control city_text fields-not-for-india" placeholder="Enter City Name" value="">--}}
                                                <input type="text" name="city_text[0]" class="form-control city_text fields-not-for-india" placeholder="Enter City Name" value="{{ isset($user_data['company_data'][0]['company_registration_detail']['company_locations'][0]['city_text']) ? $user_data['company_data'][0]['company_registration_detail']['company_locations'][0]['city_text'] : ''}}">
                                            @endif
                                        @else
                                            <select id="city" name="city[0]" class="form-control city fields-for-india">
                                                <option value="">Select Your City</option>
                                            </select>

                                            {{--<input type="text" name="city_text[0]" class="form-control city_text fields-not-for-india" placeholder="Enter City Name" value="">--}}
                                            <input type="text" name="city_text[0]" class="form-control city_text hide fields-not-for-india" placeholder="Enter City Name" value="{{ isset($user_data['company_data'][0]['company_registration_detail']['company_locations'][0]['city_text']) ? $user_data['company_data'][0]['company_registration_detail']['company_locations'][0]['city_text'] : ''}}">
                                        @endif
                                        </div>
                                    </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">
                                                Company Address <span class="symbol required"></span>
                                            </label>
                                            <div class="col-sm-7">
                                                <textarea type="text" class="form-control" id="company_address" name="company_address" placeholder="Type your company address" rows="5">{{ isset($user_data['company_data'][0]['company_registration_detail']['address']) ? $user_data['company_data'][0]['company_registration_detail']['address'] : ''}}</textarea>
                                            </div>
                                        </div>

                                        <div class="panel-heading">
                                            <h4 class="panel-title">BUSSINESS DETAILS</h4>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">
                                                Member of any club or association? <span class="symbol required"></span>
                                            </label>
                                            <div class="col-sm-7">
                                                <select name="b_i_member" class="b_i_member form-control">
                                                    <option value="">Select</option>
                                                    @foreach( \CommonHelper::member_category() as $c_index => $category)
                                                        <option value="{{ $c_index }}" {{ isset($user_data['company_data'][0]['company_registration_detail']['b_i_member']) ? $user_data['company_data'][0]['company_registration_detail']['b_i_member'] == $c_index ? 'selected' : '' : ''}}> {{ $category }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                        <?php 
                                        
                                        $show_b_i_member = 'hide';
                                        if(isset($user_data['company_data'][0]['company_registration_detail']['b_i_member']) && $user_data['company_data'][0]['company_registration_detail']['b_i_member'] == 'other'){
                                                $show_b_i_member = '';
                                            }
                                            else{
                                                $show_b_i_member = 'hide';
                                            }
                                        ?>

                                        <div class="form-group {{$show_b_i_member}}" id="other_b_i_member">
                                            <label class="col-sm-3 control-label">
                                                Other Member <span class="symbol required"></span>
                                            </label>
                                            <div class="col-sm-7">
                                                <input type="text"  class="form-control" name="other_b_i_member" value="{{ isset($user_data['company_data'][0]['company_registration_detail']['other_bi_member']) ? $user_data['company_data'][0]['company_registration_detail']['other_bi_member'] : '' }}">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">
                                                Bussiness Category <span class="symbol required"></span>
                                            </label>
                                            <div class="col-sm-7">
                                                <select name="bussiness_category" class="bussiness_category form-control">
                                                    <option value="">Select Category</option>
                                                    @foreach(\CommonHelper::bussiness_category() as $index => $categories)
                                                        <optgroup label="{{$index}}">
                                                            @foreach($categories as $c_index => $category)
                                                                <option value="{{ $c_index }}" {{ isset($user_data['company_data'][0]['company_registration_detail']['bussiness_category']) ? $user_data['company_data'][0]['company_registration_detail']['bussiness_category'] == $c_index ? 'selected' : '' : ''}}> {{ $category }}</option>
                                                            @endforeach
                                                    @endforeach
                                                    </select>
                                                </select>
                                            </div>
                                        </div>

                                        <?php 
                                        
                                        $show_other = 'hide';
                                        if(isset($user_data['company_data'][0]['company_registration_detail']['bussiness_category']) && $user_data['company_data'][0]['company_registration_detail']['bussiness_category'] == 'other'){
                                                $show_other = '';
                                            }
                                            else{
                                                $show_other = 'hide';
                                            }
                                        ?>
                                        <div class="form-group {{$show_other}}" id="other_bussiness_category">
                                            <label class="col-sm-3 control-label">
                                                Other Bussiness Category <span class="symbol required"></span>
                                            </label>
                                            <div class="col-sm-7">
                                                <input type="text"  class="form-control" name="other_buss_category" value="{{ isset($user_data['company_data'][0]['company_registration_detail']['other_buss_category']) ? $user_data['company_data'][0]['company_registration_detail']['other_buss_category'] : '' }}">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">
                                                Years In Bussiness <span class="symbol required"></span>
                                            </label>
                                            <div class="col-sm-7">
                                                <input type="text" class="form-control" name="buss_years" placeholder="Type your years in bussiness in years" value="{{ isset($user_data['company_data'][0]['company_registration_detail']['buss_years']) ? $user_data['company_data'][0]['company_registration_detail']['buss_years'] : ''}}">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">
                                                Product/Service Description <span class="symbol required"></span>
                                            </label>
                                            <div class="col-sm-7">
                                                <textarea class="form-control" name="product_description" placeholder="Type your product/service description" rows="5">{{ isset($user_data['company_data'][0]['company_registration_detail']['product_description']) ? $user_data['company_data'][0]['company_registration_detail']['product_description'] : ''}}</textarea>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">
                                                Bussiness Description <span class="symbol required"></span>
                                            </label>
                                            <div class="col-sm-7">
                                                <textarea class="form-control bussiness_description" name="bussiness_description" placeholder="Type your bussiness description" rows="5">{{ isset($user_data['company_data'][0]['company_registration_detail']['bussiness_description']) ? $user_data['company_data'][0]['company_registration_detail']['bussiness_description'] : ''}}</textarea>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">
                                                Email To Be Displayed <span class="symbol required"></span>
                                            </label>
                                            <div class="col-sm-7">
                                                <input type="text" class="form-control" name="email_display" placeholder="Type your email to be displayed" value="{{ isset($user_data['company_data'][0]['company_registration_detail']['email_display']) ? $user_data['company_data'][0]['company_registration_detail']['email_display'] : ''}}">
                                            </div>
                                        </div>

                                         <div class="form-group">
                                            <label class="col-sm-3 control-label">
                                                Contact Number To Be Displayed<span class="symbol required"></span>
                                            </label>
                                            <div class="col-sm-7">
                                                <input type="text" class="form-control" name="contact_display" placeholder="Type your contact number to be displayed" value="{{ isset($user_data['company_data'][0]['company_registration_detail']['contact_display']) ? $user_data['company_data'][0]['company_registration_detail']['contact_display'] : ''}}">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-2 col-sm-offset-10">
                                                <button type="button" data-style="zoom-in" class="btn btn-blue ladda-button btn-block advertise-details-submit" id="submitAdvertiserDetailButton">
                                                    Save <i class="fa fa-arrow-circle-right"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop
@section('js_script')
    <script type="text/javascript" src="/js/site/advertise-registration.js"></script>
    <script>
        jQuery(document).ready(function() {
            SeafarerFormWizard.init();
        });
    </script>
@stop