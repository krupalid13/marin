@extends('admin.index')

@section('content')

    <div class="container advertise-container">
        <div class="toolbar row">
            <div class="col-sm-12">
                <div class="page-header">
                    <h1>Advertise<small>Add Advertisements</small></h1>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 m-t-25">
                <!-- start: FORM WIZARD PANEL -->
                <div class="panel panel-white">
                    <div class="panel-body">
                        <input type="hidden" name="role" id="role" value="admin">
                        
                            <div id="" class="swMain">
                                <div id="step-1">

                                <form role="form" class="form-horizontal" id='add-advertise-form' action="{{route('admin.advertiser.store')}}">
                                        {{ csrf_field() }}
                                        <div class="form-group">
                                            <div class="row no-margin">
                                                <div class="col-sm-6">
                                                    <label class="control-label">
                                                        Upload Small Advertisement
                                                    </label>
                                                    <br>
                                                    <div class="advertise_upload fileupload fileupload-new" data-provides="fileupload">
                                                        <div class="fileupload-new thumbnail">
                                                            <img src="{{ asset('images/no-image-advertisements.png') }}" alt=""/>
                                                        </div>
                                                        <div class="fileupload-preview fileupload-exists thumbnail" id='advertise_img'></div>
                                                        <span class="help-block" style="color: #737373;"><i class="fa fa-info-circle"></i> Note: Please upload 400px w * 400px h size image.<br> Image size should be less than 250Kb.</span>
                                                        <div>
                                                            <label for="" class="error"></label>
                                                        </div>
                                                        <div>
                                                            <span class="btn btn-light-grey btn-file">
                                                                <span class="fileupload-new">
                                                                    <i class="fa fa-picture-o"></i> Select image</span>
                                                                <span class="fileupload-exists">
                                                                    <i class="fa fa-picture-o"></i> Change</span>
                                                                <input type="file" id="advertise-file-uplaod" name="advertise_upload">
                                                            </span>
                                                            <a href="#" class="btn fileupload-exists btn-light-grey remove_advertise_btn" data-dismiss="fileupload">
                                                                <i class="fa fa-times"></i> Remove
                                                            </a>
                                                        </div>
                                                        <label for="advertise-file-uplaod" class="error" id="advertise_upload_error"></label>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <label>
                                                        Upload Big Advertisement
                                                    </label>
                                                    <div class="advertise_full_img_upload fileupload fileupload-new" data-provides="fileupload">
                                                        <div class="fileupload-new thumbnail">
                                                            <img src="{{ asset('images/no-image-advertisements.png') }}" alt=""/>
                                                        </div>
                                                        <div class="fileupload-preview fileupload-exists thumbnail" id='advertise_full_img'></div>
                                                        <span class="help-block" style="color: #737373;">
                                                            <i class="fa fa-info-circle"></i> Note: Image size should be less than 1Mb.
                                                        </span>
                                                        <div>
                                                            <label for="" class="error"></label>
                                                        </div>
                                                        <div>
                                                            <span class="btn btn-light-grey btn-file">
                                                                <span class="fileupload-new">
                                                                    <i class="fa fa-picture-o"></i> Select image</span>
                                                                <span class="fileupload-exists">
                                                                    <i class="fa fa-picture-o"></i> Change</span>
                                                                <input type="file" id="advertise-full-image-upload" name="full_img">
                                                            </span>
                                                            <a href="#" class="btn fileupload-exists btn-light-grey remove_full_advertise_btn" data-dismiss="fileupload">
                                                                <i class="fa fa-times"></i> Remove
                                                            </a>
                                                        </div>
                                                        <label for="advertise-full-image-upload" class="error" id="advertise_full_img_upload_error"></label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                                <div class="col-sm-6">
                                                    <label class="input-label">Advertiser<span class="symbol required"></span></label>
                                                    <select name="company_id" class="form-control" data-index="0">
                                                        <option value=''>Select advertiser</option>
                                                            @foreach($advertiser_list as $a_index => $advertiser)
                                                                <option value="{{$advertiser['id']}}">
                                                                    {{$advertiser['first_name']}} 
                                                                </option>
                                                            @endforeach
                                                    </select>
                                                </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row no-margin">
                                                <div class="col-sm-6">
                                                    <label class="input-label">Advertise Display From Date<span class="symbol required"></span></label>
                                                    <input type="text" class="form-control datepicker" id="adv_from_date" name="adv_from_date" value="" placeholder="dd-mm-yyyy">
                                                </div>
                                                <div class="col-sm-6">
                                                    <label class="input-label" for="adv_to_date">Advertise Display To Date<span class="symbol required"></span></label>
                                                    <input type="text" class="form-control datepicker" id="adv_to_date" name="adv_to_date" value="" placeholder="dd-mm-yyyy">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row no-margin adv-template">
                                                <div class="col-sm-12 state_count">
                                                    <div class="col-sm-3 no-margin" style="padding-left: 0px;">
                                                        <label class="input-label">State<span class="symbol required"></span></label>
                                                        <select name="state[0]" class="form-control state advertisement-state" data-index="0" style="margin-top: 4px;">
                                                            <option value=>Select a state</option>
                                                                @foreach($state as $s_index => $state_list)
                                                                    <option value="{{$state_list['id']}}" {{ isset($data['company_type']) ? $data['company_type'] == $s_index ? 'selected' : '' : ''}}>{{$state_list['name']}} 
                                                                    </option>
                                                                @endforeach
                                                        </select>
                                                        <label class="state_error hide">Please select state</label>
                                                    </div>
                                                    <div class="col-sm-3" style="padding-left: 0px;">
                                                        <label class="input-label" for="passdateofexp">City<span class="symbol required"></span>
                                                        </label>
                                                        <select multiple="multiple" name="city[0]"  class="form-control city select2-select" style="height: auto;padding: 0px;" placeholder="Please select city">
                                                            
                                                        </select>
                                                        <label class="city_error hide">Please select city</label>
                                                    </div>
                                                    <div class="col-sm-6 add-more-state">
                                                        <button type="button" data-form-id="add-advertise-form" class="btn add-more add-state-button" id="add-more-state-0" style="margin-top: 25px !important;">Add More</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row no-margin" id="add-more-state-row">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row no-margin">
                                                <div class="col-sm-2 col-sm-offset-10">
                                                    <button type="button" data-style="zoom-in" class="btn btn-blue btn-block advertise-details-submit ladda-button" id="submitAdvertiseDetailButton">
                                                        Save <i class="fa fa-arrow-circle-right"></i>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                </form>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="adv-template hide" id="advertise-state-template">
        <div class="col-sm-12" style="margin-bottom: 15px;">
            <div class="col-sm-3" style="padding-left: 0px;margin-top: 4px !important;">
                <label class="input-label">State<span class="symbol required"></span></label>
                <select class="form-control state advertisement-state">
                    <option value="">Select a state</option>
                </select>
            </div>
            <div class="col-sm-3" style="padding-left: 0px;">
                <div class="pull-right adv-close-button adv-close-button-0">
                    <i class="fa fa-times" aria-hidden="true">
                        
                    </i>
                </div>
                <label class="input-label" for="passdateofexp">City
                    <span class="symbol required">
                    </span>
                 </label>
               
                <select multiple="multiple" class="form-control city" style="height: auto;padding: 0px;" >
                    
                </select>
            </div>
            <div class="col-sm-6 add-more-state">
                <button type="button" data-form-id="add-advertise-form" class="btn add-more add-state-button" style="margin-top: 25px;">Add More</button>
            </div>
        </div>
    </div>
@stop
<script type="text/javascript">
    var state = <?php echo json_encode($state) ?>;
</script>
@section('js_script')
    <script type="text/javascript" src="/js/site/advertise-registration.js"></script>
    <script type="text/javascript">
      $('.select2-select').select2({
         closeOnSelect: false
      });
    </script>
@stop