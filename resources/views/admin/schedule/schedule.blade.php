@extends('admin.index')

@section('content')

<div class="main-wrapper">
	<div class="main-container1">
				<!-- start: PAGE -->
		<div class="main-content">
			<div class="container">
				<!-- start: PAGE HEADER -->
				<!-- start: TOOLBAR -->
				<div class="toolbar row">
					<div class="col-sm-6">
						<div class="page-header">
							<h1>Scheduler<small>schedule email and sms</small></h1>
						</div>
					</div>
				</div>

				@if (count($errors) > 0)
				    <div class="alert alert-danger m-t-15">
				        <button data-dismiss="alert" class="close">
				            ×
				        </button>
				        <strong>
				        Some error occured <br>
				        </strong>
				            @foreach ($errors->all() as $error)
				                {{ $error }} </br>
				            @endforeach
				          <!--  Contents cannot be empty. -->
				    </div>
				@endif

				@if (session('status'))
					@if(session('status') == 'success')
				    <div class="alert alert-success m-t-15">
				        Schedule saved successfully
				    </div>
				    @elseif(session('status') == 'error')
					<div class="alert alert-info m-t-15">
				        Some error occured, try again later
				    </div>
				    @endif
				@endif

				<form id="add-schedule-form" action="{{route('admin.schedule.add')}}" method="POST">
					<div class="row m_t_25">
						<div class="col-md-12">
							<div class="panel panel-white">
								<div class="panel-body">
								{{ csrf_field() }}
									<div class="row">
										<div class="col-xs-12">
			                                <div class="form-group">
			                                    <h5 class=""> Select Email <!-- / SMS --> </h5>
			                                    <label class="checkbox-inline">
													<input type="checkbox" name="schedule_type[0]" class="flat-green" value="email" checked="checked">
													Email
												</label>
												<!-- <label class="checkbox-inline">
													<input type="checkbox" name="schedule_type[1]" class="flat-green" value="sms">
													SMS
												</label> -->
			                                </div>
			                            </div>
			                            <div class="col-xs-12">
			                                <div class="form-group">
			                                    <h5 class=""> Select user type </h5>
												<label class="radio-inline">
													<input type="radio" class="flat-green" value="seafarer" name="user_type" checked="checked">
													Seafarer
												</label>
												<label class="radio-inline">
													<input type="radio" class="flat-green" value="advertiser" name="user_type">
													Advertiser
												</label>
												<label class="radio-inline">
													<input type="radio" class="flat-green" value="company" name="user_type">
													Company
												</label>
												<label class="radio-inline">
													<input type="radio" class="flat-green" value="institute" name="user_type">
													Institute
												</label>
			                                </div>
			                            </div>
			                            <div class="col-xs-12 col-sm-6">
			                            	<div class="form-group">
			                            	<h5 class=""> Date </h5>
			                                    <div class="input-group">
													<input type="text" name="date" data-date-format="dd-mm-yyyy" data-date-viewmode="years" class="form-control datepicker" placeholder="dd-mm-yyyy">
													<span class="input-group-addon"> <i class="fa fa-calendar"></i> </span>
												</div>
			                                </div>
			                            </div>
			                            <div class="col-xs-12 col-sm-6">
			                            	<div class="form-group">
			                            	<h5 class=""> Time </h5>
			                                    <div class="input-group input-append bootstrap-timepicker">
													<input type="text" name="time" class="form-control time-picker">
													<span class="input-group-addon add-on"><i class="fa fa-clock-o"></i></span>
												</div>
			                                </div>
			                            </div>
									</div>
									
									<div class="row">
										<div class="col-xs-12 col-sm-6 p-t-15 m-b-15">
											<div class="country-location-container">
								                <h4 class=""> Select Location</h4>
								               	<div class="form-group">
								                    <h5>Select country</h5>
								                    <select name="country" id="email-country" class="form-control">
														@foreach(\CommonHelper::countries() as $cou_index => $country)
															<option value="{{$cou_index}}" {{$cou_index == 95 ? 'selected' : ''}}>{{$country}}</option>
														@endforeach
													</select>
								                </div>
					                		</div>
										</div>
									</div>

									<div class="row state-city-container">
										<div class="col-xs-12 col-sm-6 p-t-15 m-b-15">
							                <h5 class="space15"> Select state</h5>
								            <div id="state-required-block" class="hide form-error">Please select at least one state</div>

							                <div class="state-location-container">
							                	<div class="state-select"> 
													<div class="checkbox">
														<label> 
													 		<input type="checkbox" class="grey select-all-state"> Select All 
														</label> 
													</div>
												</div>
												@foreach($states as $state_index => $state)
												@if( count($state['cities']) > 0 )
													<div class="state-select">
														<div class="checkbox"> 
															<label> 
																<input type="checkbox" value="{{$state['id']}}" name="states[{{$state_index}}]" class="grey state-checkbox email-location-checkbox"> {{ucwords(strtolower($state['name']))}} 
															</label> 
														</div> 
													</div>
												@endif
												@endforeach
											</div>
										</div>
									
										<div class="col-xs-12 col-sm-6 p-t-15 m-b-15">
							                <h5 class="space15"> Select City</h5>
								            <div id="city-required-block" class="hide form-error">Please select at least one city</div>

							                <div class="city-location-container">
							                <?php 
							                	$city_count = 0;
							                 ?>
												@foreach($states as $state)
													@if( count($state['cities']) > 0 )
														<div id="state-id-{{$state['id']}}" class="city-select">
															<div class="city-heading">{{ucwords(strtolower($state['name']))}}</div>
															@foreach($state['cities'] as $index => $city)
																<div class="checkbox"> 
																	<label> 
																		<input type="checkbox" name="cities[{{$city_count}}]" value="{{$city['id']}}" class="grey city-checkbox email-location-checkbox" data-name="{{$city['name']}}"> {{$city['name']}}
																	</label> 
																</div> 
																<?php 
																	$city_count++;
																 ?>
															@endforeach
														</div>
													@endif
												@endforeach
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-12">
			                            	<div class="form-group">
												<h5 class=""> Subject for email </h5>
												<input type="text" class="form-control" name="email_subject" placeholder="Enter email subject">
											</div>
			                            </div>
									</div>
									<div class="row">
										<div class="col-xs-12">
			                            	<div class="form-group">
												<h5 class=""> Message for email </h5>
												<textarea class="form-control" name="msg_email" placeholder="Enter email message" rows="5"></textarea>
											</div>
			                            </div>
			                            <!-- <div class="col-xs-12">
			                            	<div class="form-group">
												<h5 class=""> Message for sms </h5>
												<textarea maxlength="160" name="msg_sms" class="form-control limited" rows="5" placeholder="Enter sms message"></textarea>
											</div>
			                            </div> -->
									</div>
									<div class="row">
										<div class="col-xs-12 text-center">
											<button type="submit" class="btn btn-success">
												Schedule Save
											</button>
										</div>
									</div>	
								</div>
							</div>
						</div>
					</div>
				</form>

				<!-- end: PAGE CONTENT-->
			</div>
			<div class="subviews">
				<div class="subviews-container"></div>
			</div>
		</div>
			<!-- end: PAGE -->
	</div>
</div>


@stop

@section('js_script')


@stop