@extends('admin.index')

@section('content')

	<div class="container">
		<!-- start: TOOLBAR -->
		<div class="toolbar row">
			<div class="col-sm-6 hidden-xs">
				<div class="page-header">
					<h1>Scheduled email <!-- & sms --><small>View All Scheduled email <!-- & sms --></small></h1>
				</div>
			</div>
			<div class="col-sm-6 col-xs-12">
				
				<div class="toolbar-tools pull-right">
					<!-- start: TOP NAVIGATION MENU -->
					
					<!-- end: TOP NAVIGATION MENU -->
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-xs-12 m-t-25">
				<div class="panel panel-white">
					<div class="panel-heading p-b-0">
						<div class="row">
							<div class="col-xs-12">
								<?php 
									$total_result = $schedules['current_page'] * $schedules['per_page'];
								?>
								<div class="pull-left search-count-content {{$schedules['total'] > 0 ? '' : 'hide'}}">
									Showing: <span class="fetched-count">{{$total_result > $schedules['total'] ? $schedules['total'] : $total_result}}</span> 
											out of 
											<span class="total-data-count">{{$schedules['total']}}</span> schedule(s)
								</div>
								<div>
								 {{ $paginate->links() }}
								</div>
							</div>
						</div>
					</div>
					<div id="schedules-listing-container" class="panel-body make-position-relative">
						<div class="inner-page-overlay hide">
							<i class="fa fa-spin fa-refresh"></i>
						</div>

						@if($schedules['total'] > 0)
							<div class="table-responsive">
								<table class="table table-bordered table-hover width-auto">
									<thead>
										<tr>
											<th width="100">Role</th>
											<th width="100">Type</th>
											<th width="200">Country/Cities</th>
											<th width="200">Message</th>
											<th width="150">Shedule date & time</th>
											<th width="100">Status</th>
											<th width="100">Error message</th>
											<th width="100">Action</th>
										</tr>
									</thead>
									<tbody>
										@foreach($schedules['data'] as $schedule)
											<tr>
												<td>{{ ucwords($schedule['role']) }}</td>
												<td>{{ ucwords($schedule['type']) }}</td>
												<td>
													<div class="p-b-10">
														Country : {{\CommonHelper::countries()[$schedule['country']]}}
													</div>
													<div>
														Cities : 
													</div>
													<div>
														@if( !empty($schedule['cities']) )
															<?php
																$schedule_cities = json_decode($schedule['cities'], true);
															$city_count = 0;
															$all_schedule_cities_str = '';
															?>

															@if(count($schedule_cities) >= 455)
																All
															@else

																@foreach( $cities as $city )
																	@if (in_array($city['id'], $schedule_cities))
																		<?php 
																			$all_schedule_cities_str .= $city['name'].', ';
																		?>
																		@if($city_count < 5) 
																		    {{$city['name']}},
																		@endif
																		<?php 
																		 $city_count++;
																		 ?>
																	@endif
																@endforeach

																@if( count($schedule_cities) > 5 )
																	<div class="view-more-content">
																		<a class="view-more-cities-button cursor-pointer" data-cities="{{$all_schedule_cities_str}}">View More Cities</a>
																	</div>
																@endif
															
															@endif

														@else
															all
														@endif
													</div>
												</td>
												<td>
												{{$schedule['message']}}
													
												</td>
												<td>
													<div>
														Scheduled Date : 
													</div>
													<div class="p-b-10">
														{{ date('d/m/Y', strtotime($schedule['schedule_date_time'])) }}
													</div>
													<div>
														Scheduled Time : 
													</div>
													<div>
														{{ date('h:i a ', strtotime($schedule['schedule_date_time'])) }}
													</div>
												</td>
												<td>
												@if($schedule['status'] == 0)
													Pending

												@elseif($schedule['status'] == 1)
													sent
												@else
													error
												@endif
												</td>
												<td>
												@if(!empty($schedule['error_message']) && $schedule['status'] == 2)
												<?php 
													$err = json_decode($schedule['error_message']);
												 ?>
												 @foreach($err->errors as $msg)
													<div>
														Message:{{$msg->message}}
													</div>
													<div>
														Description:{{$msg->description}}
													</div>
												 @endforeach
												</td>
												@endif
												<td>
													<button class="btn btn-dark-red delete-schedule-button" data-scheduled-id="{{$schedule['id']}}">Delete</button>
												</td>
											</tr>
										@endforeach
									</tbody>
								</table>
							</div>
						@else
							<div class="empty-result">No emails are scheduled yet.</div>
						@endif
					</div>
					<div class="panel-body padding-top-0px">
						<div class="row">
							<div class="col-xs-12">
								<div class="paginator-content {{$schedules['total'] > env('PAGINATION_LIMIT') ? '' : 'hide'}} pull-right no-margin">
									<ul id="bottom-user-paginator" class="pagination-purple make-paginator no-margin" data-type="schedules"></ul>
								</div>
								<?php 
									$total_result = $schedules['current_page'] * $schedules['per_page'];
								?>
								<div class="pull-left search-count-content {{$schedules['total'] > 0 ? '' : 'hide'}}">
									Showing: <span class="fetched-count">{{$total_result > $schedules['total'] ? $schedules['total'] : $total_result}}</span> 
											out of 
											<span class="total-data-count">{{$schedules['total']}}</span> schedule(s)
								</div>
								<div>
								 {{ $paginate->links() }}
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>	
	</div>


<div id="view-more-cities-modal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">List of all cities</h4>
      </div>
      <div class="modal-body">
        <p class="city-list-content">Some text in the modal.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<script>
	var dont_triger_history = true;
	var delete_schedule_url = "{{route('admin.schedule.delete')}}";
</script>
@stop