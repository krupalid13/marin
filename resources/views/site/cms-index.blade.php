<!DOCTYPE html>
<html lang="en">

<head>
    <?php $site_version = rand(10, 100); ?>
    <title>
        @if(isset($pageTitle) && !empty($pageTitle))
            {!! $pageTitle !!}
        @else
            {{env('APP_NAME')}}
        @endif
    </title>
    <meta name="description" content="{!! ($metaDescription) ?? '' !!}">
    <meta name="keywords" content="{!! ($metaKeywords) ?? '' !!}">
    <meta charset="utf-8" />
    <meta name="viewport"
        content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    <meta name="_token" content="{{ csrf_token() }}">
    <link rel="shortcut icon" type="image/x-icon" href="{{asset('public/images/favicon.ico')}}">
    <link rel="stylesheet" href="{{asset('public/assets/css/cssstyle.css')}}">
    <script type="text/javascript" src="{{asset('public/js/new/scripts.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/js/new/jquery.min.js')}}"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
    <script type="text/javascript" src="{{asset('public/js/new/bootstrap.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/assets/js/site_plugins.js?v=59')}}"></script>
        <script type="text/javascript" src="{{asset('public/assets/js/site_app.js?v=59')}}"></script>
        <script type="text/javascript" src="{{asset('public/assets/js/site/home.js?v=59')}}"></script>
            <link href="{{asset('public/assets/css/site_app.css?v=59')}}" rel="stylesheet">
            <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"></script>
    @yield('page-level-style')
    @yield('addStyle')
    <style>
        body {
            margin: 0;
        }

        .footer-contactus-image {
            position: relative;
        }

        .container {
            width: 100%;
            padding-right: 15px;
            padding-left: 15px;
            margin-right: auto;
            margin-left: auto;
            max-width: 95%
        }

        a,
        a img {
            text-decoration: none;
            border: 0;
            outline: none !important;
        }

        .header_main {
            display: block;
            position: absolute;
            top: 0;
            left: 0;
            z-index: 100;
            width: 100%;
        }

        .header_main .inner_header {
            display: flex;
            -ms-display: flex;
            align-items: center;
            -ms-align-items: center;
            padding: 12px 30px;
            background-color: #fff;
            border-bottom-left-radius: 7px;
            -ms-border-bottom-left-radius: 7px;
            border-bottom-right-radius: 7px;
            -ms-border-bottom-right-radius: 7px;
            box-shadow: 0 2px 2px 0 rgb(0 0 0 / 14%), 0 1px 5px 0 rgb(0 0 0 / 12%), 0 3px 1px -2px rgb(0 0 0 / 20%);
        }

        @media (max-width: 1199.98px) {
            .container {
                padding-right: 15px;
                padding-left: 15px;
            }
        }

        @media (min-width: 1200px) {
            .container {
                max-width: 1170px;
            }
        }

        .wrapper_main {
            position: relative;
            width: 100%;
            overflow: hidden;
        }

        .header_main .logo {
            margin-right: auto;
            margin-top: -7px;
            width: 150px;
        }

        .header_main .logo img {
            height: 44px;
        }

        .header_main .login {
            display: inline-block;
            padding: 13px 24px 8px;
            font-family: "poppinsbold";
            font-size: 18px;
            color: #fff;
            background-color: #34b5f2;
            border-radius: 3px;
            -ms-border-radius: 3px;
        }

        .footer_main {
            margin-top: 40px;
            position: relative;
            width: 100%;
            background: url(../images/map-img.png) center top no-repeat #0179c5;
        }

        .footer_main .links {
            display: inline-block;
            text-align: center;
            width: 100%;
            margin-top: 30px;
            margin-bottom: 20px;
        }

        .footer_main .links ul {
            display: block;
        }

        li,
        ul,
        ol {
            list-style: none;
            margin: 0;
            padding: 0;
        }

        .footer_main {
            position: relative;
            width: 100%;
            background: url("../images/map-img.png") center top no-repeat #0179c5;
        }

        .footer_main p {
            margin: 0;
            padding: 0;
            font-size: 15px;
            color: #171717;
        }
        .footer_main h2,
        .footer_main h3 {
            font-family: "montserratbold";
            font-style: normal;
            font-weight: bold;
            margin: 0;
            padding: 0;
            color: #171717;
            font-size: 1.1em;
        }
        .footer-logo {
            width: 50px;
        }
        .footer_main .footer-logo {
            margin-right: auto;
            margin-top: -7px;
            width: 150px;
        }

        .footer_main img {
            width: 9rem;
            height: auto;
        }

        .footer_main .text-block {
            width: 100%;
            padding: 30px 50px;
            /* text-align: center; */
            margin-top: -15px;
        }

        .footer_main .text-block h3 {
            margin-bottom: 6px;
            font-family: "poppinsbold";
            font-size: 32px;
            color: #fff;
            line-height: 34px;
        }

        .footer_main .text-block p {
            margin-bottom: 25px;
            font-size: 22px;
            color: #fff;
            line-height: 26px;
        }

        .footer_main .other-footer-links {
            display: inline-block;
            text-align: center;
            width: 100%;
            margin-top: -25px;
            margin-bottom: 10px;
        }

        .footer_main .other-footer-links a {
            display: inline-block;
            font-family: "poppinsbold";
            font-size: 14px;
            color: #fff;
        }

        .footer_main .other-footer-links a:hover {
            text-decoration: underline;
        }

        .footer_main .other-footer-links ul {
            display: block;
        }

        .footer_main .other-footer-links ul li {
            display: block;
            float: left;
            width: 33.333333%;
            margin-bottom: 8px;
        }

        .footer_main .pull-right {
            max-width: 430px;
        }

        .footer_main .other-footer-links p {
            display: inline-block;
        }

        .footer_main .other-footer-links p:first-child {
            margin-right: 30px;
        }

        .footer_main .other-footer-links p i {
            display: inline-block;
            vertical-align: top;
        }

        .footer_main .other-footer-links p span {
            display: inline-block;
            margin-left: 8px;
        }

        .footer_main .other-footer-links p span a {
            display: block;
        }

        .footer_main .other-footer-links p span a:first-child {
            margin-bottom: 8px;
        }

        .footer_main .copyright {
            width: 100%;
            margin: 0 auto;
            padding: 10px 0;
            background-color: #fff;
            text-align: center;
        }

        .footer_main .copyright a {
            color: #000;
        }

        .footer_main .copyright a:hover {
            color: #00acff;
        }
        .nav-link.dropdown-toggle{
                height: 100%;
            }
    </style>
</head>

<body>

    {{ csrf_field() }}
    <div class="wrapper_main">
        <header class="cs-header">
    <div class="container">
        <nav class="navbar navbar-default z-depth-1">
            <div class="navbar-header">
                <div class="hide-on-med-and-up">
                    @if(\Request::route()->getName() == 'user.profile')
                    <a class="navbar-toggler sidebar-toggler" id="sidebar-toggler" href="#">
                        <span class="navbar-toggler-icon">
                            <i class="icon-menu"></i>
                        </span>
                    </a>
                    @endif
                </div>
                @if(\Request::route()->getName() == 'user.profile')
                    <button type="button" class="navbar-toggle new-navbar-toggle" data-toggle="collapse"
                            data-target=".navbar-ex1-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                @endif
                <a class="navbar-brand" href="/">
                    <!-- <img alt="coursesea logo" src="{{ URL:: asset('public/assets/images/coursealogo.png')}}"> -->
                    <img alt="flanknot logo" src="{{ URL:: asset('public/images/flanknotlogo.png')}}" style="margin-top: 4px">
                </a>
            </div>
            <div class="navbar-right">
                @if(Auth::check() AND Auth::User()->registered_as != 'admin')
                    <?php
                    $user_details = Auth::User()->toArray();
                    // if($user_details['registered_as'] == 'company'){
                    //     $profile_path = 'site.show.company.details';
                    // }
                    if ($user_details['registered_as'] == 'seafarer') {
                        $profile_path = 'user.profile';
                    }
                    // if($user_details['registered_as'] == 'advertiser'){
                    //     $profile_path = 'site.advertiser.profile';
                    // }
                    if ($user_details['registered_as'] == 'institute') {
                        $profile_path = 'site.show.institute.details';
                    }
                    ?>
                    <ul class="nav">
                        <li class="nav-item dropdown">
                            @if(Auth::check() AND Auth::User()->registered_as != 'admin')
                                <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button"
                                   aria-haspopup="true"
                                   aria-expanded="false">{{isset($user_details['first_name']) ? ucwords($user_details['first_name']) : ''}}</a>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <div class="dropdown-header text-center">
                                        <strong>Account</strong>
                                    </div>
                                    <div class="">
                                        @if(isset($profile_path) AND !empty($profile_path))
                                            <a class="dropdown-item" href="{{route($profile_path)}}"><span
                                                        class="icon-user icon"></span> <span class="">Profile</span>
                                            </a>
                                        @endif
                                        <?php 

                                            $authDetail = Auth::user();
                                            $newUnreadJobCount = App\SendedJob::where('user_id',$authDetail->id)->where('status',2)->count();

                                        ?>
                                        <a class="dropdown-item" href="{{route('site.seafarer.job.new')}}">
                                             <span class="icon-briefcase icon"></span><span class="">New Jobs</span> 
                                            @if($newUnreadJobCount > 0)
                                            <small style="margin-left: 6px" class="label label-success new_job_a"> New </small>
                                            @endif
                                        </a>
                                        <a class="dropdown-item" href="{{route('site.how-to')}}">
                                             <span class="icon-question icon"></span><span class="">How To ?</span> 
                                        </a>
                                        @if(Auth::User()->registered_as == 'seafarer')
                                            <a class="dropdown-item" href="{{route('user.bookings')}}"><span
                                                        class="icon-notebook icon"></span> <span class="">My Bookings</span>
                                            </a>
                                        @endif
                                        <a class="dropdown-item" href="{{route('logout')}}"><span
                                                    class="icon-lock icon"></span> <span class="">Logout</span> </a>
                                    </div>

                                </div>
                            @endif
                        </li>
                    </ul>
                @else
                    @if(\Request::route()->getName() != 'register')
                        <a href="{{route('register')}}" class="btn cs-primary-btn navbar-btn">Register</a>
                    @endif
                    @if(\Request::route()->getName() != 'site.login')
                    <a href="{{route('site.login')}}" class="btn cs-primary-btn navbar-btn">Login</a>
                    @endif
                @endif
            </div>
        </nav>
    </div>
</header>
        @yield('content')
        <footer class="footer_main">
            <div class="container">
                <div class="row">

                    <!-- <div class="col-lg-4 col-sm-12"> -->
                    <div class="text-block">
                        <!-- <h2>Flankknot.com</h2> -->
                        <a class="footer-logo" href="#"><img src="{{ asset('public/images/footer-flanknot-logo.PNG') }}"
                                alt=""></a>
                        <h2>Martime Community</h2>
                    </div>
                    <!-- </div> -->
                </div>
                <div class="other-footer-links">
                    <ul>
                        <li><a href="/">Flankknot</a></li>
                        <li><a href="{{ route('site.how-to') }}">How To ?</a></li>
                        <li><a href="{{ route('faq') }}">FAQ's</a></li>
                        
                        <li><a href="{{ route('aboutUs') }}">About Us</a></li>
                        <li><a href="{{ route('site.contact-us') }}">Contacts</a></li>
                        <li><a href="{{ route('report') }}">Report</a></li>
                        
                        <li><a href="{{ route('tnc') }}">Terms & Conditions</a></li>
                        <li><a href="{{ route('disclaimer') }}">Disclaimer</a></li>
                        <li><a href="{{ route('privacy_policy') }}">Privacy Policy</a></li>
                        
                        <li><a href="{{ route('cookie-policy') }}">Cookie</a></li>
                        <li><a href="{{ route('company-inquiry') }}">Company Inquiry</a></li>
                        <li><a href="{{ route('institutes-inquiry') }}">Institutes Inquiry</a></li>
                    </ul>


                </div>
                <div class="copyright">
                    <p>Copyright @2021 | <a href="#">All Rights Reserved</a> | <a href="{{ route('tnc') }}">Terms and Conditions</a></p>
                </div>
            </div>
        </footer>
        {{-- <footer class="footer_main">
            <div class="container">
                <div class="links">
                    <ul>
                        <li><a href="{{ route('site.contact-us') }}">Contacts</a></li>
                        <li><a href="{{ route('disclaimer') }}">Disclaimer</a></li>
                        <li><a href="{{ route('privacy_policy') }}">Privacy Policy</a></li>
                        <li><a href="{{ route('tnc') }}">Terms & Conditions</a></li>
                        <li><a href="{{ route('faq') }}">FAQ's</a></li>
                        <li><a href="{{ route('cookie-policy') }}">Cookie</a></li>
                    </ul>
                </div>
                <div class="copyright">
                    <p>Copyright @2020 | <a href="#">All Rights Reserved</a> | <a href="{{ route('tnc') }}">Terms and
                            Conditions</a></p>
                </div>
            </div>
        </footer> --}}
    </div>
    @yield('js_script')
</body>

</html>
