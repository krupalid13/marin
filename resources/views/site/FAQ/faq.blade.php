@extends('site.index')
@section('content')
	<div class="faq content-section-wrapper">
		<div class="section container">
			<div class="section_heading">
				FAQs
			</div>
			<div style="font-size:21px;">
				General
			</div>
			<ul style="list-style-type: decimal;">
				<li style="margin-bottom:5px;">
					<div style="font-weight:bold">What is ConsultanSeas?</div>
					ConsultanSeas is an online portal which hosts job opportunities and resume
					exclusively from shipping industry. ConsultanSeas works in a dual way. It is a
					medium for job applicants to search job, whereas Shipping companies can post job
					requirements here.
				</li>
				<li style="margin-bottom:5px;">
					<div style="font-weight:bold">What other services does ConsultanSeas provide?</div>
					ConsultanSeas is a medium to explore various services related to Shipping Industry
					like -
					<ul style="padding-left: 18px;list-style: initial;">
						<li>Liaison with professionals like Master Mariner &amp; Chief Engineers for Audits
						and Surveys.
						</li>
						<li> Vessel Registration.</li>
						<li> Flag State Documentation.</li>
						<li> Training and Recruitment of Seafarers.</li>
						<li> Liaising with Training Institutes for Courses and Certification.</li>
					</ul>
				</li>
				<li style="margin-bottom:5px;">
					<div style="font-weight:bold">How can ConsultanSeas help me finding a job?</div>
					ConsultanSeas is specially designed to cater to shipping industry. We bring you a
					database of vacant jobs from Ship Manning Companies that is exclusively crafted to
					take care of any recruitment need arising in Shipping industry. All Ranks and Type of
					Vessels and Coast and Foreign Jobs.
				</li>
				<li style="margin-bottom:5px;">
					<div style="font-weight:bold">Why should I choose ConsultanSeas for finding me an ideal job?</div>
					ConsultanSeas hosts a variety of jobs pertaining only and only to Shipping industry
					and offers a wide variety of options to choose from. Now, job applicants don’t have
					to refer to different mediums like magazines and newspapers for job requirements,
					and can access jobs from the comfort of mobile phones.
				</li>
			</ul>
			<div style="font-size:21px;">
				Candidate
			</div>
			<ul style="list-style-type: decimal;">
				<li style="margin-bottom:5px;">
					<div style="font-weight:bold">How do I register on ConsultanSeas ?</div>
					Registering on ConsultanSeas is an easy process. You can login to the system by
					generating an user id and password to start with.
				</li>
				<li style="margin-bottom:5px;">
					<div style="font-weight:bold">Why I can’t see my cv in the database?</div>
					The cv must have failed to upload properly or might have some important criteria
					which was not uploaded. You should fill in the entire details which are listed
					mandatory and then again proceed for uploading the cv.
					If you are still unable to see your cv, you can reach to our customer support for
					further assistance.
				</li>
				<li style="margin-bottom:5px;">
					<div style="font-weight:bold">How do I update my resume?</div>
					You can update your resume by clicking the edit option on your CV.
				</li>
				<li style="margin-bottom:5px;">
					<div style="font-weight:bold">How is my resume validated?</div>
					Validated resumes ensure you have a good visibility to prospective employers and
					agents. If the resume is a verified, it is picked up easily for process.
					The resume is validated/verified on certain parameters like contact details which
					are verified through email link or through mobile by OTP.
				</li>
				<li style="margin-bottom:5px;">
					<div style="font-weight:bold">My resume shows un-validated, what do I do?</div>

					Please check once on our validation criteria and check if you have complied with the
					criteria.
					If you are facing an issue in spite of complying to the norms, please get in touch with
					our Customer Support for details.
				</li>
				<li style="margin-bottom:5px;">
					<div style="font-weight:bold">When and why does my resume deactivates?</div>
					We ensure we maintain updated and validated database for the companies
					registered with us. For this practise, resumes which are not updated over a period of
					6 months and above automatically classify as deactivated, though they still exist and
					are accessible to the companies and agents.
				</li>
				<li style="margin-bottom:5px;">
					<div style="font-weight:bold">How do I apply to a job?</div>
					You need to identify the job that best suits. Every job posting comes with an Apply
					option. You can apply to any job by clicking the apply tab.
				</li>
				<li style="margin-bottom:5px;">
					<div style="font-weight:bold">How can I deactivate my resume?</div>
					A resume gets deactivated automatically if not updated for 6 months. However, if
					you are not looking for a job change in near future and wish to remain out of job
					market, you can opt for deactivating the resume. Contact our customer support for
					more details.
				</li>
				<li style="margin-bottom:5px;">
					<div style="font-weight:bold">What are the special resume services available with ConsultanSeas?</div>
					ConsultanSeas has an in-built resume builder which generates a systematic and
					professional copy of your resume. This resume can be downloaded in pdf format.
				</li>
				<li style="margin-bottom:5px;">
					<div style="font-weight:bold">Can I search for land/ shore jobs?</div>
					Yes. ConsultanSeas offers jobs across different locations across the globe for sailing
					as well as shore jobs.
				</li>
				<li style="margin-bottom:5px;">
					<div style="font-weight:bold">Can I apply for jobs across countries?</div>
					Identify a job you wish to apply for across countries and click the apply tab for
					applying for the job.
				</li>
				<li style="margin-bottom:5px;">
					<div style="font-weight:bold">Can I choose to keep my identity confidential?</div>
					It is for the benefit of you that contacts and identity can fetch you more jobs and
					make you more visible.<br>
					However, if you represent a senior level with a company or otherwise and wish to
					keep you identity confidential. You can certainly opt for confidential status.
				</li>
				<li style="margin-bottom:5px;">
					<div style="font-weight:bold">Can I apply to multiple job posts at once?</div>
					Yes. You can apply to multiple jobs at once, but need click apply to every single job
					selected to ensure your resume reaches the prospective job offers.
				</li>
				<li style="margin-bottom:5px;">
					<div style="font-weight:bold">How do I change my id and password?</div>
					You can change your id and password through the edit profile option in your
					account.
				</li>
				<li style="margin-bottom:5px;">
					<div style="font-weight:bold">Is ConsultanSeas service paid for job applicants?</div>
					ConsultanSeas services comes free for basic services like resume posting and job
					search.<br>
					However, job applicants can opt for a premium service. The premium service
					ensures we verify your 2 documents - COC and Seaman’s book. This verification will
					reflect on the top of your resume. Resumes will also be promoted for first
					preference in Companies Search.
				</li>
			</ul>

			<div style="font-size:21px;">
				Company
			</div>
			<ul style="list-style-type: decimal;">
				<li style="margin-bottom:5px;">
					<div style="font-weight:bold">What is the benefit I derive from being registered with ConsultanSeas?</div>
					ConsultanSeas offers a whole arena of shipping industry jobs, employers, service
					providers who can act as a one stop solution to all your needs.
					2. What is the procedure for opting registering at ConsultanSeas?
					You can become a member with ConsultanSeas by simply logging in and creating a
					profile.
				</li>
				<li style="margin-bottom:5px;">
					<div style="font-weight:bold">Does ConsultanSeas help interview candidates?</div>
					Yes. We can help need based shipping companies with interviewing candidates at
					our end.
				</li>
				<li style="margin-bottom:5px;">
					<div style="font-weight:bold">How do I find the ideal candidate for my company requirement?</div>
					ConsultanSeas provides ample resume database which fits to different needs and
					requirements. Secondly, the database caters exclusively to Shipping industry and is
					therefore ideal for your requirement.
				</li>
				<li style="margin-bottom:5px;">
					<div style="font-weight:bold">Can I change my user id and password once I get registered?</div>
					Yes. You can change the user id and password once you register.
				</li>
				<li style="margin-bottom:5px;">
					<div style="font-weight:bold">Can I post multiple job openings?</div>
					Yes. As an Employer or Ship Manning Agent you can post multiple job opportunities
					at once.
				</li>
				<li style="margin-bottom:5px;">
					<div style="font-weight:bold">What is the role of ConsultanSeas in recruitment procedure?</div>
					ConsultanSeas is merely an online portal that brings you exclusive database catering
					to Shipping Industry.<br>
					We also offer you a platform to advertise your different requirements to seek ideal
					job applicants.
				</li>
				<li style="margin-bottom:5px;">
					<div style="font-weight:bold">How do I avail of the other services listed on ConsultanSeas site?</div>
					ConsultanSeas also offers parallel services pertaining to Shipping Industry. Our
					Customer support can be contacted for more details.
				</li>
				<li style="margin-bottom:5px;">
					<div style="font-weight:bold">Can I post jobs for overseas locations?</div>
					Yes. You can post a job of overseas location.
				</li>
				<li style="margin-bottom:5px;">
					<div style="font-weight:bold">How long does my ad remain active?</div>
					Your ad remains active on the portal until the closing date mentioned on your ad.
					However, an ad can remain active for one whole year in case the closing date is not
					mentioned.
				</li>
			</ul>
		</div>
	</div>
@stop