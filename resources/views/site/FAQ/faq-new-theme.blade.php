@extends('site.cms-index')
@section('content')
    <div class="footer-page-container" style="margin-top: 60px;">
        <h1 class="footer-page-title">FAQ's</h1>
        <h3 class="footer-page-midtitle">1. What is flanknot.com all about?</h3>
        <p class="footer-page-para">
            flanknot.com is an online portal with a network design that brings together the shipping universe.
        </p>
        <p class="footer-page-para">
            It brings together seafarers new and old and establishes a way of interaction, comradery and togetherness.
        </p>
        <p class="footer-page-para">
            It allows seafarers to share their professional details with companies and colleagues in a smart way and also
            apply for
            jobs.
        </p>
        <p class="footer-page-para">Shipping Companies that are part of this system too can showcase their establishment.
            Post job requirements and use the many smart features available.
        </p>
        <h3 class="footer-page-midtitle">2. What other services does flanknot.com provide?</h3>
        <p class="footer-page-para">
            flanknot.com is a medium to explore various services related to Shipping Industry like
        </p>
        <p class="footer-page-para">
            Liaising with Training Institutes for Courses and Certification.
        </p>
        <p class="footer-page-para">
            Training and Recruitment of Seafarers.
        </p>
        <p class="footer-page-para">
            Liaising Seafarers and companies with Maritime Institutes, Medical Institutions,
                Merchandise promoters.
        </p>
        <h3 class="footer-page-midtitle">3. How can flanknot.com help me find a job?</h3>
        <p class="footer-page-para">
            flanknot.com is specially designed to cater to shipping industry. We search and bring you only valid and
            existing job requirements from Ship Manning Companies around the world that is exclusively best suited for you.
            Find jobs from our registered companies and also through various sources, for all Ranks and Type of Vessels and
            Coastal and Foreign.
        </p>
        <h3 class="footer-page-midtitle">4. Why should a seafarer choose flanknot.com?</h3>
        <p class="footer-page-para">
            flanknot.com offers a variety of features and options to showcase only the best of you. A practical networking
            model enables seafarers to be updated on colleagues, friends, companies, institutes, shipping news and be aware
            of the times around. Flanknot.com hosts a variety of jobs pertaining only and only to Shipping industry and
            offers a wide variety of options to choose from. Now, job applicants don’t have to refer to different mediums
            like magazines and newspapers for job requirements, and can access jobs from the comfort of mobile phones.
            Seafarers also receive FREE storage facility to not only store all professionally required documents and
            certificates in a systematic manner that is easy to share but also save memories and sailing experiences along
            the way.
        </p>
        <h3 class="footer-page-midtitle">5. How do I login to flanknot.com?</h3>
        <p class="footer-page-para">
            First on foremost, you will have to register on flanknot.com, which is an easy process. To register you will
            have to share certain information’s like your email id, your name, rank, nationality, passport no. You will
            require to generate and confirm a password. Once your email id is verified. You can login to the system by using
            the registered email id as username and the confirmed password.
        </p>
        <h3 class="footer-page-midtitle">6. How do I build my profile?</h3>
        <p class="footer-page-para">
            Once logged in, the left panel has a button ‘Edit Profile’ which redirects the user to the page that collects
            all required information to build a professional profile.
        </p>
        <h3 class="footer-page-midtitle">7. How do I create / update / download my resume?</h3>
        <p class="footer-page-para">
            You can create / update your resume through the ‘Edit Profile’ section itself.
            There are different Resume templates depending on your Rank.
            You can download a pdf file of your resume through the download button at the bottom of your resume page.If you
            find something missing in your resume or wish to include some information that you cannot add through the ‘Edit
        Profile’ section. You can reach us with support for <a href="mailto:support@flanknot.com">grievance@flanknot.com</a>
        </p>
        <h3 class="footer-page-midtitle">8. Is my account Validated or Monitored?</h3>
        <p class="footer-page-para">
            During registration your email Id is validated. You have an option to get your contact numbers verified through
            mobile by OTP. We have the right to validate and verify your entries. We have a dedicated Trust and Safety team
            that monitors for any questionable or improper content on the website.
        </p>
        <p class="footer-page-para">
            Validated resumes ensure you have a good visibility to prospective employers and agents. If the resume is a
            verified, it is picked up easily for process by recruiters.
        </p>
        <h3 class="footer-page-midtitle">9. When and why is my account suspended or deleted?</h3>
        <p class="footer-page-para">
            Please check our Terms of Use and Privacy Policy, whether you are going against or not complying with any of our
            directives or policy criteria’s. If you are facing an issue in spite of complying to the norms, please Contact
            our customer support for more details or
        please get in touch through <a href="mailto:support@flanknot.com">grievance@flanknot.com</a>
        </p>
        <h3 class="footer-page-midtitle">10. When and why does my resume deactivate?</h3>
        <p class="footer-page-para">
            We ensure we maintain updated and validated database for our companies registered with us. For this practise,
            resumes which are not updated over a period of 1 year and above automatically classify as deactivated, though
            they still exist but are not accessible to the companies and agents.
        </p>
        <h3 class="footer-page-midtitle">11. How do I share my Resume / Profile / Documents with others?</h3>
        <p class="footer-page-para">
            In the Profile page left panel there is ‘Share’, which will redirect you to the share page. Here you have the
            option to share either your Resume, Profile and documents just by typing the email id.
        </p>
        <p class="footer-page-para">
            You can also create a contact section, which enables you to save all email id you have shared with.
        </p>
        <p class="footer-page-para">
            The history section records all the sharing correspondence and also keeps you updated on when your shared Resume
            / Profile / Document was viewed or downloaded.
        </p>
        <h3 class="footer-page-midtitle">12. What is a Quick Response?</h3>
        <p class="footer-page-para">
            Quick Response is a unique and advantageous feature available only on flanknot.com. this feature is an
            enhancement to the knowledge of whether your Resume was viewed and when it was downloaded
        </p>
        <p class="footer-page-para">
            Quick Response enables you to receive a short yet instant and direct to the point response from recruiters. This
            feature keeps you aware regarding the probability your job application has and also keeps you updated on any new
            developments.
        </p>
        <h3 class="footer-page-midtitle">13. How do I apply to a job?</h3>
        <p class="footer-page-para">
            You need to identify the job that best suits. Every job posting comes with an Apply option. You can apply to any
            job by clicking the apply tab. Job notification that you receive in your profile is calculated through an
            algorithm that decides whether a particular job requirement suits your profile.
        </p>
        <h3 class="footer-page-midtitle">14. Does flanknot.com assist in job search?</h3>
        <p class="footer-page-para">
            Apart from displaying advertisements from our registered companies, we also look out for all job requirements
            that are floating through various print, electronic media and socially networking media as well. We even forward
            such job requirement but do not validate the authenticity of such requirements. It is in the user’s judgement to
            apply on such
            advertisements. flanknot.com also forwards your resume to validated companies with urgent openings
        </p>
        <h3 class="footer-page-midtitle">15. How can I deactivate my resume?</h3>
        <p class="footer-page-para">
            A resume gets deactivated automatically if not updated for more than 1 year. However, if you are not looking for
            a job change in near future, you can opt for deactivating the resume. Contact our customer support for more
        details or please get in touch through <a href="mailto:support@flanknot.com">grievance@flanknot.com</a>
        </p>
        <h3 class="footer-page-midtitle">16. Can I search for land / shore jobs?</h3>
        <p class="footer-page-para">
            Yes. flanknot.com highlights jobs across the globe for sailing as well as shore jobs.
        </p>
        <h3 class="footer-page-midtitle">17. Can I choose to keep my identity confidential?</h3>
        <p class="footer-page-para">
            It is for the benefit of you that contacts and identity can fetch you more jobs and make you more visible.
            However, if you wish to keep your identity confidential. You can certainly opt for confidential status.
        </p>
        <h3 class="footer-page-midtitle">18. Can I apply to multiple job posts at once?</h3>
        <p class="footer-page-para">
            Yes. You can apply to multiple jobs at once, but you need click the apply button on every single requirement to
            ensure your resume reaches the prospective job offers.
        </p>
        <h3 class="footer-page-midtitle">19. How do I change my user id, username or password?</h3>
        <p class="footer-page-para">
            You can change your user id, username or password through the ‘Edit Login Details’ option in your account.
        </p>
    </div>
@stop
