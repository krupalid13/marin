@extends('site.index')
@section('content')
    <?php
    $initialize_pincode_maxlength_validation = false;
    $india_value = array_search('India', \CommonHelper::countries());
    $required_fields_for_selected_rank[] = '';
    $currentYear = date('Y');
    $first_tab_class = 'page-done page-active';
    $tab_class = 'details-tab';
    $reg_flow = 'registration-flow';
    $disabled_class = 'disabled';
    if (Route::currentRouteName() == 'site.seafarer.edit.profile') {
        $first_tab_class = '';
        $tab_class = 'edit-details-tab';
        $reg_flow = '';
        $disabled_class = '';
    }

    $required_fields = \CommonHelper::rank_required_fields();

    if (isset($user_data[0]['professional_detail']['current_rank'])) {

        $required_fields_for_selected_rank = \CommonHelper::rank_required_fields()[$user_data[0]['professional_detail']['current_rank']];
    }
    $required_fields_name[] = '';
    foreach ($required_fields_for_selected_rank as $key => $value) {
        $rank_name = explode('-', $value);
        $required_fields_name[$key] = $rank_name[0];
    }

    ?>
    <style type="text/css">
        .normal-course-name{
            display: none;
        }
    </style>
    <script type="text/javascript" src="{{ URL:: asset('public/assets/crop_image/dist/jquery.form.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.blockUI/2.70/jquery.blockUI.js"></script>
    <link rel="stylesheet" href="{{ URL:: asset('public/assets/css/cropper.min.css')}}">
    <script type="text/javascript" src="{{ URL:: asset('public/assets/js/cropper.min.js')}}"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8/dist/sweetalert2.min.js"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/sweetalert2@8/dist/sweetalert2.min.css">
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <style>
        .user-registration .user-details-container .form-group .input-label, .user-registration .user-details-container .heading {
            font-size: 16px;
            color: #8e8e8e;
            margin-bottom: 0px;
            font-weight: normal;
            width: 100%;
        }
        .user-progress{
            width: 280px !important;
            display: inline-block !important;
            margin: 0px !important;
        }
        .user-progress-text{
            text-align: center !important;
            font-size: 16px !important;
            font-size: 13px !important;
            color: #706666;
        }
        .used-mb{
            margin: 0px !important;
            margin-right: 53px !important;
        }
        .docs-upload{
            /* margin-bottom: 5px !important; */
        }
        .btn-success {
            color: #2a55a2;
            background-color: #ffffff;
            border-color: #2a55a2;
        }

        .user-registration .user-details-container .form-group .radio-inline {
            margin-top: 0px;
        }

        .btn-success:active:hover, .btn-success:active:focus, .btn-success:active.focus, .btn-success.active:hover, .btn-success.active:focus, .btn-success.active.focus, .open > .btn-success.dropdown-toggle:hover, .open > .btn-success.dropdown-toggle:focus, .open > .btn-success.dropdown-toggle.focus {
            color: #fff;
            background-color: #2a55a2;
            border-color: #2a55a2;
        }

        .btn-success:hover {
            color: #fff;
            background-color: #2a55a2;
            border-color: #2a55a2;
        }

        .nav-pills .nav-link.active, .nav-pills .show > .nav-link {
            color: #fff;
            background-color: #2a55a2;
        }

        .nav-link {
            height: 56px;
            text-align: center;

        }

        .nav-link hr {
            padding: 0px;
            margin: 0px;
        }

        .nav-pills > li.active > a, .nav-pills > li.active > a:hover, .nav-pills > li.active > a:focus {
            color: #fff;
            background-color: #2a55a2;
        }

        .nav > li > a {
            position: relative;
            display: block;
            padding: 10px 13px;
            background-color: #fff;
            color: #2a55a2;
        }

        .nav-pills > li + li {
            margin-left: 3px;
        }

        .title_heading {
            padding: 0px;
            margin: 0px;
        }

        .user-registration .page-flow-container .page-active .page-name {
            color: #47c4f0;
        }

        .user-registration .page-flow-container .page-point:not(:first-of-type) {
            margin-top: 0px !important;
        }

        .user-registration .page-flow-container .page-point:not(:first-of-type):after {
            content: "";
            position: absolute;
            bottom: 0px !important;
            left: 50%;
            border-left: 0px !important;
            height: 0px !important;
        }

        .add-more-section {
            background-color: #ffffff;
            padding: 10px;
            margin-top: 10px;
            margin-left: 5px;
            box-shadow: 0px 1px 2px 3px #eeeeee;
        }

        .user-registration .user-details-container {
            padding: 0px 15px 0px 15px;
        }

        .user-registration .page-flow-container {
            background-color: #2a55a2;
            padding-top: 20px;
            padding-bottom: 20px;
            box-shadow: 0px 2px 5px 1px #cccccc;
            padding: 15px;
        }

        .user-registration {
            background-color: #fff;
        }


        #cdc-template, #coc-template, #coe-template {
            border: 2px solid #2a55a2;
            padding: 5px;
            margin-bottom: 5px;
        }

        .user-registration .page-flow-container .page-point .page-name-xs {
            color: #47c4f0;
            font-size: 22px;
            text-align: center;
            padding-top: 5px;
        }

        .user-registration .page-flow-container .page-active .page-name {
            color: #47c4f0 !important;
        }

        .user-registration .user-details-container {
            background-color: #fff;
            padding: 0px 15px 0px 0px !important;
            color: #2a55a2 !important;
            margin-bottom: 30px;
            box-shadow: none !important;
        }

        .user-registration .page-flow-container .page-point .page-name-xs {
            padding-top: 3px;
            font-size: 14px;
            color: #ffffff;
            text-align: center;
        }

        .user-registration .page-flow-container .page-point .page-name {
            padding-top: 3px;
            font-size: 14px;
            color: #ffffff;
            text-align: center;
        }

        .user-registration .page-flow-container .page-active .page-name-xs {
            color: #47c4f0 !important;
        }

        .user-registration .user-details-container .btn-group .btn-default.active {
            background-color: #2a55a2;
            color: #fff;
        }

        .user-registration .user-details-container .btn-group .btn-default {
            width: 35px;
            height: 30px;
            border: 1px solid #2a55a2;
            background-color: #fff;
            color: #2a55a2;
        }

        .cs-footer {
            display: none;
        }

        .dropzone {
            padding: 0px;
            box-shadow: none;
            min-height: 80px;
        }

        .avatar-upload {
            position: relative;
            margin: 5px auto;
        }

        .avatar-upload .avatar-edit {
            position: absolute;
            right: 12px;
            z-index: 1;
            top: 10px;
        }

        .avatar-upload .avatar-delete {
            position: absolute;
            left: 12px;
            z-index: 1;
            top: 75%;
        }

        .avatar-upload .avatar-view {
            position: absolute;
            right: 4%;
            z-index: 1;
            top: 75%;
        }

        .avatar-upload .avatar-preview {
            /* border: 6px solid #F8F8F8;
    box-shadow: 0px 2px 4px 0px rgb(42, 85, 162);*/
        }

        .user-registration .user-details-container .form-group {
            padding-top: 0px;
        }

        .avatar-upload .avatar-edit input {
            display: none;
        }

        .avatar-upload .avatar-edit label {
            display: inline-block;
            width: 34px;
            height: 34px;
            margin-bottom: 0;
            border-radius: 100%;
            background: #FFFFFF;
            border: 1px solid transparent;
            box-shadow: 0px 2px 4px 0px rgba(0, 0, 0, 0.12);
            cursor: pointer;
            font-weight: normal;
            transition: all 0.2s ease-in-out;
        }

        .avatar-upload .avatar-delete label {
            display: inline-block;
            width: 34px;
            height: 34px;
            margin-bottom: 0;
            border-radius: 100%;
            background: #FFFFFF;
            border: 1px solid transparent;
            box-shadow: 0px 2px 4px 0px rgba(0, 0, 0, 0.12);
            cursor: pointer;
            font-weight: normal;
            transition: all 0.2s ease-in-out;
        }

        .avatar-upload .avatar-view label {
            display: inline-block;
            width: 34px;
            height: 34px;
            margin-bottom: 0;
            border-radius: 100%;
            background: #FFFFFF;
            border: 1px solid transparent;
            box-shadow: 0px 2px 4px 0px rgba(0, 0, 0, 0.12);
            cursor: pointer;
            font-weight: normal;
            transition: all 0.2s ease-in-out;
        }

        .avatar-upload .avatar-edit label:after {
            content: "\f040";
            font-family: 'FontAwesome';
            color: #757575;
            position: absolute;
            top: 6px;

            left: 0;
            right: 0;
            text-align: center;
            margin: auto;
        }

        .avatar-upload .avatar-delete label:after {
            content: "\f014";
            font-family: 'FontAwesome';
            color: #757575;
            position: absolute;
            top: 6px;
            color: red;
            left: 0;
            right: 0;
            text-align: center;
            margin: auto;
        }

        .avatar-upload .avatar-view label:after {
            content: "\f00e";
            font-family: 'FontAwesome';
            color: #757575;
            position: absolute;
            top: 6px;
            left: 0;
            right: 0;
            text-align: center;
            margin: auto;
        }


        .avatar-upload .avatar-preview > div {
            width: 100%;
            height: 100%;
            background-size: cover;
            background-repeat: no-repeat;
            background-position: center;
        }

        .box-2 {
            padding: 0.5em;
            width: calc(100% / 2 - 1em);
            text-align: center;
        }

        img {
            max-width: 100%;
        }

        .progress {
            display: none;
            position: relative;
            margin: 20px;
            width: 400px;
            background-color: #ddd;
            border: 1px solid blue;
            padding: 1px;
            left: 15px;
            border-radius: 3px;
        }

        .progress-bar {
            background-color: green;
            width: 0%;
            height: 30px;
            border-radius: 4px;
            -webkit-border-radius: 4px;
            -moz-border-radius: 4px;
        }

        .percent {
            position: absolute;
            display: inline-block;
            color: #fff;
            font-weight: bold;
            top: 50%;
            left: 50%;
            margin-top: -9px;
            margin-left: -20px;
            -webkit-border-radius: 4px;
        }

        #outputImage {
            display: none;
        }

        .error {
            color: #ad7b7b;
            background: #ffb3b3;
            padding: 10px;
            box-sizing: border-box;
            margin: 10px;
            border-radius: 3px;
            border: #f1a8a8 1px solid;
        }

        input#uploadImage {
            border: #f1f1f1 1px solid;
            padding: 6px;
            border-radius: 3px;
        }

        #outputImage img {
            max-width: 300px;
        }

        #submitButton {
            padding: 7px 20px;
            background: #9a9a9a;
            border: #898a89 1px solid;
            color: #F0F0F0;
            margin-left: 10px;
            border-radius: 3px;
            font-size: 0.8em;
        }

        .error {

        }

        .profile-pic-head{
            padding-top: 0px !important;
        }
        .home-label{
            color: gray !important;
            font-size: 12px;
            padding: 0px !important;
            margin-right: 17px;
        }
        .ship-label{
            color: gray !important;
            font-size: 12px;
            margin: 0px !important;
            padding: 0px !important;
        }
        .visibility-type{
            float: right;
            margin-top: 10px !important;
            padding: 0px !important
        }
        .file-size-error{
            margin-top: 10px !important;
        }
        .dce-row{
            text-align: center !important;
            margin-top: 7px !important;
            margin-right: 0px !important;
        }
        .reset-btn-right{margin-right: 10px;}
        @media (max-width: 1024px) {
            .user-registration .page-flow-container {
              display: flex;
              justify-content: space-around;
              align-items: center;
                  position: relative;
            }
            .user-registration .page-flow-container .page-point {
              text-align: center;
              padding: 0 14px;
            }
            .user-registration .page-flow-container .page-point:not(:first-of-type) {
              margin-top: 0px;
              position: relative;
            }
            .user-registration .page-flow-container .page-point .page-name {
              padding-top: 3px;
              font-size: 14px;
            }
            .user-registration .page-flow-container .page-point .circle {
              height: 20px;
              width: 20px;
              border: 1px solid #d8d7da;
              border-radius: 50%;
              margin: 0px auto;
            }
            .profile-edit-right-div{
                width: 100%;
            }
            .profile-edit-left-div{
                width: 100%;
            }
          }
          @media (max-width: 767px) {
              .onlyDesktop{
                  display: none;
              }
              .mobileBorder{
                  border-top: 1px #e0e0e0 dashed;
              }
              .mobileBorder:first-child{
                  border-top: none;
              }
          }
          @media (min-width: 768px) {
              .onlyMobile{
                  display: none;
              }
          }
    </style>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    <div class="user-registration content-section-wrapper">

        <div class="container" style="padding-top: 100px;">
            <div class="row">
                <div class="col-sm-3 col-md-2 col-lg-2 profile-edit-left-div">
                    <div class="page-flow-container {{$reg_flow}}">
                        <a href="{{URL::to('seafarer/profile/edit')}}" class="edit-profile-tab">
                            <div id="tab-1" class="page-point edit-details-tab   basic-details-tab cursor-pointer"
                                 data-form-id="basic-details-form">
                                <div class="page-name-xs">
                                    <i class="fa fa-user" aria-hidden="true"></i>
                                </div>
                                <div class="page-name hidden-xs">
                                    Personal
                                </div>

                            </div>
                        </a>

                        <a href="{{URL::to('seafarer/profile/edit#documents')}}" class="edit-profile-tab">
                            <div id="tab-2"
                                 class="page-point {{$disabled_class}} {{$tab_class}} {{ isset($user_data['current_route']) ? " imp-document-tab cursor-pointer page-done page-active" : '' }} "
                                 data-form-id="documents-details-form">
                                <div class="page-name-xs">
                                    <i class="fa fa-address-card" aria-hidden="true"></i>
                                </div>
                                <div class="page-name hidden-xs">
                                    Documents
                                </div>

                            </div>
                        </a>

                        <a href="{{URL::to('seafarer/profile/edit#course')}}" class="edit-profile-tab">
                            <div id="tab-3" class="page-point {{$disabled_class}} {{$tab_class}} {{
                                             isset($user_data['current_route']) ? " certificate-document-tab cursor-pointer page-done page-active" : '' }} "
                                 data-form-id="seafarer-certificates-form">
                                <div class="page-name-xs">
                                    <i class="fa fa-file-text" aria-hidden="true"></i>
                                </div>
                                <div class="page-name hidden-xs">
                                    Course
                                </div>

                            </div>
                        </a>
                        
                        <a href="{{URL::to('seafarer/profile/edit#sea-service')}}" class="edit-profile-tab">
                            <div id="tab-4"
                                 class="page-point {{$disabled_class}} {{$tab_class}} {{ isset($user_data['current_route']) ? " sea-service-document-tab cursor-pointer page-done page-active" : '' }} "
                                 data-form-id="seafarer-sea-service-form">
                                <div class="page-name-xs">
                                    <i class="fa fa-ship" aria-hidden="true"></i>
                                </div>
                                <div class="page-name hidden-xs">
                                    Sea Service
                                </div>

                            </div>
                        </a>
                        <a href="{{URL::to('seafarer/profile/edit#general')}}" class="edit-profile-tab">
                            <div id="tab-5"
                                 class="page-point {{$disabled_class}} {{$tab_class}} {{ isset($user_data['current_route']) ? " general-tab cursor-pointer page-done page-active" : '' }} "
                                 data-form-id="general-tab-form">
                                <div class="page-name-xs">
                                    <i class="fa fa-coffee" aria-hidden="true"></i>
                                </div>
                                <div class="page-name hidden-xs">
                                    General
                                </div>

                            </div>
                        </a>
                        <div id="tab-6" class="page-point {{$disabled_class}} {{$tab_class}} {{
                     isset($user_data['current_route']) ? " upload-document-tab cursor-pointer page-done page-active" : '' }} "
                             data-form-id="upload_documents">
                            <div class="page-name-xs">
                                <i class="fa fa-cloud-upload" aria-hidden="true"></i>
                            </div>
                            <div class="page-name hidden-xs">
                                Upload
                            </div>

                        </div>
                    </div>

                </div>
                <div class="col-sm-9 col-md-10 col-lg-10 profile-edit-right-div">
                    <div class="alert alert-danger alert-box" id="alert-box-documents" style="display: none">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error}}</li>
                            @endforeach
                        </ul>
                    </div>
                    <div class="md-l-pad">
                        <div class="basic-details-form-container">
                            <form id="basic-details-form"
                                  action="{{ isset($user_data['current_route']) ? route('site.user.profile.registration') : route('site.user.registration')}}"
                                  method="post">
                                <input type="hidden" value="{{csrf_token()}}">
                                <input type="hidden" name="user_id" id="user_id" class="user_id"
                                       value="{{isset($user_data[0]['id']) ? $user_data[0]['id'] : ''}}">
                                {{ csrf_field() }}
                                <input type="hidden" name="image-x" required>
                                <input type="hidden" name="image-y" required>
                                <input type="hidden" name="image-x2" required>
                                <input type="hidden" name="image-y2" required>
                                <input type="hidden" name="crop-w" required>
                                <input type="hidden" name="crop-h" required>
                                <input type="hidden" name="image-w" required>
                                <input type="hidden" name="image-h" required>
                                <input type="hidden" name="uploaded-file-name" required>
                                <input type="hidden" name="uploaded-file-path" required>

                                <div class="user-details-container" style="display:none;">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="main-heading">
                                                Login Details
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="heading">
                                                ACCOUNT CREDENTIALS<span class="symbol required"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="input-label" for="email">Email Address<span
                                                            class="symbol required"></span></label>
                                                <input type="email" class="form-control" id="email" name="email"
                                                       placeholder="Type your email address"
                                                       value="{{ isset($user_data[0]['email']) ? $user_data[0]['email'] : ''}}">
                                                <span class="hide" id="email-error" style="color: red"></span>
                                            </div>
                                        </div>
                                        @if(!isset($user_data['current_route']))
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label class="input-label" for="password">Password<span
                                                                class="symbol required"></span></label>
                                                    <input type="password" class="form-control" id="password"
                                                           name="password" placeholder="Type your password">
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label class="input-label" for="cpassword">Confirm Password<span
                                                                class="symbol required"></span></label>
                                                    <input type="password" class="form-control" id="cpassword"
                                                           name="cpassword" placeholder="Type password again">
                                                </div>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="user-details-container">
                                    {{-- <div class="col-sm-12">
                                        <div class="main-heading">
                                            Personal Details
                                        </div>
                                    </div> --}}
                                    <div class="main-heading">
                                        Personal Details
                                    </div>
                                    @if(Session::has('file-size-error'))
                                        <div class="alert alert-danger alert-dismissible file-size-error" role="alert">
                                            <strong>Storage!</strong> {{Session::get('file-size-error')}}
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                    @endif
                                    <div class="row add-more-section">
                                        <div class="col-sm-6">
                                            <div class="heading profile-pic-head">
                                                PROFILE PICTURE
                                            </div>
                                            <div class="image-wrapper">
                                                <div style="{{!isset($user_data[0]['profile_pic']) ? "display:none" : ""}}" class="pull-left close-button cdc-close-button cdc-close-button-0 delete-profile-pic">
                                                    <i class="fa fa-times" aria-hidden="true"></i>
                                                </div>
                                                <div class="upload-photo-container">
                                                    <div class="image-content">
                                                        <div class="registration-profile-image"
                                                            style="height: 100%;width: 100%;">
                                                            <?php
                                                            $image_path = '';
                                                            if (isset($user_data[0]['profile_pic'])) {
                                                                $image_path = env("AWS_URL").('public/images/uploads/seafarer_profile_pic/' . "" .
                                                                    $user_data[0]['id'] . "/" .
                                                                    $user_data[0]['profile_pic']);
                                                            }
                                                            ?>

                                                            {{-- @if(empty($image_path))
                                                                <div class="icon profile_pic_text"><i class="fa fa-camera"
                                                                                                    aria-hidden="true"></i>
                                                                </div>
                                                                <div class="image-text profile_pic_text">Upload Profile <br>
                                                                    Picture
                                                                </div>
                                                            @endif --}}

                                                            <input type="file" style="z-index: 9;" name="profile_pic"
                                                                id="profile-pic" class="cursor-pointer"
                                                                onChange="profilePicSelectHandler('basic-details-form', 'profile_pic', 'pic', 'seafarer')">

                                                            @if(!empty($image_path))
                                                                <img id="preview" style="border-radius: 50%;width: 100%;"
                                                                    src="{{ $image_path }}">
                                                                <!-- <div class="image-text profile_pic_text" style="position: absolute;
                                                                    top: 90px;
                                                                    left: 4px;
                                                                    width: 150px;">Change Profile <br> Picture</div> -->
                                                            @else
                                                            <img id="preview" style="border-radius: 50%;width: 100%;"
                                                                    src="{{URL::to('public\images\default.png')}}">
                                                                {{-- <img id="preview" style="border-radius: 50%"> --}}
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            {{-- <span class="f-14 note-clr-gray">Note:
                                                Image size should be less than 5 MB.
                                            </span> --}}
                                        </div>

                                        <div class="">
                                            <div class="col-sm-12">
                                                <div class="heading text-uppercase">
                                                    About me
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                            <textarea class="form-control" name="about_me" rows="4" cols="50" placeholder="Type your brief introduction">{{$user_data[0]['professional_detail']['about_me']}}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row add-more-section">
                                        <div class="text-center">
                                            <div class="form-group">
                                                <div class="row">
                                                    <!--<div class="col-md-3 col-sm-6"></div>-->
                                                    <!--<div class="col-md-3 col-sm-6">-->
                                                        Are You <span class="symbol required"></span> &nbsp;
                                                        <label class="radio-inline" style="margin-top: 0px;"/>
                                                            <input type="radio" class="selectvalueradio" name="where_in" value="Home" {{ !empty($user_data[0]['where_in']) ?  $user_data[0]['where_in'] == 'Home'? 'checked' : '' : 'checked' }}>
                                                            <i class="fa fa-home" style="font-size: 20px;"></i>
                                                        </label>&nbsp;&nbsp;&nbsp;&nbsp;
                                                        <label class="radio-inline" style="margin-top: 0px;">
                                                            <input type="radio" class="selectvalueradio" name="where_in" value="Ship" {{ !empty($user_data[0]['where_in']) ?  $user_data[0]['where_in'] == 'Ship'? 'checked' : '' : '' }}>
                                                            <i class="fa fa-ship" style="font-size: 16px;"></i>
                                                        </label>
                                                    <!--</div>-->
                                                </div>
                                                <div class="row" style="padding-left: 70px;">
                                                    <!--<div class="col-md-3"></div>-->
                                                    <!--<div class="col-md-4">-->
                                                        <label class="radio-inline home-label">
                                                            At Home
                                                        </label>
                                                        <label class="radio-inline ship-label">
                                                            On Ship
                                                        </label>
                                                    <!--</div>-->
                                                </div>
                                                <style type="text/css">
                                                    .displayblockf{
                                                        display: block;
                                                    }
                                                    .displaynonef{
                                                       display: none; 
                                                    }
                                                </style>
                                                <div class="row" style="margin-top: 20px; margin-left: 0px; margin-right: 0px;">
                                                    @php if(!empty($user_data[0]['where_in']) && ($user_data[0]['where_in'] == 'Ship')){
                                                       $homeshiptype = 'ship'; 
                                                    }else{
                                                        $homeshiptype = 'home'; 
                                                    }
                                                    @endphp
                                                    @php $homeships = gethomeship(); @endphp
                                                    @if(!empty($homeships))
                                                        @foreach($homeships as $homeship)
                                                        <div class="col-md-4 type{{ $homeship->type }} @if($homeship->type == $homeshiptype) displayblockf @else displaynonef @endif" style="text-align: left;  margin-bottom: 10px;" >
                                                            <label class="radio-inline" style="margin-top: 0px; color: grey;"/>
                                                                <input type="radio" name="home_ship_type" value="{{ $homeship->id }}"  {{ !empty($user_data[0]['shiphometype']) ?  $user_data[0]['shiphometype'] == $homeship->id ? 'checked' : '' : '' }}/>
                                                            {{ $homeship->typevalue }}
                                                        </div>
                                                        @endforeach
                                                    @endif
                                                    
                                                </div>
                                            </div>
                                        </div>
                                        <hr/>
                                        <div class="">
                                            <div class="col-md-12 heading">
                                                BASIC DETAILS<span class="symbol required"></span>
                                            </div>
                                        </div>
                                        <!-- <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="input-label" for="firstname">Full Name<span
                                                            class="symbol required"></span></label>
                                                <input type="text" class="form-control" id="firstname" name="firstname"
                                                       value="{{ isset($user_data[0]['first_name']) ? $user_data[0]['first_name'] : ''}}"
                                                       placeholder="Type your full name">
                                            </div>
                                        </div> -->
                                        @php
                                            $userTitle = "Mr.";
                                            if(isset($user_data[0]['title']) && !empty($user_data[0]['title'])){
                                                $userTitle = $user_data[0]['title'];
                                            }
                                        @endphp
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label class="input-label" for="title">Title<span
                                                            class="symbol required"></span></label>
                                                <select class="form-control" name="title">
                                                    <option value="Mr." {{ $userTitle == "Mr." ? 'selected' : ''}}> Mr. </option>
                                                    <option value="Miss." {{ $userTitle == "Miss." ? 'selected' : ''}}> Miss. </option>
                                                    <option value="Mrs." {{ $userTitle == "Mrs." ? 'selected' : ''}}> Mrs. </option>
                                                    <option value="Ms." {{ $userTitle == "Ms." ? 'selected' : ''}}> Ms. </option>
                                                    <option value="Capt." {{ $userTitle == "Capt." ? 'selected' : ''}}> Capt. </option>
                                                    <option value="Ch.Eng." {{ $userTitle == "Ch.Eng." ? 'selected' : ''}}> Ch.Eng. </option>
                                                    <option value="Dr." {{ $userTitle == "Dr." ? 'selected' : ''}}> Dr. </option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label class="input-label" for="firstname">First Name<span
                                                            class="symbol required"></span></label>
                                                <input type="text" class="form-control" id="firstname" name="firstname"
                                                       value="{{ isset($user_data[0]['first_name']) ? $user_data[0]['first_name'] : ''}}"
                                                       placeholder="First Name">
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label class="input-label" for="PPMname">Middle Name</label>
                                                <input type="text" class="form-control" id="PPMname" name="PPMname"
                                                       value="{{ isset($user_data[0]['PPMname']) ? $user_data[0]['PPMname'] : ''}}"
                                                       placeholder="Middle Name">
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label class="input-label" for="PPLname">Last Name<span
                                                            class="symbol required"></span></label>
                                                <input type="text" class="form-control" id="PPLname" name="PPLname"
                                                       value="{{ isset($user_data[0]['PPLname']) ? $user_data[0]['PPLname'] : ''}}"
                                                       placeholder="Last Name">
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="input-label" for="dob">Date Of Birth<span
                                                            class="symbol required"></span></label>
                                                <input type="text" class="form-control birth-date-datepicker" id="dob"
                                                       name="dob"
                                                       value="{{ isset($user_data[0]['personal_detail']['dob']) ? date('d-m-Y',strtotime($user_data[0]['personal_detail']['dob'])) : ''}}"
                                                       placeholder="dd-mm-yyyy">
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="input-label" for="place_of_birth">Place Of Birth<span
                                                            class="symbol required"></span></label>
                                                <input type="text" class="form-control" id="place_of_birth"
                                                       name="place_of_birth"
                                                       value="{{ isset($user_data[0]['personal_detail']['place_of_birth']) ? $user_data[0]['personal_detail']['place_of_birth'] : ''}}"
                                                       placeholder="Type city Name">
                                            </div>
                                        </div>

                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="input-label" for="nationality">Nationality<span
                                                            class="symbol required"></span></label>
                                                <select id='nationality' name="nationality" class="form-control" disabled="disabled">
                                                    <option value=''>Select Your nationality</option>
                                                    @foreach( \CommonHelper::countries() as $c_index => $country)
                                                        <option value="{{ $c_index }}" {{ isset($user_data[0]['personal_detail']['nationality']) ? $user_data[0]['personal_detail']['nationality'] == $c_index ? 'selected' : '' : ''}}> {{ $country }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="form-group">
                                                <div class="input-label" for="gender">Gender <span
                                                            class="symbol required"></span></div>
                                                <label class="radio-inline">
                                                    <input type="radio" name="gender" value="M"
                                                            {{ !empty($user_data[0]['gender']) ?  $user_data[0]['gender'] == 'M'? 'checked' : '' : 'checked' }}>
                                                    <i class="fa fa-male" style="font-size: 19px;"></i> </label>
                                                <label class="radio-inline">
                                                    <input type="radio" name="gender" value="F"
                                                            {{ !empty($user_data[0]['gender']) ?  $user_data[0]['gender'] == 'F'? 'checked' : '' : '' }}>
                                                    <i class="fa fa-female" style="font-size: 19px;"></i>
                                                </label>
                                            </div>
                                        </div>

                                        <div class="col-sm-2">
                                            <div class="form-group">
                                                <div class="input-label" for="marital_status">Marital Status <span
                                                            class="symbol required"></span></div>
                                                <select id="marital_status" name="marital_status"
                                                        class="form-control select-rank-exp">
                                                    <option value="">Select</option>
                                                    @foreach(\CommonHelper::marital_status() as $m_index => $status)
                                                        <option value="{{$m_index}}" {{ !empty($user_data[0]['personal_detail']['marital_status']) ? $user_data[0]['personal_detail']['marital_status'] == $m_index ? 'selected' : '' : ''}}>{{$status}}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-sm-2">
                                            <div class="form-group">
                                                <label class="input-label" for="height">Height (cm)<span
                                                            class="symbol required"></span></label>
                                                <input type="text" class="form-control" id="height" name="height"
                                                       value="{{ isset($user_data[0]['personal_detail']['height']) ? $user_data[0]['personal_detail']['height'] : ''}}"
                                                       placeholder="Height in cms">
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="form-group">
                                                <label class="input-label" for="weight">Weight (kg)<span
                                                            class="symbol required"></span></label>
                                                <input type="text" class="form-control" id="weight" name="weight"
                                                       value="{{ isset($user_data[0]['personal_detail']['weight']) ? $user_data[0]['personal_detail']['weight'] : ''}}"
                                                       placeholder="Weight in kgs">
                                            </div>
                                        </div>
                                        

                                    </div>


                                    <div class="row add-more-section">
                                        <div class="col-sm-12">
                                            <div class="col-md-4 col-sm-4 form-group" style="padding-left:0px;">
                                                <div class="heading">
                                                    Language Known
                                                </div>
                                            </div>
                                            <div class="col-md-2 col-sm-2 onlyDesktop form-group">
                                                Speak
                                            </div>
                                            <div class="col-md-2 col-sm-2 onlyDesktop form-group">
                                                Read 
                                            </div>
                                            <div class="col-md-2 col-sm-2 form-group" style="padding-left:30px;">
                                                <a href="javascript:void(0)" class="addrow" style="background: #2a56a2;padding: 3px 8px;border-radius: 53%; color: #fff; "><i class="fa fa-plus"></i></a>
                                            </div>
                                        </div>
                                        <div class="loadlanguage">

                                            @php $languagesknown = $user_data[0]['languagesknown'] ?? array(); 
                                            @endphp

                                            @if(empty($languagesknown))
                                            <div class="col-sm-12">
                                                <div class="col-md-4 col-sm-4" style="padding-left:0px;">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control" id="mobile" name="language[1][name]" placeholder="Type Language">
                                                        <span class="hide" id="mobile-error" style="color: red"></span>
                                                    </div>
                                                </div>
                                                <div class="col-md-2 col-sm-2"  style="margin-top: 5px; margin-left: 10px;">
                                                    <span class="onlyMobile form-group"> Speak &nbsp;</span>
                                                    <input type="checkbox" name="language[1][can_speak]" value="1" checked="">
                                                </div>
                                                <div class="col-md-2 col-sm-2" style="margin-top: 5px; margin-left: 10px;">
                                                    <span class="onlyMobile form-group"> Read &nbsp;</span>
                                                    <input type="checkbox" name="language[1][can_read]" value="1">
                                                </div>
                                                <div class="col-md-2 col-sm-2" style="padding-left: 10px;">

                                                </div>
                                            </div>
                                            @else
                                            @foreach($languagesknown as $langu)
                                                <div class="col-sm-12 remove{{$langu['id']}}">
                                                    <div class="col-sm-4 col-md-4" style="padding-left:0px;">
                                                        <div class="form-group">
                                                            <input type="text" class="form-control" id="mobile" name="language[{{ $langu['id'] }}][name]" value="{{ $langu['language'] }}" placeholder="Type Language">
                                                            <span class="hide" id="mobile-error" style="color: red"></span>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2 col-sm-2 form-group"  style="margin-top: 5px; margin-left: 10px;">
                                                        <span class="onlyMobile form-group"> Speak &nbsp;</span>
                                                        <input type="checkbox" name="language[{{ $langu['id'] }}][can_speak]" value="1" {{ $langu['can_speak'] == 1 ? 'checked' : ''}}>
                                                    </div>
                                                    <div class="col-md-2 col-sm-2 form-group" style="margin-top: 5px; margin-left: 10px;">
                                                        <span class="onlyMobile form-group"> Read &nbsp;</span>
                                                        <input type="checkbox" name="language[{{ $langu['id'] }}][can_read]" value="1" {{ $langu['can_read'] == 1 ? 'checked' : ''}}>
                                                    </div>
                                                    @if ($loop->first)
                                                    {{-- <div class="col-md-2 col-sm-2 form-group" style="padding-left: 10px;">
                                                        <a href="javascript:void(0)" class="addrow" style="background: #2a56a2;padding: 3px 8px;    border-radius: 53%;    color: #fff;"><i class="fa fa-plus"></i></a>
                                                    </div> --}}
                                                    @else
                                                     <div class="col-md-2  col-sm-2 form-group" style="padding-left: 10px;">
                                                        <a href="javascript:void(0)" class="removerow" data-id="{{ $langu['id'] }}" style="background: #a22a51;padding: 3px 8px;    border-radius: 53%;    color: #fff;"><i class="fa fa-minus"></i></a>
                                                    </div>
                                                    @endif
                                                </div>
                                            @endforeach
                                            @endif
                                        </div>
                                        
                                    </div>

                                    <div class="row add-more-section">
                                        <div class="col-sm-12">
                                            <div class="heading">
                                                CONTACT INFORMATION<span class="symbol required"></span>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="input-label" for="mobile">Mobile Number<span class="symbol required"></span></label>
                                                <div class="col-sm-8 col-xs-8" style="padding-left:0;padding-right:0;">
                                                    <div class="form-group" style="display: flex;">
                                                        <input type="text" class="form-control landline-country-code valid" id="country_code" name="country_code" placeholder="91"  value="{{ isset($user_data[0]['country_code']) && !empty($user_data[0]['country_code']) ? $user_data[0]['country_code'] : '91'}}" maxlength="5" autocomplete="off">
                                                    <input type="text" class="form-control" id="mobile" name="mobile"
                                                           value="{{ isset($user_data[0]['mobile']) ? $user_data[0]['mobile'] : ''}}"
                                                           placeholder="Type your mobile number">
                                                        <span class="hide" id="mobile-error" style="color: red"></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="input-label">
                                                    Alternative Number
                                                </label>
                                                <div class="row">                                                    
                                                    <div class="col-sm-8 col-xs-8" >
                                                          <div class="form-group" style="display: flex;">
                                                         <input type="text" class="form-control landline-country-code valid" id="landline_code" name="landline_code" placeholder="91"  value="{{ isset($user_data[0]['personal_detail']['landline_code']) && !empty($user_data[0]['personal_detail']['landline_code']) ? $user_data[0]['personal_detail']['landline_code'] : '91'}}" maxlength="5" autocomplete="off">
                                                        <input type="text" class="form-control landline-number" id="landline"
                                                               name="landline"
                                                               value="{{ isset($user_data[0]['personal_detail']['landline']) ? $user_data[0]['personal_detail']['landline'] : ''}}"
                                                               placeholder="Type your landline number">
                                                           </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row add-more-section">
                                        <div class="col-sm-12">
                                            <div class="heading">
                                                LOCATION DETAILS<span class="symbol required"></span>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="input-label">Country<span class="symbol required"></span></label>
                                                <select id='country' name="country[0]"
                                                        class="form-control search-select country" block-index="0">
                                                    <option value=''>Select Your Country</option>
                                                    @foreach( \CommonHelper::countries() as $c_index => $country)
                                                        @if(isset($user_data[0]['personal_detail']['country']))
                                                            <option value="{{ $c_index }}" {{ isset($user_data[0]['personal_detail']['country']) ? $user_data[0]['personal_detail']['country'] == $c_index ? 'selected' : '' : ''}}> {{ $country }}</option>
                                                        @else
                                                            <option value="{{ $c_index }}"> {{ $country }}</option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group pincode-block pincode-block-0">
                                                <label class="input-label" for="pin_code">Postal Code<span
                                                            class="symbol required"></span></label>
                                                <div>
                                                    <input type="hidden" class="pincode-id" name="pincode_id[0]"
                                                           value="">

                                                    <i class="fa fa-spin fa-refresh select-loader hide pincode-loader-0"></i>
                                                    <input type="text" block-index="0" id='pincode'
                                                           data-form-id="basic-details-form"
                                                           class="form-control pincode pin_code_fetch_input"
                                                           name="pincode[0]"
                                                           value="{{ isset($user_data[0]['personal_detail']['pincode_text']) ? $user_data[0]['personal_detail']['pincode_text'] : ''}}"
                                                           placeholder="Type your pincode">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group state-block state-block-0">
                                                <label class="input-label">State<span
                                                            class="symbol required"></span></label>
                                                @if(isset($user_data[0]['personal_detail']))
                                                    @if($user_data[0]['personal_detail']['country'] == $india_value )
                                                        <select id="state" name="state[0]"
                                                                class="form-control search-select state fields-for-india">
                                                            <option value="">Select Your State</option>
                                                            @if(isset($user_data[0]['personal_detail']['pincode']['pincodes_states']))
                                                                @foreach($user_data[0]['personal_detail']['pincode']['pincodes_states'] as $pincode_states)
                                                                    <option value="{{$pincode_states['state_id']}}" {{$pincode_states['state_id'] == $user_data[0]['personal_detail']['state_id'] ? 'selected' : ''}}>{{ucwords(strtolower($pincode_states['state']['name']))}}</option>
                                                                @endforeach
                                                            @endif
                                                        </select>
                                                        <input type="text" id="state" name="state_name[0]"
                                                               class="form-control state_text hide fields-not-for-india"
                                                               placeholder="Enter State Name" value="">

                                                    @else
                                                        <select id="state" name="state[0]"
                                                                class="form-control search-select state hide fields-for-india">
                                                            <option value="">Select Your State</option>
                                                        </select>
                                                        <input type="text" id="state" name="state_name[0]"
                                                               class="form-control state_text fields-not-for-india"
                                                               placeholder="Enter State Name"
                                                               value="{{ isset($user_data[0]['personal_detail']['state_text']) ? $user_data[0]['personal_detail']['state_text'] : ''}}">
                                                    @endif
                                                @else
                                                    <select id="state" name="state[0]"
                                                            class="form-control search-select state fields-for-india">
                                                        <option value="">Select Your State</option>
                                                    </select>
                                                    <input type="text" id="state" name="state_name[0]"
                                                           class="form-control state_text hide fields-not-for-india"
                                                           placeholder="Enter State Name"
                                                           value="{{ isset($user_data[0]['personal_detail']['state_text']) ? $user_data[0]['personal_detail']['state_text'] : ''}}">
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group city-block city-block-0">
                                                <label class="input-label">City<span
                                                            class="symbol required"></span></label>

                                                @if(isset($user_data[0]['personal_detail']['country']))
                                                    @if( $user_data[0]['personal_detail']['country'] == $india_value )
                                                        <select id="city" name="city[0]"
                                                                class="form-control city fields-for-india">
                                                            <option value="">Select Your City</option>
                                                            @if($user_data[0]['personal_detail']['pincode']['pincodes_cities'])
                                                                @foreach($user_data[0]['personal_detail']['pincode']['pincodes_cities'] as $pincode_city)
                                                                    <option value="{{$pincode_city['city_id']}}" {{$pincode_city['city_id'] == $user_data[0]['personal_detail']['city_id'] ? 'selected' : ''}}>{{ucwords(strtolower($pincode_city['city']['name']))}}</option>
                                                                @endforeach
                                                            @endif
                                                        </select>
                                                        <input type="text" name="city_text[0]"
                                                               class="form-control city_text hide fields-not-for-india"
                                                               placeholder="Enter City Name" value="">

                                                    @else
                                                        <select id="city" name="city[0]"
                                                                class="form-control city hide fields-for-india">
                                                            <option value="">Select Your City</option>
                                                        </select>

                                                        {{--<input type="text" name="city_text[0]" class="form-control city_text fields-not-for-india" placeholder="Enter City Name" value="">--}}
                                                        <input type="text" name="city_text[0]"
                                                               class="form-control city_text fields-not-for-india"
                                                               placeholder="Enter City Name"
                                                               value="{{ isset($user_data[0]['personal_detail']['city_text']) ? $user_data[0]['personal_detail']['city_text'] : ''}}">
                                                    @endif
                                                @else
                                                    <select id="city" name="city[0]"
                                                            class="form-control city fields-for-india">
                                                        <option value="">Select Your City</option>
                                                    </select>

                                                    {{--<input type="text" name="city_text[0]" class="form-control city_text fields-not-for-india" placeholder="Enter City Name" value="">--}}
                                                    <input type="text" name="city_text[0]"
                                                           class="form-control city_text hide fields-not-for-india"
                                                           placeholder="Enter City Name"
                                                           value="{{ isset($user_data[0]['personal_detail']['city_text']) ? $user_data[0]['personal_detail']['city_text'] : ''}}">
                                                @endif
                                            </div>
                                        </div>

                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="input-label" for="permananent_address">Address Line 1<span
                                                            class="symbol required"></span></label>
                                                <input type="text" class="form-control" id="permananent_address"
                                                       name="permananent_address"
                                                       value="{{ isset($user_data[0]['personal_detail']['permanent_add']) ? $user_data[0]['personal_detail']['permanent_add'] : ''}}"
                                                       placeholder="Type your address">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="input-label" for="permananent_address2">Address Line
                                                    2</label>
                                                <input type="text" class="form-control" id="permananent_address2"
                                                       name="permananent_address2"
                                                       value="{{ isset($user_data[0]['personal_detail']['permanent_add2']) ? $user_data[0]['personal_detail']['permanent_add2'] : ''}}"
                                                       placeholder="Type your address">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <div class="input-label" for="nearest_place">Nearest Airport
                                                    Station
                                                </div>
                                                <input type="text" class="form-control" id="nearest_place"
                                                       name="nearest_place"
                                                       value="{{ isset($user_data[0]['personal_detail']['nearest_place']) ? $user_data[0]['personal_detail']['nearest_place'] : ''}}"
                                                       placeholder="Type your Nearest Airport">
                                            </div>
                                        </div>
                                    </div>


                                    <div class="row add-more-section">
                                        <div class="col-sm-12">
                                            <div class="heading">
                                                PROFESSIONAL DETAILS<span class="symbol required"></span>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <input type="hidden" name="_current_rank"
                                                       value="{{ !empty($user_data[0]['professional_detail']) ? $user_data[0]['professional_detail']['current_rank'] : '' }}"/>
                                                <label class="input-label">Current Rank<span
                                                            class="symbol required"></span></label>
                                                <select id="current_rank" name="current_rank" class="form-control">
                                                    <option value="">Select Your Rank</option>
                                                    @foreach(\CommonHelper::new_rank() as $index => $category)
                                                        <optgroup label="{{$index}}">
                                                            @foreach($category as $r_index => $rank)
                                                                <option value="{{$r_index}}" {{ !empty($user_data[0]['professional_detail']) ? $user_data[0]['professional_detail']['current_rank'] == $r_index ? 'selected' : '' : ''}}>{{$rank}} </option>
                                                            @endforeach
                                                        </optgroup>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="input-label" for="rank_exp">Current Rank Experience<span
                                                            class="symbol required"></span></label>
                                                <div class="row">
                                                    <div class="col-xs-6 col-md-6">
                                                        <select id="years" name="years"
                                                                class="form-control select-rank-exp">
                                                            <option value="">Years</option>
                                                            @foreach(\CommonHelper::years() as $r_index => $rank)
                                                                <option value="{{$r_index}}" {{ !empty($user_data[0]['professional_detail']) ? $user_data[0]['professional_detail']['years'] == $r_index ? 'selected' : '' : ''}}>{{$rank}}
                                                                </option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="col-xs-6 col-md-6">
                                                        <select id="months" name="months"
                                                                class="form-control select-rank-exp">
                                                            <option value="">Months</option>
                                                            @foreach(\CommonHelper::months() as $r_index => $rank)
                                                                <option value="{{$r_index}}" {{ !empty($user_data[0]['professional_detail']) ? $user_data[0]['professional_detail']['months'] == $r_index ? 'selected' : '' : ''}}>{{$rank}}
                                                                </option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="input-label">Rank Applied For<span
                                                            class="symbol required"></span>
                                                </label>
                                                <select id="applied_rank" name="applied_rank" class="form-control">
                                                    <option value="">Select Your Rank</option>
                                                    @foreach(\CommonHelper::new_rank() as $index => $category)
                                                        <optgroup label="{{$index}}"></optgroup>
                                                        @foreach($category as $r_index => $rank)
                                                            <option value="{{$r_index}}" {{ !empty($user_data[0]['professional_detail']['applied_rank']) ? $user_data[0]['professional_detail']['applied_rank'] == $r_index ? 'selected' : '' : ''}}>{{$rank}} </option>
                                                        @endforeach
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="input-label">Date of Availability <span
                                                            class="symbol required"></span>
                                                </label>
                                                <input type="text" class="form-control datepicker" id="date_avaibility"
                                                       name="date_avaibility"
                                                       value="{{ isset($user_data[0]['professional_detail']['availability']) ? date('d-m-Y',strtotime($user_data[0]['professional_detail']['availability'])) : ''}}"
                                                       placeholder="dd-mm-yyyy">
                                            </div>
                                        </div>
                                        <?php
                                        $dollar = '';
                                        $rupees = '';
                                        if (isset($user_data[0]['professional_detail']['currency'])) {
                                            if ($user_data[0]['professional_detail']['currency'] == 'dollar') {
                                                $dollar = 'active';
                                                $currency_type = '<i class="fa fa-dollar fa-lg"></i>';
                                            } else {
                                                $rupees = 'active';
                                                $currency_type = '<i class="fa fa-inr fa-lg"></i>';
                                            }
                                        } else {
                                            $rupees = 'active';
                                            $currency_type = '<i class="fa fa-inr fa-lg"></i>';
                                        }
                                        ?>
                                        <div class="col-sm-6">

                                            <div class="form-group">
                                                <div class="col-xs-12">
                                                    <label class="input-label">Expdected Wages<span
                                                                class="symbol required"></span></label>
                                                </div>
                                                <div class="col-xs-4 m-t-5" style="margin: 6px 0px 21px 0px;">
                                                    <div class="btn-group" id="status" data-toggle="buttons"
                                                         style="width: 100px;">
                                                        <label class="btn btn-default btn-on btn-xs {{$dollar}} wages_currency1"
                                                               rel='<i class="fa fa-dollar fa-lg"></i>'>
                                                            <input type="radio" value="dollar"
                                                                   name="wages_currency" <?php if ($dollar) {
                                                                echo 'checked="checked"';
                                                            }?>><i class="fa fa-dollar fa-lg"></i>
                                                        </label>
                                                        <label class="btn btn-default btn-xs btn-off {{$rupees}} wages_currency1"
                                                               rel='<i class="fa fa fa-inr fa-lg"></i>'>
                                                            <input type="radio" value="rupees" <?php if ($rupees) {
                                                                echo 'checked="checked"';
                                                            }?> class="wages_currency" name="wages_currency">
                                                            <i class="fa fa-inr fa-lg"></i>
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="col-xs-4 p-l-0">
                                                    <input type="text" class="form-control" id="last_wages"
                                                           name="last_wages"
                                                           value="{{ isset($user_data[0]['professional_detail']['last_salary']) ? $user_data[0]['professional_detail']['last_salary'] : ''}}"
                                                           placeholder="wages">
                                                </div>
                                                <div class="col-xs-4 p-10-0">
                                                    <span id="currency_span">{!!$currency_type!!}</span>/Per Month
                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            @if(Route::currentRouteName() == 'site.seafarer.registration')
                                                <input type="checkbox" id="agree" name="agree"/>
                                                <label class="terms-conditions" style="display:inline; float:none"
                                                       for="agree">
                                                    I agree with the <a href="/terms&condition" target="_blank">terms
                                                        and conditions</a>.
                                                </label>
                                                <label for="agree" class="error"></label>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="reg-form-btn-container text-right">
                                    @if(isset($user_data['current_route']))
                                        <button type="submit" data-style="zoom-in"
                                                class="reg-form-btn blue ladda-button basic-details-submit"
                                                id="submitBasicDetailButton" value="Save" data-tab-index="1"
                                                style="margin-bottom: 60px;">Save & Next
                                        </button>
                                    @else
                                        <button type="submit" data-style="zoom-in"
                                                class="reg-form-btn blue ladda-button basic-details-submit"
                                                id="submitBasicDetailButton" value="Next" data-tab-index="1"
                                                style="margin-bottom: 60px;">Save & Next
                                        </button>
                                    @endif
                                </div>
                            </form>
                        </div>
                        <div class="alert alert-danger alert-box" id="alert-box-documents" style="display: none">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error}}</li>
                                @endforeach
                            </ul>
                        </div>

                        <input type="hidden" name="india_value" value="{{$india_value}}" required>

                        <form id="documents-details-form" class="hide" action="{{ route('site.user.store.documents') }}"
                              method="post">
                            {{ csrf_field() }}
                            <div class="user-details-container">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="main-heading">
                                            Documents Details
                                        </div>
                                    </div>
                                </div>

                                <input type="hidden" id="current_rank_id" name="current_rank_id"
                                       value="{{isset($user_data[0]['professional_detail']['current_rank']) ? $user_data[0]['professional_detail']['current_rank'] : ''}}">
                                <div class="row add-more-section">
                                    <div class="col-sm-12">
                                        <div class="heading">
                                            PASSPORT DETAILS<span class="symbol required"></span>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="input-label">Passport Country<span
                                                        class="symbol required"></span></label>
                                            <select class="form-control" id="passcountry" name="passcountry">
                                                <option value="">Select Passport Country</option>
                                                @foreach( \CommonHelper::countries() as $c_index => $country)
                                                    <option value="{{ $c_index }}" {{ !empty($user_data[0]['passport_detail']) ? $user_data[0]['passport_detail']['pass_country'] == $c_index ? 'selected' : '' : ''  }}> {{ $country }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label class="input-label" for="passno">Passport No<span
                                                        class="symbol required"></span></label>
                                            <input type="text" class="form-control" id="passno" name="passno"
                                                   value="{{ isset($user_data[0]['passport_detail']['pass_number']) ? $user_data[0]['passport_detail']['pass_number'] : ''}}"
                                                   placeholder="Type your passport number">
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label class="input-label" for="passplace">Place Of Issue<span
                                                        class="symbol required"></span></label>
                                            <input type="text" class="form-control" id="passplace" name="passplace"
                                                   value="{{ isset($user_data[0]['passport_detail']['place_of_issue']) ? $user_data[0]['passport_detail']['place_of_issue'] : ''}}"
                                                   placeholder="Type place of issue">
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label class="input-label">Passport Issue Date<span
                                                        class="symbol required"></span></label>
                                            <input type="text" class="form-control issue-datepicker"
                                                   id="passdateofissue" name="passdateofissue"
                                                   value="{{ isset($user_data[0]['passport_detail']['pass_issue_date']) ? date('d-m-Y',strtotime($user_data[0]['passport_detail']['pass_issue_date'])) : ''}}"
                                                   placeholder="dd-mm-yyyy">
                                        </div>
                                    </div>

                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label class="input-label" for="passdateofexp">Passport Expiry Date<span
                                                        class="symbol required"></span></label>
                                            <input type="text" class="form-control datepicker" id="passdateofexp"
                                                   name="passdateofexp"
                                                   value="{{ isset($user_data[0]['passport_detail']['pass_expiry_date']) ? date('d-m-Y',strtotime($user_data[0]['passport_detail']['pass_expiry_date'])) : ''}}"
                                                   placeholder="dd-mm-yyyy">
                                        </div>
                                    </div>


                                </div>
                                 <div class="row add-more-section">

                                    <div class="col-sm-12">
                                        <div class="heading">
                                            VISA DETAILS
                                        </div>
                                    </div>

                                        <div class="col-sm-12">
                                            <div class="col-md-5 col-sm-5 onlyDesktop" style="padding-left:0px;">
                                                <div class="heading input-label">
                                                    Visa for Country 
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-3 onlyDesktop" style="padding-left:0px;" >
                                                <div class="heading">
                                                Type of Visa
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-3 onlyDesktop" style="padding-left:0px;">
                                                <div class="heading">
                                                    Visa Expiry Date 
                                                </div>
                                            </div>
                                            <div class="col-md-1 col-sm-1"  style="padding-left:0px; margin-top: 14px; ">
                                                    <a href="javascript:void(0)" class="addrowvisa" style="background: #2a56a2;padding: 3px 8px;    border-radius: 53%;    color: #fff;"><i class="fa fa-plus"></i></a>
                                            </div>
                                        </div>
                                        <div class="loadcountryid" style="display: none;">
                                            <option value="">Select Passport Country</option>
                                            @foreach( \CommonHelper::countries() as $c_index => $country)
                                                <option value="{{ $c_index }}"> {{ $country }}</option>
                                            @endforeach
                                        </div>
                                         @php $visas = $user_data[0]['visas'] ?? array(); @endphp
                                        <div class="loadvisa">
                                            @if(empty($visas))
                                            <div class="col-sm-12 mobileBorder form-group">
                                                <div class="col-md-5 col-sm-5" style="padding-left:0px; margin-top: 5px;">
                                                    <div class="form-group">
                                                        <label class="input-label onlyMobile">Visa for Country<span class="symbol required"></span></label>
                                                       <select class="form-control" id="passcountry" name="visa[1][country]">
                                                        <option value="">Select Passport Country</option>
                                                        @foreach( \CommonHelper::countries() as $c_index => $country)
                                                            <option value="{{ $c_index }}"> {{ $country }}</option>
                                                        @endforeach
                                                    </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 col-sm-3" style="padding-left:0px; margin-top: 5px;">
                                                     <div class="form-group">
                                                         <label class="input-label onlyMobile">Type of Visa<span class="symbol required"></span></label>
                                                        <input type="text" class="form-control" id="mobile" name="visa[1][visa_type]" placeholder="Type of Visa">
                                                        <span class="hide" id="mobile-error" style="color: red"></span>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3 col-sm-3" style="padding-left:0px; margin-top: 5px;">
                                                    <div class="form-group">
                                                        <label class="input-label onlyMobile">Visa Expiry Date<span class="symbol required"></span></label>
                                                        <input type="text" class="form-control issue-datepicker datepicker"
                                                               id="passdateofissue" name="visa[1][expiry_date]"
                                                               placeholder="dd-mm-yyyy">
                                                    </div>
                                                </div>

                                                <div class="col-md-1 col-sm-1"  style="padding-left:0px; margin-top: 14px; ">
                                                     {{-- <a href="javascript:void(0)" class="addrowvisa" style="background: #2a56a2;padding: 3px 8px;    border-radius: 53%;    color: #fff;"><i class="fa fa-plus"></i></a> --}}
                                                </div>
                                                
                                            </div>
                                            @else
                                            @foreach($visas as $visa)
                                                <div class="col-sm-12 mobileBorder form-group removevisa{{ $visa['id'] }}">
                                                    <div class="col-md-5 col-sm-5" style="padding-left:0px; margin-top: 5px;">
                                                        <div class="form-group">
                                                            <label class="input-label onlyMobile">Visa for Country<span class="symbol required"></span></label>
                                                           <select class="form-control" id="passcountry" name="visa[{{ $visa['id'] }}][country]">
                                                            <option value="">Select Passport Country</option>
                                                            @foreach( \CommonHelper::countries() as $c_index => $country)
                                                                <option value="{{ $c_index }}" @if($c_index == $visa['country_id']) selected @endif)> {{ $country }}</option>
                                                            @endforeach
                                                        </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-sm-3" style="padding-left:0px; margin-top: 5px;">
                                                         <div class="form-group">
                                                            <label class="input-label onlyMobile">Type of Visa<span class="symbol required"></span></label>
                                                            <input type="text" class="form-control" id="mobile" name="visa[{{ $visa['id'] }}][visa_type]" placeholder="Type of Visa" value="{{ $visa['visa_type'] }}">
                                                            <span class="hide"  id="mobile-error" style="color: red"></span>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-3 col-sm-3" style="padding-left:0px; margin-top: 5px;">
                                                        <div class="form-group">
                                                            <label class="input-label onlyMobile">Visa Expiry Date<span class="symbol required"></span></label>
                                                            <input type="text" class="form-control issue-datepicker datepicker"
                                                                   id="passdateofissue" name="visa[{{ $visa['id'] }}][expiry_date]"
                                                                   placeholder="dd-mm-yyyy" value="{{ $visa['visa_expiry_date'] }}">
                                                        </div>
                                                    </div>
                                                    @if ($loop->first)
                                                    {{-- <div class="col-md-1 col-sm-1"  style=" padding-left:0px; margin-top: 14px;">
                                                         <a href="javascript:void(0)" class="addrowvisa" style="background: #2a56a2;padding: 3px 8px;    border-radius: 53%;    color: #fff;"><i class="fa fa-plus"></i></a>
                                                    </div> --}}
                                                    @else
                                                    <div class="col-md-1 col-sm-1"  style="padding-left:0px; margin-top: 14px;">
                                                         <a href="javascript:void(0)" data-id="{{ $visa['id'] }}" class="removerowvisa" style="background: #a22a51;padding: 3px 8px;    border-radius: 53%;    color: #fff;"><i class="fa fa-minus"></i></a>
                                                    </div>
                                                    
                                                    @endif
                                                    
                                                </div>
                                            @endforeach
                                            @endif
                                            
                                        </div>
                                        
                                    </div>
                                
                                <?php

                                if (in_array('FROMO', $required_fields_for_selected_rank)) {
                                    $hide_fromo = '';
                                } else {
                                    $hide_fromo = 'hide';
                                }
                                ?>

                                <div class="row {{$hide_fromo}} add-more-section" id="FROMO-details">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="input-label" for="fromo">Experience Of Framo<span
                                                        class="symbol required"></span></label>
                                            <label class="radio-inline">
                                                <input type="radio" name="fromo"
                                                       value="1" {{!empty($user_data[0]['passport_detail']['fromo']) ? $user_data[0]['passport_detail']['fromo'] == '1'? 'checked' : '' : '' }}>
                                                Yes
                                            </label>
                                            <label class="radio-inline">
                                                <input type="radio" name="fromo"
                                                       value="0" {{!empty($user_data[0]['passport_detail']['fromo']) ? $user_data[0]['passport_detail']['fromo'] == '0'? 'checked' : '' : 'checked' }}>
                                                No
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                @if(!empty($user_data[0]['personal_detail']) && $user_data[0]['personal_detail']['nationality'] == 95)
                                    <div class="row add-more-section">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="input-label" for="indosno">Indos Number</label>
                                                <input type="text" class="form-control" id="indosno" name="indosno"
                                                    value="{{ isset($user_data[0]['wkfr_detail']['indos_number']) ? $user_data[0]['wkfr_detail']['indos_number'] : ''}}"
                                                    placeholder="Type the indos number">
                                            </div>
                                        </div>
                                    </div>
                                @endif
                                @if(!empty($user_data[0]['personal_detail']) && $user_data[0]['personal_detail']['nationality'] == 95)
                                    <div id='SID-details' class="required_fields row add-more-section">
                                        <div class="row">
                                            <div class="col-sm-12 form-group">
                                                <div class="heading">
                                                    Seafarers Identity Documents (SID)
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div id="sid-template">
                                                <div class="row">
                                                    <div class="col-sm-3">
                                                        <div class="form-group">
                                                            <label class="input-label">SID Number</label>
                                                            <input type="text" class="form-control sid_number"
                                                                   name="sid_number"
                                                                   value="{{ isset($user_data[0]['professional_detail']['sid_number']) ? $user_data[0]['professional_detail']['sid_number'] : ''}}"
                                                                   placeholder="SID Number">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <div class="form-group">
                                                            <label class="input-label">SID Issue Date</label>
                                                            <input type="text" class="form-control datepicker sid_issue_date"
                                                                   name="sid_issue_date"
                                                                   value="{{ isset($user_data[0]['professional_detail']['sid_issue_date']) ? $user_data[0]['professional_detail']['sid_issue_date'] : ''}}"
                                                                   placeholder="dd-mm-yyyy">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <div class="form-group">
                                                            <label class="input-label" for="coc_no">SID Expiry Date</label>
                                                            <input type="text" class="form-control datepicker sid_expire_date"
                                                                   name="sid_expire_date"
                                                                   value="{{ isset($user_data[0]['professional_detail']['sid_expire_date']) ? $user_data[0]['professional_detail']['sid_expire_date'] : ''}}"
                                                                   placeholder="dd-mm-yyyy">
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="col-sm-3">
                                                        <div class="form-group">
                                                            <label class="input-label" for="coc_exp">Issue Place</label>
                                                            <input type="text" class="form-control issue_place"
                                                                   name="issue_place"
                                                                   value="{{ isset($user_data[0]['professional_detail']['issue_place']) ? $user_data[0]['professional_detail']['issue_place'] : ''}}"
                                                                   placeholder="Issue Place">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                    </div>
                                @endif


                                <div id='CDC-details' class="row add-more-section">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="heading">
                                                SEAMAN BOOK DETAIL<span class="symbol required"></span>
                                            </div>
                                        </div>
                                    </div>
                                    @if(isset($user_data[0]['seaman_book_detail']) && count($user_data[0]['seaman_book_detail']) > 0)
                                        @foreach($user_data[0]['seaman_book_detail'] as $index => $data)
                                            <div id="cdc-template">
                                                <div class="row">
                                                    <div class="col-xs-12">
                                                        <div class="input-label p-t-10 cdc-name cdc-name-0 pull-left">
                                                            CDC Details {{$index+1}}</div>
                                                        @if($index > 0)
                                                            <div class="pull-right close-button cdc-close-button p-t-10 cdc-close-button-0"
                                                                 data-type='cdc'>
                                                                <i class="fa fa-times" aria-hidden="true"></i>
                                                            </div>
                                                        @else
                                                            <div class="pull-right close-button cdc-close-button hide p-t-10 cdc-close-button-0"
                                                                 data-type='cdc'>
                                                                <i class="fa fa-times" aria-hidden="true"></i>
                                                            </div>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <input type="hidden" class="total-count-cdc">
                                                    <div class="col-sm-3">
                                                        <div class="form-group">
                                                            <label class="input-label" for="number">CDC Number<span
                                                                        class="symbol required"></span></label>
                                                            <input type="text" class="form-control cdcno"
                                                                   name="cdcno[{{$index}}]"
                                                                   value="{{ isset($data['cdc_number']) ? $data['cdc_number'] : ''}}"
                                                                   placeholder="Type alphanumeric number">
                                                            <input type="hidden" class="form-control cdc_id" name="cdc_id[{{$index}}]" value="{{ isset($data['id']) ? $data['id'] : ''}}">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <div class="form-group">
                                                            <label class="input-label">CDC Issue Date<span
                                                                        class="symbol required"></span></label>
                                                            <input type="text"
                                                                   class="form-control issue-datepicker cdc_issue"
                                                                   name="cdc_issue[{{$index}}]"
                                                                   value="{{ isset($data['cdc_issue_date']) ? date('d-m-Y',strtotime($data['cdc_issue_date'])) : ''}}"
                                                                   placeholder="dd-mm-yyyy">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <div class="form-group">
                                                            <label class="input-label">CDC Expiry Date<span
                                                                        class="symbol required"></span></label>
                                                            <input type="text" class="form-control datepicker cdc_exp"
                                                                   name="cdc_exp[{{$index}}]"
                                                                   value="{{ isset($data['cdc_expiry_date']) ? date('d-m-Y',strtotime($data['cdc_expiry_date'])) : ''}}"
                                                                   placeholder="dd-mm-yyyy">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <div class="form-group">
                                                            <label class="input-label">Issuing Authority<span
                                                                        class="symbol required"></span></label>
                                                            <select class="form-control cdccountry"
                                                                    name="cdccountry[{{$index}}]">
                                                                <option value="">Select CDC Country</option>
                                                                @foreach( \CommonHelper::countries() as $c_index => $country)
                                                                    <option value="{{ $c_index }}" {{ !empty($data['cdc']) ? $data['cdc'] == $c_index ? 'selected' : '' : ''  }}> {{ $country }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row" style="display:none;">
                                                    <div class="col-sm-3">
                                                        <div class="form-group">
                                                            <label class="input-label" for="cdc_ver">CDC Verification
                                                                Date<span class="symbol required"></span></label>
                                                            <input type="text"
                                                                   class="form-control issue-datepicker cdc_ver"
                                                                   name="cdc_ver[{{$index}}]"
                                                                   value="{{ isset($data['cdc_verification_date']) ? date('d-m-Y',strtotime($data['cdc_verification_date'])) : ''}}"
                                                                   placeholder="dd-mm-yyyy">
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        @endforeach
                                    @else
                                        <div id="cdc-template">
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <div class="input-label p-t-10 cdc-name cdc-name-0 pull-left">CDC
                                                        Details 1
                                                    </div>
                                                    <div class="pull-right close-button cdc-close-button hide p-t-10 cdc-close-button-0"
                                                         data-type='cdc'>
                                                        <i class="fa fa-times" aria-hidden="true"></i>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <input type="hidden" class="total-count-cdc">
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label class="input-label" for="number">CDC Number<span
                                                                    class="symbol required"></span></label>
                                                        <input type="text" class="form-control cdcno" name="cdcno[0]"
                                                               value="{{ isset($user_data[0]['seaman_book_detail']['cdc_number']) ? $user_data[0]['seaman_book_detail']['cdc_number'] : ''}}"
                                                               placeholder="Type alphanumeric number">
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label class="input-label">CDC Issue Date<span
                                                                    class="symbol required"></span></label>
                                                        <input type="text"
                                                               class="form-control issue-datepicker cdc_issue"
                                                               name="cdc_issue[0]"
                                                               value="{{ isset($data['cdc_issue_date']) ? date('d-m-Y',strtotime($data['cdc_issue_date'])) : ''}}"
                                                               placeholder="dd-mm-yyyy">
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label class="input-label">CDC Expiry Date<span
                                                                    class="symbol required"></span></label>
                                                        <input type="text" class="form-control datepicker cdc_exp"
                                                               name="cdc_exp[0]"
                                                               value="{{ isset($user_data[0]['seaman_book_detail']['cdc_expiry_date']) ? date('d-m-Y',strtotime($user_data[0]['seaman_book_detail']['cdc_expiry_date'])) : ''}}"
                                                               placeholder="dd-mm-yyyy">
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label class="input-label">Issuing Authority<span
                                                                    class="symbol required"></span></label>
                                                        <select class="form-control cdccountry" name="cdccountry[0]">
                                                            <option value="">Select CDC Country</option>
                                                            @foreach( \CommonHelper::countries() as $c_index => $country)
                                                                <option value="{{ $c_index }}" {{ !empty($user_data[0]['seaman_book_detail']) ? $user_data[0]['seaman_book_detail']['cdc'] == $c_index ? 'selected' : '' : ''  }}> {{ $country }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row" style="display:none;">
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label class="input-label" for="cdc_ver">CDC Verification
                                                            Date<span class="symbol required"></span></label>
                                                        <input type="text" class="form-control issue-datepicker cdc_ver"
                                                               name="cdc_ver[0]"
                                                               value="{{ isset($data['cdc_verification_date']) ? date('d-m-Y',strtotime($data['cdc_verification_date'])) : ''}}"
                                                               placeholder="dd-mm-yyyy">
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    @endif
                                    <div class="row" id="add-more-cdc-row">
                                        <div class="col-sm-12">
                                            <button type="button" data-form-id="seafarer-certificates-form"
                                                    class="btn add-more-button add-cdc-button" id="add-cdc">Add More CDC
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <!-- strat cop -->

                                <?php

                                if (in_array('COP-Optional', $required_fields_for_selected_rank)) {
                                    $hide_cop = 'hide';
                                } else {
                                    $hide_cop = '';
                                }
                                if (in_array('COP', $required_fields_name)) {
                                    $div_class = '';
                                } else {
                                    $div_class = 'hide';
                                }
                                ?>

                                <div id='COP-details' class="{{$div_class}} add-more-section">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="heading">
                                                CERTIFICATE OF PROFICIENCY<span
                                                        class="symbol required {{$hide_cop}}"></span>
                                            </div>
                                        </div>
                                    </div>
                                    @if(isset($user_data[0]['cop_detail']) && count($user_data[0]['cop_detail']) > 0)
                                        @foreach($user_data[0]['cop_detail'] as $index => $data)
                                            <div id="cop-template">
                                                <div class="row">
                                                    <input type="hidden" class="total-count-cop">
                                                    <div class="col-xs-12">
                                                        <div class="input-label p-t-10 cop-name pull-left">COP
                                                            Details {{$index+1}}</div>
                                                        @if($index > 0)
                                                            <div class="pull-right close-button cop-close-button p-t-10 cop-close-button-0"
                                                                 data-type='cop'>
                                                                <i class="fa fa-times" aria-hidden="true"></i>
                                                            </div>
                                                        @else
                                                            <div class="pull-right close-button cop-close-button hide p-t-10 cop-close-button-0"
                                                                 data-type='cop'>
                                                                <i class="fa fa-times" aria-hidden="true"></i>
                                                            </div>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-3">
                                                        <div class="form-group">
                                                            <label class="input-label" for="cop_no">COP Number<span
                                                                        class="symbol required {{$hide_cop}}"></span></label>
                                                            <input type="text" class="form-control cop_no"
                                                                   name="cop_no[{{$index}}]"
                                                                   value="{{ isset($data['cop_number']) ? $data['cop_number'] : ''}}"
                                                                   placeholder="Type alphanumeric number">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <div class="form-group">
                                                            <label class="input-label" for="grade">COP Grade<span
                                                                        class="symbol required {{$hide_cop}}"></span></label>
                                                            <input type="text" class="form-control cop_grade"
                                                                   name="cop_grade[{{$index}}]"
                                                                   value="{{ isset($data['cop_grade']) ? $data['cop_grade'] : ''}}"
                                                                   placeholder="Type your grade">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <div class="form-group">
                                                            <label class="input-label" for="cop_exp">Issue Date<span
                                                                        class="symbol required {{$hide_cop}}"></span></label>
                                                            <input type="text"
                                                                   class="form-control issue-datepicker cop_issue"
                                                                   name="cop_issue[{{$index}}]"
                                                                   value="{{ isset($data['cop_issue_date']) ? date('d-m-Y',strtotime($data['cop_issue_date'])) : ''}}"
                                                                   placeholder="dd-mm-yyyy">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <div class="form-group">
                                                            <label class="input-label" for="cop_exp">Expiry Date<span
                                                                        class="symbol required {{$hide_cop}}"></span></label>
                                                            <input type="text" class="form-control datepicker cop_exp"
                                                                   name="cop_exp[{{$index}}]"
                                                                   value="{{ isset($data['cop_exp_date']) ? date('d-m-Y',strtotime($data['cop_exp_date'])) : ''}}"
                                                                   placeholder="dd-mm-yyyy">
                                                        </div>
                                                    </div>
                                                </div>
                                                <hr>
                                            </div>
                                        @endforeach
                                    @else
                                        <div id="cop-template">
                                            <div class="row">
                                                <input type="hidden" class="total-count-cop">
                                                <div class="col-xs-12">
                                                    <div class="input-label p-t-10 cop-name pull-left">COP Details 1
                                                    </div>
                                                    <div class="pull-right close-button cop-close-button hide p-t-10 cop-close-button-0"
                                                         data-type='cop'>
                                                        <i class="fa fa-times" aria-hidden="true"></i>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label class="input-label" for="cop_no">COP Number<span
                                                                    class="symbol required {{$hide_cop}}"></span></label>
                                                        <input type="text" class="form-control cop_no" name="cop_no[0]"
                                                               value="{{ isset($user_data[0]['cop_detail']['cop_number']) ? $user_data[0]['cop_detail']['cop_number'] : ''}}"
                                                               placeholder="Type alphanumeric number">
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label class="input-label" for="grade">COP Grade<span
                                                                    class="symbol required {{$hide_cop}}"></span></label>
                                                        <input type="text" class="form-control cop_grade"
                                                               name="cop_grade[0]"
                                                               value="{{ isset($user_data[0]['cop_detail']['cop_grade']) ? $user_data[0]['cop_detail']['cop_grade'] : ''}}"
                                                               placeholder="Type your grade">
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label class="input-label" for="cop_exp">Issue Date<span
                                                                    class="symbol required {{$hide_cop}}"></span></label>
                                                        <input type="text"
                                                               class="form-control issue-datepicker cop_issue"
                                                               name="cop_issue[0]"
                                                               value="{{ isset($user_data[0]['cop_detail']['cop_issue_date']) ? date('d-m-Y',strtotime($user_data[0]['cop_detail']['cop_issue_date'])) : ''}}"
                                                               placeholder="dd-mm-yyyy">
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label class="input-label" for="cop_exp">Expiry Date<span
                                                                    class="symbol required {{$hide_cop}}"></span></label>
                                                        <input type="text" class="form-control datepicker cop_exp"
                                                               name="cop_exp[0]"
                                                               value="{{ isset($user_data[0]['cop_detail']['cop_exp_date']) ? date('d-m-Y',strtotime($user_data[0]['cop_detail']['cop_exp_date'])) : ''}}"
                                                               placeholder="dd-mm-yyyy">
                                                    </div>
                                                </div>
                                            </div>
                                            <hr>
                                        </div>
                                    @endif
                                    <div class="row" id="add-more-cop-row">
                                        <div class="col-sm-12">
                                            <button type="button" data-form-id="seafarer-certificates-form"
                                                    class="btn add-more-button add-cop-button" id="add-cop">Add More COP
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <!-- end cop -->
                                <?php

                                if (in_array('COC-Optional', $required_fields_for_selected_rank)) {
                                    $hide_coc = 'hide';
                                } else {
                                    $hide_coc = '';
                                }
                                if (in_array('COC', $required_fields_name)) {
                                    $div_class_coc = '';
                                } else {
                                    $div_class_coc = 'hide';
                                }
                                ?>

                                <div id='COC-details' class="required_fields {{$div_class_coc}} add-more-section">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="heading">
                                                CERTIFICATE OF COMPENTENCY<span
                                                        class="symbol required {{$hide_coc}}"></span>
                                            </div>
                                        </div>
                                    </div>
                                    @if(isset($user_data[0]['coc_detail']) && count($user_data[0]['coc_detail']) > 0)
                                        @foreach($user_data[0]['coc_detail'] as $index => $data)
                                            <div id="coc-template">
                                                <div class="row">
                                                    <input type="hidden" class="total-count-coc">
                                                    <input type="hidden" class="form-control coc_id" name="coc_id[{{$index}}]" value="{{ isset($data['id']) ? $data['id'] : ''}}">
                                                    <div class="col-xs-12">
                                                        <div class="input-label p-t-10 coc-name pull-left">COC
                                                            Details {{$index+1}}</div>
                                                        @if($index > 0)
                                                            <div class="pull-right close-button coc-close-button p-t-10 coc-close-button-0"
                                                                 data-type='coc'>
                                                                <i class="fa fa-times" aria-hidden="true"></i>
                                                            </div>
                                                        @else
                                                            <div class="pull-right close-button coc-close-button hide p-t-10 coc-close-button-0"
                                                                 data-type='coc'>
                                                                <i class="fa fa-times" aria-hidden="true"></i>
                                                            </div>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-3">
                                                        <div class="form-group">
                                                            <label class="input-label">COC<span
                                                                        class="symbol required {{$hide_coc}}"></span></label>
                                                            <select class="form-control coc_country"
                                                                    name="coc_country[{{$index}}]">
                                                                <option value="">Select COC Country</option>
                                                                @foreach( \CommonHelper::countries() as $c_index => $country)
                                                                    <option value="{{ $c_index }}" {{ !empty($data['coc']) ? $data['coc'] == $c_index ? 'selected' : '' : ''  }}> {{ $country }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <div class="form-group">
                                                            <label class="input-label" for="coc_no">COC Number<span
                                                                        class="symbol required {{$hide_coc}}"></span></label>
                                                            <input type="text" class="form-control coc_no"
                                                                   name="coc_no[{{$index}}]"
                                                                   value="{{ isset($data['coc_number']) ? $data['coc_number'] : ''}}"
                                                                   placeholder="Type alphanumeric number">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <div class="form-group">
                                                            <label class="input-label" for="grade">COC Grade<span
                                                                        class="symbol required {{$hide_coc}}"></span></label>
                                                            <input type="text" class="form-control grade"
                                                                   name="grade[{{$index}}]"
                                                                   value="{{ isset($data['coc_grade']) ? $data['coc_grade'] : ''}}"
                                                                   placeholder="Type your grade">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <div class="form-group">
                                                            <label class="input-label" for="coc_exp">COC Expiry
                                                                Date<span class="symbol required {{$hide_coc}}"></span></label>
                                                            <input type="text" class="form-control datepicker coc_exp"
                                                                   name="coc_exp[{{$index}}]"
                                                                   value="{{ isset($data['coc_expiry_date']) ? date('d-m-Y',strtotime($data['coc_expiry_date'])) : ''}}"
                                                                   placeholder="dd-mm-yyyy">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row" style="display:none;">
                                                    <div class="col-sm-3">
                                                        <div class="form-group">
                                                            <label class="input-label" for="coc_ver">COC Verification
                                                                Date<span class="symbol required {{$hide_coc}}"></span></label>
                                                            <input type="text"
                                                                   class="form-control issue-datepicker coc_ver"
                                                                   name="coc_ver[{{$index}}]"
                                                                   value="{{ isset($data['coc_verification_date']) ? date('d-m-Y',strtotime($data['coc_verification_date'])) : ''}}"
                                                                   placeholder="dd-mm-yyyy">
                                                        </div>
                                                    </div>
                                                </div>
                                                <hr>
                                            </div>
                                        @endforeach
                                    @else
                                        <div id="coc-template">
                                            <div class="row">
                                                <input type="hidden" class="total-count-coc">
                                                <input type="hidden" class="form-control coc_id" name="coc_id[]" value="">
                                                <div class="col-xs-12">
                                                    <div class="input-label p-t-10 coc-name pull-left">COC Details 1
                                                    </div>
                                                    <div class="pull-right close-button coc-close-button hide p-t-10 coc-close-button-0"
                                                         data-type='coc'>
                                                        <i class="fa fa-times" aria-hidden="true"></i>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label class="input-label">COC<span
                                                                    class="symbol required {{$hide_coc}}"></span></label>
                                                        <select class="form-control coc_country" name="coc_country[0]">
                                                            <option value="">Select COC Country</option>
                                                            @foreach( \CommonHelper::countries() as $c_index => $country)
                                                                <option value="{{ $c_index }}" {{ !empty($user_data[0]['coc_detail']['coc']) ? $user_data[0]['coc_detail']['coc'] == $c_index ? 'selected' : '' : ''  }}> {{ $country }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label class="input-label" for="coc_no">COC Number<span
                                                                    class="symbol required {{$hide_coc}}"></span></label>
                                                        <input type="text" class="form-control coc_no" name="coc_no[0]"
                                                               value=""
                                                               placeholder="Type alphanumeric number">
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label class="input-label" for="grade">COC Grade<span
                                                                    class="symbol required {{$hide_coc}}"></span></label>
                                                        <input type="text" class="form-control grade" name="grade[0]"
                                                               value=""
                                                               placeholder="Type your grade">
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label class="input-label" for="coc_exp">COC Expiry Date<span
                                                                    class="symbol required {{$hide_coc}}"></span></label>
                                                        <input type="text" class="form-control datepicker coc_exp"
                                                               name="coc_exp[0]"
                                                               value=""
                                                               placeholder="dd-mm-yyyy">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row" style="display:none;">
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label class="input-label" for="coc_ver">COC Verification
                                                            Date<span
                                                                    class="symbol required {{$hide_coc}}"></span></label>
                                                        <input type="text" class="form-control issue-datepicker coc_ver"
                                                               name="coc_ver[0]"
                                                               value=""
                                                               placeholder="dd-mm-yyyy">
                                                    </div>
                                                </div>
                                            </div>
                                            <hr>
                                        </div>
                                    @endif
                                    <div class="row" id="add-more-coc-row">
                                        <div class="col-sm-12">
                                            <button type="button" data-form-id="seafarer-certificates-form"
                                                    class="btn add-more-button add-coc-button" id="add-coc">Add More COC
                                            </button>
                                        </div>
                                    </div>
                                </div>

                                <?php
                                    $div_class_coe = $isCoe = $isDisplayCoe = 'd-none';
                                    if (in_array('COE', $required_fields_name)) {
                                        $div_class_coe = '';
                                        if (isset($user_data[0]['coe_detail']) && count($user_data[0]['coe_detail']) > 0 && $user_data[0]['coe_detail'][0]['type'] == 1) {
                                            $isCoe = '';
                                            $isDisplayCoe = '';
                                        }
                                    }

                                    

                                    // dd($div_class_coe);

                                ?>

                                <div id='COE-details' class="required_fields {{$div_class_coe}} add-more-section">
                                    <div class="row pb-10">
                                        <div class="col-xs-6">
                                            <div class="heading">
                                                CERTIFICATE OF ENDORSEMENT
                                            </div>
                                        </div>
                                        <div class="col-xs-6">
                                            <div class="heading">
                                                <label class="radio-inline">
                                                    <input type="radio" name="coe_radio"value="1" class="coe_radio" {{ ($isCoe == '') ? 'checked' : null }}>YES
                                                </label>
                                                <label class="radio-inline">
                                                    <input type="radio" name="coe_radio" value="0"  class="coe_radio" {{ ($isCoe == 'd-none') ? 'checked' : null }}>NO
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    {{-- {{dd("dsjfidhjohh")}} --}}
                                    {{-- {{dd($user_data[0]['coe_detail'][0]['type'])}} --}}
                                <div class="coe_container {{ $isDisplayCoe}}">
                                        
                                        @if(isset($user_data[0]['coe_detail']) && count($user_data[0]['coe_detail']) > 0)
                                            @foreach($user_data[0]['coe_detail'] as $index => $data)
                                                <div id="coe-template">
                                                    <div class="row">
                                                        <div class="col-xs-12">
                                                            <div class="input-label p-t-10 coe-name coe-name-0 pull-left">
                                                                COE Details {{$index+1}}</div>
                                                            @if($index > 0)
                                                                <div class="pull-right close-button coe-close-button p-t-10 coe-close-button-0"
                                                                     data-type='coe'>
                                                                    <i class="fa fa-times" aria-hidden="true"></i>
                                                                </div>
                                                            @else
                                                                <div class="pull-right close-button coe-close-button hide p-t-10 coe-close-button-0"
                                                                     data-type='coe'>
                                                                    <i class="fa fa-times" aria-hidden="true"></i>
                                                                </div>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <input type="hidden" class="total-count-coe">
                                                        <input type="hidden" class="form-control coe_id" name="coe_id[{{$index}}]" value="{{ isset($data['id']) ? $data['id'] : ''}}">
                                                        <div class="col-sm-3">
                                                            <div class="form-group">
                                                                <label class="input-label">COE</label>
                                                                <select class="form-control coecountry"
                                                                        name="coecountry[{{$index}}]">
                                                                    <option value="">Select COE Country</option>
                                                                    @foreach( \CommonHelper::countries() as $c_index => $country)
                                                                        <option value="{{ $c_index }}" {{ !empty($data['coe']) ? $data['coe'] == $c_index ? 'selected' : '' : ''  }}> {{ $country }}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-3">
                                                            <div class="form-group">
                                                                <label class="input-label" for="number">COE
                                                                    Number</label>
                                                                <input type="text" class="form-control coeno"
                                                                       name="coeno[{{$index}}]"
                                                                       value="{{ isset($data['coe_number']) ? $data['coe_number'] : ''}}"
                                                                       placeholder="Type alphanumeric number">
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-3">
                                                            <div class="form-group">
                                                                <label class="input-label" for="grade">COE Grade</label>
                                                                <input type="text" class="form-control coe_grade"
                                                                       name="coe_grade[{{$index}}]"
                                                                       value="{{ isset($data['coe_grade']) ? $data['coe_grade'] : ''}}"
                                                                       placeholder="Type your grade">
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-3">
                                                            <div class="form-group">
                                                                <label class="input-label">COE Expiry Date</label>
                                                                <input type="text"
                                                                       class="form-control datepicker coe_exp"
                                                                       name="coe_exp[{{$index}}]"
                                                                       value="{{ isset($data['coe_expiry_date']) ? date('d-m-Y',strtotime($data['coe_expiry_date'])) : ''}}"
                                                                       placeholder="dd-mm-yyyy">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <hr>
                                                </div>
                                            @endforeach
                                        @else
                                            <div id="coe-template">
                                                <div class="row">
                                                    <div class="col-xs-12">
                                                        <div class="input-label p-t-10 coe-name coe-name-0 pull-left">
                                                            COE
                                                            Details 1
                                                        </div>
                                                        <div class="pull-right close-button coe-close-button hide p-t-10 coe-close-button-0"
                                                             data-type='coe'>
                                                            <i class="fa fa-times" aria-hidden="true"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <input type="hidden" class="total-count-coe">
                                                    <div class="col-sm-3">
                                                        <div class="form-group">
                                                            <label class="input-label">COE<span
                                                                        class="symbol required"></span></label>
                                                            <select class="form-control coecountry"
                                                                    name="coecountry[0]">
                                                                <option value="">Select COE Country</option>
                                                                @foreach( \CommonHelper::countries() as $c_index => $country)
                                                                    <option value="{{ $c_index }}" {{ !empty($user_data[0]['coe_detail']) ? $user_data[0]['coe_detail']['coe'] == $c_index ? 'selected' : '' : ''  }}> {{ $country }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <div class="form-group">
                                                            <label class="input-label" for="number">COE Number<span
                                                                        class="symbol required"></span></label>
                                                            <input type="text" class="form-control coeno"
                                                                   name="coeno[0]"
                                                                   value="{{ isset($user_data[0]['coe_detail']['coe_number']) ? $user_data[0]['coe_detail']['coe_number'] : ''}}"
                                                                   placeholder="Type alphanumeric number">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <div class="form-group">
                                                            <label class="input-label" for="grade">COE Grade<span
                                                                        class="symbol required"></span></label>
                                                            <input type="text" class="form-control coe_grade"
                                                                   name="coe_grade[0]"
                                                                   value="{{ isset($user_data[0]['coe_detail']['coe_grade']) ? $user_data[0]['coe_detail']['coe_grade'] : ''}}"
                                                                   placeholder="Type your grade">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <div class="form-group">
                                                            <label class="input-label">COE Expiry Date<span
                                                                        class="symbol required"></span></label>
                                                            <input type="text" class="form-control datepicker coe_exp"
                                                                   name="coe_exp[0]"
                                                                   value="{{ isset($user_data[0]['coe_detail']['coe_expiry_date']) ? date('d-m-Y',strtotime($user_data[0]['coe_detail']['coe_expiry_date'])) : ''}}"
                                                                   placeholder="dd-mm-yyyy">
                                                        </div>
                                                    </div>
                                                </div>
                                                <hr>
                                            </div>
                                        @endif
                                        <div class="row" id="add-more-coe-row">
                                            <div class="col-sm-12">
                                                <button type="button" data-form-id="seafarer-certificates-form"
                                                        class="btn add-more-button add-coe-button" id="add-coe">Add More COE
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php
                                    $hide_wk = '';
                                    if(!in_array('WATCH_KEEPING-Optional', $required_fields_for_selected_rank)) {
                                        $hide_wk = '';
                                    }else{
                                        $hide_wk = 'hide';
                                    }

                                    if(in_array('WATCH_KEEPING', $required_fields_name)) {
                                        $div_class_wk = '';
                                    }else{
                                        $div_class_wk = 'hide';
                                    }
                                ?>

                                <div id='WATCH_KEEPING-details' class="{{$div_class_wk}} add-more-section">

                                    <div class="">
                                        <div class="row pb-10">
                                            <div class="heading col-xs-6">
                                                WATCH KEEPING FOR RATING<span
                                                        class="symbol required {{$hide_wk}}"></span>
                                            </div>
                                            <div class="heading col-xs-6">
                                                @php
                                                    $isWk = 'd-none';
                                                    $wkCopValue = '';
                                                    $_wk_cop = \App\UserWkfrDetail::whereUserId(Auth::id())->first();
                                                    if($_wk_cop){
                                                        $_wk_cop = $_wk_cop->wk_cop;
                                                        $wkCopValue = $_wk_cop;
                                                    }
                                                    if($_wk_cop == 'wk' || $_wk_cop == 'cop'){
                                                        $isWk = '';
                                                    }
                                                @endphp
                                                <label class="radio-inline"><input type="radio" name="wk_cop" class="wk_cop_radio"
                                                                                value="wk" {{($wkCopValue=='wk') ? 'checked' : ''}}>WK</label>
                                                <label class="radio-inline"><input type="radio" name="wk_cop" class="wk_cop_radio"
                                                                                value="cop" {{($wkCopValue=='cop') ? 'checked' : ''}}>COP</label>
                                                <label class="radio-inline"><input type="radio" name="wk_cop" class="wk_cop_radio"
                                                                                value="" {{($wkCopValue=='') ? 'checked' : ''}} >NILL</label>
                                                <input type="hidden" name="wk_cop_update_url"
                                                    value="{{route('wk_cop.update')}}">
                                            </div>
                                        </div>
                                        <div class="wk_cop_container row {{$isWk}}">
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label class="input-label">Country<span
                                                            class="symbol required {{$hide_wk}}"></span></label>
                                                <select class="form-control" id="watch_country" name="watch_country">
                                                    <option value="">Select Watch Keeping Country</option>
                                                    @foreach( \CommonHelper::countries() as $c_index => $country)
                                                        <option value="{{ $c_index }}" {{ !empty($user_data[0]['wkfr_detail']) ? $user_data[0]['wkfr_detail']['watch_keeping'] == $c_index ? 'selected' : '' : ''  }}> {{ $country }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label class="input-label" for="watchissud">Issue Date<span
                                                            class="symbol required {{$hide_wk}}"></span></label>
                                                <input type="text" class="form-control issue-datepicker" id="watchissud"
                                                    name="watchissud"
                                                    value="{{ isset($user_data[0]['wkfr_detail']['issue_date']) ? date('d-m-Y',strtotime($user_data[0]['wkfr_detail']['issue_date'])) : ''}}"
                                                    placeholder="dd-mm-yyyy">
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label class="input-label" for="watch_no">Number<span
                                                            class="symbol required {{$hide_wk}}"></span></label>
                                                <input type="text" class="form-control" id="watch_no" name="watch_no"
                                                    value="{{ isset($user_data[0]['wkfr_detail']['wkfr_number']) ? $user_data[0]['wkfr_detail']['wkfr_number'] : ''}}"
                                                    placeholder="Type number">
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label class="input-label" for="deckengine">Deck/Engine<span
                                                            class="symbol required {{$hide_wk}}"></span></label>
                                                <select id="deckengine" name="deckengine"
                                                        class="form-control selectpicker">
                                                    <option value="" label="Select">Select</option>
                                                    <option value="Deck" {{ !empty($user_data[0]['wkfr_detail']) ? $user_data[0]['wkfr_detail']['type'] == 'Deck' ? 'selected' : '' : ''}}>
                                                        Deck
                                                    </option>
                                                    <option value="Engine" {{ !empty($user_data[0]['wkfr_detail']) ? $user_data[0]['wkfr_detail']['type'] == 'Engine'? 'selected' : '' : ''}}>
                                                        Engine
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                                </div>
                                        
                                @if (isset($user_data[0]['professional_detail']) && !empty($user_data[0]['professional_detail']['current_rank']) && (in_array('DCE-Optional', \CommonHelper::rank_required_fields()[$user_data[0]['professional_detail']['current_rank']]) || in_array('DCE', \CommonHelper::rank_required_fields()[$user_data[0]['professional_detail']['current_rank']])))
                                    @php
                                        
                                        $dangerousCargoEndorsementStatus = !empty($user_data[0]['user_dangerous_cargo_endorsement_detail']) && isset($user_data[0]['user_dangerous_cargo_endorsement_detail']['status']) ? json_decode($user_data[0]['user_dangerous_cargo_endorsement_detail']['status']) : null;

                                        $isdangerousCargoEndorsement = (isset($dangerousCargoEndorsementStatus->status) && $dangerousCargoEndorsementStatus->status == 1) ? 1 : 0;
                                        $types = isset($dangerousCargoEndorsementStatus->type) ? $dangerousCargoEndorsementStatus->type : [];

                                        $isDangerousCargoEndorsementDetails = ($isdangerousCargoEndorsement == 0) ? 'd-none' : '';
                                        $isOil = ($isdangerousCargoEndorsement == 1 && in_array('oil', $types)) ? '' : 'd-none';
                                        $isChemical = ($isdangerousCargoEndorsement == 1 && in_array('chemical', $types)) ? '' : 'd-none';
                                        $isLequefiedGas = ($isdangerousCargoEndorsement == 1 && in_array('lequefied_gas', $types)) ? '' : 'd-none';
                                        $isAll = ($isdangerousCargoEndorsement == 1 && in_array('all', $types)) ? '' : 'd-none';
                                        $oilDetails = isset($user_data[0]['user_dangerous_cargo_endorsement_detail']['oil']) && !empty($user_data[0]['user_dangerous_cargo_endorsement_detail']['oil']) ? json_decode($user_data[0]['user_dangerous_cargo_endorsement_detail']['oil']) : null;
                                        $chemicalDetails = isset($user_data[0]['user_dangerous_cargo_endorsement_detail']['chemical']) && !empty($user_data[0]['user_dangerous_cargo_endorsement_detail']['chemical']) ? json_decode($user_data[0]['user_dangerous_cargo_endorsement_detail']['chemical']) : null;
                                        $lequefiedGasDetails = isset($user_data[0]['user_dangerous_cargo_endorsement_detail']['lequefied_gas']) && !empty($user_data[0]['user_dangerous_cargo_endorsement_detail']['lequefied_gas']) ? json_decode($user_data[0]['user_dangerous_cargo_endorsement_detail']['lequefied_gas']) : null;
                                        $allDetails = isset($user_data[0]['user_dangerous_cargo_endorsement_detail']['all']) && !empty($user_data[0]['user_dangerous_cargo_endorsement_detail']['all']) ? json_decode($user_data[0]['user_dangerous_cargo_endorsement_detail']['all']) : null;
                                    @endphp

                                    <div id='DCE-details' class="required_fields add-more-section">
                                        <div class="row pb-10">
                                            <div class="col-xs-6">
                                                <div class="heading">
                                                    DANGEROUS CARGO ENDORSEMENT
                                                </div>
                                            </div>
                                            <div class="col-xs-6">
                                                <div class="heading">
                                                    <div class="form-group">
                                                        <label class="radio-inline"><input type="radio" name="dce_radio" class="dce_radio"  value="1" {{  $isdangerousCargoEndorsement == 1 ? 'checked' : '' }}>YES</label>
                                                        <label class="radio-inline"><input type="radio" name="dce_radio" class="dce_radio"  value="0" {{  $isdangerousCargoEndorsement == 0 ? 'checked' : '' }}>NO</label>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="dce_container {{ $isDangerousCargoEndorsementDetails }}" >
                                            <div id="dce-template">
                                                <div class="row">
                                                    <div class="col-lg-2">
                                                        <label for="oil">Oil</label>
                                                    <input type="checkbox" id="oil" name="dce[]" value="oil" {{ ($isOil == '') ? 'checked' : '' }} class="dce-details" data-id="oil-details">
                                                    </div>
                                                    <div class="col-lg-3">
                                                        <label for="chemical">Chemical</label>
                                                        <input type="checkbox" id="chemical" name="dce[]" value="chemical"  {{ ($isChemical == '') ? 'checked' : '' }} class="dce-details" data-id="chemical-details">
                                                    </div>
                                                    <div class="col-lg-3">
                                                        <label for="lequefied_gas">Liquefied Gas</label>
                                                        <input type="checkbox" id="lequefied_gas" name="dce[]" value="lequefied_gas" {{ ($isLequefiedGas == '') ? 'checked' : '' }} class="dce-details" data-id="lequefied-gas-details">
                                                    </div>
                                                    <div class="col-lg-4">
                                                        <label for="all">Oil + Chemical + Liquefied Gas</label>
                                                        <input type="checkbox" id="all" name="dce[]" value="all" class="dce-details" {{ ($isAll == '') ? 'checked' : '' }} data-id="all-details">
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <span class="dce-types-required text-danger"></span>
                                                    </div>
                                                </div>
                                                <div class="row oil-details {{ $isOil }}">
                                                    <div class="col-sm-12">
                                                        <br />
                                                        <label for="oil">Oil</label>
                                                    </div>
                                                </div>
                                                <div class="row oil-details {{ $isOil }}">
                                                    <div class="col-sm-4">
                                                        <div class="form-group">
                                                            <label class="input-label">Grade</label>
                                                            <label class="radio-inline">
                                                                <input type="radio" class="dce-radio-details" name="oil[grade]" value="0" {{ (isset($oilDetails->grade) && $oilDetails->grade == 0) ? 'checked' : '' }}>Level I
                                                            </label>
                                                            <label class="radio-inline">
                                                                <input type="radio" class="dce-radio-details" name="oil[grade]" value="1" {{ (isset($oilDetails->grade) && $oilDetails->grade == 1) ? 'checked' : '' }}>Level II
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <div class="form-group">
                                                            <label class="input-label">Country</label>
                                                            <select class="form-control dce-select-details" name="oil[country]">
                                                                <option value="">Select Country</option>
                                                                @foreach( \CommonHelper::countries() as $c_index => $country)
                                                                    <option value="{{ $c_index }}" {{ isset($oilDetails->country) &&  ($oilDetails->country == $c_index) ? 'selected' : ''}}> {{ $country }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <div class="form-group">
                                                            <label class="input-label">Number</label>
                                                            <input type="text" name="oil[number]" class="form-control dce-text-details" value="{{ isset($oilDetails->number) &&  !empty($oilDetails->number) ? $oilDetails->number : null }}">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row oil-details {{ $isOil }}">
                                                    <div class="col-sm-4">
                                                        <div class="form-group">
                                                            <label class="input-label">Date of Issue</label>
                                                            <input type="text" class="form-control date-of-issue-datepicker dce-text-details" name="oil[date_of_issue]" placeholder="dd-mm-yyyy" value="{{ isset($oilDetails->date_of_issue) &&  !empty($oilDetails->date_of_issue) ? $oilDetails->date_of_issue : null }}">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <div class="form-group">
                                                            <label class="input-label">Date of Expiry</label>
                                                            <input type="text" class="form-control datepicker dce-text-details" name="oil[date_of_expiry]" placeholder="dd-mm-yyyy" value="{{ isset($oilDetails->date_of_expiry) &&  !empty($oilDetails->date_of_expiry) ? $oilDetails->date_of_expiry : null }}">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <div class="form-group">
                                                            <label class="input-label">Place of Issue</label>
                                                            <input type="text" name="oil[place_of_issue]" class="form-control dce-text-details" value="{{ isset($oilDetails->place_of_issue) &&  !empty($oilDetails->place_of_issue) ? $oilDetails->place_of_issue : null }}">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row chemical-details {{ $isChemical }}">
                                                    <div class="col-sm-12">
                                                        <br />
                                                        <label for="chemical">Chemical</label>
                                                    </div>
                                                </div>
                                                <div class="row chemical-details {{ $isChemical }}">
                                                    <div class="col-sm-4">
                                                        <div class="form-group">
                                                            <label class="input-label">Grade</label>
                                                            <label class="radio-inline">
                                                                <input type="radio" class="dce-radio-details" name="chemical[grade]" value="0"  {{ (isset($chemicalDetails->grade) && $chemicalDetails->grade == 0) ? 'checked' : '' }}>Level I
                                                            </label>
                                                            <label class="radio-inline">
                                                                <input type="radio" class="dce-radio-details" name="chemical[grade]" value="1"  {{ (isset($chemicalDetails->grade) && $chemicalDetails->grade == 1) ? 'checked' : '' }}>Level II
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <div class="form-group">
                                                            <label class="input-label">Country</label>
                                                            <select class="form-control dce-select-details" name="chemical[country]">
                                                                <option value="">Select Country</option>
                                                                @foreach( \CommonHelper::countries() as $c_index => $country)
                                                                    <option value="{{ $c_index }}" {{ isset($chemicalDetails->country) &&  ($chemicalDetails->country == $c_index) ? 'selected' : ''}}> {{ $country }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <div class="form-group">
                                                            <label class="input-label">Number</label>
                                                            <input type="text" name="chemical[number]" class="form-control dce-text-details" value="{{ isset($chemicalDetails->number) &&  !empty($chemicalDetails->number) ? $chemicalDetails->number : null }}">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row chemical-details {{ $isChemical }}">
                                                    <div class="col-sm-4">
                                                        <div class="form-group">
                                                            <label class="input-label">Date of Issue</label>
                                                            {{-- {{dd($chemicalDetails)}} --}}
                                                            <input type="text" class="form-control date-of-issue-datepicker dce-text-details" name="chemical[date_of_issue]" placeholder="dd-mm-yyyy" value="{{ isset($chemicalDetails->date_of_issue) &&  !empty($chemicalDetails->date_of_issue) ? $chemicalDetails->date_of_issue : null }}">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <div class="form-group">
                                                            <label class="input-label">Date of Expiry</label>
                                                            <input type="text" class="form-control datepicker dce-text-details" name="chemical[date_of_expiry]" placeholder="dd-mm-yyyy" value="{{ isset($chemicalDetails->date_of_expiry) &&  !empty($chemicalDetails->date_of_expiry) ? $chemicalDetails->date_of_expiry : null }}">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <div class="form-group">
                                                            <label class="input-label">Place of Issue</label>
                                                            <input type="text" name="chemical[place_of_issue]" class="form-control dce-text-details" value="{{ isset($chemicalDetails->place_of_issue) &&  !empty($chemicalDetails->place_of_issue) ? $chemicalDetails->place_of_issue : null }}">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row lequefied-gas-details {{ $isLequefiedGas }}">
                                                    <div class="col-sm-12">
                                                        <br />
                                                        <label for="lequefied_gas">Liquefied Gas</label>
                                                    </div>
                                                </div>
                                                <div class="row lequefied-gas-details {{ $isLequefiedGas }}">
                                                    <div class="col-sm-4">
                                                        <div class="form-group">
                                                            <label class="input-label">Grade</label>
                                                            <label class="radio-inline">
                                                                <input type="radio" class="dce-radio-details" name="lequefied_gas[grade]" value="0"  {{ (isset($lequefiedGasDetails->grade) && $lequefiedGasDetails->grade == 0) ? 'checked' : '' }}>Level I
                                                            </label>
                                                            <label class="radio-inline">
                                                                <input type="radio" class="dce-radio-details" name="lequefied_gas[grade]" value="1" {{ (isset($lequefiedGasDetails->grade) && $lequefiedGasDetails->grade == 1) ? 'checked' : '' }}>Level II
                                                            </label>
                                                        </div>
                                                    </div>
                                                    {{-- {{dd($lequefiedGasDetails)}} --}}
                                                    <div class="col-sm-4">
                                                        <div class="form-group">
                                                            <label class="input-label">Country</label>
                                                            <select class="form-control dce-select-details" name="lequefied_gas[country]">
                                                                <option value="">Select Country</option>
                                                                @foreach( \CommonHelper::countries() as $c_index => $country)
                                                                    <option value="{{ $c_index }}" {{ isset($lequefiedGasDetails->country) &&  ($lequefiedGasDetails->country == $c_index) ? 'selected' : ''}}> {{ $country }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <div class="form-group">
                                                            <label class="input-label">Number</label>
                                                            <input type="text" name="lequefied_gas[number]" class="form-control dce-text-details" value="{{ isset($lequefiedGasDetails->number) &&  !empty($lequefiedGasDetails->number) ? $lequefiedGasDetails->number : null }}">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row lequefied-gas-details {{ $isLequefiedGas }}">
                                                    <div class="col-sm-4">
                                                        <div class="form-group">
                                                            <label class="input-label">Date of Issue</label>
                                                            <input type="text" class="form-control date-of-issue-datepicker dce-text-details" name="lequefied_gas[date_of_issue] dce-text-details" placeholder="dd-mm-yyyy" value="{{ isset($lequefiedGasDetails->date_of_issue) &&  !empty($lequefiedGasDetails->date_of_issue) ? $lequefiedGasDetails->date_of_issue : null }}">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <div class="form-group">
                                                            <label class="input-label">Date of Expiry</label>
                                                            <input type="text" class="form-control datepicker dce-text-details" name="lequefied_gas[date_of_expiry] dce-text-details" placeholder="dd-mm-yyyy" value="{{ isset($lequefiedGasDetails->date_of_expiry) &&  !empty($lequefiedGasDetails->date_of_expiry) ? $lequefiedGasDetails->date_of_expiry : null }}">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <div class="form-group">
                                                            <label class="input-label">Place of Issue</label>
                                                            <input type="text" name="lequefied_gas[place_of_issue]" class="form-control dce-text-details" value="{{ isset($lequefiedGasDetails->place_of_issue) &&  !empty($lequefiedGasDetails->place_of_issue) ? $lequefiedGasDetails->place_of_issue : null }}">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row all-details {{ $isAll }}">
                                                    <div class="col-sm-12">
                                                        <br />
                                                        <label for="all">Oil + Chemical + Liquefied Gas</label>
                                                    </div>
                                                </div>
                                                <div class="row all-details {{ $isAll }}">
                                                    <div class="col-sm-4">
                                                        <div class="form-group">
                                                            <label class="input-label">Grade</label>
                                                            <label class="radio-inline">
                                                                <input type="radio" class="dce-radio-details" name="all[grade]" value="0"  {{ (isset($allDetails->grade) && $allDetails->grade == 0) ? 'checked' : '' }}>Level I
                                                            </label>
                                                            <label class="radio-inline">
                                                                <input type="radio" class="dce-radio-details" name="all[grade]" value="1"  {{ (isset($allDetails->grade) && $allDetails->grade == 1) ? 'checked' : '' }}>Level II
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <div class="form-group">
                                                            <label class="input-label">Country</label>
                                                            <select class="form-control dce-select-details" name="all[country]">
                                                                <option value="">Select Country</option>
                                                                @foreach( \CommonHelper::countries() as $c_index => $country)
                                                                    <option value="{{ $c_index }}"  {{ isset($allDetails->country) &&  ($allDetails->country == $c_index) ? 'selected' : ''}}> {{ $country }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <div class="form-group">
                                                            <label class="input-label">Number</label>
                                                            <input type="text" name="all[number]" class="form-control dce-text-details" value="{{ isset($allDetails->number) &&  !empty($allDetails->number) ? $allDetails->number : null }}">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row all-details {{ $isAll }}">
                                                    <div class="col-sm-4">
                                                        <div class="form-group">
                                                            <label class="input-label">Date of Issue</label>
                                                            <input type="text" class="form-control date-of-issue-datepicker dce-text-details" name="all[date_of_issue]" placeholder="dd-mm-yyyy" value="{{ isset($allDetails->date_of_issue) &&  !empty($allDetails->date_of_issue) ? $allDetails->date_of_issue : null }}">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <div class="form-group">
                                                            <label class="input-label">Date of Expiry</label>
                                                            <input type="text" class="form-control datepicker dce-text-details" name="all[date_of_expiry]" placeholder="dd-mm-yyyy" value="{{ isset($allDetails->date_of_expiry) &&  !empty($allDetails->date_of_expiry) ? $allDetails->date_of_expiry : null }}">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <div class="form-group">
                                                            <label class="input-label">Place of Issue</label>
                                                            <input type="text" name="all[place_of_issue]" class="form-control dce-text-details" value="{{ isset($allDetails->place_of_issue) &&  !empty($allDetails->place_of_issue) ? $allDetails->place_of_issue : null }}">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                                        <?php
                                        $isGmdssDetails = 'd-none';
                                        if ($user_data[0]['gmdss_detail'] && count($user_data[0]['gmdss_detail']) > 0) {
                                            $isGmdssDetails = '';
                                        }
                                        if (in_array('GMDSS', $required_fields_name)) {
                                            $div_class_gmdss = '';
                                        } else {
                                            $div_class_gmdss = 'hide';
                                        }

                                        if (in_array('GMDSS-Optional', $required_fields_for_selected_rank)) {
                                            $hide_gmdss = 'hide';
                                        } else {
                                            $hide_gmdss = '';
                                        }
                                        ?>

                                        <div id='GMDSS-details' class="required_fields {{$div_class_gmdss}} add-more-section">
                                            <div class="row pb-10">
                                                <div class="col-xs-6">
                                                    <div class="heading">
                                                        GMDSS DETAILS<span class="symbol required {{$hide_gmdss}}"></span>
                                                    </div>
                                                </div>
                                                <div class="col-xs-6">
                                                    <div class="heading">
                                                        <label class="radio-inline">
                                                            <input type="radio" name="gmdss_radio" value="1" class="gmdss_radio" {{ $isGmdssDetails == '' ? 'checked' : null }}>YES
                                                        </label>
                                                        <label class="radio-inline">
                                                            <input type="radio" name="gmdss_radio" value="0"  class="gmdss_radio" {{ $isGmdssDetails == 'd-none' ? 'checked' : null }}>NO
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="gmdss_container {{ $isGmdssDetails }}">
                                                <div class="row">
                                                    <div class="col-sm-4">
                                                        <div class="form-group">
                                                            <label class="input-label">GMDSS
                                                                <span class="symbol required {{$hide_gmdss}}"></span>
                                                            </label>
                                                            <select class="form-control" id="gmdss_country" name="gmdss_country" class="gmdss-text-details">
                                                                <option value="">Select GMDSS Country</option>
                                                                @foreach( \CommonHelper::countries() as $c_index => $country)
                                                                    <option value="{{ $c_index }}" {{ !empty($user_data[0]['gmdss_detail']) ? $user_data[0]['gmdss_detail']['gmdss'] == $c_index ? 'selected' : '' : ''  }}> {{ $country }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <div class="form-group">
                                                            <label class="input-label" for="gmdss_no">GMDSS Number<span
                                                                        class="symbol required {{$hide_gmdss}}"></span></label>
                                                            <input type="text" class="form-control gmdss-text-details" id="gmdss_no" name="gmdss_no" value="{{ isset($user_data[0]['gmdss_detail']['gmdss_number']) ? $user_data[0]['gmdss_detail']['gmdss_number'] : ''}}" placeholder="Type alphanumeric number">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <div class="form-group">
                                                            <label class="input-label" for="gmdss_doe">GMDSS Expiry Date
                                                                <span class="symbol required {{$hide_gmdss}}"></span>
                                                            </label>
                                                            <input type="text" class="form-control datepicker gmdss-text-details" id="gmdss_doe" name="gmdss_doe" value="{{ isset($user_data[0]['gmdss_detail']['gmdss_expiry_date']) ? date('d-m-Y',strtotime($user_data[0]['gmdss_detail']['gmdss_expiry_date'])) : ''}}" placeholder="dd-mm-yyyy">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row gmdss_endorsement {{isset($user_data[0]['gmdss_detail']['gmdss']) && $user_data[0]['gmdss_detail']['gmdss'] == $india_value ? '' : 'hide'}}">
                                                    <div class="col-sm-12">
                                                        <div class="heading">
                                                            GMDSS Endorsement<span class="symbol required {{$hide_gmdss}}"></span>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4 gmdss_endorsement {{isset($user_data[0]['gmdss_detail']['gmdss']) && $user_data[0]['gmdss_detail']['gmdss'] == $india_value ? '' : 'hide'}}">
                                                        <div class="form-group">
                                                            <label class="input-label" for="gmdss_endorsement">GMDSS Endorsement Number</label>
                                                            <input type="text" class="form-control gmdss-text-details" name="gmdss_endorsement" value="{{isset($user_data[0]['gmdss_detail']['gmdss_endorsement_number']) ? $user_data[0]['gmdss_detail']['gmdss_endorsement_number'] : ''}}" placeholder="Please Enter Number">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4 gmdss_valid_till {{isset($user_data[0]['gmdss_detail']['gmdss']) && $user_data[0]['gmdss_detail']['gmdss'] == $india_value ? '' : 'hide'}}">
                                                        <div class="form-group">
                                                            <label class="input-label" for="gmdss_valid_till">Valid Till</label>
                                                            <input type="text" class="form-control datepicker gmdss-text-details" name="gmdss_valid_till" value="{{isset($user_data[0]['gmdss_detail']['gmdss_endorsement_expiry_date']) ? date('d-m-Y',strtotime($user_data[0]['gmdss_detail']['gmdss_endorsement_expiry_date'])) : ''}}" placeholder="dd-mm-yyyy">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        


                                    </div>
                                

                            <div class="reg-form-btn-container">
                                @if(!isset($user_data['current_route']))
                                    <button type="button" class="reg-form-btn gray" id="imp-documents-back-button"
                                            style="margin-bottom: 60px;">Back
                                    </button>
                                @endif
                                <button type="submit" data-style="zoom-in"
                                        class="reg-form-btn blue ladda-button document-details-submit"
                                        id="submit-imp-documents-button" value="Save" data-tab-index="2"
                                        style="margin-bottom: 60px;">Save & Next
                                </button>
                            </div>
                        </form>
                        <form id="seafarer-sea-service-form" class="hide"
                              action="{{route('site.user.service.details')}}" method="post">
                            {{ csrf_field() }}
                            <div class="user-details-container">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="main-heading">
                                            Sea Service Details<span class="symbol required"></span>
                                        </div>
                                    </div>
                                </div>

                                <div id="sea-service-template" class="sea-service-template  add-more-section">
                                    <input type="hidden" name="exiting_service_id" class="exiting_service_id">
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="input-label" for="rank">Rank<span
                                                            class="symbol required"></span></label>
                                                <select name="rank[0]" class="form-control search-select rank"
                                                        block-index="0">
                                                    <option value=''>Select Rank</option>
                                                    @foreach(\CommonHelper::new_rank() as $index => $category)
                                                        <optgroup label="{{$index}}">
                                                            @foreach($category as $r_index => $rank)
                                                                <option value="{{$r_index}}">{{$rank}} </option>
                                                    @endforeach
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="input-label" for="shipping_company">Shipping Company<span
                                                            class="symbol required"></span></label>
                                                <input type="text" class="form-control shipping_company"
                                                       name="shipping_company[0]"
                                                       placeholder="Type shipping company name"/>
                                                <span role="status" aria-live="polite"
                                                      class="ui-helper-hidden-accessible hide"></span>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="input-label" for="manning_by">Manning By<span
                                                            class="symbol "></span></label>
                                                <input type="text" class="form-control manning_by"
                                                       name="manning_by[0]" placeholder="Manning By"/>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="input-label" for="name_of_ship">Name Of Ship<span
                                                            class="symbol required"></span></label>
                                                <input type="text" class="form-control name_of_ship"
                                                       name="name_of_ship[0]" placeholder="Type your ship name"
                                                       id="name_of_ship">
                                                <span role="status" aria-live="polite"
                                                      class="ui-helper-hidden-accessible hide"></span>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="input-label" for="ship_type">Ship Type<span
                                                            class="symbol required"></span></label>
                                                <select name="ship_type[0]" class="form-control search-select ship_type"
                                                        block-index="0">
                                                    <option value=''>Select Your ship type</option>
                                                    @foreach( \CommonHelper::ship_type() as $c_index => $type)
                                                        <option value="{{ $c_index }}"> {{ $type }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="input-label" for="ship_flag[0]">Ship Flag</label>
                                                <select block-index="0" class="form-control search-select ship_flag"
                                                        name="ship_flag[0]">
                                                    <option value=''>Select Ship Flag</option>
                                                    @foreach( \CommonHelper::countries() as $c_index => $name)
                                                        <option value="{{ $c_index }}"> {{ $name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="input-label" for="grt">GRT</label>
                                                <input type="text" class="form-control grt" name="grt[0]"
                                                       placeholder="Type your grt"/>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="input-label" for="engine_type[0]">Engine Type</label>
                                                <select name="engine_type[0]"
                                                        class="form-control search-select check_engine_type engine_type"
                                                        block-index="0">
                                                    <option value=''>Select Your engine type</option>
                                                    @foreach( \CommonHelper::engine_type() as $c_index => $type)
                                                        <option value="{{ $c_index }}"> {{ $type }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-4 hide other_engine_type-0" id="other_engine_type">
                                            <div class="form-group">
                                                <label class="input-label" for="other_engine_type">Other Engine
                                                    Type<span class="symbol required"></span></label>
                                                <input type="text" class="form-control other_engine_type"
                                                       name="other_engine_type[0]" placeholder="Type your engine type">
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="input-label" for="bhp[0]">BHP</label>
                                                <input type="text" class="form-control bhp" name="bhp[0]"
                                                       placeholder="Type your bhp"/>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="input-label" for="date_from">Sign On Date<span
                                                            class="symbol required"></span></label>
                                                <input type="text" class="form-control issue-datepicker date_from"
                                                       id="date_from" name="date_from[0]" placeholder="dd-mm-yyyy">
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="input-label" for="date_to">Sign Off Date
                                                    <span class="symbol required"></span>
                                                </label>
                                                <input type="text" class="form-control issue-datepicker date_to" name="date_to[0]" placeholder="dd-mm-yyyy">
                                                <label class="text-danger still-on-board">Leave blank for still Onboard</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row" id="add-more-service-row">
                                        <div class="col-sm-12">
                                            <button type="button" data-style="zoom-in"
                                                    data-form-id="seafarer-sea-service-form-form"
                                                    class="btn add-sea-service-button add-more-button ladda-button">Add
                                                Sea Service
                                            </button>
                                            <button type="reset" data-form-id="seafarer-sea-service-form-form"
                                                    class="btn btn-primary reset-btn-right ladda-button pull-right reset-sea-service-button"
                                                    data-style="zoom-in">Reset
                                            </button>
                                        </div>
                                    </div>
                                    <hr>
                                </div>

                                <div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="heading">
                                                SEA SERVICES
                                            </div>
                                        </div>
                                    </div>
                                    <div class="sea_service_detail_section">
                                        @if(isset($user_data[0]['sea_service_detail']) && !empty($user_data[0]['sea_service_detail']))
                                            <?php
                                            $show_skip_button = 'hide';
                                            $show_next_button = '';
                                            ?>
                                            @foreach($user_data[0]['sea_service_detail'] as $index => $services)
                                                <div class="row sea_service_detail_row sea_service_detail_row_{{$services['id']}}"
                                                     data-service-id={{$services['id']}} data-date="{{ isset($services['from']) ? date('d-m-Y',strtotime($services['from'])) : ''}}">
                                                    <div class="col-sm-12">
                                                        <div class="sub-details-container add-more-section">
                                                            <div class="content-container">
                                                                <div class="row">
                                                                    <div class="col-sm-12">
                                                                        <div class="m-b-5 display-flex-center">
                                                                            <div class="title sea-service-name">
                                                                                Sea Service
                                                                            </div>
                                                                            <div class="row"
                                                                                 style="flex-grow: 1; padding-left: 22px;">
                                                                                <div class="col-xs-6 col-md-4">
                                                                                    <div class="discription">
                                                                                        <span class="content-head">Sign On Date:</span>
                                                                                        <span class="content">
                                                                                            {{ isset($services['from']) ? date('d-m-Y',strtotime($services['from'])) : ''}}
                                                                                        </span>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-xs-6">
                                                                                    <div class="discription">
                                                                                        <span class="content-head">Sign Off Date:</span>
                                                                                        <span class="content">
                                                                                            {{ isset($services['to']) ? date('d-m-Y',strtotime($services['to'])) : 'On Board'}}
                                                                                        </span>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="title sea-service-buttons">
                                                                                <div class="sea-service-edit-button"
                                                                                     data-index-id={{$index}} data-service-id={{$services['id']}}>
                                                                                    <i class="fa fa-edit"
                                                                                       aria-hidden="true"
                                                                                       title="edit"></i>
                                                                                </div>
                                                                                <div class="sea-service-close-button"
                                                                                     data-id={{$services['id']}} data-service-id={{$services['id']}} data-from='{{$services["from"]}}'
                                                                                     data-to='{{$services["to"]}}'>
                                                                                    <i class="fa fa-times"
                                                                                       aria-hidden="true"
                                                                                       title="delete"></i>
                                                                                </div>
                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row sea_service_details_section">
                                                                <div class="col-xs-12 col-sm-6 col-md-4">
                                                                    <div class="discription">
                                                                        <span class="content-head">Rank:</span>
                                                                        <span class="content">
                                                                    @foreach(\CommonHelper::new_rank() as $index => $category)
                                                                                @foreach($category as $r_index => $rank)
                                                                                    {{ isset($services['rank_id']) ? $services['rank_id'] == $r_index ? $rank : '' : ''}}
                                                                                @endforeach
                                                                            @endforeach
                                                                    </span>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-6 col-md-4">
                                                                    <div class="discription">
                                                                        <span class="content-head">Shipping Company:</span>
                                                                        <span class="content">
                                                                        {{ isset($services['company_name']) ? $services['company_name'] : ''}}
                                                                    </span>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-6 col-md-4">
                                                                    <div class="discription">
                                                                        <span class="content-head">Manning By:</span>
                                                                        <span class="content">
                                                                        @if(isset($services['manning_by']) && !empty($services['manning_by']))
                                                                                {{$services['manning_by']}}
                                                                            @else
                                                                                -
                                                                            @endif
                                                                    </span>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-6 col-md-4">
                                                                    <div class="discription">
                                                                        <span class="content-head">Name Of Ship:</span>
                                                                        <span class="content">
                                                                        {{ isset($services['ship_name']) ? $services['ship_name'] : '-'}}
                                                                    </span>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-6 col-md-4">
                                                                    <div class="discription">
                                                                        <span class="content-head">Ship Type:</span>
                                                                        <span class="content">
                                                                    @foreach( \CommonHelper::ship_type() as $c_index => $type)
                                                                                {{ isset($services['ship_type']) ? $services['ship_type'] == $c_index ? $type : '' : ''}}
                                                                            @endforeach
                                                                    </span>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-6 col-md-4">
                                                                    <div class="discription">
                                                                        <span class="content-head">Ship Flag:</span>
                                                                        <span class="content">
                                                                        @if(isset($services['ship_flag']) && !empty($services['ship_flag']))
                                                                                @foreach( \CommonHelper::countries() as $c_index => $name)
                                                                                    {{ isset($services['ship_flag']) ? $services['ship_flag'] == $c_index ? $name : '' : ''}}
                                                                                @endforeach
                                                                            @else
                                                                                -
                                                                            @endif
                                                                    </span>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-6 col-md-4">
                                                                    <div class="discription">
                                                                        <span class="content-head">GRT:</span>
                                                                        <span class="content">
                                                                        {{ !empty($services['grt']) ? $services['grt'] : '-'}}
                                                                    </span>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-6 col-md-4">
                                                                    <div class="discription">
                                                                        <span class="content-head">BHP:</span>
                                                                        <span class="content">
                                                                        {{ !empty($services['bhp']) ? $services['bhp'] : '-'}}
                                                                    </span>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-6 col-md-4">
                                                                    <div class="discription">
                                                                        <span class="content-head">Engine Type:</span>
                                                                        <span class="content">
                                                                            @if(isset($services['engine_type']) && !empty($services['engine_type']))
                                                                                @if(isset($services['engine_type']) && $services['engine_type'] == '11')
                                                                                    {{ isset($services['other_engine_type']) ? $services['other_engine_type'] : '-'}}
                                                                                @else
                                                                                    @foreach( \CommonHelper::engine_type() as $c_index => $type)
                                                                                        {{ isset($services['engine_type']) ? $services['engine_type'] == $c_index ? $type  : '' : ''}}
                                                                                    @endforeach
                                                                                @endif
                                                                            @else
                                                                                -
                                                                            @endif
                                                                    </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                        @else
                                            <?php
                                            $show_skip_button = '';
                                            $show_next_button = 'hide'
                                            ?>
                                            <div class="row no-data-found">
                                                <div class="col-xs-12 text-center">
                                                    <div class="discription">
                                                        <span class="content-head">No Data Found</span>
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 no-padding">
                                    <div class="reg-form-btn-container col-sm-6 col-xs-6">
                                        <button type="button" data-style="zoom-in"
                                                class="reg-form-btn blue ladda-button sea-service-skip-button {{$show_skip_button}}"
                                                id="sea-service-skip-button" data-tab-index="4"
                                                style="margin-bottom: 60px;">Skip
                                        </button>
                                    </div>
<!--                                    <div class="reg-form-btn-container text-right col-sm-6 col-xs-6">
                                        <button type="button" data-style="zoom-in"
                                                class="reg-form-btn blue ladda-button {{$show_next_button}}"
                                                id="sea-service-details-submit" data-tab-index="4"
                                                style="margin-bottom: 60px;">Next
                                        </button>
                                    </div>-->
                                </div>
                            </div>
                        </form>
                        <form id="seafarer-certificates-form" name="seafarer-certificates-form" class="hide"
                              action="{{route('site.user.course.details')}}" method="post">
                            {{ csrf_field() }}

                            <div class="user-details-container">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="main-heading">
                                            Course Details
                                        </div>
                                    </div>
                                </div>
                                

                                @if(Route::currentRouteName() != 'site.seafarer.registration')
                                    <div id="seafarer_courses_section" class="add-more-section">
                                        <div class="">
                                            <div class="col-sm-12">
                                                <div class="heading">
                                                    POST SEA COURSES
                                                </div>
                                            </div>
                                        </div>
                                        <input type="hidden" class="existing_course_id" name="existing_course_id">
                                        <div class="">
<!--                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label class="input-label control-label" for="certificate_name">Course
                                                        Type<span class="symbol required"></span></label>
                                                    <select name="seafarer_course_type" id="seafarer_course_type"
                                                            class="form-control search-select course_type_filter">
                                                        <option value="">Select Your Course Type</option>

                                                        @foreach(\CommonHelper::institute_course_types() as $r_index => $rank)
                                                            <option value="{{$r_index}}" {{ isset($filter['course_type']) ? $filter['course_type'] == $r_index ? 'selected' : '' : ''}}>{{$rank}} </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>-->
                                            <div class="col-sm-8">
                                                <div class="form-group">
                                                    <label class="input-label control-label" for="certificate_name">Certificate
                                                        Name<span class="symbol required"></span></label>
                                                    <select name="certificate_name"
                                                            class="form-control search-select certificate_name"
                                                            block-index="0" id="certificate_name">
                                                        <option value=''>Select Your Certificate</option>
                                                        @foreach(\CommonHelper::courses_by_table() as $courses => $course)
                                                            <option value="{{$course->id}}">{{$course->course_name}} </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label class="input-label" for="place_of_issue">Certificate
                                                        Number</label>
                                                    <input type="text" class="form-control certificate_no"
                                                           name="certificate_no"
                                                           value="{{ isset($courses['certification_number']) ? $courses['certification_number'] : ''}}"
                                                           placeholder="Type your certificate number">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="">
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label class="input-label" for="date_of_issue">Date Of Issue</span>
                                                        <span class="symbol required"></label>
                                                    <input type="text"
                                                           class="form-control issue-datepicker date_of_issue"
                                                           name="date_of_issue"
                                                           value="{{ isset($courses['issue_date']) ? date('d-m-Y',strtotime($courses['issue_date'])) : ''}}"
                                                           placeholder="dd-mm-yyyy"/>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label class="input-label" for="date_of_expiry">Date Of
                                                        Expiry</label>
                                                    <input type="text" class="form-control datepicker date_of_expiry"
                                                           name="date_of_expiry"
                                                           value="{{ isset($courses['expiry_date']) ? date('d-m-Y',strtotime($courses['expiry_date'])) : ''}}"
                                                           placeholder="dd-mm-yyyy"/>
                                                    <span class="text-danger"><b>Leave blank for unlimited</b></span>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label class="input-label" for="place_of_issue">Issued By</label>
                                                    <input type="text" class="form-control issue_by" name="issue_by"
                                                           value="{{ isset($courses['issue_by']) ? $courses['issue_by'] : ''}}"
                                                           placeholder="Type your issued by institute">
                                                    <span role="status" aria-live="polite"
                                                          class="text-danger ui-helper-hidden-accessible hide"></span>
                                                          <b class="text-danger">Try using abbreviations or short forms</b>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <button type="button" data-form-id="seafarer-certificates-form"
                                                        class="btn add-seafarer-course-button add-more-button ladda-button"
                                                        data-style="zoom-in">Add Certificate
                                                </button>
                                                <button type="reset" data-form-id="seafarer-certificates-form"
                                                        class="btn btn-primary reset-btn-right ladda-button pull-right reset-course-button"
                                                        data-style="zoom-in">Reset
                                                </button>
                                            </div>
                                        </div>
                                    </div>

                                @endif

                                <div class="row section-title">
                                    <div class="col-sm-12">
                                        <div class="main-heading">
                                            Courses
                                        </div>
                                    </div>
                                </div>

                                <?php
                                $courses = \CommonHelper::courses();
                                ?>
                                @if(Route::currentRouteName() == 'site.seafarer.registration')
                                    <label for="checkbox" class="error"></label>
                                    <div class="row">
                                        <div class="form-group">
                                            <div class="col-sm-4 col-xs-8">
                                                <label>Course Name</label>
                                            </div>
                                            <div class="col-sm-2 col-xs-4">
                                                <label>Date Of Issue</label>
                                            </div>
                                            <div class="col-sm-4 col-xs-8">
                                                <label>Course Name</label>
                                            </div>
                                            <div class="col-sm-2 col-xs-4">
                                                <label>Date Of Issue</label>
                                            </div>
                                        </div>
                                    </div>
                                    @for($i=1;  $i <= (count($courses)); $i++)
                                        <div class="row">
                                            <div class="form-group">
                                                <div class="col-sm-4 col-xs-8 p-t-10">
                                                    <input type="checkbox" name="checkbox" class="course_checkbox"
                                                           data-id="{{$i}}">
                                                    <label class="f-14">
                                                        {{$courses[$i]}}
                                                    </label>
                                                </div>
                                                <div class="col-sm-2 col-xs-4">
                                                    <div class="hide date_of_issue_{{$i}}" data-check="off">
                                                        <input type="text" name="course_date_of_issue[{{$i}}]"
                                                               class="form-control issue-datepicker"
                                                               placeholder="dd-mm-yyyy">
                                                    </div>
                                                </div>
                                                <?php
                                                $i++;
                                                ?>
                                                @if($i < count($courses))
                                                    <div class="col-sm-4 col-xs-8 p-t-10">
                                                        <input type="checkbox" name="checkbox" class="course_checkbox"
                                                               data-id="{{$i}}">
                                                        <label class="f-14">
                                                            {{$courses[$i]}}
                                                        </label>
                                                    </div>
                                                    <div class="col-sm-2 col-xs-4">
                                                        <div class="hide date_of_issue_{{$i}}" data-check="off">
                                                            <input type="text" name="course_date_of_issue[{{$i}}]"
                                                                   class="form-control issue-datepicker"
                                                                   placeholder="dd-mm-yyyy">
                                                        </div>
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                    @endfor
                                @else
                                    <?php $course_count = '0';?>
                                    <div class="normal_course_section">
                                        @if(isset($user_data[0]['course_detail']) && !empty($user_data[0]['course_detail']))
                                            <?php $normal_course_count = 1; ?>
                                            @foreach($user_data[0]['course_detail'] as $index => $courses)
                                                <div class="row course_detail_row course_detail_row_{{$courses['id']}}"
                                                     data-course-id={{$courses['id']}}>
                                                    <div class="col-sm-12">
                                                        <div class="sub-details-container add-more-section m-t-0">
                                                            <div class="content-container">
                                                                <div class="row">
                                                                    
                                                                </div>
                                                            </div>
                                                            <div class="row sea_service_details_section">
                                                                <div class="col-xs-11 col-sm-11 col-md-11" style="margin-bottom: 10px;">
                                                                    <div class="discription">
                                                                        <span class="content-head">Course Name:</span>
                                                                        <span class="content"><b>
                                                                                <?php $arrData = \CommonHelper::course_name_by_course_type($courses['course_type']);

                                                                                foreach ($arrData as $val) {
                                                                                    if ($courses['course_id'] ==
                                                                                        $val->id) {
                                                                                        echo $val->course_name;
                                                                                    }
                                                                                }

                                                                                ?>
                                                                            </b>
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-1">
                                                                        <div class="title m-b-5 display-flex-center">
                                                                            <div class="normal-course-name">
                                                                            </div>
                                                                            <div class="sea-service-buttons">
                                                                                <div class="course-edit-button"
                                                                                     data-id={{$courses['id']}}>
                                                                                    <i class="fa fa-edit"
                                                                                       aria-hidden="true"
                                                                                       title="edit"></i>
                                                                                </div>
                                                                                <div class="course-close-button"
                                                                                     data-id='{{$courses['id']}}'
                                                                                     data-course-id='{{$courses['course_id']}}'>
                                                                                    <i class="fa fa-times"
                                                                                       aria-hidden="true"
                                                                                       title="delete"></i>
                                                                                </div>
                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                <div class="col-xs-12 col-sm-6 col-md-4" style="margin-bottom: 10px;">
                                                                    <div class="discription">
                                                                        <span class="content-head">Course Type:</span>
                                                                        <span class="content">

                                                                                @foreach(\CommonHelper::institute_course_types() as $c_index => $course)
                                                                                {{ isset($courses['course_type']) ? $courses['course_type'] == $c_index  ? $course : '' : ''}}
                                                                            @endforeach

                                                                        </span>
                                                                    </div>
                                                                </div>
                                                                

                                                                <div class="col-xs-12 col-sm-6 col-md-5" style="margin-bottom: 10px;">
                                                                    <div class="discription">
                                                                        <span class="content-head">Certificate Number:</span>
                                                                        <span class="content">
                                                                        {{ isset($courses['certification_number']) ? $courses['certification_number'] : '-'}}
                                                                    </span>
                                                                    </div>
                                                                </div>

                                                                <div class="col-xs-12 col-sm-6 col-md-3" style="margin-bottom: 10px;">
                                                                    <div class="discription">
                                                                        <span class="content-head">Issued By:</span>
                                                                        <span class="content">
                                                                        {{ isset($courses['issue_by']) && !empty($courses['issue_by']) ? $courses['issue_by'] : '-'}}
                                                                    </span>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-4 col-md-4" style="margin-bottom: 10px;">
                                                                    <div class="discription">
                                                                        <span class="content-head">Date Of Expiry:</span>
                                                                        <span class="content">
                                                                        {{ isset($courses['expiry_date']) && !empty($courses['expiry_date']) ? date('d-m-Y',strtotime($courses['expiry_date'])) : '-'}}
                                                                    </span>
                                                                    </div>
                                                                </div>

                                                                <div class="col-xs-12 col-sm-4 col-md-4" style="margin-bottom: 10px;">
                                                                    <div class="discription">
                                                                        <span class="content-head">Date Of Issue:</span>
                                                                        <span class="content">
                                                                        {{ isset($courses['issue_date']) ? date('d-m-Y',strtotime($courses['issue_date'])) : '-'}}
                                                                    </span>
                                                                    </div>
                                                                </div>

                                                                
                                                                

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php $normal_course_count++; ?>
                                            @endforeach
                                        @else
                                            <div class="row no-data-found">
                                                <div class="col-xs-12 text-center">
                                                    <div class="discription">
                                                        <span class="content-head">No Data Found</span>
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                    </div>
                                @endif


                            </div>
<!--                            <div class="row">
                                <div class="col-sm-12 no-padding">
                                    <div class="reg-form-btn-container col-sm-6 col-xs-6">
                                        <button type="button" data-style="zoom-in"
                                                class="reg-form-btn blue ladda-button certificate-skip-button"
                                                id="certificate-skip-button" data-tab-index="3"
                                                style="margin-bottom: 60px;">Skip
                                        </button>
                                    </div>
                                    <div class="reg-form-btn-container text-right col-sm-6 col-xs-6">
                                        <button type="button" data-style="zoom-in"
                                                class="reg-form-btn blue ladda-button certificate-details-submit"
                                                id="certificate-details-submit" data-tab-index="3"
                                                style="margin-bottom: 60px;">
                                            Save & Next
                                        </button>
                                    </div>
                                </div>
                            </div>-->
                        </form>

                        <form class="general-tab-form hide" id="general-tab-form" name="general-tab-form" class="hide"
                              action="{{route('site.user.general.details')}}" method="post">
                            {{ csrf_field() }}
                            <div class="user-details-container">
                                <div class="row">
                                    <div class="col-sm-12 title_heading">
                                        <div class="main-heading">
                                            General Details
                                        </div>
                                    </div>
                                </div>
                                <div class="row add-more-cop-row add-more-section">
                                    <div class="col-sm-12">
                                        <div class="heading">
                                            ACADEMICS AND PROFESSIONAL QUALIFICATION
                                        </div>
                                        <div class="pull-right close-button cdc-close-button p-t-10 cdc-close-button-0 clear-all-details" data-module-type="academic">
                                            <i class="fa fa-times" aria-hidden="true"></i>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label class="input-label" for="name_of_school">Name of School /
                                                College</label>
                                            <input type="text" class="form-control" id="name_of_school"
                                                   name="name_of_school"
                                                   value="{{ isset($user_data[0]['professional_detail']['name_of_school']) ? $user_data[0]['professional_detail']['name_of_school'] : ''}}"
                                                   placeholder="Type your School/College name">
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label class="input-label" for="school_to">Pass Out Year</label>
                                            <select id="school_to" name="school_to" class="form-control">
                                                <option value="">Select Year</option>
                                                @for($i = $currentYear ; $i > '1980' ; $i--)
                                                    <option value="{{$i}}" {{isset($user_data[0]['professional_detail']['school_to']) ? $i == $user_data[0]['professional_detail']['school_to'] ? 'selected' : '' : ''}}>{{$i}}</option>
                                                @endfor
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label class="input-label" for="school_qualification">Highest
                                                Qualification</label>
                                            <input type="text" class="form-control" id="school_qualification"
                                                   name="school_qualification"
                                                   value="{{ isset($user_data[0]['professional_detail']['school_qualification']) ? $user_data[0]['professional_detail']['school_qualification'] : ''}}"
                                                   placeholder="Type your highest qualification">
                                        </div>
                                    </div>
                                </div>
                                <div class="row add-more-section">
                                    <div class="col-sm-12">
                                        <div class="heading">
                                            PRE SEA TRAINING / APPRENTICE SHIP
                                            <div class="pull-right close-button cdc-close-button p-t-10 cdc-close-button-0 clear-all-details" data-module-type="presea">
                                                <i class="fa fa-times" aria-hidden="true"></i>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label class="input-label" for="institute_name">Name of Institute /
                                                College</label>
                                            <input type="text" class="form-control" id="institute_name"
                                                   name="institute_name"
                                                   value="{{ isset($user_data[0]['professional_detail']['institute_name']) ? $user_data[0]['professional_detail']['institute_name'] : ''}}"
                                                   placeholder="Type your Institute name">
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label class="input-label" for="institute_to">Pass Out Year</label>
                                            <select id="institute_to" name="institute_to" class="form-control">
                                                <option value="">Select Year</option>
                                                @for($i = $currentYear ; $i > '1980' ; $i--)
                                                    <option value="{{$i}}" {{isset($user_data[0]['professional_detail']['institute_to']) ? $i == $user_data[0]['professional_detail']['institute_to'] ? 'selected' : '' : ''}}>{{$i}}</option>
                                                @endfor
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label class="input-label" for="institute_degree">Type Of Degree</label>
                                            <input type="text" class="form-control" id="institute_degree"
                                                   name="institute_degree"
                                                   value="{{ isset($user_data[0]['professional_detail']['institute_degree']) ? $user_data[0]['professional_detail']['institute_degree'] : ''}}"
                                                   placeholder="Type your degree name">
                                        </div>
                                    </div>
                                </div>
                                <div class="row add-more-section">
                                    <div class="col-sm-12">
                                        <div class="heading">
                                            NEXT OF KIN
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label class="input-label" for="kin_name">Name</label>
                                            <input type="text" class="form-control" id="kin_name" name="kin_name"
                                                   value="{{ isset($user_data[0]['personal_detail']['kin_name']) ? $user_data[0]['personal_detail']['kin_name'] : ''}}"
                                                   placeholder="Type your kin full name">
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label class="input-label" for="kin_relation">Relation</label>
                                            <input type="text" class="form-control" id="kin_relation"
                                                   name="kin_relation"
                                                   value="{{ isset($user_data[0]['personal_detail']['kin_relation']) ? $user_data[0]['personal_detail']['kin_relation'] : ''}}"
                                                   placeholder="Type your relation with kin">
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label class="input-label" for="kin_number">Phone Number</label>
                                                
                                                  <div class="col-sm-12 col-xs-12" style="padding-left:0;padding-right:0;">
                                                    <div class="form-group" style="display: flex;">
                                                        <input type="text" class="form-control kin-country-code valid" id="kin_number_code" name="kin_number_code" placeholder="91"  value="{{ isset($user_data[0]['personal_detail']['kin_number_code']) && !empty($user_data[0]['personal_detail']['kin_number_code']) ? $user_data[0]['personal_detail']['kin_number_code'] : '91'}}" maxlength="5" autocomplete="off" >
                                                    <input type="text" class="form-control" id="kin_number" name="kin_number"
                                                           value="{{ isset($user_data[0]['personal_detail']['kin_number']) ? $user_data[0]['personal_detail']['kin_number'] : ''}}"
                                                           placeholder="Type kin phone number">
                                                        <span class="hide" id="mobile-error" style="color: red"></span>
                                                    </div>
                                                </div>
                                           
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label class="input-label">Emergency Number</label>
                                            
                                                <div class="col-sm-12 col-xs-12" style="padding-left:0;padding-right:0;">
                                                    <div class="form-group" style="display: flex;">
                                                        <input type="text" class="form-control kin-country-code valid" id="kin_alternate_number_code" name="kin_alternate_number_code" placeholder="91"  value="{{ isset($user_data[0]['personal_detail']['kin_alternate_number_code']) &&  !empty($user_data[0]['personal_detail']['kin_alternate_number_code']) ? $user_data[0]['personal_detail']['kin_alternate_number_code'] : '91' }}" maxlength="5" autocomplete="off" >
                                                        <input type="text" class="form-control" id="kin_alternate_no" name="kin_alternate_no"
                                                            value="{{ isset($user_data[0]['personal_detail']['kin_alternate_no']) ? $user_data[0]['personal_detail']['kin_alternate_no'] : ''}}"
                                                           placeholder="Type your Emergency number">
                                                        <span class="hide" id="mobile-error" style="color: red"></span>
                                                    </div>
                                                </div>                                           
                                        </div>
                                    </div>
                                </div>
                                <div class="row add-more-section">
                                    <div class="col-sm-12">
                                        <div class="heading">
                                            WEAR
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label class="input-label" for="coverall_size">Coveralls Size</label>
                                            <select id='coverall_size' name="coverall_size" class="form-control">
                                                <option value="">Select Coveralls Size</option>
                                                @foreach( \CommonHelper::coverall_size() as $c_index => $coverall_size)
                                                    <option value="{{ $c_index }}" {{ isset($user_data[0]['personal_detail']['coverall_size']) ? $user_data[0]['personal_detail']['coverall_size'] == $c_index ? 'selected' : '' : ''}}> {{ $coverall_size }}
                                                    </option>
                                                @endforeach
                                            </select>

                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label class="input-label" for="safety_shoe_size">Safety Shoe Size</label>
                                            <select id='safety_shoe_size' name="safety_shoe_size" class="form-control">
                                                <option value=''>Select Safety Shoe Size</option>
                                                @foreach( \CommonHelper::safety_shoe_size() as $c_index => $safety_shoe_size)
                                                    <option value="{{ $c_index }}" {{ isset($user_data[0]['personal_detail']['safety_shoe_size']) ? $user_data[0]['personal_detail']['safety_shoe_size'] == $c_index ? 'selected' : '' : ''}}> {{ $safety_shoe_size }}
                                                    </option>
                                                @endforeach
                                            </select>

                                        </div>
                                    </div>
                                </div>
                                <style type="text/css">
                                    .displaynonemedi{
                                        display: none;
                                    }
                                    .displayblockmedi{
                                        display: block;   
                                    }
                                </style>
                                <div class="row add-more-section">
                                    <div class="col-sm-12">
                                        <div class="heading" style="margin-bottom: 10px;">
                                            Medical Details
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label class="input-label" for="blood_type">Blood Type</label>
                                            <select id='blood_type' name="blood_type" class="form-control">
                                                <option value=''>Select Blood Type</option>
                                                @foreach( \CommonHelper::blood_type() as $b_index => $bloodType)
                                                    <option value="{{ $b_index }}" {{ isset($user_data[0]['wkfr_detail']['blood_type']) ? $user_data[0]['wkfr_detail']['blood_type'] == $b_index ? 'selected' : '' : ''}}> {{ $bloodType }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">

                                             
                                            <input type="checkbox" class="ilo_medical selectvalue" data-type="ilo_medicaldisplay" name="ilo_medical" value="1" {{ $user_data[0]['wkfr_detail']['ilo_medical'] == 1 ? 'checked' : ''}}> Valid ILO Medical
                                            

                                            <input type="checkbox" class="screening_test selectvalue" data-type="screeningtestdisplay" {{ $user_data[0]['wkfr_detail']['screening_test'] == 1 ? 'checked' : ''}}  name="screening_test" value="1" style="margin-left: 20px;"> 
                                            Screening Test
                                        
                                        <div class="row ilo_medicaldisplay @if($user_data[0]['wkfr_detail']['ilo_medical'] == 1) displayblockmedi @else  displaynonemedi @endif">
                                            <div class="col-sm-4" style="margin-top: 15px;">
                                                <div class="form-group">
                                                    <label class="input-label" for="ilo_medical">Valid ILO
                                                        Medical</label>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                    <div class="form-group ilo-block">
                                                        <label class="input-label" for="ilo_issue_date">Expiry Date<span
                                                                    class="symbol required"></span></label>
                                                        <input type="text" class="form-control dateissue ilo_medicaldisplaydate"
                                                               id="ilo_issue_date" name="ilo_issue_date"
                                                               value="{{ isset($user_data[0]['wkfr_detail']['ilo_issue_date']) ? date('d-m-Y',strtotime($user_data[0]['wkfr_detail']['ilo_issue_date'])) : ''}}"
                                                               placeholder="dd-mm-yyyy">
                                                    </div>

                                                
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label class="input-label" for="">In Country</label>
                                                    <select id='ilo_country' name="ilo_country" class="form-control ilo_medicaldisplaycountry">
                                                    <option value=''>Select In Country</option>
                                                    @foreach( \CommonHelper::countries() as $c_index => $country)
                                                        <option value="{{ $c_index }}" {{ isset($user_data[0]['wkfr_detail']['ilo_country']) ? $user_data[0]['wkfr_detail']['ilo_country'] == $c_index ? 'selected' : '' : ''}}> {{ $country }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row screeningtest screeningtestdisplay  @if($user_data[0]['wkfr_detail']['screening_test'] == 1) displayblockmedi @else  displaynonemedi @endif">
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label class="input-label" for="screening"> Screening Test</label>
                                                </div>
                                            </div>
                                           <div class="col-sm-4">
                                                    <div class="form-group ilo-block">
                                                        <label class="input-label" for="ilo_issue_date">Expiry Date<span
                                                                    class="symbol required"></span></label>

                                                        <input type="text" class="form-control dateissue screeningtestdisplaydate"
                                                               id="screening_test_date" name="screening_test_date"
                                                               value="{{ isset($user_data[0]['wkfr_detail']['screening_test_date']) ? date('d-m-Y',strtotime($user_data[0]['wkfr_detail']['screening_test_date'])) : ''}}"
                                                               placeholder="dd-mm-yyyy">
                                                    </div>

                                                
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label class="input-label" for="">In Country</label>
                                                    <select id='screening_test_county' name="screening_test_county" class="form-control screeningtestdisplaycountry">
                                                    <option value=''>Select Your nationality</option>
                                                    @foreach( \CommonHelper::countries() as $c_index => $country)
                                                        <option value="{{ $c_index }}" {{ isset($user_data[0]['wkfr_detail']['screening_test_county']) ? $user_data[0]['wkfr_detail']['screening_test_county'] == $c_index ? 'selected' : '' : ''}}> {{ $country }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                </div>
                                <div class="row add-more-section">
                                    <div class="col-sm-12">
                                        <div class="heading" style="margin-bottom: 10px;">
                                            Vaccination List : 
                                        </div>
                                    </div>
                                    <div class="col-sm-12">

                                             
                                            <input type="checkbox" class="yellow_fever selectvalue" data-type="yellow_feverdisplay" name="yellow_fever" value="1" {{ $user_data[0]['wkfr_detail']['yellow_fever'] == 1 ? 'checked' : ''}}> Yellow Fever

                                            <input type="checkbox" style="margin-left: 15px;" class="cholera selectvalue" data-type="choleradisplay" name="cholera" value="1" {{ $user_data[0]['wkfr_detail']['cholera'] == 1 ? 'checked' : ''}}> Cholera

                                            <input type="checkbox" style="margin-left: 15px;" class="hepatitis_b selectvalue" data-type="hepatitis_bdisplay" name="hepatitis_b" value="1" {{ $user_data[0]['wkfr_detail']['hepatitis_b'] == 1 ? 'checked' : ''}}> Hepatitis B
                                           
                                            <input type="checkbox" style="margin-left: 15px;" class="hepatitis_c selectvalue" data-type="hepatitis_cdisplay" name="hepatitis_c" value="1" {{ $user_data[0]['wkfr_detail']['hepatitis_c'] == 1 ? 'checked' : ''}}> Hepatitis C

                                             <input type="checkbox" style="margin-left: 15px;" class="diphtheria selectvalue" data-type="diphtheriadisplay" name="diphtheria" value="1" {{ $user_data[0]['wkfr_detail']['diphtheria'] == 1 ? 'checked' : ''}}> Diphtheria and tetanus

                                             <input type="checkbox" style="margin-left: 15px;" class="covid selectvalue" data-type="coviddisplay" name="covid" value="1" {{ $user_data[0]['wkfr_detail']['covid'] == 1 ? 'checked' : ''}}> Covid 19
                                        
                                        <div class="row yellow_feverdisplay @if($user_data[0]['wkfr_detail']['yellow_fever'] == 1) displayblockmedi @else  displaynonemedi @endif">
                                            <div class="col-sm-4" style="margin-top: 15px;">
                                                <div class="form-group">
                                                    <label class="input-label" for="yellow_fever">Yellow Fever</label>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                    <div class="form-group ilo-block">
                                                        <label class="input-label" for="yf_issue_date">Expiry Date</label>
                                                        <?php 
                                                            $yfIssueDateEdit = "";
                                                            if(isset($user_data[0]['wkfr_detail']['yf_issue_date']) && !empty($user_data[0]['wkfr_detail']['yf_issue_date']) && $user_data[0]['wkfr_detail']['yf_issue_date'] != "1970-01-01"){
                                                                $yfIssueDateEdit = date('d-m-Y',strtotime($user_data[0]['wkfr_detail']['yf_issue_date']));
                                                            }
                                                        ?>
                                                        <input type="text" class="form-control dateissue yellow_feverdisplaydate"
                                                               id="yf_issue_date" name="yf_issue_date"
                                                               value="{{$yfIssueDateEdit}}"
                                                               placeholder="dd-mm-yyyy">
                                                               <span style="color:#8e8e8e;">Leave blank for unlimited expiry date</span>
                                                    </div>

                                                
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label class="input-label" for="">In Country</label>
                                                    <select id='yf_country' name="yf_country" class="form-control yellow_feverdisplaycountry">
                                                    <option value=''>Select In Country</option>
                                                    @foreach( \CommonHelper::countries() as $c_index => $country)
                                                        <option value="{{ $c_index }}" {{ isset($user_data[0]['wkfr_detail']['yf_country']) ? $user_data[0]['wkfr_detail']['yf_country'] == $c_index ? 'selected' : '' : ''}}> {{ $country }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row choleradisplay @if($user_data[0]['wkfr_detail']['cholera'] == 1) displayblockmedi @else  displaynonemedi @endif">
                                            <div class="col-sm-4" style="margin-top: 15px;">
                                                <div class="form-group">
                                                    <label class="input-label" for="cholera">Cholera</label>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                    <div class="form-group ilo-block">
                                                        <label class="input-label" for="cholera_issue_date">Expiry Date</label>
                                                        <?php 
                                                            $choleraIssueDateEdit = "";
                                                            if(isset($user_data[0]['wkfr_detail']['cholera_issue_date']) && !empty($user_data[0]['wkfr_detail']['cholera_issue_date']) && $user_data[0]['wkfr_detail']['cholera_issue_date'] != "1970-01-01"){
                                                                $choleraIssueDateEdit = date('d-m-Y',strtotime($user_data[0]['wkfr_detail']['cholera_issue_date']));
                                                            }
                                                        ?>
                                                        <input type="text" class="form-control dateissue choleradisplaydate"
                                                               id="cholera_issue_date" name="cholera_issue_date"
                                                               value="{{$choleraIssueDateEdit}}"
                                                               placeholder="dd-mm-yyyy">
                                                               <span style="color:#8e8e8e;">Leave blank for unlimited expiry date</span>
                                                    </div>

                                                
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label class="input-label" for="">In Country</label>
                                                    <select id='cholera_country' name="cholera_country" class="form-control choleradisplaycountry">
                                                    <option value=''>Select In Country</option>
                                                    @foreach( \CommonHelper::countries() as $c_index => $country)
                                                        <option value="{{ $c_index }}" {{ isset($user_data[0]['wkfr_detail']['cholera_country']) ? $user_data[0]['wkfr_detail']['cholera_country'] == $c_index ? 'selected' : '' : ''}}> {{ $country }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                                </div>
                                            </div>
                                        </div>
                                        
                                         <div class="row hepatitis_bdisplay @if($user_data[0]['wkfr_detail']['hepatitis_b'] == 1) displayblockmedi @else  displaynonemedi @endif">
                                            <div class="col-sm-4" style="margin-top: 15px;">
                                                <div class="form-group">
                                                    <label class="input-label" for="hepatitis_b">Hepatitis B</label>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                    <div class="form-group ilo-block">
                                                        <label class="input-label" for="hepatitis_b_issue_date">Expiry Date</label>
                                                        <?php 
                                                            $hepatitisBissueDateEdit = "";
                                                            if(isset($user_data[0]['wkfr_detail']['hepatitis_b_issue_date']) && !empty($user_data[0]['wkfr_detail']['hepatitis_b_issue_date']) && $user_data[0]['wkfr_detail']['hepatitis_b_issue_date'] != "1970-01-01"){
                                                                $hepatitisBissueDateEdit = date('d-m-Y',strtotime($user_data[0]['wkfr_detail']['hepatitis_b_issue_date']));
                                                            }
                                                        ?>
                                                        <input type="text" class="form-control dateissue hepatitis_bdisplaydate"
                                                               id="yf_issue_date" name="hepatitis_b_issue_date"
                                                               value="{{$hepatitisBissueDateEdit}}"
                                                               placeholder="dd-mm-yyyy">
                                                               <span style="color:#8e8e8e;">Leave blank for unlimited expiry date</span>
                                                    </div>

                                                
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label class="input-label" for="">In Country</label>
                                                    <select id='hepatitis_b_country' name="hepatitis_b_country" class="form-control hepatitis_bdisplaycountry">
                                                    <option value=''>Select In Country</option>
                                                    @foreach( \CommonHelper::countries() as $c_index => $country)
                                                        <option value="{{ $c_index }}" {{ isset($user_data[0]['wkfr_detail']['hepatitis_b_country']) ? $user_data[0]['wkfr_detail']['hepatitis_b_country'] == $c_index ? 'selected' : '' : ''}}> {{ $country }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row hepatitis_cdisplay @if($user_data[0]['wkfr_detail']['hepatitis_c'] == 1) displayblockmedi @else  displaynonemedi @endif">
                                            <div class="col-sm-4" style="margin-top: 15px;">
                                                <div class="form-group">
                                                    <label class="input-label" for="hepatitis_c">Hepatitis C</label>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                    <div class="form-group ilo-block">
                                                        <label class="input-label" for="hepatitis_c_issue_date">Expiry Date</label>
                                                        <?php 
                                                            $hepatitisCissueDateEdit = "";
                                                            if(isset($user_data[0]['wkfr_detail']['hepatitis_c_issue_date']) && !empty($user_data[0]['wkfr_detail']['hepatitis_c_issue_date']) && $user_data[0]['wkfr_detail']['hepatitis_c_issue_date'] != "1970-01-01"){
                                                                $hepatitisCissueDateEdit = date('d-m-Y',strtotime($user_data[0]['wkfr_detail']['hepatitis_c_issue_date']));
                                                            }
                                                        ?>
                                                        <input type="text" class="form-control dateissue hepatitis_cdisplaydate"
                                                               id="yf_issue_date" name="hepatitis_c_issue_date"
                                                               value="{{$hepatitisCissueDateEdit}}"
                                                               placeholder="dd-mm-yyyy">
                                                               <span style="color:#8e8e8e;">Leave blank for unlimited expiry date</span>
                                                    </div>

                                                
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label class="input-label" for="">In Country</label>
                                                    <select id='hepatitis_c_country' name="hepatitis_c_country" class="form-control hepatitis_cdisplaycountry">
                                                    <option value=''>Select In Country</option>
                                                    @foreach( \CommonHelper::countries() as $c_index => $country)
                                                        <option value="{{ $c_index }}" {{ isset($user_data[0]['wkfr_detail']['hepatitis_c_country']) ? $user_data[0]['wkfr_detail']['hepatitis_c_country'] == $c_index ? 'selected' : '' : ''}}> {{ $country }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row diphtheriadisplay @if($user_data[0]['wkfr_detail']['diphtheria'] == 1) displayblockmedi @else  displaynonemedi @endif">
                                            <div class="col-sm-4" style="margin-top: 15px;">
                                                <div class="form-group">
                                                    <label class="input-label" for="diphtheria">Diphtheria and tetanus</label>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                    <div class="form-group ilo-block">
                                                        <label class="input-label" for="diphtheria_issue_date">Expiry Date</label>
                                                        <?php 
                                                            $diphtheriaIssueDate = "";
                                                            if(isset($user_data[0]['wkfr_detail']['diphtheria_issue_date']) && !empty($user_data[0]['wkfr_detail']['diphtheria_issue_date']) && $user_data[0]['wkfr_detail']['diphtheria_issue_date'] != "1970-01-01"){
                                                                $diphtheriaIssueDate = date('d-m-Y',strtotime($user_data[0]['wkfr_detail']['diphtheria_issue_date']));
                                                            }
                                                        ?>
                                                        <input type="text" class="form-control dateissue diphtheriadisplaydate"
                                                               id="diphtheria_issue_date" name="diphtheria_issue_date"
                                                               value="{{$diphtheriaIssueDate}}"
                                                               placeholder="dd-mm-yyyy">
                                                               <span style="color:#8e8e8e;">Leave blank for unlimited expiry date</span>
                                                    </div>

                                                
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label class="input-label" for="">In Country</label>
                                                    <select id='diphtheria_country' name="diphtheria_country" class="form-control diphtheriadisplaycountry">
                                                    <option value=''>Select In Country</option>
                                                    @foreach( \CommonHelper::countries() as $c_index => $country)
                                                        <option value="{{ $c_index }}" {{ isset($user_data[0]['wkfr_detail']['diphtheria_country']) ? $user_data[0]['wkfr_detail']['diphtheria_country'] == $c_index ? 'selected' : '' : ''}}> {{ $country }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row coviddisplay @if($user_data[0]['wkfr_detail']['covid'] == 1) displayblockmedi @else  displaynonemedi @endif">
                                            <div class="col-sm-4" style="margin-top: 15px;">
                                                <div class="form-group">
                                                    <label class="input-label" for="covid">Covid 19</label>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                    <div class="form-group ilo-block">
                                                        <label class="input-label" for="covid_issue_date">Expiry Date</label>
                                                        <?php 
                                                            $covidIssueDate = "";
                                                            if(isset($user_data[0]['wkfr_detail']['covid_issue_date']) && !empty($user_data[0]['wkfr_detail']['covid_issue_date']) && $user_data[0]['wkfr_detail']['covid_issue_date'] != "1970-01-01"){
                                                                $covidIssueDate = date('d-m-Y',strtotime($user_data[0]['wkfr_detail']['covid_issue_date']));
                                                            }
                                                        ?>
                                                        <input type="text" class="form-control dateissue coviddisplaydate"
                                                               id="covid_issue_date" name="covid_issue_date"
                                                               value="{{$covidIssueDate}}"
                                                               placeholder="dd-mm-yyyy">
                                                               <span style="color:#8e8e8e;">Leave blank for unlimited expiry date</span>
                                                    </div>

                                                
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label class="input-label" for="">In Country</label>
                                                    <select id='covid_country' name="covid_country" class="form-control coviddisplaycountry">
                                                    <option value=''>Select In Country</option>
                                                    @foreach( \CommonHelper::countries() as $c_index => $country)
                                                        <option value="{{ $c_index }}" {{ isset($user_data[0]['wkfr_detail']['covid_country']) ? $user_data[0]['wkfr_detail']['covid_country'] == $c_index ? 'selected' : '' : ''}}> {{ $country }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>

                                </div>
                                @if(!empty($user_data[0]['personal_detail']) && $user_data[0]['personal_detail']['nationality'] == 95)
                                    <div class="row add-more-section">
                                        <div class="col-sm-12">
                                            <div class="heading">
                                                Character Reference(Like Police clearance certificate)
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <div class="form-group">
                                                        <div class="input-label" for="indian_cc">Character Certificate</div>
                                                        <label class="radio-inline">
                                                            <input type="radio" name="cc"
                                                                value="1" {{ !empty($user_data[0]['wkfr_detail']) ? $user_data[0]['wkfr_detail']['character_reference'] == 1 ? 'checked' : '' : ''}}>
                                                            Yes</label>
                                                        <label class="radio-inline">
                                                            <input type="radio" name="cc"
                                                                value="0" {{ !empty($user_data[0]['wkfr_detail']) ? $user_data[0]['wkfr_detail']['character_reference'] == 0 ? 'checked' : '' : 'checked'}}>
                                                            No</label>
                                                    </div>
                                                </div>
                                                @if(isset($user_data[0]['wkfr_detail']['character_reference']) && ($user_data[0]['wkfr_detail']['character_reference'] == 1))
                                                    <div class="col-sm-4">
                                                        <div class="form-group cc-block">
                                                            <label class="input-label" for="ccdate">Type Of Certificate</label>
                                                            <input type="text" class="form-control" id="type_of_certificate"
                                                                name="type_of_certificate"
                                                                value="{{ $user_data[0]['wkfr_detail']['type_of_certificate'] }}"
                                                                placeholder="Type Of Certificate">
                                                        </div>
                                                    </div>
                                                @else
                                                    <div class="col-sm-4">
                                                        <div class="form-group cc-block hide">
                                                            <label class="input-label" for="ccdate">Type Of Certificate</label>
                                                            <input type="text" class="form-control" id="type_of_certificate"
                                                                name="type_of_certificate"
                                                                value=""
                                                                placeholder="Type Of Certificate">
                                                        </div>
                                                    </div>
                                                @endif
                                                @if(isset($user_data[0]['wkfr_detail']['character_reference']) && ($user_data[0]['wkfr_detail']['character_reference'] == 1))
                                                    <div class="col-sm-4">
                                                        <div class="form-group cc-block">
                                                            <label class="input-label" for="ccdate">Expiry Date</label>
                                                            <input type="text" class="form-control datepicker" id="ccdate"
                                                                name="ccdate"
                                                                value="{{ isset($user_data[0]['wkfr_detail']['character_reference_issue_date']) ? date('d-m-Y',strtotime($user_data[0]['wkfr_detail']['character_reference_issue_date'])) : ''}}"
                                                                placeholder="dd-mm-yyyy">
                                                        </div>
                                                    </div>
                                                @else
                                                    <div class="col-sm-4">
                                                        <div class="form-group cc-block hide">
                                                            <label class="input-label" for="ccdate">Expiry Date</label>
                                                            <input type="text" class="form-control datepicker" value="{{ isset($user_data[0]['wkfr_detail']['character_reference_issue_date']) ? date('d-m-Y',strtotime($user_data[0]['wkfr_detail']['character_reference_issue_date'])) : ''}}"
                                                                placeholder="dd-mm-yyyy">
                                                        </div>
                                                    </div>
                                                @endif
                                            </div>
                                        </div>


                                    </div>
                                @endif

                                <div class="row add-more-section">
                                    <div class="col-sm-12">
                                        <div class="heading">
                                            Non Sailing/Other Experience
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <textarea class="form-control" name="other_exp" rows="5"
                                                      placeholder="Type your other experience">{{ isset($user_data[0]['professional_detail']['other_exp']) ? $user_data[0]['professional_detail']['other_exp'] : ''}}</textarea>
                                        </div>
                                    </div>
                                </div>
                                @php 
                                    $isBankDetails = 0;
                                    if(isset($user_data[0]['personal_detail']['is_bank_details']) && $user_data[0]['personal_detail']['is_bank_details'] == 1){
                                        $isBankDetails = 1;
                                    }
                                @endphp
                                @if(!empty($user_data[0]['personal_detail']) && $user_data[0]['personal_detail']['nationality'] == 95)
                                    <div class="row add-more-section">
                                        <div class="col-sm-12">
                                            <div class="heading">
                                                <span style="color: red; font-weight: 600;">Below Mentioned Details will be maintained as private and will not be displayed either on your profile or resume.<br>These details will enable you to Upload & Save all Relavant Documents.</span>
                                                <br> <br>
                                                <!--Identification Details-->
                                            </div>
                                        </div>
                                        <br> <br>
                                        <div class="clearfix">
                                            <div class="col-xs-6">
                                                <div class="heading">
                                                    Identification & Banking Details
                                                </div>
                                            </div>
                                            <div class="col-xs-6">
                                                <div class="heading">
                                                    <label class="radio-inline">
                                                        <input type="radio" name="is_bank_details" value="1" class="is_bank_details" {{ ($isBankDetails == 1) ? 'checked' : null }}>YES
                                                    </label>
                                                    <label class="radio-inline">
                                                        <input type="radio" name="is_bank_details" value="0"  class="is_bank_details" {{ ($isBankDetails == 0) ? 'checked' : null }}>NO
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="identification_div {{ ($isBankDetails == 0) ? 'collapse' : '' }}">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="input-label" for="pancard">Pancard</label>
                                                    <input type="text" class="form-control" id="pancard" name="pancard"
                                                        value="{{ isset($user_data[0]['personal_detail']['pancard']) ? $user_data[0]['personal_detail']['pancard'] : ''}}"
                                                        placeholder="Enter Pancard No.">
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="input-label" for="aadhaar">Aadhaar</label>
                                                    <input type="text" class="form-control" id="aadhaar" name="aadhaar"
                                                        value="{{ isset($user_data[0]['personal_detail']['aadhaar']) ? $user_data[0]['personal_detail']['aadhaar'] : ''}}"
                                                        placeholder="Enter Aadhaar No.">
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="heading">
                                                    Banking Details

                                                    <div class="pull-right close-button cdc-close-button p-t-10 cdc-close-button-0 clear-all-details" data-module-type="bank">
                                                        <i class="fa fa-times" aria-hidden="true"></i>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <label class="input-label" for="account_holder_name">Account Holder Name</label>
                                                    <input type="text" class="form-control" id="account_holder_name" name="account_holder_name"
                                                        value="{{ isset($user_data[0]['personal_detail']['account_holder_name']) ? $user_data[0]['personal_detail']['account_holder_name'] : ''}}"
                                                        placeholder="Account Holder Name">
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label class="input-label" for="bank_name">Bank Name</label>
                                                    <input type="text" class="form-control" id="bank_name" name="bank_name"
                                                        value="{{ isset($user_data[0]['personal_detail']['bank_name']) ? $user_data[0]['personal_detail']['bank_name'] : ''}}"
                                                        placeholder="Enter Bank Name">
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label class="input-label" for="account_no">Account No.</label>
                                                    <input type="text" class="form-control" id="account_no" name="account_no"
                                                        value="{{ isset($user_data[0]['personal_detail']['account_no']) ? $user_data[0]['personal_detail']['account_no'] : ''}}"
                                                        placeholder="Enter Account No.">
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label class="input-label" for="ifsc_code">IFSC Code</label>
                                                    <input type="text" class="form-control" id="ifsc_code" name="ifsc_code"
                                                        value="{{ isset($user_data[0]['personal_detail']['ifsc_code']) ? $user_data[0]['personal_detail']['ifsc_code'] : ''}}"
                                                        placeholder="Enter IFSC Code">
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <label class="input-label" for="branch_address">Branch Address</label>
                                                    <input type="text" class="form-control" id="branch_address"
                                                        name="branch_address"
                                                        value="{{ isset($user_data[0]['personal_detail']['branch_address']) ? $user_data[0]['personal_detail']['branch_address'] : ''}}"
                                                        placeholder="Enter Branch Address">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endif

                                <div class="row pt-30">
                                    <div class="col-sm-12 no-padding">
                                        <div class="reg-form-btn-container col-sm-6 col-xs-6">
                                            <button type="button" data-style="zoom-in"
                                                    class="reg-form-btn blue ladda-button general-form-skip-button"
                                                    id="general-form-skip-button" data-tab-index="5"
                                                    style="margin-bottom: 60px;">Skip
                                            </button>
                                        </div>
                                        <div class="reg-form-btn-container text-right col-sm-6 col-xs-6">
                                            <button type="button" data-style="zoom-in"
                                                    class="reg-form-btn blue ladda-button general-form-submit"
                                                    id="general-form-submit" data-tab-index="5"
                                                    style="margin-bottom: 60px;">Save & Next
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>

                        <form class="upload_documents hide" id="upload_documents">
                            {{ csrf_field() }}
                            <div class="user-details-container">
                                <div class="row docs-upload">
                                    <div class="col-xs-12">
                                        <div class="main-heading col-md-8">
                                            Upload Documents<span class="symbol required"></span>
                                        </div>
                                        <div class="main-heading col-md-3">
                                            {{-- <span>Storage</span> --}}
                                            <p class="user-progress-text used-mb">{{'Used - '. number_format($usedMb,2)}}mb</p>
                                            <div class="progress user-progress">
                                                <div class="progress-bar user-bar" role="progressbar" style="{{'width: '.$percentageMb.'%'}}"
                                                     aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"> {{number_format($usedMb,2)}}mb
                                                </div>
                                            </div>
                                            <p class="user-progress-text">Max Storage - 150 mb</p>
                                        </div>
                                    </div>
                                </div>

                                <div class="documents_section">

                                </div>

                            </div>
                            <div class="row" style="display:none;">
                                <div class="col-sm-12 no-padding">
                                    <div class="reg-form-btn-container col-sm-6 col-xs-6">
                                        <button type="button" data-style="zoom-in"
                                                class="reg-form-btn blue ladda-button document-skip-button"
                                                id="document-skip-button" style="margin-bottom: 60px;">Skip
                                        </button>
                                    </div>
                                    <div class="reg-form-btn-container text-right col-sm-6 col-xs-6">
                                        <button type="button" data-style="zoom-in"
                                                class="reg-form-btn blue ladda-button dropzone-document-details-submit"
                                                style="margin-bottom: 60px;">Save
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="myModal" role="dialog">
            <div class="modal-dialog">

                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close close-otp-modal-button">&times;</button>
                        <h4 class="modal-title">Mobile Number Verification</h4>
                    </div>

                    <div class="modal-body">
                        <form id="mob-verification" method="post" action="{{ route('site.user.mob.verification') }}">
                            {{ csrf_field() }}
                            <div class="row form-group m-b-5">
                                <div class="col-md-12">
                                    <h4 class="m-b-5 m-t-0">Please enter the OTP that you have received on your mobile
                                        number.</h4>
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col-md-6">
                                    <input class="form-control" name="mob_otp" type="text" placeholder="Enter OTP">
                                </div>
                            </div>
                            <div class="alert alert-danger hide" id="invalid_mob_error">
                                <strong>Invalid!</strong> Entered OTP is Invalid.
                            </div>
                            <div class="alert alert-danger hide" id="otp_failure">

                            </div>
                            <div class="alert alert-success hide" id="otp_resend_success">
                                <strong>Success..</strong> New OTP has been sent to your mobile number..
                            </div>
                            <div class="alert alert-danger hide" id="mob_error_resend_otp">
                                <strong>Error!</strong> Error while generating OTP.
                            </div>
                        </form>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" id="resend-otp-button" style="float: left;">Resend
                            OTP
                        </button>
                        <button type="button" class="btn btn-danger" id="mob_otp_submit_button"
                                name="mob_otp_submit_button">Verify
                        </button>
                    </div>
                </div>

            </div>
        </div>

        <div class="modal fade" id="firstStepModal" role="dialog">
            <div class="modal-dialog">

                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close first-step-close-otp-modal-button">&times;</button>
                        <h4 class="modal-title">Mobile Number Verification</h4>
                    </div>

                    <div class="modal-body">
                        <form id="first-step-mob-verification" method="post"
                              action="{{ route('site.user.mob.verification') }}">
                            {{ csrf_field() }}
                            <div class="row form-group m-b-5">
                                <div class="col-md-12">
                                    <h4 class="m-b-5 m-t-0">Please enter the OTP that you have received on your mobile
                                        number.</h4>
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col-md-6">
                                    <input class="form-control" name="mob_otp" type="text" placeholder="Enter OTP">
                                </div>
                            </div>
                            <div class="alert alert-danger hide invalid_mob_error">
                                <strong>Invalid!</strong> Entered OTP is Invalid.
                            </div>
                            <div class="alert alert-danger hide" id="otp_failure">

                            </div>
                            <div class="alert alert-success hide otp_resend_success">
                                <strong>Success..</strong> New OTP has been sent to your mobile number..
                            </div>
                            <div class="alert alert-danger hide mob_error_resend_otp">
                                <strong>Error!</strong> Error while generating OTP.
                            </div>
                        </form>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger resend-otp-button ladda-button" data-style="zoom-in"
                                style="float: left;">Resend OTP
                        </button>
                        <button type="button" class="btn btn-danger mob_otp_submit_button ladda-button"
                                data-style="zoom-in" name="mob_otp_submit_button">Verify
                        </button>
                    </div>
                </div>

            </div>
        </div>


    </div>

    <div id="ImageCropModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content" id="address-form-container">
                <form action="{{route('site.user.upload')}}" id="uploadForm" name="frmupload" method="post"
                      enctype="multipart/form-data">
                    <div class="modal-header">
                        <div class="col-sm-12">
                            <div class="col-sm-6">
                                Upload and save.
                            </div>
                            <div class="col-sm-6">
                                <button type="button" class="btn btn-default" data-dismiss="modal"
                                        style="margin-left: 185px; margin-top:-10px; margin-bottom:-6px;">Close
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="modal-body">
                        {{ csrf_field() }}
                        <input type="hidden" id="image_container" name="image_container" value=""/>
                        <input type="hidden" id="document_id" name="document_id" value=""/>
                        <input type="hidden" id="current_container" name="current_container" value=""/>
                        <input type="hidden" id="document_name" name="document_name" value=""/>
                        <input type="hidden" id="type_id" name="type_id" value=""/>
                        <input type="hidden" id="type" name="type" value="photo"/>
                        <input type="file" id="uploadImage" name="file"/>
                        <img class="img-responsive" id="uploadImagePreview" src="#" alt=" " style="width:169px"/>

                        <div class='progress' id="progressDivId">
                            <div class='progress-bar' id='progressBar'></div>
                            <div class='percent' id='percent'>0%</div>
                        </div>
                        <div style="height: 10px;"></div>
                        <div id='outputImage'></div>
                    </div>
                    <div class="modal-footer">
                        <input id="submitButton" type="submit" name='btnSubmit' class="btn btn-success"
                               value="Submit Image"/>
                    </div>
                </form>
            </div>

        </div>
    </div>

    <script>
        $(document).ready(function(){
            $('.datepicker').datepicker({
                format: "dd-mm-yyyy",
                
                autoClose: true,
                
                startView: 2
            }).on('changeDate', function(ev) {
                if(ev.viewMode === 0){
                    $('.datepicker').datepicker('hide');
                }
           });
           $('.certificate_name').select2();
        })
        function generaterandomnumber() {
          
          var rendomnumber = Math.floor((Math.random() * 1000000) + 1);
          return rendomnumber;
        }
        $(document).ready(function () {
            $('body').on('change','.selectvalue',function(){
                var type = $(this).data('type');
                if ($(this).is(':checked')) {
                    $('.'+type+'date').val('')
                    $('.'+type+'country').val('')
                     $('.'+type).addClass('displayblockmedi')
                     $('.'+type).removeClass('displaynonemedi')
                }else{
                     $('.'+type).addClass('displaynonemedi')
                     $('.'+type).removeClass('displayblockmedi')
                }

            
            });
            $('body').on('click','.course-edit-button',function(){
               // $('#seafarer_course_type').attr('disabled','disabled')
            });
            $('body').on('click','.addrow',function(){
                rendomnumber = generaterandomnumber();
                var html = ` <div class="col-sm-12 remove`+rendomnumber+`" >
                                <div class="col-sm-4 col-md-4" style="padding-left:0px;">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="mobile" name="language[`+rendomnumber+`][name]" placeholder="Type Language">
                                        <span class="hide" id="mobile-error" style="color: red"></span>
                                    </div>
                                </div>
                                <div class="col-md-2 col-sm-2 form-group"  style="margin-top: 5px; margin-left: 10px;">
                                    <span class="onlyMobile"> Speak &nbsp;</span>
                                    <input type="checkbox" name="language[`+rendomnumber+`][can_speak]" value="1" checked="">
                                </div>
                                <div class="col-md-2 col-sm-2 form-group" style="margin-top: 5px; margin-left: 10px;">
                                    <span class="onlyMobile"> Read &nbsp;</span>
                                    <input type="checkbox" name="language[`+rendomnumber+`][can_read]" value="1">
                                </div>
                                <div class="col-md-2 col-sm-2 form-group" style="padding-left: 10px;">
                                    <a href="javascript:void(0)" class="removerow" data-id="`+rendomnumber+`" style="background: #a22a51;padding: 3px 8px;    border-radius: 53%;    color: #fff;"><i class="fa fa-minus"></i></a>
                                </div>
                            </div>`;
                $('.loadlanguage').append(html);
            })

            $('body').on('click','.addrowvisa',function(){
                
                rendomnumber = generaterandomnumber();
                var options = $('.loadcountryid').get(0).outerHTML;
                var html = `<div class="col-sm-12 mobileBorder form-group removevisa`+rendomnumber+`" ><div class="col-md-5 col-sm-5" style="padding-left:0px; margin-top: 5px;">
                                                    <div class="form-group">
                                                        <label class="input-label onlyMobile">Visa for Country<span class="symbol required"></span></label>
                                                       <select class="form-control" id="passcountry" name="visa[`+rendomnumber+`][country]">
                                                        `+options+`
                                                    </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 col-sm-3" style="padding-left:0px; margin-top: 5px;">
                                                     <div class="form-group">
                                                           <label class="input-label onlyMobile">Type of Visa<span class="symbol required"></span></label>
                                                        <input type="text" class="form-control" id="mobile" name="visa[`+rendomnumber+`][visa_type]" placeholder="Type of Visa">
                                                        <span class="hide" id="mobile-error" style="color: red"></span>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3 col-sm-3" style="padding-left:0px; margin-top: 5px;">
                                                    <div class="form-group">
                                                        <label class="input-label onlyMobile">Visa Expiry Date<span class="symbol required"></span></label>
                                                        <input type="text" class="form-control datepicker"
                                                               id="passdateofissue" name="visa[`+rendomnumber+`][expiry_date]"
                                                               placeholder="dd-mm-yyyy">
                                                    </div>
                                                </div>

                                                <div class="col-md-1 col-sm-1"  style="padding-left:0px; margin-top: 14px; ">
                                                     <a href="javascript:void(0)" class="removerowvisa" data-id="`+rendomnumber+`"  style="background: #a22a51;padding: 3px 8px;         border-radius: 53%;    color: #fff;"><i class="fa fa-minus"></i></a>
                                                </div></div>`;
                            $('.loadvisa').append(html);
                            $('.datepicker').datepicker({
                format: "dd-mm-yyyy",
                
                autoClose: true,
                
                startView: 2
            }).on('changeDate', function(ev) {
                if(ev.viewMode === 0){
                    $('.datepicker').datepicker('hide');
                }
           });
            })
            $('body').on('click','.removerow',function(){
                var id = $(this).data('id');
                $('.remove'+id).remove();
            })

            $('body').on('click','.removerowvisa',function(){
                var id = $(this).data('id');
                $('.removevisa'+id).remove();
            })

            $('body').on('change','.selectvalueradio',function(){
                var shiptype = $(this).val();
                if(shiptype == 'Home'){
                    $('.typehome').addClass('displayblockf');
                    $('.typehome').removeClass('displaynonef');
                    $('.typeship').addClass('displaynonef');
                    $('.typeship').removeClass('displayblockf');
                }
                if(shiptype == 'Ship'){
                    $('.typeship').addClass('displayblockf');
                    $('.typeship').removeClass('displaynonef');
                    $('.typehome').addClass('displaynonef');
                    $('.typehome').removeClass('displayblockf')
                }
            })
            date = moment().format('DD-MM-YYYY');
            $('.date-of-issue-datepicker').datepicker({
                format: "dd-mm-yyyy",
                endDate: date,
                autoclose: true,
                startView: 2
            }).on('changeDate', function(ev) {
                if(ev.viewMode === 0){
                    $('.issue-datepicker').datepicker('hide');
                }
            });

            date = moment().format('DD-MM-YYYY');
            $('.dateissue').datepicker({
                format: "dd-mm-yyyy",
                
                autoclose: true,
                startView: 2
            });
            

            $('#submitButton').click(function () {
                $('#uploadForm').ajaxForm({
                    target: '#outputImage',
                    url: "{{route('site.user.upload')}}",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content'),
                    },
                    beforeSubmit: function () {
                        $("#outputImage").hide();
                        if ($("#uploadImage").val() == "") {
                            $("#outputImage").show();
                            $("#outputImage").html("<div class='error'>Choose a file to upload.</div>");
                            return false;
                        }
                        $("#progressDivId").css("display", "block");
                        var percentValue = '0%';

                        $('#progressBar').width(percentValue);
                        $('#percent').html(percentValue);
                    },
                    uploadProgress: function (event, position, total, percentComplete) {
                        var percentValue = percentComplete + '%';
                        $("#progressBar").animate({
                            width: '' + percentValue + ''
                        }, {
                            duration: 2500,
                            easing: "linear",
                            step: function (x) {
                                percentText = Math.round(x * 100 / percentComplete);
                                $("#percent").text(percentText + "%");
                                if (percentText == "100") {
                                    $("#outputImage").show();
                                }
                            }
                        });
                    },
                    error: function (response, status, e) {
                        alert('Oops something went.');
                    },

                    complete: function (xhr) {
                        if (xhr.responseText && xhr.responseText != "error") {
                            if(xhr.status == 201){
                                location.reload();
                                return true;
                            }
                            var current = $('#current_container').val();
                            $("#outputImage").html(xhr.responseText.message);
                            setTimeout(function () {
                                $('#ImageCropModal').modal('toggle');
                                $("#outputImage").html('');
                                $(`#${current}`).trigger('click');
                            }, 2500);
                            getUserDocuments();

                            $('#progressBar').width('0%');
                            $('#percent').html('0%');
                            $('#uploadForm').trigger("reset");
                            $('#uploadImagePreview').attr('src', '#');
                            $(".delete-profile-pic").show();
                        } else {
                            $("#outputImage").show();
                            $("#outputImage").html("<div class='error'>Problem in uploading file.</div>");
                            $("#progressBar").stop();
                        }
                    }
                });
            });

            setInterval(function () {
                $("#uploadForm").removeClass("hide");
            }, 2500);


            $(document).on('click', '.edit_doc', function () {
                $('#document_id').val($(this).data('id'));
                $('#current_container').val($(this).data('current'));
                $('#type').val($(this).data('type'));
                $('#type_id').val($(this).data('typeId'));
                $('#document_name').val($(this).data("imagename"));
                $('#progressBar').width('0%');
                $('#percent').html('0%');
                $('#image_container').val($(this).data('container'));
                $('#ImageCropModal').modal('show');

            });

            $(document).on('click','.clicktoopen',function(){
                var id = $(this).data('didm');
                var type = $(this).data('type');
                
                var plus = $('.check'+id+type).val();
                if(plus == 'plus'){
                   $('.'+id+'plus'+type).text('-'); 
                   $('.check'+id+type).val('minus');
                }
                if(plus == 'minus'){
                    
                   $('.'+id+'plus'+type).text('+'); 
                   $('.check'+id+type).val('plus');
                   
                }
                $('.'+id+type).toggle();
            })

            //alert('dsds');
            $(document).on('click', '.wages_currency1', function () {

                var a = $(this).attr('rel');
                $('#currency_span').html(a);
            });
            $(".course_type_filter").on('change', function (e) {
                e.preventDefault();

                var temp_url = $("#api-institute-course-name-by-course-types").val();
                var url_arr = temp_url.split("%");
                var url = url_arr[0] + parseInt($(this).val());

                $(".other_course_name").val('');
                $("#other_course_name").addClass('hide');

                pathname = window.location.pathname;
                is_all = false;
                if (pathname == '/institute/courses') {
                    is_all = true;
                } else if (pathname == '/') {
                    is_all = true;
                }

                $.ajax({
                    type: "get",
                    url: url + '?is_all=' + is_all,
                    statusCode: {
                        200: function (response) {
                            $(".certificate_name option").remove();
                            $(".certificate_name").append('<option value="">Select Course Name</option>');
                            if (response.length > 0) {
                                $.each(response, function (index, value) {
                                    $(".certificate_name").append('<option value=' + value.id + '>' + value.course_name + '</option>');
                                });
                            }
                        }
                    }
                });
            });
        });
        function getUserStorageData(shipId){
            $.ajax({
                url: "{{URL::to('get-ship-data')}}"+'/'+shipId,
                dataType: 'json',
            }).done(function(data){
                $('.ship-data').html(data.shipData);
            }).fail(function(error){

            });
        }
        function getUserDocuments() {

            var url = $("#api-seafarer-get-document-list").val();
            var view_profile = false;
            var profile_status = '';
            var user_id = '';
            user_id = $("#user_id").val();
            if (typeof ($('.profile_status').val()) != 'undefined') {
                if ($('.profile_status').val() == 'view profile') {
                    view_profile = true;

                    profile_status = 'view_profile';
                    user_id = $(".user_id").val();
                }
            }

            $.ajax({
                url: url,
                type: "get",
                data: {
                    profile_status: profile_status,
                    user_id: user_id,
                },
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                },
                statusCode: {
                    200: function (response) {
                        var userData = null;
                        userData = response.result[0];
                        $(".documents_section").empty();
                        $(".documents_section").append(response.template);
                        var usedMb = 0
                        var cuurentMb = 0;
                        var percentageMb = 0;

                        if(userData['used_kb'] != '') {
                            usedMb = userData['used_kb'] / 1024;
                            maxKb = userData['max_kb'] / 1024;
                            percentageMb = (usedMb * 100) / maxKb; 
                        }
                       
                        if(userData['used_kb'] >= 0) {
                            cuurentMb = userData['used_kb'] / 1024;
                            cuurentMb = cuurentMb.toFixed(2);
                        }

                        usedMb = usedMb.toFixed(2);
                        $('.user-bar').text(cuurentMb+'mb');
                        $('.used-mb').text('Used - '+usedMb+'mb');
                        $('.user-bar').css('width',percentageMb);
                        $('.document_slide').slick({
                            arrows: true,
                            autoplay: false,
                            centerMode: false,
                            centerPadding: '0',
                            slidesToShow: 7,
                            slidesToScroll: 1,
                            dots: false,
                            infinite: false,
                            lazyLoad: 'ondemand',
                            prevArrow: '<span class="navArrow_prev navArrow left_arrow"><i class="fa fa-chevron-left"></i></span>',
                            nextArrow: '<span class="pull-right navArrow_next navArrow right_arrow"><i class="fa fa-chevron-right"></i></span>',
                            responsive: [
                                {
                                    breakpoint: 1200,
                                    settings: {
                                        slidesToShow: 5,
                                        slidesToScroll: 1,
                                        centerMode: false,
                                        centerPadding: '0',
                                        dots: false
                                    }
                                },
                                {
                                    breakpoint: 1024,
                                    settings: {
                                        slidesToShow: 3,
                                        slidesToScroll: 1,
                                        centerMode: false,
                                        centerPadding: '0',
                                        dots: false
                                    }
                                },
                                {
                                    breakpoint: 600,
                                    settings: {
                                        initialSlide: 0,
                                        slidesToShow: 2,
                                        slidesToScroll: 1,
                                        centerMode: true,
                                        centerPadding: '0',
                                    }
                                },
                                {
                                    breakpoint: 450,
                                    settings: {
                                        initialSlide: 1,
                                        slidesToShow: 1,
                                        slidesToScroll: 1,
                                        centerMode: true,
                                        centerPadding: '0',
                                    }
                                }
                            ]
                        });

                        /*$('.document_slide').on('setPosition', function () {
                            $(this).find('.slick-slide').height('auto');
                            var slickTrack = $(this).find('.slick-track');
                            var slickTrackHeight = $(slickTrack).height();
                            $(this).find('.slick-slide').css('height', slickTrackHeight + 'px');
                        });*/

                    },
                    400: function (response) {
                        //toastr.error(response.responseJSON.message, 'Error');
                    },
                    422: function (response) {
                        //toastr.error(response.responseJSON.message, 'Error');
                    }
                }
            });
        }
    </script>
    <script>
        $(document).on('click', '.delete_doc', function () {
            var id = $(this).data("id");
            var current = $(this).data('current');
            var img_name = $(this).data("imagename");
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        url: '{{route('site.user.upload.document.delete')}}',
                        type: "POST",
                        data: {"id": id, "img_name": img_name},
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content'),
                        },
                        success: function (data) {
                            getUserDocuments();
                            setTimeout(function () {
                                $(`#${current}`).trigger('click');
                            }, 2500);

                        }
                    });
                    Swal.fire(
                        'Deleted!',
                        'Your file has been deleted.',
                        'success'
                    )
                }
            });
        });

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $("#uploadImagePreview").attr('src', e.target.result);
                    $(".delete-profile-pic").show();
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#uploadImage").change(function () {
            readURL(this);
        });

        $('input[name="gmdss_radio"]').on("change", () => {
            if ($('input[name="gmdss_radio"]:checked').val() == 1) $('.gmdss_container').removeClass('d-none');
            else $('.gmdss_container').addClass('d-none');
        });

        $('input[name="coe_radio"]').on("change", () => {
            if ($('input[name="coe_radio"]:checked').val() == 1) $('.coe_container').removeClass('d-none');
            else $('.coe_container').addClass('d-none');
        });
        
    $(".clear-all-details").on("click", function() {
        if($(this).data("module-type") == 'bank') {
            $("#account_holder_name").val("");
            $("#bank_name").val("");
            $("#account_no").val("");
            $("#ifsc_code").val("");
            $("#branch_address").val("");
        } else if($(this).data("module-type") == 'academic') {
            $("#name_of_school").val("");
            $("#school_to").val("");
            $("#school_qualification").val("");
        } else if($(this).data("module-type") == 'presea') {
            $("#institute_name").val("");
            $("#institute_to").val("");
            $("#institute_degree").val("");
        }
    })

    $(".delete-profile-pic").on("click", function() {
        Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        url: '{{route('site.user.store.delete.profilePic')}}',
                        type: "POST",
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content'),
                        },
                        success: function (data) {
                            $("#preview").attr("src","{{asset('public/images/default.png')}}");
                            $(".delete-profile-pic").hide();
                        }
                    });
                    Swal.fire(
                        'Deleted!',
                        'Your file has been deleted.',
                        'success'
                    )
                }
            });
    });
    </script>
@stop

@section('js_script')
    <script>
        var required_fields = <?php echo json_encode($required_fields) ?>;
    </script>
    <script type="text/javascript" src="{{ URL:: asset('public/assets/js/site/registration.js')}}"></script>
@stop
