@extends('site.cms-index')
@section('content')
<div class="footer-page-container" style="margin-top: 72px;">
    <div class="contact-left">
        <img src="{{asset('public/images/contact-us/contact-us.png')}}" alt="the contact us banner image" class="footer-contactus-image">
    </div>
    <div class="contact-right">
        <div class="footer-page-contact-title">
            <h1 class="footer-page-title">CONTACT US</h1>
        </div>
        <div class="">

            <h3 class="footer-page-midtitle">Do you have any Queries?</h3>
            <p class="footer-page-para">
                Visit our FAQ page.
                If you are still facing any trouble using flanknot.com or if you're having any doubts or queries regarding our policies.
                Contact our support team at: <a href="mailto:support@flanknot.com">support@flanknot.com</a>

            </p>

            <h3 class="footer-page-midtitle">Grievances?</h3>
            <p class="footer-page-para">
                If you have any grievances, criticism, protest or find any content objectable and wish to place a complaint 
                Contact our grievances cell <a href="mailto:grievance@flanknot.com">grievance@flanknot.com</a>
            </p>

            <h3 class="footer-page-midtitle">New Ideas or Suggestions?</h3>
            <p class="footer-page-para">
                Do you have any suggestions or recommendationa for us to improve your user experience. You can even give us new ideas.
                Any of your idea if implemented will be rewarded.
                Send us your suggestions or ideas at <a href="mailto:info@flanknot.com">info@flanknot.com</a>
            </p>
        </div> <!-- footer-contactus-container -->
    </div>


</div>
@stop
