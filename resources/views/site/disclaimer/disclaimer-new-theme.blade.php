@extends('site.cms-index')
@section('content')
    <div class="footer-page-container" style="margin-top: 64px;">
        <h1 class="footer-page-title">DISCLAIMER</h1>
        <p class="footer-page-para">
            flanknot.com is an online networking portal for active seafarers that also displays job advertisements and
            floating requirements but does not guarantee any interview, meeting or job assurance with any employer.
        </p>
        <p class="footer-page-para">
            flanknot.com has provided the employer with an option to quickly respond to the resume shared by the user but
            does not guarantee a response.
        </p>
        <p class="footer-page-para">
            flanknot.com does not guarantee that a positive quick response from the employer the resume was forwarded, will
            ensure in a job.
        </p>
        <p class="footer-page-para">
            flanknot.com does not guarantee accuracy in the information provided by the user in his profile and his resume.
        </p>
        <p class="footer-page-para">
            The authenticity and accuracy of the documents uploaded by the user is the user’s responsibility and
            flanknot.com holds no responsibility in parameters or guidelines towards it.
        </p>
        <p class="footer-page-para">
            Reviews and ratings given by the user towards the onboard experience that includes the vessel and the shipping
            companies that were the user’s employers, are solely the opinion of the user. flanknot.com does not influence
            the review or the reviewer and flanknot.com holds no responsibility the article in the review or the rating.
        </p>
        <p class="footer-page-para">
            flanknot.com does not claim any liability towards emails, sms or phone calls from fraudulent source representing
            flanknot.com.
        </p>
        <p class="footer-page-para">
            flanknot.com does not claim monetary benefits from job applicants for offering job placements and candidates
            should refrain from any such engagement.
        </p>
        <p class="footer-page-para">
            flanknot.com offers only a platform to advertise and does not ensure assured returns.
        </p>
        <p class="footer-page-para">
            The assessment review and rating given by employers are solely their judgement and flanknot.com does not
            influence the review or the reviewer. flanknot.com merely provides a platform where employers can present their
            opinions and takes no responsibility towards the article in the review or the rating.
        </p>
    </div>
@stop
