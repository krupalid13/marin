@extends('site.index')
@section('content')
	<div class="disclaimer content-section-wrapper">
		<div class="section container">
			<div class="section_heading">
				DISCLAIMER
			</div>
			<ul style="padding-left: 20px;">
				<li>
					ConsultanSeas is an online recruitment portal and does not guarantee any
				interview, meeting or job assurity with any employer.
				</li>
				<li>
					ConsultanSeas does not claim any liability towards mails, sms or phone calls
				from fraudulent source representing consultanseas.in
				</li>
				<li>
					ConsultanSeas does not claim monetary benefits from job applicants for offering
				job placements and candidates should refrain from any such engagement.
				</li>
				<li>ConsultanSeas offers a fertile platform to advertise and does not ensure assured
				returns.</li>
			</ul>
		</div>
	</div>
@stop