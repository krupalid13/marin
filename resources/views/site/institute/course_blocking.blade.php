@extends('site.index')
@section('content')
<style type="text/css">
    .table-condensed{
        width: 100% !important;
    }
</style>
<div class="course_blocking">
    <div class="section">
        <div class="container">
            @if(isset($status) && ($status == '0' || $status != ''))
                <?php 
                    $start_date = isset($data['start_date']) ? $data['start_date'] : '';
                    $expiry_date = date('d-m-Y', strtotime('-7 days', strtotime($start_date))); 
                ?>
            <div class="row">
                <div class="col-xs-12">
                    <div class="alert alert-success m-b-0">
                        @if($status == '0')
                            Your booking for this course is awaiting approval from the institute. They will get back to you shortly.
                        @elseif($status == '4')
                            Your booking for this course has been approved by the institute. Your seat will be reserved once your payment has been made.
                        @elseif($status == '1')
                            You have a booking reserved for this course.
                        @elseif($status == '5')
                            Your booking has been denied by the institute.
                        @elseif($status == '6')
                            Please make the pending payment before {{$expiry_date}}. If you fail to make the payment before the {{$expiry_date}} you booking will be cancelled.
                        @endif
                    </div>
                </div>
            </div>
            @endif
            <div class="row">
                <div class="col-lg-4">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card z-depth-1">
                                <input type="hidden" class="selected_date" value="{{ isset($data['start_date']) ? $data['start_date'] : ''}}">
                                <div class="card-header">
                                    @if($status == '' && $status != '0' && isset($data['batch_type']) && $data['batch_type'] != 'on_demand')
                                        <span>Preferred Date</span>
                                    @else
                                        <span>Select Preferred Dates</span>
                                    @endif
                                </div>
                                <div class="card-body">
                                    @if($status != '' || $status == '0')
                                        <div class="list-item">
                                            <span class="icon"><i class="blue icon-calendar"></i></span>
                                                <span class="text">Course Start Date : 
                                                <span class="dark-blue">
                                                    {{ !empty($data['start_date']) ? date('d-m-Y',strtotime($data['start_date'])) : '-'}}
                                                </span>
                                            </span>
                                        </div>
                                        @endif
                                        
                                        @if($status != '' || $status == '0')
                                            <div class="list-item">
                                                <span class="icon"><i class="blue icon-doc"></i></span>
                                                    <span class="text">Order Status : 
                                                    <span class="dark-blue">
                                                        @foreach(\CommonHelper::order_status() as $r_index => $status_text)
                                                            {{ isset($status) ? $status == $r_index ? $status_text : '' : ''}}
                                                        @endforeach
                                                    </span>
                                                </span>
                                            </div>
                                        @endif
                                    @if($status == '' && $status != '0' && isset($data['batch_type']) && $data['batch_type'] != 'on_demand')
                                    <div class="detailsBlock_content">
                                        <div class="select_date"></div>
                                    </div>
                                    @endif
                                    @if($data['batch_type'] == 'on_demand' && $status == '' && $status != '0')
                                    <form id="block-form">
                                        <?php
                                            $confirmed_batches = [];
                                            if(isset($data['child_batches']) && !empty($data['child_batches'])){
                                                $collection = Collect($data['child_batches']);
                                                $confirmed_batches = $collection->pluck('preffered_type')->toArray();
                                                $data['child_batches'] = $data['child_batches']->toArray();
                                            }
                                        ?>
                                        @if(isset($data['on_demand_batch_type']) && !empty($data['on_demand_batch_type']))
                                            <?php 
                                                $start_date = $data['start_date'];

                                                $split_date = explode('-', $start_date);

                                                $year = $split_date[0];
                                                $month = $split_date[1];

                                                $start_date = date("$year-$month-1");
                                                if(date('Y-m-d') > $start_date){
                                                    $start_date = date('Y-m-d');
                                                }

                                                $end_date = date("Y-m-t", strtotime($start_date));

                                                $week = 1;
                                                $date = Carbon\Carbon::now();
                                                $date->setISODate($year,$week);
                                                $start_date = Carbon\Carbon::createFromFormat('Y-m-d', $start_date);
                                                $end_date = Carbon\Carbon::createFromFormat('Y-m-d', $end_date);
                                            ?>
                                            <div class="row mb-15">
                                                <div class="col-lg-12 text-center">
                                                    <!-- <label class="radio-inline"><input type="radio" name="optradio" checked>1-15</label>
                                                    <label class="radio-inline"><input type="radio" name="optradio">16-30</label> -->
                                                    <input type="hidden" class="on_demand_batch_type" value="{{isset($data['on_demand_batch_type']) ? $data['on_demand_batch_type'] : ''}}">
                                                    @if($data['on_demand_batch_type'] == '2')
                                                        <span class="value">
                                                            <span class="p-l-5">
                                                                @if(!in_array('1-2',$confirmed_batches))
                                                                <label class="radio-inline week-1-2">
                                                                    <input type="radio" class="week_change" name="week_change" data-week='1-2' value="1-2"> 1-15
                                                                </label>
                                                                @endif
                                                                @if(!in_array('3-4',$confirmed_batches))
                                                                <label class="radio-inline week-3-4">
                                                                    <input type="radio" class="week_change" name="week_change" data-week='3-4' value="3-4"> 15-30
                                                                </label>
                                                                @endif
                                                            </span>
                                                        </span>
                                                    @elseif($data['on_demand_batch_type'] == '3')
                                                        @if(!in_array('Monthly',$confirmed_batches))
                                                        <span class="is_redText is_bold text">Select Week : </span>
                                                        <span class="value">
                                                            <span class="p-l-5">
                                                                <label class="radio-inline">
                                                                    <input type="radio" class="week_change" name="week_change" data-week='monthly' value="monthly"> Monthly
                                                                </label>
                                                            </span>
                                                        </span>
                                                        @endif
                                                    @endif
                                                </div>
                                                <div class="col-sm-12 text-center">
                                                    <span class="is_redText is_bold text">
                                                        <label for="week_change" class="week_change_error hide">Please select week.</label>
                                                    </span>
                                                </div>
                                            </div>
                                        @endif
                                        <div class="row border-top">
                                            <div class="col-lg-12">
                                                <div id="preffered-datepicker" class="preffered-datepicker" style="width:100%;height:200px;"></div>
                                                <!-- <div id="course_block_date" class=" course_block_date  mt-15 "></div> -->
                                            </div>
                                            <span class="p-l-5 d-flex">
                                                <input type="hidden" id="dbdatepicker" class="dbdatepicker" name="dbdatepicker" data-rule-dbdatepickervalue="true">
                                            </span>
                                        </div>  
                                    </form>
                                    @endif
                                    <div class="row border-top">
                                        <div class="col-lg-12"><!-- border-right -->
                                            <div class="d-flex"> 
                                               <div class="d-flex--one">
                                                    <div class="circle">
                                                        <span> {{ !empty($data['duration']) ? $data['duration'] : '-'}}</span>
                                                    </div>
                                               </div>
                                               <div class="d-flex--two">
                                                   Course Duration (Days)
                                               </div>
                                            </div>
                                        </div>
                                        <!-- <div class="col-lg-6">
                                            <div class="d-flex">
                                               <div class="d-flex--one">
                                                    <div class="circle">
                                                        <span>15</span>
                                                    </div>
                                               </div>
                                               <div class="d-flex--two">
                                                   Booked Seats
                                               </div>
                                            </div>
                                        </div> -->
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <?php 
                                    $cgst = env('CGST_PERCENT');
                                    $sgst = env('SGST_PERCENT');

                                    $cost = $data['cost'];
                                    
                                    if(isset($discount) && !empty($discount)){
                                        $discount_amt = round(($cost*$discount)/100);
                                        $cost = $cost-$discount_amt;
                                    }

                                    $cgst_cost = round(($cost*$cgst)/100);
                                    $sgst_cost = round(($cost*$sgst)/100);
                                    $inr_symbol = "<i class='fa fa-inr'></i>";

                                    $total = $cost+$cgst_cost+$sgst_cost;

                                    if(isset($data['payment_type']) && $data['payment_type'] == 'split'){
                                        $initial_per = $data['initial_per'];
                                        $pending_per = $data['pending_per'];

                                        $initial_amt = round(($total*$data['initial_per'])/100);
                                        $pending_amt = round(($total*$data['pending_per'])/100);
                                    }
                                ?>
                                <?php
                                    $batch_id = isset($data['batch_location'][0]['batch_id']) ? $data['batch_location'][0]['batch_id'] : '';
                                    $location_id = isset($data['batch_location'][0]['location_id']) ? $data['batch_location'][0]['location_id'] : '';
                                    $order_id = isset($order_id) && !empty($order_id) ? $order_id : '';
                                ?>
                                <div class="card z-depth-1">
                                    <div class="card-header"><span>Payment Details</span></div>
                                    <div class="card-body">
                                        <div class="list">
                                            <div class="list-item">
                                                <span class="icon"><i class="blue icon-wallet"></i></span>
                                                <span class="text">Course Fees : 
                                                    <span class="label label-info">
                                                        <span class="fa fa-inr"></span>
                                                        {{ isset($data['cost']) ? $data['cost'] : '-'}}
                                                    </span>
                                                    <!-- <strike><span class="label label-info">4800</span></strike></span> -->
                                                </span>
                                            </div>
                                            <input type="hidden" name="discount" class="discount" value="{{ isset($discount) ? $discount : '-'}}">
                                            @if(isset($discount) && !empty($discount))
                                                <?php
                                                    $discount_applied = round(($data['cost']*$discount)/100);
                                                    $discounted_amount = $data['cost']-$discount_applied;
                                                ?>
                                                <div class="list-item">
                                                    <span class="icon"><i class="blue icon-wallet"></i></span><span class="text">Discounted Fees : <span class="label label-danger">{{ isset($discounted_amount) ? $discounted_amount : '-'}}</span></span>
                                                </div>
                                            @endif
                                            <div class="list-item">
                                                <span class="icon"><i class="blue icon-wallet"></i></span><span class="text">Cgst : 
                                                    <span class="label label-info"><span class="fa fa-inr"></span> {{ isset($cgst_cost) ? $cgst_cost : '-'}}</span></span>
                                            </div>
                                            <div class="list-item">
                                                <span class="icon"><i class="blue icon-wallet"></i></span><span class="text">Sgst : 
                                                    <span class="label label-info"><span class="fa fa-inr"></span> {{ isset($sgst_cost) ? $sgst_cost : '-'}}</span></span>
                                            </div>
                                            <div class="list-item">
                                                <span class="icon"><i class="blue icon-wallet"></i></span><span class="text">Total : 
                                                    <span class="label label-success">
                                                        <span class="fa fa-inr"></span>
                                                        {{ isset($total) ? $total : '-'}}
                                                    </span>
                                                </span>
                                            </div>
                                            @if($data['payment_type'] == 'split')
                                                <div class="list-item">
                                                    <span class="icon"><i class="blue icon-credit-card"></i></span><span class="text">Payment Option: </span>
                                                    <div class="sublist">
                                                        <span class="dark-blue"> <i class=""></i> Split Payment</span>
                                                        <div class="sublist-item">1<sup>st</sup> Installment - 
                                                            <span class="dark-blue">
                                                                Rs {{ isset($initial_amt) ? $initial_amt : '-'}} ({{$initial_per}}%)
                                                            </span>
                                                        </div>
                                                        <div class="sublist-item">2<sup>nd</sup> Installment - 
                                                            <span class="dark-blue">Rs {{ isset($pending_amt) ? $pending_amt : '-'}} ({{$pending_per}}%)
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                                @if($make_payment == '1' && !empty($status) && $status == '4')
                                                    <div class="detailsBlock has_border">
                                                        <div class="detailsBlock_content p_lr_15">
                                                            <div class="list-item">

                                                                Payment By :
                                                            </div>
                                                        </div>
                                                        <div class="detailsBlock_content p_lr_15">
                                                            <div class="list-item">
                                                                <span class="value">
                                                                    <span class="p-l-5">
                                                                        <label class="radio-inline">
                                                                            <input type="radio" class="payment_by" name="payment_by" value="single" checked> Single Payment 
                                                                        </label>
                                                                        @if($data['payment_type'] == 'split')
                                                                            @if($allow_split_payment)
                                                                            <label class="radio-inline">
                                                                                <input type="radio" class="payment_by" name="payment_by" value="split"> Split Payment
                                                                            </label>
                                                                            @endif
                                                                        @endif
                                                                    </span>
                                                                </span>
                                                            </div>
                                                            <div class="list-item">
                                                                <span class="icon">
                                                                    <i class="blue icon-credit-card"></i>
                                                                </span>
                                                                <span class="text">Course Fees : 
                                                                     
                                                                </span>
                                                               <span class="value dark-blue">
                                                                        <span class="fa fa-inr"></span>
                                                                        {{ isset($data['cost']) ? $data['cost'] : '-'}}
                                                                    </span>
                                                            </div>
                                                            @if(isset($discounted_amount) && !empty($discounted_amount))
                                                            <div class="list-item">
                                                                <span class="icon">
                                                                    <i class="blue icon-credit-card"></i>
                                                                </span>
                                                                <span class="text">Discounted Course Fees : 
                                                                    <span class="value dark-blue">
                                                                        <span class="fa fa-inr"></span>
                                                                        {{ isset($discounted_amount) ? $discounted_amount : '-'}}
                                                                    </span>
                                                                </span>
                                                                
                                                            </div>
                                                            @endif
                                                            <div class="list-item">
                                                                <span class="icon">
                                                                    <i class="blue icon-credit-card"></i>
                                                                </span>
                                                                <span class="text">CGST : </span>
                                                                <span class="value dark-blue">
                                                                    <span class="fa fa-inr"></span>
                                                                    {{ isset($cgst_cost) ? $cgst_cost : '-'}}
                                                                </span>
                                                            </div> 
                                                            <div class="list-item">
                                                                <span class="icon">
                                                                    <i class="blue icon-credit-card"></i>
                                                                </span>
                                                                <span class="text">SGST : </span>
                                                                <span class="value dark-blue">
                                                                    <span class="fa fa-inr"></span>
                                                                    {{ isset($sgst_cost) ? $sgst_cost : '-'}}
                                                                </span>
                                                            </div>
                                                            <div class="list-item single_payment_details">
                                                                <span class="icon">
                                                                    <i class="blue icon-credit-card"></i>
                                                                </span>
                                                                <span class="text">Amount to be paid now : </span>
                                                                <span class="value dark-blue">
                                                                    <span class="fa fa-inr"></span>
                                                                    {{ isset($total) ? $total : '-'}}
                                                                </span>
                                                            </div>
                                                            @if($data['payment_type'] == 'split')
                                                            <div class="split_payment_details hide">
                                                                <div class="list-item">
                                                                    <span class="icon">
                                                                        <i class="icon-wallet blue"></i>
                                                                    </span>
                                                                    <span class=" text">Initial Payment : </span>
                                                                    <span class="value dark-blue">
                                                                        <span class="fa fa-inr"></span>
                                                                        {{ isset($initial_amt) ? $initial_amt : '-'}}
                                                                    </span>
                                                                </div>
                                                                <div class="list-item">
                                                                    <span class="icon">
                                                                        <i class="icon-wallet blue"></i>
                                                                    </span>
                                                                    <span class=" text">Pending Payment : </span>
                                                                    <span class="value dark-blue">
                                                                        <span class="fa fa-inr"></span>
                                                                        {{ isset($pending_amt) ? $pending_amt : '-'}}
                                                                    </span>
                                                                </div>
                                                                <div class="list-item">
                                                                    <span class="icon">
                                                                        <i class="icon-wallet blue"></i>
                                                                    </span>
                                                                    <span class=" text">Amount to be paid now : </span>
                                                                    <span class="value dark-blue">
                                                                        <span class="fa fa-inr"></span>
                                                                        {{ isset($initial_amt) ? $initial_amt : '-'}}
                                                                    </span>
                                                                </div>
                                                            </div>
                                                            @endif
                                                        </div>
                                                    </div>
                                                @endif
                                            @endif
                                            <!-- <div class="list-item">
                                                <span class="red-text-only">Note* - 2nd Installment of 60% payment need to be paid on and before 10/10/2018.</span>
                                            </div> -->

                                            @if(isset($prev_batch_details[0]) && !empty($status) && $status == '6')
                                                <div class="detailsBlock has_border">
                                                    <div class="detailsBlock_content p_lr_15">
                                                        <div class="list-item">
                                                            <span class="icon">
                                                                <i class="icon-wallet blue"></i>
                                                            </span>
                                                            <span class="text">Payment Done By : </span>
                                                            <span class="value dark-blue">
                                                                Split Payment
                                                            </span>
                                                        </div>
                                                        <div class="list-item">
                                                            <span class="icon">
                                                                <i class="icon-wallet blue"></i>
                                                            </span>
                                                            <span class="text">Amount Paid : </span>
                                                            <span class="value dark-blue">
                                                                <span class="fa fa-inr"></span>
                                                                {{ isset($prev_batch_details[0]['order_payments'][0]['amount']) ? $prev_batch_details[0]['order_payments'][0]['amount'] : '-'}}
                                                            </span>
                                                        </div>
                                                        <div class="list-item">
                                                            <span class="icon">
                                                                <i class="icon-wallet blue"></i>
                                                            </span>
                                                            <span class="text">Remaining Course Fees : </span>
                                                            <span class="value dark-blue">
                                                                <span class="fa fa-inr"></span>
                                                                {{ isset($pending_amt) ? $pending_amt : '-'}}
                                                            </span>
                                                        </div>
                                                        <div class="list-item single_payment_details">
                                                            <span class="icon">
                                                                <i class="icon-wallet blue"></i>
                                                            </span>
                                                            <span class="text">Amount to be paid : </span>
                                                            <span class="value dark-blue">
                                                                <span class="fa fa-inr"></span>
                                                                {{ isset($pending_amt) ? $pending_amt : '-'}}
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                        </div>
                    </div>                   
                </div>
                <div class="col-lg-8">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card z-depth-1">
                                <div class="card-header"><span>About Course</span></div>
                                <div class="card-body">
                                    <div class="tab-wrapper">
                                        <!-- Nav tabs -->
                                        <ul class="course_list nav nav-tabs" role="tablist">
                                            <li role="presentation" class="course_list_item active">
                                                <a href="#about_course" aria-controls="about_course" role="tab" data-toggle="tab">Course Details</a>
                                            </li>
                                            <li role="presentation" class="course_list_item">
                                                <a href="#institutes_details" aria-controls="institutes_details" role="tab" data-toggle="tab">Institute Details</a>
                                            </li>
                                            <li role="presentation"class="course_list_item">
                                                <a href="#key_topics" aria-controls="key_topics" role="tab" data-toggle="tab">Training center Address</a>
                                            </li>
                                        </ul>
                                        <!-- Tab panes -->
                                        <div class="tab-content">
                                            <div role="tabpanel" class="tab-pane active" id="about_course">
                                                <div class="content">
                                                    <div class="row">
                                                        <div class="col-lg-12">
                                                            <div class="list">
                                                                <div class="list-item">
                                                                    <span class="icon"><i class="blue icon-book-open"></i></span>
                                                                    <span class="text">Course Type : 
                                                                        <span class="dark-blue">
                                                                            @foreach(\CommonHelper::institute_course_types() as $index => $course)
                                                                                {{ isset($data['course_details']['course_type']) ? $data['course_details']['course_type'] == $index ? $course : '' : ''}}
                                                                            @endforeach
                                                                        </span>
                                                                    </span>
                                                                </div>
                                                                <div class="list-item">
                                                                    <span class="icon"><i class="blue icon-heart"></i></span>
                                                                        <span class="text">Course Name : 
                                                                        <span class="dark-blue">
                                                                            @foreach($all_courses as $index => $category)
                                                                                {{ !empty($data['course_details']['course_id']) ? $data['course_details']['course_id'] == $category['id'] ? $category['course_name'] : '' : ''}}
                                                                            @endforeach
                                                                        </span>
                                                                    </span>
                                                                </div>
                                                                <div class="list-item">
                                                                    <span class="icon"><i class="blue icon-heart"></i></span>
                                                                        <span class="text">Course Status : 
                                                                            <span class="dark-blue">
                                                                                 @if($data['batch_type'] == 'on_demand')
                                                                                    To be Confirmed / On Demand
                                                                                @else
                                                                                    Confirmed
                                                                                @endif
                                                                            </span>
                                                                    </span>
                                                                </div>
                                                                <div class="list-item">
                                                                    <span class="icon"><i class="blue icon-layers"></i></span><span class="text">Batch Size : <span class="dark-blue">{{ !empty($data['size']) ? $data['size'] : '-'}} Members</span></span>
                                                                </div>
                                                                <div class="list-item">
                                                                    <span class="icon"><i class="blue icon-calendar"></i></span>
                                                                        <span class="text">Course Duration : 
                                                                            <span class="dark-blue">
                                                                                {{ !empty($data['duration']) ? $data['duration'] : '-'}} days
                                                                            </span>
                                                                        </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div role="tabpanel" class="tab-pane" id="institutes_details">
                                                <div class="content">
                                                    <div class="row">
                                                        <div class="col-lg-12">
                                                            <div class="list">
                                                                <div class="list-item">
                                                                    <span class="icon"><i class="blue icon-heart"></i></span>
                                                                    <span class="text">Institute Name : 
                                                                        <span class="dark-blue">
                                                                            {{ isset($data['course_details']['institute_registration_detail']['institute_name']) ? $data['course_details']['institute_registration_detail']['institute_name'] : '-'}}
                                                                        </span>
                                                                    </span>
                                                                </div>
                                                                <div class="list-item">
                                                                    <span class="icon"><i class="blue icon-heart"></i></span>
                                                                    <span class="text">Institute Email : 
                                                                        <span class="dark-blue">
                                                                            {{ isset($data['course_details']['institute_details']['institute_email']) ? $data['course_details']['institute_details']['institute_email'] : '-'}}
                                                                        </span>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php $location_found = 0;?>
                                            <div role="tabpanel" class="tab-pane" id="key_topics">
                                                <div class="content">
                                                    <div class="row">
                                                        <div class="col-lg-12">
                                                            <div class="list">
                                                                <div class="list-item">
                                                                    <span class="icon"><i class="blue icon-home"></i></span>
                                                                    <span class="text">Address : 
                                                                        <span class="dark-blue">
                                                                             @if(isset($data['batch_location']) && !empty(isset($data['batch_location'])))
                                                                                @foreach($data['batch_location'] as $index => $location)
                                                                                    @if($location['location']['id'] == $selected_location_id)
                                                                                        {{$location['location']['address']}},
                                                                                        {{$location['location']['city']['name']}} -
                                                                                        {{$location['location']['pincode_text']}}.
                                                                                        <?php $location_found = 1;?>
                                                                                    @endif
                                                                                @endforeach
                                                                            @endif
                                                                        </span>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="card z-depth-1">
                                <div class="card-header">Course Overview</div>
                                <div class="card-body">
                                    <div class="tab-wrapper">
                                        <!-- Nav tabs -->
                                        <ul class="course_list nav nav-tabs" role="tablist">
                                            <li role="presentation" class="course_list_item active"><a href="#course_description" aria-controls="course_description" role="tab" data-toggle="tab">Course Description</a></li>
                                            <li role="presentation" class="course_list_item"><a href="#validity_certificate" aria-controls="validity_certificate" role="tab" data-toggle="tab">Validity of Certificate</a></li>
                                            <li role="presentation"class="course_list_item"><a href="#course_obj" aria-controls="course_obj" role="tab" data-toggle="tab">Course For</a></li>
                                            <!-- <li role="presentation"class="course_list_item"><a href="#key_topics" aria-controls="key_topics" role="tab" data-toggle="tab">Course Key Topics</a></li> -->
                                        </ul>
                                        <!-- Tab panes -->
                                        <div class="tab-content">
                                            <div role="tabpanel" class="tab-pane active" id="course_description">
                                                <div class="content">
                                                    <div class="row">
                                                        <div class="col-lg-12">
                                                            {{ isset($data['course_details']['description']) ? $data['course_details']['description'] : '-'}}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div role="tabpanel" class="tab-pane" id="validity_certificate">
                                                <div class="content">
                                                    <div class="row">
                                                        <div class="col-lg-12">
                                                            {{ isset($data['course_details']['certificate_validity']) ? $data['course_details']['certificate_validity'].' Years' : '-'}}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div role="tabpanel" class="tab-pane" id="course_obj">
                                                <div class="content">
                                                    <div class="row">
                                                        <div class="col-lg-12">
                                                            {{ isset($data['course_details']['key_topics']) ? $data['course_details']['key_topics'] : '-'}}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div role="tabpanel" class="tab-pane" id="key_topics">
                                                <div class="content">
                                                    <div class="row">
                                                        <div class="col-lg-12">
                                                            <div class="content-title"></div>
                                                        </div>
                                                        <div class="col-lg-12">
                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> 
                        <div class="col-lg-12">
                             <div class="card z-depth-1">
                                <div class="card-header"><span>Required Documents</span></div>
                                <div class="card-body">
                                    <div class="list">
                                        <div class="list-item">
                                            <span class="icon"><i class="blue icon-clock"></i></span>
                                            <span class="text">Course Eligibility : 
                                                <span class="dark-blue">
                                                    {{ isset($data['course_details']['eligibility']) ? $data['course_details']['eligibility'] : '-'}}
                                                </span>
                                            </span>
                                        </div>
                                        <div class="list-item">
                                            <span class="icon"><i class="blue icon-docs"></i></span>
                                            <span class="text">Documents Required : 
                                                <span class="dark-blue">
                                                    <?php
                                                        $req_doc = [];
                                                        if(!empty($data['course_details']['documents_req'])){
                                                            $req_doc = explode(',',$data['course_details']['documents_req']);
                                                        }

                                                    ?>
                                                    <span class="ans">
                                                        @if(!empty($req_doc))
                                                            @foreach(\CommonHelper::req_documents() as $c_index => $doc)
                                                                @if(in_array($c_index,$req_doc))
                                                                    {{$doc}}
                                                                @endif
                                                            @endforeach
                                                        @else
                                                            {{ isset($data['course_details']['documents_req']) ? $data['course_details']['documents_req'] : '-'}}
                                                        @endif
                                                    </span>
                                                </span>
                                            </span>
                                        </div>
                                        @if($role == 'seafarer')
                                            <div class="list-item">
                                                <span class="icon">
                                                    <button class="btn cs-primary-btn navbar-btn edit_link ladda-button" data-style="zoom-in" href="http://dev.course4sea.com/seafarer/profile/edit" data-form="user_document">Upload Documents</button>
                                                </span>
                                                <span class="red-text-only">Note : Click this button to upload your documents</span>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @if($role == 'seafarer')
                <div class="row" style="margin-top: 20px;">
                    <div class="col-sm-12">
                        @if($make_payment == '1')
                            @if(!empty($status) && $status == '4')
                            <span>
                                <button type="button" data-style="zoom-in" class="btn coss-primary-btn ladda-button pull-right make-payment" data-batch="{{$batch_id}}" data-order-id="{{$order_id}}" data-location="{{$location_id}}">Make Payment</button>
                            </span>
                            <span class="">
                                <button type="button" data-style="zoom-in" class="m-r-10 btn coss-primary-btn ladda-button pull-right cancel-payment" data-batch="{{$batch_id}}" data-order-id="{{$order_id}}" data-location="{{$location_id}}">Cancel Booking</button>
                            </span>
                            @endif
                        @else
                            @if($prev_batch_count == 0)
                                <button type="button" data-style="zoom-in" class="btn coss-primary-btn ladda-button pull-right block-seat" data-batch="{{$batch_id}}" data-location="{{$location_id}}">Block Seat</button>
                            @endif
                        @endif
                        @if(isset($prev_batch_details[0]) && !empty($status) && $status == '6')
                            <div class="detailsBlock no-border">
                                <span>
                                    <button type="button" data-style="zoom-in" class="btn coss-primary-btn ladda-button pull-right make-payment" data-pay="split" data-batch="{{$batch_id}}" data-order-id="{{$order_id}}" data-location="{{$location_id}}">Make Pending Payment</button>
                                </span>
                            </div>
                        @endif
                    </div>
                </div>
            @elseif($role == '')
                <div class="row" style="margin-top: 20px;">
                    <div class="col-sm-12">
                        <button type="button" data-style="zoom-in" class="btn coss-primary-btn ladda-button pull-right block-seat" data-batch="{{$batch_id}}" data-location="{{$location_id}}">Block Seat</button>
                    </div>
                </div>
            @endif
        </div>
    </div>

    <form id="payumoney-form">
    </form>
</div>

<div id="login-popup-reg-modal" class="modal fade" role="dialog">
    <div id="login-popup" class="white-popup signin-sigup-default" role="dialog" style="margin-top: 5%;">
        <button type="button" class="close" data-dismiss="modal" style="margin: 10px;">&times;</button>
        <div class="signin-signup-heading">
            <div class="signin-book new-sigin-link active"><a class="open-popup-link">Login</a></div>
            <div class="signin-book pre-register-link"><a class="open-popup-link">Register</a></div>
        </div>

        <!-- login form -->
        <form id="loginFormModal-preRegister" method="post" action="{{route('site.login')}}">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">

            <div class="new-login-content">
                <div class="row">
                    
                    <div class="small-12 medium-12 columns">
                        <div class="new-login-form-container">
                            <div id="login-form" @if(session('login_error')) style="display: block;" @endif style="display: block;">
                                <div class="row margin-vert-5">
                                    <div class="col-sm-3 padding-top-5">
                                      <label class="mar-b-10"><span class="label-icon"><i class="fa fa-envelope" aria-hidden="true"></i></span><span class="label-text mandatory">Email</span>
                                    </label>
                                    </div>
                                    <div class="col-sm-9">
                                        <input class="form-control" type="email" name="email" placeholder="Enter email" value="{{ old('email') }}"/>
                                    </div>
                                </div>
                                <div class="row margin-vert-5">
                                    <div class="col-sm-3 padding-top-5">
                                      <label class="mar-b-10"><span class="label-icon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span><span class="label-text mandatory">Password</span>
                                      </label>
                                    </div>
                                    <div class="col-sm-9">
                                        <input class="form-control" type="password" name="password" placeholder="Enter password"/>
                                        <label id="login_error" class="error"></label>
                                    </div>
                                </div>
                                <div class="row margin-vert-5">
                                    <div class="col-sm-6">
                                        <input  name="remember" type="checkbox">
                                        <label for="remember">Remember Me</label>  
                                    </div>
                                    <div class="col-sm-6">
                                        <a href="{{route('site.reset.password.view')}}" class="pull-right">Forget Password?</a> 
                                    </div>
                                </div>
                                <div class="row margin-vert-5">
                                    <div class="col-sm-12">
                                        <button type="button" data-style='zoom-in' class="btn-1 light-blue new-sign-up-btn fullwidth ladda-button" id="sign-in-btn-pre-registration">Sign In</button>
                                    </div>
                                </div>
                                <!-- <div class="row margin-vert-5">
                                    <div class="col-sm-12">
                                        <div class="forget-pass-link">
                                            <a data-mfp-src="#forgot-password-popup" class="open-popup-link">Forgot Password</a>
                                        </div>
                                    </div>
                                </div> -->
                                <div class="row">
                                    <div class="small-12 columns info" align="center" style="display:none">
                                        Logging in. Please wait...
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="ajaxError small-12 columns" align="center"
                                         style="display:{{ session('login_error') ? 'block' : 'none' }}">
                                        <div class="" style="color:#ff0000;">
                                            <strong>Login failed!!!</strong>
                                        </div>
                                        <div class="reason">
                                            {{ session('login_error') ? session('login_error') : '' }}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>

        <!-- new pre registration -->
        <form id="preRegistrationFormModal" class="hide" method="post" action="{{route('site.seafarer.pre.register')}}">
            <input type="hidden" name="user_id" value="{{isset($user_data[0]['id']) ? $user_data[0]['id'] : ''}}">
            <div class="new-login-content">
                <div class="row">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="input-label" for="email">Email Address<span class="symbol required"></span></label>
                        </div>
                    </div>
                    <div class="col-sm-9">
                        <div class="form-group">
                            <input type="email" class="form-control" id="user_email" name="email" placeholder="Type your email address">
                            <span class="hide" id="email-error" style="color: red"></span>
                        </div>  
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="input-label" for="password">Password<span class="symbol required"></span></label>
                        </div>
                    </div>
                    <div class="col-sm-9">
                        <div class="form-group">
                            <input type="password" class="form-control" id="password" name="password" placeholder="Type your password">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="input-label" for="cpassword">Confirm Password<span class="symbol required"></span></label>
                        </div>
                    </div>
                    <div class="col-sm-9">
                        <div class="form-group">
                            <input type="password" class="form-control" name="cpassword" placeholder="Type password again">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="input-label" for="firstname">Full Name<span class="symbol required"></span></label>
                        </div>
                    </div>
                    <div class="col-sm-9">
                        <div class="form-group">    
                            <input type="text" class="form-control" name="firstname" placeholder="Type your full name">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="input-label" for="dob">Date Of Birth<span class="symbol required"></span></label>
                        </div>
                    </div>
                    <div class="col-sm-9">
                        <div class="form-group">
                            <input type="text" class="form-control birth-date-datepicker" name="dob" placeholder="dd-mm-yyyy">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="input-label" id="mobile" for="mobile">Mobile Number<span class="symbol required"></span></label>
                        </div>
                    </div>
                    <div class="col-sm-9">
                        <div class="form-group">
                            <input type="text" class="form-control" id="mobile" name="mobile" placeholder="Type your mobile number">
                            <span class="hide" id="mobile-error" style="color: red"></span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="input-label" for="current_rank">Current Rank<span class="symbol required"></span></label>
                        </div>
                    </div>
                    <div class="col-sm-9">
                        <div class="form-group">
                            <select id="current_rank" name="current_rank" class="form-control">
                                <option value="">Select Your Rank</option>
                                @foreach(\CommonHelper::new_rank() as $index => $category)
                                    <optgroup label="{{$index}}">
                                    @foreach($category as $r_index => $rank)
                                        <option value="{{$r_index}}">{{$rank}} </option>
                                    @endforeach
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="input-label" for="passport">Passport Number<span class="symbol required"></span></label>
                        </div>
                    </div>
                    <div class="col-sm-9">
                        <div class="form-group">
                            <input type="text" class="form-control" id="passport" name="passport" placeholder="Type your passport number">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="input-label" for="passport">Indos Number<span class="symbol required"></span></label>
                        </div>
                    </div>
                    <div class="col-sm-9">
                        <div class="form-group">
                            <input type="text" class="form-control" id="indosno" name="indosno" placeholder="Type the indos number">
                        </div>
                    </div>
                </div>
                <div class="row margin-vert-5">
                    <div class="col-sm-12">
                        <button type="button" data-style='zoom-in' class="btn-1 new-sign-up-btn fullwidth ladda-button" id="pre-register-user">Register</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<!--custom footer-->
<div class="cs-custom-footer" style="background-image:url('public/assets/images/home/footer.png')">
    <div class="container">
        <div class="row mr-bottom-0">
        <div class="col-md-2 col-xs-6">
                <div class="footer-title">Quick links</div>
                <ul class="links-list">
                    <li class="links-list-menu"><a href="">About Us</a></li>
                    <li class="links-list-menu"><a href="">Contact Us</a></li>
                    <li class="links-list-menu"><a href="">Disclaimer</a></li>
                    <li class="links-list-menu"><a href="">Policy</a></li>
                </ul>
            </div>
            <div class="col-md-2 col-xs-6">
                <div class="footer-title">Contact Us</div>
                <ul class="links-list">
                    <li class="links-list-menu"> <span class="fa fa-map-marker-alt"></span> 508/8, Coral Crest, <br> 
                        Nerul East, Navi Mumbai - 400708
                    </li>
                </ul>
            </div>
            <div class="col-md-3 col-xs-12">
               <div class="footer-title"></div>
                <ul class="links-list">
                    <li class="links-list-menu"><a href=""><span class="icon fa fa-envelope"></span>  techsupport@course4sea.com</a></li>
                    <li class="links-list-menu"><a href=""><span class="icon fa fa-envelope"></span>  usersupport@course4sea.com</a></li>
                    <li class="links-list-menu"><a href=""><span class="icon fa fa-envelope"></span>  course4sea@gmail.com</a></li>
                </ul>
            </div>
            <div class="col-md-2 col-xs-12">
                <div class="footer-title"></div>
                 <ul class="links-list">
                    <li class="links-list-menu"><a href=""><span class="icon fa fa-mobile"></span>  7738169926</a></li>
                    <li class="links-list-menu"><a href=""><span class="icon fa fa-mobile"></span>  7738169926</a></li>
                </ul>
            </div>
            <div class="col-md-3 col-xs-12">
                <div class="footer-title" style="padding:20px 10px;">Connect with us on </div>
                <ul class="social-icons">
                    <li class="social-icons-menu"><a href=""><img src="{{ URL:: asset('public/assets/images/home/fb.png')}}" alt="facebooklogo"></a></li>
                    <li class="social-icons-menu"><a href=""><img src="{{ URL:: asset('public/assets/images/home/linked-in.png')}}" alt="linked in"></a></li>
                    <li class="social-icons-menu"><a href=""><img src="{{ URL:: asset('public/assets/images/home/googleplus.png')}}" alt="google"></a></li>
                    <li class="social-icons-menu"><a href=""><img src="{{ URL:: asset('public/assets/images/home/youtub.png')}}" alt="youtube"></a></li>
                </ul>
            </div>
        </div>
    </div>
</div>

@stop

@section('js_script')
    <script type="text/javascript" src="{{ URL:: asset('public/assets/js/site/institutes_details.js')}}"></script>
@stop
