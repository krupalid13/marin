@extends('site.index')

@section('content')
<style type="text/css">
    .batch-description{
        background-color: #fff;
        border-radius: 4px;
        box-shadow: 0 1px 3px 0px #b1abab;
        margin-top: 10px;
        padding: 10px;
    }
    .other-discription{
        color: #8e8e8e;
        padding: 3px 0px 3px 15px;
        font-size: 16px;
    }
</style>

<div class="candidate-search content-section-wrapper dashboard-body dashboard-body">
    @include('site.partials.sidebar')
    
    <main class="main" style="height: 100vh;">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-12">
                    <div class="company-page-profile-heading">
                        Course Search

                        <button type="button" class="btn btn-success pull-right addCourseModal_btn" data-toggle="modal" data-target="#addCourseModal">
                            <i class="fa fa-plus-circle nav-icon" aria-hidden="true"></i>
                            Add Course
                        </button>
                    </div>
                </div>
            </div>

            <div class="candidate-search-container">

                <div class="section-1">
                    <form id="candidate-filter-form" action={{route('site.institute.course.listing')}}>
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Course Type </label>
                                    <select name="course_type" class="form-control course_type">
                                        <option value=>Select Course Type</option>
                                        @foreach(\CommonHelper::course_types_by_institute_id($institute_id) as $r_index => $rank)
                                            <option value="{{$rank->id}}" {{ isset($filter['course_type']) ? $filter['course_type'] == $rank->id ? 'selected' : '' : ''}}>{{$rank->course_type}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Course Name</label>
                                    <select class="form-control institute_course_name" name="course_name">
                                        <option value=''>Select Course Name</option>
                                        @if(isset($course_name) && !empty($course_name))
                                            @foreach($course_name as $r_index => $name)
                                                <option value="{{$r_index}}" {{ isset($filter['course_name']) ? $filter['course_name'] == $name['id'] ? 'selected' : '' : ''}}>{{$name['course_name']}} </option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="section_cta text-right m-t-25 job-btn-container">
                                    <button type="button" class="btn coss-inverse-btn search-reset-btn" id="jobResetButton">Reset</button>
                                    <button type="submit" data-style="zoom-in" class="btn coss-primary-btn search-post-btn ladda-button">
                                        <i class="fa fa-search search-icon" aria-hidden="true"></i>Search</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            @if(isset($data['data']) && !empty($data['data']))
                <div class="section-2" id="filter-results">
                    <div class="row">
                        <div class="col-sm-4">
                            <span class="heading">
                                Course Results
                            </span>
                        </div>
                        <div class="col-sm-8">
                            <span class="pagi pagi-up">
                                <span class="search-count">
                                    Showing {{$data['from']}} - {{$data['to']}} of {{$data['total']}} Courses
                                </span>
                                <nav> 
                                <ul class="pagination pagination-sm">
                                    {!! $pagination->render() !!}
                                </ul> 
                                </nav>
                            </span>
                        </div>
                    </div>
                    <?php
                        $collection = collect($data['data']);

                        $chunks = $collection->chunk(2);
                        $array = $chunks->toArray();
                    ?>
                    
                    @foreach($array as $batch1)
                        <div class="search-result-card1">
                            <div class="row">
                                @foreach($batch1 as $batch)
                                    <div class="col-xs-12 col-sm-12 col-md-6">
                                        <div class="batch-description">
                                            <div class="row">
                                                <div class="col-sm-1 dropdown setting" style="float: right;">
                                                    <a data-toggle="dropdown" class="btn-xs dropdown-toggle">
                                                        <i class="fa fa-cog"></i>
                                                    </a>
                                                    <ul class="dropdown-menu dropdown-light pull-right">
                                                        
                                                        <?php 
                                                            $disable= 'hide';
                                                            $enable= 'hide';
                                                            if(isset($batch) && $batch['status'] == 0){
                                                                $enable = '';
                                                            }else{
                                                                $disable = '';
                                                            }
                                                        ?>
                                                        <li>
                                                            <a class="courseEditButton" data-id="{{$batch['id']}}">
                                                                <span>Edit</span>
                                                            </a>
                                                        </li>

                                                        <li>
                                                            <a class="courseDisableButton disable-{{$batch['id']}} {{$disable}}" data-status='disable' data-id="{{$batch['id']}}">
                                                                <span>Deactivate</span>
                                                            </a>
                                                        </li>
                                                        
                                                        <li>
                                                            <a class="courseDisableButton enable-{{$batch['id']}} {{$enable}}" data-status='enable' data-id="{{$batch['id']}}">
                                                                <span>Activate</span>
                                                            </a>
                                                        </li>

                                                        <li>
                                                            <a class="courseDeleteButton delete-{{$batch['id']}}" data-id="{{$batch['id']}}">
                                                                <span>Delete</span>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="col-sm-10">
                                                    <div class="other-discription">
                                                        <strong>Course Type: </strong>
                                                        <span class="ans">
                                                            @foreach(\CommonHelper::institute_course_types() as $r_index => $course_type)
                                                                {{ !empty($batch['course_type']) ? $batch['course_type'] == $r_index ? $course_type : '' : ''}}
                                                            @endforeach
                                                        </span>
                                                    </div>
                                                    <div class="other-discription">
                                                        <strong>Course Name: </strong>
                                                        <span class="ans">
                                                            @foreach($courses as $r_index => $course)
                                                                {{isset($batch['course_id']) ? $batch['course_id'] == $course['id'] ? $course['course_name'] : '' : ''}}
                                                            @endforeach
                                                        </span>
                                                    </div>
                                                    <div class="other-discription">
                                                        <strong>Cost: </strong>
                                                        <span class="ans">
                                                            <i class="fa fa-inr"></i>
                                                            {{ !empty($batch['cost']) ? $batch['cost'] : '-' }}
                                                        </span>
                                                    </div>
                                                    <div class="other-discription">
                                                        <strong>Duration: </strong>
                                                        <span class="ans">
                                                            {{ !empty($batch['duration']) ? $batch['duration'] : '-' }} days
                                                        </span>
                                                    </div>
                                                    <div class="other-discription">
                                                        <strong>Size: </strong>
                                                        <span class="ans">
                                                            {{ !empty($batch['size']) ? $batch['size'] : '-' }}
                                                        </span>
                                                    </div>
                                                    <div class="other-discription">
                                                        <strong>Certificate Validity(Years): </strong>
                                                        <span class="ans">
                                                            {{ !empty($batch['certificate_validity']) ? $batch['certificate_validity'] : ' Unlimited ' }}
                                                        </span>
                                                    </div>
                                                    <div class="other-discription">
                                                        <strong>Approved By: </strong>
                                                        <span class="ans">
                                                            @if(!empty($batch['approved_by']))
                                                                @foreach(\CommonHelper::approved_by() as $r_index => $by)
                                                                    {{isset($batch['approved_by']) ? $batch['approved_by'] == $r_index ? $by : '' : ''}}
                                                                @endforeach
                                                            @else
                                                                -
                                                            @endif
                                                        </span>
                                                    </div>
                                                    <div class="other-discription">
                                                        <strong>Course For: </strong>
                                                        <span class="ans">
                                                            {{ !empty($batch['key_topics']) ? $batch['key_topics'] : '-' }}
                                                        </span>
                                                    </div>
                                                    <div class="other-discription">
                                                        <strong>Course Eligibility: </strong>
                                                        <span class="ans">
                                                            {{ !empty($batch['eligibility']) ? $batch['eligibility'] : '-' }}
                                                        </span>
                                                    </div>
                                                    <div class="other-discription">
                                                        <strong>Documents Required: </strong>
                                                        <?php
                                                            if(!empty($batch['documents_req'])){
                                                                $req_doc = explode(',',$batch['documents_req']);
                                                            }

                                                        ?>
                                                        <span class="ans">
                                                            @foreach(\CommonHelper::req_documents() as $c_index => $doc)
                                                                @if(in_array($c_index,$req_doc))
                                                                    {{$doc}}
                                                                @endif
                                                            @endforeach
                                                        </span>
                                                    </div>
                                                    <div class="other-discription">
                                                        <strong>Description: </strong>
                                                        <span class="ans">
                                                            {{ !empty($batch['description']) ? $batch['description'] : '-' }}
                                                        </span>
                                                    </div>
                                                    <div class="other-discription">
                                                        <strong>Status:</strong>
                                                        <span class="ans">
                                                            <span class="{{$disable}} disable-{{$batch['id']}} txt-green">
                                                                Activate
                                                            </span>
                                                            <span class="{{$enable}} enable-{{$batch['id']}} txt-red">
                                                                Deactivate
                                                            </span>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    @endforeach
                    
                    <span class="pagi m-t-25">
                        <span class="search-count p-t-10">
                            Showing {{$data['from']}} - {{$data['to']}} of {{$data['total']}} Courses
                        </span>
                        <nav> 
                        <ul class="pagination pagination-sm">
                            {!! $pagination->render() !!}
                        </ul> 
                        </nav>
                    </span>

                </div>
            </div>
            @else
                <div class="section-2" id="filter-results"><div class="row no-results-found">No results found. Try again with different search criteria.</div></div>
            @endif
        </div>
    </main>
</div>

<div id="addCourseModal" class="modal fade" role="dialog">
    <form id="add-institute-course" method="post" action="{{ isset($data['current_route']) ? route('site.institute.update.course.batches',$data['id']) : route('site.institute.store.course') }}">
    {{ csrf_field() }}
        <div class="modal-dialog">

        <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add Course</h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" class="course_id" name="course_id">
                    <div class="job-discription-card">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="input-label" for="course_type">Course Type<span class="symbol required"></label>
                                    <select name="course_type" class="form-control course_type_other">
                                        <option value=''>Select Course Type</option>
                                        @foreach(\CommonHelper::institute_course_types() as $index => $category)
                                            <option value="{{$index}}" {{ isset($data['course_type']) ? $data['course_type'] == $index ? 'selected' : '' : ''}}>{{$category}}</option>
                                        @endforeach
                                    </select>
                                 </div>
                            </div>
                        
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="input-label">Course Name<span class="symbol required"></label>
                                    <select name="course_name" class="form-control institute_course_name">
                                        <option value="">Select Course Name</option>
                                        @if(isset($data['courses']) && !empty($data['courses']))

                                            @foreach($data['courses'] as $index => $category)
                                                <option value="{{$category['course_type']}}" {{ isset($data['course_type']) ? $data['course_type'] == $category['course_type'] ? 'selected' : '' : ''}}>{{$category['course_name']}}</option>
                                            @endforeach
                                            <option value="other">Other</option>
                                        @endif
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row hide" id="other_course_name">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="input-label">Other Course Name<span class="symbol required"></label>
                                    <input type="text" class="form-control" class="other_course_name" name="other_course_name" placeholder="Type other course name">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="input-label" for="cost">Cost (<span class="fa fa-inr"></span>)<span class="symbol required"></label>
                                    <input type="text" class="form-control cost" name="cost" placeholder="Type your cost in rupees"></input>
                                 </div>
                            </div>
                        
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="input-label">Duration (Days)<span class="symbol required"></label>
                                    <input type="text" class="form-control duration" name="duration" placeholder="Type your duration in days"></input>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="input-label" for="size">Batch Size<span class="symbol required"></label>
                                    <input type="text" class="form-control size" name="size" placeholder="Type batch size"></input>
                                 </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="input-label" for="approved_by">Approved By<span class="symbol required"></label>
                                    <select name="approved_by" class="form-control approved_by">
                                        <option value=''>Select Approved By</option>
                                        @foreach(\CommonHelper::approved_by() as $index => $by)
                                            <option value="{{$index}}" {{ isset($data['approved_by']) ? $data['approved_by'] == $index ? 'selected' : '' : '' }}>{{$by}}</option>
                                        @endforeach
                                    </select>
                                 </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label class="input-label" for="size">Certificate Validity(Years)</label>
                                    <input type="text" class="form-control certificate_validity" name="certificate_validity" placeholder="Leave blank for unlimited"></input>
                                 </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label class="input-label">Course For</label>
                                    <textarea class="form-control key_topics" name="key_topics" rows="5" placeholder="Type course for"></textarea>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label class="input-label">Course Eligibility<span class="symbol required"></span></label>
                                    <textarea class="form-control eligibility" name="eligibility" rows="5" placeholder="Type course eligibility"></textarea>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label class="input-label">Documents Required<span class="symbol required"></span></label>
                                     <select multiple="multiple" name="documents_req[]" class="form-control course-select2-select" style="height: auto;padding: 0 10px;" placeholder="Select Documents">
                                        @foreach(\CommonHelper::req_documents() as $c_index => $country)
                                            @if(!empty($nationality_id) && in_array($c_index,$nationality_id))
                                                <option value="{{ $c_index }}" selected="selected"> {{ $country }}</option>
                                            @else
                                                <option value="{{ $c_index }}"> {{ $country }}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <!-- <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="input-label">Documents Required<span class="symbol required"></span></label>
                                    <select name="documents_req" id="" class="form-control">
                                        <option value="">documents required</option>
                                        <option value=""></option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6">
                            <button type="button" class="btn mt-24">Select</button>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <textarea class="form-control " name="" rows="5" placeholder=""></textarea>
                                </div>
                            </div>
                        </div> -->
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label class="input-label">Course Description</label>
                                    <textarea class="form-control description" name="description" rows="5" placeholder="Type course description"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default job-post-btn ladda-button" data-style="zoom-in" id="addinstitutecoursebtn">Save</button>
                </div>
            </div>

        </div>
    </form>
</div>
@stop
@section('js_script')
    <script type="text/javascript" src="/js/site/institute-registration.js"></script>
    <script type="text/javascript" src="/js/site/dashboard.js"></script>
    <script type="text/javascript">

        $('.select2-select').select2({
            closeOnSelect: false
        });

        $('.course-select2-select').select2({
            closeOnSelect: false
        });

        $(document).on('click','.pagination li a',function (e) {
            var url = $(this).attr("href");
            window.location.href = url;
        });
    </script>
@stop