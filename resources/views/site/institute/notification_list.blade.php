@extends('site.index')
@section('content')

	<div class="section candidate-search job-application-listing content-section-wrapper sm-filter-space dashboard-body">
		@include('site.partials.sidebar')
		<main class="main" style="padding: 20px 20px 80px 20px !important;">
			<div class="col-md-12" id="main-data">
	            <div class="candidate-search-container">
	                <div class="section-1">
						@if(isset($notifications['data']) AND !empty($notifications['data']))
							<?php $pagination_data = $data->toArray(); ?>
							<div class="listing-container">
								{{csrf_field()}}
								<div class="row">
									<div class="col-xs-12 col-sm-4">
					                    <div class="company-page-profile-heading">
					                        Notification Listing
					                    </div>
					                </div>
					                <div class="col-xs-12 col-sm-8">
										<span class="pagi">
					                        <span class="search-count" style="padding-top: 10px;">
					                            Showing {{$pagination_data['from']}} - {{$pagination_data['to']}} of {{$pagination_data['total']}} Notifications
					                        </span>
					                        <nav> 
					                        <ul class="pagination pagination-sm">
					                            {!! $data->render() !!}
					                        </ul> 
					                        </nav>
					                    </span>
				                    </div>
								</div>
								<div class="notification-content-main-view">
								@foreach($notifications['data'] as $notification)
									<div class="row">
										<div class="col-xs-12 col-sm-12">
											<div class="notification-content">
												{!!$notification['message']!!}
											</div>
										</div>
									</div>
								@endforeach
								</div>
								<div class="row">
					                <div class="col-xs-12 col-sm-12">
										<span class="pagi m-t-10">
					                        <span class="search-count" style="padding-top: 10px;">
					                            Showing {{$pagination_data['from']}} - {{$pagination_data['to']}} of {{$pagination_data['total']}} Notifications
					                        </span>
					                        <nav> 
					                        <ul class="pagination">
					                            {!! $data->render() !!}
					                        </ul> 
					                        </nav>
					                    </span>
				                    </div>
								</div>
							</div>
						@else
		                    <div class="company-page-profile-heading">
		                        Notification Listing
		                    </div>
							<div class="no-results-found">No Notifications Found.</div>
						@endif
					</div>
				</div>
			</div>
		</main>
	</div>

@stop