@extends('site.index')
@section('content')

	<div class="candidate-search job-application-listing content-section-wrappe dashboard-body" style="min-height: -webkit-fill-available;">
		@include('site.partials.sidebar')
		 <main class="main" style="padding: 20px 20px 80px 20px !important;">
            <div class="candidate-search-container">
                <div class="section-2">
					@if(isset($batches['data']) AND !empty($batches['data']))
					<div class="loader_overlay hide">
                        <div class="loader_ele loader" style="position:fixed !important;left: 55%"></div>
                    </div>
					<div class="listing-container">
						{{csrf_field()}}
						<div class="row">
							<div class="col-xs-12 col-sm-8">
			                    <div class="company-page-profile-heading">
			                        On-Demand Listing
			                    </div>
			                </div>
			                <div class="col-xs-12 col-sm-4">
								<span class="pagi pagi-up">
			                        <span class="search-count" style="padding-bottom: 5px;">
			                            Showing {{$batches['from']}} - {{$batches['to']}} of {{$batches['total']}} Batches
			                        </span>
			                        <nav> 
			                        <ul class="pagination pagination-sm">
			                            {!! $pagination->render() !!}
			                        </ul> 
			                        </nav>
			                    </span>
		                    </div>
						</div>
						@foreach($batches['data'] as $key => $batch_data)
							<div class="job-card">
								<div class="row m-0 flex-height">
									<div class="col-sm-8 p-0">
										<div class="company-discription">
											<div class="row">
												<div class="col-sm-8">
													<div class="row">
														<div class="col-sm-6">
															<div class="other-discription">
																<b>Batch Details</b>
															</div>
														</div>
														<div class="col-sm-6 setting-{{$batch_data['id']}} d-flex" style="justify-content: space-between;">
															<div class="other-discription">
																<b>&nbsp</b>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="row">
												<?php
													if(isset($batch_data['course_details']['course_type'])){
														$course_type = $batch_data['course_details']['course_type'];

														$course_name = $batch_data['course_details']['courses'][0]['course_name'];
													}
												?>
												<div class="col-sm-6">
													<div class="other-discription">
														<strong>Course Type:</strong>
														@foreach(\CommonHelper::institute_course_types() as $r_index => $rank)
				                                            {{ isset($course_type) ? $course_type == $r_index ? $rank : '' : ''}}
				                                        @endforeach
													</div>
												</div>
												<div class="col-sm-6">
													<div class="other-discription">
														<strong>On Demand Batch Type:</strong>
														{{ isset($batch_data['on_demand_batch_type']) ? $batch_data['on_demand_batch_type'] == '1' ? 'Weekly Course' : '' : ''}}
                                                     	{{ isset($batch_data['on_demand_batch_type']) ? $batch_data['on_demand_batch_type'] == '2' ? 'Bi-Monthly Course' : '' : ''}}
                                                   		{{ isset($batch_data['on_demand_batch_type']) ? $batch_data['on_demand_batch_type'] == '3' ? 'Monthly Course' : '' : ''}}
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-sm-6">
													<div class="other-discription">
														<strong>Course Name:</strong>
														{{ isset($course_name) ? $course_name : '-'}}
													</div>
												</div>
												<div class="col-sm-6">
													<div class="other-discription">
														<strong>Cost:</strong>
														<i class="fa fa-inr fa-sm"></i> 
														{{ isset($batch_data['cost']) ? $batch_data['cost'] : ''}}
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-sm-6">
													<div class="other-discription">
														<strong>Start Date:</strong>
														<span class="spanGreen">
														<?php

														 if(!empty( $batch_data['course_start_date'] )) {

														 	echo date('d-m-Y',strtotime($batch_data['course_start_date']));

														 } else {
														   echo date('M-y',strtotime($batch_data['start_date']));
														 }
														 ?>
														</span>
													</div>
												</div>
												<div class="col-sm-6">
													<div class="other-discription">
														<strong>Duration:</strong>
														{{ isset($batch_data['duration']) ? $batch_data['duration'] : '-'}} Days
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-sm-6">
													<div class="other-discription">
														<strong>Payment Type: </strong>
														{{ isset($batch_data['payment_type']) ? ucwords($batch_data['payment_type'])." Payment" : '-'}}
													</div>
												</div>
												<div class="col-sm-6">
													<div class="other-discription">
														<strong>Size:</strong>
														{{ isset($batch_data['size']) ? $batch_data['size'] : '-'}}
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-sm-6">
													<div class="other-discription">
														<strong>Batch Type:</strong>
														{{ isset($batch_data['batch_type']) ? $batch_data['batch_type'] == 'confirmed' ? 'Confirmed' : '' : ''}}
														{{ isset($batch_data['batch_type']) ? $batch_data['batch_type'] == 'on_demand' ? 'On Demand' : '' : ''}}
													</div>
												</div>
												<div class="col-sm-6">
													<div class="other-discription">
														<strong>Reserved Seats:</strong>
														<span class="status status-{{$batch_data['id']}}">
															{{ isset($batch_data['reserved_seats']) ? $batch_data['reserved_seats'] : '-'}}
								                        </span>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="col-sm-4 p-0">
										<div class="company-discription no-border">
											<div class="row">
												<div class="col-sm-12">
													<div class="row">
														<div class="col-sm-12">
															<div class="other-discription">
																<b>Order Details</b>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-sm-12 p-0">
													<div class="other-discription">
														
														<?php
															$confirmed_batches = [];
															if(isset($batch_data['child_batches']) && !empty($batch_data['child_batches'])){
																$collection = Collect($batch_data['child_batches']);
																$confirmed_batches = $collection->pluck('preffered_type')->toArray();
															}

															if(!empty($batch_data['child_batches'])){
																//dd($batch_data['group'],$confirmed_batches,$batch_data['child_batches']);
															}
														?>
														@foreach($batch_data['group'] as $week => $order)
															@if(!in_array(ucfirst($week),$confirmed_batches))
																{{ $week == 'monthly' ? '' : 'Week'}} {{ucwords($week)}}: 
																<span class="status status-{{$batch_data['id']}}">
																	{{count($order)}} Bookings 
																	<?php 
																		$collection = collect($order);

													                    $grouped = $collection->groupBy('preffered_date');
													                    $all_date = $grouped->toArray();
													                    $title = "";
													                    foreach ($all_date as $key => $value) {
													                    	$all_date_count = count($all_date[$key]);
													                    	$key = isset($key) ? date('d-m-Y',strtotime($key)) : '';
													                    	$title = $title." $key : $all_date_count"." Booking <br>";
													                    }

																	?>
																	<a href="#" data-toggle="tooltip" data-placement="top" title="{{$title}}" data-html="true">
																		<i class="fa fa-question-circle" aria-hidden="true" style="color:black"></i>
																	</a>

																	<button class="batchConfirmButton pull-right coss-primary-book-btn" data-week="{{ucwords($week)}}" data-batch="{{$batch_data['id']}}" data-batch-start-date="{{$batch_data['start_date']}}">Confirm Batch</button>
																	<br>
										                        </span>
										                        <br>
										                    @endif
									                    @endforeach
										                
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						@endforeach
						<div class="row">
			                <div class="col-xs-12 col-sm-12">
								<span class="pagi pagi-up m-t-10">
			                        <span class="search-count" style="padding-bottom: 5px;">
			                            Showing {{$batches['from']}} - {{$batches['to']}} of {{$batches['total']}} Batches
			                        </span>
			                        <nav> 
			                        <ul class="pagination pagination-sm">
			                            {!! $pagination->render() !!}
			                        </ul> 
			                        </nav>
			                    </span>
		                    </div>
						</div>
					</div>
					@else
	                    <div class="company-page-profile-heading">
	                        Course Applicant Listing
	                    </div>
						<div class="no-results-found">No Results Found.</div>
					@endif
				</div>
			</div>
		</main>
	</div>
	<div id="batch-confirm-modal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
          		<form id="batch-confirm-modal-form" method="post" action="{{route('site.institute.on.demand.batch.confirm')}}">
	              	<div class="modal-header">
		                <button type="button" class="close" data-dismiss="modal">&times;</button>
		                <h4 class="modal-title modal-heading">Confirm On Demand Batch</h4>
		            </div>
	              	<div class="modal-body">
	                	<p class="modal-body-content">Select Batch Start Date:
	                		<div class="row">
	                			<input type="hidden" name="batch_id" class="batch_id">
	                			<input type="hidden" name="selected_week" class="selected_week">
	                			<div class="col-sm-12">
	                				<div id="preffered-datepicker" class="" style="width:100%;height:200px;"></div>
	                			</div>
	                			<span class="p-l-5 d-flex" style="margin: 0px 20px;padding-top: 20px;">
	                                <input type="hidden" id="dbdatepicker" class="dbdatepicker ladda-button" data-style="zoom-in" name="dbdatepicker" data-rule-dbdatepickervalue="true">
	                            </span>
	                		</div>
	                	</p>
	              	</div>
	              	<div class="modal-footer">
	                	<button type="button" class="btn btn-red" data-dismiss="modal">Close</button>
	                	<button type="button" class="btn btn-info ladda-button" data-style="zoom-in" id="batch-confirm-modal-btn">Confirm</button>
	              	</div>
		        </form>
            </div>
        </div>
    </div>

@stop
@section('js_script')
    <script type="text/javascript" src="/js/site/institutes_details.js"></script>
@stop