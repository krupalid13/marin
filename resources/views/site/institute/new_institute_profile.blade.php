@extends('site.index')

@section('content')
    <div class="user-profile content-section-wrapper sm-filter-space company_profile_view">
        @if(!isset($user) && empty($user))
            <div class="container-fluid">
        @else
            <div class="container">
        @endif
            <div class="row">
                @if(!isset($user) && empty($user))
                <div class="col-md-2" id="side-navbar">
                    @include('site.partials.side_navbar_institute')
                </div>
                @endif
                @if(!isset($user) && empty($user))
                    <div class="col-md-10" id="main-data">
                @else
                    <div class="col-md-12" id="main-data">
                @endif
                @if(!isset($user) && empty($user))
                    <div class="row">
                        @if(isset($data[0]['mobile']) && strlen($data[0]['mobile']) == 10)
                        <div class="col-sm-12 col-md-6">
                            <div class="alert alert-block alert-danger fade in alert-box-verification-mob hide">
                                Your mobile is not verified. Please verify your mobile. <a class="alert-link" href="#" id="resend-otp-button">Verify</a>

                            </div>
                        </div>
                        @endif
                        <div class="col-sm-12 col-md-6">
                            <div class="alert alert-block alert-danger fade in alert-box-verification-email hide">
                                Your email is not verified. Please verify your email. <a class="alert-link" href="#" id="email_verify">Resend Email</a>
                                <i class="fa fa-spin fa-refresh resend_email_loader hide"></i>
                            </div>
                        </div>
                    </div>
                @endif
                    <div class="row profile_edit">
                        <div class="col-xs-6">
                            <div class="company-page-profile-heading">
                                My Profile
                            </div>
                        </div>
                        @if(!isset($user) && empty($user))
                            <div class="col-xs-6">
                                <div class="edit-btn-container pull-right visible-xs">
                                    <a href="{{ route('site.edit.institute.details') }}" class="profile-edit-bt">
                                        <i class="fa fa-pencil-square-o" aria-hidden="true" title="Edit profile"></i>
                                    </a>
                                </div>
                            </div>
                        @endif
                    </div>

                    @include('site.partials.institute_profile')

                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
@stop
@section('js_script')
    <script type="text/javascript" src="/js/site/institute-registration.js"></script>
@stop