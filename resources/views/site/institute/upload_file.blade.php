@extends('site.index')
@section('content')
<main class="main">
    <div class="upload_files">
        <div class="section">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <ul class="nav nav-tabs tabs-list">
                            <li class="tabs-list-menu nav-item active">
                                <a class="nav-link" href="">Basic Details</a>
                            </li>
                            <li class="tabs-list-menu nav-item">
                                <a class="nav-link" href="">General Documents</a>
                            </li>
                            <li class="tabs-list-menu nav-item">
                                <a class="nav-link" href="">Sea Service Details</a>
                            </li>
                            <li class="tabs-list-menu nav-item">
                                <a class="nav-link" href="">Course Details</a>
                            </li>
                            <li class="tabs-list-menu nav-item">
                                <a class="nav-link" href="">Upload Documents</a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane"></div>
                            <div class="tab-pane"></div>
                            <div class="tab-pane"></div>
                            <div class="tab-pane"></div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="slider-nav">
                            <div class="upload-type-selector">
                                <div class="upload-type-manager"><!-- three dot menu -->
                                    <div class="dropdown">
                                        <button class="btn-dots dropdown-toggle" type="button" id="menu1" data-toggle="dropdown">
                                        <span class="fa fas fa-ellipsis-v"></span></button>
                                        <ul class="dropdown-menu" role="menu" aria-labelledby="menu1">
                                            <li role="presentation"><a role="menuitem" href="#">HTML</a></li>
                                            <li role="presentation"><a role="menuitem" href="#">CSS</a></li>

                                        </ul>
                                    </div>
                                </div>
                                <div class="upload-type-selector-body">Passport / Visa</div>
                            </div>
                            <div class="upload-type-selector">
                                <div class="upload-type-manager"><!-- three dot menu -->
                                    <div class="dropdown">
                                        <button class="btn-dots dropdown-toggle" type="button" id="menu1" data-toggle="dropdown">
                                        <span class="fa fas fa-ellipsis-v"></span></button>
                                        <ul class="dropdown-menu" role="menu" aria-labelledby="menu1">
                                            <li role="presentation"><a role="menuitem" href="#">HTML</a></li>
                                            <li role="presentation"><a role="menuitem" href="#">CSS</a></li>

                                        </ul>
                                    </div>
                                </div>
                                <div class="upload-type-selector-body">CDC</div>
                            </div>
                            <div class="upload-type-selector">
                                <div class="upload-type-manager"><!-- three dot menu -->
                                    <div class="dropdown">
                                        <button class="btn-dots dropdown-toggle" type="button" id="menu1" data-toggle="dropdown">
                                        <span class="fa fas fa-ellipsis-v"></span></button>
                                        <ul class="dropdown-menu" role="menu" aria-labelledby="menu1">
                                            <li role="presentation"><a role="menuitem" href="#">HTML</a></li>
                                            <li role="presentation"><a role="menuitem" href="#">CSS</a></li>

                                        </ul>
                                    </div>
                                </div>
                                <div class="upload-type-selector-body">COC /COE</div>
                            </div>
                            <div class="upload-type-selector">
                                <div class="upload-type-manager"><!-- three dot menu -->
                                    <div class="dropdown">
                                        <button class="btn-dots dropdown-toggle" type="button" id="menu1" data-toggle="dropdown">
                                        <span class="fa fas fa-ellipsis-v"></span></button>
                                        <ul class="dropdown-menu" role="menu" aria-labelledby="menu1">
                                            <li role="presentation"><a role="menuitem" href="#">HTML</a></li>
                                            <li role="presentation"><a role="menuitem" href="#">CSS</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="upload-type-selector-body">GMDSS / WK</div>
                            </div>
                            <div class="upload-type-selector">
                                <div class="upload-type-manager"><!-- three dot menu -->
                                    <div class="dropdown">
                                        <button class="btn-dots dropdown-toggle" type="button" id="menu1" data-toggle="dropdown">
                                        <span class="fa fas fa-ellipsis-v"></span></button>
                                        <ul class="dropdown-menu" role="menu" aria-labelledby="menu1">
                                            <li role="presentation"><a role="menuitem" href="#">HTML</a></li>
                                            <li role="presentation"><a role="menuitem" href="#">CSS</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="upload-type-selector-body">PRESEA / POSTSEA COURSE CIRTIFICAES</div>
                            </div>
                            <div class="upload-type-selector">
                                <div class="upload-type-manager"><!-- three dot menu -->
                                    <div class="dropdown">
                                        <button class="btn-dots dropdown-toggle" type="button" id="menu1" data-toggle="dropdown">
                                        <span class="fa fas fa-ellipsis-v"></span></button>
                                        <ul class="dropdown-menu" role="menu" aria-labelledby="menu1">
                                            <li role="presentation"><a role="menuitem" href="#">HTML</a></li>
                                            <li role="presentation"><a role="menuitem" href="#">CSS</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="upload-type-selector-body">Sea service letters</div>
                            </div>
                            <div class="upload-type-selector">
                                <div class="upload-type-manager"><!-- three dot menu -->
                                    <div class="dropdown">
                                        <button class="btn-dots dropdown-toggle" type="button" id="menu1" data-toggle="dropdown">
                                        <span class="fa fas fa-ellipsis-v"></span></button>
                                        <ul class="dropdown-menu" role="menu" aria-labelledby="menu1">
                                            <li role="presentation"><a role="menuitem" href="#">HTML</a></li>
                                            <li role="presentation"><a role="menuitem" href="#">CSS</a></li>

                                        </ul>
                                    </div>
                                </div>
                                <div class="upload-type-selector-body">Medicals YF vaccibations</div>
                            </div>
                        </div>
                        <div class="slider-for">
                            <div class="upload-details-wrapper">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="heading-wrapper">
                                            <div class="title">Passport / Visa</div>
                                            <div class="edit-manager">
                                                <div class="dropdown">
                                                    <button class="btn-dots dropdown-toggle" type="button" id="menu1" data-toggle="dropdown">
                                                    <span class="fa fas fa-ellipsis-v"></span></button>
                                                    <ul class="dropdown-menu" role="menu" aria-labelledby="menu1">
                                                        <li role="presentation"><a role="menuitem" href="#">HTML</a></li>
                                                        <li role="presentation"><a role="menuitem" href="#">CSS</a></li>

                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="previews">
                                            <div class="row">
                                                <div class="col-md-9 col-xs-12">
                                                    <div class="previews-heading">
                                                        <span class="icon-check tick-mark icon"></span>
                                                        <span class="title">Course 1 Elementary First Aid</span>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 col-xs-12 text-right">
                                                    <button class="btn cs-primary-btn">Upload</button>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="preview">
                                                        <div class="preview-card" stlye="background-image:url(' ')">
                                                        
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="upload-details-wrapper">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="heading-wrapper">
                                            <div class="title"></div>
                                            <div class="edit-manager">
                                                <div class="dropdown">
                                                    <button class="btn-dots dropdown-toggle" type="button" id="menu1" data-toggle="dropdown">
                                                    <span class="fa fas fa-ellipsis-v"></span></button>
                                                    <ul class="dropdown-menu" role="menu" aria-labelledby="menu1">
                                                        <li role="presentation"><a role="menuitem" href="#">HTML</a></li>
                                                        <li role="presentation"><a role="menuitem" href="#">CSS</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="previews">
                                            <div class="row">
                                                <div class="col-md-9 col-xs-12">
                                                    <div class="previews-heading">
                                                        <span class="tick-mark icon"></span>
                                                        <span class="title">Course 1 Elementary First Aid</span>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 col-xs-12 text-right">
                                                    <button class="btn cs-primary-btn">Upload</button>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="preview">
                                                        <div class="preview-card" stlye="background-image:url(' ')">
                                                        
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="upload-details-wrapper">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="heading-wrapper">
                                            <div class="title"></div>
                                            <div class="edit-manager">
                                                <div class="dropdown">
                                                    <button class="btn-dots dropdown-toggle" type="button" id="menu1" data-toggle="dropdown">
                                                    <span class="fa fas fa-ellipsis-v"></span></button>
                                                    <ul class="dropdown-menu" role="menu" aria-labelledby="menu1">
                                                        <li role="presentation"><a role="menuitem" href="#">HTML</a></li>
                                                        <li role="presentation"><a role="menuitem" href="#">CSS</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-9 col-xs-12"></div>
                                    <div class="col-md-3 col-xs-12"></div>
                                </div>
                            </div>
                            <div class="upload-details-wrapper">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="heading-wrapper">
                                            <div class="title"></div>
                                            <div class="edit-manager">
                                                <div class="dropdown">
                                                    <button class="btn-dots dropdown-toggle" type="button" id="menu1" data-toggle="dropdown">
                                                    <span class="fa fas fa-ellipsis-v"></span></button>
                                                    <ul class="dropdown-menu" role="menu" aria-labelledby="menu1">
                                                        <li role="presentation"><a role="menuitem" href="#">HTML</a></li>
                                                        <li role="presentation"><a role="menuitem" href="#">CSS</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="previews">
                                            <div class="row">
                                                <div class="col-md-9 col-xs-12">
                                                    <div class="previews-heading">
                                                        <span class="icon-check tick-mark icon"></span>
                                                        <span class="title">Course 1 Elementary First Aid</span>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 col-xs-12 text-right">
                                                    <button class="btn cs-primary-btn">Upload</button>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="preview">
                                                        <div class="preview-card" stlye="background-image:url(' ')">
                                                        
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="upload-details-wrapper">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="heading-wrapper">
                                            <div class="title"></div>
                                            <div class="edit-manager">
                                                <div class="dropdown">
                                                    <button class="btn-dots dropdown-toggle" type="button" id="menu1" data-toggle="dropdown">
                                                    <span class="fa fas fa-ellipsis-v"></span></button>
                                                    <ul class="dropdown-menu" role="menu" aria-labelledby="menu1">
                                                        <li role="presentation"><a role="menuitem" href="#">HTML</a></li>
                                                        <li role="presentation"><a role="menuitem" href="#">CSS</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="previews">
                                            <div class="row">
                                                <div class="col-md-9 col-xs-12">
                                                    <div class="previews-heading">
                                                        <span class="tick-mark icon"></span>
                                                        <span class="title">Course 1 Elementary First Aid</span>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 col-xs-12 text-right">
                                                    <button class="btn cs-primary-btn">Upload</button>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="preview">
                                                        <div class="preview-card" stlye="background-image:url(' ')">
                                                        
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="upload-details-wrapper">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="heading-wrapper">
                                            <div class="title"></div>
                                            <div class="edit-manager">
                                                <div class="dropdown">
                                                    <button class="btn-dots dropdown-toggle" type="button" id="menu1" data-toggle="dropdown">
                                                    <span class="fa fas fa-ellipsis-v"></span></button>
                                                    <ul class="dropdown-menu" role="menu" aria-labelledby="menu1">
                                                        <li role="presentation"><a role="menuitem" href="#">HTML</a></li>
                                                        <li role="presentation"><a role="menuitem" href="#">CSS</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="previews">
                                            <div class="row">
                                                <div class="col-md-9 col-xs-12">
                                                    <div class="previews-heading">
                                                        <span class="icon-check tick-mark icon"></span>
                                                        <span class="title">Course 1 Elementary First Aid</span>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 col-xs-12 text-right">
                                                    <button class="btn cs-primary-btn">Upload</button>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="preview">
                                                        <div class="preview-card" stlye="background-image:url(' ')">
                                                        
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="upload-details-wrapper">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="heading-wrapper">
                                            <div class="title"></div>
                                            <div class="edit-manager">
                                                <div class="dropdown">
                                                    <button class="btn-dots dropdown-toggle" type="button" id="menu1" data-toggle="dropdown">
                                                    <span class="fa fas fa-ellipsis-v"></span></button>
                                                    <ul class="dropdown-menu" role="menu" aria-labelledby="menu1">
                                                        <li role="presentation"><a role="menuitem" href="#">HTML</a></li>
                                                        <li role="presentation"><a role="menuitem" href="#">CSS</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="previews">
                                            <div class="row">
                                                <div class="col-md-9 col-xs-12">
                                                    <div class="previews-heading">
                                                        <span class="icon-check tick-mark icon"></span>
                                                        <span class="title">Course 1 Elementary First Aid</span>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 col-xs-12 text-right">
                                                    <button class="btn cs-primary-btn">Upload</button>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="preview">
                                                        <div class="preview-card" stlye="background-image:url(' ')">
                                                        
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <button class="btn cs-secondary-btn">Skip</button>
                        <button class="btn cs-primary-btn">Save & Finish</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="cs-custom-footer">
    <div class="container">
        <div class="row mr-bottom-30">
        <div class="col-md-2 col-xs-12">
                <div class="footer-title">Quick links</div>
                <ul class="links-list">
                    <li class="links-list-menu"><a href="" class=""><span class="show_on_hover">-</span> About Us</a></li>
                    <li class="links-list-menu"><a href="" class=""><span class="show_on_hover">-</span> Conatct Us</a></li>
                    <li class="links-list-menu"><a href="" class=""><span class="show_on_hover">-</span> Disclaimer</a></li>
                    <li class="links-list-menu"><a href="" class=""><span class="show_on_hover">-</span> Policy</a></li>
                </ul>
            </div>
            <div class="col-md-2 col-xs-12">
                <div class="footer-title">Contact Us</div>
                <ul class="links-list">
                    <li class="links-list-menu flex mt-5"> 
                        <span class="flex--one icon-image" style="background-image:url('/images/home/map.png')"></span>
                        <span  class="flex--two">508/8, Coral Crest, Nerul East, Navi Mumbai - 400708</span>
                    </li>
                </ul>
            </div>
            <div class="col-md-3 col-xs-12">
               <div class="footer-title"></div>
                <ul class="links-list">
                    <li class="links-list-menu flex mt-5">
                        <span class="flex--one icon-image" style="background-image:url('/images/home/Email.png')"></span>
                        <span class="flex--two"><a href="mailto:techsupport@course4sea.com">techsupport@course4sea.com</a></span>  
                    </li>
                    <li class="links-list-menu flex mt-5">
                        <span class="flex--one icon-image" style="background-image:url('/images/home/Email.png')"></span>
                        <span class="flex--two"><a href="mailto:usersupport@course4sea.com">usersupport@course4sea.com</a></span>  
                    </li>
                    <li class="links-list-menu flex mt-5">
                        <span class="flex--one icon-image" style="background-image:url('/images/home/Email.png')"></span>
                        <span class="flex--two"><a href="mailto:course4sea@gmail.com">course4sea@gmail.com</a> </span>  </li>
                </ul>
            </div>
            <div class="col-md-2 col-xs-12">
                <div class="footer-title"></div>
                 <ul class="links-list">
                    <li class="links-list-menu mt-5"><a href="" class="flex"><span class="icon-image flex--one" style="background-image:url('/images/home/Mobile.png')"></span>  <span  class="flex--two">7738169926</span>   </a></li>
                    <li class="links-list-menu mt-5"><a href="" class="flex"><span class="icon-image flex--one" style="background-image:url('/images/home/Mobile.png')"></span>  <span  class="flex--two">7738169926</span>   </a></li>
                </ul>
            </div>
            <div class="col-md-3 col-xs-12">
                <div class="footer-title" style="padding:20px 10px;">Connect with us on </div>
                <ul class="social-icons">
                    <li class="social-icons-menu"><a href=""><img src="/images/home/fb.png" alt="facebooklogo"></a></li>
                    <li class="social-icons-menu"><a href=""><img src="/images/home/ln.png" alt="linked in"></a></li>
                    <li class="social-icons-menu"><a href=""><img src="/images/home/g.png" alt="google"></a></li>
                    <li class="social-icons-menu"><a href=""><img src="/images/home/utube.png" alt="youtube"></a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
</main>

@stop