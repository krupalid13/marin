@extends('site.index')

@section('content')
<div class="user-job-post content-section-wrapper sm-filter-space dashboard-body">
    @include('site.partials.sidebar')
    
    <main class="main" style="padding: 20px 20px 80px 20px !important;">
        <div class="row">
            <div class="col-xs-12">
                <div class="">
                    Image Gallery
                </div>
            </div>
        </div>
        
        <form id="add-image-gallery-form" method="post" action="{{ route('site.institute.store.image.gallery') }}">
            {{ csrf_field() }}
            <div class="section job-post" style="padding: 0px !important;height:100% !important">
                <div class="job-discription-card" style="padding: 20px;">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="fileupload fileupload-new" data-provides="fileupload">
                                <div class="fileupload-new thumbnail" style="height: auto !important;">
                                    @if(isset($img_path) AND !empty($img_path))
                                        <img src="{{ asset($img_path) }}" alt=""/>
                                    @else
                                        <img src="{{ asset('images/no-image-advertisements.png') }}" alt=""/>
                                    @endif
                                </div>
                                <div class="fileupload-preview fileupload-exists thumbnail advertise_img" name="advertise_img" id='advertise_img'>
                                </div>
                                <span class="help-block" style="color: #737373;"><i class="fa fa-info-circle"></i> 
                                    Note: Please upload .jpeg .png file only.<br>You can upload maximum 15 images only.</span>
                                <div>
                                    <span class="btn btn-light-grey btn-file">
                                        <span class="fileupload-new">
                                            <i class="fa fa-picture-o"></i> Select image</span>
                                        <span class="fileupload-exists">
                                            <i class="fa fa-picture-o"></i> Change</span>
                                        <input type="file" class="advertise-file-uplaod" name="advertise_upload">
                                    </span>
                                    <a href="#" class="btn fileupload-exists btn-light-grey remove_advertise_btn" data-dismiss="fileupload">
                                        <i class="fa fa-times"></i> Remove
                                    </a>
                                </div>
                                <label for="advertise_upload" class="error" id="advertise_upload_error"></label>
                            </div>
                        </div>
                    </div>

                    <div class="reg-form-btn-container text-right">
                        <button type="button" data-style="zoom-in" class="reg-form-btn blue ladda-button" id="saveImageGalleryButton" value="Save" data-tab-index="1">Save</button>
                    </div>
                </div>
            </div>
        </form>

        <div class="job-post m-t-25">
            <div class="job-discription-card" style="padding: 20px;">
                 <div class="row">
                    <div class="col-xs-12">
                        <div class="
                            Uploaded Images
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="loader_overlay hide">
                        <div class="loader_ele loader" style="position:fixed !important;left: 55%"></div>
                    </div>
                    <div class="image-section">
                        @if(isset($data[0]['institute_registration_detail']['institute_gallery']) && !empty($data[0]['institute_registration_detail']['institute_gallery']))
                            @foreach($data[0]['institute_registration_detail']['institute_gallery'] as $images)

                                <?php
                                    $image_path = '';
                                    if(isset($images)){
                                        $image_path = "/".env('INSTITUTE_IMAGE_GALLERY_PATH')."".$images['institute_id']."/".$images['image'];
                                    }
                                ?>

                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3 gallery-image">
                                    <div class="img-grid" style="background-image: url({{ $image_path }});">
                                        <!-- <img id="preview" src="{{ $image_path }}"> -->
                                        <div class="gallery-close-icon" data-id="{{$images['id']}}">
                                            <svg aria-hidden="true" style="width: 100%;height: 100%;" data-prefix="fal" data-icon="times" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512" class="svg-inline--fa fa-times fa-w-10 fa-2x"><path fill="currentColor" d="M193.94 256L296.5 153.44l21.15-21.15c3.12-3.12 3.12-8.19 0-11.31l-22.63-22.63c-3.12-3.12-8.19-3.12-11.31 0L160 222.06 36.29 98.34c-3.12-3.12-8.19-3.12-11.31 0L2.34 120.97c-3.12 3.12-3.12 8.19 0 11.31L126.06 256 2.34 379.71c-3.12 3.12-3.12 8.19 0 11.31l22.63 22.63c3.12 3.12 8.19 3.12 11.31 0L160 289.94 262.56 392.5l21.15 21.15c3.12 3.12 8.19 3.12 11.31 0l22.63-22.63c3.12-3.12 3.12-8.19 0-11.31L193.94 256z" class=""></path></svg>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @else
                           <div class="row no-data-found">
                                <div class="col-xs-12 text-center">
                                    <div class="discription">
                                        <span class="content-head">No Images Found</span>
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </main>
</div>
@stop
@section('js_script')
    <script type="text/javascript" src="/js/site/institute-registration.js"></script>
@stop