@extends('site.index')
@section('content')
    <link rel="stylesheet" href="/css/jquery.fancybox.css" type="text/css" media="screen" />
    <style type="text/css">
        .dashboard-body .main .banner-image {
            background-position: inherit;
        }
    </style>
    <div class="dashboard-body">
        @if(!isset($user) && empty($user))
            @include('site.partials.sidebar')
        @else
            <?php
                $user_view = true;
            ?>
        @endif
        <main class="main" style="{{ isset($user_view) ? 'margin-left: 0px !important;' : ''}}">
            <div class="{{ isset($user_view) ? 'container' : 'container-fluid'}}">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="cs-dashboard">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="d-flex block-small">
                                        <div class="d-flex--one">
                                            <span class="dark-blue title">My Profile</span>
                                        </div>
                                        @if(!isset($user_view))
                                        <div class="d-flex--two">
                                            <a href="{{ route('site.edit.institute.details') }}"><button class="btn btn-primary">Edit Profile</button></a>
                                        </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row d-flex-row">
                                <div class="col-md-4 col-xs-12">
                                    <div class="card z-depth-1">
                                        <div class="card-body">
                                            @if($data[0]['profile_pic'])
                                                <!-- <div class="logo-pic text-center">
                                                    <img id="preview" src="/{{ env('INSTITUTE_LOGO_PATH')}}{{$data[0]['id']}}/{{$data[0]['profile_pic']}}" avatar-holder-src="{{ asset('images/user_default_image.png') }}">
                                                </div> -->
                                                <div class="logo" style="background-image:url('/{{ env('INSTITUTE_LOGO_PATH')}}{{$data[0]['id']}}/{{$data[0]['profile_pic']}}')"></div>

                                            @else
                                                <!-- <div class="logo-pic pic">
                                                    <img id="preview" src="{{ asset('images/user_default_image.png') }}" avatar-holder-src="{{ asset('images/user_default_image.png') }}">
                                                </div> -->
                                                <div class="logo" style="background-image:url('/images/user_default_image.png')"></div>

                                            @endif

                                        </div>
                                        <div class="card-footer">
                                            <div class="logo-title">{{ isset($data[0]['institute_registration_detail']['institute_name']) ? strtoupper($data[0]['institute_registration_detail']['institute_name']) : ''}}</div>
                                        </div>
                                    </div>
                                </div>
                                @if(isset($data[0]['institute_registration_detail']['advertisment_details'][0]['img_path']))
                                    <div class="col-md-8 col-xs-12">
                                        <div class="card z-depth-1">
                                            <div class="card-body p-0">
                                                <div class="banner">
                                                    <?php
                                                        $img_path = '/'.env('INSTITUTE_LOGO_PATH').$data[0]['id'].'/'.$data[0]['institute_registration_detail']['advertisment_details'][0]['img_path'];
                                                    ?>
                                                    <div class="banner-image logo-pic img-responsive pointer" id="logo-pic" style="background-image: url(/{{ env('INSTITUTE_LOGO_PATH')}}{{$data[0]['id']}}/{{$data[0]['institute_registration_detail']['advertisment_details'][0]['img_path']}})">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            </div>
                            <?php
                                if(isset($data[0]['institute_registration_detail']['institute_detail']['institute_description']) && !empty($data[0]['institute_registration_detail']['institute_detail']['institute_description'])){
                                    $description = $data[0]['institute_registration_detail']['institute_detail']['institute_description'];
                                }
                                if(isset($data[0]['institute_registration_detail']['institute_notice']['notice']) && !empty($data[0]['institute_registration_detail']['institute_notice']['notice'])){
                                    $notice = $data[0]['institute_registration_detail']['institute_notice']['notice'];
                                }
                            ?>
                            <?php 
                                $curr_date = date('Y-m-d');
                                $start_date = $data[0]['institute_registration_detail']['institute_notice']['start_date'];
                                $end_date = $data[0]['institute_registration_detail']['institute_notice']['end_date'];
                                //dd($curr_date,$data[0]['institute_registration_detail']['institute_notice']['start_date'],$data[0]['institute_registration_detail']['institute_notice']['end_date']);
                            ?>
                            <div class="row d-flex-row">
                                @if($start_date <= $curr_date && $end_date >= $curr_date)
                                    <div class="col-md-8 col-xs-12">
                                @else
                                    <div class="col-md-12 col-xs-12">
                                @endif
                                    <div class="card">
                                        <div class="card-header">
                                            <div>Institute Description</div>
                                        </div>
                                        <div class="card-body z-depth-1">
                                            <div>
                                                {{ isset($description) ? ucfirst($description) : '-'}}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @if(isset($data[0]['institute_registration_detail']['institute_notice']['notice']) && !empty($data[0]['institute_registration_detail']['institute_notice']['notice']))
                                    
                                    @if($start_date <= $curr_date && $end_date >= $curr_date)
                                        <div class="col-md-4 col-xs-12">
                                            <div class="card">
                                                <div class="card-header">
                                                    <div>Notice</div>
                                                </div>
                                                <div class="card-body z-depth-1">
                                                    <div class="notice-image">
                                                        {{ $data[0]['institute_registration_detail']['institute_notice']['notice']}}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                @endif
                            </div>
                            @if(isset($institute_course_type) && !empty($institute_course_type))
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="card courses">
                                            <div class="card-header">
                                                <div>Institute Courses</div>
                                            </div>
                                            <div class="card-body z-depth-1">
                                                <div>
                                                    <!-- Nav tabs -->
                                                    <ul class=" nav nav-tabs" role="tablist">
                                                        <?php $active = 'active';$active_in = 'active in'?>
                                                        @foreach($institute_course_type as $course_type => $course)
                                                            <li role="presentation" class="intc-text {{$active}}">
                                                                <a href="#test{{$course_type}}" aria-controls="pre_sea" role="tab" data-toggle="tab">
                                                                @foreach(\CommonHelper::institute_course_types() as $r_index => $type)
                                                                    {{ isset($course_type) ? $course_type == $r_index ? $type : '' : ''}}
                                                                @endforeach
                                                                </a>
                                                            </li>
                                                            <?php $active = ''?>
                                                        @endforeach
                                                    </ul>
                                                    <!-- Tab panes -->
                                                    <div class="tab-content">
                                                        <?php $active = 'active';$active_in = 'active in'?>
                                                        @foreach($institute_course_type as $course_type => $courses)
                                                        <div role="tabpanel" class="tab-pane {{$active}}" id="test{{$course_type}}">
                                                            <div class="content">
                                                                <div class="row">
                                                                    <div class="col-lg-12">
                                                                        <div class="content-title"></div>
                                                                    </div>
                                                                    <div class="col-lg-12">
                                                                        <div class="list">
                                                                            @foreach($courses as $course_index => $course)
                                                                            <div class="list-item">
                                                                                <span class="icon">-</span>
                                                                                <span class="text">
                                                                                    <a href="{{route('site.seafarer.course.search',['course_type' => $course_type,'course_name' => $course['course_details']['id'],'institute_name' => [$data[0]['institute_registration_detail']['id']]])}}">
                                                                                        {{$course['course_details']['course_name']}}
                                                                                    </a>
                                                                                </span>
                                                                            </div>
                                                                            @endforeach
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <?php $active = ''?>
                                                        @endforeach
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif

                            <!-- Contact section -->
                            @if(isset($data[0]['institute_registration_detail']['institute_locations']) && !empty(isset($data[0]['institute_registration_detail']['institute_locations']) && $data[0]['institute_registration_detail']['institute_locations'] != NULL))
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="card courses">
                                        <div class="card-header">
                                            <div>Institute Contact Details</div>
                                        </div>
                                        <div class="card-body z-depth-1">
                                            <div>
                                                <!-- Nav tabs -->
                                                <ul class="nav nav-tabs course_list" role="tablist">
                                                @foreach($data[0]['institute_registration_detail']['institute_locations'] as $index => $institute_location)
                                                        @if($institute_location['headbranch'] == '1')
                                                            <li role="presentation" class="course_list_item active">
                                                                <a href="#ho" aria-controls="ho" role="tab" data-toggle="tab">Head Office</a>
                                                            </li>
                                                        @else
                                                            <li role="presentation" class="course_list_item">
                                                                <a href="#{{$index}}" aria-controls="{{$index}}" role="tab" data-toggle="tab">Branch</a>
                                                            </li>
                                                        @endif
                                                @endforeach
                                                </ul>

                                                <!-- Tab panes -->
                                                <div class="tab-content">
                                                    <?php $active='active'; ?>
                                                    @foreach($data[0]['institute_registration_detail']['institute_locations'] as $index => $institute_location)
                                                    <div role="tabpanel" class="tab-pane {{$active}}" id="{{ $institute_location['headbranch'] == '1' ? 'ho' : $index}}">
                                                        <div class="content">
                                                            <div class="row">
                                                                <div class="col-lg-12">
                                                                    <div class="content-title">
                                                                        <span class="orange-label">
                                                                            @foreach( \CommonHelper::institute_branch_type() as $b_index => $branch_type)
                                                                                {{ isset($institute_location['branch_type']) ? $institute_location['branch_type'] == $b_index ? $branch_type : '' : ''}}
                                                                            @endforeach
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-12">
                                                                    <div class="list">
                                                                        @if(isset($institute_location['address']) && !empty($institute_location['address']))
                                                                        <div class="list-item">
                                                                            <div class="icon"><i class="blue icon-location-pin"></i></div>
                                                                            <div class="text">
                                                                                {{ $institute_location['address'] }} ,
                                                                                {{ isset($institute_location['city_id']) ? $institute_location['pincode']['pincodes_cities'][0]['city']['name'] : $institute_location['city_text']}} - 
                                                                                {{ isset($institute_location['pincode_text']) ? $institute_location['pincode_text'] : ''}}
                                                                                {{ isset($institute_location['state_id']) ? $institute_location['pincode']['pincodes_states'][0]['state']['name'] : $institute_location['state_text']}}
                                                                            </div>
                                                                        </div>
                                                                        @endif
                                                                        @if(isset($institute_location['email']) && !empty($institute_location['email']))
                                                                        <div class="list-item">
                                                                            <div class="icon"><i class="blue icon-envelope"></i></div>
                                                                            <div class="text">{{$institute_location['email']}}</div>
                                                                        </div>
                                                                        @endif
                                                                        @if(isset($institute_location['telephone']) && !empty($institute_location['telephone']))
                                                                        <div class="list-item">
                                                                            <div class="icon"><i class="blue icon-phone"></i></div>
                                                                            <div class="text">{{$institute_location['telephone']}}</div>
                                                                        </div>
                                                                        @endif
                                                                        @if(isset($data[0]['institute_registration_detail']['website']) && !empty($data[0]['institute_registration_detail']['website']))
                                                                            <div class="list-item">
                                                                                <div class="icon"><i class="blue icon-globe"></i></div>
                                                                                <div class="text">
                                                                                    <a target="_blank" href="{{ "http://".$data[0]['institute_registration_detail']['website'] }}">
                                                                                        {{ isset($data[0]['institute_registration_detail']['website']) ? "http://".$data[0]['institute_registration_detail']['website'] : '-' }}
                                                                                    </a>
                                                                                </div>
                                                                            </div>
                                                                        @endif
                                                                        @if(isset($institute_location['number']) && !empty($institute_location['number']))
                                                                        <div class="list-item">
                                                                            <div class="icon"><i class="blue icon-phone"></i></div>
                                                                            <div class="text">Institute Phone Number : {{$institute_location['number']}}</div>
                                                                        </div>
                                                                        @endif
                                                                        <!-- location -->
                                                                        @if(isset($institute_location['location_contact']) && !empty($institute_location['location_contact']))
                                                                            <?php $loc = $institute_location['location_contact'][0];
                                                                            ?>
                                                                            @if(!empty($loc['contact_name']))
                                                                            <div class="list-item">
                                                                                <div class="icon"><i class="blue icon-phone"></i></div>
                                                                                
                                                                                <div class="text">Contact Person Name : {{$loc['contact_name']}}</div>
                                                                            </div>
                                                                            @endif
                                                                            @if(!empty($loc['designation']))
                                                                            <div class="list-item">
                                                                                <div class="icon"><i class="blue icon-phone"></i></div>
                                                                                <div class="text">Designation : {{$loc['designation']}}</div>
                                                                            </div>
                                                                            @endif
                                                                            @if(!empty($loc['contact_number']))
                                                                            <div class="list-item">
                                                                                <div class="icon"><i class="blue icon-phone"></i></div>
                                                                                <div class="text">Contact Person Number :  {{$loc['contact_number']}}</div>
                                                                            </div>
                                                                            @endif
                                                                        @endif
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <?php $active=''; ?>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endif

                            @if(isset($data[0]['institute_registration_detail']['institute_gallery']) && !empty($data[0]['institute_registration_detail']['institute_gallery']))
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="card z-depth-1">
                                            <div class="card-header">
                                                <div>Gallery</div>
                                            </div>
                                            <div class="card-body">
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <div class="gallery-list">
                                                            @foreach($data[0]['institute_registration_detail']['institute_gallery'] as $images)
                                                                <?php
                                                                    $image_path = '';
                                                                    if(isset($images)){
                                                                        $image_path = "/".env('INSTITUTE_IMAGE_GALLERY_PATH')."".$images['institute_id']."/".$images['image'];
                                                                    } 
                                                                ?>
                                                                <a class="fancybox gallery-list-item" rel="gallery1" href="{{$image_path}}" title="">
                                                                    <img style="width: 100%;height: 100%;cursor: pointer;" src="{{$image_path}}" alt="" />
                                                                </a>
                                                                <!-- <div class="gallery-list-item" style="background-image:url({{ $image_path }})"></div> -->
                                                            @endforeach
                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>            
                </div>
            </div>
        </main>
    </div>
    <div id="advModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Advertisement</h4>
                </div>
                <div class="modal-body">
                    <img src="{{isset($img_path) ? $img_path : ''}}" class="img-responsive w-100" style="box-shadow:0px 2px 5px 1px #b3aeae;">
                </div>
            </div>

        </div>
    </div>
@stop
@section('js_script')
    <script type="text/javascript" src="/js/site/institute-registration.js"></script>
    <script type="text/javascript" src="/js/site/jquery.fancybox.js"></script>
    <script type="text/javascript">
        
        $(document).ready(function() {
            $(".fancybox").fancybox({
                openEffect  : 'none',
                closeEffect : 'none'
            });
        });

    </script>
@stop