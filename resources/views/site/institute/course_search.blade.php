@extends('site.index')

@section('content')

<div class="search-listing content-section-wrapper">
	<div class="container">
		<div class="row">
			<div class="col-sm-3">
				
			</div>
		</div>
		<div class="search-listing-container">
			<button class="btn search-listing-sm-filter-btn" type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#search-listing-filter-modal"><i class="fa fa-filter" aria-hidden="true"></i></button>
			<div class="row">
				<div class="col-sm-3">
					<div class="filter-container filter-container-lg">
						<div class="heading">
							Filter
						</div>
						<form id="seafarer-course-search-filter" action="{{route('site.seafarer.course.search')}}" method="GET">
							{{csrf_field()}}
							<div class="content-container">
								<div class="form-group">
				                    <label class="input-label" for="course_type">Course Type</label>
				                    <select name="course_type" class="form-control course_type_filter">
                                        <option value=>Select Course Type</option>
                                        @foreach(\CommonHelper::institute_course_types() as $r_index => $rank)
                                            <option value="{{$r_index}}" {{ isset($filter['course_type']) ? $filter['course_type'] == $r_index ? 'selected' : '' : ''}}>{{$rank}} </option>
                                        @endforeach
                                    </select>
				                </div>
				                <div class="form-group">
									<label class="input-label">Course Name</label>
									<select class="form-control institute_course_name" name="course_name">
                                        <option value=''>Select Course Name</option>
                                        @if(isset($course_name) && !empty($course_name))
                                            @foreach($course_name as $r_index => $name)
                                                <option value="{{$r_index}}" {{ isset($filter['course_name']) ? $filter['course_name'] == $name['id'] ? 'selected' : '' : ''}}>{{$name['course_name']}} </option>
                                            @endforeach
                                        @endif
                                    </select>
								</div>
								<div class="form-group">
									<label class="input-label">Start Date</label>
									<input type="text" name="start_date" class="form-control m-y-datepicker" value="{{isset($filter['start_date']) ? !empty($filter['start_date']) ? date('m-Y',strtotime('01-'.$filter['start_date'])) : '' : ''}}" placeholder="mm-yyyy">
								</div>
				                <div class="form-group">
				                	<button type="button" data-style="zoom-in" class="btn coss-primary-btn ladda-button" id="seafarer-course-search-button">Search</button>
				                </div>
							</div>
						</form>
					</div>
					@include('site.partials.featured_advertisement_template')
				</div>
				<div class="col-sm-9">
					<div class="sort-by-container">
						<div class="row">
							<div class="col-xs-12">
								<div class="search-title">
									Course Search Results
								</div>
							</div>
						</div>
					</div>
					
					<div class="card-container">
					@if(isset($data) && !empty($data))
						<div class="row p-t-10 flex-height">
							<div class="col-md-12 col-xs-12">
								<div class="pagi">
					                <div class="search-count">
					                    Showing {{$pagination_data['from']}} - {{$pagination_data['to']}} of {{$pagination_data['total']}} Courses
					                </div>
					                <nav> 
						                <ul class="pagination pagination-sm">
						                    {!! $pagination->render() !!}
						                </ul> 
					                </nav>
					            </div>
				            </div>
			            </div>
						@foreach($data as $index => $course)

							<div class="job-card">
								@if(isset($course['user_applied_courses']) && count($course['user_applied_courses']) > 0)
									<div class="applied-company" id="applied-for-course-{{$course['course_id']}}">
										<i class="fa fa-check-circle-o applied-icon" aria-hidden="true"></i>
										Already Applied
									</div>
								@else
									<div class="applied-company hide" id="applied-for-course-{{$course['course_id']}}">
										<i class="fa fa-check-circle-o applied-icon" aria-hidden="true"></i>
										Already Applied
									</div>
								@endif

					    		<div class="row m-0 flex-height">
					    			<div class="col-sm-4 col-md-3 p-0 flex-center">
					    				<div class="job-company-image">
					    					@if(isset($course['institute_registration_detail']['user']['profile_pic']))
					    						<img class="profile-pic" src="/{{ isset($course['institute_registration_detail']['user']['profile_pic']) ? env('INSTITUTE_LOGO_PATH')."/".$course['institute_registration_detail']['user']['id']."/".$course['institute_registration_detail']['user']['profile_pic'] : 'images/user_default_image.png'}}" avatar-holder-src="{{ asset('images/user_default_image.png') }}" alt="">
					    					@else
					    						<img class="profile-pic" src="{{asset('images/user_default_image.png')}}" avatar-holder-src="{{ asset('images/user_default_image.png') }}" alt="" width="140px">
					    					@endif
					    				</div>
					    			</div>
					    			
					    			<div class="col-sm-8 col-md-9 col-xs-12 p-0">
					    				<div class="company-discription">
					    					<div class="other-discription row">
						    					<div class="col-sm-12 f-18" style="font-size: 18px;">
							    					{{ isset($course['institute_registration_detail']['institute_name']) ? ucfirst($course['institute_registration_detail']['institute_name']) : ''}}
							    				</div>
						    				</div>
						    				<div class="other-discription row">
						    					<div class="col-sm-6">
						    						Course Type:
						                            @foreach(\CommonHelper::institute_course_types() as $index => $category)
			                                            {{ !empty($course['course_type']) ? $course['course_type'] == $index ? $category : '' : ''}}
			                                        @endforeach
						    					</div>
						    					<div class="col-sm-6">
							    					Course Name: 
							    					@foreach($all_courses as $index => $category)
			                                            {{ !empty($course['course_id']) ? $course['course_id'] == $category['id'] ? $category['course_name'] : '' : ''}}
			                                        @endforeach
							                    </div>
						    				</div>

						    				<?php
                                                $collection = collect($course['institute_batches']);
                                                
                                                $batches = array_values($collection->sortBy('start_date')->toArray());
                                                $course['institute_batches'] = $batches;
                                                $Select = 'select';
                                                //dd($course['institute_batches']);
                                            ?>
                                            <div class="other-discription">
                                                Batches Date: 
                                                <span class="ans">
                                                    @if(isset($course['institute_batches'][0]))
                                                        <div class="batches dont-break height-equalizer-wrapper dont-break p-t-10">
                                                       
                                                            @foreach($course['institute_batches'] as $key => $value)
                                                                <div class="date_slide date_slide_{{$course['course_id']}} z-depth-1 {{ $key == 0 ? $Select : ''}}" data-id="{{$value['id']}}" data-key="{{$course['course_id']}}">
                                                                    {{isset($value['start_date']) ? date('d',strtotime($value['start_date'])) : ''}}
                                                                    {{isset($value['start_date']) ? date('M',strtotime($value['start_date'])) : ''}}
                                                                </div>
                                                            @endforeach
                                                        </div>
                                                    @else
                                                        No batches found.
                                                    @endif
                                                </span>
                                            </div>

                                            
                                            @foreach($course['institute_batches'] as $key => $value)
	                                            <div class="row details details-key-{{$course['course_id']}} details-{{$value['id']}} {{ $key == 0 ? '' : 'hide'}}">
	                                                <div class="col-sm-6">
	                                                    <div class="other-discription p-l-0 p-r-0">
	                                                        Start Date: 
	                                                        <span class="ans">
	                                                            {{isset($value['start_date']) ? date('d-m-Y',strtotime($value['start_date'])) : ''}}
	                                                        </span>
	                                                    </div>
	                                                    <div class="other-discription p-l-0 p-r-0">
	                                                        Duration: 
	                                                        <span class="ans">
	                                                            {{isset($value['duration']) ? $value['duration'] : ''}} Days
	                                                        </span>
	                                                    </div>
	                                                </div>
	                                                <div class="col-sm-6">
	                                                    <div class="other-discription p-l-0 p-r-0">
	                                                        Cost: 
	                                                        <span class="ans">
	                                                            <i class="fa fa-inr"></i>
	                                                            {{isset($value['cost']) ? $value['cost'] : ''}}
	                                                        </span>
	                                                    </div>
	                                                    <div class="other-discription p-l-0 p-r-0">
	                                                        Size: 
	                                                        <span class="ans">
	                                                            {{isset($value['size']) ? $value['size'] : ''}}
	                                                        </span>
	                                                    </div>
	                                                </div>

	                                                <?php
	                                                    $location_ids = '';
	                                                    $locations = '';
	                                                    $comma = 0;

	                                                    if(isset($value['institute_location']) && !empty($value['institute_location'])){
	                                                        $location_ids = array_values(collect($value['institute_location'])->pluck('location_id')->toArray());
	                                                    }
	                                                    
	                                                    // dd($course['institute_location']);
	                                                ?>

	                                                <div class="col-sm-12">
	                                                    <div class="other-discription">
	                                                        Location:
	                                                        <span class="ans">
	                                                            @if(isset($course['institute_location']))
	                                                                @foreach($course['institute_location'] as $index => $data1)
	                                                                    @if($data1['country'] == '95')
	                                                                        @if(!empty($location_ids) && in_array($data1['id'],$location_ids))
	                                                                        	@if($comma == 1)
	                                                                        		,
	                                                                        	@endif

	                                                                            {{$data1['city']['name']}} - {{ucfirst(strtolower($data1['state']['name']))}}
	                                                                            
	                                                                            <?php $comma = 1 ?>
	                                                                        @endif

	                                                                    @endif
	                                                                @endforeach
	                                                            @endif
	                                                        </span>
	                                                    </div>
	                                                </div>
	                                            </div>
                                            @endforeach
					    				</div>
					    				@if((isset($role) AND $role == 'seafarer') || empty($user_id))
											@if(isset($course['user_applied_courses']) && count($course['user_applied_courses']) > 0)
												
													<button  data-style="zoom-in" class="btn coss-primary-btn apply-btn course-apply-button ladda-button hide" id="course-apply-button-{{$course['course_id']}}" data-course-id="{{$value['id']}}" data-institute-id="{{$course['institute_id']}}" data-btn-id={{$course['course_id']}}>
							    					Apply Now
							    					</button>
												
											@else
												<button  data-style="zoom-in" class="btn coss-primary-btn apply-btn course-apply-button ladda-button" id="course-apply-button-{{$course['course_id']}}" data-course-id="{{$value['id']}}" data-institute-id="{{$course['institute_id']}}" data-btn-id={{$course['course_id']}}>
						    					Apply Now
						    					</button>
											@endif
										@endif
					    			</div>
					    		</div>
					    	</div>
					    @endforeach
						<div class="row p-t-10 flex-height">
							<div class="col-md-12 col-xs-12">
								<div class="pagi">
					                <div class="search-count">
					                    Showing {{$pagination_data['from']}} - {{$pagination_data['to']}} of {{$pagination_data['total']}} Courses
					                </div>
					                <nav> 
					                <ul class="pagination pagination-sm">
					                    {!! $pagination->render() !!}
					                </ul> 
					                </nav>
					            </div>
				            </div>
			            </div>
					</div>
					@else
						<div class="row no-results-found">No results found. Try again with different search criteria.</div>
					@endif
					</div>
				</div>
			</div>
		</div>
	</div>
	  <!-- Modal -->
	<div class="modal fade" id="search-listing-filter-modal" role="dialog">
		<div class="modal-dialog">

		  <!-- Modal content-->
			<div class="modal-content">
			    <div class="modal-header">
			      <button type="button" class="close" data-dismiss="modal">&times;</button>
			      <h4 class="modal-title text-center">Filter</h4>
			    </div>
			    <div class="modal-body">
			      	<div class="filter-container-sm">
			      		<form id="seafarer-course-search-filter" action="{{route('site.seafarer.course.search')}}" method="GET">
							{{csrf_field()}}
							<div class="content-container">
								<div class="form-group">
				                    <label class="input-label" for="course_type">Course Type</label>
				                    <select name="course_type" class="form-control course_type_filter">
	                                    <option value=>Select Course Type</option>
	                                    @foreach(\CommonHelper::institute_course_types() as $r_index => $rank)
	                                        <option value="{{$r_index}}" {{ isset($filter['course_type']) ? $filter['course_type'] == $r_index ? 'selected' : '' : ''}}>{{$rank}} </option>
	                                    @endforeach
	                                </select>
				                </div>
				                <div class="form-group">
									<label class="input-label">Course Name</label>
									<select class="form-control institute_course_name" name="course_name">
	                                    <option value=''>Select Course Name</option>
	                                    @if(isset($course_name) && !empty($course_name))
	                                        @foreach($course_name as $r_index => $name)
	                                            <option value="{{$r_index}}" {{ isset($filter['course_name']) ? $filter['course_name'] == $name['id'] ? 'selected' : '' : ''}}>{{$name['course_name']}} </option>
	                                        @endforeach
	                                    @endif
	                                </select>
								</div>
								<div class="form-group">
									<label class="input-label">Start Date</label>
									<input type="text" name="start_date" class="form-control m-y-datepicker" value="{{isset($filter['start_date']) ? !empty($filter['start_date']) ? date('m-Y',strtotime('01-'.$filter['start_date'])) : '' : ''}}" placeholder="mm-yyyy">
								</div>
				                <div class="form-group">
				                	<button type="submit" data-style="zoom-in" class="btn coss-primary-btn ladda-button" id="seafarer-course-search-button">Search</button>
				                </div>
							</div>
						</form>
					</div>
			    </div>
			    <div class="modal-footer">
			      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			    </div>
			</div>
		  
		</div>
	</div>
</div>
@stop

@section('js_script')
	<script type="text/javascript" src="/js/site/job_search.js"></script>

	<script type="text/javascript" src="/js/site/institute-registration.js"></script>
@stop