@extends('site.index')
@section('content')

	<div class="candidate-search job-application-listing content-section-wrapper sm-filter-space dashboard-body">
		@include('site.partials.sidebar')
		<main class="main" style="padding: 20px 20px 80px 20px !important;min-height: -webkit-fill-available !important;">
			<div class="row">
                <div class="col-xs-12">
                    <div class="company-page-profile-heading">
                        Course Booking Filter
                    </div>
                </div>
            </div>
            <div class="candidate-search-container">
                <div class="section-1">
                    <form id="candidate-filter-form" action={{route('site.institute.list.batch.applicant')}} method="get">
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label>Seafarer Name </label>
                                   	<input type="text" class="form-control" name="name" placeholder="Enter name" value="{{isset($filter['name']) ? $filter['name'] : ''}}"></input>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label>Seafarer Email</label>
                                    <input type="text" class="form-control" name="email" placeholder="Enter email" value="{{isset($filter['email']) ? $filter['email'] : ''}}"></input>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label>Mobile</label>
                                    <input type="text" class="form-control" name="mobile" placeholder="Enter mobile" value="{{isset($filter['mobile']) ? $filter['mobile'] : ''}}"></input>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label>From Date</label>
                                    <input type="text" name="from_date" class="form-control issue-datepicker" value="{{isset($filter['from_date']) ? !empty($filter['from_date']) ? date('d-m-Y',strtotime($filter['from_date'])) : '' : ''}}" placeholder="dd-mm-yyyy">
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label>To Date</label>
                                    <input type="text" name="to_date" class="form-control datepicker" value="{{isset($filter['to_date']) ? !empty($filter['to_date']) ? date('d-m-Y',strtotime($filter['to_date'])) : '' : ''}}" placeholder="dd-mm-yyyy">
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label>Status</label>
                                    <select name="status" class="form-control status">
                                        <option value=>Select Status</option>
                                        @foreach(\CommonHelper::order_status() as $r_index => $status)
                                            <option value="{{$r_index}}" {{ isset($filter['status']) && $filter['status'] != '' ? $filter['status'] == $r_index ? 'selected' : '' : ''}}>{{$status}} </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label>Batch Type</label>
                                    <select name="batch_type" class="form-control batch_type">
                                        <option value=>Select Batch Type</option>
                                        <option value="confirmed" {{ isset($filter['batch_type']) ? $filter['batch_type'] == 'confirmed' ? 'selected' : '' : ''}}>Confirmed</option>
                                        <option value="on_demand" {{ isset($filter['batch_type']) ? $filter['batch_type'] == 'on_demand' ? 'selected' : '' : ''}}>On Demand</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="section_cta text-right m-t-25 job-btn-container">
                                    <button type="button" class="btn coss-inverse-btn search-reset-btn" id="resetButton">Reset</button>
                                    <button type="submit" data-style="zoom-in" class="btn coss-primary-btn search-post-btn ladda-button" id="candidate-filter-search-button"><i class="fa fa-search search-icon" aria-hidden="true"></i>Search</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="section-2">
					@if(isset($data) AND !empty($data))
					<div class="loader_overlay hide">
                        <div class="loader_ele loader" style="position:fixed !important;left: 55%"></div>
                    </div>
					<div class="listing-container">
						{{csrf_field()}}
						<div class="row">
							<div class="col-xs-12 col-sm-8">
			                    <div class="company-page-profile-heading">
			                        Course Booking Listing
			                    </div>
			                </div>
			                <div class="col-xs-12 col-sm-4">
								<span class="pagi pagi-up">
			                        <span class="search-count" style="padding-bottom: 5px;">
			                            Showing {{$pagination_data['from']}} - {{$pagination_data['to']}} of {{$pagination_data['total']}} Applicants
			                        </span>
			                        <nav> 
			                        <ul class="pagination pagination-sm">
			                            {!! $pagination->render() !!}
			                        </ul> 
			                        </nav>
			                    </span>
		                    </div>
						</div>
						@foreach($data as $batch_data)
							<div class="job-card">
								<div class="row m-0 flex-height">
									<div class="col-xs-12 col-sm-3 p-0">
										<div class="job-company-image h-100 job-applicant-profile-pic-box">
											<div class="profile-btn-container">
												<img src="{{!empty($batch_data['user']['profile_pic']) ? "/".env('SEAFARER_PROFILE_PATH').$batch_data['user_id']."/".$batch_data['user']['profile_pic'] : '/images/user_default_image.png'}}" avatar-holder-src="{{ asset('images/user_default_image.png') }}" class="job-applicant-user-profile-pic">
												<br>
                                            	<a class="btn coss-primary-btn view-profile-btn m-t-10" target="_blank" href="{{route('user.view.profile',['id' => $batch_data['user']['id']])}}">View Profile</a>
                                        	</div>
	                                    </div>
									</div>
									<div class="col-sm-9 p-0">
										<div class="company-discription">
											<div class="row">
												<div class="col-sm-4">
													<div class="other-discription">
														<b>Seafarer Details</b>
													</div>
												</div>
												<div class="col-sm-4">
													<div class="other-discription">
														<b>Batch Details</b>
													</div>
												</div>
												<?php //if(isset($batch_data['status']) && $batch_data['status'] == '0') ?>
												<div class="col-sm-4 setting-{{$batch_data['id']}} d-flex" style="justify-content: space-between;">
													<div class="other-discription">
														<b>Booking Details</b>
													</div>
													@if(isset($batch_data['status']) && $batch_data['status'] == '0')
													<div class="dropdown setting" style="float: right;">
			                                            <a data-toggle="dropdown" class="btn-xs dropdown-toggle">
			                                                <i class="fa fa-cog"></i>
			                                            </a>
			                                            <ul class="dropdown-menu dropdown-light pull-right">
			                                                <li>
			                                                    <a class="batchStatusChangeButton" data-order-id="{{$batch_data['id']}}" data-batch-id="{{$batch_data['batch_id']}}" data-status="4">
			                                                        <span>Approve</span>
			                                                    </a>
			                                                </li>
			                                                
			                                                <li>
			                                                    <a class="batchStatusChangeButton" data-order-id="{{$batch_data['id']}}" data-batch-id="{{$batch_data['batch_id']}}" data-status="5">
			                                                        <span>Deny</span>
			                                                    </a>
			                                                </li>
			                                            </ul>
			                                        </div>
			                                        @endif
												</div>
											</div>
											<div class="row">
												<div class="col-sm-4">
													<div class="other-discription">
														<strong>Seafarer Name:</strong> {{ isset($batch_data['user']['first_name']) ? $batch_data['user']['first_name'] : ''}}
													</div>
												</div>
												<?php
													if(isset($batch_data['batch_details']['course_details']['course_type'])){
														$course_type = $batch_data['batch_details']['course_details']['course_type'];

														$course_name = $batch_data['batch_details']['course_details']['courses'][0]['course_name'];
													}
												?>
												<div class="col-sm-4">
													<div class="other-discription">
														<strong>Course Type:</strong>
														@foreach(\CommonHelper::institute_course_types() as $r_index => $rank)
				                                            {{ isset($course_type) ? $course_type == $r_index ? $rank : '' : ''}}
				                                        @endforeach
													</div>
												</div>
												<div class="col-sm-4">
													<div class="other-discription">
														<strong>Booking Date:</strong>
														{{ isset($batch_data['created_at']) ? date('d-m-Y',strtotime($batch_data['created_at'])) : ''}}
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-sm-4">
													<div class="other-discription">
														<strong>Email:</strong>
														{{ isset($batch_data['user']['email']) ? $batch_data['user']['email'] : '-'}}
													</div>
												</div>
												<div class="col-sm-4">
													<div class="other-discription">
														<strong>Course Name:</strong>
														{{ isset($course_name) ? $course_name : '-'}}
													</div>
												</div>
												<div class="col-sm-4">
													<div class="other-discription">
														<strong>Discount:</strong>
														{{ isset($batch_data['discount']) ? $batch_data['discount'] : '-'}}%
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-sm-4">
													 <?php
						                                if(isset($batch_data['user']['professional_detail']['current_rank_exp'])){
						                                    $rank_exp = explode(".",$batch_data['user']['professional_detail']['current_rank_exp']);
						                                    $rank_exp[1] = number_format($rank_exp[1]);
						                                }
						                            ?>
						                            <div class="other-discription">
						                                <strong>Mobile: </strong>
						                                <span class="ans">
						                                    {{ isset($batch_data['user']['mobile']) ? $batch_data['user']['mobile'] : '-'}}
						                                </span>
						                            </div>
												</div>
												<div class="col-sm-4">
													<div class="other-discription">
														<strong>Start Date:</strong>
														{{ isset($batch_data['batch_details']['course_start_date']) ? date('d-m-Y',strtotime($batch_data['batch_details']['course_start_date'])) : ' On Demand '}}

														<?php
															
														    if($batch_data['batch_details']['batch_type'] == 'on_demand' && empty($batch_data['batch_details']['course_start_date']) ) {
														        echo "<span class='sub-date'> ( " . date('M y', strtotime($batch_data['batch_details']['start_date'])) . ")</span>";
														    }
														?>
													</div>
												</div>
												<div class="col-sm-4">
													<div class="other-discription">
														<strong>Cost:</strong>
														<i class="fa fa-inr fa-sm"></i> 
														{{ isset($batch_data['subscription_cost']) ? $batch_data['subscription_cost'] : ''}}
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-sm-4">
													<div class="other-discription">
														<strong>Current Rank:</strong>
														@foreach(\CommonHelper::new_rank() as $index => $category)
															@foreach($category as $r_index => $rank)
																{{ !empty($batch_data['user']['professional_detail']['current_rank']) ? $batch_data['user']['professional_detail']['current_rank'] == $r_index ? $rank : '' : ''}}
															@endforeach
														@endforeach
													</div>
												</div>
												<div class="col-sm-4">
													<div class="other-discription">
														<strong>Payment Type: </strong>
														{{ isset($batch_data['batch_details']['payment_type']) ? ucwords($batch_data['batch_details']['payment_type'])." Payment" : '-'}}
													</div>
												</div>
												<div class="col-sm-4">
													<div class="other-discription">
														<strong>Tax Amount:</strong>
														<i class="fa fa-inr fa-sm"></i> 
														{{ isset($batch_data['tax_amount']) ? $batch_data['tax_amount'] : '-'}}
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-sm-4">
													<div class="other-discription">
														<strong>Passport Number: </strong>
														{{ isset($batch_data['user']['passport_detail']['pass_number']) && !empty($batch_data['user']['passport_detail']['pass_number']) ? $batch_data['user']['passport_detail']['pass_number'] : '-'}}
													</div>
												</div>
												<div class="col-sm-4">
													<div class="other-discription">
														<strong>Batch Type:</strong>
														{{ isset($batch_data['batch_details']['batch_type']) ? $batch_data['batch_details']['batch_type'] == 'confirmed' ? 'Confirmed' : '' : ''}}
														{{ isset($batch_data['batch_details']['batch_type']) ? $batch_data['batch_details']['batch_type'] == 'on_demand' ? 'On Demand' : '' : ''}}
													</div>
												</div>
												<div class="col-sm-4">
													<div class="other-discription">
														<strong>Total:</strong>
														<i class="fa fa-inr fa-sm"></i> 
														{{ isset($batch_data['total']) ? $batch_data['total'] : '-'}}
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-sm-4">
													<div class="other-discription">
														<strong>Indos Number: </strong>
														{{ isset($batch_data['user']['wkfr_detail']['indos_number']) && !empty($batch_data['user']['wkfr_detail']['indos_number']) ? $batch_data['user']['wkfr_detail']['indos_number'] : '-'}}
													</div>
												</div>
												<div class="col-sm-4">
													<div class="other-discription">
														<?php $location = $batch_data['batch_details']['batch_location'][0]; ?>
														<strong>Batch Location:</strong>
														{{$location['location']['address']}},
	                                                    {{$location['location']['city']['name']}} -
	                                                    {{$location['location']['pincode_text']}}.
													</div>
												</div>
												<div class="col-sm-4">
													<div class="other-discription">
														<strong>Status:</strong>
														<?php 
															$b_status = isset($batch_data['status']) ? $batch_data['status'] : '';
															$last_order = last($batch_data['order_history']);
														?>
														<span class="status status-{{$batch_data['id']}}">
															@foreach(\CommonHelper::order_status() as $o_index => $status)
							                                    {{ isset($batch_data['status']) ? $batch_data['status'] == $o_index ? $status : '' : ''}}
								                            @endforeach
								                            @if($b_status == '5')
								                            	{{ isset($last_order['reason']) && !empty($last_order['reason']) ? " - ".$last_order['reason'] : '' }}
								                            @endif
								                        </span>
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-sm-12">
		                                            <div class="status-block">
		                                                <div class="status">
		                                                    <div class="track-status">
		                                                        <span class="green-dot"></span>

		                                                        <span class="green-status-line"></span>
		                                                    </div>
		                                                    <div class="status-text-line">
		                                                        <span class="status-text"> 
		                                                            Blocked
		                                                            <br>
		                                                            <span class="status-date">
		                                                            {{isset($batch_data['created_at']) ? date('d-m-Y H:i',strtotime('+5 hour +30 minutes',strtotime($batch_data['created_at']))) : '-'}}
		                                                            </span>
		                                                        </span>
		                                                    </div>
		                                                </div>
		                                                @foreach($batch_data['order_history'] as $my_orders)
		                                                <div class="status">
		                                                    <div class="track-status">
		                                                        <span class="green-dot"></span>

		                                                        <span class="green-status-line"></span>
		                                                    </div>
		                                                    <div class="status-text-line">
		                                                        <span class="status-text"> 
		                                                            @foreach(\CommonHelper::order_status() as $s_index => $status)
		                                                                {{ isset($my_orders['to_status']) ? $my_orders['to_status'] == $s_index ? $status : '' : ''}}
		                                                            @endforeach
		                                                            <br>
		                                                            @if($my_orders['to_status'] != '0')
		                                                                <span class="status-date">
		                                                                    {{isset($my_orders['created_at']) ? date('d-m-Y H:i',strtotime('+5 hour +30 minutes',strtotime($my_orders['created_at']))) : '-'}}
		                                                                </span>
		                                                            @endif
		                                                        </span>
		                                                    </div>
		                                                </div>
		                                                @endforeach
		                                                @if($batch_data['status'] == '1')
		                                                <div class="status">
		                                                    <div class="track-status">
		                                                        <span class="green-dot"></span>

		                                                        <span class="green-status-line"></span>
		                                                    </div>
		                                                    <div class="status-text-line">
		                                                        <span class="status-text"> 
		                                                            Seat Confirmed
		                                                            <br>
		                                                            <span class="status-date">
		                                                                {{isset($batch_data['updated_at']) ? date('d-m-Y H:i',strtotime('+5 hour +30 minutes',strtotime($batch_data['updated_at']))) : '-'}}
		                                                            </span>
		                                                        </span>
		                                                    </div>
		                                                </div>
		                                                @endif
		                                            </div>
		                                        </div>
											</div>
										</div>
									</div>
								</div>
							</div>
						@endforeach
						<div class="row">
			                <div class="col-xs-12 col-sm-12">
								<span class="pagi pagi-up m-t-10">
			                        <span class="search-count" style="padding-bottom: 5px;">
			                            Showing {{$pagination_data['from']}} - {{$pagination_data['to']}} of {{$pagination_data['total']}} Applicants
			                        </span>
			                        <nav> 
			                        <ul class="pagination pagination-sm">
			                            {!! $pagination->render() !!}
			                        </ul> 
			                        </nav>
			                    </span>
		                    </div>
						</div>
					</div>
					@else
	                    <div class="company-page-profile-heading">
	                        Course Applicant Listing
	                    </div>
						<div class="no-results-found">No Results Found.</div>
					@endif
				</div>
			</div>
		</main>
	</div>
	<div id="batch-denied-modal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
              	<div class="modal-header">
	                <button type="button" class="close" data-dismiss="modal">&times;</button>
	                <h4 class="modal-title modal-heading">Batch Status</h4>
	            </div>
              	<div class="modal-body">
                	<p class="modal-body-content">Reason for batch deniel:
                		<div class="row">
                			<div class="col-sm-12">
                				<textarea class="form-control reason" name="reason"></textarea>
                			</div>
                		</div>
                	</p>
              	</div>
              	<div class="modal-footer">
                	<button type="button" class="btn btn-red" data-dismiss="modal">Close</button>
                	<button type="button" class="btn btn-info ladda-button" data-style="zoom-in" id="batch-denied-ok">Ok</button>
              	</div>
            </div>
        </div>
    </div>

@stop
@section('js_script')
    <script type="text/javascript" src="/js/site/institutes_details.js"></script>
@stop