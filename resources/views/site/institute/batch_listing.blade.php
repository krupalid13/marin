@extends('site.index')

@section('content')

<style type="text/css">
    .batch-description{
        background-color: #fff;
        border-radius: 4px;
        box-shadow: 0 1px 3px 0px #b1abab;
        margin-top: 10px;
        padding: 10px;
    }
    .other-discription{
        color: #8e8e8e;
        padding: 3px 0px 3px 15px;
        font-size: 16px;
    }
    span.glyphicon.glyphicon-th-list {
        font-size: 25px;
    }
    span.glyphicon.glyphicon-calendar {
        font-size: 25px;
    }
    .col-sm-3.icons-div {
        margin-top: 30px;
        margin-left: 1%;
    }
        td.blur > a {
        color: #fff;
    }
    .planner .blur {
        background: #fbfbfb;
        color: #fff;
    }
    select.form-control.course_type {
        width: 235px;
    }
    select.form-control.institute_course_name {
        width: 239px;
    }
</style>

<div class="candidate-search content-section-wrapper dashboard-body">
    <button type="button" class="btn candidate-search-modal-btn" data-toggle="modal" data-target="#candidate-search-filter-modal"><i class="fa fa-filter" aria-hidden="true"></i></button>
    <div class="row no-margin">
        @include('site.partials.sidebar')
        <main class="main" style="padding: 20px 20px 80px 20px !important;">
            <div class="row">
                <div class="col-xs-12">
                    <div class="company-page-profile-heading">
                        Batch Listing
                    </div>
                </div>
            </div>
            <div class="candidate-search-container">
                <div class="section-1">
                    <?php $batch_data = $data->toArray(); ?>
                    <form id="candidate-filter-form" action={{route('site.institute.batch.listing')}}>

                        <div class="row">
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label>Course Type </label>
                                    <select name="course_type" class="form-control course_type">
                                        <option value=>Select Course Type</option>
                                        @foreach($course_type_list as $r_index => $rank)
                                           
                                            <option value="{{$rank['id']}}" {{ isset($filter['course_type']) ? $filter['course_type'] == $rank['id'] ? 'selected' : '' : ''}}>{{$rank['name']}} </option>
                                            }
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label>Course Name</label>
                                    <?php
                                        $selected_course_name = isset($_GET['course_name']) ? $_GET['course_name'] : 0;

                                    ?>
                                    <select class="form-control institute_course_name" name="course_name">
                                        <option value=''>Select Course Name</option>
                                        @if(isset($course_name) && !empty($course_name))
                                            @foreach($course_name as $r_index => $name)
                                                <option value="{{$name['id']}}" 
                                                    <?php if($selected_course_name == $name['id']) {
                                                        echo 'selected';
                                                    } ?>
                                                >
                                                    {{$name['course_name']}} 
                                                </option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-3 icons-div">
                                <div class="form-group">
                                <a href="javascript:void(0)" onclick="changeView('list')">
                                    <span class="glyphicon glyphicon-th-list"></span>
                                 </a>
                                 &nbsp;
                                 <a href="javascript:void(0)" onclick="changeView('calendar')">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                 </a>
                               </div>
                            </div>
                            <div class="col-sm-3" style="display: none;">
                                <div class="form-group">
                                    <label>Start Date</label>
                                    <input type="text" name="start_date" class="form-control datepicker" value="{{isset($filter['course_start_date']) ? !empty($filter['course_start_date']) ? date('d-m-Y',strtotime($filter['course_start_date'])) : '' : ''}}" placeholder="dd-mm-yyyy">
                                </div>
                            </div>
                            <div class="col-sm-1">
                                <div class="section_cta text-right m-t-25 job-btn-container">
                                    <button type="button" class="btn coss-inverse-btn search-reset-btn" id="jobResetButton">Reset</button>
                                    <button style="display: none;" type="button" data-style="zoom-in" class="btn coss-primary-btn search-post-btn ladda-button candidate-filter-search-button" id="candidate-filter-search-button"><i class="fa fa-search search-icon" aria-hidden="true"></i>Search</button>
                                </div>
                            </div>
                        </div>
                        <input type="hidden" id="display_type" name="display_type" value="list"> 
                    </form>
                </div>

                @if($filter['display_type'] == 'list')

                @if(isset($batch_data['data']) && !empty($batch_data['data']))

                <style type="text/css">
                    .main{
                        width: 90%;
                    }
                </style>
                <div class="section-2" id="filter-results">
                    <div class="row">
                        <div class="col-sm-4">
                            <span class="heading">
                                Results <br>
                            </span>
                        </div>
                        <div class="col-sm-8">
                            <span class="pagi pagi-up">
                                <span class="search-count">
                                    Showing {{$batch_data['from']}} - {{$batch_data['to']}} of {{$batch_data['total']}} Batches
                                </span>
                                <nav> 
                                <ul class="pagination pagination-sm">
                                    {!! $data->render() !!}
                                </ul> 
                                </nav>
                            </span>
                        </div>
                    </div>
                    <div class="row">
                    
                    @foreach($batch_data['data'] as $batch)
                        <div class="search-result-card1">
                            <div class="col-xs-12 col-sm-12 col-md-6">
                                <div class="batch-description">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="dropdown setting" style="float: right;">
                                            <a data-toggle="dropdown" class="btn-xs dropdown-toggle">
                                                <i class="fa fa-cog"></i>
                                            </a>
                                            <ul class="dropdown-menu dropdown-light pull-right">
                                                <li>
                                                    <a href="{{route('site.institute.course.batches.edit', $batch['id'])}}">
                                                        <span>Edit</span>
                                                    </a>
                                                </li>
                                                <?php 
                                                    $disable= 'hide';
                                                    $enable= 'hide';
                                                    if(isset($batch) && $batch['status'] == 0){
                                                        $enable = '';
                                                    }else{
                                                        $disable = '';
                                                    }
                                                ?>
                                                <li>
                                                    <a class="batchDisableButton disable-{{$batch['id']}} {{$disable}}" data-status='disable' data-id="{{$batch['id']}}">
                                                        <span>Deactive</span>
                                                    </a>
                                                </li>
                                                
                                                <li>
                                                    <a class="batchDisableButton enable-{{$batch['id']}} {{$enable}}" data-status='enable' data-id="{{$batch['id']}}">
                                                        <span>Active</span>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a class="" onclick="shiftBatch('{{$batch['id']}}')">
                                                        <span>Shift Batch</span>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                            <div class="other-discription">
                                               <strong>Course Type: </strong>
                                                <span class="ans">
                                                    @foreach(\CommonHelper::institute_course_types() as $r_index => $rank)
                                                        {{ !empty($batch['course_details']['course_type']) ? $batch['course_details']['course_type'] == $r_index ? $rank : '' : ''}}
                                                    @endforeach
                                                </span>
                                            </div>
                                            <?php
                                                $courses = \App\Courses::all()->toArray();
                                                $Select = 'select';
                                            ?>
                                            <div class="other-discription">
                                               <strong> Course Name: </strong>
                                                <span class="ans">
                                                    @foreach($courses as $index => $course)
                                                        {{ !empty($batch['course_details']['course_id']) ? $batch['course_details']['course_id'] == $course['id'] ? $course['course_name'] : '' : ''}}
                                                    @endforeach
                                                </span>
                                            </div>
                                            <div class="details">
                                                <div class="col-sm-6">
                                                    <div class="other-discription p-l-0 p-r-0">
                                                        <strong> Start Date: </strong>
                                                        <span class="ans spanGreen">
                                                            {{!empty($batch['course_start_date']) ? date('d-m-Y',strtotime($batch['course_start_date'])) : '-'}}
                                                        </span>
                                                    </div>
                                                    <div class="other-discription p-l-0 p-r-0">
                                                        <strong> Duration: </strong>
                                                        <span class="ans">
                                                            {{!empty($batch['duration']) ? $batch['duration'] : '-'}} Days
                                                        </span>
                                                    </div>
                                                    <div class="other-discription p-l-0 p-r-0">
                                                        <strong> Batch Size: </strong>
                                                        <span class="ans">
                                                            {{!empty($batch['size']) ? $batch['size'] : '-'}}
                                                        </span>
                                                    </div>
                                                    <div class="other-discription p-l-0 p-r-0">
                                                        <strong> Batch Type: </strong>
                                                        <span class="ans">
                                                            {{!empty($batch['batch_type']) ? $batch['batch_type'] == 'on_demand' ? 'On Demand' : 'Confirmed' : '-'}}

                                                            <?php
                                                            
                                                            if($batch['batch_type'] == 'on_demand' && empty($batch['course_start_date']) ) {
                                                                echo "<span class='sub-date'> ( " . date('M y', strtotime($batch['start_date'])) . ")</span>";
                                                            }
                                                            ?>
                                                        </span>
                                                    </div>
                                                    <div class="other-discription p-l-0 p-r-0">
                                                        <strong> Status: </strong>
                                                        <span class="ans">
                                                            <span class="disable-{{$batch['id']}} {{$disable}}" data-status='disable' data-id="{{$batch['id']}}">
                                                                Active
                                                            </span>
                                                            <span class="enable-{{$batch['id']}} {{$enable}}" data-status='enable' data-id="{{$batch['id']}}">
                                                                Deactive
                                                            </span>
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="other-discription p-l-0 p-r-0">
                                                        <strong> Cost: </strong>
                                                        <span class="ans">
                                                            <i class="fa fa-inr"></i>
                                                            {{!empty($batch['cost']) ? $batch['cost'] : '-'}}
                                                        </span>
                                                    </div>
                                                    <div class="other-discription p-l-0 p-r-0">
                                                        <strong> Payment Type: </strong>
                                                        <span class="ans">
                                                            {{!empty($batch['payment_type']) ? ucwords($batch['payment_type'])." Payment" : '-'}}
                                                        </span>
                                                    </div>
                                                    <div class="other-discription p-l-0 p-r-0">
                                                        <strong> Reserved Seats: </strong>
                                                        <span class="ans">
                                                            {{!empty($batch['reserved_seats']) ? $batch['reserved_seats'] : '0'}}
                                                        </span>
                                                    </div>
                                                    <!-- <div class="other-discription p-l-0 p-r-0">
                                                        Batch Type On Demand: 
                                                        <span class="ans">
                                                            @if(isset($batch['on_demand_batch_type']) && !empty($batch['on_demand_batch_type']))
                                                                {{ isset($batch['on_demand_batch_type']) ? $batch['on_demand_batch_type'] == '1' ? 'Weekly Course' : '' : ''}}
                                                                {{ isset($batch['on_demand_batch_type']) ? $batch['on_demand_batch_type'] == '2' ? 'Bi-Monthly Course' : '' : ''}}
                                                                {{ isset($batch['on_demand_batch_type']) ? $batch['on_demand_batch_type'] == '3' ? 'Monthly Course' : '' : ''}}
                                                            @else
                                                                <br>-
                                                            @endif
                                                        </span>
                                                    </div> -->
                                                </div>

                                                <?php
                                                    $location_ids = '';
                                                    $locations = '';
                                                    $comma = 0;
                                                ?>

                                                <div class="col-sm-12">
                                                    <div class="other-discription p-l-0 p-r-0">
                                                       <strong>  Location: </strong>
                                                        <span class="ans">
                                                            @if(isset($batch['batch_location']))
                                                                @foreach($batch['batch_location'] as $index => $data1)
                                                                    @if($data1['location']['country'] == '95')
                                                                        @if($comma == 1)
                                                                        ,
                                                                        @endif

                                                                        {{$data1['location']['city']['name']}} - {{ucfirst(strtolower($data1['location']['state']['name']))}}
                                                                        <?php $comma = 1 ?>
                                                                    @endif
                                                                @endforeach
                                                            @endif
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                    </div>
                    
                    <span class="pagi m-t-25">
                        <span class="search-count">
                            Showing {{$batch_data['from']}} - {{$batch_data['to']}} of {{$batch_data['total']}} Batches
                        </span>
                        <nav> 
                        <ul class="pagination pagination-sm">
                            {!! $data->render() !!}
                        </ul> 
                        </nav>
                    </span>

                @else
                    <div class="section-2" id="filter-results"><div class="row no-results-found">No results found. Try again with different search criteria.</div></div>
                @endif

                @else

                <style type="text/css">
                    .main{
                        width: 75%;
                    }
                </style>

                <?php
                    $batches_dates_array = array();

                    //print_r($batch_data); exit;

                    if(!empty($batch_data)) {
                        $unique_dates = array();
                        $u_key = 0;
                        foreach ($batch_data as $bd_key => $bd_value) {
                            if(!empty($bd_value['course_start_date'])) {
                                if(!in_array($bd_value['course_start_date'], $unique_dates)) {
                                    $batches_dates_array[$u_key]['batch_id'] = $bd_value['batch_location'][0]['batch_id'];
                                    $batches_dates_array[$u_key]['start_date'] = $bd_value['course_start_date'];
                                    $unique_dates[]= $bd_value['course_start_date'];
                                    $u_key++;
                                }
                            }
                        }
                     }
                     
                ?>

                <div class="section-2" id="filter-results">
                    <div class="row">
                        <div class="col-sm-4">
                            <span class="heading">
                                Results <br>
                            </span>
                        </div>
                     </div>
                </div>        

                <?php 

                for ($no_of_month_data=0; $no_of_month_data < 12; $no_of_month_data++) { 
                ?>
                <div class="col-sm-12 col-md-4 col-xs-4 custom-month-calender">
                  <section id="content" class="planner">  
                    <h5><?php echo date("F Y", strtotime("+$no_of_month_data month")) ?></h5>
                    <table class="month table">
                        <tr class="days">
                            <td>Mon</td>
                            <td>Tue</td>
                            <td>Wed</td>
                            <td>Thu</td>
                            <td>Fri</td>
                            <td>Sat</td>
                            <td>Sun</td>
                        </tr>
                        <?php 
                            $class = '';
                            $istodayCurrentMonth = date("Y-m-d");
        
                            $today = date("d", strtotime("+$no_of_month_data month")); // Current day
                            $month = date("m", strtotime("+$no_of_month_data month")); // Current month
                            $year = date("Y", strtotime("+$no_of_month_data month")); // Current year
                            $days = cal_days_in_month(CAL_GREGORIAN,$month,$year); // Days in current month
                            
                            $lastmonth = date("t", mktime(0,0,0,$month-1,1,$year)); // Days in previous month
                            
                            $start = date("N", mktime(0,0,0,$month,1,$year)); // Starting day of current month
                            $finish = date("N", mktime(0,0,0,$month,$days,$year)); // Finishing day of  current month
                            $laststart = $start - 1; // Days of previous month in calander
                            
                            $counter = 1;
                            $nextMonthCounter = 1;
                            
                            if($start > 5){ $rows = 6; }else {$rows = 5; }

                            for($i = 1; $i <= $rows; $i++){
                                echo '<tr class="week">';
                                for($x = 1; $x <= 7; $x++){             
                                    
                                    if(($counter - $start) < 0){
                                        $date = (($lastmonth - $laststart) + $counter);
                                        $class = 'class="blur"';
                                    }else if(($counter - $start) >= $days){
                                        $date = ($nextMonthCounter);
                                        $nextMonthCounter++;
                                        
                                        $class = 'class="blur"';
                                            
                                    }else {

                                        $date = ($counter - $start + 1);
                                        
                                        if($today == $counter - $start + 1){

                                            if($istodayCurrentMonth == "$year-$month-$today") {
                                                $class = 'class="today "';
                                            }
                                        }else {

                                            $match_date = date('Y-m-d',strtotime("$year-$month-$date"));
                                            
                                            
                                            $get_event_id = array_keys(array_column($batches_dates_array, 'start_date'), $match_date);
                                            
                                            if(!empty($get_event_id)) {
                                                // print_r($get_event_id[0]); //exit;
                                                // print_r($batches_dates_array[$get_event_id[0]]['start_date']); exit;
                                                if($batches_dates_array[$get_event_id[0]]['start_date'] == $match_date ) {
                                                    $class = 'class="event" onclick="getEventDetails('.$batches_dates_array[$get_event_id[0]]['batch_id'].')"';
                                                }
                                            }
                                        }

                                        /*$date = ($counter - $start + 1);
                                        if($today == $counter - $start + 1){
                                            $class = 'class="today"';
                                        }*/
                                    }
                                        
                                    
                                    echo '<td '.$class.'><a class="date">'. $date . '</a></td>';
                                
                                    $counter++;
                                    $class = '';
                                }
                                echo '</tr>';
                            }
                            
                        ?>

                     </table>   
                  </section>
                </div>
                <?php } ?>
                @endif

                </div>
            </div>
        </main>
    </div>
</div>

<div class="modal fade" id="candidate-search-filter-modal" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title text-center">Batch search filter</h4>
        </div>
        <div class="modal-body">
            <form id="candidate-filter-form" action={{route('site.institute.batch.listing')}}>
                <div class="row">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label>Course Type </label>
                            <select name="course_type" class="form-control course_type">
                                <option value=>Select Course Type</option>
                                @foreach(\CommonHelper::institute_course_types() as $r_index => $rank)
                                    <option value="{{$r_index}}" {{ isset($filter['course_type']) ? $filter['course_type'] == $r_index ? 'selected' : '' : ''}}>{{$rank}} </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label>Course Name</label>
                            <select class="form-control institute_course_name" name="course_name">
                                <option value=''>Select Course Name</option>
                                @if(isset($course_name) && !empty($course_name))
                                    @foreach($course_name as $r_index => $name)
                                        <option value="{{$r_index}}" {{ isset($filter['course_name']) ? $filter['course_name'] == $name['id'] ? 'selected' : '' : ''}}>{{$name['course_name']}} </option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label>Start Date</label>
                            <input type="text" name="start_date" class="form-control datepicker" value="{{isset($filter['start_date']) ? !empty($filter['start_date']) ? date('d-m-Y',strtotime($filter['start_date'])) : '' : ''}}" placeholder="dd-mm-yyyy">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="section_cta text-right m_t_25 job-btn-container">
                            <button type="button" class="btn coss-inverse-btn search-reset-btn" id="jobResetButton">Reset</button>
                            <button type="button" data-style="zoom-in" class="btn coss-primary-btn search-post-btn ladda-button candidate-filter-search-button" id="candidate-filter-search-button"><i class="fa fa-search search-icon" aria-hidden="true"></i>Search</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
    </div>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
<div class="modal-dialog">

<!-- Modal content-->
<div class="modal-content">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <h4 class="modal-title" id="course_title_popup"></h4>
  </div>
  <div class="modal-body" id="insertDetails"></div>
  <div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
  </div>
</div>

</div>
</div>


    <!-- Modal -->
  <div id="myModalshift" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Shift Batch</h4>
      </div>
      <div class="modal-body">
          <div>
                <label>Batch Date :</label>
                <span id="batch_date"></span>
          </div>
          <div>
                <label>Course Name :</label>
                <span id="course_name"></span>
          </div>
          <div>
                <label>Batch Size :</label>
                <span id="batch_size"></span>
          </div>
          <div>
                <label>Reserved Seats :</label>
                <span id="reserved_seats"></span>
          </div>

          <div>
              <label>Student Batch Details :</label>
              <span>No Student Found For this batch</span>
          </div>
          <div>
            <br>
              <button>Add Students</button>
              <br><br>
          </div>
          <div>
              <label>Select New Batch Date :</label>
              <br>
              <div id="datepicker" style="text-align: center;"></div>
              <br>
          </div>

          <div>
            <label>Reserved Seats :</label>
            <span>0</span>
          </div>
          <div>
              <label>Reserve Seats After Shift :</label>
              <span>0</span>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">SHIFT BATCH</button>
      </div>
    </div>

      </div>
    </div>

<script type="application/javascript">
    $(document).on('ready',function () {
        $( "#datepicker" ).datepicker();
        $('.switch-checkbox').checkboxpicker();

        var History = window.History;
        var state = History.getState(),
            $log = $("#log");

        History.pushState(null,null,state.url);

        History.Adapter.bind(window, 'statechange', function(){
            var state = History.getState();
            fetchCandidateResult(state.url);

            var temp_current_state_url = state.url.split('?');
            if(temp_current_state_url.length > 1 && temp_current_state_url[1] != ''){
                var parameters = temp_current_state_url[1].split('&');

                var dropdown_elements = ['course_type','course_name'];

                for(var i=0; i<parameters.length; i++){

                    var param_array = parameters[i].split('=');
                    if($.inArray(param_array[0],dropdown_elements) > -1){
                        $('select[name="'+param_array[0]+'"]').val(param_array[1]);
                    }else if($.inArray(param_array[0],['start_date']) > -1){
                        $('input[name="'+param_array[0]+'"]').val(param_array[1]);
                    }
                }
            } else {
                $(':input','#candidate-filter-form')
                    .not(':button, :submit, :reset, :hidden')
                    .val('')
                    .removeAttr('checked')
                    .removeAttr('selected');
                }
        });
    });


    function shiftBatch(batch_id) {
        

        $.ajax({
            url: "{{url('institute/getdetailsByBatchId')}}/"+batch_id,
            type: "GET",
            contentType: "application/json",
            success: function(datas, textStatus, jqXHR){
                console.log(datas);

                $('#batch_date').html(datas.data[0]['course_start_date']);

                $('#course_name').html(datas.data[0]['course_details']['course_name']);

                $('#batch_size').html(datas.data[0]['course_details']['size']);

                $('#reserved_seats').html(datas.data[0]['course_details']['reserved_seats']);

                $('#myModalshift').modal('show'); 
            }
        });

    }
    
</script>
@stop

@section('js_script')
    <script type="text/javascript" src="/js/site/institute-registration.js"></script>
    <script type="text/javascript" src="/js/site/dashboard.js"></script>
    <script type="text/javascript">

        $(document).on('ready',function () {

            $('.institute_course_name').on('change', function() {
                // $('#candidate-filter-form').submit();
            });

        });

        function getEventDetails(batchId)  {

                     $('#insertDetails').html('');
                     $.ajax({
                        url: "{{url('institute/getdetailsByBatchId')}}/"+batchId,
                        type: "GET",
                        contentType: "application/json",
                        success: function(datas, textStatus, jqXHR){

                              
                              let html = '<table class="table table-striped">';

                                                                 
                              html += '<tr><td>Course Type</td><td>'+datas.data[0]['course_details']['course_type']+'</td></tr>';

                              html += '<tr><td>Course Name</td><td>'+datas.data[0]['course_details']['course_name']+'</td></tr>';

                              html += '<tr><td>Start Date</td><td>'+datas.data[0]['course_start_date']+'</td></tr>';

                              html += '<tr><td>Duration</td><td>'+datas.data[0]['duration']+' days</td></tr>';

                              html += '<tr><td>Batch Size</td><td>'+datas.data[0]['size']+'</td></tr>';

                              html += '<tr><td>Batch Type</td><td>'+datas.data[0]['batch_type']+'</td></tr>';

                              html += '<tr><td>Reserved Seats</td><td>'+datas.data[0]['reserved_seats']+'</td></tr>';

                              html += '<tr><td>Status</td><td>'+datas.data[0]['status']+'</td></tr>';

                              html += '<tr><td>Cost</td><td>'+datas.data[0]['cost']+'</td></tr>';

                              html += '<tr><td>Payment Type</td><td>'+datas.data[0]['payment_type']+'</td></tr>';
                             
                              html += '</table>';
                              

                              $('#insertDetails').html(html);
                              $("#myModal").modal();
                        },
                        error: function (jqXHR, textStatus, errorThrown){
                             console.log('Error ' + jqXHR);
                        }
                    });
        }
    </script>
@stop