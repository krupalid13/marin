<!-- Old design -->
@extends('site.index')
@section('content')
    <div class="user-profile content-section-wrapper sm-filter-space">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-2" id="side-navbar">
                    @include('site.partials.side_navbar_institute')
                </div>
                <div class="col-md-10" id="main-data">

                    <div class="row">
                        
                        @if(isset($data[0]['mobile']) && strlen($data[0]['mobile']) == 10)
                        <div class="col-sm-12 col-md-6">
                            <div class="alert alert-block alert-danger fade in alert-box-verification-mob hide">
                                Your mobile is not verified. Please verify your mobile. <a class="alert-link" href="#" id="resend-otp-button">Verify</a>
                                
                            </div>
                        </div>
                        @endif
                        <div class="col-sm-12 col-md-6">
                            <div class="alert alert-block alert-danger fade in alert-box-verification-email hide">
                                Your email is not verified. Please verify your email. <a class="alert-link" href="#" id="email_verify">Resend Email</a>
                                <i class="fa fa-spin fa-refresh resend_email_loader hide"></i>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-12">
                            <div class="company-page-profile-heading">
                                My profile
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-12">
                            <div class="section-1">
                            
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="section-2 user-imp-details">
                                <input type="hidden" name="verify_email" id="verify_email" value="{{isset($data[0]['is_email_verified']) ? $data[0]['is_email_verified'] : ''}}">
                                <input type="hidden" name="verify_mobile" id="verify_mobile" value="{{isset($data[0]['is_mob_verified']) ? $data[0]['is_mob_verified'] : ''}}">
                                <div class="row">
                                    <div class="col-sm-12 col-md-4 col-lg-3">
                                        <div class="photo-container">
                                            <div class="photo-card">
                                                <div>
                                                    <div class="text-center">
                                                        @if($data[0]['profile_pic'])
                                                            <img id="preview" src="/{{ env('INSTITUTE_LOGO_PATH')}}{{$data[0]['id']}}/{{$data[0]['profile_pic']}}" avatar-holder-src="{{ asset('images/user_default_image.png') }}">
                                                        @else
                                                            <img id="preview" src="{{ asset('images/user_default_image.png') }}" avatar-holder-src="{{ asset('images/user_default_image.png') }}">
                                                        @endif
                                                    </div>
                                                    <div class="user-name">
                                                        {{ $data[0]['institute_registration_detail']['institute_name'] }}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-10 col-sm-11 col-md-6 col-lg-7">
                                        <div class="imp-details-container">
                                            <div class="detail">
                                                Institute Email: {{ $data[0]['institute_registration_detail']['email'] }}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-2 col-sm-1 col-md-2 col-lg-2">
                                        <div class="edit-btn-container">
                                            <a href="{{ route('site.edit.institute.details') }}"><span class="profile-edit-btn-sm visible-xs-block visible-sm-block"><i class="fa fa-pencil-square-o" aria-hidden="true" title="Edit profile"></i></span>
                                                <button class="profile-edit-btn hidden-xs hidden-sm">
                                                    Edit Profile
                                                </button>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-12">
                            <div class="section-3">
                                <div class="row">
                                    <div class="col-sm-12 col-md-4 col-lg-3"></div>
                                    <div class="col-sm-12 col-md-8 col-lg-9">
                                        <div class="sub-details-container">
                                            <input type="hidden" name="verify_email" id="verify_email" value="{{isset($data[0]['is_email_verified']) ? $data[0]['is_email_verified'] : ''}}">
                                            <input type="hidden" name="verify_mobile" id="verify_mobile" value="{{isset($data[0]['is_mob_verified']) ? $data[0]['is_mob_verified'] : ''}}">
                                            <div class="content-container" style="height:158px">
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <div class="title">INSTITUTE DETAILS</div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <div class="discription">
                                                            <span class="content-head">Name :</span>
                                                            <span class="content">{{ isset($data[0]['institute_registration_detail']['institute_name']) ? $data[0]['institute_registration_detail']['institute_name'] : '-'}}</span>
                                                        </div>
                                                        <div class="discription">
                                                            <span class="content-head">Description :</span>
                                                            <span class="content">{{$data[0]['institute_registration_detail']['institute_detail']['institute_description']}}</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="details-main-heading">
                                            More Details
                                        </div>
                                        <div class="sub-details-container">
                                            <div class="content-container">
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <div class="title">CONTACT INFORMATION</div>
                                                    </div>

                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-6 col-md-4">
                                                        <div class="discription">
                                                            <span class="content-head">Phone Number:</span>
                                                            <span class="content">{{ $data[0]['institute_registration_detail']['institute_detail']['institute_contact_number'] }}</span>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-6 col-md-4">
                                                        <div class="discription">
                                                            <span class="content-head">Website:</span>
                                                            <span class="content"><a href="https://{{ isset($data[0]['institute_registration_detail']['website']) ? $data[0]['institute_registration_detail']['website'] : '' }}" target="_blank">{{ isset($data[0]['institute_registration_detail']['website']) && !empty($data[0]['institute_registration_detail']['website']) ? $data[0]['institute_registration_detail']['website'] : '-' }}</a></span>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-6 col-md-4">
                                                        <div class="discription">
                                                            <span class="content-head">Fax:</span>
                                                            <span class="content">{{ isset($data[0]['institute_registration_detail']['institute_detail']['fax']) ? $data[0]['institute_registration_detail']['institute_detail']['fax'] : '-'}}</span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-6 col-md-4">
                                                        <div class="discription">
                                                            <span class="content-head">Contact Person:</span>
                                                            <span class="content">{{ isset($data[0]['institute_registration_detail']['contact_person']) ? $data[0]['institute_registration_detail']['contact_person'] : '' }}</span>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-6 col-md-4">
                                                        <div class="discription">
                                                            <span class="content-head">Contact Person Number:</span>
                                                            <span class="content">{{ isset($data[0]['institute_registration_detail']['contact_number']) ? $data[0]['institute_registration_detail']['contact_number'] : '' }}</span>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-6 col-md-4">
                                                        <div class="discription">
                                                            <span class="content-head">Contact Person Designation:</span>
                                                            <span class="content">{{ isset($data[0]['institute_registration_detail']['designation']) ? $data[0]['institute_registration_detail']['designation'] : '' }}</span>
                                                        </div>
                                                    </div>
                                                    <!-- <div class="col-xs-12 col-sm-6 col-md-4">
                                                        <div class="discription">
                                                            <span class="content-head">Contact Person Email ID:</span>
                                                            <span class="content">{{ $data[0]['institute_registration_detail']['contact_email'] }}</span>
                                                        </div>
                                                    </div> -->
                                                </div>
                                            </div>
                                        </div>
                                        <div class="sub-details-container">
                                            <div class="content-container">
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <div class="title">LOCATION DETAILS</div>
                                                    </div>
                                                </div>
                                                @if(isset($data[0]['institute_registration_detail']['institute_locations']) && !empty($data[0]['institute_registration_detail']['institute_locations']))
                                                    @foreach ($data[0]['institute_registration_detail']['institute_locations'] as $index => $institute_location)
                                                        <div class="row" style="padding-top: 5px; padding-bottom: 5px">
                                                            <div class="col-xs-12 col-sm-6 col-md-4">
                                                                <div class="discription" style="font-size: 17px">
                                                                    <span class="content-head">Location {{ $index+1 }} :</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-xs-12 col-sm-6 col-md-4">
                                                                <div class="discription">
                                                                    <span class="content-head">Country:</span>
                                                                    <span class="content">
                                                                        @foreach( \CommonHelper::countries() as $c_index => $country)
                                                                            {{ $institute_location['country'] == $c_index ? $country : '' }}
                                                                        @endforeach
                                                                    </span>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-6 col-md-4">
                                                                <div class="discription">
                                                                    <span class="content-head">State:</span>
                                                                    <span class="content">
                                                                        {{ isset($institute_location['state_id']) ? $institute_location['pincode']['pincodes_states'][0]['state']['name'] : $institute_location['state_text']}}
                                                                    </span>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-6 col-md-4">
                                                                <div class="discription">
                                                                    <span class="content-head">City:</span>
                                                                    <span class="content">
                                                                        {{ isset($institute_location['city_id']) ? $institute_location['pincode']['pincodes_cities'][0]['city']['name'] : $institute_location['city_text']}}
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-xs-12 col-sm-6 col-md-4">
                                                                <div class="discription">
                                                                    <span class="content-head">Pincode:</span>
                                                                    <span class="content">
                                                                        {{ isset($institute_location['pincode_text']) ? $institute_location['pincode_text'] : ''}}
                                                                    </span>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-6 col-md-4">
                                                                <div class="discription">
                                                                    <span class="content-head">Branch Type:</span>
                                                                    <span class="content">
                                                                        @foreach( \CommonHelper::institute_branch_type() as $b_index => $branch_type)
                                                                           {{ isset($institute_location['branch_type']) ? $institute_location['branch_type'] == $b_index ? $branch_type : '' : ''}}
                                                                        @endforeach
                                                                    </span>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-6 col-md-4">
                                                                <div class="discription">
                                                                    <span class="content-head">Is Head Branch:</span>
                                                                    <span class="content">
                                                                        {{ !empty($institute_location['headbranch']) && $institute_location['headbranch'] == '1' ? 'Yes' : 'No' }}

                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-xs-12 col-sm-6 col-md-8">
                                                                <div class="discription">
                                                                    <span class="content-head">Address:</span>
                                                                    <span class="content">{{ $institute_location['address'] }}</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    <hr>
                                                    @endforeach
                                                @else
                                                    <div class="row">
                                                        <div class="col-xs-12 text-center">
                                                            <div class="discription">
                                                                <span class="content-head">No Data Found</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>    
                </div>
            </div>
        </div>
    </div>
@stop
