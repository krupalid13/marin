@extends('site.index')

@section('content')
<div class="section user-job-post content-section-wrapper sm-filter-space dashboard-body">
        @include('site.partials.sidebar')
        
        <main class="main" style="padding: 20px 20px 80px 20px !important;">
            <div class="row">
                <div class="col-xs-12">
                    <div class="company-page-profile-heading">
                        Add Course Batches
                    </div>
                </div>
            </div>
            
            <form id="add-institute-batch" method="post" action="{{ isset($data['current_route']) ? route('site.institute.update.course.batches',$data['id']) : route('site.institute.store.course.batches') }}">
                {{ csrf_field() }}
                <div class="job-post">
                    <div class="job-discription-card">
                        <div class="col-sm-12">
                            <div class="heading">
                               COURSE DETAILS
                            </div>
                        </div>
                        <div class="row">
                            <input type="hidden" id="institute_id" value="{{$location[0]['institute_registration_detail']['id']}}">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="input-label" for="course_type">Course Type<span class="symbol required"></label>
                                    <select id="course_type" name="course_type" class="form-control">
                                        <option value=''>Select Course Type</option>
                                        @if(isset($course_type) && !empty($course_type))
                                            @foreach($course_type as $index => $category)
                                                <option value="{{$index}}" {{ isset($data['course_details']['course_type']) ? $data['course_details']['course_type'] == $index ? 'selected' : '' : ''}}>{{$category}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                 </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="input-label">Course Name<span class="symbol required"></label>
                                    <select name="course_name" class="form-control institute_course_name">
                                        <option value="">Select Course Name</option>
                                        @if(isset($data['courses']) && !empty($data['courses']))
                                            @foreach($data['courses'] as $index => $category)
                                                <option value="{{$category['id']}}" {{ isset($data['course_details']['course_type']) ? $data['course_details']['id'] == $category['id'] ? 'selected' : '' : ''}}>{{$category['course_details']['course_name']}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="heading">
                               BATCH DETAILS
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div id="future">
                                    @if(isset($data['institute_batches']) AND !empty($data['institute_batches']))
                                        @foreach($data['institute_batches'] as $key => $data)
                                            <input type="hidden" class="total_block_index" value={{$key}}>
                                            <div class="batch-template" id="batch-template">
                                                <input type="hidden" name="batch_id[{{$key}}]" value={{$data['id']}}>
                                                <div class="col-xs-12">
                                                    <div class="input-label batch-name pull-left heading">Batch {{$key+1}}</div>
                                                    @if($key > 0)
                                                        <div class="pull-right batch-close-button close-button-0 p-t-10 heading">
                                                            <i class="fa fa-times" aria-hidden="true"></i>
                                                        </div>
                                                    @else
                                                        <div class="pull-right batch-close-button hide close-button-0 p-t-10 heading">
                                                            <i class="fa fa-times" aria-hidden="true"></i>
                                                        </div>
                                                    @endif
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-4">
                                                        <div class="form-group">
                                                            <label class="input-label" for="ship">Start Date<span class="symbol required"></label>
                                                            <i class="fa fa-calendar calendar-icon m-t-15" aria-hidden="true"></i>
                                                            <input type="text" class="form-control batch-datepicker start_date" name="start_date[{{$key}}]" value="{{ isset($data['start_date']) ? date('d-m-Y',strtotime($data['start_date'])) : '' }}" placeholder="dd-mm-yyyy">
                                                         </div>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <div class="form-group">
                                                            <label class="input-label">Duration (Days)<span class="symbol required"></label>
                                                            <input type="text" class="form-control duration" name="duration[{{$key}}]" value="{{ isset($data['duration']) ? $data['duration'] : '' }}" placeholder="Type your duration in days">
                                                        </div>
                                                    </div>
                                                
                                                    <div class="col-sm-4">
                                                        <div class="form-group">
                                                            <label class="input-label" for="cost">Cost<span class="symbol required"></label>
                                                            <input type="text" class="form-control cost" placeholder="Type your cost" name="cost[{{$key}}]" value="{{ isset($data['cost']) ? $data['cost'] : '' }}" data-id="{{$key}}">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-4">
                                                        <div class="form-group">
                                                            <label class="input-label" for="size">Batch Size<span class="symbol required"></label>
                                                            <input type="text" class="form-control size" placeholder="Type your batch size" name="size[{{$key}}]" value="{{ isset($data['size']) ? $data['size'] : '' }}">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <div class="form-group">
                                                            <label class="input-label" for="reserved_seats">Reserved Seats<span class="symbol required"></label>
                                                            <input type="text" class="form-control reserved_seats" placeholder="Type your reserved seats" name="reserved_seats[{{$key}}]" value="{{ isset($data['reserved_seats']) ? $data['reserved_seats'] : '' }}">
                                                        </div>
                                                    </div>
                                                </div>

                                                <?php
                                                    $split = '';
                                                    $single = '';
                                                    $split_show = 'hide';
                                                    if(isset($data['payment_type']) && $data['payment_type'] == 'split'){
                                                        $split = 'checked';
                                                        $split_show = '';
                                                    }else{
                                                        $single = 'checked';
                                                    }
                                                ?>
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label class="input-label" for="payment_type">Booking Payment Type<span class="symbol required"></label>
                                                            <label class="radio-inline">
                                                                <input type="radio" class="payment_type" {{ !empty($single) ? 'checked=checked' : ''}} name="payment_type[{{$key}}]" data-id="{{$key}}" value="single" >Single Payment</input>
                                                            </label>
                                                            <label class="radio-inline">
                                                               <input type="radio" class="payment_type" {{ !empty($split) ? 'checked=checked' : ''}} name="payment_type[{{$key}}]" data-id="{{$key}}" value="split" >Split Payment</input>
                                                            <label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6 payment_details payment_details_{{$key}} {{$split_show}}">
                                                        <div class="form-group">
                                                            <div class="row">
                                                                <div class="col-sm-4">
                                                                    <label class="input-label m-t-15">Initial Payment<span class="symbol required"></label>
                                                                </div>
                                                                <div class="col-sm-4">
                                                                    <div class="d-flex-center">
                                                                        <input type="text" class="form-control per_amt initial_per" data-type="initial" placeholder="Type payment in %" name="initial_per[{{$key}}]" value="{{ isset($data['initial_per']) ? $data['initial_per'] : '' }}" data-id="{{$key}}">
                                                                        %
                                                                    </div>
                                                                    <label class="label-ip" for="initial_per[{{$key}}]" class="error"></label>
                                                                </div>
                                                                <div class="col-sm-4">
                                                                    <div class="d-flex-center">
                                                                        <input type="text" class="form-control initial_amt" name="initial_amt[{{$key}}]" value="{{ isset($data['initial_amt']) ? $data['initial_amt'] : '' }}" readonly="readonly">
                                                                        <i class="fa fa-inr" aria-hidden="true"></i>
                                                                    </div>
                                                                    <label for="initial_amt[{{$key}}]" class="error"></label>
                                                                </div>
                                                            </div>
                                                            
                                                            <div class="row">
                                                                <div class="col-sm-4">
                                                                    <label class="input-label m-t-15">Pending Payment<span class="symbol required"></label>
                                                                </div>
                                                                <div class="col-sm-4">
                                                                    <div class="d-flex-center">
                                                                        <input type="text" class="form-control per_amt pending_per" data-type="pending" placeholder="Type payment in %" name="pending_per[{{$key}}]" value="{{ isset($data['pending_per']) ? $data['pending_per'] : '' }}" data-id="{{$key}}">
                                                                        %
                                                                    </div>
                                                                    <label for="pending_per[{{$key}}]" class="error"></label>
                                                                </div>
                                                                <div class="col-sm-4">
                                                                    <div class="d-flex-center">
                                                                        <input type="text" class="form-control pending_amt" name="pending_amt[{{$key}}]" value="{{ isset($data['pending_amt']) ? $data['pending_amt'] : '' }}" readonly="readonly">
                                                                        <i class="fa fa-inr" aria-hidden="true"></i>
                                                                    </div>
                                                                    <label for="pending_amt[{{$key}}]" class="error"></label>
                                                                </div>
                                                              
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label class="input-label" for="batch_type_on_demand">Batch Type<span class="symbol required"></label>
                                                            <label class="radio-inline">
                                                                <input type="radio" class="batch_type_on_demand" {{ !empty($single) ? 'checked=checked' : ''}} name="batch_type_on_demand[{{$key}}]" data-id="{{$key}}" value="confirmed">Confirmed</input>
                                                            </label>
                                                            <label class="radio-inline">
                                                               <input type="radio" class="batch_type_on_demand" {{ !empty($split) ? 'checked=checked' : ''}} name="batch_type_on_demand[{{$key}}]" data-id="{{$key}}" value="on_demand">On Demand</input>
                                                            <label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6 batch_type_details bactch_type_details_{{$key}}">
                                                        <div class="form-group">
                                                            <label class="input-label" for="batch_type">Batch Type On Demand<span class="symbol required"></label>

                                                            <select name="batch_type" class="form-control search-select batch_type" block-index="0">
                                                                <option value="">Select Batch Type</option>
                                                                <option value="1">Weekly</option>
                                                                <option value="2">Bi-Monthly</option>
                                                                <option value="3">Monthly</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row hide">
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label class="input-label" for="batch_type">Early Bird Discount<span class="symbol required"></label>
                                                            <select name="discount" class="form-control search-select discount" block-index="0">
                                                                <option value="">Select Days</option>
                                                                @for($i=7;$i<=30;$i++)
                                                                    <option value="{{$i}}">{{$i}}</option>
                                                                @endfor
                                                            </select>
                                                        </div>
                                                    </div>
                                                   <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label class="input-label" for="discount_per">Discount<span class="symbol required"></label>
                                                            <div class="d-flex-center">
                                                                <input type="text" class="form-control 3" data-type="pending" placeholder="Type payment in %" name="discount_per[{{$key}}]" value="{{ isset($data['pending_per']) ? $data['pending_per'] : '' }}" data-id="{{$key}}">
                                                                %
                                                            </div>
                                                        </div>  
                                                    </div>
                                                </div>

                                                <?php
                                                    $location_ids = '';

                                                    if(isset($data['institute_location']) && !empty($data['institute_location'])){
                                                        $location_ids = array_values(collect($data['institute_location'])->pluck('location_id')->toArray());
                                                        $locations = $data['institute_location'];
                                                    }

                                                    //dd($data['institute_registration_detail']['institute_location'],$location_ids);
                                                ?>
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label class="input-label" for="location">Institute Location<span class="symbol required">
                                                            <select multiple="multiple" name="location[{{$key}}][]" class="form-control location select2-select" style="height: auto;" placeholder="Please select location">

                                                            @if(isset($location[0]['institute_registration_detail']['institute_locations']))
                                                                @foreach($location[0]['institute_registration_detail']['institute_locations'] as $index => $data)

                                                                    @if($data['country'] == '95')

                                                                        @if(!empty($location_ids) && in_array($data['id'],$location_ids))
                                                                            <option value="{{$data['id']}}" selected="selected">{{$data['city']['name']}}, {{$data['state']['name']}}</option>
                                                                            
                                                                        @else
                                                                            <option value="{{$data['id']}}">{{$data['city']['name']}}, {{$data['state']['name']}}</option>
                                                                        @endif

                                                                    @endif
                                                                @endforeach
                                                            @endif
                                                            </select>
                                                            <label class="location_error hide">Please select location</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <hr>
                                        @endforeach
                                    @else
                                        <div class="batch-template" id="batch-template">
                                            @if(isset($data['id']) && !empty($data['id']))
                                                <input class="batch_id" type="hidden" name="batch_id[0]" value={{ isset($data['id']) ? $data['id'] : ''}}>
                                            @endif
                                            <div class="col-xs-12">
                                                <div class="input-label batch-name pull-left heading">Batch 1</div>
                                                <div class="pull-right batch-close-button hide close-button-0 p-t-10 heading">
                                                    <i class="fa fa-times" aria-hidden="true"></i>
                                                </div>
                                            </div>
                                            <input type="hidden" class="total_block_index" value="0">
                                            <?php
                                                $on_demand = 'hide';
                                                $on_demand_type = '';
                                                $confirmed_type = '';
                                                if(isset($data['batch_type']) && $data['batch_type'] == 'on_demand'){
                                                    $on_demand_type = 'checked';
                                                    $on_demand = '';
                                                }else{
                                                    $confirmed_type = 'checked';
                                                }
                                                
                                            ?>

                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label class="input-label" for="batch_type">Batch Type<span class="symbol required"></label>
                                                        <label class="radio-inline">
                                                            <input type="radio" class="batch_type" checked="{{$confirmed_type}}" name="batch_type[0]" data-id="0" value="confirmed">Confirmed</input>
                                                        </label>
                                                        <label class="radio-inline">
                                                           <input type="radio" class="batch_type" name="batch_type[0]" {{ !empty($on_demand_type)? "checked=checked" : ''}} data-id="0" value="on_demand">On Demand</input>
                                                        <label>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6 batch_type_details batch_type_details_0 {{$on_demand}}">
                                                    <div class="form-group">
                                                        <label class="input-label" for="batch_type_on_demand">Batch Type On Demand<span class="symbol required"></label>

                                                        <select name="batch_type_on_demand[0]" class="form-control search-select batch_type_on_demand" block-index="0">
                                                            <option value="">Select Batch Type</option>
                                                            <option value="1" {{ isset($data['on_demand_batch_type']) ? $data['on_demand_batch_type'] == '1' ? 'selected' : '' : ''}} >Weekly Course</option>
                                                            <option value="2" {{ isset($data['on_demand_batch_type']) ? $data['on_demand_batch_type'] == '2' ? 'selected' : '' : ''}} >Bi-Monthly Course</option>
                                                            <option value="3" {{ isset($data['on_demand_batch_type']) ? $data['on_demand_batch_type'] == '3' ? 'selected' : '' : ''}} >Monthly Course</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <div class="form-group">
                                                        <label class="input-label" for="ship">Start Date<span class="symbol required"></label>
                                                        <i class="fa fa-calendar calendar-icon m-t-15" aria-hidden="true"></i>
                                                        <input type="text" class="form-control batch-datepicker start_date" name="start_date[0]" value="{{ isset($data['start_date']) ? date('d-m-Y',strtotime($data['start_date'])) : '' }}" placeholder="dd-mm-yyyy">
                                                     </div>
                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="form-group">
                                                        <label class="input-label">Duration (Days)<span class="symbol required"></label>
                                                        <input type="text" class="form-control duration" name="duration[0]" value="{{ isset($data['duration']) ? $data['duration'] : '' }}" placeholder="Type your duration in days">
                                                    </div>
                                                </div>
                                            
                                                <div class="col-sm-4">
                                                    <div class="form-group">
                                                        <label class="input-label" for="cost">Cost<span class="symbol required"></label>
                                                        <input type="text" class="form-control cost" placeholder="Type your cost" name="cost[0]" value="{{ isset($data['cost']) ? $data['cost'] : '' }}">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                 <div class="col-sm-4">
                                                    <div class="form-group">
                                                        <label class="input-label" for="size">Batch Size<span class="symbol required"></label>
                                                        <input type="text" class="form-control size" placeholder="Type your batch size" name="size[0]" value="{{ isset($data['size']) ? $data['size'] : '' }}">
                                                    </div>
                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="form-group">
                                                        <label class="input-label" for="reserved_seats">Reserved Seats<span class="symbol required"></label>
                                                        <input type="text" class="form-control reserved_seats" placeholder="Type your reserved seats" name="reserved_seats[0]" value="{{ isset($data['reserved_seats']) ? $data['reserved_seats'] : '' }}">
                                                    </div>
                                                </div>
                                            </div>
                                            <?php
                                                $split = '';
                                                $single = '';
                                                $split_show = 'hide';
                                                if(isset($data['payment_type']) && $data['payment_type'] == 'split'){
                                                    $split = 'checked';
                                                    $split_show = '';
                                                }else{
                                                    $single = 'checked';
                                                }
                                            ?>

                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label class="input-label" for="payment_type">Booking Payment Type<span class="symbol required"></label>
                                                        <label class="radio-inline">
                                                            <input type="radio" class="payment_type" {{ !empty($single) ? 'checked=checked' : ''}} name="payment_type[0]" data-id="0" value="single" >Single Payment</input>
                                                        </label>
                                                        <label class="radio-inline">
                                                           <input type="radio" class="payment_type" {{ !empty($split) ? 'checked=checked' : ''}} name="payment_type[0]" data-id="0" value="split" >Split Payment</input>
                                                        <label>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6 payment_details payment_details_0 {{$split_show}}">
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-sm-4">
                                                                <label class="input-label m-t-15">Initial Payment<span class="symbol required"></label>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <div class="d-flex-center">
                                                                    <input type="text" class="form-control per_amt initial_per" data-type="initial" placeholder="Type payment in %" name="initial_per[0]" value="{{ isset($data['initial_per']) ? $data['initial_per'] : '' }}" data-id="0">
                                                                    %
                                                                </div>
                                                                <label class="label-ip" for="initial_per[0]" class="error"></label>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <div class="d-flex-center">
                                                                    <input type="text" class="form-control initial_amt" name="initial_amt[0]" value="{{ isset($data['initial_amt']) ? $data['initial_amt'] : '' }}" readonly="readonly">
                                                                    <i class="fa fa-inr" aria-hidden="true"></i>
                                                                </div>
                                                                <label for="initial_amt[0]" class="error"></label>
                                                            </div>
                                                        </div>
                                                        
                                                        <div class="row">
                                                            <div class="col-sm-4">
                                                                <label class="input-label m-t-15">Pending Payment<span class="symbol required"></label>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <div class="d-flex-center">
                                                                    <input type="text" class="form-control per_amt pending_per" data-type="pending" placeholder="Type payment in %" name="pending_per[0]" value="{{ isset($data['pending_per']) ? $data['pending_per'] : '' }}" data-id="0">
                                                                    %
                                                                </div>
                                                                <label for="pending_per[0]" class="error"></label>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <div class="d-flex-center">
                                                                    <input type="text" class="form-control pending_amt" name="pending_amt[0]" value="{{ isset($data['pending_amt']) ? $data['pending_amt'] : '' }}" readonly="readonly">
                                                                    <i class="fa fa-inr" aria-hidden="true"></i>
                                                                </div>
                                                                <label for="pending_amt[0]" class="error"></label>
                                                            </div>
                                                          
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row hide">
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label class="input-label" for="batch_type">Early Bird Discount<span class="symbol required"></label>
                                                        <select name="discount[0]" class="form-control search-select discount" block-index="0">
                                                            <option value="">Select Days</option>
                                                            @for($i=7;$i<=30;$i++)
                                                                <option value="{{$i}}">{{$i}}</option>
                                                            @endfor
                                                        </select>
                                                    </div>
                                                </div>
                                               <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label class="input-label" for="batch_type">Discount<span class="symbol required"></label>
                                                        <div class="d-flex-center">
                                                            <input type="text" class="form-control discount_per" placeholder="Type payment in %" name="discount_per[0]" value="{{ isset($data['discount']) ? $data['discount'] : '' }}" data-id="0">
                                                            %
                                                        </div>
                                                    </div>  
                                                </div>
                                            </div>

                                            <?php
                                                $location_ids = '';
                                                if(isset($data['batch_location']) && !empty($data['batch_location'])){
                                                    $location_ids = array_values(collect($data['batch_location'])->pluck('location_id')->toArray());
                                                }

                                            ?>

                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label class="input-label" for="location">Institute Location<span class="symbol required">
                                                        <select multiple="multiple" name="location[0][]" class="form-control location select2-select" style="height: auto;" placeholder="Please select location">
                                                        @if(isset($location[0]['institute_registration_detail']['institute_locations']))
                                                            @foreach($location[0]['institute_registration_detail']['institute_locations'] as $index => $data)
                                                                @if($data['country'] == '95')

                                                                    @if(!empty($location_ids) && in_array($data['id'],$location_ids))
                                                                        <option value="{{$data['id']}}" selected="selected">{{$data['city']['name']}}, {{$data['state']['name']}}</option>
                                                                    @else
                                                                        <option value="{{$data['id']}}">{{$data['city']['name']}}, {{$data['state']['name']}}</option>
                                                                    @endif
                                                                @endif
                                                            @endforeach
                                                        @endif
                                                        </select>
                                                        <label class="location_error hide">Please select location</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <hr>
                                    @endif

                                    <div class="row" id="add-more-batch-row">
                                        <div class="col-sm-12">
                                            <button type="button" data-form-id="seafarer-certificates-form" class="btn add-more-button add-course-batch-button" id="add-batch">
                                            Add More Batch</button>
                                        </div>
                                    </div>
                                    <div class="section_cta text-right m_t_25 job-btn-container">
                                        <button type="button" style="height: 44px;" data-style="zoom-in" class="btn coss-primary-btn job-post-btn ladda-button" id="addinstitutebatchbtn">Save</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </main>
</div>
@stop
@section('js_script')
    <script type="text/javascript" src="/js/site/institute-registration.js"></script>
    <script type="text/javascript">
        // $('.select2-select').select2({
        //     maximumSelectionSize: 1,
        //     closeOnSelect: false,
        // });
        $('.select2-select').select2({
            maximumSelectionSize: 1
        }).on('select2-opening', function(e) {
            if ($(this).select2('val').length > 0) {
                e.preventDefault();
            }
        });
    </script>
@stop