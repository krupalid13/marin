@extends('site.index')

@section('content')
<div class="user-job-post content-section-wrapper dashboard-body" style="display: inherit !important;">
    <div class="no-margin row">
        @include('site.partials.sidebar')
        
        <main class="main" style="padding: 20px 20px 80px 20px !important;">
            <div class="row">
                <div class="col-xs-12">
                    <div class="company-page-profile-heading">
                        Add Notice
                    </div>
                </div>
            </div>
            
            <form id="add-institute-notice" method="post" action="{{ route('site.institute.store.notice') }}">
                {{ csrf_field() }}
                <div class="section job-post" style="padding: 0px !important;">
                    <div class="job-discription-card">
                        <div class="col-sm-12">
                            <div class="heading">
                               ADD NOTICE
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
	                                <label class="input-label">Start Date<span class="symbol required"></label>
	                                <input type="text" class="form-control datepicker" name="start_date" value="{{ isset($data[0]['institute_registration_detail']['institute_notice']['start_date']) ? date('d-m-Y',strtotime($data[0]['institute_registration_detail']['institute_notice']['start_date'])) : '' }}" placeholder="dd-mm-yyyy">
	                            </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="input-label">End Date<span class="symbol required"></label>
	                                <input type="text" class="form-control datepicker" name="end_date" value="{{ isset($data[0]['institute_registration_detail']['institute_notice']['end_date']) ? date('d-m-Y',strtotime($data[0]['institute_registration_detail']['institute_notice']['end_date'])) : '' }}" placeholder="dd-mm-yyyy">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
	                                <label class="input-label">Notice<span class="symbol required"></label>
	                                <textarea class="form-control notice" name="notice" rows="5" placeholder="Type your notice">{{ isset($data[0]['institute_registration_detail']['institute_notice']['notice']) ? $data[0]['institute_registration_detail']['institute_notice']['notice'] : '' }}</textarea>
	                            </div>
                            </div>
                        </div>
                        <div class="row">
                        	<div class="col-sm-12">
	                            <div class="text-right m_t_25 job-btn-container">
	                                <button type="button" data-style="zoom-in" class="btn coss-primary-btn ladda-button" id="addinstitutenoticebtn">Save</button>
	                            </div>
	                        </div>
                        </div>
                    </div>
                </form>
            </div>
        </main>
    </div>
</div>
@stop
@section('js_script')
    <script type="text/javascript" src="/js/site/institute-registration.js"></script>
@stop