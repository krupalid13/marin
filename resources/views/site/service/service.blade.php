@extends('site.index')


@section('content')
    <div class="service">
        <div class="section_1 z-depth-1 banner_section">
            <!-- <div class="full_overlay"></div> -->
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="banner_title text-center">
                            Our Services
                        </div>
                        <!-- <div class="banner_subTitle text-center m_t_25">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                        </div> -->
                    </div>
                </div>

            </div>
        </div>

        <?php 
            $endorsement = '';
            $seamanBook = '';
            $coc = '';
            $watchkeeping = '';

            if(isset($data) && !empty($data)){

                if($data['tab'] == 'endorsement'){
                    $endorsement = 'active';
                }elseif($data['tab'] == 'coc'){
                    $coc = 'active';
                }elseif($data['tab'] == 'watchkeeping'){
                    $watchkeeping = 'active';
                }else{
                    $seamanBook = 'active';
                }
            }else{
                $seamanBook = 'active';
            }

        ?>
        <div class="section_2">
            <div class="row width_100 m_lr_0">
                <div class="col-xs-12 col-md-3 col-lg-3 p_lr_0 calculate__width">
                    <div class="coss_sideBar z-depth-1 hidden-xs hidden-sm">
                        <ul class="nav nav-tabs coss_sideBar-menu" role="tablist">
                            <li role="presentation" class="coss_sideBar-menuItem {{$seamanBook}}" co_name="seamanBook">
                                <a href="#seamanBook" aria-controls="seamanBook" role="tab" data-toggle="tab">
                                    Seaman Book
                                </a>
                            </li>
                            <li role="presentation" class="coss_sideBar-menuItem {{$coc}}" co_name="coc">
                                <a href="#coc" aria-controls="coc" role="tab" data-toggle="tab">
                                    COC (Certificate of Competency)
                                </a>
                            </li>
                            <li role="presentation" class="coss_sideBar-menuItem {{$watchkeeping}}" co_name="watchkeeping">
                                <a href="#watchkeeping" aria-controls="watchkeeping" role="tab" data-toggle="tab">
                                    Watchkeeping
                                </a>
                            </li>
                            <li role="presentation" class="coss_sideBar-menuItem {{$endorsement}}" co_name="endorsement">
                                <a href="#endorsement" aria-controls="endorsement" role="tab" data-toggle="tab">
                                    Endorsements
                                </a>
                            </li>
                        </ul>
                    </div>

                    <div class="z-depth-1 dropdown dropdown_xs hidden-md hidden-lg">
                        <button class="btn btn-default dropdown-toggle dropdown_xs-btn" type="button" id="dropdownMenu1" data-toggle="dropdown" 
                        aria-haspopup="true" aria-expanded="false">
                            <!-- Dropdown --> 
                            <span class="caret"></span>
                        </button>
                        <span class="dropdown_xs-title" id="seamanBook-book">Seaman Book</span>
                        <span class="dropdown_xs-title hide" id="coc-book">COC (Certificate of Competency)</span>
                        <span class="dropdown_xs-title hide" id="watchkeeping-book">Watchkeeping</span>
                        <span class="dropdown_xs-title hide" id="endorsement-book">Endorsements</span>

                        <ul class="dropdown-menu dropdown_xs-menu " aria-labelledby="dropdownMenu1">
                            <li class="dropdown_xs-menuItem">
                                <a href="#seamanBook" aria-controls="seamanBook">
                                    Seaman Book
                                </a>
                            </li>
                            <li class="dropdown_xs-menuItem">
                                <a href="#coc" aria-controls="coc">
                                    COC (Certificate of Competency)
                                </a>
                            </li>
                            <li class="dropdown_xs-menuItem">
                                <a href="#watchkeeping" aria-controls="watchkeeping">
                                    Watchkeeping
                                </a>
                            </li>
                            <li class="dropdown_xs-menuItem">
                                <a href="#endorsement" aria-controls="endorsement">
                                    Endorsments
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9 p_lr_0">
                    <div class="tab-content coss_sideBarContent">
                        <div role="tabpanel" class="tab-pane seamanBook {{$seamanBook}}" id="seamanBook">
                            <div class="sideBarContent">
                                <div class="slider_wrapper hidden-xs hidden-sm">
                                    <div class="seaman_book page-dont-break">
                                        <div class="seaman_book_slide active" cardId="#indian">
                                            <img class="seaman_book_slideImg" src="{{ asset('/images/services/india.jpg') }}">
                                            <span class="seaman_book_slideTitle">Indian</span>
                                        </div>

                                        <div class="seaman_book_slide" cardId="#panama">
                                            <img class="seaman_book_slideImg" src="{{ asset('/images/services/panama.jpg') }}">
                                            <span class="seaman_book_slideTitle">Panama</span>
                                        </div>

                                        <div class="seaman_book_slide" cardId="#palau">
                                            <img class="seaman_book_slideImg" src="{{ asset('/images/services/palau.jpg') }}">
                                            <span class="seaman_book_slideTitle">Palau</span>
                                        </div>

                                        <div class="seaman_book_slide" cardId="#kitts">
                                            <img class="seaman_book_slideImg" src="{{ asset('/images/services/kitts.jpg') }}">
                                            <span class="seaman_book_slideTitle">St. Kitts & Nevis</span>
                                        </div>

                                        <div class="seaman_book_slide" cardId="#dominican">
                                            <img class="seaman_book_slideImg" src="{{ asset('/images/services/dominican.jpg') }}">
                                            <span class="seaman_book_slideTitle">Dominican</span>
                                        </div>

                                        <div class="seaman_book_slide" cardId="#bahamas">
                                            <img class="seaman_book_slideImg" src="{{ asset('/images/services/bahama.jpg') }}">
                                            <span class="seaman_book_slideTitle">Bahamas</span>
                                        </div>

                                        <div class="seaman_book_slide" cardId="#liberian">
                                            <img class="seaman_book_slideImg" src="{{ asset('/images/services/liberian.jpg') }}">
                                            <span class="seaman_book_slideTitle">Liberian</span>
                                        </div>
                                        
                                        <!-- <div class="seaman_book_slide" cardId="#micronesia">
                                            <img class="seaman_book_slideImg" src="{{ asset('/images/services/micronesia.jpg') }}">
                                            <span class="seaman_book_slideTitle">Micronesia</span>
                                        </div> -->
                                                                    
                                        <!-- <div class="seaman_book_slide" cardId="#cook">
                                            <img class="seaman_book_slideImg" src="{{ asset('/images/services/cook.jpg') }}">
                                            <span class="seaman_book_slideTitle">Cook</span>
                                        </div> -->

                                        <!-- <div class="seaman_book_slide" cardId="#liberian1">
                                            <img class="seaman_book_slideImg" src="{{ asset('/images/services/liberian.jpg') }}">
                                            <span class="seaman_book_slideTitle">Liberian</span>
                                        </div> -->
                                    </div>
                                </div>

                                <div id="indian" class="seamanBook_card">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-10 col-md-10">
                                            <div class="seamanBook_card-title">
                                                <div class="logo ind"></div>
                                                <div class="desc">
                                                    Indian Seaman Book
                                                </div>
                                            </div>

                                            <div class="seamanBook_card-list">
                                                <div class="seamanBook_card-listItem">
                                                    CDC – Continuous Discharge Certificate.
                                                </div>
                                                <div class="seamanBook_card-listItem">
                                                    File Number : 5 Days Time  Original CDC will be couriered by MMD. Enquiry to MMD only.</div> 
                                                <div class="seamanBook_card-listItem">
                                                    Photo size (3.5cm x 3.5cm) Photograph with clear white background - peg Scanned Photo.</div>
                                                <div class="seamanBook_card-listItem">
                                                    Scanned signature (black ink) of seafarer - JPEG</div>
                                                <div class="seamanBook_card-listItem">
                                                    Coloured scanned copy of original Passport (Pages where personal detials, Photo,address details and signature of passport authority are displayed ) - PDF</div>
                                                <div class="seamanBook_card-listItem">Coloured scanned copy of original 10th Standard Pass Certificate / Marksheet -PDF</div>
                                                <div class="seamanBook_card-listItem">
                                                    Documents required Basic 4 STCW+STSDSD / DG Approved Certificates
                                                </div>
                                                <div class="seamanBook_card-listItem">
                                                    ILO Medicals - DG Approved.
                                                </div>
                                            </div>

                                            <div class="seamanBook_card-content">
                                                Indian COE Fees on Request
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-2 col-md-2">
                                            <div class="seamanBook_card-cta">
                                                <button class="btn coss-primary-btn seaman-book-enquiry" data-enquire-for="Liberian Seaman book" type="button">Enquire</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div id="panama" class="seamanBook_card">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-10 col-md-10">
                                            <div class="seamanBook_card-title">
                                                <div class="logo pasb"></div>
                                                <div class="desc">
                                                    Panama Seaman Book
                                                </div>
                                            </div>

                                            <div class="seamanBook_card-list">
                                                <div class="seamanBook_card-listItem">CRA and copy of STSDSD in 4 Days.</div>
                                                <div class="seamanBook_card-listItem">Original in 45 days.</div>
                                                <div class="seamanBook_card-listItem">Photo / Passport / Application form and Medicals. To be uploaded in the system to download.</div>
                                                <div class="seamanBook_card-listItem">
                                                    Documents required Basic 4 STCW+STSDSD / DG Approved Certificates.
                                                </div>
                                            </div>

                                            <div class="seamanBook_card-content">
                                                Panama COE Fees on Request
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-2 col-md-2">
                                            <div class="seamanBook_card-cta">
                                                <button class="btn coss-primary-btn seaman-book-enquiry" data-enquire-for="Panama Seaman book" type="button">Enquire</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div id="palau" class="seamanBook_card">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-10 col-md-10">
                                            <div class="seamanBook_card-title">
                                                <div class="logo psb"></div>
                                                <div class="desc">
                                                    Palau Seaman Book
                                                </div>
                                            </div>

                                            <div class="seamanBook_card-list">
                                                <div class="seamanBook_card-listItem">Copy of CDC on 2nd Day of Application.</div>
                                                <div class="seamanBook_card-listItem">Original on the 5th Day.</div>
                                                <div class="seamanBook_card-listItem">Photo / Passport  /  Application form and Medicals. To be uploaded in the system to download.</div>
                                                <div class="seamanBook_card-listItem">
                                                    Documents required Basic 4 STCW+STSDSD / DG Approved Certificates.
                                                </div>
                                            </div>

                                            <div class="seamanBook_card-content">
                                                Palau COE Fees on Request
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-2 col-md-2">
                                            <div class="seamanBook_card-cta">
                                                <button class="btn coss-primary-btn seaman-book-enquiry" data-enquire-for="Palau Seaman book" type="button">Enquire</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div id="kitts" class="seamanBook_card">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-10 col-md-10">
                                            <div class="seamanBook_card-title">
                                                <div class="logo ssb"></div>
                                                <div class="desc">
                                                    St. Kitts & Nevis Seaman Book
                                                </div>
                                            </div>

                                            <div class="seamanBook_card-list">
                                                <div class="seamanBook_card-listItem">Copy of CDC on 2nd Day of Application.</div>
                                                <div class="seamanBook_card-listItem">Original on the 5th Day.</div>
                                                <div class="seamanBook_card-listItem">Photo / Passport  /  Application form and Medicals. To be uploaded in the system to download.</div>
                                                <div class="seamanBook_card-listItem">
                                                    Documents required Basic 4 STCW+STSDSD / Non DG / DG Approved Certificates.
                                                </div>
                                                <div class="seamanBook_card-listItem">No Rank on Seafarer Book.</div>
                                            </div>

                                            <div class="seamanBook_card-content">
                                                St. Kitts & Nevis COE Fees on Request
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-2 col-md-2">
                                            <div class="seamanBook_card-cta">
                                                <button class="btn coss-primary-btn seaman-book-enquiry" data-enquire-for="St. Kitts & Nevis Seaman book" type="button">Enquire</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div id="dominican" class="seamanBook_card">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-10 col-md-10">
                                            <div class="seamanBook_card-title">
                                                <div class="logo dsb"></div>
                                                <div class="desc">
                                                    Dominican Seaman Book
                                                </div>
                                            </div>

                                            <div class="seamanBook_card-list">
                                                <div class="seamanBook_card-listItem">CRA 4 Days of Application.</div>
                                                <div class="seamanBook_card-listItem">Original on the 3 Weeks.</div>
                                                <div class="seamanBook_card-listItem">Photo / Passport  /  Application form and Medicals. To be uploaded in the system to download.</div>
                                                <div class="seamanBook_card-listItem">
                                                    Documents required Basic 4 STCW+STSDSD / Non DG / DG Approved Certificates.
                                                </div>
                                            </div>

                                            <div class="seamanBook_card-content">
                                                Dominican COE Fees on Request
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-2 col-md-2">
                                            <div class="seamanBook_card-cta">
                                                <button class="btn coss-primary-btn seaman-book-enquiry" data-enquire-for="Dominican Seaman book" type="button">Enquire</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div id="bahamas" class="seamanBook_card">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-10 col-md-10">
                                            <div class="seamanBook_card-title">
                                                <div class="logo bah"></div>
                                                <div class="desc">
                                                    Bahamas Seaman Book
                                                </div>
                                            </div>

                                            <div class="seamanBook_card-list">
                                                <div class="seamanBook_card-listItem">CRA / Acknowledgement in 5 Days.</div>
                                                <div class="seamanBook_card-listItem">Original in 15 Days.</div>
                                                <div class="seamanBook_card-listItem">Photo / Passport / Medicals.</div>
                                                <div class="seamanBook_card-listItem">No Rank of Seafarers Book.</div>
                                            </div>

                                            <div class="seamanBook_card-content">
                                                Bahamas COE Fees on Request
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-2 col-md-2">
                                            <div class="seamanBook_card-cta">
                                                <button class="btn coss-primary-btn seaman-book-enquiry" data-enquire-for="Dominican Seaman book" type="button">Enquire</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div id="liberian" class="seamanBook_card">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-10 col-md-10">
                                            <div class="seamanBook_card-title">
                                                <div class="logo lsb"></div>
                                                <div class="desc">
                                                    Liberian Seaman Book
                                                </div>
                                            </div>

                                            <div class="seamanBook_card-list">
                                                <div class="seamanBook_card-listItem">CRA /Application form 5 Days.</div>
                                                <div class="seamanBook_card-listItem">Original in 45 days.</div>
                                                <div class="seamanBook_card-listItem">Photo / Passport / Application form and Medicals. To be uploaded in the system to download.</div>
                                                <div class="seamanBook_card-listItem">
                                                    Documents required Basic 4 STCW+STSDSD / DG Approved Certificates.
                                                </div> 
                                                <div class="seamanBook_card-listItem">No Rank on Seafarers Book.</div>
                                            </div>

                                            <div class="seamanBook_card-content">
                                                Liberian COE Fees on Request
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-2 col-md-2">
                                            <div class="seamanBook_card-cta">
                                                <button class="btn coss-primary-btn seaman-book-enquiry" data-enquire-for="Liberian Seaman book" type="button">Enquire</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!-- <div id="micronesia" class="seamanBook_card">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-10 col-md-10">
                                            <div class="seamanBook_card-title">
                                                <div class="logo msb"></div>
                                                <div class="desc">
                                                    Micronesia Seaman book
                                                </div>
                                            </div>

                                            <div class="seamanBook_card-list">
                                                <div class="seamanBook_card-listItem">CRA in 48 Hours</div>
                                                <div class="seamanBook_card-listItem">Original is 45 working Days.</div>
                                                <div class="seamanBook_card-listItem">Photo / Passport / Application Form / Medicals.</div>
                                                <div class="seamanBook_card-listItem">
                                                    Documents required Basic 4 STCW+STSDSD / DG Approved Certificates
                                                </div>
                                            </div>

                                            <div class="seamanBook_card-content">
                                                Micronesia COE Fees on Request
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-2 col-md-2">
                                            <div class="seamanBook_card-cta">
                                                <button class="btn coss-primary-btn seaman-book-enquiry" data-enquire-for="Micronesia Seaman book" type="button">Enquire</button>
                                            </div>
                                        </div>
                                    </div>
                                </div> -->

                                <!-- <div id="cook" class="seamanBook_card">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-10 col-md-10">
                                            <div class="seamanBook_card-title">
                                                <div class="logo csb"></div>
                                                <div class="desc">
                                                    Cook Seaman book
                                                </div>
                                            </div>

                                            <div class="seamanBook_card-list">
                                                <div class="seamanBook_card-listItem">CRA in 48 Hours</div>
                                                <div class="seamanBook_card-listItem">Original is 45 working Days.</div>
                                                <div class="seamanBook_card-listItem">Photo / Passport / Application Form / Medicals.</div>
                                                <div class="seamanBook_card-listItem">
                                                    Documents required Basic 4 STCW+STSDSD / DG Approved Certificates
                                                </div>
                                            </div>

                                            <div class="seamanBook_card-content">
                                                Cook COE Fees on Request
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-2 col-md-2">
                                            <div class="seamanBook_card-cta">
                                                <button class="btn coss-primary-btn seaman-book-enquiry" data-enquire-for="Cook Seaman book" type="button">Enquire</button>
                                            </div>
                                        </div>
                                    </div>
                                </div> -->

                                <!-- <div id="liberian1" class="seamanBook_card">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-10 col-md-10">
                                            <div class="seamanBook_card-title">
                                                <div class="logo lsb"></div>
                                                <div class="desc">
                                                    Liberian Seaman book
                                                </div>
                                            </div>

                                            <div class="seamanBook_card-list">
                                                <div class="seamanBook_card-listItem">CRA in 48 Hours</div>
                                                <div class="seamanBook_card-listItem">Original is 45 working Days.</div>
                                                <div class="seamanBook_card-listItem">Photo / Passport / Application Form / Medicals.</div>
                                                <div class="seamanBook_card-listItem">
                                                    Documents required Basic 4 STCW+STSDSD / DG Approved Certificates
                                                </div>
                                            </div>

                                            <div class="seamanBook_card-content">
                                                Liberian COE Fees on Request
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-2 col-md-2">
                                            <div class="seamanBook_card-cta">
                                                <button class="btn coss-primary-btn seaman-book-enquiry" data-enquire-for="Liberian Seaman book" type="button">Enquire</button>
                                            </div>
                                        </div>
                                    </div>
                                </div> -->
                            </div>
                        </div>

                        <div role="tabpanel" class="tab-pane coc {{$coc}}" id="coc">
                            <div class="sideBarContent">
                                <div class="slider_wrapper hidden-xs hidden-sm">
                                    <div class="coc_tab page-dont-break">
                                        <div class="coc_tab_slide active" cardId="#honduras">
                                            <img class="coc_tab_slideImg" src="{{ asset('/images/services/honduras.jpg') }}">
                                            <span class="coc_tab_slideTitle">Honduras</span>
                                        </div>
                                    </div>
                                </div>

                                <div id="honduras" class="seamanBook_card">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-10 col-md-10">
                                            <div class="seamanBook_card-title">
                                                <div class="logo hnd"></div>
                                                <div class="desc">
                                                    Honduras COC
                                                </div>
                                            </div>

                                            <div class="seamanBook_card-list">
                                                <div class="seamanBook_card_rank">
                                                    <b>Ranks : </b>Master , Chief Officer , Second Officer , Chief Engineer ,  First Engineer , Second Engineer
                                                </div>
                                                <div class="seamanBook_card_heading">Documents Required : </div>
                                                <div class="seamanBook_card-listItem">
                                                    Photo (Colour)
                                                </div>

                                                <div class="seamanBook_card-listItem">
                                                    Passport (Colour- 1st & Last page)
                                                </div>

                                                <div class="seamanBook_card-listItem">
                                                    CDC & Stamping Pages (Valid)- More than 12 months sea service in current rank.
                                                </div>

                                                <div class="seamanBook_card-listItem">
                                                    COC (Certificate of Competency)
                                                </div>

                                                <div class="seamanBook_card-listItem">
                                                    Medical Fitness Certificate (Valid)
                                                </div>

                                                <div class="seamanBook_card-listItem">
                                                    Honduras Application Form
                                                </div>
                                            </div>

                                            <hr>

                                            <div class="seamanBook_card-list">
                                                <div class="seamanBook_card_rank">
                                                    <b>Ranks : </b>Third Engineer , Third Officer
                                                </div>
                                                <div class="seamanBook_card_heading">Documents Required : </div>
                                                <div class="seamanBook_card-listItem">
                                                    Photo (Colour)
                                                </div>

                                                <div class="seamanBook_card-listItem">
                                                    Passport (Colour- 1st & Last page)
                                                </div>

                                                <div class="seamanBook_card-listItem">
                                                    CDC & Stamping Pages (Valid)

                                                    <div>
                                                        - More than 36 months sea service as Engine Cadet / Deck Cadet
                                                    </div>
                                                    <div>
                                                        - More Than 36 months sea service as Ordinary Seaman / 16 months as Able Seaman with watchkeeping Certificate
                                                    </div>
                                                    <div>
                                                        - More Than 36 months sea service as Wiper / 16 months as Motorman with watchkeeping Certificate
                                                    </div>
                                                </div>

                                                <div class="seamanBook_card-listItem">
                                                    Medical Fitness Certificate (Valid)
                                                </div>

                                                <div class="seamanBook_card-listItem">
                                                    Honduras Application Form
                                                </div>
                                            </div>

                                            <hr>

                                            <div class="seamanBook_card-list">
                                                <div class="seamanBook_card_rank">
                                                    <b>Ranks : </b>ETO - Electro Technical Officer
                                                </div>
                                                <div class="seamanBook_card_heading">Documents Required : </div>
                                                <div class="seamanBook_card-listItem">
                                                    Photo (Colour)
                                                </div>

                                                <div class="seamanBook_card-listItem">
                                                    Passport (Colour- 1st & Last page)
                                                </div>

                                                <div class="seamanBook_card-listItem">
                                                    CDC & Stamping Pages (Valid) Exp for more than 18 months
                                                </div>

                                                <div class="seamanBook_card-listItem">
                                                    Electrical Engineering or Diploma Certificate / ITI Certificate.
                                                </div>

                                                <div class="seamanBook_card-listItem">
                                                    Medical Fitness Certificate (Valid)
                                                </div>

                                                <div class="seamanBook_card-listItem">
                                                    Honduras Application Form
                                                </div>
                                            </div>

                                            <hr>

                                            <div class="seamanBook_card-list">
                                                <div class="seamanBook_card_rank">
                                                    <b>Ranks : </b>Able Seaman (Reg II/4) , Motorman (III/4)
                                                </div>
                                                <div class="seamanBook_card_heading">Documents Required : </div>
                                                <div class="seamanBook_card-listItem">
                                                    Photo (Colour)
                                                </div>

                                                <div class="seamanBook_card-listItem">
                                                    Passport (Colour- 1st & Last page)
                                                </div>

                                                <div class="seamanBook_card-listItem">
                                                    CDC & Stamping Pages (Valid) Exp as Ordinary Seaman / Wiper for more than 36 months.
                                                </div>

                                                <div class="seamanBook_card-listItem">
                                                    Medical Fitness Certificate (Valid)
                                                </div>

                                                <div class="seamanBook_card-listItem">
                                                    Honduras Application Form
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div role="tabpanel" class="tab-pane watchkeeping {{$watchkeeping}}" id="watchkeeping">
                            <div class="sideBarContent">
                                <div class="slider_wrapper hidden-xs hidden-sm">
                                    <div class="cop_endorsement_tab page-dont-break">
                                        <div class="cop_endorsement_tab_slide active" cardId="#honuras">
                                            <img class="cop_endorsement_tab_slideImg" src="{{ asset('/images/services/honduras.jpg') }}">
                                            <span class="cop_endorsement_tab_slideTitle">Honduras</span>
                                        </div>
                                    </div>
                                </div>

                                <div id="honuras" class="seamanBook_card">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-10 col-md-10">
                                            <div class="seamanBook_card-title">
                                                <div class="logo hnd"></div>
                                                <div class="desc">
                                                    Honduras COP
                                                </div>
                                            </div>

                                            <div class="seamanBook_card-list">
                                                <div class="seamanBook_card_rank">
                                                    <b>Ranks : </b>Able Seaman- COP (Reg II/5) , Motorman- COP (Reg III/5)
                                                </div>
                                                <div class="seamanBook_card_heading">Documents Required : </div>
                                                <div class="seamanBook_card-listItem">
                                                    Photo (Colour)
                                                </div>

                                                <div class="seamanBook_card-listItem">
                                                    Passport (Colour- 1st & Last page)
                                                </div>

                                                <div class="seamanBook_card-listItem">
                                                    CDC & Stamping Pages (Valid) 12 Months Exp. as Able Seaman / Motorman.
                                                </div>

                                                <div class="seamanBook_card-listItem">
                                                    Watchkeeping Certificate
                                                </div>

                                                <div class="seamanBook_card-listItem">
                                                    Medical Fitness Certificate (Valid)
                                                </div>

                                                <div class="seamanBook_card-listItem">
                                                    Honduras Application Form
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div role="tabpanel" class="tab-pane endorsement {{$endorsement}}" id="endorsement">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="seaman-book-enquiry-modal" class="modal extended-modal fade no-display" tabindex="-1" data-width="760">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="advertise-enquiry-form-container">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                          &times;
                        </button>
                        <div class="heading"> 
                            <h4 class="modal-title input-label">Seaman Book Enquiry</h4>
                        </div>
                    </div>
                    <div class="modal-body enquire-advertisements-container">

                        <form id="seaman-book-enquiry-modal-form" action="{{ route('site.seaman.book.enquiry') }}" method="post">
                            {{csrf_field()}}
                            <div class="form-group">
                                <label class="input-label" for="name">Enquiry For</label>
                                <input type="text" class="form-control" id="enquiry_for" name="enquiry_for" readonly="readonly">
                            </div>
                            <div class="form-group">
                                <label class="input-label" for="name">Name</label>
                                <input type="text" class="form-control" id="name" name="name">
                            </div>
                            <div class="form-group">
                                <label class="input-label" for="name">Phone Number</label>
                                <input type="text" class="form-control" id="phone" name="phone">
                            </div>
                            <div class="form-group">
                                <label class="input-label" for="name">Email</label>
                                <input type="email" class="form-control" id="email" name="email">
                            </div>
                            <div class="form-group">
                                <label class="input-label" for="name">Message</label>
                                <textarea class="form-control" rows="6" name="message" id="message"></textarea>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <div class="row right">
                            <div class="col-sm-12">
                                <button type="button" data-style="zoom-in" class="btn btn-blue btn-default ladda-button" id="send-seaman-book-enquiry-button">
                                    Send Enquiry
                                </button>   
                            </div>
                        </div>  
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop