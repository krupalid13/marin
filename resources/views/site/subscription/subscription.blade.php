@extends('site.index')
@section('content')

<div id="pricing-page">
	<div class="section-1">
		<div class="pricing-overlay"></div>
		<div class="page-title">
			SUBSCRIPTION
		</div>
	</div>
@if(isset($my_subscription))
	<div class="section-2">
		<div class="container">
			@if(!empty($subscriptions))
			<div class="row">
				@foreach( $subscriptions as $subscription )
					@if(strtolower($subscription['duration_title']) != 'free')
					<div class="col-xs-12 col-sm-6 col-md-4">
						
						<div class="pricing-card {{ $subscription['duration'] == 3 ? 'main-card' : '' }}">
							<div class="pricing-head">
								<div class="rate">
									<i class="fa fa-inr" aria-hidden="true"></i>
									<div class="value">{{ round($subscription['amount']) }}</div>
									<div class="period">{{$subscription['duration_title']}}</div>
								</div>
								<div class="rate-discription">
									
								</div>
							</div>
							<div class="included-content">
								@foreach( $subscription['subscription_features'] as $feature )
								<div class="point">
									<span class="tick"><i class="fa fa-check-circle" aria-hidden="true"></i></span>
									<span class="point-discription">
										{{ $feature['title'] }}
									</span>
								</div>
								@endforeach
							</div>
							@if(empty($my_subscription['up_coming']))

								@if(empty($my_subscription['active']) || strtolower($my_subscription['active']['subscription_details']['duration_title']) == 'free')
									<div class="purchase-btn-container">
										<a class="btn coss-inverse-btn pointer purchase-btn buy-feature-button" data-subscription-id="{{$subscription['id']}}" data-buy-type="purchase">Purchase</a>
									</div>
								@else
									<div class="purchase-btn-container">
										@if(strtolower($my_subscription['active']['subscription_details']['duration_title']) == strtolower($subscription['duration_title']))
											<a class="btn coss-inverse-btn pointer purchase-btn buy-feature-button" data-subscription-id="{{$subscription['id']}}" data-buy-type="renew">Renew</a>
										@elseif(strtolower($my_subscription['active']['subscription_details']['duration_title']) == 'monthly' && (strtolower($subscription['duration_title']) == 'quaterly' || strtolower($subscription['duration_title']) == 'yearly'))
											<a class="btn coss-inverse-btn pointer purchase-btn buy-feature-button" data-subscription-id="{{$subscription['id']}}" data-buy-type="upgrade">Upgrade</a>
										@elseif(strtolower($my_subscription['active']['subscription_details']['duration_title']) == 'quaterly' && strtolower($subscription['duration_title']) == 'monthly')
											<a class="btn coss-inverse-btn pointer purchase-btn buy-feature-button" data-subscription-id="{{$subscription['id']}}" data-buy-type="buynow">Buy now</a>
										@elseif(strtolower($my_subscription['active']['subscription_details']['duration_title']) == 'quaterly' && strtolower($subscription['duration_title']) == 'yearly')
											<a class="btn coss-inverse-btn pointer purchase-btn buy-feature-button" data-subscription-id="{{$subscription['id']}}" data-buy-type="upgrade">Upgrade</a>
										@elseif(strtolower($my_subscription['active']['subscription_details']['duration_title']) == 'yearly' && (strtolower($subscription['duration_title']) == 'monthly' || strtolower($subscription['duration_title']) == 'quaterly'))
											<a class="btn coss-inverse-btn pointer purchase-btn buy-feature-button" data-subscription-id="{{$subscription['id']}}" data-buy-type="buynow">Buy now</a>

										@endif
									</div>
								@endif

							@endif
						</div>
					</div>
					@endif
				@endforeach
			</div>
			@else
				<h3>No subscription present for {{Auth::user()->registered_as}}</h3>
			@endif
		</div>
	</div>
@else
	<div class="section-2">
		<div class="container">
			<ul class="nav nav-tabs" role="tablist">
				@if(isset($subscriptions['company']) && !empty($subscriptions['company']))
			    <li role="presentation" class="{{( isset($_GET['type']) && trim($_GET['type']) == 'company' ) ? 'active' : ''}} {{isset($_GET['type']) ? '' : 'active'}}"><a href="#company-plan" aria-controls="company-plan" role="tab" data-toggle="tab">Company</a></li>
			    @endif
			    @if(isset($subscriptions['institute']) && !empty($subscriptions['institute']))
			    <li role="presentation" class="{{( isset($_GET['type']) && trim($_GET['type']) == 'institute' ) ? 'active' : ''}}"><a href="#institute-plan" aria-controls="institute-plan" role="tab" data-toggle="tab">Institute</a></li>
			    @endif
			    @if(isset($subscriptions['advertiser']) && !empty($subscriptions['advertiser']))
			    <li role="presentation" class="{{( isset($_GET['type']) && trim($_GET['type']) == 'advertiser' ) ? 'active' : ''}}"><a href="#advertiser-plan" aria-controls="advertiser-plan" role="tab" data-toggle="tab">Advertiser</a></li>
			    @endif
			    @if(isset($subscriptions['seafarer']) && !empty($subscriptions['seafarer']))
			    <li role="presentation" class="{{( isset($_GET['type']) && trim($_GET['type']) == 'seafarer' ) ? 'active' : ''}}"><a href="#seafarer-plan" aria-controls="seafarer-plan" role="tab" data-toggle="tab">Seafarer</a></li>
			    @endif
			</ul>
		</div>
	</div>
	<div class="tab-content">
	@if(isset($subscriptions['company']) && !empty($subscriptions['company']))
		<div id="company-plan" role="tabpanel" class="tab-pane {{( isset($_GET['type']) && trim($_GET['type']) == 'company' ) ? 'active' : ''}} {{isset($_GET['type']) ? '' : 'active'}}">
			<div class="container p-t-35 p-b-35">
				<div class="row">
					<div class="section-2">
					@foreach( $subscriptions['company'] as $subscription )
					@if($subscription['duration_title'] != 'free')
						<div class="col-xs-12 col-sm-6 col-md-4">
							<div class="pricing-card {{ $subscription['duration'] == 3 ? 'all-main-card' : '' }}">
								<div class="pricing-head">
									<div class="rate">
										<i class="fa fa-inr" aria-hidden="true"></i>
										<div class="value">{{ round($subscription['amount']) }}</div>
										<div class="period">{{$subscription['duration_title']}}</div>
									</div>
									<div class="rate-discription">
										
									</div>
								</div>
								<div class="included-content">
									@foreach( $subscription['subscription_features'] as $feature )
									<div class="point">
										<span class="tick"><i class="fa fa-check-circle" aria-hidden="true"></i></span>
										<span class="point-discription">
											{{ $feature['title'] }}
										</span>
									</div>
									@endforeach
								</div>
								<div class="purchase-btn-container">
									<a href="{{route('site.login')}}" class="btn coss-inverse-btn pointer purchase-btn">Buy now</a>
								</div>
							</div>
						</div>
					@endif
					@endforeach
					</div>
				</div>
			</div>
			@foreach( $subscriptions['company'] as $subscription )
			@if($subscription['duration_title'] == 'free')
			<div class="section-3">
				<div class="container">
					<div class="subcription-title">
						<span class="icon"><i class="fa fa-rss" aria-hidden="true"></i></span>
						<span class="subcription-content">Or Continue with Free Subscription</span>
					</div>
					<div class="subcription-discription">
						<!-- <span class="spcl">Lorem ipsum</span> dolor sit amet, ea amet tibique splendide est, sit te dignissim sit amet, ea amet tibique splendide est, si --> 
					</div>
					<div>
						<div class="row">
							<div class="col-xs-12 col-sm-6 col-md-4 col-md-offset-2">
								<div class="pricing-card">
									<div class="included-content">
										@foreach( $subscription['subscription_features'] as $feature )
											<div class="point point-border">
												<span class="tick"><i class="fa fa-check-circle" aria-hidden="true"></i></span>
												<span class="point-discription">
													{{ $feature['title'] }}
												</span>
											</div>
										@endforeach
									</div>
									<div class="purchase-btn-container">
										<a class="btn coss-primary-btn pointer" href="{{route('site.login')}}">Register Now</a>
									</div>
								</div>
							</div>
							<div class="col-xs-12 col-sm-6 col-md-4 text-center">
								<img class="free-subscription" src="{{asset('/images/pricing/free_subscription.png')}}" alt="">
							</div>
						</div>
					</div>
				</div>
			</div>
			@endif
			@endforeach
		</div>
	@endif
	@if(isset($subscriptions['institute']) && !empty($subscriptions['institute']))
		<div id="institute-plan" role="tabpanel" class="tab-pane {{( isset($_GET['type']) && trim($_GET['type']) == 'institute' ) ? 'active' : ''}}">
			<div class="container p-t-35 p-b-35">
				<div class="row">
					<div class="section-2">
					@foreach( $subscriptions['institute'] as $subscription )
					@if($subscription['duration_title'] != 'free')
						<div class="col-xs-12 col-sm-6 col-md-4">
							<div class="pricing-card {{ $subscription['duration'] == 3 ? 'all-main-card' : '' }}">
								<div class="pricing-head">
									<div class="rate">
										<i class="fa fa-inr" aria-hidden="true"></i>
										<div class="value">{{ round($subscription['amount']) }}</div>
										<div class="period">{{$subscription['duration_title']}}</div>
									</div>
									<div class="rate-discription">
										
									</div>
								</div>
								<div class="included-content">
									@foreach( $subscription['subscription_features'] as $feature )
									<div class="point">
										<span class="tick"><i class="fa fa-check-circle" aria-hidden="true"></i></span>
										<span class="point-discription">
											{{ $feature['title'] }}
										</span>
									</div>
									@endforeach
								</div>
								<div class="purchase-btn-container">
									<a href="{{route('site.login')}}" class="btn coss-inverse-btn pointer purchase-btn">Buy now</a>
								</div>
							</div>
						</div>
					@endif
					@endforeach
					</div>
				</div>
			</div>
			@foreach( $subscriptions['institute'] as $subscription )
			@if($subscription['duration_title'] == 'free')
			<div class="section-3">
				<div class="container">
					<div class="subcription-title">
						<span class="icon"><i class="fa fa-rss" aria-hidden="true"></i></span>
						<span class="subcription-content">Or Continue with Free Subscription</span>
					</div>
					<div class="subcription-discription">
						<!-- <span class="spcl">Lorem ipsum</span> dolor sit amet, ea amet tibique splendide est, sit te dignissim sit amet, ea amet tibique splendide est, si --> 
					</div>
					<div>
						<div class="row">
							<div class="col-xs-12 col-sm-6 col-md-4 col-md-offset-2">
								<div class="pricing-card">
									<div class="included-content">
										@foreach( $subscription['subscription_features'] as $feature )
											<div class="point point-border">
												<span class="tick"><i class="fa fa-check-circle" aria-hidden="true"></i></span>
												<span class="point-discription">
													{{ $feature['title'] }}
												</span>
											</div>
										@endforeach
									</div>
									<div class="purchase-btn-container">
										<a class="btn coss-primary-btn pointer" href="{{route('site.login')}}">Register</a>
									</div>
								</div>
							</div>
							<div class="col-xs-12 col-sm-6 col-md-4 text-center">
								<img class="free-subscription" src="{{asset('/images/pricing/free_subscription.png')}}" alt="">
							</div>
						</div>
					</div>
				</div>
			</div>
			@endif
			@endforeach
		</div>
	@endif
	@if(isset($subscriptions['advertiser']) && !empty($subscriptions['advertiser']))
		<div id="advertiser-plan" role="tabpanel" class="tab-pane {{( isset($_GET['type']) && trim($_GET['type']) == 'advertiser' ) ? 'active' : ''}}">
			<div class="container p-t-35 p-b-35">
				<div class="row">
					<div class="section-2">
					@foreach( $subscriptions['advertiser'] as $subscription )
					@if($subscription['duration_title'] != 'free')
						<div class="col-xs-12 col-sm-6 col-md-4">
							<div class="pricing-card {{ $subscription['duration'] == 3 ? 'all-main-card' : '' }}">
								<div class="pricing-head">
									<div class="rate">
										<i class="fa fa-inr" aria-hidden="true"></i>
										<div class="value">{{ round($subscription['amount']) }}</div>
										<div class="period">{{$subscription['duration_title']}}</div>
									</div>
									<div class="rate-discription">
										
									</div>
								</div>
								<div class="included-content">
									@foreach( $subscription['subscription_features'] as $feature )
									<div class="point">
										<span class="tick"><i class="fa fa-check-circle" aria-hidden="true"></i></span>
										<span class="point-discription">
											{{ $feature['title'] }}
										</span>
									</div>
									@endforeach
								</div>
								<div class="purchase-btn-container">
									<a href="{{route('site.login')}}" class="btn coss-inverse-btn pointer purchase-btn">Buy now</a>
								</div>
							</div>
						</div>
					@endif
					@endforeach
					</div>
				</div>
			</div>
			@foreach( $subscriptions['advertiser'] as $subscription )
			@if($subscription['duration_title'] == 'free')
			<div class="section-3">
				<div class="container">
					<div class="subcription-title">
						<span class="icon"><i class="fa fa-rss" aria-hidden="true"></i></span>
						<span class="subcription-content">Or Continue with Free Subscription</span>
					</div>
					<div class="subcription-discription">
						<!-- <span class="spcl">Lorem ipsum</span> dolor sit amet, ea amet tibique splendide est, sit te dignissim sit amet, ea amet tibique splendide est, si --> 
					</div>
					<div>
						<div class="row">
							<div class="col-xs-12 col-sm-6 col-md-4 col-md-offset-2">
								<div class="pricing-card">
									<div class="included-content">
										@foreach( $subscription['subscription_features'] as $feature )
											<div class="point point-border">
												<span class="tick"><i class="fa fa-check-circle" aria-hidden="true"></i></span>
												<span class="point-discription">
													{{ $feature['title'] }}
												</span>
											</div>
										@endforeach
									</div>
									<div class="purchase-btn-container">
										<a class="btn coss-primary-btn pointer" href="{{route('site.login')}}">Register</a>
									</div>
								</div>
							</div>
							<div class="col-xs-12 col-sm-6 col-md-4 text-center">
								<img class="free-subscription" src="{{asset('/images/pricing/free_subscription.png')}}" alt="">
							</div>
						</div>
					</div>
				</div>
			</div>
			@endif
			@endforeach
		</div>
	@endif
	@if(isset($subscriptions['seafarer']) && !empty($subscriptions['seafarer']))
		<div id="seafarer-plan" role="tabpanel" class="tab-pane {{( isset($_GET['type']) && trim($_GET['type']) == 'seafarer' ) ? 'active' : ''}}">
			<div class="container p-t-35 p-b-35">
				<div class="row">
					<div class="section-2">
					@foreach( $subscriptions['seafarer'] as $subscription )
					@if($subscription['duration_title'] != 'free')
						<div class="col-xs-12 col-sm-6 col-md-4">
							<div class="pricing-card {{ $subscription['duration'] == 3 ? 'all-main-card' : '' }}">
								<div class="pricing-head">
									<div class="rate">
										<i class="fa fa-inr" aria-hidden="true"></i>
										<div class="value">{{ round($subscription['amount']) }}</div>
										<div class="period">{{$subscription['duration_title']}}</div>
									</div>
									<div class="rate-discription">
										
									</div>
								</div>
								<div class="included-content">
									@foreach( $subscription['subscription_features'] as $feature )
									<div class="point">
										<span class="tick"><i class="fa fa-check-circle" aria-hidden="true"></i></span>
										<span class="point-discription">
											{{ $feature['title'] }}
										</span>
									</div>
									@endforeach
								</div>
								<div class="purchase-btn-container">
									<a href="{{route('site.login')}}" class="btn coss-inverse-btn pointer purchase-btn">Buy now</a>
								</div>
							</div>
						</div>
					@endif
					@endforeach
					</div>
				</div>
			</div>
			@foreach( $subscriptions['seafarer'] as $subscription )
			@if($subscription['duration_title'] == 'free')
			<div class="section-3">
				<div class="container">
					<div class="subcription-title">
						<span class="icon"><i class="fa fa-rss" aria-hidden="true"></i></span>
						<span class="subcription-content">Or Continue with Free Subscription</span>
					</div>
					<div class="subcription-discription">
						<!-- <span class="spcl">Lorem ipsum</span> dolor sit amet, ea amet tibique splendide est, sit te dignissim sit amet, ea amet tibique splendide est, si --> 
					</div>
					<div>
						<div class="row">
							<div class="col-xs-12 col-sm-6 col-md-4 col-md-offset-2">
								<div class="pricing-card">
									<div class="included-content">
										@foreach( $subscription['subscription_features'] as $feature )
											<div class="point point-border">
												<span class="tick"><i class="fa fa-check-circle" aria-hidden="true"></i></span>
												<span class="point-discription">
													{{ $feature['title'] }}
												</span>
											</div>
										@endforeach
									</div>
									<div class="purchase-btn-container">
										<a class="btn coss-primary-btn pointer" href="{{route('site.login')}}">Register</a>
									</div>
								</div>
							</div>
							<div class="col-xs-12 col-sm-6 col-md-4 text-center">
								<img class="free-subscription" src="{{asset('/images/pricing/free_subscription.png')}}" alt="">
							</div>
						</div>
					</div>
				</div>
			</div>
			@endif
			@endforeach
		</div>
	@endif
	</div>
@endif

@if(isset($my_subscription))
	@if((empty($my_subscription['active']) && empty($my_subscription['expired']) && empty($my_subscription['up_coming'])) || (!empty($my_subscription['active']) && strtolower($my_subscription['active']['subscription_details']['duration_title']) == 'free'))

		@foreach( $subscriptions as $subscription )
			@if(strtolower($subscription['duration_title']) == 'free')
				<div class="section-3">
					<div class="container">
						<div class="subcription-title">
							<span class="icon"><i class="fa fa-rss" aria-hidden="true"></i></span>
							<span class="subcription-content">Or Continue with Free Subscription</span>
						</div>
						<div class="subcription-discription">
							<!-- <span class="spcl">Lorem ipsum</span> dolor sit amet, ea amet tibique splendide est, sit te dignissim sit amet, ea amet tibique splendide est, si --> 
						</div>
						<div>
							<div class="row">
								<div class="col-xs-12 col-sm-6 col-md-4 col-md-offset-2">
									<div class="pricing-card">
										<div class="included-content">
													@foreach( $subscription['subscription_features'] as $feature )
														<div class="point point-border">
															<span class="tick"><i class="fa fa-check-circle" aria-hidden="true"></i></span>
															<span class="point-discription">
																{{ $feature['title'] }}
															</span>
														</div>
													@endforeach
										</div>
										<div class="purchase-btn-container">
											<a class="btn coss-primary-btn pointer buy-free-subscription-button ladda-button" data-style="zoom-in">Continue With Free</a>
										</div>
									</div>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-4 text-center">
									<img class="free-subscription" src="{{asset('/images/pricing/free_subscription.png')}}" alt="">
								</div>
							</div>
						</div>
					</div>
				</div>
			@endif
		@endforeach
	@endif
@endif

@if(isset($my_subscription['up_coming']) && !empty($my_subscription['up_coming']))
<div class="container">
	<div class="alert alert-danger">
		You already have a future subscription, once that subscription begins then you will be able to renew / upgrade subscriptions <br>For more details visit 
		@if(Auth::check() && Auth::User()->registered_as == 'company')
		<a href="{{route('site.company.mysubscription')}}">my subscription page</a>
		@elseif(Auth::check() && Auth::User()->registered_as == 'institute')
			<a href="{{route('site.institute.mysubscription')}}">my subscription page</a>
		@elseif(Auth::check() && Auth::User()->registered_as == 'advertiser')
			<a href="{{route('site.advertiser.mysubscription')}}">my subscription page</a>
		@else
			your profiles My Subscription
		@endif
	</div>
</div>
@endif


</div>
<div id="payumoney-form" class="hide"> 
</div>
<div id="buy-subcription-date-modal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Subcription Confirmation</h4>
      </div>
      <div class="modal-body">
      <form action="" id="buy-subcription-date-form">
      	<div class="amount-break-up table-responsive">
      		<table class="table table-bordered table-hover">
      			<thead>
      				<tr>
      					<th>
      						Plan Name
      					</th>
      					<th>
      						Plan Cost
      					</th>

      					<th>
      						Tax cgst %
      					</th>
      					<th>
      						Tax sgst %
      					</th>
      					<th>
      						Total Cost
      					</th>
      				</tr>
      			</thead>
      			<tbody>
      				<tr>
      					<td class="plan-name">
      						
      					</td>
      					<td class="plan-cost">
      						
      					</td>
      					<td class="tax-cgst">
      						
      					</td>
      					<td class="tax-sgst">
      						
      					</td>
      					<td class="total-cost">
      						
      					</td>
      				</tr>
      			</tbody>
      		</table>
      	</div>
      	{{ csrf_field() }}
      	<input type="hidden" name="subscription_id" value="">
      	<div class="upgrade-date">
	        <p style="display:inline-block;vertical-align: top;">Start free subcription : </p>
	        <div class="form-group" style="display:inline-block;">
	        	<div>
	        		<label class="radio-inline">
					<input type="radio" class="flat-green" value="now" name="subscription_date" checked="checked">
					Immediately
					</label>
					<label class="radio-inline">
						<input type="radio" class="flat-green" value="after" name="subscription_date">
						After current plan completion
					</label>
	        	</div>
	        	<div for="subscription_date" class="error" style="font-size:16px;"></div>
	        </div>
      	</div>
      </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pointer" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-success buy-subscription-modal-confirm-btn ladda-button" data-style="zoom-in">Pay Now</button>
      </div>
    </div>

  </div>
</div>

<script type="text/javascript" src="/js/site/subscription.js"></script>
<script>
	var subscription_arr = <?php echo(json_encode($subscriptions)) ?>;
	var cgst_percent = <?php echo(env('CGST_PERCENT')) ?>;
	var sgst_percent = <?php echo(env('SGST_PERCENT')) ?>;
	var home_route = "{{route('home')}}";
</script>
@stop