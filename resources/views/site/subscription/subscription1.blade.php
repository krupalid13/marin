@extends('site.index')

@section('content')
<div id="pricing-page content-section-wrapper sm-filter-space">
	<div class="section-1">
		<div class="pricing-overlay"></div>
		<div class="page-title">
			SUBSCRIPTION
		</div>
	</div>
	<div class="section-2">
		<div class="container">
			<div class="row">
				@foreach( $subscriptions as $subscription )
					@if($subscription['duration_title'] != 'Free')
					<div class="col-xs-12 col-sm-6 col-md-4">
						
						<div class="pricing-card {{ $subscription['duration'] == 3 ? 'main-card' : '' }}">
							<div class="pricing-head">
								<div class="rate">
									<i class="fa fa-inr" aria-hidden="true"></i>
									<div class="value">{{ \CommonHelper::moneyFormatIndia($subscription['amount']) }}K</div>
									<div class="period">{{$subscription['duration_title']}}</div>
								</div>
								<div class="rate-discription">
									Lorem ipsum dolor sit ametes amet tibique sit te siggnissum sit amet ia amet tibique
								</div>
							</div>
							<div class="included-content">
								@foreach( $subscription['subscription_features'] as $feature )
								<div class="point">
									<span class="tick"><i class="fa fa-check-circle" aria-hidden="true"></i></span>
									<span class="point-discription">
										{{ $feature['title'] }}
									</span>
								</div>
								@endforeach
							</div>
							<div class="purchase-btn-container">
								<a class="btn coss-inverse-btn pointer purchase-btn buy-feature-button" data-subscription-id="{{$subscription['id']}}">Purchase</a>
							</div>
						</div>
						
					</div>
					@endif
				@endforeach
				<!-- <div class="col-xs-12 col-sm-6 col-md-4">
					<div class="pricing-card main-card">
						<div class="pricing-head">
							<div class="rate">
								<i class="fa fa-inr" aria-hidden="true"></i>
								<div class="value">42K</div>
								<div class="period">Quaterly</div>
							</div>
							<div class="rate-discription">
								Lorem ipsum dolor sit ametes amet tibique sit te siggnissum sit amet ia amet tibique
							</div>
						</div>
						<div class="included-content">
							<div class="point">
								<span class="tick"><i class="fa fa-check-circle" aria-hidden="true"></i></span>
								<span class="point-discription">
									Est aperiri ancillae ea facete liberav
								</span>
							</div>
							<div class="point">
								<span class="tick"><i class="fa fa-check-circle" aria-hidden="true"></i></span>
								<span class="point-discription">
									Est aperiri ancillae ea facete
								</span>
							</div>
							<div class="point">
								<span class="tick"><i class="fa fa-check-circle" aria-hidden="true"></i></span>
								<span class="point-discription">
									Est aperiri ancillae ea facete liberav
								</span>
							</div>
							<div class="point">
								<span class="tick"><i class="fa fa-check-circle" aria-hidden="true"></i></span>
								<span class="point-discription">
									Est aperiri ancillae ea facete
								</span>
							</div>
							<div class="point">
								<span class="tick"><i class="fa fa-check-circle" aria-hidden="true"></i></span>
								<span class="point-discription">
									Est aperiri ancillae ea facete liberav
								</span>
							</div>
						</div>
						<div class="purchase-btn-container">
							<a class="btn coss-primary-btn pointer purchase-btn">Purchase</a>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-4">
					<div class="pricing-card">
						<div class="pricing-head">
							<div class="rate">
								<i class="fa fa-inr" aria-hidden="true"></i>
								<div class="value">65K</div>
								<div class="period">Yearly</div>
							</div>
							<div class="rate-discription">
								Lorem ipsum dolor sit ametes amet tibique sit te siggnissum sit amet ia amet tibique
							</div>
						</div>
						<div class="included-content">
							<div class="point">
								<span class="tick"><i class="fa fa-check-circle" aria-hidden="true"></i></span>
								<span class="point-discription">
									Est aperiri ancillae ea facete liberav
								</span>
							</div>
							<div class="point">
								<span class="tick"><i class="fa fa-check-circle" aria-hidden="true"></i></span>
								<span class="point-discription">
									Est aperiri ancillae ea facete
								</span>
							</div>
							<div class="point">
								<span class="tick"><i class="fa fa-check-circle" aria-hidden="true"></i></span>
								<span class="point-discription">
									Est aperiri ancillae ea facete liberav
								</span>
							</div>
							<div class="point">
								<span class="tick"><i class="fa fa-check-circle" aria-hidden="true"></i></span>
								<span class="point-discription">
									Est aperiri ancillae ea facete
								</span>
							</div>
							<div class="point">
								<span class="tick"><i class="fa fa-check-circle" aria-hidden="true"></i></span>
								<span class="point-discription">
									Est aperiri ancillae ea facete liberav
								</span>
							</div>
						</div>
						<div class="purchase-btn-container">
							<a class="btn coss-inverse-btn pointer purchase-btn">Purchase</a>
						</div>
					</div>
				</div> -->
			</div>
		</div>
	</div>
	<div class="section-3">
		<div class="container">
			<div class="subcription-title">
				<span class="icon"><i class="fa fa-rss" aria-hidden="true"></i></span>
				<span class="subcription-content">Or Continue with Free Subscription</span>
			</div>
			<div class="subcription-discription">
				<span class="spcl">Lorem ipsum</span> dolor sit amet, ea amet tibique splendide est, sit te dignissim sit amet, ea amet tibique splendide est, si 
			</div>
			<div>
				<div class="row">
					<div class="col-xs-12 col-sm-6 col-md-4 col-md-offset-2">
						<div class="pricing-card">
							<div class="included-content">
								@foreach( $subscriptions as $subscription )
									@if($subscription['duration_title'] == 'Free')
										@foreach( $subscription['subscription_features'] as $feature )
											<div class="point point-border">
												<span class="tick"><i class="fa fa-check-circle" aria-hidden="true"></i></span>
												<span class="point-discription">
													{{ $feature['title'] }}
												</span>
											</div>
										@endforeach
									@endif
								@endforeach
							</div>
							<div class="purchase-btn-container">
								<a class="btn coss-primary-btn pointer buy-free-subscription-button ladda-button" data-style="zoom-in"  data-subscription-id="{{$subscription['id']}}">Continue With Free</a>
							</div>
						</div>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-4 text-center">
						<img class="free-subscription" src="{{asset('/images/pricing/free_subscription.png')}}" alt="">
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript" src="/js/site/subscription.js"></script>
@stop