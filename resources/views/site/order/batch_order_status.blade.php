@extends('site.index')
@section('content')
<div class="view-order-page content-section-wrapper">
	<div class="container">
		<div class="view-order-container">
		<?php 
		 	$order_date = date('d-m-Y h:i a',strtotime($order_details[0]['updated_at']));

		 	$cost = $order_details[0]['subscription_cost'];

		 	// if(isset($order_details[0]['discount']) && !empty($order_details[0]['discount'])){
    //             $discount = $order_details[0]['discount'];
    //             $discount_amt = round(($cost*$discount)/100);
    //             $cost = $cost-$discount_amt;
    //         }

			$tax = json_decode($order_details[0]['tax'],true);
			$cgst = $tax['cgst'];

			$cgst_cost = round(($cost*$cgst)/100);
			$sgst = $tax['sgst'];

			$sgst_cost = round(($cost*$sgst)/100);
			$inr_symbol = "<i class='fa fa-inr'></i>";

		 	$split_pay = '';
		 	if($order_details[0]['payment_type'] == 'split' && !empty($payment_type)){
		 		$split_pay = $payment_type;

                $cgst_cost = round(($cost*$cgst)/100);
                $sgst_cost = round(($cost*$sgst)/100);
                $inr_symbol = "<i class='fa fa-inr'></i>";

                $total = $cost+$cgst_cost+$sgst_cost;

                if(isset($order_details[0]['payment_type']) && $order_details[0]['payment_type'] == 'split'){
                    $initial_per = $order_details[0]['batch_details']['initial_per'];
                    $pening_per = $order_details[0]['batch_details']['pending_per'];

                    $batch_cost = $cost;

                    $initial_amt_without_tax = round(($batch_cost*$initial_per)/100);
                    $pending_amt_without_tax = round(($batch_cost*$pening_per)/100);

                    $initial_tax_cgst = (($initial_amt_without_tax*$cgst)/100);
                    $initial_tax_sgst = (($initial_amt_without_tax*$sgst)/100);

                    $pending_tax_cgst = (($pending_amt_without_tax*$cgst)/100);
                    $pending_tax_sgst = (($pending_amt_without_tax*$sgst)/100);

                    $initial_amt = round(($total*$initial_per )/100);
                    $pending_amt = round(($total*$order_details[0]['batch_details']['pending_per'])/100);
                }
		 		//dd($order_details[0]);

                if($payment_type == '1'){
                	$paid_price = $initial_amt_without_tax;
                	$total_paid = $paid_price+$initial_tax_cgst+$initial_tax_sgst;
                	$total_tax_paid = $initial_tax_cgst+$initial_tax_sgst;

                	$cgst_cost = $initial_tax_cgst;
                	$sgst_cost = $initial_tax_sgst;

                	$pending_amt_to_paid = $pending_amt;

                }

                if($payment_type == '2'){
                	$paid_price = $pending_amt_without_tax;
                	$total_paid = $paid_price+$pending_tax_cgst+$pending_tax_sgst;
                	$total_tax_paid = $pending_tax_cgst+$pending_tax_sgst;

                	$cgst_cost = $pending_tax_cgst;
                	$sgst_cost = $pending_tax_sgst;
                }
		 	}
		?>
			<div class="order-date">
				Order date : {{$order_date}}
			</div>
			<div class="view-order-heading">
				@if($order_details[0]['status'] == 1)
					Payment Done Successfully.
				@elseif($order_details[0]['status'] == 6)
					Partial Payment Done Successfully.
				@else 
					Payment Failed.
				@endif
			</div>
			<div class="view-order-text">
				@if($order_details[0]['status'] == 1)
					Your payment for the course seat booking was done succesfully. To view your order details click on <a href="{{route('user.bookings')}}">My Bookings</a>. Below are you payment details.
				@elseif($order_details[0]['status'] == 6)
					Your partial payment for the course seat booking was done succesfully. To view your order details click on <a href="{{route('user.bookings')}}">My Bookings</a>. Below are you payment details.
				@else
					Your partial payment for the course seat booking was failed. To view your order details click on <a href="{{route('user.bookings')}}">My Bookings</a>. Below are you payment details.
				@endif
			</div>
			<div class="view-order-sub-heading">
				<div class="sub-content">
					Order Id : {{$order_details[0]['id']}}
				</div>
			</div>
			<div class="table-responsive">
	      		<table class="table table-striped table-hover">
	      			<thead>
	      				<tr>
	      					<th>
	      						Institute Name
	      					</th>
	      					<th>
	      						Course Name
	      					</th>

	      					<th>
	      						Start Date
	      					</th>
	      					<th>
	      						Duration
	      					</th>
	      					<th>
	      						Price 
	      					</th>
	      				</tr>
	      			</thead>
	      			<tbody>
	      				<tr>
	      					<td>
	      						{{ isset($order_details[0]['batch_details']['course_details']['institute_registration_detail']['institute_name']) ? $order_details[0]['batch_details']['course_details']['institute_registration_detail']['institute_name'] : '-'}}
	      					</td>
	      					<td>
      							{{ isset($order_details[0]['batch_details']['course_details']['courses'][0]['course_name']) ? $order_details[0]['batch_details']['course_details']['courses'][0]['course_name'] : '-'}}
	      					</td>
	      					<?php 
	      						$tax = json_decode($order_details[0]['tax'],true);
	      						$cgst = $tax['cgst'];
	      						$sgst = $tax['sgst'];
	      					 ?>
	      					<td>
	      						{{ isset($order_details[0]['batch_details']['start_date']) ? date('d-m-Y',strtotime($order_details[0]['batch_details']['start_date'])) : '-'}}
      						
	      					</td>
	      					<td>
	      						{{ isset($order_details[0]['batch_details']['duration']) ? $order_details[0]['batch_details']['duration'] : '-'}} Days
	      					</td>
	      					<td>
	      						<i class="fa fa-inr"></i>
	      						{{ isset($cost) ? $cost : ''}}
	      					</td>
	      				</tr>
	      				<tr>
	      					<td colspan="5">
	      						<div class="row">
									<div class="col-xs-6 col-sm-10">
	      								<div class="text-right" style="font-weight: bold;">
		      								<div>
		      									Subtotal
		      								</div>
	      								</div>
	      							</div>
	      							<div class="col-xs-6 col-sm-2">
		      							<div>
	      									<i class="fa fa-inr"></i> 
	      									@if(isset($paid_price) && !empty($paid_price))
				      							{{$paid_price}}
				      						@else
				      							{{ isset($cost) ? $cost : ''}}
				      						@endif
		      							</div>
	      							</div>
								</div>
								<div class="row">
	      							<div class="col-xs-6 col-sm-10">
	      								<div class="text-right" style="font-weight:bold;">
		      								<div>
		      									Tax <a href="#" data-toggle="tooltip" data-placement="top" title="Cgst({{$cgst}}%):Rs {{$cgst_cost}} Sgst({{$sgst}}%):Rs {{$sgst_cost}}"><i class="fa fa-question-circle" aria-hidden="true" style="color:black"></i></a>
		      								</div>
	      								</div>
	      							</div>
	      							<div class="col-xs-6 col-sm-2">
		      							<div>
		      								<i class="fa fa-inr"></i> 
		      								@if(isset($total_tax_paid) && !empty($total_tax_paid))
				      							{{$total_tax_paid}}
				      						@else
		      									{{$order_details[0]['tax_amount']}}
				      						@endif
		      							</div>
	      							</div>
	      						</div>
	      					</td>
	      				</tr>
	      				<tr>
	      					<td colspan="5">
	      						<div class="row">
									<div class="col-xs-6 col-sm-10">
	      								<div class="text-right" style="font-weight: bold;">
		      								<div>
		      									Total
		      								</div>
	      								</div>
	      							</div>
	      							<div class="col-xs-6 col-sm-2">
		      							<div>
	      									<i class="fa fa-inr"></i>
	      									@if(isset($total_paid) && !empty($total_paid))
				      							{{$total_paid}}
				      						@else
				      							{{$order_details[0]['total']}}
				      						@endif

		      							</div>
	      							</div>
								</div>
	      					</td>
	      				</tr>

  						@if(isset($pending_amt_to_paid) && !empty($pending_amt_to_paid))
  						<tr>
	      					<td colspan="5">
		  						<div class="row">
									<div class="col-xs-6 col-sm-10">
		  								<div class="text-right" style="font-weight: bold;">
		      								<div>
		      									Payment Made
		      								</div>
		  								</div>
		  							</div>
		  							<div class="col-xs-6 col-sm-2">
		      							<div>
		  									<i class="fa fa-inr"></i> 
				      							@if(isset($total_paid) && !empty($total_paid))
					      							{{$total_paid}}
					      						@else
					      							{{$order_details[0]['total']}}
					      						@endif
		      							</div>
		  							</div>
								</div>
							</td>
						</tr>
						<tr>
	      					<td colspan="5">
		  						<div class="row">
									<div class="col-xs-6 col-sm-10">
		  								<div class="text-right" style="font-weight: bold;">
		      								<div>
		      									Pending Amount
		      								</div>
		  								</div>
		  							</div>
		  							<div class="col-xs-6 col-sm-2">
		      							<div>
		  									<i class="fa fa-inr"></i> 
				      							{{$pending_amt_to_paid}}
		      							</div>
		  							</div>
								</div>
							</td>
						</tr>
		      			@endif

		      			@if($order_details[0]['status'] == '1')
                            @if(isset($order_details[0]['order_payments'][1]['amount']))
	                            <tr>
			      					<td colspan="5">
				  						<div class="row">
											<div class="col-xs-6 col-sm-10">
				  								<div class="text-right" style="font-weight: bold;">
				      								<div>
				      									Initital Payment Made
				      								</div>
				  								</div>
				  							</div>
				  							<div class="col-xs-6 col-sm-2">
				      							<div>
				  									<i class="fa fa-inr"></i> 
						      							{{isset($order_details[0]['order_payments'][1]['amount']) ? $order_details[0]['order_payments'][1]['amount'] : '-'}}
				      							</div>
				  							</div>
										</div>
									</td>
								</tr>
								<tr>
			      					<td colspan="5">
				  						<div class="row">
											<div class="col-xs-6 col-sm-10">
				  								<div class="text-right" style="font-weight: bold;">
				      								<div>
				      									Pending Payment Made
				      								</div>
				  								</div>
				  							</div>
				  							<div class="col-xs-6 col-sm-2">
				      							<div>
				  									<i class="fa fa-inr"></i> 
						      							{{isset($order_details[0]['order_payments'][0]['amount']) ? $order_details[0]['order_payments'][0]['amount'] : '-'}}
				      							</div>
				  							</div>
										</div>
									</td>
								</tr>
                            @endif
                            <tr>
		      					<td colspan="5">
			  						<div class="row">
										<div class="col-xs-6 col-sm-10">
			  								<div class="text-right" style="font-weight: bold;">
			      								<div>
			      									Total Payment Made
			      								</div>
			  								</div>
			  							</div>
			  							<div class="col-xs-6 col-sm-2">
			      							<div>
			  									<i class="fa fa-inr"></i> 
					      							{{isset($order_details[0]['total']) ? $order_details[0]['total'] : '-'}}
			      							</div>
			  							</div>
									</div>
								</td>
							</tr>
                        @endif
	      			</tbody>
	      		</table>
	      	</div>
	      	@if($order_details[0]['order_payments'][0]['message']['error_code'])
	      	<div class="msg">
	      		{{$order_details[0]['order_payments'][0]['message']['message']}}
	      	</div>
	      	@endif
		</div>
	</div>
</div>
<script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip(); 
});
</script>

@stop