@extends('site.index')
@section('content')
<div class="view-order-page content-section-wrapper">
	<div class="container">
		<div class="view-order-container">
		<?php 
		 $order_date = date('d-m-Y h:i a',strtotime($order_details[0]['updated_at']));
		 ?>
			<div class="order-date">
				Order date : {{$order_date}}
			</div>
			<div class="view-order-heading">
			@if($order_details[0]['status'] == 1)
				Subscription Payment Success.
			@else 
				Subscription Payment Failed.
			@endif
			</div>
			<div class="view-order-sub-heading">
				<div class="sub-content">
					Order Id : {{$order_details[0]['id']}}
				</div>
				<!-- <div class="sub-content" style="color:#ababab;">
					Transaction Id : {{$order_details[0]['order_payment']['transaction_id']}}
				</div> -->
			</div>
			<div class="table-responsive">
	      		<table class="table table-striped table-hover">
	      			<thead>
	      				<tr>
	      					<th>
	      						Plan Name
	      					</th>
	      					<th>
	      						Duration
	      					</th>

	      					<th>
	      						Start Date
	      					</th>
	      					<th>
	      						End Date
	      					</th>
	      					<th>
	      						Price 
	      					</th>
	      				</tr>
	      			</thead>
	      			<tbody>
	      				<tr>
	      					<td>
	      					@if($order_details[0]['status'] == 1)
	      						{{ isset($order_details[0]['my_subscription']['subscription_details']['title']) ? $order_details[0]['my_subscription']['subscription_details']['title'] : ''}}
      						@else
								{{ isset($order_details[0]['subscription']['title']) ? $order_details[0]['subscription']['title'] : ''}}
      						@endif
	      					</td>
	      					<td>
      						@if($order_details[0]['status'] == 1)
	      						{{ isset($order_details[0]['my_subscription']['subscription_details']['duration']) ? $order_details[0]['my_subscription']['subscription_details']['duration'] : ''}} month
      						@else
								{{ isset($order_details[0]['subscription']['duration']) ? $order_details[0]['subscription']['duration'] : ''}} month
      						@endif
	      					</td>
	      					<?php 
	      						$tax = json_decode($order_details[0]['tax'],true);
	      						$cgst = $tax['cgst'];
	      						$sgst = $tax['sgst'];
	      					 ?>
	      					<td>
	      					@if($order_details[0]['status'] == 1)
	      						{{ isset($order_details[0]['my_subscription']['valid_from']) ? date('d-m-Y',strtotime($order_details[0]['my_subscription']['valid_from'])) : ''}}
      						@else
								-
      						@endif
	      					</td>
	      					<td>
	      					@if($order_details[0]['status'] == 1)
	      						{{ isset($order_details[0]['my_subscription']['valid_to']) ? date('d-m-Y',strtotime($order_details[0]['my_subscription']['valid_to'])) : ''}}
      						@else
								-
      						@endif
	      					</td>
	      					<td>
	      						<i class="fa fa-inr"></i> {{ isset($order_details[0]['subscription_cost']) ? $order_details[0]['subscription_cost'] : ''}}
	      					</td>
	      				</tr>
	      				<?php 
      						$tax = json_decode($order_details[0]['tax'],true);
      						$cgst = $tax['cgst'];
      						$cgst_cost = ($order_details[0]['subscription_cost']*$cgst)/100;
      						$sgst = $tax['sgst'];
      						$sgst_cost = ($order_details[0]['subscription_cost']*$sgst)/100;
      						$inr_symbol = "<i class='fa fa-inr'></i>";
      					 ?>
	      				<tr>
	      					<td colspan="5">
	      						<div class="row">
									<div class="col-xs-6 col-sm-10">
	      								<div class="text-right" style="font-weight: bold;">
		      								<div>
		      									Subtotal
		      								</div>
	      								</div>
	      							</div>
	      							<div class="col-xs-6 col-sm-2">
		      							<div>
	      									<i class="fa fa-inr"></i> {{$order_details[0]['subscription_cost']}}
		      							</div>
	      							</div>
								</div>
								<div class="row">
	      							<div class="col-xs-6 col-sm-10">
	      								<div class="text-right" style="font-weight:bold;">
		      								<div>
		      									Tax <a href="#" data-toggle="tooltip" data-placement="top" title="Cgst({{$cgst}}%):Rs {{$cgst_cost}} Sgst({{$sgst}}%):Rs {{$sgst_cost}}"><i class="fa fa-question-circle" aria-hidden="true" style="color:black"></i></a>
		      								</div>
	      								</div>
	      							</div>
	      							<div class="col-xs-6 col-sm-2">
		      							<div>
		      								<i class="fa fa-inr"></i> {{$order_details[0]['tax_amount']}}
		      							</div>
	      							</div>
	      						</div>
	      					</td>
	      				</tr>
	      				<tr>
	      					<td colspan="5">
	      						<div class="row">
									<div class="col-xs-6 col-sm-10">
	      								<div class="text-right" style="font-weight: bold;">
		      								<div>
		      									Total
		      								</div>
	      								</div>
	      							</div>
	      							<div class="col-xs-6 col-sm-2">
		      							<div>
	      									<i class="fa fa-inr"></i> {{$order_details[0]['total']}}
		      							</div>
	      							</div>
								</div>
	      					</td>
	      				</tr>
	      			</tbody>
	      		</table>
	      	</div>
	      	@if($order_details[0]['order_payment']['message']['error_code'])
	      	<div class="msg">
	      		{{$order_details[0]['order_payment']['message']['message']}}
	      	</div>
	      	@endif
		</div>
	</div>
</div>
<script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip(); 
});
</script>

@stop