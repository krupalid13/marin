@extends('site.index')
@section('content')
<?php
    $initialize_pincode_maxlength_validation = false;
    $india_value = array_search('India',\CommonHelper::countries());
?>
<div class="user-registration content-section-wrapper">
	<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<form id="advertise-login-form" method="POST">
						{{ csrf_field() }}
						<div class="user-details-container">
							<div class="row">
								<div class="col-sm-12">
									<div class="main-heading">
										Login Details<span class="symbol required"></span>
									</div>
									<div class="row">
										<div class="col-sm-6">
											<div class="heading">
												PROFILE PICTURE
											</div>
											<div class="upload-photo-container">

							                    <input type="hidden" name="image-x" required>
							                    <input type="hidden" name="image-y" required>
							                    <input type="hidden" name="image-x2" required>
							                    <input type="hidden" name="image-y2" required>
							                    <input type="hidden" name="crop-w" required>
							                    <input type="hidden" name="crop-h" required>
							                    <input type="hidden" name="image-w" required>
							                    <input type="hidden" name="image-h" required>
							                    <input type="hidden" name="role" value='advertiser'>
			                                    <input type="hidden" name="uploaded-file-name" required>
			                                    <input type="hidden" name="uploaded-file-path" required>

			                                    <input type="hidden" id="advertise_id" value="{{isset($user_data['user'][0]['id']) ? $user_data['user'][0]['id'] : ''}}">

												<div class="image-content">
													<div class="registration-profile-image" style="height: 100%;width: 100%;">
														<?php
															$image_path = '';
															if(isset($user_data['user'][0]['profile_pic'])){
																$image_path = "/".env('SEAFARER_PROFILE_PATH')."".$user_data['user'][0]['id']."/".$user_data['user'][0]['profile_pic'];
															}
														?>

														@if(empty($image_path))
															<div class="icon profile_pic_text"><i class="fa fa-camera" aria-hidden="true"></i></div>
															<div class="image-text profile_pic_text">Upload Profile <br> Picture</div>
														@endif
														
														<input type="file" style="z-index: 9;" name="profile_pic" id="profile-pic" class="cursor-pointer" onChange="profilePicSelectHandler('advertise-login-form', 'profile_pic', 'pic', 'seafarer')">

														@if(!empty($image_path))
														 	<img id="preview" style="border-radius: 50%;width: 100%;" src="{{ $image_path }}">
														 	<!-- <div class="image-text profile_pic_text" style="position: absolute;
														 	    top: 90px;
														 	    left: 4px;
														 	    width: 150px;">Change Profile <br> Picture</div> -->
		                                                @else                
														 	<img id="preview" style="border-radius: 50%">
		                                                @endif
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-6">
											<div class="form-group">
							                    <label class="input-label" for="firstname">Owner Name<span class="symbol required"></span></label>
						    	                <input type="text" class="form-control" id="firstname" name="firstname" value="{{ isset($user_data['user'][0]['first_name']) ? $user_data['user'][0]['first_name'] : ''}}" placeholder="Type your full name">
							                 </div>
										</div>
										<div class="col-sm-6">
											<div class="form-group">
							                    <label class="input-label" for="mobile">Owner Mobile Number<span class="symbol required"></span></label>
							                    <input type="text" class="form-control" id="mobile" name="mobile" value="{{ isset($user_data['user'][0]['mobile']) ? $user_data['user'][0]['mobile'] : ''}}" placeholder="Type your mobile number">
							                    <span class="hide" id="mobile-error" style="color: red"></span>
							                 </div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-4">
											<div class="form-group">
							                    <label class="input-label" for="email">Owner Email Address<span class="symbol required"></span></label>
							                    <input type="text" class="form-control" id="email" name="email" placeholder="Type your email address" 
							                    value="{{ isset($user_data['user'][0]['email']) ? $user_data['user'][0]['email'] : ''}}">
							                    <span class="hide" id="email-error" style="color: red"></span>
							                </div>
										</div>
										@if(!isset($user_data['current_route']))
										<div class="col-sm-4">
											<div class="form-group">
							                    <label class="input-label" for="password">Password<span class="symbol required"></span></label>
							                    <input type="password" class="form-control" id="password" name="password" placeholder="Type your password">
							                 </div>
										</div>
										<div class="col-sm-4">
											<div class="form-group">
							                    <label class="input-label" for="cpassword">Confirm Password<span class="symbol required"></span></label>
							                    <input type="password" class="form-control" id="cpassword" name="cpassword" placeholder="Type password again">
							                 </div>
										</div>
										@else
											<input type="hidden" id='user_id' name='user_id' value='{{$user_data['user'][0]['id']}}'>
										@endif
									</div>
								</div>
							</div>
						</div>
					</form>

					<form id="advertise-company-form" method="POST">
						{{ csrf_field() }}
						<div id="company-details-form">
							
							<input type="hidden" name="image-x" required>
		                    <input type="hidden" name="image-y" required>
		                    <input type="hidden" name="image-x2" required>
		                    <input type="hidden" name="image-y2" required>
		                    <input type="hidden" name="crop-w" required>
		                    <input type="hidden" name="crop-h" required>
		                    <input type="hidden" name="image-w" required>
		                    <input type="hidden" name="image-h" required>
		                    <input type="hidden" name="role" value='advertiser'>
                            <input type="hidden" name="uploaded-file-name" required>
                            <input type="hidden" name="uploaded-file-path" required>

		                    <div class="user-details-container">
		                        <div class="row">
		                            <div class="col-sm-12">
		                                <div class="heading">
		                                    COMPANY DETAILS<span class="symbol required"></span>
		                                </div>
		                            </div>
		                        </div>
	                        <div class="upload-photo-container upload-photo-container-company">
		                            {{ csrf_field() }}
		                            <div class="image-content">
		                                <div class="registration-profile-image">
		                                    <?php
		                                    $image_path = '';
		                                    if(isset($user_data['company_data'][0]['company_registration_detail']['company_logo'])){
		                                        $image_path = env('COMPANY_LOGO_PATH')."".$user_data['company_data'][0]['id']."/".$user_data['company_data'][0]['company_registration_detail']['company_logo'];
		                                    }
		                                    ?>
		                                    @if(empty($image_path))
		                                        <div class="icon profile_pic_text"><i class="fa fa-camera" aria-hidden="true"></i></div>
		                                        <div class="image-text profile_pic_text">Upload Logo <br> Picture</div>
		                                    @endif

		                                    <input type="file" name="profile_pic" id="profile-pic" class="cursor-pointer" onChange="profilePicSelectHandler('advertise-company-form', 'profile_pic', 'logo' , 'company')">

		                                    @if(!empty($image_path))
		                                        <img id="preview" class="preview" src="/{{ $image_path }}">
		                                    @else
		                                        <img id="preview">
		                                    @endif
		                                </div>
		                            </div>
		                        </div>
		                        <div class="row">
		                            <div class="col-sm-6">
		                                <div class="form-group">
		                                    <label class="input-label" for="company_name">Company Name<span class="symbol required"></span></label>
		                                    <input type="text" class="form-control" id="company_name" name="company_name" placeholder="Type company name" value="{{ isset($user_data['company_data'][0]['company_registration_detail']['company_name']) ? $user_data['company_data'][0]['company_registration_detail']['company_name'] : ''}}">
		                                </div>
		                            </div>
		                            <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="input-label">
                                                Company Website
                                            </label>
                                            <div class="input-group">
                                                <span class="input-group-addon http-label">http://</span>
                                                    <input type="text" class="form-control p-l-5" id="website" name="website" placeholder="Type your website address" value="{{ isset($user_data['company_data'][0]['company_registration_detail']['website']) ? $user_data['company_data'][0]['company_registration_detail']['website'] : ''}}">
                                            </div>
                                            <label for="website" class="error"></label>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-6">
                                    	<div class="form-group">
                                            <label class="input-label">
                                                Country<span class="symbol required"></span>
                                            </label>
                                            <select id='country' name="country[0]" class="form-control country" block-index="0">
                                                <option value=''>Select Your Country</option>
                                                @foreach( \CommonHelper::countries() as $c_index => $country)
                                                    @if(isset($user_data['company_data'][0]['company_registration_detail']['company_locations'][0]['country']))
                                                        <option value="{{ $c_index }}" {{ isset($user_data['company_data'][0]['company_registration_detail']['company_locations'][0]['country']) ? $user_data['company_data'][0]['company_registration_detail']['company_locations'][0]['country'] == $c_index ? 'selected' : '' : ''}}> {{ $country }}</option>
                                                    @else
                                                        <option value="{{ $c_index }}" {{ $india_value == $c_index ? 'selected' : ''}}> {{ $country }}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-6">
	                                    <div class="form-group">
	                                        <label class="input-label">
	                                            Postal Code<span class="symbol required"></span>
	                                        </label>
	                                        <div class="pincode-block-0">
	                                            <input type="hidden" class="pincode-id" name="pincode_id[0]" value="{{ isset($user_data['company_data'][0]['company_registration_detail']['company_locations'][0]['pincode_id']) ? $user_data['company_data'][0]['company_registration_detail']['company_locations'][0]['pincode_id'] : ''}}">
	                                            
	                                            <i class="fa fa-spin fa-refresh select-loader hide pincode-loader-0"></i>
	                                            <input type="text" block-index="0" id='pincode' data-form-id="advertise-company-form" class="form-control pincode pin_code_fetch_input" name="pincode[0]" value="{{ isset($user_data['company_data'][0]['company_registration_detail']['company_locations'][0]['pincode_text']) ? $user_data['company_data'][0]['company_registration_detail']['company_locations'][0]['pincode_text'] : ''}}" placeholder="Type your pincode">
	                                        </div>
	                                    </div>
                                    </div>
                                </div>
                                <div class="row">
                                	<div class="col-sm-6">
	                                    <div class="form-group">
	                                        <label class="input-label">
	                                            State<span class="symbol required"></span>
	                                        </label>
	                                        <div class="state-block state-block-0">
	                                            @if(isset($user_data['company_data'][0]['company_registration_detail']['company_locations'][0]))
	                                            @if($user_data['company_data'][0]['company_registration_detail']['company_locations'][0]['country'] == $india_value )
	                                                <select id="state" name="state[0]" class="form-control state fields-for-india">
	                                                    <option value="">Select Your State</option>
	                                                    @foreach($user_data['company_data'][0]['company_registration_detail']['company_locations'][0]['pincode']['pincodes_states'] as $pincode_states)
	                                                        <option value="{{$pincode_states['state_id']}}" {{$pincode_states['state_id'] == $user_data['company_data'][0]['company_registration_detail']['company_locations'][0]['state_id'] ? 'selected' : ''}}>{{ucwords(strtolower($pincode_states['state']['name']))}}</option>
	                                                    @endforeach
	                                                </select>
	                                                <input type="text" id="state" name="state_text[0]" class="form-control state_text hide fields-not-for-india" placeholder="Enter State Name" value="">

	                                            @else
	                                                <select id="state" name="state[0]" class="form-control state hide fields-for-india">
	                                                    <option value="">Select Your State</option>
	                                                </select>
	                                                <input type="text" id="state" name="state_text[0]" class="form-control state_text fields-not-for-india" placeholder="Enter State Name" value="{{ isset($user_data['company_data'][0]['company_registration_detail']['company_locations'][0]['state_text']) ? $user_data['company_data'][0]['company_registration_detail']['company_locations'][0]['state_text'] : ''}}">
	                                            @endif
	                                        @else
	                                            <select id="state" name="state[0]" class="form-control state fields-for-india">
	                                                <option value="">Select Your State</option>
	                                            </select>
	                                            <input type="text" id="state" name="state_text[0]" class="form-control state_text hide fields-not-for-india" placeholder="Enter State Name" value="{{ isset($user_data['company_data'][0]['company_registration_detail']['company_locations'][0]['state_text']) ? $user_data['company_data'][0]['company_registration_detail']['company_locations'][0]['state_text'] : ''}}">
	                                        @endif
	                                        </div>
	                                    </div>
	                                </div>
	                                <div class="col-sm-6">
                                    	<div class="form-group">
                                    
	                                        <label class="input-label">
	                                            City<span class="symbol required"></span>
	                                        </label>
	                                        <div class="city-block city-block-0">
	                                            @if(isset($user_data['company_data'][0]['company_registration_detail']['company_locations'][0]['country']))
	                                            @if( $user_data['company_data'][0]['company_registration_detail']['company_locations'][0]['country'] == $india_value )
	                                                <select id="city" name="city[0]" class="form-control city fields-for-india">
	                                                    <option value="">Select Your City</option>
	                                                    @foreach($user_data['company_data'][0]['company_registration_detail']['company_locations'][0]['pincode']['pincodes_cities'] as $pincode_city)
	                                                        <option value="{{$pincode_city['city_id']}}" {{$pincode_city['city_id'] == $user_data['company_data'][0]['company_registration_detail']['company_locations'][0]['city_id'] ? 'selected' : ''}}>{{ucwords(strtolower($pincode_city['city']['name']))}}</option>
	                                                    @endforeach
	                                                </select>
	                                                <input type="text" name="city_text[0]" class="form-control city_text hide fields-not-for-india" placeholder="Enter City Name" value="">

	                                            @else
	                                                <select id="city" name="city[0]" class="form-control city hide fields-for-india">
	                                                    <option value="">Select Your City</option>
	                                                </select>

	                                                {{--<input type="text" name="city_text[0]" class="form-control city_text fields-not-for-india" placeholder="Enter City Name" value="">--}}
	                                                <input type="text" name="city_text[0]" class="form-control city_text fields-not-for-india" placeholder="Enter City Name" value="{{ isset($user_data['company_data'][0]['company_registration_detail']['company_locations'][0]['city_text']) ? $user_data['company_data'][0]['company_registration_detail']['company_locations'][0]['city_text'] : ''}}">
	                                            @endif
	                                        @else
	                                            <select id="city" name="city[0]" class="form-control city fields-for-india">
	                                                <option value="">Select Your City</option>
	                                            </select>

	                                            {{--<input type="text" name="city_text[0]" class="form-control city_text fields-not-for-india" placeholder="Enter City Name" value="">--}}
	                                            <input type="text" name="city_text[0]" class="form-control city_text hide fields-not-for-india" placeholder="Enter City Name" value="{{ isset($user_data['company_data'][0]['company_registration_detail']['company_locations'][0]['city_text']) ? $user_data['company_data'][0]['company_registration_detail']['company_locations'][0]['city_text'] : ''}}">
	                                        @endif
	                                        </div>
	                                    </div>
	                                </div>
	                            </div>
                                <div class="row">
		                            <div class="col-sm-6">
		                                <div class="form-group">
		                                    <label class="input-label" for="company_address">Company Address<span class="symbol required"></label>
		                                    <textarea rows="5" class="form-control" id="company_address" name="company_address" placeholder="Type your company address" >{{ isset($user_data['company_data'][0]['company_registration_detail']['address']) ? $user_data['company_data'][0]['company_registration_detail']['address'] : ''}}</textarea>
		                                </div>
		                            </div>
									<div class="col-sm-6">
										<div class="form-group">
                                            <label class="input-label">
                                                Years In Bussiness<span class="symbol required"></span>
                                            </label>
                                            <input type="text" class="form-control" name="buss_years" placeholder="Type your years in bussiness in years" value="{{ isset($user_data['company_data'][0]['company_registration_detail']['buss_years']) ? $user_data['company_data'][0]['company_registration_detail']['buss_years'] : ''}}">
                                        </div>
                                    </div>
		                        </div>
		                        <div class="row">
		                        	<div class="col-sm-6">
		                        		<div class="form-group">
	                                        <label class="input-label">
	                                            Member of any club or association? <span class="symbol required"></span>
	                                        </label>
                                            <select name="b_i_member" class="b_i_member form-control">
                                                <option value="">Select</option>
                                                @foreach( \CommonHelper::member_category() as $c_index => $category)
                                                    <option value="{{ $c_index }}" {{ isset($user_data['company_data'][0]['company_registration_detail']['b_i_member']) ? $user_data['company_data'][0]['company_registration_detail']['b_i_member'] == $c_index ? 'selected' : '' : ''}}> {{ $category }}</option>
                                                @endforeach
                                            </select>
	                                    </div>
		                        	</div>
		                        	
		                            <?php 
                                        
                                        $show_b_i_member = 'hide';
                                        if(isset($user_data['company_data'][0]['company_registration_detail']['b_i_member']) && $user_data['company_data'][0]['company_registration_detail']['b_i_member'] == 'other'){
                                                $show_b_i_member = '';
                                            }
                                            else{
                                                $show_b_i_member = 'hide';
                                            }
                                    ?>
                                    <div class="col-sm-6">
	                                    <div class="form-group {{$show_b_i_member}}" id="other_b_i_member">
	                                        <label class="input-label">
	                                            Other Member <span class="symbol required"></span>
	                                        </label>
                                            <input type="text"  class="form-control" name="other_b_i_member" value="{{ isset($user_data['company_data'][0]['company_registration_detail']['other_bi_member']) ? $user_data['company_data'][0]['company_registration_detail']['other_bi_member'] : '' }}">
	                                    </div>
	                                </div>
                                </div>
		                        <div class="row">
		                            <div class="col-sm-6">
		                                <div class="form-group">
		                                    <label class="input-label" for="bussiness_category">Bussiness Category<span class="symbol required"></span></label>
		                                    <select name="bussiness_category" class="form-control search-select bussiness_category">
		                                        <option value="">Select Category</option>
		                                        @foreach(\CommonHelper::bussiness_category() as $index => $categories)
													<optgroup label="{{$index}}">
														@foreach($categories as $c_index => $category)
															<option value="{{ $c_index }}" {{ isset($user_data['company_data'][0]['company_registration_detail']['bussiness_category']) ? $user_data['company_data'][0]['company_registration_detail']['bussiness_category'] == $c_index ? 'selected' : '' : ''}}> {{ $category }}</option>
														@endforeach
												@endforeach
		                                    </select>
		                                </div>
		                            </div>
		                            <?php 
                                        
                                    $show_other = 'hide';
                                    if(isset($user_data['company_data'][0]['company_registration_detail']['bussiness_category']) && $user_data['company_data'][0]['company_registration_detail']['bussiness_category'] == 'other'){
                                            $show_other = '';
                                        }
                                        else{
                                            $show_other = 'hide';
                                        }
                                    ?>

		                            <div class="col-sm-6">
			                            <div class="form-group {{$show_other}}" id="other_bussiness_category">
	                                        <label class="input-label">
	                                            Other Bussiness Category<span class="symbol required"></span>
	                                        </label>
	                                        <input type="text"  class="form-control" name="other_buss_category" value="{{ isset($user_data['company_data'][0]['company_registration_detail']['other_buss_category']) ? $user_data['company_data'][0]['company_registration_detail']['other_buss_category'] : '' }}">
	                                    </div>
	                                </div>
		                        </div>
		                        <div class="row">
		                        	<div class="col-sm-6">
			                        	<div class="form-group">
	                                        <label class="input-label">
	                                            Product/Service Description<span class="symbol required"></span>
	                                        </label>
	                                        <textarea class="form-control" name="product_description" placeholder="Type your product/service description" rows="5">{{ isset($user_data['company_data'][0]['company_registration_detail']['product_description']) ? $user_data['company_data'][0]['company_registration_detail']['product_description'] : ''}}</textarea>
	                                    </div>
	                                </div>
		                        	<div class="col-sm-6">
		                                <div class="form-group">
		                                    <label class="input-label" for="bussiness_description">Bussiness Description<span class="symbol required"></span></label>
		                                    <textarea class="form-control bussiness_description" name="bussiness_description" placeholder="Type your bussiness description" rows="5">{{ isset($user_data['company_data'][0]['company_registration_detail']['bussiness_description']) ? $user_data['company_data'][0]['company_registration_detail']['bussiness_description'] : ''}}</textarea>
		                                </div>
		                            </div>
		                        </div>

		                        <div class="row">
		                        	<div class="col-sm-6">
			                        	<div class="form-group">
	                                        <label class="input-label">
	                                            Email To Be Displayed<span class="symbol required"></span>
	                                        </label>
	                                        <input type="text" class="form-control" name="email_display" placeholder="Type your email to be displayed" value="{{ isset($user_data['company_data'][0]['company_registration_detail']['email_display']) ? $user_data['company_data'][0]['company_registration_detail']['email_display'] : ''}}">
	                                    </div>
	                                </div>

	                                <div class="col-sm-6">
	                                    <div class="form-group">
	                                        <label class="input-label">
	                                            Contact Number To Be Displayed<span class="symbol required"></span>
	                                        </label>
	                                        <input type="text" class="form-control" name="contact_display" placeholder="Type your contact number to be displayed" value="{{ isset($user_data['company_data'][0]['company_registration_detail']['contact_display']) ? $user_data['company_data'][0]['company_registration_detail']['contact_display'] : ''}}">
	                                    </div>
	                                </div>
	                            </div>
								@if(Route::currentRouteName() == 'site.advertiser.registration')
	                            <div class="row">
            						<div class="col-md-6">
										  <input type="checkbox" id="agree" name="agree" value="1">
										  <label class="terms-conditions">
										    I agree with the <a href="/terms&condition" target="_blank">terms and conditions</a>.
										  </label>
										<label for="agree" class="error"></label>
									</div>
		                        </div>
								@endif
		                    </div>
		                </div>
					<div class="row">
						<div class="col-md-12">
							<div class="reg-form-btn-container text-right">
									<button type="button" data-style="zoom-in" class="reg-form-btn blue ladda-button advertise-details-submit" id="submitAdvertiserDetailButton" value="Save" data-tab-index="1">Save</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
@stop

@section('js_script')
	<script type="text/javascript" src="/js/site/advertise-registration.js"></script>
@stop
