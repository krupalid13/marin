<?php
	$india_value = array_search('India',\CommonHelper::countries());
?>

@if(!isset($user))
<div class="row">
	@if(isset($data['company_data'][0]['company_registration_detail']['company_locations'][0]['country']) && $data['company_data'][0]['company_registration_detail']['company_locations'][0]['country'] == $india_value)
	    <div class="col-sm-12 col-md-6">
	        <div class="alert alert-block alert-danger fade in hide alert-box-verification-mob">
	            Your mobile is not verified. Please verify your mobile. <a class="alert-link" href="#" id="resend-otp-button">Verify</a>
	        </div>
	    </div>
    @endif
    <div class="col-sm-12 col-md-6">
        <div class="alert alert-block alert-danger fade in hide alert-box-verification-email">
            Your email is not verified. Please verify your email. <a class="alert-link" href="#" id="email_verify">Resend Email</a>
            <i class="fa fa-spin fa-refresh resend_email_loader hide"></i>
        </div>
    </div>
</div>
@endif
<div class="row">
	<div class="col-xs-12 col-sm-8">
		@if(!isset($user))
	        <div class="company-page-profile-heading">
	            My Profile
	        </div>
	    @else
	    	<div class="company-page-profile-heading">
	            Advertiser Profile
	        </div>
	    @endif
    </div>
</div>
<div class="row">
    <div class="col-xs-12">
		<div class="section-1">
			
		</div>
	</div>
</div>
<div class="row">
    <div class="col-xs-12">
		<div class="section-2 user-imp-details">
			<div class="">
				<div class="row">
					<div class="col-sm-12 col-md-4 col-lg-3">
						<div class="photo-container">
							<div class="photo-card">
								<div>
									<div class="pic">
										@if(isset($data['user'][0]['profile_pic']))
											<img id="preview" src="/{{ env('SEAFARER_PROFILE_PATH')}}{{$data['user'][0]['id']}}/{{$data['user'][0]['profile_pic']}}" avatar-holder-src="{{ asset('images/user_default_image.png') }}">
										@else
											<img id="preview" src="{{ asset('images/user_default_image.png') }}" avatar-holder-src="{{ asset('images/user_default_image.png') }}">
										@endif
									</div>
									<div class="user-name">
										{{ isset($data['user'][0]['first_name']) ? $data['user'][0]['first_name'] : ''}}
									</div>
								</div>
							</div>
						</div>
					</div>
					<input type="hidden" name="verify_email" id="verify_email" value="{{isset($data['user'][0]['is_email_verified']) ? $data['user'][0]['is_email_verified'] : ''}}">
					<input type="hidden" name="verify_mobile" id="verify_mobile" value="{{isset($data['user'][0]['is_mob_verified']) ? $data['user'][0]['is_mob_verified'] : ''}}">
					<?php
						$p5 = '';
						if(isset($user)){
							$p5 = 'p-5';
						}
					?>
					<div class="col-xs-10 col-sm-11 col-md-6 col-lg-7 {{$p5}}">
						<div class="imp-details-container">
							<div class="detail">
								@if(!isset($user))
									Email ID: {{ isset($data['user'][0]['email']) ? $data['user'][0]['email'] : ''}}
								@endif
							</div>
							<div class="detail">
								@if(!isset($user))
									Mobile Number: {{ isset($data['user'][0]['mobile']) ? $data['user'][0]['mobile'] : '' }}
								@endif
							</div>
							
						</div>
					</div>
					@if(!isset($user))
					<div class="col-xs-2 col-sm-1 col-md-2 col-lg-1">
						<div class="edit-btn-container">
							<a href="{{ route('site.advertiser.edit.profile') }}"><span class="profile-edit-btn-sm visible-xs-block visible-sm-block"><i class="fa fa-pencil-square-o" aria-hidden="true" title="Edit profile"></i></span>
							<button class="profile-edit-btn hidden-xs hidden-sm">
								Edit Profile
							</button>
							</a>
						</div>
					</div>
					@endif
				</div>
			</div>
		</div>
	</div>
</div>
<div class="row">
    <div class="col-xs-12">
		<div class="section-3">
			<div class="row">
				<div class="col-sm-12 col-md-4 col-lg-3"></div>
				<div class="col-sm-12 col-md-8 col-lg-9">
					<div class="sub-details-container">
						<div class="content-container content-height">
							<div class="row">
								<div class="col-sm-12">
									<div class="title">COMPANY DETAILS</div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-7">
									<div class="discription">
										<span class="content-head">Company Name:</span>
										<span class="content">
											<span class="content">{{ isset($data['company_data'][0]['company_registration_detail']['company_name']) ? $data['company_data'][0]['company_registration_detail']['company_name'] : ''}}</span>
										</span>
									</div>
								</div>
								<div class="col-sm-5">
									<div class="discription">
										<span class="content-head">Address:</span>
										<span class="content">
											<span class="content">{{ isset($data['company_data'][0]['company_registration_detail']['address']) ? $data['company_data'][0]['company_registration_detail']['address'] : ''}}</span>
                                        </span>
									</div>
								</div>
							</div>
							
							<div class="row">
								@if(!isset($user))
								<div class="col-sm-7">
									<div class="discription">
										<span class="content-head">Member of any club or association:</span>
										<span class="content">
											<?php
											$bi = '';
												if(!empty($data['company_data'][0]['company_registration_detail']['b_i_member'])){
													$bi = $data['company_data'][0]['company_registration_detail']['b_i_member'];
												}
											?>
											@if($bi == 'other')
												{{ isset($data['company_data'][0]['company_registration_detail']['other_bi_member']) ? $data['company_data'][0]['company_registration_detail']['other_bi_member'] : '-'}}
											@else
												@foreach( \CommonHelper::member_category() as $c_index => $category)
	                                                {{ isset($bi) ? $bi == $c_index ? $category : '' : ''}}
	                                            @endforeach
	                                        @endif
										</span>
									</div>
								</div>
								@endif
								<div class="col-sm-5">
									<div class="discription">
										<span class="content-head">Bussiness Category:</span>
										<span class="content">
											@foreach(\CommonHelper::bussiness_category() as $index => $categories)
												@foreach( $categories as $c_index => $category)
		                                            {{ isset($data['company_data'][0]['company_registration_detail']['bussiness_category']) ? $data['company_data'][0]['company_registration_detail']['bussiness_category'] == $c_index ? $category : '' : ''}}
		                                        @endforeach
											@endforeach
											
										</span>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-7">
									<div class="discription">
										<span class="content-head">Bussiness Description:</span>
										<span class="content">{{ isset($data['company_data'][0]['company_registration_detail']['bussiness_description']) ? $data['company_data'][0]['company_registration_detail']['bussiness_description'] : ''}}</span>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12">
					<div class="details-main-heading">
						ADVERTISEMENTS
						@if(!isset($user))
						<a href="{{route('site.advertiser.add.advertise')}}">
							<button class="btn pull-right edit_link" style="color: #fff;background-color: #47c4f0;border: 1px solid;">
								<i class="fa fa-plus"> Add Advertisement</i>
							</button>
						</a>
						@endif
					</div>
					<div class="sub-details-container">
						<div class="content-container">
							@if(isset($data['user'][0]['company_registration_detail']['advertisment_details']) && !empty($data['user'][0]['company_registration_detail']['advertisment_details']))
								<div class="row hidden-xs p-b-15">
									<div class="col-sm-5 col-md-4">
										<th>Small Advertisement</th>
									</div>
									<div class="col-sm-4 col-md-4">
										<th>Big Advertisement</th>
									</div>
									@if(!isset($user))
									<div class="col-sm-3 col-md-4">
										<div class="col-sm-6">
											<th>Status</th>
										</div>
										<div class="col-sm-6">
											<th><span class="pull-right">Action</span></th>
										</div>
									</div>
									@else
									<div class="col-sm-3 col-md-4">
										<th>Action</th>
									</div>
									@endif
								</div>
								@foreach($data['user'][0]['company_registration_detail']['advertisment_details'] as $key => $value)
								<div class="row">
									<div class="col-xs-12 col-sm-5 col-md-4">
										<div class="advertise-card-section">
											<div class="ad_card">
											    <div class="ad_cardImg" style="height: 138px;">
											        <img src="/{{env('ADVERTISE_PATH')}}{{$value['company_id']}}/{{$value['img_path']}}" style="width: 260px;height: 128px;">
											    </div>
											</div>
										</div>
									</div>
									<div class="col-xs-12 col-sm-4 col-md-4">
										<div class="ad_card">
										    <div class="ad_cardImg">
										    	<a href="#" class="pop">
										    	@if(!empty($value['full_img']))
										        	<img class="imageresource" src="/{{env('ADVERTISE_PATH')}}{{$value['company_id']}}/{{$value['full_img']}}" style="height:300px;">
										        @else
										        	<img class="imageresource" src="/images/no-image-advertisements.png">
										        @endif
										    	</a>
										    </div>
										</div>
									</div>
									<div class="col-xs-12 col-sm-3 col-md-4">
										@if(!isset($user))

											<div class="col-sm-6">
												<div class="advertise-label">
													@if($value['status'] == 0)
														<div class="label label-danger">Admin Unapproved</div>
													@elseif($value['status'] == 1)
														<div class="label label-info">Deactivated</div>
													@elseif($value['status'] == 2)
														<div class="label label-success">Active</div>
													@endif
												</div>
											</div>
											<div class="col-sm-6">
												<div class="dropdown setting pull-right">
													<a data-toggle="dropdown" class="btn-xs dropdown-toggle">
														<i class="fa fa-cog" style="color: #b9b9b9;cursor:pointer;font-size:16px;"></i>
													</a>
													@if($value['status'] == 1 || $value['status'] == 2)
														<ul class="dropdown-menu dropdown-light pull-right">
														@if($value['status'] == 1)
															<li class="activate-deactivate-advertise" data-type="{{config('feature.feature3')}}" data-id="{{$value['id']}}" data-status="live">
																<a>
																	<span class="activate-deactivate-text">Activate</span>
																</a>
															</li>
														@elseif($value['status'] == 2)
															<li class="activate-deactivate-advertise" data-type="{{config('feature.feature3')}}" data-id="{{$value['id']}}" data-status="active">
																<a>
																	<span class="activate-deactivate-text">Deactivate</span>
																</a>
															</li>
														@endif
														</ul>
													@endif
												</div>
											</div>
										@else
											<a class="advertisement_enquiry" data-target="#enquire-advertisements-modal" data-company-id="{{$value['company_id']}}" data-advertise-id="{{$value['id']}}">
												<button type="button" class="coss-primary-btn">Enquire</button>
											</a>
										@endif
									</div>
								</div>
								<hr>
								@endforeach
							@else
								<div class="row">
									<div class="col-xs-12 text-center">
										<div class="discription">
											<span class="content-head">No Advertisements Found.</span>
										</div>
									</div>
								</div>
							@endif
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div id="enquire-advertisements-modal" class="modal extended-modal fade no-display" tabindex="-1" data-width="760">
    <div class="modal-dialog">
        <div class="modal-content b-i-none">
            <div class="advertise-enquiry-form-container">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                      &times;
                    </button>
                    <div class="heading"> 
                        <h4 class="modal-title input-label">Advertisement Enquiry</h4>
                    </div>
                </div>
                <div class="modal-body enquire-advertisements-container">

                    <form id="advertisements-enquiry-modal-form" action="{{ route('site.advertisements.enquiry') }}">
                        {{csrf_field()}}
                        <input type="hidden" name="advertisement_id" id="advertisement_id" value="">
                        <input type="hidden" name="company_id" id="company_id" value="">
                        <div class="form-group">
                            <label class="input-label" for="name">Name</label>
                            <input type="text" class="form-control" id="name" name="name" value="{{isset($data['first_name']) ? $data['first_name'] : ''}} ">
                        </div>
                        <div class="form-group">
                            <label class="input-label" for="name">Phone Number</label>
                            <input type="text" class="form-control" id="phone" name="phone" value="{{isset($data['mobile'])?$data['mobile']:''}}">
                        </div>
                        <div class="form-group">
                            <label class="input-label" for="name">Email</label>
                            <input type="email" class="form-control" id="email" name="email" value="{{isset($data['email']) ? $data['email'] : ''}} ">
                        </div>
                        <div class="form-group">
                            <label class="input-label" for="name">Message</label>
                            <textarea class="form-control" rows="6" name="message" id="message"></textarea>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <div class="row right">
                        <div class="col-sm-12">
                            <button type="button" data-style="zoom-in" class="btn btn-blue btn-default ladda-button" id="send-advertise-enquiry-button">
                                Send Enquiry
                            </button>   
                        </div>
                    </div>  
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Image preview</h4>
      </div>
      <div class="modal-body">
        <img src="" id="imagepreview" style="width: 100%; height: 100%;" >
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
@section('js_script')
	<script type="text/javascript" src="/js/site/advertise-registration.js"></script>
@stop