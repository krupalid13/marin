@extends('site.index')
@section('content')

<div class="user-profile content-section-wrapper sm-filter-space">
	 <div class="container-fluid">
        <div class="row">
            <div class="col-md-2" id="side-navbar">
                @include('site.partials.side_navbar_advertiser')
            </div>

            <div class="section col-md-10 p-t-0" id="main-data">
            	@include('site.advertiser.partials.profile')
			</div>
		</div>
	</div>
</div>
@stop
@section('js_script')
    <script type="script/javascript">
        var advertisement_status_change_url = "{{route('site.advertiser.statusChange')}}";
        var advertisement_status_degrade_url = "{{route('site.advertiser.statusDegrade')}}";
    </script>
    <script type="text/javascript" src="/js/site/advertise-registration.js"></script>
@stop