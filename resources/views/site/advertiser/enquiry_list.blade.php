@extends('site.index')
@section('content')
	<div class="job-application-listing content-section-wrapper sm-filter-space">
		<div class="row no-margin">
			<div class="col-md-2">
				@include('site.partials.side_navbar_advertiser')
			</div>
			<div class="col-md-10" id="main-data">
				<div class="row">
					<div class="col-xs-12 col-sm-6">
				        <div class="company-page-profile-heading">
				            Advertisement Enquiries
				        </div>
				    </div>
				</div>
				@if(isset($data) AND !empty($data))
                <span class="pagi pagi-up">
                    <span class="search-count">
                        Showing {{$link['from']}} - {{$link['to']}} of {{$link['total']}} Enquiries
                    </span>
                    <nav> 
                    <ul class="pagination pagination-sm">
                        {!! $paginate->render() !!}
                    </ul> 
                    </nav>
                </span>
				<div class="section listing-container" style="padding: 25px 0px !important">
					{{csrf_field()}}
						@foreach($data as $adv_details)
						<div class="job-card">
							<div class="row m-0 flex-height">
								<div class="col-sm-3 p-0">
									<div class="job-company-image h-100">
									<img src="{{!empty($adv_details['advertisement_details']['img_path']) ? "/".env('ADVERTISE_PATH').$adv_details['company_id']."/".$adv_details['advertisement_details']['img_path'] : 'images/user_default_image.png'}}" avatar-holder-src="{{ asset('images/user_default_image.png') }}">
									</div>
								</div>
								<div class="col-sm-9 p-0">
									<div class="company-discription">
										<div class="row">
											<div class="col-sm-6">
												<div class="other-discription">
													Name:
													{{ isset($adv_details['name']) ? $adv_details['name'] : ''}}
												</div>
											</div>
											<div class="col-sm-6">
												<div class="other-discription">
													Advertisement Id:
													{{ isset($adv_details['advertisement_id']) ? $adv_details['advertisement_id'] : ''}}
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-6">
												<div class="other-discription">
													Phone:
													{{ isset($adv_details['phone']) ? $adv_details['phone'] : ''}}
												</div>
											</div>
											<div class="col-sm-6">
												<div class="other-discription">
													Enquiry Date:
													{{ isset($adv_details['created_at']) ? date('d-m-Y',strtotime($adv_details['created_at'])) : ''}}
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-6">
												<div class="other-discription">
													Email:
													{{ isset($adv_details['email']) ? $adv_details['email'] : ''}}
												</div>
											</div>
											<div class="col-sm-6">
												<div class="other-discription">
													Enquiry Time:
													{{ isset($adv_details['created_at']) ? date('h:i a',strtotime($adv_details['created_at'])) : ''}}
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-6">
												<div class="other-discription">
													Message:
													{{ isset($adv_details['message']) ? $adv_details['message'] : ''}}
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						@endforeach
					<span class="pagi pagi-up">
                    <span class="search-count">
                        Showing {{$link['from']}} - {{$link['to']}} of {{$link['total']}} Enquiries
                    </span>
                    <nav> 
                    <ul class="pagination pagination-sm">
                        {!! $paginate->render() !!}
                    </ul> 
                    </nav>
                </span>
				</div>
				@else
					<div class="row no-results-found">No Results Found.</div>
				@endif
			</div>
		</div>
	</div>
@stop