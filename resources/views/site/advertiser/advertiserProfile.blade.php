@extends('site.index')
@section('content')

<div class="user-profile content-section-wrapper sm-filter-space">
	 <div class="container">
        <div class="section p-t-0 p-b-0" id="main-data">
        	 @include('site.advertiser.partials.profile')
		</div>
	</div>
</div>
@stop