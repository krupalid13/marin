@extends('site.index')

@section('content')
<div class="home-new home featured_company_section">
    <div class="content-wrapper search-listing content-section-wrapper">
        <div class="container">
            <div class="row search-listing-container">
                <button class="btn search-listing-sm-filter-btn" type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#search-listing-filter-modal"><i class="fa fa-filter" aria-hidden="true"></i></button>
                <div class="col-sm-3 col-xs-hidden">
                    <div class="filter-container filter-container-lg">
                        <div class="heading">
                            Filter
                        </div>
                        <form id="company-search-filter" method="get">
                            {{csrf_field()}}
                            <div class="content-container">
                                <div class="form-group">
                                    <label class="input-label" for="company_name">Company Name</label>
                                    <input type="text" class="form-control" id="company_name" name="company_name" value="{{ isset($filter['company_name']) ? $filter['company_name'] : ''}}" placeholder="Enter Company Name">
                                </div>
                                <div class="form-group">
                                    <label class="input-label">State</label>
                                        <select id="state" name="state" class="form-control search-select state fields-for-india">
                                            <option value="">Select Your State</option>
                                            @foreach($state as $states)
                                                <option value="{{$states['id']}}" {{ isset($filter['state']) ? $states['id'] == $filter['state'] ? 'selected' : '' : ''}}>{{ucwords(strtolower($states['name']) )}}</option>
                                            @endforeach
                                        </select>
                                </div>
                                <div class="form-group">
                                    <label class="input-label" for="city">City</label>
                                    <select id="city" name="city" class="form-control city fields-for-india">
                                        <option value="">Select City</option>
                                        @if(isset($city) && !empty($city))
                                            @foreach($city as $c)
                                                <option value="{{$c['id']}}" {{ isset($filter['city']) ? $c['id'] == $filter['city'] ? 'selected' : '' : ''}}>{{ucwords(strtolower($c['name']) )}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                                <div class="form-group">
                                    <button type="submit" data-style="zoom-in" class="btn coss-primary-btn ladda-button" id="seafarer-job-search-button">Search</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    @include('site.partials.featured_advertisement_template')
                </div>
                <div class="col-sm-9 col-xs-12">
                    <div class="sort-by-container">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="search-title">
                                    Featured Advertisements
                                </div>
                            </div>
                        </div>
                    </div>
                    @if(isset($advertisements) && !empty($advertisements))
                        <?php
                            $advertisement_details = $advertisements->toArray();
                        ?>
                        @if(isset($advertisement_details['data']) && !empty($advertisement_details['data']))
                        <div class="section_2" style="background-color: transparent !important;">
                            <div class="row">
                                <div class="col-xs-12 col-md-12">
                                    <div class="row">
                                        <div class="col-xs-12 col-md-12">
                                            <div class="pagi pagi-up p-t-10">
                                                <span class="search-count p-t-10">
                                                    Showing {{$advertisement_details['from']}} - {{$advertisement_details['to']}} of {{$advertisement_details['total']}} Advertisements
                                                </span>
                                                <nav> 
                                                <ul class="pagination pagination-sm">
                                                    {!! $advertisements->render() !!}
                                                </ul> 
                                                </nav>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        @foreach($advertisement_details['data'] as $key => $value)
                                            @if(isset($value['advertisement_details']) AND !empty($value['advertisement_details']))
                                                <div class="col-sm-4">
                                                    <div class="ad_card">
                                                        <div class="ad_cardTitle">
                                                            Advertisement
                                                            <a class='advertisement_enquiry' data-target="#enquire-advertisements-modal" data-company-id="{{isset($value['advertisement_details']['company_id']) ? $value['advertisement_details']['company_id'] : ''}}" data-advertise-id="{{isset($value['advertise_id']) ? $value['advertise_id'] : ''}}">Enquire</a>
                                                        </div>

                                                        <div class="ad_cardImg">
                                                            <a href="{{ isset($value['advertisement_details']['company_registration_details']['user_id']) ? route('site.advertiser.profile.id',$value['advertisement_details']['company_registration_details']['user_id']) : '' }}" target = "_blank">
                                                                <img src="{{env('ADVERTISE_PATH')}}{{$value['advertisement_details']['company_id']}}/{{$value['advertisement_details']['img_path']}}">
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endif
                                        @endforeach
                                    </div>
                                        

                                    <div class="row">
                                        <div class="col-xs-12 col-md-12">
                                            <span class="pagi pagi-up">
                                                <span class="search-count m-t-10">
                                                    Showing {{$advertisement_details['from']}} - {{$advertisement_details['to']}} of {{$advertisement_details['total']}} Advertisements
                                                </span>
                                                <nav> 
                                                <ul class="pagination pagination-sm">
                                                    {!! $advertisements->render() !!}
                                                </ul> 
                                                </nav>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @else
                            <div class="card-container">
                                <div class="row no-results-found">No results found. Try again with different search criteria.</div>
                            </div>
                        @endif
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>

<div id="enquire-advertisements-modal" class="modal extended-modal fade no-display" tabindex="-1" data-width="760">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="advertise-enquiry-form-container">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                      &times;
                    </button>
                    <div class="heading"> 
                        <h4 class="modal-title input-label">Advertisement Enquiry</h4>
                    </div>
                </div>
                <div class="modal-body enquire-advertisements-container">

                    <form id="advertisements-enquiry-modal-form" action="{{ route('site.advertisements.enquiry') }}">
                        {{csrf_field()}}
                        <input type="hidden" name="advertisement_id" id="advertisement_id" value="">
                        <input type="hidden" name="company_id" id="company_id" value="">
                        <div class="form-group">
                            <label class="input-label" for="name">Name</label>
                            <input type="text" class="form-control" id="name" name="name" value="{{isset($data['first_name']) ? $data['first_name'] : ''}} ">
                        </div>
                        <div class="form-group">
                            <label class="input-label" for="name">Phone Number</label>
                            <input type="text" class="form-control" id="phone" name="phone" value="{{isset($data['mobile'])?$data['mobile']:''}}">
                        </div>
                        <div class="form-group">
                            <label class="input-label" for="name">Email</label>
                            <input type="email" class="form-control" id="email" name="email" value="{{isset($data['email']) ? $data['email'] : ''}} ">
                        </div>
                        <div class="form-group">
                            <label class="input-label" for="name">Message</label>
                            <textarea class="form-control" rows="6" name="message" id="message"></textarea>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <div class="row right">
                        <div class="col-sm-12">
                            <button type="button" data-style="zoom-in" class="btn btn-blue btn-default ladda-button" id="send-advertise-enquiry-button">
                                Send Enquiry
                            </button>   
                        </div>
                    </div>  
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="search-listing-filter-modal" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title text-center">Filter</h4>
            </div>
            <div class="modal-body">
                <div class="filter-container-sm">
                    <form id="company-search-filter" method="get">
                            {{csrf_field()}}
                            <div class="content-container">
                                <div class="form-group">
                                    <label class="input-label" for="company_name">Company Name</label>
                                    <input type="text" class="form-control" id="company_name" name="company_name" value="{{ isset($filter['company_name']) ? $filter['company_name'] : ''}}" placeholder="Enter Company Name">
                                </div>
                                <div class="form-group">
                                    <label class="input-label">State</label>
                                        <select id="state" name="state" class="form-control search-select state fields-for-india">
                                            <option value="">Select Your State</option>
                                            @foreach($state as $states)
                                                <option value="{{$states['id']}}" {{ isset($filter['state']) ? $states['id'] == $filter['state'] ? 'selected' : '' : ''}}>{{ucwords(strtolower($states['name']) )}}</option>
                                            @endforeach
                                        </select>
                                </div>
                                <div class="form-group">
                                    <label class="input-label" for="city">City</label>
                                    <select id="city" name="city" class="form-control city fields-for-india">
                                        <option value="">Select City</option>
                                        @if(isset($city) && !empty($city))
                                            @foreach($city as $c)
                                                <option value="{{$c['id']}}" {{ isset($filter['city']) ? $c['id'] == $filter['city'] ? 'selected' : '' : ''}}>{{ucwords(strtolower($c['name']) )}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                                <div class="form-group">
                                    <button type="submit" data-style="zoom-in" class="btn coss-primary-btn ladda-button" id="seafarer-job-search-button">Search</button>
                                </div>
                            </div>
                        </form>
                </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>
    
@stop

@section('js_script')
    <script type="text/javascript" src="/js/site/job.js"></script>
@stop