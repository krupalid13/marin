@extends('site.index')
@section('content')
	<div class="privacy-policy content-section-wrapper">
		<div class="section container">
			<div class="section_heading">
				Privacy Policy
			</div>
			<div style="font-size:14px">
				<div style="padding-top:4px;padding-bottom:4px;">
				The Privacy Policy highlights the type of information we collect from you, how we use the
				information and where we share it. We offer you choice to restrict the use and disclosure of your
				personal information.
				</div>
				<div style="padding-top:4px;padding-bottom:4px;">
					We urge you to keep yourself on this Privacy Policy from time to time, as we may update it
					periodically to without prior notice, yet for better practices.
				</div>
				<div style="font-size:16px;font-weight:bold;padding-top:4px;">
					Your Personal Information
				</div>
				<ul style="padding-left: 20px;font-size:14px;">
					<li>
						ConsultanSeas collects and maintains certain information like (email address, location, IP address)
						for its website via forms. This is done to facilitate our customers with better service and with
						valuable information regarding products and services.
					</li>
				</ul>
			</div>
			<div style="font-size:16px;font-weight:bold;padding-top:4px;">
					Your Consent
			</div>
			<ul style="padding-left: 20px;font-size:14px;">
				<li>
					While using ConsultanSeas you consent to the terms of Privacy Policy.
				</li>
				<li>
					While we are relentlessly working on creating better services for you, we are at the same
				time committed to protecting your personal information and will ensure your data remains
				secured with us. We are bound by our standards of integrity and all information and related
				requests will be handled efficiently.
				</li>
				<li>
					We have tried and tested security measures in place for protecting your data, but you need
				to note that no electronic transmission of data is entirely secure. As a user of the site, you
				understand and agree on the risk for use of the site, the documents you post and access on
				and off the site.
				</li>
				<li>
					As a secure measure, you should well protect your login id and password. You should signoff
				your account and ensure you log off your browser window after you have finished visiting
				our site.
				</li>
			</ul>
			<div style="font-size:16px;font-weight:bold;padding-top:4px;">
					Information we collect
			</div>
			<ul style="padding-left: 20px;font-size:14px;">
				<li>
					You have chosen to use our services and in this regard, you also abide to share information
					with us for the benefit you wish to derive from this medium. While you share your
					information with us, the type of information we may obtain from you includes
				</li>
				<li>
					 Username and password for your login to our site. Job search pattern and preferences. This
					information we collect to present you better and closely matching job opportunities to your
					profile.
				</li>
				<li>
					Your demographic information (like pin code, occupation, experience, age, gender and ethnicity)
				</li>
				<li>
					 Payment Details if you buy some product / services from or through our site (Card number, card holder name, billing address, security code and expiration date).
				</li>
			</ul>
			<div style="font-size:16px;font-weight:bold;padding-top:4px;">
					Information that comes to us through automated means
			</div>
			<ul style="padding-left: 20px;font-size:14px;">
				<li>
					Information on the devices used and the IP addresses, browser and Operating System.
				</li>
				<li>
					 Periodic information on date and time you accessed our site.
				</li>
				<li>
					Search parameters and keywords
				</li>
				<li>
					General Geographic location from which a visitor access the site.
				</li>
				<li>
					Information on page views, sites and microsites accessed, jobs viewed, services viewed and site navigation.
				</li>
			</ul>
			<div style="font-size:16px;font-weight:bold;padding-top:4px;">
					How we use the information we collect
			</div>
			<ul style="padding-left: 20px;font-size:14px;">
				<li>
					 Inform you on relevant job services and offers.
				</li>
				<li>
					 Maintain a record of your probable searches and job choices.
				</li>
				<li>
					Analyse statistics and trends on the use of site and services.
				</li>
				<li>
					To revert to your queries and comments.
				</li>
				<li>
					To keep you posted on notices and communications relevant to your use of services with us.
				</li>
				<li>
					To evaluate our product periodically in lieu of the changing trends.
				</li>
				<li>
					To analyse and enhance our marketing strategies and communications.
				</li>
			</ul>
			<div style="font-size:16px;font-weight:bold;padding-top:4px;">
					Information we share
			</div>
			<ul style="padding-left: 20px;font-size:14px;">
				<li>
					 When you show potential interest in a job and submit a resume with the listed job opportunities, your information directly reaches the employers you have applied to.
				</li>
				<li>
					 When you submit your resume in our database for prospective job opportunities, your resume becomes a part of our database.
				</li>
				<li>
					 When your resume is downloaded from our database by an employer, your information is shared with that employer.
				</li>
				<li>
					 The information shared by you and received by the employer is subject to our Privacy Policy. We are not responsible for how the employer uses your information.
				</li>
				<li>
					The users should be well aware that the Personal Information they submit through our site (like email id and Contact number) can be collected and used by others and could result in unsolicited messages from third parties. ConsultanSeas is not responsible for this. 
				</li>
				<li>
					 We also extract and share Personal Information with our service providers who help us in making our services &amp; products better for your usage.
				</li>
				<li>
					 We are bound by law to submit / share / disclose any information that is necessary to comply with law, to co-operate and assist law enforcements in prevention of crime, protect national security and interests and security of other users and ConsultanSeas.
				</li>
				<li>
					The transfer of Personal Information is applicable in event of transfer of ownership, bankruptcy. 
				</li>
			</ul>
			<div style="font-size:16px;font-weight:bold;padding-top:4px;">
					Cookies and other Tracking technologies
			</div>
			<ul style="padding-left: 20px;font-size:14px;">
				<li>
					ConsultanSeas may place cookies on your computer when you visit our site. The cookies are placed to monitor your searches, clicks and sections you visit most on our site. This information is used to anonymously customize advertisements and display to you when you use other websites.
				</li>
				<li>
					Most browsers control cookies, and you can decide to accept them and remove them. You may also set your browser to block the cookies.
				</li>
				<li>
					We additionally use tracking devices to record information like Internet domain, Internet protocol (IP), host name, browser software and operating system, frequency of date and time our site was accessed. We make use of tracking information to study and analyse for trends &amp; statistics. The use of cookies and tracking devices is to offer you improved and better services in line with development and better experiences.
				</li>
			</ul>	
			<div style="font-size:16px;font-weight:bold;padding-top:4px;">
					Resume Privacy options
			</div>
			<ul style="padding-left: 20px;font-size:14px;">
				<li>
					ConsultanSeas offers you a flexibility to display the status of your resume as active or inactive. Active status means the resume is accessible and visible to all. Inactive means the resume is not accessible and not visible to all or selected companies.
				</li>
				<li>
					However, it is the discretion of the job seeker to edit his/her profile and remains entirely the responsibility of the jobseeker.
				</li>
				<li>
					Once the resume is active it remains a property of employers and agents and ConsultanSeas is not responsible for any unauthorised use of this information.
				</li>
			</ul>
			<div style="font-size:16px;font-weight:bold;padding-top:4px;">
					Third Party Services
			</div>
			<ul style="padding-left: 20px;font-size:14px;">
				<li>
					While we strive to bring you the best services and experiences, we have to bank on some third party
					services to bring you some dedicated and exclusive services under one banner. Some services like
					latest news, related or extended products or services, blogs, plug-ins, advertisements and employer
					/agents websites with whom are associated, affiliated as third party service providers. ConsultanSeas
					might share some Personal Information that ConsultanSeas collects on the website to help deliver
					better services &amp; information.
				</li>
				<li>
					ConsultanSeas is obligated to protect your Personal Information, but at the same time ConsultanSeas will ensure that we share your information with third parties with your consent.
				</li>
			</ul>
		    <div style="font-size:16px;font-weight:bold;padding-top:4px;">
					Linking to other sites
			</div>
			<ul style="padding-left: 20px;font-size:14px;">
				<li>
					We aim to provide you an array of related service pertaining to the shipping industry. Our
					site contains links to other sites for related services and products. These sites may be
					operated by companies not owned by ConsultanSeas and DO NOT hold the same Privacy
					Policies as ConsultanSeas. We request you to review the related service provider’s Privacy
					Policies whenever you drift to third party links and advertisements on our site.
				</li>
			</ul>
			<div style="font-size:16px;font-weight:bold;padding-top:4px;">
					Update your account information
			</div>
			<ul style="padding-left: 20px;font-size:14px;">
				<li>
					You should keep visiting your Personal Information and stay updated for the benefit of yourself and the services you are applying for on our platform.
				</li>
				<li>
					If you are not using our platform for any reason, you may opt for deletion of your account
					from our database. Deletion of account will remove your resume and Personal Information
					with ConsultanSeas. If you have applied to a job in the near past, the employer will retain the
					resume in their database. Some prospective employers who must have downloaded and
					saved your resume in their databank also hold your information in their database.
				</li>
			</ul>
			<div style="font-size:16px;font-weight:bold;padding-top:4px;">
					Protect your sensitive information
			</div>
			<ul style="padding-left: 20px;font-size:14px;">
				<li>
					ConsultanSeas is a medium to explore relevant jobs and services. Though we are a link
					between the employer and candidates we are not responsible for any fraudulent job offers
					or hoax mails. We are just a platform to bring forth information to you in good faith. We suggest you thoroughly verify at your end on a company you wish to share your information before applying.
				</li>
				<li>
					We do not charge any fees for uploading your cv’s. Any demand on transactional fees or convenience or agency charges, is not our terms &amp; concern.
				</li>
				<li>
					You should be your own judge while sharing sensitive information of any matterto prospective employers.
				</li>
				<li>
					We urge you not to disclose Sensitive Information like Pan Number, Aadhar Number, Credit or Debit card numbers, Bank Details Login information on your resume or on our platform.
				</li>
			</ul>
		</div>
	</div>
@stop


