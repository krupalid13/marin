@extends('site.cms-index')
@section('content')
<div class="footer-page-container" style="margin-top: 60px;">
    <h1 class="footer-page-title">PRIVACY POLICY</h1>
    <p class="footer-page-para">
        We may share personal information with our other corporate entities and affiliates. These entities and
        affiliates may market to you as a result of such sharing unless you explicitly opt-out.
    </p>
    <p class="footer-page-para">
        We may disclose personal information to third parties. This disclosure may be required for us to provide you
        access to better Services.
    </p>
    <p class="footer-page-para">
        We also share your personal information to third parties registered with us for their marketing and
        advertising
        purposes.
    </p>
    <p class="footer-page-para">
        We may disclose personal information if required to do so by law or in the good faith belief that such
        disclosure is reasonably necessary to respond to subpoenas, court orders, or other legal process. We and our
        affiliates will share / sell some or all of your personal information with another business entity should we
        (or our assets) plan to merge with, or be acquired by that business entity, or re-organization,
        amalgamation,
        restructuring of business. Should such a transaction occur that other business entity (or the new combined
        entity) will be required to follow this privacy policy with respect to your personal information.
    </p>
    <h3 class="footer-page-midtitle">Information Shared by User</h3>
    <p class="footer-page-para">
        Professional details included in the Resume and Profile page.
    </p>
    <p class="footer-page-para">
        Contact & location details.
    </p>
    <p class="footer-page-para">
        Documents uploaded by the user.
    </p>
    <p class="footer-page-para">
        Feedback regarding your onboard experience.
    </p>
    <p class="footer-page-para">
        Reviews and Rating for owner and manning shipping companies you have worked with.
    </p>
    <p class="footer-page-para">
        Photographic memories uploaded along with caption about that moment.
    </p>
    <h3 class="footer-page-midtitle">Communications</h3>
    <p class="footer-page-para">
        When You use the Website Flanknot.com or book a course or interact with an institute or company, you agree and understand
        that You are communicating through Us via electronic records and You consent to receive communications via
        electronic records from flanknot about institutes / companies or advertisers aligned with us, periodically and
        as
        and when required.
    </p>
    <p class="footer-page-para">
        We may communicate with you by email or by such other mode of communication, electronic or otherwise.
    </p>
    <p class="footer-page-para">
        Users can unsubscribe from the website by contacting us via email grievance@flanknot.com.
    </p>
    <p class="footer-page-para">
        Location of user enables the company to provide better services to the user.Tracking your movement on the
        website via
        Google Analytics or other related tools will ensure better coordination between user and the
        institute/company.
    </p>
    <h3 class="footer-page-midtitle">Registration Charges</h3>
    <p class="footer-page-para">
        Membership on the Website is free for users. The Company(Flankknot) does not charge any fee for browsing the Website Flanknot.com.
    </p>
    <p class="footer-page-para">
        The Company(Flankknot) reserves the right to change its Fee Policy from time to time. In particular, The Company(Flankknot) may at
        its
        sole discretion introduce new services and modify some or all of the existing services offered on the
        Website.
    </p>
    <p class="footer-page-para">
        If required the Company(Flankknot) reserves the right to introduce fees for the new services offered or amend/introduce
        fees for existing services,as the case may be. Changes to the Fee Policy shall be posted on the Website and
        such
        changes shall automatically become effective
        immediately after they are posted on the Website. Unless otherwise stated, all fees shall be quoted in
        Indian
        Rupees or US dollars.
    </p>
    <h3 class="footer-page-midtitle">Promotions on and about the Website</h3>
    <p class="footer-page-para">
        There will be promotion content on the website. These promotion contents are for discounts and offers on
        certain
        course bookings. Google Ads (AdWords)/ Bing ADs / Facebook / LinkedIn / YouTube / Pinterest can be used as
        Advertisement on the website.
    </p>
    <p class="footer-page-para">
        Remarketing services are used to advertise our website.
    </p>
    <h3 class="footer-page-midtitle">Information sharing</h3>
    <p class="footer-page-para">
        User information will be viewed by Institute / Companies / Promoters.
    </p>
    <p class="footer-page-para">
        If required by law or by a government agency, the information of users can be disclosed to law enforcement
        agents.
    </p>
    <p class="footer-page-para">
        In case of an acquisition, merger or asset sale the information will be passed on.
    </p>
    <h3 class="footer-page-midtitle">Questions Regarding your Privacy Policy?</h3>
    <p class="footer-page-para">
        Any doubts or questions about our Privacy Policy, you can contact us by email at support@flanknot.com
    </p>
</div>
@stop
