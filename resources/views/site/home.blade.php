<!DOCTYPE html>
<html lang="en">
    <head>
        <?php $site_version = rand(10, 100); ?>
        <title>
        @if(isset($pageTitle) && !empty($pageTitle))
            {!! $pageTitle !!}
        @else
            {{env('APP_NAME')}}
        @endif
        </title>
        <meta name="description" content="{!! ($metaDescription) ?? '' !!}">
        <meta name="keywords" content="{!! ($metaKeywords) ?? '' !!}">
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
        <meta name="_token" content="{{ csrf_token() }}">
        <link rel="shortcut icon" type="image/x-icon" href="{{asset('public/images/favicon.ico')}}">
        <link rel="stylesheet" href="{{asset('public/assets/css/bootstrap.min.css')}}">
        <link rel="stylesheet" href="{{asset('public/assets/css/home-style.css')}}">
        <link rel="stylesheet" href="{{asset('public/assets/css/styleslider1.css') }}">
        <link href="{{asset('public/assets/css/site_app.css?v=59') }}" rel="stylesheet">
        @yield('page-level-style')
        @yield('addStyle')
        <style type="text/css">
    .nav-link.dropdown-toggle{
                height: 100%;
            }
            .navbar-brand{
                padding-bottom: 1rem !important;
            }
        </style>
    </head>
    <body>
        {{csrf_field()}}
        <input type="hidden" id="captcha_key" value="{{env('RECAPTCHA_SITE_KEY')}}">
        <input type="hidden" id="current-route-name" value="{{ Route::currentRouteName() }}">
        <input type="hidden" id="api-check-email" value="{{ route('site.check.email') }}">
        <input type="hidden" id="api-check-mobile" value="{{ route('site.check.mobile') }}">
        <input type="hidden" id="api-resend-otp-route" value="{{ route('site.user.mob.resendOtp') }}">
        <input type="hidden" id="api-resend-email-route" value="{{ route('site.user.email.resendEmail') }}">
        <input type="hidden" id="api-seafarer-profile-pic" value="{{ route('site.user.store.profilePic') }}">
        <input type="hidden" id="api-company-team-profile-pic" value="{{ route('site.company.store.profilePic') }}">
        <input type="hidden" id="api-add-subscription-route" value="{{ route('site.subscription.add.to.cart') }}">
        <input type="hidden" id="api-view-cart-route" value="{{ route('site.subscription.cart') }}">
        <input type="hidden" id="api-remove-subscription-route" value="{{ route('site.delete.from.cart','%test') }}">
        <input type="hidden" id="api-proceed-to-payment-route" value="{{ route('site.proceed.payment') }}">
        <input type="hidden" id="api-company-logo" value="{{ route('site.company.store.logo') }}">
        <input type="hidden" id="api-institute-logo" value="{{ route('site.institute.store.logo') }}">
        <input type="hidden" id="api-pincode-related-data" value="{{ route('site.pincode.get.states_cities') }}">
        <input type="hidden" id="api-auto-welcome-route" value="{{ route('home',['auto_action' => 'welcome']) }}">
        <input type="hidden" id="api-seafarer-profile-route" value="{{ route('user.profile') }}">
        <input type="hidden" id="api-seafarer-job-apply-route" value="{{ route('site.user.job.apply') }}">
        <input type="hidden" id="api-seafarer-course-apply-route" value="{{ route('site.user.course.apply') }}">
        <input type="hidden" id="api-activate-subscription-route" value="{{ route('site.activate.subscription') }}">
        <input type="hidden" id="api-store-advertiser-form-route" value="{{ route('site.advertise.store') }}">
        <input type="hidden" id="api-admin-state-list" value="{{ route('admin.state.list','%test') }}">
        <input type="hidden" id="api-site-advertise-list" value="{{ route('site.advertisements.list') }}">
        <input type="hidden" id="api-site-seafarer-registration" value="{{ route('site.seafarer.registration') }}">
        <input type="hidden" id="api-site-company-registration" value="{{ route('site.company.registration') }}">
        <input type="hidden" id="api-site-institute-registration" value="{{ route('site.institute.registration') }}">
        <input type="hidden" id="api-site-advertise-registration" value="{{ route('site.advertiser.registration') }}">
        <input type="hidden" id="api-site-add-job" value="{{ route('site.company.add.jobs') }}">
        <input type="hidden" id="api-site-subscription" value="{{ route('site.user.subscription') }}">

        <input type="hidden" id="api-site-company-data-ships" value="{{ route('site.company.details.related.ships','%test') }}">

        <input type="hidden" id="api-site-company-data-ships-by-ship-type" value="{{ route('site.company.details.related.ships.by.ship.type','%test') }}">

        <input type="hidden" id="\" value="{{ route('site.company.details.ship.name.by.ship.id','%test') }}">

               <input type="hidden" id="api-site-company-candidate-download-cv" value="{{ route('site.company.candidate.download-cv','%test') }}">

        <input type="hidden" id="api-institute-course-name-by-course-types" value="{{ route('site.institute.get.course.name',['%test','%test']) }}">

        <input type="hidden" id="api-seafarer-delete-service-by-service-id" value="{{ route('site.user.delete.service.details') }}">
        <input type="hidden" id="api-seafarer-get-service-by-service-id" value="{{ route('site.user.get.service.details') }}">

        <input type="hidden" id="api-seafarer-store-single-service-details" value="{{ route('site.user.store.single.service.details') }}">

        <input type="hidden" id="api-institute-delete-course" value="{{ route('site.institute.delete.course','%test') }}">

        <input type="hidden" id="api-site-seafarer-download-cv" value="{{ route('site.user.download-cv') }}">

        <input type="hidden" id="api-site-company-download-file" value="{{ route('site.user.download-file') }}">

        <input type="hidden" id="api-site-seafarer-store-course" value="{{ route('site.user.store.seafarer.course.details') }}">
        <input type="hidden" id="api-site-seafarer-delete-course" value="{{ route('site.user.seafarer.delete.details') }}">
        <input type="hidden" id="api-seafarer-get-course-by-course-id" value="{{ route('site.user.seafarer.get.course.details') }}">

        <input type="hidden" id="api-seafarer-get-course-by-course-type" value="{{ route('site.user.seafarer.get.course.by.course.type') }}">

        <input type="hidden" id="api-seafarer-get-document-list" value="{{ route('site.user.get.documents.list') }}">

        <input type="hidden" id="api-seafarer-change-document-status" value="{{ route('site.user.change.documents.status') }}">

        <input type="hidden" id="api-seafarer-store-requested-document-list" value="{{ route('site.user.store.requested.documents.list') }}">

        <input type="hidden" id="api-seafarer-requested-document-status" value="{{ route('site.user.requested.documents.status') }}">

        <input type="hidden" id="api-seafarer-delete-document" value="{{ route('site.user.upload.document.delete') }}">
        <input type="hidden" id="api-seafarer-store-permissions" value="{{ route('site.user.store.permissions') }}">

        <input type="hidden" id="api-site-user-get-documents-path" value="{{ route('site.user.get.documents.path',['%test','%test','%test']) }}">

        <input type="hidden" id="api-company-delete-vessel-by-vessel-id" value="{{ route('site.company.delete.vessel.details') }}">
        <input type="hidden" id="api-company-get-vessel-by-vessel-id" value="{{ route('site.company.get.vessel.details') }}">

        <input type="hidden" id="api-company-delete-agent-by-agent-id" value="{{ route('site.company.delete.agent.details') }}">
        <input type="hidden" id="api-company-get-agent-by-agent-id" value="{{ route('site.company.get.agent.details') }}">

        <input type="hidden" id="api-company-delete-team-by-team-id" value="{{ route('site.company.delete.team.details') }}">

        <input type="hidden" id="api-company-update-team-by-team-id" value="{{ route('site.company.update.team.details') }}">

        <input type="hidden" id="api-agent-team-profile-pic" value="{{ route('site.company.store.agent.profilePic') }}">

        <input type="hidden" id="api-site-state-list-by-country-id" value="{{ route('site.get.state.by.country.id','%test') }}">

        <input type="hidden" id="api-site-list-course-search" value="{{ route('site.seafarer.course.search') }}">

        <input type="hidden" id="api-site-course-search" value="{{ route('site.seafarer.homepage.course.search') }}">
        <input type="hidden" id="api-site-institute-search" value="{{ route('site.seafarer.homepage.institute.search') }}">

        <input type="hidden" id="api-site-institute-course-details-by-id" value="{{ route('site.institute.course.details','%test') }}">
        <input type="hidden" id="api-site-institute-batch-details-by-id" value="{{ route('site.institute.get.batch.details.by.id','%test') }}">
        <input type="hidden" id="api-site-institute-batch-book-by-location" value="{{ route('site.seafarer.course.book',['%test','%test']) }}">

        <input type="hidden" id="api-site-institute-block-seat" value="{{ route('site.institute.block.seat') }}">

        <input type="hidden" id="api-site-institute-change-batch-status" value="{{ route('site.institute.change.batch.status') }}">

        <input type="hidden" id="api-institute-batch-proceed-to-payment-route" value="{{ route('site.institute.batch.proceed.payment') }}">
        <input type="hidden" id="api-institute-batch-cancel-payment-route" value="{{ route('site.institute.batch.cancel.order') }}">

        <input type="hidden" id="api-institute-delete-image-gallery" value="{{ route('site.institute.delete.image.gallery') }}">

        <input type="hidden" id="api-get-institute-advertise-by-batch-id" value="{{ route('site.get.institute.advertise.by.batch.id') }}">

        <!-- Notification -->
        <input type="hidden" id="api-get-user-notification" value="{{ route('site.get.user.notification') }}">

        <input type="hidden" id="api-upload-user-notification" value="{{ route('site.upload.user.notification') }}">

        <input type="hidden" id="api-set-user-notification-read" value="{{ route('site.set.user.notification.read') }}">

        <script>
            var initialize_pincode_maxlength_validation = false;
            var another_user = false;
            var another_user_update_availability = false;
            var job_apply = false;
            var already_login = 0;
            var auto_action = '';
            var location_block_index = 1;
            var ship_block_index = 1;
            var block_index = 1;
        </script>
        <div class="wrapper_main">
            <header class="cs-header">
    <div class="container">
        <nav class="navbar navbar-default z-depth-1">
            <div class="navbar-header">
                <div class="hide-on-med-and-up">
                    @if(\Request::route()->getName() == 'user.profile')
                    <a class="navbar-toggler sidebar-toggler" id="sidebar-toggler" href="#">
                        <span class="navbar-toggler-icon">
                            <i class="icon-menu"></i>
                        </span>
                    </a>
                    @endif
                </div>
                @if(\Request::route()->getName() == 'user.profile')
                    <button type="button" class="navbar-toggle new-navbar-toggle" data-toggle="collapse"
                            data-target=".navbar-ex1-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                @endif
                <a class="navbar-brand" href="/">
                    <!-- <img alt="coursesea logo" src="{{ URL:: asset('public/assets/images/coursealogo.png')}}"> -->
                    <img alt="flanknot logo" src="{{ URL:: asset('public/assets/images/logo-white-bg.png')}}" style="margin-top: 4px">
                </a>
            </div>
            <div class="navbar-right">
                @if(Auth::check() AND Auth::User()->registered_as != 'admin')
                    <?php
                    $user_details = Auth::User()->toArray();
                    // if($user_details['registered_as'] == 'company'){
                    //     $profile_path = 'site.show.company.details';
                    // }
                    if ($user_details['registered_as'] == 'seafarer') {
                        $profile_path = 'user.profile';
                    }
                    // if($user_details['registered_as'] == 'advertiser'){
                    //     $profile_path = 'site.advertiser.profile';
                    // }
                    if ($user_details['registered_as'] == 'institute') {
                        $profile_path = 'site.show.institute.details';
                    }
                    ?>
                    <ul class="nav">
                        <li class="nav-item dropdown">
                            @if(Auth::check() AND Auth::User()->registered_as != 'admin')
                                <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button"
                                   aria-haspopup="true"
                                   aria-expanded="false">{{isset($user_details['first_name']) ? ucwords($user_details['first_name']) : ''}}</a>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <div class="dropdown-header text-center">
                                        <strong>Account</strong>
                                    </div>
                                    <div class="">
                                        @if(isset($profile_path) AND !empty($profile_path))
                                            <a class="dropdown-item" href="{{route($profile_path)}}"><span
                                                        class="icon-user icon"></span> <span class="">Profile</span>
                                            </a>
                                        @endif
                                        <?php 

                                            $authDetail = Auth::user();
                                            $newUnreadJobCount = App\SendedJob::where('user_id',$authDetail->id)->where('status',2)->count();

                                        ?>
                                        <a class="dropdown-item" href="{{route('site.seafarer.job.new')}}">
                                             <span class="icon-briefcase icon"></span><span class="">New Jobs</span> 
                                            @if($newUnreadJobCount > 0)
                                            <small style="margin-left: 6px" class="label label-success new_job_a"> New </small>
                                            @endif
                                        </a>
                                        <a class="dropdown-item" href="{{route('site.how-to')}}">
                                             <span class="icon-question icon"></span><span class="">How To ?</span> 
                                        </a>
                                        @if(Auth::User()->registered_as == 'seafarer')
                                            <a class="dropdown-item" href="{{route('user.bookings')}}"><span
                                                        class="icon-notebook icon"></span> <span class="">My Bookings</span>
                                            </a>
                                        @endif
                                        <a class="dropdown-item" href="{{route('logout')}}"><span
                                                    class="icon-lock icon"></span> <span class="">Logout</span> </a>
                                    </div>

                                </div>
                            @endif
                        </li>
                    </ul>
                @else
                    @if(\Request::route()->getName() != 'register')
                        <a href="{{route('register')}}" class="btn cs-primary-btn navbar-btn">Register</a>
                    @endif
                    @if(\Request::route()->getName() != 'site.login')
                    <a href="{{route('site.login')}}" class="btn cs-primary-btn navbar-btn">Login</a>
                    @endif
                @endif
            </div>
        </nav>
    </div>
</header>
            {{csrf_field()}}

            @yield('content')

            <footer class="footer_main">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-4 col-sm-6">
                            <div class="img-fluid"><img src="{{asset('public/images/footer/footer-img1.png')}}" alt=""></div>
                        </div>
                        <div class="col-lg-4 col-sm-12">
                            <div class="text-block">
                                <h3>Join Flanknot</h3>
                                <p>Move ahead with a community of support behind you</p>
                                <a class="button" href="{{route('aboutUs')}}">ABOUT US</a>
                            </div>
                        </div>
                        <div class="col-lg-4 col-sm-6">
                            <div class="img-fluid"><img src="{{asset('public/images/footer/footer-img2.png')}}" alt=""></div>
                        </div>
                    </div>
                    <div class="links">
                        <ul>
                            <li><a href="/">Flankknot</a></li>
                            <li><a href="{{ route('site.how-to') }}">How To ?</a></li>
                            <li><a href="{{ route('faq') }}">FAQ's</a></li>

                            <li><a href="{{ route('aboutUs') }}">About Us</a></li>
                            <li><a href="{{ route('site.contact-us') }}">Contacts</a></li>
                            <li><a href="{{ route('report') }}">Report</a></li>

                            <li><a href="{{ route('tnc') }}">Terms & Conditions</a></li>
                            <li><a href="{{ route('disclaimer') }}">Disclaimer</a></li>
                            <li><a href="{{ route('privacy_policy') }}">Privacy Policy</a></li>

                            <li><a href="{{ route('cookie-policy') }}">Cookie</a></li>
                            <li><a href="{{ route('company-inquiry') }}">Company Inquiry</a></li>
                            <li><a href="{{ route('institutes-inquiry') }}">Institutes Inquiry</a></li>
                        </ul>
                    </div>
                    <div class="copyright"><p>Copyright @2020  |  <a href="#">All Rights Reserved</a>  |  <a href="{{route('tnc')}}">Terms and Conditions</a></p></div>
                </div>
            </footer>
<script type="text/javascript" src="{{asset('public/js/new/scripts.js')}}"></script>
            <script type="text/javascript" src="{{asset('public/js/new/jquery.min.js')}}"></script>
            <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
            <script type="text/javascript" src="{{asset('public/js/new/bootstrap.min.js')}}"></script>
            <script type="text/javascript" src="{{asset('public/assets/js/site_plugins.js?v=59')}}"></script>
        <script type="text/javascript" src="{{asset('public/assets/js/site_app.js?v=59')}}"></script>
        <script type="text/javascript" src="{{asset('public/assets/js/site/home.js?v=59')}}"></script>
            
            <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"></script>
            @yield('js_script')

            <script>
            $('#preRegistrationFormModalTop').validate({
                rules: {
                    firstname: {
                        //alpha: true,
                        required: true
                    },
                    email: {
                        required: true,
                        email: true,
                        remote: {
                            url: $("#api-check-email").val(),
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content'),
                            },
                            type: "post",
                            async: false,
                            data: {
                                id: function () {
                                    return $("input[name='user_id']").val()
                                },
                                email: function () {
                                    return $("#user_email").val();
                                }
                            }
                        }
                    },
                    password: {
                        minlength: 6,
                        required: true
                    },
                    cpassword: {
                        required: true,
                        minlength: 6,
                        equalTo: "#o_password"
                    },
                    gender: {
                        required: true
                    },
                    passport: {
                        required: true,
                    },
                    current_rank: {
                        required: true,
                    },
                    nationality: {
                        required: true,
                    },
                    dob: {
                        required: true,
                    },
                    mobile: {
                        required: true,
                        number: true,
                        minlength: 10,
                        remote: {
                            url: $("#api-check-mobile").val(),
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content'),
                            },
                            type: "post",
                            async: false,
                            data: {
                                id: function () {
                                    return $("input[name='user_id']").val()
                                },
                                mobile: function () {
                                    return $("input[name='mobile']").val();
                                }
                            }
                        }
                    },
                },
                messages: {
                    username: {
                        alpha: "Please enter only alphabets",
                        required: "Please specify your full name"
                    },
                    email: {
                        required: "We need your email address to contact you",
                        email: "Your email address must be in the format of name@domain.com",
                        remote: "Email already exist"
                    },
                    cpassword: {
                        equalTo: "Confirm password is not matching with password "
                    },
                    mobile: {
                        number: "Please enter valid Mobile number",
                        remote: "Mobile already exist"
                    },
                    mobile: {
                        number: "Please enter valid Mobile number",
                        remote: "Mobile already exist"
                    },
                    gender: "Please check a gender!",
                },
                errorPlacement: function (error, element) {
                    if (element.attr('name') == 'gender') {
                        error.insertAfter($('.gender-option-container'));
                    } else {
                        error.insertAfter(element);
                    }
                },
                submitHandler: function (form) {

                    if ($('#nationality').val() == 95 && $('#indosno').val() == '') {
                        $('.indos-error').removeClass('d-none');
                        return false;
                    }

                    $.ajax({
                        type: "POST",
                        url: "{{ route('site.seafarer.pre.register') }}",
                        data: $("#preRegistrationFormModalTop").serialize(),
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content'),
                        },
                        success: function (response) {
                            alert('Thanks for registration');
                            location.reload();
                        }
                    });
                }
            });
            </script>
            <script type="text/javascript">
                $('.login-btn').click(function () {
                    $('.login-div').toggleClass('collapse');
                });
            </script>
        </div>
    </body>
</html>