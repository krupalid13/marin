@extends('site.index')
@section('content')
<?php
    $rpsl_company_id = array_search('RPSL Company',\CommonHelper::company_type());
?>
	<div class="section job-application-listing content-section-wrapper sm-filter-space">
		<div class="row no-margin">
			<div class="col-md-2">
				@include('site.partials.side_navbar')
			</div>
			<div class="col-md-10" id="main-data">
				@if(isset($data) AND !empty($data))
				<div class="section listing-container">
					{{csrf_field()}}
					<div class="row">
		                <div class="col-xs-12 col-sm-6">
		                    <div class="company-page-profile-heading">
		                        Job Listing
		                    </div>
		                </div>
		                <div class="col-xm-12 col-sm-6 m-b-10">
		                	<span class="pagi pagi-up">
		                        <span class="search-count m-t-10">
		                            Showing {{$pagination_data['from']}} - {{$pagination_data['to']}} of {{$pagination_data['total']}} Jobs
		                        </span>
		                        <nav> 
			                        <ul class="pagination pagination-sm">
			                            {!! $paginate->render() !!}
			                        </ul> 
		                        </nav>
		                    </span>
		                </div>	
		            </div>

					@foreach($data as $job_details)
						<div class="job-card">
							<div class="dropdown setting">
								<a data-toggle="dropdown" class="btn-xs dropdown-toggle">
									<i class="fa fa-cog"></i>
								</a>
								<ul class="dropdown-menu dropdown-light pull-right">
									<li>
										<a href="{{route('site.company.edit.jobs', $job_details['id'])}}">
											<span>Edit</span>
										</a>
									</li>
									
									<?php 
										$disable= 'hide';
										$enable= 'hide';
										if(isset($job_details) && $job_details['status'] == 1){
											$disable = '';
										}else{
											$enable = '';
										}
									?>
									<li>
										<a class="jobDisableButton disable-{{$job_details['id']}} {{$disable}}" data-status='disable' data-id={{$job_details['id']}}>
											<span>Disable</span>
										</a>
									</li>
									
									<li>
										<a class="jobDisableButton enable-{{$job_details['id']}} {{$enable}}" data-status='enable' data-id={{$job_details['id']}}>
											<span>Enable</span>
										</a>
									</li>
								</ul>
							</div>
							
							<div class="row m-0 flex-height">
								<div class="col-xs-12 col-sm-3 p-0">
									<div class="job-company-image h-100 p-5">
									<img src="{{asset(!empty($job_details['company_registration_details']['user_details']['profile_pic']) ? "/".env('COMPANY_LOGO_PATH').$job_details['company_registration_details']['user_id']."/".$job_details['company_registration_details']['user_details']['profile_pic'] : 'images/user_default_image.png')}}" avatar-holder-src="{{ asset('images/user_default_image.png') }}">
									</div>
								</div>
								<div class="col-xs-12 col-sm-9 p-0">
									<div class="company-discription">
										<div class="row">
											<div class="col-sm-12">
												<div class="company-name">
													{{isset($job_details['job_title']) ? $job_details['job_title'] : ''}} 
													
													{{$job_details['company_details']['company_type'] == $rpsl_company_id ? isset($job_details['company_details']['rpsl_no']) ? "RPSL - ".$job_details['company_details']['rpsl_no'] : '' : ''}}
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-6">
												<div class="other-discription">
													Opening Valid From: {{ isset($job_details['valid_from']) && !empty($job_details['valid_from']) ? $job_details['valid_from'] : '-'}}
												</div>
												<div class="other-discription">
													Rank:
													@foreach(\CommonHelper::new_rank() as $rank_index => $category)
														@foreach($category as $r_index => $rank)
															{{ isset($job_details['rank']) ? $job_details['rank'] == $r_index ? $rank : '' : ''}}
														@endforeach
													@endforeach
												</div>
												<div class="other-discription">
													Flag:
														@if(isset($job_details['ship_flag']) && !empty($job_details['ship_flag']))
															@foreach(\CommonHelper::countries() as $index => $flag)
																{{ $job_details['ship_flag'] == $index ? $flag : '' }}
															@endforeach
														@else
															-
														@endif
												</div>
												<div class="other-discription">
													GRT: {{ isset($job_details['grt']) ? $job_details['grt'] : '-'}}
												</div>
												<div class="other-discription">
													Minimum Experience Required: {{ isset($job_details['min_rank_exp']) ? $job_details['min_rank_exp'] : '-'}} Years
												</div>
												<div class="other-discription">
													Date of Joining: {{ isset($job_details['date_of_joining']) && !empty($job_details['date_of_joining']) ? $job_details['date_of_joining'] : '-'}}
												</div>
												<?php
						                            $nationality_id = '';
						                            $count_nationality = 1;
						                            if(isset($job_details['job_nationality'] ) && !empty($job_details['job_nationality'] )){
						                                $nationality_id = array_values(collect($job_details['job_nationality'] )->pluck('nationality')->toArray());
						                            }

						                        ?>
												<div class="other-discription">
													Nationality:

													@if(!empty($nationality_id))
														@foreach( \CommonHelper::countries() as $c_index => $country)
															@if(!empty($nationality_id) && in_array($c_index,$nationality_id))
					                                            {{ $country }}
					                                            @if(count($nationality_id) > $count_nationality)
					                                            	,<?php $count_nationality++; ?>
					                                            @endif
					                                        @endif
														@endforeach
													@else
														-
													@endif
												</div>
											</div>
											<div class="col-sm-6">
												<div class="other-discription">
													Job Posting In:
													@foreach(\CommonHelper::seafarer_job_category() as $index => $category)
														{{ isset($job_details['job_category']) ? $job_details['job_category'] == $index ? $category : '' : ''}}
													@endforeach
												</div>
												<div class="other-discription">
													Ship Type:
													@foreach( \CommonHelper::ship_type() as $c_index => $ship_type)
														{{ isset($job_details['ship_type']) ? $job_details['ship_type'] == $c_index ? $ship_type : '' : ''}}
													@endforeach
												</div>
												<div class="other-discription">
													Engine Type:
													@if(isset($job_details['engine_type']) && !empty($job_details['engine_type']))
														@foreach( \CommonHelper::engine_type() as $c_index => $engine_type)
															{{ !empty($job_details['engine_type']) ? $job_details['engine_type'] == $c_index ? $engine_type : '' : ''  }}
														@endforeach
													@else
														-
													@endif
												</div>
												<div class="other-discription">
													BHP: {{ isset($job_details['bhp']) ? $job_details['bhp'] : '-'}}
												</div>
												<div class="other-discription">
													Sal Offered:
														@if(isset($job_details['wages_currency']) && $job_details['wages_currency'] == 'rupees')
															<i class="fa fa-inr" aria-hidden="true"></i>
														@elseif(isset($job_details['wages_currency']) && $job_details['wages_currency'] == 'dollar')
															<i class="fa fa-usd" aria-hidden="true"></i>
														@else
															-
														@endif

														@if(isset($job_details['wages_offered']) && !empty(isset($job_details['wages_offered'])))
															{{$job_details['wages_offered']}}
														@endif
												</div>
												<div class="other-discription">
													Created On: {{ isset($job_details['created_at']) && !empty($job_details['created_at']) ? date('d-m-Y H:i a', strtotime($job_details['created_at'])) : '-'}}
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-12">
												<div class="other-discription">
													Job Description: {{ isset($job_details['job_description']) ? $job_details['job_description'] : '-'}}
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					@endforeach

					<div class="row">
		                <div class="col-xm-12 col-sm-12 m-t-10">
		                	<span class="pagi pagi-up">
		                        <span class="search-count m-t-10">
		                            Showing {{$pagination_data['from']}} - {{$pagination_data['to']}} of {{$pagination_data['total']}} Jobs
		                        </span>
		                        <nav> 
			                        <ul class="pagination pagination-sm">
			                            {!! $paginate->render() !!}
			                        </ul> 
		                        </nav>
		                    </span>
		                </div>	
		            </div>
				</div>
				@else
					<div class="company-page-profile-heading">
	                    Job Listing
	                </div>
					<div class="no-results-found">No Results Found.</div>
				@endif
			</div>
		</div>
	</div>
	<script type="text/javascript" src="/js/site/job.js"></script>
@stop