@extends('site.index')
@section('content')

<input type="hidden" id="max-limit-download-resume" class="max-limit-download-resume" value="{{!empty(session()->has('message')) ? session('message') : ''}}">

<div id="max-limit-resume-download-reached" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Warning</h4>
      </div>
      <div class="modal-body danger">
        <p id='resume-message'>Maximum resume download limit reached.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

	<div class="section job-application-listing content-section-wrapper sm-filter-space">
		<div class="row no-margin">
			<div class="col-md-2">
				@include('site.partials.side_navbar')
			</div>
			<div class="col-md-10" id="main-data">
				@if(isset($data) AND !empty($data))
				<div class="listing-container">
					{{csrf_field()}}
					<div class="row">
						<div class="col-xs-12 col-sm-4">
		                    <div class="company-page-profile-heading">
		                        Job Applicant Listing
		                    </div>
		                </div>
		                <div class="col-xs-12 col-sm-8">
							<span class="pagi pagi-up m-t-10">
		                        <span class="search-count" style="padding-bottom: 5px;">
		                            Showing {{$pagination_data['from']}} - {{$pagination_data['to']}} of {{$pagination_data['total']}} Applicants
		                        </span>
		                        <nav>
		                        <ul class="pagination pagination-sm">
		                            {!! $pagination->render() !!}
		                        </ul>
		                        </nav>
		                    </span>
	                    </div>
					</div>
					@foreach($data as $job_data)
							<div class="job-card">
								<!-- <div class="dropdown setting">
									<a data-toggle="dropdown" class="btn-xs dropdown-toggle">
										<i class="fa fa-cog"></i>
									</a>
									<ul class="dropdown-menu dropdown-light pull-right">
										<li>
											<a href="{{route('site.company.edit.jobs', $job_data['id'])}}">
												<span>Edit</span>
											</a>
										</li>
										<li>
											<a class="jobDisableButton" data-id={{$job_data['id']}}>
												<span>Disable</span>
											</a>
										</li>
									</ul>
								</div> -->
								<div class="row m-0 flex-height">
									<div class="col-xs-12 col-sm-3 p-0">
										<div class="job-company-image h-100 job-applicant-profile-pic-box job_applicant_pic">
											<div class="job_applicant_pic_name">
												<img src="{{!empty($job_data['user']['profile_pic']) ? "/".env('SEAFARER_PROFILE_PATH').$job_data['user_id']."/".$job_data['user']['profile_pic'] : 'images/user_default_image.png'}}" avatar-holder-src="{{ asset('images/user_default_image.png') }}" class="job-applicant-user-profile-pic">
												<span>{{ isset($job_data['user']['first_name']) ? ucwords($job_data['user']['first_name']) : ''}}</span>
											</div>
											<div class="profile-btn-container">
	                                            <!-- <a href="{{route('site.company.candidate.download-cv',$job_data['user']['id'])}}" class="btn coss-inverse-btn cv-btn d-btn download-cv" seafarer-index='{{$job_data['user']['id']}}'>
	                                            	Download CV
	                                            </a> -->
	                                            <a class="btn coss-inverse-btn cv-btn d-btn download-cv check-company-subscription" seafarer-index='{{$job_data['user']['id']}}' data-type="{{config('feature.feature1')}}">
	                                            	Download CV
	                                            </a>
	                                            <a class="btn coss-primary-btn view-profile-btn d-btn" target="_blank" href="{{route('user.view.profile',[$job_data['user']['id']])}}">
	                                            	View Profile
	                                            </a>
	                                        </div>
	                                    </div>
									</div>
									<div class="col-sm-9 p-0">
										<div class="company-discription">
											<div class="row">
												<div class="col-sm-6">
													<div class="row">
														<div class="col-xs-12">
															<div class="other-discription">
																<b>Seafarer Details</b>
															</div>
														</div>
														<div class="col-xs-12">
															<div class="other-discription">
																Applied Rank:
																@foreach(\CommonHelper::new_rank() as $rank_index => $category)
																	@foreach($category as $r_index => $rank)
																		{{ isset($job_data['user']['professional_detail']['applied_rank']) ? $job_data['user']['professional_detail']['applied_rank'] == $r_index ? $rank : '' : ''}}
																	@endforeach
																@endforeach
															</div>
														</div>
														<div class="col-xs-12">
															<div class="other-discription">
																Nationality:
																@foreach( \CommonHelper::countries() as $c_index => $country)
																	{{ isset($job_data['company_job']['nationality']) ? $job_data['company_job']['nationality'] == $c_index ? $country : '' : ''}}
																@endforeach
															</div>
														</div>
														<div class="col-xs-12">
															<?php
															if(isset($job_data['user']['professional_detail']['current_rank_exp'])){
																$rank_exp = explode(".",$job_data['user']['professional_detail']['current_rank_exp']);
																$rank_exp[1] = number_format($rank_exp[1]);
															}
															?>
															<div class="other-discription">
																Rank Exp:
																<span class="ans">
																	{{ isset($job_data['user']['professional_detail']['current_rank_exp']) ? $rank_exp[0] ." Years ". $rank_exp[1] . " Months" : ''}}
																</span>
															</div>
														</div>
														<div class="col-xs-12">
															<div class="other-discription">
																Availability:
																{{ isset($job_data['user']['professional_detail']['availability']) ? date('d-m-Y',strtotime($job_data['user']['professional_detail']['availability'])) : ''}}
															</div>
														</div>
														<div class="col-xs-12">
															<div class="other-discription">
																Last Wages:
																<i class="fa fa-inr" aria-hidden="true"></i>
																{{ isset($job_data['user']['professional_detail']['last_salary']) ? $job_data['user']['professional_detail']['last_salary'] : ''}}
															</div>
														</div>
													</div>
												</div>

												<hr class="visible-xs">

												<div class="col-sm-6">
													<div class="row">
														<div class="col-xs-12">
															<div class="other-discription">
																<b>Applied Job Details</b>
															</div>
														</div>
														<div class="col-xs-12">
															<div class="other-discription">
																Rank:
																@foreach(\CommonHelper::new_rank() as $rank_index => $category)
																	@foreach($category as $r_index => $rank)
																		{{ isset($job_data['company_job']['rank']) ? $job_data['company_job']['rank'] == $r_index ? $rank : '' : ''}}
																	@endforeach
																@endforeach
															</div>
														</div>
														<div class="col-xs-12 col-sm-6">
															<div class="other-discription">
																Type:
																@foreach( \CommonHelper::ship_type() as $c_index => $ship_type)
																	{{ isset($job_data['company_job']['ship_type']) ? $job_data['company_job']['ship_type'] == $c_index ? $ship_type : '' : ''}}
																@endforeach
															</div>
														</div>
														<div class="col-xs-12 col-sm-6">
															<div class="other-discription">
																Vessel Flag:
																@if(isset($job_data['company_job']['ship_flag']) && !empty($job_data['company_job']['ship_flag']))
																	@foreach(\CommonHelper::countries() as $index => $flag)
																		{{ $job_data['company_job']['ship_flag'] == $index ? $flag : '' }}
																	@endforeach
																@else
																	-
																@endif
															</div>
														</div>
														<div class="col-xs-12">
															<div class="other-discription">
																Min Rank Exp:
																{{ isset($job_data['company_job']['min_rank_exp']) ? $job_data['company_job']['min_rank_exp'] : ''}} yrs
															</div>
														</div>
														<div class="col-xs-12">
															<div class="other-discription">
																Expected Joining:
																{{ isset($job_data['company_job']['date_of_joining']) ? date('d-m-Y',strtotime($job_data['company_job']['date_of_joining'])) : '-'}}
															</div>
														</div>
														<div class="col-xs-12">
															<div class="other-discription">
																Wages offered:
																@if(isset($job_data['company_job']['wages_currency']) && $job_data['company_job']['wages_currency'] == 'rupees')
																	<i class="fa fa-inr" aria-hidden="true"></i>
																@elseif(isset($job_data['company_job']['wages_currency']) && $job_data['company_job']['wages_currency'] == 'dollar')
																	<i class="fa fa-usd" aria-hidden="true"></i>
																@else
																	-
																@endif
																@if(isset($job_data['company_job']['wages_offered']) && !empty(isset($job_data['company_job']['wages_offered'])))
																	{{$job_data['company_job']['wages_offered']}}
																@endif
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
					@endforeach
					<div class="row">
		                <div class="col-xs-12 col-sm-12">
							<span class="pagi pagi-up m-t-10">
		                        <span class="search-count" style="padding-bottom: 5px;">
		                            Showing {{$pagination_data['from']}} - {{$pagination_data['to']}} of {{$pagination_data['total']}} Applicants
		                        </span>
		                        <nav>
		                        <ul class="pagination pagination-sm">
		                            {!! $pagination->render() !!}
		                        </ul>
		                        </nav>
		                    </span>
	                    </div>
					</div>
				</div>
				@else
                    <div class="company-page-profile-heading">
                        Job Applicant Listing
                    </div>
					<div class="no-results-found">No Results Found.</div>
				@endif
			</div>
		</div>
	</div>
@stop
@section('js_script')
    <script type="text/javascript" src="/js/site/dashboard.js"></script>
@stop