@extends('site.index')
@section('content')

	<div class="section job-application-listing content-section-wrapper sm-filter-space">
		<div class="row no-margin">
			<div class="col-md-2">
				@include('site.partials.side_navbar')
			</div>
			<input type="hidden" id="max-limit-download-resume" class="max-limit-download-resume" value="{{!empty(session()->has('message')) ? session('message') : ''}}">
                    
			<div id="max-limit-resume-download-reached" class="modal fade" role="dialog">
				<div class="modal-dialog">

				    <!-- Modal content-->
				    <div class="modal-content">
				      <div class="modal-header">
				        <button type="button" class="close" data-dismiss="modal">&times;</button>
				        <h4 class="modal-title">Warning</h4>
				      </div>
				      <div class="modal-body danger">
				        <p id='resume-message'>Maximum resume download limit reached.</p>
				      </div>
				      <div class="modal-footer">
				        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				      </div>
				    </div>

				</div>
			</div>
			<div class="col-md-10" id="main-data">
				@if(isset($data) AND !empty($data))
				<div class="section listing-container">
					{{csrf_field()}}
					<div class="row">
		                <div class="col-xs-12 col-sm-6">
		                    <div class="company-page-profile-heading">
		                        Resume Downloaded
		                    </div>
		                </div>
		                <div class="col-xm-12 col-sm-6">
		                	<span class="pagi pagi-up">
		                        <span class="search-count m-t-10">
		                            Showing {{$pagination_data['from']}} - {{$pagination_data['to']}} of {{$pagination_data['total']}} Resumes
		                        </span>
		                        <nav> 
			                        <ul class="pagination pagination-sm">
			                            {!! $pagination->render() !!}
			                        </ul> 
		                        </nav>
		                    </span>
		                </div>	
		            </div>

					@foreach($data as $details)
						<div class="job-card">
							<div class="row m-0 flex-height">
								<div class="col-xs-12 col-sm-3 text-center p-10">
									<img src="{{!empty($details[0]['user_details']['profile_pic']) ? "/".env('SEAFARER_PROFILE_PATH').$details[0]['user_details']['id']."/".$details[0]['user_details']['profile_pic'] : 'images/user_default_image.png'}}" avatar-holder-src="{{ asset('images/user_default_image.png') }}" class="job-applicant-user-profile-pic">
									<span class="seafarer-name d-block p-t-10">
										{{ isset($details[0]['user_details']['first_name']) && !empty($details[0]['user_details']['first_name']) ? ucwords($details[0]['user_details']['first_name']) : ''}}
									</span>
								</div>
								<div class="col-xs-12 col-sm-9 p-0 d-flex">
									<div class="company-discription">
										<div class="row">
											
											<div class="col-sm-6">
												<div class="other-discription">
													Current Rank: 
													@foreach(\CommonHelper::new_rank() as $index => $category)
														@foreach($category as $r_index => $rank)
															{{ !empty($details[0]['user_details']['professional_detail']['current_rank']) ? $details[0]['user_details']['professional_detail']['current_rank'] == $r_index ? $rank : '' : ''}}
														@endforeach
													@endforeach
												</div>
											</div>
											<div class="col-sm-6">
												<div class="other-discription">
													Resume Downloaded On:
													<span class="txt-red">
														{{ isset($details[0]['created_at']) ? date('h:i a , d-m-Y', strtotime($details[0]['created_at'])) : ''}}
													</span>
												</div>
											</div>
											<?php 
												if (isset($details[0]['user_details']['professional_detail']['current_rank_exp']) && !empty($details[0]['user_details']['professional_detail']['current_rank_exp'])){
									                $rank_exp = explode(".", $details[0]['user_details']['professional_detail']['current_rank_exp']);
									                $details[0]['user_details']['professional_detail']['years'] = $rank_exp[0];
									                $details[0]['user_details']['professional_detail']['months'] = $rank_exp[1];
									            }
											?>
											<div class="col-sm-6">
												<div class="other-discription">
													Current Rank Experience: 
													{{ isset($details[0]['user_details']['professional_detail']['years']) ? $details[0]['user_details']['professional_detail']['years'] : '0'}} Years 
													{{ isset($details[0]['user_details']['professional_detail']['months']) && ($details[0]['user_details']['professional_detail']['months'] != '') ? $details[0]['user_details']['professional_detail']['months'] : '0' }} Months
												</div>
											</div>
											<div class="col-sm-6">
												<div class="other-discription">
													Profile Updated On:
													<span class="txt-green">
														{{ isset($details[0]['user_details']['updated_on']) ? date('h:i a , d-m-Y', strtotime($details[0]['user_details']['updated_on'])) : '-'}}
													</span>
												</div>
											</div>
											<div class="col-sm-offset-6 col-sm-6">
												<div class="other-discription">
													<!-- <a href="{{route('site.company.candidate.download-cv',$details[0]['user_details']['id'])}}" class="btn coss-inverse-btn cv-btn d-btn download-cv" seafarer-index='{{$details[0]['user_details']['id']}}'>
				                                    	
				                                    </a> -->
				                                    <a class="btn coss-inverse-btn cv-btn d-btn check-company-subscription" seafarer-index="{{$details[0]['user_details']['id']}}" data-type="{{config('feature.feature1')}}">Re-Download CV</a>
				                                </div>
				                            </div>
										</div>
									</div>
								</div>
							</div>
						</div>
					@endforeach
					<div class="row">
						<div class="col-xm-12 col-sm-12">
		                	<span class="pagi pagi-up">
		                        <span class="search-count m-t-10">
		                            Showing {{$pagination_data['from']}} - {{$pagination_data['to']}} of {{$pagination_data['total']}} Resumes
		                        </span>
		                        <nav> 
			                        <ul class="pagination pagination-sm">
			                            {!! $pagination->render() !!}
			                        </ul> 
		                        </nav>
		                    </span>
		                </div>
		            </div>
				</div>
				@else
					<div class="company-page-profile-heading">
	                    Resume Dowloaded Listing
	                </div>
					<div class="no-results-found">No Results Found.</div>
				@endif
			</div>
		</div>
	</div>

	<script type="text/javascript" src="/js/site/dashboard.js"></script>
	<script type="application/javascript">
		var check_company_subscription_url = "{{route('site.checkSubscriptionAvailability')}}";
	    var feature_1 = "{{config('feature.feature1')}}";
    </script>

@stop