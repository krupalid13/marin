@extends('site.index')

@section('content')

<div class="candidate-search content-section-wrapper sm-filter-space">
    <button type="button" class="btn candidate-search-modal-btn" data-toggle="modal" data-target="#candidate-search-filter-modal"><i class="fa fa-filter" aria-hidden="true"></i></button>
    <div class="row no-margin">
        <div class="col-md-2">
            @include('site.partials.side_navbar')
        </div>
        <div class="col-md-10" id="main-data">
            <div class="row">
                <div class="col-xs-12">
                    <div class="company-page-profile-heading">
                        Candidate Search
                    </div>
                </div>
            </div>
            @if(isset($not_activated))
                <div class="row">
                    <div class="col-md-12" id="main-data">
                        <div class="alert alert-block alert-danger fade in">
                            <p>
                                Company is not approved by admin yet.
                            </p>
                        </div>
                    </div>
                </div>
            @else
                <div class="candidate-search-container">
                    <div class="section-1">
                        <?php $seafarer_data = $data->toArray(); ?>
                        <form id="candidate-filter-form" action={{route('site.company.candidate.search')}}>
                            <div class="row">
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label>Applied Rank </label>
                                        <select id="rank" name="rank" class="form-control">
                                            <option value=>Select Rank</option>
                                            @foreach(\CommonHelper::new_rank() as $rank_index => $category)
                                                <optgroup label="{{$rank_index}}">
                                                @foreach($category as $r_index => $rank)
                                                    <option value="{{$r_index}}" {{ isset($filter['rank']) ? $filter['rank'] == $r_index ? 'selected' : '' : ''}}>{{$rank}} </option>
                                                @endforeach
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label>Ship Type</label>
                                        <select class="form-control" name="ship_type" id="ship_type">
                                            <option value=>Select Ship Type</option>
                                            @foreach(\CommonHelper::ship_type() as $r_index => $ship_type)
                                                <option value="{{$r_index}}" {{ !empty($filter['ship_type']) ? $filter['ship_type'] == $r_index ? 'selected' : '' : ''}}>{{$ship_type}} </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label>Rank Experience</label>
                                            <select class="form-control" id="rank_exp_from" name="rank_exp_from">
                                                  <option value=''>Select Rank Experience</option>
                                                  <option value='0-1'>0-1</option>
                                                  <option value='1-2'>1-2</option>
                                                  <option value='2-3'>2-3</option>
                                                  <option value='3+'>3+</option>
                                            </select>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label>Nationality</label>
                                        <select class="form-control" id="nationality" name="nationality">
                                            <option value="">Select Nationality</option>
                                            @foreach( \CommonHelper::countries() as $c_index => $country)
                                                <option value="{{ $c_index }}"> {{ $country }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <!-- <div class="col-sm-3">
                                    <div class="form-group">
                                        <label>COC</label>
                                        <select class="form-control" id="coc" name="coc">
                                            <option value="">Select COC</option>
                                            @foreach( \CommonHelper::countries() as $c_index => $country)
                                                <option value="{{ $c_index }}"> {{ $country }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div> -->
                                
                                <!-- <div class="col-sm-4">
                                    <div><label>Availability</label></div>
                                        <div class="start">
                                            <div class="form-group">
                                                
                                                <div class="input-group">
                                                  <div class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></div>
                                                  <input type="text" class="form-control" name='availability_from' id="exampleInputAmount" placeholder="From Date">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="to">
                                            To
                                        </div>
                                        <div class="end">
                                            <div class="form-group">
                                                
                                                <div class="input-group">
                                                  <div class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></div>
                                                  <input type="text" class="form-control" name='availability_to' id="exampleInputAmount" placeholder="To Date">
                                                </div>
                                            </div>
                                        </div>
                                </div> -->
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label>CDC</label>
                                        <select class="form-control" id="cdc" name="cdc">
                                            <option value="">Select CDC</option>
                                            @foreach( \CommonHelper::countries() as $c_index => $country)
                                                <option value="{{ $c_index }}"> {{ $country }}</option>
                                            @endforeach
                                        </select>   
                                    </div>
                                </div>
                                <!-- <div class="col-sm-3">
                                    <div class="form-group">
                                        <label>COP</label>
                                        <select class="form-control" id="cop" name="cop">
                                            <option value="">Select COP</option>
                                            @foreach( \CommonHelper::countries() as $c_index => $country)
                                                <option value="{{ $c_index }}"> {{ $country }}</option>
                                            @endforeach
                                        </select>   
                                    </div>
                                </div> -->
                                <!-- <div class="col-sm-3">
                                    <div class="form-group">
                                        <label>Watch Keeping</label>
                                        <select class="form-control" id="watch_keeping" name="watch_keeping">
                                            <option value="">Select Watch Keeping</option>
                                            @foreach( \CommonHelper::countries() as $c_index => $country)
                                                <option value="{{ $c_index }}"> {{ $country }}</option>
                                            @endforeach
                                        </select>   
                                    </div>
                                </div> -->
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label>Availability From</label>
                                        <input type="text" name="availability_from" class="form-control datepicker" placeholder="dd-mm-yyyy" value="{{isset($filter['availability_from']) ? $filter['availability_from'] : ''}}"> 
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label>Availability To</label>
                                        <input type="text" name="availability_to" class="form-control datepicker" placeholder="dd-mm-yyyy" value="{{isset($filter['availability_to']) ? $filter['availability_to'] : ''}}"> 
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="section_cta text-right m-t-25 job-btn-container">
                                        <button type="button" class="btn coss-inverse-btn search-reset-btn" id="jobResetButton">Reset</button>
                                        <button type="button" data-style="zoom-in" class="btn coss-primary-btn search-post-btn ladda-button candidate-filter-search-button" id="candidate-filter-search-button"><i class="fa fa-search search-icon" aria-hidden="true"></i>Search</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>

                    @if(isset($seafarer_data['data']) && !empty($seafarer_data['data']))

                    <div class="section-2" id="filter-results">
                        <span class="heading">
                            Candidate Search Results
                        </span>
                        <span class="pagi pagi-up">
                            <span class="search-count">
                                Showing {{$seafarer_data['from']}} - {{$seafarer_data['to']}} of {{$seafarer_data['total']}} Candidates
                            </span>
                            <nav> 
                            <ul class="pagination pagination-sm">
                                {!! $data->render() !!}
                            </ul> 
                            </nav>
                        </span>

                        @foreach($seafarer_data['data'] as $seafarer_details)

                            <div class="search-result-card">
                                <div class="row m-0">
                                    <div class="col-sm-3 p-0">
                                        <div class="profile-pic-container">
                                            <img class="profile-pic" src="/{{ isset($seafarer_details['profile_pic']) ? env('SEAFARER_PROFILE_PATH')."/".$seafarer_details['id']."/".$seafarer_details['profile_pic'] : 'images/user_default_image.png'}}" avatar-holder-src="{{ asset('images/user_default_image.png') }}" alt="">

                                            <div class="profile-btn-container">
                                                <!-- <a href="{{route('site.company.candidate.download-cv',$seafarer_details['id'])}}" class="btn coss-inverse-btn cv-btn download-cv" seafarer-index='{{$seafarer_details['id']}}'>Download CV</a>
                                                <a class="btn coss-primary-btn view-profile-btn" target="_blank" href="{{route('user.view.profile',[$seafarer_details['id']])}}">View Profile</a> -->
                                                <a class="btn coss-inverse-btn cv-btn check-company-subscription" seafarer-index="{{$seafarer_details['id']}}" data-type="{{config('feature.feature1')}}">Download CV</a>
                                                <a class="btn coss-primary-btn view-profile-btn" target="_blank" href="{{route('user.view.profile',[$seafarer_details['id']])}}">View Profile</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="company-discription">
                                        <div class="company-name">
                                            {{isset($seafarer_details['first_name']) ? ucfirst($seafarer_details['first_name'])." ".ucfirst($seafarer_details['last_name']) : ''}}
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="other-discription">
                                                    Current Rank: 
                                                    <span class="ans">
                                                        @foreach(\CommonHelper::new_rank() as $rank_index => $category)
                                                            @foreach($category as $r_index => $rank)
                                                                {{ !empty($seafarer_details['professional_detail']['current_rank']) ? $seafarer_details['professional_detail']['current_rank'] == $r_index ? $rank : '' : ''}}
                                                            @endforeach
                                                        @endforeach
                                                    </span>
                                                </div>
                                                <?php
                                                    if(isset($seafarer_details['professional_detail']['current_rank_exp'])){
                                                        $rank_exp = explode(".",$seafarer_details['professional_detail']['current_rank_exp']);
                                                        $rank_exp[1] = number_format($rank_exp[1]);
                                                    }
                                                ?>
                                                <div class="other-discription">
                                                    Rank Experience: 
                                                    <span class="ans">
                                                        {{ isset($seafarer_details['professional_detail']['current_rank_exp']) ? $rank_exp[0] ." Year ". $rank_exp[1] .' Months' : ''}}
                                                    </span>
                                                </div>
                                                <div class="other-discription">
                                                    Applied Rank: 
                                                    <span class="ans">
                                                        @foreach(\CommonHelper::new_rank() as $rank_index => $category)
                                                            @foreach($category as $r_index => $rank)
                                                                {{ !empty($seafarer_details['professional_detail']['applied_rank']) ? $seafarer_details['professional_detail']['applied_rank'] == $r_index ? $rank : '' : ''}}
                                                            @endforeach
                                                        @endforeach
                                                    </span>
                                                </div>
                                                <div class="other-discription">
                                                    Availability: 
                                                    <span class="ans">{{isset($seafarer_details['professional_detail']['availability']) ? date('d-m-Y',strtotime($seafarer_details['professional_detail']['availability'])) : ''}}
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="other-discription">
                                                    Nationality: 
                                                    <span class="ans">
                                                        @foreach( \CommonHelper::countries() as $c_index => $country)
                                                            {{ !empty($seafarer_details['personal_detail']['nationality']) ? $seafarer_details['personal_detail']['nationality'] == $c_index ? $country : '' : ''  }}
                                                        @endforeach
                                                    </span>
                                                </div>
                                                <div class="other-discription">
                                                    Passport: 
                                                    <span class="ans">
                                                        @if(!empty($seafarer_details['passport_detail']['pass_country']))
                                                            @foreach( \CommonHelper::countries() as $c_index => $country)
                                                                {{ !empty($seafarer_details['passport_detail']['pass_country']) ? $seafarer_details['passport_detail']['pass_country'] == $c_index ? $country : '' : ''  }}
                                                            @endforeach
                                                        @else
                                                            -
                                                        @endif
                                                    </span>
                                                </div>
                                                <div class="other-discription">
                                                    COC: 
                                                    <span class="ans">
                                                        @if(!empty($seafarer_details['coc_detail'][0]['coc']))
                                                            @foreach( \CommonHelper::countries() as $c_index => $country)
                                                                {{ !empty($seafarer_details['coc_detail'][0]['coc']) ? $seafarer_details['coc_detail'][0]['coc'] == $c_index ? $country : '' : ''  }}
                                                            @endforeach
                                                        @else
                                                            -
                                                        @endif
                                                    </span>
                                                </div>
                                                <div class="other-discription">
                                                    CDC: 
                                                    <span class="ans">
                                                        @if(!empty($seafarer_details['coc_detail'][0]['cdc']))
                                                            @foreach( \CommonHelper::countries() as $c_index => $country)
                                                                {{ !empty($seafarer_details['seaman_book_detail'][0]['cdc']) ? $seafarer_details['seaman_book_detail'][0]['cdc'] == $c_index ? $country : '' : ''  }}
                                                            @endforeach
                                                        @else
                                                            -
                                                        @endif
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                        
                        <span class="pagi m-t-25">
                            <span class="search-count">
                                Showing {{$seafarer_data['from']}} - {{$seafarer_data['to']}} of {{$seafarer_data['total']}} Candidates
                            </span>
                            <nav> 
                            <ul class="pagination pagination-sm">
                                {!! $data->render() !!}
                            </ul> 
                            </nav>
                        </span>

                    @else
                        <div class="section-2" id="filter-results"><div class="row no-results-found">No results found. Try again with different search criteria.</div></div>
                    @endif

                    </div>
                </div>
            @endif
        </div>
    </div>
</div>

<div class="modal fade" id="candidate-search-filter-modal" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title text-center">Candidate search filter</h4>
        </div>
        <div class="modal-body">
        <form id="candidate-filter-form" action={{route('site.company.candidate.search')}}>
            <div class="row">
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Rank </label>
                        <select id="rank" name="rank" class="form-control">
                            <option value=>Select</option>
                            @foreach(\CommonHelper::new_rank() as $rank_index => $category)
                                <optgroup label="{{$rank_index}}">
                                @foreach($category as $r_index => $rank)
                                    <option value="{{$r_index}}" {{ isset($filter['rank']) ? $filter['rank'] == $r_index ? 'selected' : '' : ''}}>{{$rank}} </option>
                                @endforeach
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Ship Type</label>
                        <select class="form-control" name="ship_type" id="ship_type">
                            <option value=>Select ship type</option>
                            @foreach(\CommonHelper::ship_type() as $r_index => $ship_type)
                                <option value="{{$r_index}}" {{ !empty($filter['ship_type']) ? $filter['ship_type'] == $r_index ? 'selected' : '' : ''}}>{{$ship_type}} </option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Rank Experience</label>
                            <select class="form-control" id="rank_exp_from" name="rank_exp_from">
                                  <option value=''>Select Rank Experience</option>
                                  <option value='0-1'>0-1</option>
                                  <option value='1-2'>1-2</option>
                                  <option value='2-3'>2-3</option>
                                  <option value='3+'>3+</option>
                            </select>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Nationality</label>
                        <select class="form-control" id="nationality" name="nationality">
                            <option value="">Select Nationality</option>
                            @foreach( \CommonHelper::countries() as $c_index => $country)
                                <option value="{{ $c_index }}"> {{ $country }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>COC</label>
                        <select class="form-control" id="coc" name="coc">
                            <option value="">Select COC</option>
                            @foreach( \CommonHelper::countries() as $c_index => $country)
                                <option value="{{ $c_index }}"> {{ $country }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                
                <!-- <div class="col-sm-4">
                    <div><label>Availability</label></div>
                        <div class="start">
                            <div class="form-group">
                                
                                <div class="input-group">
                                  <div class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></div>
                                  <input type="text" class="form-control" name='availability_from' id="exampleInputAmount" placeholder="From Date">
                                </div>
                            </div>
                        </div>
                        <div class="to">
                            To
                        </div>
                        <div class="end">
                            <div class="form-group">
                                
                                <div class="input-group">
                                  <div class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></div>
                                  <input type="text" class="form-control" name='availability_to' id="exampleInputAmount" placeholder="To Date">
                                </div>
                            </div>
                        </div>
                </div> -->
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>CDC</label>
                        <select class="form-control" id="cdc" name="cdc">
                            <option value="">Select CDC</option>
                            @foreach( \CommonHelper::countries() as $c_index => $country)
                                <option value="{{ $c_index }}"> {{ $country }}</option>
                            @endforeach
                        </select>   
                    </div>
                </div>
                <!-- <div class="col-sm-3">
                    <div class="form-group">
                        <label>COP</label>
                        <select class="form-control" id="cop" name="cop">
                            <option value="">Select COP</option>
                            @foreach( \CommonHelper::countries() as $c_index => $country)
                                <option value="{{ $c_index }}"> {{ $country }}</option>
                            @endforeach
                        </select>   
                    </div>
                </div> -->
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Watch Keeping</label>
                        <select class="form-control" id="watch_keeping" name="watch_keeping">
                            <option value="">Select Watch Keeping</option>
                            @foreach( \CommonHelper::countries() as $c_index => $country)
                                <option value="{{ $c_index }}"> {{ $country }}</option>
                            @endforeach
                        </select>   
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Date Of Availability From</label>
                        <input type="text" name="availability_from" class="form-control datepicker" placeholder="dd-mm-yyyy" value="{{isset($filter['availability_from']) ? $filter['availability_from'] : ''}}"> 
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Date Of Availability To</label>
                        <input type="text" name="availability_to" class="form-control datepicker" placeholder="dd-mm-yyyy" value="{{isset($filter['availability_to']) ? $filter['availability_to'] : ''}}"> 
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="section_cta text-right m_t_25 job-btn-container">
                        <button type="button" class="btn coss-inverse-btn search-reset-btn" id="jobResetButton">Reset</button>
                        <button type="button" data-style="zoom-in" class="btn coss-primary-btn search-post-btn ladda-button candidate-filter-search-button" id="candidate-filter-search-button"><i class="fa fa-search search-icon" aria-hidden="true"></i>Search</button>
                    </div>
                </div>
            </div>
        </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
    </div>

<script type="application/javascript">

    $(document).on('ready',function () {
        $('.switch-checkbox').checkboxpicker();

        var History = window.History;
        var state = History.getState(),
            $log = $("#log");

        History.pushState(null,null,state.url);

        History.Adapter.bind(window, 'statechange', function(){
            var state = History.getState();
            fetchCandidateResult(state.url);
            
            $('input[name="passport"]').closest('.switch-checkbox-container').find('a[is="1"]').removeClass('btn-default').addClass('btn-danger active');

            $('input[name="passport"]').closest('.switch-checkbox-container').find('a[is="0"]').addClass('btn-default').removeClass('btn-success active');

            var temp_current_state_url = state.url.split('?');
            if(temp_current_state_url.length > 1 && temp_current_state_url[1] != ''){
                var parameters = temp_current_state_url[1].split('&');

                var dropdown_elements = ['rank','ship_type','rank_exp_from','rank_exp_to','coc','cdc','nationality'];

                for(var i=0; i<parameters.length; i++){

                    var param_array = parameters[i].split('=');
                    if($.inArray(param_array[0],dropdown_elements) > -1){
                        $('select[name="'+param_array[0]+'"]').val(param_array[1]);
                    }else if($.inArray(param_array[0],['availability_from','availability_to','rank_exp_from']) > -1){
                        $('input[name="'+param_array[0]+'"]').val(param_array[1]);
                    }else if(param_array[0] == 'passport'){
                        $('input[name="'+param_array[0]+'"]').closest('.switch-checkbox-container').find('a[is="0"]').removeClass('btn-default').addClass('btn-success active');

                        $('input[name="'+param_array[0]+'"]').closest('.switch-checkbox-container').find('a[is="1"]').addClass('btn-default').removeClass('btn-danger active');
                    }

                }
            } else {
                 $(':input','#candidate-filter-form')
                    .not(':button, :submit, :reset, :hidden')
                    .val('')
                    .removeAttr('checked')
                    .removeAttr('selected');
                        }
        });
    });

    
</script>
@stop

@section('js_script')
    <script type="text/javascript" src="/js/site/dashboard.js"></script>
@stop