@extends('site.index')

@section('content')
<div class="user-job-post content-section-wrapper sm-filter-space">
    <div class="no-margin row">
        <div class="col-md-2">
            @include('site.partials.side_navbar')
        </div>
        
        <div class="col-md-10" id="main-data">
            <div class="row">
                <div class="col-xs-12">
                    <div class="company-page-profile-heading">
                        Add Job Post
                    </div>
                </div>
            </div>
            @if(isset($not_activated))
                <div class="row">
                    <div class="col-md-12" id="main-data">
                        <div class="alert alert-block alert-danger fade in">
                            <p>
                                Company is not approved by admin yet.
                            </p>
                        </div>
                    </div>
                </div>
            @else
            <form id="add-job-form" method="post" action="{{ isset($job_data[0]['current_route']) ? route('site.company.update.jobs') : route('site.company.store.jobs') }}">
                {{ csrf_field() }}
                <div class="section job-post">
                <div class="job-discription-card">
                    <input type="hidden" id="job_id" name="job_id" value="{{isset($job_data[0]['job_id'])? $job_data[0]['job_id'] : ''}}">
                    <div class="row">
                        <!-- <div class="col-sm-6">
                            <div class="form-group">
                                <label class="input-label">Job Title<span class="symbol required"></label>
                                <input type="text" class="form-control" placeholder="Type your job title" id="job_title" name="job_title" value="{{ isset($job_data[0]['job_title']) ? $job_data[0]['job_title'] : '' }}">
                            </div>
                        </div> -->
                        
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="input-label" for="ship">Ship Name<span class="symbol required"></label>
                                <select id="ship_detail_id" name="ship_detail_id" class="form-control ship_name_change">
                                    <option value=''>Select Ship Name</option>
                                    @if(isset($ship_data[0]['ship_type']) && !empty($ship_data[0]['ship_type']))
                                        @foreach($ship_data[0]['ship_type'] as $c_index => $ship_type)
                                            <option value="{{ $ship_type['id'] }}" {{ !empty($job_data[0]['ship_detail_id']) ? $job_data[0]['ship_detail_id'] == $ship_type['id'] ? 'selected' : '' : ''  }}> {{ $ship_type['ship_name'] }}</option>
                                        @endforeach
                                    @endif
                                </select>
                             </div>
                        </div>

                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="input-label" for="job_category">Job Category<span class="symbol required"></label>
                                <select id="job_category" name="job_category" class="form-control">
                                    <option value=''>Select Category</option>
                                    @foreach(\CommonHelper::seafarer_job_category() as $index => $category)
                                        <option value="{{$index}}" {{ isset($job_data[0]['job_category']) ? $job_data[0]['job_category'] == $index ? 'selected' : '' : ''}}>{{$category}}</option>
                                    @endforeach
                                </select>
                             </div>
                        </div>
                    
                    </div>

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="input-label" for="ship">Ship Type<span class="symbol required"></label>
                                <select id="ship_type" class="form-control ship_type_change_name" disabled="disabled">
                                    <option value=''>Select Ship Type</option>
                                    @foreach( \CommonHelper::ship_type() as $c_index => $ship_type)
                                        @if(isset($ships) && !empty($ships))
                                            @if(in_array($c_index,$ships))
                                                <option value="{{ $c_index }}" {{ !empty($job_data[0]['ship_type']) ? $job_data[0]['ship_type'] == $c_index ? 'selected' : '' : ''  }}> {{ $ship_type }}</option>
                                            @endif
                                        @endif
                                    @endforeach
                                </select>
                                <input type="hidden" name="ship_type" class="ship_type" value="{{isset($job_data[0]['ship_type']) ? $job_data[0]['ship_type'] : ''}}">
                             </div>
                        </div>
                        
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="input-label">Rank<span class="symbol required"></label>
                                <select id="rank" name="rank" class="form-control">
                                    <option value=''>Select Rank</option>
                                    @foreach(\CommonHelper::new_rank() as $rank_index => $category)
                                        <optgroup label="{{$rank_index}}">
                                        @foreach($category as $r_index => $rank)
                                            <option value="{{$r_index}}" {{ isset($job_data[0]['rank']) ? $job_data[0]['rank'] == $r_index ? 'selected' : '' : ''}}>{{$rank}} </option>
                                        @endforeach
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="input-label">Ship Flag<span class="symbol required"></label>
                                <select class="form-control ship_flag" id="ship_flag" name="ship_flag">
                                    <option value="">Select Ship Flag</option>
                                    @foreach( \CommonHelper::countries() as $c_index => $country)
                                        <option value="{{ $c_index }}" {{ !empty($job_data[0]['ship_flag']) ? $job_data[0]['ship_flag'] == $c_index ? 'selected' : '' : ''  }}> {{ $country }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div> 

                        <?php
                            $nationality_id = '';
                            if(isset($job_data[0]['job_nationality'] ) && !empty($job_data[0]['job_nationality'] )){
                                $nationality_id = array_values(collect($job_data[0]['job_nationality'] )->pluck('nationality')->toArray());
                            }

                        ?>

                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="input-label">Nationality<span class="symbol required"></label>
                                <select multiple="multiple" name="nationality[]" class="form-control location select2-select" style="height: auto;" placeholder="Select Nationality">
                                    @foreach(\CommonHelper::countries() as $c_index => $country)
                                        @if(!empty($nationality_id) && in_array($c_index,$nationality_id))
                                            <option value="{{ $c_index }}" selected="selected"> {{ $country }}</option>
                                        @else
                                            <option value="{{ $c_index }}"> {{ $country }}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <?php 
                        if(isset($job_data[0]['date_of_joining']) && !empty($job_data[0]['date_of_joining'])){
                            $parts = explode('-', $job_data[0]['date_of_joining']);
                            $month = $parts[1];
                            $year = $parts[2];
                            
                            $last_day = cal_days_in_month(CAL_GREGORIAN,$month,$year);
                            $job_data[0]['date_of_joining'] = $month."-".$year;
                        }
                    ?>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="input-label" for="valid_from">Valid From<span class="symbol required"></label>
                                <i class="fa fa-calendar calendar-icon m-t-15" aria-hidden="true"></i>
                                <input type="text" class="form-control datepicker" id="valid_from" name="valid_from" value="{{ isset($job_data[0]['valid_from']) ? $job_data[0]['valid_from'] : '' }}" placeholder="dd-mm-yyyy">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="input-label" for="joiningdate">Date of Joining</label>
                                <i class="fa fa-calendar calendar-icon m-t-15" aria-hidden="true"></i>
                                <input type="text" class="form-control m-y-datepicker" id="date_of_joining" name="date_of_joining" value="{{ isset($job_data[0]['date_of_joining']) ? $job_data[0]['date_of_joining'] : '' }}" placeholder="mm-yyyy">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="input-label" for="grt">Approx GRT</label>
                                <input type="text" class="form-control grt" placeholder="Please enter grt" id="grt" value="{{ isset($job_data[0]['grt']) ? $job_data[0]['grt'] : '' }}" disabled="disabled">
                                <input type="hidden" class="form-control grt" name="grt" value="{{ isset($job_data[0]['grt']) ? $job_data[0]['grt'] : '' }}">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="input-label" for="bhp">Approx BHP</label>
                                <input type="text" class="form-control bhp" placeholder="Please enter bhp" id="bhp" value="{{ isset($job_data[0]['bhp']) ? $job_data[0]['bhp'] : '' }}" disabled="disabled">
                                <input type="hidden" class="form-control bhp" name="bhp" value="{{ isset($job_data[0]['bhp']) ? $job_data[0]['bhp'] : '' }}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="input-label" for="engine_type">Engine Type<span class="symbol required"></label>
                                <select class="form-control search-select engine_type" block-index="0" disabled="disabled">
                                        <option value="">Select Engine Type</option>
                                        @foreach( \CommonHelper::engine_type() as $c_index => $engine_type)
                                            <option value="{{ $c_index }}" {{ isset($job_data[0]['engine_type']) ? $job_data[0]['engine_type'] == $c_index ? 'selected' : '' : ''}}> {{ $engine_type }}</option>
                                        @endforeach
                                </select>
                                <input type="hidden" name="engine_type" class="form-control engine_type" name="engine_type"
                                @foreach( \CommonHelper::engine_type() as $c_index => $engine_type)
                                    value="{{ isset($job_data[0]['engine_type']) ? $job_data[0]['engine_type'] == $c_index ? 'selected' : '' : ''}}"
                                @endforeach
                                >
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="input-label" for="rankexp">Minimum rank exp<span class="symbol required"></span></label>
                                <input type="text" class="form-control" placeholder="Please enter minimum rank experience in years" id="min_rank_exp" name="min_rank_exp" value="{{ isset($job_data[0]['min_rank_exp']) ? $job_data[0]['min_rank_exp'] : '' }}">
                            </div>
                        </div> 
                        <?php
                            $dollar = '';
                            $rupees = '';
                            if(isset($job_data[0]['wages_currency'])){
                                if($job_data[0]['wages_currency'] == 'dollar'){
                                    $dollar = 'active';
                                }else{
                                    $rupees = 'active';
                                }
                            }else{
                                $rupees = 'active';
                            }
                        ?>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="input-label" for="wages_currency">Wages Offered</label>
                                <div class="col-xs-2 m-t-5 p-0">
                                    <div class="btn-group" id="wages_currency" data-toggle="buttons">
                                        <label class="btn btn-default btn-on btn-xs {{$dollar}}">
                                            <input type="radio" value="dollar" name="wages_currency" checked="checked">
                                            <i class="fa fa-dollar fa-lg"></i>
                                        </label>
                                        <label class="btn btn-default btn-xs btn-off {{$rupees}}">
                                            <input type="radio" value="rupees" name="wages_currency">
                                            <i class="fa fa-inr fa-lg"></i>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-xs-6 p-l-0">
                                    <input type="text" class="form-control" placeholder="Please enter wages offered" name="wages_offered" value="{{ isset($job_data[0]['wages_offered']) ? $job_data[0]['wages_offered'] : '' }}">
                                </div>
                                <div class="col-xs-4 p-10-0">
                                    Per Month
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="input-label" for="jobdescription">Job Description</label>
                                <textarea class="form-control" placeholder="Please enter job description" id="job_description" name="job_description" rows="5">{{isset($job_data[0]['job_description']) ? $job_data[0]['job_description']:''}}</textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="section_cta text-right m_t_25 job-btn-container">
                    <button type="button" class="job-reset-btn" data-style="zoom-in" class="ladda-button" id="jobResetButton">Reset</button>
                    <button type="button" style="height: 44px;" data-style="zoom-in" class="btn coss-primary-btn job-post-btn ladda-button submitCompanyJobsButton">Save</button>
                    <button type="button" style="height: 44px;" data-style="zoom-in" class="btn coss-primary-btn job-post-btn ladda-button data-add-more submitCompanyJobsButton">Save & Add</button>
                </div>
                </div>
            </form>
            @endif
        </div>
    </div>
</div>
@stop
@section('js_script')
    <script type="text/javascript" src="/js/site/job.js"></script>
    <script type="text/javascript">
        $('.select2-select').select2({
            closeOnSelect: false
        });
    </script>
@stop