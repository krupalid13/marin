@extends('site.index')

@section('content')
<div class="user-job-post content-section-wrapper sm-filter-space">
    <div class="no-margin row">
        <div class="col-md-2">
            @include('site.partials.side_navbar')
        </div>
        
        <div class="col-md-10" id="main-data">
            <div class="row">
                <div class="col-xs-12">
                    <div class="company-page-profile-heading">
                        My Scope Of Work
                    </div>
                </div>
            </div>
            @if(isset($not_activated))
                <div class="row">
                    <div class="col-md-12" id="main-data">
                        <div class="alert alert-block alert-danger fade in">
                            <p>
                                Company is not approved by admin yet.
                            </p>
                        </div>
                    </div>
                </div>
            @else
                <div class="scope-section">
                    <ul class="nav nav-tabs user-tabs m-t-25">
                        <li class="active">
                            <a data-toggle="tab" href="#owners">
                                <span>Owners</span>
                            </a>
                        </li>
                        <li>
                            <a data-toggle="tab" href="#managers">
                                <span>Managers</span>
                            </a>
                        </li>
                    </ul>
                    <?php
                        $owner_ship_yes = '';
                        $owner_ship_no = '';
                        $owner_section = 'hide';
                        if(isset($data[0]['owner_ship']) && !empty($data[0]['owner_ship'])){
                            $owner_ship_yes = 'checked';
                            $owner_section = '';
                        }else{
                            $owner_ship_no = 'checked';
                        }
                    ?>
                    <div class="tab-content">
                        <div id="owners" class="tab-pane fade in active">
                            <div class="owner-section">
                                
                                <?php
                                    $appointed_as_yes = '';
                                    $appointed_as_no = '';
                                    $appointed_as_section = 'hide';
                                    if(isset($data[0]['company_registration_detail']['appointed_agent']) && !empty($data[0]['company_registration_detail']['appointed_agent'])){
                                        $appointed_as_yes = 'checked';
                                        $appointed_as_section = '';
                                    }else{
                                        $appointed_as_no = 'checked';
                                    }
                                ?>
                                <div class="appointed-section display-section">
                                    <div class="vessel-details">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="scope-title">
                                                    Our appointed managers / associates

                                                    <span class="pull-right">
                                                        <label class="radio-inline">
                                                            <input type="radio" name="appointed" class="scope-radio" data-section="appointed-as" value="1" {{$appointed_as_yes}}>Yes</input>
                                                        </label>
                                                        <label class="radio-inline">
                                                            <input type="radio" name="appointed" class="scope-radio" data-section="appointed-as" {{$appointed_as_no}} value="0">No</input>
                                                        <label>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="m-t-15 appointed-as-section {{$appointed_as_section}}">
                                            <form id="appointed_agent" method="post" action="{{route('site.company.store.agent')}}">
                                                <input class="hidden" name="agent_type" value="appointed"></input>
                                                <input type="hidden" class="existing_agent_id" name="existing_agent_id" value="">
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-3">
                                                        <div class="agent" id="agent">
                                                            <label class="input-label" for="pic">Profile Pic</label>
                                                            <div class="upload-photo-container">
                                                                {{ csrf_field() }}
                                                                <input type="hidden" name="image-x" required>
                                                                <input type="hidden" name="image-y" required>
                                                                <input type="hidden" name="image-x2" required>
                                                                <input type="hidden" name="image-y2" required>
                                                                <input type="hidden" name="crop-w" required>
                                                                <input type="hidden" name="crop-h" required>
                                                                <input type="hidden" name="image-w" required>
                                                                <input type="hidden" name="image-h" required>
                                                                <input type="hidden" name="uploaded-file-name" required>
                                                                <input type="hidden" name="uploaded-file-path" required>
                                                                <input type="hidden" class="path" value="{{env('COMPANY_TEAM_AGENT_PIC_PATH')}}"></input>

                                                                <div class="image-content">
                                                                    <div class="profile-image registration-profile-image">
                                                                        <?php
                                                                            $image_path = '';
                                                                            if(isset($team['profile_pic'])){
                                                                                $image_path = env('COMPANY_TEAM_AGENT_PIC_PATH')."".$team['id']."/".$team['profile_pic'];
                                                                            }
                                                                        ?>
                                                                        @if(empty($image_path))
                                                                            <div class="icon profile_pic_text"><i class="fa fa-camera" aria-hidden="true"></i></div>
                                                                            <div class="image-text profile_pic_text">Upload Profile <br> Picture</div>
                                                                        @endif

                                                                        <input type="file" name="profile_pic" id="profile-pic" class="cursor-pointer" onChange="profilePicSelectHandler('appointed_agent', 'profile_pic', '' , 'agent_team')">

                                                                        @if(!empty($image_path))
                                                                            <img id="preview" class="preview" src="/{{ $image_path }}">
                                                                        @else
                                                                            <img id="preview">
                                                                        @endif
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <label class="input-label" for="ship_name">Company Name<span class="symbol required"></span></label>
                                                        <input type="text" name="company_name" class="form-control search-select company_name" value="{{ isset($value['company_name']) ? $value['company_name'] : ''}}" placeholder="Type your company name">
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <label class="input-label" for="location">Location<span class="symbol required"></span></label>
                                                        <input type="text" name="location" class="form-control search-select location" value="{{ isset($value['location']) ? $value['location'] : ''}}" placeholder="Type your location">
                                                    </div>
                                                    <div class="col-sm-3 m-t-25">
                                                        <button type="button" class="btn add-more-btn ladda-button save-agent-details" data-style="zoom-in" data-form="appointed_agent">Save</button>
                                                    </div>
                                                </div>
                                                <div class="row m-t-15">
                                                    <div class="col-sm-12">
                                                        <div class="description">
                                                            Agent Details
                                                        </div>
                                                    </div>
                                                </div> 
                                                <div class="appointed_agent">
                                                    @if(isset($data[0]['company_registration_detail']['appointed_agent']) && !empty($data[0]['company_registration_detail']['appointed_agent']))
                                                        @foreach($data[0]['company_registration_detail']['appointed_agent'] as $index => $agents)
                                                        <div class="vessel-section agent_section_{{$agents['id']}}">
                                                            <div class="row vessel-row">
                                                                <div class="col-sm-12 vessel-title">
                                                                    <div class="description">
                                                                        <span class="content-head agent-name">Agent {{$index+1}}</span>
                                                                        <div class="ship-buttons">
                                                                            <div class="agent-edit-button" data-agent-id="{{$agents['id']}}" data-scope='appointed'>
                                                                                <i class="fa fa-edit" aria-hidden="true" title="edit"></i>
                                                                            </div>
                                                                            <div class="agent-close-button" data-agent-id="{{$agents['id']}}" data-scope="appointed">
                                                                                <i class="fa fa-times" aria-hidden="true" title="delete"></i>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <?php
                                                                    $image_path = '';
                                                                    if(isset($agents['logo'])){
                                                                        $image_path = env('COMPANY_TEAM_AGENT_PIC_PATH')."".$agents['id']."/".$agents['logo'];
                                                                    }
                                                                ?>
                                                                <div class="col-sm-3">
                                                                    <div class="description">
                                                                        <span class="content">
                                                                            @if(!empty($image_path))
                                                                                <img class="agent-preview" src="/{{ $image_path }}">
                                                                            @else
                                                                                <img class="agent-preview" src="/images/user_default_image.png">
                                                                            @endif
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-3">
                                                                    <div class="description">
                                                                        <span class="content-head">Company Name</span>
                                                                        <span class="content">{{ !empty($agents['to_company']) ? $agents['to_company'] : '-'}}</span>
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-3">
                                                                    <div class="description">
                                                                        <span class="content-head">Location</span>
                                                                        <span class="content">{{ !empty($agents['location']) ? $agents['location'] : '-'}}</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                        </div>                                            
                                                        @endforeach
                                                    @else
                                                        <div class="row">
                                                            <div class='col-xs-12 text-center'>
                                                                <div class='discription'>
                                                                    <span class='content-head'>No Data Found</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endif
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <div class="owner-to-section display-section">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="scope-title">
                                                We are owners to
                                                <span class="pull-right">
                                                    <label class="radio-inline">
                                                        <input type="radio" name="owners_to" class="scope-radio" data-section="vessel" value="1" {{$owner_ship_yes}}>Yes</input>
                                                    </label>
                                                    <label class="radio-inline">
                                                        <input type="radio" name="owners_to" class="scope-radio" data-section="vessel" {{$owner_ship_no}} value="0">No</input>
                                                    <label>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                
                                    <div class="vessel-details vessel-section {{$owner_section}} m-t-25">
                                        <form id="owner-vessel" method="post" action="{{route('site.company.store.ship.details')}}">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="scope-title">
                                                        Enter the vessel details
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row m-t-15">
                                                <input type="hidden" name="scope_type" value="owner">
                                                <input type="hidden" class="existing_vessel_id" name="existing_vessel_id" value="">

                                                <div class="col-sm-3">
                                                    <label class="input-label" for="ship_name">Ship Name</label>
                                                    <div class="form-group">
                                                        <input type="text" name="ship_name" class="form-control search-select ship_name" value="{{ isset($value['ship_name']) ? $value['ship_name'] : ''}}" placeholder="Type your ship name">
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <label class="input-label">Type Of Ship<span class="symbol required"></span></label>
                                                    <div class="form-group">
                                                        <select name="ship_type" class="form-control search-select ship_type">
                                                            <option value="">Select Ship Type</option>
                                                            @foreach( \CommonHelper::ship_type() as $c_index => $ship_type)
                                                                <option value="{{ $c_index }}" {{ isset($value['ship_type']) ? $value['ship_type'] == $c_index ? 'selected' : '' : ''}}> {{ $ship_type }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <label class="input-label" for="no-of-ships">Flag Of Ship<span class="symbol required"></span></label>
                                                    <div class="form-group">
                                                        <select name="ship_flag" class="form-control search-select ship_flag">
                                                                <option value="">Select Ship Flag</option>
                                                                @foreach( \CommonHelper::countries() as $c_index => $ship_flag)
                                                                    <option value="{{ $c_index }}" {{ isset($value['ship_flag']) ? $value['ship_flag'] == $c_index ? 'selected' : '' : ''}}> {{ $ship_flag }}</option>
                                                                @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label class="input-label" for="no-of-ships">Engine Type<span class="symbol required"></span></label>
                                                        <select name="engine_type" class="form-control search-select engine_type">
                                                            <option value="">Select Engine Type</option>
                                                            @foreach( \CommonHelper::engine_type() as $c_index => $engine_type)
                                                                <option value="{{ $c_index }}" {{ isset($value['engine_type']) ? $value['engine_type'] == $c_index ? 'selected' : '' : ''}}> {{ $engine_type }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label class="input-label" for="grt">GRT</label>
                                                        <input type="text" class="form-control grt" name="grt" value="{{ isset($value['grt']) ? $value['grt'] : ''}}" placeholder="Type your GRT">
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label class="input-label" for="no-of-ships">BHP</label>
                                                        <input type="text" class="form-control bhp" name="bhp" value="{{ isset($value['bhp']) ? $value['bhp'] : ''}}" placeholder="Type your BHP">
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label class="input-label" for="p-i-cover">P & I cover</label>
                                                        <div>
                                                            <label class="radio-inline">
                                                                <input type="radio" block-index='0' class="p-i-cover" name="p-i-cover" value="1"> Yes
                                                            </label>
                                                            <label class="radio-inline">
                                                                <input type="radio" block-index='0' class="p-i-cover" name="p-i-cover" value="0" checked="checked"> No
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3 m-t-25">
                                                    <button type="button" class="btn add-more-btn ladda-button save-vessel-details" data-form="owner-vessel" data-style="zoom-in">Save</button>
                                                </div>
                                            </div>

                                            <div class="row m-t-15">
                                                <div class="col-sm-12">
                                                    <div class="description">
                                                        Vessel Details
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="owner-vessel">
                                                @if(isset($data[0]['owner_ship']) && !empty($data[0]['owner_ship']))    
                                                    @foreach($data[0]['owner_ship'] as $index => $ship)
                                                    <div class="vessel-section vessel_section_{{$ship['id']}}">
                                                        <div class="row vessel-row">
                                                            <div class="col-sm-12 vessel-title">
                                                                <div class="description">
                                                                    <span class="content-head vessel-name">Vessel {{$index+1}}</span>
                                                                    <div class="ship-buttons">
                                                                        <div class="vessel-edit-button" data-vessel-id="{{$ship['id']}}" data-scope='owner'>
                                                                            <i class="fa fa-edit" aria-hidden="true" title="edit"></i>
                                                                        </div>
                                                                        <div class="vessel-close-button" data-vessel-id="{{$ship['id']}}" data-scope='owner'>
                                                                            <i class="fa fa-times" aria-hidden="true" title="delete"></i>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-6 col-sm-2">
                                                                <div class="description">
                                                                    <span class="content-head">Vessel Name</span>
                                                                    <span class="content">{{ !empty($ship['ship_name']) ? $ship['ship_name'] : '-' }}</span>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-6 col-sm-2">
                                                                <div class="description">
                                                                    <span class="content-head">Type</span>
                                                                    <span class="content">
                                                                        @foreach( \CommonHelper::ship_type() as $c_index => $ship_type)
                                                                            {{ isset($ship['ship_type']) ? $ship['ship_type'] == $c_index ? $ship_type : '' : ''}}
                                                                        @endforeach
                                                                    </span>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-6 col-sm-2">
                                                                <div class="description">
                                                                    <span class="content-head">Flag</span>
                                                                    <span class="content">
                                                                        @if(isset($ship['ship_flag']) && !empty($ship['ship_flag']))
                                                                            @foreach( \CommonHelper::countries() as $c_index => $ship_flag)
                                                                                {{ isset($ship['ship_flag']) ? $ship['ship_flag'] == $c_index ? $ship_flag : '' : ''}}
                                                                            @endforeach
                                                                        @else
                                                                            -
                                                                        @endif
                                                                    </span>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-6 col-sm-1">
                                                                <div class="description">
                                                                    <span class="content-head">GRT</span>
                                                                    <span class="content">{{ !empty($ship['grt']) ? $ship['grt'] : '-' }}</span>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-6 col-sm-1">
                                                                <div class="description">
                                                                    <span class="content-head">BHP</span>
                                                                    <span class="content">{{ !empty($ship['bhp']) ? $ship['bhp'] : '-' }}</span>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-6 col-sm-2">
                                                                <div class="description">
                                                                    <span class="content-head">Engine</span>
                                                                    <span class="content">
                                                                        @if(isset($ship['engine_type']) && !empty($ship['engine_type']))
                                                                            @foreach( \CommonHelper::engine_type() as $c_index => $engine_type)
                                                                                {{ isset($ship['engine_type']) ? $ship['engine_type'] == $c_index ? $engine_type : '' : ''}}
                                                                            @endforeach
                                                                        @else
                                                                            -
                                                                        @endif
                                                                    </span>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <div class="description">
                                                                    <span class="content-head">P & I</span>
                                                                    <span class="content">
                                                                        {{ isset($ship['p_i_cover']) ? $ship['p_i_cover'] == '1' ? 'Yes' : 'No' : ''}}
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                    </div>
                                                    @endforeach
                                                @else
                                                    <div class="row">
                                                        <div class='col-xs-12 text-center'>
                                                            <div class='discription'>
                                                                <span class='content-head'>No Data Found</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endif
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php
                            $associate_as_yes = '';
                            $associate_as_no = '';
                            $associate_as_section = 'hide';
                            if(isset($data[0]['company_registration_detail']['associate_agent']) && !empty($data[0]['company_registration_detail']['associate_agent'])){
                                $associate_as_yes = 'checked';
                                $associate_as_section = '';
                            }else{
                                $associate_as_no = 'checked';
                            }
                        ?>
                        <div id="managers" class="tab-pane fade">
                            <div class="manager-section">
                                <div class="appointed-section display-section">
                                    <div class="vessel-details">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="scope-title">
                                                    We are crew managers to
                                                    <span class="pull-right">
                                                        <label class="radio-inline">
                                                            <input type="radio" name="crew" class="scope-radio" data-section="crew" value="1" {{$associate_as_yes}}>Yes</input>
                                                        </label>
                                                        <label class="radio-inline">
                                                            <input type="radio" name="crew" class="scope-radio" data-section="crew" {{$associate_as_no}} value="0">No</input>
                                                        <label>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="m-t-15 crew-section {{$associate_as_section}}">
                                            <form id="associate_agent" method="post" action="{{route('site.company.store.agent')}}">
                                                <input class="hidden" name="agent_type" value="associate"></input>
                                                <input type="hidden" class="existing_agent_id" name="existing_agent_id" value="">
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-3">
                                                        <div class="agent" id="agent">
                                                            <label class="input-label" for="pic">Profile Pic</label>
                                                            <div class="upload-photo-container">
                                                                {{ csrf_field() }}
                                                                <input type="hidden" name="image-x" required>
                                                                <input type="hidden" name="image-y" required>
                                                                <input type="hidden" name="image-x2" required>
                                                                <input type="hidden" name="image-y2" required>
                                                                <input type="hidden" name="crop-w" required>
                                                                <input type="hidden" name="crop-h" required>
                                                                <input type="hidden" name="image-w" required>
                                                                <input type="hidden" name="image-h" required>
                                                                <input type="hidden" name="uploaded-file-name" required>
                                                                <input type="hidden" name="uploaded-file-path" required>
                                                                <input type="hidden" class="path" value="{{env('COMPANY_TEAM_AGENT_PIC_PATH')}}"></input>

                                                                <div class="image-content">
                                                                    <div class="profile-image registration-profile-image">
                                                                        <?php
                                                                            $image_path = '';
                                                                            if(isset($team['profile_pic'])){
                                                                                $image_path = env('COMPANY_TEAM_AGENT_PIC_PATH')."".$team['id']."/".$team['profile_pic'];
                                                                            }
                                                                        ?>
                                                                        @if(empty($image_path))
                                                                            <div class="icon profile_pic_text"><i class="fa fa-camera" aria-hidden="true"></i></div>
                                                                            <div class="image-text profile_pic_text">Upload Profile <br> Picture</div>
                                                                        @endif

                                                                        <input type="file" name="profile_pic" id="profile-pic" class="cursor-pointer" onChange="profilePicSelectHandler('associate_agent', 'profile_pic', '' , 'agent_team')">

                                                                        @if(!empty($image_path))
                                                                            <img id="preview" class="preview" src="/{{ $image_path }}">
                                                                        @else
                                                                            <img id="preview">
                                                                        @endif
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <label class="input-label" for="ship_name">Company Name<span class="symbol required"></span></label>
                                                        <input type="text" name="company_name" class="form-control search-select company_name" value="{{ isset($value['company_name']) ? $value['company_name'] : ''}}" placeholder="Type your company name">
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <label class="input-label" for="location">Location<span class="symbol required"></span></label>
                                                        <input type="text" name="location" class="form-control search-select location" value="{{ isset($value['location']) ? $value['location'] : ''}}" placeholder="Type your location">
                                                    </div>
                                                    <div class="col-sm-3 m-t-25">
                                                        <button type="button" class="btn add-more-btn ladda-button save-agent-details" data-style="zoom-in" data-form="associate_agent">Save</button>
                                                    </div>
                                                </div>
                                                <div class="row m-t-15">
                                                    <div class="col-sm-12">
                                                        <div class="description">
                                                            Agent Details
                                                        </div>
                                                    </div>
                                                </div> 
                                                <div class="associate_agent">
                                                    @if(isset($data[0]['company_registration_detail']['associate_agent']) && !empty($data[0]['company_registration_detail']['associate_agent']))
                                                        @foreach($data[0]['company_registration_detail']['associate_agent'] as $index => $agents)
                                                        <div class="vessel-section agent_section_{{$agents['id']}}">
                                                            <div class="row vessel-row">
                                                                <div class="col-sm-12 vessel-title">
                                                                    <div class="description">
                                                                        <span class="content-head agent-name">Agent {{$index+1}}</span>
                                                                        <div class="ship-buttons">
                                                                            <div class="agent-edit-button" data-agent-id="{{$agents['id']}}" data-scope='associate'>
                                                                                <i class="fa fa-edit" aria-hidden="true" title="edit"></i>
                                                                            </div>
                                                                            <div class="agent-close-button" data-agent-id="{{$agents['id']}}" data-scope="associate">
                                                                                <i class="fa fa-times" aria-hidden="true" title="delete"></i>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <?php
                                                                    $image_path = '';
                                                                    if(isset($agents['logo'])){
                                                                        $image_path = env('COMPANY_TEAM_AGENT_PIC_PATH')."".$agents['id']."/".$agents['logo'];
                                                                    }
                                                                ?>
                                                                <div class="col-sm-3">
                                                                    <div class="description">
                                                                        <span class="content">
                                                                            @if(!empty($image_path))
                                                                                <img class="agent-preview" src="/{{ $image_path }}">
                                                                            @else
                                                                                <img class="agent-preview" src="/images/user_default_image.png">
                                                                            @endif
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-3">
                                                                    <div class="description">
                                                                        <span class="content-head">Company Name</span>
                                                                        <span class="content">{{ !empty($agents['from_company']) ? $agents['from_company'] : '-'}}</span>
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-3">
                                                                    <div class="description">
                                                                        <span class="content-head">Location</span>
                                                                        <span class="content">{{ !empty($agents['location']) ? $agents['location'] : '-'}}</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                        </div>
                                                        @endforeach
                                                    @else
                                                        <div class="row">
                                                            <div class='col-xs-12 text-center'>
                                                                <div class='discription'>
                                                                    <span class='content-head'>No Data Found</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endif
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <?php
                                    $manager_vessel_yes = '';
                                    $manager_vessel_no = '';
                                    $manager_vessel_section = 'hide';
                                    if(isset($data[0]['manager_ship']) && !empty($data[0]['manager_ship'])){
                                        $manager_vessel_yes = 'checked';
                                        $manager_vessel_section = '';
                                    }else{
                                        $manager_vessel_no = 'checked';
                                    }
                                ?>
                                <div class="manager-vessels-section display-section">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="scope-title">
                                                The vessels we manage

                                                <span class="pull-right">
                                                    <label class="radio-inline">
                                                        <input type="radio" name="manager-vessel" class="scope-radio" data-section="manager-vessel" value="1" {{$manager_vessel_yes}}>Yes</input>
                                                    </label>
                                                    <label class="radio-inline">
                                                        <input type="radio" name="manager-vessel" class="scope-radio" data-section="manager-vessel" {{$manager_vessel_no}} value="0">No</input>
                                                    <label>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                
                                    <div class="vessel-details manager-vessel-section m-t-25 {{$manager_vessel_section}}">
                                        <form id="manager-vessel" method="post" action="{{route('site.company.store.ship.details')}}">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="scope-title">
                                                        Enter the vessel details
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row m-t-15">
                                                <input type="hidden" name="scope_type" value="manager">
                                                <input type="hidden" class="existing_vessel_id" name="existing_vessel_id" value="">

                                                <div class="col-sm-3">
                                                    <label class="input-label" for="ship_name">Ship Name</label>
                                                    <div class="form-group">
                                                        <input type="text" name="ship_name" class="form-control search-select ship_name" value="{{ isset($value['ship_name']) ? $value['ship_name'] : ''}}" placeholder="Type your ship name">
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <label class="input-label">Type Of Ship<span class="symbol required"></span></label>
                                                    <div class="form-group">
                                                        <select name="ship_type" class="form-control search-select ship_type">
                                                            <option value="">Select Ship Type</option>
                                                            @foreach( \CommonHelper::ship_type() as $c_index => $ship_type)
                                                                <option value="{{ $c_index }}" {{ isset($value['ship_type']) ? $value['ship_type'] == $c_index ? 'selected' : '' : ''}}> {{ $ship_type }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <label class="input-label" for="no-of-ships">Flag Of Ship<span class="symbol required"></span></label>
                                                    <div class="form-group">
                                                        <select name="ship_flag" class="form-control search-select ship_flag">
                                                                <option value="">Select Ship Flag</option>
                                                                @foreach( \CommonHelper::countries() as $c_index => $ship_flag)
                                                                    <option value="{{ $c_index }}" {{ isset($value['ship_flag']) ? $value['ship_flag'] == $c_index ? 'selected' : '' : ''}}> {{ $ship_flag }}</option>
                                                                @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label class="input-label" for="no-of-ships">Engine Type<span class="symbol required"></span></label>
                                                        <select name="engine_type" class="form-control search-select engine_type">
                                                            <option value="">Select Engine Type</option>
                                                            @foreach( \CommonHelper::engine_type() as $c_index => $engine_type)
                                                                <option value="{{ $c_index }}" {{ isset($value['engine_type']) ? $value['engine_type'] == $c_index ? 'selected' : '' : ''}}> {{ $engine_type }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label class="input-label" for="grt">GRT</label>
                                                        <input type="text" class="form-control grt" name="grt" value="{{ isset($value['grt']) ? $value['grt'] : ''}}" placeholder="Type your GRT">
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label class="input-label" for="no-of-ships">BHP</label>
                                                        <input type="text" class="form-control bhp" name="bhp" value="{{ isset($value['bhp']) ? $value['bhp'] : ''}}" placeholder="Type your BHP">
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label class="input-label" for="p-i-cover">P & I cover</label>
                                                        <div>
                                                            <label class="radio-inline">
                                                                <input type="radio" block-index='0' class="p-i-cover" name="p-i-cover" value="1"> Yes
                                                            </label>
                                                            <label class="radio-inline">
                                                                <input type="radio" block-index='0' class="p-i-cover" name="p-i-cover" value="0" checked="checked"> No
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3 m-t-25">
                                                    <button type="button" class="btn add-more-btn ladda-button save-vessel-details" data-form="manager-vessel" data-style="zoom-in">Save</button>
                                                </div>
                                            </div>

                                            <div class="row m-t-15">
                                                <div class="col-sm-12">
                                                    <div class="description">
                                                        Vessel Details
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="manager-vessel">
                                                @if(isset($data[0]['manager_ship']) && !empty($data[0]['manager_ship'])) 
                                                    @foreach($data[0]['manager_ship']  as $index => $ship)
                                                    <div class="vessel-section vessel_section_{{$ship['id']}}">
                                                        <div class="row vessel-row">
                                                            <div class="col-sm-12 vessel-title">
                                                                <div class="description">
                                                                    <span class="content-head vessel-name">Vessel {{$index+1}}</span>
                                                                    <div class="ship-buttons">
                                                                        <div class="vessel-edit-button" data-vessel-id="{{$ship['id']}}" data-scope="manager">
                                                                            <i class="fa fa-edit" aria-hidden="true" title="edit"></i>
                                                                        </div>
                                                                        <div class="vessel-close-button" data-vessel-id="{{$ship['id']}}" data-scope="manager">
                                                                            <i class="fa fa-times" aria-hidden="true" title="delete"></i>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <div class="description">
                                                                    <span class="content-head">Vessel Name</span>
                                                                    <span class="content">{{ !empty($ship['ship_name']) ? $ship['ship_name'] : '-' }}</span>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <div class="description">
                                                                    <span class="content-head">Type</span>
                                                                    <span class="content">
                                                                        @foreach( \CommonHelper::ship_type() as $c_index => $ship_type)
                                                                            {{ isset($ship['ship_type']) ? $ship['ship_type'] == $c_index ? $ship_type : '' : ''}}
                                                                        @endforeach
                                                                    </span>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <div class="description">
                                                                    <span class="content-head">Flag</span>
                                                                    <span class="content">
                                                                        @if(isset($ship['ship_flag']) && !empty($ship['ship_flag']))
                                                                            @foreach( \CommonHelper::countries() as $c_index => $ship_flag)
                                                                                {{ isset($ship['ship_flag']) ? $ship['ship_flag'] == $c_index ? $ship_flag : '' : ''}}
                                                                            @endforeach
                                                                        @else
                                                                            -
                                                                        @endif
                                                                    </span>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-1">
                                                                <div class="description">
                                                                    <span class="content-head">GRT</span>
                                                                    <span class="content">{{ !empty($ship['grt']) ? $ship['grt'] : '-' }}</span>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-1">
                                                                <div class="description">
                                                                    <span class="content-head">BHP</span>
                                                                    <span class="content">{{ !empty($ship['bhp']) ? $ship['bhp'] : '-' }}</span>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <div class="description">
                                                                    <span class="content-head">Engine</span>
                                                                    <span class="content">
                                                                        @if(isset($ship['engine_type']) && !empty($ship['engine_type']))
                                                                            @foreach( \CommonHelper::engine_type() as $c_index => $engine_type)
                                                                                {{ isset($ship['engine_type']) ? $ship['engine_type'] == $c_index ? $engine_type : '' : ''}}
                                                                            @endforeach
                                                                        @else
                                                                            -
                                                                        @endif
                                                                    </span>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <div class="description">
                                                                    <span class="content-head">P & I</span>
                                                                    <span class="content">
                                                                        {{ isset($ship['p_i_cover']) ? $ship['p_i_cover'] == '1' ? 'Yes' : 'No' : ''}}
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                    </div>
                                                    @endforeach
                                                @else
                                                    <div class="row">
                                                        <div class='col-xs-12 text-center'>
                                                            <div class='discription'>
                                                                <span class='content-head'>No Data Found</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endif
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>
</div>
@stop
@section('js_script')
    <script type="text/javascript" src="/js/site/company-registration.js"></script>
@stop
