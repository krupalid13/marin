@extends('site.index')
@section('content')

	<div class="candidate-search section job-application-listing content-section-wrapper sm-filter-space">
		<div class="row no-margin">
			<div class="col-md-2">
				@include('site.partials.side_navbar')
			</div>

			<div class="col-md-10" id="main-data">

				<div class="row">
	                <div class="col-xs-12">
	                    <div class="company-page-profile-heading">
	                        Document Search
	                    </div>
	                </div>
	            </div>

	            <div class="candidate-search-container">
                    <div class="section-1">
                        <form id="candidate-filter-form" action={{route('site.company.list.permission.requests')}}>
                            <div class="row">
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label>Name </label>
                                        <input type="text" name="name" class="form-control" value="{{isset($filter['name']) ? $filter['name'] : ''}}"> 
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label>Email</label>
                                        <input type="text" name="email" class="form-control" value="{{isset($filter['email']) ? $filter['email'] : ''}}"> 
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label>Mobile</label>
                                        <input type="text" name="mob" class="form-control" value="{{isset($filter['mob']) ? $filter['mob'] : ''}}">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-12">
                                	<div class="section_cta text-right m-t-15 m-b-15 job-btn-container">
                                		<button type="button" class="btn coss-inverse-btn search-reset-btn" id="jobResetButton">Reset</button>
                                    	<button type="submit" class="btn coss-primary-btn search-post-btn ladda-button"><i class="fa fa-search search-icon" aria-hidden="true"></i>Search</button>
                                    </div>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>

                @if($errors->any())
					<div class="row">
		                <div class="col-sm-12 col-md-12">
		                    <div class="alert alert-block alert-danger fade in">
		                		{{$errors->first()}}
		                    </div>
	                	</div>
	                </div>
				@endif

				@if(isset($pagination_data['data']) AND !empty($pagination_data['data']))

	                <div class="section-2" id="filter-results">
						<div class="section listing-container">
							{{csrf_field()}}
							<div class="row">
				                <div class="col-xs-12 col-sm-6">
				                    <div class="company-page-profile-heading">
				                        Document Permission Listing
				                    </div>
				                </div>
				                <div class="col-xm-12 col-sm-6">
				                	<span class="pagi pagi-up">
				                        <span class="search-count m-t-10">
				                            Showing {{$pagination_data['from']}} - {{$pagination_data['to']}} of {{$pagination_data['total']}} Requests
				                        </span>
				                        <nav> 
					                        <ul class="pagination pagination-sm">
					                            {!! $pagination->render() !!}
					                        </ul> 
				                        </nav>
				                    </span>
				                </div>	
				            </div>

							@foreach($pagination_data['data'] as $details)
								<div class="job-card">
									<div class="row m-0 flex-height">
										<div class="col-xs-12 col-sm-3 text-center p-10">
											<img src="{{!empty($details['owner']['profile_pic']) ? "/".env('SEAFARER_PROFILE_PATH').$details['owner']['id']."/".$details['owner']['profile_pic'] : asset('images/user_default_image.png') }}" class="job-applicant-user-profile-pic">
										</div>
										<div class="col-xs-12 col-sm-9 p-0">
											<div class="company-discription p-25-20">
												<div class="row">
													<div class="col-sm-6">
														<div class="other-discription">
															Name:
															{{ isset($details['owner']['first_name']) ? ucwords($details['owner']['first_name']) : ''}}
														</div>
													</div>
													<div class="col-sm-6">
														<div class="other-discription">
															Requests On:
															<span class="txt-green">
																{{ isset($details['owner']['updated_on']) ? date('h:i a , d-m-Y', strtotime($details['owner']['updated_on'])) : '-'}}
															</span>
														</div>
													</div>
													<div class="col-sm-6">
														<div class="other-discription">
															Email: {{ isset($details['owner']['email']) ? $details['owner']['email'] : ''}}
														</div>
													</div>
													<div class="col-sm-6">
														<div class="other-discription">
															Requested Documents:
															<span>
																@if(isset($details['document_type']) && !empty($details['document_type']))
																	@foreach($details['document_type'] as $k => $documents)
																		{{strToUpper($documents['type']['type'])}}
																		@if($documents['type']['type_id'] != '0')
																			{{$documents['type']['type_id']}}
																		@endif

																		@if(count($details['document_type']) > $k+1)
																			,
																		@endif
																	@endforeach
																@endif
															</span>
														</div>
													</div>
													<div class="col-sm-6">
														<div class="other-discription">
															Mobile: {{ isset($details['owner']['mobile']) ? $details['owner']['mobile'] : '-'}}
														</div>
													</div>
													<div class="col-sm-6">
														<div class="other-discription">
															Status: 
															@if($details['status'] == '0')
																Pending

															@elseif($details['status'] == '1')
																<?php
																	$download_path = route('site.company.download.document.by.permission.id',['permission_id' => $details['id']]);
																?>
																<span class="txt-green">Accepted</span>

																<span class="m-l-15">
																	<a class="btn coss-inverse-btn cv-btn d-btn" href="{{$download_path}}">Download Document</a>
																</span>
															@else
																<span class="txt-red">Rejected</span>
															@endif
						                                </div>
						                            </div>
												</div>
											</div>
										</div>
									</div>
								</div>
							@endforeach
							<div class="row">
								<div class="col-xm-12 col-sm-12">
				                	<span class="pagi pagi-up">
				                        <span class="search-count m-t-10">
				                            Showing {{$pagination_data['from']}} - {{$pagination_data['to']}} of {{$pagination_data['total']}} Requests
				                        </span>
				                        <nav> 
					                        <ul class="pagination pagination-sm">
					                            {!! $pagination->render() !!}
					                        </ul> 
				                        </nav>
				                    </span>
				                </div>
				            </div>
						</div>
					</div>

				@else
					<div class="company-page-profile-heading">
	                    Document Permission Listing
	                </div>
					<div class="no-results-found">No Results Found.</div>
				@endif
			</div>
		</div>
	</div>

@stop

@section('js_script')
    <script type="text/javascript" src="/js/site/dashboard.js"></script>
@stop