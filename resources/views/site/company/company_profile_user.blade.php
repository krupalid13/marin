@extends('site.index')

@section('content')
    <div class="user-profile content-section-wrapper sm-filter-space company_profile_view">
        <div class="container">
            <div class="row">
                <div class="col-md-12" id="main-data">

                    <div class="row profile_edit">
                        <div class="col-xs-6">
                            <div class="company-page-profile-heading">
                                My profile
                            </div>
                        </div>
                        @if(!isset($user) && empty($user))
                            <div class="col-xs-6">
                                <div class="edit-btn-container pull-right visible-xs">
                                    <a href="{{ route('site.edit.company.details') }}" class="profile-edit-bt">
                                        <i class="fa fa-pencil-square-o" aria-hidden="true" title="Edit profile"></i>
                                    </a>
                                </div>
                            </div>
                        @endif
                    </div>

                    @include('site.partials.company_profile')
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div id="advModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Advertisement</h4>
                </div>
                <div class="modal-body">
                    <img src="{{isset($img_path) ? $img_path : ''}}" class="img-responsive w-100">
                </div>
            </div>

        </div>
    </div>
@stop
@section('js_script')
    <script type="text/javascript" src="/js/site/company-registration.js"></script>
    <script type="text/javascript" src="/js/site/job_search.js"></script>
@stop