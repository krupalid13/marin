@extends('site.index')

@section('content')

    <?php
        $india_value = array_search('India',\CommonHelper::countries());
    ?>

    <div class="user-profile content-section-wrapper sm-filter-space company_team_details">
        <div class="no-margin row">
            <div class="col-md-2">
                @include('site.partials.side_navbar')
            </div>

            <div class="col-md-10" id="main-data">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="company-page-profile-heading">
                            Add Team Details
                        </div>
                    </div>
                </div>

                <form class="team-section-form" id="team-section-form" method="post" action="{{ route('site.company.team.details') }}">
                    <div class="main-form-details sub-details-container">
                        <div class="owner-section">
                            <div class="team-details">
                                <div class="row m-t-15 team-details-row">
                                    <div class="col-xs-12 col-sm-3">
                                        <div class="company_team_profile_pic" id="company_team_profile_pic">
                                            <label class="input-label" for="pic">Profile Pic</label>
                                            <div class="upload-photo-container">
                                                {{ csrf_field() }}
                                                <input type="hidden" name="image-x" required>
                                                <input type="hidden" name="image-y" required>
                                                <input type="hidden" name="image-x2" required>
                                                <input type="hidden" name="image-y2" required>
                                                <input type="hidden" name="crop-w" required>
                                                <input type="hidden" name="crop-h" required>
                                                <input type="hidden" name="image-w" required>
                                                <input type="hidden" name="image-h" required>
                                                <input type="hidden" name="uploaded-file-name" required>
                                                <input type="hidden" name="uploaded-file-path" required>
                                                <input type="hidden" class="path" value="{{env('COMPANY_TEAM_PROFILE_PIC_PATH')}}"></input>

                                                <div class="image-content">
                                                    <div class="profile-image registration-profile-image">
                                                        <?php
                                                            $image_path = '';
                                                            if(isset($team['profile_pic'])){
                                                                $image_path = env('COMPANY_TEAM_PROFILE_PIC_PATH')."".$team['id']."/".$team['profile_pic'];
                                                            }
                                                        ?>
                                                        @if(empty($image_path))
                                                            <div class="icon profile_pic_text"><i class="fa fa-camera" aria-hidden="true"></i></div>
                                                            <div class="image-text profile_pic_text">Upload Profile <br> Picture</div>
                                                        @endif

                                                        <input type="file" name="profile_pic" id="profile-pic" class="cursor-pointer" onChange="profilePicSelectHandler('company_team_profile_pic', 'profile_pic', '' , 'company_team')">

                                                        @if(!empty($image_path))
                                                            <img id="preview" class="preview" src="/{{ $image_path }}">
                                                        @else
                                                            <img id="preview">
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-3">
                                        <div class="form-group">
                                            <label class="input-label" for="team_agent_name">Member Name<span class="symbol required"></span></label>
                                            <input type="text" name="agent_name" class="form-control search-select agent_name" value="" placeholder="Type your name">
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-3">
                                        <div class="form-group">
                                            <label class="input-label" for="team_agent_designation">Designation<span class="symbol required"></span></label>
                                            <input type="text" name="agent_designation" class="form-control search-select agent_designation" value="" placeholder="Type your designation">
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-3">
                                        <div class="form-group">
                                            <label class="input-label">Location<span class="symbol required"></span></label>
                                            <select name="location_id" class="form-control search-select location_id">
                                                <option value="">Select Location</option>
                                                @if(isset($location_data[0]['company_locations']) && !empty($location_data[0]['company_locations']))
                                                    @foreach($location_data[0]['company_locations'] as $company_location_details)
                                                        @if($company_location_details['country'] == $india_value)
                                                            <option value="{{ $company_location_details['id'] }}" {{ isset($company_location_details['country']) ? $company_location_details['country'] : ''}}> {{ ucfirst(\CommonHelper::countries()[$company_location_details['country']]) }} {{ '/' }} {{ ucfirst($company_location_details['state']['name']) }} {{ '/' }} {{ ucfirst($company_location_details['city']['name']) }}</option>
                                                        @else
                                                            <option value="{{ $company_location_details['id'] }}" {{ isset($company_location_details['country']) ? $company_location_details['country'] : ''}}> {{ ucfirst(\CommonHelper::countries()[$company_location_details['country']]) }} {{ '/' }} {{ ucfirst($company_location_details['state_text']) }} {{ '/' }} {{ ucfirst($company_location_details['city_text']) }}</option>
                                                        @endif
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </div>

                                    <input type="hidden" name="company_id" value="{{$location_data[0]['company_locations'][0]['company_id']}}">
                                    <input type="hidden" name="exiting_team_id" class="exiting_team_id">

                                </div>
                                <div class="row m-t-15">

                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 reg-form-btn-container text-right">
                                <button type="button" data-style="zoom-in" class="reg-form-btn blue ladda-button company-team-details-submit" id="company-team-details-submit" value="Save">Save</button>
                            </div>
                        </div>
                    </div>

                    <div class="company-team-details sub-details-container">
                        <div class="row ship_details_title">
                            <div class="col-xs-12">
                                <div class="title">Team Details</div>
                            </div>
                        </div>
                        <div class="company_team_data">
                            @if(isset($user_data) && !empty($user_data))
                                @foreach($user_data as $index => $team)
                                    <div class="company_team_show_data">
                                        <div class="company_team_show_data_{{ $team['id'] }}">
                                            <div class="row team-row">
                                                <div class="col-sm-6 team-title">
                                                    <div class="description">
                                                        <span class="content-head team-title team-name">Team Agent {{$index+1}}</span>
                                                    </div>
                                                </div>
                                                <div class="col-xs-6">
                                                    <div class="company-team-buttons">
                                                        <div class="company-team-edit-button" data-index-id="{{$index}}" data-team-id="{{$team['id'] }}" data-company-id="{{$team['location']['company_id'] }}">
                                                            <i class="fa fa-edit" aria-hidden="true" title="edit"></i>
                                                        </div>
                                                        <div class="company-team-close-button" data-id="{{ $team['id'] }}" data-team-id="{{ $team['id']}}" data-company-id="{{$team['location']['company_id']}}">
                                                            <i class="fa fa-times" aria-hidden="true" title="delete"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php
                                                $image_path = '';
                                                if(isset($team['profile_pic'])){
                                                    $image_path = env('COMPANY_TEAM_PROFILE_PIC_PATH')."".$team['id']."/".$team['profile_pic'];
                                                }
                                            ?>
                                            <div class="row">
                                                <div class="col-sm-3">
                                                    <div class="description">
                                                        <span class="content">
                                                            @if(!empty($image_path))
                                                                <img id="preview" class="preview" src="/{{ $image_path }}">
                                                            @else
                                                                <img id="preview" class="preview" src="/images/user_default_image.png">
                                                            @endif
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="description">
                                                        <span class="content-head">Member Name : </span>
                                                        <span class="content">{{ !empty($team['agent_name']) ? $team['agent_name'] : '-' }}</span>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="description">
                                                        <span class="content-head">Designation : </span>
                                                        <span class="content">{{ !empty($team['agent_designation']) ? $team['agent_designation'] : '-' }}</span>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="description">
                                                        <span class="content-head">Location : </span>
                                                        <span class="content">
                                                            @if(isset($team['location']['country']) && !empty($team['location']['country']))
                                                                @if($team['location']['country'] == $india_value)
                                                                    {{ ucfirst(\CommonHelper::countries()[$team['location']['country']]) }} {{ '/' }} {{ ucfirst($team['location']['state']['name']) }} {{ '/' }} {{ ucfirst($team['location']['city']['name']) }}
                                                                @else
                                                                    {{ ucfirst(\CommonHelper::countries()[$team['location']['country']]) }} {{ '/' }} {{ ucfirst($team['location']['state_text']) }} {{ '/' }} {{ ucfirst($team['location']['city_text']) }}
                                                                @endif
                                                            @else
                                                                -
                                                            @endif
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <hr>
                                        </div>
                                    </div>
                                @endforeach
                            @else
                                <div class="sub-details-container" style="box-shadow:none;margin:0px;">
                                    <div class="content-container">
                                        <div class="row">
                                            <div class="col-xs-12 text-center">
                                                <div class="discription">
                                                    <span class="content-head">No Data Found.</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>

                </form>

            </div>
        </div>
    </div>
@stop
@section('js_script')
    <script type="text/javascript" src="/js/site/company-registration.js"></script>
@stop