@extends('site.index')
@section('content')
    <div class="user-profile content-section-wrapper sm-filter-space">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-2" id="side-navbar">
                    @include('site.partials.side_navbar')
                </div>
                <div class="col-md-10" id="main-data">
                	<div class="row">
						<div class="col-xs-12 col-sm-8">
					        <div class="company-page-profile-heading">
					            Add Advertisement
					        </div>
					    </div>
					</div>
					
					@if(isset($data[0]['company_registration_detail']['advertisment_company_details'][0]['img_path']) && !empty($data[0]['company_registration_detail']['advertisment_company_details'][0]['img_path']))
                    <?php 
                    	$img_path = '';
                        $img_path = '/'.env('COMPANY_LOGO_PATH').$data[0]['id'].'/'.$data[0]['company_registration_detail']['advertisment_company_details'][0]['img_path'];
                    ?>
                    @endif

                	<div class="section advertise-container1">
                		<div class="panel panel-white m-b-0">
	                		<div class="panel-body" style="box-shadow: 0px 1px 2px 1px #d8d5d5;">
								<form role="form" class="form-horizontal" id='add-advertise-form' action="{{route('site.company.advertise.store')}}">
									<div class="form-group">
										<div class="col-sm-6">
											<label>
												Image Upload<span class="symbol required"></span>
											</label>
											<div class="fileupload fileupload-new" data-provides="fileupload">
												<div class="fileupload-new thumbnail" style="height: auto !important;">
													@if(isset($img_path) AND !empty($img_path))
														<img src="{{ asset($img_path) }}" alt=""/>
													@else
														<img src="{{ asset('images/no-image-advertisements.png') }}" alt=""/>
													@endif
												</div>
												<div class="fileupload-preview fileupload-exists thumbnail" id='advertise_img'></div>
												<span class="help-block" style="color: #737373;"><i class="fa fa-info-circle"></i> Note: Please upload .jpeg .png .gif file only.<br>(Latest image will be displayed.)</span>
												<div>
													<label for="" class="error"></label>
												</div>
												<div>
													<span class="btn btn-light-grey btn-file">
														<span class="fileupload-new">
															<i class="fa fa-picture-o"></i> Select image</span>
														<span class="fileupload-exists">
															<i class="fa fa-picture-o"></i> Change</span>
														<input type="file" class="advertise-file-uplaod" name="advertise_upload">
													</span>
													<a href="#" class="btn fileupload-exists btn-light-grey remove_advertise_btn" data-dismiss="fileupload">
														<i class="fa fa-times"></i> Remove
													</a>
												</div>
												<label for="advertise_upload" class="error" id="advertise_upload_error"></label>
											</div>
										</div>		
									</div>
									
									<div class="row" id="add-more-state-row">
			                        </div>
									<div class="reg-form-btn-container p-t-10 text-right">
										<button type="button" data-style="zoom-in" class="reg-form-btn blue ladda-button advertise-details-submit" id="submitAdvertiseDetailButton" value="Save" data-tab-index="1">Save</button>
									</div>
								</form>
							</div>
						</div>
                	</div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('js_script')
	<script type="text/javascript" src="/js/site/company-registration.js"></script>
@stop