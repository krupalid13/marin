
<!DOCTYPE html> 
<html lang="en">
    <head>
        <?php $site_version = rand(10,100); ?>
        <title>
        @if(isset($pageTitle) && !empty($pageTitle))
            {!! $pageTitle !!}
        @else
            {{env('APP_NAME')}}
        @endif
        </title>
        <meta name="description" content="{!! ($metaDescription) ?? '' !!}">
        <meta name="keywords" content="{!! ($metaKeywords) ?? '' !!}">
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
        <meta name="_token" content="{{ csrf_token() }}">
        <link rel="shortcut icon" type="image/x-icon" href="{{asset('public/images/favicon.ico')}}">
        <!-- <link href="/css/vendors/plugins/bootstrap/bootstrap.min.css" rel="stylesheet"> -->
        <link href="{{ URL:: asset('public/assets/css/vendors/plugins/bootstrap/bootstrap-theme.min.css')}}" rel="stylesheet">
        <link rel="stylesheet" href="{{ URL:: asset('public/assets/css/vendors/plugins/ladda-bootstrap/ladda.min.css')}}">
        <link rel="stylesheet" href="{{ URL:: asset('public/assets/css/vendors/plugins/ladda-bootstrap/ladda-themeless.min.css')}}">
        <link rel="stylesheet" href="{{ URL:: asset('public/assets/css/vendors/plugins/Jcrop/jquery.Jcrop.min.css')}}">
        <link rel="stylesheet" href="{{ URL:: asset('public/assets/css/vendors/plugins/toastr/toastr.min.css')}}">
        <link rel="stylesheet" href="{{ URL:: asset('public/assets/css/vendors/plugins/bootstrap-fileupload/bootstrap-fileupload.min.css')}}">
        <link rel="stylesheet" href="{{ URL:: asset('public/assets/css/vendors/plugins/select2/select2.css')}}">
        <link rel="stylesheet" href="{{ URL:: asset('public/assets/css/vendors/plugins/dropzone/dropzone.css')}}">
        <link rel="stylesheet" href="{{ URL:: asset('public/assets/css/vendors/plugins.css')}}">
        <link rel="stylesheet" href="{{ URL:: asset('public/assets/css/style.css')}}">

        <link rel="stylesheet" href="{{ URL:: asset('public/assets/css/vendors/plugins/bootstrap-datepicker/bootstrap-datepicker.css')}}">
        <link rel="stylesheet" href="{{ URL:: asset('public/assets/css/vendors/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.css')}}">
        <link href="{{ URL:: asset('public/assets/css/site_app.css')}}?v=<?php echo $site_version; ?>" rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/simple-line-icons/2.4.1/css/simple-line-icons.css">
        @yield('page-level-style')
        <script type="text/javascript" src="{{ URL:: asset('public/assets/js/site_jquery.js')}}?v=<?php echo $site_version; ?>"></script>
        <style>
            .d-none{
                display: none !important;
            }
            .indos-error{
                color: #e2412d;
            }
            .nav-link.dropdown-toggle{
                height: 100%;
            }
        </style>
        
        <link rel="shortcut icon" href="favicon.ico" />
        <script src="https://www.google.com/recaptcha/api.js?onload=myCallBack&render=explicit" async defer></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"></script>
        <script type="text/javascript">
            var siteBaseUrl = {!! json_encode(url('/')) !!};
        </script>
        <script>
          var recaptcha1;
          var recaptcha2;
          var key = $("#captcha_key").val();
          var myCallBack = function() {
            if($("#recaptcha1").length > 0){
                recaptcha1 = grecaptcha.render('recaptcha1', {
                  'sitekey' : $("#captcha_key").val(),
                });
            }
            
            if($("#recaptcha2").length > 0){
                recaptcha2 = grecaptcha.render('recaptcha2', {
                  'sitekey' : $("#captcha_key").val(), 
                });
            }
        };
        </script>
        @yield('addStyle')
    </head>
    <body>

    {{csrf_field()}}
        <input type="hidden" id="captcha_key" value="{{env('RECAPTCHA_SITE_KEY')}}">
        <input type="hidden" id="current-route-name" value="{{ Route::currentRouteName() }}">
        <input type="hidden" id="api-check-email" value="{{ route('site.check.email') }}">
        <input type="hidden" id="api-check-mobile" value="{{ route('site.check.mobile') }}">
        <input type="hidden" id="api-resend-otp-route" value="{{ route('site.user.mob.resendOtp') }}">
        <input type="hidden" id="api-resend-email-route" value="{{ route('site.user.email.resendEmail') }}">
        <input type="hidden" id="api-seafarer-profile-pic" value="{{ route('site.user.store.profilePic') }}">
        <input type="hidden" id="api-company-team-profile-pic" value="{{ route('site.company.store.profilePic') }}">
        <input type="hidden" id="api-add-subscription-route" value="{{ route('site.subscription.add.to.cart') }}">
        <input type="hidden" id="api-view-cart-route" value="{{ route('site.subscription.cart') }}">
        <input type="hidden" id="api-remove-subscription-route" value="{{ route('site.delete.from.cart','%test') }}">
        <input type="hidden" id="api-proceed-to-payment-route" value="{{ route('site.proceed.payment') }}">
        <input type="hidden" id="api-company-logo" value="{{ route('site.company.store.logo') }}">
        <input type="hidden" id="api-institute-logo" value="{{ route('site.institute.store.logo') }}">
        <input type="hidden" id="api-pincode-related-data" value="{{ route('site.pincode.get.states_cities') }}">
        <input type="hidden" id="api-auto-welcome-route" value="{{ route('home',['auto_action' => 'welcome']) }}">
        <input type="hidden" id="api-seafarer-profile-route" value="{{ route('user.profile') }}">
        <input type="hidden" id="api-seafarer-job-apply-route" value="{{ route('site.user.job.apply') }}">
        <input type="hidden" id="api-seafarer-course-apply-route" value="{{ route('site.user.course.apply') }}">
        <input type="hidden" id="api-activate-subscription-route" value="{{ route('site.activate.subscription') }}">
        <input type="hidden" id="api-store-advertiser-form-route" value="{{ route('site.advertise.store') }}">
        <input type="hidden" id="api-admin-state-list" value="{{ route('admin.state.list','%test') }}">
        <input type="hidden" id="api-site-advertise-list" value="{{ route('site.advertisements.list') }}">
        <input type="hidden" id="api-site-seafarer-registration" value="{{ route('site.seafarer.registration') }}">
        <input type="hidden" id="api-site-company-registration" value="{{ route('site.company.registration') }}">
        <input type="hidden" id="api-site-institute-registration" value="{{ route('site.institute.registration') }}">
        <input type="hidden" id="api-site-advertise-registration" value="{{ route('site.advertiser.registration') }}">
        <input type="hidden" id="api-site-add-job" value="{{ route('site.company.add.jobs') }}">
        <input type="hidden" id="api-site-subscription" value="{{ route('site.user.subscription') }}">

        <input type="hidden" id="api-site-company-data-ships" value="{{ route('site.company.details.related.ships','%test') }}">
        
        <input type="hidden" id="api-site-company-data-ships-by-ship-type" value="{{ route('site.company.details.related.ships.by.ship.type','%test') }}">

        <input type="hidden" id="\" value="{{ route('site.company.details.ship.name.by.ship.id','%test') }}">
        
        <input type="hidden" id="api-site-company-candidate-download-cv" value="{{ route('site.company.candidate.download-cv','%test') }}">

        <input type="hidden" id="api-institute-course-name-by-course-types" value="{{ route('site.institute.get.course.name',['%test','%test']) }}">

        <input type="hidden" id="api-seafarer-delete-service-by-service-id" value="{{ route('site.user.delete.service.details') }}">
        <input type="hidden" id="api-seafarer-get-service-by-service-id" value="{{ route('site.user.get.service.details') }}">

        <input type="hidden" id="api-seafarer-store-single-service-details" value="{{ route('site.user.store.single.service.details') }}">

        <input type="hidden" id="api-institute-delete-course" value="{{ route('site.institute.delete.course','%test') }}">

        <input type="hidden" id="api-site-seafarer-download-cv" value="{{ route('site.user.download-cv') }}">

        <input type="hidden" id="api-site-company-download-file" value="{{ route('site.user.download-file') }}">

        <input type="hidden" id="api-site-seafarer-store-course" value="{{ route('site.user.store.seafarer.course.details') }}">
        <input type="hidden" id="api-site-seafarer-delete-course" value="{{ route('site.user.seafarer.delete.details') }}">
        <input type="hidden" id="api-seafarer-get-course-by-course-id" value="{{ route('site.user.seafarer.get.course.details') }}">

        <input type="hidden" id="api-seafarer-get-course-by-course-type" value="{{ route('site.user.seafarer.get.course.by.course.type') }}">

        <input type="hidden" id="api-seafarer-get-document-list" value="{{ route('site.user.get.documents.list') }}">

        <input type="hidden" id="api-seafarer-change-document-status" value="{{ route('site.user.change.documents.status') }}">

        <input type="hidden" id="api-seafarer-store-requested-document-list" value="{{ route('site.user.store.requested.documents.list') }}">

        <input type="hidden" id="api-seafarer-requested-document-status" value="{{ route('site.user.requested.documents.status') }}">

        <input type="hidden" id="api-seafarer-delete-document" value="{{ route('site.user.upload.document.delete') }}">
        <input type="hidden" id="api-seafarer-store-permissions" value="{{ route('site.user.store.permissions') }}">

        <input type="hidden" id="api-site-user-get-documents-path" value="{{ route('site.user.get.documents.path',['%test','%test','%test']) }}">

        <input type="hidden" id="api-company-delete-vessel-by-vessel-id" value="{{ route('site.company.delete.vessel.details') }}">
        <input type="hidden" id="api-company-get-vessel-by-vessel-id" value="{{ route('site.company.get.vessel.details') }}">

        <input type="hidden" id="api-company-delete-agent-by-agent-id" value="{{ route('site.company.delete.agent.details') }}">
        <input type="hidden" id="api-company-get-agent-by-agent-id" value="{{ route('site.company.get.agent.details') }}">

        <input type="hidden" id="api-company-delete-team-by-team-id" value="{{ route('site.company.delete.team.details') }}">

        <input type="hidden" id="api-company-update-team-by-team-id" value="{{ route('site.company.update.team.details') }}">

        <input type="hidden" id="api-agent-team-profile-pic" value="{{ route('site.company.store.agent.profilePic') }}">
        
        <input type="hidden" id="api-site-state-list-by-country-id" value="{{ route('site.get.state.by.country.id','%test') }}">

        <input type="hidden" id="api-site-list-course-search" value="{{ route('site.seafarer.course.search') }}">
        
        <input type="hidden" id="api-site-course-search" value="{{ route('site.seafarer.homepage.course.search') }}">
        <input type="hidden" id="api-site-institute-search" value="{{ route('site.seafarer.homepage.institute.search') }}">

        <input type="hidden" id="api-site-institute-course-details-by-id" value="{{ route('site.institute.course.details','%test') }}">
        <input type="hidden" id="api-site-institute-batch-details-by-id" value="{{ route('site.institute.get.batch.details.by.id','%test') }}">
        <input type="hidden" id="api-site-institute-batch-book-by-location" value="{{ route('site.seafarer.course.book',['%test','%test']) }}">

        <input type="hidden" id="api-site-institute-block-seat" value="{{ route('site.institute.block.seat') }}">

        <input type="hidden" id="api-site-institute-change-batch-status" value="{{ route('site.institute.change.batch.status') }}">

        <input type="hidden" id="api-institute-batch-proceed-to-payment-route" value="{{ route('site.institute.batch.proceed.payment') }}">
        <input type="hidden" id="api-institute-batch-cancel-payment-route" value="{{ route('site.institute.batch.cancel.order') }}">

        <input type="hidden" id="api-institute-delete-image-gallery" value="{{ route('site.institute.delete.image.gallery') }}">

        <input type="hidden" id="api-get-institute-advertise-by-batch-id" value="{{ route('site.get.institute.advertise.by.batch.id') }}">

        <!-- Notification -->
        <input type="hidden" id="api-get-user-notification" value="{{ route('site.get.user.notification') }}">

        <input type="hidden" id="api-upload-user-notification" value="{{ route('site.upload.user.notification') }}">

        <input type="hidden" id="api-set-user-notification-read" value="{{ route('site.set.user.notification.read') }}">

        <script>
            var initialize_pincode_maxlength_validation = false;
            var another_user = false;
            var another_user_update_availability = false;
            var job_apply = false;
            var already_login = 0;
            var auto_action = '';
            var location_block_index = 1;
            var ship_block_index = 1;
            var block_index = 1;
        </script>


        @include('site.partials.header')
        
        <div id="content-wrapper">
        @yield('content')
        </div>

        @include('site.partials.new_footer')

        @include('common.profile-pic-modal')
        
        <!-- Modal old-->
        <div id="login-popup-modal" class="modal fade" role="dialog">
          <div id="login-popup" class="white-popup signin-sigup-default" role="dialog" style="margin-top: 5%;">
            <button type="button" class="close" data-dismiss="modal" style="margin: 10px;">&times;</button>
            <div class="signin-signup-heading">
                <div class="signin-signup new-sigin-link active"><a class="open-popup-link">Login</a></div>
                <div class="signin-signup new-sigup-link"><a class="open-popup-link">Register</a></div>
            </div>
            <form id="loginFormModal" method="post" action="{{route('site.login')}}">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="new-login-content">
                    <div class="row">
                        
                        <div class="small-12 medium-12 columns">
                            <div class="new-login-form-container">
                                <div id="login-form" @if(session('login_error')) style="display: block;" @endif style="display: block;">
                                    <div class="row margin-vert-5">
                                        <div class="col-sm-3 padding-top-5">
                                          <label class="mar-b-10"><span class="label-icon"><i class="fa fa-envelope" aria-hidden="true"></i></span><span class="label-text mandatory">Email</span>
                                        </label>
                                        </div>
                                        <div class="col-sm-9">
                                            <input class="form-control" type="email" name="email" id="email" placeholder="Enter email" value="{{ old('email') }}"/>
                                        </div>
                                    </div>
                                    <div class="row margin-vert-5">
                                        <div class="col-sm-3 padding-top-5">
                                          <label class="mar-b-10"><span class="label-icon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span><span class="label-text mandatory">Password</span>
                                          </label>
                                        </div>
                                        <div class="col-sm-9">
                                            <input class="form-control" type="password" id="password" name="password" placeholder="Enter password"/>
                                            <label id="login_error" class="error"></label>
                                        </div>
                                    </div>
                                    <div class="row margin-vert-5">
                                        <div class="col-sm-6">
<!--                                            <input id="remember" name="remember" type="checkbox">
                                            <label for="remember">Remember Me</label>  -->
                                        </div>
                                        <div class="col-sm-6">
                                            <a href="{{route('site.reset.password.view')}}" class="pull-right">Forget Password?</a> 
                                        </div>
                                    </div>
                                    <div class="row margin-vert-5">
                                        <div class="col-sm-12">
                                            <button type="button" data-style='zoom-in' class="btn-1 light-blue new-sign-up-btn fullwidth ladda-button" id="sign-in-btn">Sign In</button>
                                        </div>
                                    </div>
                                    <!-- <div class="row margin-vert-5">
                                        <div class="col-sm-12">
                                            <div class="forget-pass-link">
                                                <a data-mfp-src="#forgot-password-popup" class="open-popup-link">Forgot Password</a>
                                            </div>
                                        </div>
                                    </div> -->
                                    <div class="row">
                                        <div class="small-12 columns info" align="center" style="display:none">
                                            Logging in. Please wait...
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="ajaxError small-12 columns" align="center"
                                             style="display:{{ session('login_error') ? 'block' : 'none' }}">
                                            <div class="" style="color:#ff0000;">
                                                <strong>Login failed!!!</strong>
                                            </div>
                                            <div class="reason">
                                                {{ session('login_error') ? session('login_error') : '' }}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
          </div>
        </div>

		
        <div id="signup-popup-modal" class="modal fade" role="dialog">
          <div id="signup-popup" class="white-popup signin-sigup-default" style="margin-top: 5%;">
          <button type="button" class="close" data-dismiss="modal" style="margin: 10px;">&times;</button>
            <div class="signin-signup-heading">
                <div class="signin-signup new-sigin-link"><a class="open-popup-link">Login</a></div>
                <div class="signin-signup new-sigup-link active"><a class="open-popup-link">Register</a></div>
				
            </div>
			
           <form id="preRegistrationFormModalTop"  method="post" action="">
            <input type="hidden" name="user_id" value="{{isset($user_data[0]['id']) ? $user_data[0]['id'] : ''}}">
            <div class="new-login-content">
                <div class="row">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="input-label" for="email">Email Address<span class="symbol required"></span></label>
                        </div>
                    </div>
                    <div class="col-sm-9">
                        <div class="form-group">
                            <input type="email" class="form-control" id="user_email" name="email" placeholder="Type your email address">
                            <span class="hide" id="email-error" style="color: red"></span>
                        </div>  
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="input-label" for="password">Password<span class="symbol required"></span></label>
                        </div>
                    </div>
                    <div class="col-sm-9">
                        <div class="form-group">
                            <input type="password" class="form-control" id="o_password" name="password" placeholder="Type your password">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="input-label" for="cpassword">Confirm Password<span class="symbol required"></span></label>
                        </div>
                    </div>
                    <div class="col-sm-9">
                        <div class="form-group">
                            <input type="password" class="form-control" name="cpassword" id="c_password" placeholder="Type password again">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="input-label" for="firstname">Full Name<span class="symbol required"></span></label>
                        </div>
                    </div>
                    <div class="col-sm-9">
                        <div class="form-group">    
                            <input type="text" class="form-control" name="firstname" placeholder="Type your full name">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="input-label" for="dob">Date Of Birth<span class="symbol required"></span></label>
                        </div>
                    </div>
                    <div class="col-sm-9">
                        <div class="form-group">
                            <input type="text" class="form-control birth-date-datepicker" name="dob" placeholder="dd-mm-yyyy">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="input-label" id="mobile" for="mobile">Mobile Number<span class="symbol required"></span></label>
                        </div>
                    </div>
                    <div class="col-sm-9">
                        <div class="form-group">
                            <input type="text" class="form-control" id="mobile" name="mobile" placeholder="Type your mobile number">
                            <span class="hide" id="mobile-error" style="color: red"></span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="input-label" for="nationality">Nationality<span class="symbol required"></span></label>
                        </div>
                    </div>
                    <div class="col-sm-9">
                        <div class="form-group">
                            {{-- {{Form::select('nationality',\CommonHelper::countries(),'',['class'=>'form-control','id'=>'nationality','Placeholder'=>'Select Your Nationality'])}} --}}
                            <select id='nationality' name="nationality" class="form-control" id="nationality">
                                <option value=''>Select Your Nationality</option>
                                @foreach( \CommonHelper::countries() as $c_index => $country)
                                    <option value="{{ $c_index }}"> {{ $country }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
				
                <div class="row">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="input-label" for="current_rank">Current Rank<span class="symbol required"></span></label>
                        </div>
                    </div>
                    <div class="col-sm-9">
                        <div class="form-group">
                            <select id="current_rank" name="current_rank" class="form-control">
                                <option value="">Select Your Rank</option>
                                @foreach(\CommonHelper::new_rank() as $index => $category)
                                    <optgroup label="{{$index}}">
                                    @foreach($category as $r_index => $rank)
                                        <option value="{{$r_index}}">{{$rank}} </option>
                                    @endforeach
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="input-label" for="passport">Passport Number<span class="symbol required"></span></label>
                        </div>
                    </div>
                    <div class="col-sm-9">
                        <div class="form-group">
                            <input type="text" class="form-control" id="passport" name="passport" placeholder="Type your passport number">
                        </div>
                    </div>
                </div>
                <div class="row indos-number d-none">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="input-label" for="passport">Indos Number<span class="symbol required"></span></label>
                        </div>
                    </div>
                    <div class="col-sm-9">
                        <div class="form-group">
                            <input type="text" class="form-control" id="indosno" name="indosno" placeholder="Type the indos number">
                            <label class="indos-error d-none">This field is required.</label>
                        </div>
                    </div>
                </div>
                <div class="row margin-vert-5">
                    <div class="col-sm-12">
                        <button type="submit" data-style='zoom-in' class="btn-1 new-sign-up-btn fullwidth ladda-button" >Register</button>
                    </div>
                </div>
            </div>
        </form>
		
		
    </div>

          </div>
        </div>

        <div id="contact-us-modal" class="modal fade" role="dialog">
          <div class="modal-dialog">

            <!-- Modal content-->
            <button type="button" class="close modal-close" data-dismiss="modal">&times;</button>
            <div class="modal-content">

              <!-- <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Modal Header</h4>
              </div>
              <div class="modal-body">
                <p>Some text in the modal.</p>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div> -->
              <div class="contact-us-discription-container">
                  <div class="heading">
                      Contact Us
                  </div>
                  <div class="contact-details">
                        ConsultanSeas Services <br>

                        Office No. 10. National Complex. <br>

                        Sector 06. New Panvel. <br>

                        Navi Mumbai. <br>

                        Pin Code : 410206. <br>

                        Email : jobs@consultanseas.in <br>

                        Jobs Enquiry : jobs@consultanseas.in <br>

                        Advertisements Enquiry : ad@consultanseas.in <br>

                        Certification Enquiry : cert@consultanseas.in <br>

                        Other Enquiry : admin@consultanseas.in
                  </div>
                  <!-- <div class="para">
                      Lorem ipsum dolor sit amet, his ex justo audiam habemus ashik iuyt jillks
                  </div>
                  <div class="para">
                      Lorem ipsum dolor sit amet, his ex justo audiam habemus ashik iuyt jillks
                  </div> -->
              </div>
              <div class="contact-us-form-container">
                <form id="contact-us-modal-form" action="{{ route('site.contact.us') }}">
                    {{ csrf_field() }}
                  <div class="form-group">
                    <label class="input-label" for="name">Name</label>
                    <input type="text" class="form-control" id="name" name="name">
                  </div>
                  <div class="form-group">
                    <label class="input-label" for="phone">Phone Number</label>
                    <input type="text" class="form-control" id="phone" name="phone">
                  </div>
                  <div class="form-group">
                    <label class="input-label" for="email">Email</label>
                    <!-- <input type="email" class="form-control" id="email" name="email"> -->
                  </div>
                  <div class="form-group">
                    <label class="input-label" for="message">Message</label>
                    <textarea class="form-control" rows="6" name="message" id="message"></textarea>
                  </div>
                    <div class="form-group">
                        <div id="recaptcha1" name="recaptcha1"></div>
                        <label for="yourMessage" class="recaptcha1-error error"></label>
                    </div>
                    <button type="button" data-style="zoom-in" class="ladda-button btn btn-default contact-modal-btn" id="contact-us-submit-button">
                        <span class="ladda-label">
                            Submit
                        </span>
                        <span class="ladda-spinner"></span>
                        <div class="ladda-progress" style="width: 0px;">

                        </div>
                    </button>
                </form>
              </div>
            </div>

          </div>
        </div>

        <div class="modal fade" id="resend_otp_Modal" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close close-otp-modal-button" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Mobile Number Verification</h4>
                    </div>

                    <div class="modal-body">
                        <form id="mob-verification" method="post" action1="{{ route('site.user.mob.verification') }}" action="">
                            {{ csrf_field() }}
                            <div class="row form-group m-b-5">
                                <div class="col-md-12">
                                    <h4 class="m-b-5 m-t-0">Please enter the OTP that you have received on your mobile number.</h4>
                                </div>
                            </div>
                           
                            <div class="row form-group">
                                <div class="col-md-6">
                                    <input class="form-control" name="mob_otp" type="text" placeholder="Enter OTP" id="mob_otp1">
                                </div>
                            </div>
                            <div class="alert alert-danger hide" id="invalid_mob_error">
                                <strong>Invalid!</strong> Entered OTP is Invalid.
                            </div>
                            <div class="alert alert-danger hide" id="otp_failure">

                            </div>
                            <div class="alert alert-success hide" id="otp_resend_success">
                                <strong>Success..</strong> New OTP has been sent to your mobile number..
                            </div>
                            <div class="alert alert-danger hide" id="mob_error_resend_otp">
                                <strong>Error!</strong> Error while generating OTP.
                            </div>
							<div id="default_mob_val"></div>
                        </form>
                    </div>

                    <div class="modal-footer">
                        <button type="button" data-style="zoom-in" class="btn btn-danger ladda-button" id="resend-otp-button-again" style="float: left;">Resend OTP</button>
                        <button type="button" data-style="zoom-in" class="btn btn-danger ladda-button" id="mob_otp_submit_button1" name="mob_otp_submit_button">Verify</button>
                    </div>
                </div>
            </div>
        </div>

        <?php
            $india_value = array_search('India',\CommonHelper::countries());
            $user_id = '';

            if(Auth::check()){
                $user_id = Auth::User()->id;
            }
            if(isset($_GET['availability']) && !empty($_GET['availability'])){
                if($_GET['availability'] == '1'){
                    $result = Auth::User()->professional_detail;

                    if(isset($result) && !empty($result)){
                        $result = $result->toArray();
                        if(isset($result['availability']) && !empty($result['availability'])){
                            $now = time(); // or your date as well
                            $your_date = strtotime($result['availability']);
                            $datediff = $now - $your_date;

                            $day_diff = floor($datediff / (60 * 60 * 24));
                            if($day_diff >= 15 && $your_date < $now){
                                $availability = $result['availability'];
                                $current_rank = $result['applied_rank'];
                                $auto_action = 'availability';
                            }
                        }
                    }

                }
            }
        ?>


    <div id="availability-modal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <form id='rank_availability' action="{{route('site.user.availability.change')}}" method="post">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Update Availability</h4>
                    </div>
                    <div class="modal-body">
                        <p>Please update your availability so that we can match your resume to the appropriate jobs for you.</p>
                        <div class="row">
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label class="input-label">Rank Applied For<span class="symbol required"></span>
                                    </label>
                                    <select id="applied_rank" name="applied_rank" class="form-control">
                                        <option value="">Select Your Rank</option>
                                        @foreach(\CommonHelper::new_rank() as $index => $category)
                                            <optgroup label="{{$index}}"></optgroup>
                                            @foreach($category as $r_index => $rank)
                                                <option value="{{$r_index}}" {{ !empty($current_rank) ? $current_rank == $r_index ? 'selected' : '' : ''}}>{{$rank}} </option>
                                            @endforeach
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label class="input-label">Date of Availability <span class="symbol required"></span>
                                    </label>
                                    <input type="text" class="form-control datepicker" id="date_avaibility" name="date_avaibility" value="{{ isset($availability) ? date('d-m-Y',strtotime($availability)) : ''}}" placeholder="dd-mm-yyyy">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default availability_submit_btn ladda-button" data-style="zoom-in">Save</button>
                    </div>
                </form>
            </div>

        </div>
    </div>

    <div id="feature-avaibility-modal" class="modal fade" role="dialog">
      <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close feature-failed-modal-btn" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Current Plan Availability</h4>
          </div>
          <div class="modal-body">
            <div class="content-head">
                <div class="modal-plan-name-container" style="padding-bottom: 10px;">Current plan : <span class="modal-plan-name"></span></div>
                <div class="table-responsive">
                <table class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>
                                Feature
                            </th>
                            <th>
                                Current limits
                            </th>
                            <th>
                                Used
                            </th>
                            <th>
                                Available
                            </th>
                        </tr>
                    </thead>
                    <tbody class="table-body">

                    </tbody>
                </table>
            </div>
            <div class="msg-error alert alert-danger text-center"></div>
            <div class="msg-success alert alert-success text-center"></div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-danger feature-failed-modal-btn" data-dismiss="modal">Close</button>
            <button type="button" data-id="" data-type="" data-style="zoom-in" class="btn btn-success feature-success-modal-btn ladda-button">Continue</button>
          </div>
        </div>

      </div>
    </div>

        <script type="text/javascript">
            var auto_action;
            auto_action = "{{isset($auto_action) ? $auto_action : ''}}";

            // subscription feature variable start
            var check_company_subscription_url = "{{route('site.checkSubscriptionAvailability')}}";
            var feature_1 = "{{config('feature.feature1')}}";
            var feature_2 = "{{config('feature.feature2')}}";
            var feature_3 = "{{config('feature.feature3')}}";
     $(document).ready(function () {
 
        $(document).on('change','#nationality',function(){
            var val = $(this).val();
            $('.indos-number').addClass('d-none');
            if(val == 95){
                $('.indos-number').removeClass('d-none');
            }
        });
     //ajax request for mobile 
 
 
    //  {{--$.ajax({--}}
    // 	{{--			type: "GET",--}}
    // 	{{--			url: "{{route('preemail')}}",--}}
    // 	{{--			data:  {--}}
    //  {{--                       id: '12'--}}
    //  {{--                   } ,--}}
    // 	{{--			headers: {--}}
    // 	{{--			'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content'),--}}
    // 	{{--			},--}}
    // 	{{--			success:function(response) {--}}
    // 	{{--			//alert(response->otp);--}}
    // 	{{--			--}}
    // 	{{--		     $('#default_mob_val').html(response[0].otp);--}}
    // 	{{--			    console.log(response[0].otp);--}}
    // 	{{--				//alert('Thanks for registration');--}}
    // 	{{--				//location.reload();--}}
    // 	{{--				//$('#preRegistrationFormModalTop').modal('hide');--}}
    // 	{{--				//$('#show_forget').html(response.msg);--}}
    // 	{{--				//$('#forget_button').prop('disabled', true);--}}
    // 	{{--			}--}}
    // 	{{--		});--}}
 
 

     $("#mob_otp_submit_button1").click(function(){
       var get_val=$('#mob_otp1').val();
       var val2=$('#default_mob_val').html();
      //alert("The paragraph was clicked.");
      //alert(get_val);
       //alert(val2);
       if(get_val==val2){
         // alert('hdhdhdhkkk equal');
             // alert('your mobile is verifyed');
              $.ajax({
                                    type: "POST",
                                    url: "{{route('site.seafarer.pre.register')}}",
                                    data: $("#preRegistrationFormModalTop").serialize(),
                                    headers: {
                                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content'),
                                    },
                                    success:function(response) {
                                            alert('Thanks for registration');
                                            location.reload();
                                            //$('#preRegistrationFormModalTop').modal('hide');
                                            //$('#show_forget').html(response.msg);
                                            //$('#forget_button').prop('disabled', true);
                                    }
                            });
			
       }else{
           alert('mobile verification is mendatory');
                return false;
       }
    });

            $('#preRegistrationFormModalTop').validate({
                rules: {
                firstname: {
                    //alpha: true,
                    required: true
                },
                email: {
                    required: true,
                    email: true,
                    remote: {
                        url: $("#api-check-email").val(),
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content'),
                        },
                        type: "post",
                        async: false,
                        data: {
                            id: function(){ return $("input[name='user_id']").val()},
                            email: function(){ return $("#user_email").val(); }
                        }
                    }
                },
                password: {
                    minlength: 6,
                    required: true
                },
                cpassword: {
                    required: true,
                    minlength: 6,
                    equalTo: "#o_password"
                },
                gender: {
                    required: true
                },
                passport: {
                    required: true,
                },
                current_rank: {
                    required: true,
                },
                nationality: {
                    required: true,
                },
                dob: {
                    required: true,
                },
                terms_condition: {
                    required: true,
                },
                mobile: {
                    required: true,
                    number: true,
                    minlength: 10,
                    remote: {
                        url: $("#api-check-mobile").val(),
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content'),
                        },
                        type: "post",
                        async: false,
                        data: {
                            id: function(){ return $("input[name='user_id']").val()},
                            mobile: function(){ return $("input[name='mobile']").val(); }
                        }
                    }
                },
            },
            messages: {
                firstname: {
                    alpha: "Please enter only alphabets",
                    required: "Please specify your full name"
                },
                email: {
                    required: "We need your email address to contact you",
                    email: "Your email address must be in the format of name@domain.com",
                    remote: "Email already exist"
                },
                cpassword: {
                    equalTo: "Confirm password is not matching with password "
                },
                mobile: {
                    number: "Please enter valid Mobile number",
                    remote: "Mobile already exist"
                },
                mobile: {
                    number: "Please enter valid Mobile number",
                    remote: "Mobile already exist"
                },
                terms_condition: {
                    required: "Please agree Terms & Conditions"
                },
                gender: "Please check a gender!",
            },
            errorPlacement: function(error, element) {
                if( element.attr('name') == 'gender'){
                    error.insertAfter($('.gender-option-container'));
                }else if(element.attr("name") == "terms_condition") {
                    error.insertAfter($(element).parent('label'));
                }else{
                    error.insertAfter(element);
                }
            },
                    submitHandler: function (form){
		
                    /*  var validate_mob=1;
		
                                     alert('ddd');
               
                                     $("#resend_otp_Modal").modal();
               return false();
                    if(validate_mob==0){
                       alert('validate bob');
                    }else{
                             alert('validate bob');
                    }*/
		
            if($('#nationality').val() == 95 && $('#indosno').val() == ''){
                    $('.indos-error').removeClass('d-none');
                    return false;
            }
                    // $("#resend_otp_Modal").modal();
                    // return false;
		
                            $.ajax({
                                    type: "POST",
                                    url: "{{route('site.seafarer.pre.register')}}",
                                    data: $("#preRegistrationFormModalTop").serialize(),
                                    beforeSend: function( xhr ) {
                                        $('#preRegistrationFormModalTop')
                                                .find('.new-sign-up-btn')
                                                .html("<i class='fa fa-spin fa-spinner'></i>")
                                                .attr('disabled', true);
                                    },
                                    headers: {
                                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content'),
                                    },
                                    success:function(response) {
                                        $('.register-alert-message').removeClass('d-none');
                                        $('html, body').animate({scrollTop:0},500);
                                        setTimeout(function(){
                                            window.location.href = "{{route('user.profile')}}";
                                        }, 2000);
//                                            alert('Thanks for registration');
//                                            location.reload();
                                            //$('#preRegistrationFormModalTop').modal('hide');
                                            //$('#show_forget').html(response.msg);
                                            //$('#forget_button').prop('disabled', true);
                                    }
                            });
			
			
			
                    }

                    /*submitHandler: function(form) {
                              $.ajax({
                                                    type: "POST",
                                                    url: "{{route('site.seafarer.pre.register')}}",
                                                    data: $("#preRegistrationFormModalTop").serialize(),
                                                    headers: {
                                                            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content'),
                                                    },
                                                    statusCode: {
                                                            200:function (response) {
                                                                    //$('#login-popup-reg-modal-').modal('hide');
                                                                    //if(response.block_seat == '1'){
                                                                            //$(".block-seat").trigger('click');
                                                                    //}
                                                                    //l.stop();
                                                                    alert('ddtt');
                                                            },
                                                            400:function (response) {
                                                                    l.stop();
                                                            }
                                                    }
                                            });*/
            //}
		
        });
            });
        </script>

        <script>
            var india_value = '{{$india_value}}';
            var advertisement_status_change_url = "{{route('site.advertiser.statusChange')}}";
            var advertisement_status_degrade_url = "{{route('site.advertiser.statusDegrade')}}";
            var subscribeKey = "{{env('PUBNUB_SUBSCRIBE_KEY')}}";
            var publishKey = "{{env('PUBNUB_PUBLISH_KEY')}}";
            var user_id = "{{$user_id}}";
        </script>

        <script type="text/javascript" src="{{ URL:: asset('public/assets/js/site_plugins.js')}}?v=<?php echo $site_version; ?>"></script>
        <script type="text/javascript" src="{{ URL:: asset('public/assets/js/site_app.js')}}?v=<?php echo $site_version; ?>"></script>
        <script type="text/javascript" src="{{ URL:: asset('public/assets/js/site/home.js')}}?v=<?php echo $site_version; ?>"></script>
		
        @yield('js_script')
    </body>
</html>