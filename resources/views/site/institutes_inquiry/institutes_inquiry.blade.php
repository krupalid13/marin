@extends('site.cms-index')
@section('content')
<style>
    .enq-cell-col label{
        display: inline-table;
        text-align: left;
        width: 80%;
        font-weight: normal;
        font-size: 1.2em;
        color: grey;
    }
</style>
<div class="container-enq-section" style="padding:20px;">
    <!-- this the page container start -->
    <div class="enq-page-column">
        <div class="enq-form-col">
            <!--the form starts -->
            <h1 class="enquirypage-title">SEND US YOUR DETAILS</h1>
            <form action="" method="" id="inquiry_form">
                <div class="enq-cell-col">
                    <label for="name">
                        Name<span class="enq-star">*</span>
                    </label>
                    <input type="text" name="name" class="EnqInput" placeholder="Enter your Name" id="name" tabindex="1">
                    <input type="hidden" name="type" value="2">
                </div>

                <div class="enq-cell-col">
                    <label for="title">
                        Institute name<span class="enq-star">*</span>
                    </label>
                    <input type="text" name="title" class="EnqInput" placeholder="Enter your Institute Name" id="title" tabindex="2">
                    <br>
                </div>

                <div class="enq-cell-col">
                    <label for="website">
                        The Website
                    </label>
                    <input type="url" name="website" class="EnqInput" placeholder="Enter your Institue Website" id="website" tabindex="3">
                    <br>
                </div>

                <div class="enq-cell-col">
                    <label for="email">
                        Contact Email<span class="enq-star">*</span>
                    </label>
                    <input type="email" name="email" class="EnqInput" placeholder="Enter your Email ID" id="email" tabindex="4">
                    <br>
                </div>

                <div class="enq-cell-col">
                    <label for="number">
                        Contact Number<span class="enq-star">*</span>
                    </label>
                    <input type="text" name="number" class="EnqInput" placeholder="Enter your Contact Number" id="number" tabindex="5">
                    <br>
                </div>

                <br>
                <div class="enq-cell-col">
                    <input type="button" value="SUBMIT" class="EnqButton" id="submit_inquiry" tabindex="6">
                    <br>
                </div>

                <div class="enq-end-st">
                    <h2>We shall get back to you!</h2>
                </div>

            </form>
<!--            <script>
                function msg() {
                    alert("Thank you for showing interest! We shall get back to you.");
                }

            </script>-->
        </div><!-- enq-form-col .... the form ends -->

        <div class="enq-column-data">
            <h1 class="enquirypage-title">FOR MARITIME INSTITUTE</h1>

            <div class="enq-data1">
                <p class="enq-page-data">Maritime centric profile design makes us the best place to showcase your
                    Institute.</p>
                <!-- <p class="enq-page-data">When you can contact credited seafarers that are interested in your job.</p> -->
            </div>

            <div class="enq-data2">
                <p class="enq-page-data">We are helping our user find the best course suited for them.</p>
            </div>

            <div class="enq-data3">
                <p class="enquirypage-data">We are already in collaboration with institutes and helping them reach out
                    to users looking out for the courses they conduct.</p>
            </div>

            <div class="enqpage-letstalk">
                <h1 class="enquirypage-letstalk">LET'S TALK</h1>
                <p class="enquirypage-letstalk-data">On how we can create an advantageous professional platform for you.
                </p>
            </div>

        </div>
    </div>
</div>
<input type="hidden" value="{{route('post-inquiry')}}" id="post-inquiry">
<script type="text/javascript">
    $(document).ready(function () {
        $('#submit_inquiry').click(function () {
            if ($('#name').val() == false) {
                alert("Please enter name");
                return false;
            }
            if ($('#title').val() == false) {
                alert("Please enter institute name");
                return false;
            }
            if ($('#email').val() == false) {
                alert("Please enter email");
                return false;
            }
            var inputvalues = $('#email').val();
            var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            if (!regex.test(inputvalues)) {
                alert("invalid email id");
                return false;
            }
            if ($('#number').val() == false) {
                alert("Please select number");
                return false;
            }
            $.ajax({
                url: $('#post-inquiry').val(),
                data: $("#inquiry_form").serialize(),
                type: "post",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content'),
                },
                statusCode: {
                    200: function (response) {
                        if (response.status == 'success') {
                            document.getElementById("inquiry_form").reset();
                            alert(response.message);
                        } else {
                            alert("Something is wrong, Please try again later");
                        }
                    },
                    400: function (response) {
                        alert("Something is wrong, Please try again later");
                    }
                }
            });
        })
    })
</script>
@stop
