<div class="overlay overlay-fadein"></div>
<div class="dashboard-menu">
	<div class="menubar-btn-down"><i class="fa fa-angle-double-down" aria-hidden="true"></i></div>
	<div class="menubar-btn-up"><i class="fa fa-angle-double-up" aria-hidden="true"></i></div>
	<div class="menubar-name">Dashboard Menu</div>	
</div>
<div class="navbar-container">
	<ul class="nav nav-pills nav-stacked nav-pills-stacked-example">
	  	<li class="nav-link {{Route::currentRouteName() == 'site.show.company.details' ? 'nav-active' : '' }}">
	  		<a href="{{ route('site.show.company.details') }}"><i class="fa fa-building-o nav-icon" aria-hidden="true"></i>My Profile</a>
			<ul class="company-sidebar-submenu">
				<li class="nav-link {{Route::currentRouteName() == 'site.show.company.scope' ? 'nav-active' : '' }}">
					<a href="{{ route('site.show.company.scope') }}">Our Scope</a>
				</li>
				<li class="nav-link {{Route::currentRouteName() == 'site.show.company.team.details' ? 'nav-active' : '' }}">
					<a href="{{ route('site.show.company.team.details') }}">Our Team</a>
				</li>
			</ul>
	  	</li> 
	  	<!-- <li class="nav-link {{Route::currentRouteName() == 'site.company.mysubscription' ? 'nav-active' : '' }}">
	  		<a href="{{ route('site.company.mysubscription') }}"><i class="fa fa-envelope-o nav-icon" aria-hidden="true"></i></i>My Subscriptions</a>
	  	</li>  -->
	  	<li class="nav-link {{Route::currentRouteName() == 'site.company.candidate.search' ? 'nav-active' : '' }}">
	  		<a href="{{ route('site.company.candidate.search') }}"><i class="fa fa-search nav-icon" aria-hidden="true"></i>Candidate Search</a>
	  	</li>
	  	<li class="nav-link {{Route::currentRouteName() == 'site.company.add.jobs' ? 'nav-active' : '' }}">
	  		<a href="{{ route('site.company.add.jobs') }}"><i class="fa fa-briefcase nav-icon" aria-hidden="true"></i>Add Job Post</a>
	  	</li>
	  	<li class="nav-link {{Route::currentRouteName() == 'site.company.list.jobs' || Route::currentRoutename() == 'site.company.edit.jobs'? 'nav-active' : '' }}">
	  		<a href="{{ route('site.company.list.jobs') }}"><i class="fa fa-file-text-o nav-icon" aria-hidden="true"></i>Job Listings</a>
	  	</li>
	  	<li class="nav-link {{Route::currentRouteName() == 'site.company.list.job.applicant' || Route::currentRoutename() == 'site.company.list.job.applicant'? 'nav-active' : '' }}">
	  		<a href="{{ route('site.company.list.job.applicant') }}"><i class="fa fa-file-text-o nav-icon" aria-hidden="true"></i>Job Applicant Listings</a>
	  	</li>
	  	<li class="nav-link {{Route::currentRouteName() == 'site.company.resume.download.list' || Route::currentRoutename() == 'site.company.resume.download.list'? 'nav-active' : '' }}">
	  		<a href="{{ route('site.company.resume.download.list') }}"><i class="fa fa-file-text-o nav-icon" aria-hidden="true">
	  		</i>Resume Downloads</a>
	  	</li>
	  	<li class="nav-link {{Route::currentRouteName() == 'site.company.add.advertise' ? 'nav-active' : '' }}">
	  		<a href="{{ route('site.company.add.advertise') }}"><i class="fa fa-file-text-o nav-icon" aria-hidden="true"></i></i>Add Advertisement</a>
	  	</li>
	  	<li class="nav-link {{Route::currentRouteName() == 'site.company.list.permission.requests' ? 'nav-active' : '' }}">
	  		<a href="{{ route('site.company.list.permission.requests') }}"><i class="fa fa-file-text-o nav-icon" aria-hidden="true"></i></i>Document Permission</a>
	  	</li>
    </ul>
</div>