<div class="notification {{ isset($data['status']) ? $data['status'] == '1' ? 'unread' : 'read' : 'unread'}}" data-notification="{{$data['notification_id']}}" data-url="{{ isset($data['redirect_url']) ? $data['redirect_url'] : ''}}">
    <div class="wrapper">
        <div class="notification-icon" style="background-image: url({{asset($data['image_path'])}})">
            
        </div>
        <div class="notification-contents">
            <div class="notification-heading">
               {{isset($data['title']) ? $data['title'] : ''}} 
            </div>
            <div class="notification-details">
                {{isset($data['message']) ? $data['message'] : ''}} 
            </div>
            <div class="notification-day-time-details">{{isset($data['date']) ? $data['date'] : ''}} </div>
        </div>
    </div>
</div>