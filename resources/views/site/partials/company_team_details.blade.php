<?php
    $india_value = array_search('India',\CommonHelper::countries());
?>

@if(isset($user_data) && !empty($user_data))
    @foreach($user_data as $index => $team)
        <div class="company_team_show_data">
            <div class="company_team_show_data_{{ $team['id'] }}">
                <div class="row team-row">
                    <div class="col-sm-6 team-title">
                        <div class="description">
                            <span class="content-head team-title team-name">Team Agent {{$index+1}}</span>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="company-team-buttons">
                            <div class="company-team-edit-button" data-index-id="{{$index}}" data-team-id="{{$team['id'] }}">
                                <i class="fa fa-edit" aria-hidden="true" title="edit"></i>
                            </div>
                            <div class="company-team-close-button" data-id="{{ $team['id'] }}" data-team-id="{{ $team['id']}}">
                                <i class="fa fa-times" aria-hidden="true" title="delete"></i>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
                    $image_path = '';
                    if(isset($team['profile_pic'])){
                        $image_path = env('COMPANY_TEAM_PROFILE_PIC_PATH')."".$team['id']."/".$team['profile_pic'];
                    }
                ?>
                <div class="row">
                    <input type="hidden" name="team_id" value="{{$team['id']}}">
                    <div class="col-sm-3">
                        <div class="description">
                            <span class="content">
                                @if(!empty($image_path))
                                    <img id="preview" class="preview" src="/{{ $image_path }}">
                                @else
                                    <img id="preview" class="preview" src="/images/user_default_image.png">
                                @endif
                            </span>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="description">
                            <span class="content-head">Member Name : </span>
                            <span class="content">{{ !empty($team['agent_name']) ? $team['agent_name'] : '-' }}</span>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="description">
                            <span class="content-head">Designation : </span>
                            <span class="content">{{ !empty($team['agent_designation']) ? $team['agent_designation'] : '-' }}</span>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="description">
                            <span class="content-head">Location : </span>
                            <span class="content">
                                @if(isset($team['location']['country']) && !empty($team['location']['country']))
                                    @if($team['location']['country'] == $india_value)
                                        {{ ucfirst(\CommonHelper::countries()[$team['location']['country']]) }} {{ '/' }} {{ ucfirst($team['location']['state']['name']) }} {{ '/' }} {{ ucfirst($team['location']['city']['name']) }}
                                    @else
                                        {{ ucfirst(\CommonHelper::countries()[$team['location']['country']]) }} {{ '/' }} {{ ucfirst($team['location']['state_text']) }} {{ '/' }} {{ ucfirst($team['location']['city_text']) }}
                                    @endif
                                @else
                                    -
                                @endif
                            </span>
                        </div>
                    </div>
                </div>
                <hr>
            </div>
        </div>
    @endforeach
@else
    <div class="sub-details-container" style="box-shadow:none;margin:0px;">
        <div class="content-container">
            <div class="row">
                <div class="col-xs-12 text-center">
                    <div class="discription">
                        <span class="content-head">No Data Found.</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif
    