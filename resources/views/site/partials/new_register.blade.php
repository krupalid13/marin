<div class="login_page" style="border: 1px solid #e1e1e1;border-radius: 5px;max-width: 570px;background-color: #f5f5f5;margin:0 auto;padding-bottom: 30px;">
    <div id="login-popup" class="white-popup signin-sigup-default no-margin" role="dialog" >
        <div class="signin-signup-heading">
            <div class="signin-signup new-sigup-link login_page" style="color: #5bb6c8;">
                <b>Register as Seafarer</b>
            </div>
        </div>
        <form id="preRegistrationFormModalTop"  method="post" action="">
            <input type="hidden" name="user_id" value="{{isset($user_data[0]['id']) ? $user_data[0]['id'] : ''}}">
            <input type="hidden" name="user_referral_code" value="@if(isset($_GET['referral']) && !empty($_GET['referral'])){{\Request::query('referral')}}@endif">
            <div class="new-login-content">
                <div class="row">
                    <div class="alert alert-success alert-dismissible show register-alert-message s-msg d-none" role="alert">
                        <strong>Successfully!</strong>
                        <span class="success-msg">Registration Successfully.</span>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="input-label" for="email">Email Address<span class="symbol required"></span></label>
                        </div>
                    </div>
                    <div class="col-sm-9">
                        <div class="form-group">
                            <input type="email" class="form-control" id="user_email" name="email" placeholder="Type your email address">
                            <span class="hide" id="email-error" style="color: red"></span>
                        </div>  
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="input-label" for="password">Password<span class="symbol required"></span></label>
                        </div>
                    </div>
                    <div class="col-sm-9">
                        <div class="form-group">
                            <input type="password" class="form-control" id="o_password" name="password" placeholder="Type your password">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="input-label" for="cpassword">Confirm Password<span class="symbol required"></span></label>
                        </div>
                    </div>
                    <div class="col-sm-9">
                        <div class="form-group">
                            <input type="password" class="form-control" name="cpassword" id="c_password" placeholder="Type password again">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="input-label" for="firstname">First Name<span class="symbol required"></span></label>
                        </div>
                    </div>
                    <div class="col-sm-9">
                        <div class="form-group">    
                            <input type="text" class="form-control" name="firstname" placeholder="Type your first name">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="input-label" for="dob">Date Of Birth<span class="symbol required"></span></label>
                        </div>
                    </div>
                    <div class="col-sm-9">
                        <div class="form-group">
                            <input type="text" class="form-control birth-date-datepicker" name="dob" placeholder="dd-mm-yyyy">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="input-label" id="mobile" for="mobile">Mobile Number<span class="symbol required"></span></label>
                        </div>
                    </div>
                    <div class="col-sm-9">
                        <div class="form-group">
                            <input type="text" class="form-control reg-country-code" id="country_code" name="country_code" placeholder="91" value="91">
                            <input type="text" class="form-control reg-mobile-no" id="mobile" name="mobile" placeholder="Type your mobile number">
                            <span class="hide" id="mobile-error" style="color: red"></span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="input-label" for="nationality">Nationality<span class="symbol required"></span></label>
                        </div>
                    </div>
                    <div class="col-sm-9">
                        <div class="form-group">
                            {{-- {{Form::select('nationality',\CommonHelper::countries(),'',['class'=>'form-control','id'=>'nationality','Placeholder'=>'Select Your Nationality'])}} --}}
                            <select id='nationality' name="nationality" class="form-control" id="nationality">
                                <option value=''>Select Your Nationality</option>
                                @foreach( \CommonHelper::countries() as $c_index => $country)
                                <option value="{{ $c_index }}"> {{ $country }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="input-label" for="current_rank">Current Rank<span class="symbol required"></span></label>
                        </div>
                    </div>
                    <div class="col-sm-9">
                        <div class="form-group">
                            <select id="current_rank" name="current_rank" class="form-control">
                                <option value="">Select Your Rank</option>
                                @foreach(\CommonHelper::new_rank() as $index => $category)
                                <optgroup label="{{$index}}">
                                    @foreach($category as $r_index => $rank)
                                    <option value="{{$r_index}}">{{$rank}} </option>
                                    @endforeach
                                    @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="input-label" for="passport">Passport Number<span class="symbol required"></span></label>
                        </div>
                    </div>
                    <div class="col-sm-9">
                        <div class="form-group">
                            <input type="text" class="form-control" id="passport" name="passport" placeholder="Type your passport number">
                        </div>
                    </div>
                </div>
                <div class="row indos-number d-none">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="input-label" for="passport">Indos Number<span class="symbol required"></span></label>
                        </div>
                    </div>
                    <div class="col-sm-9">
                        <div class="form-group">
                            <input type="text" class="form-control" id="indosno" name="indosno" placeholder="Type the indos number">
                            <label class="indos-error d-none">This field is required.</label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <label>
                            <input type="checkbox" id="terms_condition" name="terms_condition">
                            I have read and agree to the <a href="{{route('tnc')}}" target="_blank">Terms & Conditions</a>.
                        </label>
                    </div>
                    <label calss="terms_condition_error"></label>
                </div>
                <div class="row margin-vert-5">
                    <div class="col-sm-12">
                        <button type="submit" data-style='zoom-in' class="btn-1 new-sign-up-btn fullwidth ladda-button" >Register</button>
                    </div>
                </div>
            </div>
        </form>
    </div>


</div>