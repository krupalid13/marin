<?php error_reporting(0);  $index = 0;$doc = 100; ?>

<?php
if (isset($user_uploaded_documents) && !empty($user_uploaded_documents)) {
    $all_user_uploaded_documents = $user_uploaded_documents;
} else {
    $all_user_uploaded_documents = [];
}

$active_class = "active";
$active_class_tab = "active";
$all_user_uploaded_documents = json_encode($all_user_uploaded_documents);




?>
<style>
    .nav > li > a {
        padding: 10px 11px;
    }

    /* Styles the lightbox, removes it from sight and adds the fade-in transition */

    .lightbox-target {
        position: fixed;
        top: -100%;
        width: 100%;
        background: rgba(0, 0, 0, .7);
        width: 100%;
        opacity: 0;
        -webkit-transition: opacity .5s ease-in-out;
        -moz-transition: opacity .5s ease-in-out;
        -o-transition: opacity .5s ease-in-out;
        transition: opacity .5s ease-in-out;
        overflow: hidden;
    }

    /* Styles the lightbox image, centers it vertically and horizontally, adds the zoom-in transition and makes it responsive using a combination of margin and absolute positioning */

    .lightbox-target img {
        margin: auto;
        position: absolute;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        max-height: 0%;
        max-width: 0%;
        border: 3px solid white;
        box-shadow: 0px 0px 8px rgba(0, 0, 0, .3);
        box-sizing: border-box;
        -webkit-transition: .5s ease-in-out;
        -moz-transition: .5s ease-in-out;
        -o-transition: .5s ease-in-out;
        transition: .5s ease-in-out;
    }

    /* Styles the close link, adds the slide down transition */

    a.lightbox-close {
        display: block;
        width: 50px;
        height: 50px;
        box-sizing: border-box;
        background: white;
        color: black;
        text-decoration: none;
        position: absolute;
        top: -80px;
        right: 0;
        -webkit-transition: .5s ease-in-out;
        -moz-transition: .5s ease-in-out;
        -o-transition: .5s ease-in-out;
        transition: .5s ease-in-out;
    }

    /* Provides part of the "X" to eliminate an image from the close link */

    a.lightbox-close:before {
        content: "";
        display: block;
        height: 30px;
        width: 1px;
        background: black;
        position: absolute;
        left: 26px;
        top: 10px;
        -webkit-transform: rotate(45deg);
        -moz-transform: rotate(45deg);
        -o-transform: rotate(45deg);
        transform: rotate(45deg);
    }

    /* Provides part of the "X" to eliminate an image from the close link */

    a.lightbox-close:after {
        content: "";
        display: block;
        height: 30px;
        width: 1px;
        background: black;
        position: absolute;
        left: 26px;
        top: 10px;
        -webkit-transform: rotate(-45deg);
        -moz-transform: rotate(-45deg);
        -o-transform: rotate(-45deg);
        transform: rotate(-45deg);
    }

    /* Uses the :target pseudo-class to perform the animations upon clicking the .lightbox-target anchor */

    .lightbox-target:target {
        opacity: 1;
        top: 0;
        bottom: 0;
        left: 0;
        z-index: 999;
    }

    .lightbox-target:target img {
        max-height: 100%;
        max-width: 100%;
    }

    .lightbox-target:target a.lightbox-close {
        top: 0px;
    }

    .blockOverlay {

        z-index: 99999 !important;
    }

    .form-container {
        background: #F0F0F0;
        border: #e0dfdf 1px solid;
        padding: 20px;
        border-radius: 2px;
        width: 550px;
        height: 450px;
    }
    .float_right{
        float: right;
    }
</style>
<div class="row">
    
    @if(Session::has('file-size-error'))
        <div class="alert alert-danger alert-dismissible file-size-error" role="alert">
            <strong>Storage!</strong> {{Session::get('file-size-error')}}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif
    
    <div class="col-xs-12">
        <ul class="nav nav-pills nav-fill navtop">

            <li class="nav-item">
                <a id="menu1_" class="nav-link active" href="#menu1" data-toggle="tab">PHOTO</a>
            </li>


            <li class="nav-item">
                <a class="nav-link" href="#menu2" id="menu2_" data-toggle="tab">PASSPORT</a>
            </li>


            <li class="nav-item">
                <a class="nav-link" id="menu3_" href="#menu3" data-toggle="tab">CDC</a>
            </li>
            
            <?php if(@count($documents['sea service']))
            {?>

            <li class="nav-item">
                <a class="nav-link" id="menu8_" href="#menu8" data-toggle="tab">SEA SERVICES</a>
            </li>
            <?php }?>
            <?php if($documents['watch keeping']['watch_keeping'] != '' || $documents['watch keeping']['watch_keeping'] != null)
            {?>
            <li class="nav-item">
                <a class="nav-link" id="menu4_" href="#menu4" data-toggle="tab">WK</a>
            </li>
            <?php }?>
            <?php if(($documents['coc'][0]['coc_number'] != "") || ($documents['coe'][0]['coe_number'] != ""))
            {?>
            <li class="nav-item">
                <a class="nav-link" id="menu5_" href="#menu5" data-toggle="tab">COC</a>
            </li>
            <?php }?>
            @if(isset($documents['user_dangerous_cargo_endorsement_detail']))
                <li class="nav-item">
                    <a class="nav-link" id="menu12_" href="#menu12" data-toggle="tab">DCE</a>
                </li>
            @endif
            <?php if($documents['gmdss']['gmdss_number'] != '')
            {?>
            
            <li class="nav-item">
                <a class="nav-link" id="menu6_" href="#menu6" data-toggle="tab">GMDSS</a>
            </li>
            <?php } ?>

            <li class="nav-item">
                <a class="nav-link" id="menu7_" href="#menu7" data-toggle="tab">COURSES</a>
            </li>
            <?php if(@$documents['watch keeping']['yellow_fever'] == 1 || @$documents['watch keeping']['ilo_medical'] == 1 || @$documents['watch keeping']['screening_test'] == 1 || @$documents['watch keeping']['cholera'] == 1 || @$documents['watch keeping']['hepatitis_b'] == 1 || @$documents['watch keeping']['hepatitis_c'] == 1 || @$documents['watch keeping']['diphtheria'] == 1 || @$documents['watch keeping']['covid'] == 1)
            {?>
            <li class="nav-item">
                <a class="nav-link" id="menu9_" href="#menu9" data-toggle="tab">MEDICALS</a>
            </li>
            <?php } ?>
            <?php if($documents['professional_detail']['school_qualification'] != '' ||  $documents['professional_detail']['institute_degree'])
            {?>
            <li class="nav-item">
                <a class="nav-link" id="menu10_" href="#menu10" data-toggle="tab">ACADEMICS</a>
            </li>
            <?php }?>

            <?php 
                if(@$documents['personal_detail']['account_no'] != '' || @$documents['personal_detail']['aadhaar'] != '' || @$documents['personal_detail']['pancard'] || $documents['watch keeping']['character_reference'] != 0 )
            {?>

            <li class="nav-item">
                <a class="nav-link" id="menu11_" href="#menu11" data-toggle="tab">ID</a>
            </li>
            <?php } ?>

        </ul>
        <div class="tab-content">
            <?php

            $private_document = false;
            $public_document = false;
            //print_r($user_uploaded_documents['photo'][0]);
            if (isset($user_uploaded_documents['photo'][0]['status']) && $user_uploaded_documents['photo'][0]['status'] == '1') {
                $public_document = true;
            } else {
                $private_document = true;
            }

            ?>
            <div class="tab-pane active" role="tabpanel" id="menu1">
                <div class="row" style="margin-left: 1px;margin-top: 10px;">
                    <div class="col-sm-12"
                         style="background: #2a55a2;color: #fff;border-radius: 4px;padding: 10px;">
                        <span style="font-size: 16px;">PHOTO</span> &nbsp; &nbsp;<span style="font-size: 13px;">(The First photo upload will be used in the Resume) </span>
                    </div>
                </div>
                <div class="row add-more-section" id="profile_photo">
                    <div class="col-xs-6 col-sm-7">
                        <span style="color:#47c4f0;float: left;"><i class="fa fa-check-circle-o"
                                                                    style="font-size: 22px;"></i></span>
                        <span style="margin-left: 10px;padding-top: 2px;float: left;color: #47c4f0;">PHOTO</span>
                    </div>
                    <div class="col-xs-6 col-sm-2 pull-right">
                        <?php if(count($user_uploaded_documents['photo'][0]['user_documents']) < 2)
                        {?>
                        <label><a href="javascript:;" class="btn btn-primary edit_doc pull-right" data-type='photo'
                                  data-current='menu1_' data-type-id="0">Add New Image</a></label>
                        <?php }?>
                    </div>
                    


                    <div class="col-xs-12 col-sm-12">
                        <?php $i = 1;$photo_count = 0; foreach($user_uploaded_documents['photo'][0]['user_documents'] as $arrProfilePic){ $photo_count++;?>
                        <div class="col-xs-12 col-sm-2">
                            <div class="avatar-upload">
                                <div class="avatar-delete delete_doc" data-id="<?php echo $arrProfilePic['id'];?>"
                                     data-imagename="<?php echo $arrProfilePic['document_path'];?>" data-type="photo"
                                     data-current="menu1_">
                                    <label for="imageUpload"></label>
                                </div>
                            <!--<div class="avatar-edit edit_doc" data-id="<?php echo $arrProfilePic['id'];?>" data-imagename="<?php echo $arrProfilePic['document_path'];?>" data-type="photo" data-container="photo-<?php echo $i;?>">
															<label for="imageUpload"></label>
														</div>-->
                                <a class="lightbox" href="#preview-photo-<?php echo $i;?>">
                                    <div class="avatar-view" data-id="<?php echo $arrProfilePic['id'];?>"
                                         data-imagename="<?php echo $arrProfilePic['document_path'];?>"
                                         data-type="photo">

                                        <label for="imageUpload"></label>
                                    </div>
                                </a>
                                <?php if(isset($arrProfilePic['document_path']))
                                {
                                $storage_path = 'public/uploads/user_documents/';
                                $type = 'photo';
                                // $url = url($storage_path . $user_id . "/" . $type . "/" . $arrProfilePic['document_path']);
                                $url = env("AWS_URL") . $storage_path . $user_id . "/" . $type . "/" . $arrProfilePic['document_path'];
                                ?>

                                <div class="avatar-preview">
                                    <div class="avatar-edit-img" id="photo-<?php echo $i;?>">

                                        <img src="{{$url}}" class="img-thumbnail">

                                    </div>
                                    <div class="lightbox-target" id="preview-photo-<?php echo $i;?>">
                                        <img src="{{$url}}"/>
                                        <a class="lightbox-close" href="#photo-<?php echo $i;?>"></a>
                                    </div>

                                </div>
                                <?php }else{?>
                                <div class="avatar-preview">
                                    <div class="avatar-edit-img" id="photo-<?php echo $i;?>">
                                        <img src="{{url('public/assets/images/jpg.png')}}" data-action="zoom"
                                             class="pull-left" style="height: 130px;width:130px;">
                                    </div>
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                        <?php $i++;}

                        if($photo_count < 2){
                        for($j = $photo_count;$j < 2;$j++)
                        {?>
                        <div class="col-xs-12 col-sm-2">
                            <div class="avatar-upload">

                            <!--<div class="avatar-edit edit_doc" data-id="" data-imagename="" data-type="photo" data-container="photo-<?php echo $photo_count + 1;?>">
															<label for="imageUpload"></label>
														</div>-->


                                <div class="avatar-preview">
                                    <div class="avatar-edit-img" id="photo-<?php echo $photo_count + 1;?>">
                                        <img src="{{url('public/assets/images/jpg.png')}}" data-action="zoom"
                                             class="pull-left" style="height: 130px;width:130px;">
                                    </div>
                                </div>

                            </div>
                        </div>
                        <?php }}?>

                    </div>
                    <div class="col-xs-12 col-sm-3 pull-right">

                        <div class="form-group pull-right">
                            <label class="radio-inline"><input type="radio" class="photo_show doc-permission-radio-btn"
                                                               name="photo" data-name="photo"
                                                               value="1" {{ $public_document ? 'checked=checked' : ''}}>Public
                            </label>
                            <label class="radio-inline"><input type="radio" class="photo_show doc-permission-radio-btn"
                                                               name="photo" data-name="photo"
                                                               value="0" {{ $private_document ? 'checked=checked' : ''}}>
                                Private</label>

                        </div>
                    </div>
                </div>


            </div>

            <div class="tab-pane" role="tabpanel" id="menu2">
                <div class="row" style="margin-left: 1px;margin-top: 10px;">
                    <div class="col-sm-12"
                         style="background: #2a55a2;color: #fff;border-radius: 4px;padding: 10px;font-size: 16px;">
                        PASSPORT | VISA
                    </div>
                </div>


                <?php if(@count($documents['passport']))
                {
                $private_document = false;
                $public_document = false;

                if (isset($user_uploaded_documents['passport'][0]['status']) && $user_uploaded_documents['passport'][0]['status'] == '1') {
                    $public_document = true;
                } else {
                    $private_document = true;
                }
                ?>


                <div class="row add-more-section" id="profile_passport">
                    <div class="col-xs-6 col-sm-7">
                        <span style="color:#47c4f0;float: left;"><i class="fa fa-check-circle-o"
                                                                    style="font-size: 22px;"></i></span>
                        <span style="margin-left: 10px;padding-top: 2px;float: left;color: #47c4f0;">PASSPOSRT - <?php echo \CommonHelper::countries()[$documents['passport']['pass_country']];?></span>
                    </div>
                    <div class="col-sm-3"></div>
                    <div class="col-xs-6 col-sm-2 pull-right">
                        <?php if(count($user_uploaded_documents['passport'][0]['user_documents']) < 5)
                        {?>
                        <label><a href="javascript:;" class="btn btn-primary edit_doc pull-right" data-type='passport'
                                  data-current='menu2_' data-type-id="0">Add New Image</a></label>
                        <?php }?>
                    </div>
                    <div class="col-xs-12 col-sm-12">
                        <?php $i = 1;$passport_count = 0; foreach($user_uploaded_documents['passport'][0]['user_documents'] as $arrAadhaar){ $passport_count++;?>
                        <div class="col-xs-12 col-sm-2">
                            <div class="avatar-upload">
                                <div class="avatar-delete delete_doc" data-id="<?php echo $arrAadhaar['id'];?>"
                                     data-imagename="<?php echo $arrAadhaar['document_path'];?>" data-type="passport"
                                     data-current="menu2_">
                                    <label for="imageUpload"></label>
                                </div>

                                <a class="lightbox" href="#preview-passport-<?php echo $i;?>">
                                    <div class="avatar-view" data-id="<?php echo $arrAadhaar['id'];?>"
                                         data-imagename="<?php echo $arrAadhaar['document_path'];?>"
                                         data-type="passport">

                                        <label for="imageUpload"></label>
                                    </div>
                                </a>
                                <?php if(isset($arrAadhaar['document_path']))
                                {
                                $storage_path = 'public/uploads/user_documents/';
                                $type = 'passport';
                                // $url = url($storage_path . $user_id . "/" . $type . "/" . $arrAadhaar['document_path']);
                                $url = env("AWS_URL") . $storage_path . $user_id . "/" . $type . "/" . $arrAadhaar['document_path'];
                                ?>

                                <div class="avatar-preview">
                                    <div class="avatar-edit-img" id="passport-<?php echo $i;?>">

                                        <img src="{{$url}}" class="img-thumbnail">

                                    </div>
                                    <div class="lightbox-target" id="preview-passport-<?php echo $i;?>">
                                        <img src="{{$url}}"/>
                                        <a class="lightbox-close" href="#passport-<?php echo $i;?>"></a>
                                    </div>

                                </div>
                                <?php }else{?>
                                <div class="avatar-preview">
                                    <div class="avatar-edit-img" id="passport-<?php echo $i;?>">
                                        <img src="{{url('public/assets/images/jpg.png')}}" data-action="zoom"
                                             class="pull-left" style="height: 130px;width:130px;">
                                    </div>
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                        <?php $i++;}

                        if($passport_count < 5){
                        for($j = $passport_count;$j < 5;$j++)
                        {?>
                        <div class="col-xs-12 col-sm-2">
                            <div class="avatar-upload">
                                <div class="avatar-preview">
                                    <div class="avatar-edit-img" id="passport-<?php echo $passport_count + 1;?>">
                                        <img src="{{url('public/assets/images/jpg.png')}}" data-action="zoom"
                                             class="pull-left" style="height: 130px;width:130px;">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php }}?>
                    </div>
                        <div class="col-xs-12 col-sm-4 visibility-type">
                            <div class="form-group pull-right">
                                <label class="radio-inline"><input type="radio"
                                                                   class="passport_show doc-permission-radio-btn"
                                                                   name="passport" data-name="passport"
                                                                   value="1" {{ $public_document ? 'checked=checked' : ''}}>Public
                                </label>
                                <label class="radio-inline"><input type="radio"
                                                                   class="passport_show doc-permission-radio-btn"
                                                                   name="passport" data-name="passport"
                                                                   value="0" {{ $private_document ? 'checked=checked' : ''}}>
                                    Private</label>
    
                            </div>
                        </div>

                </div>

                <?php }?>

                <?php if(@$documents['passport']['us_visa']){

                $private_document = false;
                $public_document = false;

                if (isset($user_uploaded_documents['us_visa'][0]['status']) && $user_uploaded_documents['us_visa'][0]['status'] == '1') {
                    $public_document = true;
                } else {
                    $private_document = true;
                }

                ?>


                <div class="row add-more-section" id="profile_us_visa">
                    <div class="col-xs-6 col-sm-7">
                        <span style="color:#47c4f0;float: left;"><i class="fa fa-check-circle-o"
                                                                    style="font-size: 22px;"></i></span>
                        <span style="margin-left: 10px;padding-top: 2px;float: left;color: #47c4f0;">VISA - US</span>
                    </div>
                    <div class="col-xs-6 pull-right col-sm-2 pull-right">
                        <?php if(count($user_uploaded_documents['us_visa'][0]['user_documents']) < 1)
                        {?>
                        <label><a href="javascript:;" class="btn btn-primary edit_doc pull-right" data-type='us_visa'
                                  data-current='menu2_' data-type-id="0">Add New Image</a></label>
                        <?php }?>
                    </div>
                    


                    <div class="col-xs-12 col-sm-12">
                        <?php $i = 1;$us_visa_count = 0; foreach($user_uploaded_documents['us_visa'][0]['user_documents'] as $arrAadhaar){ $us_visa_count++;?>
                        <div class="col-xs-12 col-sm-2">
                            <div class="avatar-upload">
                                <div class="avatar-delete delete_doc" data-id="<?php echo $arrAadhaar['id'];?>"
                                     data-imagename="<?php echo $arrAadhaar['document_path'];?>" data-type="us_visa"
                                     data-current="menu2_">
                                    <label for="imageUpload"></label>
                                </div>

                                <a class="lightbox" href="#preview-us_visa-<?php echo $i;?>">
                                    <div class="avatar-view" data-id="<?php echo $arrAadhaar['id'];?>"
                                         data-imagename="<?php echo $arrAadhaar['document_path'];?>"
                                         data-type="us_visa">

                                        <label for="imageUpload"></label>
                                    </div>
                                </a>
                                <?php if(isset($arrAadhaar['document_path']))
                                {
                                $storage_path = 'public/uploads/user_documents/';
                                $type = 'us_visa';
                                $url = env("AWS_URL") . $storage_path . $user_id . "/" . $type . "/" . $arrAadhaar['document_path'];?>

                                <div class="avatar-preview">
                                    <div class="avatar-edit-img" id="us_visa-<?php echo $i;?>">

                                        <img src="{{$url}}" class="img-thumbnail">

                                    </div>
                                    <div class="lightbox-target" id="preview-us_visa-<?php echo $i;?>">
                                        <img src="{{$url}}"/>
                                        <a class="lightbox-close" href="#us_visa-<?php echo $i;?>"></a>
                                    </div>

                                </div>
                                <?php }else{?>
                                <div class="avatar-preview">
                                    <div class="avatar-edit-img" id="us_visa-<?php echo $i;?>">
                                        <img src="{{url('public/assets/images/jpg.png')}}" data-action="zoom"
                                             class="pull-left" style="height: 130px;width:130px;">
                                    </div>
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                        <?php $i++;}

                        if($us_visa_count < 1){
                        for($j = $us_visa_count;$j < 1;$j++)
                        {?>
                        <div class="col-xs-12 col-sm-2">
                            <div class="avatar-upload">


                                <div class="avatar-preview">
                                    <div class="avatar-edit-img" id="us_visa-<?php echo $us_visa_count + 1;?>">
                                        <img src="{{url('public/assets/images/jpg.png')}}" data-action="zoom"
                                             class="pull-left" style="height: 130px;width:130px;">
                                    </div>
                                </div>

                            </div>
                        </div>
                        <?php }}?>
                    </div>
                        <div class="col-xs-12 col-sm-4 visibility-type">
                            <div class="form-group pull-right">
                                <label class="radio-inline"><input type="radio"
                                                                   class="us_visa_show doc-permission-radio-btn"
                                                                   name="us_visa" data-name="us_visa"
                                                                   value="1" {{ $public_document ? 'checked=checked' : ''}}>Public
                                </label>
                                <label class="radio-inline"><input type="radio"
                                                                   class="us_visa_show doc-permission-radio-btn"
                                                                   name="us_visa" data-name="us_visa"
                                                                   value="0" {{ $private_document ? 'checked=checked' : ''}}>
                                    Private</label>
    
                            </div>
                        </div>

                </div>


                <?php } ?>
            </div>
            <div class="tab-pane" role="tabpanel" id="menu3">
                <div class="row" style="margin-left: 1px;margin-top: 10px;">
                    <div class="col-sm-12"
                         style="background: #2a55a2;color: #fff;border-radius: 4px;padding: 10px;font-size: 16px;">
                        <?php 
                            if(!empty($data['personal_detail']) && $data['personal_detail']['nationality'] == 95){ ?>
                                INDOS | 
                            <?php } 
                        ?>
                        <?php 
                            if(!empty($data['professional_detail']['sid_number']) && !empty($data['professional_detail']['sid_number'])){ ?>
                                SID | 
                            <?php } 
                        ?>
                         CDC
                    </div>
                </div>
                <?php //if(@count($documents['indos']))
                //{
                $private_document = false;
                $public_document = false;
                $private_document_sid = false;
                $public_document_sid = false;

                if (isset($user_uploaded_documents['indos'][0]['status']) && $user_uploaded_documents['indos'][0]['status'] == '1') {
                    $public_document = true;
                } else {
                    $private_document = true;
                }
                
                if (isset($user_uploaded_documents['sid'][0]['status']) && $user_uploaded_documents['sid'][0]['status'] == '1') {
                    $public_document_sid = true;
                } else {
                    $private_document_sid = true;
                }



                ?>

                <?php 
                if(!empty($data['personal_detail']) && $data['personal_detail']['nationality'] == 95){ ?>
                    
                
                <div class="row add-more-section" id="profile_indos">
                    <div class="col-xs-6 col-sm-7">
                        <span style="color:#47c4f0;float: left;"><i class="fa fa-check-circle-o"
                                                                    style="font-size: 22px;"></i></span>
                        <span style="margin-left: 10px;padding-top: 2px;float: left;color: #47c4f0;">INDOS</span>
                    </div>
                    <div class="col-xs-6 col-sm-2 pull-right">
                        <?php if(count($user_uploaded_documents['indos'][0]['user_documents']) < 1)
                        {?>
                        <label><a href="javascript:;" class="btn btn-primary edit_doc pull-right" data-type='indos'
                                  data-current='menu3_' data-type-id="0">Add New Image</a></label>
                        <?php }?>
                    </div>
                    


                    <div class="col-xs-12 col-sm-12">
                        <?php $i = 1;$indos_count = 0; foreach($user_uploaded_documents['indos'][0]['user_documents'] as $arrIndos){ $indos_count++;?>
                        <div class="col-xs-12 col-sm-2">
                            <div class="avatar-upload">
                                <div class="avatar-delete delete_doc" data-id="<?php echo $arrIndos['id'];?>"
                                     data-imagename="<?php echo $arrIndos['document_path'];?>" data-type="indos"
                                     data-current="menu3_">
                                    <label for="imageUpload"></label>
                                </div>

                                <a class="lightbox" href="#preview-indos-<?php echo $i;?>">
                                    <div class="avatar-view" data-id="<?php echo $arrIndos['id'];?>"
                                         data-imagename="<?php echo $arrIndos['document_path'];?>" data-type="indos">

                                        <label for="imageUpload"></label>
                                    </div>
                                </a>
                                <?php if(isset($arrIndos['document_path']))
                                {
                                $storage_path = 'public/uploads/user_documents/';
                                $type = 'indos';
                                $url = env("AWS_URL") . $storage_path . $user_id . "/" . $type . "/" . $arrIndos['document_path'];?>

                                <div class="avatar-preview">
                                    <div class="avatar-edit-img" id="indos-<?php echo $i;?>">

                                        <img src="{{$url}}" class="img-thumbnail">

                                    </div>
                                    <div class="lightbox-target" id="preview-indos-<?php echo $i;?>">
                                        <img src="{{$url}}"/>
                                        <a class="lightbox-close" href="#indos-<?php echo $i;?>"></a>
                                    </div>

                                </div>
                                <?php }else{?>
                                <div class="avatar-preview">
                                    <div class="avatar-edit-img" id="indos-<?php echo $i;?>">
                                        <img src="{{url('public/assets/images/jpg.png')}}" data-action="zoom"
                                             class="pull-left" style="height: 130px;width:130px;">
                                    </div>
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                        <?php $i++;}

                        if($indos_count < 1){
                        for($j = $indos_count;$j < 1;$j++)
                        {?>
                        <div class="col-xs-12 col-sm-2">
                            <div class="avatar-upload">


                                <div class="avatar-preview">
                                    <div class="avatar-edit-img" id="indos-<?php echo $indos_count + 1;?>">
                                        <img src="{{url('public/assets/images/jpg.png')}}" data-action="zoom"
                                             class="pull-left" style="height: 130px;width:130px;">
                                    </div>
                                </div>

                            </div>
                        </div>
                        <?php }}?>
                    </div>
                        <div class="col-xs-12 col-sm-4 visibility-type">

                            <div class="form-group pull-right">
                                <label class="radio-inline"><input type="radio" class="indos_show doc-permission-radio-btn"
                                                                   name="indos" data-name="indos"
                                                                   value="1" {{ $public_document ? 'checked=checked' : ''}}>Public
                                </label>
                                <label class="radio-inline"><input type="radio" class="indos_show doc-permission-radio-btn"
                                                                   name="indos" data-name="indos"
                                                                   value="0" {{ $private_document ? 'checked=checked' : ''}}>
                                    Private</label>
    
                            </div>
                        </div>

                </div>
                <?php 
                            if(!empty($data['professional_detail']['sid_number']) && !empty($data['professional_detail']['sid_number'])){ ?>
                
                <div class="row add-more-section" id="profile_indos">
                    <div class="col-xs-6 col-sm-7">
                        <span style="color:#47c4f0;float: left;"><i class="fa fa-check-circle-o"
                                                                    style="font-size: 22px;"></i></span>
                        <span style="margin-left: 10px;padding-top: 2px;float: left;color: #47c4f0;">SID</span>
                    </div>
                    <div class="col-xs-6 col-sm-2 pull-right">
                        <?php if(count($user_uploaded_documents['sid'][0]['user_documents']) < 1)
                        {?>
                        <label><a href="javascript:;" class="btn btn-primary edit_doc pull-right" data-type='sid'
                                  data-current='menu3_' data-type-id="0">Add New Image</a></label>
                        <?php }?>
                    </div>
                    


                    <div class="col-xs-12 col-sm-12">
                        <?php $i = 1;$sid_count = 0; foreach($user_uploaded_documents['sid'][0]['user_documents'] as $arrSid){ $sid_count++;?>
                        <div class="col-xs-12 col-sm-2">
                            <div class="avatar-upload">
                                <div class="avatar-delete delete_doc" data-id="<?php echo $arrSid['id'];?>"
                                     data-imagename="<?php echo $arrSid['document_path'];?>" data-type="sid"
                                     data-current="menu3_">
                                    <label for="imageUpload"></label>
                                </div>

                                <a class="lightbox" href="#preview-indos-<?php echo $i;?>">
                                    <div class="avatar-view" data-id="<?php echo $arrSid['id'];?>"
                                         data-imagename="<?php echo $arrSid['document_path'];?>" data-type="sid">

                                        <label for="imageUpload"></label>
                                    </div>
                                </a>
                                <?php if(isset($arrSid['document_path']))
                                {
                                $storage_path = 'public/uploads/user_documents/';
                                $type = 'sid';
                                $url = env("AWS_URL") . $storage_path . $user_id . "/" . $type . "/" . $arrSid['document_path'];?>

                                <div class="avatar-preview">
                                    <div class="avatar-edit-img" id="indos-<?php echo $i;?>">

                                        <img src="{{$url}}" class="img-thumbnail">

                                    </div>
                                    <div class="lightbox-target" id="preview-indos-<?php echo $i;?>">
                                        <img src="{{$url}}"/>
                                        <a class="lightbox-close" href="#sid-<?php echo $i;?>"></a>
                                    </div>

                                </div>
                                <?php }else{?>
                                <div class="avatar-preview">
                                    <div class="avatar-edit-img" id="sid-<?php echo $i;?>">
                                        <img src="{{url('public/assets/images/jpg.png')}}" data-action="zoom"
                                             class="pull-left" style="height: 130px;width:130px;">
                                    </div>
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                        <?php $i++;}

                        if($sid_count < 1){
                        for($j = $sid_count;$j < 1;$j++)
                        {?>
                        <div class="col-xs-12 col-sm-2">
                            <div class="avatar-upload">


                                <div class="avatar-preview">
                                    <div class="avatar-edit-img" id="indos-<?php echo $sid_count + 1;?>">
                                        <img src="{{url('public/assets/images/jpg.png')}}" data-action="zoom"
                                             class="pull-left" style="height: 130px;width:130px;">
                                    </div>
                                </div>

                            </div>
                        </div>
                        <?php }}?>
                    </div>
                        <div class="col-xs-12 col-sm-4 visibility-type">

                            <div class="form-group pull-right">
                                <label class="radio-inline"><input type="radio" class="sid_show doc-permission-radio-btn"
                                                                   name="sid" data-name="sid"
                                                                   value="1" {{ $public_document_sid ? 'checked=checked' : ''}}>Public
                                </label>
                                <label class="radio-inline"><input type="radio" class="sid_show doc-permission-radio-btn"
                                                                   name="sid" data-name="sid"
                                                                   value="0" {{ $private_document_sid ? 'checked=checked' : ''}}>
                                    Private</label>
    
                            </div>
                        </div>

                </div>
                <?php }
                ?>
                <?php }
                ?>

                <?php //}?>
                <?php for($i = 0;$i < count($documents['cdc']);$i++){
                $k = $i + 1;
                $_documents = $documents['cdc'][$i];
                $_unique_id = $documents['cdc'][$i]['id'];
                ?>
                <div class="row add-more-section" id="profile_cdc">
                    <div class="col-xs-6 col-sm-7">
                        <span style="color:#47c4f0;float: left;"><i class="fa fa-check-circle-o"
                                                                    style="font-size: 22px;"></i></span>
                        <span style="margin-left: 10px;padding-top: 2px;float: left;color: #47c4f0;">CDC - <?php echo \CommonHelper::countries()[$documents['cdc'][$i]['cdc']];?></span>
                    </div>
                    <div class="col-sm-3"></div>
                    <div class="col-xs-6 col-sm-2">
                        <?php if(count($user_uploaded_documents['cdc'][$_unique_id]['user_documents']) < 5)
                        {?>
                        <label><a href="javascript:;" class="btn btn-primary edit_doc pull-right" data-type='cdc'
                                  data-current='menu3_' data-type-id="{{$_unique_id}}">Add New Image</a></label>
                        <?php }?>
                    </div>
                    @php
                            if($user_uploaded_documents['cdc'][$_unique_id]['status']==1) $_status=1;
                            else $_status=0;
                        @endphp
                    

                    <div class="col-xs-12 col-sm-12">
                        <?php foreach($user_uploaded_documents['cdc'][$_unique_id]['user_documents'] as $_docs_up){?>
                        <div class="col-xs-12 col-sm-2">
                            <div class="avatar-upload">
                                <div class="avatar-delete delete_doc" data-id="<?php echo $_docs_up['id'];?>"
                                     data-imagename="<?php echo $_docs_up['document_path'];?>" data-type="cdc"
                                     data-current="menu3_">
                                    <label for="imageUpload"></label>
                                </div>

                                <a class="lightbox" href="#preview-cdc-{{$_docs_up['id']*2}}">
                                    <div class="avatar-view" data-id="<?php echo $_docs_up['id'];?>"
                                         data-imagename="<?php echo $_docs_up['document_path'];?>" data-type="cdc">

                                        <label for="imageUpload"></label>
                                    </div>
                                </a>
                                <?php if(isset($_docs_up['document_path']))
                                {
                                $storage_path = 'public/uploads/user_documents/';
                                $type = 'cdc';
                                $url = env("AWS_URL") . $storage_path . $user_id . "/" . $type . "/" . $_unique_id . "/" . $_docs_up['document_path'];?>

                                <div class="avatar-preview">
                                    <div class="avatar-edit-img" id="cdc-{{$_docs_up['id']*2}}">

                                        <img src="{{$url}}" class="img-thumbnail">

                                    </div>
                                    <div class="lightbox-target" id="preview-cdc-{{$_docs_up['id']*2}}">
                                        <img src="{{$url}}"/>
                                        <a class="lightbox-close" href="#cdc-{{$_docs_up['id']*2}}"></a>
                                    </div>

                                </div>
                                <?php }else{?>
                                <div class="avatar-preview">
                                    <div class="avatar-edit-img" id="cdc-{{$_docs_up['id']*2}}">
                                        <img src="{{url('public/assets/images/jpg.png')}}" data-action="zoom"
                                             class="pull-left" style="height: 130px;width:130px;">
                                    </div>
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                        <?php }

                        $doc_count = count($user_uploaded_documents['cdc'][$_unique_id]['user_documents']);{
                        for($j = $doc_count;$j < 5;$j++)
                        {?>
                        <div class="col-xs-12 col-sm-2">
                            <div class="avatar-upload">


                                <div class="avatar-preview">
                                    <div class="avatar-edit-img" id="cdc-<?php echo $doc_count + 1;?>">
                                        <img src="{{url('public/assets/images/jpg.png')}}" data-action="zoom"
                                             class="pull-left" style="height: 130px;width:130px;">
                                    </div>
                                </div>

                            </div>
                        </div>
                        <?php }}?>
                    </div>
                        <div class="col-xs-12 col-sm-4 visibility-type">
                        
                            <div class="form-group pull-right">
                                <label class="radio-inline"><input type="radio" class="doc_show doc-permission-radio-btn"
                                                                   name="cdc{{$_unique_id}}" data-name="cdc"
                                                                   value="1" {{($_status==1)?'checked=checked':''}}>Public
                                </label>
                                <label class="radio-inline"><input type="radio" class="doc_show doc-permission-radio-btn"
                                                                   name="cdc{{$_unique_id}}" data-name="cdc"
                                                                   value="0" {{($_status==1)?'':'checked=checked'}}>Private
                                </label>
                            </div>
                        </div>


                </div>


                <?php } ?>
            </div>

            <div class="tab-pane" role="tabpanel" id="menu4">
                <div class="row" style="margin-left: 1px;margin-top: 10px;">
                    <div class="col-sm-12"
                         style="background: #2a55a2;color: #fff;border-radius: 4px;padding: 10px;font-size: 16px;">
                        WK
                    </div>
                </div>
                <?php  $watch_keeping = $documents['watch keeping']['watch_keeping'];?>

                <?php if($documents['watch keeping']['watch_keeping'])
                {
                $private_document = false;
                $public_document = false;

                if (isset($user_uploaded_documents['wk_cop'][0]['status']) && $user_uploaded_documents['wk_cop'][0]['status'] == '1') {
                    $public_document = true;
                } else {
                    $private_document = true;
                }
                ?>


                <div class="row add-more-section" id="profile_wk_cop">
                    <div class="col-xs-6 col-sm-7">
                        <span style="color:#47c4f0;float: left;"><i class="fa fa-check-circle-o"
                                                                    style="font-size: 22px;"></i></span>
                        <span style="margin-left: 10px;padding-top: 2px;float: left;color: #47c4f0;">WK / COP - <?php if ($watch_keeping) {
                                echo \CommonHelper::countries()[$watch_keeping];
                            }?></span>
                    </div>
                    <div class="col-xs-6 col-sm-2 pull-right">
                        <?php if(count($user_uploaded_documents['wk_cop'][0]['user_documents']) < 1)
                        {?>
                        <label><a href="javascript:;" class="btn btn-primary edit_doc pull-right" data-type='wk_cop'
                                  data-current='menu4_' data-type-id="0">Add New Image</a></label>
                        <?php }?>
                    </div>

                    <div class="col-xs-12 col-sm-12">
                        <?php $i = 1;$wk_cop_count = 0; foreach($user_uploaded_documents['wk_cop'][0]['user_documents'] as $arrAadhaar){ $wk_cop_count++;?>
                        <div class="col-xs-12 col-sm-2">
                            <div class="avatar-upload">
                                <div class="avatar-delete delete_doc" data-id="<?php echo $arrAadhaar['id'];?>"
                                     data-imagename="<?php echo $arrAadhaar['document_path'];?>" data-type="wk_cop"
                                     data-current="menu4_">
                                    <label for="imageUpload"></label>
                                </div>

                                <a class="lightbox" href="#preview-wk_cop-<?php echo $i;?>">
                                    <div class="avatar-view" data-id="<?php echo $arrAadhaar['id'];?>"
                                         data-imagename="<?php echo $arrAadhaar['document_path'];?>" data-type="wk_cop">

                                        <label for="imageUpload"></label>
                                    </div>
                                </a>
                                <?php if(isset($arrAadhaar['document_path']))
                                {
                                $storage_path = 'public/uploads/user_documents/';
                                $type = 'wk_cop';
                                $url = env("AWS_URL") . $storage_path . $user_id . "/" . $type . "/" . $arrAadhaar['document_path'];?>

                                <div class="avatar-preview">
                                    <div class="avatar-edit-img" id="wk_cop-<?php echo $i;?>">

                                        <img src="{{$url}}" class="img-thumbnail">

                                    </div>
                                    <div class="lightbox-target" id="preview-wk_cop-<?php echo $i;?>">
                                        <img src="{{$url}}"/>
                                        <a class="lightbox-close" href="#wk_cop-<?php echo $i;?>"></a>
                                    </div>

                                </div>
                                <?php }else{?>
                                <div class="avatar-preview">
                                    <div class="avatar-edit-img" id="wk_cop-<?php echo $i;?>">
                                        <img src="{{url('public/assets/images/jpg.png')}}" data-action="zoom"
                                             class="pull-left" style="height: 130px;width:130px;">
                                    </div>
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                        <?php $i++;}

                        if($wk_cop_count < 1){
                        for($j = $wk_cop_count;$j < 1;$j++)
                        {?>
                        <div class="col-xs-12 col-sm-2">
                            <div class="avatar-upload">


                                <div class="avatar-preview">
                                    <div class="avatar-edit-img" id="wk_cop-<?php echo $wk_cop_count + 1;?>">
                                        <img src="{{url('public/assets/images/jpg.png')}}" data-action="zoom"
                                             class="pull-left" style="height: 130px;width:130px;">
                                    </div>
                                </div>

                            </div>
                        </div>
                        <?php }}?>
                    </div>
                        <div class="col-xs-12 col-sm-4 visibility-type">

                            <div class="form-group pull-right">
                                <label class="radio-inline"><input type="radio" class="wk_cop_show doc-permission-radio-btn"
                                                                   name="wk_cop" data-name="wk_cop"
                                                                   value="1" {{ $public_document ? 'checked=checked' : ''}}>Public
                                </label>
                                <label class="radio-inline"><input type="radio" class="wk_cop_show doc-permission-radio-btn"
                                                                   name="wk_cop" data-name="wk_cop"
                                                                   value="0" {{ $private_document ? 'checked=checked' : ''}}>
                                    Private</label>
    
                            </div>
                        </div>

                </div>

                <?php }?>


            </div>

            <div class="tab-pane" role="tabpanel" id="menu5">
                <div class="row" style="margin-left: 1px;margin-top: 10px;">
                    <div class="col-sm-12"
                         style="background: #2a55a2;color: #fff;border-radius: 4px;padding: 10px;font-size: 16px;">
                        COC | COE
                    </div>
                </div>
                <?php if(@count($documents['coc']) && $documents['coc'][0]['coc_number'] != "")
                {?>
                <?php for($i = 0;$i < count($documents['coc']);$i++){
                $_coc_document = $documents['coc'][$i];
                ?>
                <div class="row add-more-section" id="profile_cdc">
                    <div class="col-xs-6 col-sm-7">
                        <span style="color:#47c4f0;float: left;"><i class="fa fa-check-circle-o"
                                                                    style="font-size: 22px;"></i></span>
                        <span style="margin-left: 10px;padding-top: 2px;float: left;color: #47c4f0;">COC - <?php echo $documents['coc'][$i]['coc_grade'];?> - <?php echo \CommonHelper::countries()[$documents['coc'][$i]['coc']];?></span>
                    </div>
                    <div class="col-sm-3"></div>
                    <div class="col-xs-6 col-sm-2">
                        @if(count($user_uploaded_documents['coc'][$_coc_document['id']]['user_documents'])<5)
                            <label><a href="javascript:;" class="btn btn-primary edit_doc pull-right" data-type='coc'
                                      data-current='menu5_' data-type-id="{{$_coc_document['id']}}">Add New
                                    Image</a></label>
                        @endif
                        @php
                            if($user_uploaded_documents['coc'][$_coc_document['id']]['status']==1) $_status=1;
                            else $_status=0;
                        @endphp
                    </div>
                    <div class="col-xs-12 col-sm-12">
                        <?php $cocDocumentIndex = 0;
                        foreach($user_uploaded_documents['coc'][$_coc_document['id']]['user_documents'] as $_docs_ups) { ?>
                        <div class="col-xs-12 col-sm-2">
                            <div class="avatar-upload">
                                <div class="avatar-delete delete_doc" data-id="<?php echo $_docs_ups['id'];?>"
                                     data-imagename="<?php echo $_docs_ups['document_path'];?>" data-type="coc"
                                     data-current="menu5_">
                                    <label for="imageUpload"></label>
                                </div>

                                <a class="lightbox" href="#preview-coc-{{$_coc_document['id']}}{{$cocDocumentIndex}}">
                                    <div class="avatar-view" data-id="<?php echo $_docs_ups['id'];?>"
                                         data-imagename="<?php echo $_docs_ups['document_path'];?>" data-type="coc">

                                        <label for="imageUpload"></label>
                                    </div>
                                </a>
                                <?php if(isset($_docs_ups['document_path']))
                                {
                                $storage_path = 'public/uploads/user_documents/';
                                $type = 'coc';
                                $url = env("AWS_URL") . $storage_path . $user_id . "/" . $type . "/" . $_coc_document['id'] . "/" . $_docs_ups['document_path'];
                                ?>
                                <div class="avatar-preview">
                                    <div class="avatar-edit-img" id="coc-{{$_coc_document['id']}}">

                                        <img src="{{$url}}" class="img-thumbnail">

                                    </div>
                                    <div class="lightbox-target" id="preview-coc-{{$_coc_document['id']}}{{$cocDocumentIndex}}">
                                        <img src="{{$url}}"/>
                                        <a class="lightbox-close" href="#coc-{{$_coc_document['id']}}"></a>
                                    </div>

                                </div>
                                <?php }else{?>
                                <div class="avatar-preview">
                                    <div class="avatar-edit-img" id="coc-{{$_coc_document['id']}}">
                                        <img src="{{url('public/assets/images/jpg.png')}}" data-action="zoom"
                                             class="pull-left" style="height: 130px;width:130px;">
                                    </div>
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                        <?php $cocDocumentIndex++; }

                        $doc_count = count($user_uploaded_documents['coc'][$_coc_document['id']]['user_documents']);
                        {
                        for($j = $doc_count;$j < 5;$j++)
                        {?>
                        <div class="col-xs-12 col-sm-2">
                            <div class="avatar-upload">
                                <div class="avatar-preview">
                                    <div class="avatar-edit-img" id="coc-{{$_coc_document['id']}}">
                                        <img src="{{url('public/assets/images/jpg.png')}}" data-action="zoom"
                                             class="pull-left" style="height: 130px;width:130px;">
                                    </div>
                                </div>

                            </div>
                        </div>
                        <?php }}?>
                    </div>
                        <div class="col-xs-12 col-sm-4 visibility-type">
                            <div class="form-group pull-right">
                                <label class="radio-inline"><input type="radio" class="doc_show doc-permission-radio-btn"
                                                                   name="coc-{{$user_uploaded_documents['coc'][$_coc_document['id']]['type_id']}}"
                                                                   data-name="coc"
                                                                   data-type-id="{{$user_uploaded_documents['coc'][$_coc_document['id']]['type_id']}}"
                                                                   value="1" {{($_status==1)?'checked=checked':''}}>Public
                                </label>
                                <label class="radio-inline"><input type="radio" class="doc_show doc-permission-radio-btn"
                                                                   name="coc-{{$user_uploaded_documents['coc'][$_coc_document['id']]['type_id']}}"
                                                                   data-name="coc"
                                                                   data-type-id="{{$user_uploaded_documents['coc'][$_coc_document['id']]['type_id']}}"
                                                                   value="0" {{($_status==1)?'':'checked=checked'}}> Private</label>
                            </div>
                        </div>

                </div>
                <?php }
                }?>

                <?php if(@count($documents['coe']) && $documents['coe'][0]['id'] != "")
                {?>
                <?php for($i = 0;$i < count($documents['coe']);$i++){
                $_coe_document = $documents['coe'][$i];
                $_unique_id_coe = $documents['coe'][$i]['id'];
                ?>
                <div class="row add-more-section" id="profile_cdc">
                    <div class="col-xs-6 col-sm-7">
                        <span style="color:#47c4f0;float: left;"><i class="fa fa-check-circle-o"
                                                                    style="font-size: 22px;"></i></span>
                        <span style="margin-left: 10px;padding-top: 2px;float: left;color: #47c4f0;">COE - <?php echo $documents['coe'][$i]['coe_grade'];?> - <?php echo \CommonHelper::countries()[$documents['coe'][$i]['coe']];?></span>
                    </div>
                    <div class="col-sm-3"></div>
                    <div class="col-xs-6 col-sm-2">
                        <?php if(count($user_uploaded_documents['coe'][$_unique_id_coe]['user_documents']) < 2)
                        {?>
                        <label><a href="javascript:;" class="btn btn-primary edit_doc pull-right" data-type='coe'
                                  data-current="menu5_" data-type-id="{{$_unique_id_coe}}">Add New Image</a></label>
                        <?php }?>
                        @php
                            if($user_uploaded_documents['coe'][$_unique_id_coe]['status']==1) $_status=1;
                            else $_status=0;
                        @endphp
                    </div>
                   

                    <div class="col-xs-12 col-sm-12">
                        <?php $doc_count = 0; foreach($user_uploaded_documents['coe'][$_unique_id_coe]['user_documents'] as $_coe_docs_up){ $doc_count++;?>
                        <div class="col-xs-12 col-sm-2">
                            <div class="avatar-upload">
                                <div class="avatar-delete delete_doc" data-id="<?php echo $_coe_docs_up['id'];?>"
                                     data-imagename="<?php echo $_coe_docs_up['document_path'];?>" data-type="coe"
                                     data-current="menu5_">
                                    <label for="imageUpload"></label>
                                </div>

                                <a class="lightbox" href="#preview-coe-{{$_unique_id_coe}}">
                                    <div class="avatar-view" data-id="<?php echo $_coe_docs_up['id'];?>"
                                         data-imagename="<?php echo $_coe_docs_up['document_path'];?>" data-type="coe">

                                        <label for="imageUpload"></label>
                                    </div>
                                </a>
                                <?php if(isset($_coe_docs_up['document_path']))
                                {
                                $storage_path = 'public/uploads/user_documents/';
                                $type = 'coe';
                                $url = env("AWS_URL") . $storage_path . $user_id . "/" . $type . "/" . $_unique_id_coe . "/" . $_coe_docs_up['document_path'];?>

                                <div class="avatar-preview">
                                    <div class="avatar-edit-img" id="coe{{$_unique_id_coe}}">

                                        <img src="{{$url}}" class="img-thumbnail">

                                    </div>
                                    <div class="lightbox-target" id="preview-coe-{{$_unique_id_coe}}">
                                        <img src="{{$url}}"/>
                                        <a class="lightbox-close" href="#coe{{$_unique_id_coe}}"></a>
                                    </div>

                                </div>
                                <?php }else{?>
                                <div class="avatar-preview">
                                    <div class="avatar-edit-img" id="coe-{{$_unique_id_coe}}">
                                        <img src="{{url('public/assets/images/jpg.png')}}" data-action="zoom"
                                             class="pull-left"
                                             style="height: 130px;width:130px;">
                                    </div>
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                        <?php }

                        if($doc_count < 2){
                        for($j = $doc_count;$j < 2;$j++)
                        {?>
                        <div class="col-xs-12 col-sm-2">
                            <div class="avatar-upload">


                                <div class="avatar-preview">
                                    <div class="avatar-edit-img" id="coe-<?php echo $doc_count + 1;?>">
                                        <img src="{{url('public/assets/images/jpg.png')}}" data-action="zoom"
                                             class="pull-left"
                                             style="height: 130px;width:130px;">
                                    </div>
                                </div>

                            </div>
                        </div>
                        <?php }}?>
                    </div>
                        <div class="col-xs-12 col-sm-4 visibility-type">
                            <div class="form-group pull-right">
                                <label class="radio-inline"><input type="radio" class="doc_show doc-permission-radio-btn"
                                                                   name="coe-{{$_unique_id_coe}}"
                                                                   data-name="coe"
                                                                   data-type-id="{{$_unique_id_coe}}"
                                                                   value="1" {{($_status==1)?'checked=checked':''}}>Public
                                </label>
                                <label class="radio-inline"><input type="radio" class="doc_show doc-permission-radio-btn"
                                                                   name="coe-{{$_unique_id_coe}}"
                                                                   data-name="coe"
                                                                   data-type-id="{{$_unique_id_coe}}"
                                                                   value="0" {{($_status==1)?'':'checked=checked'}}> Private</label>
                            </div>
                        </div>


                </div>


                <?php }}?>

            </div>
            <div class="tab-pane" role="tabpanel" id="menu6">
                <div class="row" style="margin-left: 1px;margin-top: 10px;">
                    <div class="col-sm-12"
                         style="background: #2a55a2;color: #fff;border-radius: 4px;padding: 10px;font-size: 16px;">
                        GMDSS
                    </div>
                </div>
                <?php //if($documents['gmdss'])
                //{
                $private_document = false;
                $public_document = false;

                if (isset($user_uploaded_documents['gmdss'][0]['status']) && $user_uploaded_documents['gmdss'][0]['status'] == '1') {
                    $public_document = true;
                } else {
                    $private_document = true;
                }
                ?>


                <div class="row add-more-section" id="profile_gmdss">
                    <div class="col-xs-6 col-sm-7">
                        <span style="color:#47c4f0;float: left;"><i class="fa fa-check-circle-o"
                                                                    style="font-size: 22px;"></i></span>
                        <span style="margin-left: 10px;padding-top: 2px;float: left;color: #47c4f0;">GMDSS - {{\CommonHelper::countries()[$documents['gmdss']['gmdss']] }}</span>
                    </div>
                    <div class="col-sm-3"></div>
                    <div class="col-xs-6 col-sm-2">
                        <?php if(count($user_uploaded_documents['gmdss'][0]['user_documents']) < 5)
                        {?>
                        <label><a href="javascript:;" class="btn btn-primary edit_doc pull-right" data-type='gmdss'
                                  data-current="menu6_" data-type-id="0">Add New Image</a></label>
                        <?php }?>
                    </div>
            
                    <div class="col-xs-12 col-sm-12">
                        <?php $i = 1;$gmdss_count = 0; foreach($user_uploaded_documents['gmdss'][0]['user_documents'] as $arrAadhaar){ $gmdss_count++;?>
                        <div class="col-xs-12 col-sm-2">
                            <div class="avatar-upload">
                                <div class="avatar-delete delete_doc" data-id="<?php echo $arrAadhaar['id'];?>"
                                     data-imagename="<?php echo $arrAadhaar['document_path'];?>" data-type="gmdss"
                                     data-current="menu6_">
                                    <label for="imageUpload"></label>
                                </div>

                                <a class="lightbox" href="#preview-gmdss-<?php echo $i;?>">
                                    <div class="avatar-view" data-id="<?php echo $arrAadhaar['id'];?>"
                                         data-imagename="<?php echo $arrAadhaar['document_path'];?>" data-type="gmdss">

                                        <label for="imageUpload"></label>
                                    </div>
                                </a>
                                <?php if(isset($arrAadhaar['document_path']))
                                {
                                $storage_path = 'public/uploads/user_documents/';
                                $type = 'gmdss';
                                $url = env("AWS_URL") . $storage_path . $user_id . "/" . $type . "/" . $arrAadhaar['document_path'];?>

                                <div class="avatar-preview">
                                    <div class="avatar-edit-img" id="gmdss-<?php echo $i;?>">

                                        <img src="{{$url}}" class="img-thumbnail">

                                    </div>
                                    <div class="lightbox-target" id="preview-gmdss-<?php echo $i;?>">
                                        <img src="{{$url}}"/>
                                        <a class="lightbox-close" href="#gmdss-<?php echo $i;?>"></a>
                                    </div>

                                </div>
                                <?php }else{?>
                                <div class="avatar-preview">
                                    <div class="avatar-edit-img" id="gmdss-<?php echo $i;?>">
                                        <img src="{{url('public/assets/images/jpg.png')}}" data-action="zoom"
                                             class="pull-left" style="height: 130px;width:130px;">
                                    </div>
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                        <?php $i++;}

                        if($gmdss_count < 5){
                        for($j = $gmdss_count;$j < 5;$j++)
                        {?>
                        <div class="col-xs-12 col-sm-2">
                            <div class="avatar-upload">


                                <div class="avatar-preview">
                                    <div class="avatar-edit-img" id="gmdss-<?php echo $gmdss_count + 1;?>">
                                        <img src="{{url('public/assets/images/jpg.png')}}" data-action="zoom"
                                             class="pull-left" style="height: 130px;width:130px;">
                                    </div>
                                </div>

                            </div>
                        </div>
                        <?php }}?>
                    </div>
                        <div class="col-xs-12 col-sm-4 visibility-type">

                            <div class="form-group pull-right">
                                <label class="radio-inline"><input type="radio" class="gmdss_show doc-permission-radio-btn"
                                                                   name="gmdss" data-name="gmdss"
                                                                   value="1" {{ $public_document ? 'checked=checked' : ''}}>Public
                                </label>
                                <label class="radio-inline"><input type="radio" class="gmdss_show doc-permission-radio-btn"
                                                                   name="gmdss" data-name="gmdss"
                                                                   value="0" {{ $private_document ? 'checked=checked' : ''}}>
                                    Private</label>
    
                            </div>
                        </div>

                </div>

                <?php //}?>
            </div>
            <div class="tab-pane" role="tabpanel" id="menu7">
                <div class="row" style="margin-left: 1px;margin-top: 10px;">
                    <div class="col-sm-12"
                         style="background: #2a55a2;color: #fff;border-radius: 4px;padding: 10px;font-size: 16px;">
                        COURSE CERTIFICATE
                    </div>
                </div>
                <?php for($i = 0;$i < count($documents['courses']);$i++)
                {
                $k = $i + 1;
                $private_document = false;
                $public_document = false;
                $_documents = $documents['courses'][$i];
                if (isset($user_uploaded_documents['courses'][$_documents['course_id']]['status']) && $user_uploaded_documents['courses'][$_documents['course_id']]['status'] == '1') {
                    $public_document = true;
                } else {
                    $private_document = true;
                }?>
                <div class="row add-more-section" id="profile_sea_service">
                    <div class="col-xs-8 col-sm-8">
                        <span style="color:#47c4f0;float: left;"><i class="fa fa-check-circle-o"
                                                                    style="font-size: 22px;"></i></span>
                        <span style="margin-left: 10px;padding-top: 2px;float: left;color: #47c4f0;">COURSE :
														 <?php $arrData = \CommonHelper::course_name_by_course_type($_documents['course_type']);

                            foreach ($arrData as $val) {
                                if ($_documents['course_id'] == $val->id) {
                                    echo $val->course_name;
                                }
                            }

                            ?>
													</span>
                    </div>
                    <div class="col-sm-3"></div>
                    <div class="col-xs-6 col-sm-2" style="float: right;">
                        @if(count($user_uploaded_documents['courses'][$_documents['course_id']]['user_documents'])<1)
                            <label><a href="javascript:;" class="btn btn-primary edit_doc pull-right"
                                      data-type='courses' data-current="menu7_"
                                      data-type-id="{{$documents['courses'][$i]['course_id']}}">Add New
                                    Image</a></label>
                        @endif
                    </div>
                    

                    <div class="col-xs-12 col-sm-12">
                        @if(!empty($user_uploaded_documents['courses'][$_documents['course_id']]['user_documents']))
                            @foreach($user_uploaded_documents['courses'][$_documents['course_id']]['user_documents'] as $user_docs)
                                <div class="col-xs-12 col-sm-2">
                                    <div class="avatar-upload">
                                        <div class="avatar-delete delete_doc" data-id="{{ $user_docs['id'] }}"
                                             data-imagename="{{ $user_docs['document_path'] }}" data-type="courses"
                                             data-current="menu7_">
                                            <label for="imageUpload"></label>
                                        </div>
                                        <a class="lightbox"
                                           href="#preview-course-{{ $_documents['course_id'] }}-{{ $user_docs['id'] }}">
                                            <div class="avatar-view" data-id="{{ $user_docs['id'] }}"
                                                 data-imagename="{{ $user_docs['document_path'] }}" data-type="courses">
                                                <label for="imageUpload"></label>
                                            </div>
                                        </a>
                                        <?php
                                        $storage_path = 'public/uploads/user_documents/';
                                        $type = 'courses';
                                        $url = env("AWS_URL") . $storage_path . $user_id . "/" . $type . "/" . $_documents['course_id'] . "/" . $user_docs['document_path'];?>
                                        <div class="avatar-preview">
                                            <div class="avatar-edit-img"
                                                 id="course{{ $_documents['course_id'] }}-{{ $user_docs['id'] }}">

                                                <img src="{{$url}}" class="img-thumbnail">

                                            </div>
                                            <div class="lightbox-target"
                                                 id="preview-course-{{ $_documents['course_id'] }}-{{ $user_docs['id'] }}">
                                                <img src="{{$url}}"/>
                                                <a class="lightbox-close"
                                                   href="#course{{ $_documents['course_id'] }}-{{ $user_docs['id'] }}"></a>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @else
                            <div class="avatar-preview">
                                <div class="avatar-edit-img" id="courses">
                                    <img src="{{url('public/assets/images/jpg.png')}}" data-action="zoom"
                                         class="pull-left" style="height: 130px;width:130px;">
                                </div>
                            </div>
                        @endif
                    </div>
                        <div class="col-xs-12 col-sm-4 visibility-type">
                            <div class="form-group pull-right">
                                <label class="radio-inline"><input type="radio" class="doc_show doc-permission-radio-btn"
                                                                   name="courses-{{$documents['courses'][$i]['course_id']}}"
                                                                   data-name="courses"
                                                                   data-type-id="{{$documents['courses'][$i]['course_id']}}"
                                                                   value="1" {{ $public_document ? 'checked=checked' : ''}}>Public
                                </label>
                                <label class="radio-inline"><input type="radio" class="doc_show doc-permission-radio-btn"
                                                                   name="courses-{{$documents['courses'][$i]['course_id']}}"
                                                                   data-name="courses"
                                                                   data-type-id="{{$documents['courses'][$i]['course_id']}}"
                                                                   value="0" {{ $private_document ? 'checked=checked' : ''}}>
                                    Private</label>
                            </div>
                        </div>
                </div>
                <?php }?>
            </div>

            <div class="tab-pane" role="tabpanel" id="menu8">
                <div class="row" style="margin-left: 1px;margin-top: 10px;">
                    <div class="col-sm-12"
                         style="background: #2a55a2;color: #fff;border-radius: 4px;padding: 10px;font-size: 16px;">
                        SEA SERVICES
                    </div>
<!--                    <div class="col-sm-12">
                        <p style="color:red;">Upload a cropped image of your CDC service stamp page</p>
                    </div>-->
                </div>
                <?php for($i = 0;$i < count($documents['sea service']);$i++)
                {
                $k = $i + 1;
                $_documents = $documents['sea service'][$i];
                $private_document = false;
                $public_document = false;

                if (isset($user_uploaded_documents['sea_service'][$_documents['id']]['status']) && $user_uploaded_documents['sea_service'][$_documents['id']]['status'] == '1') {
                    $public_document = true;
                } else {
                    $private_document = true;
                }
                ?>
                <div class="row add-more-section" id="profile_sea_service">
                    <div class="col-xs-8 col-sm-8">
                        
                        @php
                            $toDate = isset($documents['sea service'][$i]['to']) && !empty($documents['sea service'][$i]['to']) ? date('d-m-Y', strtotime($documents['sea service'][$i]['to'])) : 'Still on board';
                        @endphp
                        <span style="margin-left: 10px;padding-top: 2px;float: left;color: #47c4f0;"><?php echo 'FROM: ' . date('d-m-Y', strtotime($documents['sea service'][$i]['from'])) . ' TO ' .  $toDate . '&nbsp;&nbsp;SHIP: ' . $documents['sea service'][$i]['ship_name'];?></span>
                    </div>
                    <div class="col-xs-8 col-sm-8" style="margin: 8px 0px;">
                        <span style="margin-left: 10px; color:#47c4f0;float: left;"><i class="fa fa-check-circle-o"style="font-size: 19px;"></i> CDC STAMPED PAGE </span>
                    </div>
                    <div class="col-sm-3"></div>
                    <div class="col-xs-6 col-sm-2 pull-right">
                        <?php if(count($user_uploaded_documents['sea_service'][$_documents['id']]['user_documents']) < 1)
                        {?>
                        <label><a href="javascript:;" class="btn btn-primary edit_doc pull-right"
                                  data-type='sea_service' data-current="menu8_"
                                  data-type-id="{{$_documents['id']}}">Add New Image</a></label>
                        <?php }?>
                    </div>

                    <div class="col-xs-12 col-sm-12">
                        <?php $j = 1;$doc_count = 0; foreach($user_uploaded_documents['sea_service'][$_documents['id']]['user_documents'] as $docs){ $doc_count++;?>
                        <div class="col-xs-12 col-sm-2">
                            <div class="avatar-upload">
                                <div class="avatar-delete delete_doc" data-id="<?php echo $docs['id'];?>"
                                     data-imagename="<?php echo $docs['document_path'];?>" data-type="sea_service"
                                     data-current="menu8_">
                                    <label for="imageUpload"></label>
                                </div>

                                <a class="lightbox"
                                   href="#preview-sea_service-{{ $_documents['id'] }}-{{  $docs['id'] }}">
                                    <div class="avatar-view" data-id="<?php echo $docs['id'];?>"
                                         data-imagename="<?php echo $docs['document_path'];?>" data-type="sea_service">

                                        <label for="imageUpload"></label>
                                    </div>
                                </a>
                                <?php if(isset($docs['document_path']))
                                {
                                $storage_path = 'public/uploads/user_documents/';
                                $type = 'sea_service';
                                $_date = $_documents['id'];
                                $url = env("AWS_URL") . $storage_path . $user_id . "/" . $type . "/" . $_date . "/" . $docs['document_path'];?>
                                <div class="avatar-preview">
                                    <div class="avatar-edit-img" id="sea_service{{$_date}}-{{  $docs['id'] }}">

                                        <img src="{{$url}}" class="img-thumbnail">

                                    </div>
                                    <div class="lightbox-target"
                                         id="preview-sea_service-{{$_date}}-{{  $docs['id'] }}">
                                        <img src="{{$url}}"/>
                                        <a class="lightbox-close" href="#sea_service{{$_date}}-{{  $docs['id'] }}"></a>
                                    </div>

                                </div>
                                <?php }else{?>
                                <div class="avatar-preview">
                                    <div class="avatar-edit-img" id="sea_service{{$_date}}-{{  $docs['id'] }}">
                                        <img src="{{url('public/assets/images/jpg.png')}}" data-action="zoom"
                                             class="pull-left" style="height: 130px;width:130px;">
                                    </div>
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                        <?php $j++;}

                        if($doc_count < 1){
                        for($j = $doc_count;$j < 1;$j++)
                        {?>
                        <div class="col-xs-12 col-sm-2">
                            <div class="avatar-upload">


                                <div class="avatar-preview">
                                    <div class="avatar-edit-img" id="sea_service"<?php $doc_count + 1;?>>
                                        <img src="{{url('public/assets/images/jpg.png')}}" data-action="zoom"
                                             class="pull-left" style="height: 130px;width:130px;">
                                    </div>
                                </div>

                            </div>
                        </div>
                        <?php }}?>
                    </div>
                        <div class="col-xs-12 col-sm-4 visibility-type">
                            <div class="form-group pull-right">
                                <label class="radio-inline"><input type="radio" class="doc_show doc-permission-radio-btn"
                                                                   name="sea_service{{$_documents['id']}}"
                                                                   data-name="sea_service" value="1"
                                                                   {{ $public_document ? 'checked=checked' : ''}} data-type-id="{{$_documents['id']}}">Public
                                </label>
                                <label class="radio-inline"><input type="radio" class="doc_show doc-permission-radio-btn"
                                                                   name="sea_service{{$_documents['id']}}"
                                                                   data-name="sea_service" value="0"
                                                                   {{ $private_document ? 'checked=checked' : ''}} data-type-id="{{$_documents['id']}}">
                                    Private</label>
                            </div>
                        </div>
                        <input type="hidden"  class="check{{$_documents['id']}}contract" value="plus">
                        <input type="hidden"  class="check{{$_documents['id']}}article" value="plus">
                         <div class="col-xs-12 col-sm-8 "  style="    padding: 0px 30px;    margin-top: 16px;">
                                <a href="javascript:void(0)" data-didm="{{$_documents['id']}}" data-type="contract" data-plus="plus" class="btn btn-success btn-sm clicktoopen" style="text-decoration: none;">Contract <span class="{{$_documents['id']}}pluscontract">+</span></a>
                                @if($documents['sea service'][$i]['ship_flag'] == 95)
                                <a href="javascript:void(0)" data-didm="{{$_documents['id']}}" data-type="article"  class="btn btn-success btn-sm clicktoopen" style="margin-left: 15px; text-decoration: none;">Article <span class="{{$_documents['id']}}plusarticle">+</span></a>
                                @endif
                                
                            
                        </div>
                        
                        <!------------ Contract section start ------------------>
                    <div class="row {{$_documents['id']}}contract" style="display: none; margin-left: 0px; margin-right: 0px;">
                        <?php 
                        $_documents = $documents['sea service'][$i];
                        $private_document = false;
                        $public_document = false;

                        if (isset($user_uploaded_documents['sea_service'][$_documents['id']]['status']) && $user_uploaded_documents['sea_service'][$_documents['id']]['status'] == '1') {
                            $public_document = true;
                        } else {
                            $private_document = true;
                        }
                        ?>
                        <div class="col-xs-8 col-sm-8" style="margin: 8px 0px;">
                            <span style="margin-left: 10px; color:#47c4f0;float: left;"><i class="fa fa-check-circle-o"style="font-size: 19px;"></i> Contract </span>
                        </div>
                        <div class="col-sm-3"></div>
                        <div class="col-xs-6 col-sm-2 pull-right">
                            <?php if(count($user_uploaded_documents['sea_service_contract'][$_documents['id'].'contract']['user_documents']) < 5)
                            {?>
                            <label><a href="javascript:;" class="btn btn-primary edit_doc pull-right"
                                      data-type='sea_service_contract' data-current="menu8_"
                                      data-type-id="{{$_documents['id'].'contract'}}">Add New Image</a></label>
                            <?php }?>
                        </div>
                        <div class="col-xs-12 col-sm-12">
                        <?php $j = 1;$doc_count_contract = 0; foreach($user_uploaded_documents['sea_service_contract'][$_documents['id'].'contract']['user_documents'] as $docs){ $doc_count_contract++;?>
                        <div class="col-xs-12 col-sm-2">
                            <div class="avatar-upload">
                                <div class="avatar-delete delete_doc" data-id="<?php echo $docs['id'];?>"
                                     data-imagename="<?php echo $docs['document_path'];?>" data-type="sea_service_contract"
                                     data-current="menu8_">
                                    <label for="imageUpload"></label>
                                </div>

                                <a class="lightbox"
                                   href="#preview-sea_service_contract-{{ $_documents['id'].'contract' }}-{{  $docs['id'] }}">
                                    <div class="avatar-view" data-id="<?php echo $docs['id'];?>"
                                         data-imagename="<?php echo $docs['document_path'];?>" data-type="sea_service_contract">

                                        <label for="imageUpload"></label>
                                    </div>
                                </a>
                                <?php if(isset($docs['document_path']))
                                {
                                $storage_path = 'public/uploads/user_documents/';
                                $type = 'sea_service_contract';
                                $_date = $_documents['id'].'contract';
                                $url = env("AWS_URL") . $storage_path . $user_id . "/" . $type . "/" . $_date . "/" . $docs['document_path'];?>
                                <div class="avatar-preview">
                                    <div class="avatar-edit-img" id="sea_service_contract{{$_date}}-{{  $docs['id'] }}">

                                        <img src="{{$url}}" class="img-thumbnail">

                                    </div>
                                    <div class="lightbox-target"
                                         id="preview-sea_service_contract-{{$_date}}-{{  $docs['id'] }}">
                                        <img src="{{$url}}"/>
                                        <a class="lightbox-close" href="#sea_service_contract{{$_date}}-{{  $docs['id'] }}"></a>
                                    </div>

                                </div>
                                <?php }else{?>
                                <div class="avatar-preview">
                                    <div class="avatar-edit-img" id="sea_service_contract{{$_date}}-{{  $docs['id'] }}">
                                        <img src="{{url('public/assets/images/jpg.png')}}" data-action="zoom"
                                             class="pull-left" style="height: 130px;width:130px;">
                                    </div>
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                        <?php $j++;}

                        if($doc_count_contract < 5){
                        for($j = $doc_count_contract;$j < 5;$j++)
                        {?>
                        <div class="col-xs-12 col-sm-2">
                            <div class="avatar-upload">


                                <div class="avatar-preview">
                                    <div class="avatar-edit-img" id="sea_service_contract"<?php $doc_count_contract + 1;?>>
                                        <img src="{{url('public/assets/images/jpg.png')}}" data-action="zoom"
                                             class="pull-left" style="height: 130px;width:130px;">
                                    </div>
                                </div>

                            </div>
                        </div>
                        <?php }}?>
                    </div>
                    <div class="col-xs-12 col-sm-4 visibility-type">
                        <div class="form-group pull-right">
                            <label class="radio-inline"><input type="radio" class="doc_show doc-permission-radio-btn"
                                                               name="sea_service_contract{{$_documents['id'].'contract'}}"
                                                               data-name="sea_service_contract" value="1"
                                                               {{ $public_document ? 'checked=checked' : ''}} data-type-id="{{$_documents['id'].'contract'}}">Public
                            </label>
                            <label class="radio-inline"><input type="radio" class="doc_show doc-permission-radio-btn"
                                                               name="sea_service_contract{{$_documents['id'].'contract'}}"
                                                               data-name="sea_service_contract" value="0"
                                                               {{ $private_document ? 'checked=checked' : ''}} data-type-id="{{$_documents['id'].'contract'}}">
                                Private</label>
                        </div>
                    </div>
                </div>
                        <!------------ Contract section end ------------------>
                    <!------------ Atricle section start ------------------>
                    @if($documents['sea service'][$i]['ship_flag'] == 95)
                    <div class="row {{$_documents['id']}}article" style="display: none; margin-left: 0px; margin-right: 0px;">
                    
                        <?php 
                        $_documents = $documents['sea service'][$i];
                        $private_document = false;
                        $public_document = false;

                        if (isset($user_uploaded_documents['sea_service'][$_documents['id']]['status']) && $user_uploaded_documents['sea_service'][$_documents['id']]['status'] == '1') {
                            $public_document = true;
                        } else {
                            $private_document = true;
                        }
                        ?>
                        <div class="col-xs-8 col-sm-8" style="margin: 8px 0px;">
                            <span style="margin-left: 10px; color:#47c4f0;float: left;"><i class="fa fa-check-circle-o"style="font-size: 19px;"></i> Article </span>
                        </div>
                        <div class="col-sm-3"></div>
                        <div class="col-xs-6 col-sm-2 pull-right">
                            <?php if(count($user_uploaded_documents['sea_service_article'][$_documents['id'].'article']['user_documents']) < 2)
                            {?>
                            <label><a href="javascript:;" class="btn btn-primary edit_doc pull-right"
                                      data-type='sea_service_article' data-current="menu8_"
                                      data-type-id="{{$_documents['id'].'article'}}">Add New Image</a></label>
                            <?php }?>
                        </div>
                        <div class="col-xs-12 col-sm-12">
                        <?php $j = 1;$doc_count_article = 0; foreach($user_uploaded_documents['sea_service_article'][$_documents['id'].'article']['user_documents'] as $docs){ $doc_count_article++;?>
                        <div class="col-xs-12 col-sm-2">
                            <div class="avatar-upload">
                                <div class="avatar-delete delete_doc" data-id="<?php echo $docs['id'];?>"
                                     data-imagename="<?php echo $docs['document_path'];?>" data-type="sea_service_article"
                                     data-current="menu8_">
                                    <label for="imageUpload"></label>
                                </div>

                                <a class="lightbox"
                                   href="#preview-sea_service_article-{{ $_documents['id'].'article' }}-{{  $docs['id'] }}">
                                    <div class="avatar-view" data-id="<?php echo $docs['id'];?>"
                                         data-imagename="<?php echo $docs['document_path'];?>" data-type="sea_service_article">

                                        <label for="imageUpload"></label>
                                    </div>
                                </a>
                                <?php if(isset($docs['document_path']))
                                {
                                $storage_path = 'public/uploads/user_documents/';
                                $type = 'sea_service_article';
                                $_date = $_documents['id'].'article';
                                $url = env("AWS_URL") . $storage_path . $user_id . "/" . $type . "/" . $_date . "/" . $docs['document_path'];?>
                                <div class="avatar-preview">
                                    <div class="avatar-edit-img" id="sea_service_article{{$_date}}-{{  $docs['id'] }}">

                                        <img src="{{$url}}" class="img-thumbnail">

                                    </div>
                                    <div class="lightbox-target"
                                         id="preview-sea_service_article-{{$_date}}-{{  $docs['id'] }}">
                                        <img src="{{$url}}"/>
                                        <a class="lightbox-close" href="#sea_service_article{{$_date}}-{{  $docs['id'] }}"></a>
                                    </div>

                                </div>
                                <?php }else{?>
                                <div class="avatar-preview">
                                    <div class="avatar-edit-img" id="sea_service_article{{$_date}}-{{  $docs['id'] }}">
                                        <img src="{{url('public/assets/images/jpg.png')}}" data-action="zoom"
                                             class="pull-left" style="height: 130px;width:130px;">
                                    </div>
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                        <?php $j++;}

                        if($doc_count_article < 2){
                        for($j = $doc_count_article;$j < 2;$j++)
                        {?>
                        <div class="col-xs-12 col-sm-2">
                            <div class="avatar-upload">


                                <div class="avatar-preview">
                                    <div class="avatar-edit-img" id="sea_service_article"<?php $doc_count_article + 1;?>>
                                        <img src="{{url('public/assets/images/jpg.png')}}" data-action="zoom"
                                             class="pull-left" style="height: 130px;width:130px;">
                                    </div>
                                </div>

                            </div>
                        </div>
                        <?php }}?>
                    </div>
                    <div class="col-xs-12 col-sm-4 visibility-type">
                        <div class="form-group pull-right">
                            <label class="radio-inline"><input type="radio" class="doc_show doc-permission-radio-btn"
                                                               name="sea_service_article{{$_documents['id'].'article'}}"
                                                               data-name="sea_service_article" value="1"
                                                               {{ $public_document ? 'checked=checked' : ''}} data-type-id="{{$_documents['id'].'article'}}">Public
                            </label>
                            <label class="radio-inline"><input type="radio" class="doc_show doc-permission-radio-btn"
                                                               name="sea_service_article{{$_documents['id'].'article'}}"
                                                               data-name="sea_service_article" value="0"
                                                               {{ $private_document ? 'checked=checked' : ''}} data-type-id="{{$_documents['id'].'article'}}">
                                Private</label>
                        </div>
                    </div>
                </div>
                @endif
                        <!------------ article section end ------------------>

                </div>

                <?php } ?>
            </div>


            <div class="tab-pane" role="tabpanel" id="menu9">
                <div class="row" style="margin-left: 1px;margin-top: 10px;">
                    <div class="col-sm-12"
                         style="background: #2a55a2;color: #fff;border-radius: 4px;padding: 10px;font-size: 16px;">
                        MEDICAL | VACCINATION
                    </div>
                </div>
                <?php if(@$documents['watch keeping']['ilo_medical'] == 1)
                {

                $private_document = false;
                $public_document = false;

                if (isset($user_uploaded_documents['ilo_medical'][0]['status']) && $user_uploaded_documents['ilo_medical'][0]['status'] == '1') {
                    $public_document = true;
                } else {
                    $private_document = true;
                }

                ?>


                <div class="row add-more-section" id="profile_yellow_fever">
                    <div class="col-xs-6 col-sm-7">
                        <span style="color:#47c4f0;float: left;"><i class="fa fa-check-circle-o"
                                                                    style="font-size: 22px;"></i></span>
                        <span style="margin-left: 10px;padding-top: 2px;float: left;color: #47c4f0;">ILO MEDICAL</span>
                    </div>
                    <div class="col-xs-6 col-sm-2 pull-right">
                        <?php if(count($user_uploaded_documents['ilo_medical'][0]['user_documents']) < 10)
                        {?>
                        <label><a href="javascript:;" class="btn btn-primary edit_doc pull-right"
                                  data-type='ilo_medical' data-current="menu9_" data-type-id="0">Add New
                                Image</a></label>
                        <?php }?>
                    </div>
                  

                    
                    <div class="col-xs-12 col-sm-12">
                        <?php $i = 1;$ilo_medical_count = 0; foreach($user_uploaded_documents['ilo_medical'][0]['user_documents'] as $arrAadhaar){ $ilo_medical_count++;?>
                        <div class="col-xs-12 col-sm-2">
                            <div class="avatar-upload">
                                <div class="avatar-delete delete_doc" data-id="<?php echo $arrAadhaar['id'];?>"
                                     data-imagename="<?php echo $arrAadhaar['document_path'];?>" data-type="ilo_medical"
                                     data-current="menu9_">
                                    <label for="imageUpload"></label>
                                </div>

                                <a class="lightbox" href="#preview-ilo_medical-<?php echo $i;?>">
                                    <div class="avatar-view" data-id="<?php echo $arrAadhaar['id'];?>"
                                         data-imagename="<?php echo $arrAadhaar['document_path'];?>"
                                         data-type="ilo_medical">

                                        <label for="imageUpload"></label>
                                    </div>
                                </a>
                                <?php if(isset($arrAadhaar['document_path']))
                                {
                                $storage_path = 'public/uploads/user_documents/';
                                $type = 'ilo_medical';
                                $url = env("AWS_URL") . $storage_path . $user_id . "/" . $type . "/" . $arrAadhaar['document_path'];?>

                                <div class="avatar-preview">
                                    <div class="avatar-edit-img" id="ilo_medical-<?php echo $i;?>">

                                        <img src="{{$url}}" class="img-thumbnail">

                                    </div>
                                    <div class="lightbox-target" id="preview-ilo_medical-<?php echo $i;?>">
                                        <img src="{{$url}}"/>
                                        <a class="lightbox-close" href="#ilo_medical-<?php echo $i;?>"></a>
                                    </div>

                                </div>
                                <?php }else{?>
                                <div class="avatar-preview">
                                    <div class="avatar-edit-img" id="ilo_medical-<?php echo $i;?>">
                                        <img src="{{url('public/assets/images/jpg.png')}}" data-action="zoom"
                                             class="pull-left" style="height: 130px;width:130px;">
                                    </div>
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                        <?php $i++;}

                        if($ilo_medical_count < 10){
                        for($j = $ilo_medical_count;$j < 10;$j++)
                        {?>
                        <div class="col-xs-12 col-sm-2" style="height: 170px;">
                            <div class="avatar-upload">


                                <div class="avatar-preview">
                                    <div class="avatar-edit-img" id="ilo_medical-<?php echo $ilo_medical_count + 1;?>">
                                        <img src="{{url('public/assets/images/jpg.png')}}" data-action="zoom"
                                             class="pull-left" style="height: 130px;width:130px;">
                                    </div>
                                </div>

                            </div>
                        </div>
                        <?php }}?>
                    </div>
                        <div class="col-xs-12 col-sm-4 visibility-type">

                            <div class="form-group pull-right">
                                <label class="radio-inline"><input type="radio"
                                                                   class="passport_show doc-permission-radio-btn"
                                                                   name="ilo_medical" data-name="ilo_medical"
                                                                   value="1" {{ $public_document ? 'checked=checked' : ''}}>Public
                                </label>
                                <label class="radio-inline"><input type="radio"
                                                                   class="passport_show doc-permission-radio-btn"
                                                                   name="ilo_medical" data-name="ilo_medical"
                                                                   value="0" {{ $private_document ? 'checked=checked' : ''}}>
                                    Private</label>
    
                            </div>
                        </div>

                </div>

                <?php } ?>

                <?php if(@$documents['watch keeping']['screening_test'] == 1)
                {

                $private_document = false;
                $public_document = false;

                if (isset($user_uploaded_documents['screening_test'][0]['status']) && $user_uploaded_documents['screening_test'][0]['status'] == '1') {
                    $public_document = true;
                } else {
                    $private_document = true;
                }

                ?>


                <div class="row add-more-section" id="profile_yellow_fever">
                    <div class="col-xs-6 col-sm-7">
                        <span style="color:#47c4f0;float: left;"><i class="fa fa-check-circle-o"
                                                                    style="font-size: 22px;"></i></span>
                        <span style="margin-left: 10px;padding-top: 2px;float: left;color: #47c4f0;">SCREENING TEST</span>
                    </div>
                    <div class="col-xs-6 col-sm-2 pull-right">
                        <?php if(count($user_uploaded_documents['screening_test'][0]['user_documents']) < 2)
                        {?>
                        <label><a href="javascript:;" class="btn btn-primary edit_doc pull-right"
                                  data-type='screening_test' data-current="menu9_" data-type-id="0">Add New
                                Image</a></label>
                        <?php }?>
                    </div>
                  

                    
                    <div class="col-xs-12 col-sm-12">
                        <?php $i = 1;$screening_test_count = 0; foreach($user_uploaded_documents['screening_test'][0]['user_documents'] as $arrAadhaar){ $screening_test_count++;?>
                        <div class="col-xs-12 col-sm-2">
                            <div class="avatar-upload">
                                <div class="avatar-delete delete_doc" data-id="<?php echo $arrAadhaar['id'];?>"
                                     data-imagename="<?php echo $arrAadhaar['document_path'];?>" data-type="screening_test"
                                     data-current="menu9_">
                                    <label for="imageUpload"></label>
                                </div>

                                <a class="lightbox" href="#preview-screening_test-<?php echo $i;?>">
                                    <div class="avatar-view" data-id="<?php echo $arrAadhaar['id'];?>"
                                         data-imagename="<?php echo $arrAadhaar['document_path'];?>"
                                         data-type="screening_test">

                                        <label for="imageUpload"></label>
                                    </div>
                                </a>
                                <?php if(isset($arrAadhaar['document_path']))
                                {
                                $storage_path = 'public/uploads/user_documents/';
                                $type = 'screening_test';
                                $url = env("AWS_URL") . $storage_path . $user_id . "/" . $type . "/" . $arrAadhaar['document_path'];?>

                                <div class="avatar-preview">
                                    <div class="avatar-edit-img" id="screening_test-<?php echo $i;?>">

                                        <img src="{{$url}}" class="img-thumbnail">

                                    </div>
                                    <div class="lightbox-target" id="preview-screening_test-<?php echo $i;?>">
                                        <img src="{{$url}}"/>
                                        <a class="lightbox-close" href="#screening_test-<?php echo $i;?>"></a>
                                    </div>

                                </div>
                                <?php }else{?>
                                <div class="avatar-preview">
                                    <div class="avatar-edit-img" id="screening_test-<?php echo $i;?>">
                                        <img src="{{url('public/assets/images/jpg.png')}}" data-action="zoom"
                                             class="pull-left" style="height: 130px;width:130px;">
                                    </div>
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                        <?php $i++;}

                        if($screening_test_count < 2){
                        for($j = $screening_test_count;$j < 2;$j++)
                        {?>
                        <div class="col-xs-12 col-sm-2">
                            <div class="avatar-upload">


                                <div class="avatar-preview">
                                    <div class="avatar-edit-img" id="screening_test-<?php echo $screening_test_count + 1;?>">
                                        <img src="{{url('public/assets/images/jpg.png')}}" data-action="zoom"
                                             class="pull-left" style="height: 130px;width:130px;">
                                    </div>
                                </div>

                            </div>
                        </div>
                        <?php }}?>
                    </div>
                        <div class="col-xs-12 col-sm-4 visibility-type">

                            <div class="form-group pull-right">
                                <label class="radio-inline"><input type="radio"
                                                                   class="passport_show doc-permission-radio-btn"
                                                                   name="screening_test" data-name="screening_test"
                                                                   value="1" {{ $public_document ? 'checked=checked' : ''}}>Public
                                </label>
                                <label class="radio-inline"><input type="radio"
                                                                   class="passport_show doc-permission-radio-btn"
                                                                   name="screening_test" data-name="screening_test"
                                                                   value="0" {{ $private_document ? 'checked=checked' : ''}}>
                                    Private</label>
    
                            </div>
                        </div>

                </div>

                <?php } ?>
                <?php if(@$documents['watch keeping']['yellow_fever'] == 1)
                {

                $private_document = false;
                $public_document = false;

                if (isset($user_uploaded_documents['yellow_fever'][0]['status']) && $user_uploaded_documents['yellow_fever'][0]['status'] == '1') {
                    $public_document = true;
                } else {
                    $private_document = true;
                }

                ?>


                <div class="row add-more-section" id="profile_yellow_fever">
                    <div class="col-xs-6 col-sm-7">
                        <span style="color:#47c4f0;float: left;"><i class="fa fa-check-circle-o"
                                                                    style="font-size: 22px;"></i></span>
                        <span style="margin-left: 10px;padding-top: 2px;float: left;color: #47c4f0;">VACCINATION - Yellow Fever</span>
                    </div>
                    <div class="col-xs-6 col-sm-2 pull-right">
                        <?php if(count($user_uploaded_documents['yellow_fever'][0]['user_documents']) < 2)
                        {?>
                        <label><a href="javascript:;" class="btn btn-primary edit_doc pull-right"
                                  data-type='yellow_fever' data-current="menu9_" data-type-id="0">Add New
                                Image</a></label>
                        <?php }?>
                    </div>
                   


                    <div class="col-xs-12 col-sm-12">
                        <?php $i = 1;$yellow_fever_count = 0; foreach($user_uploaded_documents['yellow_fever'][0]['user_documents'] as $arrAadhaar){ $yellow_fever_count++;?>
                        <div class="col-xs-12 col-sm-2">
                            <div class="avatar-upload">
                                <div class="avatar-delete delete_doc" data-id="<?php echo $arrAadhaar['id'];?>"
                                     data-imagename="<?php echo $arrAadhaar['document_path'];?>"
                                     data-type="yellow_fever" data-current="menu9_">
                                    <label for="imageUpload"></label>
                                </div>

                                <a class="lightbox" href="#preview-yellow_fever-<?php echo $i;?>">
                                    <div class="avatar-view" data-id="<?php echo $arrAadhaar['id'];?>"
                                         data-imagename="<?php echo $arrAadhaar['document_path'];?>"
                                         data-type="yellow_fever">

                                        <label for="imageUpload"></label>
                                    </div>
                                </a>
                                <?php if(isset($arrAadhaar['document_path']))
                                {
                                $storage_path = 'public/uploads/user_documents/';
                                $type = 'yellow_fever';
                                $url = env("AWS_URL") . $storage_path . $user_id . "/" . $type . "/" . $arrAadhaar['document_path'];?>

                                <div class="avatar-preview">
                                    <div class="avatar-edit-img" id="yellow_fever-<?php echo $i;?>">
                                        <img src="{{$url}}" class="img-thumbnail">
                                    </div>
                                    <div class="lightbox-target" id="preview-yellow_fever-<?php echo $i;?>">
                                        <img src="{{$url}}"/>
                                        <a class="lightbox-close" href="#yellow_fever-<?php echo $i;?>"></a>
                                    </div>

                                </div>
                                <?php }else{?>
                                <div class="avatar-preview">
                                    <div class="avatar-edit-img" id="yellow_fever-<?php echo $i;?>">
                                        <img src="{{url('public/assets/images/jpg.png')}}" data-action="zoom"
                                             class="pull-left" style="height: 130px;width:130px;">
                                    </div>
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                        <?php $i++;}

                        if($yellow_fever_count < 2){
                        for($j = $yellow_fever_count;$j < 2;$j++)
                        {?>
                        <div class="col-xs-12 col-sm-2">
                            <div class="avatar-upload">
                                <div class="avatar-preview">
                                    <div class="avatar-edit-img"
                                         id="yellow_fever-<?php echo $yellow_fever_count + 1;?>">
                                        <img src="{{url('public/assets/images/jpg.png')}}" data-action="zoom"
                                             class="pull-left" style="height: 130px;width:130px;">
                                    </div>
                                </div>

                            </div>
                        </div>
                        <?php }}?>
                    </div>
                        <div class="col-xs-12 col-sm-4 visibility-type">

                            <div class="form-group pull-right">
                                <label class="radio-inline"><input type="radio"
                                                                   class="passport_show doc-permission-radio-btn"
                                                                   name="yellow_fever" data-name="yellow_fever"
                                                                   value="1" {{ $public_document ? 'checked=checked' : ''}}>Public
                                </label>
                                <label class="radio-inline"><input type="radio"
                                                                   class="passport_show doc-permission-radio-btn"
                                                                   name="yellow_fever" data-name="yellow_fever"
                                                                   value="0" {{ $private_document ? 'checked=checked' : ''}}>
                                    Private</label>
    
                            </div>
                        </div>

                </div>

                <?php }?>

                <?php if(@$documents['watch keeping']['cholera'] == 1)
                {

                $private_document = false;
                $public_document = false;

                if (isset($user_uploaded_documents['cholera'][0]['status']) && $user_uploaded_documents['cholera'][0]['status'] == '1') {
                    $public_document = true;
                } else {
                    $private_document = true;
                }

                ?>


                <div class="row add-more-section" id="profile_yellow_fever">
                    <div class="col-xs-6 col-sm-7">
                        <span style="color:#47c4f0;float: left;"><i class="fa fa-check-circle-o"
                                                                    style="font-size: 22px;"></i></span>
                        <span style="margin-left: 10px;padding-top: 2px;float: left;color: #47c4f0;">VACCINATION - Cholera</span>
                    </div>
                    <div class="col-xs-6 col-sm-2 pull-right">
                        <?php if(count($user_uploaded_documents['cholera'][0]['user_documents']) < 2)
                        {?>
                        <label><a href="javascript:;" class="btn btn-primary edit_doc pull-right"
                                  data-type='cholera' data-current="menu9_" data-type-id="0">Add New
                                Image</a></label>
                        <?php }?>
                    </div>
                  

                    
                    <div class="col-xs-12 col-sm-12">
                        <?php $i = 1;$cholera_count = 0; foreach($user_uploaded_documents['cholera'][0]['user_documents'] as $arrAadhaar){ $cholera_count++;?>
                        <div class="col-xs-12 col-sm-2">
                            <div class="avatar-upload">
                                <div class="avatar-delete delete_doc" data-id="<?php echo $arrAadhaar['id'];?>"
                                     data-imagename="<?php echo $arrAadhaar['document_path'];?>" data-type="cholera"
                                     data-current="menu9_">
                                    <label for="imageUpload"></label>
                                </div>

                                <a class="lightbox" href="#preview-cholera-<?php echo $i;?>">
                                    <div class="avatar-view" data-id="<?php echo $arrAadhaar['id'];?>"
                                         data-imagename="<?php echo $arrAadhaar['document_path'];?>"
                                         data-type="cholera">

                                        <label for="imageUpload"></label>
                                    </div>
                                </a>
                                <?php if(isset($arrAadhaar['document_path']))
                                {
                                $storage_path = 'public/uploads/user_documents/';
                                $type = 'cholera';
                                $url = env("AWS_URL") . $storage_path . $user_id . "/" . $type . "/" . $arrAadhaar['document_path'];?>

                                <div class="avatar-preview">
                                    <div class="avatar-edit-img" id="cholera-<?php echo $i;?>">

                                        <img src="{{$url}}" class="img-thumbnail">

                                    </div>
                                    <div class="lightbox-target" id="preview-cholera-<?php echo $i;?>">
                                        <img src="{{$url}}"/>
                                        <a class="lightbox-close" href="#cholera-<?php echo $i;?>"></a>
                                    </div>

                                </div>
                                <?php }else{?>
                                <div class="avatar-preview">
                                    <div class="avatar-edit-img" id="cholera-<?php echo $i;?>">
                                        <img src="{{url('public/assets/images/jpg.png')}}" data-action="zoom"
                                             class="pull-left" style="height: 130px;width:130px;">
                                    </div>
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                        <?php $i++;}

                        if($cholera_count < 2){
                        for($j = $cholera_count;$j < 2;$j++)
                        {?>
                        <div class="col-xs-12 col-sm-2" >
                            <div class="avatar-upload">


                                <div class="avatar-preview">
                                    <div class="avatar-edit-img" id="cholera-<?php echo $cholera_count + 1;?>">
                                        <img src="{{url('public/assets/images/jpg.png')}}" data-action="zoom"
                                             class="pull-left" style="height: 130px;width:130px;">
                                    </div>
                                </div>

                            </div>
                        </div>
                        <?php }}?>
                    </div>
                        <div class="col-xs-12 col-sm-4 visibility-type">

                            <div class="form-group pull-right">
                                <label class="radio-inline"><input type="radio"
                                                                   class="passport_show doc-permission-radio-btn"
                                                                   name="cholera" data-name="cholera"
                                                                   value="1" {{ $public_document ? 'checked=checked' : ''}}>Public
                                </label>
                                <label class="radio-inline"><input type="radio"
                                                                   class="passport_show doc-permission-radio-btn"
                                                                   name="cholera" data-name="cholera"
                                                                   value="0" {{ $private_document ? 'checked=checked' : ''}}>
                                    Private</label>
    
                            </div>
                        </div>
                </div>

                <?php } ?>

                 <?php if(@$documents['watch keeping']['hepatitis_b'] == 1)
                {

                $private_document = false;
                $public_document = false;

                if (isset($user_uploaded_documents['hepatitis_b'][0]['status']) && $user_uploaded_documents['hepatitis_b'][0]['status'] == '1') {
                    $public_document = true;
                } else {
                    $private_document = true;
                }

                ?>


                <div class="row add-more-section" id="profile_yellow_fever">
                    <div class="col-xs-6 col-sm-7">
                        <span style="color:#47c4f0;float: left;"><i class="fa fa-check-circle-o"
                                                                    style="font-size: 22px;"></i></span>
                        <span style="margin-left: 10px;padding-top: 2px;float: left;color: #47c4f0;">VACCINATION - Hepatitis B</span>
                    </div>
                    <div class="col-xs-6 col-sm-2 pull-right">
                        <?php if(count($user_uploaded_documents['hepatitis_b'][0]['user_documents']) < 2)
                        {?>
                        <label><a href="javascript:;" class="btn btn-primary edit_doc pull-right"
                                  data-type='hepatitis_b' data-current="menu9_" data-type-id="0">Add New
                                Image</a></label>
                        <?php }?>
                    </div>
                  

                    
                    <div class="col-xs-12 col-sm-12">
                        <?php $i = 1;$hepatitis_b_count = 0; foreach($user_uploaded_documents['hepatitis_b'][0]['user_documents'] as $arrAadhaar){ $hepatitis_b_count++;?>
                        <div class="col-xs-12 col-sm-2">
                            <div class="avatar-upload">
                                <div class="avatar-delete delete_doc" data-id="<?php echo $arrAadhaar['id'];?>"
                                     data-imagename="<?php echo $arrAadhaar['document_path'];?>" data-type="hepatitis_b"
                                     data-current="menu9_">
                                    <label for="imageUpload"></label>
                                </div>

                                <a class="lightbox" href="#preview-hepatitis_b-<?php echo $i;?>">
                                    <div class="avatar-view" data-id="<?php echo $arrAadhaar['id'];?>"
                                         data-imagename="<?php echo $arrAadhaar['document_path'];?>"
                                         data-type="hepatitis_b">

                                        <label for="imageUpload"></label>
                                    </div>
                                </a>
                                <?php if(isset($arrAadhaar['document_path']))
                                {
                                $storage_path = 'public/uploads/user_documents/';
                                $type = 'hepatitis_b';
                                $url = env("AWS_URL") . $storage_path . $user_id . "/" . $type . "/" . $arrAadhaar['document_path'];?>

                                <div class="avatar-preview">
                                    <div class="avatar-edit-img" id="hepatitis_b-<?php echo $i;?>">

                                        <img src="{{$url}}" class="img-thumbnail">

                                    </div>
                                    <div class="lightbox-target" id="preview-hepatitis_b-<?php echo $i;?>">
                                        <img src="{{$url}}"/>
                                        <a class="lightbox-close" href="#hepatitis_b-<?php echo $i;?>"></a>
                                    </div>

                                </div>
                                <?php }else{?>
                                <div class="avatar-preview">
                                    <div class="avatar-edit-img" id="hepatitis_b-<?php echo $i;?>">
                                        <img src="{{url('public/assets/images/jpg.png')}}" data-action="zoom"
                                             class="pull-left" style="height: 130px;width:130px;">
                                    </div>
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                        <?php $i++;}

                        if($hepatitis_b_count < 2){
                        for($j = $hepatitis_b_count;$j < 2;$j++)
                        {?>
                        <div class="col-xs-12 col-sm-2" >
                            <div class="avatar-upload">


                                <div class="avatar-preview">
                                    <div class="avatar-edit-img" id="hepatitis_b-<?php echo $hepatitis_b_count + 1;?>">
                                        <img src="{{url('public/assets/images/jpg.png')}}" data-action="zoom"
                                             class="pull-left" style="height: 130px;width:130px;">
                                    </div>
                                </div>

                            </div>
                        </div>
                        <?php }}?>
                    </div>
                        <div class="col-xs-12 col-sm-4 visibility-type">

                            <div class="form-group pull-right">
                                <label class="radio-inline"><input type="radio"
                                                                   class="passport_show doc-permission-radio-btn"
                                                                   name="hepatitis_b" data-name="hepatitis_b"
                                                                   value="1" {{ $public_document ? 'checked=checked' : ''}}>Public
                                </label>
                                <label class="radio-inline"><input type="radio"
                                                                   class="passport_show doc-permission-radio-btn"
                                                                   name="hepatitis_b" data-name="hepatitis_b"
                                                                   value="0" {{ $private_document ? 'checked=checked' : ''}}>
                                    Private</label>
    
                            </div>
                        </div>
                </div>

                <?php } ?>
                <?php if(@$documents['watch keeping']['hepatitis_c'] == 1)
                {

                $private_document = false;
                $public_document = false;

                if (isset($user_uploaded_documents['hepatitis_c'][0]['status']) && $user_uploaded_documents['hepatitis_c'][0]['status'] == '1') {
                    $public_document = true;
                } else {
                    $private_document = true;
                }

                ?>


                <div class="row add-more-section" id="profile_yellow_fever">
                    <div class="col-xs-6 col-sm-7">
                        <span style="color:#47c4f0;float: left;"><i class="fa fa-check-circle-o"
                                                                    style="font-size: 22px;"></i></span>
                        <span style="margin-left: 10px;padding-top: 2px;float: left;color: #47c4f0;">VACCINATION - Hepatitis C</span>
                    </div>
                    <div class="col-xs-6 col-sm-2 pull-right">
                        <?php if(count($user_uploaded_documents['hepatitis_c'][0]['user_documents']) < 2)
                        {?>
                        <label><a href="javascript:;" class="btn btn-primary edit_doc pull-right"
                                  data-type='hepatitis_c' data-current="menu9_" data-type-id="0">Add New
                                Image</a></label>
                        <?php }?>
                    </div>
                  

                    
                    <div class="col-xs-12 col-sm-12">
                        <?php $i = 1;$hepatitis_c_count = 0; foreach($user_uploaded_documents['hepatitis_c'][0]['user_documents'] as $arrAadhaar){ $hepatitis_c_count++;?>
                        <div class="col-xs-12 col-sm-2">
                            <div class="avatar-upload">
                                <div class="avatar-delete delete_doc" data-id="<?php echo $arrAadhaar['id'];?>"
                                     data-imagename="<?php echo $arrAadhaar['document_path'];?>" data-type="hepatitis_c"
                                     data-current="menu9_">
                                    <label for="imageUpload"></label>
                                </div>

                                <a class="lightbox" href="#preview-hepatitis_c-<?php echo $i;?>">
                                    <div class="avatar-view" data-id="<?php echo $arrAadhaar['id'];?>"
                                         data-imagename="<?php echo $arrAadhaar['document_path'];?>"
                                         data-type="hepatitis_c">

                                        <label for="imageUpload"></label>
                                    </div>
                                </a>
                                <?php if(isset($arrAadhaar['document_path']))
                                {
                                $storage_path = 'public/uploads/user_documents/';
                                $type = 'hepatitis_c';
                                $url = env("AWS_URL") . $storage_path . $user_id . "/" . $type . "/" . $arrAadhaar['document_path'];?>

                                <div class="avatar-preview">
                                    <div class="avatar-edit-img" id="hepatitis_c-<?php echo $i;?>">

                                        <img src="{{$url}}" class="img-thumbnail">

                                    </div>
                                    <div class="lightbox-target" id="preview-hepatitis_c-<?php echo $i;?>">
                                        <img src="{{$url}}"/>
                                        <a class="lightbox-close" href="#hepatitis_c-<?php echo $i;?>"></a>
                                    </div>

                                </div>
                                <?php }else{?>
                                <div class="avatar-preview">
                                    <div class="avatar-edit-img" id="hepatitis_c-<?php echo $i;?>">
                                        <img src="{{url('public/assets/images/jpg.png')}}" data-action="zoom"
                                             class="pull-left" style="height: 130px;width:130px;">
                                    </div>
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                        <?php $i++;}

                        if($hepatitis_c_count < 2){
                        for($j = $hepatitis_c_count;$j < 2;$j++)
                        {?>
                        <div class="col-xs-12 col-sm-2" >
                            <div class="avatar-upload">


                                <div class="avatar-preview">
                                    <div class="avatar-edit-img" id="hepatitis_c-<?php echo $hepatitis_c_count + 1;?>">
                                        <img src="{{url('public/assets/images/jpg.png')}}" data-action="zoom"
                                             class="pull-left" style="height: 130px;width:130px;">
                                    </div>
                                </div>

                            </div>
                        </div>
                        <?php }}?>
                    </div>
                        <div class="col-xs-12 col-sm-4 visibility-type">

                            <div class="form-group pull-right">
                                <label class="radio-inline"><input type="radio"
                                                                   class="passport_show doc-permission-radio-btn"
                                                                   name="hepatitis_c" data-name="hepatitis_c"
                                                                   value="1" {{ $public_document ? 'checked=checked' : ''}}>Public
                                </label>
                                <label class="radio-inline"><input type="radio"
                                                                   class="passport_show doc-permission-radio-btn"
                                                                   name="hepatitis_c" data-name="hepatitis_c"
                                                                   value="0" {{ $private_document ? 'checked=checked' : ''}}>
                                    Private</label>
    
                            </div>
                        </div>
                </div>

                <?php } ?>

                <?php if(@$documents['watch keeping']['diphtheria'] == 1)
                {

                $private_document = false;
                $public_document = false;

                if (isset($user_uploaded_documents['diphtheria'][0]['status']) && $user_uploaded_documents['diphtheria'][0]['status'] == '1') {
                    $public_document = true;
                } else {
                    $private_document = true;
                }

                ?>


                <div class="row add-more-section" id="profile_yellow_fever">
                    <div class="col-xs-6 col-sm-7">
                        <span style="color:#47c4f0;float: left;"><i class="fa fa-check-circle-o"
                                                                    style="font-size: 22px;"></i></span>
                        <span style="margin-left: 10px;padding-top: 2px;float: left;color: #47c4f0;">VACCINATION - Diphtheria and Tenanus</span>
                    </div>
                    <div class="col-xs-6 col-sm-2 pull-right">
                        <?php if(count($user_uploaded_documents['diphtheria'][0]['user_documents']) < 2)
                        {?>
                        <label><a href="javascript:;" class="btn btn-primary edit_doc pull-right"
                                  data-type='diphtheria' data-current="menu9_" data-type-id="0">Add New
                                Image</a></label>
                        <?php }?>
                    </div>
                  

                    
                    <div class="col-xs-12 col-sm-12">
                        <?php $i = 1;$diphtheria_count = 0; foreach($user_uploaded_documents['diphtheria'][0]['user_documents'] as $arrAadhaar){ $diphtheria_count++;?>
                        <div class="col-xs-12 col-sm-2">
                            <div class="avatar-upload">
                                <div class="avatar-delete delete_doc" data-id="<?php echo $arrAadhaar['id'];?>"
                                     data-imagename="<?php echo $arrAadhaar['document_path'];?>" data-type="diphtheria"
                                     data-current="menu9_">
                                    <label for="imageUpload"></label>
                                </div>

                                <a class="lightbox" href="#preview-diphtheria-<?php echo $i;?>">
                                    <div class="avatar-view" data-id="<?php echo $arrAadhaar['id'];?>"
                                         data-imagename="<?php echo $arrAadhaar['document_path'];?>"
                                         data-type="diphtheria">

                                        <label for="imageUpload"></label>
                                    </div>
                                </a>
                                <?php if(isset($arrAadhaar['document_path']))
                                {
                                $storage_path = 'public/uploads/user_documents/';
                                $type = 'diphtheria';
                                $url = env("AWS_URL") . $storage_path . $user_id . "/" . $type . "/" . $arrAadhaar['document_path'];?>

                                <div class="avatar-preview">
                                    <div class="avatar-edit-img" id="diphtheria-<?php echo $i;?>">

                                        <img src="{{$url}}" class="img-thumbnail">

                                    </div>
                                    <div class="lightbox-target" id="preview-diphtheria-<?php echo $i;?>">
                                        <img src="{{$url}}"/>
                                        <a class="lightbox-close" href="#diphtheria-<?php echo $i;?>"></a>
                                    </div>

                                </div>
                                <?php }else{?>
                                <div class="avatar-preview">
                                    <div class="avatar-edit-img" id="diphtheria-<?php echo $i;?>">
                                        <img src="{{url('public/assets/images/jpg.png')}}" data-action="zoom"
                                             class="pull-left" style="height: 130px;width:130px;">
                                    </div>
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                        <?php $i++;}

                        if($diphtheria_count < 2){
                        for($j = $diphtheria_count;$j < 2;$j++)
                        {?>
                        <div class="col-xs-12 col-sm-2" >
                            <div class="avatar-upload">


                                <div class="avatar-preview">
                                    <div class="avatar-edit-img" id="diphtheria-<?php echo $diphtheria_count + 1;?>">
                                        <img src="{{url('public/assets/images/jpg.png')}}" data-action="zoom"
                                             class="pull-left" style="height: 130px;width:130px;">
                                    </div>
                                </div>

                            </div>
                        </div>
                        <?php }}?>
                    </div>
                        <div class="col-xs-12 col-sm-4 visibility-type">

                            <div class="form-group pull-right">
                                <label class="radio-inline"><input type="radio"
                                                                   class="passport_show doc-permission-radio-btn"
                                                                   name="diphtheria" data-name="diphtheria"
                                                                   value="1" {{ $public_document ? 'checked=checked' : ''}}>Public
                                </label>
                                <label class="radio-inline"><input type="radio"
                                                                   class="passport_show doc-permission-radio-btn"
                                                                   name="diphtheria" data-name="diphtheria"
                                                                   value="0" {{ $private_document ? 'checked=checked' : ''}}>
                                    Private</label>
    
                            </div>
                        </div>
                </div>

                <?php } ?>

                <?php if(@$documents['watch keeping']['covid'] == 1)
                {

                $private_document = false;
                $public_document = false;

                if (isset($user_uploaded_documents['covid'][0]['status']) && $user_uploaded_documents['covid'][0]['status'] == '1') {
                    $public_document = true;
                } else {
                    $private_document = true;
                }

                ?>


                <div class="row add-more-section" id="profile_yellow_fever">
                    <div class="col-xs-6 col-sm-7">
                        <span style="color:#47c4f0;float: left;"><i class="fa fa-check-circle-o"
                                                                    style="font-size: 22px;"></i></span>
                        <span style="margin-left: 10px;padding-top: 2px;float: left;color: #47c4f0;">VACCINATION - Covid 19</span>
                    </div>
                    <div class="col-xs-6 col-sm-2 pull-right">
                        <?php if(count($user_uploaded_documents['covid'][0]['user_documents']) < 2)
                        {?>
                        <label><a href="javascript:;" class="btn btn-primary edit_doc pull-right"
                                  data-type='covid' data-current="menu9_" data-type-id="0">Add New
                                Image</a></label>
                        <?php }?>
                    </div>
                  

                    
                    <div class="col-xs-12 col-sm-12">
                        <?php $i = 1;$covid_count = 0; foreach($user_uploaded_documents['covid'][0]['user_documents'] as $arrAadhaar){ $covid_count++;?>
                        <div class="col-xs-12 col-sm-2">
                            <div class="avatar-upload">
                                <div class="avatar-delete delete_doc" data-id="<?php echo $arrAadhaar['id'];?>"
                                     data-imagename="<?php echo $arrAadhaar['document_path'];?>" data-type="covid"
                                     data-current="menu9_">
                                    <label for="imageUpload"></label>
                                </div>

                                <a class="lightbox" href="#preview-covid-<?php echo $i;?>">
                                    <div class="avatar-view" data-id="<?php echo $arrAadhaar['id'];?>"
                                         data-imagename="<?php echo $arrAadhaar['document_path'];?>"
                                         data-type="covid">

                                        <label for="imageUpload"></label>
                                    </div>
                                </a>
                                <?php if(isset($arrAadhaar['document_path']))
                                {
                                $storage_path = 'public/uploads/user_documents/';
                                $type = 'covid';
                                $url = env("AWS_URL") . $storage_path . $user_id . "/" . $type . "/" . $arrAadhaar['document_path'];?>

                                <div class="avatar-preview">
                                    <div class="avatar-edit-img" id="covid-<?php echo $i;?>">

                                        <img src="{{$url}}" class="img-thumbnail">

                                    </div>
                                    <div class="lightbox-target" id="preview-covid-<?php echo $i;?>">
                                        <img src="{{$url}}"/>
                                        <a class="lightbox-close" href="#covid-<?php echo $i;?>"></a>
                                    </div>

                                </div>
                                <?php }else{?>
                                <div class="avatar-preview">
                                    <div class="avatar-edit-img" id="covid-<?php echo $i;?>">
                                        <img src="{{url('public/assets/images/jpg.png')}}" data-action="zoom"
                                             class="pull-left" style="height: 130px;width:130px;">
                                    </div>
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                        <?php $i++;}

                        if($covid_count < 2){
                        for($j = $covid_count;$j < 2;$j++)
                        {?>
                        <div class="col-xs-12 col-sm-2" >
                            <div class="avatar-upload">


                                <div class="avatar-preview">
                                    <div class="avatar-edit-img" id="covid-<?php echo $covid_count + 1;?>">
                                        <img src="{{url('public/assets/images/jpg.png')}}" data-action="zoom"
                                             class="pull-left" style="height: 130px;width:130px;">
                                    </div>
                                </div>

                            </div>
                        </div>
                        <?php }}?>
                    </div>
                        <div class="col-xs-12 col-sm-4 visibility-type">

                            <div class="form-group pull-right">
                                <label class="radio-inline"><input type="radio"
                                                                   class="passport_show doc-permission-radio-btn"
                                                                   name="covid" data-name="covid"
                                                                   value="1" {{ $public_document ? 'checked=checked' : ''}}>Public
                                </label>
                                <label class="radio-inline"><input type="radio"
                                                                   class="passport_show doc-permission-radio-btn"
                                                                   name="covid" data-name="covid"
                                                                   value="0" {{ $private_document ? 'checked=checked' : ''}}>
                                    Private</label>
    
                            </div>
                        </div>
                </div>

                <?php } ?>
            </div>
            <div class="tab-pane" role="tabpanel" id="menu10">
                <div class="row" style="margin-left: 1px;margin-top: 10px;">
                    <div class="col-sm-12"
                         style="background: #2a55a2;color: #fff;border-radius: 4px;padding: 10px;font-size: 16px;">
                        ACADEMICS
                    </div>
                </div>
                <?php if($documents['professional_detail']['institute_degree']){?>
                <div class="row add-more-section">
                    <div class="col-xs-6 col-sm-7">
                        <span style="color:#47c4f0;float: left;"><i class="fa fa-check-circle-o"
                                                                    style="font-size: 22px;"></i></span>
                        <span style="margin-left: 10px;padding-top: 2px;float: left;color: #47c4f0;">Course : PRE SEA - <?php echo $documents['professional_detail']['institute_degree'];?></span>
                    </div>
                    <div class="col-sm-3"></div>
                    <div class="col-xs-6 col-sm-2" style="text-align:right;">
                        <?php if(count($user_uploaded_documents['institute_degree'][0]['user_documents']) < 5)
                        {?>
                        <label><a href="javascript:;" class="btn btn-primary edit_doc pull-right"
                                  data-type='institute_degree' data-current="menu10_" data-type-id="0">Add New Image</a></label>
                        <?php }?>
                    </div>
                    
                    <div class="col-xs-12 col-sm-12">
                        <?php $i = 1;$institute_degree_count = 0; foreach($user_uploaded_documents['institute_degree'][0]['user_documents'] as $arrAadhaar){ $institute_degree_count++;?>
                        <div class="col-xs-12 col-sm-2">
                            <div class="avatar-upload">
                                <div class="avatar-delete delete_doc" data-id="<?php echo $arrAadhaar['id'];?>"
                                     data-imagename="<?php echo $arrAadhaar['document_path'];?>"
                                     data-type="institute_degree" data-current="menu10_">
                                    <label for="imageUpload"></label>
                                </div>

                                <a class="lightbox" href="#preview-institute_degree-<?php echo $i;?>">
                                    <div class="avatar-view" data-id="<?php echo $arrAadhaar['id'];?>"
                                         data-imagename="<?php echo $arrAadhaar['document_path'];?>"
                                         data-type="institute_degree">

                                        <label for="imageUpload"></label>
                                    </div>
                                </a>
                                <?php if(isset($arrAadhaar['document_path']))
                                {
                                $storage_path = 'public/uploads/user_documents/';
                                $type = 'institute_degree';
                                $url = env("AWS_URL") . $storage_path . $user_id . "/" . $type . "/" . $arrAadhaar['document_path'];
                                ?>
                                <div class="avatar-preview">
                                    <div class="avatar-edit-img" id="institute_degree-<?php echo $i;?>">

                                        <img src="{{$url}}" class="img-thumbnail">

                                    </div>
                                    <div class="lightbox-target" id="preview-institute_degree-<?php echo $i;?>">
                                        <img src="{{$url}}"/>
                                        <a class="lightbox-close" href="#institute_degree-<?php echo $i;?>"></a>
                                    </div>

                                </div>
                                <?php }else{?>
                                <div class="avatar-preview">
                                    <div class="avatar-edit-img" id="institute_degree-<?php echo $i;?>">
                                        <img src="{{url('public/assets/images/jpg.png')}}" data-action="zoom"
                                             class="pull-left" style="height: 130px;width:130px;">
                                    </div>
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                        <?php $i++;}

                        if($institute_degree_count < 5){
                        for($j = $institute_degree_count;$j < 5;$j++)
                        {?>
                        <div class="col-xs-12 col-sm-2">
                            <div class="avatar-upload">
                                <div class="avatar-preview">
                                    <div class="avatar-edit-img"
                                         id="institute_degree-<?php echo $institute_degree_count + 1;?>">
                                        <img src="{{url('public/assets/images/jpg.png')}}" data-action="zoom"
                                             class="pull-left" style="height: 130px;width:130px;">
                                    </div>
                                </div>

                            </div>
                        </div>
                        <?php }}?>
                    </div>
                        <div class="col-xs-12 col-sm-4 visibility-type">
                            <div class="form-group pull-right">
                                @php
                                    if($user_uploaded_documents['institute_degree'][0]['status']==1) $_status=1;
                                    else $_status=0;
                                @endphp
                                <label class="radio-inline"><input type="radio"
                                                                   class="institute_degree doc-permission-radio-btn"
                                                                   name="pre_sea_show" data-name="institute_degree"
                                                                   value="1" {{($_status==1)?'checked=checked':''}}>Public
                                </label>
                                <label class="radio-inline"><input type="radio"
                                                                   class="institute_degree doc-permission-radio-btn"
                                                                   name="pre_sea_show" data-name="institute_degree"
                                                                   value="0" {{($_status==1)?'':'checked=checked'}}> Private</label>
                            </div>
                        </div>
                </div>
                <?php } ?>
                @if ($documents['professional_detail']['school_qualification']!='')
                    <div class="row add-more-section">
                        <div class="col-xs-6 col-sm-7">
                            <span style="color:#47c4f0;float: left;"><i class="fa fa-check-circle-o"
                                                                        style="font-size: 22px;"></i></span>
                            <span style="margin-left: 10px;padding-top: 2px;float: left;color: #47c4f0;">ACADEMICS - <?php echo $documents['professional_detail']['school_qualification'];?></span>
                        </div>
                        <div class="col-sm-3"></div>
                        <div class="col-xs-6 col-sm-2" style="text-align:right;">
                            <?php if(count($user_uploaded_documents['school_qualification'][0]['user_documents']) < 5)
                            {?>
                            <label><a href="javascript:;" class="btn btn-primary edit_doc pull-right"
                                      data-type='school_qualification' data-current="menu10_" data-type-id="0">Add New
                                    Image</a></label>
                            <?php }?>
                        </div>
                        <div class="col-sm-12">
                            <div class="col-xs-12 col-sm-12">
                                <?php $i = 1;$school_count = 0; foreach($user_uploaded_documents['school_qualification'][0]['user_documents'] as $arrSchool){ $school_count++;?>
                                <div class="col-xs-12 col-sm-2">
                                    <div class="avatar-upload">
                                        <div class="avatar-delete delete_doc" data-id="<?php echo $arrSchool['id'];?>"
                                             data-imagename="<?php echo $arrSchool['document_path'];?>"
                                             data-type="school_qualification" data-current="menu10_">
                                            <label for="imageUpload"></label>
                                        </div>
                                        <a class="lightbox" href="#preview-school_qualification-<?php echo $i;?>">
                                            <div class="avatar-view" data-id="<?php echo $arrSchool['id'];?>"
                                                 data-imagename="<?php echo $arrSchool['document_path'];?>"
                                                 data-type="school_qualification">

                                                <label for="imageUpload"></label>
                                            </div>
                                        </a>
                                        <?php if(isset($arrSchool['document_path']))
                                        {
                                        $storage_path = 'public/uploads/user_documents/';
                                        $type = 'school_qualification';
                                        $url = env("AWS_URL") . $storage_path . $user_id . "/" . $type . "/" . $arrSchool['document_path'];?>

                                        <div class="avatar-preview">
                                            <div class="avatar-edit-img" id="school_qualification-<?php echo $i;?>">

                                                <img src="{{$url}}" class="img-thumbnail">
                                            </div>
                                            <div class="lightbox-target"
                                                 id="preview-school_qualification-<?php echo $i;?>">
                                                <img src="{{$url}}"/>
                                                <a class="lightbox-close"
                                                   href="#school_qualification-<?php echo $i;?>"></a>
                                            </div>
                                        </div>
                                        <?php }else{?>
                                        <div class="avatar-preview">
                                            <div class="avatar-edit-img" id="school_qualification-<?php echo $i;?>">
                                                <img src="{{url('public/assets/images/jpg.png')}}" data-action="zoom"
                                                     class="pull-left" style="height: 130px;width:130px;">
                                            </div>
                                        </div>
                                        <?php } ?>
                                    </div>
                                </div>
                                <?php $i++;}

                                if($school_count < 5){
                                for($j = $school_count;$j < 5;$j++)
                                {?>
                                <div class="col-xs-12 col-sm-2">
                                    <div class="avatar-upload">
                                        <div class="avatar-preview">
                                            <div class="avatar-edit-img"
                                                 id="school_qualification-<?php echo $school_count + 1;?>">
                                                <img src="{{url('public/assets/images/jpg.png')}}" data-action="zoom"
                                                     class="pull-left" style="height: 130px;width:130px;">
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <?php }}?>
                            </div>
                                <div class="col-xs-12 col-sm-4 visibility-type">
                                    <div class="form-group pull-right">
                                        @php
                                            if($user_uploaded_documents['school_qualification'][0]['status']==1) $school_qualification_status=1;
                                            else $school_qualification_status=0;
                                        @endphp
                                        <label class="radio-inline"><input type="radio"
                                                                           class="academics doc-permission-radio-btn"
                                                                           name="academics_show"
                                                                           data-name="school_qualification"
                                                                           value="1" {{($school_qualification_status==1)?'checked=checked':''}}>Public
                                        </label>
                                        <label class="radio-inline"><input type="radio"
                                                                           class="academics doc-permission-radio-btn"
                                                                           name="academics_show"
                                                                           data-name="school_qualification"
                                                                           value="0" {{($school_qualification_status==1)?'':'checked=checked'}}>
                                            Private</label>
                                    </div>
                                </div>
                        </div>
                    </div>
                @endif

                

            </div>
            <div class="tab-pane" role="tabpanel" id="menu11">
                <div class="row" style="margin-left: 1px;margin-top: 10px;">
                    <div class="col-sm-12"
                         style="background: #2a55a2;color: #fff;border-radius: 4px;padding: 10px;font-size: 16px;">
                        ID
                    </div>
                </div>

                <?php if(@$documents['watch keeping']['character_reference'] == 1)
                {?>
                <div class="row add-more-section">
                    <div class="col-xs-6 col-sm-7">
                        <span style="color:#47c4f0;float: left;"><i class="fa fa-check-circle-o"
                                                                    style="font-size: 22px;"></i></span>
                        <span style="margin-left: 10px;padding-top: 2px;float: left;color: #47c4f0;">CHARACTER CERTIFICATE</span>
                    </div>
                    <div class="col-sm-3"></div>
                    <div class="col-xs-6 col-sm-2" style="text-align:right;">
                        <?php if(count($user_uploaded_documents['character_reference'][0]['user_documents']) < 1)
                        {?>
                        <label><a href="javascript:;" class="btn btn-primary edit_doc pull-right"
                                  data-type='character_reference' data-current="menu11_" data-type-id="0">Add New
                                Image</a></label>
                        <?php }?>
                    </div>
                    <div class="col-xs-12 col-sm-12">
                        <?php $i = 1;$cc_count = 0; foreach($user_uploaded_documents['character_reference'][0]['user_documents'] as $arrSchool){ $cc_count++;?>
                        <div class="col-xs-12 col-sm-2">
                            <div class="avatar-upload">
                                <div class="avatar-delete delete_doc" data-id="<?php echo $arrSchool['id'];?>"
                                     data-imagename="<?php echo $arrSchool['document_path'];?>"
                                     data-type="character_reference" data-current="menu11_">
                                    <label for="imageUpload"></label>
                                </div>

                                <a class="lightbox" href="#preview-character_reference-<?php echo $i;?>">
                                    <div class="avatar-view" data-id="<?php echo $arrSchool['id'];?>"
                                         data-imagename="<?php echo $arrSchool['document_path'];?>"
                                         data-type="character_reference">
                                        <label for="imageUpload"></label>
                                    </div>
                                </a>
                                <?php if(isset($arrSchool['document_path']))
                                {
                                $storage_path = 'public/uploads/user_documents/';
                                $type = 'character_reference';
                                $url = env("AWS_URL") . $storage_path . $user_id . "/" . $type . "/" . $arrSchool['document_path'];?>

                                <div class="avatar-preview">
                                    <div class="avatar-edit-img" id="character_reference-<?php echo $i;?>">

                                        <img src="{{$url}}" class="img-thumbnail">

                                    </div>
                                    <div class="lightbox-target" id="preview-character_reference-<?php echo $i;?>">
                                        <img src="{{$url}}"/>
                                        <a class="lightbox-close" href="#character_reference-<?php echo $i;?>"></a>
                                    </div>

                                </div>
                                <?php }else{?>
                                <div class="avatar-preview">
                                    <div class="avatar-edit-img" id="character_reference-<?php echo $i;?>">
                                        <img src="{{url('public/assets/images/jpg.png')}}" data-action="zoom"
                                             class="pull-left" style="height: 130px;width:130px;">
                                    </div>
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                        <?php $i++;}

                        if($cc_count < 1){
                        for($j = $cc_count;$j < 1;$j++)
                        {?>
                        <div class="col-xs-12 col-sm-2">
                            <div class="avatar-upload">
                                <div class="avatar-preview">
                                    <div class="avatar-edit-img" id="character_reference-<?php echo $cc_count + 1;?>">
                                        <img src="{{url('public/assets/images/jpg.png')}}" data-action="zoom"
                                             class="pull-left" style="height: 130px;width:130px;">
                                    </div>
                                </div>

                            </div>
                        </div>
                        <?php }}?>
                    </div>
                        <div class="col-xs-12 col-sm-4 visibility-type">
                            <div class="form-group pull-right">
                                @php
                                    if($user_uploaded_documents['character_reference'][0]['status']==1) $character_reference_status=1;
                                         else $character_reference_status=0;
                                @endphp
                                <label class="radio-inline"><input type="radio"
                                                                   class="character_reference doc-permission-radio-btn"
                                                                   name="character_reference_show"
                                                                   data-name="character_reference"
                                                                   value="1" {{($character_reference_status==1)?'checked=checked':''}}>Public
                                </label>
                                <label class="radio-inline"><input type="radio"
                                                                   class="character_reference doc-permission-radio-btn"
                                                                   name="character_reference_show"
                                                                   data-name="character_reference"
                                                                   value="0" {{($character_reference_status==1)?'':'checked=checked'}}>
                                    Private</label>
                            </div>
                        </div>


                </div>
                <?php }?>
                <?php if($documents['personal_detail']['pancard'])
                {
                $private_document = false;
                $public_document = false;

                if (isset($user_uploaded_documents['pancard'][0]['status']) && $user_uploaded_documents['pancard'][0]['status'] == '1') {
                    $public_document = true;
                } else {
                    $private_document = true;
                }

                $hide = '';
                if (isset($value['hide'])) {
                    $hide = 'hide';
                    $active_class = '';
                }

                ?>


                <div class="row add-more-section" id="profile_pancard">
                    <div class="col-xs-6 col-sm-7">
                        <span style="color:#47c4f0;float: left;"><i class="fa fa-check-circle-o"
                                                                    style="font-size: 22px;"></i></span>
                        <span style="margin-left: 10px;padding-top: 2px;float: left;color: #47c4f0;">PANCARD</span>
                    </div>
                    <div class="col-sm-3"></div>
                    <div class="col-xs-6 col-sm-2">
                        <?php if(count($user_uploaded_documents['pancard'][0]['user_documents']) < 2)
                        {?>
                        <label><a href="javascript:;" class="btn btn-primary edit_doc pull-right" data-type='pancard'
                                  data-current="menu11_" data-type-id="0">Add New Image</a></label>
                        <?php }?>
                    </div>
                 


                    <div class="col-xs-12 col-sm-12">
                        <?php $i = 1;$pancard_count = 0; foreach($user_uploaded_documents['pancard'][0]['user_documents'] as $arrAadhaar){ $pancard_count++;?>
                        <div class="col-xs-12 col-sm-2">
                            <div class="avatar-upload">
                                <div class="avatar-delete delete_doc" data-id="<?php echo $arrAadhaar['id'];?>"
                                     data-imagename="<?php echo $arrAadhaar['document_path'];?>" data-type="pancard"
                                     data-current="menu11_">
                                    <label for="imageUpload"></label>
                                </div>

                                <a class="lightbox" href="#preview-pancard-<?php echo $i;?>">
                                    <div class="avatar-view" data-id="<?php echo $arrAadhaar['id'];?>"
                                         data-imagename="<?php echo $arrAadhaar['document_path'];?>"
                                         data-type="pancard">

                                        <label for="imageUpload"></label>
                                    </div>
                                </a>
                                <?php if(isset($arrAadhaar['document_path']))
                                {
                                $storage_path = 'public/uploads/user_documents/';
                                $type = 'pancard';
                                $url = env("AWS_URL") . $storage_path . $user_id . "/" . $type . "/" . $arrAadhaar['document_path'];?>

                                <div class="avatar-preview">
                                    <div class="avatar-edit-img" id="pancard-<?php echo $i;?>">

                                        <img src="{{$url}}" class="img-thumbnail">

                                    </div>
                                    <div class="lightbox-target" id="preview-pancard-<?php echo $i;?>">
                                        <img src="{{$url}}"/>
                                        <a class="lightbox-close" href="#pancard-<?php echo $i;?>"></a>
                                    </div>

                                </div>
                                <?php }else{?>
                                <div class="avatar-preview">
                                    <div class="avatar-edit-img" id="pancard-<?php echo $i;?>">
                                        <img src="{{url('public/assets/images/jpg.png')}}" data-action="zoom"
                                             class="pull-left" style="height: 130px;width:130px;">
                                    </div>
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                        <?php $i++;}

                        if($pancard_count < 2){
                        for($j = $pancard_count;$j < 2;$j++)
                        {?>
                        <div class="col-xs-12 col-sm-2">
                            <div class="avatar-upload">


                                <div class="avatar-preview">
                                    <div class="avatar-edit-img" id="pancard-<?php echo $pancard_count + 1;?>">
                                        <img src="{{url('public/assets/images/jpg.png')}}" data-action="zoom"
                                             class="pull-left" style="height: 130px;width:130px;">
                                    </div>
                                </div>

                            </div>
                        </div>
                        <?php }}?>
                    </div>
                        <div class="col-xs-12 col-sm-4 visibility-type">

                            <div class="form-group pull-right">
                                <label class="radio-inline"><input type="radio"
                                                                   class="pancard_show doc-permission-radio-btn"
                                                                   name="pancard" data-name="pancard"
                                                                   value="1" {{ $public_document ? 'checked=checked' : ''}}>Public
                                </label>
                                <label class="radio-inline"><input type="radio"
                                                                   class="pancard_show doc-permission-radio-btn"
                                                                   name="pancard" data-name="pancard"
                                                                   value="0" {{ $private_document ? 'checked=checked' : ''}}>
                                    Private</label>
    
                            </div>
                        </div>

                </div>

                <?php }?>
                <?php
                if($documents['personal_detail']['aadhaar'])
                {
                $private_document = false;
                $public_document = false;

                if (isset($user_uploaded_documents['aadhaar'][0]['status']) && $user_uploaded_documents['aadhaar'][0]['status'] == '1') {
                    $public_document = true;
                } else {
                    $private_document = true;
                }

                ?>

                <div class="row add-more-section" id="profile_aadhaar">
                    <div class="col-xs-6 col-sm-7">
                        <span style="color:#47c4f0;float: left;"><i class="fa fa-check-circle-o"
                                                                    style="font-size: 22px;"></i></span>
                        <span style="margin-left: 10px;padding-top: 2px;float: left;color: #47c4f0;">AADHAAR CARD</span>
                    </div>
                    <div class="col-xs-6 col-sm-2 pull-right">
                        <?php if(count($user_uploaded_documents['aadhaar'][0]['user_documents']) < 2)
                        {?>
                        <label><a href="javascript:;" class="btn btn-primary edit_doc pull-right" data-type='aadhaar'
                                  data-current="menu11_" data-type-id="0">Add New Image</a></label>
                        <?php }?>
                    </div>
                   
                    <div class="col-xs-12 col-sm-12">
                        <?php $i = 1;$aadhaar_count = 0; foreach($user_uploaded_documents['aadhaar'][0]['user_documents'] as $arrAadhaar){ $aadhaar_count++;?>
                        <div class="col-xs-12 col-sm-2">
                            <div class="avatar-upload">
                                <div class="avatar-delete delete_doc" data-id="<?php echo $arrAadhaar['id'];?>"
                                     data-imagename="<?php echo $arrAadhaar['document_path'];?>" data-type="aadhaar"
                                     data-current="menu11_">
                                    <label for="imageUpload"></label>
                                </div>

                                <a class="lightbox" href="#preview-aadhaar-<?php echo $i;?>">
                                    <div class="avatar-view" data-id="<?php echo $arrAadhaar['id'];?>"
                                         data-imagename="<?php echo $arrAadhaar['document_path'];?>"
                                         data-type="aadhaar">

                                        <label for="imageUpload"></label>
                                    </div>
                                </a>
                                <?php if(isset($arrAadhaar['document_path']))
                                {
                                $storage_path = 'public/uploads/user_documents/';
                                $type = 'aadhaar';
                                $url = env("AWS_URL") . $storage_path . $user_id . "/" . $type . "/" . $arrAadhaar['document_path'];?>

                                <div class="avatar-preview">
                                    <div class="avatar-edit-img" id="aadhaar-<?php echo $i;?>">

                                        <img src="{{$url}}" class="img-thumbnail">

                                    </div>
                                    <div class="lightbox-target" id="preview-aadhaar-<?php echo $i;?>">
                                        <img src="{{$url}}"/>
                                        <a class="lightbox-close" href="#aadhaar-<?php echo $i;?>"></a>
                                    </div>

                                </div>
                                <?php }else{?>
                                <div class="avatar-preview">
                                    <div class="avatar-edit-img" id="aadhaar-<?php echo $i;?>">
                                        <img src="{{url('public/assets/images/jpg.png')}}" data-action="zoom"
                                             class="pull-left" style="height: 130px;width:130px;">
                                    </div>
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                        <?php $i++;}

                        if($aadhaar_count < 2){
                        for($j = $aadhaar_count;$j < 2;$j++)
                        {?>
                        <div class="col-xs-12 col-sm-2">
                            <div class="avatar-upload">


                                <div class="avatar-preview">
                                    <div class="avatar-edit-img" id="aadhaar-<?php echo $aadhaar_count + 1;?>">
                                        <img src="{{url('public/assets/images/jpg.png')}}" data-action="zoom"
                                             class="pull-left" style="height: 130px;width:130px;">
                                    </div>
                                </div>

                            </div>
                        </div>
                        <?php }}?>
                    </div>
                        <div class="col-xs-12 col-sm-4 visibility-type">

                            <div class="form-group pull-right">
                                <label class="radio-inline"><input type="radio"
                                                                   class="aadhaar_show doc-permission-radio-btn"
                                                                   name="aadhaar" data-name="aadhaar"
                                                                   value="1" {{ $public_document ? 'checked=checked' : ''}}>Public
                                </label>
                                <label class="radio-inline"><input type="radio"
                                                                   class="aadhaar_show doc-permission-radio-btn"
                                                                   name="aadhaar" data-name="aadhaar"
                                                                   value="0" {{ $private_document ? 'checked=checked' : ''}}>
                                    Private</label>
    
                            </div>
                        </div>

                </div>

                <?php }?>
                <?php if($documents['personal_detail']['account_no'])
                {


                $private_document = false;
                $public_document = false;

                if (isset($user_uploaded_documents['passbook'][0]['status']) && $user_uploaded_documents['passbook'][0]['status'] == '1') {
                    $public_document = true;
                } else {
                    $private_document = true;
                }
                ?>


                <div class="row add-more-section" id="profile_passbook">
                    <div class="col-xs-6 col-sm-7">
                        <span style="color:#47c4f0;float: left;"><i class="fa fa-check-circle-o"
                                                                    style="font-size: 22px;"></i></span>
                        <span style="margin-left: 10px;padding-top: 2px;float: left;color: #47c4f0;">PASSBOOK / CHEQUE LEAF - <?php echo $documents['personal_detail']['bank_name']?></span>
                    </div>
                    <div class="col-xs-6 col-sm-2 pull-right">
                        <?php if(count($user_uploaded_documents['passbook'][0]['user_documents']) < 5)
                        {?>
                        <label><a href="javascript:;" class="btn btn-primary edit_doc pull-right" data-type='passbook'
                                  data-current="menu11_" data-type-id="0">Add New Image</a></label>
                        <?php }?>
                    </div>
                    


                    <div class="col-xs-12 col-sm-12">
                        <?php $i = 1;$passbook_count = 0; foreach($user_uploaded_documents['passbook'][0]['user_documents'] as $arrPassbook){ $passbook_count++;?>
                        <div class="col-xs-12 col-sm-2">
                            <div class="avatar-upload">
                                <div class="avatar-delete delete_doc" data-id="<?php echo $arrPassbook['id'];?>"
                                     data-imagename="<?php echo $arrPassbook['document_path'];?>" data-type="passbook"
                                     data-current="menu11_">
                                    <label for="imageUpload"></label>
                                </div>

                                <a class="lightbox" href="#preview-passbook-<?php echo $i;?>">
                                    <div class="avatar-view" data-id="<?php echo $arrPassbook['id'];?>"
                                         data-imagename="<?php echo $arrPassbook['document_path'];?>"
                                         data-type="passbook">

                                        <label for="imageUpload"></label>
                                    </div>
                                </a>
                                <?php if(isset($arrPassbook['document_path']))
                                {
                                $storage_path = 'public/uploads/user_documents/';
                                $type = 'passbook';
                                $url = env("AWS_URL") . $storage_path . $user_id . "/" . $type . "/" . $arrPassbook['document_path'];?>

                                <div class="avatar-preview">
                                    <div class="avatar-edit-img" id="passbook-<?php echo $i;?>">

                                        <img src="{{$url}}" class="img-thumbnail">

                                    </div>
                                    <div class="lightbox-target" id="preview-passbook-<?php echo $i;?>">
                                        <img src="{{$url}}"/>
                                        <a class="lightbox-close" href="#passbook-<?php echo $i;?>"></a>
                                    </div>

                                </div>
                                <?php }else{?>
                                <div class="avatar-preview">
                                    <div class="avatar-edit-img" id="passbook-<?php echo $i;?>">
                                        <img src="{{url('public/assets/images/jpg.png')}}" data-action="zoom"
                                             class="pull-left" style="height: 130px;width:130px;">
                                    </div>
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                        <?php $i++;}

                        if($passbook_count < 5){
                        for($j = $passbook_count;$j < 5;$j++)
                        {?>
                        <div class="col-xs-12 col-sm-2">
                            <div class="avatar-upload">


                                <div class="avatar-preview">
                                    <div class="avatar-edit-img" id="passbook-<?php echo $passbook_count + 1;?>">
                                        <img src="{{url('public/assets/images/jpg.png')}}" data-action="zoom"
                                             class="pull-left" style="height: 130px;width:130px;">
                                    </div>
                                </div>

                            </div>
                        </div>
                        <?php }}?>

                    </div>
                    <div class="col-xs-12 col-sm-4 visibility-type">

                        <div class="form-group pull-right">
                            <label class="radio-inline"><input type="radio"
                                                               class="passbook_show doc-permission-radio-btn"
                                                               name="passbook" data-name="passbook"
                                                               value="1" {{ $public_document ? 'checked=checked' : ''}}>Public
                            </label>
                            <label class="radio-inline"><input type="radio"
                                                               class="passbook_show doc-permission-radio-btn"
                                                               name="passbook" data-name="passbook"
                                                               value="0" {{ $private_document ? 'checked=checked' : ''}}>
                                Private</label>

                        </div>
                    </div>

                </div>


                <?php } ?>



            </div>

            @if (isset($data['user_dangerous_cargo_endorsement_detail']))
                <div class="tab-pane" role="tabpanel" id="menu12">
                    <div class="row" style="margin-left: 1px;margin-top: 10px;">
                        <div class="col-sm-12" style="background: #2a55a2;color: #fff;border-radius: 4px;padding: 10px;font-size: 16px;">
                            DCE
                        </div>
                    </div>
                    
                    @php
                        $dceDetails = isset($data['user_dangerous_cargo_endorsement_detail']) && !empty($data['user_dangerous_cargo_endorsement_detail']) ? $data['user_dangerous_cargo_endorsement_detail'] : null;
                        $dceStatus = !empty($dceDetails) && isset($dceDetails['status']) ? json_decode($dceDetails['status']) : null;
                        $dceTypes = !empty($dceStatus) && isset($dceStatus->type) ? $dceStatus->type : [];
                    @endphp
                    
                    @if(in_array('oil', $dceTypes))
                        @php
                            $public_document = false;
                            if (isset($user_uploaded_documents['dce']['oil']['status']) && $user_uploaded_documents['dce']['oil']['status'] == '1') {
                                $public_document = true;
                            }
                        @endphp
                        <div class="row add-more-section" id="dce_oil">
                            <div class="col-xs-6 col-sm-7">
                                <span style="color:#47c4f0;float: left;">
                                    <i class="fa fa-check-circle-o" style="font-size: 22px;"></i>
                                </span>
                                <span style="margin-left: 10px;padding-top: 2px;float: left;color: #47c4f0;">DCE - Oil</span>
                            </div>
                            <div class="col-sm-3"></div>
                            <div class="col-xs-6 col-sm-2 pull-right">
                                @if ((!isset($user_uploaded_documents['dce']['oil'])) || (isset($user_uploaded_documents['dce']['oil'])) && count($user_uploaded_documents['dce']['oil']['user_documents']) < 2)
                                    <label>
                                        <a href="javascript:;" class="btn btn-primary edit_doc pull-right" data-type='dce' data-current="menu12_" data-type-id="oil">Add New Image</a>
                                    </label>
                                @endif
                            </div>
                            <div class="col-xs-12 col-sm-12">
                                @php 
                                    $i = 1;
                                    $oil_count = 0;
                                @endphp
                                @if (isset($user_uploaded_documents['dce']['oil']['user_documents']) && count($user_uploaded_documents['dce']['oil']['user_documents']) != 0)
                                    @foreach ($user_uploaded_documents['dce']['oil']['user_documents'] as $oilImages)
                                        @php $oil_count++; @endphp
                                        <div class="col-xs-12 col-sm-2">
                                            <div class="avatar-upload">
                                                <div class="avatar-delete delete_doc" data-id="{{ $oilImages['id'] }}" data-imagename="{{ $oilImages['document_path'] }}" data-type="oil" data-current="menu12_">
                                                    <label for="imageUpload"></label>
                                                </div>

                                                <a class="lightbox" href="#preview-oil-{{ $i }}">
                                                    <div class="avatar-view" data-id="{{ $oilImages['id'] }}" data-imagename="{{ $oilImages['document_path'] }}" data-type="oil">
                                                        <label for="imageUpload"></label>
                                                    </div>
                                                </a>
                                                @if(isset($oilImages['document_path']))
                                                    @php
                                                        $storage_path = 'public/uploads/user_documents/';
                                                        $type = 'dce';
                                                        $typeId = 'oil';
                                                        $url = env("AWS_URL") . $storage_path . $user_id . "/" . $type . "/" . $typeId . '/' . $oilImages['document_path'];
                                                    @endphp

                                                    <div class="avatar-preview">
                                                        <div class="avatar-edit-img" id="oil-{{ $i }}">
                                                            <img src="{{ $url }}" class="img-thumbnail">
                                                        </div>
                                                        <div class="lightbox-target" id="preview-oil-{{ $i }}">
                                                            <img src="{{ $url }}"/>
                                                            <a class="lightbox-close" href="#oil-{{ $i }}"></a>
                                                        </div>
                                                    </div>
                                                @else
                                                    <div class="avatar-preview">
                                                        <div class="avatar-edit-img" id="oil-{{ $i }}">
                                                            <img src="{{ url('public/assets/images/jpg.png') }}" data-action="zoom" class="pull-left" style="height: 130px; width:130px;">
                                                        </div>
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                    @endforeach
                                @endif
                                @php $i++; @endphp
                                @if($oil_count < 2)
                                    @for($j = $oil_count; $j < 2; $j++)
                                        <div class="col-xs-12 col-sm-2">
                                            <div class="avatar-upload">
                                                <div class="avatar-preview">
                                                    <div class="avatar-edit-img" id="oil-{{ $oil_count + 1 }}">
                                                        <img src="{{url('public/assets/images/jpg.png')}}" data-action="zoom" class="pull-left" style="height: 130px;width:130px;">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endfor
                                @endif
                            </div>
                                <div class="col-xs-12 col-sm-4 visibility-type">
                                    <div class="form-group pull-right">
                                        <label class="radio-inline">
                                            <input type="radio" class="oil_show doc-permission-radio-btn" name="oil" data-name="dce" data-type-id="oil" value="1" {{ ($public_document == true) ? 'checked' : ''}}>Public
                                        </label>
                                        <label class="radio-inline">
                                            <input type="radio" class="oil_show doc-permission-radio-btn" name="oil" data-name="dce" data-type-id="oil" value="0" {{ ($public_document == false) ? 'checked' : ''}}>Private
                                        </label>
                                    </div>
                                </div>
                        </div>
                    @endif
                    @if(in_array('chemical', $dceTypes))
                        @php
                            $public_document = false;
                            if (isset($user_uploaded_documents['dce']['chemical']['status']) && $user_uploaded_documents['dce']['chemical']['status'] == '1') {
                                $public_document = true;
                            }
                        @endphp
                        <div class="row add-more-section" id="dce_chemical">
                            <div class="col-xs-6 col-sm-7">
                                <span style="color:#47c4f0;float: left;">
                                    <i class="fa fa-check-circle-o" style="font-size: 22px;"></i>
                                </span>
                                <span style="margin-left: 10px;padding-top: 2px;float: left;color: #47c4f0;">DCE - Chemical</span>
                            </div>
                            <div class="col-sm-3"></div>
                            <div class="col-xs-6 col-sm-2 pull-right">

                                @if ((!isset($user_uploaded_documents['dce']['chemical'])) || (isset($user_uploaded_documents['dce']['chemical']) && count($user_uploaded_documents['dce']['chemical']['user_documents']) < 2))
                                    <label>
                                        <a href="javascript:;" class="btn btn-primary edit_doc pull-right" data-type='dce' data-current="menu12_" data-type-id="chemical">Add New Image</a>
                                    </label>
                                @endif
                            </div>
                            <div class="col-xs-12 col-sm-12">
                                @php 
                                    $i = 1;
                                    $chemical_count = 0;
                                    // dd($user_uploaded_documents);
                                @endphp
                                
                                @if (isset($user_uploaded_documents['dce']['chemical']['user_documents'])  && count($user_uploaded_documents['dce']['chemical']['user_documents']) != 0)
                                    @foreach ($user_uploaded_documents['dce']['chemical']['user_documents'] as $chemicalImages)
                                        @php $chemical_count++; @endphp
                                        <div class="col-xs-12 col-sm-2">
                                            <div class="avatar-upload">
                                                <div class="avatar-delete delete_doc" data-id="{{ $chemicalImages['id'] }}" data-imagename="{{ $chemicalImages['document_path'] }}" data-type="chemical" data-current="menu12_">
                                                    <label for="imageUpload"></label>
                                                </div>

                                                <a class="lightbox" href="#preview-chemical-{{ $i }}">
                                                    <div class="avatar-view" data-id="{{ $chemicalImages['id'] }}" data-imagename="{{ $chemicalImages['document_path'] }}" data-type="chemical">
                                                        <label for="imageUpload"></label>
                                                    </div>
                                                </a>
                                                {{-- {{ dd($chemicalImages) }} --}}
                                                @if(isset($chemicalImages['document_path']))
                                                    @php
                                                        $storage_path = 'public/uploads/user_documents/';
                                                        $type = 'dce';
                                                        $typeId = 'chemical';
                                                        $url = env("AWS_URL") . $storage_path . $user_id . "/" . $type . "/" . $typeId . '/' . $chemicalImages['document_path'];
                                                    @endphp

                                                    <div class="avatar-preview">
                                                        <div class="avatar-edit-img" id="chemical-{{ $i }}">
                                                            <img src="{{ $url }}" class="img-thumbnail">
                                                        </div>
                                                        <div class="lightbox-target" id="preview-chemical-{{ $i }}">
                                                            <img src="{{ $url }}"/>
                                                            <a class="lightbox-close" href="#chemical-{{ $i }}"></a>
                                                        </div>
                                                    </div>
                                                @else
                                                    <div class="avatar-preview">
                                                        <div class="avatar-edit-img" id="chemical-{{ $i }}">
                                                            <img src="{{ url('public/assets/images/jpg.png') }}" data-action="zoom" class="pull-left" style="height: 130px; width:130px;">
                                                        </div>
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                    @endforeach
                                @endif
                                @php $i++; @endphp
                                @if($chemical_count < 2)
                                    @for($j = $chemical_count; $j < 2; $j++)
                                        <div class="col-xs-12 col-sm-2">
                                            <div class="avatar-upload">
                                                <div class="avatar-preview">
                                                    <div class="avatar-edit-img" id="chemical-{{ $chemical_count + 1 }}">
                                                        <img src="{{url('public/assets/images/jpg.png')}}" data-action="zoom" class="pull-left" style="height: 130px;width:130px;">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endfor
                                @endif
                            </div>
                                <div class="col-xs-12 col-sm-4 visibility-type">
                                    <div class="form-group pull-right">
                                        <label class="radio-inline">
                                            <input type="radio" class="chemical_show doc-permission-radio-btn" name="chemical" data-name="dce" data-type-id="chemical" value="1" {{ ($public_document == true) ? 'checked' : ''}}>Public
                                        </label>
                                        <label class="radio-inline">
                                            <input type="radio" class="chemical_show doc-permission-radio-btn" name="chemical" data-name="dce" data-type-id="chemical" value="0" {{ ($public_document == false) ? 'checked' : ''}}>Private
                                        </label>
                                    </div>
                                </div>
                        </div>
                    @endif
                    @if(in_array('lequefied_gas', $dceTypes))
                        @php
                            $public_document = false;
                            if (isset($user_uploaded_documents['dce']['lequefied_gas']['status']) && $user_uploaded_documents['dce']['lequefied_gas']['status'] == '1') {
                                $public_document = true;
                            }
                        @endphp
                        <div class="row add-more-section" id="dce_lequefied_gas">
                            <div class="col-xs-6 col-sm-7">
                                <span style="color:#47c4f0;float: left;">
                                    <i class="fa fa-check-circle-o" style="font-size: 22px;"></i>
                                </span>
                                <span style="margin-left: 10px;padding-top: 2px;float: left;color: #47c4f0;">DCE - Liquefied Gas</span>
                            </div>
                            <div class="col-sm-3"></div>
                            <div class="col-xs-6 col-sm-2 pull-right">
                                @if ((!isset($user_uploaded_documents['dce']['lequefied_gas'])) || (isset($user_uploaded_documents['dce']['lequefied_gas']) && count($user_uploaded_documents['dce']['lequefied_gas']['user_documents']) < 2))
                                    <label>
                                        <a href="javascript:;" class="btn btn-primary edit_doc pull-right" data-type='dce' data-current="menu12_" data-type-id="lequefied_gas">Add New Image</a>
                                    </label>
                                @endif
                            </div>
                            <div class="col-xs-12 col-sm-12">
                                @php 
                                    $i = 1;
                                    $lequefied_gas_count = 0;
                                @endphp
                                @if (isset($user_uploaded_documents['dce']['lequefied_gas']['user_documents'])  && count($user_uploaded_documents['dce']['lequefied_gas']['user_documents']) != 0)
                                    @foreach ($user_uploaded_documents['dce']['lequefied_gas']['user_documents'] as $lequefiedGasImages)
                                        @php $lequefied_gas_count++; @endphp
                                        <div class="col-xs-12 col-sm-2">
                                            <div class="avatar-upload">
                                                <div class="avatar-delete delete_doc" data-id="{{ $lequefiedGasImages['id'] }}" data-imagename="{{ $lequefiedGasImages['document_path'] }}" data-type="lequefied_gas" data-current="menu12_">
                                                    <label for="imageUpload"></label>
                                                </div>

                                                <a class="lightbox" href="#preview-lequefied-gas-{{ $i }}">
                                                    <div class="avatar-view" data-id="{{ $lequefiedGasImages['id'] }}" data-imagename="{{ $lequefiedGasImages['document_path'] }}" data-type="lequefied_gas">
                                                        <label for="imageUpload"></label>
                                                    </div>
                                                </a>
                                                {{-- {{ dd($lequefiedGasImages) }} --}}
                                                @if(isset($lequefiedGasImages['document_path']))
                                                    @php
                                                        $storage_path = 'public/uploads/user_documents/';
                                                        $type = 'dce';
                                                        $typeId = 'lequefied_gas';
                                                        $url = env("AWS_URL") . $storage_path . $user_id . "/" . $type . "/" . $typeId . '/' . $lequefiedGasImages['document_path'];
                                                    @endphp

                                                    <div class="avatar-preview">
                                                        <div class="avatar-edit-img" id="lequefied-gas-{{ $i }}">
                                                            <img src="{{ $url }}" class="img-thumbnail">
                                                        </div>
                                                        <div class="lightbox-target" id="preview-lequefied-gas-{{ $i }}">
                                                            <img src="{{ $url }}"/>
                                                            <a class="lightbox-close" href="#lequefied-gas-{{ $i }}"></a>
                                                        </div>
                                                    </div>
                                                @else
                                                    <div class="avatar-preview">
                                                        <div class="avatar-edit-img" id="lequefied-gas-{{ $i }}">
                                                            <img src="{{ url('public/assets/images/jpg.png') }}" data-action="zoom" class="pull-left" style="height: 130px; width:130px;">
                                                        </div>
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                    @endforeach
                                @endif
                                @php $i++; @endphp
                                @if($lequefied_gas_count < 2)
                                    @for($j = $lequefied_gas_count; $j < 2; $j++)
                                        <div class="col-xs-12 col-sm-2">
                                            <div class="avatar-upload">
                                                <div class="avatar-preview">
                                                    <div class="avatar-edit-img" id="lequefied-gas-{{ $lequefied_gas_count + 1 }}">
                                                        <img src="{{url('public/assets/images/jpg.png')}}" data-action="zoom" class="pull-left" style="height: 130px;width:130px;">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endfor
                                @endif
                            </div>
                                <div class="col-xs-12 col-sm-4 visibility-type">
                                    <div class="form-group pull-right">
                                        <label class="radio-inline">
                                            <input type="radio" class="lequefied_gas_show doc-permission-radio-btn" name="lequefied_gas" data-name="dce" data-type-id="lequefied_gas" value="1" {{ ($public_document == true) ? 'checked' : ''}}>Public
                                        </label>
                                        <label class="radio-inline">
                                            <input type="radio" class="lequefied_gas_show doc-permission-radio-btn" name="lequefied_gas" data-name="dce" data-type-id="lequefied_gas" value="0" {{ ($public_document == false) ? 'checked' : ''}}>Private
                                        </label>
                                    </div>
                                </div>
                        </div>
                    @endif
                    @if(in_array('all', $dceTypes))
                        @php
                            $public_document = false;
                            if (isset($user_uploaded_documents['dce']['all']['status']) && $user_uploaded_documents['dce']['all']['status'] == '1') {
                                $public_document = true;
                            }
                        @endphp
                        <div class="row add-more-section" id="dce_all">
                            <div class="col-xs-6 col-sm-7">
                                <span style="color:#47c4f0;float: left;">
                                    <i class="fa fa-check-circle-o" style="font-size: 22px;"></i>
                                </span>
                                <span style="margin-left: 10px;padding-top: 2px;float: left;color: #47c4f0;">DCE - Oil + Chemical + Liquefied Gas</span>
                            </div>
                            <div class="col-sm-3"></div>
                            <div class="col-xs-6 col-sm-2 pull-right">
                                @if ((!isset($user_uploaded_documents['dce']['all'])) || (isset($user_uploaded_documents['dce']['all']) && count($user_uploaded_documents['dce']['all']['user_documents']) < 2))
                                    <label>
                                        <a href="javascript:;" class="btn btn-primary edit_doc pull-right" data-type='dce' data-current="menu12_" data-type-id="all">Add New Image</a>
                                    </label>
                                @endif
                            </div>
                            <div class="col-xs-12 col-sm-12">
                                @php 
                                    $i = 1;
                                    $all_count = 0;
                                @endphp
                                 @if (isset($user_uploaded_documents['dce']['all']['user_documents'])  && count($user_uploaded_documents['dce']['all']['user_documents']) != 0)
                                    @foreach ($user_uploaded_documents['dce']['all']['user_documents'] as $allImages)
                                        @php $all_count++; @endphp
                                        <div class="col-xs-12 col-sm-2">
                                            <div class="avatar-upload">
                                                <div class="avatar-delete delete_doc" data-id="{{ $allImages['id'] }}" data-imagename="{{ $allImages['document_path'] }}" data-type="all" data-current="menu12_">
                                                    <label for="imageUpload"></label>
                                                </div>

                                                <a class="lightbox" href="#preview-all-{{ $i }}">
                                                    <div class="avatar-view" data-id="{{ $allImages['id'] }}" data-imagename="{{ $allImages['document_path'] }}" data-type="all">
                                                        <label for="imageUpload"></label>
                                                    </div>
                                                </a>
                                                @if(isset($allImages['document_path']))
                                                    @php
                                                        $storage_path = 'public/uploads/user_documents/';
                                                        $type = 'dce';
                                                        $typeId = 'all';
                                                        $url = env("AWS_URL") . $storage_path . $user_id . "/" . $type . "/" . $typeId . '/' . $allImages['document_path'];
                                                    @endphp

                                                    <div class="avatar-preview">
                                                        <div class="avatar-edit-img" id="all-{{ $i }}">
                                                            <img src="{{ $url }}" class="img-thumbnail">
                                                        </div>
                                                        <div class="lightbox-target" id="preview-all-{{ $i }}">
                                                            <img src="{{ $url }}"/>
                                                            <a class="lightbox-close" href="#all-{{ $i }}"></a>
                                                        </div>
                                                    </div>
                                                @else
                                                    <div class="avatar-preview">
                                                        <div class="avatar-edit-img" id="all-{{ $i }}">
                                                            <img src="{{ url('public/assets/images/jpg.png') }}" data-action="zoom" class="pull-left" style="height: 130px; width:130px;">
                                                        </div>
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                    @endforeach
                                @endif
                                @php $i++; @endphp
                                @if($all_count < 2)
                                    @for($j = $all_count; $j < 2; $j++)
                                        <div class="col-xs-12 col-sm-2">
                                            <div class="avatar-upload">
                                                <div class="avatar-preview">
                                                    <div class="avatar-edit-img" id="all-{{ $all_count + 1 }}">
                                                        <img src="{{url('public/assets/images/jpg.png')}}" data-action="zoom" class="pull-left" style="height: 130px;width:130px;">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endfor
                                @endif
                            </div>
                                <div class="col-xs-12 col-sm-4 visibility-type">
                                    <div class="form-group pull-right">
                                        <label class="radio-inline">
                                            <input type="radio" class="all_show doc-permission-radio-btn" name="all" data-name="dce" data-type-id="all" value="1" {{ ($public_document == true) ? 'checked' : ''}}>Public
                                        </label>
                                        <label class="radio-inline">
                                            <input type="radio" class="all_show doc-permission-radio-btn" name="all" data-name="dce" data-type-id="all" value="1" {{ ($public_document == false) ? 'checked' : ''}}>Public
                                        </label>
                                    </div>
                                </div>
                        </div>
                    @endif
                </div>
            @endif
        </div>
    </div>
</div>

<script type="text/javascript">
    $('.documents_section .nav-item a').click(function (e) {
        e.preventDefault();
        $(this).tab('show');

        //removing active class from other selected/default tab
        $(".nav-item .active").removeClass("active");

        //adding active class to current clicked tab
        $(this).parent().addClass("active");
    });
</script>
