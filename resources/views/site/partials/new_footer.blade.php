<footer class="cs-footer" >
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="copyryt">Copyright @ {{\Carbon\Carbon::now()->year}} | <a href="{{env('app_url')}}">{{env('app_name')}}</a> | All Rights Reserved | Terms and Conditions</div>
            </div>
        </div>
    </div>
</footer>