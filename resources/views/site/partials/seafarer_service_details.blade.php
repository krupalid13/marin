@if(isset($services) && !empty($services))
	@foreach($services as $index => $services)
		<div class="row sea_service_detail_row sea_service_detail_row_{{$services['id']}}"
			 data-service-id={{$services['id']}} data-date="{{ isset($services['from']) ? date('d-m-Y',strtotime($services['from'])) : ''}}">
			<div class="col-sm-12">
				<div class="sub-details-container add-more-section">
					<div class="content-container">
						<div class="row">
							<div class="col-sm-12">
								<div class="m-b-5 display-flex-center">
									<div class="title sea-service-name">
										Sea Service
									</div>
									<div class="row"
										 style="flex-grow: 1; padding-left: 22px;">
										<div class="col-xs-6 col-md-4">
											<div class="discription">
												<span class="content-head">Sign On Date:</span>
												<span class="content">
																		{{ isset($services['from']) ? date('d-m-Y',strtotime($services['from'])) : ''}}
																	</span>
											</div>
										</div>
										<div class="col-xs-6">
											<div class="discription">
												<span class="content-head">Sign Off Date:</span>
												<span class="content">
																		{{ isset($services['to']) && !empty($services['to']) ? date('d-m-Y',strtotime($services['to'])) : 'On Board'}}
																	</span>
											</div>
										</div>
									</div>
									<div class="title sea-service-buttons">
										<div class="sea-service-edit-button"
											 data-index-id={{$index}} data-service-id={{$services['id']}}>
											<i class="fa fa-edit"
											   aria-hidden="true"
											   title="edit"></i>
										</div>
										<div class="sea-service-close-button"
											 data-id={{$services['id']}} data-service-id={{$services['id']}} data-from='{{$services["from"]}}'
											 data-to='{{$services["to"]}}'>
											<i class="fa fa-times"
											   aria-hidden="true"
											   title="delete"></i>
										</div>
									</div>

								</div>
							</div>
						</div>
					</div>
					<div class="row sea_service_details_section">
						<div class="col-xs-12 col-sm-6 col-md-4">
							<div class="discription">
								<span class="content-head">Rank:</span>
								<span class="content">
																	@foreach(\CommonHelper::new_rank() as $index => $category)
										@foreach($category as $r_index => $rank)
											{{ isset($services['rank_id']) ? $services['rank_id'] == $r_index ? $rank : '' : ''}}
										@endforeach
									@endforeach
																	</span>
							</div>
						</div>
						<div class="col-xs-12 col-sm-6 col-md-4">
							<div class="discription">
								<span class="content-head">Shipping Company:</span>
								<span class="content">
																		{{ isset($services['company_name']) ? $services['company_name'] : ''}}
							                                        </span>
							</div>
						</div>
						<div class="col-xs-12 col-sm-6 col-md-4">
							<div class="discription">
								<span class="content-head">Manning By:</span>
								<span class="content">
																		@if(isset($services['manning_by']) && !empty($services['manning_by']))
										{{$services['manning_by']}}
									@else
										-
									@endif
																	</span>
							</div>
						</div>
						<div class="col-xs-12 col-sm-6 col-md-4">
							<div class="discription">
								<span class="content-head">Name Of Ship:</span>
								<span class="content">
																		{{ isset($services['ship_name']) ? $services['ship_name'] : '-'}}
																	</span>
							</div>
						</div>
						<div class="col-xs-12 col-sm-6 col-md-4">
							<div class="discription">
								<span class="content-head">Ship Type:</span>
								<span class="content">
																	@foreach( \CommonHelper::ship_type() as $c_index => $type)
										{{ isset($services['ship_type']) ? $services['ship_type'] == $c_index ? $type : '' : ''}}
									@endforeach
																	</span>
							</div>
						</div>
						<div class="col-xs-12 col-sm-6 col-md-4">
							<div class="discription">
								<span class="content-head">Ship Flag:</span>
								<span class="content">
																		@if(isset($services['ship_flag']) && !empty($services['ship_flag']))
										@foreach( \CommonHelper::countries() as $c_index => $name)
											{{ isset($services['ship_flag']) ? $services['ship_flag'] == $c_index ? $name : '' : ''}}
										@endforeach
									@else
										-
									@endif
																	</span>
							</div>
						</div>
						<div class="col-xs-12 col-sm-6 col-md-4">
							<div class="discription">
								<span class="content-head">GRT:</span>
								<span class="content">
																		{{ !empty($services['grt']) ? $services['grt'] : '-'}}
																	</span>
							</div>
						</div>
						<div class="col-xs-12 col-sm-6 col-md-4">
							<div class="discription">
								<span class="content-head">BHP:</span>
								<span class="content">
																		{{ !empty($services['bhp']) ? $services['bhp'] : '-'}}
																	</span>
							</div>
						</div>
						<div class="col-xs-12 col-sm-6 col-md-4">
							<div class="discription">
								<span class="content-head">Engine Type:</span>
								<span class="content">
																	@if(isset($services['engine_type']) && !empty($services['engine_type']))
										@if(isset($services['engine_type']) && $services['engine_type'] == 'other')
											{{ isset($services['other_engine_type']) ? $services['other_engine_type'] : '-'}}
										@else
											@foreach( \CommonHelper::engine_type() as $c_index => $type)
												{{ isset($services['engine_type']) ? $services['engine_type'] == $c_index ? $type  : '' : ''}}
											@endforeach
										@endif
									@else
										-
									@endif
																	</span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	@endforeach
@else
	<div class="row no-data-found">
		<div class="col-xs-12 text-center">
			<div class="discription">
				<span class="content-head">No Data Found</span>
			</div>
		</div>
	</div>
@endif