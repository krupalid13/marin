<?php 
    
    if(isset($data) && !empty($data))
        $batch_data = $data->toArray(); 

?>

@if(isset($batch_data['data']) && !empty($batch_data['data']))

    <div class="section-2" id="filter-results">
        <span class="heading">
         Results <br>
        </span>
        <span class="pagi pagi-up">
            <span class="search-count">
                Showing {{$batch_data['from']}} - {{$batch_data['to']}} of {{$batch_data['total']}} Batches
            </span>
            <nav> 
            <ul class="pagination pagination-sm">
                {!! $data->render() !!}
            </ul> 
            </nav>
        </span>
        <div class="row">
        
        @foreach($batch_data['data'] as $batch)
            <div class="search-result-card1">
                <div class="col-xs-12 col-sm-12 col-md-6">
                    <div class="batch-description">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="dropdown setting" style="float: right;">
                                <a data-toggle="dropdown" class="btn-xs dropdown-toggle">
                                    <i class="fa fa-cog"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-light pull-right">
                                    <li>
                                        <a href="{{route('site.institute.course.batches.edit', $batch['id'])}}">
                                            <span>Edit</span>
                                        </a>
                                    </li>
                                    <?php 
                                        $disable= 'hide';
                                        $enable= 'hide';
                                        if(isset($batch) && $batch['status'] == 0){
                                            $enable = '';
                                        }else{
                                            $disable = '';
                                        }
                                    ?>
                                    <li>
                                        <a class="batchDisableButton disable-{{$batch['id']}} {{$disable}}" data-status='disable' data-id="{{$batch['id']}}">
                                            <span>Deactive</span>
                                        </a>
                                    </li>
                                    
                                    <li>
                                        <a class="batchDisableButton enable-{{$batch['id']}} {{$enable}}" data-status='enable' data-id="{{$batch['id']}}">
                                            <span>Active</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="" onclick="shiftBatch('{{$batch['id']}}')">
                                             <span>Shift Batch</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                                <div class="other-discription">
                                  <strong> Course Type: </strong>
                                    <span class="ans">
                                        @foreach(\CommonHelper::institute_course_types() as $r_index => $rank)
                                            {{ !empty($batch['course_details']['course_type']) ? $batch['course_details']['course_type'] == $r_index ? $rank : '' : ''}}
                                        @endforeach
                                    </span>
                                </div>
                                <?php
                                    $courses = \App\Courses::all()->toArray();
                                    $Select = 'select';
                                ?>
                                <div class="other-discription">
                                   <strong> Course Name: </strong>
                                    <span class="ans">
                                        @foreach($courses as $index => $course)
                                            {{ !empty($batch['course_details']['course_id']) ? $batch['course_details']['course_id'] == $course['id'] ? $course['course_name'] : '' : ''}}
                                        @endforeach
                                    </span>
                                </div>
                                <div class="details">
                                    <div class="col-sm-6">
                                        <div class="other-discription p-l-0 p-r-0">
                                          <strong>  Start Date: </strong>
                                            <span class="ans spanGreen">
                                                {{!empty($batch['course_start_date']) ? date('d-m-Y',strtotime($batch['course_start_date'])) : '-'}}
                                            </span>
                                        </div>
                                        <div class="other-discription p-l-0 p-r-0">
                                           <strong> Duration: </strong>
                                            <span class="ans">
                                                {{!empty($batch['duration']) ? $batch['duration'] : '-'}} Days
                                            </span>
                                        </div>
                                        <div class="other-discription p-l-0 p-r-0">
                                           <strong> Batch Size: </strong>
                                            <span class="ans">
                                                {{!empty($batch['size']) ? $batch['size'] : '-'}}
                                            </span>
                                        </div>
                                         <div class="other-discription p-l-0 p-r-0">
                                                    <strong>    Batch Type: </strong>
                                                        <span class="ans">
                                                            {{!empty($batch['batch_type']) ? $batch['batch_type'] == 'on_demand' ? 'On Demand' : 'Confirmed' : '-'}}

                                                            <?php
                                                            
                                                            if($batch['batch_type'] == 'on_demand' && empty($batch['course_start_date']) ) {
                                                                echo "<span class='sub-date'> ( " . date('M y', strtotime($batch['start_date'])) . ")</span>";
                                                            }
                                                            ?>
                                                        </span>
                                                    </div>
                                         <div class="other-discription p-l-0 p-r-0">
                                          <strong>  Status: </strong>
                                            <span class="ans">
                                                <span class="disable-{{$batch['id']}} {{$disable}}" data-status='disable' data-id="{{$batch['id']}}">
                                                    Active
                                                </span>
                                                <span class="enable-{{$batch['id']}} {{$enable}}" data-status='enable' data-id="{{$batch['id']}}">
                                                    Deactive
                                                </span>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="other-discription p-l-0 p-r-0">
                                          <strong>  Cost: </strong>
                                            <span class="ans">
                                                <i class="fa fa-inr"></i>
                                                {{!empty($batch['cost']) ? $batch['cost'] : '-'}}
                                            </span>
                                        </div>
                                        <div class="other-discription p-l-0 p-r-0">
                                          <strong>  Payment Type: </strong>
                                            <span class="ans">
                                                {{!empty($batch['payment_type']) ? ucwords($batch['payment_type'])." Payment" : '-'}}
                                            </span>
                                        </div>
                                        <div class="other-discription p-l-0 p-r-0">
                                          <strong>  Reserved Seats: </strong>
                                            <span class="ans">
                                                {{!empty($batch['reserved_seats']) ? $batch['reserved_seats'] : '0'}}
                                            </span>
                                        </div>
                                    </div>

                                    <?php
                                        $location_ids = '';
                                        $locations = '';
                                        $comma = 0;
                                        
                                    ?>

                                    <div class="col-sm-12">
                                        <div class="other-discription p-l-0 p-r-0">
                                           <strong> Location: </strong>
                                            <span class="ans">
                                                @if(isset($batch['batch_location']))
                                                    @foreach($batch['batch_location'] as $index => $data1)
                                                        @if($data1['location']['country'] == '95')
                                                            @if($comma == 1)
                                                            ,
                                                            @endif

                                                            {{$data1['location']['city']['name']}} - {{ucfirst(strtolower($data1['location']['state']['name']))}}
                                                            <?php $comma = 1 ?>
                                                        @endif
                                                    @endforeach
                                                @endif
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
        </div>
        
        <span class="pagi m-t-25">
            <span class="search-count">
                Showing {{$batch_data['from']}} - {{$batch_data['to']}} of {{$batch_data['total']}} Batches
            </span>
            <nav> 
            <ul class="pagination pagination-sm">
                {!! $data->render() !!}
            </ul> 
            </nav>
        </span>

    @else
        <div class="section-2" id="filter-results"><div class="row no-results-found">No results found. Try again with different search criteria.</div></div>
    @endif