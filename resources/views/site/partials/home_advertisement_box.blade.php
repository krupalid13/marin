@foreach($data as $key => $value)
	@if(isset($value['advertisement_details']['img_path']) AND !empty($value['advertisement_details']['img_path']))
		<div class="ad_card">
		    <div class="ad_cardTitle">
		        Advertisement
		        <a class='advertisement_enquiry' data-target="#enquire-advertisements-modal" data-company-id="{{isset($value['advertisement_details']['company_id']) ? $value['advertisement_details']['company_id'] : ''}}" data-advertise-id="{{isset($value['advertise_id']) ? $value['advertise_id'] : ''}}">Enquire</a>
		    </div>

		    <div class="ad_cardImg">
		    	<a href="{{ isset($value['advertisement_details']['company_registration_details']['user_id']) ? route('site.advertiser.profile.id',$value['advertisement_details']['company_registration_details']['user_id']) : '' }}" target = "_blank">
		    		<?php
		    			$path = '';
		    			if(isset($value['advertisement_details']['img_path']) && !empty($value['advertisement_details']['img_path'])){
		    				$path = env('ADVERTISE_PATH').$value['advertisement_details']['company_id']."/".$value['advertisement_details']['img_path'];
		    			}
		    		?> 
		        	<img src="{{ !empty($path) ? asset($path) : ''}}">
		        </a>
		    </div>
		</div>
	@endif
@endforeach