<div class="overlay overlay-fadein"></div>
<div class="dashboard-menu">
	<div class="menubar-btn-down"><i class="fa fa-angle-double-down" aria-hidden="true"></i></div>
	<div class="menubar-btn-up"><i class="fa fa-angle-double-up" aria-hidden="true"></i></div>
	<div class="menubar-name">Dashboard Menu</div>	
</div>
<div class="navbar-container">
	<ul class="nav nav-pills nav-stacked nav-pills-stacked-example">
	  	<li class="nav-link {{Route::currentRouteName() == 'site.advertiser.profile' ? 'nav-active' : '' }}">
	  		<a href="{{ route('site.advertiser.profile') }}"><i class="fa fa-building-o nav-icon" aria-hidden="true"></i>My Profile</a>
	  	</li> 
	  	<!-- <li class="nav-link {{Route::currentRouteName() == 'site.advertiser.mysubscription' ? 'nav-active' : '' }}">
	  		<a href="{{ route('site.advertiser.mysubscription') }}"><i class="fa fa-envelope-o nav-icon" aria-hidden="true"></i></i>My Subscriptions</a>
	  	</li> --> 
	  	<li class="nav-link {{Route::currentRouteName() == 'site.advertiser.add.advertise' ? 'nav-active' : '' }}">
	  		<a href="{{ route('site.advertiser.add.advertise') }}"><i class="fa fa-envelope-o nav-icon" aria-hidden="true"></i></i>Add Advertise</a>
	  	</li>
	  	<li class="nav-link {{Route::currentRouteName() == 'site.advertisements.list.enquiries' ? 'nav-active' : '' }}">
	  		<a href="{{ route('site.advertisements.list.enquiries') }}"><i class="fa fa-envelope-o nav-icon" aria-hidden="true"></i></i>Advertise Enquiries</a>
	  	</li>
    </ul>
</div>