<div class="col-xs-12 col-sm-6 col-md-4">
	<div class="discription">
		<span class="content-head">Shipping Company:</span>
		<span class="content">
			{{ isset($data['company_name']) ? $data['company_name'] : '-'}}
        </span>
	</div>
</div>
<div class="col-xs-12 col-sm-6 col-md-4">
	<div class="discription">
		<span class="content-head">Name Of Ship:</span>
		<span class="content">
			{{ isset($data['ship_name']) ? $data['ship_name'] : '-'}}
		</span>
	</div>
</div>
<div class="col-xs-12 col-sm-6 col-md-4">
	<div class="discription">
		<span class="content-head">Ship Type:</span>
		<span class="content">
		@foreach( \CommonHelper::ship_type() as $c_index => $type)
			{{ isset($data['ship_type']) ? $data['ship_type'] == $c_index ? $type : '' : ''}}
		@endforeach
		</span>
	</div>
</div>
<div class="col-xs-12 col-sm-6 col-md-4">
	<div class="discription">
		<span class="content-head">Rank:</span>
		<span class="content">
		@foreach(\CommonHelper::new_rank() as $index => $category)
	    	@foreach($category as $r_index => $rank)
            	{{ isset($data['rank_id']) ? $data['rank_id'] == $r_index ? $rank : '' : ''}}
            @endforeach
        @endforeach
		</span>
	</div>
</div>
<div class="col-xs-12 col-sm-6 col-md-4">
	<div class="discription">
		<span class="content-head">GRT:</span>
		<span class="content">
			{{ !empty($data['grt']) ? $data['grt'] : '-'}}
		</span>
	</div>
</div>
<div class="col-xs-12 col-sm-6 col-md-4">
	<div class="discription">
		<span class="content-head">BHP:</span>
		<span class="content">
			{{ !empty($data['bhp']) ? $data['bhp'] : '-'}}
		</span>
	</div>
</div>
<div class="col-xs-12 col-sm-6 col-md-4">
	<div class="discription">
		<span class="content-head">Engine Type:</span>
		<span class="content">
		@if(isset($data['engine_type']) && !empty($data['engine_type']))
			@if(isset($data['engine_type']) && $data['engine_type'] == '11')
				{{ isset($data['other_engine_type']) ? $data['other_engine_type'] : '-'}}
			@else
				@foreach( \CommonHelper::engine_type() as $c_index => $type)
					{{ isset($data['engine_type']) ? $data['engine_type'] == $c_index ? $type  : '' : '-'}}
				@endforeach
			@endif
		@else
			-
		@endif
	</select>
		</span>
	</div>
</div>
<div class="col-xs-12 col-sm-6 col-md-4">
	<div class="discription">
		<span class="content-head">Sign On Date:</span>
		<span class="content">
			{{ isset($data['from']) ? date('d-m-Y',strtotime($data['from'])) : ''}}
		</span>
	</div>
</div>
<div class="col-xs-12 col-sm-6 col-md-4">
	<div class="discription">
		<span class="content-head">Sign Off Date:</span>
		<span class="content">
			{{ isset($data['to']) ? date('d-m-Y',strtotime($data['to'])) : ''}}
		</span>
	</div>
</div>
<div class="col-xs-12 col-sm-6 col-md-4">
	<div class="discription">
		<span class="content-head">Ship Flag:</span>
		<span class="content">
			@if(isset($data['ship_flag']) && !empty($data['ship_flag']))
				@foreach( \CommonHelper::countries() as $c_index => $name)
					{{ isset($data['ship_flag']) ? $data['ship_flag'] == $c_index ? $name : '' : ''}}
				@endforeach
			@else
				-
			@endif
		</span>
	</div>
</div>