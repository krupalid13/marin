@if(isset($data) && !empty($data))
    @foreach($data as $index => $agents)
    <div class="vessel-section agent_section_{{$agents['id']}}">
        <div class="row vessel-row">
            <div class="col-sm-12 vessel-title">
                <div class="description">
                    <span class="content-head agent-name">Agent {{$index+1}}</span>
                    <div class="ship-buttons">
                        <div class="agent-edit-button" data-agent-id="{{$agents['id']}}" data-scope="{{$agents['agent_type']}}">
                            <i class="fa fa-edit" aria-hidden="true" title="edit"></i>
                        </div>
                        <div class="agent-close-button" data-agent-id="{{$agents['id']}}" data-scope="{{$agents['agent_type']}}">
                            <i class="fa fa-times" aria-hidden="true" title="delete"></i>
                        </div>
                    </div>
                </div>
            </div>
            <?php
                $image_path = '';
                if(isset($agents['logo'])){
                    $image_path = env('COMPANY_TEAM_AGENT_PIC_PATH')."".$agents['id']."/".$agents['logo'];
                }
            ?>
            <div class="col-sm-3">
                <div class="description">
                    <span class="content">
                        @if(!empty($image_path))
                            <img class="agent-preview" src="/{{ $image_path }}">
                        @else
                            <img class="agent-preview" src="/images/user_default_image.png">
                        @endif
                    </span>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="description">
                    <span class="content-head">Company Name</span>
                    <span class="content">
                        {{ !empty($agents['to_company']) ? $agents['to_company'] : ''}}
                        {{ !empty($agents['from_company']) ? $agents['from_company'] : ''}}
                    </span>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="description">
                    <span class="content-head">Location</span>
                    <span class="content">{{ !empty($agents['location']) ? $agents['location'] : '-'}}</span>
                </div>
            </div>
        </div>
        <hr>
    </div> 
    @endforeach
@else
    <div class="row">
        <div class='col-xs-12 text-center'>
            <div class='discription'>
                <span class='content-head'>No Data Found</span>
            </div>
        </div>
    </div>
@endif
