<div class="row">
    <div class="col-xs-12">
        <div class="section-3">
            <div class="row">

                {{--profile left side--}}

                <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                    <input type="hidden" name="verify_email" id="verify_email" value="{{isset($data[0]['is_email_verified']) ? $data[0]['is_email_verified'] : ''}}">
                    <input type="hidden" name="verify_mobile" id="verify_mobile" value="{{isset($data[0]['is_mob_verified']) ? $data[0]['is_mob_verified'] : ''}}">
                    <div class="sub-details-container photo-container">
                        <div class="photo-card">
                            <div class="photo_section">
                                @if($data[0]['profile_pic'])
                                    <div class="logo-pic text-center">
                                        <img id="preview" src="/{{ env('INSTITUTE_LOGO_PATH')}}{{$data[0]['id']}}/{{$data[0]['profile_pic']}}" avatar-holder-src="{{ asset('images/user_default_image.png') }}">
                                    </div>
                                @else
                                    <div class="logo-pic pic">
                                        <img id="preview" src="{{ asset('images/user_default_image.png') }}" avatar-holder-src="{{ asset('images/user_default_image.png') }}">
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="sub-details-container">
                        <div class="content-container">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="title text-center">
                                        {{ isset($data[0]['institute_registration_detail']['institute_name']) ? strtoupper($data[0]['institute_registration_detail']['institute_name']) : ''}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <?php
                    if(isset($data[0]['institute_registration_detail']['institute_detail']['institute_description']) && !empty($data[0]['institute_registration_detail']['institute_detail']['institute_description'])){
                        $description = $data[0]['institute_registration_detail']['institute_detail']['institute_description'];
                    }
                    ?>

                    <div class="sub-details-container">
                        <div class="content-container">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="title">
                                        Institute Description
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="discription">
                                        <span class="content-head">
                                            {{ isset($description) ? ucfirst($description) : '-'}}
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    @if(isset($data[0]['institute_registration_detail']['institute_locations']) && !empty(isset($data[0]['institute_registration_detail']['institute_locations']) && $data[0]['institute_registration_detail']['institute_locations'] != NULL))
                        <div class="sub-details-container company_contact_information dont-break-out">
                            <div class="content-container">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="title">Contact Information</div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12 m-t-10">
                                        <div class="discription">
                                            <span class="content-head">Website:</span>
                                            <span class="content display_block">
                                            @if(isset($data[0]['institute_registration_detail']['website']) && !empty($data[0]['institute_registration_detail']['website']))
                                                <a target="_blank" href="{{ "http://".$data[0]['institute_registration_detail']['website'] }}">{{ isset($data[0]['institute_registration_detail']['website']) ? "http://".$data[0]['institute_registration_detail']['website'] : '-' }}</a>
                                            @else
                                                -
                                            @endif
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                @if(!isset($user))
                                <div class="row">
                                    <div class="col-sm-12 m-t-10">
                                        <div class="discription">
                                            <span class="content-head">Email ID:</span>
                                            <span class="content display_block">
                                                @if(isset($data[0]['institute_registration_detail']['email']) && !empty($data[0]['institute_registration_detail']['email']))
                                                    {{ $data[0]['institute_registration_detail']['email']}}
                                                @else
                                                    -
                                                @endif
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                @endif
                                <hr>

                                <div class="row m-t-10 m-b-10">
                                    <div class="col-sm-12">
                                        <div class="discription">
                                            <span class="title">Locations</span>
                                        </div>
                                    </div>
                                </div>

                                @if(isset($data[0]['institute_registration_detail']['institute_locations']) && !empty($data[0]['institute_registration_detail']['institute_locations']))
                                    @foreach($data[0]['institute_registration_detail']['institute_locations'] as $index => $institute_location)
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <div class="discription">
                                                    @if($institute_location['headbranch'] == '1')
                                                        <span class="content-head">Head Office</span>
                                                    @else
                                                        <span class="content-head">Branch</span>
                                                    @endif

                                                    <span class="content display_block">
                                                        {{ isset($institute_location['address']) ? ucfirst($institute_location['address']) : '-' }} ,
                                                        {{ isset($institute_location['city_id']) ? $institute_location['pincode']['pincodes_cities'][0]['city']['name'] : $institute_location['city_text']}} 
                                                        {{ isset($institute_location['pincode_text']) ? $institute_location['pincode_text'] : ''}}
                                                        {{ isset($institute_location['state_id']) ? $institute_location['pincode']['pincodes_states'][0]['state']['name'] : $institute_location['state_text']}}
                                                    </span>
                                                </div>
                                            </div>
                                            @if(isset($institute_location['telephone']) && !empty($institute_location['telephone']))
                                                <div class="col-xs-12 m-t-10">
                                                    <div class="discription">
                                                        <span class="content-head">Contact No:</span>
                                                        <span class="content display_block">
                                                            {{ $institute_location['telephone']}}
                                                        </span>
                                                    </div>
                                                </div>
                                            @endif
                                        </div>

                                        @if(sizeOf($data[0]['institute_registration_detail']['institute_locations']) > $index + 1)
                                            <hr>
                                        @endif
                                    @endforeach
                                @endif
                            </div>
                        </div>
                    @endif

                    @if(!isset($user))
                        <div class="sub-details-container company_edit_button hidden-xs">
                            <div class="content-container">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="edit-btn-container">
                                            <a href="{{ route('site.edit.institute.details') }}" class="profile-edit-bt">
                                                EDIT PROFILE
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif

                    @include('site.partials.featured_advertisement_template')
                </div>

                {{--profile left side--}}

                {{--profile center side--}}
                
                <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
                    @if(isset($data[0]['institute_registration_detail']['advertisment_details'][0]['img_path']) && !empty($data[0]['institute_registration_detail']['advertisment_details'][0]['img_path']))
                        @if(isset($data[0]['institute_registration_detail']['advertisment_details'][0]['img_path']) && !empty($data[0]['institute_registration_detail']['advertisment_details'][0]['img_path']))
                            <?php
                                $img_path = '/'.env('INSTITUTE_LOGO_PATH').$data[0]['id'].'/'.$data[0]['institute_registration_detail']['advertisment_details'][0]['img_path'];

                                if(!empty($data[0]['institute_registration_detail']['advertisment_details'][0]['updated_at'])){
                                    $start = $data[0]['institute_registration_detail']['advertisment_details'][0]['updated_at'];
                                }else{
                                    $start = $data[0]['institute_registration_detail']['advertisment_details'][0]['created_at'];
                                } 

                                $start  = date_create(date('Y-m-d',strtotime($start)));
                                $end    = date_create(); // Current time and date
                                $diff   = date_diff( $start, $end );

                                $uploaded_before = $diff->format("%a");

                                //dd($uploaded_before,$start,$end,$diff);
                            ?>

                            @if(isset($user) && $uploaded_before > 15)

                            @else
                            <div class="sub-details-container content-container">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="content-container company_advertisement" style="height:auto;">
                                            <div class="row">
                                                <div class="logo-pic img-responsive" id="logo-pic" style="background-image: url(/{{ env('INSTITUTE_LOGO_PATH')}}{{$data[0]['id']}}/{{$data[0]['institute_registration_detail']['advertisment_details'][0]['img_path']}});">
                                                </div>
                                            </div>
                                            @if($uploaded_before > 15)
                                                <div class="row m-t-15">
                                                    <div class="col-sm-12">
                                                        <div class="alert alert-block alert-danger fade in">
                                                            Note: You haven't updated your advertisement since last 15 days. Please update your advertisement to enable it.
                                                        </div>
                                                    </div>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endif
                        @endif
                    @endif
                </div>

                @if(isset($data[0]['institute_registration_detail']['institute_notice']['notice']) && !empty($data[0]['institute_registration_detail']['institute_notice']['notice']))
                    <?php 
                        $curr_date = date('Y-m-d');
                    ?>
                    @if($data[0]['institute_registration_detail']['institute_notice']['start_date'] <= $curr_date && $data[0]['institute_registration_detail']['institute_notice']['end_date'] > $curr_date)
                    <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
                        <div class="sub-details-container content-container">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="content-container">
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <div class="title">Notice</div>
                                                <div class="txt-red">
                                                    <div class="pull-left">
                                                        ********
                                                    </div>
                                                    <br>
                                                    <div class="notice-text p-l-50">
                                                        {{ $data[0]['institute_registration_detail']['institute_notice']['notice']}}
                                                    </div>
                                                    <div class="pull-right">
                                                        ********
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif
                @endif

                <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
                        <div class="sub-details-container content-container">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="content-container">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="title">Institute Courses</div>
                                            
                                            @if(isset($institute_course_type) && !empty($institute_course_type))
                                            <ul class="nav nav-pills mb-3 institute-pills-tab m-t-10" id="institute-pills-tab" role="tablist">
                                                <?php $active = 'active';$active_in = 'active in'?>
                                                    @foreach($institute_course_type as $course_type => $course)
                                                        <li class="nav-item {{$active}}">
                                                            <a class="nav-link" data-toggle="pill" href="#test{{$course_type}}" role="tab" aria-controls="pills-home" aria-selected="true">
                                                                @foreach(\CommonHelper::institute_course_types() as $r_index => $rank)
                                                                    {{ isset($course_type) ? $course_type == $r_index ? $rank : '' : ''}}
                                                                @endforeach
                                                            </a>
                                                        </li>
                                                    <?php $active = ''?>
                                                @endforeach
                                            </ul>
                                            <div class="tab-content institute-pills-tabContent" id="institute-pills-tabContent">
                                                @foreach($institute_course_type as $course_type => $courses)
                                                    <div class="tab-pane fade {{$active_in}}" id="test{{$course_type}}" role="tabpanel" aria-labelledby="#test{{$course_type}}">
                                                        <div class="course_type_section">
                                                            @foreach(\CommonHelper::institute_course_types() as $r_index => $rank)
                                                                {{ isset($course_type) ? $course_type == $r_index ? $rank : '' : ''}}
                                                            @endforeach
                                                        </div>
                                                    
                                                        @foreach($courses as $course_index => $course)
                                                            <div class="course_name_section">
                                                                <a href="{{route('site.seafarer.course.search',['course_type' => $course_type,'course_name' => $course['course_details']['id'],'institute_name' => [$data[0]['institute_registration_detail']['id']]])}}">
                                                                    {{$course['course_details']['course_name']}}
                                                                </a>
                                                            </div>
                                                        @endforeach
                                                    </div>
                                                    <?php $active_in = ''?>
                                                @endforeach
                                            </div>
                                            @else
                                                <div class="row no-data-found">
                                                    <div class="col-xs-12 text-center">
                                                        <div class="discription">
                                                            <span class="content-head">No Courses Found</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                @if(isset($data[0]['institute_registration_detail']['institute_gallery']) && !empty($data[0]['institute_registration_detail']['institute_gallery']))
                <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
                    <div class="sub-details-container content-container">
                        <div class="row content-container">
                            <div class="col-xs-12">
                                <div class="title">Institute Gallery</div>
                            </div>
                        </div>
                        <div id="img-gallery" class="slider" role="toolbar">
                            @foreach($data[0]['institute_registration_detail']['institute_gallery'] as $images)

                                <?php
                                    $image_path = '';
                                    if(isset($images)){
                                        $image_path = "/".env('INSTITUTE_IMAGE_GALLERY_PATH')."".$images['institute_id']."/".$images['image'];
                                    } 
                                ?>

                                <div class="slick-slide" data-slick-index="0" aria-hidden="true" style="width: 140px;" tabindex="-1" role="option" aria-describedby="slick-slide50">
                                    <div class="img" style="background-image: url({{ $image_path }});">
                                        <!-- <img src="{{$image_path}}" style="box-shadow: 0px 2px 4px 0px #908d8d;width:100%;height:100%;"> -->
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>      
                </div>
                @endif
            </div>

        </div>
    </div>
</div>

<div id="advModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Advertisement</h4>
            </div>
            <div class="modal-body">
                <img src="{{isset($img_path) ? $img_path : ''}}" class="img-responsive w-100" style="box-shadow:0px 2px 5px 1px #b3aeae;">
            </div>
        </div>

    </div>
</div>