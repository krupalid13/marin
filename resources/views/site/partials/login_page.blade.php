<div class="login_page" style="border: 1px solid #e1e1e1;border-radius: 5px;max-width: 570px;background-color: #f5f5f5;margin:0 auto;">
    <div id="login-popup" class="white-popup signin-sigup-default no-margin" role="dialog" >
        <div class="signin-signup-heading">
            <div class="signin-signup new-sigin-link active login_page"><a class="open-popup-link">Login</a></div>
            <!--<div class="signin-signup new-sigup-link login_page"><a class="open-popup-link">Register</a></div>-->
        </div>
        <form id="loginForm" method="post" action="{{route('site.login')}}">
            <input type="hidden" name="_token" value="{{{ csrf_token() }}}">
            <input type="hidden" name="redirect_url" value="{{ isset($_GET['redirect_url']) ? !empty($_GET['redirect_url']) ? $_GET['redirect_url'] : '' : ''}}">

            <div class="new-login-content">
                <div class="row">

                    <div class="small-12 medium-12 columns">
                        <div class="new-login-form-container">
                            <div id="login-form" @if(session('login_error')) style="display: block;" @endif style="display: block;">
                                <div class="row margin-vert-5">
                                    <div class="col-sm-3 padding-top-5">
                                      <label class="mar-b-10"><span class="label-icon"><i class="fa fa-envelope" aria-hidden="true"></i></span><span class="label-text mandatory">Email</span>
                                    </label>
                                    </div>
                                    <div class="col-sm-9">
                                        <input class="form-control" type="email" name="email" id="email" placeholder="Enter email" value="{{ old('email') }}"/>
                                    </div>
                                </div>
                                <div class="row margin-vert-5">
                                    <div class="col-sm-3 padding-top-5">
                                      <label class="mar-b-10"><span class="label-icon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span><span class="label-text mandatory">Password</span>
                                      </label>
                                    </div>
                                    <div class="col-sm-9">
                                        <input class="form-control" type="password" id="password" name="password" placeholder="Enter password"/>
                                        @if (session('error'))
                                            <label id="login_error" class="error">{{ session('error') }}</label>
                                        @endif
                                    </div>
                                </div>
                                <div class="row margin-vert-5">
                                    <div class="col-xs-6 col-sm-6">
<!--                                        <input id="remember" name="remember" type="checkbox">
                                        <label for="remember">Remember Me</label>-->
                                    </div>
                                    <div class="col-sm-6">
                                        <a href="{{route('site.reset.password.view')}}" class="pull-right">Forget Password?</a>
                                    </div>
                                </div>
                                <div class="row margin-vert-5">
                                    <div class="col-sm-12">
                                        <button type="submit" data-style='zoom-in' class="btn-1 light-blue new-sign-up-btn fullwidth ladda-button">Sign In</button>
                                    </div>
                                </div>
                                <!-- <div class="row margin-vert-5">
                                    <div class="col-sm-12">
                                        <div class="forget-pass-link">
                                            <a data-mfp-src="#forgot-password-popup" class="open-popup-link">Forgot Password</a>
                                        </div>
                                    </div>
                                </div> -->
                                <div class="row">
                                    <div class="small-12 columns info" align="center" style="display:none">
                                        Logging in. Please wait...
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="ajaxError small-12 columns" align="center"
                                         style="display:{{ session('login_error') ? 'block' : 'none' }}">
                                        <div class="" style="color:#ff0000;">
                                            <strong>Login failed!!!</strong>
                                        </div>
                                        <div class="reason">
                                            {{ session('login_error') ? session('login_error') : '' }}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>

    <div id="signup-popup" class="white-popup signin-sigup-default no-margin hide">
            <div class="signin-signup-heading">
                <div class="signin-signup new-sigin-link login_page"><a class="open-popup-link">Login</a></div>
                <div class="signin-signup new-sigup-link active login_page"><a class="open-popup-link">Register</a></div>
            </div>
            <form id="signupForm" action="" method="post">
                <div class="new-signup-role-content">
                    <div class="row">
                        <div class="col-xs-6 col-sm-6">
                            <div class="role-seafarer role-active role-container" data-role='seafarer'>
                                <img class="doctor-icon" src="{{asset('/images/registration/seafarer.png')}}" alt="">
                                <div class="role-title">
                                    As Seafarer
                                </div>
                                <div class="tic tic-check"><i class="fa fa-check" aria-hidden="true"></i></div>
                            </div>
                        </div>
                        <!-- <div class="col-xs-6 col-sm-3">
                            <div class="role-company role-container" data-role='company'>
                                <img class="doctor-icon" src="{{asset('/images/registration/shipping_company.png')}}" alt="">
                                <div class="role-title">
                                    As Shipping Company
                                </div>
                                <div class="tic tic-uncheck"><i class="fa fa-check" aria-hidden="true"></i></div>
                            </div>
                        </div>
                        <div class="col-xs-6 col-sm-3">
                            <div class="role-institute role-container" data-role='advertise'>
                                <img class="doctor-icon" src="{{asset('/images/registration/advertise.png')}}" alt="">
                                <div class="role-title">
                                    As Advertiser
                                </div>
                                <div class="tic tic-uncheck"><i class="fa fa-check" aria-hidden="true"></i></div>
                            </div>
                        </div> -->
                        <div class="col-xs-6 col-sm-6">
                            <div class="role-institute role-container" data-role='institute'>
                                <img class="doctor-icon" src="{{asset('/images/registration/shipping_institute.png')}}" alt="">
                                <div class="role-title">
                                    As Institute
                                </div>
                                <div class="tic tic-uncheck"><i class="fa fa-check" aria-hidden="true"></i></div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                      <div class="col-sm-12">
                        <button type="button" class="btn-1 coss-primary-btn new-sign-up-btn fullwidth" id="sign-up-btn" data-role='seafarer'>Register</button>
                      </div>
                    </div>
                </div>
            </form>
          </div>
</div>