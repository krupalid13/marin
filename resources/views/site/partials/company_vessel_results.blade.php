@if(isset($ship_details) && !empty($ship_details))
    @foreach($ship_details as $index => $ship)
    <div class="vessel-section vessel_section_{{$ship['id']}}">
        <div class="row vessel-row">
            <div class="col-sm-12 vessel-title">
                <div class="description">
                    <span class="content-head vessel-name">Vessel {{$index+1}}</span>
                    <div class="ship-buttons">
                        <div class="vessel-edit-button" data-vessel-id="{{$ship['id']}}" data-scope="{{$ship['scope_type']}}">
                            <i class="fa fa-edit" aria-hidden="true" title="edit"></i>
                        </div>
                        <div class="vessel-close-button" data-vessel-id="{{$ship['id']}}" data-scope="{{$ship['scope_type']}}">
                            <i class="fa fa-times" aria-hidden="true" title="delete"></i>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-6 col-sm-2">
                <div class="description">
                    <span class="content-head">Vessel Name</span>
                    <span class="content">{{ !empty($ship['ship_name']) ? $ship['ship_name'] : '-' }}</span>
                </div>
            </div>
            <div class="col-xs-6 col-sm-2">
                <div class="description">
                    <span class="content-head">Type</span>
                    <span class="content">
                        @foreach( \CommonHelper::ship_type() as $c_index => $ship_type)
                            {{ isset($ship['ship_type']) ? $ship['ship_type'] == $c_index ? $ship_type : '' : ''}}
                        @endforeach
                    </span>
                </div>
            </div>
            <div class="col-xs-6 col-sm-2">
                <div class="description">
                    <span class="content-head">Flag</span>
                    <span class="content">
                        @if(isset($ship['ship_flag']) && !empty($ship['ship_flag']))
                            @foreach( \CommonHelper::countries() as $c_index => $ship_flag)
                                {{ isset($ship['ship_flag']) ? $ship['ship_flag'] == $c_index ? $ship_flag : '' : ''}}
                            @endforeach
                        @else
                            -
                        @endif
                    </span>
                </div>
            </div>
            <div class="col-xs-6 col-sm-1">
                <div class="description">
                    <span class="content-head">GRT</span>
                    <span class="content">{{ !empty($ship['grt']) ? $ship['grt'] : '-' }}</span>
                </div>
            </div>
            <div class="col-xs-6 col-sm-1">
                <div class="description">
                    <span class="content-head">BHP</span>
                    <span class="content">{{ !empty($ship['bhp']) ? $ship['bhp'] : '-' }}</span>
                </div>
            </div>
            <div class="col-xs-6 col-sm-2">
                <div class="description">
                    <span class="content-head">Engine</span>
                    <span class="content">
                        @if(isset($ship['engine_type']) && !empty($ship['engine_type']))
                            @foreach( \CommonHelper::engine_type() as $c_index => $engine_type)
                                {{ isset($ship['engine_type']) ? $ship['engine_type'] == $c_index ? $engine_type : '' : ''}}
                            @endforeach
                        @else
                            -
                        @endif
                    </span>
                </div>
            </div>
            <div class="col-xs-6 col-sm-2">
                <div class="description">
                    <span class="content-head">P & I</span>
                    <span class="content">
                        {{ isset($ship['p_i_cover']) ? $ship['p_i_cover'] == '1' ? 'Yes' : 'No' : ''}}
                    </span>
                </div>
            </div>
        </div>
        <hr>
    </div>
    @endforeach
@else
    <div class='col-xs-12 text-center'>
        <div class='discription'>
            <span class='content-head'>No Data Found</span>
        </div>
    </div>
@endif