<div class="row">
    <div class="col-xs-12">
        <div class="section-3">
            <div class="row">

                {{--profile left side--}}

                <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                    <input type="hidden" name="verify_email" id="verify_email" value="{{isset($data[0]['is_email_verified']) ? $data[0]['is_email_verified'] : ''}}">
                    <input type="hidden" name="verify_mobile" id="verify_mobile" value="{{isset($data[0]['is_mob_verified']) ? $data[0]['is_mob_verified'] : ''}}">
                    <div class="sub-details-container photo-container">
                        <div class="photo-card">
                            <div class="photo_section">
                                @if($data[0]['profile_pic'])
                                    <div class="logo-pic text-center">
                                        <img id="preview" src="/{{ env('COMPANY_LOGO_PATH')}}{{$data[0]['id']}}/{{$data[0]['profile_pic']}}" avatar-holder-src="{{ asset('images/user_default_image.png') }}">
                                    </div>
                                @else
                                    <div class="logo-pic pic">
                                        <img id="preview" src="{{ asset('images/user_default_image.png') }}" avatar-holder-src="{{ asset('images/user_default_image.png') }}">
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="sub-details-container">
                        <div class="content-container">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="title">
                                        {{ isset($data[0]['company_registration_detail']['company_name']) ? strtoupper($data[0]['company_registration_detail']['company_name']) : ''}}
                                    </div>
                                </div>
                            </div>
                            
                            @if(isset($data[0]['company_registration_detail']['company_detail']['company_type']) and $data[0]['company_registration_detail']['company_detail']['company_type'] == 1)
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="discription">
                                            <span class="content-head">RPSL :</span>
                                            <span class="content">
                                                {{isset($data[0]['company_registration_detail']['company_detail']['rpsl_no']) ? $data[0]['company_registration_detail']['company_detail']['rpsl_no'] : ''}}
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>

                    <?php
                    if(isset($data[0]['company_registration_detail']['company_detail']['company_description']) && !empty($data[0]['company_registration_detail']['company_detail']['company_description'])){
                        $description = $data[0]['company_registration_detail']['company_detail']['company_description'];
                    }
                    ?>

                    <div class="sub-details-container">
                        <div class="content-container">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="title">
                                        Company Summary
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="discription">
                                        <span class="content-head">
                                            {{ isset($description) ? ucfirst($description) : '-'}}
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    @if(isset($data[0]['company_registration_detail']['company_documents']['incorporation_certificate']) || isset($data[0]['company_registration_detail']['company_documents']['rpsl_certificate']))
                        <div class="sub-details-container">
                            <div class="content-container">
                                @if(isset($data[0]['company_registration_detail']['company_documents']['incorporation_certificate']))
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="discription">
                                                <span class="content-head">Incorporation Cert :</span>
                                                <a href="/{{env('COMPANY_DOC_PATH')}}/{{$data[0]['company_registration_detail']['id']}}/{{isset($data[0]['company_registration_detail']['company_documents']['incorporation_certificate']) ? $data[0]['company_registration_detail']['company_documents']['incorporation_certificate'] : '-'}}" target="_blank">View</a>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                                @if($data[0]['company_registration_detail']['company_detail']['company_type'] == 1 && isset($data[0]['company_registration_detail']['company_documents']['rpsl_certificate']))
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="discription">
                                                <span class="content-head">RPSL Cert :</span>
                                                <a href="/{{env('COMPANY_DOC_PATH')}}/{{$data[0]['company_registration_detail']['id']}}/{{isset($data[0]['company_registration_detail']['company_documents']['rpsl_certificate']) ? $data[0]['company_registration_detail']['company_documents']['rpsl_certificate'] : '-'}}" target="_blank">View</a>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            </div>
                        </div>
                    @endif

                    @if(isset($data[0]['company_registration_detail']['company_locations']) && !empty(isset($data[0]['company_registration_detail']['company_locations']) && $data[0]['company_registration_detail']['company_locations'] != NULL))
                        <div class="sub-details-container company_contact_information dont-break-out">
                            <div class="content-container">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="title">Contact Information</div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12 m-t-10">
                                        <div class="discription">
                                            <span class="content-head">Website:</span>
                                            <span class="content display_block">
                                            @if(isset($data[0]['company_registration_detail']['website']) && !empty($data[0]['company_registration_detail']['website']))
                                                <a target="_blank" href="{{ "http://".$data[0]['company_registration_detail']['website'] }}">{{ isset($data[0]['company_registration_detail']['website']) ? "http://".$data[0]['company_registration_detail']['website'] : '-' }}</a>
                                            @else
                                                -
                                            @endif
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                @if(!isset($user))
                                <div class="row">
                                    <div class="col-sm-12 m-t-10">
                                        <div class="discription">
                                            <span class="content-head">Email ID:</span>
                                            <span class="content display_block">
                                                @if(isset($data[0]['company_registration_detail']['email']) && !empty($data[0]['company_registration_detail']['email']))
                                                    {{ $data[0]['company_registration_detail']['email']}}
                                                @else
                                                    -
                                                @endif
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                @endif
                                <hr>

                                <div class="row m-t-10 m-b-10">
                                    <div class="col-sm-12">
                                        <div class="discription">
                                            <span class="title">Locations</span>
                                        </div>
                                    </div>
                                </div>

                                @if(isset($data[0]['company_registration_detail']['company_locations']) && !empty($data[0]['company_registration_detail']['company_locations']))
                                    @foreach($data[0]['company_registration_detail']['company_locations'] as $index => $company_location)
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <div class="discription">
                                                    @if($company_location['is_headoffice'] == '1')
                                                        <span class="content-head">Head Office</span>
                                                    @else
                                                        <span class="content-head">Branch</span>
                                                    @endif

                                                    <span class="content display_block">
                                                        {{ isset($company_location['address']) ? ucfirst($company_location['address']) : '-' }}
                                                    </span>
                                                </div>
                                            </div>
                                            @if(isset($company_location['telephone']) && !empty($company_location['telephone']))
                                                <div class="col-xs-12 m-t-10">
                                                    <div class="discription">
                                                        <span class="content-head">Contact No:</span>
                                                        <span class="content display_block">
                                                            {{ $company_location['telephone']}}
                                                        </span>
                                                    </div>
                                                </div>
                                            @endif
                                        </div>

                                        @if(sizeOf($data[0]['company_registration_detail']['company_locations']) > $index + 1)
                                            <hr>
                                        @endif
                                    @endforeach
                                @endif
                            </div>
                        </div>
                    @endif

                    @if(!isset($user))
                        <div class="sub-details-container company_edit_button hidden-xs">
                            <div class="content-container">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="edit-btn-container">
                                            <a href="{{ route('site.edit.company.details') }}" class="profile-edit-bt">
                                                EDIT PROFILE
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif

                    @include('site.partials.featured_advertisement_template')
                </div>

                {{--profile left side--}}

                {{--profile center side--}}
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                    @if(isset($data[0]['advertisment_company_details']['img_path']) && !empty($data[0]['advertisment_company_details']['img_path']))
                        @if(isset($data[0]['advertisment_company_details']['img_path']) && !empty($data[0]['advertisment_company_details']['img_path']))
                            <?php
                                $img_path = '/'.env('COMPANY_LOGO_PATH').$data[0]['id'].'/'.$data[0]['advertisment_company_details']['img_path'];

                                if(!empty($data[0]['advertisment_company_details']['updated_at'])){
                                    $start = $data[0]['advertisment_company_details']['updated_at'];
                                }else{
                                    $start = $data[0]['advertisment_company_details']['created_at'];
                                } 

                                $start  = date_create(date('Y-m-d',strtotime($start)));
                                $end    = date_create(); // Current time and date
                                $diff   = date_diff( $start, $end );

                                $uploaded_before = $diff->format("%a");

                                //dd($uploaded_before,$start,$end,$diff);
                            ?>

                            @if(isset($user) && $uploaded_before > 15)

                            @else
                            <div class="sub-details-container content-container">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="content-container company_advertisement">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="title m-b-15">Advertisement</div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="logo-pic img-responsive" id="logo-pic" style="background-image: url(/{{ env('COMPANY_LOGO_PATH')}}{{$data[0]['id']}}/{{$data[0]['advertisment_company_details']['img_path']}});">
                                                </div>
                                            </div>
                                            @if($uploaded_before > 15)
                                                <div class="row m-t-15">
                                                    <div class="col-sm-12">
                                                        <div class="alert alert-block alert-danger fade in">
                                                            Note: You haven't updated your advertisement since last 15 days. Please update your advertisement to enable it.
                                                        </div>
                                                    </div>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endif
                        @endif
                        <?php $index_count = 0 ;?>
                        <div class="sub-details-container content-container">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="content-container company_job_details">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="title">Recent Job Posting</div>
                                            </div>
                                            @if(isset($data[0]['company_registration_detail']['next_company_jobs']) && !empty(isset($data[0]['company_registration_detail']['next_company_jobs']) && $data[0]['company_registration_detail']['next_company_jobs'] != NULL))
                                            <div class="col-sm-6">
                                                <a href="{{route('site.seafarer.job.search',['company_name' => isset($data[0]['company_registration_detail']['company_name']) ? $data[0]['company_registration_detail']['company_name'] : ''])}}" class="title pull-right">View All</a>
                                            </div>
                                            @endif
                                        </div>
                                        @if(isset($data[0]['company_registration_detail']['next_company_jobs']) && !empty(isset($data[0]['company_registration_detail']['next_company_jobs']) && $data[0]['company_registration_detail']['next_company_jobs'] != NULL))
                                            <div class="row">
                                                <div class=" job_posting_slider">
                                                    <div class="section sm-filter-space-new scrollbar">
                                                        <div class="row no-margin">
                                                            <div class="col-md-12" id="main-data">
                                                                <div class="section listing-container company_main_container">
                                                                    {{csrf_field()}}
                                                                    <div class="ship_details_container_main">
                                                                        <div class="row m-0">
                                                                            @foreach($data[0]['company_registration_detail']['next_company_jobs'] as $index => $job_details)
                                                                                <?php
                                                                                    $current_date = time();
                                                                                    $show_apply_button = false;

                                                                                    if(isset($job_details['user_applied_jobs'][0]['created_at'])){
                                                                                        $job_create_date = strtotime($job_details['user_applied_jobs'][0]['created_at']);
                                                                                        $datediff = $current_date - $job_create_date;

                                                                                        $days = floor($datediff / (60 * 60 * 24));
                                                                                        if ($days > 15) {
                                                                                            $show_apply_button = true;
                                                                                        }
                                                                                    }

                                                                                    if(!Auth::check()){
                                                                                        $show_apply_button = true;
                                                                                    }

                                                                                ?>
                                                                                <div class="col-xs-12 p-0">
                                                                                    <div class="company_job_card">
                                                                                        <div class="row">
                                                                                            <div class="col-sm-6">
                                                                                                <div class="other-discription">
                                                                                                    Opening Valid From:
                                                                                                    <span class="display_block black">
                                                                                                        {{ isset($job_details['valid_from']) && !empty($job_details['valid_from']) ? $job_details['valid_from'] : '-'}}
                                                                                                    </span>
                                                                                                </div>
                                                                                                <div class="other-discription discription">
                                                                                                    Rank:
                                                                                                    <span class="black">
                                                                                                        @foreach(\CommonHelper::new_rank() as $rank_index => $category)
                                                                                                            @foreach($category as $r_index => $rank)
                                                                                                                {{ isset($job_details['rank']) ? $job_details['rank'] == $r_index ? $rank : '' : ''}}
                                                                                                            @endforeach
                                                                                                        @endforeach
                                                                                                    </span>
                                                                                                </div>
                                                                                                <div class="other-discription">
                                                                                                    Flag:
                                                                                                    <span class="black">
                                                                                                        @if(isset($job_details['ship_flag']) && !empty($job_details['ship_flag']))
                                                                                                            @foreach(\CommonHelper::countries() as $index => $flag)
                                                                                                                {{ $job_details['ship_flag'] == $index ? $flag : '' }}
                                                                                                            @endforeach
                                                                                                        @else
                                                                                                            -
                                                                                                        @endif
                                                                                                    </span>
                                                                                                </div>
                                                                                                <div class="other-discription">
                                                                                                    GRT:
                                                                                                    <span class="black">
                                                                                                        {{ isset($job_details['grt']) ? $job_details['grt'] : '-'}}
                                                                                                    </span>
                                                                                                </div>
                                                                                                <div class="other-discription">
                                                                                                    Min Exp Required:
                                                                                                    <span class="black">
                                                                                                        {{ isset($job_details['min_rank_exp']) ? $job_details['min_rank_exp'] : '-'}} Years
                                                                                                    </span>
                                                                                                </div>
                                                                                                <div class="other-discription">
                                                                                                    Date of Joining:
                                                                                                    <span class="black">
                                                                                                        {{ isset($job_details['date_of_joining']) && !empty($job_details['date_of_joining']) ? $job_details['date_of_joining'] : '-'}}
                                                                                                    </span>
                                                                                                </div>
                                                                                                <?php
                                                                                                $nationality_id = '';
                                                                                                $count_nationality = 1;
                                                                                                if(isset($job_details['job_nationality'] ) && !empty($job_details['job_nationality'] )){
                                                                                                    $nationality_id = array_values(collect($job_details['job_nationality'] )->pluck('nationality')->toArray());
                                                                                                }

                                                                                                ?>
                                                                                                <div class="other-discription">
                                                                                                    Nationality:
                                                                                                    <span class="black">
                                                                                                    @if(!empty($nationality_id))
                                                                                                        @foreach( \CommonHelper::countries() as $c_index => $country)
                                                                                                            @if(!empty($nationality_id) && in_array($c_index,$nationality_id))
                                                                                                                {{ $country }}
                                                                                                                @if(count($nationality_id) > $count_nationality)
                                                                                                                    ,<?php $count_nationality++; ?>
                                                                                                                @endif
                                                                                                            @endif
                                                                                                        @endforeach
                                                                                                    @else
                                                                                                        -
                                                                                                    @endif
                                                                                                    </span>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="col-sm-6">
                                                                                                <div class="other-discription">
                                                                                                    Job Posting In:
                                                                                                    <span class="display_block black">
                                                                                                        @foreach(\CommonHelper::seafarer_job_category() as $index => $category)
                                                                                                            {{ isset($job_details['job_category']) ? $job_details['job_category'] == $index ? $category : '' : ''}}
                                                                                                        @endforeach
                                                                                                    </span>
                                                                                                </div>
                                                                                                <div class="other-discription">
                                                                                                    Ship Type:
                                                                                                    <span class="black">
                                                                                                        @foreach( \CommonHelper::ship_type() as $c_index => $ship_type)
                                                                                                            {{ isset($job_details['ship_type']) ? $job_details['ship_type'] == $c_index ? $ship_type : '' : ''}}
                                                                                                        @endforeach
                                                                                                    </span>
                                                                                                </div>
                                                                                                <div class="other-discription">
                                                                                                    BHP:
                                                                                                    <span class="black">
                                                                                                        {{ isset($job_details['bhp']) ? $job_details['bhp'] : '-'}}
                                                                                                    </span>
                                                                                                </div>
                                                                                                <div class="other-discription">
                                                                                                    Engine Type:
                                                                                                    <span class="black">
                                                                                                        @if(isset($job_details['engine_type']) && !empty($job_details['engine_type']))
                                                                                                            @foreach( \CommonHelper::engine_type() as $c_index => $engine_type)
                                                                                                                {{ !empty($job_details['engine_type']) ? $job_details['engine_type'] == $c_index ? $engine_type : '' : ''  }}
                                                                                                            @endforeach
                                                                                                        @else
                                                                                                            -
                                                                                                        @endif
                                                                                                    </span>
                                                                                                </div>
                                                                                                <div class="other-discription">
                                                                                                    Sal Offered:
                                                                                                    <span class="black">
                                                                                                        @if(isset($job_details['wages_currency']) && $job_details['wages_currency'] == 'rupees')
                                                                                                            <i class="fa fa-inr" aria-hidden="true"></i>
                                                                                                        @elseif(isset($job_details['wages_currency']) && $job_details['wages_currency'] == 'dollar')
                                                                                                            <i class="fa fa-usd" aria-hidden="true"></i>
                                                                                                        @else
                                                                                                            -
                                                                                                        @endif

                                                                                                        @if(isset($job_details['wages_offered']) && !empty(isset($job_details['wages_offered'])))
                                                                                                            {{$job_details['wages_offered']}}
                                                                                                        @endif
                                                                                                    </span>
                                                                                                </div>
                                                                                                <div class="other-discription">
                                                                                                    Posted On:
                                                                                                    <span class="black">
                                                                                                        {{ isset($job_details['created_at']) && !empty($job_details['created_at']) ? date('d-m-Y H:i a', strtotime($job_details['created_at'])) : '-'}}
                                                                                                    </span>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="row">
                                                                                            <div class="col-sm-12">
                                                                                                <div class="other-discription">
                                                                                                    Job Description:
                                                                                                    <span class="black">
                                                                                                        {{ isset($job_details['job_description']) ? $job_details['job_description'] : '-'}}
                                                                                                    </span>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="col-sm-12">
                                                                                                @if(empty($user_id) || (isset($role) AND $role == 'seafarer'))
                                                                                                    @if(isset($job_details['user_applied_jobs']) && count($job_details['user_applied_jobs']) > 0)
                                                                                                        @if($show_apply_button == false)
                                                                                                            <div class="italic-text">
                                                                                                                <i>You can apply for the same job after 15 days.</i>
                                                                                                            </div>
                                                                                                        @endif
                                                                                                    @else
                                                                                                        <div class="italic-text hide apply-after-15-{{$job_details['id']}}">
                                                                                                            <i>You can apply for the same job after 15 days.</i>
                                                                                                        </div>
                                                                                                    @endif
                                                                                                @endif
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>

                                                                                    @if(empty($user_id) || (isset($role) AND $role == 'seafarer'))
                                                                                        @if(isset($job_details['user_applied_jobs']) && count($job_details['user_applied_jobs']) > 0)
                                                                                            @if($show_apply_button == false)
                                                                                                <button  data-style="zoom-in" class="btn coss-primary-btn apply-btn job-apply-button ladda-button hide" id="job-apply-button-{{$job_details['id']}}" data-form-id="{{$job_details['id']}}" data-company-id="{{$job_details['company_id']}}">
                                                                                                    Apply Now
                                                                                                </button>
                                                                                            @else
                                                                                                <button  data-style="zoom-in" class="btn coss-primary-btn apply-btn job-apply-button ladda-button" id="job-apply-button-{{$job_details['id']}}" data-form-id="{{$job_details['id']}}" data-company-id="{{$job_details['company_id']}}">
                                                                                                    Apply Now
                                                                                                </button>
                                                                                            @endif
                                                                                        @else
                                                                                            <button  data-style="zoom-in" class="btn coss-primary-btn apply-btn job-apply-button ladda-button" id="job-apply-button-{{$job_details['id']}}" data-form-id="{{$job_details['id']}}" data-company-id="{{$job_details['company_id']}}">
                                                                                                Apply Now
                                                                                            </button>
                                                                                        @endif
                                                                                    @endif

                                                                                    <?php
                                                                                        $count = count($data[0]['company_registration_detail']['next_company_jobs']);
                                                                                        $index_count++;
                                                                                    ?>

                                                                                    @if( $count > $index_count)
                                                                                        <hr>
                                                                                    @endif
                                                                                </div>
                                                                            @endforeach
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @else
                                            <div class="sub-details-container" style="box-shadow:none;">
                                                <div class="content-container">
                                                    <div class="row">
                                                        <div class="col-xs-12 text-center">
                                                            <div class="discription">
                                                                <span class="content-head">No Jobs Posted.</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif

                    <div class="sub-details-container content-container">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="content-container">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12">
                                            <div class="title">We Are Owners To</div>
                                        </div>
                                    </div>
                                    <div class="row m-t-5">
                                        <div class="col-xs-8 discription">
                                            <span class="content-head">Ship Details</span>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="ship_details_container">
                                            <div class="section company_ship_details scrollbar">
                                                <div class="row no-margin">
                                                    <div id="main-data">
                                                        <div class="section listing-container company_main_container">
                                                            <div class="ship_owner_details">
                                                                <?php $index_count = 0; ?>
                                                                <div class="row m-0">
                                                                    @if(isset($data[0]['owner_ship']) && !empty(isset($data[0]['owner_ship']) && $data[0]['owner_ship'] != NULL))
                                                                        @foreach($data[0]['owner_ship'] as $index => $value)
                                                                            <div class="col-xs-12 col-sm-12">
                                                                                <div class="row">
                                                                                    <div class="col-xs-6 col-sm-3">
                                                                                        <div class="discription">
                                                                                            <span class="content-head">Vessel</span>
                                                                                            <span class="content content_description">
                                                                                                {{ isset($value['ship_name']) && !empty($value['ship_name']) ? $value['ship_name'] : '-'}}
                                                                                            </span>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-xs-6 col-sm-3">
                                                                                        <div class="discription">
                                                                                            <span class="content-head">Type</span>
                                                                                            <span class="content content_description">
                                                                                                @foreach( \CommonHelper::ship_type() as $c_index => $ship_type)
                                                                                                    {{ isset($value['ship_type']) ? $value['ship_type'] == $c_index ? $ship_type : '' : ''}}
                                                                                                @endforeach
                                                                                            </span>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-xs-6 col-sm-3">
                                                                                        <div class="discription">
                                                                                            <span class="content-head">Flag</span>
                                                                                            <span class="content content_description">
                                                                                                @if(isset($value['ship_flag']) && !empty($value['ship_flag']))
                                                                                                    @foreach( \CommonHelper::countries() as $c_index => $ship_flag)
                                                                                                        {{ isset($value['ship_flag']) ? $value['ship_flag'] == $c_index ? $ship_flag : '' : ''}}
                                                                                                    @endforeach
                                                                                                @else
                                                                                                    -
                                                                                                @endif
                                                                                            </span>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-xs-6 col-sm-3">
                                                                                        <div class="discription">
                                                                                            <span class="content-head">P & I Cover</span>
                                                                                            <span class="content content_description">
                                                                                                {{ isset($value['p_i_cover']) ? $value['p_i_cover'] == '1' ? 'Yes' : 'No' : 'No'}}
                                                                                            </span>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>

                                                                                <div class="row section_one p-t-10">
                                                                                    <div class="col-xs-6 col-sm-3">
                                                                                        <div class="discription">
                                                                                            <span class="content-head">GRT</span>
                                                                                            <span class="content content_description">
                                                                                                {{ isset($value['grt']) && !empty($value['grt']) ? $value['grt'] : '-'}}
                                                                                             </span>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-xs-6 col-sm-3">
                                                                                        <div class="discription">
                                                                                            <span class="content-head">BHP</span>
                                                                                            <span class="content content_description">
                                                                                                {{ isset($value['bhp']) && !empty($value['bhp']) ? $value['bhp'] : '-'}}
                                                                                            </span>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-xs-6 col-sm-6">
                                                                                        <div class="discription">
                                                                                            <span class="content-head">Engine Type</span>
                                                                                            <span class="content content_description">
                                                                                                @if(isset($value['engine_type']) && !empty($value['engine_type']))
                                                                                                    @foreach( \CommonHelper::engine_type() as $c_index => $engine_type)
                                                                                                        {{ isset($value['engine_type']) ? $value['engine_type'] == $c_index ? $engine_type : '' : ''}}
                                                                                                    @endforeach
                                                                                                @else
                                                                                                    -
                                                                                                @endif
                                                                                            </span>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>

                                                                                <?php
                                                                                    $count = count($data[0]['company_registration_detail']['company_ship_details']);
                                                                                    $index_count++;
                                                                                ?>

                                                                                @if( $count > $index_count)
                                                                                    <hr>
                                                                                @endif
                                                                            </div>
                                                                        @endforeach
                                                                    @else
                                                                        <div class="col-xs-12 text-center">
                                                                            <div class="discription">
                                                                                <span class="content-head">No Data Found.</span>
                                                                            </div>
                                                                        </div>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="sub-details-container content-container">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="content-container scrollbar">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="title">The Vessels We Manage</div>
                                        </div>
                                    </div>
                                    <div class="row m-t-5">
                                        <div class="col-xs-8 discription">
                                            <span class="content-head">Ship Details</span>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="ship_details_container">
                                            <div class="section sm-filter-space company_ship_details scrollbar">
                                                <div class="row no-margin">
                                                    <div id="main-data">
                                                        <div class="section listing-container company_main_container">
                                                            <div class="ship_owner_details">
                                                                <?php $index_count = 0; ?>
                                                                <div class="row m-0">
                                                                    @if(isset($data[0]['manager_ship']) && !empty(isset($data[0]['manager_ship']) && $data[0]['manager_ship'] != NULL))
                                                                        @foreach($data[0]['manager_ship'] as $index => $value)
                                                                            <div class="col-xs-12 col-sm-12">
                                                                                <div class="row">
                                                                                    <div class="col-xs-6 col-sm-3">
                                                                                        <div class="discription">
                                                                                            <span class="content-head">Vessel</span>
                                                                                            <span class="content content_description">
                                                                                                {{ isset($value['ship_name']) && !empty($value['ship_name']) ? $value['ship_name'] : '-'}}
                                                                                            </span>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-xs-6 col-sm-3">
                                                                                        <div class="discription">
                                                                                            <span class="content-head">Type</span>
                                                                                            <span class="content content_description">
                                                                                                @foreach( \CommonHelper::ship_type() as $c_index => $ship_type)
                                                                                                    {{ isset($value['ship_type']) ? $value['ship_type'] == $c_index ? $ship_type : '' : ''}}
                                                                                                @endforeach
                                                                                            </span>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-xs-6 col-sm-3">
                                                                                        <div class="discription">
                                                                                            <span class="content-head">Flag</span>
                                                                                            <span class="content content_description">
                                                                                                @if(isset($value['ship_flag']) && !empty($value['ship_flag']))
                                                                                                    @foreach( \CommonHelper::countries() as $c_index => $ship_flag)
                                                                                                        {{ isset($value['ship_flag']) ? $value['ship_flag'] == $c_index ? $ship_flag : '' : ''}}
                                                                                                    @endforeach
                                                                                                @else
                                                                                                    -
                                                                                                @endif
                                                                                            </span>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-xs-6 col-sm-3">
                                                                                        <div class="discription">
                                                                                            <span class="content-head">P & I Cover</span>
                                                                                            <span class="content content_description">
                                                                                                {{ isset($value['p_i_cover']) ? $value['p_i_cover'] == '1' ? 'Yes' : 'No' : ''}}
                                                                                            </span>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>

                                                                                <div class="row section_one p-t-10">
                                                                                    <div class="col-xs-6 col-sm-3">
                                                                                        <div class="discription">
                                                                                            <span class="content-head">GRT</span>
                                                                                            <span class="content content_description">
                                                                                                {{ isset($value['grt']) && !empty($value['grt']) ? $value['grt'] : '-'}}
                                                                                             </span>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-xs-6 col-sm-3">
                                                                                        <div class="discription">
                                                                                            <span class="content-head">BHP</span>
                                                                                            <span class="content content_description">
                                                                                                {{ isset($value['bhp']) && !empty($value['bhp']) ? $value['bhp'] : '-'}}
                                                                                            </span>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-xs-6 col-sm-6">
                                                                                        <div class="discription">
                                                                                            <span class="content-head">Engine Type</span>
                                                                                            <span class="content content_description">
                                                                                                @if(isset($value['engine_type']) && !empty($value['engine_type']))
                                                                                                    @foreach( \CommonHelper::engine_type() as $c_index => $engine_type)
                                                                                                        {{ isset($value['engine_type']) ? $value['engine_type'] == $c_index ? $engine_type : '' : ''}}
                                                                                                    @endforeach
                                                                                                @else
                                                                                                    -
                                                                                                @endif
                                                                                            </span>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>

                                                                                <?php
                                                                                $count = count($data[0]['company_registration_detail']['company_ship_details']);
                                                                                $index_count++;
                                                                                ?>

                                                                                @if( $count > $index_count)
                                                                                    <hr>
                                                                                @endif
                                                                            </div>
                                                                        @endforeach
                                                                    @else
                                                                        <div class="col-xs-12 text-center">
                                                                            <div class="discription">
                                                                                <span class="content-head">No Data Found.</span>
                                                                            </div>
                                                                        </div>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {{--profile center side--}}

                {{--profile right side--}}
                <?php $no_data = ''; ?>
                <div class="col-xs-12 col-sm-3">
                    <div class="sub-details-container company_team_details scrollbar">
                        <div class="content-container">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="title">Our Team</div>
                                </div>
                                @if(isset($data[0]['head_branch'][0]['team_members']) && !empty($data[0]['head_branch'][0]['team_members']))
                                <div class="col-xs-12 m-t-10">
                                    <div class="discription">
                                        <span class="content-head">HO {{ strToUpper(\CommonHelper::countries()[$data[0]['head_branch'][0]['country']]) }}</span>
                                    </div>
                                </div>
                                @endif
                            </div>
                            @if(isset($data[0]['head_branch'][0]['team_members']) && !empty($data[0]['head_branch'][0]['team_members']))
                                <div class="row team_information">
                                @foreach($data[0]['head_branch'][0]['team_members'] as $branch)
                                    <?php
                                        $image_path = '';
                                        if(isset($branch['profile_pic'])){
                                            $image_path = env('COMPANY_TEAM_PROFILE_PIC_PATH')."".$branch['id']."/".$branch['profile_pic'];
                                        }
                                        $no_data = 1;
                                    ?>
                                    <div class="col-xs-6 col-sm-12 col-md-6 team_description">
                                        <div class="discription text-center">
                                            <span class="content">
                                                @if(!empty($image_path))
                                                    <img class="preview" src="/{{ $image_path }}">
                                                @else
                                                    <img class="preview" src="/images/user_default_image.png">
                                                @endif
                                            </span>
                                            <span class="content-head">{{$branch['agent_name']}}</span>
                                            <span class="content-desg">{{$branch['agent_designation']}}</span>
                                        </div>
                                    </div>
                                @endforeach
                                </div>
                                <hr>
                            @endif
                        </div>

                        @if(isset($data[0]['branch']) && !empty($data[0]['branch']))
                            <div class="content-container">
                                @foreach($data[0]['branch'] as $branches)
                                    @if(isset($branches['team_members']) && !empty($branches['team_members']))
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <div class="discription">
                                                    <span class="content-head">Branch 
                                                        @if($branches['country'] == '95')
                                                            {{ strToUpper(\CommonHelper::countries()[$branches['country']]) }} {{ ',' }} {{ strToUpper($branches['state']['name']) }}
                                                        @else
                                                            {{ strToUpper(\CommonHelper::countries()[$branches['country']]) }} {{ ',' }} {{ strToUpper($branches['state_text']) }}
                                                        @endif
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row team_information">
                                            @foreach($branches['team_members'] as $branch)
                                                <?php
                                                    $image_path = '';
                                                    if(isset($branch['profile_pic'])){
                                                        $image_path = env('COMPANY_TEAM_PROFILE_PIC_PATH')."".$branch['id']."/".$branch['profile_pic'];
                                                    }
                                                    $no_data = 1;
                                                ?>
                                                <div class="col-xs-6 col-sm-12 col-md-6 team_description">
                                                    <div class="discription text-center">
                                                        <span class="content">
                                                            @if(!empty($image_path))
                                                                <img class="preview" src="/{{ $image_path }}">
                                                            @else
                                                                <img class="preview" src="/images/user_default_image.png">
                                                            @endif
                                                        </span>
                                                        <span class="content-head">{{$branch['agent_name']}}</span>
                                                        <span class="content-desg">{{$branch['agent_designation']}}</span>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    @endif
                                @endforeach
                                
                                @if(empty($no_data) || $no_data == '')
                                <div class="row">
                                    <div class="col-xs-12 text-center">
                                        <div class="discription">
                                            <span class="content-head">No Data Found.</span>
                                        </div>
                                    </div>
                                </div>
                                @endif
                            </div>
                        @endif
                    </div>
                    
                    <div class="sub-details-container company_team_details ship_and_country_details scrollbar">
                        <div class="content-container">
                            <div class="row ship_details_title">
                                <div class="col-xs-12">
                                    <div class="title">Our Appointed Associates / Managers</div>
                                </div>
                            </div>
                            <div class="row m-t-15">
                                @if(isset($data[0]['company_registration_detail']['appointed_agent']) && !empty($data[0]['company_registration_detail']['appointed_agent']))
                                    @foreach($data[0]['company_registration_detail']['appointed_agent'] as $agents)
                                    <?php
                                        $image_path = '';
                                        if(isset($agents['logo'])){
                                            $image_path = env('COMPANY_TEAM_AGENT_PIC_PATH')."".$agents['id']."/".$agents['logo'];
                                        }
                                    ?>
                                    <div class="col-xs-6 col-sm-12 section_two">
                                        <div class="discription">
                                            <div class="col-sm-12 col-md-4">
                                                <span class="content">
                                                    @if(!empty($image_path))
                                                        <img class="agent-preview" src="/{{ $image_path }}">
                                                    @else
                                                        <img class="agent-preview" src="/images/user_default_image.png">
                                                    @endif
                                                </span>
                                            </div>
                                            <div class="col-sm-12 col-md-8">
                                                <span class="content-head">{{ !empty($agents['to_company']) ? ucwords($agents['to_company']) : '-'}}</span>
                                                <span class="content-head city_heading">{{ !empty($agents['location']) ? ucwords($agents['location']) : '-'}}</span>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                @else
                                    <div class="sub-details-container" style="box-shadow:none;margin:0px;">
                                        <div class="content-container">
                                            <div class="row">
                                                <div class="col-xs-12 text-center">
                                                    <div class="discription">
                                                        <span class="content-head">No Data Found.</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            </div>
                        </div>

                        <hr>

                        <div class="content-container">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="title">We are Associates / Managers to</div>
                                </div>
                            </div>
                            <div class="row m-t-25">
                                @if(isset($data[0]['company_registration_detail']['associate_agent']) && !empty($data[0]['company_registration_detail']['associate_agent']))
                                    @foreach($data[0]['company_registration_detail']['associate_agent'] as $agents)
                                    <?php
                                        $image_path = '';
                                        if(isset($agents['logo'])){
                                            $image_path = env('COMPANY_TEAM_AGENT_PIC_PATH')."".$agents['id']."/".$agents['logo'];
                                        }
                                    ?>

                                    <div class="col-xs-6 col-sm-12 section_two">
                                        <div class="discription">
                                            <div class="col-sm-12 col-md-5">
                                                <span class="content">
                                                    @if(!empty($image_path))
                                                        <img class="agent-preview" src="/{{ $image_path }}">
                                                    @else
                                                        <img class="agent-preview" src="/images/user_default_image.png">
                                                    @endif
                                                </span>
                                            </div>
                                            <div class="ol-sm-12 col-md-7">
                                                <span class="content-head">{{ !empty($agents['from_company']) ? ucwords($agents['from_company']) : '-'}}</span>
                                                <span class="content-head city_heading">{{ !empty($agents['location']) ? ucwords($agents['location']) : '-'}}</span>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                @else
                                    <div class="sub-details-container" style="box-shadow:none;margin:0px;padding-bottom: 20px">
                                        <div class="content-container">
                                            <div class="row">
                                                <div class="col-xs-12 text-center">
                                                    <div class="discription">
                                                        <span class="content-head">No Data Found.</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                {{--profile right side--}}

            </div>

        </div>
    </div>
</div>

<div id="advModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Advertisement</h4>
            </div>
            <div class="modal-body">
                <img src="{{isset($img_path) ? $img_path : ''}}" class="img-responsive w-100">
            </div>
        </div>

    </div>
</div>