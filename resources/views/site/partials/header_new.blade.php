
<header class="coss_header z-depth-1">
    <nav class="navbar navbar-default coss_navbar">
        <div class="container width-100">
            <div class="navbar-header coss_navbar_logowrapper">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand coss_navbar_logo" href="{{route('home')}}">
                    <img src="{{ URL:: asset('public/assets/images/Course4Sea_logo.jpg')}}" class="">
                </a>
                @if(Auth::check() AND Auth::User()->registered_as != 'admin')
                <div class="notify_icon hidden-lg" style="position: relative;padding-top: 18px;display: inline-block;float: right;">
                    <div class="overlay_section"></div>
                    <div class="notification-count hide" data-notification="0"></div>
                    <i class="fa fa-bell notify_icon_bell" style="float: right;margin-right: 16px;"></i>
                    <div class="notification_wrapper" style="position: relative;">
                        <div class="dropdown-menu animated notification-main-view flipInY pointer" style="transform: translateX(-50%);top: 30px;">
                            <div class="notification-title">
                                Notifications
                            </div>
                            <div class="notification-content-view" id="notification">
                                
                            </div>
                        </div>
                    </div>
                </div>
                @endif
            </div>
            <div class="collapse navbar-collapse coss_navbar_collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right coss_navbar-right coss_nav">
                    @if(Auth::check() && Auth::user()->registered_as != 'seafarer')
                    <li class="coss_navMenuItem btn coss-primary-btn header-subscription-btn">
                        <a href="{{route('site.user.subscription')}}" class="coss_navMenuItem-Link">
                            Subscribe Now
                        </a>
                    </li>
                    @endif
                    <li class="coss_navMenuItem {{Route::currentRouteName() == 'home' ? 'active' : '' }}">
                        <a href="{{route('home')}}" class="coss_navMenuItem-Link">
                            Home
                        </a>
                    </li>

                    <!-- <li class="coss_navMenuItem {{Route::currentRouteName() == 'services' ? 'active' : '' }}"">
                        <a href="{{route('services')}}" class="coss_navMenuItem-Link">
                            Our Services
                        </a>
                    </li>
`
                    <li class="coss_navMenuItem {{Route::currentRouteName() == 'aboutUs' ? 'active' : '' }}">
                        <a href="{{route('aboutUs')}}" class="coss_navMenuItem-Link">
                            About Us
                        </a>
                    </li>

                    <li class="coss_navMenuItem">
                        <a class="coss_navMenuItem-Link pointer" data-toggle="modal" data-target="#contact-us-modal">
                            Contact Us
                        </a>
                    </li> -->

                    <!-- <li class="coss_navMenuItem">
                        <a href="" class="coss_navMenuItem-Link enquire_btn">
                            Enquire Now
                        </a>
                    </li> -->
                    @if(Auth::check() AND Auth::User()->registered_as != 'admin')
                        <?php 
                            $user_details = Auth::User()->toArray();
                            if($user_details['registered_as'] == 'company'){
                                $profile_path = 'site.show.company.details';
                            }
                            if($user_details['registered_as'] == 'seafarer'){
                                $profile_path = 'user.profile';
                            }
                            if($user_details['registered_as'] == 'advertiser'){
                                $profile_path = 'site.advertiser.profile';
                            }
                            if($user_details['registered_as'] == 'institute'){
                                $profile_path = 'site.show.institute.details';
                            }
                        ?>
                        <!-- <li class="coss_navMenuItem">
                            <a href="{{route('logout')}}" class="coss_navMenuItem-Link enquire_btn">
                                Logout
                            </a>
                        </li> -->
                    @else
                        <li class="coss_navMenuItem">
                            <a data-toggle="modal" data-target="#login-popup-modal" class="coss_navMenuItem-Link enquire_btn home_login_btn">
                                Login / Register
                            </a>
                        </li>
                    @endif

                    @if(Auth::check() AND Auth::User()->registered_as != 'admin')
                        
                        <li class="dropdown center">
                            <a href="#" class="dropdown-toggle dropdown_login" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">{{isset($user_details['first_name']) ? ucwords($user_details['first_name']) : ''}} <span class="caret"></span></a>
                                <ul class="dropdown-menu center">
                                @if(isset($profile_path) AND !empty($profile_path))
                                  <li><a class="center" href="{{route($profile_path)}}">My Profile</a></li><hd></hd>
                                @endif
                                  <li><a class="center" href="{{route('logout')}}">Log Out</a></li>
                                </ul>
                        </li>

                        <li class="hidden-xs hidden-sm hidden-md">
                            <div class="overlay_section"></div>
                            <a class="pointer notification_bell">
                                <div class="notification-count hide" data-notification="0"></div>
                                <i class="fa fa-bell"></i>
                                <div class="notification_wrapper" style="position: relative;">
                                    <div class="dropdown-menu animated notification-main-view pointer flipInY">
                                        <div class="notification-title">
                                            Notifications
                                        </div>
                                        <div class="notification-content-view" id="notification">
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </li>
                    @endif

                    <li class="coss_navMenuItem">
                        <a href="" class="social_icon">
                            <i class="fa fa-facebook"></i>
                        </a>
                        <a href="" class="social_icon">
                            <i class="fa fa-twitter"></i>
                        </a>
                        <a href="" class="social_icon">
                            <i class="fa fa-linkedin"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </div>  
    </nav>
</header>

