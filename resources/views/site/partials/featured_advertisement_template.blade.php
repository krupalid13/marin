
<div class="hide featured_advertise_section" id="featured_advertise_section">
    <div class="row">
        <div class="hidden-xs hidden-sm hidden-md col-lg-12 p-0">
            <div class="ad_card_box">
                <div class="loader"></div>                        
            </div>
        </div>
    </div>
</div>

<div id="enquire-advertisements-modal" class="modal extended-modal fade no-display" tabindex="-1" data-width="760">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="advertise-enquiry-form-container">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                      &times;
                    </button>
                    <div class="heading"> 
                        <h4 class="modal-title input-label">Advertisement Enquiry</h4>
                    </div>
                </div>
                <div class="modal-body enquire-advertisements-container">

                    <form id="advertisements-enquiry-modal-form" action="{{ route('site.advertisements.enquiry') }}">
                        {{csrf_field()}}
                        <input type="hidden" name="advertisement_id" id="advertisement_id" value="">
                        <input type="hidden" name="company_id" id="company_id" value="">
                        <div class="form-group">
                            <label class="input-label" for="name">Name</label>
                            <input type="text" class="form-control" id="name" name="name" value="{{isset($data['first_name']) ? $data['first_name'] : ''}} ">
                        </div>
                        <div class="form-group">
                            <label class="input-label" for="name">Phone Number</label>
                            <input type="text" class="form-control" id="phone" name="phone" value="{{isset($data['mobile'])?$data['mobile']:''}}">
                        </div>
                        <div class="form-group">
                            <label class="input-label" for="name">Email</label>
                            <input type="email" class="form-control" id="email" name="email" value="{{isset($data['email']) ? $data['email'] : ''}} ">
                        </div>
                        <div class="form-group">
                            <label class="input-label" for="name">Message</label>
                            <textarea class="form-control" rows="6" name="message" id="message"></textarea>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <div class="row right">
                        <div class="col-sm-12">
                            <button type="button" data-style="zoom-in" class="btn btn-blue btn-default ladda-button" id="send-advertise-enquiry-button">
                                Send Enquiry
                            </button>   
                        </div>
                    </div>  
                </div>
            </div>
        </div>
    </div>
</div>