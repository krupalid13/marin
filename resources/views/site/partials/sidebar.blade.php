<?php 
    if(Auth::check()){
        $institute_id = Auth::user()->id;
        $options['with'] = ['institute_registration_detail.institute_detail','institute_registration_detail','institute_registration_detail.institute_locations.state','institute_registration_detail.institute_locations.city','institute_registration_detail.institute_locations.pincode.pincodes_states.state','institute_registration_detail.institute_locations.pincode.pincodes_cities.city'];

        $userService = New App\Services\UserService();

        $data = $userService->getDataByUserId($institute_id, $options)->toArray();
    }
?>
<div class="sidebar">
        <nav class="sidebar-nav ps ps--active-y">
            <div class="brand">
                <a href="{{ route('site.show.institute.details') }}" class="brand-image">
                    @if(isset($data[0]['profile_pic']))
                        <img src="/{{ env('INSTITUTE_LOGO_PATH')}}{{$data[0]['id']}}/{{$data[0]['profile_pic']}}" alt="">
                    @else
                        <img src="/images/user_default_image.png" alt="">
                    @endif
                </a>
                <div class="brand-title">{{ isset($data[0]['institute_registration_detail']['institute_name']) ? strtoupper($data[0]['institute_registration_detail']['institute_name']) : ''}}</div>
            </div>
            <ul class="nav">
                <li class="nav-item">
                    <a href="{{ route('site.show.institute.details') }}" class="nav-link {{Route::currentRouteName() == 'site.show.institute.details' ? 'nav-active' : '' }}">
                        <i class=""></i>
                        <span class="">Profile</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('site.institute.view.notification') }}" class="nav-link {{Route::currentRouteName() == 'site.institute.view.notification' ? 'nav-active' : '' }}">
                        <i class=""></i>
                        <span class="">Notifications</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('site.institute.course.listing') }}" class="nav-link {{Route::currentRouteName() == 'site.institute.course.listing' ? 'nav-active' : '' }}">
                        <i class=""></i>
                        <span class="">Courses</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('site.institute.list.batch.applicant') }}" class="nav-link {{Route::currentRouteName() == 'site.institute.list.batch.applicant' ? 'nav-active' : '' }}">
                        <i class=""></i>
                        <span class="">Booked Courses</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('site.institute.batch.listing') }}" class="nav-link {{Route::currentRouteName() == 'site.institute.batch.listing' ? 'nav-active' : '' }}">
                        <i class=""></i>
                        <span class="">Batches</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('site.institute.on.demand.booking') }}" class="nav-link {{Route::currentRouteName() == 'site.institute.on.demand.booking' ? 'nav-active' : '' }}">
                        <i class=""></i>
                        <span class="">On Demand Batches</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('site.institute.course.batches') }}" class="nav-link {{Route::currentRouteName() == 'site.institute.course.batches' ? 'nav-active' : '' }}">
                        <i class=""></i>
                        <span class="">Add Batches</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('site.institute.add.advertise') }}" class="nav-link {{Route::currentRouteName() == 'site.institute.add.advertise' ? 'nav-active' : '' }}">
                        <i class=""></i>
                        <span class="">Advertise Here</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('site.institute.add.notice') }}" class="nav-link {{Route::currentRouteName() == 'site.institute.add.notice' ? 'nav-active' : '' }}">
                        <i class=""></i>
                        <span class="">Notice/News</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('site.institute.add.image.gallery') }}" class="nav-link {{Route::currentRouteName() == 'site.institute.add.image.gallery' ? 'nav-active' : '' }}">
                        <i class=""></i>
                        <span class="">Create Gallery</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('site.institute.list.permission.requests') }}" class="nav-link {{Route::currentRouteName() == 'site.institute.list.permission.requests' ? 'nav-active' : '' }}">
                        <i class=""></i>
                        <span class="">User Documents</span>
                    </a>
                </li>
            </ul>
        </nav>
    </div>