<?php $index = 0;$doc = 100; ?>

<?php
	if(isset($user_uploaded_documents) && !empty($user_uploaded_documents)){
		$all_user_uploaded_documents = $user_uploaded_documents;
	}else{
		$all_user_uploaded_documents = [];
	}

	$active_class = "active";
	$active_class_tab = "active";
	$all_user_uploaded_documents = json_encode($all_user_uploaded_documents);
?>
<script type="text/javascript">
	var documents = <?php echo $all_user_uploaded_documents ?>;
</script>
<div class="row">
	<div class="document_slide">
		@foreach($documents as $key => $value)
			@if(isset($documents[$key][0]) AND !empty($documents[$key][0]))
				<?php
					$hide = '';
					if(isset($documents[$key][0]['hide'])){
						$hide = 'hide';
					}
					// dd($user_uploaded_documents[$key][$document_count+1],$public_document);

				?>
				@if($hide != 'hide')
					<div class="document_slide_tab documents-tab {{$active_class_tab}}" data-block={{$key}}-block>
						<div class="document_tab_heading">
							{{ isset($documents[$key][0]['title']) ? strtoupper($documents[$key][0]['title']) : ''}}
						</div>
						<!-- <span class="doc_count">
							{{count($documents[$key])}} Documents Uploaded
						</span>	 -->
					</div>
				@endif

			@else
				<?php
					$hide = '';
					if(isset($value['hide'])){
						$hide = 'hide';
					}
				?>
				@if($hide != 'hide')
					<div class="document_slide_tab documents-tab {{$active_class_tab}}" data-block={{$key}}-block>
						<div class="document_tab_heading">
							{{ isset($value['title']) ? strtoupper($value['title']) : ''}}
						</div>
					</div>
				@endif
			@endif

			<?php  $active_class_tab = ""; ?>
		@endforeach
	</div>
</div>

@foreach($documents as $key => $value)
	@if(isset($documents[$key][0]) AND !empty($documents[$key][0]))

		<?php
			$hide = '';
			if(isset($documents[$key][0]['hide'])){
				$hide = 'hide';
			}
			// dd($user_uploaded_documents[$key][$document_count+1],$public_document);

		?>
		<div class="doc-section {{$key}}-block {{$active_class}}">
			@if($hide != 'hide')
			<div class="row {{$hide}}">
				<div class="col-xs-12 upload-title-section">
					<div class="title">{{ isset($documents[$key][0]['title']) ? strtoupper($documents[$key][0]['title']) : ''}}
					</div>
				</div>
			</div>
			@endif
			@foreach($documents[$key] as $document_count => $details)
				<?php
					$private_document = false;
					$public_document = false;

					if(isset($user_uploaded_documents[$key][$document_count+1]['status']) && $user_uploaded_documents[$key][$document_count+1]['status'] == '1'){
						$public_document = true;
					}else{
						$private_document = true;
					}
					// dd($user_uploaded_documents[$key][$document_count+1],$public_document);
					$extra_details = '';

					if($key == 'cdc' && isset($details['cdc']) && !empty($details['cdc'])){
						$extra_details = \CommonHelper::countries()[$details['cdc']];
					}
					elseif($key == 'coc'){
						$extra_details = $details['coc_grade'];
					}
					elseif($key == 'coe' && isset($details['coe']) && !empty($details['coe'])){
						$extra_details = \CommonHelper::countries()[$details['coe']];
					}
					elseif($key == 'courses' && isset($details['course_id']) && !empty($details['course_id'])){
						$extra_details = \CommonHelper::courses()[$details['course_id']];
					}
					else{
						$extra_details = '';
					}

				?>
				<form class="dropzone" id="real-dropzone{{$doc}}" files="true" action="{{route('site.user.upload',['type' => $key,'type_id' => $document_count+1])}}">
					{{ csrf_field() }}
					<input type="hidden" class="doc_index" value={{$doc}}>
					<div class="main_dropzone_view">
						<div class="row">
							<div class="col-xs-12 col-sm-9 is_flex title">
								<span class="icon-check tick-mark icon"></span>
								<div class="heading p-t-0">
									{{strToUpper($key)}} {{$document_count+1}}
								</div>
								@if(isset($extra_details) && !empty($extra_details))
									<span> - </span>
									<span>
										{{ isset($extra_details) ? ucwords($extra_details) : '' }}
									</span>
								@endif
							</div>
							<div class="col-xs-12 col-sm-3 dz-message-div">
								<div class="section1" style="position: relative;">
									<div class="dz-default dz-message">
										<div class="view_dropzone">Upload</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row main-dropzone-row">
							<div class="col-xs-12 col-sm-8">
								<div class="fallback">
									<input name="file" type="file" multiple />
								</div>
								<div id="view_dropzone_files{{$doc}}">
									{{--<div class="dz-image">
										<a class="image_dropzone_link" href="">
										</a>
									</div>--}}
								</div>
							</div>
							<div class="col-xs-12 col-sm-4">
								<div class="section radio-section1">

									<div class="permission_checkbox radio-section">
										<label class="radio-inline">
											<input type="radio" class="doc-permission-radio-btn" name="{{$key}}" data-name="{{$key}}" value="1" data-type-id="{{$document_count+1}}" {{ $public_document ? 'checked=checked' : ''}} /> Public
										</label>

										<label class="radio-inline">
											<input type="radio" class="doc-permission-radio-btn" name="{{$key}}" data-name="{{$key}}" value="0" {{ $private_document ? 'checked=checked' : ''}} data-type-id="{{$document_count+1}}"> Private
										</label>
									</div>
								</div>
							</div>
						</div>
					</div>
				</form>
				<script type="text/javascript">

						Dropzone.autoDiscover = false;

						var index = '{{$doc}}';
						var type = '{{$key}}';
						var type_id = '{{$document_count+1}}';
						var container = "#view_dropzone_files"+index;
						var name = "real-dropzone"+index;
						var photo_counter = 0;
						var user_id = '{{$data['id']}}';

						var myDropzone = new Dropzone("#"+name, {
					        uploadMultiple: false,
					        parallelUploads: 100,
					        maxFilesize: 5,
					        previewsContainer: container,
					        // previewTemplate: document.querySelector('#preview-template').innerHTML,
					        addRemoveLinks: true,
					        dictRemoveFile: 'Remove',
					        dictFileTooBig: 'Image is bigger than 5MB',
					        // The setting up of the dropzone
					        init:function() {

					        	this.on("thumbnail", function(file) {
					        		var url = "{{ route('site.user.view.documents.path',[$data['id'],$key,$document_count+1]) }}";
									var image_path = url+'/'+file.image;

									$(file.previewTemplate).attr('data-img', file.image);
					        		$(file.previewTemplate).attr('data-img-id', file.image_id);
					        		$(file.previewTemplate).attr('data-img-type', file.image_type);
					        		$(file.previewTemplate).attr('data-img-type-id', file.image_type_id);
					        		$(file.previewTemplate).attr('data-img-download', image_path);
					        	});

					        	var docsDropzone = this;

					        	if(documents){
					        		$.each(documents, function (key,value){

					        			if(type == key){

											if(value[type_id]){
						        				$.each(value[type_id]['user_documents'], function(k,v){

						        					var extension = v.document_path.split('.').pop();

						        					if(extension == 'jpg' || extension == 'jpeg'){
						        						var logo = '/images/jpg.jpg';
						        					}
						        					else if(extension == 'pdf'){
						        						var logo = '/images/pdf.png';
						        					}
						        					else if(extension == 'gif'){
						        						var logo = '/images/gif.png';
						        					}
						        					else if(extension == 'png'){
						        						var logo = '/images/png.png';
						        					}
						        					else if(extension == 'doc' || extension == 'docx'){
						        						var logo = '/images/doc.png';
						        					}else {
						        						var logo = '/images/file.png';
						        					}

								        			var mockFile = { name: logo, size: 0, status: 'success' , image:v.document_path , image_id: v.id, image_type: type , image_type_id:type };

													docsDropzone.emit("addedfile", mockFile );
													docsDropzone.emit("thumbnail", mockFile, logo );
													mockFile.previewElement.classList.add('dz-success');
													mockFile.previewElement.classList.add('dz-complete');
						        				});
						        			}
					        			}

					        		});
					        	}

					            this.on("removedfile", function(file) {
					            	var file_name = $(file.previewTemplate).attr('data-img');
				        			var image_id = $(file.previewTemplate).attr('data-img-id');
				        			var type = $(file.previewTemplate).attr('data-img-type');
				        			var type_id = $(file.previewTemplate).attr('data-img-type-id');

					                $.ajax({
					                    type: 'POST',
					                    url: $("#api-seafarer-delete-document").val(),
					                    headers: {
					                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content'),
					                    },
					                    data: {name: file_name, id: image_id, type: type, type_id: type_id, _token : $('meta[name="_token"]').attr('content')},
					                    dataType: 'html',
					                    success: function(data){
					                        var rep = JSON.parse(data);
					                        if(rep.code == 200)
					                        {
					                            photo_counter--;
					                            $("#photoCounter").text( "(" + photo_counter + ")");
					                        }

					                        toastr.success(rep.message, 'Success');

					                    }
					                });

					            } );
					        },
					        error: function(file, response) {
					            if($.type(response) === "string")
					                var message = response; //dropzone sends it's own error messages in string
					            else
					                var message = response.message;
					                file.previewElement.classList.add("dz-error");
					                _ref = file.previewElement.querySelectorAll("[data-dz-errormessage]");
					                _results = [];
					                for (_i = 0, _len = _ref.length; _i < _len; _i++) {
					                    node = _ref[_i];
					                    _results.push(node.textContent = message);
					                }
					            return _results;
					        },
					        success: function(file,response) {
								var url = "{{ route('site.user.view.documents.path',[$data['id'],$key,$document_count+1]) }}";
								var image_path = url+'/'+response.filename;

					        	toastr.success(response.message, 'Success');
					        	$(file.previewTemplate).attr('data-img',response.filename);
				        		$(file.previewTemplate).attr('data-img-id',response.id);
				        		$(file.previewTemplate).attr('data-img-type',response.type);
				        		$(file.previewTemplate).attr('data-img-type-id',response.type_id);
								$(file.previewTemplate).attr('data-img-download', image_path);
					            photo_counter++;
					            $("#photoCounter").text( "(" + photo_counter + ")");
					        }
					    });

				</script>
				<?php $doc++; ?>
			@endforeach
		</div>
	@else

		<?php
			$private_document = false;
			$public_document = false;

			if(isset($user_uploaded_documents[$key][0]['status']) && $user_uploaded_documents[$key][0]['status'] == '1'){
				$public_document = true;
			}else{
				$private_document = true;
			}

			$hide = '';
			if(isset($value['hide'])){
				$hide = 'hide';
				$active_class = '';
			}

			if($key == 'passport' && isset($documents['passport']) && !empty($documents['passport'])){
				$extra_details = \CommonHelper::countries()[$documents['passport']['pass_country']];
			}
			elseif($key == 'visa' && isset($documents['visa']) && !empty($documents['visa'])){
				$extra_details = \CommonHelper::countries()[$documents['visa']['pass_country']];
			}
			elseif($key == 'watch keeping' && isset($documents['watch keeping']) && !empty($documents['watch keeping'])){
				$extra_details = $documents['watch keeping']['type'];
			}
			else{
				$extra_details = '';
			}
		?>

		<div class="doc-section {{$key}}-block {{$active_class}}">
			@if($hide != 'hide')
			<div class="row {{$hide}}">
				<div class="col-xs-12 upload-title-section">
					<div class="title">{{ isset($value['title']) ? strtoupper($value['title']) : ''}}
					</div>
				</div>
			</div>
			@endif
			<form class="dropzone" id="real-dropzone{{$index}}" files="true" action="{{route('site.user.upload',['type' => $key])}}">
				{{ csrf_field() }}
				<input type="hidden" class="index" value={{$index}}>
				<div class="main_dropzone_view">
					<div class="row">
						<div class="col-xs-12 col-sm-6 is_flex title">
							<span class="icon-check tick-mark icon"></span>
							<div class="heading p-t-0">
								{{strToUpper($key)}}
							</div>
							@if(isset($extra_details) && !empty($extra_details))
								<span> - </span>
								<span>
									{{ isset($extra_details) ? ucwords($extra_details) : '' }}
								</span>
							@endif
						</div>
						<div class="col-xs-12 col-sm-6 dz-message-div">
							<div class="section1" style="position: relative;">
								<div class="dz-default dz-message">
									<div class="view_dropzone">Upload</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="row main-dropzone-row">
					<div class="col-xs-12 col-sm-8">
						<div class="fallback">
							<input name="file" type="file" multiple />
						</div>
						<div id="view_dropzone_files{{$index}}">
							<a href=""></a>
						</div>
					</div>
					<div class="col-xs-12 col-sm-4">
						<div class="section radio-section1">
							<div class="permission_checkbox radio-section">
								<label class="radio-inline">
									<input type="radio" class="doc-permission-radio-btn" name="{{$key}}" data-name="{{$key}}" value="1" {{ $public_document ? 'checked=checked' : ''}}> Public
								</label>

								<label class="radio-inline">
									<input type="radio" class="doc-permission-radio-btn" name="{{$key}}" data-name="{{$key}}" value="0" {{ $private_document ? 'checked=checked' : ''}}> Private
								</label>
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>
		<script type="text/javascript">

				Dropzone.autoDiscover = false;

				var index = '{{$index}}';
				var type = '{{$key}}';
				var container = "#view_dropzone_files"+index;
				var name = "real-dropzone"+index;
				var user_id = '{{$data['id']}}';

				var photo_counter = 0;

				var myDropzone = new Dropzone("#"+name, {
			        uploadMultiple: false,
			        parallelUploads: 100,
			        maxFilesize: 5,
			        previewsContainer: container,
			        // previewTemplate: document.querySelector('#preview-template').innerHTML,
			        addRemoveLinks: true,
			        dictRemoveFile: 'Remove',
			        dictFileTooBig: 'Image is bigger than 5MB',
			        // The setting up of the dropzone
			        init:function() {

			        	this.on("thumbnail", function(file) {
							
							var url = "{{ route('site.user.view.documents.path',[$data['id'],$key,0]) }}";
							var image_path = url+'/'+file.image;

			        		$(file.previewTemplate).attr('data-img', file.image);
			        		$(file.previewTemplate).attr('data-img-id', file.image_id);
			        		$(file.previewTemplate).attr('data-img-type', file.image_type);
							$(file.previewTemplate).attr('data-img-download', image_path);
			        	});

			        	var docsDropzone = this;

			        	if(documents){
			        		$.each(documents, function (key,value){

			        			if(type == key){

			        				$.each(value[0]['user_documents'], function(k,v){

			        					var extension = v.document_path.split('.').pop();

			        					if(extension == 'jpg' || extension == 'jpeg'){
			        						var logo = '/images/jpg.jpg';
			        					}
			        					else if(extension == 'pdf'){
			        						var logo = '/images/pdf.png';
			        					}
			        					else if(extension == 'gif'){
			        						var logo = '/images/gif.png';
			        					}
			        					else if(extension == 'png'){
			        						var logo = '/images/png.png';
			        					}
			        					else if(extension == 'doc' || extension == 'docx'){
			        						var logo = '/images/doc.png';
			        					}else {
			        						var logo = '/images/file.png';
			        					}

					        			var mockFile = { name: logo, size: 0, status: 'success' , image:v.document_path , image_id: v.id, image_type: type };

										docsDropzone.emit("addedfile", mockFile );
										docsDropzone.emit("thumbnail", mockFile, logo );
										mockFile.previewElement.classList.add('dz-success');
										mockFile.previewElement.classList.add('dz-complete');
			        				});
			        			}

			        		});
			        	}

			            this.on("removedfile", function(file) {
			        		var file_name = $(file.previewTemplate).attr('data-img');
			        		var image_id = $(file.previewTemplate).attr('data-img-id');
			        		var type = $(file.previewTemplate).attr('data-img-type');

			                $.ajax({
			                    type: 'POST',
			                    url: $("#api-seafarer-delete-document").val(),
			                    headers: {
			                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content'),
			                    },
			                    data: {name: file_name, id: image_id, type: type, _token : $('meta[name="_token"]').attr('content')},
			                    dataType: 'html',
			                    success: function(data){
			                        var rep = JSON.parse(data);
			                        if(rep.code == 200)
			                        {
			                            photo_counter--;
			                            $("#photoCounter").text( "(" + photo_counter + ")");
			                        }

			                        toastr.success(rep.message, 'Success');
			                    }
			                });

			            });
			        },
			        error: function(file, response) {
			            if($.type(response) === "string")
			                var message = response; //dropzone sends it's own error messages in string
			            else
			                var message = response.message;
			                file.previewElement.classList.add("dz-error");
			                _ref = file.previewElement.querySelectorAll("[data-dz-errormessage]");
			                _results = [];
			                for (_i = 0, _len = _ref.length; _i < _len; _i++) {
			                    node = _ref[_i];
			                    _results.push(node.textContent = message);
			                }
			            return _results;
			        },
			        success: function(file,response) {
			        	toastr.success(response.message, 'Success');

			        	var url = "{{ route('site.user.view.documents.path',[$data['id'],$key,0]) }}";
						var image_path = url+'/'+response.filename;

						$(file.previewTemplate).attr('data-img',response.filename);
			        	$(file.previewTemplate).attr('data-img-id',response.id);
			        	$(file.previewTemplate).attr('data-img-type',response.type);
						$(file.previewTemplate).attr('data-img-download', image_path);

			            photo_counter++;
			            $("#photoCounter").text( "(" + photo_counter + ")");
			        }
			    });

		</script>
		<?php $index++; ?>
	@endif
	<?php $active_class= "hide"; ?>
@endforeach