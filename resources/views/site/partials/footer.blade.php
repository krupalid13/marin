<footer class="coss_footer">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-5 col-md-6">
				<div class="powered_by">
					<div class="powered_by_content">
						<span>Course4sea <i class="fa fa-copyright" aria-hidden="true"></i> {{date('Y')}} All Rights Reserved</span>
					</div>
				</div>
			</div>
			<!-- <div class="col-xs-12 col-sm-7 col-md-6">
				<div class="rights_bar">
					 <a href="{{route('tnc')}}">Terms & condition</a>  <a href="{{route('privacy_policy')}}">Privacy policy</a> <a href="{{route('disclaimer')}}">Disclaimer</a> <a href="{{route('faq')}}">FAQs</a>
				</div>
			</div> -->
		</div>
	</div>
</footer>