<header class="cs-header">
    <div class="container"> 
        <nav class="navbar navbar-default z-depth-1">
            <div class="navbar-header">
                <a class="navbar-brand" href="#">
                    <img alt="coursesea logo" src="{{ URL:: asset('public/assets/images/coursealogo.png')}}">
                </a>
            </div>
            <div class="navbar-right">
                <button class="btn cs-primary-btn navbar-btn">Login</button>
            </div>
        </nav>
    </div>
</header>