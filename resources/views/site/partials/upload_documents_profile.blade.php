<?php $index = 0;$doc = 100; ?>

<?php
	if(isset($user_uploaded_documents) && !empty($user_uploaded_documents)){
		$all_user_uploaded_documents = $user_uploaded_documents;
	}else{
		$all_user_uploaded_documents = [];
	}

	$active_class = "active";
	$active_class_tab = "active";
	$all_user_uploaded_documents = json_encode($all_user_uploaded_documents);
	//echo '<pre>';
	//print_r($user_uploaded_documents);
	
	
	
?>
<style>
.nav-pills .nav-link.active, .nav-pills .show>.nav-link {
    color: #fff;
    background-color: #2a55a2;
}
.nav-link{
	height:56px;
	text-align: center;
	
}
.nav-link hr{
	padding: 0px;
    margin: 0px;
}

.nav-pills > li.active > a, .nav-pills > li.active > a:hover, .nav-pills > li.active > a:focus {
    color: #fff;
    background-color: #2a55a2;
}
.nav > li > a {
    position: relative;
    display: block;
    padding: 10px 13px;
	    background-color: #fff;
    color: #2a55a2;
}
.nav-pills > li + li {
    margin-left: 3px;
}
/* Styles the lightbox, removes it from sight and adds the fade-in transition */

.lightbox-target {
position: fixed;
top: -100%;
width: 100%;
background: rgba(0,0,0,.7);
width: 100%;
opacity: 0;
-webkit-transition: opacity .5s ease-in-out;
-moz-transition: opacity .5s ease-in-out;
-o-transition: opacity .5s ease-in-out;
transition: opacity .5s ease-in-out;
overflow: hidden;
}

/* Styles the lightbox image, centers it vertically and horizontally, adds the zoom-in transition and makes it responsive using a combination of margin and absolute positioning */

.lightbox-target img {
margin: auto;
position: absolute;
top: 0;
left:0;
right:0;
bottom: 0;
max-height: 0%;
max-width: 0%;
border: 3px solid white;
box-shadow: 0px 0px 8px rgba(0,0,0,.3);
box-sizing: border-box;
-webkit-transition: .5s ease-in-out;
-moz-transition: .5s ease-in-out;
-o-transition: .5s ease-in-out;
transition: .5s ease-in-out;
}

/* Styles the close link, adds the slide down transition */

a.lightbox-close {
display: block;
width:50px;
height:50px;
box-sizing: border-box;
background: white;
color: black;
text-decoration: none;
position: absolute;
top: -80px;
right: 0;
-webkit-transition: .5s ease-in-out;
-moz-transition: .5s ease-in-out;
-o-transition: .5s ease-in-out;
transition: .5s ease-in-out;
}

/* Provides part of the "X" to eliminate an image from the close link */

a.lightbox-close:before {
content: "";
display: block;
height: 30px;
width: 1px;
background: black;
position: absolute;
left: 26px;
top:10px;
-webkit-transform:rotate(45deg);
-moz-transform:rotate(45deg);
-o-transform:rotate(45deg);
transform:rotate(45deg);
}

/* Provides part of the "X" to eliminate an image from the close link */

a.lightbox-close:after {
content: "";
display: block;
height: 30px;
width: 1px;
background: black;
position: absolute;
left: 26px;
top:10px;
-webkit-transform:rotate(-45deg);
-moz-transform:rotate(-45deg);
-o-transform:rotate(-45deg);
transform:rotate(-45deg);
}

/* Uses the :target pseudo-class to perform the animations upon clicking the .lightbox-target anchor */

.lightbox-target:target {
opacity: 1;
top: 0;
bottom: 0;
left: 0;
z-index: 999;
}

.lightbox-target:target img {
max-height: 100%;
max-width: 100%;
}

.lightbox-target:target a.lightbox-close {
top: 0px;
}
.blockOverlay{z-index:99999 !important';}
.form-container {
    background: #F0F0F0;
    border: #e0dfdf 1px solid;
    padding: 20px;
    border-radius: 2px;
    width: 550px;
    height: 450px;
}
</style>

<div class="row">
							    <div class="col-xs-12">
									<ul class="nav nav-pills nav-fill navtop">
									    
									    <?php
                                         
									   if(count($user_uploaded_documents['photo'][0]['user_documents']))
									   {
										if(isset($documents['profile_pic'])){?>
										<li class="nav-item">
											<a class="nav-link active" href="#menu1" data-toggle="tab">PHOTO</a>
										</li>
										<?php }} ?>
										
										<li class="nav-item" id="passport_visa_menu2">
											<a class="nav-link" href="#menu2" data-toggle="tab">PASSPORT<hr>VISA</a>
										</li>
										
										
											
										<li class="nav-item">
											<a class="nav-link" href="#menu3" data-toggle="tab">INDOS<hr>CDC</a>
										</li>
										<?php if(@count($documents['sea service']))
										     {?>
											
										<li class="nav-item" id="sea_service">
											<a class="nav-link" href="#menu8" data-toggle="tab">SEA<hr>SERVICES</a>
										</li>
											<?php }?>
										<?php if(@count($documents['watch keeping']))
										{?>
											<li class="nav-item" id="wk_cop_menu4">
												<a class="nav-link" href="#menu4" data-toggle="tab">WK<hr>COP</a>
											</li>
										<?php }?>
										<?php if(@count($documents['coc']))
										{?>
										<li class="nav-item" id="coc_coe_menu5">
											<a class="nav-link" href="#menu5" data-toggle="tab">COC<hr>COE</a>
										</li>
										<?php }?>
										@if(isset($documents['user_dangerous_cargo_endorsement_detail']))
											<li class="nav-item">
												<a class="nav-link" id="menu12_" href="#menu12" data-toggle="tab">DCE</a>
											</li>
										@endif
										<?php if(@count($documents['gmdss']))
										{?>
											<li class="nav-item" id="gmdss_menu6">
												<a class="nav-link" href="#menu6" data-toggle="tab">GM<hr>DSS</a>
											</li>
										<?php } ?>
										
										<li class="nav-item" id="course_menu7">
											<a class="nav-link" href="#menu7" data-toggle="tab">COURSE<hr>CERTIFICATE</a>
										</li>
										<?php if(@$documents['yellow fever']['yellow_fever']==1 || @$documents['yellow fever']['ilo_medical']==1)
										{?>
										<li class="nav-item" id="medicals_vicination_menu9">
											<a class="nav-link" href="#menu9" data-toggle="tab">MEDICALS<hr>VICINATION</a>
										</li>
										<?php } ?>
										<?php if(@$documents['professional_detail']['school_qualification']!='')
										{?>
										
										<li class="nav-item">
											<a class="nav-link" href="#menu10" data-toggle="tab">ACADEMICS<hr>CERTIFICATE</a>
										</li>
										<?php }?>
										<?php if(@$documents['personal_detail']['account_no']!='' || @$documents['personal_detail']['aadhaar']!='' || @$documents['personal_detail']['pancard'])
										{?>
										
										<li class="nav-item">
											<a class="nav-link" href="#menu11" data-toggle="tab">BANK DETAILS</a>
										</li>
										<?php } ?>
									</ul>
									<div class="tab-content">
									   <?php  
									   if(count($user_uploaded_documents['photo'][0]['user_documents']))
									   {
										$private_document = false;
										$public_document = false;
                                        //print_r($user_uploaded_documents['photo'][0]);
										if(isset($user_uploaded_documents['photo'][0]['status']) && $user_uploaded_documents['photo'][0]['status'] == '1'){
											$public_document = true;
										}else{
											$private_document = true;
										}

										$hide = '';
										if(isset($value['hide'])){
											$hide = 'hide';
											$active_class = '';
										}?>
										<div class="tab-pane active" role="tabpanel" id="menu1" >
										<div class="row" style="margin-left: 1px;margin-top: 10px;">
											<div class="col-sm-12" style="background: #2a55a2;color: #fff;border-radius: 4px;padding: 10px;font-size: 16px;">
											    PHOTO
											</div>
										</div>
										<div class="row add-more-section" id="profile_photo">
										     <div class="col-xs-6 col-sm-9">
													<span style="color:#47c4f0;float: left;"><i class="fa fa-check-circle-o" style="font-size: 22px;"></i></span>
													<span style="margin-left: 10px;padding-top: 2px;float: left;color: #47c4f0;">PHOTO</span>		
											</div>
											
										    <div class="col-xs-12 col-sm-3">
											       
												<div class="form-group pull-right">Visibility:					
													<?php if($private_document){ echo 'Public';}else{ echo 'Public';}?>
													
												</div>		
											</div>
											
											
											<div class="col-xs-12 col-sm-12">
											    <?php $i=1;$photo_count=0; foreach($user_uploaded_documents['photo'][0]['user_documents'] as $arrProfilePic){ $photo_count++;?>
												<div class="col-xs-12 col-sm-2">												
													 <div class="avatar-upload">
													    <div class="avatar-delete delete_doc" data-id="<?php echo $arrProfilePic['id'];?>" data-imagename="<?php echo $arrProfilePic['document_path'];?>" data-type="photo">
															<label for="imageUpload"></label>
														</div>
														<!--<div class="avatar-edit edit_doc" data-id="<?php echo $arrProfilePic['id'];?>" data-imagename="<?php echo $arrProfilePic['document_path'];?>" data-type="photo" data-container="photo-<?php echo $i;?>">
															<label for="imageUpload"></label>
														</div>-->
														<a class="lightbox" href="#preview-photo-<?php echo $i;?>">
														<div class="avatar-view" data-id="<?php echo $arrProfilePic['id'];?>" data-imagename="<?php echo $arrProfilePic['document_path'];?>" data-type="photo">
														    
															<label for="imageUpload"></label>
														</div>
														</a>
														<?php if(isset($arrProfilePic['document_path']))
														{ 
													      $storage_path = env('DOCUMENT_STORAGE_PATH');
													      $type='photo';
														  $url=env('APP_URL').'/'.$storage_path . $user_id ."/". $type."/".$arrProfilePic['document_path'];?>
														
														<div class="avatar-preview" >
															<div class="avatar-edit-img" id="photo-<?php echo $i;?>">
															
															<img src="{{$url}}" class="img-thumbnail">
															
															</div>
															<div class="lightbox-target" id="preview-photo-<?php echo $i;?>">
															   <img src="{{$url}}"/>
															   <a class="lightbox-close" href="#"></a>
															</div>
															
														</div>
														<?php } ?>
													</div>
												</div>
												<?php $i++;}
												
												?>
											
											</div>
													
										</div>
										
											
									    </div>				
									   <?php } ?>
										<div class="tab-pane" role="tabpanel" id="menu2" <?php if(!count($user_uploaded_documents['passport'][0]['user_documents']) && !count($user_uploaded_documents['us_visa'][0]['user_documents'])){ echo 'style="display:none;"';}?>>
											<div class="row" style="margin-left: 1px;margin-top: 10px;">
												<div class="col-sm-12" style="background: #2a55a2;color: #fff;border-radius: 4px;padding: 10px;font-size: 16px;">
													PASSPORT | VISA
												</div>
											</div>
										
											
										    
												<?php if(@count($documents['passport']))
										{
										    	$private_document = false;
										$public_document = false;
                                        
										if(isset($user_uploaded_documents['passport'][0]['status']) && $user_uploaded_documents['passport'][0]['status'] == '1'){
											$public_document = true;
										}else{
											$private_document = true;
										}

										
											
											?>
												
												
										
										<div class="row add-more-section" id="profile_passport" <?php if(!count($user_uploaded_documents['passport'][0]['user_documents'])){ echo 'style="display:none;"';} ?>>
										     <div class="col-xs-6 col-sm-6">
													<span style="color:#47c4f0;float: left;"><i class="fa fa-check-circle-o" style="font-size: 22px;"></i></span>
													<span style="margin-left: 10px;padding-top: 2px;float: left;color: #47c4f0;">PASSPOSRT - <?php echo \CommonHelper::countries()[$documents['passport']['pass_country']];?></span>		
											</div>
											
										    <div class="col-xs-12 col-sm-6">
											       
												<div class="form-group pull-right">Visibility:					
													<?php if($private_document){ echo 'Public';}else{ echo 'Public';}?>
													
												</div>		
											</div>
											
											
											<div class="col-xs-12 col-sm-12">
											    <?php $i=1;$passport_count=0; foreach($user_uploaded_documents['passport'][0]['user_documents'] as $arrAadhaar){ $passport_count++;?>
												<div class="col-xs-12 col-sm-2">												
													 <div class="avatar-upload">
													    <div class="avatar-delete delete_doc" data-id="<?php echo $arrAadhaar['id'];?>" data-imagename="<?php echo $arrAadhaar['document_path'];?>" data-type="passport">
															<label for="imageUpload"></label>
														</div>
														
														<a class="lightbox" href="#preview-passport-<?php echo $i;?>">
														<div class="avatar-view" data-id="<?php echo $arrAadhaar['id'];?>" data-imagename="<?php echo $arrAadhaar['document_path'];?>" data-type="passport">
														    
															<label for="imageUpload"></label>
														</div>
														</a>
														<?php if(isset($arrAadhaar['document_path']))
														{ 
													      $storage_path = env('DOCUMENT_STORAGE_PATH');
													      $type='passport';
														  $url=env('APP_URL').'/'.$storage_path . $user_id ."/". $type."/".$arrAadhaar['document_path'];?>
														
														<div class="avatar-preview" >
															<div class="avatar-edit-img" id="passport-<?php echo $i;?>">
															
															<img src="{{$url}}" class="img-thumbnail">
															
															</div>
															<div class="lightbox-target" id="preview-passport-<?php echo $i;?>">
															   <img src="{{$url}}"/>
															   <a class="lightbox-close" href="#"></a>
															</div>
															
														</div>
														<?php } ?>
													</div>
												</div>
												<?php $i++;}
												
												?>
											
											</div>
													
										</div>
										
										<?php }?>
										
										<?php if(@$documents['passport']['us_visa']){
									       	
										    	$private_document = false;
										$public_document = false;
                                        
										if(isset($user_uploaded_documents['us_visa'][0]['status']) && $user_uploaded_documents['us_visa'][0]['status'] == '1'){
											$public_document = true;
										}else{
											$private_document = true;
										}

										$hide = '';
										if(isset($value['hide'])){
											$hide = 'hide';
											$active_class = '';
										}
											
											?>
												
												
										
										<div class="row add-more-section" id="profile_us_visa" <?php if(!count($user_uploaded_documents['us_visa'][0]['user_documents'])){ echo 'style="display:none;"';}?>>
										     <div class="col-xs-6 col-sm-6">
													<span style="color:#47c4f0;float: left;"><i class="fa fa-check-circle-o" style="font-size: 22px;"></i></span>
													<span style="margin-left: 10px;padding-top: 2px;float: left;color: #47c4f0;">us_visa</span>		
											</div>
											
										    <div class="col-xs-12 col-sm-6">
											       
												<div class="form-group pull-right">Visibility:					
													<?php if($private_document){ echo 'Public';}else{ echo 'Public';}?>
													
												</div>		
											</div>
											
											
											<div class="col-xs-12 col-sm-12">
											    <?php $i=1;$us_visa_count=0; foreach($user_uploaded_documents['us_visa'][0]['user_documents'] as $arrAadhaar){ $us_visa_count++;?>
												<div class="col-xs-12 col-sm-2">												
													 <div class="avatar-upload">
													    <div class="avatar-delete delete_doc" data-id="<?php echo $arrAadhaar['id'];?>" data-imagename="<?php echo $arrAadhaar['document_path'];?>" data-type="us_visa">
															<label for="imageUpload"></label>
														</div>
														
														<a class="lightbox" href="#preview-us_visa-<?php echo $i;?>">
														<div class="avatar-view" data-id="<?php echo $arrAadhaar['id'];?>" data-imagename="<?php echo $arrAadhaar['document_path'];?>" data-type="us_visa">
														    
															<label for="imageUpload"></label>
														</div>
														</a>
														<?php if(isset($arrAadhaar['document_path']))
														{ 
													      $storage_path = env('DOCUMENT_STORAGE_PATH');
													      $type='us_visa';
														  $url=env('APP_URL').'/'.$storage_path . $user_id ."/". $type."/".$arrAadhaar['document_path'];?>
														
														<div class="avatar-preview" >
															<div class="avatar-edit-img" id="us_visa-<?php echo $i;?>">
															
															<img src="{{$url}}" class="img-thumbnail">
															
															</div>
															<div class="lightbox-target" id="preview-us_visa-<?php echo $i;?>">
															   <img src="{{$url}}"/>
															   <a class="lightbox-close" href="#"></a>
															</div>
															
														</div>
														<?php }else{?>
														<div class="avatar-preview" >
															<div class="avatar-edit-img" id="us_visa-<?php echo $i;?>">
															<img src="{{url('public/assets/images/jpg.jpg')}}" data-action="zoom" class="pull-left" style="height: 130px;width:130px;">
															</div>
														</div>
														<?php } ?>
													</div>
												</div>
												<?php $i++;}
												
												?>
											
											</div>
													
										</div>
										
										

									   <?php } ?>
										</div>
										
										
										
										<div class="tab-pane" role="tabpanel" id="menu3">
										<div class="row" style="margin-left: 1px;margin-top: 10px;">
											<div class="col-sm-12" style="background: #2a55a2;color: #fff;border-radius: 4px;padding: 10px;font-size: 16px;">
											    INDOS | CDC
											</div>
										</div>
											<?php //if(@count($documents['indos']))
										//{
										    	$private_document = false;
										$public_document = false;
                                        
										if(isset($user_uploaded_documents['indos'][0]['status']) && $user_uploaded_documents['indos'][0]['status'] == '1'){
											$public_document = true;
										}else{
											$private_document = true;
										}

										$hide = '';
										if(isset($value['hide'])){
											$hide = 'hide';
											$active_class = '';
										}
											
											?>
												
												
										
										<div class="row add-more-section" id="profile_indos">
										     <div class="col-xs-6 col-sm-6">
													<span style="color:#47c4f0;float: left;"><i class="fa fa-check-circle-o" style="font-size: 22px;"></i></span>
													<span style="margin-left: 10px;padding-top: 2px;float: left;color: #47c4f0;">indos</span>		
											</div>
											
										    <div class="col-xs-12 col-sm-6">
											       
												<div class="form-group pull-right">Visibility:					
													<?php if($private_document){ echo 'Public';}else{ echo 'Public';}?>
													
												</div>		
											</div>
											
											
											<div class="col-xs-12 col-sm-12">
											    <?php $i=1;$indos_count=0; foreach($user_uploaded_documents['indos'][0]['user_documents'] as $arrIndos){ $indos_count++;?>
												<div class="col-xs-12 col-sm-2">												
													 <div class="avatar-upload">
													    <div class="avatar-delete delete_doc" data-id="<?php echo $arrIndos['id'];?>" data-imagename="<?php echo $arrIndos['document_path'];?>" data-type="indos">
															<label for="imageUpload"></label>
														</div>
														
														<a class="lightbox" href="#preview-indos-<?php echo $i;?>">
														<div class="avatar-view" data-id="<?php echo $arrIndos['id'];?>" data-imagename="<?php echo $arrIndos['document_path'];?>" data-type="indos">
														    
															<label for="imageUpload"></label>
														</div>
														</a>
														<?php if(isset($arrIndos['document_path']))
														{ 
													      $storage_path = env('DOCUMENT_STORAGE_PATH');
													      $type='indos';
														  $url=env('APP_URL').'/'.$storage_path . $user_id ."/". $type."/".$arrIndos['document_path'];?>
														
														<div class="avatar-preview" >
															<div class="avatar-edit-img" id="indos-<?php echo $i;?>">
															
															<img src="{{$url}}" class="img-thumbnail">
															
															</div>
															<div class="lightbox-target" id="preview-indos-<?php echo $i;?>">
															   <img src="{{$url}}"/>
															   <a class="lightbox-close" href="#"></a>
															</div>
															
														</div>
														<?php }else{?>
														<div class="avatar-preview" >
															<div class="avatar-edit-img" id="indos-<?php echo $i;?>">
															<img src="{{url('public/assets/images/jpg.jpg')}}" data-action="zoom" class="pull-left" style="height: 130px;width:130px;">
															</div>
														</div>
														<?php } ?>
													</div>
												</div>
												<?php $i++;}
												
												?>
											
											</div>
													
										</div>
										
										<?php //}?>
									    
										<?php for($i=0;$i<count($documents['cdc']);$i++)
												{
													
													
										$k=$i+1;
										//print_r($user_uploaded_documents['cdc'.$k]);
										//echo $user_uploaded_documents['cdc'.$k][0]['status'];
										$private_document = false;
										$public_document = false;
                                        
										if(isset($user_uploaded_documents['cdc'.$k][0]['status']) && $user_uploaded_documents['cdc'.$k][0]['status'] == '1'){
											$public_document = true;
										}else{
											$private_document = true;
										}

											?>
												
												
										
										<div class="row add-more-section" id="profile_cdc">
										     <div class="col-xs-6 col-sm-6">
													<span style="color:#47c4f0;float: left;"><i class="fa fa-check-circle-o" style="font-size: 22px;"></i></span>
													<span style="margin-left: 10px;padding-top: 2px;float: left;color: #47c4f0;">CDC - <?php echo \CommonHelper::countries()[$documents['cdc'][$i]['cdc']];?></span>		
											</div>
											
										    <div class="col-xs-12 col-sm-6">
											       
												<div class="form-group pull-right">Visibility:					
												<?php if($private_document){ echo 'Public';}else{ echo 'Public';}?>
													
												</div>		
											</div>
											
											<div class="col-xs-12 col-sm-12">
											    <?php $j=1;$doc_count=0; foreach($user_uploaded_documents['cdc'.$k][0]['user_documents'] as $arrAadhaar){ $doc_count++;?>
												<div class="col-xs-12 col-sm-2">												
													 <div class="avatar-upload">
													    <div class="avatar-delete delete_doc" data-id="<?php echo $arrAadhaar['id'];?>" data-imagename="<?php echo $arrAadhaar['document_path'];?>" data-type="cdc<?php echo $k;?>">
															<label for="imageUpload"></label>
														</div>
														
														<a class="lightbox" href="#preview-cdc-<?php echo $k;?>-<?php echo $j;?>">
														<div class="avatar-view" data-id="<?php echo $arrAadhaar['id'];?>" data-imagename="<?php echo $arrAadhaar['document_path'];?>" data-type="cdc<?php echo $k;?>">
														    
															<label for="imageUpload"></label>
														</div>
														</a>
														<?php if(isset($arrAadhaar['document_path']))
														{ 
													      $storage_path = env('DOCUMENT_STORAGE_PATH');
													      $type='cdc'.$k;
														  $url=env('APP_URL').'/'.$storage_path . $user_id ."/". $type."/".$arrAadhaar['document_path'];?>
														
														<div class="avatar-preview" >
															<div class="avatar-edit-img" id="cdc<?php echo $k;?>">
															
															<img src="{{$url}}" class="img-thumbnail">
															
															</div>
															<div class="lightbox-target" id="preview-indos-<?php echo $k;?>">
															   <img src="{{$url}}"/>
															   <a class="lightbox-close" href="#"></a>
															</div>
															
														</div>
														<?php }else{?>
														<div class="avatar-preview" >
															<div class="avatar-edit-img" id="cdc-<?php echo $k;?>">
															<img src="{{url('public/assets/images/jpg.jpg')}}" data-action="zoom" class="pull-left" style="height: 130px;width:130px;">
															</div>
														</div>
														<?php } ?>
													</div>
												</div>
												<?php $j++;}
												
												?>
											
											</div>
												
											
												
										</div>
										
											
											
											
											
											
											
											
											
										
									    <?php } ?>
										</div>
									
									     <div class="tab-pane" role="tabpanel" id="menu4">
											 <div class="row" style="margin-left: 1px;margin-top: 10px;">
												<div class="col-sm-12" style="background: #2a55a2;color: #fff;border-radius: 4px;padding: 10px;font-size: 16px;">
													WK/COP
												</div>
											</div>
											<?php  $watch_keeping=$documents['watch keeping']['watch_keeping'];?>
											
											<?php if($documents['watch keeping']['watch_keeping'])
										{
										    	$private_document = false;
										$public_document = false;
                                        
										if(isset($user_uploaded_documents['wk_cop'][0]['status']) && $user_uploaded_documents['wk_cop'][0]['status'] == '1'){
											$public_document = true;
										}else{
											$private_document = true;
										}

										$hide = '';
										if(isset($value['hide'])){
											$hide = 'hide';
											$active_class = '';
										}
											
											?>
												
												
										
										<div class="row add-more-section" id="profile_wk_cop">
										     <div class="col-xs-6 col-sm-6">
													<span style="color:#47c4f0;float: left;"><i class="fa fa-check-circle-o" style="font-size: 22px;"></i></span>
													<span style="margin-left: 10px;padding-top: 2px;float: left;color: #47c4f0;">WK / COP - <?php if($watch_keeping){ echo \CommonHelper::countries()[$watch_keeping];}?></span>		
											</div>
											
										    <div class="col-xs-12 col-sm-6">
											       
												<div class="form-group pull-right">Visibility:					
													<?php if($private_document){ echo 'Public';}else{ echo 'Public';}?>
													
												</div>		
											</div>
											
											
											<div class="col-xs-12 col-sm-12">
											    <?php $i=1;$wk_cop_count=0; foreach($user_uploaded_documents['wk_cop'][0]['user_documents'] as $arrAadhaar){ $wk_cop_count++;?>
												<div class="col-xs-12 col-sm-2">												
													 <div class="avatar-upload">
													    <div class="avatar-delete delete_doc" data-id="<?php echo $arrAadhaar['id'];?>" data-imagename="<?php echo $arrAadhaar['document_path'];?>" data-type="wk_cop">
															<label for="imageUpload"></label>
														</div>
														
														<a class="lightbox" href="#preview-wk_cop-<?php echo $i;?>">
														<div class="avatar-view" data-id="<?php echo $arrAadhaar['id'];?>" data-imagename="<?php echo $arrAadhaar['document_path'];?>" data-type="wk_cop">
														    
															<label for="imageUpload"></label>
														</div>
														</a>
														<?php if(isset($arrAadhaar['document_path']))
														{ 
													      $storage_path = env('DOCUMENT_STORAGE_PATH');
													      $type='wk_cop';
														  $url=env('APP_URL').'/'.$storage_path . $user_id ."/". $type."/".$arrAadhaar['document_path'];?>
														
														<div class="avatar-preview" >
															<div class="avatar-edit-img" id="wk_cop-<?php echo $i;?>">
															
															<img src="{{$url}}" class="img-thumbnail">
															
															</div>
															<div class="lightbox-target" id="preview-wk_cop-<?php echo $i;?>">
															   <img src="{{$url}}"/>
															   <a class="lightbox-close" href="#"></a>
															</div>
															
														</div>
														<?php }else{?>
														<div class="avatar-preview" >
															<div class="avatar-edit-img" id="wk_cop-<?php echo $i;?>">
															<img src="{{url('public/assets/images/jpg.jpg')}}" data-action="zoom" class="pull-left" style="height: 130px;width:130px;">
															</div>
														</div>
														<?php } ?>
													</div>
												</div>
												<?php $i++;}
												
												?>
											
											</div>
													
										</div>
										
										<?php }?>
									    
										
									    </div>
									
									     <div class="tab-pane" role="tabpanel" id="menu5">
											<div class="row" style="margin-left: 1px;margin-top: 10px;">
												<div class="col-sm-12" style="background: #2a55a2;color: #fff;border-radius: 4px;padding: 10px;font-size: 16px;">
													COC|COE
												</div>
											</div>
											 <?php if(@count($documents['coc']))
										     {?>
											<?php 
											
											
											$coc_display=0;for($i=0;$i<count($documents['coc']);$i++){?>
										  
											
										<?php $k=$i+1;
										
										$private_document = false;
										$public_document = false;
                                        
										if(isset($user_uploaded_documents['coc'.$k][0]['status']) && $user_uploaded_documents['coc'.$k][0]['status'] == '1'){
											$public_document = true;
										}else{
											$private_document = true;
										}

										
										
										   $coc_count=count($user_uploaded_documents['coc'.$k][0]['user_documents']);
                                           if($coc_count)
										   {
											   $coc_display=1;
										   }											   
													?>
										 <div class="row add-more-section" id="profile_cdc" <?php if(!$coc_count){ echo 'style="display:none;"';}?>>
										     <div class="col-xs-6 col-sm-6">
													<span style="color:#47c4f0;float: left;"><i class="fa fa-check-circle-o" style="font-size: 22px;"></i></span>
													<span style="margin-left: 10px;padding-top: 2px;float: left;color: #47c4f0;">COC - <?php echo $documents['coc'][$i]['coc_grade'];?> - <?php echo \CommonHelper::countries()[$documents['coc'][$i]['coc']];?></span>		
											</div>
											
										    <div class="col-xs-12 col-sm-6">
											       
												<div class="form-group pull-right">Visibility:					
													<?php if($private_document){ echo 'Public';}else{ echo 'Public';}?>
													
												</div>		
											</div>
											
											<div class="col-xs-12 col-sm-12">
											    <?php $j=1;$doc_count=0; foreach($user_uploaded_documents['coc'.$k][0]['user_documents'] as $arrAadhaar){ $doc_count++;?>
												<div class="col-xs-12 col-sm-2">												
													 <div class="avatar-upload">
													    <div class="avatar-delete delete_doc" data-id="<?php echo $arrAadhaar['id'];?>" data-imagename="<?php echo $arrAadhaar['document_path'];?>" data-type="coc<?php echo $k;?>">
															<label for="imageUpload"></label>
														</div>
														
														<a class="lightbox" href="#preview-coc-<?php echo $k;?>-<?php echo $j;?>">
														<div class="avatar-view" data-id="<?php echo $arrAadhaar['id'];?>" data-imagename="<?php echo $arrAadhaar['document_path'];?>" data-type="coc<?php echo $k;?>">
														    
															<label for="imageUpload"></label>
														</div>
														</a>
														<?php if(isset($arrAadhaar['document_path']))
														{ 
													      $storage_path = env('DOCUMENT_STORAGE_PATH');
													      $type='coc'.$k;
														  $url=env('APP_URL').'/'.$storage_path . $user_id ."/". $type."/".$arrAadhaar['document_path'];?>
														
														<div class="avatar-preview" >
															<div class="avatar-edit-img" id="coc<?php echo $k;?>">
															
															<img src="{{$url}}" class="img-thumbnail">
															
															</div>
															<div class="lightbox-target" id="preview-coc-<?php echo $k;?>">
															   <img src="{{$url}}"/>
															   <a class="lightbox-close" href="#"></a>
															</div>
															
														</div>
														<?php } ?>
													</div>
												</div>
												<?php $j++;}
												
												?>
											
											</div>
												
											
												
										</div>
										
											
										    <?php }}?>
											
											<?php if(@count($documents['coe']))
										     {?>
											<?php for($i=0;$i<count($documents['coe']);$i++){
											
										$k=$i+1;
										//print_r($user_uploaded_documents['cdc'.$k]);
										//echo $user_uploaded_documents['cdc'.$k][0]['status'];
										$private_document = false;
										$public_document = false;
                                        
										if(isset($user_uploaded_documents['coe'.$k][0]['status']) && $user_uploaded_documents['coe'.$k][0]['status'] == '1'){
											$public_document = true;
										}else{
											$private_document = true;
										}

											?>
												
												
										
										<div class="row add-more-section" id="profile_cdc">
										     <div class="col-xs-6 col-sm-6">
													<span style="color:#47c4f0;float: left;"><i class="fa fa-check-circle-o" style="font-size: 22px;"></i></span>
													<span style="margin-left: 10px;padding-top: 2px;float: left;color: #47c4f0;">COE - <?php echo $documents['coe'][$i]['coe_grade'];?> - <?php echo \CommonHelper::countries()[$documents['coe'][$i]['coe']];?></span>		
											</div>
											
										    <div class="col-xs-12 col-sm-6">
											       
												<div class="form-group pull-right">Visibility:					
													<?php if($private_document){ echo 'Public';}else{ echo 'Public';}?>
													
												</div>		
											</div>
											
											<div class="col-xs-12 col-sm-12">
											    <?php $j=1;$doc_count=0; foreach($user_uploaded_documents['coe'.$k][0]['user_documents'] as $arrAadhaar){ $doc_count++;?>
												<div class="col-xs-12 col-sm-2">												
													 <div class="avatar-upload">
													    <div class="avatar-delete delete_doc" data-id="<?php echo $arrAadhaar['id'];?>" data-imagename="<?php echo $arrAadhaar['document_path'];?>" data-type="coe<?php echo $k;?>">
															<label for="imageUpload"></label>
														</div>
														
														<a class="lightbox" href="#preview-coe-<?php echo $k;?>-<?php echo $j;?>">
														<div class="avatar-view" data-id="<?php echo $arrAadhaar['id'];?>" data-imagename="<?php echo $arrAadhaar['document_path'];?>" data-type="coe<?php echo $k;?>">
														    
															<label for="imageUpload"></label>
														</div>
														</a>
														<?php if(isset($arrAadhaar['document_path']))
														{ 
													      $storage_path = env('DOCUMENT_STORAGE_PATH');
													      $type='coe'.$k;
														  $url=env('APP_URL').'/'.$storage_path . $user_id ."/". $type."/".$arrAadhaar['document_path'];?>
														
														<div class="avatar-preview" >
															<div class="avatar-edit-img" id="coe<?php echo $k;?>">
															
															<img src="{{$url}}" class="img-thumbnail">
															
															</div>
															<div class="lightbox-target" id="preview-coe-<?php echo $k;?>">
															   <img src="{{$url}}"/>
															   <a class="lightbox-close" href="#"></a>
															</div>
															
														</div>
														<?php }else{?>
														<div class="avatar-preview" >
															<div class="avatar-edit-img" id="coe-<?php echo $k;?>">
															<img src="{{url('public/assets/images/jpg.jpg')}}" data-action="zoom" class="pull-left" style="height: 130px;width:130px;">
															</div>
														</div>
														<?php } ?>
													</div>
												</div>
												<?php $j++;}
												
												?>
											
											</div>
												
											
												
										</div>
										
											
											
										
										
											<?php }}?>
									    
									    
										</div>
									     <div class="tab-pane" role="tabpanel" id="menu6">
										 <div class="row" style="margin-left: 1px;margin-top: 10px;">
											<div class="col-sm-12" style="background: #2a55a2;color: #fff;border-radius: 4px;padding: 10px;font-size: 16px;">
											    GMDSS
											</div>
										</div>
										 	<?php //if($documents['gmdss'])
										//{
										    	$private_document = false;
										$public_document = false;
                                        
										if(isset($user_uploaded_documents['gmdss'][0]['status']) && $user_uploaded_documents['gmdss'][0]['status'] == '1'){
											$public_document = true;
										}else{
											$private_document = true;
										}

										$hide = '';
										if(isset($value['hide'])){
											$hide = 'hide';
											$active_class = '';
										}
											
											?>
												
												
										
										<div class="row add-more-section" id="profile_gmdss">
										     <div class="col-xs-6 col-sm-9">
													<span style="color:#47c4f0;float: left;"><i class="fa fa-check-circle-o" style="font-size: 22px;"></i></span>
													<span style="margin-left: 10px;padding-top: 2px;float: left;color: #47c4f0;">GMDSS</span>		
											</div>
											
										    <div class="col-xs-12 col-sm-3">
											       
												<div class="form-group pull-right">Visibility:					
													<?php if($private_document){ echo 'Public';}else{ echo 'Public';}?>
													
												</div>		
											</div>
											
											
											<div class="col-xs-12 col-sm-12">
											    <?php $i=1;$gmdss_count=0; foreach($user_uploaded_documents['gmdss'][0]['user_documents'] as $arrAadhaar){ $gmdss_count++;?>
												<div class="col-xs-12 col-sm-2">												
													 <div class="avatar-upload">
													    <div class="avatar-delete delete_doc" data-id="<?php echo $arrAadhaar['id'];?>" data-imagename="<?php echo $arrAadhaar['document_path'];?>" data-type="gmdss">
															<label for="imageUpload"></label>
														</div>
														
														<a class="lightbox" href="#preview-gmdss-<?php echo $i;?>">
														<div class="avatar-view" data-id="<?php echo $arrAadhaar['id'];?>" data-imagename="<?php echo $arrAadhaar['document_path'];?>" data-type="gmdss">
														    
															<label for="imageUpload"></label>
														</div>
														</a>
														<?php if(isset($arrAadhaar['document_path']))
														{ 
													      $storage_path = env('DOCUMENT_STORAGE_PATH');
													      $type='gmdss';
														  $url=env('APP_URL').'/'.$storage_path . $user_id ."/". $type."/".$arrAadhaar['document_path'];?>
														
														<div class="avatar-preview" >
															<div class="avatar-edit-img" id="gmdss-<?php echo $i;?>">
															
															<img src="{{$url}}" class="img-thumbnail">
															
															</div>
															<div class="lightbox-target" id="preview-gmdss-<?php echo $i;?>">
															   <img src="{{$url}}"/>
															   <a class="lightbox-close" href="#"></a>
															</div>
															
														</div>
														<?php }else{?>
														<div class="avatar-preview" >
															<div class="avatar-edit-img" id="gmdss-<?php echo $i;?>">
															<img src="{{url('public/assets/images/jpg.jpg')}}" data-action="zoom" class="pull-left" style="height: 130px;width:130px;">
															</div>
														</div>
														<?php } ?>
													</div>
												</div>
												<?php $i++;}
												?>
											
											</div>
													
										</div>
										
										<?php //}?>
										</div>
									     <div class="tab-pane" role="tabpanel" id="menu7">
												 <div class="row" style="margin-left: 1px;margin-top: 10px;">
													<div class="col-sm-12" style="background: #2a55a2;color: #fff;border-radius: 4px;padding: 10px;font-size: 16px;">
														COURSE CERTIFICATE
													</div>
												</div>
												<?php if($documents['professional_detail']['institute_degree']){?>
												<div class="row add-more-section">
										     <div class="col-xs-6 col-sm-6">
													<span style="color:#47c4f0;float: left;"><i class="fa fa-check-circle-o" style="font-size: 22px;"></i></span>
													<span style="margin-left: 10px;padding-top: 2px;float: left;color: #47c4f0;">Course : 
													PRE SEA - <?php echo $documents['professional_detail']['institute_degree'];?></span>		
											</div>
										    <div class="col-xs-6 col-sm-6" style="text-align:right;">
													max - 5 images			
											</div>
											<div class="col-sm-12">

												
												<div class="col-xs-12 col-sm-2">
													<img src="{{url('public/assets/images/jpg.jpg')}}" style="width:100px;">
												</div>
												
												<div class="col-xs-12 col-sm-10" style="text-align:right;">
													<div class="form-group">
												
														<label class="radio-inline"><input type="radio" name="pre_sea_show" value="public" checked="">Public </label>
														<label class="radio-inline"><input type="radio" name="pre_sea_show" value="private"> Private</label>
													</div>	
												</div>												

											</div>
											
										    
										
									    </div>
										<?php } ?>
													<?php $course_certificate_display=0;for($i=0;$i<count(@$documents['course']);$i++)
												{
												     $k=$i+1;
										//print_r($user_uploaded_documents['cdc'.$k]);
										//echo $user_uploaded_documents['cdc'.$k][0]['status'];
										$private_document = false;
										$public_document = false;
                                        
										if(isset($user_uploaded_documents['course'.$k][0]['status']) && $user_uploaded_documents['course'.$k][0]['status'] == '1'){
											$public_document = true;
										}else{
											$private_document = true;
										}
											$course_certificate=count($user_uploaded_documents['course'.$k][0]['user_documents']);
                                           if($course_certificate)
										   {
											   $course_certificate_display=1;
										   }											   
													?>
										 <div class="row add-more-section" id="profile_course_certificate" <?php if(!$course_certificate){ echo 'style="display:none;"';}?>>
										     <div class="col-xs-6 col-sm-9">
													<span style="color:#47c4f0;float: left;"><i class="fa fa-check-circle-o" style="font-size: 22px;"></i></span>
													<span style="margin-left: 10px;padding-top: 2px;float: left;color: #47c4f0;">Course : 
													<?php $arrData=\CommonHelper::course_name_by_course_type($documents['course'][$i]['course_type']);
																				
																				foreach($arrData as $val)
																				{								
																					if($documents['course'][$i]['course_id']==$val->id)
																					{
																						echo $val->course_name;
																					}							
																				}
																				
																				?>
													</span>		
											</div>
											
										    <div class="col-xs-12 col-sm-3">
											       
												<div class="form-group pull-right">Visibility:	<?php if($private_document){ echo 'Private';}else{ echo 'Public';}?>			
												</div>		
											</div>
											
											<div class="col-xs-12 col-sm-12">
											    <?php $j=1;$doc_count=0; foreach($user_uploaded_documents['course'.$k][0]['user_documents'] as $arrAadhaar){ $doc_count++;?>
												<div class="col-xs-12 col-sm-2">												
													 <div class="avatar-upload">
													    <div class="avatar-delete delete_doc" data-id="<?php echo $arrAadhaar['id'];?>" data-imagename="<?php echo $arrAadhaar['document_path'];?>" data-type="course<?php echo $k;?>">
															<label for="imageUpload"></label>
														</div>
														
														<a class="lightbox" href="#preview-course-<?php echo $k;?>-<?php echo $j;?>">
														<div class="avatar-view" data-id="<?php echo $arrAadhaar['id'];?>" data-imagename="<?php echo $arrAadhaar['document_path'];?>" data-type="course<?php echo $k;?>">
														    
															<label for="imageUpload"></label>
														</div>
														</a>
														<?php if(isset($arrAadhaar['document_path']))
														{ 
													      $storage_path = env('DOCUMENT_STORAGE_PATH');
													      $type='course'.$k;
														  $url=env('APP_URL').'/'.$storage_path . $user_id ."/". $type."/".$arrAadhaar['document_path'];?>
														
														<div class="avatar-preview" >
															<div class="avatar-edit-img" id="course<?php echo $k;?>">
															
															<img src="{{$url}}" class="img-thumbnail">
															
															</div>
															<div class="lightbox-target" id="preview-course-<?php echo $k;?>">
															   <img src="{{$url}}"/>
															   <a class="lightbox-close" href="#"></a>
															</div>
															
														</div>
														<?php }else{?>
														<div class="avatar-preview" >
															<div class="avatar-edit-img" id="course-<?php echo $k;?>">
															<img src="{{url('public/assets/images/jpg.jpg')}}" data-action="zoom" class="pull-left" style="height: 130px;width:130px;">
															</div>
														</div>
														<?php } ?>
													</div>
												</div>
												<?php $j++;}
												
												?>
											
											</div>
												
											
												
										</div>
										
												
												
												
												
												
												<?php } ?>    
										
										</div>
									     
										 <div class="tab-pane" role="tabpanel" id="menu8">
										 <div class="row" style="margin-left: 1px;margin-top: 10px;">
											<div class="col-sm-12" style="background: #2a55a2;color: #fff;border-radius: 4px;padding: 10px;font-size: 16px;">
											    SEA SERVICES
											</div>
											
										</div>
										<?php $sea_service_display=0;for($i=0;$i<count($documents['sea service']);$i++)
												{
													$k=$i+1;
										//print_r($user_uploaded_documents['cdc'.$k]);
										//echo $user_uploaded_documents['cdc'.$k][0]['status'];
										$private_document = false;
										$public_document = false;
                                        
										if(isset($user_uploaded_documents['sea_service'.$k][0]['status']) && $user_uploaded_documents['sea_service'.$k][0]['status'] == '1'){
											$public_document = true;
										}else{
											$private_document = true;
										}
											$sea_service=count($user_uploaded_documents['sea_service'.$k][0]['user_documents']);
                                           if($sea_service)
										   {
											   $sea_service_display=1;
										   }			
													?>
										 <div class="row add-more-section" id="profile_sea_service" <?php if(!$sea_service){ echo 'style="display:none;"';}?>>
										     <div class="col-xs-6 col-sm-9">
													<span style="color:#47c4f0;float: left;"><i class="fa fa-check-circle-o" style="font-size: 22px;"></i></span>
													<span style="margin-left: 10px;padding-top: 2px;float: left;color: #47c4f0;"><?php echo 'From: '.date('d-m-Y',strtotime($documents['sea service'][$i]['from'])).' To '.date('d-m-Y',strtotime($documents['sea service'][$i]['to'])).'&nbsp;&nbsp;Ship Name: '.$documents['sea service'][$i]['ship_name'];?></span>		
											</div>
											
										    <div class="col-xs-12 col-sm-3">
											       
												<div class="form-group pull-right">Visibility:	<?php if($private_document){ echo 'Public';}else{ echo 'Public';}?>			
													
													
												</div>		
											</div>
											
											<div class="col-xs-12 col-sm-12">
											    <?php $j=1;$doc_count=0; foreach($user_uploaded_documents['sea_service'.$k][0]['user_documents'] as $arrAadhaar){ $doc_count++;?>
												<div class="col-xs-12 col-sm-2">												
													 <div class="avatar-upload">
													    <div class="avatar-delete delete_doc" data-id="<?php echo $arrAadhaar['id'];?>" data-imagename="<?php echo $arrAadhaar['document_path'];?>" data-type="sea_service<?php echo $k;?>">
															<label for="imageUpload"></label>
														</div>
														
														<a class="lightbox" href="#preview-sea_service-<?php echo $k;?>-<?php echo $j;?>">
														<div class="avatar-view" data-id="<?php echo $arrAadhaar['id'];?>" data-imagename="<?php echo $arrAadhaar['document_path'];?>" data-type="sea_service<?php echo $k;?>">
														    
															<label for="imageUpload"></label>
														</div>
														</a>
														<?php if(isset($arrAadhaar['document_path']))
														{ 
													      $storage_path = env('DOCUMENT_STORAGE_PATH');
													      $type='sea_service'.$k;
														  $url=env('APP_URL').'/'.$storage_path . $user_id ."/". $type."/".$arrAadhaar['document_path'];?>
														
														<div class="avatar-preview" >
															<div class="avatar-edit-img" id="sea_service<?php echo $k;?>">
															
															<img src="{{$url}}" class="img-thumbnail">
															
															</div>
															<div class="lightbox-target" id="preview-sea_service-<?php echo $k;?>">
															   <img src="{{$url}}"/>
															   <a class="lightbox-close" href="#"></a>
															</div>
															
														</div>
														<?php }else{?>
														<div class="avatar-preview" >
															<div class="avatar-edit-img" id="sea_service-<?php echo $k;?>">
															<img src="{{url('public/assets/images/jpg.jpg')}}" data-action="zoom" class="pull-left" style="height: 130px;width:130px;">
															</div>
														</div>
														<?php } ?>
													</div>
												</div>
												<?php $j++;}
												
												?>
											
											</div>
												
											
												
										</div>
										
										<?php } ?>
										</div>
									    
										 
										 <div class="tab-pane" role="tabpanel" id="menu9">
											    <div class="row" style="margin-left: 1px;margin-top: 10px;">
													<div class="col-sm-12" style="background: #2a55a2;color: #fff;border-radius: 4px;padding: 10px;font-size: 16px;">
														MEDICAL VACINATION
													</div>
												</div>
												<?php if(@$documents['yellow fever']['ilo_medical']==1)
												{
												
												$private_document = false;
												$public_document = false;

												if(isset($user_uploaded_documents['yellow_fever'][0]['status']) && $user_uploaded_documents['yellow_fever'][0]['status'] == '1'){
													$public_document = true;
												}else{
													$private_document = true;
												}

											?>
												
												
										
										<div class="row add-more-section" id="profile_yellow_fever" <?php if(!count($user_uploaded_documents['ilo_medical'][0]['user_documents'])){ echo 'style="display:none;"';}?>>
										     <div class="col-xs-6 col-sm-9">
													<span style="color:#47c4f0;float: left;"><i class="fa fa-check-circle-o" style="font-size: 22px;"></i></span>
													<span style="margin-left: 10px;padding-top: 2px;float: left;color: #47c4f0;">ILO MEDICAL</span>		
											</div>
											
										    <div class="col-xs-12 col-sm-3">
											       
												<div class="form-group pull-right">Visibility:					
													<?php if($private_document){ echo 'Public';}else{ echo 'Public';}?>
													
												</div>		
											</div>
											
											
											<div class="col-xs-12 col-sm-12">
											    <?php $i=1;$ilo_medical_count=0; foreach($user_uploaded_documents['ilo_medical'][0]['user_documents'] as $arrAadhaar){ $ilo_medical_count++;?>
												<div class="col-xs-12 col-sm-2">												
													 <div class="avatar-upload">
													    <div class="avatar-delete delete_doc" data-id="<?php echo $arrAadhaar['id'];?>" data-imagename="<?php echo $arrAadhaar['document_path'];?>" data-type="ilo_medical">
															<label for="imageUpload"></label>
														</div>
														
														<a class="lightbox" href="#preview-ilo_medical-<?php echo $i;?>">
														<div class="avatar-view" data-id="<?php echo $arrAadhaar['id'];?>" data-imagename="<?php echo $arrAadhaar['document_path'];?>" data-type="ilo_medical">
														    
															<label for="imageUpload"></label>
														</div>
														</a>
														<?php if(isset($arrAadhaar['document_path']))
														{ 
													      $storage_path = env('DOCUMENT_STORAGE_PATH');
													      $type='ilo_medical';
														  $url=env('APP_URL').'/'.$storage_path . $user_id ."/". $type."/".$arrAadhaar['document_path'];?>
														
														<div class="avatar-preview" >
															<div class="avatar-edit-img" id="ilo_medical-<?php echo $i;?>">
															
															<img src="{{$url}}" class="img-thumbnail">
															
															</div>
															<div class="lightbox-target" id="preview-ilo_medical-<?php echo $i;?>">
															   <img src="{{$url}}"/>
															   <a class="lightbox-close" href="#"></a>
															</div>
															
														</div>
														<?php }?>
													</div>
												</div>
												<?php $i++;}
												?>
											
											</div>
													
										</div>
										
												<?php } ?>
												<?php if(@$documents['yellow fever']['yellow_fever']==1)
												{
												
												$private_document = false;
										$public_document = false;
                                        
										if(isset($user_uploaded_documents['yellow_fever'][0]['status']) && $user_uploaded_documents['yellow_fever'][0]['status'] == '1'){
											$public_document = true;
										}else{
											$private_document = true;
										}

											?>
												
												
										
										<div class="row add-more-section" id="profile_yellow_fever" <?php if(!count($user_uploaded_documents['yellow_fever'][0]['user_documents'])){ echo 'style="display:none;"';}?>>
										     <div class="col-xs-6 col-sm-9">
													<span style="color:#47c4f0;float: left;"><i class="fa fa-check-circle-o" style="font-size: 22px;"></i></span>
													<span style="margin-left: 10px;padding-top: 2px;float: left;color: #47c4f0;">YELLOW FEVER VACINATION</span>		
											</div>
											
										    <div class="col-xs-12 col-sm-3">
											       
												<div class="form-group pull-right">Visibility:					
													<?php if($private_document){ echo 'Public';}else{ echo 'Public';}?>
													
												</div>		
											</div>
											
											
											<div class="col-xs-12 col-sm-12">
											    <?php $i=1;$yellow_fever_count=0; foreach($user_uploaded_documents['yellow_fever'][0]['user_documents'] as $arrAadhaar){ $yellow_fever_count++;?>
												<div class="col-xs-12 col-sm-2">												
													 <div class="avatar-upload">
													    <div class="avatar-delete delete_doc" data-id="<?php echo $arrAadhaar['id'];?>" data-imagename="<?php echo $arrAadhaar['document_path'];?>" data-type="yellow_fever">
															<label for="imageUpload"></label>
														</div>
														
														<a class="lightbox" href="#preview-yellow_fever-<?php echo $i;?>">
														<div class="avatar-view" data-id="<?php echo $arrAadhaar['id'];?>" data-imagename="<?php echo $arrAadhaar['document_path'];?>" data-type="yellow_fever">
														    
															<label for="imageUpload"></label>
														</div>
														</a>
														<?php if(isset($arrAadhaar['document_path']))
														{ 
													      $storage_path = env('DOCUMENT_STORAGE_PATH');
													      $type='yellow_fever';
														  $url=env('APP_URL').'/'.$storage_path . $user_id ."/". $type."/".$arrAadhaar['document_path'];?>
														
														<div class="avatar-preview" >
															<div class="avatar-edit-img" id="yellow_fever-<?php echo $i;?>">
															
															<img src="{{$url}}" class="img-thumbnail">
															
															</div>
															<div class="lightbox-target" id="preview-yellow_fever-<?php echo $i;?>">
															   <img src="{{$url}}"/>
															   <a class="lightbox-close" href="#"></a>
															</div>
															
														</div>
														<?php }?>
													</div>
												</div>
												<?php $i++;}
												
												?>
											
											</div>
													
										</div>
										
										        <?php }?>
										 
										 </div>
										 <div class="tab-pane" role="tabpanel" id="menu10">
										    <div class="row" style="margin-left: 1px;margin-top: 10px;">
													<div class="col-sm-12" style="background: #2a55a2;color: #fff;border-radius: 4px;padding: 10px;font-size: 16px;">
														ACADEMICS | CHARACTER CERTIFICATE
													</div>
											</div>
											<div class="row add-more-section">
													<div class="col-xs-6 col-sm-6">
															<span style="color:#47c4f0;float: left;"><i class="fa fa-check-circle-o" style="font-size: 22px;"></i></span>
															<span style="margin-left: 10px;padding-top: 2px;float: left;color: #47c4f0;">ACADEMICS - <?php echo $documents['professional_detail']['school_qualification'];?></span>		
													</div>
													<div class="col-xs-6 col-sm-6" style="text-align:right;">
															max - 2 images			
													</div>
													<div class="col-sm-12">
														<div class="col-xs-12 col-sm-2">
															<img src="{{url('public/assets/images/jpg.jpg')}}" style="width:100px;">
														</div>
														<div class="col-xs-12 col-sm-10" style="text-align:right;">
															<div class="form-group">
														
																<label class="radio-inline"><input type="radio" name="academics_show" value="public" checked="">Public </label>
																<label class="radio-inline"><input type="radio" name="academics_show" value="private" > Private</label>
															</div>	
														</div>
												

													</div>
													
											
										   
										
									    </div>
										
										<?php if(@$documents['watch keeping']['character_reference']==1)
												{?>
										    <div class="row add-more-section">
													<div class="col-xs-6 col-sm-6">
															<span style="color:#47c4f0;float: left;"><i class="fa fa-check-circle-o" style="font-size: 22px;"></i></span>
															<span style="margin-left: 10px;padding-top: 2px;float: left;color: #47c4f0;">CHARACTER CERTIFICATE</span>		
													</div>
													<div class="col-xs-6 col-sm-6" style="text-align:right;">
															max - 1 image			
													</div>
													<div class="col-sm-12">
														<div class="col-xs-12 col-sm-2">
															<img src="{{url('public/assets/images/jpg.jpg')}}" style="width:100px;">
														</div>
														<div class="col-xs-12 col-sm-10" style="text-align:right;">
															<div class="form-group">
														
																<label class="radio-inline"><input type="radio" name="character_certificate_show" value="public" checked="">Public </label>
																<label class="radio-inline"><input type="radio" name="character_certificate_show" value="private" > Private</label>
															</div>	
														</div>
												

													</div>
													
											
									    </div>
									   <?php }?>
										    
										</div>
									     <div class="tab-pane" role="tabpanel" id="menu11">
										     <div class="row" style="margin-left: 1px;margin-top: 10px;">
													<div class="col-sm-12" style="background: #2a55a2;color: #fff;border-radius: 4px;padding: 10px;font-size: 16px;">
														BANK DETAILS
													</div>
											</div>
											<?php if($documents['personal_detail']['pancard'])
										{
										$private_document = false;
										$public_document = false;
                                        
										if(isset($user_uploaded_documents['pancard'][0]['status']) && $user_uploaded_documents['pancard'][0]['status'] == '1'){
											$public_document = true;
										}else{
											$private_document = true;
										}

										$hide = '';
										if(isset($value['hide'])){
											$hide = 'hide';
											$active_class = '';
										}
											
											?>
												
												
										
										<div class="row add-more-section" id="profile_pancard">
										     <div class="col-xs-6 col-sm-9">
													<span style="color:#47c4f0;float: left;"><i class="fa fa-check-circle-o" style="font-size: 22px;"></i></span>
													<span style="margin-left: 10px;padding-top: 2px;float: left;color: #47c4f0;">PANCARD</span>		
											</div>
											
										    <div class="col-xs-12 col-sm-3">
											       
												<div class="form-group pull-right">Visibility:					
													<?php if($private_document){ echo 'Public';}else{ echo 'Public';}?>
													
												</div>		
											</div>
											
											
											<div class="col-xs-12 col-sm-12">
											    <?php $i=1;$pancard_count=0; foreach($user_uploaded_documents['pancard'][0]['user_documents'] as $arrAadhaar){ $pancard_count++;?>
												<div class="col-xs-12 col-sm-2">												
													 <div class="avatar-upload">
													    <div class="avatar-delete delete_doc" data-id="<?php echo $arrAadhaar['id'];?>" data-imagename="<?php echo $arrAadhaar['document_path'];?>" data-type="pancard">
															<label for="imageUpload"></label>
														</div>
														
														<a class="lightbox" href="#preview-pancard-<?php echo $i;?>">
														<div class="avatar-view" data-id="<?php echo $arrAadhaar['id'];?>" data-imagename="<?php echo $arrAadhaar['document_path'];?>" data-type="pancard">
														    
															<label for="imageUpload"></label>
														</div>
														</a>
														<?php if(isset($arrAadhaar['document_path']))
														{ 
													      $storage_path = env('DOCUMENT_STORAGE_PATH');
													      $type='pancard';
														  $url=env('APP_URL').'/'.$storage_path . $user_id ."/". $type."/".$arrAadhaar['document_path'];?>
														
														<div class="avatar-preview" >
															<div class="avatar-edit-img" id="pancard-<?php echo $i;?>">
															
															<img src="{{$url}}" class="img-thumbnail">
															
															</div>
															<div class="lightbox-target" id="preview-pancard-<?php echo $i;?>">
															   <img src="{{$url}}"/>
															   <a class="lightbox-close" href="#"></a>
															</div>
															
														</div>
														<?php }?>
													</div>
												</div>
												<?php $i++;}
												
												?>
											
											</div>
													
										</div>
										
										<?php }?>
										<?php 
										if($documents['personal_detail']['aadhaar'])
										{
										$private_document = false;
										$public_document = false;
                                        
										if(isset($user_uploaded_documents['aadhaar'][0]['status']) && $user_uploaded_documents['aadhaar'][0]['status'] == '1'){
											$public_document = true;
										}else{
											$private_document = true;
										}

										$hide = '';
										if(isset($value['hide'])){
											$hide = 'hide';
											$active_class = '';
										}
											
											?>
												
												
										
										<div class="row add-more-section" id="profile_aadhaar">
										     <div class="col-xs-6 col-sm-9 pull-right">
													<span style="color:#47c4f0;float: left;"><i class="fa fa-check-circle-o" style="font-size: 22px;"></i></span>
													<span style="margin-left: 10px;padding-top: 2px;float: left;color: #47c4f0;">AADHAAR CARD</span>		
											</div>
											
										    <div class="col-xs-12 col-sm-3">
											       
												<div class="form-group pull-right">Visibility:					
													<?php if($private_document){ echo 'Public';}else{ echo 'Public';}?>
													
												</div>		
											</div>
											
											
											<div class="col-xs-12 col-sm-12">
											    <?php $i=1;$aadhaar_count=0; foreach($user_uploaded_documents['aadhaar'][0]['user_documents'] as $arrAadhaar){ $aadhaar_count++;?>
												<div class="col-xs-12 col-sm-2">												
													 <div class="avatar-upload">
													    <div class="avatar-delete delete_doc" data-id="<?php echo $arrAadhaar['id'];?>" data-imagename="<?php echo $arrAadhaar['document_path'];?>" data-type="aadhaar">
															<label for="imageUpload"></label>
														</div>
														
														<a class="lightbox" href="#preview-aadhaar-<?php echo $i;?>">
														<div class="avatar-view" data-id="<?php echo $arrAadhaar['id'];?>" data-imagename="<?php echo $arrAadhaar['document_path'];?>" data-type="aadhaar">
														    
															<label for="imageUpload"></label>
														</div>
														</a>
														<?php if(isset($arrAadhaar['document_path']))
														{ 
													      $storage_path = env('DOCUMENT_STORAGE_PATH');
													      $type='aadhaar';
														  $url=env('APP_URL').'/'.$storage_path . $user_id ."/". $type."/".$arrAadhaar['document_path'];?>
														
														<div class="avatar-preview" >
															<div class="avatar-edit-img" id="aadhaar-<?php echo $i;?>">
															
															<img src="{{$url}}" class="img-thumbnail">
															
															</div>
															<div class="lightbox-target" id="preview-aadhaar-<?php echo $i;?>">
															   <img src="{{$url}}"/>
															   <a class="lightbox-close" href="#"></a>
															</div>
															
														</div>
														<?php }?>
													</div>
												</div>
												<?php $i++;}
												
												?>
											
											</div>
													
										</div>
										
										<?php }?>
										<?php if($documents['personal_detail']['account_no'])
										{
											
											
										$private_document = false;
										$public_document = false;
                                        
										if(isset($user_uploaded_documents['passbook'][0]['status']) && $user_uploaded_documents['passbook'][0]['status'] == '1'){
											$public_document = true;
										}else{
											$private_document = true;
										}

										$hide = '';
										if(isset($value['hide'])){
											$hide = 'hide';
											$active_class = '';
										}
											
											?>
												
												
										
										<div class="row add-more-section" id="profile_passbook" <?php if(!count($user_uploaded_documents['passbook'][0]['user_documents'])){ echo 'style="display:none;"';}?>>
										     <div class="col-xs-6 col-sm-9">
													<span style="color:#47c4f0;float: left;"><i class="fa fa-check-circle-o" style="font-size: 22px;"></i></span>
													<span style="margin-left: 10px;padding-top: 2px;float: left;color: #47c4f0;">PASSBOOK / CHEQUE LEAF - <?php echo $documents['personal_detail']['bank_name']?></span>		
											</div>
											
										    <div class="col-xs-12 col-sm-3">
											       
												<div class="form-group pull-right">Visibility:					
													<?php if($private_document){ echo 'Public';}else{ echo 'Public';}?>
													
												</div>		
											</div>
											
											
											<div class="col-xs-12 col-sm-12">
											    <?php $i=1;$passbook_count=0; foreach($user_uploaded_documents['passbook'][0]['user_documents'] as $arrPassbook){ $passbook_count++;?>
												<div class="col-xs-12 col-sm-2">												
													 <div class="avatar-upload">
													    <div class="avatar-delete delete_doc" data-id="<?php echo $arrPassbook['id'];?>" data-imagename="<?php echo $arrPassbook['document_path'];?>" data-type="passbook">
															<label for="imageUpload"></label>
														</div>
														
														<a class="lightbox" href="#preview-passbook-<?php echo $i;?>">
														<div class="avatar-view" data-id="<?php echo $arrPassbook['id'];?>" data-imagename="<?php echo $arrPassbook['document_path'];?>" data-type="passbook">
														    
															<label for="imageUpload"></label>
														</div>
														</a>
														<?php if(isset($arrPassbook['document_path']))
														{ 
													      $storage_path = env('DOCUMENT_STORAGE_PATH');
													      $type='passbook';
														  $url=env('APP_URL').'/'.$storage_path . $user_id ."/". $type."/".$arrPassbook['document_path'];?>
														
														<div class="avatar-preview" >
															<div class="avatar-edit-img" id="passbook-<?php echo $i;?>">
															
															<img src="{{$url}}" class="img-thumbnail">
															
															</div>
															<div class="lightbox-target" id="preview-passbook-<?php echo $i;?>">
															   <img src="{{$url}}"/>
															   <a class="lightbox-close" href="#"></a>
															</div>
															
														</div>
														<?php } ?>
													</div>
												</div>
												<?php $i++;}
												?>
											
											</div>
													
										</div>
										
										
										<?php } ?>
										
										</div>
									
										@if (isset($data['user_dangerous_cargo_endorsement_detail']))
											<div class="tab-pane" role="tabpanel" id="menu12">
												<div class="row" style="margin-left: 1px;margin-top: 10px;">
													<div class="col-sm-12" style="background: #2a55a2;color: #fff;border-radius: 4px;padding: 10px;font-size: 16px;">
														DCE DETAILS
													</div>
												</div>
												
												@php
													$dceDetails = isset($data['user_dangerous_cargo_endorsement_detail']) && !empty($data['user_dangerous_cargo_endorsement_detail']) ? $data['user_dangerous_cargo_endorsement_detail'] : null;
													$dceStatus = !empty($dceDetails) && isset($dceDetails['status']) ? json_decode($dceDetails['status']) : null;
													$dceTypes = !empty($dceStatus) && isset($dceStatus->type) ? $dceStatus->type : [];
												@endphp
												
												@if(in_array('oil', $dceTypes))
													@php
														$public_document = false;
														if (isset($user_uploaded_documents['dce']['oil']['status']) && $user_uploaded_documents['dce']['oil']['status'] == '1') {
															$public_document = true;
														}
													@endphp
													<div class="row add-more-section" id="dce_oil">
														<div class="col-xs-6 col-sm-7">
															<span style="color:#47c4f0;float: left;">
																<i class="fa fa-check-circle-o" style="font-size: 22px;"></i>
															</span>
															<span style="margin-left: 10px;padding-top: 2px;float: left;color: #47c4f0;">OIL</span>
														</div>
														<div class="col-sm-3"></div>
														<div class="col-xs-6 col-sm-2">
															@if ((!isset($user_uploaded_documents['dce']['oil'])) || (isset($user_uploaded_documents['dce']['oil'])) && count($user_uploaded_documents['dce']['oil']['user_documents']) < 2)
																<label>
																	<a href="javascript:;" class="btn btn-success edit_doc pull-right" data-type='dce' data-current="menu12_" data-type-id="oil">Add New Image</a>
																</label>
															@endif
														</div>
														<div class="col-xs-12 col-sm-12">
															@php 
																$i = 1;
																$oil_count = 0;
															@endphp
															@if (isset($user_uploaded_documents['dce']['oil']['user_documents']) && count($user_uploaded_documents['dce']['oil']['user_documents']) != 0)
																@foreach ($user_uploaded_documents['dce']['oil']['user_documents'] as $oilImages)
																	@php $oil_count++; @endphp
																	<div class="col-xs-12 col-sm-2">
																		<div class="avatar-upload">
																			<div class="avatar-delete delete_doc" data-id="{{ $oilImages['id'] }}" data-imagename="{{ $oilImages['document_path'] }}" data-type="oil" data-current="menu12_">
																				<label for="imageUpload"></label>
																			</div>

																			<a class="lightbox" href="#preview-oil-{{ $i }}">
																				<div class="avatar-view" data-id="{{ $oilImages['id'] }}" data-imagename="{{ $oilImages['document_path'] }}" data-type="oil">
																					<label for="imageUpload"></label>
																				</div>
																			</a>
																			@if(isset($oilImages['document_path']))
																				@php
																					$storage_path = 'public/uploads/user_documents/';
																					$type = 'dce';
																					$typeId = 'oil';
																					$url = url($storage_path . $user_id . "/" . $type . "/" . $typeId . '/' . $oilImages['document_path']);
																				@endphp

																				<div class="avatar-preview">
																					<div class="avatar-edit-img" id="oil-{{ $i }}">
																						<img src="{{ $url }}" class="img-thumbnail">
																					</div>
																					<div class="lightbox-target" id="preview-oil-{{ $i }}">
																						<img src="{{ $url }}"/>
																						<a class="lightbox-close" href="#oil-{{ $i }}"></a>
																					</div>
																				</div>
																			@else
																				<div class="avatar-preview">
																					<div class="avatar-edit-img" id="oil-{{ $i }}">
																						<img src="{{ url('public/assets/images/jpg.png') }}" data-action="zoom" class="pull-left" style="height: 130px; width:130px;">
																					</div>
																				</div>
																			@endif
																		</div>
																	</div>
																@endforeach
															@endif
															@php $i++; @endphp
															@if($oil_count < 2)
																@for($j = $oil_count; $j < 2; $j++)
																	<div class="col-xs-12 col-sm-2">
																		<div class="avatar-upload">
																			<div class="avatar-preview">
																				<div class="avatar-edit-img" id="oil-{{ $oil_count + 1 }}">
																					<img src="{{url('public/assets/images/jpg.png')}}" data-action="zoom" class="pull-left" style="height: 130px;width:130px;">
																				</div>
																			</div>
																		</div>
																	</div>
																@endfor
															@endif
															<div class="col-xs-12 col-sm-4 visibility-type">
																<div class="form-group pull-right">Visibility:
																	<label class="radio-inline">
																		<input type="radio" class="oil_show doc-permission-radio-btn" name="oil" data-name="dce" data-type-id="oil" value="1" {{ ($public_document == true) ? 'checked' : ''}}>Public
																	</label>
																	<label class="radio-inline">
																		<input type="radio" class="oil_show doc-permission-radio-btn" name="oil" data-name="dce" data-type-id="oil" value="0" {{ ($public_document == false) ? 'checked' : ''}}>Private
																	</label>
																</div>
															</div>
														</div>
													</div>
												@endif
												@if(in_array('chemical', $dceTypes))
													@php
														$public_document = false;
														if (isset($user_uploaded_documents['dce']['chemical']['status']) && $user_uploaded_documents['dce']['chemical']['status'] == '1') {
															$public_document = true;
														}
													@endphp
													<div class="row add-more-section" id="dce_chemical">
														<div class="col-xs-6 col-sm-7">
															<span style="color:#47c4f0;float: left;">
																<i class="fa fa-check-circle-o" style="font-size: 22px;"></i>
															</span>
															<span style="margin-left: 10px;padding-top: 2px;float: left;color: #47c4f0;">Chemical</span>
														</div>
														<div class="col-sm-3"></div>
														<div class="col-xs-6 col-sm-2">

															@if ((!isset($user_uploaded_documents['dce']['chemical'])) || (isset($user_uploaded_documents['dce']['chemical']) && count($user_uploaded_documents['dce']['chemical']['user_documents']) < 2))
																<label>
																	<a href="javascript:;" class="btn btn-success edit_doc pull-right" data-type='dce' data-current="menu12_" data-type-id="chemical">Add New Image</a>
																</label>
															@endif
														</div>
														<div class="col-xs-12 col-sm-12">
															@php 
																$i = 1;
																$chemical_count = 0;
																// dd($user_uploaded_documents);
															@endphp
															
															@if (isset($user_uploaded_documents['dce']['chemical']['user_documents'])  && count($user_uploaded_documents['dce']['chemical']['user_documents']) != 0)
																@foreach ($user_uploaded_documents['dce']['chemical']['user_documents'] as $chemicalImages)
																	@php $chemical_count++; @endphp
																	<div class="col-xs-12 col-sm-2">
																		<div class="avatar-upload">
																			<div class="avatar-delete delete_doc" data-id="{{ $chemicalImages['id'] }}" data-imagename="{{ $chemicalImages['document_path'] }}" data-type="chemical" data-current="menu12_">
																				<label for="imageUpload"></label>
																			</div>

																			<a class="lightbox" href="#preview-chemical-{{ $i }}">
																				<div class="avatar-view" data-id="{{ $chemicalImages['id'] }}" data-imagename="{{ $chemicalImages['document_path'] }}" data-type="chemical">
																					<label for="imageUpload"></label>
																				</div>
																			</a>
																			{{-- {{ dd($chemicalImages) }} --}}
																			@if(isset($chemicalImages['document_path']))
																				@php
																					$storage_path = 'public/uploads/user_documents/';
																					$type = 'dce';
																					$typeId = 'chemical';
																					$url = url($storage_path . $user_id . "/" . $type . "/" . $typeId . '/' . $chemicalImages['document_path']);
																				@endphp

																				<div class="avatar-preview">
																					<div class="avatar-edit-img" id="chemical-{{ $i }}">
																						<img src="{{ $url }}" class="img-thumbnail">
																					</div>
																					<div class="lightbox-target" id="preview-chemical-{{ $i }}">
																						<img src="{{ $url }}"/>
																						<a class="lightbox-close" href="#chemical-{{ $i }}"></a>
																					</div>
																				</div>
																			@else
																				<div class="avatar-preview">
																					<div class="avatar-edit-img" id="chemical-{{ $i }}">
																						<img src="{{ url('public/assets/images/jpg.png') }}" data-action="zoom" class="pull-left" style="height: 130px; width:130px;">
																					</div>
																				</div>
																			@endif
																		</div>
																	</div>
																@endforeach
															@endif
															@php $i++; @endphp
															@if($chemical_count < 2)
																@for($j = $chemical_count; $j < 2; $j++)
																	<div class="col-xs-12 col-sm-2">
																		<div class="avatar-upload">
																			<div class="avatar-preview">
																				<div class="avatar-edit-img" id="chemical-{{ $chemical_count + 1 }}">
																					<img src="{{url('public/assets/images/jpg.png')}}" data-action="zoom" class="pull-left" style="height: 130px;width:130px;">
																				</div>
																			</div>
																		</div>
																	</div>
																@endfor
															@endif
															<div class="col-xs-12 col-sm-4 visibility-type">
																<div class="form-group pull-right">Visibility:
																	<label class="radio-inline">
																		<input type="radio" class="chemical_show doc-permission-radio-btn" name="chemical" data-name="dce" data-type-id="chemical" value="1" {{ ($public_document == true) ? 'checked' : ''}}>Public
																	</label>
																	<label class="radio-inline">
																		<input type="radio" class="chemical_show doc-permission-radio-btn" name="chemical" data-name="dce" data-type-id="chemical" value="0" {{ ($public_document == false) ? 'checked' : ''}}>Private
																	</label>
																</div>
															</div>
														</div>
													</div>
												@endif
												@if(in_array('lequefied_gas', $dceTypes))
													@php
														$public_document = false;
														if (isset($user_uploaded_documents['dce']['lequefied_gas']['status']) && $user_uploaded_documents['dce']['lequefied_gas']['status'] == '1') {
															$public_document = true;
														}
													@endphp
													<div class="row add-more-section" id="dce_lequefied_gas">
														<div class="col-xs-6 col-sm-7">
															<span style="color:#47c4f0;float: left;">
																<i class="fa fa-check-circle-o" style="font-size: 22px;"></i>
															</span>
															<span style="margin-left: 10px;padding-top: 2px;float: left;color: #47c4f0;">Liquefied Gas</span>
														</div>
														<div class="col-sm-3"></div>
														<div class="col-xs-6 col-sm-2">
															@if ((!isset($user_uploaded_documents['dce']['lequefied_gas'])) || (isset($user_uploaded_documents['dce']['lequefied_gas']) && count($user_uploaded_documents['dce']['lequefied_gas']['user_documents']) < 2))
																<label>
																	<a href="javascript:;" class="btn btn-success edit_doc pull-right" data-type='dce' data-current="menu12_" data-type-id="lequefied_gas">Add New Image</a>
																</label>
															@endif
														</div>
														<div class="col-xs-12 col-sm-12">
															@php 
																$i = 1;
																$lequefied_gas_count = 0;
															@endphp
															@if (isset($user_uploaded_documents['dce']['lequefied_gas']['user_documents'])  && count($user_uploaded_documents['dce']['lequefied_gas']['user_documents']) != 0)
																@foreach ($user_uploaded_documents['dce']['lequefied_gas']['user_documents'] as $lequefiedGasImages)
																	@php $lequefied_gas_count++; @endphp
																	<div class="col-xs-12 col-sm-2">
																		<div class="avatar-upload">
																			<div class="avatar-delete delete_doc" data-id="{{ $lequefiedGasImages['id'] }}" data-imagename="{{ $lequefiedGasImages['document_path'] }}" data-type="lequefied_gas" data-current="menu12_">
																				<label for="imageUpload"></label>
																			</div>

																			<a class="lightbox" href="#preview-lequefied-gas-{{ $i }}">
																				<div class="avatar-view" data-id="{{ $lequefiedGasImages['id'] }}" data-imagename="{{ $lequefiedGasImages['document_path'] }}" data-type="lequefied_gas">
																					<label for="imageUpload"></label>
																				</div>
																			</a>
																			{{-- {{ dd($lequefiedGasImages) }} --}}
																			@if(isset($lequefiedGasImages['document_path']))
																				@php
																					$storage_path = 'public/uploads/user_documents/';
																					$type = 'dce';
																					$typeId = 'lequefied_gas';
																					$url = url($storage_path . $user_id . "/" . $type . "/" . $typeId . '/' . $lequefiedGasImages['document_path']);
																				@endphp

																				<div class="avatar-preview">
																					<div class="avatar-edit-img" id="lequefied-gas-{{ $i }}">
																						<img src="{{ $url }}" class="img-thumbnail">
																					</div>
																					<div class="lightbox-target" id="preview-lequefied-gas-{{ $i }}">
																						<img src="{{ $url }}"/>
																						<a class="lightbox-close" href="#lequefied-gas-{{ $i }}"></a>
																					</div>
																				</div>
																			@else
																				<div class="avatar-preview">
																					<div class="avatar-edit-img" id="lequefied-gas-{{ $i }}">
																						<img src="{{ url('public/assets/images/jpg.png') }}" data-action="zoom" class="pull-left" style="height: 130px; width:130px;">
																					</div>
																				</div>
																			@endif
																		</div>
																	</div>
																@endforeach
															@endif
															@php $i++; @endphp
															@if($lequefied_gas_count < 2)
																@for($j = $lequefied_gas_count; $j < 2; $j++)
																	<div class="col-xs-12 col-sm-2">
																		<div class="avatar-upload">
																			<div class="avatar-preview">
																				<div class="avatar-edit-img" id="lequefied-gas-{{ $lequefied_gas_count + 1 }}">
																					<img src="{{url('public/assets/images/jpg.png')}}" data-action="zoom" class="pull-left" style="height: 130px;width:130px;">
																				</div>
																			</div>
																		</div>
																	</div>
																@endfor
															@endif
															<div class="col-xs-12 col-sm-4 visibility-type">
																<div class="form-group pull-right">Visibility:
																	<label class="radio-inline">
																		<input type="radio" class="lequefied_gas_show doc-permission-radio-btn" name="lequefied_gas" data-name="dce" data-type-id="lequefied_gas" value="1" {{ ($public_document == true) ? 'checked' : ''}}>Public
																	</label>
																	<label class="radio-inline">
																		<input type="radio" class="lequefied_gas_show doc-permission-radio-btn" name="lequefied_gas" data-name="dce" data-type-id="lequefied_gas" value="0" {{ ($public_document == false) ? 'checked' : ''}}>Private
																	</label>
																</div>
															</div>
														</div>
													</div>
												@endif
												@if(in_array('all', $dceTypes))
													@php
														$public_document = false;
														if (isset($user_uploaded_documents['dce']['all']['status']) && $user_uploaded_documents['dce']['all']['status'] == '1') {
															$public_document = true;
														}
													@endphp
													<div class="row add-more-section" id="dce_all">
														<div class="col-xs-6 col-sm-7">
															<span style="color:#47c4f0;float: left;">
																<i class="fa fa-check-circle-o" style="font-size: 22px;"></i>
															</span>
															<span style="margin-left: 10px;padding-top: 2px;float: left;color: #47c4f0;">All</span>
														</div>
														<div class="col-sm-3"></div>
														<div class="col-xs-6 col-sm-2">
															@if ((!isset($user_uploaded_documents['dce']['all'])) || (isset($user_uploaded_documents['dce']['all']) && count($user_uploaded_documents['dce']['all']['user_documents']) < 2))
																<label>
																	<a href="javascript:;" class="btn btn-success edit_doc pull-right" data-type='dce' data-current="menu12_" data-type-id="all">Add New Image</a>
																</label>
															@endif
														</div>
														<div class="col-xs-12 col-sm-12">
															@php 
																$i = 1;
																$all_count = 0;
															@endphp
															@if (isset($user_uploaded_documents['dce']['all']['user_documents'])  && count($user_uploaded_documents['dce']['all']['user_documents']) != 0)
																@foreach ($user_uploaded_documents['dce']['all']['user_documents'] as $allImages)
																	@php $all_count++; @endphp
																	<div class="col-xs-12 col-sm-2">
																		<div class="avatar-upload">
																			<div class="avatar-delete delete_doc" data-id="{{ $allImages['id'] }}" data-imagename="{{ $allImages['document_path'] }}" data-type="all" data-current="menu12_">
																				<label for="imageUpload"></label>
																			</div>

																			<a class="lightbox" href="#preview-all-{{ $i }}">
																				<div class="avatar-view" data-id="{{ $allImages['id'] }}" data-imagename="{{ $allImages['document_path'] }}" data-type="all">
																					<label for="imageUpload"></label>
																				</div>
																			</a>
																			@if(isset($allImages['document_path']))
																				@php
																					$storage_path = 'public/uploads/user_documents/';
																					$type = 'dce';
																					$typeId = 'all';
																					$url = url($storage_path . $user_id . "/" . $type . "/" . $typeId . '/' . $allImages['document_path']);
																				@endphp

																				<div class="avatar-preview">
																					<div class="avatar-edit-img" id="all-{{ $i }}">
																						<img src="{{ $url }}" class="img-thumbnail">
																					</div>
																					<div class="lightbox-target" id="preview-all-{{ $i }}">
																						<img src="{{ $url }}"/>
																						<a class="lightbox-close" href="#all-{{ $i }}"></a>
																					</div>
																				</div>
																			@else
																				<div class="avatar-preview">
																					<div class="avatar-edit-img" id="all-{{ $i }}">
																						<img src="{{ url('public/assets/images/jpg.png') }}" data-action="zoom" class="pull-left" style="height: 130px; width:130px;">
																					</div>
																				</div>
																			@endif
																		</div>
																	</div>
																@endforeach
															@endif
															@php $i++; @endphp
															@if($all_count < 2)
																@for($j = $all_count; $j < 2; $j++)
																	<div class="col-xs-12 col-sm-2">
																		<div class="avatar-upload">
																			<div class="avatar-preview">
																				<div class="avatar-edit-img" id="all-{{ $all_count + 1 }}">
																					<img src="{{url('public/assets/images/jpg.png')}}" data-action="zoom" class="pull-left" style="height: 130px;width:130px;">
																				</div>
																			</div>
																		</div>
																	</div>
																@endfor
															@endif
															<div class="col-xs-12 col-sm-4 visibility-type">
																<div class="form-group pull-right">Visibility:
																	<label class="radio-inline">
																		<input type="radio" class="all_show doc-permission-radio-btn" name="all" data-name="dce" data-type-id="all" value="1" {{ ($public_document == true) ? 'checked' : ''}}>Public
																	</label>
																	<label class="radio-inline">
																		<input type="radio" class="all_show doc-permission-radio-btn" name="all" data-name="dce" data-type-id="all" value="1" {{ ($public_document == false) ? 'checked' : ''}}>Public
																	</label>
																</div>
															</div>
														</div>
													</div>
												@endif
											</div>
										@endif
									</div>
								</div>
							</div>		
											
		
<?php 

if(!count($user_uploaded_documents['wk_cop'][0]['user_documents'])){ echo '<style>#menu4{ display:none;}#course_menu4{ display:none;}</style>';}
if(!count($user_uploaded_documents['gmdss'][0]['user_documents'])){ echo '<style>#menu6{ display:none;}#gmdss_menu6{ display:none;}</style>';}
if(!count($user_uploaded_documents['ilo_medical'][0]['user_documents']) && !count($user_uploaded_documents['yellow_fever'][0]['user_documents'])){ echo '<style>#menu9{ display:none;}#medicals_vicination_menu9{ display:none;}</style>';}

if(!$coc_display){ echo '<style>#menu5{ display:none;}#coc_coe_menu5{ display:none;}</style>';}
if(!$course_certificate_display){ echo '<style>#menu7{ display:none;}#course_menu7{ display:none;}</style>';}
if(!$sea_service_display){ echo '<style>#menu8{ display:none;}#sea_service_menu8{ display:none;}</style>';}
if(count($user_uploaded_documents['passport'][0]['user_documents'])<1 && count($user_uploaded_documents['us_visa'][0]['user_documents'])<1){ echo '<style>#menu2{ display:none;}#passport_visa_menu2{ display:none;}</style>';}
if(!count($user_uploaded_documents['pancard'][0]['user_documents'])){ echo '<style>#menu11{ display:none;}#coc_coe_menu5{ display:none;}</style>';}
if(!count($user_uploaded_documents['aadhaar'][0]['user_documents'])){ echo '<style>#menu11{ display:none;}#coc_coe_menu5{ display:none;}</style>';}
if(!count($user_uploaded_documents['passbook'][0]['user_documents'])){ echo '<style>#menu11{ display:none;}#coc_coe_menu5{ display:none;}</style>';}

?>