<div class="card-container">
@if(isset($data) && !empty($data))
    <div class="row p-t-10 flex-height">
        <div class="col-md-12 col-xs-12">
            <div class="pagi">
                <div class="search-count">
                    Showing {{$pagination_data['from']}} - {{$pagination_data['to']}} of {{$pagination_data['total']}} Courses
                </div>
                <nav> 
                    <ul class="pagination pagination-sm">
                        {!! $pagination->render() !!}
                    </ul> 
                </nav>
            </div>
        </div>
    </div>
    @foreach($data as $index => $course)

        <div class="job-card">
            @if(isset($course['user_applied_courses']) && count($course['user_applied_courses']) > 0)
                <div class="applied-company" id="applied-for-course-{{$course['course_id']}}">
                    <i class="fa fa-check-circle-o applied-icon" aria-hidden="true"></i>
                    Already Applied
                </div>
            @else
                <div class="applied-company hide" id="applied-for-course-{{$course['course_id']}}">
                    <i class="fa fa-check-circle-o applied-icon" aria-hidden="true"></i>
                    Already Applied
                </div>
            @endif

            <div class="row m-0 flex-height">
                <div class="col-sm-4 col-md-3 p-0 flex-center">
                    <div class="job-company-image">
                        @if(isset($course['institute_registration_detail']['user']['profile_pic']))
                            <img class="profile-pic" src="/{{ isset($course['institute_registration_detail']['user']['profile_pic']) ? env('INSTITUTE_LOGO_PATH')."/".$course['institute_registration_detail']['user']['id']."/".$course['institute_registration_detail']['user']['profile_pic'] : 'images/user_default_image.png'}}" avatar-holder-src="{{ asset('images/user_default_image.png') }}" alt="">
                        @else
                            <img class="profile-pic" src="{{asset('images/user_default_image.png')}}" avatar-holder-src="{{ asset('images/user_default_image.png') }}" alt="" width="140px">
                        @endif
                    </div>
                </div>
                
                <div class="col-sm-8 col-md-9 col-xs-12 p-0">
                    <div class="company-discription">
                        <div class="other-discription row">
                            <div class="col-sm-12 f-18" style="font-size: 18px;">
                                {{ isset($course['institute_registration_detail']['institute_name']) ? ucfirst($course['institute_registration_detail']['institute_name']) : ''}}
                            </div>
                        </div>
                        <div class="other-discription row">
                            <div class="col-sm-6">
                                Course Type:
                                @foreach(\CommonHelper::institute_course_types() as $index => $category)
                                    {{ !empty($course['course_type']) ? $course['course_type'] == $index ? $category : '' : ''}}
                                @endforeach
                            </div>
                            <div class="col-sm-6">
                                Course Name: 
                                @foreach($all_courses as $index => $category)
                                    {{ !empty($course['course_id']) ? $course['course_id'] == $category['id'] ? $category['course_name'] : '' : ''}}
                                @endforeach
                            </div>
                        </div>

                        <?php
                            $collection = collect($course['institute_batches']);
                            
                            $batches = array_values($collection->sortBy('start_date')->toArray());
                            $course['institute_batches'] = $batches;
                            $Select = 'select';
                            //dd($course['institute_batches']);
                        ?>
                        <div class="other-discription">
                            Batches Date: 
                            <span class="ans">
                                @if(isset($course['institute_batches'][0]))
                                    <div class="batches dont-break height-equalizer-wrapper dont-break p-t-10">
                                   
                                        @foreach($course['institute_batches'] as $key => $value)
                                            <div class="date_slide date_slide_{{$course['course_id']}} z-depth-1 {{ $key == 0 ? $Select : ''}}" data-id="{{$value['id']}}" data-key="{{$course['course_id']}}">
                                                {{isset($value['start_date']) ? date('d',strtotime($value['start_date'])) : ''}}
                                                {{isset($value['start_date']) ? date('M',strtotime($value['start_date'])) : ''}}
                                            </div>
                                        @endforeach
                                    </div>
                                @else
                                    No batches found.
                                @endif
                            </span>
                        </div>

                        
                        @foreach($course['institute_batches'] as $key => $value)
                            <div class="row details details-key-{{$course['course_id']}} details-{{$value['id']}} {{ $key == 0 ? '' : 'hide'}}">
                                <div class="col-sm-6">
                                    <div class="other-discription p-l-0 p-r-0">
                                        Start Date: 
                                        <span class="ans">
                                            {{isset($value['start_date']) ? date('d-m-Y',strtotime($value['start_date'])) : ''}}
                                        </span>
                                    </div>
                                    <div class="other-discription p-l-0 p-r-0">
                                        Duration: 
                                        <span class="ans">
                                            {{isset($value['duration']) ? $value['duration'] : ''}} Days
                                        </span>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="other-discription p-l-0 p-r-0">
                                        Cost: 
                                        <span class="ans">
                                            <i class="fa fa-inr"></i>
                                            {{isset($value['cost']) ? $value['cost'] : ''}}
                                        </span>
                                    </div>
                                    <div class="other-discription p-l-0 p-r-0">
                                        Size: 
                                        <span class="ans">
                                            {{isset($value['size']) ? $value['size'] : ''}}
                                        </span>
                                    </div>
                                </div>

                                <?php
                                    $location_ids = '';
                                    $locations = '';
                                    $comma = 0;

                                    if(isset($value['institute_location']) && !empty($value['institute_location'])){
                                        $location_ids = array_values(collect($value['institute_location'])->pluck('location_id')->toArray());
                                    }
                                    
                                    // dd($course['institute_location']);
                                ?>

                                <div class="col-sm-12">
                                    <div class="other-discription">
                                        Location:
                                        <span class="ans">
                                            @if(isset($course['institute_location']))
                                                @foreach($course['institute_location'] as $index => $data1)
                                                    @if($data1['country'] == '95')
                                                        @if(!empty($location_ids) && in_array($data1['id'],$location_ids))
                                                            @if($comma == 1)
                                                                ,
                                                            @endif

                                                            {{$data1['city']['name']}} - {{ucfirst(strtolower($data1['state']['name']))}}
                                                            
                                                            <?php $comma = 1 ?>
                                                        @endif

                                                    @endif
                                                @endforeach
                                            @endif
                                        </span>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    @if((isset($role) AND $role == 'seafarer') || empty($user_id))
                        @if(isset($course['user_applied_courses']) && count($course['user_applied_courses']) > 0)
                            
                                <button  data-style="zoom-in" class="btn coss-primary-btn apply-btn course-apply-button ladda-button hide" id="course-apply-button-{{$course['course_id']}}" data-course-id="{{$course['institute_batches'][0]['id']}}" data-institute-id="{{$course['institute_id']}}" data-btn-id={{$course['course_id']}}>
                                Apply Now
                                </button>
                            
                        @else
                            <button  data-style="zoom-in" class="btn coss-primary-btn apply-btn course-apply-button ladda-button" id="course-apply-button-{{$course['course_id']}}" data-course-id="{{$course['institute_batches'][0]['id']}}" data-institute-id="{{$course['institute_id']}}" data-btn-id={{$course['course_id']}}>
                            Apply Now
                            </button>
                        @endif
                    @endif
                </div>
            </div>
        </div>
    @endforeach
    <div class="row p-t-10 flex-height">
        <div class="col-md-12 col-xs-12">
            <div class="pagi">
                <div class="search-count">
                    Showing {{$pagination_data['from']}} - {{$pagination_data['to']}} of {{$pagination_data['total']}} Courses
                </div>
                <nav> 
                <ul class="pagination pagination-sm">
                    {!! $pagination->render() !!}
                </ul> 
                </nav>
            </div>
        </div>
    </div>
</div>
@else
    <div class="row no-results-found">No results found. Try again with different search criteria.</div>
@endif
</div>