<div class="overlay overlay-fadein"></div>
<div class="dashboard-menu">
	<div class="menubar-btn-down"><i class="fa fa-angle-double-down" aria-hidden="true"></i></div>
	<div class="menubar-btn-up"><i class="fa fa-angle-double-up" aria-hidden="true"></i></div>
	<div class="menubar-name">Dashboard Menu</div>	
</div>
<div class="navbar-container">
	<ul class="nav nav-pills nav-stacked nav-pills-stacked-example">
	  	<li class="nav-link {{Route::currentRouteName() == 'user.profile' ? 'nav-active' : '' }}">
	  		<a href="{{ route('user.profile') }}"><i class="fa fa-building-o nav-icon" aria-hidden="true"></i>My Profile</a>
	  	</li> 
	  	<li class="nav-link {{Route::currentRouteName() == 'user.bookings'? 'nav-active' : '' }}">
	  		<a href="{{ route('user.bookings') }}"><i class="fa fa-file-text-o nav-icon" aria-hidden="true"></i><span style="display: inline-table;">My Bookings </span></a>
	  	</li>
    </ul>
</div>