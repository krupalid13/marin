<?php 
    $seafarer_data = $data->toArray();
?>
@if(isset($seafarer_data['data']) and !empty($seafarer_data['data']))

<div class="section-2" id="filter-results">
    <span class="heading">
        Candidate Search Results
    </span>
    <span class="pagi pagi-up">
        <span class="search-count">
            Showing {{$seafarer_data['from']}} - {{$seafarer_data['to']}} of {{$seafarer_data['total']}} Candidates
        </span>
        <nav> 
        <ul class="pagination pagination-sm">
            {!! $data->render() !!}
        </ul> 
        </nav>
    </span>

    @foreach($seafarer_data['data'] as $seafarer_details)

        <div class="search-result-card">
            <div class="row m-0">
                <div class="col-sm-3 p-0">
                    <div class="profile-pic-container">
                        <img class="profile-pic" src="/{{ isset($seafarer_details['profile_pic']) ? env('SEAFARER_PROFILE_PATH')."/".$seafarer_details['id']."/".$seafarer_details['profile_pic'] : 'images/user_default_image.png'}}" avatar-holder-src="{{ asset('images/user_default_image.png') }}" alt="">

                        <div class="profile-btn-container">
                            <!-- <a href="{{route('site.company.candidate.download-cv',$seafarer_details['id'])}}" class="btn coss-inverse-btn cv-btn download-cv" seafarer-index='{{$seafarer_details['id']}}'>Download CV</a>
                            <a class="btn coss-primary-btn view-profile-btn" target="_blank" href="{{route('user.view.profile',[$seafarer_details['id']])}}">View Profile</a> -->
                            <a class="btn coss-inverse-btn cv-btn check-company-subscription" seafarer-index="{{$seafarer_details['id']}}" data-type="{{config('feature.feature1')}}">Download CV</a>
                            <a class="btn coss-primary-btn view-profile-btn" target="_blank" href="{{route('user.view.profile',[$seafarer_details['id']])}}">View Profile</a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-9">
                    <div class="company-discription">
                    <div class="company-name">
                        {{isset($seafarer_details['first_name']) ? ucfirst($seafarer_details['first_name'])." ".ucfirst($seafarer_details['last_name']) : ''}}
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="other-discription">
                                Current Rank: 
                                <span class="ans">
                                    @foreach(\CommonHelper::new_rank() as $rank_index => $category)
                                        @foreach($category as $r_index => $rank)
                                            {{ !empty($seafarer_details['professional_detail']['current_rank']) ? $seafarer_details['professional_detail']['current_rank'] == $r_index ? $rank : '' : ''}}
                                        @endforeach
                                    @endforeach
                                </span>
                            </div>
                            <?php
                                if(isset($seafarer_details['professional_detail']['current_rank_exp'])){
                                    $rank_exp = explode(".",$seafarer_details['professional_detail']['current_rank_exp']);
                                    $rank_exp[1] = number_format($rank_exp[1]);
                                }
                            ?>
                            <div class="other-discription">
                                Rank Experience: 
                                <span class="ans">
                                    {{ isset($seafarer_details['professional_detail']['current_rank_exp']) ? $rank_exp[0] ." Year ". $rank_exp[1] .' Months' : ''}}
                                </span>
                            </div>
                            <div class="other-discription">
                                Applied Rank: 
                                <span class="ans">
                                    @foreach(\CommonHelper::new_rank() as $rank_index => $category)
                                        @foreach($category as $r_index => $rank)
                                            {{ !empty($seafarer_details['professional_detail']['applied_rank']) ? $seafarer_details['professional_detail']['applied_rank'] == $r_index ? $rank : '' : ''}}
                                        @endforeach
                                    @endforeach
                                </span>
                            </div>
                            <div class="other-discription">
                                Availability: 
                                <span class="ans">{{isset($seafarer_details['professional_detail']['availability']) ? date('d-m-Y',strtotime($seafarer_details['professional_detail']['availability'])) : ''}}
                                </span>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="other-discription">
                                Nationality: 
                                <span class="ans">
                                    @foreach( \CommonHelper::countries() as $c_index => $country)
                                        {{ !empty($seafarer_details['personal_detail']['nationality']) ? $seafarer_details['personal_detail']['nationality'] == $c_index ? $country : '' : ''  }}
                                    @endforeach
                                </span>
                            </div>
                            <div class="other-discription">
                                Passport: 
                                <span class="ans">
                                    @if(!empty($seafarer_details['passport_detail']['pass_country']))
                                        @foreach( \CommonHelper::countries() as $c_index => $country)
                                            {{ !empty($seafarer_details['passport_detail']['pass_country']) ? $seafarer_details['passport_detail']['pass_country'] == $c_index ? $country : '' : ''  }}
                                        @endforeach
                                    @else
                                        -
                                    @endif
                                </span>
                            </div>
                            <div class="other-discription">
                                COC: 
                                <span class="ans">
                                    @if(!empty($seafarer_details['coc_detail'][0]['coc']))
                                        @foreach( \CommonHelper::countries() as $c_index => $country)
                                            {{ !empty($seafarer_details['coc_detail'][0]['coc']) ? $seafarer_details['coc_detail'][0]['coc'] == $c_index ? $country : '' : ''  }}
                                        @endforeach
                                    @else
                                        -
                                    @endif
                                </span>
                            </div>
                            <div class="other-discription">
                                CDC: 
                                <span class="ans">
                                    @if(!empty($seafarer_details['coc_detail'][0]['cdc']))
                                        @foreach( \CommonHelper::countries() as $c_index => $country)
                                            {{ !empty($seafarer_details['seaman_book_detail'][0]['cdc']) ? $seafarer_details['seaman_book_detail'][0]['cdc'] == $c_index ? $country : '' : ''  }}
                                        @endforeach
                                    @else
                                        -
                                    @endif
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
            </div>
        </div>
    @endforeach
    
    <span class="pagi m-t-25">
        <span class="search-count">
            Showing {{$seafarer_data['from']}} - {{$seafarer_data['to']}} of {{$seafarer_data['total']}} Candidates
        </span>
        <nav> 
        <ul class="pagination pagination-sm">
            {!! $data->render() !!}
        </ul> 
        </nav>
    </span>
                        
@endif