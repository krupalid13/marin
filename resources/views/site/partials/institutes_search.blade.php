@if(isset($data) && !empty($data))
    @foreach($data as $index => $course)
        <tr class="flexRow">
            <td class="flexCol">
                <div class="content name">
                    
                    <span class="value">
                        {{ isset($course['course_details']['institute_registration_details']['institute_name']) ? ucfirst($course['course_details']['institute_registration_details']['institute_alias']) : '-'}}
                    </span>
                </div>
            </td>
            <td class="flexCol">
                <div class="content name">
                   
                    <span class="value">
                     

                        @foreach($all_courses as $index => $category)
                            {{ !empty($course['course_details']['course_id']) ? $course['course_details']['course_id'] == $category['id'] ? $category['course_name'] : '' : ''}}
                        @endforeach
                    </span>
                </div>
            </td>
            <td class="flexCol">
                <div class="content date">
                    <span class="value">
                        <?php                           
                         $approved_by = '';
                         if($course['course_details']['approved_by'] == '1') {
                            $approved_by = 'DG India';
                         }elseif ($course['course_details']['approved_by'] == '2') {
                             $approved_by = 'OPITO';
                         }else {
                            $approved_by = 'Value Added';
                         }
                        ?>
                      {{$approved_by}}
                    </span>
                </div>
            </td>
            <td class="flexCol">
                <div class="content loc">
                    <span class="value">
                        @foreach($course['batch_location'] as $index => $location)
                            {{ !empty( $location['location']['city']['name']) ?  $location['location']['city']['name'] : ''}}@if(count($course['batch_location']) > $index+1),@endif
                        @endforeach
                    </span>
                </div>
            </td>
            
            <td class="flexCol">
                <div class="content duration">
                    <span class="value">
                        {{ !empty($course['duration']) ? $course['duration'] : ''}} days
                    </span>
                </div>
            </td>
            <td class="flexCol">
                <div class="content date">
                    <span class="value">
                        {{ !empty($course['course_start_date']) ? date('d M Y', strtotime($course['course_start_date'])) : 'On Demand'}}

                        <?php
                            if($course['batch_type'] == 'on_demand' && empty($course['course_start_date']) ) {
                                echo "<span class='sub-date'> ( " . date('M y', strtotime($course['start_date'])) . ")</span>";
                            }
                        ?>
                    </span>
                </div>
            </td>                            
            <td class="flexCol">
                <div class="content fee">
                    <span class="value">
                        <?php $discount = ''; ?>
                        @if(isset($course['batch_discount']['discount']) && !empty($course['batch_discount']['discount']) && isset($course['batch_discount']['company_registration']['advertisment_details']) && !empty($course['batch_discount']['company_registration']['advertisment_details']))
                            <?php
                                $discount = $course['batch_discount']['discount'];

                                $cost = !empty($course['cost']) ? $course['cost'] : '';
                                $discounted_cost = $cost - ($cost*$discount/100);
                            ?>
                            <div class="striked_cost">{{$cost}}</div><br>
                            <div class="new_cost">{{ !empty($discounted_cost) ? $discounted_cost : ''}}</div>

                        @else
                            <div class="new_cost">{{ !empty($course['cost']) ? $course['cost'] : ''}}</div> 
                        @endif

                        <?php
                            if($course['available_size'] <= 3) {
                                echo "<span class='sub-date span-redColor'>".$course['available_size']." seat left</span>";
                            }
                        ?>
                    </span>
                </div>
            </td>
            
            @if(isset($discount) && !empty($discount))
                <td class="flexCol">
                    <div class="content discount">
                        <div class="burst">{{$discount}}%</div>
                        <div class="text">Discount</div>
                    </div>
                </td>
            @else
                <td class="flexCol">
                    <div class="content discount">
                        <div class="">&nbsp;</div>
                        <div class="text">&nbsp;</div>
                    </div>
                </td>
            @endif
            <td class="flexCol">
                <div class="content cta">
                    <a data-batch="{{$course['id']}}" class="book_cta book_institute_batch">Book</a>
                </div>
            </td>
        </tr>
    @endforeach
    <div class="row">
        <div class="col-sm-12">
            <div class="pull-right" id="couse_pagi">
                {{ $pagination->render() }}
            </div>
        </div>
    </div>
@else
    <tr class="flexRow">
        <td colspan="7"  class="flexCol">
            No results found. Try again with different search criteria.
        </td>
    </tr>
@endif