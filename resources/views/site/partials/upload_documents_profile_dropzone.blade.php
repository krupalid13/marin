<?php $index = 0;$doc = 100; ?>

<?php
	if(isset($user_uploaded_documents) && !empty($user_uploaded_documents)){
		$all_user_uploaded_documents = $user_uploaded_documents;
	}else{
		$all_user_uploaded_documents = [];
	}

	$active_class = "active";
	$active_class_tab = "active";
	$all_user_uploaded_documents = json_encode($all_user_uploaded_documents);
?>
<script type="text/javascript">
	var documents = <?php echo $all_user_uploaded_documents ?>;
</script>

<style type="text/css">
	.dz-message{
		display: none;
	}
	.dz-success-mark{
		display: none !important;
	}
</style>
<div class="row">
	<div class="document_slide">
		@foreach($documents as $key => $value)
			@if(isset($documents[$key][0]) AND !empty($documents[$key][0]))
				<?php
					$hide = '';
					if(isset($documents[$key][0]['hide'])){
						$hide = 'hide';
					}
					// dd($user_uploaded_documents[$key][$document_count+1],$public_document);

				?>
				@if($hide != 'hide')
					<div class="document_slide_tab documents-tab {{$active_class_tab}}" data-block="{{$key}}-block">
						<div class="document_tab_heading">
							{{ isset($documents[$key][0]['title']) ? strtoupper($documents[$key][0]['title']) : ''}}
						</div>
						<!-- <span class="doc_count">
							{{count($documents[$key])}} Documents Uploaded
						</span>	 -->
					</div>
				@endif

			@else
				<?php
					$hide = '';
					if(isset($value['hide'])){
						$hide = 'hide';
					}
				?>
				@if($hide != 'hide')
					<div class="document_slide_tab documents-tab {{$active_class_tab}}" data-block="{{$key}}-block">
						<div class="document_tab_heading">
							{{ isset($value['title']) ? strtoupper($value['title']) : ''}} 34
						</div>
					</div>
				@endif
			@endif

			<?php  $active_class_tab = ""; ?>
		@endforeach
	</div>
</div>

@foreach($documents as $key => $value)
	@if(isset($documents[$key][0]) AND !empty($documents[$key][0]))

		<?php
			$hide = '';
			if(isset($documents[$key][0]['hide'])){
				$hide = 'hide';
			}
			// dd($user_uploaded_documents[$key][$document_count+1],$public_document);

		?>
		<div class="doc-section {{$key}}-block {{$active_class}}">
			@if($hide != 'hide')
			<div class="row {{$hide}}">
				<div class="col-xs-12 m-t-15 upload-title-section">
					<div class="title">{{ isset($documents[$key][0]['title']) ? strtoupper($documents[$key][0]['title']) : ''}}
					</div>
				</div>
			</div>
			@endif
			@foreach($documents[$key] as $document_count => $details)
				<?php
					$private_document = false;
					$public_document = false;

					if(isset($user_uploaded_documents[$key][$document_count+1]['status']) && $user_uploaded_documents[$key][$document_count+1]['status'] == '1'){
						$public_document = true;
					}else{
						$private_document = true;
					}
					// dd($user_uploaded_documents[$key][$document_count+1],$public_document);

				?>
				<form class="dropzone" id="real-dropzone{{$doc}}" files="true" action="{{route('site.user.upload',['type' => $key,'type_id' => $document_count+1])}}">
					{{ csrf_field() }}
					<input type="hidden" class="doc_index" value={{$doc}}>
					<div class="main_dropzone_view">
						<div class="row">
							<div class="col-xs-6 col-sm-6">
								<div class="heading p-t-0">
									{{strToUpper($key)}} {{$document_count+1}}
								</div>
							</div>
						</div>
						<div class="row main-dropzone-row">
							<div class="col-xs-12 col-sm-8">
								<div class="fallback">
									<input name="file" type="file" multiple />
								</div>
								<div id="view_dropzone_files{{$doc}}">
									<!-- <a class="dz-remove" href="javascript:undefined;" data-dz-remove=""></a> -->
								</div>
							</div>
						</div>
					</div>
				</form>
				<script type="text/javascript">

						Dropzone.autoDiscover = false;

						var index = {{$doc}};
						var type = '{{$key}}';
						var type_id = '{{$document_count+1}}';
						var container = "#view_dropzone_files"+index;
						var name = "real-dropzone"+index;
						var photo_counter = 0;

						var myDropzone = new Dropzone("#"+name, {
					        uploadMultiple: false,
					        parallelUploads: 100,
					        maxFilesize: 5,
					        previewsContainer: container,
					        // previewTemplate: document.querySelector('#preview-template').innerHTML,
					        addRemoveLinks: false,
					        dictRemoveFile: 'Remove',
					        dictFileTooBig: 'Image is bigger than 5MB',
					        // The setting up of the dropzone
					        init:function() {

					        	this.on("thumbnail", function(file) {
					        		$(file.previewTemplate).attr('data-img', file.image);
					        		$(file.previewTemplate).attr('data-img-id', file.image_id);
					        		$(file.previewTemplate).attr('data-img-type', file.image_type);
					        		$(file.previewTemplate).attr('data-img-type-id', file.image_type_id);

					        	});

					        	var docsDropzone = this;

					        	if(documents){
					        		$.each(documents, function (key,value){
					        			if(type == key){

					        				if(value[type_id]){
						        				$.each(value[type_id]['user_documents'], function(k,v){

						        					var extension = v.document_path.split('.').pop();

						        					if(extension == 'jpg' || extension == 'jpeg'){
						        						var logo = '/images/jpg.jpg';
						        					}
						        					else if(extension == 'pdf'){
						        						var logo = '/images/pdf.png';
						        					}
						        					else if(extension == 'gif'){
						        						var logo = '/images/gif.png';
						        					}
						        					else if(extension == 'png'){
						        						var logo = '/images/png.png';
						        					}
						        					else if(extension == 'doc' || extension == 'docx'){
						        						var logo = '/images/doc.png';
						        					}else {
						        						var logo = '/images/file.png';
						        					}

								        			var mockFile = { name: logo, size: 0, status: 'success' , image:v.document_path , image_id: v.id, image_type: type , image_type_id:type };

													docsDropzone.emit("addedfile", mockFile );
													docsDropzone.emit("thumbnail", mockFile, logo );
													mockFile.previewElement.classList.add('dz-success');
													mockFile.previewElement.classList.add('dz-complete');
						        				});
						        			}
					        			}

					        		});
					        	}

					            this.on("removedfile", function(file) {
					            	var file_name = $(file.previewTemplate).attr('data-img');
				        			var image_id = $(file.previewTemplate).attr('data-img-id');
				        			var type = $(file.previewTemplate).attr('data-img-type');
				        			var type_id = $(file.previewTemplate).attr('data-img-type-id');

					                $.ajax({
					                    type: 'POST',
					                    url: $("#api-seafarer-delete-document").val(),
					                    headers: {
					                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content'),
					                    },
					                    data: {name: file_name, id: image_id, type: type, type_id: type_id, _token : $('meta[name="_token"]').attr('content')},
					                    dataType: 'html',
					                    success: function(data){
					                        var rep = JSON.parse(data);
					                        if(rep.code == 200)
					                        {
					                            photo_counter--;
					                            $("#photoCounter").text( "(" + photo_counter + ")");
					                        }

					                        toastr.success(rep.message, 'Success');

					                    }
					                });

					            } );
					        },
					        error: function(file, response) {
					            if($.type(response) === "string")
					                var message = response; //dropzone sends it's own error messages in string
					            else
					                var message = response.message;
					                file.previewElement.classList.add("dz-error");
					                _ref = file.previewElement.querySelectorAll("[data-dz-errormessage]");
					                _results = [];
					                for (_i = 0, _len = _ref.length; _i < _len; _i++) {
					                    node = _ref[_i];
					                    _results.push(node.textContent = message);
					                }
					            return _results;
					        },
					        success: function(file,response) {

					        	toastr.success(response.message, 'Success');
					        	$(file.previewTemplate).attr('data-img',response.filename);
				        		$(file.previewTemplate).attr('data-img-id',response.id);
				        		$(file.previewTemplate).attr('data-img-type',response.type);
				        		$(file.previewTemplate).attr('data-img-type-id',response.type_id);
					            photo_counter++;
					            $("#photoCounter").text( "(" + photo_counter + ")");
					        }
					    });

				</script>
				<?php $doc++; ?>
			@endforeach
		</div>
	@else

		<?php
			$private_document = false;
			$public_document = false;

			if(isset($user_uploaded_documents[$key][0]['status']) && $user_uploaded_documents[$key][0]['status'] == '1'){
				$public_document = true;
			}else{
				$private_document = true;
			}

			$hide = '';
			if(isset($value['hide'])){
				$hide = 'hide';
				$active_class = '';
			}

		?>

		<div class="doc-section {{$key}}-block {{$active_class}}">
			@if($hide != 'hide')
			<div class="row {{$hide}}">
				<div class="col-xs-12 m-t-15 upload-title-section">
					<div class="title">{{ isset($value['title']) ? strtoupper($value['title']) : ''}}
					</div>
				</div>
			</div>
			@endif
			<form class="dropzone" id="real-dropzone{{$index}}" files="true" action="{{route('site.user.upload',['type' => $key])}}">
				{{ csrf_field() }}
				<input type="hidden" class="index" value={{$index}}>
				<div class="main_dropzone_view">
					<div class="row">
						<div class="col-xs-6 col-sm-6">
							<div class="heading p-t-0">
								{{strToUpper($key)}}
							</div>
						</div>
					</div>
				</div>
				<div class="row main-dropzone-row">
					<div class="col-xs-12 col-sm-8">
						<div class="fallback">
							<input name="file" type="file" multiple />
						</div>
						<div id="view_dropzone_files{{$index}}">

						</div>
					</div>
				</div>
			</form>
		</div>
		<script type="text/javascript">

				Dropzone.autoDiscover = false;

				var index = {{$index}};
				var type = '{{$key}}';
				var container = "#view_dropzone_files"+index;
				var name = "real-dropzone"+index;

				var photo_counter = 0;

				var myDropzone = new Dropzone("#"+name, {
			        uploadMultiple: false,
			        parallelUploads: 100,
			        maxFilesize: 5,
			        previewsContainer: container,
			        // previewTemplate: document.querySelector('#preview-template').innerHTML,
			        addRemoveLinks: false,
			        dictRemoveFile: 'Remove',
			        dictFileTooBig: 'Image is bigger than 5MB',
			        // The setting up of the dropzone
			        init:function() {

			        	this.on("thumbnail", function(file) {
			        		$(file.previewTemplate).attr('data-img', file.image);
			        		$(file.previewTemplate).attr('data-img-id', file.image_id);
			        		$(file.previewTemplate).attr('data-img-type', file.image_type);

			        	});

			        	var docsDropzone = this;

			        	if(documents){
			        		$.each(documents, function (key,value){

			        			if(type == key){

			        				$.each(value[0]['user_documents'], function(k,v){

			        					var extension = v.document_path.split('.').pop();

			        					if(extension == 'jpg' || extension == 'jpeg'){
			        						var logo = '/images/jpg.jpg';
			        					}
			        					else if(extension == 'pdf'){
			        						var logo = '/images/pdf.png';
			        					}
			        					else if(extension == 'gif'){
			        						var logo = '/images/gif.png';
			        					}
			        					else if(extension == 'png'){
			        						var logo = '/images/png.png';
			        					}
			        					else if(extension == 'doc' || extension == 'docx'){
			        						var logo = '/images/doc.png';
			        					}else {
			        						var logo = '/images/file.png';
			        					}

					        			var mockFile = { name: logo, size: 0, status: 'success' , image:v.document_path , image_id: v.id, image_type: type };

										docsDropzone.emit("addedfile", mockFile );
										docsDropzone.emit("thumbnail", mockFile, logo );
										mockFile.previewElement.classList.add('dz-success');
										mockFile.previewElement.classList.add('dz-complete');
			        				});
			        			}

			        		});
			        	}

			            this.on("removedfile", function(file) {
			        		var file_name = $(file.previewTemplate).attr('data-img');
			        		var image_id = $(file.previewTemplate).attr('data-img-id');
			        		var type = $(file.previewTemplate).attr('data-img-type');

			                $.ajax({
			                    type: 'POST',
			                    url: $("#api-seafarer-delete-document").val(),
			                    headers: {
			                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content'),
			                    },
			                    data: {name: file_name, id: image_id, type: type, _token : $('meta[name="_token"]').attr('content')},
			                    dataType: 'html',
			                    success: function(data){
			                        var rep = JSON.parse(data);
			                        if(rep.code == 200)
			                        {
			                            photo_counter--;
			                            $("#photoCounter").text( "(" + photo_counter + ")");
			                        }

			                        toastr.success(rep.message, 'Success');
			                    }
			                });

			            });
			        },
			        error: function(file, response) {
			            if($.type(response) === "string")
			                var message = response; //dropzone sends it's own error messages in string
			            else
			                var message = response.message;
			                file.previewElement.classList.add("dz-error");
			                _ref = file.previewElement.querySelectorAll("[data-dz-errormessage]");
			                _results = [];
			                for (_i = 0, _len = _ref.length; _i < _len; _i++) {
			                    node = _ref[_i];
			                    _results.push(node.textContent = message);
			                }
			            return _results;
			        },
			        success: function(file,response) {
			        	toastr.success(response.message, 'Success');
			        	$(file.previewTemplate).attr('data-img',response.filename);
			        	$(file.previewTemplate).attr('data-img-id',response.id);
			        	$(file.previewTemplate).attr('data-img-type',response.type);

			            photo_counter++;
			            $("#photoCounter").text( "(" + photo_counter + ")");
			        }
			    });

		</script>
		<?php $index++; ?>
	@endif
	<?php $active_class= "hide"; ?>
@endforeach