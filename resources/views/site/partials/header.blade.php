<header class="cs-header">
    <div class="container">
        <nav class="navbar navbar-default z-depth-1">
            <div class="navbar-header">
                <div class="hide-on-med-and-up">
                    @if(\Request::route()->getName() == 'user.profile')
                    <a class="navbar-toggler sidebar-toggler" id="sidebar-toggler" href="#">
                        <span class="navbar-toggler-icon">
                            <i class="icon-menu"></i>
                        </span>
                    </a>
                    @endif
                </div>
                @if(\Request::route()->getName() == 'user.profile')
                    <button type="button" class="navbar-toggle new-navbar-toggle" data-toggle="collapse"
                            data-target=".navbar-ex1-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                @endif
                <a class="navbar-brand" href="/">
                    <!-- <img alt="coursesea logo" src="{{ URL:: asset('public/assets/images/coursealogo.png')}}"> -->
                    <img alt="flanknot logo" src="{{ URL:: asset('public/images/flanknotlogo.png')}}" style="margin-top: 4px">
                </a>
            </div>
            <div class="navbar-right">
                @if(Auth::check() AND Auth::User()->registered_as != 'admin')
                    <?php
                    $user_details = Auth::User()->toArray();
                    // if($user_details['registered_as'] == 'company'){
                    //     $profile_path = 'site.show.company.details';
                    // }
                    if ($user_details['registered_as'] == 'seafarer') {
                        $profile_path = 'user.profile';
                    }
                    // if($user_details['registered_as'] == 'advertiser'){
                    //     $profile_path = 'site.advertiser.profile';
                    // }
                    if ($user_details['registered_as'] == 'institute') {
                        $profile_path = 'site.show.institute.details';
                    }
                    ?>
                    <ul class="nav">
                        <li class="nav-item dropdown">
                            @if(Auth::check() AND Auth::User()->registered_as != 'admin')
                                <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button"
                                   aria-haspopup="true"
                                   aria-expanded="false">{{isset($user_details['first_name']) ? ucwords($user_details['first_name']) : ''}}</a>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <div class="dropdown-header text-center">
                                        <strong>Account</strong>
                                    </div>
                                    <div class="">
                                        @if(isset($profile_path) AND !empty($profile_path))
                                            <a class="dropdown-item" href="{{route($profile_path)}}"><span
                                                        class="icon-user icon"></span> <span class="">Profile</span>
                                            </a>
                                        @endif
                                        <?php 

                                            $authDetail = Auth::user();
                                            $newUnreadJobCount = App\SendedJob::where('user_id',$authDetail->id)->where('status',2)->count();

                                        ?>
                                @if(Auth::check() AND Auth::User()->affiliate_referral_by == 0)
                                    <a class="dropdown-item" href="{{URL::to('affiliate_form')}}"><i class="fa fa-user"></i> &nbsp;&nbsp;Referral</a>
                                @endif
                                        <a class="dropdown-item" href="{{route('site.seafarer.job.new')}}">
                                             <span class="icon-briefcase icon"></span><span class="">New Jobs</span> 
                                            @if($newUnreadJobCount > 0)
                                            <small style="margin-left: 6px" class="label label-success new_job_a"> New </small>
                                            @endif
                                        </a>
                                        <a class="dropdown-item" href="{{route('site.how-to')}}">
                                             <span class="icon-question icon"></span><span class="">How To ?</span> 
                                        </a>
                                        @if(Auth::User()->registered_as == 'seafarer')
                                            <a class="dropdown-item" href="{{route('user.bookings')}}"><span
                                                        class="icon-notebook icon"></span> <span class="">My Bookings</span>
                                            </a>
                                        @endif
                                        <a class="dropdown-item" href="{{route('logout')}}"><span
                                                    class="icon-lock icon"></span> <span class="">Logout</span> </a>
                                    </div>

                                </div>
                            @endif
                        </li>
                    </ul>
                @else
                    @if(\Request::route()->getName() != 'register')
                        <a href="{{route('register')}}" class="btn cs-primary-btn navbar-btn">Register</a>
                    @endif
                    @if(\Request::route()->getName() != 'site.login')
                    <a href="{{route('site.login')}}" class="btn cs-primary-btn navbar-btn">Login</a>
                    @endif
                @endif
            </div>
        </nav>
    </div>
</header>