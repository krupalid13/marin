@if(isset($course_details) && !empty($course_details))
	<?php $course_count = 1; ?>
	@foreach($course_details as $index => $courses)
		<div class="row course_detail_row course_detail_row_{{$courses['id']}}" data-course-id={{$courses['id']}}>
			<div class="col-sm-12">
				<div class="sub-details-container add-more-section">
					<div class="content-container">
						<div class="row">
							
						</div>
					</div>
					<div class="row sea_service_details_section">
						<div class="col-xs-11 col-sm-11 col-md-11"  style="margin-bottom: 10px;">
							<div class="discription">
								<span class="content-head">Certificate Name:</span>
								<span class="content"><b>
									<?php $arrData=\CommonHelper::course_name_by_course_type($courses['course_type']);
																				
										foreach($arrData as $val)
										{								
											if($courses['course_id']==$val->id)
											{
												echo $val->course_name;
											}							
										}
										
									?></b>
                                </span>
							</div>
						</div>
						<div class="col-sm-1">
							<div class="title m-b-5 display-flex-center">
								<div class="normal-course-name">
									<!--Certificate {{$course_count}}-->
								</div>
								<div class="sea-service-buttons">
									<div class="course-edit-button" data-id={{$courses['id']}} data-course-id='{{$courses['course_id']}}'>
                                	    <i class="fa fa-edit" aria-hidden="true" title="edit"></i>
                                	</div>
									<div class="course-close-button" data-id={{$courses['id']}} data-course-id='{{$courses['course_id']}}'>
                            	    	<i class="fa fa-times" aria-hidden="true" title="delete"></i>
                                	</div>
								</div>
								
                        	</div>
						</div>
						<div class="col-xs-12 col-sm-6 col-md-4" style="margin-bottom: 10px;">
							<div class="discription">
								<span class="content-head">Certificate Type:</span>
								<span class="content">
									@foreach(\CommonHelper::institute_course_types() as $c_index => $course)
																					{{ isset($courses['course_type']) ? $courses['course_type'] == $c_index  ? $course : '' : ''}}
																				@endforeach
                                </span>
							</div>
						</div>
					    
						<div class="col-xs-12 col-sm-6 col-md-5" style="margin-bottom: 10px;">
							<div class="discription">
								<span class="content-head">Certificate Number:</span>
								<span class="content">
									{{ isset($courses['certification_number']) && !empty($courses['certification_number']) ? $courses['certification_number'] : '-'}}
                                </span>
							</div>
						</div>
						<div class="col-xs-12 col-sm-6 col-md-3" style="margin-bottom: 10px;">
							<div class="discription">
								<span class="content-head">Issued By:</span>
								<span class="content">
									{{ isset($courses['issue_by']) && !empty($courses['issue_by']) ? $courses['issue_by'] : '-'}}
                                </span>
							</div>
						</div>
						<div class="col-xs-12 col-sm-4 col-md-4" style="margin-bottom: 10px;">
							<div class="discription">
								<span class="content-head">Date Of Expiry:</span>
								<span class="content">
									{{ isset($courses['expiry_date']) && !empty($courses['expiry_date']) ? date('d-m-Y',strtotime($courses['expiry_date'])) : '-'}}
                                </span>
							</div>
						</div>
						<div class="col-xs-12 col-sm-4 col-md-4" style="margin-bottom: 10px;">
							<div class="discription">
								<span class="content-head">Date Of Issue:</span>
								<span class="content">
									{{ isset($courses['issue_date']) && !empty($courses['issue_date']) ? date('d-m-Y',strtotime($courses['issue_date'])) : '-'}}
                                </span>
							</div>
						</div>

						

						

					</div>
				</div>
			</div>
		</div>
		<?php $course_count++; ?>
	@endforeach
@else
	<div class="row no-data-found">
		<div class="col-xs-12 text-center">
			<div class="discription">
				<span class="content-head">No Data Found</span>
			</div>
		</div>
	</div>
@endif