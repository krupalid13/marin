<div class="overlay overlay-fadein"></div>
<div class="dashboard-menu">
	<div class="menubar-btn-down"><i class="fa fa-angle-double-down" aria-hidden="true"></i></div>
	<div class="menubar-btn-up"><i class="fa fa-angle-double-up" aria-hidden="true"></i></div>
	<div class="menubar-name">Dashboard Menu</div>	
</div>
<div class="navbar-container">
	<ul class="nav nav-pills nav-stacked nav-pills-stacked-example">
	  	<li class="nav-link {{Route::currentRouteName() == 'site.show.institute.details' ? 'nav-active' : '' }}">
	  		<a href="{{ route('site.show.institute.details') }}"><i class="fa fa-building-o nav-icon" aria-hidden="true"></i>My Profile</a>
	  	</li> 
	  	<!-- <li class="nav-link {{Route::currentRouteName() == 'site.institute.mysubscription' ? 'nav-active' : '' }}">
	  		<a href="{{ route('site.institute.mysubscription') }}"><i class="fa fa-envelope-o nav-icon" aria-hidden="true"></i></i>My Subscriptions</a>
	  	</li>  -->
	  	<li class="nav-link {{Route::currentRouteName() == 'site.institute.course.listing' ? 'nav-active' : '' }}">
	  		<a href="{{ route('site.institute.course.listing') }}"><i class="fa fa-file-text-o nav-icon" aria-hidden="true"></i></i>Courses</a>
	  	</li>
	  	<li class="nav-link {{Route::currentRouteName() == 'site.institute.course.batches' ? 'nav-active' : '' }}">
	  		<a href="{{ route('site.institute.course.batches') }}"><i class="fa fa-envelope-o nav-icon" aria-hidden="true"></i></i>Add Batches</a>
	  	</li> 
	  	<li class="nav-link {{Route::currentRouteName() == 'site.institute.batch.listing' ? 'nav-active' : '' }}">
	  		<a href="{{ route('site.institute.batch.listing') }}"><i class="fa fa-search nav-icon" aria-hidden="true"></i>View All Batches</a>
	  	</li>
	  	<li class="nav-link {{Route::currentRouteName() == 'site.institute.list.batch.applicant'? 'nav-active' : '' }}">
	  		<a href="{{ route('site.institute.list.batch.applicant') }}"><i class="fa fa-file-text-o nav-icon" aria-hidden="true"></i>
	  			<span style="display: inline-table;">Course Bookings</span></a>
	  	</li>
	  	<li class="nav-link {{Route::currentRouteName() == 'site.institute.on.demand.booking'? 'nav-active' : '' }}">
	  		<a href="{{ route('site.institute.on.demand.booking') }}"><i class="fa fa-file-text-o nav-icon" aria-hidden="true"></i>
	  			<span style="display: inline-table;">On Demand Bookings</span></a>
	  	</li>
	  	<li class="nav-link {{Route::currentRouteName() == 'site.institute.add.advertise' ? 'nav-active' : '' }}">
	  		<a href="{{ route('site.institute.add.advertise') }}"><i class="fa fa-image nav-icon" aria-hidden="true"></i></i>Add Advertisement</a>
	  	</li>
		<li class="nav-link {{Route::currentRouteName() == 'site.institute.list.permission.requests' ? 'nav-active' : '' }}">
			<a href="{{ route('site.institute.list.permission.requests') }}"><i class="fa fa-file-text-o nav-icon" aria-hidden="true"></i>
			</i>Document Permission</a>
		</li>
		<li class="nav-link {{Route::currentRouteName() == 'site.institute.add.notice' ? 'nav-active' : '' }}">
			<a href="{{ route('site.institute.add.notice') }}"><i class="fa fa-file-text-o nav-icon" aria-hidden="true"></i>
			</i>Add Notice</a>
		</li>
		<li class="nav-link {{Route::currentRouteName() == 'site.institute.add.image.gallery' ? 'nav-active' : '' }}">
			<a href="{{ route('site.institute.add.image.gallery') }}"><i class="fa fa-image nav-icon" aria-hidden="true"></i>
			</i>Image Gallery</a>
		</li>
		<li class="nav-link {{Route::currentRouteName() == 'site.institute.view.notification' ? 'nav-active' : '' }}">
			<a href="{{ route('site.institute.view.notification') }}"><i class="fa fa-bell nav-icon" aria-hidden="true"></i>
			</i>Notifications</a>
		</li>
    </ul>
</div>