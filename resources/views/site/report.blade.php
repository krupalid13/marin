@extends('site.cms-index')
@section('content')
<div class="footer-page-container" style="margin-top: 60px;">
    <h1 class="footer-page-title">Report</h1>

    <p></p>
    <form id="report_form">
        <div class="form-group">
            <label class="input-label" for="name">Name<span
                    class="symbol required"></span></label>
            <input type="text" placeholder="Name" class="form-control" name="name" id="name">
        </div>
        <div class="form-group">
            <label class="input-label" for="email">Email<span
                    class="symbol required"></span></label>
            <input type="email" class="form-control" name="email" id="email" placeholder="Email">
        </div>
        <div class="form-group">
            <label class="input-label" for="issue">I am having issues on<span
                    class="symbol required"></span></label>
            <select class="form-control" name="issue" id="issue">
                <option value="">Select Issue</option>
                @foreach( \CommonHelper::report_issue() as $reportIssue)
                <option value="{{ $reportIssue }}">
                    {{ $reportIssue }}
                </option>
                @endforeach
            </select>
        </div>
        <div class="form-group sub_issue_div collapse">
            <label class="input-label" for="sub_issue">The precise issue is
                <span class="symbol required"></span>
            </label>
            <select class="form-control" name="sub_issue" id="sub_issue">
                
            </select>
        </div>
        <div class="form-group">
            <label class="input-label" for="title">Issue in Detail
                <span class="symbol required"></span>
            </label>
            <textarea col="5" class="form-control" name="details" id="details" style="width: 772px;height: 64px;max-width: 100%;"></textarea>
        </div>
    </form>
    <div class="form-group">
        <button type="button" class="btn btn-primary" id="submit_report">Submit</button>
    </div>
</div> <!-- <div class="footer-page-container"> -->
<input type="hidden" value="{{route('send-report')}}" id="send-report">
<script type="text/javascript">
    $(document).ready(function () {
        var subIssue = <?php echo json_encode(\CommonHelper::report_sub_issue()); ?>;
        $('#issue').change(function () {
            $('#sub_issue')
                    .find('option')
                    .remove()
                    .end();
            var issue = $(this).val();
            if (issue == "Other" || issue == "") {
                $('.sub_issue_div').addClass('collapse');
            } else {
                $('.sub_issue_div').removeClass('collapse');
                let subIssueData = subIssue[issue];
                $(subIssueData).each(function (key, Value) {
                    $('#sub_issue').append(`<option value="${Value}">
                                       ${Value}
                                  </option>`);
                });
            }
        });
        $('#submit_report').click(function () {
            if ($('#name').val() == false) {
                alert("Please enter name");
                return false;
            }
            if ($('#email').val() == false) {
                alert("Please enter email");
                return false;
            }
            var inputvalues = $('#email').val();
            var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            if (!regex.test(inputvalues)) {
                alert("invalid email id");
                return false;
            }
            if ($('#issue').val() == false) {
                alert("Please select issue");
                return false;
            }
            if ($('#details').val().trim().length == 0) {
                alert("Please enter issue in detail");
                return false;
            }
            $.ajax({
                url: $('#send-report').val(),
                data: $("#report_form").serialize(),
                type: "post",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content'),
                },
                statusCode: {
                    200: function (response) {
                        if(response.status == 'success'){
                            document.getElementById("report_form").reset();
                            $('.sub_issue_div').addClass('collapse');
                            alert(response.message);
                        }else{
                            alert("Something is wrong, Please try again later");
                        }
                    },
                    400: function (response) {
                        alert("Something is wrong, Please try again later");
                    }
                }
            });
        })
    })
</script>
@stop
