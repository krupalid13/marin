@extends('site.index')
@section('content')
<?php
	$india_value = array_search('India',\CommonHelper::countries());
	if(isset($data[0]['professional_detail']['current_rank'])){
		$required_fields = \CommonHelper::rank_required_fields()[$data[0]['professional_detail']['current_rank']];
	}
	if(isset($data[0]['document_permissions']) && !empty($data[0]['document_permissions'])){
		$document_permissions = $data[0]['document_permissions'];
	}
	if(Auth::check()){
		$registered_as = Auth::user()->registered_as;
	}
?>
<main class="main">
	<input type="hidden" class="user_id" value="{{ isset($user_id) ? $user_id : ''}}">
	<input type="hidden" name="verify_email" id="verify_email" value="{{isset($data[0]['is_email_verified']) ? $data[0]['is_email_verified'] : ''}}">
	@if(isset($data[0]['personal_detail']['country']) && $data[0]['personal_detail']['country'] == $india_value)
		<input type="hidden" name="verify_mobile" id="verify_mobile" value="{{isset($data[0]['is_mob_verified']) ? $data[0]['is_mob_verified'] : ''}}">
	@endif
    <div class="my-profile">
        <div class="container">
            <div class="section">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="d-flex block-small">
                            <div class="d-flex--one">
                                <span class="dark-blue title">My Profile</span>
                            </div>
                            @if($user != 'another_user')
                            <div class="d-flex--two">
								{{--<a href="/resume/{{Auth::user()->id}}" class="btn cs-secondary-btn resume-download ladda-button view_resume_btn" target="blank" data-style="zoom-in" seafarer-index="{{$data[0]['id']}}" style="width: 140px !important;">View Resume</a>--}}
                                <a href="{{route('graph.index')}}" class="btn cs-secondary-btn btn" target="blank" data-style="zoom-in" style="width: 140px !important;">View Graph</a>
                                <a href="{{route('create-pdf',['resume'=>\CommonHelper::encodeKey($show_resume)])}}" class="btn cs-secondary-btn btn" target="blank" data-style="zoom-in" style="width: 140px !important;">View Resume</a>
                                <a class="btn cs-primary-btn" href="{{ route('site.seafarer.edit.profile') }}">Edit Profile</a>
								<a class="btn cs-primary-btn" href="{{ route('documents.download') }}">Documents Document</a>
								<a class="btn cs-primary-btn" href="{{ route('share.index') }}">Share Document</a>
								<?php
								//echo $download_all_path = route('site.user.get.documents.path',['user_id' => $data[0]['id']]);
								?>
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-4">
                        <div class="card z-depth-1">
                            <div class="card-body top-borderradius">
                            	@if(isset($data[0]['profile_pic']))
									<div class="logo" style="background-image:url('{{env('AWS_URL') . 'public/images/uploads/seafarer_profile_pic/'}}{{$data[0]['id']}}/{{$data[0]['profile_pic']}}')"></div>
								@else
									<div class="logo" style="background-image:url({{ asset('images/user_default_image.png') }})"></div>
								@endif
                            </div>
                            <div class="card-footer">
                                <div class="logo-title">{{ !empty($data[0]['first_name']) ? ucwords($data[0]['first_name']) : ''}}</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-8">
                        <div class="card">
                            <div class="card-header">
                                <div>User Details</div>
                            </div>
                            <div class="card-body z-depth-1">
                                <div>
                                    <!-- Nav tabs -->
                                    <ul class="nav nav-tabs course_list" role="tablist">
                                        <li role="presentation" class="course_list_item active">
                                            <a href="#bd" aria-controls="bd" role="tab" data-toggle="tab">Basic Details</a>
                                        </li>
                                        <li role="presentation" class="course_list_item">
                                            <a href="#ld" aria-controls="ld" role="tab" data-toggle="tab">Location Details</a>
                                        </li>
                                        <li role="presentation" class="course_list_item">
                                            <a href="#pd" aria-controls="pd" role="tab" data-toggle="tab">Professional Details</a>
                                        </li>
                                    </ul>
                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane active" id="bd">
                                            <div class="content">
                                                <div class="row">
                                                    <div class="col-md-5">
                                                        <div class="list">
                                                            <div class="list-item">
                                                                <div class="text">
                                                                    <span class="black-text">Email</span> :
                                                                    <span class="blue">
                                                                    	{{ isset($data[0]['email']) ? $data[0]['email'] : ''}}

                                                                    	@if($data[0]['is_email_verified'] == '1')
																			<i class="fa fa-check-circle green f-15" aria-hidden="true" title="Email Verified"></i>
																		@else
																			<i class="fa fa-times-circle red-icon f-15" aria-hidden="true" title="Click on verify in verfication email"></i>
																		@endif
                                                                    </span>
                                                                </div>
                                                            </div>
                                                            <div class="list-item">
                                                                <div class="text">
                                                                    <span class="black-text">Mobile Number</span> :
                                                                    <span class="blue">
                                                                    	{{ isset($data[0]['mobile']) ? $data[0]['mobile'] : '' }}

                                                                    	@if($data[0]['is_mob_verified'] == '1')
																			<i class="fa fa-check-circle green f-15" aria-hidden="true" title="Mobile Verified"></i>
																		@else
																			<i class="fa fa-minus-circle red-icon f-15" aria-hidden="true" title="Mobile Unverified"></i>
																		@endif
                                                                    </span>
                                                                </div>
                                                            </div>
                                                            <div class="list-item">
                                                                <div class="text">
                                                                    <span class="black-text">Gender</span> :
                                                                    <span class="blue">
                                                                    	{{ !empty($data[0]['gender']) ?  $data[0]['gender'] == 'M'? 'Male' : 'Female' : 'Male' }}
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-7">
                                                        <div class="list">
                                                            <div class="list-item">
                                                                <div class="text">
                                                                    <span class="black-text">About me</span> :
                                                                    <span class="blue">
                                                                        {{$data[0]['professional_detail']['about_me']}}
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div role="tabpanel" class="tab-pane" id="ld">
                                            <div class="content">
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <div class="list">
                                                            <div class="list-item">
                                                                <div class="text">
                                                                    <span class="black-text">Country</span> :
                                                                    <span class="blue">
                                                                    	@foreach( \CommonHelper::countries() as $c_index => $country)
																			{{ isset($data[0]['personal_detail']['country']) ? $data[0]['personal_detail']['country'] == $c_index ? $country : '' : ''}}
																		@endforeach
                                                                    </span>
                                                                </div>
                                                            </div>
                                                            <div class="list-item">
                                                                <div class="text">
                                                                    <span class="black-text">State</span> :
                                                                    <span class="blue">
                                                                    	{{ isset($data[0]['personal_detail']['state_id']) ? $data[0]['personal_detail']['pincode']['pincodes_states'][0]['state']['name'] : (isset($data[0]['personal_detail']['state_text']) ? $data[0]['personal_detail']['state_text'] : '')}}
                                                                    </span>
                                                                </div>
                                                            </div>
                                                            <div class="list-item">
                                                                <div class="text">
                                                                    <span class="black-text">City</span> :
                                                                    <span class="blue">
                                                                    	{{ isset($data[0]['personal_detail']['city_id']) ? $data[0]['personal_detail']['pincode']['pincodes_cities'][0]['city']['name'] : (isset($data[0]['personal_detail']['city_text']) ? $data[0]['personal_detail']['city_text'] : '')}}
                                                                    </span>
                                                                </div>
                                                            </div>
                                                            <div class="list-item">
                                                                <div class="text">
                                                                    <span class="black-text">Pincode</span> :
                                                                    <span class="blue">
                                                                    	{{ isset($data[0]['personal_detail']['pincode_text']) ? $data[0]['personal_detail']['pincode_text'] : ''}}
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div role="tabpanel" class="tab-pane" id="pd">
                                            <div class="content">
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <div class="list">
                                                            <div class="list-item">
                                                                <div class="text">
                                                                    <span class="black-text">Current Rank</span> :
                                                                    <span class="blue">
                                                                    	@foreach(\CommonHelper::new_rank() as $index => $category)
																			@foreach($category as $r_index => $rank)
																				{{ !empty($data[0]['professional_detail']['current_rank']) ? $data[0]['professional_detail']['current_rank'] == $r_index ? $rank : '' : ''}}
																			@endforeach
																		@endforeach
                                                                    </span>
                                                                </div>
                                                            </div>
                                                            <div class="list-item">
                                                                <div class="text">
                                                                    <span class="black-text">Applied Rank</span> :
                                                                    <span class="blue">
                                                                    	@foreach(\CommonHelper::new_rank() as $index => $category)
																			@foreach($category as $r_index => $rank)
																				{{ !empty($data[0]['professional_detail']['applied_rank']) ? $data[0]['professional_detail']['applied_rank'] == $r_index ? $rank : '' : ''}}
																			@endforeach
																		@endforeach
                                                                    </span>
                                                                </div>
                                                            </div>
                                                            <div class="list-item">
                                                                <div class="text">
                                                                    <span class="black-text">Current Rank Experience</span> :
                                                                    <span class="blue">
                                                                    	{{ isset($data[0]['professional_detail']['years']) ? $data[0]['professional_detail']['years'] : '0'}} Years {{ isset($data[0]['professional_detail']['months']) && ($data[0]['professional_detail']['months'] != '') ? $data[0]['professional_detail']['months'] : '0' }} Months
                                                                    </span>
                                                                </div>
                                                            </div>
                                                            <div class="list-item">
                                                                <div class="text">
                                                                    <span class="black-text ">Date of Availability</span> :
                                                                    <span class="blue">
                                                                    	{{ isset($data[0]['professional_detail']['availability']) ? date('d-m-Y',strtotime($data[0]['professional_detail']['availability'])) : '-'}}
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header" style="padding:0;">
                                <ul class="nav nav-tabs course_list" role="tablist">
                                    <li role="presentation" class="course_list_item active">
                                        <a href="#gi" aria-controls="gi" role="tab" data-toggle="tab">General Information</a>
                                    </li>
									<li role="presentation" class="course_list_item">
                                        <a href="#cd" aria-controls="cd" role="tab" data-toggle="tab">Course Details</a>
                                    </li>
									<li role="presentation" class="course_list_item">
                                        <a href="#ssd" aria-controls="ssd" role="tab" data-toggle="tab">Sea Service Details</a>
                                    </li>
                                    <li role="presentation" class="course_list_item">
                                        <a href="#user_document" aria-controls="udoc" role="tab" data-toggle="tab">User Documents</a>
                                    </li>
                                    <li role="presentation" class="course_list_item">
                                        <a href="#notifi" aria-controls="notifi" role="tab" data-toggle="tab">Notifications</a>
                                    </li>
                                    <!-- <li role="presentation" class="course_list_item">
                                        <a href="#td" aria-controls="td" role="tab" data-toggle="tab">Tracking Details</a>
                                    </li> -->
                                </ul>
                            </div>
                            <div class="card-body z-depth-1">
                               <div class="tab-content">
                                   <div role="tabpanel" class="tab-pane active" id="gi">
                                       <div class="content">
                                       		<div class="row">
                                               <div class="col-lg-12">
		                                           <div class="content-title" style="border-bottom:1px solid #a9b6cb;">
		                                                <div class="d-flex block-small">
		                                                    <div class="d-flex--one">
		                                                        <span class="dark-blue title">GENERAL DETAILS</span>
		                                                    </div>
		                                                    @if($user != 'another_user')
			                                                    <div class="d-flex--two">
			                                                        <button href="{{route('site.seafarer.edit.profile')}}" class="btn cs-primary-btn edit_link" data-form='documents'><i class="fa fa-pencil-square-o"> Edit</i></button>
			                                                    </div>
															@endif
		                                                </div>
		                                            </div>
		                                        </div>
											</div>
											<div class="row">
                                               <div class="col-lg-12">
		                                           <div class="content-title">
		                                                <div class="d-flex block-small">
		                                                    <div class="d-flex--one">
		                                                        <span class="dark-blue sub-title">PASSPORT DETAILS</span>
		                                                    </div>
		                                                </div>
		                                            </div>
		                                        </div>
		                                    </div>
		                                    <div class="row">
                                                <div class="col-sm-3">
                                                    <div class="list">
                                                        <div class="list-item">
                                                            <div class="text"><span class="black">Passport Country</span> :
                                                            	<span class="blue">
                                                            		@if(isset($data[0]['passport_detail']['pass_country']) && !empty($data[0]['passport_detail']['pass_country']))
																		@foreach( \CommonHelper::countries() as $c_index => $country)
																			{{ isset($data[0]['passport_detail']['pass_country']) ? $data[0]['passport_detail']['pass_country'] == $c_index ? $country : '' : ''}}
																		@endforeach
																	@else
																		-
																	@endif
                                                            	</span>
                                                            </div>
                                                        </div>
                                                        <div class="list-item">
                                                            <div class="text"><span class="black">US Visa</span> :
                                                            	<span class="blue">
                                                            		{{ isset($data[0]['passport_detail']['us_visa']) ? $data[0]['passport_detail']['us_visa'] == 1 ? 'Yes' : 'No' : 'No'}}
                                                            	</span>
                                                            </div>
                                                        </div>
                                                        <div class="list-item">
                                                            <div class="text"><span class="black">Yellow Fever Vaccination</span> :
                                                            	<span class="blue">
                                                            		{{ isset($data[0]['wkfr_detail']['yellow_fever']) ? $data[0]['wkfr_detail']['yellow_fever'] == '1'? 'Yes' : 'No' : 'NO' }}
                                                            	</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="list">
                                                        <div class="list-item">
                                                            <div class="text"><span class="black">Passport Number</span> :
                                                            	<span class="blue">
                                                            		{{ isset($data[0]['passport_detail']['pass_number']) ? $data[0]['passport_detail']['pass_number'] : '-' }}
                                                            	</span>
                                                            </div>
                                                        </div>
                                                        <div class="list-item">
                                                            <div class="text"><span class="black">Expiry Date</span> :
                                                            	<span class="blue">
                                                            		{{ isset($data[0]['passport_detail']['us_visa_expiry_date']) ? date('d-m-Y',strtotime($data[0]['passport_detail']['us_visa_expiry_date'])) : '-'}}
                                                            	</span>
                                                            </div>
                                                        </div>
                                                        <div class="list-item">
                                                            <div class="text"><span class="black">Yellow Fever Issue Date</span> :
                                                            	<span class="blue">
                                                            		{{ isset($data[0]['wkfr_detail']['yf_issue_date']) ? date('d-m-Y',strtotime($data[0]['wkfr_detail']['yf_issue_date'])) : '-'}}
                                                            	</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="list">
                                                        <div class="list-item">
                                                            <div class="text"><span class="black">Place Of Issue</span> :
                                                            	<span class="blue">
                                                            		{{ isset($data[0]['passport_detail']['place_of_issue']) ? $data[0]['passport_detail']['place_of_issue'] : '-' }}
                                                            	</span>
                                                            </div>
                                                        </div>
                                                        <div class="list-item">
                                                            <div class="text"><span class="black">Indian PCC</span> :
                                                            	<span class="blue">
                                                            		{{ isset($data[0]['passport_detail']['indian_pcc']) ? $data[0]['passport_detail']['indian_pcc'] == '1'? 'Yes' : 'No' : 'No'}}
                                                            	</span>
                                                            </div>
                                                        </div>
                                                        @if($data[0]['passport_detail']['fromo'] == 1 || ($data[0]['passport_detail']['fromo'] == 0))
                                                        <div class="list-item">
                                                            <div class="text"><span class="black">Experience Of Framo</span> :
                                                            	<span class="blue">
                                                            		{{ isset($data[0]['passport_detail']['fromo']) ? $data[0]['passport_detail']['fromo'] == '1'? 'Yes' : 'No' : 'NO' }}
                                                            	</span>
                                                            </div>
                                                        </div>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="list">
                                                        <div class="list-item">
                                                            <div class="text"><span class="black">Expiry Date</span> :
                                                            	<span class="blue">
                                                            		{{ isset($data[0]['passport_detail']['pass_expiry_date'])? date('d-m-Y',strtotime($data[0]['passport_detail']['pass_expiry_date'])) : '-' }}
                                                            	</span>
                                                            </div>
                                                        </div>
                                                        <div class="list-item">
                                                            <div class="text"><span class="black">INDOS Number</span> :
                                                            	<span class="blue">
                                                            		{{ !empty($data[0]['wkfr_detail']['indos_number']) ? $data[0]['wkfr_detail']['indos_number'] : '-'}}
                                                            	</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- SEAMAN BOOK DETAILS -->
                                            <div class="row">
                                               <div class="col-lg-12">
		                                           <div class="content-title" style="border-top:1px solid #a9b6cb;">
		                                                <div class="d-flex block-small">
		                                                    <div class="d-flex--one">
		                                                        <span class="dark-blue sub-title">SEAMAN BOOK DETAILS</span>
		                                                    </div>
		                                                </div>
		                                            </div>
		                                        </div>
		                                    </div>
		                                    <div class="row">
		                                    	@foreach($data[0]['seaman_book_detail'] as $index => $cdc_data)
		                                    	<div class="col-sm-2">
                                                    <div class="list">
                                                        <div class="list-item">
                                                            <div class="text"><span class="black">CDC</span> :
                                                            	<span class="blue">
                                                            		@foreach( \CommonHelper::countries() as $c_index => $country)
																		{{ isset($cdc_data['cdc']) ? $cdc_data['cdc'] == $c_index ? $country : '' : ''}}
																	@endforeach
                                                            	</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-2">
                                                    <div class="list">
                                                        <div class="list-item">
                                                            <div class="text"><span class="black">Number</span> :
                                                            	<span class="blue">
                                                            		{{isset($cdc_data['cdc_number']) ? $cdc_data['cdc_number'] : ''}}
                                                            	</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-2">
                                                    <div class="list">
                                                        <div class="list-item">
                                                            <div class="text"><span class="black">Issue Date</span> :
                                                            	<span class="blue">
                                                            		{{isset($cdc_data['cdc_issue_date']) ? date('d-m-Y',strtotime($cdc_data['cdc_issue_date'])) : ''}}
                                                            	</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="list">
                                                        <div class="list-item">
                                                            <div class="text"><span class="black">Verification Date</span> :
                                                            	<span class="blue">
                                                            		{{ !is_null($cdc_data['cdc_verification_date']) ? date('d-m-Y',strtotime($cdc_data['cdc_verification_date'])) : '-' }}
                                                            	</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="list">
                                                        <div class="list-item">
                                                            <div class="text"><span class="black">Expiry Date</span> :
                                                            	<span class="blue">
                                                            		{{isset($cdc_data['cdc_expiry_date']) ? date('d-m-Y',strtotime($cdc_data['cdc_expiry_date'])) : ''}}
																	@if($cdc_data['status'] == 1)
																		<i class="fa fa-check-circle green f-15 m-l-15" aria-hidden="true" title="Verified"></i>
																	@endif
                                                            	</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
												@endforeach
		                                    </div>

		                                    <!-- CERTIFICATE OF COMPENTENCY -->
		                                    @if(isset($required_fields))
												@if(isset($data[0]['coc_detail']) AND !empty($data[0]['coc_detail']) && (in_array('COC',$required_fields) OR in_array('COC-Optional',$required_fields)))
				                                    <div class="row">
		                                               <div class="col-lg-12">
				                                           <div class="content-title" style="border-top:1px solid #a9b6cb;">
				                                                <div class="d-flex block-small">
				                                                    <div class="d-flex--one">
				                                                        <span class="dark-blue sub-title">CERTIFICATE OF COMPENTENCY</span>
				                                                    </div>
				                                                </div>
				                                            </div>
				                                        </div>
				                                    </div>
				                                    <div class="row">
				                                    	@foreach($data[0]['coc_detail'] as $index => $coc_data) <div class="col-sm-2">
		                                                    <div class="list">
		                                                        <div class="list-item">
		                                                            <div class="text"><span class="black">COC</span> :
		                                                            	<span class="blue">
		                                                            		@if(isset($coc_data['coc']) && !empty($coc_data['coc']))
																				@foreach( \CommonHelper::countries() as $c_index => $country)
																					{{ isset($coc_data['coc']) ? $coc_data['coc'] == $c_index ? $country : '' : ''}}
																				@endforeach
																			@else
																				-
																			@endif
		                                                            	</span>
		                                                            </div>
		                                                        </div>
		                                                    </div>
		                                                </div>
		                                                <div class="col-sm-2">
		                                                    <div class="list">
		                                                        <div class="list-item">
		                                                            <div class="text"><span class="black">Number</span> :
		                                                            	<span class="blue">
		                                                            		{{ !empty($coc_data['coc_number']) ? $coc_data['coc_number'] : '-'}}
		                                                            	</span>
		                                                            </div>
		                                                        </div>
		                                                    </div>
		                                                </div>
		                                                <div class="col-sm-2">
		                                                    <div class="list">
		                                                        <div class="list-item">
		                                                            <div class="text"><span class="black">Grade</span> :
		                                                            	<span class="blue">
		                                                            		{{ !empty($coc_data['coc_grade']) ? $coc_data['coc_grade'] : '-' }}
		                                                            	</span>
		                                                            </div>
		                                                        </div>
		                                                    </div>
		                                                </div>
		                                                <div class="col-sm-3">
		                                                    <div class="list">
		                                                        <div class="list-item">
		                                                            <div class="text"><span class="black">Verification Date</span> :
		                                                            	<span class="blue">
		                                                            		{{ !is_null($coc_data['coc_verification_date']) ? date('d-m-Y',strtotime($coc_data['coc_verification_date'])) : '-' }}
		                                                            	</span>
		                                                            </div>
		                                                        </div>
		                                                    </div>
		                                                </div>
		                                                <div class="col-sm-3">
		                                                    <div class="list">
		                                                        <div class="list-item">
		                                                            <div class="text"><span class="black">Expiry Date</span> :
		                                                            	<span class="blue">
		                                                            		{{ !is_null($coc_data['coc_expiry_date']) ? date('d-m-Y',strtotime($coc_data['coc_expiry_date'])) : '-' }}

																			@if(isset($coc_data['status']) && $coc_data['status'] == 1)
																				<i class="fa fa-check-circle green f-15 m-l-15" aria-hidden="true" title="Verified"></i>
																			@endif
		                                                            	</span>
		                                                            </div>
		                                                        </div>
		                                                    </div>
		                                                </div>
														@endforeach
				                                    </div>
				                                @endif
				                            @endif

				                            <!-- CERTIFICATE OF ENDORSEMENT -->
		                                    @if(isset($required_fields))
												@if(isset($data[0]['coe_detail']) AND !empty($data[0]['coe_detail']) && (in_array('COE',$required_fields) OR in_array('COE-Optional',$required_fields)) && (!empty($data[0]['coe_detail'][0]['coe']) || !empty($data[0]['coe_detail'][0]['coe_number']) || !empty($data[0]['coe_detail'][0]['coe_grade']) || !empty($data[0]['coe_detail'][0]['coe_expiry_date']) || !empty($data[0]['coe_detail'][0]['coe_verification_date'])))
				                                    <div class="row">
		                                               <div class="col-lg-12">
				                                           <div class="content-title" style="border-top:1px solid #a9b6cb;">
				                                                <div class="d-flex block-small">
				                                                    <div class="d-flex--one">
				                                                        <span class="dark-blue sub-title">CERTIFICATE OF ENDORSEMENT</span>
				                                                    </div>
				                                                </div>
				                                            </div>
				                                        </div>
				                                    </div>
				                                    <div class="row">
				                                    	@foreach($data[0]['coe_detail'] as $index => $coe_data)
				                                    	<div class="col-sm-3">
		                                                    <div class="list">
		                                                        <div class="list-item">
		                                                            <div class="text"><span class="black">COE</span> :
		                                                            	<span class="blue">
		                                                            		@if(isset($coe_data['coe']) && !empty($coe_data['coe']))
																				@foreach( \CommonHelper::countries() as $c_index => $country)
																					{{ isset($coe_data['coe']) ? $coe_data['coe'] == $c_index ? $country : '' : ''}}
																				@endforeach
																			@else
																				-
																			@endif
		                                                            	</span>
		                                                            </div>
		                                                        </div>
		                                                    </div>
		                                                </div>
		                                                <div class="col-sm-3">
		                                                    <div class="list">
		                                                        <div class="list-item">
		                                                            <div class="text"><span class="black">Number</span> :
		                                                            	<span class="blue">
		                                                            		{{ !empty($coe_data['coe_number']) ? $coe_data['coe_number'] : '-'}}
		                                                            	</span>
		                                                            </div>
		                                                        </div>
		                                                    </div>
		                                                </div>
		                                                <div class="col-sm-3">
		                                                    <div class="list">
		                                                        <div class="list-item">
		                                                            <div class="text"><span class="black">Grade</span> :
		                                                            	<span class="blue">
		                                                            		{{ !empty($coe_data['coe_grade']) ? $coe_data['coe_grade'] : '-' }}
		                                                            	</span>
		                                                            </div>
		                                                        </div>
		                                                    </div>
		                                                </div>
		                                                <div class="col-sm-3">
		                                                    <div class="list">
		                                                        <div class="list-item">
		                                                            <div class="text"><span class="black">Expiry Date</span> :
		                                                            	<span class="blue">
		                                                            		{{ !empty($coe_data['coe_expiry_date']) ? date('d-m-Y',strtotime($coe_data['coe_expiry_date'])) : '-' }}
		                                                            	</span>
		                                                            </div>
		                                                        </div>
		                                                    </div>
		                                                </div>
														@endforeach
				                                    </div>
				                                @endif
				                            @endif

				                            <!-- GMDSS DETAILS -->
		                                    @if(isset($required_fields) && !empty($required_fields))
												@if(isset($data[0]['gmdss_detail']) AND !empty($data[0]['gmdss_detail']) && (in_array('GMDSS',$required_fields) OR in_array('GMDSS-Optional',$required_fields)) && ((!empty($data[0]['gmdss_detail']['gmdss'])) || (!empty($data[0]['gmdss_detail']['gmdss_number'])) || (!empty($data[0]['gmdss_detail']['gmdss_expiry_date']))))
				                                    <div class="row">
		                                               <div class="col-lg-12">
				                                           <div class="content-title" style="border-top:1px solid #a9b6cb;">
				                                                <div class="d-flex block-small">
				                                                    <div class="d-flex--one">
				                                                        <span class="dark-blue sub-title">GMDSS DETAILS</span>
				                                                    </div>
				                                                </div>
				                                            </div>
				                                        </div>
				                                    </div>
				                                    <div class="row">
				                                    	<div class="col-sm-2">
		                                                    <div class="list">
		                                                        <div class="list-item">
		                                                            <div class="text"><span class="black">GMDSS</span> :
		                                                            	<span class="blue">
		                                                            		@foreach( \CommonHelper::countries() as $c_index => $country)
																				{{ isset($data[0]['gmdss_detail']['gmdss']) ? $data[0]['gmdss_detail']['gmdss'] == $c_index ? $country : '' : ''}}
																			@endforeach
		                                                            	</span>
		                                                            </div>
		                                                        </div>
		                                                    </div>
		                                                </div>
		                                                <div class="col-sm-2">
		                                                    <div class="list">
		                                                        <div class="list-item">
		                                                            <div class="text"><span class="black">Number</span> :
		                                                            	<span class="blue">
		                                                            		{{ isset($data[0]['gmdss_detail']['gmdss_number']) ? $data[0]['gmdss_detail']['gmdss_number'] : '-'}}
		                                                            	</span>
		                                                            </div>
		                                                        </div>
		                                                    </div>
		                                                </div>
		                                                <div class="col-sm-2">
		                                                    <div class="list">
		                                                        <div class="list-item">
		                                                            <div class="text"><span class="black">Expiry Date</span> :
		                                                            	<span class="blue">
		                                                            		{{ isset($data[0]['gmdss_detail']['gmdss_expiry_date']) ? date('d-m-Y',strtotime($data[0]['gmdss_detail']['gmdss_expiry_date'])) : '-' }}
		                                                            	</span>
		                                                            </div>
		                                                        </div>
		                                                    </div>
		                                                </div>
		                                                <div class="col-sm-3 gmdss_endorsement {{isset($data[0]['gmdss_detail']['gmdss']) && $data[0]['gmdss_detail']['gmdss'] == $india_value ? '' : 'hide'}}">
		                                                    <div class="list">
		                                                        <div class="list-item">
		                                                            <div class="text"><span class="black">Endorsement Number</span> :
		                                                            	<span class="blue">
		                                                            		{{ isset($data[0]['gmdss_detail']['gmdss_endorsement_number']) && !empty($data[0]['gmdss_detail']['gmdss_endorsement_number']) ? $data[0]['gmdss_detail']['gmdss_endorsement_number'] : '-'}}
		                                                            	</span>
		                                                            </div>
		                                                        </div>
		                                                    </div>
		                                                </div>
		                                                <div class="col-sm-3 gmdss_valid_till {{isset($data[0]['gmdss_detail']['gmdss']) && $data[0]['gmdss_detail']['gmdss'] == $india_value ? '' : 'hide'}}">
		                                                    <div class="list">
		                                                        <div class="list-item">
		                                                            <div class="text"><span class="black">Valid Till</span> :
		                                                            	<span class="blue">
		                                                            		{{ isset($data[0]['gmdss_detail']['gmdss_endorsement_expiry_date']) && !empty($data[0]['gmdss_detail']['gmdss_endorsement_expiry_date']) ? date('d-m-Y',strtotime($data[0]['gmdss_detail']['gmdss_endorsement_expiry_date'])) : '-'}}
		                                                            	</span>
		                                                            </div>
		                                                        </div>
		                                                    </div>
		                                                </div>
				                                    </div>
				                                @endif
				                            @endif
                                       </div>
                                   </div>
                                   <div role="tabpanel" class="tab-pane" id="ssd">
                                		<div class="content">
                                           <div class="row">
                                               <div class="col-lg-12">
                                                    <div class="content-title" style="border-bottom:1px solid #a9b6cb;">
                                                        <div class="d-flex block-small">
                                                            <div class="d-flex--one">
                                                                <span class="dark-blue title">SEA SERVICE DETAILS</span>
                                                            </div>
                                                            @if($user != 'another_user')
			                                                    <div class="d-flex--two">
			                                                        <button href="{{route('site.seafarer.edit.profile')}}" class="btn cs-primary-btn edit_link" data-form='sea_service'><i class="fa fa-pencil-square-o"> Edit</i></button>
			                                                    </div>
															@endif
                                                        </div>
                                                    </div>
                                               </div>
                                           </div>
                                       		@if(isset($data[0]['sea_service_detail']) AND !empty($data[0]['sea_service_detail']))
	                                       		@foreach($data[0]['sea_service_detail'] as $index => $services)
	                               					<div class="row" style="margin-top:15px;">
		                                                <div class="col-sm-4">
		                                                    <div class="list">
																<div class="list-item">
		                                                            <div class="text"><span class="black">Rank</span> :
		                                                            	<span class="blue">
		                                                            		@foreach(\CommonHelper::new_rank() as $index => $category)
																				@foreach($category as $r_index => $rank)
																					{{ isset($services['rank_id']) ? $services['rank_id'] == $r_index ? $rank : '' : ''}}
																				@endforeach
																			@endforeach
		                                                            	</span>
		                                                            </div>
		                                                        </div>
		                                                        <div class="list-item">
		                                                            <div class="text"><span class="black">Shipping Company</span> :
		                                                            	<span class="blue">
		                                                            		{{ isset($services['company_name']) ? $services['company_name'] : '-'}}
		                                                            	</span>
		                                                            </div>
		                                                        </div>
																<div class="list-item">
		                                                            <div class="text"><span class="black">GRT</span> :
		                                                            	<span class="blue">
		                                                            		{{ !empty($services['grt']) ? $services['grt'] : '-'}}
		                                                            	</span>
		                                                            </div>
		                                                        </div>
																<div class="list-item">
		                                                            <div class="text"><span class="black">Sign On Date</span> :
		                                                            	<span class="blue">
		                                                            		{{ isset($services['from']) ? date('d-m-Y',strtotime($services['from'])) : ''}}
		                                                            	</span>
		                                                            </div>
		                                                        </div>
		                                                    </div>
		                                                </div>
		                                                <div class="col-sm-4">
		                                                    <div class="list">
		                                                        <div class="list-item">
		                                                            <div class="text"><span class="black">Name Of Ship</span> :
		                                                            	<span class="blue">
		                                                            		{{ isset($services['ship_name']) ? $services['ship_name'] : '-'}}
		                                                            	</span>
		                                                            </div>
		                                                        </div>
																<div class="list-item">
		                                                            <div class="text"><span class="black">BHP</span> :
		                                                            	<span class="blue">
		                                                            		{{ !empty($services['bhp']) ? $services['bhp'] : '-'}}
		                                                            	</span>
		                                                            </div>
		                                                        </div>
																<div class="list-item">
		                                                            <div class="text"><span class="black">Sign Off Date</span> :
		                                                            	<span class="blue">
		                                                            		{{ isset($services['to']) ? date('d-m-Y',strtotime($services['to'])) : ''}}
		                                                            	</span>
		                                                            </div>
		                                                        </div>
		                                                    </div>
		                                                </div>
		                                                <div class="col-sm-4">
		                                                    <div class="list">
		                                                        <div class="list-item">
		                                                            <div class="text"><span class="black">Ship Type</span> :
		                                                            	<span class="blue">
		                                                            		@foreach( \CommonHelper::ship_type() as $c_index => $type)
																				{{ isset($services['ship_type']) ? $services['ship_type'] == $c_index ? $type : '' : ''}}
																			@endforeach
		                                                            	</span>
		                                                            </div>
		                                                        </div>
																<div class="list-item">
		                                                            <div class="text"><span class="black">Engine Type</span> :
		                                                            	<span class="blue">
		                                                            		@if(isset($services['engine_type']) && !empty($services['engine_type']))
																				@if(isset($services['engine_type']) && $services['engine_type'] == 'other')
																					{{ isset($services['other_engine_type']) ? $services['other_engine_type'] : '-'}}
																				@else
																					@foreach( \CommonHelper::engine_type() as $c_index => $type)
																						{{ isset($services['engine_type']) ? $services['engine_type'] == $c_index ? $type  : '' : ''}}
																					@endforeach
																				@endif
																			@else
																				-
																			@endif
		                                                            	</span>
		                                                            </div>
		                                                        </div>
																<div class="list-item">
		                                                            <div class="text"><span class="black">Manning By</span> :
		                                                            	<span class="blue">
		                                                            		@if(isset($services['manning_by']) && !empty($services['manning_by']))
																				{{$services['manning_by']}}
																			@else
																				-
																			@endif
		                                                            	</span>
		                                                            </div>
		                                                        </div>
		                                                    </div>
		                                                </div>
		                                            </div>
		                                            <div class="row">
		                                            	<hr>
		                                            </div>
	                                            @endforeach
	                                        @else
	                                        	<div class="row" style="margin-top:15px;">
	                                        		<div class="col-sm-12 ">
	                                                    <div class="list">
	                                                        <div class="list-item">
	                                                            <div class="text ">
	                                                            	No Data Found
	                                                            </div>
	                                                        </div>
	                                                    </div>
	                                                </div>
	                                        	</div>
                                            @endif
                                        </div>
                                   </div>
                                   <div role="tabpanel" class="tab-pane" id="cd">
                                   		<div class="content">
                                           <div class="row">
                                               <div class="col-lg-12">
                                                    <div class="content-title" style="border-bottom:1px solid #a9b6cb;">
                                                        <div class="d-flex block-small">
                                                            <div class="d-flex--one">
                                                                <span class="dark-blue title">COURSE DETAILS</span>
                                                            </div>
                                                            @if($user != 'another_user')
			                                                    <div class="d-flex--two">
			                                                        <button href="{{route('site.seafarer.edit.profile')}}" class="btn cs-primary-btn edit_link" data-form='courses'><i class="fa fa-pencil-square-o"> Edit</i></button>
			                                                    </div>
															@endif
                                                        </div>
                                                    </div>
                                               </div>
                                           </div>
                                           	@if(isset($data[0]['course_detail']) AND !empty($data[0]['course_detail']))
                                       			<?php $normal_course_count = 1; ?>
												@foreach($data[0]['course_detail'] as $index => $courses)
	                               					<div class="row" style="margin-top:15px;">
														<div class="col-sm-12">
		                                       				<div style="font-weight:bold;">
															   	@foreach( \CommonHelper::courses() as $c_index => $course)
																	{{ isset($courses['course_id']) ? $courses['course_id'] == $c_index ? $course : '' : ''}}
																@endforeach
															</div>
		                                       			</div>
		                                                <div class="col-sm-4">
		                                                    <div class="list">
																<div class="list-item">
		                                                            <div class="text"><span class="black">Certificate Number</span> :
		                                                            	<span class="blue">
		                                                            		{{ isset($courses['certification_number']) && !empty($courses['certification_number'])? $courses['certification_number'] : '-'}}
		                                                            	</span>
		                                                            </div>
		                                                        </div>
																<div class="list-item">
		                                                            <div class="text"><span class="black">Issued By</span> :
		                                                            	<span class="blue">
		                                                            		{{ isset($courses['issue_by']) && !empty($courses['issue_by']) ? $courses['issue_by'] : '-'}}
		                                                            	</span>
		                                                            </div>
		                                                        </div>
		                                                    </div>
		                                                </div>
		                                                <div class="col-sm-4">
		                                                    <div class="list">
																<div class="list-item">
		                                                            <div class="text"><span class="black">Date Of Issue</span> :
		                                                            	<span class="blue">
		                                                            		{{ isset($courses['issue_date']) && !empty($courses['issue_date']) ? date('d-m-Y',strtotime($courses['issue_date'])) : '-'}}
		                                                            	</span>
		                                                            </div>
		                                                        </div>
		                                                        <div class="list-item">
		                                                            <div class="text"><span class="black">Date Of Expiry</span> :
		                                                            	<span class="blue">
		                                                            		{{ isset($courses['expiry_date']) && !empty(isset($courses['expiry_date'])) ? date('d-m-Y',strtotime($courses['expiry_date'])) : '-'}}
		                                                            	</span>
		                                                            </div>
		                                                        </div>
		                                                    </div>
		                                                </div>
		                                                <div class="col-sm-4">
		                                                    <div class="list">

		                                                    </div>
		                                                </div>
		                                            </div>
		                                            <div class="row">
		                                            	<hr>
		                                            </div>
		                                            <?php $normal_course_count++; ?>
	                                            @endforeach
	                                        @else
	                                        	<div class="row" style="margin-top:15px;">
	                                        		<div class="col-sm-12 ">
	                                                    <div class="list">
	                                                        <div class="list-item">
	                                                            <div class="text ">
	                                                            	No Data Found
	                                                            </div>
	                                                        </div>
	                                                    </div>
	                                                </div>
	                                        	</div>
                                            @endif

                                            <div class="row">
                                               <div class="col-lg-12">
                                                    <div class="content-title" style="border-bottom:1px solid #a9b6cb;">
                                                        <div class="d-flex block-small">
                                                            <div class="d-flex--one">
                                                                <span class="dark-blue title">ADD-ON COURSE DETAILS</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                               </div>
                                           </div>
                                           	@if(isset($data[0]['value_added_course_detail']) AND !empty($data[0]['value_added_course_detail']))
												@foreach($data[0]['value_added_course_detail'] as $index => $courses)
	                               					<div class="row" style="margin-top:15px;">
		                                       			<div class="col-sm-12">
		                                       				<div style="font-weight:bold;">CERTIFICATE {{$index+1}}</div>
		                                       			</div>
		                                                <div class="col-sm-4">
		                                                    <div class="list">
		                                                        <div class="list-item">
		                                                            <div class="text"><span class="black">Certificate Name</span> :
		                                                            	<span class="blue">
		                                                            		@foreach( \CommonHelper::value_added_courses() as $c_index => $course)
																				{{ isset($courses['course_id']) ? $courses['course_id'] == $c_index ? $course : '' : ''}}
																			@endforeach
		                                                            	</span>
		                                                            </div>
		                                                        </div>
		                                                        <div class="list-item">
		                                                            <div class="text"><span class="black">Date Of Expiry</span> :
		                                                            	<span class="blue">
		                                                            		{{ isset($courses['expiry_date']) && !empty(isset($courses['expiry_date'])) ? date('d-m-Y',strtotime($courses['expiry_date'])) : '-'}}
		                                                            	</span>
		                                                            </div>
		                                                        </div>
		                                                    </div>
		                                                </div>
		                                                <div class="col-sm-4">
		                                                    <div class="list">
		                                                        <div class="list-item">
		                                                            <div class="text"><span class="black">Certificate Number</span> :
		                                                            	<span class="blue">
		                                                            		{{ isset($courses['certification_number']) && !empty($courses['certification_number'])? $courses['certification_number'] : '-'}}
		                                                            	</span>
		                                                            </div>
		                                                        </div>
		                                                        <div class="list-item">
		                                                            <div class="text"><span class="black">Issued By</span> :
		                                                            	<span class="blue">
		                                                            		{{ isset($courses['issue_by']) && !empty($courses['issue_by']) ? $courses['issue_by'] : '-'}}
		                                                            	</span>
		                                                            </div>
		                                                        </div>
		                                                    </div>
		                                                </div>
		                                                <div class="col-sm-4">
		                                                    <div class="list">
		                                                        <div class="list-item">
		                                                            <div class="text"><span class="black">Date Of Issue</span> :
		                                                            	<span class="blue">
		                                                            		{{ isset($courses['issue_date']) && !empty($courses['issue_date']) ? date('d-m-Y',strtotime($courses['issue_date'])) : '-'}}
		                                                            	</span>
		                                                            </div>
		                                                        </div>
		                                                    </div>
		                                                </div>
		                                            </div>
		                                            <div class="row">
		                                            	<hr>
		                                            </div>
	                                            @endforeach
	                                        @else
	                                        	<div class="row" style="margin-top:15px;">
	                                        		<div class="col-sm-12 ">
	                                                    <div class="list">
	                                                        <div class="list-item">
	                                                            <div class="text ">
	                                                            	No Data Found
	                                                            </div>
	                                                        </div>
	                                                    </div>
	                                                </div>
	                                        	</div>
                                            @endif

                                            @if(isset($data[0]['professional_detail']['other_exp']) && !empty($data[0]['professional_detail']['other_exp']))
                                            	<div class="row">
	                                               <div class="col-lg-12">
	                                                    <div class="content-title" style="border-bottom:1px solid #a9b6cb;">
	                                                        <div class="d-flex block-small">
	                                                            <div class="d-flex--one">
	                                                                <span class="dark-blue title">OTHER EXPERIENCE</span>
	                                                            </div>
	                                                        </div>
	                                                    </div>
	                                               </div>
	                                                <div class="col-sm-12">
	                                                    <div class="list">
	                                                        <div class="list-item">
	                                                            <div class="text">
	                                                            	{{$data[0]['professional_detail']['other_exp']}}
	                                                            </div>
	                                                        </div>
	                                                    </div>
	                                                </div>
	                                           </div>
											@endif
                                        </div>
                                   </div>
                                   <div role="tabpanel" class="tab-pane" id="user_document">


										<?php
											$download_all_path = route('site.user.get.documents.path',['user_id' => $data[0]['id']]);
											$current_route = Route::currentRouteName();
										?>

										@if(isset($registered_as) && ($registered_as != 'advertiser'))
											<div class="tab-content">
												<div id="upload_documents" class="tab-pane fade in active">
													@if($current_route == 'user.view.profile')
														<input type="hidden" class="profile_status" value="view profile">

													@endif

													@if($current_route == 'user.profile')
														<input type="hidden" class="profile_status" value="own profile">
														<div class="documents_section">

														</div>
													@endif
												</div>

											</div>
										@endif
                                   </div>
                                   <div role="tabpanel" class="tab-pane" id="notifi">
                                       <div class="row" style="margin-top:15px;">
                                            <div class="col-sm-12 ">
                                                <div class="list">
                                                    <div class="list-item">
                                                        <div class="text ">
                                                            No Notifications Found
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                   </div>
                                   <div role="tabpanel" class="tab-pane" id="td">
                                       <div class="content">
                                           <div class="row">
                                               <div class="col-lg-12">
                                                    <div class="content-title" style="border-bottom:1px solid #a9b6cb;">
                                                        <div class="d-flex block-small">
                                                            <div class="d-flex--one">
                                                                <span class="dark-blue title">Tracking Details</span>
                                                            </div>
                                                            <div class="d-flex--two">
                                                                <a href="" class="btn cs-primary-btn">Edit</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                               </div>
                                           </div>
                                           <div class="row" style="margin-top:15px;">
                                                <div class="col-sm-6">
                                                    <div style="font-weight:bold;">Course Details</div>
                                                    <div class="list">
                                                        <div class="list-item">
                                                            <div class="text"><span class="black">Institute Name</span> :<span class="blue"></span> </div>
                                                        </div>
                                                        <div class="list-item">
                                                            <div class="text"><span class="black">Course Type</span> :<span class="blue"></span> </div>
                                                        </div>
                                                        <div class="list-item">
                                                            <div class="text"><span class="black">Course Name</span> :<span class="blue"></span> </div>
                                                        </div>
                                                        <div class="list-item">
                                                            <div class="text"><span class="black">Start Date</span> :<span class="blue"></span> </div>
                                                        </div>
                                                        <div class="list-item">
                                                            <div class="text"><span class="black">Booking Date</span> :<span class="blue"></span> </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div style="font-weight:bold;">Payment Details</div>
                                                    <div class="list">
                                                        <div class="list-item">
                                                            <div class="text"><span class="black">Booking Amount</span> :<span class="blue"></span> </div>
                                                        </div>
                                                        <div class="list-item">
                                                            <div class="text"><span class="black">Tax</span> :<span class="blue"></span> </div>
                                                        </div>
                                                        <div class="list-item">
                                                            <div class="text"><span class="black">Total Amount</span> :<span class="blue"></span> </div>
                                                        </div>
                                                        <div class="list-item">
                                                            <div class="text"><span class="black"></span> <span class="blue"></span> </div>
                                                        </div>
                                                        <div class="list-item">
                                                            <div class="text"><span class="black">Order Status</span> :<span class="green-label">Payment Success</span>
                                                        </div>
                                                    </div>
                                                </div>
                                           </div>
                                           <!-- <div class="row">
                                               <div class="col-lg-12">
                                                   <ul>
                                                       <li>Blocking</li>
                                                       <li>Pending</li>
                                                       <li>Approved</li>
                                                       <li>
                                                           Payment Succesful
                                                       </li>
                                                       <li>Seat Confirmed</li>
                                                   </ul>
                                               </div>
                                           </div> -->
                                       </div>
                                   </div>
                               </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
@stop
@section('js_script')
	<script type="text/javascript" src="{{ URL:: asset('public/assets/js/site/registration.js')}}"></script>
	<script type="text/javascript" src="{{ URL:: asset('public/assets/js/site/company-registration.js')}}"></script>
@stop