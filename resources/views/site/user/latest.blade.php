@extends('site.index')
@section('content')
    <?php
    $india_value = array_search('India', CommonHelper::countries());
    if (isset($data[0]['professional_detail']['current_rank'])) {
        $required_fields = CommonHelper::rank_required_fields()[$data[0]['professional_detail']['current_rank']];
    }
    if (isset($data[0]['document_permissions']) && !empty($data[0]['document_permissions'])) {
        $document_permissions = $data[0]['document_permissions'];
    }
    if (Auth::check()) {
        $registered_as = Auth::user()->registered_as;
    }
    ?>
    <style>
        h1 {
            display: block;
            font-size: 50px;
            margin: 25px auto 0;
            width: 975px;
        }

        .profile-content > small {
            color: #aaaaaa;
            font-size: 14px;
        }

        /*.profile-content>p>small{*/
        /*    color: #aaaaaa;*/
        /*}*/

        header {
            box-shadow: 1px 1px 4px rgba(0, 0, 0, 0.5);
            /*margin:   25px auto 50px;*/
            height: 70px;
            position: relative;
            width: auto;
        }

        figure.profile-banner {
            left: 0;
            overflow: hidden;
            position: absolute;
            top: 0;
            z-index: 1;
        }

        @media (min-width: 768px) {
            .nav-mobile {
                display: none;
            }

            .row.contentBoxes {
                text-align: center;
                padding-left: 20px;
            }

            figure.profile-picture {
                background-position: center center;
                background-size: cover;
                border: 5px #efefef solid;
                border-radius: 50%;
                bottom: -50px;
                box-shadow: inset 1px 1px 3px rgba(0, 0, 0, 0.2), 1px 1px 4px rgba(0, 0, 0, 0.3);
                height: 150px;
                margin-left: 95px;
                /*position: absolute;*/
                width: 150px;
                z-index: 3;
                margin-top: -73px;
            }

            .profile-content {
                padding-left: 23px;
            }
        }

        @media (max-width: 768px) {
            nav.navbar.navbar-inverse {
                display: none;
            }

            ul.nav-mobile {
                display: inline-flex;
                background: black;
                width: 100%;
                margin-top: 14px;
            }

            .nav-mobile li a {
                color: white;
                font-size: 11px;
            }

            .nav-mobile li {
                color: white;
                padding: 8px;
                margin: 1px;
            }

            ul.nav.navbar-right.top-nav {
                display: none;
            }

            span.navbar-toggler-icon {
                display: none;
            }

            li.course_list_item {
                font-size: 11px;
            }

            .row.contentBoxes {
                text-align: center;
                padding: 35px;
            }

            .profile-details {
                margin-top: 40px;
                text-align: left;
                margin: 17px 40px 4px 29px;
            }

            .profile-content {
                padding-left: 51px;
            }

            figure.profile-picture {
                background-position: center center;
                background-size: cover;
                border: 5px #efefef solid;
                border-radius: 50%;
                bottom: -50px;
                box-shadow: inset 1px 1px 3px rgba(0, 0, 0, 0.2), 1px 1px 4px rgba(0, 0, 0, 0.3);
                height: 150px;
                margin-left: 95px;
                /*position: absolute;*/
                width: 150px;
                z-index: 3;
                margin-top: 25px;
            }
        }

        /*figure.profile-picture {*/
        /*    background-position: center center;*/
        /*    background-size: cover;*/
        /*    border: 5px #efefef solid;*/
        /*    border-radius: 50%;*/
        /*    bottom: -50px;*/
        /*    box-shadow: inset 1px 1px 3px rgba(0, 0, 0, 0.2), 1px 1px 4px rgba(0, 0, 0, 0.3);*/
        /*    height: 150px;*/
        /*    margin-left: 95px;*/
        /*    !*position: absolute;*!*/
        /*    width: 150px;*/
        /*    z-index: 3;*/
        /*    margin-top: -73px;*/
        /*}*/

        div.profile-stats {
            top: 60px;
            left: 46px;
            padding: 38px 15px 24px 350px;
            position: absolute;
            right: 46px;
            z-index: 2;

            background: #043559;
            /* Generated Gradient */
            /*background: -moz-linear-gradient(top,  rgba(255,255,255,0.5) 0%, rgba(0,0,0,0.51) 3%, rgba(0,0,0,0.75) 61%, rgba(0,0,0,0.5) 100%);*/
            /*background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(255,255,255,0.5)), color-stop(3%,rgba(0,0,0,0.51)), color-stop(61%,rgba(0,0,0,0.75)), color-stop(100%,rgba(0,0,0,0.5)));*/
            /*background: -webkit-linear-gradient(top,  rgba(255,255,255,0.5) 0%,rgba(0,0,0,0.51) 3%,rgba(0,0,0,0.75) 61%,rgba(0,0,0,0.5) 100%);*/
            /*background: -o-linear-gradient(top,  rgba(255,255,255,0.5) 0%,rgba(0,0,0,0.51) 3%,rgba(0,0,0,0.75) 61%,rgba(0,0,0,0.5) 100%);*/
            /*background: -ms-linear-gradient(top,  rgba(255,255,255,0.5) 0%,rgba(0,0,0,0.51) 3%,rgba(0,0,0,0.75) 61%,rgba(0,0,0,0.5) 100%);*/
            /*background: linear-gradient(to bottom,  rgba(255,255,255,0.5) 0%,rgba(0,0,0,0.51) 3%,rgba(0,0,0,0.75) 61%,rgba(0,0,0,0.5) 100%);*/
            filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#80ffffff', endColorstr='#80000000', GradientType=0);

        }

        div.profile-stats ul {
            list-style: none;
            margin: 0;
            padding: 0;
        }

        div.profile-stats ul li {
            color: #efefef;
            display: block;
            float: left;
            font-size: 14px;
            font-weight: bold;
            margin-right: 50px;
            text-shadow: 1px 1px 2px rgba(0, 0, 0, 0.7)
        }

        div.profile-stats li span {
            display: block;
            font-size: 16px;
            font-weight: normal;
        }

        div.profile-stats a.follow {
            display: block;
            float: right;
            color: #ffffff;
            margin-top: 5px;
            text-decoration: none;

            /* This is a copy and paste from Bootstrap */
            background-color: #49afcd;
            text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.25);
            background-color: #49afcd;
            background-image: -moz-linear-gradient(top, #5bc0de, #2f96b4);
            background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#5bc0de), to(#2f96b4));
            background-image: -webkit-linear-gradient(top, #5bc0de, #2f96b4);
            background-image: -o-linear-gradient(top, #5bc0de, #2f96b4);
            background-image: linear-gradient(to bottom, #5bc0de, #2f96b4);
            background-repeat: repeat-x;
            border-color: #2f96b4 #2f96b4 #1f6377;
            border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.25);
            filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ff5bc0de', endColorstr='#ff2f96b4', GradientType=0);
            filter: progid:DXImageTransform.Microsoft.gradient(enabled=false);
            display: inline-block;
            padding: 4px 12px;
            margin-bottom: 0;
            font-size: 14px;
            line-height: 20px;
            box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.2), 0 1px 2px rgba(0, 0, 0, 0.05);
            border-radius: 4px;
        }

        .profile-name > small {
            /*display: flex;*/
            font-size: small;
        }

        div.profile-stats a.follow.followed {

            /* Once again copied from Boostrap */
            color: #ffffff;
            text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.25);
            background-color: #5bb75b;
            background-image: -moz-linear-gradient(top, #62c462, #51a351);
            background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#62c462), to(#51a351));
            background-image: -webkit-linear-gradient(top, #62c462, #51a351);
            background-image: -o-linear-gradient(top, #62c462, #51a351);
            background-image: linear-gradient(to bottom, #62c462, #51a351);
            background-repeat: repeat-x;
            border-color: #51a351 #51a351 #387038;
            border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.25);
            filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ff62c462', endColorstr='#ff51a351', GradientType=0);
            filter: progid:DXImageTransform.Microsoft.gradient(enabled=false);
        }


        .profile-content > h2 {
            color: #354B63;
            font-size: 25px;
            margin-bottom: 5px;
            /*padding-top: 5px;*/
            /*left: 350px;*/
            /*position: absolute;*/
            /*z-index: 5;*/
            /*padding-top: 4%;*/
        }

        .smClass {
            color: #aaaaaa;
            font-size: .5em;
        }

        .height-155 {
            height: 300px;
        }

        #wrapper-page {
            padding-left: 0;
        }

        #page-wrapper {
            width: 100%;
            padding: 0;
            background-color: #fff;
        }

        @media (min-width: 768px) {
            #wrapper-page {
                padding-left: 208px;
            }

            #page-wrapper {
                padding: 22px 10px;
            }
        }

        /* Top Navigation */


        .top-nav > li {
            display: inline-block;
            float: left;
        }

        .top-nav > li > a {
            padding-top: 20px;
            padding-bottom: 20px;
            line-height: 20px;
            color: #fff;
        }

        .top-nav > li > a:hover,
        .top-nav > li > a:focus,
        .top-nav > .open > a,
        .top-nav > .open > a:hover,
        .top-nav > .open > a:focus {
            color: #fff;
            background-color: #043559;
        }

        .top-nav > .open > .dropdown-menu {
            float: left;
            position: absolute;
            margin-top: 0;
            /*border: 1px solid rgba(0,0,0,.15);*/
            border-top-left-radius: 0;
            border-top-right-radius: 0;
            background-color: #fff;
            -webkit-box-shadow: 0 6px 12px #043559;
            box-shadow: 0 6px 12px #043559;
        }

        .top-nav > .open > .dropdown-menu > li > a {
            white-space: normal;
        }

        /* Side Navigation */

        @media (min-width: 768px) {

            .top-nav {
                padding: 0 90px 0px 15px;
            }

            .side-nav {
                position: fixed;
                top: 0px;
                padding-top: 100px;
                left: 225px;
                width: 225px;
                margin-left: -225px;
                border: none;
                border-radius: 0;
                border-top: 1px #043559 solid;
                overflow-y: auto;
                background-color: #043559;
                /*background-color: #5A6B7D;*/
                bottom: 0;
                overflow-x: hidden;
                padding-bottom: 40px;
            }

            .side-nav > li > a {
                width: 225px;
                border-bottom: #043559;
            }

            .side-nav li a:hover,
            .side-nav li a:focus {
                outline: none;
                background-color: #043559 !important;
            }
        }

        .side-nav > li > ul {
            padding: 0;
            border-bottom: 1px #043559 solid;
        }

        .side-nav > li > ul > li > a {
            display: block;
            padding: 10px 15px 10px 38px;
            text-decoration: none;
            /*color: #999;*/
            color: #fff;
        }

        .side-nav > li > ul > li > a:hover {
            color: #fff;
        }

        .navbar .nav > li > a > .label {
            -webkit-border-radius: 50%;
            -moz-border-radius: 50%;
            border-radius: 50%;
            position: absolute;
            top: 14px;
            right: 6px;
            font-size: 10px;
            font-weight: normal;
            min-width: 15px;
            min-height: 15px;
            line-height: 1.0em;
            text-align: center;
            padding: 2px;
        }

        .navbar .nav > li > a:hover > .label {
            top: 10px;
        }

        .my-profile {
            padding: 37px 0 80px 0 !important;
        }

        .container-fluid {
            /*margin-right: 0 !important;*/
            padding-right: 0 !important;
        }

        .cs-header {
            background: #043559;
        }

        .navbar-inverse .navbar-collapse, .navbar-inverse .navbar-form {
            background-color: #043559 !important;
            border-color: #043559 !important;
        }

        .navbar {
            margin-top: 18px;
            border: 0 !IMPORTANT;
        }

        ::-webkit-scrollbar {
            width: 5px;
        }

        /* Track */
        ::-webkit-scrollbar-track {
            background: #3399FF;
        }

        /* Handle */
        ::-webkit-scrollbar-thumb {
            background: #5ad3ff;
        }

        /* Handle on hover */
        ::-webkit-scrollbar-thumb:hover {
            background: #5ad3ff;
        }


        .profile-content h3 {
            margin-top: 6px;
            font-size: 23px;
        }

        .boxdata {
            width: 100%;
            margin-left: 6px;
        }

        .card-header {
            background-color: #043559;
            border-radius: 0 !important;
        }

        .profile-details p {
            margin: 0 0 15px;
            color: lightblue;
        }

        .button-text {
            margin-top: 10px;
            padding: 6px 50px;
            background: skyblue;
            border: 0 !important;
            color: white;
            font-size: 16px;
        }

        .hrLine {
            margin: 20px 10px 20px 10px;
        }

        .contentline {
            padding: 14px;
            font-style: italic;
            margin-bottom: 0 !important;
        }

        .contentBoxes .col-md-2 {
            border: 1px solid #dedede;
            padding: 8px;
            margin: 15px;
        }


        .contentBoxes .col-md-2:hover {
            box-shadow: 0 3px 5px 0 #34B5F2;
            transition: box-shadow 0.3s ease-in-out;
        }

        .contentBoxes .box {
            text-align: center;
        }

        .contentBoxes .box img {
            width: 80%;
        }

        i.fa.fa-check-circle {
            color: green;
        }

        .profile-details b, .profile-content h3 {
            color: #043559;
        }

        p.tag-line {
            margin-top: 15px;
            margin-bottom: 1px;
        }

        .experience-box {
            border: 1px solid #dedede;
        }

        .experience-box .header {
            font-weight: 500;
            background: #fbf8f8;
            padding: 11px 13px 13px 13px;
            border-bottom: 1px solid #dedede;
            font-size: 18px;
        }

        .tags span {
            background: #e8dfdf;
            padding: 3px 4px;
            font-size: 11px;
            border-radius: 3px;
            margin: 4px;
        }

        .progress {
            width: 150px;
            margin-top: 13px;
        }

        .storage span {
            color: white;
            font-weight: 500;
        }

        .progress-bar {
            font-size: 11px;
        }

        .storage {
            padding: 15px;
        }

        .tags {
            padding: 10px;
        }

        .other-service {
            width: 100%;
            height: 165px;
            border-radius: 18px;
            background: #F1F3F7;
            border: 1px solid lightgray;
        }
    </style>
    <main class="main">
        <input type="hidden" class="user_id" value="{{ isset($user_id) ? $user_id : ''}}">
        <input type="hidden" name="verify_email" id="verify_email"
               value="{{isset($data[0]['is_email_verified']) ? $data[0]['is_email_verified'] : ''}}">
        @if(isset($data[0]['personal_detail']['country']) && $data[0]['personal_detail']['country'] == $india_value)
            <input type="hidden" name="verify_mobile" id="verify_mobile"
                   value="{{isset($data[0]['is_mob_verified']) ? $data[0]['is_mob_verified'] : ''}}">
        @endif
        <div class="my-profile">
            <div class="container-fluid">
                <div id="wrapper-page">
                    <!-- Navigation -->
                    <nav class="navbar navbar-inverse" role="navigation">
                        <!-- Brand and toggle get grouped for better mobile display -->
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse"
                                    data-target=".navbar-ex1-collapse">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>
                        <!-- Top Menu Items -->
                        <ul class="nav navbar-right top-nav">
                            <li><a href="#" data-placement="bottom" data-toggle="tooltip" href="#"
                                   data-original-title="Stats">Contact
                                </a>
                            </li>
                            <li><a href="#" data-placement="bottom" data-toggle="tooltip" href="#"
                                   data-original-title="Stats">Personal
                                </a>
                            </li>
                            <li><a href="#" data-placement="bottom" data-toggle="tooltip" href="#"
                                   data-original-title="Stats">Professional
                                </a>
                            </li>
                            <li><a href="#" data-placement="bottom" data-toggle="tooltip" href="#"
                                   data-original-title="Stats">Personality
                                </a>
                            </li>
                        </ul>
                        <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
                        <div class="collapse navbar-collapse navbar-ex1-collapse">
                            <ul class="nav navbar-nav side-nav">
                                {{--                                <li>--}}
                                {{--                                    <a href="#" data-toggle="collapse" data-target="#submenu-1"><i--}}
                                {{--                                                class="fa fa-fw fa-search"></i>Edit Profile<i--}}
                                {{--                                                class="fa fa-fw fa-angle-down pull-right"></i></a>--}}
                                {{--                                    <ul id="submenu-1" class="collapse">--}}
                                {{--                                        <li><a href="#"><i class="fa fa-angle-double-right"></i> SUBMENU 1.1</a></li>--}}
                                {{--                                        <li><a href="#"><i class="fa fa-angle-double-right"></i> SUBMENU 1.2</a></li>--}}
                                {{--                                        <li><a href="#"><i class="fa fa-angle-double-right"></i> SUBMENU 1.3</a></li>--}}
                                {{--                                    </ul>--}}
                                {{--                                </li>--}}
                                {{--                                <li>--}}
                                {{--                                    <a href="#" data-toggle="collapse" data-target="#submenu-2"><i--}}
                                {{--                                                class="fa fa-fw fa-star"></i> MENU 2 <i--}}
                                {{--                                                class="fa fa-fw fa-angle-down pull-right"></i></a>--}}
                                {{--                                    <ul id="submenu-2" class="collapse">--}}
                                {{--                                        <li><a href="#"><i class="fa fa-angle-double-right"></i> SUBMENU 2.1</a></li>--}}
                                {{--                                        <li><a href="#"><i class="fa fa-angle-double-right"></i> SUBMENU 2.2</a></li>--}}
                                {{--                                        <li><a href="#"><i class="fa fa-angle-double-right"></i> SUBMENU 2.3</a></li>--}}
                                {{--                                    </ul>--}}
                                {{--                                </li>--}}
                                <li>
                                    <a href="investigaciones/favoritas"><i class="fa fa-pencil-square-o"></i> Edit
                                        Profile</a>
                                </li>
                                <li>
                                    <a href="sugerencias"><i class="fa fa-download"></i> Download document</a>
                                </li>
                                <li>
                                    <a href="faq"><i class="fa fa-paper-plane"></i> Share details</a>
                                </li>
                                <li>
                                    <a href="faq"><i class="fa fa-address-book"></i> Mark experience</a>
                                </li>
                                <li>
                                    <a href="faq"><i class="fa fa-fw fa-user-plus"></i> Connect</a>
                                </li>
                                <li>
                                    <a href="faq"><i class="fa fa-fw fa fa-question-circle"></i> Save memories</a>
                                </li>
                                <hr>
                                <li class="storage">
                                    <hr>
                                    <span>Storage</span>
                                    <div class="progress">
                                        <div class="progress-bar" role="progressbar" style="width: 30%"
                                             aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"> 150mb
                                        </div>
                                    </div>
                                    <span style="text-align: center;">250 mb        -</span>
                                </li>
                            </ul>
                        </div>
                        <!-- /.navbar-collapse -->
                    </nav>

                    <div id="page-wrapper">
                        <div class="container-fluid">
                            <!-- Page Heading -->
                            <div class="row" id="main">
                                <div class="section">
                                    <div class="row">
                                        <div class="col-md-6 col-sm-12">
                                            <figure class="profile-picture"
                                                    style="background-image: url('http://unsplash.it/150/150')">
                                            </figure>
                                            <div class="profile-content">
                                                <h2 class="profile-name">Jatin Balkrishna
                                                    <small>At home</small>
                                                </h2>
                                                <small class="smClass"><i class="fa fa-globe">&nbsp;</i>india</small>
                                                <h3>Second enginner</h3>
                                                <button class="button-text">Checkout my resume</button>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 nav-mobile">
                                            <ul class="nav-mobile">
                                                <li><a href="#" data-placement="bottom" data-toggle="tooltip"
                                                       data-original-title="Stats">Contact
                                                    </a>
                                                </li>
                                                <li><a href="#" data-placement="bottom" data-toggle="tooltip"
                                                       data-original-title="Stats">Personal
                                                    </a>
                                                </li>
                                                <li><a href="#" data-placement="bottom" data-toggle="tooltip"
                                                       data-original-title="Stats">Professional
                                                    </a>
                                                </li>
                                                <li><a href="#" data-placement="bottom" data-toggle="tooltip"
                                                       data-original-title="Stats">Personality
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="col-md-6 col-sm-12">
                                            <div class="profile-details">
                                                <p><b>Email:- </b>balkrishna@gmail.com <i
                                                            class="fa fa-check-circle"></i></p>
                                                <p><b>Mobile number:- </b>145565356566 <i
                                                            class="fa fa-check-circle"></i></p>
                                                <p><b>Altertnate number:- </b>122 349 5949</p>
                                                <p><b>Nearest Airport:- </b>Mumbai airpot</p>
                                                <p><b>Residential Address:- </b>B 21 shivdhara building<br> west mumbai- 20203
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <hr class="hrLine">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <p class="contentline">Lorem Ipsum is simply dummy text of the printing and
                                                typesetting
                                                industry. Lorem Ipsum has been the industry's standard dummy text ever
                                                since the 1500s, when an unknown printer took a galley of type and
                                                scrambled it to make a type specimen book.</p>
                                        </div>
                                    </div>
                                    <hr class="hrLine">
                                    <div class="row contentBoxes">
                                        <div class="col-md-2 col-sm-12">
                                            <div class="box">
                                                <img src="https://www.w3schools.com/html/img_chania.jpg "
                                                     alt="Italian Trulli">
                                                <p class="tag-line">My Documents</p>
                                            </div>
                                        </div>
                                        <div class="col-md-2 col-sm-12">
                                            <div class="box">
                                                <img src="https://www.w3schools.com/html/img_chania.jpg "
                                                     alt="Italian Trulli">
                                                <p class="tag-line">My Documents</p>
                                            </div>
                                        </div>
                                        <div class="col-md-2 col-sm-12">
                                            <div class="box">
                                                <img src="https://www.w3schools.com/html/img_chania.jpg "
                                                     alt="Italian Trulli">
                                                <p class="tag-line">My Documents</p>
                                            </div>
                                        </div>
                                        <div class="col-md-2 col-sm-12">
                                            <div class="box">
                                                <img src="https://www.w3schools.com/html/img_chania.jpg "
                                                     alt="Italian Trulli">
                                                <p class="tag-line">My Documents</p>
                                            </div>
                                        </div>
                                        <div class="col-md-2 col-sm-12">
                                            <div class="box">
                                                <img src="https://www.w3schools.com/html/img_chania.jpg "
                                                     alt="Italian Trulli">
                                                <p class="tag-line">My Documents</p>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="card boxdata">
                                                <div class="card-header" style="padding:0;">
                                                    <ul class="nav nav-tabs course_list" role="tablist">
                                                        <li role="presentation" class="course_list_item active">
                                                            <a href="#gi" aria-controls="gi" role="tab"
                                                               data-toggle="tab">General Information</a>
                                                        </li>
                                                        <li role="presentation" class="course_list_item">
                                                            <a href="#cd" aria-controls="cd" role="tab"
                                                               data-toggle="tab">Course Details</a>
                                                        </li>
                                                        <li role="presentation" class="course_list_item">
                                                            <a href="#ssd" aria-controls="ssd" role="tab"
                                                               data-toggle="tab">Sea Service Details</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="card-body z-depth-1">
                                                    <div class="tab-content">
                                                        <div role="tabpanel" class="tab-pane active" id="gi">
                                                            <div class="content">
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <div class="experience-box">
                                                                            <div class="header">
                                                                                Experience on
                                                                            </div>
                                                                            <div class="tags">
                                                                                <span>Herry</span>
                                                                                <span>Peh</span>
                                                                                <span>jofnph</span>
                                                                                <span>php</span>
                                                                                <span>SQl</span>
                                                                                <hr>
                                                                                <span>Herry</span>
                                                                                <span>Peh</span>
                                                                                <span>jofnph</span>
                                                                                <span>php</span>
                                                                                <span>SQl</span>
                                                                            </div>


                                                                        </div>
                                                                    </div>
                                                                    <hr>
                                                                    <div class="col-lg-12">

                                                                        <div class="content-title"
                                                                             style="border-bottom:1px solid #a9b6cb;">
                                                                            <div class="d-flex block-small">
                                                                                <div class="d-flex--one">
                                                                                    <span class="dark-blue title">GENERAL DETAILS</span>
                                                                                </div>
                                                                                @if($user != 'another_user')
                                                                                    <div class="d-flex--two">
                                                                                        <button href="{{route('site.seafarer.edit.profile')}}"
                                                                                                class="btn cs-primary-btn edit_link"
                                                                                                data-form='documents'><i
                                                                                                    class="fa fa-pencil-square-o">
                                                                                                Edit</i></button>
                                                                                    </div>
                                                                                @endif
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-lg-12">
                                                                        <div class="content-title">
                                                                            <div class="d-flex block-small">
                                                                                <div class="d-flex--one">
                                                                                    <span class="dark-blue sub-title">PASSPORT DETAILS</span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-sm-3">
                                                                        <div class="list">
                                                                            <div class="list-item">
                                                                                <div class="text"><span class="black">Passport Country</span>
                                                                                    :
                                                                                    <span class="blue">
                                                            		@if(isset($data[0]['passport_detail']['pass_country']) && !empty($data[0]['passport_detail']['pass_country']))
                                                                                            @foreach( CommonHelper::countries() as $c_index => $country)
                                                                                                {{ isset($data[0]['passport_detail']['pass_country']) ? $data[0]['passport_detail']['pass_country'] == $c_index ? $country : '' : ''}}
                                                                                            @endforeach
                                                                                        @else
                                                                                            -
                                                                                        @endif
                                                            	</span>
                                                                                </div>
                                                                            </div>
                                                                            <div class="list-item">
                                                                                <div class="text"><span class="black">US Visa</span>
                                                                                    :
                                                                                    <span class="blue">
                                                            		{{ isset($data[0]['passport_detail']['us_visa']) ? $data[0]['passport_detail']['us_visa'] == 1 ? 'Yes' : 'No' : 'No'}}
                                                            	</span>
                                                                                </div>
                                                                            </div>
                                                                            <div class="list-item">
                                                                                <div class="text"><span class="black">Yellow Fever Vaccination</span>
                                                                                    :
                                                                                    <span class="blue">
                                                            		{{ isset($data[0]['wkfr_detail']['yellow_fever']) ? $data[0]['wkfr_detail']['yellow_fever'] == '1'? 'Yes' : 'No' : 'NO' }}
                                                            	</span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-sm-3">
                                                                        <div class="list">
                                                                            <div class="list-item">
                                                                                <div class="text"><span class="black">Passport Number</span>
                                                                                    :
                                                                                    <span class="blue">
                                                            		{{ isset($data[0]['passport_detail']['pass_number']) ? $data[0]['passport_detail']['pass_number'] : '-' }}
                                                            	</span>
                                                                                </div>
                                                                            </div>
                                                                            <div class="list-item">
                                                                                <div class="text"><span class="black">Expiry Date</span>
                                                                                    :
                                                                                    <span class="blue">
                                                            		{{ isset($data[0]['passport_detail']['us_visa_expiry_date']) ? date('d-m-Y',strtotime($data[0]['passport_detail']['us_visa_expiry_date'])) : '-'}}
                                                            	</span>
                                                                                </div>
                                                                            </div>
                                                                            <div class="list-item">
                                                                                <div class="text"><span class="black">Yellow Fever Issue Date</span>
                                                                                    :
                                                                                    <span class="blue">
                                                            		{{ isset($data[0]['wkfr_detail']['yf_issue_date']) ? date('d-m-Y',strtotime($data[0]['wkfr_detail']['yf_issue_date'])) : '-'}}
                                                            	</span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-sm-3">
                                                                        <div class="list">
                                                                            <div class="list-item">
                                                                                <div class="text"><span class="black">Place Of Issue</span>
                                                                                    :
                                                                                    <span class="blue">
                                                            		{{ isset($data[0]['passport_detail']['place_of_issue']) ? $data[0]['passport_detail']['place_of_issue'] : '-' }}
                                                            	</span>
                                                                                </div>
                                                                            </div>
                                                                            <div class="list-item">
                                                                                <div class="text"><span class="black">Indian PCC</span>
                                                                                    :
                                                                                    <span class="blue">
                                                            		{{ isset($data[0]['passport_detail']['indian_pcc']) ? $data[0]['passport_detail']['indian_pcc'] == '1'? 'Yes' : 'No' : 'No'}}
                                                            	</span>
                                                                                </div>
                                                                            </div>
                                                                            @if($data[0]['passport_detail']['fromo'] == 1 || ($data[0]['passport_detail']['fromo'] == 0))
                                                                                <div class="list-item">
                                                                                    <div class="text"><span
                                                                                                class="black">Experience Of Framo</span>
                                                                                        :
                                                                                        <span class="blue">
                                                            		{{ isset($data[0]['passport_detail']['fromo']) ? $data[0]['passport_detail']['fromo'] == '1'? 'Yes' : 'No' : 'NO' }}
                                                            	</span>
                                                                                    </div>
                                                                                </div>
                                                                            @endif
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-sm-3">
                                                                        <div class="list">
                                                                            <div class="list-item">
                                                                                <div class="text"><span class="black">Expiry Date</span>
                                                                                    :
                                                                                    <span class="blue">
                                                            		{{ isset($data[0]['passport_detail']['pass_expiry_date'])? date('d-m-Y',strtotime($data[0]['passport_detail']['pass_expiry_date'])) : '-' }}
                                                            	</span>
                                                                                </div>
                                                                            </div>
                                                                            <div class="list-item">
                                                                                <div class="text"><span class="black">INDOS Number</span>
                                                                                    :
                                                                                    <span class="blue">
                                                            		{{ !empty($data[0]['wkfr_detail']['indos_number']) ? $data[0]['wkfr_detail']['indos_number'] : '-'}}
                                                            	</span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!-- SEAMAN BOOK DETAILS -->
                                                                <div class="row">
                                                                    <div class="col-lg-12">
                                                                        <div class="content-title"
                                                                             style="border-top:1px solid #a9b6cb;">
                                                                            <div class="d-flex block-small">
                                                                                <div class="d-flex--one">
                                                                                    <span class="dark-blue sub-title">SEAMAN BOOK DETAILS</span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    @foreach($data[0]['seaman_book_detail'] as $index => $cdc_data)
                                                                        <div class="col-sm-2">
                                                                            <div class="list">
                                                                                <div class="list-item">
                                                                                    <div class="text"><span
                                                                                                class="black">CDC</span>
                                                                                        :
                                                                                        <span class="blue">
                                                            		@foreach( CommonHelper::countries() as $c_index => $country)
                                                                                                {{ isset($cdc_data['cdc']) ? $cdc_data['cdc'] == $c_index ? $country : '' : ''}}
                                                                                            @endforeach
                                                            	</span>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-sm-2">
                                                                            <div class="list">
                                                                                <div class="list-item">
                                                                                    <div class="text"><span
                                                                                                class="black">Number</span>
                                                                                        :
                                                                                        <span class="blue">
                                                            		{{isset($cdc_data['cdc_number']) ? $cdc_data['cdc_number'] : ''}}
                                                            	</span>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-sm-2">
                                                                            <div class="list">
                                                                                <div class="list-item">
                                                                                    <div class="text"><span
                                                                                                class="black">Issue Date</span>
                                                                                        :
                                                                                        <span class="blue">
                                                            		{{isset($cdc_data['cdc_issue_date']) ? date('d-m-Y',strtotime($cdc_data['cdc_issue_date'])) : ''}}
                                                            	</span>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-sm-3">
                                                                            <div class="list">
                                                                                <div class="list-item">
                                                                                    <div class="text"><span
                                                                                                class="black">Verification Date</span>
                                                                                        :
                                                                                        <span class="blue">
                                                            		{{ !is_null($cdc_data['cdc_verification_date']) ? date('d-m-Y',strtotime($cdc_data['cdc_verification_date'])) : '-' }}
                                                            	</span>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-sm-3">
                                                                            <div class="list">
                                                                                <div class="list-item">
                                                                                    <div class="text"><span
                                                                                                class="black">Expiry Date</span>
                                                                                        :
                                                                                        <span class="blue">
                                                            		{{isset($cdc_data['cdc_expiry_date']) ? date('d-m-Y',strtotime($cdc_data['cdc_expiry_date'])) : ''}}
                                                                                            @if($cdc_data['status'] == 1)
                                                                                                <i class="fa fa-check-circle green f-15 m-l-15"
                                                                                                   aria-hidden="true"
                                                                                                   title="Verified"></i>
                                                                                            @endif
                                                            	</span>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    @endforeach
                                                                </div>

                                                                <!-- CERTIFICATE OF COMPENTENCY -->
                                                                @if(isset($required_fields))
                                                                    @if(isset($data[0]['coc_detail']) AND !empty($data[0]['coc_detail']) && (in_array('COC',$required_fields) OR in_array('COC-Optional',$required_fields)))
                                                                        <div class="row">
                                                                            <div class="col-lg-12">
                                                                                <div class="content-title"
                                                                                     style="border-top:1px solid #a9b6cb;">
                                                                                    <div class="d-flex block-small">
                                                                                        <div class="d-flex--one">
                                                                                            <span class="dark-blue sub-title">CERTIFICATE OF COMPENTENCY</span>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row">
                                                                            @foreach($data[0]['coc_detail'] as $index => $coc_data)
                                                                                <div class="col-sm-2">
                                                                                    <div class="list">
                                                                                        <div class="list-item">
                                                                                            <div class="text"><span
                                                                                                        class="black">COC</span>
                                                                                                :
                                                                                                <span class="blue">
		                                                            		@if(isset($coc_data['coc']) && !empty($coc_data['coc']))
                                                                                                        @foreach( CommonHelper::countries() as $c_index => $country)
                                                                                                            {{ isset($coc_data['coc']) ? $coc_data['coc'] == $c_index ? $country : '' : ''}}
                                                                                                        @endforeach
                                                                                                    @else
                                                                                                        -
                                                                                                    @endif
		                                                            	</span>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-sm-2">
                                                                                    <div class="list">
                                                                                        <div class="list-item">
                                                                                            <div class="text"><span
                                                                                                        class="black">Number</span>
                                                                                                :
                                                                                                <span class="blue">
		                                                            		{{ !empty($coc_data['coc_number']) ? $coc_data['coc_number'] : '-'}}
		                                                            	</span>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-sm-2">
                                                                                    <div class="list">
                                                                                        <div class="list-item">
                                                                                            <div class="text"><span
                                                                                                        class="black">Grade</span>
                                                                                                :
                                                                                                <span class="blue">
		                                                            		{{ !empty($coc_data['coc_grade']) ? $coc_data['coc_grade'] : '-' }}
		                                                            	</span>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-sm-3">
                                                                                    <div class="list">
                                                                                        <div class="list-item">
                                                                                            <div class="text"><span
                                                                                                        class="black">Verification Date</span>
                                                                                                :
                                                                                                <span class="blue">
		                                                            		{{ !is_null($coc_data['coc_verification_date']) ? date('d-m-Y',strtotime($coc_data['coc_verification_date'])) : '-' }}
		                                                            	</span>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-sm-3">
                                                                                    <div class="list">
                                                                                        <div class="list-item">
                                                                                            <div class="text"><span
                                                                                                        class="black">Expiry Date</span>
                                                                                                :
                                                                                                <span class="blue">
		                                                            		{{ !is_null($coc_data['coc_expiry_date']) ? date('d-m-Y',strtotime($coc_data['coc_expiry_date'])) : '-' }}

                                                                                                    @if(isset($coc_data['status']) && $coc_data['status'] == 1)
                                                                                                        <i class="fa fa-check-circle green f-15 m-l-15"
                                                                                                           aria-hidden="true"
                                                                                                           title="Verified"></i>
                                                                                                    @endif
		                                                            	</span>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            @endforeach
                                                                        </div>
                                                                    @endif
                                                                @endif

                                                            <!-- CERTIFICATE OF ENDORSEMENT -->
                                                                @if(isset($required_fields))
                                                                    @if(isset($data[0]['coe_detail']) AND !empty($data[0]['coe_detail']) && (in_array('COE',$required_fields) OR in_array('COE-Optional',$required_fields)) && (!empty($data[0]['coe_detail'][0]['coe']) || !empty($data[0]['coe_detail'][0]['coe_number']) || !empty($data[0]['coe_detail'][0]['coe_grade']) || !empty($data[0]['coe_detail'][0]['coe_expiry_date']) || !empty($data[0]['coe_detail'][0]['coe_verification_date'])))
                                                                        <div class="row">
                                                                            <div class="col-lg-12">
                                                                                <div class="content-title"
                                                                                     style="border-top:1px solid #a9b6cb;">
                                                                                    <div class="d-flex block-small">
                                                                                        <div class="d-flex--one">
                                                                                            <span class="dark-blue sub-title">CERTIFICATE OF ENDORSEMENT</span>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row">
                                                                            @foreach($data[0]['coe_detail'] as $index => $coe_data)
                                                                                <div class="col-sm-3">
                                                                                    <div class="list">
                                                                                        <div class="list-item">
                                                                                            <div class="text"><span
                                                                                                        class="black">COE</span>
                                                                                                :
                                                                                                <span class="blue">
		                                                            		@if(isset($coe_data['coe']) && !empty($coe_data['coe']))
                                                                                                        @foreach( CommonHelper::countries() as $c_index => $country)
                                                                                                            {{ isset($coe_data['coe']) ? $coe_data['coe'] == $c_index ? $country : '' : ''}}
                                                                                                        @endforeach
                                                                                                    @else
                                                                                                        -
                                                                                                    @endif
		                                                            	</span>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-sm-3">
                                                                                    <div class="list">
                                                                                        <div class="list-item">
                                                                                            <div class="text"><span
                                                                                                        class="black">Number</span>
                                                                                                :
                                                                                                <span class="blue">
		                                                            		{{ !empty($coe_data['coe_number']) ? $coe_data['coe_number'] : '-'}}
		                                                            	</span>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-sm-3">
                                                                                    <div class="list">
                                                                                        <div class="list-item">
                                                                                            <div class="text"><span
                                                                                                        class="black">Grade</span>
                                                                                                :
                                                                                                <span class="blue">
		                                                            		{{ !empty($coe_data['coe_grade']) ? $coe_data['coe_grade'] : '-' }}
		                                                            	</span>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-sm-3">
                                                                                    <div class="list">
                                                                                        <div class="list-item">
                                                                                            <div class="text"><span
                                                                                                        class="black">Expiry Date</span>
                                                                                                :
                                                                                                <span class="blue">
		                                                            		{{ !empty($coe_data['coe_expiry_date']) ? date('d-m-Y',strtotime($coe_data['coe_expiry_date'])) : '-' }}
		                                                            	</span>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            @endforeach
                                                                        </div>
                                                                    @endif
                                                                @endif
                                                                <hr>
                                                                <div class="row">
                                                                    <div class="col-lg-12">
                                                                        <div class="content-title"
                                                                             style="border-top:1px solid #a9b6cb;">
                                                                            <div class="d-flex block-small">
                                                                                <div class="d-flex--one">
                                                                                    <span class="dark-blue sub-title">OTHER SERVICE</span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-lg-12">
                                                                        <textarea class="other-service"></textarea>
                                                                    </div>
                                                                </div>
                                                                <!-- GMDSS DETAILS -->
                                                                @if(isset($required_fields) && !empty($required_fields))
                                                                    @if(isset($data[0]['gmdss_detail']) AND !empty($data[0]['gmdss_detail']) && (in_array('GMDSS',$required_fields) OR in_array('GMDSS-Optional',$required_fields)) && ((!empty($data[0]['gmdss_detail']['gmdss'])) || (!empty($data[0]['gmdss_detail']['gmdss_number'])) || (!empty($data[0]['gmdss_detail']['gmdss_expiry_date']))))
                                                                        <div class="row">
                                                                            <div class="col-lg-12">
                                                                                <div class="content-title"
                                                                                     style="border-top:1px solid #a9b6cb;">
                                                                                    <div class="d-flex block-small">
                                                                                        <div class="d-flex--one">
                                                                                            <span class="dark-blue sub-title">GMDSS DETAILS</span>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row">
                                                                            <div class="col-sm-2">
                                                                                <div class="list">
                                                                                    <div class="list-item">
                                                                                        <div class="text"><span
                                                                                                    class="black">GMDSS</span>
                                                                                            :
                                                                                            <span class="blue">
		                                                            		@foreach( CommonHelper::countries() as $c_index => $country)
                                                                                                    {{ isset($data[0]['gmdss_detail']['gmdss']) ? $data[0]['gmdss_detail']['gmdss'] == $c_index ? $country : '' : ''}}
                                                                                                @endforeach
		                                                            	</span>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-sm-2">
                                                                                <div class="list">
                                                                                    <div class="list-item">
                                                                                        <div class="text"><span
                                                                                                    class="black">Number</span>
                                                                                            :
                                                                                            <span class="blue">
		                                                            		{{ isset($data[0]['gmdss_detail']['gmdss_number']) ? $data[0]['gmdss_detail']['gmdss_number'] : '-'}}
		                                                            	</span>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-sm-2">
                                                                                <div class="list">
                                                                                    <div class="list-item">
                                                                                        <div class="text"><span
                                                                                                    class="black">Expiry Date</span>
                                                                                            :
                                                                                            <span class="blue">
		                                                            		{{ isset($data[0]['gmdss_detail']['gmdss_expiry_date']) ? date('d-m-Y',strtotime($data[0]['gmdss_detail']['gmdss_expiry_date'])) : '-' }}
		                                                            	</span>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-sm-3 gmdss_endorsement {{isset($data[0]['gmdss_detail']['gmdss']) && $data[0]['gmdss_detail']['gmdss'] == $india_value ? '' : 'hide'}}">
                                                                                <div class="list">
                                                                                    <div class="list-item">
                                                                                        <div class="text"><span
                                                                                                    class="black">Endorsement Number</span>
                                                                                            :
                                                                                            <span class="blue">
		                                                            		{{ isset($data[0]['gmdss_detail']['gmdss_endorsement_number']) && !empty($data[0]['gmdss_detail']['gmdss_endorsement_number']) ? $data[0]['gmdss_detail']['gmdss_endorsement_number'] : '-'}}
		                                                            	</span>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-sm-3 gmdss_valid_till {{isset($data[0]['gmdss_detail']['gmdss']) && $data[0]['gmdss_detail']['gmdss'] == $india_value ? '' : 'hide'}}">
                                                                                <div class="list">
                                                                                    <div class="list-item">
                                                                                        <div class="text"><span
                                                                                                    class="black">Valid Till</span>
                                                                                            :
                                                                                            <span class="blue">
		                                                            		{{ isset($data[0]['gmdss_detail']['gmdss_endorsement_expiry_date']) && !empty($data[0]['gmdss_detail']['gmdss_endorsement_expiry_date']) ? date('d-m-Y',strtotime($data[0]['gmdss_detail']['gmdss_endorsement_expiry_date'])) : '-'}}
		                                                            	</span>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    @endif
                                                                @endif
                                                            </div>
                                                        </div>
                                                        <div role="tabpanel" class="tab-pane" id="ssd">
                                                            <div class="content">
                                                                <div class="row">
                                                                    <div class="col-lg-12">
                                                                        <div class="content-title"
                                                                             style="border-bottom:1px solid #a9b6cb;">
                                                                            <div class="d-flex block-small">
                                                                                <div class="d-flex--one">
                                                                                    <span class="dark-blue title">SEA SERVICE DETAILS</span>
                                                                                </div>
                                                                                @if($user != 'another_user')
                                                                                    <div class="d-flex--two">
                                                                                        <button href="{{route('site.seafarer.edit.profile')}}"
                                                                                                class="btn cs-primary-btn edit_link"
                                                                                                data-form='sea_service'>
                                                                                            <i class="fa fa-pencil-square-o">
                                                                                                Edit</i></button>
                                                                                    </div>
                                                                                @endif
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                @if(isset($data[0]['sea_service_detail']) AND !empty($data[0]['sea_service_detail']))
                                                                    @foreach($data[0]['sea_service_detail'] as $index => $services)
                                                                        <div class="row" style="margin-top:15px;">
                                                                            <div class="col-sm-4">
                                                                                <div class="list">
                                                                                    <div class="list-item">
                                                                                        <div class="text"><span
                                                                                                    class="black">Rank</span>
                                                                                            :
                                                                                            <span class="blue">
		                                                            		@foreach(CommonHelper::new_rank() as $index => $category)
                                                                                                    @foreach($category as $r_index => $rank)
                                                                                                        {{ isset($services['rank_id']) ? $services['rank_id'] == $r_index ? $rank : '' : ''}}
                                                                                                    @endforeach
                                                                                                @endforeach
		                                                            	</span>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="list-item">
                                                                                        <div class="text"><span
                                                                                                    class="black">Shipping Company</span>
                                                                                            :
                                                                                            <span class="blue">
		                                                            		{{ isset($services['company_name']) ? $services['company_name'] : '-'}}
		                                                            	</span>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="list-item">
                                                                                        <div class="text"><span
                                                                                                    class="black">GRT</span>
                                                                                            :
                                                                                            <span class="blue">
		                                                            		{{ !empty($services['grt']) ? $services['grt'] : '-'}}
		                                                            	</span>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="list-item">
                                                                                        <div class="text"><span
                                                                                                    class="black">Sign On Date</span>
                                                                                            :
                                                                                            <span class="blue">
		                                                            		{{ isset($services['from']) ? date('d-m-Y',strtotime($services['from'])) : ''}}
		                                                            	</span>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-sm-4">
                                                                                <div class="list">
                                                                                    <div class="list-item">
                                                                                        <div class="text"><span
                                                                                                    class="black">Name Of Ship</span>
                                                                                            :
                                                                                            <span class="blue">
		                                                            		{{ isset($services['ship_name']) ? $services['ship_name'] : '-'}}
		                                                            	</span>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="list-item">
                                                                                        <div class="text"><span
                                                                                                    class="black">BHP</span>
                                                                                            :
                                                                                            <span class="blue">
		                                                            		{{ !empty($services['bhp']) ? $services['bhp'] : '-'}}
		                                                            	</span>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="list-item">
                                                                                        <div class="text"><span
                                                                                                    class="black">Sign Off Date</span>
                                                                                            :
                                                                                            <span class="blue">
		                                                            		{{ isset($services['to']) ? date('d-m-Y',strtotime($services['to'])) : ''}}
		                                                            	</span>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-sm-4">
                                                                                <div class="list">
                                                                                    <div class="list-item">
                                                                                        <div class="text"><span
                                                                                                    class="black">Ship Type</span>
                                                                                            :
                                                                                            <span class="blue">
		                                                            		@foreach( CommonHelper::ship_type() as $c_index => $type)
                                                                                                    {{ isset($services['ship_type']) ? $services['ship_type'] == $c_index ? $type : '' : ''}}
                                                                                                @endforeach
		                                                            	</span>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="list-item">
                                                                                        <div class="text"><span
                                                                                                    class="black">Engine Type</span>
                                                                                            :
                                                                                            <span class="blue">
		                                                            		@if(isset($services['engine_type']) && !empty($services['engine_type']))
                                                                                                    @if(isset($services['engine_type']) && $services['engine_type'] == '11')
                                                                                                        {{ isset($services['other_engine_type']) ? $services['other_engine_type'] : '-'}}
                                                                                                    @else
                                                                                                        @foreach( CommonHelper::engine_type() as $c_index => $type)
                                                                                                            {{ isset($services['engine_type']) ? $services['engine_type'] == $c_index ? $type  : '' : ''}}
                                                                                                        @endforeach
                                                                                                    @endif
                                                                                                @else
                                                                                                    -
                                                                                                @endif
		                                                            	</span>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="list-item">
                                                                                        <div class="text"><span
                                                                                                    class="black">Manning By</span>
                                                                                            :
                                                                                            <span class="blue">
		                                                            		@if(isset($services['manning_by']) && !empty($services['manning_by']))
                                                                                                    {{$services['manning_by']}}
                                                                                                @else
                                                                                                    -
                                                                                                @endif
		                                                            	</span>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row">
                                                                            <hr>
                                                                        </div>
                                                                    @endforeach
                                                                @else
                                                                    <div class="row" style="margin-top:15px;">
                                                                        <div class="col-sm-12 ">
                                                                            <div class="list">
                                                                                <div class="list-item">
                                                                                    <div class="text ">
                                                                                        No Data Found
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                @endif
                                                            </div>
                                                        </div>
                                                        <div role="tabpanel" class="tab-pane" id="cd">
                                                            <div class="content">
                                                                <div class="row">
                                                                    <div class="col-lg-12">
                                                                        <div class="content-title"
                                                                             style="border-bottom:1px solid #a9b6cb;">
                                                                            <div class="d-flex block-small">
                                                                                <div class="d-flex--one">
                                                                                    <span class="dark-blue title">COURSE DETAILS</span>
                                                                                </div>
                                                                                @if($user != 'another_user')
                                                                                    <div class="d-flex--two">
                                                                                        <button href="{{route('site.seafarer.edit.profile')}}"
                                                                                                class="btn cs-primary-btn edit_link"
                                                                                                data-form='courses'><i
                                                                                                    class="fa fa-pencil-square-o">
                                                                                                Edit</i></button>
                                                                                    </div>
                                                                                @endif
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                @if(isset($data[0]['course_detail']) AND !empty($data[0]['course_detail']))
                                                                    <?php $normal_course_count = 1; ?>
                                                                    @foreach($data[0]['course_detail'] as $index => $courses)
                                                                        <div class="row" style="margin-top:15px;">
                                                                            <div class="col-sm-12">
                                                                                <div style="font-weight:bold;">
                                                                                    @foreach( CommonHelper::courses() as $c_index => $course)
                                                                                        {{ isset($courses['course_id']) ? $courses['course_id'] == $c_index ? $course : '' : ''}}
                                                                                    @endforeach
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-sm-4">
                                                                                <div class="list">
                                                                                    <div class="list-item">
                                                                                        <div class="text"><span
                                                                                                    class="black">Certificate Number</span>
                                                                                            :
                                                                                            <span class="blue">
		                                                            		{{ isset($courses['certification_number']) && !empty($courses['certification_number'])? $courses['certification_number'] : '-'}}
		                                                            	</span>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="list-item">
                                                                                        <div class="text"><span
                                                                                                    class="black">Issued By</span>
                                                                                            :
                                                                                            <span class="blue">
		                                                            		{{ isset($courses['issue_by']) && !empty($courses['issue_by']) ? $courses['issue_by'] : '-'}}
		                                                            	</span>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-sm-4">
                                                                                <div class="list">
                                                                                    <div class="list-item">
                                                                                        <div class="text"><span
                                                                                                    class="black">Date Of Issue</span>
                                                                                            :
                                                                                            <span class="blue">
		                                                            		{{ isset($courses['issue_date']) && !empty($courses['issue_date']) ? date('d-m-Y',strtotime($courses['issue_date'])) : '-'}}
		                                                            	</span>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="list-item">
                                                                                        <div class="text"><span
                                                                                                    class="black">Date Of Expiry</span>
                                                                                            :
                                                                                            <span class="blue">
		                                                            		{{ isset($courses['expiry_date']) && !empty(isset($courses['expiry_date'])) ? date('d-m-Y',strtotime($courses['expiry_date'])) : '-'}}
		                                                            	</span>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-sm-4">
                                                                                <div class="list">

                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row">
                                                                            <hr>
                                                                        </div>
                                                                        <?php $normal_course_count++; ?>
                                                                    @endforeach
                                                                @else
                                                                    <div class="row" style="margin-top:15px;">
                                                                        <div class="col-sm-12 ">
                                                                            <div class="list">
                                                                                <div class="list-item">
                                                                                    <div class="text ">
                                                                                        No Data Found
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                @endif

                                                                <div class="row">
                                                                    <div class="col-lg-12">
                                                                        <div class="content-title"
                                                                             style="border-bottom:1px solid #a9b6cb;">
                                                                            <div class="d-flex block-small">
                                                                                <div class="d-flex--one">
                                                                                    <span class="dark-blue title">ADD-ON COURSE DETAILS</span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                @if(isset($data[0]['value_added_course_detail']) AND !empty($data[0]['value_added_course_detail']))
                                                                    @foreach($data[0]['value_added_course_detail'] as $index => $courses)
                                                                        <div class="row" style="margin-top:15px;">
                                                                            <div class="col-sm-12">
                                                                                <div style="font-weight:bold;">
                                                                                    CERTIFICATE {{$index+1}}</div>
                                                                            </div>
                                                                            <div class="col-sm-4">
                                                                                <div class="list">
                                                                                    <div class="list-item">
                                                                                        <div class="text"><span
                                                                                                    class="black">Certificate Name</span>
                                                                                            :
                                                                                            <span class="blue">
		                                                            		@foreach( CommonHelper::value_added_courses() as $c_index => $course)
                                                                                                    {{ isset($courses['course_id']) ? $courses['course_id'] == $c_index ? $course : '' : ''}}
                                                                                                @endforeach
		                                                            	</span>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="list-item">
                                                                                        <div class="text"><span
                                                                                                    class="black">Date Of Expiry</span>
                                                                                            :
                                                                                            <span class="blue">
		                                                            		{{ isset($courses['expiry_date']) && !empty(isset($courses['expiry_date'])) ? date('d-m-Y',strtotime($courses['expiry_date'])) : '-'}}
		                                                            	</span>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-sm-4">
                                                                                <div class="list">
                                                                                    <div class="list-item">
                                                                                        <div class="text"><span
                                                                                                    class="black">Certificate Number</span>
                                                                                            :
                                                                                            <span class="blue">
		                                                            		{{ isset($courses['certification_number']) && !empty($courses['certification_number'])? $courses['certification_number'] : '-'}}
		                                                            	</span>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="list-item">
                                                                                        <div class="text"><span
                                                                                                    class="black">Issued By</span>
                                                                                            :
                                                                                            <span class="blue">
		                                                            		{{ isset($courses['issue_by']) && !empty($courses['issue_by']) ? $courses['issue_by'] : '-'}}
		                                                            	</span>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-sm-4">
                                                                                <div class="list">
                                                                                    <div class="list-item">
                                                                                        <div class="text"><span
                                                                                                    class="black">Date Of Issue</span>
                                                                                            :
                                                                                            <span class="blue">
		                                                            		{{ isset($courses['issue_date']) && !empty($courses['issue_date']) ? date('d-m-Y',strtotime($courses['issue_date'])) : '-'}}
		                                                            	</span>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row">
                                                                            <hr>
                                                                        </div>
                                                                    @endforeach
                                                                @else
                                                                    <div class="row" style="margin-top:15px;">
                                                                        <div class="col-sm-12 ">
                                                                            <div class="list">
                                                                                <div class="list-item">
                                                                                    <div class="text ">
                                                                                        No Data Found
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                @endif

                                                                @if(isset($data[0]['professional_detail']['other_exp']) && !empty($data[0]['professional_detail']['other_exp']))
                                                                    <div class="row">
                                                                        <div class="col-lg-12">
                                                                            <div class="content-title"
                                                                                 style="border-bottom:1px solid #a9b6cb;">
                                                                                <div class="d-flex block-small">
                                                                                    <div class="d-flex--one">
                                                                                        <span class="dark-blue title">OTHER EXPERIENCE</span>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-sm-12">
                                                                            <div class="list">
                                                                                <div class="list-item">
                                                                                    <div class="text">
                                                                                        {{$data[0]['professional_detail']['other_exp']}}
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                @endif
                                                            </div>
                                                        </div>
                                                        <div role="tabpanel" class="tab-pane" id="user_document">


                                                            <?php
                                                            $download_all_path = route('site.user.get.documents.path', ['user_id' => $data[0]['id']]);
                                                            $current_route = Route::currentRouteName();
                                                            ?>

                                                            @if(isset($registered_as) && ($registered_as != 'advertiser'))
                                                                <div class="tab-content">
                                                                    <div id="upload_documents"
                                                                         class="tab-pane fade in active">
                                                                        @if($current_route == 'user.view.profile')
                                                                            <input type="hidden" class="profile_status"
                                                                                   value="view profile">

                                                                        @endif

                                                                        @if($current_route == 'user.profile')
                                                                            <input type="hidden" class="profile_status"
                                                                                   value="own profile">
                                                                            <div class="documents_section">

                                                                            </div>
                                                                        @endif
                                                                    </div>

                                                                </div>
                                                            @endif
                                                        </div>
                                                        <div role="tabpanel" class="tab-pane" id="notifi">
                                                            <div class="row" style="margin-top:15px;">
                                                                <div class="col-sm-12 ">
                                                                    <div class="list">
                                                                        <div class="list-item">
                                                                            <div class="text ">
                                                                                No Notifications Found
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div role="tabpanel" class="tab-pane" id="td">
                                                            <div class="content">
                                                                <div class="row">
                                                                    <div class="col-lg-12">
                                                                        <div class="content-title"
                                                                             style="border-bottom:1px solid #a9b6cb;">
                                                                            <div class="d-flex block-small">
                                                                                <div class="d-flex--one">
                                                                                    <span class="dark-blue title">Tracking Details</span>
                                                                                </div>
                                                                                <div class="d-flex--two">
                                                                                    <a href=""
                                                                                       class="btn cs-primary-btn">Edit</a>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row" style="margin-top:15px;">
                                                                    <div class="col-sm-6">
                                                                        <div style="font-weight:bold;">Course Details
                                                                        </div>
                                                                        <div class="list">
                                                                            <div class="list-item">
                                                                                <div class="text"><span class="black">Institute Name</span>
                                                                                    :<span class="blue"></span></div>
                                                                            </div>
                                                                            <div class="list-item">
                                                                                <div class="text"><span class="black">Course Type</span>
                                                                                    :<span class="blue"></span></div>
                                                                            </div>
                                                                            <div class="list-item">
                                                                                <div class="text"><span class="black">Course Name</span>
                                                                                    :<span class="blue"></span></div>
                                                                            </div>
                                                                            <div class="list-item">
                                                                                <div class="text"><span class="black">Start Date</span>
                                                                                    :<span class="blue"></span></div>
                                                                            </div>
                                                                            <div class="list-item">
                                                                                <div class="text"><span class="black">Booking Date</span>
                                                                                    :<span class="blue"></span></div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-sm-6">
                                                                        <div style="font-weight:bold;">Payment Details
                                                                        </div>
                                                                        <div class="list">
                                                                            <div class="list-item">
                                                                                <div class="text"><span class="black">Booking Amount</span>
                                                                                    :<span class="blue"></span></div>
                                                                            </div>
                                                                            <div class="list-item">
                                                                                <div class="text"><span class="black">Tax</span>
                                                                                    :<span class="blue"></span></div>
                                                                            </div>
                                                                            <div class="list-item">
                                                                                <div class="text"><span class="black">Total Amount</span>
                                                                                    :<span class="blue"></span></div>
                                                                            </div>
                                                                            <div class="list-item">
                                                                                <div class="text"><span
                                                                                            class="black"></span> <span
                                                                                            class="blue"></span></div>
                                                                            </div>
                                                                            <div class="list-item">
                                                                                <div class="text"><span class="black">Order Status</span>
                                                                                    :<span class="green-label">Payment Success</span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <!-- <div class="row">
                                                                        <div class="col-lg-12">
                                                                            <ul>
                                                                                <li>Blocking</li>
                                                                                <li>Pending</li>
                                                                                <li>Approved</li>
                                                                                <li>
                                                                                    Payment Succesful
                                                                                </li>
                                                                                <li>Seat Confirmed</li>
                                                                            </ul>
                                                                        </div>
                                                                    </div> -->
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- /.container-fluid -->
                    </div>
                    <!-- /#page-wrapper -->
                </div>

            </div>
    </main>
@stop
@section('js_script')
    <script type="text/javascript" src="{{ URL:: asset('public/assets/js/site/registration.js')}}"></script>
    <script type="text/javascript" src="{{ URL:: asset('public/assets/js/site/company-registration.js')}}"></script>
@stop