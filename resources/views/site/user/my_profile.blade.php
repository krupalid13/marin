@extends('site.index')
@section('content')
<main class="main">
    <div class="my-profile">
        <div class="container">
            <div class="section">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="d-flex block-small">
                            <div class="d-flex--one">
                                <span class="dark-blue title">My Profile</span>
                            </div>
                            <div class="d-flex--two">
                                <button class="btn cs-secondary-btn">View Resume</button>
                                <button class="btn cs-primary-btn">Edit Profile</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-4">
                        <div class="card z-depth-1">
                            <div class="card-body top-borderradius">
                                <div class="logo" style="background-image:url('/images/uploads/institute_logo/1/1539626339-pic.png')"></div>   
                            </div>
                            <div class="card-footer">
                                <div class="logo-title">Sunil Chttopadhhya</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-8">
                        <div class="card">
                            <div class="card-header">
                                <div>User Details</div>
                            </div>
                            <div class="card-body z-depth-1">
                                <div>
                                    <!-- Nav tabs -->
                                    <ul class="nav nav-tabs course_list" role="tablist">                                                                                                                                                               
                                        <li role="presentation" class="course_list_item">
                                            <a href="#bd" aria-controls="bd" role="tab" data-toggle="tab">Basic Details</a>
                                        </li>
                                        <li role="presentation" class="course_list_item">
                                            <a href="#ld" aria-controls="ld" role="tab" data-toggle="tab">Location Details</a>
                                        </li>
                                        <li role="presentation" class="course_list_item active">
                                            <a href="#pd" aria-controls="pd" role="tab" data-toggle="tab">Professtional Details</a>
                                        </li>
                                    </ul>
                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane" id="bd">
                                            <div class="content">
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <div class="content-title">
                                                            <!-- <span class="orange-label"> </span> -->
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12">
                                                        <div class="list">                                                                                                                                    
                                                            <div class="list-item">
                                                                <div class="icon"><i class="blue icon-location-pin"></i></div>
                                                                <div class="text"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div role="tabpanel" class="tab-pane" id="ld">
                                            <div class="content">
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <div class="content-title">
                                                            <!-- <span class="orange-label"> </span> -->
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12">
                                                        <div class="list">                                                                                                                                    
                                                            <div class="list-item">
                                                                <div class="icon"><i class="blue icon-location-pin"></i></div>
                                                                <div class="text"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div role="tabpanel" class="tab-pane active" id="pd">
                                            <div class="content">
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <div class="list">                                                                                                                                    
                                                            <div class="list-item">
                                                                <div class="text">
                                                                    <span class="black-text">Current Rank</span> : <span class="blue">Chief Officer</span>
                                                                </div>
                                                            </div>
                                                            <div class="list-item">
                                                                <div class="text">
                                                                    <span class="black-text">Applied Bank</span> : <span class="blue">Chief Officer</span>
                                                                </div>
                                                            </div>
                                                            <div class="list-item">
                                                                <div class="text">
                                                                    <span class="black-text">Current Rank Experience</span> : <span class="blue">2 Years 6 Months</span>
                                                                </div>
                                                            </div>
                                                            <div class="list-item">
                                                                <div class="text">
                                                                    <span class="black-text">Date of Availability</span> : <span class="blue">13-11-2018</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header" style="padding:0;">
                                <ul class="nav nav-tabs course_list" role="tablist">                                                                                                                                                               
                                    <li role="presentation" class="course_list_item">
                                        <a href="#gi" aria-controls="gi" role="tab" data-toggle="tab">General Information</a>
                                    </li>
                                    <li role="presentation" class="course_list_item">
                                        <a href="#ssd" aria-controls="ssd" role="tab" data-toggle="tab">Sea Service Details</a>
                                    </li>
                                    <li role="presentation" class="course_list_item">
                                        <a href="#cd" aria-controls="cd" role="tab" data-toggle="tab">Course Details</a>
                                    </li>
                                    <li role="presentation" class="course_list_item">
                                        <a href="#udoc" aria-controls="udoc" role="tab" data-toggle="tab">User Documents</a>
                                    </li>
                                    <li role="presentation" class="course_list_item">
                                        <a href="#notifi" aria-controls="notifi" role="tab" data-toggle="tab">Notifications</a>
                                    </li>   
                                    <li role="presentation" class="course_list_item active">
                                        <a href="#td" aria-controls="td" role="tab" data-toggle="tab">Tracking Details</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="card-body z-depth-1">
                               <div class="tab-content">
                                   <div  role="tabpanel" class="tab-pane" id="gi">
                                       <div class="content">
                                           <div class="content-title"></div>
                                       </div>
                                   </div>
                                   <div  role="tabpanel" class="tab-pane" id="ssd"></div>
                                   <div  role="tabpanel" class="tab-pane" id="cd"></div>
                                   <div  role="tabpanel" class="tab-pane" id="udoc"></div>
                                   <div  role="tabpanel" class="tab-pane" id="notifi"></div>
                                   <div  role="tabpanel" class="tab-pane active" id="td">
                                       <div class="content">
                                           <div class="row">
                                               <div class="col-lg-12">
                                                    <div class="content-title" style="border-bottom:1px solid #a9b6cb;">
                                                        <div class="d-flex block-small">
                                                            <div class="d-flex--one">
                                                                <span class="dark-blue title">Tracking Details</span>
                                                            </div>
                                                            <div class="d-flex--two">
                                                                <a href="" class="btn cs-primary-btn">Edit</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                               </div>
                                           </div>
                                           <div class="row" style="margin-top:15px;">
                                                <div class="col-sm-6">
                                                    <div style="font-weight:bold;">Course Details</div>
                                                    <div class="list">
                                                        <div class="list-item">
                                                            <div class="text"><span class="black">Institute Name</span> :<span class="blue"></span> </div>
                                                        </div>
                                                        <div class="list-item">
                                                            <div class="text"><span class="black">Course Type</span> :<span class="blue"></span> </div>
                                                        </div>
                                                        <div class="list-item">
                                                            <div class="text"><span class="black">Course Name</span> :<span class="blue"></span> </div>
                                                        </div>
                                                        <div class="list-item">
                                                            <div class="text"><span class="black">Start Date</span> :<span class="blue"></span> </div>
                                                        </div>
                                                        <div class="list-item">
                                                            <div class="text"><span class="black">Booking Date</span> :<span class="blue"></span> </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div style="font-weight:bold;">Payment Details</div>
                                                    <div class="list">
                                                        <div class="list-item">
                                                            <div class="text"><span class="black">Booking Amount</span> :<span class="blue"></span> </div>
                                                        </div>
                                                        <div class="list-item">
                                                            <div class="text"><span class="black">Tax</span> :<span class="blue"></span> </div>
                                                        </div>
                                                        <div class="list-item">
                                                            <div class="text"><span class="black">Total Amount</span> :<span class="blue"></span> </div>
                                                        </div>
                                                        <div class="list-item">
                                                            <div class="text"><span class="black"></span> <span class="blue"></span> </div>
                                                        </div>
                                                        <div class="list-item">
                                                            <div class="text"><span class="black">Order Status</span> :<span class="green-label">Payment Success</span>
                                                        </div>
                                                    </div>
                                                </div>
                                           </div>
                                           <!-- <div class="row">
                                               <div class="col-lg-12">
                                                   <ul>
                                                       <li>Blocking</li>
                                                       <li>Pending</li>
                                                       <li>Approved</li>
                                                       <li>
                                                           Payment Succesful
                                                       </li>
                                                       <li>Seat Confirmed</li>
                                                   </ul>
                                               </div>
                                           </div> -->
                                       </div>
                                   </div>
                               </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
@stop