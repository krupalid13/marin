<?php $normal_course_count = 1; ?>
<div class="page-container">
    @if ($courseId == 'institute_degree' || $courseId == 'all')

    <div class="main-title">PRE SEA COURSE</div>
    <div class="outer-div">
        <div class="pre-sea-div">
            @if($userProData->institute_degree)
            <div class="col-pre-sea-1">
                <div class="col-label-pre-sea-deg">
                    <h4>Degree: </h4>
                </div>
                <div class="col-data-pre-sea-deg">
                    <h4>{{$userProData->institute_degree}}</h4>
                </div>
            </div>
            @endif
            @if($userProData->institute_name)
            <div class="col-pre-sea-2">
                <div class="col-label-pre-sea-insti">
                    <h4>Institute: </h4>
                </div>
                <div class="col-data-pre-sea-insti">
                    <h4>{{$userProData->institute_name}}</h4>
                </div>
            </div>
            @endif
            @if($userProData->institute_to)
            <div class="col-pre-sea-3">
                <div class="col-label-pre-sea-pass">
                    <h4>Pass Out: </h4>
                </div>
                <div class="col-data-pre-sea-pass">
                    <h4>{{$userProData->institute_to}}</h4>
                </div>
            </div>
            @endif
            <div class="col-pre-sea-4"></div>
        </div>
    </div>
    @endif 
    @if ($courseId !== 'institute_degree' || $courseId == 'all' )
        <div class="main-title">POST SEA COURSES</div>
        @if(isset($data[0]['course_detail']) AND !empty($data[0]['course_detail']) AND count($data[0]['course_detail']) != 0)
            @foreach($data[0]['course_detail'] as $index => $courses)
                <div class="outer-div">
                    <div class="course-div1">
                        <div class="col-course-row1-1">
                            <div class="col-label-course-name">
                                <h4>Course: </h4>
                            </div>
                            <div class="col-data-course-name">
                                <h4>{{$coursesName[$courses['course_id']]}}</h4>
                            </div>
                        </div>
                    </div>
                    <!-- course-div1 -->
                    <div class="course-div2">
                        <div class="col-course-row2-1">
                            <div class="col-label-course-issue">
                                <h4>Issued At: </h4>
                            </div>
                            <div class="col-data-course-issue">
                                <h4>{{ isset($courses['issue_by']) && !empty($courses['issue_by']) ? $courses['issue_by'] : '-'}}</h4>
                            </div>
                        </div>
                        <div class="col-course-row2-2">
                            <div class="col-label-course-number">
                                <h4>Number: </h4>
                            </div>
                            <div class="col-data-course-number">
                                <h4>{{ isset($courses['certification_number']) && !empty($courses['certification_number'])? $courses['certification_number'] : '-'}}</h4>
                            </div>
                        </div>
                        <div class="col-course-row2-3">
                            <div class="col-label-course-doi">
                                <h4>Issue Dt.: </h4>
                            </div>
                            <div class="col-data-course-doi">
                                <h4>{{ isset($courses['issue_date']) && !empty($courses['issue_date']) ? date('d-m-Y',strtotime($courses['issue_date'])) : '-'}}</h4>
                            </div>
                        </div>
                        <div class="col-course-row2-4">
                            <div class="col-label-course-doe">
                                <h4>Expire Dt.: </h4>
                            </div>
                            <div class="col-data-course-doe">
                                <h4>{{ isset($courses['expiry_date']) && !empty(isset($courses['expiry_date'])) ? date('d-m-Y',strtotime($courses['expiry_date'])) : 'Unlimited'}}</h4>
                            </div>
                        </div>
                    </div>
                    <!-- course-div2-->
                </div>
                <!-- outer-div -->
                <div class="outer-div-between"></div>

                <?php $normal_course_count++; ?>
            @endforeach
        @else
            <div class="row" style="margin-top:15px;">
                <div class="col-sm-12 ">
                    <div class="list">
                        <div class="list-item">
                            <div class="text ">
                                No Data Found
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    @endif
</div>