<html>
	<head>
		<style>
			table, th, td {
			    /* border: 1px solid black; */
			    border-collapse: collapse;
			    margin: 8px auto;
			    width: 100%;
			}
			.left {
				text-align: left;
			}
			th{
				padding: 10px;
				font-size: 13px;
				border: 1px solid black;
			}
			.border-class {
				border: 1px solid black;
			}
			.border-none {
				border: none;
			}
			.f-10 {
				font-size: 12px;
			}
			.center {
				text-align: center
			}
		</style>
	</head>
	<body>
		<table style="width: 700px;margin: 0 auto;">
			<tbody>	
				<tr>
					<td>
						<table style="width: 700px;margin: 0px;">
							<tbody>
								<tr>
									<td style="width: 70%;">
										<img src="{{asset('images/coss-logo.png')}}" height="60">
									</td>
									<th class="border-none" style="font-size: 25px;float: left;">
										<u>RESUME</u>
									</th>
								</tr>
							</tbody>
						</table>
					</td>
				</tr>
				<tr>
					<td>	
						<table cellpadding="5" style="border: 1px solid black;">
							<tbody>
								<tr>
									<th colspan="4">
										PERSONAL DETAILS
									</th>
								</tr>
								<?php
									if(isset($data['profile_pic'])){
								    	$img_path = env('SEAFARER_PROFILE_PATH').$data['id'].'/'.$data['profile_pic'];
									}
								?>
								<tr class="f-10">
									@if(isset($data['profile_pic']))
								    	<td class="border-class center" rowspan='2'>
									    		<img src="{{asset($img_path)}}" height="110" width="110">
									    </td>
						    		@endif
								    <td class="border-class"><b>Name:</b> 
								    	{{isset($data['first_name']) ? $data['first_name'] : ''}}
								    </td>
								    <td class="border-class"><b>Nationality:</b>
										@foreach( \CommonHelper::countries() as $c_index => $country)
				                            {{ $data['personal_detail']['country'] == $c_index ? $country : '' }}
				                        @endforeach
								    </td>
								    <td class="border-class"><b>Date Of Birth:</b>
								    {{isset($data['personal_detail']['dob']) ? date('d-m-Y',strtotime($data['personal_detail']['dob'])) : '' }}
								    </td>
								</tr>
								<tr class="f-10">
								    <td class="border-class"><b>Mobile:</b> 
										{{isset($data['mobile']) ? $data['mobile'] : ''}}
								    </td>
								    <td class="border-class" colspan="2"><b>Email Id:</b>
								    	{{isset($data['email']) ? $data['email'] : ''}}
								    </td>
								</tr>
							</tbody>
						</table>
					</td>
				</tr>

				<tr>
					<td>	
						<table cellpadding="5" style="border: 1px solid black;">
							<tbody>
								<tr>
									<th>Document</th>
									<th>Number</th>
									<th>Date Of Issue</th>
									<th>Place Of Issue</th>
									<th>Valid Till</th>
								</tr>
								<tr class="f-10">
								    <td class="border-class" style="width: 2%;">Passport</td>
								    <td class="border-class" style="width: 2%;">{{isset($data['passport_detail']['pass_number']) ? $data['passport_detail']['pass_number'] : ''}}</td>
								    <td class="border-class" style="width: 2%;">03/05/1993</td>
								    <td class="border-class" style="width: 2%;">
								    	@foreach( \CommonHelper::countries() as $c_index => $country)
											{{ isset($data['passport_detail']['pass_country']) ? $data['passport_detail']['pass_country'] == $c_index ? $country : '' : ''}}
										@endforeach
								    </td>
								    <td class="border-class" style="width: 2%;">
								    	{{ isset($data['passport_detail']['pass_expiry_date']) ? date('d-m-Y',strtotime($data['passport_detail']['pass_expiry_date'])) : ''}}
								    </td>
								</tr>
								@if(isset($data['seaman_book_detail']))
									@foreach($data['seaman_book_detail'] as $index => $cdc)
									<tr class="f-10">
									    <td class="border-class" style="width: 2%;">CDC Details {{$index+1}}</td>
									    <td class="border-class" style="width: 2%;">{{ isset($cdc['cdc_number']) ? $cdc['cdc_number'] : '-'}}</td>
									    <td class="border-class" style="width: 2%;"> - </td>
									    <td class="border-class" style="width: 2%;">
											@foreach( \CommonHelper::countries() as $c_index => $country)
												{{ isset($cdc['cdc']) ? $cdc['cdc'] == $c_index ? $country : '' : '-'}}
											@endforeach
									    </td>
									    <td class="border-class" style="width: 2%;">
									    	{{ isset($cdc['cdc_expiry_date']) ? date('d-m-Y',strtotime($cdc['cdc_expiry_date'])) : '-'}}
									    </td>
									</tr>
									@endforeach
								@endif
								<tr class="f-10">
								    <td class="border-class" colspan="2" style="width: 50%; " ><b>INDOS No:</b> 
								    	{{isset($data['wkfr_detail']['indos_number']) ? $data['wkfr_detail']['indos_number'] : '-'}}
								    </td>
								    <td class="border-class" style="width: 50%; " ><b>PCC:</b>
										{{isset($data['passport_detail']['indian_pcc']) ? ($data['passport_detail']['indian_pcc'] == '1') ? 'Yes' : 'No' : '-' }}
								    </td>
								    <td class="border-class" colspan="2" style="width: 50%; " ><b>PCC Issue Date:</b>
								    @if(isset($data['passport_detail']['indian_pcc']) && $data['passport_detail']['indian_pcc'] == '1')
										{{isset($data['passport_detail']['indian_pcc_issue_date']) ? date('d-m-Y',strtotime($data['passport_detail']['indian_pcc_issue_date'])) : '-' }}
								    @endif
								    </td>
								</tr>
							</tbody>
						</table>
					</td>
				</tr>

				<tr>
					<td>	
						<table cellpadding="5" style="border: 1px solid black;">
							<tbody>
								<tr>
									<th colspan="6">
										CERTIFICATE OF COMPETENCY/GMDSS/WATCH KEEPING
									</th>
								</tr>
								<tr>
									<th>Certificate Name</th>
									<th>Number</th>
									<th>Grade</th>
									<th>DOI</th>
									<th>DOE</th>
									<th>ISSUED BY</th>
								</tr>
								@if(isset($data['coc_detail']))
									@foreach($data['coc_detail'] as $index => $coc)
										<tr class="f-10">
										    <td class="border-class">COC Details {{$index+1}}</td>
										    <td class="border-class">{{ isset($coc['coc_number']) ? $coc['coc_number'] : ''}}</td>
										    <td class="border-class">{{ isset($coc['coc_grade']) ? $coc['coc_grade'] : ''}}</td>
										    <td class="border-class"> - </td>
										    <td class="border-class">
										    	{{ isset($coc['coc_expiry_date']) ? date('d-m-Y',strtotime($coc['coc_expiry_date'])) : ''}}
										    </td>
										    <td class="border-class">
												@foreach( \CommonHelper::countries() as $c_index => $country)
													{{ isset($coc['coc']) ? $coc['coc'] == $c_index ? $country : '' : ''}}
												@endforeach
										    </td>
										</tr>
									@endforeach
								@endif

								@if(isset($data['gmdss_detail']))
								<tr class="f-10">
								    <td class="border-class" style="width: 10%;">
								    	GMDSS
								    </td>
								    <td class="border-class" style="width: 2%;">
								    	{{isset($data['gmdss_detail']['gmdss_number']) ? $data['gmdss_detail']['gmdss_number'] : '-'}}
								    </td>
								    <td class="border-class" style="width: 2%;">-</td>
								    <td class="border-class" style="width: 2%;">-</td>
								    <td class="border-class" style="width: 2%;">
								    	{{isset($data['gmdss_detail']['gmdss_expiry_date']) ? date('d-m-Y',strtotime($data['gmdss_detail']['gmdss_expiry_date'])) : ''}}
								    </td>
								    <td class="border-class" style="width: 2%;">-</td>
								</tr>
								@endif

								@if(isset($data['wkfr_detail']))
								<tr class="f-10">
								    <td class="border-class" style="width: 10%;">
								    	Watch Keeping
								    </td>
								    <td class="border-class" style="width: 2%;">
								    	{{isset($data['wkfr_detail']['wkfr_number']) ? $data['wkfr_detail']['wkfr_number'] : '-'}}
								    </td>
								    <td class="border-class" style="width: 2%;">-</td>
								    <td class="border-class" style="width: 2%;">
								    	{{isset($data['wkfr_detail']['issue_date']) ? date('d-m-Y',strtotime($data['wkfr_detail']['issue_date'])) : ''}}
								    </td>
								    <td class="border-class" style="width: 2%;">-</td>
								    <td class="border-class" style="width: 2%;">-</td>
								</tr>
								@endif
							</tbody>
						</table>
					</td>
				</tr>
				
				<tr>
					<td>	
						<table cellpadding="6" style="border: 1px solid black;">
							<tbody>
								<tr>
									<th colspan="5">
										CERTIFICATE OF PROFICIENCY/CERTIFICATE OF ENDORSEMENT
									</th>
								</tr>
								<tr>
									<th>Name</th>
									<th>Number</th>
									<th>Grade</th>
									<th>DOI</th>
									<th>DOE</th>
								</tr>
								@if(isset($data['cop_detail']))
									@foreach($data['cop_detail'] as $index => $cop)
										<tr class="f-10">
										    <td class="border-class" style="width: 8%;">COP Details {{$index+1}}</td>
										    <td class="border-class" style="width: 2%;">{{ isset($cop['cop_number']) ? $cop['cop_number'] : '-'}}</td>
										    <td class="border-class" style="width: 2%;">{{ isset($cop['cop_grade']) ? $cop['cop_grade'] : '-'}}</td>
										    <td class="border-class" style="width: 2%;">
										    	{{ isset($cop['cop_issue_date']) ? date('d-m-Y',strtotime($cop['cop_issue_date'])) : '-'}}
										    </td>
										    <td class="border-class" style="width: 2%;">
										    	{{ isset($cop['cop_exp_date']) ? date('d-m-Y',strtotime($cop['cop_exp_date'])) : '-'}}
										    </td>
										</tr>
									@endforeach
								@endif

								@if(isset($data['coe_detail']))
									@foreach($data['coe_detail'] as $index => $coe)
										<tr class="f-10">
										    <td class="border-class" style="width: 8%;">COE Details {{$index+1}}</td>
										    <td class="border-class" style="width: 2%;">{{ isset($coe['coe_number']) ? $coe['coe_number'] : '-'}}</td>
										    <td class="border-class" style="width: 2%;">{{ isset($coe['coe_grade']) ? $coe['coe_grade'] : '-'}}</td>
										    <td class="border-class" style="width: 2%;">-</td>
										    <td class="border-class" style="width: 2%;">
										    	{{ isset($coe['coe_expiry_date']) ? date('d-m-Y',strtotime($coe['coe_expiry_date'])) : '-'}}
										    </td>
										</tr>
									@endforeach
								@endif

							</tbody>
						</table>
					</td>
				</tr>

				<tr>
					<td>	
						<table cellpadding="5" style="border: 1px solid black;">
							<tbody>
								<tr>
									<th colspan="9">
										SEA SERVICE RECORD
									</th>
								</tr>
								<tr>
									<th>Vessel Name</th>
									<th>Company</th>
									<th>Type</th>
									<th>GRT</th>
									<th>Engine</th>
									<th>BHP</th>
									<th>Rank</th>
									<th>Service From</th>
									<th>Service To</th>
								</tr>
								@foreach($data['sea_service_detail'] as $service_index => $services)
									<tr class="f-10">
									    <td class="border-class" style="width: 15%;">
									    	{{ isset($services['ship_name']) ? $services['ship_name'] : ''}}
									    </td>
									    <td class="border-class" style="width: 2%;">
											{{ isset($services['company_name']) ? $services['company_name'] : ''}}
									    </td>
									    <td class="border-class" style="width: 15%;">
									    	@foreach( \CommonHelper::ship_type() as $c_index => $type)
												{{ isset($services['ship_type']) ? $services['ship_type'] == $c_index ? $type : '' : ''}}
											@endforeach
									    </td>
									    <td class="border-class" style="width: 2%;">{{ isset($services['grt']) ? $services['grt'] : ''}}</td>
									    <td class="border-class" style="width: 2%;">
									    	@foreach( \CommonHelper::engine_type() as $e_index => $engine_type)
												{{ isset($services['engine_type']) ? $services['engine_type'] == $e_index ? $engine_type : '' : ''}}
											@endforeach
									    </td>
									    <td class="border-class" style="width: 2%;">{{ isset($services['bhp']) ? $services['bhp'] : ''}}</td>
									    <td class="border-class">
									    	@foreach(\CommonHelper::new_rank() as $rank_index => $category)
										    	@foreach( $category as $c_index => $type)
													{{ isset($services['rank_id']) ? $services['rank_id'] == $c_index ? $type : '' : ''}}
												@endforeach
											@endforeach
									    </td>
									    <td class="border-class">{{ isset($services['from']) ? date('d-m-Y',strtotime($services['from'])) : ''}}</td>
									    <td class="border-class" >{{ isset($services['to']) ? date('d-m-Y',strtotime($services['to'])) : ''}}</td>
									</tr>
								@endforeach
							</tbody>
						</table>
					</td>
				</tr>

				<tr>
					<td>	
						<table cellpadding="5" style="border: 1px solid black;">
							<tbody>
								<tr>
									<th colspan="6">
										COURSE AND CERTIFICATES
									</th>
								</tr>
								<tr>
									<th>Course Title</th>
									<th>Date Of Issue</th>
									<th>Certificate Number</th>
									<th>Issue By</th>
									<th>Issuing Authority</th>
									<th>Place Of Issue</th>
								</tr>
								@foreach($data['course_detail'] as $index => $courses)
									<tr class="f-10">
									    <td class="border-class" style="width: 2%;">
									    	@foreach( \CommonHelper::courses() as $c_index => $course)
												{{ isset($courses['course_id']) ? $courses['course_id'] == $c_index ? $course : '' : '-'}}
											@endforeach
									    </td>
									    <td class="border-class" style="width: 2%;">
									    	{{ isset($courses['issue_date']) ? date('d-m-Y',strtotime($courses['issue_date'])) : ''}}
									    </td>
									    <td class="border-class" style="width: 2%;">
									    	{{ isset($courses['certification_number']) ? $courses['certification_number'] : ''}}
									    </td>
									    <td class="border-class" style="width: 2%;">
									    	@foreach( \CommonHelper::certificate_issued_by() as $c_index => $issued_by)
												{{ isset($courses['issue_by']) ? $courses['issue_by'] == $c_index ? $issued_by : '' : ''}}
											@endforeach
										</td>
										<td class="border-class" style="width: 2%;">
											@foreach( \CommonHelper::issuing_authority() as $c_index => $authority)
												{{ isset($courses['issuing_authority']) ? $courses['issuing_authority'] == $c_index ? $authority : '' : ''}}
											@endforeach
									    </td>
									    <td class="border-class" style="width: 2%;">
										@foreach( \CommonHelper::countries() as $c_index => $country)
											{{ isset($courses['place_of_issue']) ? $courses['place_of_issue'] == $c_index ? $country : '' : ''}}
										@endforeach
								    </td>
									</tr>
								@endforeach
							</tbody>
						</table>
					</td>
				</tr>

				<tr class="f-10">
					<td style="padding: 5px;">
						Last wages drawn : ________________________ Date of Availability : ________________________ Signature: _______________________
					</td>
				</tr>

				<tr class="f-10">
					<td style="padding: 5px;">
						Remarks : _________________________________________________________________________________________________________
					</td>
				</tr>

			</tbody>

		</table>
	</body>
</html>