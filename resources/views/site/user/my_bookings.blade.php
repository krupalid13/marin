@extends('site.index')

@section('content')

<div class="candidate-search content-section-wrapper sm-filter-space" style="padding: 0;">
    <!-- <button type="button" class="btn candidate-search-modal-btn" data-toggle="modal" data-target="#candidate-search-filter-modal"><i class="fa fa-filter" aria-hidden="true"></i></button> -->
    <div class="row">
        <div class="col-md-12" id="main-data">
            <?php $orders = $data->toArray(); ?>
            <div class="candidate-search-container container" style="padding-top: 70px;">
                @if(isset($orders['data']) && !empty($orders['data']))

                <div class="section-2" id="filter-results">
                    <span class="heading">
                        My Bookings
                    </span>
                    <span class="pagi pagi-up">
                        <span class="search-count">
                            Showing {{$orders['from']}} - {{$orders['to']}} of {{$orders['total']}} Bookings
                        </span>
                        <nav> 
                            <ul class="pagination pagination-sm">
                                {!! $data->render() !!}
                            </ul> 
                        </nav>
                    </span>

                    @foreach($orders['data'] as $order_details)

                    <div class="search-result-card">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="company-discription">
                                    <div class="company-name">
                                        {{isset($order_details['first_name']) ? ucfirst($order_details['first_name'])." ".ucfirst($order_details['last_name']) : ''}}
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div class="other-discription">
                                                Institute Name: 
                                                <span class="ans">
                                                    {{ isset($order_details['batch_details']['course_details']['institute_registration_detail']['institute_name']) ? $order_details['batch_details']['course_details']['institute_registration_detail']['institute_name'] : '-'}}
                                                </span>
                                            </div>
                                            <div class="other-discription">
                                                Course Type: 
                                                <span class="ans">
                                                    @foreach(\CommonHelper::institute_course_types() as $r_index => $rank)
                                                    {{ isset($order_details['batch_details']['course_details']['course_type']) ? $order_details['batch_details']['course_details']['course_type'] == $r_index ? $rank : '' : ''}}
                                                    @endforeach
                                                </span>
                                            </div>
                                            <div class="other-discription">
                                                Course Name: 
                                                <span class="ans">
                                                    {{ isset($order_details['batch_details']['course_details']['course_details']['course_name'] ) ? $order_details['batch_details']['course_details']['course_details']['course_name'] : '-'}}
                                                </span>
                                            </div>
                                            <div class="other-discription">
                                                Start Date: 
                                                <span class="ans">
                                                    {{isset($order_details['batch_details']['start_date']) ? date('d-m-Y',strtotime($order_details['batch_details']['start_date'])) : '-'}}
                                                </span>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">

                                            <div class="other-discription">
                                                Booking Date: 
                                                <span class="ans">
                                                    {{isset($order_details['created_at']) ? date('d-m-Y H:i',strtotime('+5 hour +30 minutes',strtotime($order_details['created_at']))) : '-'}}
                                                </span>
                                            </div>
                                            <div class="other-discription">
                                                Booking Amount: 
                                                <span class="ans">
                                                    <i class="fa fa-inr"></i>
                                                    {{isset($order_details['subscription_cost']) ? $order_details['subscription_cost'] : '-'}}
                                                </span>
                                            </div>
                                            <div class="other-discription">
                                                Tax: 
                                                <span class="ans">
                                                    <i class="fa fa-inr"></i>
                                                    {{isset($order_details['tax_amount']) ? $order_details['tax_amount'] : '-'}}
                                                </span>
                                            </div>
                                            <div class="other-discription">
                                                Total Amount: 
                                                <span class="ans">
                                                    <i class="fa fa-inr"></i>
                                                    {{isset($order_details['total']) ? $order_details['total'] : '-'}}
                                                </span>
                                            </div>
                                            <div class="other-discription">
                                                Order Status: 
                                                <?php
                                                $b_status = isset($order_details['status']) ? $order_details['status'] : '';
                                                $last_order = last($order_details['order_history']);
                                                ?>
                                                <span class="ans">
                                                    @foreach(\CommonHelper::order_status() as $s_index => $status)
                                                    {{ isset($order_details['status']) ? $order_details['status'] == $s_index ? $status : '' : ''}}
                                                    @endforeach
                                                    @if($b_status == '5')
                                                    {{ isset($last_order['reason']) && !empty($last_order['reason']) ? " - ".$last_order['reason'] : '' }}
                                                    @endif
                                                </span>
                                            </div>
                                        </div>
                                        @if(isset($order_details['discount']) && !empty($order_details['discount']))
                                        <div class="col-sm-4">
                                            <div class="other-discription">
                                                Discount Applied: 
                                                <span class="ans">
                                                    {{isset($order_details['discount']) ? $order_details['discount'] : '-'}}%
                                                </span>
                                            </div>
                                        </div>
                                        @endif
                                        @if($order_details['status'] == '6')
                                        <div class="col-sm-4">
                                            <div class="other-discription">
                                                Initital Payment Made: 
                                                <span class="ans">
                                                    <i class="fa fa-inr"></i>
                                                    {{isset($order_details['order_payments'][0]['amount']) ? $order_details['order_payments'][0]['amount'] : '-'}}
                                                </span>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="other-discription">
                                                Pending Payment: 
                                                <span class="ans">
                                                    <i class="fa fa-inr"></i>
                                                    {{isset($order_details['order_payments'][0]['amount']) && isset($order_details['total']) ? $order_details['total'] - $order_details['order_payments'][0]['amount'] : '-'}}
                                                </span>
                                            </div>
                                        </div>
                                        @endif
                                        @if($order_details['status'] == '1')
                                        @if(isset($order_details['order_payments'][1]['amount']))
                                        <div class="col-sm-4">
                                            <div class="other-discription">
                                                Initital Payment Made: 
                                                <span class="ans">
                                                    <i class="fa fa-inr"></i>
                                                    {{isset($order_details['order_payments'][1]['amount']) ? $order_details['order_payments'][1]['amount'] : '-'}}
                                                </span>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="other-discription">
                                                Pending Payment Made: 
                                                <span class="ans">
                                                    <i class="fa fa-inr"></i>
                                                    {{isset($order_details['order_payments'][0]['amount']) ? $order_details['order_payments'][0]['amount'] : '-'}}
                                                </span>
                                            </div>
                                        </div>
                                        @endif
                                        <div class="col-sm-4">
                                            <div class="other-discription">
                                                Total Payment Made: 
                                                <span class="ans">
                                                    <i class="fa fa-inr"></i>
                                                    {{isset($order_details['total']) ? $order_details['total'] : '-'}}
                                                </span>
                                            </div>
                                        </div>
                                        @endif
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-10">
                                            <div class="status-block">
                                                <div class="status">
                                                    <div class="track-status">
                                                        <span class="green-dot"></span>

                                                        <span class="green-status-line"></span>
                                                    </div>
                                                    <div class="status-text-line">
                                                        <span class="status-text"> 
                                                            Blocked
                                                            <br>
                                                            <span class="status-date">
                                                                {{isset($order_details['created_at']) ? date('d-m-Y H:i',strtotime('+5 hour +30 minutes',strtotime($order_details['created_at']))) : '-'}}
                                                            </span>
                                                        </span>
                                                    </div>
                                                </div>
                                                @foreach($order_details['order_history'] as $my_orders)
                                                <div class="status">
                                                    <div class="track-status">
                                                        <span class="green-dot"></span>

                                                        <span class="green-status-line"></span>
                                                    </div>
                                                    <div class="status-text-line">
                                                        <span class="status-text"> 
                                                            @foreach(\CommonHelper::order_status() as $s_index => $status)
                                                            {{ isset($my_orders['to_status']) ? $my_orders['to_status'] == $s_index ? $status : '' : ''}}
                                                            @endforeach
                                                            <br>
                                                            @if($my_orders['to_status'] != '0')
                                                            <span class="status-date">
                                                                {{isset($my_orders['created_at']) ? date('d-m-Y H:i',strtotime('+5 hour +30 minutes',strtotime($my_orders['created_at']))) : '-'}}
                                                            </span>
                                                            @endif
                                                        </span>
                                                    </div>
                                                </div>
                                                @endforeach
                                                @if($order_details['status'] == '1')
                                                <div class="status">
                                                    <div class="track-status">
                                                        <span class="green-dot"></span>

                                                        <span class="green-status-line"></span>
                                                    </div>
                                                    <div class="status-text-line">
                                                        <span class="status-text"> 
                                                            Seat Confirmed
                                                            <br>
                                                            <span class="status-date">
                                                                {{isset($order_details['updated_at']) ? date('d-m-Y H:i',strtotime('+5 hour +30 minutes',strtotime($order_details['updated_at']))) : '-'}}
                                                            </span>
                                                        </span>
                                                    </div>
                                                </div>
                                                @endif
                                            </div>
                                        </div>
                                        @if($order_details['status'] == '4' )
                                        <div class="col-sm-2">
                                            <a href="{{route('site.seafarer.course.book',['batch_id' => $order_details['batch_id'],'location_id' => $order_details['batch_details']['batch_location'][0]['location_id']])}}">
                                                <button type="button" data-style="zoom-in" class="btn coss-primary-btn ladda-button pull-right make-payment">
                                                    Make Payment</button>
                                            </a>
                                        </div>
                                        @endif
                                        @if($order_details['status'] == '6' )
                                        <div class="col-sm-2">
                                            <a href="{{route('site.seafarer.course.book',['batch_id' => $order_details['batch_id'],'location_id' => $order_details['batch_details']['batch_location'][0]['location_id']])}}">
                                                <button type="button" data-style="zoom-in" class="btn coss-primary-btn ladda-button pull-right make-payment">
                                                    Make Full Payment</button>
                                            </a>
                                        </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach

                    <span class="pagi m-t-25" style="padding-bottom: 70px;">
                        <span class="search-count">
                            Showing {{$orders['from']}} - {{$orders['to']}} of {{$orders['total']}} Candidates
                        </span>
                        <nav> 
                            <ul class="pagination pagination-sm">
                                {!! $data->render() !!}
                            </ul> 
                        </nav>
                    </span>

                    @else
                    <div class="section-2" id="filter-results"><div class="row no-results-found">You have not booked any course. </div></div>
                    <div class="mt-30 panel panel-bricky">
                        <div class="panel-heading text-center">
                            <h4 class="h4">Due to the Covid19 crisis we have stopped course booking until further notice.</h4>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <img class="img-responsive center-block graph-img" src="{{ URL:: asset('public/images/course_bookings.png')}}">
                    </div>
                    @endif

                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="candidate-search-filter-modal" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title text-center">Candidate search filter</h4>
            </div>
            <div class="modal-body">
                <form id="candidate-filter-form" action={{route('site.company.candidate.search')}}>
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label>Rank </label>
                                <select id="rank" name="rank" class="form-control">
                                    <option value=>Select</option>
                                    @foreach(\CommonHelper::new_rank() as $rank_index => $category)
                                    <optgroup label="{{$rank_index}}">
                                        @foreach($category as $r_index => $rank)
                                        <option value="{{$r_index}}" {{ isset($filter['rank']) ? $filter['rank'] == $r_index ? 'selected' : '' : ''}}>{{$rank}} </option>
                                        @endforeach
                                        @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="col-sm-3">
                            <div class="form-group">
                                <label>Ship Type</label>
                                <select class="form-control" name="ship_type" id="ship_type">
                                    <option value=>Select ship type</option>
                                    @foreach(\CommonHelper::ship_type() as $r_index => $ship_type)
                                    <option value="{{$r_index}}" {{ !empty($filter['ship_type']) ? $filter['ship_type'] == $r_index ? 'selected' : '' : ''}}>{{$ship_type}} </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label>Rank Experience</label>
                                <select class="form-control" id="rank_exp_from" name="rank_exp_from">
                                    <option value=''>Select Rank Experience</option>
                                    <option value='0-1'>0-1</option>
                                    <option value='1-2'>1-2</option>
                                    <option value='2-3'>2-3</option>
                                    <option value='3+'>3+</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label>Nationality</label>
                                <select class="form-control" id="nationality" name="nationality">
                                    <option value="">Select Nationality</option>
                                    @foreach( \CommonHelper::countries() as $c_index => $country)
                                    <option value="{{ $c_index }}"> {{ $country }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label>COC</label>
                                <select class="form-control" id="coc" name="coc">
                                    <option value="">Select COC</option>
                                    @foreach( \CommonHelper::countries() as $c_index => $country)
                                    <option value="{{ $c_index }}"> {{ $country }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div><label>Availability</label></div>
                            <div class="start">
                                <div class="form-group">

                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></div>
                                        <input type="text" class="form-control" name='availability_from' id="exampleInputAmount" placeholder="From Date">
                                    </div>
                                </div>
                            </div>
                            <div class="to">
                                To
                            </div>
                            <div class="end">
                                <div class="form-group">

                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></div>
                                        <input type="text" class="form-control" name='availability_to' id="exampleInputAmount" placeholder="To Date">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label>CDC</label>
                                <select class="form-control" id="cdc" name="cdc">
                                    <option value="">Select CDC</option>
                                    @foreach( \CommonHelper::countries() as $c_index => $country)
                                    <option value="{{ $c_index }}"> {{ $country }}</option>
                                    @endforeach
                                </select>   
                            </div>
                        </div>
                        <!-- <div class="col-sm-3">
                            <div class="form-group">
                                <label>COP</label>
                                <select class="form-control" id="cop" name="cop">
                                    <option value="">Select COP</option>
                                    @foreach( \CommonHelper::countries() as $c_index => $country)
                                        <option value="{{ $c_index }}"> {{ $country }}</option>
                                    @endforeach
                                </select>   
                            </div>
                        </div> -->
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label>Watch Keeping</label>
                                <select class="form-control" id="watch_keeping" name="watch_keeping">
                                    <option value="">Select Watch Keeping</option>
                                    @foreach( \CommonHelper::countries() as $c_index => $country)
                                    <option value="{{ $c_index }}"> {{ $country }}</option>
                                    @endforeach
                                </select>   
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label>Date Of Availability From</label>
                                <input type="text" name="availability_from" class="form-control datepicker" placeholder="dd-mm-yyyy" value="{{isset($filter['availability_from']) ? $filter['availability_from'] : ''}}"> 
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label>Date Of Availability To</label>
                                <input type="text" name="availability_to" class="form-control datepicker" placeholder="dd-mm-yyyy" value="{{isset($filter['availability_to']) ? $filter['availability_to'] : ''}}"> 
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="section_cta text-right m_t_25 job-btn-container">
                                <button type="button" class="btn coss-inverse-btn search-reset-btn" id="jobResetButton">Reset</button>
                                <button type="button" data-style="zoom-in" class="btn coss-primary-btn search-post-btn ladda-button candidate-filter-search-button" id="candidate-filter-search-button"><i class="fa fa-search search-icon" aria-hidden="true"></i>Search</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>

<script type="application/javascript">

    $(document).on('ready',function () {
    $('.switch-checkbox').checkboxpicker();

    var History = window.History;
    var state = History.getState(),
    $log = $("#log");

    History.pushState(null,null,state.url);

    History.Adapter.bind(window, 'statechange', function(){
    var state = History.getState();
    fetchCandidateResult(state.url);

    $('input[name="passport"]').closest('.switch-checkbox-container').find('a[is="1"]').removeClass('btn-default').addClass('btn-danger active');

    $('input[name="passport"]').closest('.switch-checkbox-container').find('a[is="0"]').addClass('btn-default').removeClass('btn-success active');

    var temp_current_state_url = state.url.split('?');
    if(temp_current_state_url.length > 1 && temp_current_state_url[1] != ''){
    var parameters = temp_current_state_url[1].split('&');

    var dropdown_elements = ['rank','ship_type','rank_exp_from','rank_exp_to','coc','cdc','nationality'];

    for(var i=0; i<parameters.length; i++){

    var param_array = parameters[i].split('=');
    if($.inArray(param_array[0],dropdown_elements) > -1){
    $('select[name="'+param_array[0]+'"]').val(param_array[1]);
    }else if($.inArray(param_array[0],['availability_from','availability_to','rank_exp_from']) > -1){
    $('input[name="'+param_array[0]+'"]').val(param_array[1]);
    }else if(param_array[0] == 'passport'){
    $('input[name="'+param_array[0]+'"]').closest('.switch-checkbox-container').find('a[is="0"]').removeClass('btn-default').addClass('btn-success active');

    $('input[name="'+param_array[0]+'"]').closest('.switch-checkbox-container').find('a[is="1"]').addClass('btn-default').removeClass('btn-danger active');
    }

    }
    } else {
    $(':input','#candidate-filter-form')
    .not(':button, :submit, :reset, :hidden')
    .val('')
    .removeAttr('checked')
    .removeAttr('selected');
    }
    });
    });


</script>
@stop

@section('js_script')
<script type="text/javascript" src="/js/site/dashboard.js"></script>
@stop