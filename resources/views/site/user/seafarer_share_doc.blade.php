@extends('site.index')
@section('content')
<?php
	$india_value = array_search('India',\CommonHelper::countries());
	if(isset($data[0]['professional_detail']['current_rank'])){
		$required_fields = \CommonHelper::rank_required_fields()[$data[0]['professional_detail']['current_rank']];
	}
	if(isset($data[0]['document_permissions']) && !empty($data[0]['document_permissions'])){
		$document_permissions = $data[0]['document_permissions'];
	}
	if(Auth::check()){
		$registered_as = Auth::user()->registered_as;
	}
?>
<main class="main">
	
    <div class="my-profile">
        <div class="container">
            <div class="section">
              
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header" style="padding:0;">
                                <ul class="nav nav-tabs course_list" role="tablist">                                                                                                                                                                                
                                    <li role="presentation" class="course_list_item">
                                        <a href="#user_document" aria-controls="udoc" role="tab" data-toggle="tab">User Documents</a>
                                    </li>
                                   
                                </ul>
                            </div>
                            <div class="card-body z-depth-1">
                               <div class="tab-content">
                                  
								   <div role="tabpanel" class="tab-pane" id="user_document">
                                		

										<?php
											$download_all_path = route('site.user.get.documents.path',['user_id' => $data[0]['id']]);
											$current_route = Route::currentRouteName();
										?>

										@if(isset($registered_as) && ($registered_as != 'advertiser'))
											<div class="tab-content">
												<div id="upload_documents" class="tab-pane fade in active">
													
													
													@if($current_route == 'site.user.share-file')
														<input type="hidden" class="profile_status" value="own profile">
														<div class="documents_section">

														</div>
													@endif
													
													
												</div>

											</div>
										@endif
                                   </div>
                                  
							</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
@stop
@section('js_script')
	<script type="text/javascript" src="{{ URL:: asset('public/assets/js/site/registration.js')}}"></script>
	<script type="text/javascript" src="{{ URL:: asset('public/assets/js/site/company-registration.js')}}"></script>
@stop