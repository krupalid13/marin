<style>
    a.expSpan {
        margin-left: 0;
        right: 0;
    }
</style>
<div class="page-container">
    <div class="main-title">SEA SERVICE</div>
    @if(isset($data[0]['sea_service_detail']) AND !empty($data[0]['sea_service_detail']) AND count($data[0]['sea_service_detail']) != 0)
        @foreach($data[0]['sea_service_detail'] as $index => $services)
            @php
                $exp = App\RateReview::where('user_sea_service_id',$services['id'])->value('experience');
            @endphp




            <div class="col-md-6 work-box d-none ship-data" id="{{'exp-'.$services['id']}}">
                <div class="header">{{ isset($services['ship_name']) ? $services['ship_name'] : '-'}}</div>
                <div class="bodyBox">
                    <p>{{$exp}}</p>
                </div>
            </div>

            <div class="outer-div">
                <div class="service-div1">
                    <div class="col-service-row1-1"></div>
                    <div class="col-service-row1-2"></div>
                    {{-- <div class="col-service-row1-3"></div> --}}
                    <div class="col-service-row1-4" style="width: 40%">
                        @if($exp)
                            <a href="javascript:void(0)" id="{{'pophoverexp-'.$services['id']}}" data-toggle="popover" title="{{ isset($services['ship_name']) ? $services['ship_name'] : '-'}}" data-placement="left" data-content="{{$exp}}" class="expSpan exp-data" data-id="{{'exp-'.$services['id']}}">Job Profile</a>
                        @else
                        &nbsp;
                        @endif
                    </div>
                </div>
                <!-- course-div1 -->
                <div class="service-div2">
                    <div class="col-service-row2-1">
                        <div class="col-label-service-rank">
                            <h4>Rank: </h4>
                        </div>
                        <div class="col-data-service-rank">
                            <h4>
                                @foreach(CommonHelper::new_rank() as $index => $category)
                                    @foreach($category as $r_index => $rank)
                                        {{ isset($services['rank_id']) ? $services['rank_id'] == $r_index ? $rank : '' : ''}}
                                    @endforeach
                                @endforeach
                            </h4>
                        </div>
                    </div>
                    <div class="col-service-row2-2">
                        <div class="col-label-service-son">
                            <h4>Sign-On: </h4>
                        </div>
                        <div class="col-data-service-son">
                            <h4>{{\Carbon\Carbon::parse($services['from'])->format('d-m-Y')}}</h4>
                        </div>
                    </div>
                    <div class="col-service-row2-3">
                        <div class="col-label-service-soff">
                            <h4>Sign-Off: </h4>
                        </div>
                        <div class="col-data-service-soff">
                            <h4>
                                @if(!empty($services['to']))
                                    {{\Carbon\Carbon::parse($services['to'])->format('d-m-Y')}}
                                @else
                                    On Board
                                @endif
                            </h4>
                        </div>
                    </div>
                    <div class="col-service-row2-4">
                        <div class="col-label-service-soff">
                            <h4>Onboard: </h4>
                        </div>
                        <div class="col-data-service-soff">
                            @php
                                $from = \Carbon\Carbon::createFromFormat('Y-m-d', $services['from']);
                                if(!empty($services['to'])){
                                    $to = \Carbon\Carbon::createFromFormat('Y-m-d', $services['to']);
                                }else{
                                    $to = date('Y-m-d');
                                    $to = date('Y-m-d', strtotime($to. ' + 1 days'));
                                }
                            @endphp
                            <h4>{{$from->diffInDays($to)+1}} Days
                                @if(empty($services['to']))
                                    ...
                                @endif
                            </h4>
                        </div>
                    </div>
                    <!-- created blank to align boxes -->
                </div>
                <!--service-div2-->
                <div class="service-div3">
                    <div class="col-service-row3-1">
                        <div class="col-label-service-ship">
                            <h4>Ship: </h4>
                        </div>
                        <div class="col-data-service-ship">
                            <h4>{{ isset($services['ship_name']) ? $services['ship_name'] : '-'}}</h4>
                        </div>
                    </div>
                    <div class="col-service-row3-2">
                        <div class="col-label-service-flag">
                            <h4>Flag: </h4>
                        </div>
                        <div class="col-data-service-flag">
                            <h4>{{ !empty($services['ship_flag']) ? $country[$services['ship_flag']] : '-'}}</h4>
                        </div>
                    </div>
                    <div class="col-service-row3-3">
                        <div class="col-label-service-owners">
                            <h4>Owners: </h4>
                        </div>
                        <div class="col-data-service-owners">
                            <h4>{{ !empty($services['company_name']) ? $services['company_name'] : '-'}}</h4>
                        </div>
                    </div>
                    <div class="col-service-row3-4">
                        <div class="col-label-service-manners">
                            <h4>Manning-By: </h4>
                        </div>
                        <div class="col-data-service-manners">
                            <h4>{{ !empty($services['manning_by']) ? $services['manning_by'] : '-'}}</h4>
                        </div>
                    </div>
                </div>
                <!--service-div3-->
                <div class="service-div4">
                    <div class="col-service-row4-1">
                        <div class="col-label-service-type">
                            <h4>Ship-Type: </h4>
                        </div>
                        <div class="col-data-service-ship">
                            <h4>{{ isset($services['ship_type']) ? $shipData[$services['ship_type']] : '-'}}</h4>
                        </div>
                    </div>
                    <div class="col-service-row4-2">
                        <div class="col-label-service-grt">
                            <h4>GRT: </h4>
                        </div>
                        <div class="col-data-service-grt">
                            <h4>{{ !empty($services['grt']) ? $services['grt'] : '-'}}</h4>
                        </div>
                    </div>
                    <div class="col-service-row4-3">
                        <div class="col-label-service-bhp">
                            <h4>BHP: </h4>
                        </div>
                        <div class="col-data-service-bhp">
                            <h4>{{ !empty($services['bhp']) ? $services['bhp'] : '-'}}</h4>
                        </div>
                    </div>
                    <div class="col-service-row4-4">
                        <div class="col-label-service-etype">
                            <h4>Engine-Type: </h4>
                        </div>
                        <div class="col-data-service-etype">
                            <h4>
                                @if(isset($services['engine_type']) && !empty($services['engine_type']))
                                    @if(isset($services['engine_type']) && $services['engine_type'] == 'other')
                                        {{ isset($services['other_engine_type']) ? $services['other_engine_type'] : '-'}}
                                    @else
                                        @foreach( CommonHelper::engine_type_by_user_id(isset($data[0]['id'])) as $c_index => $type)
                                            {{ isset($services['engine_type']) ? $services['engine_type'] == $c_index ? $type  : '' : ''}}
                                        @endforeach
                                    @endif
                                @else
                                    -
                                @endif
                            </h4>
                        </div>
                    </div>
                </div>
                <!--service-div4-->
            </div>
            <!-- outer-div -->
            <div class="outer-div-between"></div>



        @endforeach
    @else
        <div class="row ship-data" style="margin-top:15px;">
            <div class="col-sm-12">
                <div class="list">
                    <div class="list-item">
                        <div class="text">
                            No Data Found
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
    @if($userProData->other_exp)
        <div class="main-title">OTHER SERVICE</div>
        <div class="other-service-div">
            {{$userProData->other_exp}}
        </div>
    @endif
</div>