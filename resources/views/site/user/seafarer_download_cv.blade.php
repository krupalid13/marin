<html>
	<head>
		<style>
			table, th, td {
			    /* border: 1px solid black; */
			    border-collapse: collapse;
			    margin: 6px auto;
			    width: 100%;
			}
			.left {
				text-align: left;
			}
			th{
				padding: 10px;
				font-size: 13px;
				border: 1px solid black;
			}
			.border-class {
				border: 1px solid black;
			}
			.border-none {
				border: none;
			}
			.f-10 {
				font-size: 12px;
			}
			.f-9 {
				font-size: 10px;
			}
			.center {
				text-align: center
			}
			.p-5 {
				padding: 5px;
			}
		</style>
	</head>
	<body>
		<table style="width: 700px;margin: 0 auto;">
			<tbody>	
				<tr>
					<td>
						<table style="width: 700px;margin: 0px;">
							<tbody>
								<tr>
									<td style="width: 28%;">
										<img src="{{asset('images/coss-logo.png')}}" height="55">
									</td>
									<th class="border-none" style="font-size: 23;float: left;width: 42%;">
										<u>Application Form</u>
									</td>
									<td style="float: right;width: 30%;">
										<t class="f-10">
											<b>Date of Availability:</b>
											{{isset($data['professional_detail']['availability']) ? date('d/m/Y',strtotime($data['professional_detail']['availability'])):'-' }}
										</t>
										<br>
										<t class="f-10">
											<b>Rank Applied:</b>
											@foreach(\CommonHelper::new_rank() as $rank_index => $category)
										    	@foreach( $category as $c_index => $type)
													{{ isset($data['professional_detail']['applied_rank']) ? $data['professional_detail']['applied_rank'] == $c_index ? $type : '' : ''}}
												@endforeach
											@endforeach
										</t>
									</td>
									</th>
								</tr>
							</tbody>
						</table>
					</td>
				</tr>
				<tr>
					<td>
						<table cellpadding="5" style="border: 1px solid black;border-radius: 8px;overflow: hidden;background-color: black;" class="main_table">
							<tbody style="background-color: #ffffff;">
								<tr>
									<th colspan="5">
										<span style="font-size: 16px;">Personal Details</span>
									<t class="f-10" style="float: right;">
										<id style="padding-right: 5px;">
										ID: {{isset($data['id']) ? $data['id'] : ''}}
										</id>
										Last Updated:  {{isset($data['updated_at']) ? date('d/m/Y',strtotime($data['updated_at'])) : '' }}
									</t>
									</th>
								</tr>
								<?php
									$img_path = '';
									if(isset($data['profile_pic'])){
								    	$img_path = env('SEAFARER_PROFILE_PATH').$data['id'].'/'.$data['profile_pic'];
									}

								?>
								@if(isset($img_path) AND !empty($img_path))
								<tr class="f-10">
									
							    	<td class="border-class center" rowspan='2'>
								    	@if(isset($img_path) AND !empty($img_path))
									    	<img src="{{asset($img_path)}}" height="110" width="110">
									    @else
									    	<img src="{{asset('/images/user_default_image.png')}}" height="110" width="110">
									    @endif
								    </td>
						    		
								    <td class="border-class" colspan="2"><b>Name:</b> 
								    	{{isset($data['first_name']) ? $data['first_name'] : ''}}
								    </td>
								    
								    <td class="border-class"><b>DOB:</b>
								    	{{isset($data['personal_detail']['dob']) ? date('d/m/Y',strtotime($data['personal_detail']['dob'])) : '' }}
								    </td>
								    <td class="border-class"><b>Marital Status:</b>
									    @foreach( \CommonHelper::marital_status() as $c_index => $status)
											{{ isset($data['personal_detail']['marital_status']) ? $data['personal_detail']['marital_status'] == $c_index ? $status : '' : ''}}
										@endforeach
								    </td>
								</tr>
								@else
									<tr class="f-10">
						    		
								    <td class="border-class" colspan="3"><b>Name:</b> 
								    	{{isset($data['first_name']) ? $data['first_name'] : ''}}
								    </td>
								    
								    <td class="border-class"><b>DOB:</b>
								    {{isset($data['personal_detail']['dob']) ? date('d/m/Y',strtotime($data['personal_detail']['dob'])) : '' }}
								    </td>
								    <td class="border-class"><b>Marital Status:</b>
								    @foreach( \CommonHelper::marital_status() as $c_index => $status)
										{{ isset($data['personal_detail']['marital_status']) ? $data['personal_detail']['marital_status'] == $c_index ? $status : '' : ''}}
									@endforeach
								    </td>
								</tr>
								@endif
								@if(isset($img_path) AND !empty($img_path))
								<tr class="f-10">
									<td class="border-class"><b>Nationality:</b>
										@foreach( \CommonHelper::countries() as $c_index => $country)
											{{ isset($data['personal_detail']['nationality']) ? $data['personal_detail']['nationality'] == $c_index ? $country : '' : ''}}
											</option>
										@endforeach
									</select>
								    </td>
								    <td class="border-class"><b>Mobile:</b> 
										{{isset($data['mobile']) ? $data['mobile'] : '-'}}
										@if($data['is_mob_verified'] == '1')
											<img src="{{asset('/images/tick.png')}}" height="15" width="15" style="padding-left: 2px;">
										@else
											<img src="{{asset('/images/untick.png')}}" height="15" width="15" style="padding-left: 2px;">
										@endif
								    </td>
								    <td class="border-class" colspan="2"><b>Email Id:</b>
								    	{{isset($data['email']) ? $data['email'] : '-'}}
								    	@if($data['is_email_verified'] == '1')
											<img src="{{asset('/images/tick.png')}}" height="15" width="15" style="padding-left: 2px;">
										@else
											<img src="{{asset('/images/untick.png')}}" height="15" width="15" style="padding-left: 2px;">
										@endif
								    </td>
								</tr>
								@else
									<tr class="f-10">
									<td class="border-class" colspan="2"><b>Nationality:</b>
										@foreach( \CommonHelper::countries() as $c_index => $country)
											{{ isset($data['personal_detail']['nationality']) ? $data['personal_detail']['nationality'] == $c_index ? $country : '' : ''}}
											</option>
										@endforeach
									</select>
								    </td>
								    <td class="border-class"><b>Mobile:</b> 
										{{isset($data['mobile']) ? $data['mobile'] : ''}}
								    </td>
								    <td class="border-class" colspan="2"><b>Email Id:</b>
								    	{{isset($data['email']) ? $data['email'] : ''}}
								    </td>
								</tr>
								@endif
								<tr class="f-10">
									<td colspan="5" style="border-right: 1px solid black"><b>Address:</b>
										{{isset($data['personal_detail']['permanent_add']) ? $data['personal_detail']['permanent_add'] : '' }}
									</td>
								</tr>
								<tr class="f-10">
									<td><b>City:</b>
										{{ isset($data['personal_detail']['city_id']) ? $data['personal_detail']['pincode']['pincodes_cities'][0]['city']['name'] : (isset($data['personal_detail']['city_text']) ? $data['personal_detail']['city_text'] : '')}}
									</td>
									<td><b>State:</b>
										{{ isset($data['personal_detail']['state_id']) ? ucfirst(strtolower($data['personal_detail']['pincode']['pincodes_states'][0]['state']['name'])) : (isset($data['personal_detail']['state_text']) ? ucfirst(strtolower($data['personal_detail']['state_text'])) : '')}}
									</td>
									<td><b>Country:</b>
										@foreach( \CommonHelper::countries() as $c_index => $country)
	                                        {{ isset($data['personal_detail']['country']) ? $data['personal_detail']['country'] == $c_index ? $country : '' : ''}}
	                                    @endforeach
									</td>
									<td colspan="2"><b>Pin Code:</b>
										{{ isset($data['personal_detail']['pincode_text']) ? $data['personal_detail']['pincode_text'] : ''}}
									</td>	
								</tr>
								<tr class="f-10">
									<td class="border-class"><b>Height:</b>
										{{isset($data['personal_detail']['height']) ? $data['personal_detail']['height'] : '-' }} cms
									</td>
									<td class="border-class"><b>Weight:</b>
										{{isset($data['personal_detail']['weight']) ? $data['personal_detail']['weight'] : '-' }} kg
									</td>
									<td class="border-class"><b>Current Rank:</b>
										@if(isset($data['professional_detail']['current_rank']) && !empty($data['professional_detail']['current_rank']))
											@foreach(\CommonHelper::new_rank() as $rank_index => $category)
												@foreach( $category as $c_index => $type)
													{{ isset($data['professional_detail']['current_rank']) ? $data['professional_detail']['current_rank'] == $c_index ? $type : '' : ''}}
												@endforeach
											@endforeach
										@else
											-
										@endif
									</td>
									<td class="border-class" colspan="2"><b>Last wages drawn:</b>
										{{isset($data['professional_detail']['currency']) ? $data['professional_detail']['currency'] == 'dollar' ? '$' : 'Rs' : 'Rs'}} {{ isset($data['professional_detail']['last_salary']) ? $data['professional_detail']['last_salary'] : '-'}}
										<b>Per Month</b>
									</td>
								</tr>
								<tr class="f-10">
									<td class="border-class" colspan="2"><b>Next Of Kin Name:</b>
											{{isset($data['personal_detail']['kin_name']) ? $data['personal_detail']['kin_name'] : '-' }}
									</td>
									<td class="border-class" ><b>Relation:</b>
										{{isset($data['personal_detail']['kin_relation']) ? $data['personal_detail']['kin_relation'] : '-' }}
									</td>
									<td class="border-class" colspan="2"><b>Phone Number:</b>
										{{isset($data['personal_detail']['kin_number']) ? $data['personal_detail']['kin_number'] : '-' }}
									</td>
								</tr>
							</tbody>
						</table>
					</td>
				</tr>

				<tr>
					<td>	
						<table cellpadding="5" style="border: 1px solid black;border-radius: 8px;overflow: hidden;background-color: black;">
							<tbody style="background-color: #ffffff;">
								<tr>
									<th colspan="5">
										<span style="font-size: 16px;">Important Documents</span>
										<t style="float: right;"> Verified </t> <span style="float: right;padding: 0 2px;"> = </span>
										<img src="{{asset('/images/tick.png')}}" height="15" width="15" style="float: right;">
									</th>
								</tr>
								<tr>
									<th>Document</th>
									<th>Number</th>
									<th>Date Of Issue</th>
									<th>Place Of Issue</th>
									<th>Valid Till</th>
								</tr>
								<tr class="f-10">
								    <td class="border-class" style="width: 2%;">Passport</td>
								    <td class="border-class" style="width: 2%;">
								    	{{isset($data['passport_detail']['pass_number']) && !empty($data['passport_detail']['pass_number']) ? $data['passport_detail']['pass_number'] : '-'}}</td>
								    <td class="border-class" style="width: 2%;"> 
								    	{{ isset($data['passport_detail']['pass_issue_date']) && !empty($data['passport_detail']['pass_issue_date']) ? date('d/m/Y',strtotime($data['passport_detail']['pass_issue_date'])) : '-'}}
								    </td>
								    <td class="border-class" style="width: 2%;">
								    	{{ isset($data['passport_detail']['place_of_issue']) && !empty($data['passport_detail']['place_of_issue']) ? $data['passport_detail']['place_of_issue'] : '-'}}
								    </td>
								    <td class="border-class" style="width: 2%;">
								    	{{ isset($data['passport_detail']['pass_expiry_date']) && !empty($data['passport_detail']['pass_expiry_date']) ? date('d/m/Y',strtotime($data['passport_detail']['pass_expiry_date'])) : '-'}}
								    </td>
								</tr>
								@if(isset($data['seaman_book_detail']))
									@foreach($data['seaman_book_detail'] as $index => $cdc)
									<tr class="f-10">
									    <td class="border-class" style="width: 2%;">CDC {{$index+1}}
									    	@if(isset($cdc['status']) AND $cdc['status'] == '1')
									    		<img src="{{asset('/images/tick.png')}}" height="15" width="15" style="padding-left: 5px;">
											@else
												<img src="{{asset('/images/untick.png')}}" height="15" width="15" style="padding-left: 5px;">
									    	@endif
									    </td>
									    <td class="border-class" style="width: 2%;">{{ isset($cdc['cdc_number']) ? $cdc['cdc_number'] : '-'}}</td>
									    <td class="border-class" style="width: 2%;"> 
									    	{{ isset($cdc['cdc_issue_date']) ? date('d/m/Y',strtotime($cdc['cdc_issue_date'])) : ''}}
									    </td>
									    <td class="border-class" style="width: 2%;">
											@foreach( \CommonHelper::countries() as $c_index => $country)
												{{ isset($cdc['cdc']) ? $cdc['cdc'] == $c_index ? $country : '' : '-'}}
											@endforeach
									    </td>
									    <td class="border-class" style="width: 2%;">
									    	{{ isset($cdc['cdc_expiry_date']) ? date('d/m/Y',strtotime($cdc['cdc_expiry_date'])) : '-'}}
									    </td>
									</tr>
									@endforeach
								@endif
								@if((isset($data['wkfr_detail']['indos_number']) && !empty($data['wkfr_detail']['indos_number'])) || (isset($data['wkfr_detail']['yellow_fever']) && $data['wkfr_detail']['yellow_fever'] == '1'))
								<tr class="f-10">
								    <td class="border-class" colspan="1"><b>INDOS No:</b> </td>
								    <td class="border-class" colspan="1">
								    	{{ isset($data['wkfr_detail']['indos_number']) ? !empty($data['wkfr_detail']['indos_number']) ? $data['wkfr_detail']['indos_number'] : '-' : '-'}}
								    </td>
								    <td class="border-class" colspan="1"><b>Yellow Fever:</b>
										{{ isset($data['wkfr_detail']['yellow_fever']) ? ($data['wkfr_detail']['yellow_fever'] == '1') ? 'Yes' : 'No' : '-' }}
								    </td>
								    <td class="border-class" colspan="2"><b>Issue Date:</b>
								    @if(isset($data['wkfr_detail']['yellow_fever']) && $data['wkfr_detail']['yellow_fever'] == '1')
										{{isset($data['wkfr_detail']['yf_issue_date']) ? date('d/m/Y',strtotime($data['wkfr_detail']['yf_issue_date'])) : '-' }}
									@else 
										NA
								    @endif
								</tr>
								@endif
								@if((isset($data['passport_detail']['us_visa']) && ($data['passport_detail']['us_visa'] == '1')) || (isset($data['passport_detail']['indian_pcc']) && $data['passport_detail']['indian_pcc'] == '1'))
								<tr class="f-10">
									<td class="border-class"><b>US Visa:</b>
										{{isset($data['passport_detail']['us_visa']) ? ($data['passport_detail']['us_visa'] == '1') ? 'Yes' : 'No' : '-' }}
								    </td>
								    <td class="border-class" colspan="2" style="width: 50%; " ><b>US Visa Expiry Date:</b>
								    @if(isset($data['passport_detail']['us_visa']) && $data['passport_detail']['us_visa'] == '1')
										{{isset($data['passport_detail']['us_visa_expiry_date']) ? date('d/m/Y',strtotime($data['passport_detail']['us_visa_expiry_date'])) : '-' }}
									@else 
										NA
								    @endif
								    </td>
								    <td class="border-class"><b>PCC:</b>
										{{isset($data['passport_detail']['indian_pcc']) ? ($data['passport_detail']['indian_pcc'] == '1') ? 'Yes' : 'No' : '-' }}
								    </td>
								    <td class="border-class" colspan="1"><b>PCC Issue Date:</b>
								    @if(isset($data['passport_detail']['indian_pcc']) && $data['passport_detail']['indian_pcc'] == '1')
										{{isset($data['passport_detail']['indian_pcc_issue_date']) ? date('d/m/Y',strtotime($data['passport_detail']['indian_pcc_issue_date'])) : '-' }}
									@else
										NA
								    @endif
								    </td>
								</tr>
								@endif
							</tbody>
						</table>
					</td>
				</tr>

				<?php
					$wkfr_count = 5;

					if(isset($data['wkfr_detail']['issue_date']) AND !empty($data['wkfr_detail']['issue_date'])){
						$show_doi = true;
						$wkfr_count = $wkfr_count+1;
					}
					if(isset($data['gmdss_detail']['gmdss_endorsement_number']) AND !empty($data['gmdss_detail']['gmdss_endorsement_number'])){
						$gmend = $data['gmdss_detail']['gmdss_endorsement_number'];
						//$wkfr_count = $wkfr_count+1;
					}
					if(isset($data['gmdss_detail']['gmdss_endorsement_expiry_date']) AND !empty($data['gmdss_detail']['gmdss_endorsement_expiry_date'])){
						$gmendate = date('d/m/Y',strtotime($data['gmdss_detail']['gmdss_endorsement_expiry_date']));
						//$wkfr_count = $wkfr_count+1;
					}
				?>

				@if((isset($data['coc_detail']) && (!empty($coc['coc_number']) || !empty($coc['coc_grade']) || !empty($coc['coc_expiry_date']) || !empty($coc['coc'])))  || (isset($data['gmdss_detail']['gmdss_number']) && !empty($data['gmdss_detail']['gmdss_number'])) || (isset($data['gmdss_detail']['gmdss_expiry_date']) && !empty($data['gmdss_detail']['gmdss_expiry_date'])) || (isset($data['gmdss_detail']['gmdss_expiry_date']) && !empty($data['gmdss_detail']['gmdss_expiry_date'])) || (isset($data['wkfr_detail']['wkfr_number']) AND !empty($data['wkfr_detail']['wkfr_number'])) || (isset($data['wkfr_detail']['issue_date']) AND !empty($data['wkfr_detail']['issue_date'])))
				<tr>
					<td>	
						<table cellpadding="5" style="border: 1px solid black;border-radius: 8px;overflow: hidden;background-color: black;">
							<tbody style="background-color: #ffffff;">
								<tr>
									<th colspan="{{$wkfr_count}}">
										<span style="font-size: 16px;">Certificate of Competency / GMDSS / Watch Keeping / COP</span>
										<t style="float: right;"> Verified </t> <span style="float: right;padding: 0 2px;"> = </span>
										<img src="{{asset('/images/tick.png')}}" height="15" width="15" style="float: right;">
									</th>
								</tr>
								<tr>
									<th>Certificate</th>
									<th>Number</th>
									<th>Grade</th>
									@if(isset($show_doi)) <th>Date Of Issue</th> @endif
									<th>Date Of Expiry</th>
									<th>Issued By</th>
								</tr>
								@if((isset($data['coc_detail']) && !empty($data['coc_detail'])) && (!empty($coc['coc_number']) || !empty($coc['coc_grade']) || !empty($coc['coc_expiry_date']) || !empty($coc['coc'])))
									@foreach($data['coc_detail'] as $index => $coc)
										<tr class="f-10">
										    <td class="border-class">COC {{$index+1}}
										    @if(isset($coc['status']) AND $coc['status'] == '1')
									    		<img src="{{asset('/images/tick.png')}}" height="15" width="15" style="padding-left: 5px;">
											@else
													<img src="{{asset('/images/untick.png')}}" height="15" width="15" style="padding-left: 5px;">
									    	@endif
										    </td>
										    <td class="border-class">{{ isset($coc['coc_number']) && !empty($coc['coc_number']) ? $coc['coc_number'] : '-'}}</td>
										    <td class="border-class">{{ isset($coc['coc_grade']) && !empty($coc['coc_grade']) ? $coc['coc_grade'] : '-'}}</td>
										    @if(isset($show_doi)) <td class="border-class"> - </td> @endif
										    <td class="border-class">
										    	{{ isset($coc['coc_expiry_date']) ? date('d/m/Y',strtotime($coc['coc_expiry_date'])) : '-'}}
										    </td>
										    <td class="border-class">
										    	@if(isset($coc['coc']) && !empty($coc['coc']))
													@foreach( \CommonHelper::countries() as $c_index => $country)
														{{ isset($coc['coc']) ? $coc['coc'] == $c_index ? $country : '' : ''}}
													@endforeach
												@else
													-
												@endif
										    </td>
										</tr>
									@endforeach
								@endif
								
								@if((isset($data['gmdss_detail']['gmdss_number']) && !empty($data['gmdss_detail']['gmdss_number'])) || (isset($data['gmdss_detail']['gmdss_expiry_date']) && !empty($data['gmdss_detail']['gmdss_expiry_date'])) || (isset($data['gmdss_detail']['gmdss_expiry_date']) && !empty($data['gmdss_detail']['gmdss_expiry_date'])))
								<tr class="f-10">
								    <td class="border-class" style="width: 10%;">
								    	GMDSS
								    </td>
								    <td class="border-class" style="width: 2%;">
								    	{{isset($data['gmdss_detail']['gmdss_number']) ? $data['gmdss_detail']['gmdss_number'] : '-'}}
								    </td>
								    <td class="border-class" style="width: 2%;">General Operator</td>
								   	@if(isset($show_doi))  <td class="border-class" style="width: 2%;">-</td> @endif
								    <td class="border-class" style="width: 2%;">
								    	{{isset($data['gmdss_detail']['gmdss_expiry_date']) ? date('d/m/Y',strtotime($data['gmdss_detail']['gmdss_expiry_date'])) : ''}}
								    </td>
								    <td class="border-class" style="width: 2%;">
								    	@if(isset($data['gmdss_detail']['gmdss']) && !empty($data['gmdss_detail']['gmdss']))
									    	@foreach( \CommonHelper::countries() as $c_index => $country)
												{{ isset($data['gmdss_detail']['gmdss']) ? $data['gmdss_detail']['gmdss'] == $c_index ? $country : '' : ''}}
											@endforeach
										@else
											-
										@endif
								    </td>
								</tr>
								@endif

								@if(isset($gmend) || isset($gmendate))
								<tr class="f-10">
								    <td class="border-class" style="width: 10%;">
								    	GMDSS Endorsement
								    </td>
						 		    <td class="border-class" style="width: 2%;">{{ isset($gmend) ? $gmend : '-' }}</td>
						 		    <td class="border-class" style="width: 2%;">-</td>
						 		    @if(isset($show_doi)) <td class="border-class"> - </td> @endif
									<td class="border-class" style="width: 2%;">{{ isset($gmendate) ? $gmendate : '-' }}</td>
									<td class="border-class" style="width: 2%;">-</td>
								</tr>
								@endif

								@if((isset($data['wkfr_detail']['wkfr_number']) AND !empty($data['wkfr_detail']['wkfr_number'])) || (isset($data['wkfr_detail']['issue_date']) AND !empty($data['wkfr_detail']['issue_date'])))
								<tr class="f-10">
								    <td class="border-class" style="width: 10%;">
								    	Watch Keeping
								    	@if(isset($data['wkfr_detail']['status']) AND $data['wkfr_detail']['status'] == '1')
								    		<img src="{{asset('/images/tick.png')}}" height="15" width="15" style="padding-left: 5px;">
										@else
											<img src="{{asset('/images/untick.png')}}" height="15" width="15" style="padding-left: 5px;">
								    	@endif
								    </td>
								    <td class="border-class" style="width: 2%;">
								    	{{!empty($data['wkfr_detail']['wkfr_number']) ? $data['wkfr_detail']['wkfr_number'] : '-'}}
								    </td>
								    <td class="border-class" style="width: 2%;">-</td>
								    @if(isset($show_doi))
								    <td class="border-class" style="width: 2%;">
								    	{{!empty($data['wkfr_detail']['issue_date']) ? date('d/m/Y',strtotime($data['wkfr_detail']['issue_date'])) : '-'}}
								    </td>
								    @endif
								    <td class="border-class" style="width: 2%;">-</td>
						 		    <td class="border-class" style="width: 2%;">-</td>
								</tr>
								@endif
							</tbody>
						</table>
					</td>
				</tr>
				@endif
				@if((isset($data['cop_detail']) AND !empty($data['cop_detail'])) || (isset($data['coe_detail']) AND !empty($data['coe_detail'])))
				<tr>
					<td>	
						<table cellpadding="6" style="border: 1px solid black;border-radius: 8px;overflow: hidden;background-color: black;">
							<tbody style="background-color: #ffffff;">
								<tr>
									<th colspan="5">
										<span style="font-size: 16px;">Certificate of Proficiency / Certificate of Endorsement</span>
									</th>
								</tr>
								<tr>
									<th>Name</th>
									<th>Number</th>
									<th>Grade</th>
									<th>Date Of Issue</th>
									<th>Date Of Expiry</th>
								</tr>
								@if(isset($data['cop_detail']))
									@foreach($data['cop_detail'] as $index => $cop)
										<tr class="f-10">
										    <td class="border-class" style="width: 8%;">COP Details {{$index+1}}</td>
										    <td class="border-class" style="width: 2%;">{{ isset($cop['cop_number']) && !empty($cop['cop_number']) ? $cop['cop_number'] : '-'}}</td>
										    <td class="border-class" style="width: 2%;">{{ isset($cop['cop_grade']) && !empty($cop['cop_grade']) ? $cop['cop_grade'] : '-'}}</td>
										    <td class="border-class" style="width: 2%;">
										    	{{ isset($cop['cop_issue_date']) && !empty($cop['cop_issue_date']) ? date('d/m/Y',strtotime($cop['cop_issue_date'])) : '-'}}
										    </td>
										    <td class="border-class" style="width: 2%;">
										    	{{ isset($cop['cop_exp_date']) && !empty($cop['cop_exp_date']) ? date('d/m/Y',strtotime($cop['cop_exp_date'])) : '-'}}
										    </td>
										</tr>
									@endforeach
								@endif

								@if(isset($data['coe_detail']))
									@foreach($data['coe_detail'] as $index => $coe)
										<tr class="f-10">
										    <td class="border-class" style="width: 8%;">COE Details {{$index+1}}</td>
										    <td class="border-class" style="width: 2%;">{{ isset($coe['coe_number']) && !empty($coe['coe_number']) ? $coe['coe_number'] : '-'}}</td>
										    <td class="border-class" style="width: 2%;">{{ isset($coe['coe_grade']) && !empty($coe['coe_grade']) ? $coe['coe_grade'] : '-'}}</td>
										    <td class="border-class" style="width: 2%;">-</td>
										    <td class="border-class" style="width: 2%;">
										    	{{ isset($coe['coe_expiry_date']) && !empty($coe['coe_expiry_date']) ? date('d/m/Y',strtotime($coe['coe_expiry_date'])) : '-'}}
										    </td>
										</tr>
									@endforeach
								@endif

							</tbody>
						</table>
					</td>
				</tr>
				@endif


				@if(!empty($data['professional_detail']['name_of_school']) || !empty($data['professional_detail']['school_from']) || !empty($data['professional_detail']['school_to']) || !empty($data['professional_detail']['school_qualification']))
				<tr>
					<td>	
						<table cellpadding="6" style="border: 1px solid black;border-radius: 8px;overflow: hidden;background-color: black;">
							<tbody style="background-color: #ffffff;">
								<tr class="f-10" colspan='6'>
									<th colspan='6'>
										<span style="font-size: 16px;">Academics and Professional Qualification</span>
									</th>
								</tr>
								<tr class="f-10 border-class">
								<td class="f-10 border-class" colspan='2'><b>Name of School/College:</b>
									{{ isset($data['professional_detail']['name_of_school']) && !empty($data['professional_detail']['name_of_school']) ? $data['professional_detail']['name_of_school'] : '-'}}
								</td>
								<td class="f-10 border-class" colspan='2'><b>Pass Out Year:</b>
									{{ isset($data['professional_detail']['school_to']) && !empty($data['professional_detail']['school_to']) ? $data['professional_detail']['school_to'] : '-'}}
								</td>
								<td class="f-10 border-class" colspan='2'><b>Highest Qualification:</b>
									{{ isset($data['professional_detail']['school_qualification']) && !empty($data['professional_detail']['school_qualification']) ? $data['professional_detail']['school_qualification'] : '-'}}
								</td>	
							</tr>
							</tbody>
						</table>
					</td>
				</tr>
				@endif

				@if(!empty($data['professional_detail']['institute_name']) || !empty($data['professional_detail']['institute_from']) || !empty($data['professional_detail']['institute_to']) || !empty($data['professional_detail']['institute_degree']))
				<tr>
					<td>	
						<table cellpadding="6" style="border: 1px solid black;border-radius: 8px;overflow: hidden;background-color: black;">
							<tbody style="background-color: #ffffff;">
								<tr class="f-10" colspan='6'>
									<th colspan='6'>
										<span style="font-size: 16px;">Pra Sea Training / Apprentice Ship</span>
									</th>
								</tr>
								<tr class="f-10 border-class">
								<td class="f-10 border-class" colspan='2'><b>Name of Institute/ College/ Workshop:</b>
									{{ isset($data['professional_detail']['institute_name']) && !empty($data['professional_detail']['institute_name']) ? $data['professional_detail']['institute_name'] : '-'}}
								</td>
								<td class="f-10 border-class" colspan='2'><b>Pass Out Year:</b>
									{{ isset($data['professional_detail']['institute_to']) && !empty($data['professional_detail']['institute_to']) ? $data['professional_detail']['institute_to'] : '-'}}
								</td>
								<td class="f-10 border-class" colspan='2'><b>Type Of Degree:</b>
									{{ isset($data['professional_detail']['institute_degree']) && !empty($data['professional_detail']['institute_degree']) ? $data['professional_detail']['institute_degree'] : '-'}}
								</td>	
							</tr>
							</tbody>
						</table>
					</td>
				</tr>
				@endif

				@if(isset($data['sea_service_detail']) AND !empty($data['sea_service_detail']))

				<?php 
					$grt = '';
					$bhp = '';
					$flag = '';
					$engine = '';
					$count = 7;
				?>

				@foreach($data['sea_service_detail'] as $index => $services)

					@if(isset($services['grt']) && !empty($services['grt']) && empty($grt))
						<?php $grt = 1;$count +=1; ?>
					@endif

					@if(isset($services['bhp']) && !empty($services['bhp']) && empty($bhp))
						<?php $bhp = 1;$count +=1; ?>
					@endif

					@if(isset($services['ship_flag']) && !empty($services['ship_flag']) && empty($flag))
						<?php $flag = 1;$count +=1; ?>
					@endif

					@if(isset($services['engine_type']) && !empty($services['engine_type']) && empty($engine))
						<?php $engine = 1;$count +=1; ?>
					@endif
				@endforeach

				<tr>
					<td>	
						<table cellpadding="{{$count}}" style="border: 1px solid black;width: 100%;border-radius: 8px;overflow: hidden;background-color: black;">
							<tbody style="background-color: #ffffff;">
								<tr>
									<th colspan="{{$count}}">
										<span style="font-size: 16px;">Sea Service Record</span>
									</th>
								</tr>
								<tr class="f-9">
									<th>Vessel Name</th>
									<th>Company</th>
									<th>Type</th>
									@if(!empty($flag)) <th>Flag</th> @endif
									@if(!empty($grt)) <th>GRT</th> @endif
									@if(!empty($engine)) <th>Engine</th> @endif
									@if(!empty($bhp)) <th>BHP</th> @endif
									<th>Rank</th>
									<th>Sign On Date</th>
									<th>Sign Off Date</th>
									<th>Total</th>
								</tr>
								@foreach($data['sea_service_detail'] as $service_index => $services)
									<tr class="f-10">
									    <td class="border-class">
									    	{{ isset($services['ship_name']) && !empty($services['ship_name']) ? $services['ship_name'] : '-'}}
									    </td>
									    <td class="border-class">
											{{ isset($services['company_name']) && !empty($services['company_name']) ? $services['company_name'] : '-'}}
									    </td>
									    <td class="border-class">
									    	@foreach( \CommonHelper::ship_type() as $c_index => $type)
												{{ isset($services['ship_type']) ? $services['ship_type'] == $c_index ? $type : '' : ''}}
											@endforeach
									    </td>
									    @if(!empty($flag))
									    <td class="border-class">
									    	@if(isset($services['ship_flag']) && !empty($services['ship_flag']))
										    	@foreach( \CommonHelper::countries() as $c_index => $country)
													{{ isset($services['ship_flag']) ? $services['ship_flag'] == $c_index ? $country : '' : '-'}}
												@endforeach
											@else
												-
											@endif
									    </td>
									    @endif
									    @if(!empty($grt))
									    <td class="border-class">{{ !empty($services['grt']) ? $services['grt'] : '-'}}</td>
									    @endif
									    @if(!empty($engine))
									    <td class="border-class">
									    	@if(isset($services['engine_type']) && !empty($services['engine_type']))
										    	@foreach( \CommonHelper::engine_type() as $e_index => $engine_type)
													{{ isset($services['engine_type']) ? $services['engine_type'] == $e_index ? $engine_type == 'Other' ? $services['other_engine_type']: $engine_type  : '' : ''}}
												@endforeach
											@else
												-
											@endif
									    </td>
									    @endif
									    @if(!empty($bhp))
									    <td class="border-class">{{ !empty($services['bhp']) ? $services['bhp'] : '-'}}</td>
									    @endif
									    <td class="border-class">
									    	@if(isset($services['rank_id']) && !empty($services['rank_id']))
										    	@foreach(\CommonHelper::new_rank() as $rank_index => $category)
											    	@foreach( $category as $c_index => $type)
														{{ isset($services['rank_id']) ? $services['rank_id'] == $c_index ? $type : '' : ''}}
													@endforeach
												@endforeach
											@else
												-
											@endif
									    </td>
									    <td class="border-class">{{ isset($services['from']) && !empty($services['from']) ? date('d/m/Y',strtotime($services['from'])) : '-'}}</td>
									    <td class="border-class">{{ isset($services['to']) && !empty($services['to']) ? date('d/m/Y',strtotime($services['to'])) : '-'}}</td>
									 	<?php
											$date1 = new DateTime($services['from']);
									 		$date2 = new DateTime($services['to']);
											$interval = $date1->diff($date2);
									 	?>
									    <td class="border-class">{{$interval->y*12 + $interval->m}} months/ {{$interval->d}} days</td>
									</tr>
								@endforeach
							</tbody>
						</table>
					</td>
				</tr>
				@endif

				@if((isset($data['course_detail']) AND !empty($data['course_detail'])) || ((isset($data['value_added_course_detail']) AND !empty($data['value_added_course_detail']))))

					<?php 
						$certification = '';
						$expiry_date = '';
						$issue_by = '';
						$issue_date = '';
						$count = 1;
					?>

					@if((isset($data['course_detail']) AND !empty($data['course_detail'])))
						@foreach($data['course_detail'] as $index => $courses)
							@if(isset($courses['certification_number']) && !empty($courses['certification_number']) && empty($certification))
								<?php $certification = 1;$count +=1; ?>
							@endif

							@if(isset($courses['expiry_date']) && !empty($courses['expiry_date']) && empty($expiry_date))
								<?php $expiry_date = 1;$count +=1; ?>
							@endif

							@if(isset($courses['issue_by']) && !empty($courses['issue_by']) && empty($issue_by))
								<?php $issue_by = 1;$count +=1; ?>
							@endif

							@if(isset($courses['issue_date']) && !empty($courses['issue_date']) && empty($issue_date))
								<?php $issue_date = 1;$count +=1; ?>
							@endif
						@endforeach
					@endif


					@if((isset($data['value_added_course_detail']) AND !empty($data['value_added_course_detail'])))
						@foreach($data['value_added_course_detail'] as $index => $courses)
							@if(isset($courses['certification_number']) && !empty($courses['certification_number']) && empty($certification))
								<?php $certification = 1;$count +=1; ?>
							@endif

							@if(isset($courses['expiry_date']) && !empty($courses['expiry_date']) && empty($expiry_date))
								<?php $expiry_date = 1;$count +=1; ?>
							@endif

							@if(isset($courses['issue_by']) && !empty($courses['issue_by']) && empty($issue_by))
								<?php $issue_by = 1;$count +=1; ?>
							@endif

							@if(isset($courses['issue_date']) && !empty($courses['issue_date']) && empty($issue_date))
								<?php $issue_date = 1;$count +=1; ?>
							@endif
						@endforeach
					@endif

				<tr>
					<td>	
						<table cellpadding="{{$count}}" style="border: 1px solid black;border-radius: 8px;overflow: hidden;background-color: black;">
							<tbody style="background-color: #ffffff;">
								<tr>
									<th colspan="{{$count}}">
										<span style="font-size: 16px;">Course and Certificates</span>
									</th>
								</tr>
								<tr>
									<th>Course Title</th>
									@if(!empty($certification)) <th>Certificate Number</th> @endif
									@if(!empty($issue_date)) <th>Date Of Issue</th> @endif
									@if(!empty($expiry_date)) <th>Date Of Expiry</th> @endif
									@if(!empty($issue_by)) <th>Issued By</th> @endif
								</tr>
								@if((isset($data['course_detail']) AND !empty($data['course_detail'])))
									@foreach($data['course_detail'] as $index => $courses)
										<tr class="f-10">
											<td class="border-class p-5" style="width: 2%;">
												@foreach( \CommonHelper::courses() as $c_index => $course)
													{{ isset($courses['course_id']) ? $courses['course_id'] == $c_index ? $course : '' : ''}}
												@endforeach
											</td>
											@if(!empty($certification))
											<td class="border-class p-5" style="width: 2%;">
												{{ isset($courses['certification_number']) && !empty($courses['certification_number']) ? $courses['certification_number'] : '-'}}
											</td>
											@endif
											@if(!empty($issue_date))
											<td class="border-class p-5" style="width: 2%;">
												{{ isset($courses['issue_date']) && !empty($courses['issue_date']) ? date('d/m/Y',strtotime($courses['issue_date'])) : '-'}}
											</td>
											@endif
											@if(!empty($expiry_date))
											<td class="border-class p-5" style="width: 2%;">
												{{ isset($courses['expiry_date']) && !empty($courses['expiry_date']) ? date('d/m/Y',strtotime($courses['expiry_date'])) : '-'}}
											</td>
											@endif
											@if(!empty($issue_by))
											<td class="border-class p-5" style="width: 2%;">
												{{ isset($courses['issue_by']) && !empty($courses['issue_by']) ? $courses['issue_by'] : '-'}}
											</td>
											@endif
										</tr>
									@endforeach
								@endif

								@if((isset($data['value_added_course_detail']) AND !empty($data['value_added_course_detail'])))
									@foreach($data['value_added_course_detail'] as $index => $courses)
										<tr class="f-10">
											<td class="border-class p-5" style="width: 2%;">
												@foreach( \CommonHelper::value_added_courses() as $c_index => $course)
													{{ isset($courses['course_id']) ? $courses['course_id'] == $c_index ? $course : '' : ''}}
												@endforeach
											</td>
											@if(!empty($certification))
												<td class="border-class p-5" style="width: 2%;">
													{{ isset($courses['certification_number']) && !empty($courses['certification_number']) ? $courses['certification_number'] : '-'}}
												</td>
											@endif
											@if(!empty($issue_date))
												<td class="border-class p-5" style="width: 2%;">
													{{ isset($courses['issue_date']) && !empty($courses['issue_date']) ? date('d/m/Y',strtotime($courses['issue_date'])) : '-'}}
												</td>
											@endif
											@if(!empty($expiry_date))
												<td class="border-class p-5" style="width: 2%;">
													{{ isset($courses['expiry_date']) && !empty($courses['expiry_date']) ? date('d/m/Y',strtotime($courses['expiry_date'])) : '-'}}
												</td>
											@endif
											@if(!empty($issue_by))
												<td class="border-class p-5" style="width: 2%;">
													{{ isset($courses['issue_by']) && !empty($courses['issue_by']) ? $courses['issue_by'] : '-'}}
												</td>
											@endif
										</tr>
									@endforeach
								@endif
							</tbody>
						</table>
					</td>
				</tr>
				@endif

				@if(isset($data['professional_detail']['other_exp']) && !empty($data['professional_detail']['other_exp']))
				<tr>
					<td>	
						<table cellpadding="5" style="border: 1px solid black;border-radius: 8px;overflow: hidden;background-color: black;">
							<tbody style="background-color: #ffffff;">
								<tr>
									<th colspan="1">
										<span style="font-size: 16px;">Other Experience</span>
									</th>
								</tr>
								<tr class="f-10">
								    <td class="border-class" style="width: 100%;">
								    	{{$data['professional_detail']['other_exp']}}
								    </td>
								</tr>
							</tbody>
						</table>
					</td>
				</tr>
				@endif

			</tbody>

		</table>
	</body>
</html>