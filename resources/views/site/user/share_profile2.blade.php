@extends('site.index')
@section('content')
    <?php
    $india_value = array_search('India', CommonHelper::countries());
    if (isset($data[0]['professional_detail']['current_rank'])) {
        $required_fields = CommonHelper::rank_required_fields()[$data[0]['professional_detail']['current_rank']];
    }
    if (isset($data[0]['document_permissions']) && !empty($data[0]['document_permissions'])) {
        $document_permissions = $data[0]['document_permissions'];
    }
    if ($data[0]['registered_as']) {
        $registered_as = $data[0]['registered_as'];
    }
    $gender = "M";
    if(isset($data[0]['gender']) && $data[0]['gender'] == 'F'){
        $gender = 'F';
    }
    ?>
    <link rel="stylesheet" href="{{ URL:: asset('public/css/doctable.css')}}">
    <style>
        h1 {
            display: block;
            font-size: 50px;
            margin: 25px auto 0;
            width: 975px;
        }

        .profile-content > small {
            color: #aaaaaa;
            font-size: 14px;
        }

        /*.profile-content>p>small{*/
        /*    color: #aaaaaa;*/
        /*}*/

        header {
            box-shadow: 1px 1px 4px rgba(0, 0, 0, 0.5);
            /*margin:   25px auto 50px;*/
            height: 70px;
            position: relative;
            width: auto;
        }

        figure.profile-banner {
            left: 0;
            overflow: hidden;
            position: absolute;
            top: 0;
            z-index: 1;
        }

        .seperator {
            border-bottom: 1px dotted #000000;
        }
        
        @media (min-width: 992px) {
            .nav-mobile {
                display: none;
            }
        }
        @media (min-width: 768px) {
            

            .row.contentBoxes {
                text-align: center;
                padding-left: 20px;
            }

            figure.profile-picture {
                background-position: center center;
                background-size: cover;
                border: 5px #efefef solid;
                border-radius: 50%;
                bottom: -50px;
                box-shadow: inset 1px 1px 3px rgba(0, 0, 0, 0.2), 1px 1px 4px rgba(0, 0, 0, 0.3);
                height: 150px;
                margin-left: 95px;
                /*position: absolute;*/
                width: 150px;
                z-index: 3;
                margin-top: -73px;
            }

            .profile-content {
                padding-left: 23px;
            }
        }
        @media (max-width: 991px) {
            ul.nav-mobile {
                display: inline-flex;
                background: black;
                width: 100%;
                margin-top: 14px;
            }

            .nav-mobile li a {
                color: white;
                font-size: 11px;
            }

            .nav-mobile li {
                color: white;
                padding: 8px;
                margin: 1px;
            }

            ul.nav.navbar-right.top-nav {
                display: none;
            }
        }
        @media (max-width: 768px) {
            .navbar-inverse .navbar-toggle .icon-bar {
                background-color: #337ab7;
                width: 20px;
                height: 2px;
            }
            .navbar-inverse .navbar-toggle:hover, .navbar-inverse .navbar-toggle:focus{
                background-color: transparent;
            }

            .navbar-inverse {
                background-color: white;
                background-image: none !important;
                min-height: 1px !important;
            }

            .navbar-inverse .navbar-toggle {
                border-color: #333;
                position: absolute;
                top: -52px;
                z-index: 10000;
                left: -4px;
                color: skyblue;
                border: 0 !important;
            }

            /*nav.navbar.navbar-inverse {*/
            /*    display: none;*/
            /*}*/
            span.navbar-toggler-icon {
                display: none;
            }

            ul.nav-mobile {
                display: inline-flex;
                background: black;
                width: 100%;
                margin-top: 14px;
            }

            .nav-mobile li a {
                color: white;
                font-size: 11px;
            }

            .nav-mobile li {
                color: white;
                padding: 8px;
                margin: 1px;
            }

            ul.nav.navbar-right.top-nav {
                display: none;
            }


            li.course_list_item {
                font-size: 11px;
            }

            .row.contentBoxes {
                text-align: center;
                padding: 35px;
            }

            .profile-details {
                margin-top: 40px;
                text-align: left;
                margin: 17px 40px 4px 29px;
            }

            .profile-content {
                padding-left: 51px;
            }

            figure.profile-picture {
                background-position: center center;
                background-size: cover;
                border: 5px #efefef solid;
                border-radius: 50%;
                bottom: -50px;
                box-shadow: inset 1px 1px 3px rgba(0, 0, 0, 0.2), 1px 1px 4px rgba(0, 0, 0, 0.3);
                height: 150px;
                margin-left: 95px;
                /*position: absolute;*/
                width: 150px;
                z-index: 3;
                margin-top: 25px;
            }
        }

        /*figure.profile-picture {*/
        /*    background-position: center center;*/
        /*    background-size: cover;*/
        /*    border: 5px #efefef solid;*/
        /*    border-radius: 50%;*/
        /*    bottom: -50px;*/
        /*    box-shadow: inset 1px 1px 3px rgba(0, 0, 0, 0.2), 1px 1px 4px rgba(0, 0, 0, 0.3);*/
        /*    height: 150px;*/
        /*    margin-left: 95px;*/
        /*    !*position: absolute;*!*/
        /*    width: 150px;*/
        /*    z-index: 3;*/
        /*    margin-top: -73px;*/
        /*}*/

        div.profile-stats {
            top: 60px;
            left: 46px;
            padding: 38px 15px 24px 350px;
            position: absolute;
            right: 46px;
            z-index: 2;

            background: #043559;
            /* Generated Gradient */
            /*background: -moz-linear-gradient(top,  rgba(255,255,255,0.5) 0%, rgba(0,0,0,0.51) 3%, rgba(0,0,0,0.75) 61%, rgba(0,0,0,0.5) 100%);*/
            /*background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(255,255,255,0.5)), color-stop(3%,rgba(0,0,0,0.51)), color-stop(61%,rgba(0,0,0,0.75)), color-stop(100%,rgba(0,0,0,0.5)));*/
            /*background: -webkit-linear-gradient(top,  rgba(255,255,255,0.5) 0%,rgba(0,0,0,0.51) 3%,rgba(0,0,0,0.75) 61%,rgba(0,0,0,0.5) 100%);*/
            /*background: -o-linear-gradient(top,  rgba(255,255,255,0.5) 0%,rgba(0,0,0,0.51) 3%,rgba(0,0,0,0.75) 61%,rgba(0,0,0,0.5) 100%);*/
            /*background: -ms-linear-gradient(top,  rgba(255,255,255,0.5) 0%,rgba(0,0,0,0.51) 3%,rgba(0,0,0,0.75) 61%,rgba(0,0,0,0.5) 100%);*/
            /*background: linear-gradient(to bottom,  rgba(255,255,255,0.5) 0%,rgba(0,0,0,0.51) 3%,rgba(0,0,0,0.75) 61%,rgba(0,0,0,0.5) 100%);*/
            filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#80ffffff', endColorstr='#80000000', GradientType=0);

        }

        div.profile-stats ul {
            list-style: none;
            margin: 0;
            padding: 0;
        }

        div.profile-stats ul li {
            color: #efefef;
            display: block;
            float: left;
            font-size: 14px;
            font-weight: bold;
            margin-right: 50px;
            text-shadow: 1px 1px 2px rgba(0, 0, 0, 0.7)
        }

        div.profile-stats li span {
            display: block;
            font-size: 16px;
            font-weight: normal;
        }

        div.profile-stats a.follow {
            display: block;
            float: right;
            color: #ffffff;
            margin-top: 5px;
            text-decoration: none;

            /* This is a copy and paste from Bootstrap */
            background-color: #49afcd;
            text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.25);
            background-color: #49afcd;
            background-image: -moz-linear-gradient(top, #5bc0de, #2f96b4);
            background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#5bc0de), to(#2f96b4));
            background-image: -webkit-linear-gradient(top, #5bc0de, #2f96b4);
            background-image: -o-linear-gradient(top, #5bc0de, #2f96b4);
            background-image: linear-gradient(to bottom, #5bc0de, #2f96b4);
            background-repeat: repeat-x;
            border-color: #2f96b4 #2f96b4 #1f6377;
            border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.25);
            filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ff5bc0de', endColorstr='#ff2f96b4', GradientType=0);
            filter: progid:DXImageTransform.Microsoft.gradient(enabled=false);
            display: inline-block;
            padding: 4px 12px;
            margin-bottom: 0;
            font-size: 14px;
            line-height: 20px;
            box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.2), 0 1px 2px rgba(0, 0, 0, 0.05);
            border-radius: 4px;
        }

        .profile-name > small {
            /*display: flex;*/
            font-size: small;
        }

        div.profile-stats a.follow.followed {

            /* Once again copied from Boostrap */
            color: #ffffff;
            text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.25);
            background-color: #5bb75b;
            background-image: -moz-linear-gradient(top, #62c462, #51a351);
            background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#62c462), to(#51a351));
            background-image: -webkit-linear-gradient(top, #62c462, #51a351);
            background-image: -o-linear-gradient(top, #62c462, #51a351);
            background-image: linear-gradient(to bottom, #62c462, #51a351);
            background-repeat: repeat-x;
            border-color: #51a351 #51a351 #387038;
            border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.25);
            filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ff62c462', endColorstr='#ff51a351', GradientType=0);
            filter: progid:DXImageTransform.Microsoft.gradient(enabled=false);
        }


        .profile-content > h2 {
            color: #354B63;
            font-size: 23px;
            margin-bottom: 5px;
            /*padding-top: 5px;*/
            /*left: 350px;*/
            /*position: absolute;*/
            /*z-index: 5;*/
            /*padding-top: 4%;*/
        }

        .smClass {
            color: #aaaaaa;
            font-size: .5em;
        }

        .height-155 {
            height: 300px;
        }

        #wrapper-page {
            padding-left: 0;
        }

        #page-wrapper {
            width: 100%;
            padding: 0;
            background-color: #fff;
        }

        @media (min-width: 768px) {
            #wrapper-page {
                padding-left: 208px;
            }

            #page-wrapper {
                padding: 22px 10px;
            }
        }

        
        /* Top Navigation */


        .top-nav > li {
            display: inline-block;
            float: left;
        }

        .top-nav > li > a {
            padding-top: 20px;
            padding-bottom: 20px;
            line-height: 20px;
            color: #fff;
        }

        .top-nav > li > a:hover,
        .top-nav > li > a:focus,
        .top-nav > .open > a,
        .top-nav > .open > a:hover,
        .top-nav > .open > a:focus {
            color: #fff;
            background-color: #043559;
        }

        .top-nav > .open > .dropdown-menu {
            float: left;
            position: absolute;
            margin-top: 0;
            /*border: 1px solid rgba(0,0,0,.15);*/
            border-top-left-radius: 0;
            border-top-right-radius: 0;
            background-color: #fff;
            -webkit-box-shadow: 0 6px 12px #043559;
            box-shadow: 0 6px 12px #043559;
        }

        .top-nav > .open > .dropdown-menu > li > a {
            white-space: normal;
        }

/*        .user-data{
            margin-left: 100px !important;
        }*/
        /* Side Navigation */

        @media (min-width: 768px) {

            .top-nav {
                padding: 0 15px 0px 15px;
                float: right !important;
            }

            .side-nav {
                position: fixed;
                top: 0px;
                padding-top: 100px;
                left: 225px;
                width: 225px;
                margin-left: -225px;
                border: none;
                border-radius: 0;
                border-top: 1px #043559 solid;
                overflow-y: auto;
                background-color: #043559;
                /*background-color: #5A6B7D;*/
                bottom: 0;
                overflow-x: hidden;
                padding-bottom: 40px;
            }

            .side-nav > li > a {
                width: 225px;
                border-bottom: #043559;
            }

            .side-nav li a:hover,
            .side-nav li a:focus {
                outline: none;
                background-color: #043559 !important;
            }
        }

        .side-nav > li > ul {
            padding: 0;
            border-bottom: 1px #043559 solid;
        }

        .side-nav > li > ul > li > a {
            display: block;
            padding: 10px 15px 10px 38px;
            text-decoration: none;
            /*color: #999;*/
            color: #fff;
        }

        .side-nav > li > ul > li > a:hover {
            color: #fff;
        }

        .navbar .nav > li > a > .label {
            -webkit-border-radius: 50%;
            -moz-border-radius: 50%;
            border-radius: 50%;
            position: absolute;
            top: 14px;
            right: 6px;
            font-size: 10px;
            font-weight: normal;
            min-width: 15px;
            min-height: 15px;
            line-height: 1.0em;
            text-align: center;
            padding: 2px;
        }

        .navbar .nav > li > a:hover > .label {
            top: 10px;
        }

        a.expSpan {
            /* position: absolute;
            right: 32px;
            font-weight: 600;
            padding: 9px; */
            right: 32px;
            font-weight: 600;
            padding: 9px;
            /* padding: 91px 111px 49px 59px; */
            margin-left: 146px;
            /* margin-top: 51px; */
            margin-bottom: 23px;
            padding: 10px;
        }

        .my-profile {
            padding: 37px 0 80px 0 !important;
        }

        .container-fluid {
            /*margin-right: 0 !important;*/
            padding-right: 0 !important;
        }

        .cs-header {
            background: #043559;
        }

        .navbar-inverse .navbar-collapse, .navbar-inverse .navbar-form {
            background-color: #043559 !important;
            border-color: #043559 !important;
        }

        .navbar {
            margin-top: 18px;
            border: 0 !IMPORTANT;
        }

        ::-webkit-scrollbar {
            width: 5px;
        }

        /* Track */
        ::-webkit-scrollbar-track {
            background: #3399FF;
        }

        /* Handle */
        ::-webkit-scrollbar-thumb {
            background: #5ad3ff;
        }

        /* Handle on hover */
        ::-webkit-scrollbar-thumb:hover {
            background: #5ad3ff;
        }


        .profile-content h3 {
            margin-top: 6px;
            font-size: 20px;
        }

        .boxdata {
            width: 100%;
            margin-left: 6px;
            padding-right: 26px;
        }

        .card-header {
            background-color: #043559;
            border-radius: 0 !important;
        }

        .profile-details p {
            margin: 0 0 15px;
            color: #2ab7e6;
            font-size: 16px;
        }

        .button-text {
            margin-top: 10px;
            padding: 6px 50px;
            background: #34b5f2;
            border: 0 !important;
            color: white;
            font-size: 16px;
        }

        .hrLine {
            margin: 20px 10px 20px 10px;
        }

        .contentline {
            padding: 14px;
            font-style: italic;
            margin-bottom: 0 !important;
        }

        .contentBoxes .col-md-2 {
            /* border: 1px solid #dedede; */
            padding: 8px;
            margin: 15px;
            width: 180px;
        }


        .contentBoxes .col-md-2:hover {
            box-shadow: 0 3px 5px 0 #34B5F2;
            transition: box-shadow 0.3s ease-in-out;
        }

        .contentBoxes .box {
            text-align: center;
        }

        .contentBoxes .box img {
            width: 80%;
        }

        i.fa.fa-check-circle {
            color: green;
        }

        .profile-details b, .profile-content h3 {
            color: #043559;
        }

        p.tag-line {
            margin-top: 15px;
            margin-bottom: 1px;
        }

        .experience-box {
            border: 1px solid #dedede;
        }

        .experience-box .header {
            font-weight: 500;
            background: #fbf8f8;
            padding: 11px 13px 13px 13px;
            border-bottom: 1px solid #dedede;
            font-size: 18px;
        }
        .blackHr{
            border: 1px solid #dcdcdc;
        }

        .tags span {
            background: lightgray;
            padding: 3px 4px;
            font-size: 11px;
            border-radius: 3px;
            margin: 4px;
            letter-spacing: 1.4px;
            cursor: -webkit-grab;
            cursor: pointer;
            line-height: 27px;
        }

        .progress {
            width: 150px;
            margin-top: 13px;
        }

        .storage span {
            color: white;
            font-weight: 500;
        }

        .progress-bar {
            font-size: 11px;
        }

        .storage {
            padding: 15px;
        }

        .tags {
            padding: 10px;
        }

        .other-service {
            width: 100%;
            height: 165px;
            border-radius: 18px;
            background: #F1F3F7;
            border: 1px solid lightgray;
        }

        .row.seaService {
            background: #f1f3f7;
            margin: 1px;
        }
        .user-type-menu{
            color: skyblue !important;
        }
        .font-0{
            font-size: 0px !important;
        }
        .course-title{
            padding:12px !important;
        }
        .border-bottom-line{
            border-top:1px solid #a9b6cb !important;
        }
        .col-md-6.work-box {
            border: 3px solid #52c1f4;
            background: #dedede;
            border-radius: 15px;
            padding: 10px;
            position: absolute;
            left: 384px;
            z-index: 100;
        }
        .work-box .header {
            padding-bottom: 4px;
            border-bottom: 1px dashed black;
            color: #043559;
            font-weight: 600;
            margin-bottom: 6px;
            text-transform: uppercase;
        }
        .bodyBox p {
            font-size: 13.5px;
            color: #000000;
        }
        .d-none{
            display: none !important;
        }
        .padding-0{
            padding: 0px !important;
        }
        .user-kb{
            font-weight : normal !important;
        }
        .sub-title {
            font-size: 15px;
        }
        .title {
            font-size: 15px;   
        }

        @media (max-width: 1200px) {
            .main {
                background-color: #ffffff !important;
            }
        }
        .m-l-10{
            margin-left: 10px;
        }
        .active-span{
            background-color: #0d609c !important;
            color: white;
        }
    </style>
    <main class="main">
        <input type="hidden" class="user_id" value="{{ isset($user_id) ? $user_id : ''}}">
        <input type="hidden" class="share_user_id" value="{{ isset($user_id) ? $user_id : ''}}">
        <input type="hidden" name="verify_email" id="verify_email"
               value="{{isset($data[0]['is_email_verified']) ? $data[0]['is_email_verified'] : ''}}">
        @if(isset($data[0]['personal_detail']['country']) && $data[0]['personal_detail']['country'] == $india_value)
            <input type="hidden" name="verify_mobile" id="verify_mobile"
                   value="{{isset($data[0]['is_mob_verified']) ? $data[0]['is_mob_verified'] : ''}}">
        @endif
        <div class="my-profile">
            <div class="container">
                <div id="">
                    <!-- Navigation -->
                    <nav class="navbar navbar-inverse" role="navigation">
                        <!-- Brand and toggle get grouped for better mobile display -->
<!--                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse"
                                    data-target=".navbar-ex1-collapse">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>-->
                        <!-- Top Menu Items -->
                        <ul class="nav navbar-right top-nav">
                            <li><a href="#" class="user-type user-type-menu" data-type="contact" data-placement="bottom" data-toggle="tooltip"
                                data-original-title="Stats">Contact
                             </a>
                         </li>
                         <li><a href="#" class="user-type" data-type="personal" data-placement="bottom" data-toggle="tooltip"
                                data-original-title="Stats">Personal
                             </a>
                         </li>
                         <li><a href="#" class="user-type" data-type="professional" data-placement="bottom" data-toggle="tooltip"
                                data-original-title="Stats">Professional
                             </a>
                         </li>
                         <li><a href="#" class="user-type"  data-type="personality" data-placement="bottom" data-toggle="tooltip"
                                data-original-title="Stats">Personality
                             </a>
                         </li>
                        </ul>
                        <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
                        
                        <!-- /.navbar-collapse -->
                    </nav>

                    <div id="page-wrapper">
                        <div class="container-fluid">
                            <!-- Page Heading -->
                            <div class="row" id="main">
                                <div class="section">
                                    <div class="row">
                                        <div class="col-md-6 col-sm-12">
                                            @if(isset($data[0]['profile_pic']) && !empty($data[0]['profile_pic']))
                                                <figure class="profile-picture"
                                                        style="{{'background-image: url('.env('AWS_URL') . 'public/images/uploads/seafarer_profile_pic/'.$data[0]['id'].'/'.$data[0]['profile_pic'].')'}}">
                                                </figure>
                                            @else
                                                @if($gender == 'M')
                                                    <figure class="profile-picture"
                                                        style="{{'background-image: url('.url('public/assets/images/default-user-male.png').')'}}">
                                                    </figure>
                                                @else
                                                    <figure class="profile-picture"
                                                        style="{{'background-image: url('.url('public/assets/images/defualt-user-female.png').')'}}">
                                                    </figure>
                                                @endif
                                            @endif
                                            <div class="profile-content">
                                                <h2 class="profile-name">{{isset($data[0]['title']) ? $data[0]['title'] : ''}} {{isset($data[0]['first_name']) ? $data[0]['first_name'] : ''}} {{$data[0]['PPMname']}} {{$data[0]['PPLname']}}
                                                    <small class="smClass"><i class="fa fa-globe">&nbsp;</i>{{$country[$userPersonalData->nationality]}}</small>
                                                </h2>
                                                <small>
                                                    <span style="color: #2ab7e6; font-weight:bold">
                                                    {{$data[0]['where_in'] == 'Ship' ? 'On Ship' : 'At Home'}}
                                                    </span>
                                                    <span>
                                                        @php $homeships = gethomeship(); @endphp
                                                        @if(!empty($homeships))
                                                            @foreach($homeships as $homeship)
                                                                @if($homeship->id == $data[0]['shiphometype'])
                                                                : {{ $homeship->typevalue }}
                                                                @endif
                                                            @endforeach
                                                        @endif
                                                    </span>
                                                </small>
                                                <h3 class="form-group" style="margin-bottom: 10px;margin-top: 10px;">
                                                    @foreach(\CommonHelper::new_rank() as $index => $category)
                                                        @foreach($category as $r_index => $rank)
                                                            {{ !empty($userProData['current_rank']) ? $userProData['current_rank'] == $r_index ? $rank : '' : ''}}
                                                        @endforeach
                                                    @endforeach
                                                </h3>
                                                <?php 
                                                    $showResume = 3;
                                                    if(isset($show_resume)){
                                                        $showResume = $show_resume;
                                                    }
                                                    $showResume = \CommonHelper::encodeKey($showResume);
                                                    $userIdEncodeKey = \CommonHelper::encodeKey($user_id);
                                                ?>
                                                <a href="{{route('create-pdf',['resume'=>$showResume, 'user' => $userIdEncodeKey])}}" class="btn btn-success ">
                                                    <i class="fa fa-file" aria-hidden="true"></i>&nbsp;&nbsp;Checkout my resume
                                                </a>
                                                <br>
                                                <br>
                                                
                                            </div>
                                        </div>
                                        <div class="col-sm-12 nav-mobile">
                                            <ul class="nav-mobile">
                                                <li><a href="#" class="user-type user-type-menu" data-type="contact" data-placement="bottom" data-toggle="tooltip"
                                                       data-original-title="Stats">Contact
                                                    </a>
                                                </li>
                                                <li><a href="#" class="user-type" data-type="personal" data-placement="bottom" data-toggle="tooltip"
                                                       data-original-title="Stats">Personal
                                                    </a>
                                                </li>
                                                <li><a href="#" class="user-type" data-type="professional" data-placement="bottom" data-toggle="tooltip"
                                                       data-original-title="Stats">Professional
                                                    </a>
                                                </li>
                                                <li><a href="#" class="user-type"  data-type="personality" data-placement="bottom" data-toggle="tooltip"
                                                       data-original-title="Stats">Personality
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="col-md-6 col-sm-12">
                                            <div class="user-data"></div>
                                        </div>
                                    </div>
                                    <hr class="hrLine">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <p class="contentline m-l-10">
                                                {{$data[0]['professional_detail']['about_me']}}
                                            </p>
                                        </div>
                                    </div>
                                    <hr class="hrLine">
                                    <div class="row contentBoxes">
                                        <div class="col-md-2 col-sm-12">
                                            <?php 
                                                $encodeUserId = $user_id;
                                                if(is_numeric($encodeUserId)){
                                                    $encodeUserId = \CommonHelper::encodeKey($user_id);
                                                }
                                            ?>
                                            <a href="{{route('share-documents',['documents'=> $encodeUserId])}}">
                                                <div class="box">
                                                    <img src="{{URL::to('public/images/my-documents.png')}}" alt="Italian Trulli" height="120px" width="40px">
                                                    <p class="tag-line">My Documents</p>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="col-md-2 col-sm-12">
                                            <a href="{{route('share-experience-onboard',['share-experience-onboard'=> $encodeUserId])}}">
                                                <div class="box">
                                                    <img src="{{URL::to('public/images/onboard-experience.png')}}" alt="Onboard Experience" height="120px" width="40px">
                                                    <p class="tag-line">Onboard Experience</p>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="col-md-2 col-sm-12">
                                            <a href="{{route('share-graph',['graph'=>$encodeUserId])}}">
                                                <div class="box">
                                                    <img src="{{URL::to('public/images/skill-infographic.png')}}" alt="Italian Trulli" height="120px" width="40px">
                                                    <p class="tag-line">Skill Infographics</p>
                                                </div>
                                            </a>
                                        </div>
                                        
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="card boxdata">
                                                <div class="card-header" style="padding:0;">
                                                    <ul class="nav nav-tabs course_list" role="tablist">
                                                        <li role="presentation" class="course_list_item active">
                                                            <a href="#gi"  class="docs-type-value" data-id="all"aria-controls="gi" role="tab"
                                                               data-toggle="tab">Document Details</a>
                                                        </li>
                                                        <li role="presentation" class="course_list_item">
                                                            <a href="#cd" class="course-type-value active-span" data-id="all" aria-controls="cd" role="tab"
                                                               data-toggle="tab">Course Details</a>
                                                        </li>
                                                        <li role="presentation" class="course_list_item">
                                                            <a href="#ssd" class="ship-type-value active-span" data-id="all" aria-controls="ssd" role="tab"
                                                               data-toggle="tab">Sea Service Details</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="card-body z-depth-1">
                                                    <div class="tab-content">
                                                        <div role="tabpanel" class="tab-pane active" id="gi">
                                                            <div class="content">
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <div class="experience-box">
                                                                            <div class="header">
                                                                                Documents
                                                                            </div>
                                                                            <div class="tags">
                                                                                @foreach($docsArray as $key=>$row)
                                                                                    <span class="docs-type-value" data-id="{{$key}}">{{$row}}</span>
                                                                                @endforeach
                                                                            </div>


                                                                        </div>
                                                                    </div>
                                                                    <hr>
                                                                    {{-- <div class="col-lg-12">

                                                                        <div class="content-title"
                                                                             style="border-top:1px solid #a9b6cb;">
                                                                            <div class="d-flex block-small">
                                                                                <div class="d-flex--one">
                                                                                    <span class="dark-blue title">Document Details</span>
                                                                                </div>
                                                                                @if($user != 'another_user')
                                                                                    <div class="d-flex--two">
                                                                                        <button href="{{route('site.seafarer.edit.profile')}}" class="btn cs-primary-btn edit_link" data-form='documents'><i class="fa fa-pencil-square-o">Edit</i></button>
                                                                                    </div>
                                                                                @endif
                                                                            </div>
                                                                        </div>
                                                                    </div> --}}
                                                                </div>
                                                                <div class="docs-data">
                                                                    {{-- here docs data include --}}
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div role="tabpanel" class="tab-pane" id="ssd">
                                                            <div class="content">
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <div class="experience-box">
                                                                            <div class="header">
                                                                                Experience on
                                                                            </div>
                                                                            <div class="tags">
                                                                                {{-- {{dd($userProData)}} --}}
                                                                                
                                                                                @foreach($shipType as $key=>$row)
                                                                                    <span class="ship-type-value" data-id="{{$row->ship_type}}">{{$shipData[$row->ship_type]}}</span>
                                                                                @endforeach
                                                                                @if(!empty($userProData->other_exp))
                                                                                    <hr>
                                                                                    <span class="ship-type-value " data-id="other_details">Other Services</span>
                                                                                @endif
                                                                                @if($userPassData->fromo == 1)
                                                                                    <hr>
                                                                                    <span style="cursor: not-allowed;">Experience in FRAMO</span>
                                                                                @endif
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    {{-- <div class="col-lg-12 sea-service-details">
                                                                        <div class="content-title"
                                                                            style="border-top:1px solid #a9b6cb;">
                                                                            <div class="d-flex block-small">
                                                                                <div class="d-flex--one">
                                                                                    <span class="dark-blue title">SEA SERVICE</span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div> --}}
                                                                </div>
                                                                <div class="ship-type-data">
                                                                   {{-- here data include --}}
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div role="tabpanel" class="tab-pane" id="cd">
                                                            <div class="content">
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <div class="experience-box">
                                                                            <div class="header">
                                                                                Courses
                                                                            </div>
                                                                            <div class="tags">
                                                                                {{-- {{dd($userProData)}} --}}
                                                                                @if($userProData->institute_degree)
                                                                                    <span class="course-type-value" data-id="institute_degree">{{$userProData->institute_degree}}</span>
                                                                                    <hr>
                                                                                @endif
                                                                                @foreach($userCourses as $key=>$row)
                                                                                {{-- {{dump($coursesName)}} --}}
                                                                                @php
                                                                                    $word = $coursesName[$row->course_id];
                                                                                    $stringContainRoundBracket = strpos($coursesName[$row->course_id], '(');
                                                                                    $stringContainRoundBracket = strpos($coursesName[$row->course_id], ')');
                                                                                    if ($stringContainRoundBracket) {
                                                                                        $word = substr($word, strpos($word, "(") + 1);
                                                                                        $word = substr($word, 0, strpos($word, ")"));
                                                                                    }
                                                                                    // dd($variable);
                                                                                @endphp
                                                                                    <span class="course-type-value" data-id="{{$row->course_id}}">{{$word}}</span>
                                                                                @endforeach
                                                                                
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    {{-- <div class="col-lg-12 pre-sea-training">
                                                                        <div class="content-title"
                                                                             style="border-top:1px solid #a9b6cb;">
                                                                            <div class="d-flex block-small">
                                                                                <div class="d-flex--one">
                                                                                    <span class="dark-blue title">PRE SEA TRAINING</span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div> --}}
                                                                </div>
                                                                @if(isset($data[0]['course_detail']) AND !empty($data[0]['course_detail']))
                                                                        {{-- <div class="row seaService pre-sea-training" style="margin-top:15px;">
                                                                            @if($userProData->institute_degree)
                                                                                <div class="col-sm-3">
                                                                                    <div class="list">
                                                                                        <div class="list-item">
                                                                                            <div class="text"><span class="black">Degree</span> :
                                                                                                <span class="blue">
                                                                                                    {{$userProData->institute_degree}}
                                                                                                </span>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            @endif
                                                                            @if($userProData->institute_name)
                                                                                <div class="col-sm-2">
                                                                                    <div class="list">
                                                                                        <div class="list-item">
                                                                                            <div class="text"><span class="black">Institute</span> :
                                                                                                <span class="blue">
                                                                                                    {{$userProData->institute_name}}
                                                                                                </span>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            @endif
                                                                            @if($userProData->institute_to)
                                                                                <div class="col-sm-2">
                                                                                    <div class="list">
                                                                                        <div class="list-item">
                                                                                            <div class="text"><span class="black">Pass Out</span> :
                                                                                                <span class="blue">
                                                                                                    {{$userProData->institute_to}}
                                                                                                </span>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            @endif
                                                                        </div> --}}
                                                                        
                                                                        
                                                                    <div class="course-data">
                                                                        
                                                                    </div>
                                                                @else
                                                                    <div class="row" style="margin-top:15px;">
                                                                        <div class="col-sm-12 ">
                                                                            <div class="list">
                                                                                <div class="list-item">
                                                                                    <div class="text ">
                                                                                        No Data Found
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                @endif

                                                                {{-- <div class="row">
                                                                    <div class="col-lg-12">
                                                                        <div class="content-title"
                                                                             style="border-bottom:1px solid #a9b6cb;">
                                                                            <div class="d-flex block-small">
                                                                                <div class="d-flex--one">
                                                                                    <span class="dark-blue title">ADD-ON COURSE DETAILS</span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div> --}}
                                                                @if(isset($data[0]['value_added_course_detail']) AND !empty($data[0]['value_added_course_detail']))
                                                                    @foreach($data[0]['value_added_course_detail'] as $index => $courses)
                                                                        <div class="row" style="margin-top:15px;">
                                                                            <div class="col-sm-12">
                                                                                <div style="font-weight:bold;">
                                                                                    CERTIFICATE {{$index+1}}</div>
                                                                            </div>
                                                                            <div class="col-sm-4">
                                                                                <div class="list">
                                                                                    <div class="list-item">
                                                                                        <div class="text"><span class="black">Certificate Name</span> :
                                                                                            <span class="blue">
		                                                            		                    @foreach( CommonHelper::value_added_courses() as $c_index => $course)
                                                                                                    {{ isset($courses['course_id']) ? $courses['course_id'] == $c_index ? $course : '' : ''}}
                                                                                                @endforeach
                                                                                            </span>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="list-item">
                                                                                        <div class="text"><span class="black">Date Of Expiry</span>:
                                                                                            <span class="blue">
                                                                                                {{ isset($courses['expiry_date']) && !empty(isset($courses['expiry_date'])) ? date('d-m-Y',strtotime($courses['expiry_date'])) : '-'}}
                                                                                            </span>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-sm-4">
                                                                                <div class="list">
                                                                                    <div class="list-item">
                                                                                        <div class="text"><span class="black">Certificate Number</span> :
                                                                                            <span class="blue">
		                                                            		                    {{ isset($courses['certification_number']) && !empty($courses['certification_number'])? $courses['certification_number'] : '-'}}
		                                                            	                    </span>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="list-item">
                                                                                        <div class="text"><span class="black">Issued By</span> :
                                                                                            <span class="blue">
		                                                            		                    {{ isset($courses['issue_by']) && !empty($courses['issue_by']) ? $courses['issue_by'] : '-'}}
                                                                                            </span>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-sm-4">
                                                                                <div class="list">
                                                                                    <div class="list-item">
                                                                                        <div class="text"><span class="black">Date Of Issue</span> :
                                                                                            <span class="blue">
                                                                                                {{ isset($courses['issue_date']) && !empty($courses['issue_date']) ? date('d-m-Y',strtotime($courses['issue_date'])) : '-'}}
                                                                                            </span>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row">
                                                                            <hr>
                                                                        </div>
                                                                    @endforeach
                                                                @else
                                                                    {{-- <div class="row" style="margin-top:15px;">
                                                                        <div class="col-sm-12 ">
                                                                            <div class="list">
                                                                                <div class="list-item">
                                                                                    <div class="text ">
                                                                                        No Data Found
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div> --}}
                                                                @endif

                                                                {{-- @if(isset($data[0]['professional_detail']['other_exp']) && !empty($data[0]['professional_detail']['other_exp']))
                                                                    <div class="row">
                                                                        <div class="col-lg-12">
                                                                            <div class="content-title"
                                                                                 style="border-bottom:1px solid #a9b6cb;">
                                                                                <div class="d-flex block-small">
                                                                                    <div class="d-flex--one">
                                                                                        <span class="dark-blue title">OTHER EXPERIENCE</span>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-sm-12">
                                                                            <div class="list">
                                                                                <div class="list-item">
                                                                                    <div class="text">
                                                                                        {{$data[0]['professional_detail']['other_exp']}}
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                @endif --}}
                                                            </div>
                                                        </div>
                                                        <div role="tabpanel" class="tab-pane" id="user_document">


                                                            <?php
                                                            $download_all_path = route('site.user.get.documents.path', ['user_id' => $data[0]['id']]);
                                                            $current_route = Route::currentRouteName();
                                                            ?>

                                                            @if(isset($registered_as) && ($registered_as != 'advertiser'))
                                                                <div class="tab-content">
                                                                    <div id="upload_documents"
                                                                         class="tab-pane fade in active">
                                                                        @if($current_route == 'user.view.profile')
                                                                            <input type="hidden" class="profile_status"
                                                                                   value="view profile">

                                                                        @endif

                                                                        @if($current_route == 'user.profile')
                                                                            <input type="hidden" class="profile_status"
                                                                                   value="own profile">
                                                                            <div class="documents_section">

                                                                            </div>
                                                                        @endif
                                                                    </div>

                                                                </div>
                                                            @endif
                                                        </div>
                                                        <div role="tabpanel" class="tab-pane" id="notifi">
                                                            <div class="row" style="margin-top:15px;">
                                                                <div class="col-sm-12 ">
                                                                    <div class="list">
                                                                        <div class="list-item">
                                                                            <div class="text ">
                                                                                No Notifications Found
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div role="tabpanel" class="tab-pane" id="td">
                                                            <div class="content">
                                                                <div class="row">
                                                                    <div class="col-lg-12">
                                                                        <div class="content-title"
                                                                             style="border-bottom:1px solid #a9b6cb;">
                                                                            <div class="d-flex block-small">
                                                                                <div class="d-flex--one">
                                                                                    <span class="dark-blue title">Tracking Details</span>
                                                                                </div>
                                                                                <div class="d-flex--two">
                                                                                    <a href=""
                                                                                       class="btn cs-primary-btn">Edit</a>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row" style="margin-top:15px;">
                                                                    <div class="col-sm-6">
                                                                        <div style="font-weight:bold;">Course Details
                                                                        </div>
                                                                        <div class="list">
                                                                            <div class="list-item">
                                                                                <div class="text"><span class="black">Institute Name</span>
                                                                                    :<span class="blue"></span></div>
                                                                            </div>
                                                                            <div class="list-item">
                                                                                <div class="text"><span class="black">Course Type</span>
                                                                                    :<span class="blue"></span></div>
                                                                            </div>
                                                                            <div class="list-item">
                                                                                <div class="text"><span class="black">Course Name</span>
                                                                                    :<span class="blue"></span></div>
                                                                            </div>
                                                                            <div class="list-item">
                                                                                <div class="text"><span class="black">Start Date</span>
                                                                                    :<span class="blue"></span></div>
                                                                            </div>
                                                                            <div class="list-item">
                                                                                <div class="text"><span class="black">Booking Date</span>
                                                                                    :<span class="blue"></span></div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-sm-6">
                                                                        <div style="font-weight:bold;">Payment Details
                                                                        </div>
                                                                        <div class="list">
                                                                            <div class="list-item">
                                                                                <div class="text"><span class="black">Booking Amount</span>
                                                                                    :<span class="blue"></span></div>
                                                                            </div>
                                                                            <div class="list-item">
                                                                                <div class="text"><span class="black">Tax</span>
                                                                                    :<span class="blue"></span></div>
                                                                            </div>
                                                                            <div class="list-item">
                                                                                <div class="text"><span class="black">Total Amount</span>
                                                                                    :<span class="blue"></span></div>
                                                                            </div>
                                                                            <div class="list-item">
                                                                                <div class="text"><span
                                                                                            class="black"></span> <span
                                                                                            class="blue"></span></div>
                                                                            </div>
                                                                            <div class="list-item">
                                                                                <div class="text"><span class="black">Order Status</span>
                                                                                    :<span class="green-label">Payment Success</span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- /.container-fluid -->
                    </div>
                    <!-- /#page-wrapper -->
                </div>

            </div>
    </main>
@stop
@section('js_script')
    <script type="text/javascript" src="{{ URL:: asset('public/assets/js/site/registration.js')}}"></script>
    <script type="text/javascript" src="{{ URL:: asset('public/assets/js/site/company-registration.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            var userType = '';
            var share_user_id = $('.share_user_id').val();
            $(document).on('click','.user-type',function(){
                $('.user-type').removeClass('user-type-menu');
                $(this).addClass('user-type-menu');
                userType = $(this).data('type');
                getProfileData('user_type='+userType);
            });
            $(document).on('click','.ship-type-value',function(){
                $('.ship-type-value').removeClass('active-span');
                $(this).addClass('active-span');
                var shipId = $(this).data('id');
                $('.sea-service-details').show();
                $('.ship-data').show();
                
                $('.course-type-value').addClass('active-span');
                $('.docs-type-value').addClass('active-span');

                getUserTypeData('shipId='+shipId);
            });
            $(document).on('click','.course-type-value',function(){
                $('.course-type-value').removeClass('active-span');
                $(this).addClass('active-span');
                var courseId = $(this).data('id');
                $('.pre-sea-training').hide();
                if (courseId == 'all' || courseId == 'institute_degree') {
                    $('.pre-sea-training').show();
                }

                $('.ship-type-value').addClass('active-span');
                $('.docs-type-value').addClass('active-span');

                getUserTypeData('courseId='+courseId);
            });
            $(document).on('click','.expSpan',function(){
                $('.work-box').addClass('d-none');
                //  var exId = $(this).data('id');
                //  $('#'+exId).removeClass('d-none');
            });
            $(document).on('click','.docs-type-value',function(){
                $('.docs-type-value').removeClass('active-span');
                $(this).addClass('active-span');

                $('.course-type-value').addClass('active-span');
                $('.ship-type-value').addClass('active-span');

                 var docsId = $(this).data('id');
                 getUserTypeData('docsId='+docsId);
            });
            getProfileData('user_type=contact');
            getUserTypeData('docsId=all');
        });

        $(window).click(function(e) {
            if(e.target.className != 'expSpan exp-data'){
               $('.work-box').addClass('d-none');
            }
        });

        function getProfileData(userType){
            var share_user_id = $('.share_user_id').val();
            $.ajax({
                url: "{{URL::to('get-share-userinfo')}}?"+userType+"&share_user_id="+share_user_id,
                dataType: 'json',
            }).done(function(data){
                $('.user-data').html(data.userInfo);
            }).fail(function(error){

            });
        }
        function getUserTypeData(qstring){
            var share_user_id = $('.share_user_id').val();
            $.ajax({
                url: "{{URL::to('share-profile/' . $user_id)}}?"+qstring+"&profile="+share_user_id,
                dataType: 'json',
            }).done(function(data){
                var className = '';
                if(data.type == 1){
                    className = 'ship-type-data';
                }
                if(data.type == 2){
                    className = 'course-data';
                }
                if(data.type == 3){
                    className = 'docs-data';
                }
                $('.'+className).html(data.shipData);
                // console.log();
                $('.other-service-section').hide();
                if ((qstring.split('='))[1] == 'other_details' || (qstring.split('='))[1] == 'all') {
                    $('.other-service-section').show();
                }
                
                if ((qstring.split('='))[1] == 'other_details') {
                    $('.sea-service-details').hide();
                    $('.ship-data').hide();
                    // $('.other-service-section').show();
                }
                // console.log(qstring);
                $('.expSpan').each(function() {
                    $(this).popover();
                })           
                
            }).fail(function(error){

            });
        }
    </script>
@stop