@extends('site.index')
@section('content')
	<?php
		$india_value = array_search('India',\CommonHelper::countries());
		if(isset($data[0]['professional_detail']['current_rank'])){
			$required_fields = \CommonHelper::rank_required_fields()[$data[0]['professional_detail']['current_rank']];
		}
		if(isset($data[0]['document_permissions']) && !empty($data[0]['document_permissions'])){
			$document_permissions = $data[0]['document_permissions'];
		}
		if(Auth::check()){
			$registered_as = Auth::user()->registered_as;
		}
	?>

	<div class="user-profile content-section-wrapper profile_view_page">

		<div class="container-fluid">
            <div class="row">
            	@if(isset($user) && $user == 'another_user')
                	<div class="col-md-12" id="main-data">
                		<?php $contain = 'container'; ?>
                @else
               		<?php $contain = 'container-fluid'; ?>
                	<div class="col-md-2" id="side-navbar">
	                    @include('site.partials.side_navbar_seafarer')
	                </div>
	                <div class="col-md-10" id="main-data">
                @endif

                    <div class="row">
						@if($user != 'another_user')

							<input type="hidden" name="india_value" value="{{$india_value}}" required>

							<div class="{{$contain}}">
								<div class="row">
									<div class="col-sm-12 col-md-6 hide alert-box-verification-mob">
										<div class="alert alert-block alert-danger fade in">
											Your mobile is not verified. Please verify your mobile. <a class="alert-link" href="#" id="resend-otp-button">Verify</a>
										</div>
									</div>

									<div class="col-sm-12 col-md-6 hide alert-box-verification-email">
										<div class="alert alert-block alert-danger fade in">
											Your email is not verified. Please verify your email. <a class="alert-link" href="#" id="email_verify">Resend Email</a>
											<i class="fa fa-spin fa-refresh resend_email_loader hide"></i>
										</div>
									</div>
								</div>
							</div>
						@endif
						@if($errors->any())
							<div class="{{$contain}}">
								<div class="row">
					                <div class="col-sm-12 col-md-12">
					                    <div class="alert alert-block alert-danger fade in">
					                		{{$errors->first()}}
					                    </div>
					            	</div>
					            </div>
				           </div>
						@endif
						<div class="section-1">
						</div>
						<div class="section-2 user-imp-details">
							<input type="hidden" class="user_id" value="{{ isset($user_id) ? $user_id : ''}}">
							<div class="{{$contain}}">
								<div class="row">
									<input type="hidden" name="verify_email" id="verify_email" value="{{isset($data[0]['is_email_verified']) ? $data[0]['is_email_verified'] : ''}}">
									@if(isset($data[0]['personal_detail']['country']) && $data[0]['personal_detail']['country'] == $india_value)
										<input type="hidden" name="verify_mobile" id="verify_mobile" value="{{isset($data[0]['is_mob_verified']) ? $data[0]['is_mob_verified'] : ''}}">
									@endif
									<div class="col-sm-12 col-md-4 col-lg-3">
										<div class="photo-container">
											<div class="photo-card" style="height: 325px !important;">
												<div>
													<div class="pic">
														@if(isset($data[0]['profile_pic']))
															<img id="preview" src="/{{ env('SEAFARER_PROFILE_PATH')}}{{$data[0]['id']}}/{{$data[0]['profile_pic']}}" avatar-holder-src="{{ asset('images/user_default_image.png') }}">
														@else
															<img id="preview" src="{{ asset('images/user_default_image.png') }}" avatar-holder-src="{{ asset('images/user_default_image.png') }}">
														@endif
													</div>
													<div class="user-name">
														{{ isset($data[0]['first_name']) ? ucwords($data[0]['first_name']) : ''}}
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="col-xs-10 col-sm-11 col-md-5 col-lg-7 p-0">
										<div class="imp-details-container">
											@if($user != 'another_user')
												<div class="detail p-r-0">
													Email ID: {{ isset($data[0]['email']) ? $data[0]['email'] : ''}}

													@if($data[0]['is_email_verified'] == '1')
														<i class="fa fa-check-circle green f-15" aria-hidden="true" title="Email Verified"></i>
													@else
														<i class="fa fa-minus-circle red-icon f-15" aria-hidden="true" title="Email Unverified"></i>
													@endif
												</div>
												<div class="detail p-r-0 p-l-15">
													Mobile Number1: {{ isset($data[0]['mobile']) ? $data[0]['mobile'] : '' }}

													@if($data[0]['is_mob_verified'] == '1')
														<i class="fa fa-check-circle green f-15" aria-hidden="true" title="Mobile Verified"></i>
													@else
														<i class="fa fa-minus-circle red-icon f-15" aria-hidden="true" title="Mobile Unverified"></i>
													@endif
												</div>
											@endif
											<div class="detail p-l-15">
												Gender: {{ !empty($data[0]['gender']) ?  $data[0]['gender'] == 'M'? 'Male' : 'Female' : 'Male' }}
											</div>
										</div>
									</div>
									@if($user != 'another_user')
										<div class="col-xs-2 col-sm-1 col-md-3 col-lg-2" style="padding: 0px;margin-left: -15px;">
											<div class="edit-btn-container pull-right" style="padding: 0px;margin-left: 5px;">
												<a href="{{ route('site.seafarer.edit.profile') }}"><span class="profile-edit-btn-sm visible-xs-block visible-sm-block"><i class="fa fa-pencil-square-o" aria-hidden="true" title="Edit profile"></i></span>
													<button class="profile-edit-btn hidden-xs hidden-sm">
														Edit Profile
													</button>
												</a>
											</div>
											<div class="edit-btn-container pull-right">
												<a>
													<span class="profile-edit-btn-sm visible-xs-block visible-sm-block" target="_blank">
														<i class="fa fa-download" aria-hidden="true" title="View Resume"></i>
													</span>
													<button class="profile-edit-btn hidden-xs hidden-sm resume-download ladda-button view_resume_btn" data-style="zoom-in" seafarer-index="{{$data[0]['id']}}">
														View Resume
													</button>
												</a>
											</div>
										</div>
									@endif

									@if(isset($role) && !empty($role) && $role == 'company')
										<div class="col-xs-2 col-sm-1 col-md-3 col-lg-2">
											<div class="edit-btn-container pull-right">
												<a>
												<span class="profile-edit-btn-sm visible-xs-block visible-sm-block" target="_blank">
													<i class="fa fa-download" aria-hidden="true" title="Download CV"></i>
												</span>
												</a>
												<a class="profile-edit-btn hidden-xs hidden-sm cv-btn check-company-subscription" seafarer-index="{{$data[0]['id']}}" data-type="{{config('feature.feature1')}}" style="width: 100px;cursor: default;">
													Download CV
												</a>
											</div>
										</div>
									@endif

								</div>
							</div>
						</div>
						<div class="section-3">
							<div class="{{$contain}}">
								<div class="row">
									<div class="col-sm-12 col-md-4 col-lg-3"></div>
									<div class="col-sm-12 col-md-8 col-lg-9">
										<div class="sub-details-container location-container">
											<div class="content-container">
												<div class="row">
													<div class="col-sm-12">
														<div class="title">LOCATION DETAILS</div>
													</div>
												</div>
												<div class="row">
													<div class="col-sm-7">
														<div class="discription">
															<span class="content-head">Country:</span>
															<span class="content">
															@foreach( \CommonHelper::countries() as $c_index => $country)
																	{{ isset($data[0]['personal_detail']['country']) ? $data[0]['personal_detail']['country'] == $c_index ? $country : '' : ''}}
																@endforeach
														</span>
														</div>
													</div>
													<div class="col-sm-5">
														<div class="discription">
															<span class="content-head">State:</span>
															<span class="content">
															{{ isset($data[0]['personal_detail']['state_id']) ? $data[0]['personal_detail']['pincode']['pincodes_states'][0]['state']['name'] : (isset($data[0]['personal_detail']['state_text']) ? $data[0]['personal_detail']['state_text'] : '')}}
				                                        </span>
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-sm-7">
														<div class="discription">
															<span class="content-head">City:</span>
															<span class="content">
															{{ isset($data[0]['personal_detail']['city_id']) ? $data[0]['personal_detail']['pincode']['pincodes_cities'][0]['city']['name'] : (isset($data[0]['personal_detail']['city_text']) ? $data[0]['personal_detail']['city_text'] : '')}}
														</span>
														</div>
													</div>
													<div class="col-sm-5">
														<div class="discription">
															<span class="content-head">Pin-code:</span>
															<span class="content">{{ isset($data[0]['personal_detail']['pincode_text']) ? $data[0]['personal_detail']['pincode_text'] : ''}}</span>
														</div>
													</div>
												</div>
											</div>
											<div class="content-container">
												<div class="row">
													<div class="col-sm-12">
														<div class="title">PROFFESSION DETAILS</div>
													</div>
												</div>
												<div class="row">
													<div class="col-sm-7">
														<div class="discription">
															<span class="content-head">Current Rank:</span>
															<span class="content">
					                                    		@foreach(\CommonHelper::new_rank() as $index => $category)
																	@foreach($category as $r_index => $rank)
																		{{ !empty($data[0]['professional_detail']['current_rank']) ? $data[0]['professional_detail']['current_rank'] == $r_index ? $rank : '' : ''}}
																	@endforeach
																@endforeach
														</span>
														</div>
													</div>
													<div class="col-sm-5">
														<div class="discription">
															<span class="content-head">Current Rank Experience:</span>
															<span class="content">{{ isset($data[0]['professional_detail']['years']) ? $data[0]['professional_detail']['years'] : '0'}} Years {{ isset($data[0]['professional_detail']['months']) && ($data[0]['professional_detail']['months'] != '') ? $data[0]['professional_detail']['months'] : '0' }} Months</span>
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-sm-7">
														<div class="discription">
															<span class="content-head">Applied Rank:</span>
															<span class="content">
					                                    @foreach(\CommonHelper::new_rank() as $index => $category)
																	@foreach($category as $r_index => $rank)
																		{{ !empty($data[0]['professional_detail']['applied_rank']) ? $data[0]['professional_detail']['applied_rank'] == $r_index ? $rank : '' : ''}}
																	@endforeach
																@endforeach
														</span>
														</div>
													</div>
													<div class="col-sm-5">
														<div class="discription">
															<span class="content-head">Date Of Availability:</span>
															<span class="content">{{ isset($data[0]['professional_detail']['availability']) ? date('d-m-Y',strtotime($data[0]['professional_detail']['availability'])) : '-'}}</span>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>

								@include('site.partials.featured_advertisement_template')

								<ul class="nav nav-tabs user-tabs m-t-25">
									<li class="active">
										<a data-toggle="tab" href="#imp_documents">
											<span class="hidden-xs">GENERAL INFORMATION</span>
											<i class="fa fa-file-text visible-xs" aria-hidden="true"></i>
										</a>
									</li>
									<li>
										<a data-toggle="tab" href="#sea_service">
											<span class="hidden-xs">SEA SERVICE DETAILS</span>
											<i class="fa fa-ship visible-xs" aria-hidden="true"></i>
										</a>
									</li>
									<li>
										<a data-toggle="tab" href="#course_detail">
											<span class="hidden-xs">COURSE DETAILS</span>
											<i class="fa fa-certificate visible-xs" aria-hidden="true"></i>
										</a>
									</li>
									<li>
										<a data-toggle="tab" href="#user_document">
											<span class="hidden-xs">USER DOCUMENTS</span>
											<i class="fa fa-file-text visible-xs" aria-hidden="true"></i>
										</a>
									</li>
								</ul>

								<div class="tab-content">
									<div id="imp_documents" class="tab-pane fade in active">
										<div class="row">
											<div class="col-sm-12">
												<div class="details-main-heading">
													GENERAL INFORMATION
													@if($user != 'another_user')
														<button class="btn pull-right edit_link" style="color: #fff;background-color: #47c4f0;border: 1px solid;" data-form='documents' href="{{route('site.seafarer.edit.profile')}}">
															<i class="fa fa-pencil-square-o"> Edit</i>
														</button>
													@endif
												</div>
												<div class="sub-details-container">
													<div class="content-container">
														<div class="row">
															<div class="col-sm-12">
																<div class="title">PASSPORT DETAILS</div>
															</div>
														</div>
														<div class="row">
															<div class="col-xs-12 col-sm-6 col-md-3">
																<div class="discription">
																	<span class="content-head">Passport Country:</span>
																	<span class="content">
																		@if(isset($data[0]['passport_detail']['pass_country']) && !empty($data[0]['passport_detail']['pass_country']))
																			@foreach( \CommonHelper::countries() as $c_index => $country)
																				{{ isset($data[0]['passport_detail']['pass_country']) ? $data[0]['passport_detail']['pass_country'] == $c_index ? $country : '' : ''}}
																			@endforeach
																		@else
																			-
																		@endif
																</span>
																</div>
															</div>
															<div class="col-xs-12 col-sm-6 col-md-3">
																<div class="discription">
																	<span class="content-head">Passport Number:</span>
																	<span class="content">{{ isset($data[0]['passport_detail']['pass_number']) ? $data[0]['passport_detail']['pass_number'] : '-' }}</span>
																</div>
															</div>
															<div class="col-xs-12 col-sm-6 col-md-3">
																<div class="discription">
																	<span class="content-head">Place Of Issue:</span>
																	<span class="content">{{ isset($data[0]['passport_detail']['place_of_issue']) ? $data[0]['passport_detail']['place_of_issue'] : '-' }}</span>
																</div>
															</div>
															<div class="col-xs-12 col-sm-6 col-md-3">
																<div class="discription">
																	<span class="content-head">Expiry Date:</span>
																	<span class="content">{{ isset($data[0]['passport_detail']['pass_expiry_date'])? date('d-m-Y',strtotime($data[0]['passport_detail']['pass_expiry_date'])) : '-' }}</span>
																</div>
															</div>
														</div>
														<div class="row">
															<div class="col-xs-12 col-sm-6 col-md-3">
																<div class="discription">
																	<span class="content-head">US Visa:</span>
																	<span class="content">
																	{{ isset($data[0]['passport_detail']['us_visa']) ? $data[0]['passport_detail']['us_visa'] == 1 ? 'Yes' : 'No' : 'No'}}
																</span>
																</div>
															</div>
															@if($data[0]['passport_detail']['us_visa'] == 1)
																<div class="col-xs-12 col-sm-6 col-md-3">
																	<div class="discription">
																		<span class="content-head">Expiry Date:</span>
																		<span class="content">
																	{{ isset($data[0]['passport_detail']['us_visa_expiry_date']) ? date('d-m-Y',strtotime($data[0]['passport_detail']['us_visa_expiry_date'])) : '-'}}
																</span>
																	</div>
																</div>
															@endif
															<div class="col-xs-12 col-sm-6 col-md-3">
																<div class="discription">
																	<span class="content-head">Indian PCC:</span>
																	<span class="content">
																	{{ isset($data[0]['passport_detail']['indian_pcc']) ? $data[0]['passport_detail']['indian_pcc'] == '1'? 'Yes' : 'No' : 'No'}}
																</span>
																</div>
															</div>
															@if($data[0]['passport_detail']['indian_pcc_issue_date'] == 1)
																<div class="col-xs-12 col-sm-6 col-md-3">
																	<div class="discription">
																		<span class="content-head">Issue Date:</span>
																		<span class="content">
																	{{ isset($data[0]['passport_detail']['indian_pcc_issue_date']) ? date('d-m-Y',strtotime($data[0]['passport_detail']['indian_pcc_issue_date'])) : '-'}}
																</span>
																	</div>
																</div>
															@endif
															<div class="col-xs-12 col-sm-6 col-md-3">
																<div class="discription">
																	<span class="content-head">INDOS Number:</span>
																	<span class="content">
																	{{ !empty($data[0]['wkfr_detail']['indos_number']) ? $data[0]['wkfr_detail']['indos_number'] : '-'}}
																</span>
																</div>
															</div>
															<div class="col-xs-12 col-sm-6 col-md-3">
																<div class="discription">
																	<span class="content-head">Yellow Fever Vaccination:</span>
																	<span class="content">
																	{{ isset($data[0]['wkfr_detail']['yellow_fever']) ? $data[0]['wkfr_detail']['yellow_fever'] == '1'? 'Yes' : 'No' : 'NO' }}
																</span>
																</div>
															</div>
															@if($data[0]['wkfr_detail']['yellow_fever'] == 1)
																<div class="col-xs-12 col-sm-6 col-md-3">
																	<div class="discription">
																		<span class="content-head">Yellow Fever Issue Date:</span>
																		<span class="content">
																	{{ isset($data[0]['wkfr_detail']['yf_issue_date']) ? date('d-m-Y',strtotime($data[0]['wkfr_detail']['yf_issue_date'])) : '-'}}
																</span>
																	</div>
																</div>
															@endif
															@if($data[0]['passport_detail']['fromo'] == 1 || ($data[0]['passport_detail']['fromo'] == 0))
																<div class="col-xs-12 col-sm-6 col-md-3">
																	<div class="discription">
																		<span class="content-head">Experience Of Framo:</span>
																		<span class="content">
																	{{ isset($data[0]['passport_detail']['fromo']) ? $data[0]['passport_detail']['fromo'] == '1'? 'Yes' : 'No' : 'NO' }}
																</span>
																	</div>
																</div>
															@endif
														</div>
													</div>
												</div>
											</div>
										</div>

										@if(isset($data[0]['seaman_book_detail']) AND !empty($data[0]['seaman_book_detail']))
											<div class="row">
												<div class="col-sm-12">
													<div class="sub-details-container">
														<div class="content-container">
															<div class="row">
																<div class="col-sm-12">
																	<div class="title">SEAMAN BOOK DETAILS</div>
																</div>
															</div>
														@foreach($data[0]['seaman_book_detail'] as $index => $cdc_data)
															<!-- <div class="discription">
																<span class="content-head">CDC Details {{$index+1}}</span>
															</div> -->
																<div class="row">
																	<div class="col-xs-12 col-sm-6 col-md-2">
																		<div class="discription">
																			<span class="content-head">CDC:</span>
																			<span class="content">
																			@foreach( \CommonHelper::countries() as $c_index => $country)
																					{{ isset($cdc_data['cdc']) ? $cdc_data['cdc'] == $c_index ? $country : '' : ''}}
																				@endforeach
								                                        </span>
																		</div>
																	</div>
																	<div class="col-xs-12 col-sm-6 col-md-2">
																		<div class="discription">
																			<span class="content-head">Number:</span>
																			<span class="content">
																			{{isset($cdc_data['cdc_number']) ? $cdc_data['cdc_number'] : ''}}
																		</span>
																		</div>
																	</div>
																	<div class="col-xs-12 col-sm-6 col-md-2 p-0">
																		<div class="discription">
																			<span class="content-head">Issue Date:</span>
																			<span class="content">
																			{{isset($cdc_data['cdc_issue_date']) ? date('d-m-Y',strtotime($cdc_data['cdc_issue_date'])) : ''}}
																		</span>
																		</div>
																	</div>
																	<div class="col-xs-12 col-sm-6 col-md-3">
																			<div class="discription">
																				<span class="content-head">Verification Date:</span>
																					<span class="content">
																					{{ !is_null($cdc_data['cdc_verification_date']) ? date('d-m-Y',strtotime($cdc_data['cdc_verification_date'])) : '-' }}
																				</span>
																			</div>
																		</div>
																	<div class="col-xs-12 col-sm-6 col-md-3">
																		<div class="discription">
																			<span class="content-head">Expiry Date:</span>
																			<span class="content">
																			{{isset($cdc_data['cdc_expiry_date']) ? date('d-m-Y',strtotime($cdc_data['cdc_expiry_date'])) : ''}}
																				@if($cdc_data['status'] == 1)
																					<i class="fa fa-check-circle green f-15 m-l-15" aria-hidden="true" title="Verified"></i>
																				@endif
																		</span>
																		</div>
																	</div>
																</div>
																<hr>
															@endforeach
														</div>
													</div>
												</div>
											</div>
										@endif

										@if(isset($data[0]['cop_detail']) AND !empty($data[0]['cop_detail']))
											<div class="row">
												<div class="col-sm-12">
													<div class="sub-details-container">
														<div class="content-container">
															<div class="row">
																<div class="col-sm-12">
																	<div class="title">CERTIFICATE OF PROFICIENCY</div>
																</div>
															</div>
														@foreach($data[0]['cop_detail'] as $index => $cop_data)
															<!-- <div class="discription">
																<span class="content-head">COP Details {{$index+1}}</span>
															</div> -->
																<div class="row">
																	<div class="col-xs-12 col-sm-6 col-md-3">
																		<div class="discription">
																			<span class="content-head">Number:</span>
																			<span class="content">
																			{{ isset($cop_data['cop_number']) ? $cop_data['cop_number'] : ''}}
																		</span>
																		</div>
																	</div>
																	<div class="col-xs-12 col-sm-6 col-md-3">
																		<div class="discription">
																			<span class="content-head">Grade:</span>
																			<span class="content">
																			{{ isset($cop_data['cop_grade']) ? $cop_data['cop_grade'] : '' }}
																		</span>
																		</div>
																	</div>
																	<div class="col-xs-12 col-sm-6 col-md-3">
																		<div class="discription">
																			<span class="content-head">Issue Date:</span>
																			<span class="content">
																			{{ isset($cop_data['cop_issue_date']) ? date('d-m-Y',strtotime($cop_data['cop_issue_date'])) : '' }}
																		</span>
																		</div>
																	</div>
																	<div class="col-xs-12 col-sm-6 col-md-3">
																		<div class="discription">
																			<span class="content-head">Expiry Date:</span>
																			<span class="content">
																			{{ isset($cop_data['cop_exp_date']) ? date('d-m-Y',strtotime($cop_data['cop_exp_date'])) : '' }}
																		</span>
																		</div>
																	</div>
																</div>
																<hr>
															@endforeach
														</div>
													</div>
												</div>
											</div>
										@endif

										@if(isset($required_fields))
											@if(isset($data[0]['coc_detail']) AND !empty($data[0]['coc_detail']) && (in_array('COC',$required_fields) OR in_array('COC-Optional',$required_fields)))
												<div class="row">
													<div class="col-sm-12">
														<div class="sub-details-container">
															<div class="content-container">
																<div class="row">
																	<div class="col-sm-12">
																		<div class="title">CERTIFICATE OF COMPENTENCY</div>
																	</div>
																</div>
															@foreach($data[0]['coc_detail'] as $index => $coc_data)
																<!-- <div class="discription">
																	<span class="content-head">COC Details {{$index+1}}</span>
																</div> -->
																	<div class="row">
																		<div class="col-xs-12 col-sm-6 col-md-2">
																			<div class="discription">
																				<span class="content-head">COC:</span>
																				<span class="content">
																				@if(isset($coc_data['coc']) && !empty($coc_data['coc']))
																						@foreach( \CommonHelper::countries() as $c_index => $country)
																							{{ isset($coc_data['coc']) ? $coc_data['coc'] == $c_index ? $country : '' : ''}}
																						@endforeach
																					@else
																						-
																					@endif
									                                        </span>
																			</div>
																		</div>
																		<div class="col-xs-12 col-sm-6 col-md-2">
																			<div class="discription">
																				<span class="content-head">Number:</span>
																				<span class="content">
																				{{ !empty($coc_data['coc_number']) ? $coc_data['coc_number'] : '-'}}
																			</span>
																			</div>
																		</div>
																		<div class="col-xs-12 col-sm-6 col-md-2">
																			<div class="discription">
																				<span class="content-head">Grade:</span>
																				<span class="content">
																				{{ !empty($coc_data['coc_grade']) ? $coc_data['coc_grade'] : '-' }}
																			</span>
																			</div>
																		</div>
																		<div class="col-xs-12 col-sm-6 col-md-3">
																			<div class="discription">
																				<span class="content-head">Verification Date:</span>
																					<span class="content">
																					{{ !is_null($coc_data['coc_verification_date']) ? date('d-m-Y',strtotime($coc_data['coc_verification_date'])) : '-' }}
																				</span>
																			</div>
																		</div>
																		<div class="col-xs-12 col-sm-6 col-md-3">
																			<div class="discription">
																				<span class="content-head">Expiry Date:</span>
																				<span class="content">
																				{{ !is_null($coc_data['coc_expiry_date']) ? date('d-m-Y',strtotime($coc_data['coc_expiry_date'])) : '-' }}

																				@if(isset($coc_data['status']) && $coc_data['status'] == 1)
																					<i class="fa fa-check-circle green f-15 m-l-15" aria-hidden="true" title="Verified"></i>
																				@endif
																			</span>
																			</div>
																		</div>
																	</div>
																	<hr>
																@endforeach
															</div>
														</div>
													</div>
												</div>
											@endif
										@endif

										@if(isset($required_fields))
											@if(isset($data[0]['coe_detail']) AND !empty($data[0]['coe_detail']) && (in_array('COE',$required_fields) OR in_array('COE-Optional',$required_fields)) && (!empty($data[0]['coe_detail'][0]['coe']) || !empty($data[0]['coe_detail'][0]['coe_number']) || !empty($data[0]['coe_detail'][0]['coe_grade']) || !empty($data[0]['coe_detail'][0]['coe_expiry_date']) || !empty($data[0]['coe_detail'][0]['coe_verification_date'])))
												<div class="row">
													<div class="col-sm-12">
														<div class="sub-details-container">
															<div class="content-container">
																<div class="row">
																	<div class="col-sm-12">
																		<div class="title">CERTIFICATE OF ENDORSEMENT</div>
																	</div>
																</div>
															@foreach($data[0]['coe_detail'] as $index => $coe_data)
																<!-- <div class="discription">
																	<span class="content-head">COE Details {{$index+1}}</span>
																</div> -->
																	<div class="row">
																		<div class="col-xs-12 col-sm-6 col-md-3">
																			<div class="discription">
																				<span class="content-head">COE:</span>
																				<span class="content">
																				@if(isset($coe_data['coe']) && !empty($coe_data['coe']))
																						@foreach( \CommonHelper::countries() as $c_index => $country)
																							{{ isset($coe_data['coe']) ? $coe_data['coe'] == $c_index ? $country : '' : ''}}
																						@endforeach
																					@else
																						-
																					@endif
									                                        </span>
																			</div>
																		</div>
																		<div class="col-xs-12 col-sm-6 col-md-3">
																			<div class="discription">
																				<span class="content-head">Number:</span>
																				<span class="content">
																				{{ !empty($coe_data['coe_number']) ? $coe_data['coe_number'] : '-'}}
																			</span>
																			</div>
																		</div>
																		<div class="col-xs-12 col-sm-6 col-md-3">
																			<div class="discription">
																				<span class="content-head">Grade:</span>
																				<span class="content">
																				{{ !empty($coe_data['coe_grade']) ? $coe_data['coe_grade'] : '-' }}
																			</span>
																			</div>
																		</div>
																		<div class="col-xs-12 col-sm-6 col-md-3">
																			<div class="discription">
																				<span class="content-head">Expiry Date:</span>
																				<span class="content">
																				{{ !empty($coe_data['coe_expiry_date']) ? date('d-m-Y',strtotime($coe_data['coe_expiry_date'])) : '-' }}
																			</span>
																			</div>
																		</div>
																	</div>
																	<hr>
																@endforeach
															</div>
														</div>
													</div>
												</div>
											@endif
										@endif

										@if(isset($required_fields) && !empty($required_fields))
											@if(isset($data[0]['gmdss_detail']) AND !empty($data[0]['gmdss_detail']) && (in_array('GMDSS',$required_fields) OR in_array('GMDSS-Optional',$required_fields)) && ((!empty($data[0]['gmdss_detail']['gmdss'])) || (!empty($data[0]['gmdss_detail']['gmdss_number'])) || (!empty($data[0]['gmdss_detail']['gmdss_expiry_date']))))

												<div class="row">
													<div class="col-sm-12">
														<div class="sub-details-container">
															<div class="content-container">
																<div class="row">
																	<div class="col-sm-12">
																		<div class="title">GMDSS DETAILS</div>
																	</div>
																</div>
																<div class="row">
																	<div class="col-xs-12 col-sm-6 col-md-2">
																		<div class="discription">
																			<span class="content-head">GMDSS:</span>
																			<span class="content">
																			@foreach( \CommonHelper::countries() as $c_index => $country)
																					{{ isset($data[0]['gmdss_detail']['gmdss']) ? $data[0]['gmdss_detail']['gmdss'] == $c_index ? $country : '' : ''}}
																				@endforeach
																		</span>
																		</div>
																	</div>
																	<div class="col-xs-12 col-sm-6 col-md-2">
																		<div class="discription">
																			<span class="content-head">Number:</span>
																			<span class="content">
																			{{ isset($data[0]['gmdss_detail']['gmdss_number']) ? $data[0]['gmdss_detail']['gmdss_number'] : '-'}}
																		</span>
																		</div>
																	</div>
																	<div class="col-xs-12 col-sm-6 col-md-3">
																		<div class="discription">
																			<span class="content-head">Expiry Date:</span>
																			<span class="content">
																			{{ isset($data[0]['gmdss_detail']['gmdss_expiry_date']) ? date('d-m-Y',strtotime($data[0]['gmdss_detail']['gmdss_expiry_date'])) : '-' }}
																		</span>
																		</div>
																	</div>
																	<div class="col-xs-12 col-sm-6 col-md-3 gmdss_endorsement {{isset($data[0]['gmdss_detail']['gmdss']) && $data[0]['gmdss_detail']['gmdss'] == $india_value ? '' : 'hide'}}">
																		<div class="discription">
																			<span class="content-head">Endorsement Number:</span>
																			<span class="content">
																			{{ isset($data[0]['gmdss_detail']['gmdss_endorsement_number']) && !empty($data[0]['gmdss_detail']['gmdss_endorsement_number']) ? $data[0]['gmdss_detail']['gmdss_endorsement_number'] : '-'}}
																		</span>
																		</div>
																	</div>
																	<div class="col-xs-12 col-sm-6 col-md-2 gmdss_valid_till {{isset($data[0]['gmdss_detail']['gmdss']) && $data[0]['gmdss_detail']['gmdss'] == $india_value ? '' : 'hide'}}">
																		<div class="discription">
																			<span class="content-head">Valid Till:</span>
																			<span class="content">
																			{{ isset($data[0]['gmdss_detail']['gmdss_endorsement_expiry_date']) && !empty($data[0]['gmdss_detail']['gmdss_endorsement_expiry_date']) ? date('d-m-Y',strtotime($data[0]['gmdss_detail']['gmdss_endorsement_expiry_date'])) : '-'}}
																		</span>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											@endif
										@endif

										@if(!empty($data[0]['wkfr_detail']['watch_keeping']) || !empty($data[0]['wkfr_detail']['wkfr_number']) || !empty($data[0]['wkfr_detail']['issue_date']))
											<div class="row">
												<div class="col-sm-12">
													<div class="sub-details-container">
														<div class="content-container">
															<div class="row">
																<div class="col-sm-12">
																	<div class="title">WATCH KEEPING FOR RATING</div>
																</div>
															</div>
															<div class="row">
																<div class="col-xs-12 col-sm-6 col-md-3">
																	<div class="discription">
																		<span class="content-head">Watch Keeping:</span>
																		<span class="content">
																	@foreach( \CommonHelper::countries() as $c_index => $country)
																				{{ isset($data[0]['wkfr_detail']['watch_keeping']) ? $data[0]['wkfr_detail']['watch_keeping'] == $c_index ? $country : '' : ''}}
																			@endforeach
						                                        </span>
																	</div>
																</div>
																<div class="col-xs-12 col-sm-6 col-md-3">
																	<div class="discription">
																		<span class="content-head">Deck / Engine:</span>
																		<span class="content">
																			{{ isset($data[0]['wkfr_detail']['type']) ? $data[0]['wkfr_detail']['type'] == 'Deck' ? 'Deck' : 'Engine' : 'Engine'}}
																		</span>
																	</div>
																</div>
																<div class="col-xs-12 col-sm-6 col-md-3">
																	<div class="discription">
																		<span class="content-head">Number:</span>
																		<span class="content">
																			{{ isset($data[0]['wkfr_detail']['wkfr_number']) && !empty($data[0]['wkfr_detail']['wkfr_number']) ? $data[0]['wkfr_detail']['wkfr_number'] : '-'}}
																	</span>
																	</div>
																</div>
																<div class="col-xs-12 col-sm-6 col-md-3">
																	<div class="discription">
																		<span class="content-head">Issue Date:</span>
																		<span class="content">
																			{{ isset($data[0]['wkfr_detail']['issue_date']) ? date('d-m-Y',strtotime($data[0]['wkfr_detail']['issue_date'])) : '-'}}
																					@if(isset($data[0]['wkfr_detail']['status']) && $data[0]['wkfr_detail']['status'] == 1)
																						<i class="fa fa-check-circle green f-15 m-l-15" aria-hidden="true" title="Verified"></i>
																					@endif
																		</span>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										@endif
									</div>
									<div id="sea_service" class="tab-pane fade">
										<div class="row">
											<div class="col-sm-12">
												<div class="details-main-heading">
													SEA SERVICE DETAILS
													@if($user != 'another_user')
														<button class="btn pull-right edit_link" style="color: #fff;background-color: #47c4f0;border: 1px solid;" data-form='sea_service' href="{{route('site.seafarer.edit.profile')}}">
															<i class="fa fa-pencil-square-o"> Edit</i>
														</button>
													@endif
												</div>
												@if(isset($data[0]['sea_service_detail']) AND !empty($data[0]['sea_service_detail']))
													<div class="sub-details-container">
														@foreach($data[0]['sea_service_detail'] as $index => $services)
															<div class="content-container">
																<div class="row">
																	<div class="col-sm-12">
																		<div class="title">SERVICE {{$index+1}}</div>
																	</div>
																</div>
																<div class="row">
																	<div class="col-xs-12 col-sm-6 col-md-4">
																		<div class="discription">
																			<span class="content-head">Shipping Company:</span>
																			<span class="content">
																		{{ isset($services['company_name']) ? $services['company_name'] : '-'}}
																		</span>
																		</div>
																	</div>
																	<div class="col-xs-12 col-sm-6 col-md-4">
																		<div class="discription">
																			<span class="content-head">Name Of Ship:</span>
																			<span class="content">
																			{{ isset($services['ship_name']) ? $services['ship_name'] : '-'}}
																		</span>
																		</div>
																	</div>
																	<div class="col-xs-12 col-sm-6 col-md-4">
																		<div class="discription">
																			<span class="content-head">Ship Type:</span>
																			<span class="content">
																		@foreach( \CommonHelper::ship_type() as $c_index => $type)
																					{{ isset($services['ship_type']) ? $services['ship_type'] == $c_index ? $type : '' : ''}}
																				@endforeach
																		</span>
																		</div>
																	</div>
																</div>
																<div class="row">
																	<div class="col-xs-12 col-sm-6 col-md-4">
																		<div class="discription">
																			<span class="content-head">Rank:</span>
																			<span class="content">
																		@foreach(\CommonHelper::new_rank() as $index => $category)
																					@foreach($category as $r_index => $rank)
																						{{ isset($services['rank_id']) ? $services['rank_id'] == $r_index ? $rank : '' : ''}}
																					@endforeach
																				@endforeach
																		</span>
																		</div>
																	</div>
																	<div class="col-xs-12 col-sm-6 col-md-4">
																		<div class="discription">
																			<span class="content-head">GRT:</span>
																			<span class="content">
																			{{ !empty($services['grt']) ? $services['grt'] : '-'}}
																		</span>
																		</div>
																	</div>
																	<div class="col-xs-12 col-sm-6 col-md-4">
																		<div class="discription">
																			<span class="content-head">BHP:</span>
																			<span class="content">
																			{{ !empty($services['bhp']) ? $services['bhp'] : '-'}}
																		</span>
																		</div>
																	</div>
																</div>
																<div class="row">
																	<div class="col-xs-12 col-sm-6 col-md-4">
																		<div class="discription">
																			<span class="content-head">Engine Type:</span>
																			<span class="content">
																		@if(isset($services['engine_type']) && !empty($services['engine_type']))
																					@if(isset($services['engine_type']) && $services['engine_type'] == 'other')
																						{{ isset($services['other_engine_type']) ? $services['other_engine_type'] : '-'}}
																					@else
																						@foreach( \CommonHelper::engine_type() as $c_index => $type)
																							{{ isset($services['engine_type']) ? $services['engine_type'] == $c_index ? $type  : '' : ''}}
																						@endforeach
																					@endif
																				@else
																					-
																					@endif
																		</span>
																		</div>
																	</div>
																	<div class="col-xs-12 col-sm-6 col-md-4">
																		<div class="discription">
																			<span class="content-head">Sign On Date:</span>
																			<span class="content">
																			{{ isset($services['from']) ? date('d-m-Y',strtotime($services['from'])) : ''}}
																		</span>
																		</div>
																	</div>
																	<div class="col-xs-12 col-sm-6 col-md-4">
																		<div class="discription">
																			<span class="content-head">Sign Off Date:</span>
																			<span class="content">
																			{{ isset($services['to']) ? date('d-m-Y',strtotime($services['to'])) : ''}}
																		</span>
																		</div>
																	</div>
																	<div class="col-xs-12 col-sm-6 col-md-4">
																		<div class="discription">
																			<span class="content-head">Manning By:</span>
																			<span class="content">
																			{{ isset($services['manning_by']) ? $services['manning_by'] : ' - '}}
																		</span>
																		</div>
																	</div>
																</div>
																<hr />
															</div>
														@endforeach
													</div>
												@else
													<div class="sub-details-container">
														<div class="content-container">
															<div class="row">
																<div class="col-xs-12 text-center">
																	<div class="discription">
																		<span class="content-head">No Data Found</span>
																	</div>
																</div>
															</div>
														</div>
													</div>
												@endif
											</div>
										</div>
									</div>
									<div id="course_detail" class="tab-pane fade">
										<div class="row">
											<div class="col-sm-12">
												<div class="details-main-heading">
													COURSE DETAILS
													@if($user != 'another_user')
														<button class="btn pull-right edit_link" style="color: #fff;background-color: #47c4f0;border: 1px solid;" data-form='courses' href="{{route('site.seafarer.edit.profile')}}">
															<i class="fa fa-pencil-square-o"> Edit</i>
														</button>
													@endif
												</div>
												@if(isset($data[0]['course_detail']) AND !empty($data[0]['course_detail']))
													<div class="sub-details-container">
														<?php $normal_course_count = 1; ?>
														@foreach($data[0]['course_detail'] as $index => $courses)
															<div class="content-container">
																<div class="row">
																	<div class="col-sm-12">
																		<div class="title">CERTIFICATE {{$normal_course_count}}</div>
																	</div>
																</div>
																<div class="row">
																	<div class="col-xs-12 col-sm-6 col-md-4">
																		<div class="discription">
																			<span class="content-head">Certificate Name:</span>
																			<span class="content">
																				@foreach( \CommonHelper::courses() as $c_index => $course)
																					{{ isset($courses['course_id']) ? $courses['course_id'] == $c_index ? $course : '' : ''}}
																				@endforeach
																		</span>
																		</div>
																	</div>
																	<div class="col-xs-12 col-sm-6 col-md-4">
																		<div class="discription">
																			<span class="content-head">Certificate Number:</span>
																			<span class="content">
																			{{ isset($courses['certification_number']) && !empty($courses['certification_number'])? $courses['certification_number'] : '-'}}
																		</span>
																		</div>
																	</div>
																	<div class="col-xs-12 col-sm-6 col-md-4">
																		<div class="discription">
																			<span class="content-head">Date Of Issue:</span>
																			<span class="content">
																			{{ isset($courses['issue_date']) && !empty($courses['issue_date']) ? date('d-m-Y',strtotime($courses['issue_date'])) : '-'}}
																		</span>
																		</div>
																	</div>
																	<div class="col-xs-12 col-sm-6 col-md-4">
																		<div class="discription">
																			<span class="content-head">Date Of Expiry:</span>
																			<span class="content">
																			{{ isset($courses['expiry_date']) && !empty(isset($courses['expiry_date'])) ? date('d-m-Y',strtotime($courses['expiry_date'])) : '-'}}
																		</span>
																		</div>
																	</div>
																	<div class="col-xs-12 col-sm-6 col-md-4">
																		<div class="discription">
																			<span class="content-head">Issued By:</span>
																			<span class="content">
																			{{ isset($courses['issue_by']) && !empty($courses['issue_by']) ? $courses['issue_by'] : '-'}}
																		</span>
																		</div>
																	</div>
																</div>
																<hr>
															</div>
															<?php $normal_course_count++; ?>
														@endforeach
													</div>
												@else
													<div class="sub-details-container">
														<div class="content-container">
															<div class="row">
																<div class="col-xs-12 text-center">
																	<div class="discription">
																		<span class="content-head">No Data Found</span>
																	</div>
																</div>
															</div>
														</div>
													</div>
												@endif
											</div>
										</div>

										<div class="row">
											<div class="col-sm-12">
												<div class="details-main-heading">
													ADD-ON COURSE DETAILS
												</div>
												@if(isset($data[0]['value_added_course_detail']) AND !empty($data[0]['value_added_course_detail']))

													<div class="sub-details-container">
														@foreach($data[0]['value_added_course_detail'] as $index => $courses)
															<div class="content-container">
																<div class="row">
																	<div class="col-sm-12">
																		<div class="title">CERTIFICATE {{$index+1}}</div>
																	</div>
																</div>
																<div class="row">
																	<div class="col-xs-12 col-sm-6 col-md-4">
																		<div class="discription">
																			<span class="content-head">Certificate Name:</span>
																			<span class="content">
																		@foreach( \CommonHelper::value_added_courses() as $c_index => $course)
																			{{ isset($courses['course_id']) ? $courses['course_id'] == $c_index ? $course : '' : ''}}
																		@endforeach
																		</span>
																		</div>
																	</div>
																	<div class="col-xs-12 col-sm-6 col-md-4">
																		<div class="discription">
																			<span class="content-head">Certificate Number:</span>
																			<span class="content">
																			{{ isset($courses['certification_number']) ? $courses['certification_number'] : '-'}}
																		</span>
																		</div>
																	</div>
																	<div class="col-xs-12 col-sm-6 col-md-4">
																		<div class="discription">
																			<span class="content-head">Date Of Issue:</span>
																			<span class="content">
																			{{ isset($courses['issue_date']) ? date('d-m-Y',strtotime($courses['issue_date'])) : '-'}}
																		</span>
																		</div>
																	</div>
																	<div class="col-xs-12 col-sm-6 col-md-4">
																		<div class="discription">
																			<span class="content-head">Date Of Expiry:</span>
																			<span class="content">
																			{{ isset($courses['expiry_date']) ? date('d-m-Y',strtotime($courses['expiry_date'])) : '-'}}
																		</span>
																		</div>
																	</div>
																	<div class="col-xs-12 col-sm-6 col-md-4">
																		<div class="discription">
																			<span class="content-head">Issued By:</span>
																			<span class="content">
																			{{ isset($courses['issue_by']) ? $courses['issue_by'] : '-'}}
																		</span>
																		</div>
																	</div>
																</div>
																<hr>
															</div>
														@endforeach
													</div>
												@else
													<div class="sub-details-container">
														<div class="content-container">
															<div class="row">
																<div class="col-xs-12 text-center">
																	<div class="discription">
																		<span class="content-head">No Data Found</span>
																	</div>
																</div>
															</div>
														</div>
													</div>
												@endif
											</div>
										</div>

										@if(isset($data[0]['professional_detail']['other_exp']) && !empty($data[0]['professional_detail']['other_exp']))
											<div class="row">
												<div class="col-sm-12">
													<div class="details-main-heading">
														OTHER EXPERIENCE
													</div>
													<div class="sub-details-container">
														<div class="content-container">
															<div class="row">
																<div class="col-sm-12">
																	{{$data[0]['professional_detail']['other_exp']}}
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										@endif
				                </div>
									<div id="user_document" class="tab-pane fade">
										@if(isset($registered_as) && ($registered_as != 'advertiser'))
											<ul class="nav nav-tabs user-tabs-document">
												<li class="active">
													<a data-toggle="tab" href="#upload_documents">
														<span class="hidden-xs">UPLOADED DOCUMENTS</span>
														<i class="fa fa-file-text visible-xs" aria-hidden="true"></i>
													</a>
												</li>
												@if(Auth::user()->registered_as == 'seafarer')
													<li>
														<a data-toggle="tab" href="#request">
															<span class="hidden-xs">DOCUMENTS REQUEST</span>
															<i class="fa fa-certificate visible-xs" aria-hidden="true"></i>
														</a>
													</li>
												@endif

													@if($user == 'another_user')
														<li class="download_and_edit_profile_option">
															<?php
																$download_all_path = route('site.user.get.documents.path',['user_id' => $data[0]['id']]);
															?>
															<div class="details-main-heading">
																<button class="download_all_button btn pull-right">
																	<i class="fa fa-download">
																		<a href="{{$download_all_path}}">
																			Download All
																		</a>
																	</i>
																</button>
															</div>
														</li>
													@else
														<li class="download_and_edit_profile_option">
															<div class="details-main-heading">
																<button class="btn pull-right edit_link" style="color: #fff;background-color: #47c4f0;border: 1px solid;" data-form='user_document' href="{{route('site.seafarer.edit.profile')}}">
																	<i class="fa fa-pencil-square-o"> Edit</i>
																</button>
															</div>
														</li>
													@endif
											</ul>
										@else
											@if(isset($registered_as) && ($registered_as == 'advertiser'))
												<div class="login_message sub-details-container">
													<div class="content-container">
														<div class="col-xs-12 text-center">
															<div class="discription">
															<span class="content-head">
																You are not authorized to see seafarer documents.
															</span>
															</div>
														</div>
													</div>
												</div>
											@else
												<div class="login_message sub-details-container">
													<div class="content-container">
														<div class="col-xs-12 text-center">
															<div class="discription">
															<span class="content-head">
																Please login to see or request seafarer documents.
															</span>
															</div>
														</div>
													</div>
												</div>
											@endif
										@endif

										<?php
											$download_all_path = route('site.user.get.documents.path',['user_id' => $data[0]['id']]);
											$current_route = Route::currentRouteName();
										?>

									@if(isset($registered_as) && ($registered_as != 'advertiser'))
										<div class="tab-content">
											<div id="upload_documents" class="tab-pane fade in active">
												@if($current_route == 'user.view.profile')
													<input type="hidden" class="profile_status" value="view profile">
													@if(!isset($user_documents) && empty($user_documents))
														<div class="row">
															<div class="col-xs-12 text-center">
																<div class="discription">
																	<span class="content-head">No Documents Found</span>
																</div>
															</div>
														</div>
													@else
														<?php
															$active_class = "active";
															$active_class_tab = "active";
															$no_documents = true;
															//dd($documents,$user_documents);
														?>
														<div class="row">
															<div class="col-sm-12">
															<div class="document_slide_new document_slide">
																@foreach($documents as $key => $value)
																	@if(isset($value[0]['imp_doc']) AND !empty($value[0]['imp_doc']))
																		<?php
																			$hide = '';
																			if(isset($value[0]['hide'])){
																				$hide = 'hide';
																			}

																		?>
																		@if($hide != 'hide')
																			<div class="document_slide_tab documents-tab {{$active_class_tab}}" data-block="{{$key}}-block">
																				<div class="document_tab_heading">
																					{{ isset($value[0]['title']) ? strtoupper($value[0]['title']) : ''}}
																				</div>
																			</div>
																			<?php $no_documents = false; ?>
																		@endif

																	@else
																		<?php
																			$hide = '';

																			if(isset($value['hide'])){
																				$hide = 'hide';
																			}

																		?>
																		@if($hide != 'hide')

																			@if(isset($value['imp_doc']))
																			<div class="document_slide_tab documents-tab {{$active_class_tab}}" data-block="{{$key}}-block">
																				<div class="document_tab_heading">
																					{{ isset($value['title']) ? strtoupper($value['title']) : ''}}
																				</div>
																			</div>
																			<?php $no_documents = false; ?>
																			@endif
																		@endif
																	@endif

																	<?php  $active_class_tab = ""; ?>
																@endforeach
															</div>
															</div>
														</div>

														<div class="main-section m-t-0 m-b-0">
															@if(!$no_documents)
																@foreach($documents as $name => $value)
																	<div class="content-container doc-section no-margin {{$name}}-block {{ isset($value['hide']) ? '' : $active_class }}">
																		@if(isset($value[0]['imp_doc']) && !empty($value[0]['imp_doc']))

																			<?php
																				$docs = $value[0]['imp_doc'];
																				$document_path = route('site.user.get.documents.path',['user_id' => $docs['user_id'],'type' => $name]);
																			?>

																			<div class="row">
																				<div class="col-xs-12 m-t-15 upload-title">
																					<div class="title">{{strtoupper($name)}}
																						@if($docs['status'] != '0' && $user == 'another_user')
																							<button class="download_button btn pull-right">
																								<i class="fa fa-download">
																									<a href="{{$document_path}}">
																										Download
																									</a>
																								</i>
																							</button>
																						@endif
																					</div>
																				</div>
																			</div>

																			<div class="user_documents_download m-b-15 m-t-15">
																				<div class="row">
																					<div class="">
																						<div class="view_documents">

																							@foreach($value as $document)
																								@if(isset($document['imp_doc']['user_documents']))
																									<div class="col-xs-12 m-t-15">
																										<div class="title">{{strtoupper($name)}}
																											{{ isset($document['imp_doc']['type_id']) ? $document['imp_doc']['type_id'] : ''}}
																										</div>
																									</div>

																									@if(isset($document['imp_doc']['status'] ) && $document['imp_doc']['status'] == '0')

																										@if($user != 'another_user')
																											@foreach($document['imp_doc']['user_documents'] as $user_docs)
																												<?php
																													$extension = '';
																													if(isset($user_docs['document_path']) && !empty($user_docs['document_path'])){
																														$extension = last(explode('.',$user_docs['document_path']));
																													}

																													if($extension == 'jpg' || $extension == 'jpeg'){
																														$logo = '/images/jpg.jpg';
																													}
																													else if($extension == 'pdf'){
																														$logo = '/images/pdf.png';
																													}
																													else if($extension == 'gif'){
																														$logo = '/images/gif.png';
																													}
																													else if($extension == 'png'){
																														$logo = '/images/png.png';
																													}
																													else if($extension == 'doc' || $extension == 'docx'){
																														$logo = '/images/doc.png';
																													}else {
																														$logo = '/images/file.png';
																													}
																												?>
																												<a href="#" style="display: inline-block;vertical-align: top;margin: 16px;">
																													<img src="{{asset($logo)}}" alt="" width="100" height="100">
																												</a>
																											@endforeach
																											<div class="document_type pull-right">Document Type : <b>Private</b></div>
																										@else

																											<div class="col-xs-12 m-t-15 private-document-request" style="display: flex;align-items: center;">
																												<div class="col-xs-12 col-sm-8 private-document-request-count">	There are {{ isset($document['imp_doc']['user_documents']) ? count($document['imp_doc']['user_documents']) : '0'}} private documents.</div>
																												<div class="col-xs-12 col-sm-4 private-document-request-button">
																													<button type="button" class="btn btn-info btn-sm pull-right" data-toggle="modal" data-target="#download_document_list">Request for documents</button>
																												</div>
																											</div>
																										@endif
																									@else

																										<!-- <div class="document_type pull-right">Document Type : <b>Public</b></div> -->

																										@if(isset($document['imp_doc']['user_documents']))
																											<div class="image">
																											@foreach($document['imp_doc']['user_documents'] as $user_docs)

																												<?php
																													$extension = '';
																													if(isset($user_docs['document_path']) && !empty($user_docs['document_path'])){
																														$extension = last(explode('.',$user_docs['document_path']));
																													}

																													if($extension == 'jpg' || $extension == 'jpeg'){
																														$logo = '/images/jpg.jpg';
																													}
																													else if($extension == 'pdf'){
																														$logo = '/images/pdf.png';
																													}
																													else if($extension == 'gif'){
																														$logo = '/images/gif.png';
																													}
																													else if($extension == 'png'){
																														$logo = '/images/png.png';
																													}
																													else if($extension == 'doc' || $extension == 'docx'){
																														$logo = '/images/doc.png';
																													}else {
																														$logo = '/images/file.png';
																													}
																												?>
																												<a href="#" style="display: inline-block;vertical-align: top;margin: 16px;">
																													<img src="{{asset($logo)}}" alt="" width="70" height="70" img_id={{$user_docs['id']}}>
																												</a>
																											@endforeach
																											</div>
																										@endif
																									@endif
																								@endif
																							@endforeach
																						</div>
																					</div>
																				</div>
																			</div>

																		@else

																			<?php
																				if(isset($value['user_id']) && isset($value['user_id'])){
																					$doc_user = $value['user_id'];
																				}

																				$show_download = true;

																				if(isset($value['imp_doc'])){
																					foreach($value['imp_doc'] as $index => $data){
																						$doc_user = $value['imp_doc'][$index]['user_id'];
																						if($data['status'] == '0' && $show_download){
																							$show_download = false;
																						}
																					}
																				}

																				$document_path = route('site.user.get.documents.path',['user_id' => $doc_user,'type' => $name]);
																			?>

																			<div class="row">
																				<div class="col-xs-12 m-t-15 upload-title">
																					<div class="title">{{strtoupper($name)}}
																						@if($show_download && $user == 'another_user')
																							<button class="download_button btn pull-right">
																								<i class="fa fa-download">
																									<a href="{{$document_path}}">
																										Download
																									</a>
																								</i>
																							</button>
																						@endif
																					</div>
																				</div>
																			</div>
																			@if(isset($value['imp_doc']))
																				@foreach($value['imp_doc'] as $index => $data)
																					<?php
																						$type_document_path = route('site.user.get.documents.path',['user_id' => $data['user_id'],'type' => $name,'type_id' => $data['type_id']]);
																					?>
																					<div class="user_documents_download m-b-15 m-t-15">
																						<div class="row">
																							<div class="col-xs-12">
																								<div class="view_documents">
																									<div class="m-b-15 m-t-15">
																										<div class="title">{{strtoupper($name)}}</div>
																									</div>

																									@if($data['status'] == '0')
																										@if($user != 'another_user')
																											@foreach($data['user_documents'] as $document)
																												<?php
																													$extension = '';
																													if(isset($document['document_path']) && !empty($document['document_path'])){
																														$extension = last(explode('.',$document['document_path']));
																													}

																													if($extension == 'jpg' || $extension == 'jpeg'){
																														$logo = '/images/jpg.jpg';
																													}
																													else if($extension == 'pdf'){
																														$logo = '/images/pdf.png';
																													}
																													else if($extension == 'gif'){
																														$logo = '/images/gif.png';
																													}
																													else if($extension == 'png'){
																														$logo = '/images/png.png';
																													}
																													else if($extension == 'doc' || $extension == 'docx'){
																														$logo = '/images/doc.png';
																													}else {
																														$logo = '/images/file.png';
																													}
																												?>
																												<a href="#" style="display: inline-block;vertical-align: top;margin: 16px;">
																													<img src="{{asset($logo)}}" alt="" width="70" height="70">
																												</a>
																											@endforeach

																											<div class="document_type pull-right">Document Type : <b>Private</b></div>
																										@else

																											<div class="row m-t-15 private-document-request" style="display: flex;align-items: center;">
																												<div class="col-xs-12 col-sm-8 private-document-request-count">
																													There are {{ isset($data['user_documents']) ? count($data['user_documents']) : ''}} private documents.
																												</div>
																												<div class="col-xs-12 col-sm-4 private-document-request-button">
																													<button type="button" class="btn btn-info btn-sm pull-right" data-toggle="modal" data-target="#download_document_list">Request for documents</button>
																												</div>
																											</div>
																										@endif
																									@else


																										@foreach($data['user_documents'] as $document)

																											<?php
																												$extension = '';
																												if(isset($document['document_path']) && !empty($document['document_path'])){
																													$extension = last(explode('.',$document['document_path']));
																												}

																												if($extension == 'jpg' || $extension == 'jpeg'){
																													$logo = '/images/jpg.jpg';
																												}
																												else if($extension == 'pdf'){
																													$logo = '/images/pdf.png';
																												}
																												else if($extension == 'gif'){
																													$logo = '/images/gif.png';
																												}
																												else if($extension == 'png'){
																													$logo = '/images/png.png';
																												}
																												else if($extension == 'doc' || $extension == 'docx'){
																													$logo = '/images/doc.png';
																												}else {
																													$logo = '/images/file.png';
																												}
																											?>
																											<a href="#" style="display: inline-block;vertical-align: top;margin: 16px;">
																												<img src="{{asset($logo)}}" alt="" width="70" height="70">
																											</a>
																										@endforeach
																									@endif
																								</div>
																							</div>
																						</div>
																					</div>
																				@endforeach
																			@endif
																		@endif
																	</div>
																	<?php $active_class= "hide"; ?>
																@endforeach
															@else
																<div class="row">
																	<div class="col-xs-12 text-center">
																		<div class="discription">
																			<span class="content-head">No Documents Found</span>
																		</div>
																	</div>
																</div>
															@endif
														</div>
														<!-- <div class="documents_section">

														</div> -->
													@endif
												@endif

												@if($current_route == 'user.profile')
													<input type="hidden" class="profile_status" value="own profile">
													<div class="documents_section">

													</div>
												@endif
											</div>


											<div id="request" class="tab-pane fade">
												<div class="sub-details-container  m-t-25">
													<div class="content-container">
														@if(!isset($document_permissions) && empty($document_permissions))
															<div class="row">
																<div class="col-xs-12 text-center">
																	<div class="discription">
																		<span class="content-head">No Data Found</span>
																	</div>
																</div>
															</div>
														@else
															@foreach($document_permissions as $doc_permissions)
																<div class="row">
																	<div class="col-xs-12 col-sm-6 col-md-3">
				                                                        <div class="discription">
																			@if($doc_permissions['requester']['registered_as'] == 'institute')
																				<span class="content-head">Institute Name:</span>
																			@endif
																			@if($doc_permissions['requester']['registered_as'] == 'company')
																				<span class="content-head">Company Name:</span>
																			@endif
				                                                            <span class="content">
																				@if($doc_permissions['requester']['registered_as'] == 'institute')
																					{{ isset($doc_permissions['requester']['institute_registration_detail']['institute_name']) ? ucwords($doc_permissions['requester']['institute_registration_detail']['institute_name']) : '-' }}
																				@endif
																				@if($doc_permissions['requester']['registered_as'] == 'company')
																					{{ isset($doc_permissions['requester']['company_registration_detail']['company_name']) ? ucwords($doc_permissions['requester']['company_registration_detail']['company_name']) : '-' }}
																				@endif
				                                                        	</span>
				                                                        </div>
				                                                    </div>
																	<div class="col-xs-12 col-sm-6 col-md-4">
				                                                        <div class="discription">
																			@if($doc_permissions['requester']['registered_as'] == 'institute')
																				<span class="content-head">Institute Email:</span>
																			@endif
																			@if($doc_permissions['requester']['registered_as'] == 'company')
																				<span class="content-head">Company Email:</span>
																			@endif
				                                                            <span class="content">
																				@if($doc_permissions['requester']['registered_as'] == 'institute')
																					{{ isset($doc_permissions['requester']['institute_registration_detail']['email']) ? ucwords($doc_permissions['requester']['institute_registration_detail']['email']) : '-' }}
																				@endif
																				@if($doc_permissions['requester']['registered_as'] == 'company')
																					{{ isset($doc_permissions['requester']['company_registration_detail']['email']) ? $doc_permissions['requester']['company_registration_detail']['email'] : '-'}}
																				@endif
				                                                        	</span>
				                                                        </div>
				                                                    </div>

																	<div class="col-xs-12 col-sm-6 col-md-3">
																		<div class="discription">
																			<span class="content-head">Document Type:</span>
																			<span class="content">
																			@if(isset($doc_permissions['document_type']) && !empty($doc_permissions['document_type']))

																				@foreach($doc_permissions['document_type'] as $k => $documents)
																					{{strToUpper($documents['type']['type'])}}
																					@if($documents['type']['type_id'] != '0')
																						{{$documents['type']['type_id']}}
																					@endif

																					@if(count($doc_permissions['document_type']) > $k+1)
																						,
																					@endif
																				@endforeach
																			@endif
																		</span>
																		</div>
																	</div>
																	<div class="col-xs-12 col-sm-6 col-md-2">
																		<div class="discription">
																			<span class="content-head">Status:</span>
																			<span class="content">
																			@if($doc_permissions['status'] == '0')
																				<span class="status-{{$doc_permissions['id']}}">
																					Pending
																				</span>

																				<div class="accept_reject_buttons">
																					<button class="default-site-button-style permission_status button-status-{{$doc_permissions['id']}} ladda-button" data-style="zoom-in" data-value="Accept" data-document-permission-id="{{$doc_permissions['id']}}" data-requester-id="{{$doc_permissions['requester_id']}}" data-owner-id="{{$doc_permissions['owner_id']}}"> Accept </button>

																					<button class="add-course-batch-button permission_status button-status-{{$doc_permissions['id']}} ladda-button" data-style="zoom-in" data-value="Reject"  data-document-permission-id="{{$doc_permissions['id']}}" data-requester-id="{{$doc_permissions['requester_id']}}" data-owner-id="{{$doc_permissions['owner_id']}}"> Reject </button>
																				</div>
																			@elseif($doc_permissions['status'] == '1')
																				<span>Accepted</span>
																			@else
																				<span>Rejected</span>
																			@endif
																			</span>
																		</div>
																	</div>
																</div>
																<hr/>
															@endforeach
														@endif
													</div>
												</div>
											</div>

										</div>
									@endif
									</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade" id="download_document_list" role="dialog">
		<div class="modal-dialog">
			<form id="request_document_form" action="{{route('site.user.store.requested.documents.list')}}">
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Private Document List</h4>
					</div>

						<div class="modal-body">
							<div class="checkbox_data">
								<ul class="checkbox_list">
									@if(isset($user_documents) && !empty($user_documents))
										@foreach($user_documents as $name => $val)
											@if(isset($val[0]) && !empty($val[0]))
                                                @if($val[0]['status'] == '0')
                                                    <li class="main_list">
                                                        <label class="main_list_input">
                                                            <input type="checkbox" class="con_parent_document_category get_value" value="{{$val[0]['id']}}" data-onwer-id="{{$val[0]['user_id']}}"/> {{strtoupper($name)}}
                                                    </label>
                                                @endif

                                            @else
												<?php
													$show_block = false;
													foreach($val as $index => $val_new){
														if($val_new['status'] == '0'){
															$show_block = true;
														}
													}
												?>

												@if($show_block)
												<li class="main_list">
													<label class="main_list_input">
														<input type="checkbox" class="con_parent_document_category"/> {{strtoupper($name)}}
													</label>
													@foreach($val as $index => $val_new)
														@if($val_new['status'] == '0')
															<ul class="main_sub_list">
																<li><label class="sub_list_input"><input type="checkbox" class="sub_list con_child_document_category get_value" value="{{$val_new['id']}}" data-onwer-id="{{$val_new['user_id']}}"/> {{strtoupper($name)}} {{$index}} </label></li>
															</ul>
														@endif
													@endforeach
												</li>
												@endif
											@endif
										@endforeach
									@else
										<div class="no-data">
											No Data Found.
										</div>
									@endif
								</ul>
							</div>
						</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default ladda-button" id="request_button" data-style="zoom-in">Request</button>
					</div>
				</div>
			</form>

		</div>
	</div>
@stop
@section('js_script')
	<script type="text/javascript" src="/js/site/registration.js"></script>
	<script type="text/javascript" src="/js/site/company-registration.js"></script>
@stop