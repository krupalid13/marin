@if($type == 'contact')
    <div class="profile-details">
        <p><b>Email : </b>{{$userData->email}} 
            @if($userData->is_email_verified == 1)
                <i class="fa fa-check-circle" title="Verified"></i>
            @else
                <i class="fa fa-times-circle text-danger" title="Verified"></i>
            @endif
        </p>
        <p><b>Mobile number : </b>  {{ isset($userData->country_code) && !empty($userData->country_code) ?  $userData->country_code : '91' }}&nbsp; {{$userData->mobile}} 
            <!--<i class="fa fa-check-circle"></i>-->
        </p>
        <p><b>Altertnate number : </b> {{ isset($userData->landline_code) && !empty($userData->landline_code) ?  $userData->landline_code : '91' }}&nbsp; {{$userPersonalData['landline'] ? $userPersonalData->landline : '&nbsp;&nbsp;-'}}</p>
        <p><b>Nearest Airport : </b>{{$userPersonalData['nearest_place'] ? $userPersonalData->nearest_place : '&nbsp;&nbsp;-'}}</p>
        <p><b>Residential Address : </b>
            @if(isset($userPersonalData['permanent_add']) && !empty($userPersonalData['permanent_add']))
                {{$userPersonalData['permanent_add']}}
            @else
                 &nbsp;&nbsp;-
            @endif
            @if(isset($userPersonalData['permanent_add2']) && !empty($userPersonalData['permanent_add2']))
                , {{$userPersonalData['permanent_add2']}}
            @endif
            @if(isset($userLocationCity) && !empty($userLocationCity))
                , {{$userLocationCity}}
            @endif
            @if(isset($userLocationState) && !empty($userLocationState))
                , {{$userLocationState}}
            @endif
            @if(isset($userLocationCountry) && !empty($userLocationCountry))
                , {{$userLocationCountry}}
            @endif
            @if(isset($userLocationPostalCode) && !empty($userLocationPostalCode))
                , {{$userLocationPostalCode}}
            @endif
        </p>
    </div>
@endif
@if($type == 'personal')
    <div class="profile-details">
        <p><b>DOB | POB : </b>{{$userPersonalData['dob'] ? \Carbon\Carbon::parse($userPersonalData->dob)->format('d-m-Y') : '&nbsp;&nbsp;'}} <span style="color:#aaaaaa;"> | </span> {{$userPersonalData['place_of_birth'] ? $userPersonalData['place_of_birth'] : '-'}}
        <p><b>Age : </b>{{\Carbon\Carbon::parse($userPersonalData['dob'])->diff(\Carbon\Carbon::now())->format('%yy, %mm %dd')}}</p>
        <p><b>Marital Status : </b>{{$userPersonalData['marital_status'] ? $mStatus[$userPersonalData['marital_status']] : '&nbsp;&nbsp;-'}}</p>
        <p><b>Next Of Kin : </b>{{$userPersonalData['kin_name']}}{{$userPersonalData['kin_relation'] ? '('.$userPersonalData['kin_relation'].')' : ''}} {{ isset($userPersonalData['kin_number_code']) && !empty($userPersonalData['kin_number_code']) ?  $userPersonalData['kin_number_code'] : '91' }}&nbsp;  {{$userPersonalData['kin_number']}}</p>
        <p><b>Academics Qualification : </b>{{$userProData->school_qualification ? $userProData->school_qualification : '&nbsp;&nbsp;-'}}
        </p>
    </div>
@endif
@if($type == 'professional')
    <div class="profile-details">
        <p><b>Current Rank : </b>@foreach(\CommonHelper::new_rank() as $index => $category)
            @foreach($category as $r_index => $rank)
                {{ !empty($userProData['current_rank']) ? $userProData['current_rank'] == $r_index ? $rank : '' : ''}}
            @endforeach
        @endforeach</p>
        @php
            $exp = explode('.',$userProData['current_rank_exp']);
        @endphp
        <p><b>Grade : </b>
            @if(isset($userWkfrDetail['wk_cop']) && !empty($userWkfrDetail['wk_cop']) && isset($userWkfrDetail['type']) && !empty($userWkfrDetail['type']))
                {{ strtoupper($userWkfrDetail['wk_cop']).' '.$userWkfrDetail['type'] }}
            @else
                @php
                    $gradeExist = false;
                @endphp
                @foreach($userCocDetail as $userCocDetail_Index => $userCocDetail_Data)
                    @php
                    $country = \CommonHelper::countries();
                    @endphp
                    @if(isset($country[$userCocDetail_Data['coc']]) && !empty($country[$userCocDetail_Data['coc']]))
                        @php
                            $gradeExist = true;
                        @endphp
                        @if($userCocDetail_Index != 0)
                        <span style="color:#aaaaaa;"> {{' | '}}</span>
                        @endif
                        {{$userCocDetail_Data['coc_grade'] .' - '. $country[$userCocDetail_Data['coc']]}}
                    @endif
                @endforeach
                @if($gradeExist == false)
                    {{'&nbsp;&nbsp;-'}}
                @endif
            @endif
            </p>
        <p><b>Experience : </b>{{$exp[0].' Years '. $exp[1].' Month'}}</p>
        <p><b>Date Of Availability : </b>{{\Carbon\Carbon::parse($userProData['availability'])->format('d-m-Y')}}</p>
        <p><b>Expected Wages : </b>
            @if(isset($userProData['last_salary']) && !empty($userProData['last_salary']))
                @if(isset($userProData['currency']) && $userProData['currency'] == 'dollar')
                    <i class="fa fa-dollar"></i> {{$userProData['last_salary']}} p/m
                @else
                    <i class="fa fa-inr"></i> {{$userProData['last_salary']}} p/m
                @endif
            @else
               &nbsp;&nbsp;-
            @endif
            
        </p>
    </div>
@endif

@if($type == 'personality')
    <div class="profile-details">
        @php 
            $gender = '&nbsp;&nbsp;-';
            if($userData->gender == 'M'){
                $gender = 'Male';
            }
            if($userData->gender == 'F'){
                $gender = 'Female';
            }
        @endphp
        <p><b>Gender : </b>{{$gender}}</p>
        <p><b>Height : </b>{{$userPersonalData['height'] ? $userPersonalData['height'] . 'cm' : '&nbsp;&nbsp;-'}} &nbsp;&nbsp;&nbsp;<b>Weight : </b>{{$userPersonalData['weight'] ? $userPersonalData['weight'] . 'kg' : '&nbsp;&nbsp;-'}}</p>
        <p><b>Blood Type : </b>{{$userWkfrDetail['blood_type'] ? $bloodType[$userWkfrDetail['blood_type']] : "&nbsp;&nbsp;-"}}
        <p>
            <b>Coverall Size : </b>
            @if(isset($coverallSize[$userPersonalData['coverall_size']]))
                {{$coverallSize[$userPersonalData['coverall_size']]}} &nbsp;&nbsp;&nbsp;
            @else
                &nbsp;&nbsp;-
            @endif
            <b>Safety Shoe Size : </b>
            @if(isset($safetyShoeSize[$userPersonalData['safety_shoe_size']]))
                {{$safetyShoeSize[$userPersonalData['safety_shoe_size']]}}
            @else
                &nbsp;&nbsp;-
            @endif
        </p>
        <p><b>Known Languages : </b>{!!$knownLanguages ? $knownLanguages : "&nbsp;&nbsp;-"!!}
        {{-- <p><b>ILO Medical : </b>{{$userWkfrDetail['ilo_medical'] == 1 && $userWkfrDetail['ilo_issue_date'] ? 'Validity ' .\Carbon\Carbon::parse($userWkfrDetail['ilo_issue_date'])->format('d-m-Y') : '&nbsp;&nbsp;-'}}</p>
        <p><b>Vacination : </b>{{$userWkfrDetail['yellow_fever'] == 1 && $userWkfrDetail['yf_issue_date'] ? 'Yellow Fever - Validity '. \Carbon\Carbon::parse($userWkfrDetail['yf_issue_date'])->format('d-m-Y') : 'Nill'}} --}}
        </p>
    </div>
@endif

