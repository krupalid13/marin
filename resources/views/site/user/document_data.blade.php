<?php
    $india_value = array_search('India', CommonHelper::countries());
    if (isset($data[0]['professional_detail']['current_rank'])) {
        $required_fields = CommonHelper::rank_required_fields()[$data[0]['professional_detail']['current_rank']];
    }
    if (isset($data[0]['document_permissions']) && !empty($data[0]['document_permissions'])) {
        $document_permissions = $data[0]['document_permissions'];
    }
    if (Auth::check()) {
        $registered_as = Auth::user()->registered_as;
    }
?>
<div class="page-container">
    {{-- PASSPORT --}}
    @if($docsType == '0' || $docsType == '1' || $docsType == 'all')
        <div class="main-title">TRAVELLING DOCUMENTS</div>
    @endif
    @if($docsType == '0' || $docsType == 'all')
    <div class="outer-div">
        <div class="title">PASSPORT</div>
        <div class="passport-div">
           <div class="col-pp-1">
              <div class="col-label-pp-country"><h4>Country: </h4></div>
              <div class="col-data-pp-country">
                 <h4>
                    @if(isset($data[0]['passport_detail']['pass_country']) && !empty($data[0]['passport_detail']['pass_country']))
                       @foreach( CommonHelper::countries() as $c_index => $country)
                          {{ isset($data[0]['passport_detail']['pass_country']) ? $data[0]['passport_detail']['pass_country'] == $c_index ? $country : '' : ''}}
                       @endforeach
                 @else
                       -
                 @endif
                 </h4> 
              </div>
           </div>
  
           <div class="col-pp-2">
              <div class="col-label-pp-nos"><h4>Passport Nos: </h4></div>
              <div class="col-data-pp-nos"><h4>{{ isset($data[0]['passport_detail']['pass_number']) ? $data[0]['passport_detail']['pass_number'] : '-' }}</h4></div>
           </div>
  
           <div class="col-pp-3">
              <div class="col-label-pp-poi"><h4>Issued In: </h4></div>
              <div class="col-data-pp-poi"><h4>{{ isset($data[0]['passport_detail']['place_of_issue']) ? $data[0]['passport_detail']['place_of_issue'] : '-' }}</h4></div>
           </div>
  
           <div class="col-pp-4">
              <div class="col-label-pp-doe"><h4>Expire Dt.: </h4></div>
              <div class="col-data-pp-doe"><h4>{{ isset($data[0]['passport_detail']['pass_expiry_date'])? date('d-m-Y',strtotime($data[0]['passport_detail']['pass_expiry_date'])) : '-' }}</h4></div>
           </div>
        </div> <!-- passport-div -->
     </div> <!-- outer-div -->
     <div class="outer-div-between"></div>
    @endif
    @if($docsType == '1' || $docsType == 'all')
        @if(isset($data[0]['visas']) && count($data[0]['visas']) > 0)
            <div class="outer-div">
                <div class="title">VISA</div>
                @foreach($data[0]['visas'] as $key => $visa)
                    <div class="visa-div" style="{{$key == 0 ? "border-top:none" : ""}}">
                        <div class="col-visa-1">
                            <div class="col-label-visa-country"><h4>Country: </h4></div>
                            <div class="col-data-visa-country">
                                <h4>
                                    @foreach( \CommonHelper::countries() as $c_index => $country)
                                    {{ isset($visa['country_id']) ? $visa['country_id'] == $c_index ? $country : '' : ''}}
                                    @endforeach
                                </h4>
                            </div>
                        </div>
                
                        <div class="col-visa-2">
                            <div class="col-label-visa-type"><h4>Type: </h4></div>
                            <div class="col-data-visa-type"><h4>{{$visa['visa_type']}}</h4></div>
                        </div>
                        
                        <div class="col-visa-3">
                            <div class="col-label-visa-doe"><h4>Expire Dt.: </h4></div>
                            <div class="col-data-visa-doe"><h4>{{(isset($visa['visa_expiry_date']) ? date('d-m-Y',strtotime($visa['visa_expiry_date'])) : '-')}}</h4></div>
                        </div>
                
                        <div class="col-visa-4"></div>
                    </div>
                    {{-- <div class="seperator-line"></div> --}}
                @endforeach
            </div>
            <div class="outer-div-between"></div>
        @endif
    @endif

    @if($docsType == '2' || $docsType == 'all')

        @if(isset($data[0]['wkfr_detail']) && ($data[0]['wkfr_detail']['ilo_medical'] == 1 || $data[0]['wkfr_detail']['screening_test'] == 1))
            <div class="outer-div">
                <div class="title">MEDICAL</div>

                @foreach($medicalArray as $key => $medical)
                    <div class="med-div"  style="{{$key == 0 ? 'border-top:none' : ''}}">
                        <div class="col-med-2">
                            <div class="col-label-med-country">
                                <h4>Country: </h4>
                            </div>
                            <div class="col-data-med-country">
                                <h4>
                                    @foreach( \CommonHelper::countries() as $c_index => $country)
                                    {{ isset($medical['country']) ? $medical['country'] == $c_index ? $country : '' : ''}}
                                    @endforeach
                                </h4>
                            </div>
                        </div>
                        <div class="col-med-1">
                            <div class="col-label-med-type">
                                <h4>Type: </h4>
                            </div>
                            <div class="col-data-med-type">
                                <h4>{{$medical['type']}}</h4>
                            </div>
                        </div>
                        <div class="col-med-3">
                            <div class="col-label-med-doe">
                                <h4>Issue Dt.: </h4>
                            </div>
                            <div class="col-data-med-doe">
                                <h4>{{$medical['issue_on']}}</h4>
                            </div>
                        </div>
                        <div class="col-med-4"></div>
                        <!-- created blank to align boxes -->
                    </div>
                    {{-- <div class="seperator-line"></div> --}}
                @endforeach

                @if(isset($data[0]['wkfr_detail']['screening_test']) && $data[0]['wkfr_detail']['screening_test'] == 1)
                    {{-- <div class="med-div">
                        <div class="col-med-2">
                            <div class="col-label-med-country">
                                <h4>Country: </h4>
                            </div>
                            <div class="col-data-med-country">
                                <h4>
                                    @foreach( \CommonHelper::countries() as $c_index => $country)
                                    {{ isset($data[0]['wkfr_detail']['screening_test_county']) ? $data[0]['wkfr_detail']['screening_test_county'] == $c_index ? $country : '' : ''}}
                                    @endforeach
                                </h4>
                            </div>
                        </div>
                        <div class="col-med-1">
                            <div class="col-label-med-type">
                                <h4>Type: </h4>
                            </div>
                            <div class="col-data-med-type">
                                <h4>Screening Test</h4>
                            </div>
                        </div>
                        <div class="col-med-3">
                            <div class="col-label-med-doe">
                                <h4>Issue Dt.: </h4>
                            </div>
                            <div class="col-data-med-doe">
                                <h4>{{(isset($data[0]['wkfr_detail']['screening_test_date']) ? date('d-m-Y',strtotime($data[0]['wkfr_detail']['screening_test_date'])) : '-')}}</h4>
                            </div>
                        </div>
                        <div class="col-med-4"></div>
                        <!-- created blank to align boxes -->
                    </div> --}}
                    {{-- <div class="seperator-line"></div> --}}
                @endif
            </div>
            <div class="outer-div-between"></div>
        @endif
    @endif

    @if($docsType == '3' || $docsType == 'all')
        @if(isset($data[0]['wkfr_detail']) && (
            $data[0]['wkfr_detail']['yellow_fever'] == 1 || 
            $data[0]['wkfr_detail']['cholera'] == 1 || 
            $data[0]['wkfr_detail']['hepatitis_b'] == 1 || 
            $data[0]['wkfr_detail']['hepatitis_c'] == 1 || 
            $data[0]['wkfr_detail']['diphtheria'] == 1 || 
            $data[0]['wkfr_detail']['covid'] == 1
            ))
            <div class="outer-div">
                <div class="title">VACCINATIONS</div>
                @foreach($vaccineArray as $key => $vaccine)
                <div class="med-div" style="{{$key == 0 ? 'border-top:none' : ''}}">
                    <div class="col-med-2">
                        <div class="col-label-med-country">
                            <h4>Country: </h4>
                        </div>
                        <div class="col-data-med-country">
                            <h4>
                                @foreach( \CommonHelper::countries() as $c_index => $country)
                                {{ isset($vaccine['country']) ? $vaccine['country'] == $c_index ? $country : '' : ''}}
                                @endforeach
                            </h4>
                        </div>
                    </div>
                    <div class="col-med-1">
                        <div class="col-label-med-type">
                            <h4>Type: </h4>
                        </div>
                        <div class="col-data-med-type">
                            <h4>{{$vaccine['type']}}</h4>
                        </div>
                    </div>
                    <div class="col-med-3">
                        <div class="col-label-med-doe">
                            <h4>Expire Dt.: </h4>
                        </div>
                        <div class="col-data-med-doe">
                            @if($vaccine['issue_on'] != '01-01-1970')
                                <h4>{{$vaccine['issue_on']}}</h4>
                            @else
                                <h4>Unlimited</h4>
                            @endif
                        </div>
                    </div>
                    <div class="col-med-4"></div>
                    <!-- created blank to align boxes -->
                </div>
                {{-- <div class="seperator-line"></div> --}}
                @endforeach
            </div>
            <div class="outer-div-between"></div>
            @endif
    @endif
        <!-- SEAMAN BOOK DETAILS -->
    @if($docsType == '4' || $docsType == '5' || $docsType == '11' || $docsType == 'all')
    <div class="main-title">SAILING DOCUMENTS</div>
        @if($docsType == '4' || $docsType == 'all')
            @if(isset($data[0]['wkfr_detail']['indos_number']) && !empty($data[0]['wkfr_detail']['indos_number']))
                <div class="outer-div">
                    <div class="title">INDOS</div>
                    <div class="indos-div">
                        <div class="col-indos-1">
                            <!--this is static and will always show Country as India-->
                            <div class="col-label-indos-country">
                                <h4>Country: </h4>
                            </div>
                            <div class="col-data-indos-country">
                                <h4>India</h4>
                            </div>
                        </div>
                        <div class="col-indos-1">
                            <div class="col-label-indos-number">
                                <h4>Number: </h4>
                            </div>
                            <div class="col-data-indos-number">
                                <h4>{{ !empty($data[0]['wkfr_detail']['indos_number']) ? $data[0]['wkfr_detail']['indos_number'] : '-'}}</h4>
                            </div>
                        </div>
                        <div class="col-indos-2"></div>
                        <div class="col-indos-3"></div>
                    </div>
                </div>
                <div class="outer-div-between"></div>
            @endif
        @endif
        @if($docsType == '11' || $docsType == 'all')
            @if(isset($data[0]['professional_detail']['sid_number']) && !empty($data[0]['professional_detail']['sid_number']))
                <div class="outer-div">
                    <div class="title">SID</div>
                    <div class="indos-div">
                        <div class="col-indos-1">
                            <!--this is static and will always show Country as India-->
                            <div class="col-label-indos-country">
                                <h4>Country: </h4>
                            </div>
                            <div class="col-data-indos-country">
                                <h4>India</h4>
                            </div>
                        </div>
                        <div class="col-indos-1">
                            <div class="col-label-indos-number">
                                <h4>Number: </h4>
                            </div>
                            <div class="col-data-indos-number">
                                <h4>{{ !empty($data[0]['professional_detail']['sid_number']) ? $data[0]['professional_detail']['sid_number'] : '-'}}</h4>
                            </div>
                        </div>
                        <div class="col-indos-1">
                            <div class="col-label-indos-number">
                                <h4>Issued In: </h4>
                            </div>
                            <div class="col-data-indos-number">
                                <h4>{{ !empty($data[0]['professional_detail']['issue_place']) ? $data[0]['professional_detail']['issue_place'] : '-'}}</h4>
                            </div>
                        </div>
                        <div class="col-indos-1">
                            <div class="col-label-indos-number">
                                <h4>Expire Dt.: </h4>
                            </div>
                            <div class="col-data-indos-number">
                                <h4>{{ !empty($data[0]['professional_detail']['sid_expire_date']) ? $data[0]['professional_detail']['sid_expire_date'] : '-'}}</h4>
                            </div>
                        </div>
<!--                        <div class="col-indos-2"></div>
                        <div class="col-indos-3"></div>-->
                    </div>
                </div>
                <div class="outer-div-between"></div>
            @endif
        @endif
        @if($docsType == '5' || $docsType == 'all')
            <div class="outer-div">
                <div class="title">CONTINUES DISCHARGE CERTIFICATE</div>
                @foreach($data[0]['seaman_book_detail'] as $index => $cdc_data)
                    <div class="cdc-div" style="{{$index == 0 ? "border-top:none" : ""}}">
                        <div class="col-cdc-1">
                            <div class="col-label-cdc-country">
                                <h4>Country: </h4>
                            </div>
                            <div class="col-data-cdc-country">
                                <h4>
                                    @foreach( CommonHelper::countries() as $c_index => $country)
                                        {{ isset($cdc_data['cdc']) ? $cdc_data['cdc'] == $c_index ? $country : '' : ''}}
                                    @endforeach
                                </h4>
                            </div>
                        </div>
                        <div class="col-cdc-2">
                            <div class="col-label-cdc-number">
                                <h4>Number: </h4>
                            </div>
                            <div class="col-data-cdc-number">
                                <h4>{{isset($cdc_data['cdc_number']) ? $cdc_data['cdc_number'] : ''}}</h4>
                            </div>
                        </div>
                        <div class="col-cdc-3">
                            <div class="col-label-cdc-doi">
                                <h4>Issue Dt.: </h4>
                            </div>
                            <div class="col-data-cdc-doi">
                                <h4>{{isset($cdc_data['cdc_issue_date']) ? date('d-m-Y',strtotime($cdc_data['cdc_issue_date'])) : ''}}</h4>
                            </div>
                        </div>
                        <div class="col-cdc-4">
                            <div class="col-label-cdc-doe">
                                <h4>Expire Dt.: </h4>
                            </div>
                            <div class="col-data-cdc-doe">
                                <h4>
                                    {{isset($cdc_data['cdc_expiry_date']) ? date('d-m-Y',strtotime($cdc_data['cdc_expiry_date'])) : ''}}
                                    @if($cdc_data['status'] == 1)
                                        <i class="fa fa-check-circle green f-15 m-l-15"
                                        aria-hidden="true"
                                        title="Verified"></i>
                                    @endif
                                </h4>
                            </div>
                        </div>
                    </div>
                    {{-- <div class="seperator-line"></div> --}}
                @endforeach
            </div>
            <div class="outer-div-between"></div>
        @endif
    @endif
        <!-- CERTIFICATE OF COMPENTENCY -->
    @if(isset($required_fields) && ($docsType == '6' || $docsType == 'all'))
        @if(isset($data[0]['coc_detail']) AND !empty($data[0]['coc_detail']) && (in_array('COC',$required_fields) OR in_array('COC-Optional',$required_fields)))
            <div class="outer-div">
                <div class="title">CERTIFICATE OF COMPENTENCY</div>
                @foreach($data[0]['coc_detail'] as $index => $coc_data)
                    <div class="coc-div" style="{{$index == 0 ? "border-top:none" : ""}}">
                        <div class="col-coc-1">
                            <div class="col-label-coc-country">
                                <h4>Country: </h4>
                            </div>
                            <div class="col-data-coc-country">
                                <h4>
                                    @if(isset($coc_data['coc']) && !empty($coc_data['coc']))
                                        @foreach( CommonHelper::countries() as $c_index => $country)
                                            {{ isset($coc_data['coc']) ? $coc_data['coc'] == $c_index ? $country : '' : ''}}
                                        @endforeach
                                    @else
                                        -
                                    @endif
                                </h4>
                            </div>
                        </div>
                        <div class="col-coc-2">
                            <div class="col-label-coc-number">
                                <h4>Number: </h4>
                            </div>
                            <div class="col-data-coc-number">
                                <h4>{{ !empty($coc_data['coc_number']) ? $coc_data['coc_number'] : '-'}}</h4>
                            </div>
                        </div>
                        <div class="col-coc-3">
                            <div class="col-label-coc-grade">
                                <h4>Grade: </h4>
                            </div>
                            <div class="col-data-coc-grade">
                                <h4>{{ !empty($coc_data['coc_grade']) ? $coc_data['coc_grade'] : '-' }}</h4>
                            </div>
                        </div>
                        <div class="col-coc-4">
                            <div class="col-label-coc-doe">
                                <h4>Expire Dt.: </h4>
                            </div>
                            <div class="col-data-coc-doe">
                                <h4>
                                    {{ !is_null($coc_data['coc_expiry_date']) ? date('d-m-Y',strtotime($coc_data['coc_expiry_date'])) : '-' }}

                                    @if(isset($coc_data['status']) && $coc_data['status'] == 1)
                                        <i class="fa fa-check-circle green f-15 m-l-15"
                                        aria-hidden="true"
                                        title="Verified"></i>
                                    @endif

                                </h4>
                            </div>
                        </div>
                    </div>
                    {{-- <div class="seperator-line"></div> --}}
                @endforeach
            </div>
            <!-- outer-div -->
            <div class="outer-div-between"></div>
        @endif
    @endif
        <!-- CERTIFICATE OF ENDORSEMENT -->
    @if(isset($required_fields) && ($docsType == '7' || $docsType == 'all'))
        @if(isset($data[0]['coe_detail']) AND !empty($data[0]['coe_detail']) && (in_array('COE',$required_fields) OR in_array('COE-Optional',$required_fields)) && (!empty($data[0]['coe_detail'][0]['coe']) || !empty($data[0]['coe_detail'][0]['coe_number']) || !empty($data[0]['coe_detail'][0]['coe_grade']) || !empty($data[0]['coe_detail'][0]['coe_expiry_date']) || !empty($data[0]['coe_detail'][0]['coe_verification_date'])))
            <div class="outer-div">
                <div class="title">CERTIFICATE OF ENDORSEMENT</div>
                @foreach($data[0]['coe_detail'] as $index => $coe_data)
                    <div class="coe-div" style="{{$index == 0 ? "border-top:none" : ""}}">
                        <div class="col-coe-1">
                            <div class="col-label-coe-country">
                                <h4>Country: </h4>
                            </div>
                            <div class="col-data-coe-country">
                                <h4>
                                    @if(isset($coe_data['coe']) && !empty($coe_data['coe']))
                                        @foreach( CommonHelper::countries() as $c_index => $country)
                                            {{ isset($coe_data['coe']) ? $coe_data['coe'] == $c_index ? $country : '' : ''}}
                                        @endforeach
                                    @else
                                        -
                                    @endif
                                </h4>
                            </div>
                        </div>
                        <div class="col-coe-2">
                            <div class="col-label-coe-number">
                                <h4>Number: </h4>
                            </div>
                            <div class="col-data-coe-number">
                                <h4>{{ !empty($coe_data['coe_number']) ? $coe_data['coe_number'] : '-'}}</h4>
                            </div>
                        </div>
                        <div class="col-coe-3">
                            <div class="col-label-coe-grade">
                                <h4>Grade: </h4>
                            </div>
                            <div class="col-data-coe-grade">
                                <h4>{{ !empty($coe_data['coe_grade']) ? $coe_data['coe_grade'] : '-' }}</h4>
                            </div>
                        </div>
                        <div class="col-coe-4">
                            <div class="col-label-coe-doe">
                                <h4>Expire Dt.: </h4>
                            </div>
                            <div class="col-data-coe-doe">
                                <h4>{{ !empty($coe_data['coe_expiry_date']) ? date('d-m-Y',strtotime($coe_data['coe_expiry_date'])) : '-' }}</h4>
                            </div>
                        </div>
                    </div>
                    {{-- <div class="seperator-line"></div> --}}
                @endforeach
            </div>
            <div class="outer-div-between"></div>
        @endif
    @endif
    @if(($docsType == '8' || $docsType == 'all') && $sailingCertificate)
            @if($docsType == '8')
                <div class="main-title">SAILING DOCUMENTS</div>
            @endif
        <div class="outer-div">
            <div class="title">
                @if(isset($userWkfrDetail['wk_cop']))
                    @if($userWkfrDetail['wk_cop'] == 'cop')
                        CERTIFICATE OF PROFICIENCY
                    @elseif($userWkfrDetail['wk_cop'] == 'wk')
                        WATCHKEEPING
                    @else
                        SIALING CERTIFICATE
                    @endif
                @else
                    SIALING CERTIFICATE
                @endif
            </div>
            <!--To show which radiobutton COP or WK is selected by the user in the form  -->
            <div class="cdc-div" style="border-top:none">
                @if($userWkfrDetail['watch_keeping'])
                    <div class="col-cdc-1">
                        <div class="col-label-cdc-country">
                            <h4>Country: </h4>
                        </div>
                        <div class="col-data-cdc-country">
                            <h4>
                                @foreach( CommonHelper::countries() as $c_index => $country)
                                    {{ isset($userWkfrDetail['watch_keeping']) ? $userWkfrDetail['watch_keeping'] == $c_index ? $country : '' : ''}}
                                @endforeach
                            </h4>
                        </div>
                    </div>
                @endif
                @if($userWkfrDetail['wkfr_number'])
                    <div class="col-cdc-2">
                        <div class="col-label-cdc-number">
                            <h4>Number: </h4>
                        </div>
                        <div class="col-data-cdc-number">
                            <h4>{{$userWkfrDetail['wkfr_number']}}</h4>
                        </div>
                    </div>
                @endif
                @if($userWkfrDetail['issue_date'])
                    <div class="col-cdc-3">
                        <div class="col-label-cdc-doi">
                            <h4>Issue Dt.: </h4>
                        </div>
                        <div class="col-data-cdc-doi">
                            <h4>{{\Carbon\Carbon::parse($userWkfrDetail['issue_date'])->format('d-m-Y')}}</h4>
                        </div>
                    </div>
                @endif
                @if($userWkfrDetail['wk_cop'] || $userWkfrDetail['type'])
                    <div class="col-cdc-4">
                        <div class="col-label-cdc-doe">
                            <h4>Grade: </h4>
                        </div>
                        <div class="col-data-cdc-doe">
                            <h4>
                                {{($userWkfrDetail['wk_cop'] && $userWkfrDetail['type'] ? strtoupper($userWkfrDetail['wk_cop']).' '.$userWkfrDetail['type'] : $userCocDetail['coc_grade']) }}
                            </h4>
                        </div>
                    </div>
                @endif
            </div>
        </div>
        <div class="outer-div-between"></div>
    @endif
        <!-- GMDSS DETAILS -->
    @if(isset($required_fields) && !empty($required_fields)  && ($docsType == '9' || $docsType == 'all'))
        @if(isset($data[0]['gmdss_detail']) AND !empty($data[0]['gmdss_detail']) && (in_array('GMDSS',$required_fields) OR in_array('GMDSS-Optional',$required_fields)) && ((!empty($data[0]['gmdss_detail']['gmdss'])) || (!empty($data[0]['gmdss_detail']['gmdss_number'])) || (!empty($data[0]['gmdss_detail']['gmdss_expiry_date']))))
            <div class="outer-div">
                <div class="title">GLOBAL MARITIME DISTRESS AND SAFETY SYSTEM</div>
                <div class="gmdss-div1">
                    <div class="col-gmdss-1">
                        <div class="col-label-gmdss-country">
                            <h4>Country: </h4>
                        </div>
                        <div class="col-data-gmdss-country">
                            <h4>
                                @foreach( CommonHelper::countries() as $c_index => $country)
                                    {{ isset($data[0]['gmdss_detail']['gmdss']) ? $data[0]['gmdss_detail']['gmdss'] == $c_index ? $country : '' : ''}}
                                @endforeach
                            </h4>
                        </div>
                    </div>
                    <div class="col-gmdss-2">
                        <div class="col-label-gmdss-number">
                            <h4>Number: </h4>
                        </div>
                        <div class="col-data-gmdss-number">
                            <h4>{{ isset($data[0]['gmdss_detail']['gmdss_number']) ? $data[0]['gmdss_detail']['gmdss_number'] : '-'}}</h4>
                        </div>
                    </div>
                    <div class="col-gmdss-3">
                        <div class="col-label-gmdss-doe">
                            <h4>Expire Dt.: </h4>
                        </div>
                        <div class="col-data-gmdss-doe">
                            <h4>{{ isset($data[0]['gmdss_detail']['gmdss_expiry_date']) ? date('d-m-Y',strtotime($data[0]['gmdss_detail']['gmdss_expiry_date'])) : '-' }}</h4>
                        </div>
                    </div>
                    <div class="col-gmdss-4"></div>
                    <!-- created blank to align boxes -->
                </div>

                <div class="gmdss-div2">
                    <div class="col-gmdss-endorse-1">
                        <!--this is static and will always show Country as India-->
                        <div class="col-label-gmdss-endorse-country">
                            <h4>Country: </h4>
                        </div>
                        <div class="col-data-gmdss-endorse-country">
                            <h4>India</h4>
                        </div>
                    </div>
                    <div class="col-gmdss-endorse-2">
                        <div class="col-label-gmdss-endorse-number">
                            <h4>Endorse:</h4>
                        </div>
                        <div class="col-data-gmdss-endorse-number">
                            <h4>{{ isset($data[0]['gmdss_detail']['gmdss_endorsement_number']) ? $data[0]['gmdss_detail']['gmdss_endorsement_number'] : '-'}}</h4>
                        </div>
                    </div>
                    <div class="col-gmdss-endorse-3">
                        <div class="col-label-gmdss-endorse-doe">
                            <h4>Expire Dt.: </h4>
                        </div>
                        <div class="col-data-gmdss-endorse-doe">
                            <h4>{{ isset($data[0]['gmdss_detail']['gmdss_endorsement_expiry_date']) ? date('d-m-Y',strtotime($data[0]['gmdss_detail']['gmdss_endorsement_expiry_date'])) : '-' }}</h4>
                        </div>
                    </div>
                    <div class="col-gmdss-endorse-4"></div>
                    <!-- created blank to align boxes -->
                </div>
            </div>

            <div class="outer-div-between"></div>
        @else
            @if($docsType != 'all')
                No records available
            @endif
        @endif
    @endif
    {{-- {{dd()}} --}}
    @php
        $dceDetails = isset($data[0]['user_dangerous_cargo_endorsement_detail']) ? $data[0]['user_dangerous_cargo_endorsement_detail'] : null;
        $dceStatus = !empty($dceDetails) ? json_decode($dceDetails['status']) : null;
    @endphp
    @if(isset($required_fields) && ($docsType == '10' || $docsType == 'all'))
        @if(isset($data[0]['user_dangerous_cargo_endorsement_detail']) AND !empty($data[0]['user_dangerous_cargo_endorsement_detail']) && (in_array('DCE',$required_fields) OR in_array('DCE-Optional',$required_fields)) && !empty($dceStatus->type))



            <div class="outer-div">
                <div class="title">DANGEROUS CARGO ENDORSEMENT</div>
                @foreach($dceStatus->type as $index => $dceData)
                    @php
                        $dceDetailsData = json_decode($dceDetails[$dceData]);
                    @endphp
                    <div class="dce-div1" style="{{$index == 0 ? "border-top:none" : ""}}">
                        <div class="col-dce-1">
                            <div class="col-label-dce-country">
                                <h4>Country: </h4>
                            </div>
                            <div class="col-data-dce-country">
                                <h4>
                                    @if(isset($dceDetailsData->country) && !empty($dceDetailsData->country))
                                        {{ \CommonHelper::countries()[$dceDetailsData->country] }}
                                    @else
                                        -
                                    @endif
                                </h4>
                            </div>
                        </div>
                        <div class="col-dce-2">
                            <div class="col-label-dce-number">
                                <h4>Number: </h4>
                            </div>
                            <div class="col-data-gmdss-number">
                                <h4>
                                    @if(isset($dceDetailsData->number) && !empty($dceDetailsData->number))
                                        {{ $dceDetailsData->number }}
                                    @else
                                        -
                                    @endif
                                </h4>
                            </div>
                        </div>
                        <div class="col-dce-3">
                            <div class="col-label-dce-type">
                                <h4>Type: </h4>
                            </div>
                            <div class="col-data-dce-type">
                                <h4>
                                    @if ($dceData == 'lequefied_gas')
                                        {{ 'Liquefied Gas'}}
                                    @elseif ($dceData == 'all')
                                        {{ 'Oil + Chemical + Liquefied Gas'}}
                                    @else
                                        {{ ucwords(strtolower($dceData)) }}
                                    @endif
                                    
                                </h4>
                            </div>
                        </div>
                        <div class="col-dce-4">
                            <div class="col-label-dce-grade">
                                <h4>Grade: </h4>
                            </div>
                            <div class="col-data-dce-type">
                                <h4>
                                    @if(isset($dceDetailsData->grade))
                                        @if($dceDetailsData->grade == 0)
                                            {{ 'Level I' }}
                                        @else
                                            {{ 'Level II' }}
                                        @endif
                                    @else
                                        -
                                    @endif
                                </h4>
                            </div>
                        </div>
                    </div>
                    <div class="dce-div2">
                        <div class="col-dce-row2-1">
                            <div class="col-label-dce-poi">
                                <h4>Issued At: </h4>
                            </div>
                            <div class="col-data-dce-poi">
                                <h4>
                                    @if(isset($dceDetailsData->place_of_issue) && !empty($dceDetailsData->place_of_issue))
                                        {{  $dceDetailsData->place_of_issue }}
                                    @endif
                                </h4>
                            </div>
                        </div>
                        <div class="col-dce-row2-2">
                            <div class="col-label-dce-doi">
                                <h4>Issue Dt.:</h4>
                            </div>
                            <div class="col-data-dce-doi">
                                <h4>
                                    @if(isset($dceDetailsData->date_of_issue) && !empty($dceDetailsData->date_of_issue))
                                        {{  \Carbon\Carbon::parse($dceDetailsData->date_of_issue)->format('d-m-Y') }}
                                    @endif
                                </h4>
                            </div>
                        </div>
                        <div class="col-dce-row2-3">
                            <div class="col-label-dce-doe">
                                <h4>Expire Dt.: </h4>
                            </div>
                            <div class="col-data-dce-doe">
                                <h4>
                                    @if(isset($dceDetailsData->date_of_expiry) && !empty($dceDetailsData->date_of_expiry))
                                        {{  \Carbon\Carbon::parse($dceDetailsData->date_of_expiry)->format('d-m-Y') }}
                                    @endif
                                </h4>
                            </div>
                        </div>
                        <div class="col-dce-row2-4"></div>
                        <!-- created blank to align boxes -->
                    </div>
                    {{-- <div class="seperator-line"></div> --}}
                @endforeach
            </div>
            <!-- outer-div -->
            <div class="outer-div-between"></div>
        @endif
    @endif
</div