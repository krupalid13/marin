@extends('site.index')
@section('content')

<div class="login_home_page content-section-wrapper">
	<div class="section container">
	<div class="row">
		<div class="col-xs-12">
			<div id="availability-modal" class="availability-modal">
		        <div class="modal-dialog modal-dialog-availability">
		            <form id='rank_availability' action="{{route('site.user.availability.change')}}" method="post">
		                    <div class="modal-header" style="background: #fff;">
		                        <h4 class="modal-title">Update Availability</h4>
		                    </div>
		                    <div class="modal-body">
		                        <p>Please update your availability so that we can match your resume to the appropriate jobs for you.</p>
		                        <div class="row">
		                            <div class="col-xs-6">
		                                <div class="form-group">
		                                    <label class="input-label">Rank Applied For <span class="symbol required"></span>
		                                    </label>
		                                    <select id="applied_rank" name="applied_rank" class="form-control">
		                                        <option value="">Select Your Rank</option>
		                                        @foreach(\CommonHelper::new_rank() as $index => $category)
		                                            <optgroup label="{{$index}}"></optgroup>
		                                            @foreach($category as $r_index => $rank)
		                                                <option value="{{$r_index}}" {{ !empty($current_rank) ? $current_rank == $r_index ? 'selected' : '' : ''}}>{{$rank}} </option>
		                                            @endforeach
		                                        @endforeach
		                                    </select>
		                                </div>
		                            </div>
		                            <div class="col-xs-6">
		                                <div class="form-group">
		                                    <label class="input-label">Date of Availability <span class="symbol required"></span>
		                                    </label>
		                                    <input type="text" class="form-control datepicker" id="date_avaibility" name="date_avaibility" value="{{ isset($availability) ? date('d-m-Y',strtotime($availability)) : ''}}" placeholder="dd-mm-yyyy">
		                                </div>
		                            </div>
		                        </div>
		                    </div>
		                    <div class="modal-footer">
		                        <button type="button" class="btn btn-1 fullwidth btn-default availability_submit_btn ladda-button" data-style="zoom-in">Save</button>
		                    </div>
		            </form>
		        </div>
		    </div>
		</div>
	</div>
	</div>
</div>

@stop