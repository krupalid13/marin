@extends('site.index')

@section('content')
<?php
	$current_date = time();
	$rpsl_company_id = array_search('RPSL Company',\CommonHelper::company_type());
?>
<div class="search-listing content-section-wrapper">
	<div class="container">
		<div class="search-listing-container">
			<button class="btn search-listing-sm-filter-btn" type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#search-listing-filter-modal"><i class="fa fa-filter" aria-hidden="true"></i></button>
			<div class="row">
				<div class="col-sm-3">
					<div class="filter-container filter-container-lg">
						<div class="heading">
							Filter
						</div>
						<form id="seafarer-job-search-filter" action="{{route('site.seafarer.job.search')}}" method="get">
							{{csrf_field()}}
							<?php
								$data1 = $_GET;
								if(isset($data1['param'])){
									$data1 = base64_decode($data1['param']);

						            $data1 = parse_url($data1);
						            parse_str($data1['path'], $data1);
						        }
							?>
							<input type="hidden" name="auto_apply" id="auto_apply" value="{{ isset($data1['auto_apply']) ? $data1['auto_apply'] : ''}}">
							<input type="hidden" name="job_id" id="job_id" value="{{ isset($data1['job_id']) ? $data1['job_id'] : ''}}">
							<input type="hidden" name="user_id" id="user_id" value="{{ isset($data1['user_id']) ? $data1['user_id'] : ''}}">
							<input type="hidden" name="company_id" id="company_id" value="{{ isset($data1['company_id']) ? $data1['company_id'] : ''}}">
							<div class="content-container">
								<div class="form-group">
				                    <label class="input-label" for="company_name">Company Name</label>
				                    <input type="text" class="form-control" id="company_name" name="company_name" value="{{ isset($search_data['company_name']) && !empty($search_data['company_name']) ? $search_data['company_name'] : ''}}" placeholder="Enter Company Name">
				                </div>
				                <div class="form-group">
				                    <label class="input-label" for="date_of_joining">Joining Date</label>
				                    <i class="fa fa-calendar calendar-icon" aria-hidden="true"></i>
				                    <input type="text" class="form-control datepicker" id="date_of_joining" name="date_of_joining" value="" placeholder="dd-mm-yyyy">
				                </div>
				                <div class="form-group">
									<label class="input-label">Nationality</label>
									<select id="nationality" name="nationality" class="form-control">
										<option value=>Select Nationality</option>
										@foreach(\CommonHelper::countries() as $n_index => $nationality)
			                                <option value="{{$n_index}}" {{ !empty($search_data['nationality']) ? $search_data['nationality'] == $n_index ? 'selected' : '' : ''}}>{{$nationality}}</option>
			                            @endforeach
									</select>
								</div>
								<div class="form-group">
									<label class="input-label">Rank</label>
									<select id="rank" name="rank" class="form-control">
		                                <option value=>Select Rank</option>
		                                @foreach(\CommonHelper::new_rank() as $index => $category)
                                            <optgroup label="{{$index}}">
                                            @foreach($category as $r_index => $rank)
                                                <option value="{{$r_index}}" {{ !empty($search_data['rank']) ? $search_data['rank'] == $r_index ? 'selected' : '' : ''}}>{{$rank}} </option>
                                            @endforeach
                                        @endforeach
		                            </select>
								</div>
								<div class="form-group">
				                    <label class="input-label" for="ship">Ship type</label>
					                <select id="ship_type" name="ship_type" class="form-control">
			                            <option value=''>Select Ship Type</option>
			                            @foreach(\CommonHelper::ship_type() as $index => $ships)
			                                <option value="{{$index}}" {{ !empty($search_data['ship_type']) ? $search_data['ship_type'] == $index ? 'selected' : '' : ''}}>{{$ships}}</option>
			                            @endforeach
			                        </select>
				                </div>
				                <div class="form-group">
				                    <label class="input-label" for="min_rank_exp">Minimum Work Exp</label>
					                <!-- <input type="text" class="form-control" id="min_rank_exp" name="min_rank_exp" value="{{isset($search_data['min_rank_exp']) ? $search_data['min_rank_exp'] : ''}}" placeholder="Enter exp"> -->
					                <select id="min_rank_exp" name="min_rank_exp" class="form-control">
                                        <option value=''>Min Exp</option>
                                        @foreach(\CommonHelper::work_experience() as $w_index => $exp)
                                            <option value="{{$w_index}}" {{ !empty($search_data['min_rank_exp']) ? $search_data['min_rank_exp'] == $w_index ? 'selected' : '' : ''}}>{{$exp}} </option>
                                        @endforeach
                                    </select>
				                </div>
				                <div class="form-group">
				                	<button type="submit" data-style="zoom-in" class="btn coss-primary-btn ladda-button" id="seafarer-job-search-button">Search</button>
				                </div>
							</div>
						</form>
					</div>
					@include('site.partials.featured_advertisement_template')
				</div>
				<div class="col-sm-9">
					<div class="sort-by-container">
						<div class="row">
							<div class="col-xs-12">
								<div class="search-title">
									Job Search Results
								</div>
							</div>
						</div>
					</div>

					<div class="card-container">
					@if(isset($data) && !empty($data))
						<div class="row p-t-10 flex-height">
							<div class="col-md-12 col-xs-12">
								<div class="pagi">
					                <div class="search-count p-t-10">
					                    Showing {{$pagination_data['from']}} - {{$pagination_data['to']}} of {{$pagination_data['total']}} Jobs
					                </div>
					                <nav>
						                <ul class="pagination pagination-sm">
						                    {!! $pagination->render() !!}
						                </ul>
					                </nav>
					            </div>
				            </div>
			            </div>
						@foreach($data as $index => $jobs)

							<?php
								$show_apply_button = false;
								if(isset($jobs['user_applied_jobs'][0]['created_at'])){
									$job_create_date = strtotime($jobs['user_applied_jobs'][0]['created_at']);
									$datediff = $current_date - $job_create_date;

									$days = floor($datediff / (60 * 60 * 24));
									if ($days > 15) {
										$show_apply_button = true;
									}
								}

							?>
							<div class="job-card">

                  				@if(isset($role) AND $role == 'seafarer')
									@if(isset($jobs['user_applied_jobs']) && count($jobs['user_applied_jobs']) > 0)
										@if($show_apply_button == false)
											<div class="applied-company" id="applied-for-job-{{$jobs['id']}}">
												<i class="fa fa-check-circle-o applied-icon" aria-hidden="true"></i>
												Already Applied
											</div>
										@else
											<div class="applied-company hide" id="applied-for-job-{{$jobs['id']}}">
												<i class="fa fa-check-circle-o applied-icon" aria-hidden="true"></i>
												Already Applied
											</div>
										@endif
									@else
										<div class="applied-company hide" id="applied-for-job-{{$jobs['id']}}">
											<i class="fa fa-check-circle-o applied-icon" aria-hidden="true"></i>
											Already Applied
										</div>
									@endif
								@endif

					    		<div class="row m-0 flex-height">
									<div class="col-sm-4 col-md-3 p-0 flex-center">
										<div class="row">
											<div class="col-xs-12">
												<div class="job-company-image">
													<img class="profile-pic" src="/{{ isset($jobs['company_registration_details']['user_details']['profile_pic']) ? env('COMPANY_LOGO_PATH')."/".$jobs['company_registration_details']['user_details']['id']."/".$jobs['company_registration_details']['user_details']['profile_pic'] : 'images/user_default_image.png'}}" avatar-holder-src="{{ asset('images/user_default_image.png') }}" alt="" style="width: 135px;">
												</div>
											</div>
											<div class="col-xs-12 job_applicant_pic_name">
												<span class="display_block text-center m-t-5">
													{{ isset($jobs['company_registration_details']['company_name']) ? ucwords($jobs['company_registration_details']['company_name']) : ''}}
												</span>
											</div>
										</div>
									</div>
					    			<div class="col-sm-8 col-md-9 col-xs-12 p-0">
					    				<div class="company-discription">
						    				<div class="other-discription row">
						    					<div class="col-sm-12">
							    					<!-- Sea Trading Services & Ship Management Pvt. Ltd-RPSL-MUM-207 -->
							    					{{ isset($jobs['job_title']) ? ucwords($jobs['job_title']) : ''}}

						                            @if(isset($jobs['company_registration_details']['company_detail']['company_type']))
						                                {{ $jobs['company_registration_details']['company_detail']['company_type'] == $rpsl_company_id ? isset($jobs['company_registration_details']['company_detail']['rpsl_no']) ? "RPSL - ".$jobs['company_registration_details']['company_detail']['rpsl_no'] : '' : '' }}
						                            @endif
						                        </div>
						    				</div>
						    				<div class="other-discription row">
						    					<div class="col-sm-6">
						    						Rank:
						                            @foreach(\CommonHelper::new_rank() as $index => $category)
			                                            @foreach($category as $r_index => $rank)
			                                                {{ !empty($jobs['rank']) ? $jobs['rank'] == $r_index ? $rank : '' : ''}}
			                                            @endforeach
			                                        @endforeach
						    					</div>
						    					<div class="col-sm-6">
							    					Ship Type:
							    						@foreach(\CommonHelper::ship_type() as $s_index => $ship_type)
							                                {{ !empty($jobs['ship_type']) ? $jobs['ship_type'] == $s_index ? $ship_type : '' : ''}}
							                            @endforeach
							                    </div>
						    				</div>
						    				@if(Auth::check())
						    				<?php
					                            $nationality_id = '';

					                            $count_nationality = 1;
					                            if(isset($jobs['job_nationality'] ) && !empty($jobs['job_nationality'] )){
					                                $nationality_id = array_values(collect($jobs['job_nationality'] )->pluck('nationality')->toArray());
					                            }

					                        ?>
						    				<div class="other-discription row">
												<div class="col-sm-6">
													Opening Valid From: {{ isset($jobs['valid_from']) && !empty($jobs['valid_from']) ? $jobs['valid_from'] : '-'}}
												</div>
							                    <div class="col-sm-6">
													Job Posting In:
													@foreach(\CommonHelper::seafarer_job_category() as $index => $category)
														{{ isset($jobs['job_category']) ? $jobs['job_category'] == $index ? $category : '' : ''}}
													@endforeach
							                    </div>
						    				</div>
											<div class="other-discription row">
												<div class="col-sm-6">
													Flag:
													@if(isset($jobs['ship_flag']) && !empty($jobs['ship_flag']))
														@foreach(\CommonHelper::countries() as $index => $flag)
															{{ $jobs['ship_flag'] == $index ? $flag : '' }}
														@endforeach
													@else
														-
													@endif
												</div>
												<div class="col-sm-6">
													Engine Type:
													@if(isset($jobs['engine_type']) && !empty($jobs['engine_type']))
														@foreach( \CommonHelper::engine_type() as $c_index => $engine_type)
															{{ !empty($jobs['engine_type']) ? $jobs['engine_type'] == $c_index ? $engine_type : '' : ''  }}
														@endforeach
													@else
														-
													@endif
												</div>
											</div>
											<div class="other-discription row">
												<div class="col-sm-6">
													GRT:
													{{ !empty($jobs['grt']) ? $jobs['grt'] : ''}}
												</div>
												<div class="col-sm-6">
													BHP:
													{{ !empty($jobs['bhp']) ? $jobs['bhp'] : ''}}
												</div>
											</div>
						    				<div class="other-discription row">
						    					<div class="col-sm-6">
													Min Exp Required: {{ isset($jobs['min_rank_exp']) ? $jobs['min_rank_exp'] : ''}} Years
						    					</div>
												<div class="col-sm-6">
													Sal Offered:
													@if(isset($jobs['wages_currency']) && $jobs['wages_currency'] == 'rupees')
														<i class="fa fa-inr" aria-hidden="true"></i>
													@elseif(isset($jobs['wages_currency']) && $jobs['wages_currency'] == 'dollar')
														<i class="fa fa-usd" aria-hidden="true"></i>
													@else
														-
													@endif

													@if(isset($jobs['wages_offered']) && !empty(isset($jobs['wages_offered'])))
														{{$jobs['wages_offered']}}
													@endif
						    					</div>
						    				</div>
											<div class="other-discription row">
												<div class="col-sm-6">
													Date of Joining: {{ isset($jobs['date_of_joining']) ? $jobs['date_of_joining'] : '-'}}
												</div>
												<div class="col-sm-6">
													Posted On: {{ isset($jobs['created_at']) && !empty($jobs['created_at']) ? date('d-m-Y H:i a', strtotime($jobs['created_at'])) : '-'}}
												</div>
											</div>
						    				<div class="other-discription row">
						    					<div class="col-sm-12">
													Nationality:
													@if(!empty($nationality_id))
														@foreach( \CommonHelper::countries() as $c_index => $country)
															@if(!empty($nationality_id) && in_array($c_index,$nationality_id))
																{{ $country }}
																@if(count($nationality_id) > $count_nationality)
																	,<?php $count_nationality++; ?>
																@endif
															@endif
														@endforeach
													@else
														-
													@endif
						    					</div>
						    				</div>
											<div class="other-discription row">
												<div class="col-sm-12">
													Job Description: {{ isset($jobs['job_description']) ? $jobs['job_description'] : ''}}
												</div>
						    				</div>
						    				@endif

						    				@if(empty($user_id) || (isset($role) AND $role == 'seafarer'))
												@if(isset($jobs['user_applied_jobs']) && count($jobs['user_applied_jobs']) > 0)
													@if($show_apply_button == false)
														<div class="italic-text">
								    						<i>You can apply for the same job after 15 days.</i>
														</div>
													@endif
												@else
													<div class="italic-text hide apply-after-15-{{$jobs['id']}}">
							    						<i>You can apply for the same job after 15 days.</i>
													</div>
												@endif
											@endif
					    				</div>
					    				
					    				@if(empty($user_id) || (isset($role) AND $role == 'seafarer'))
											@if(isset($jobs['user_applied_jobs']) && count($jobs['user_applied_jobs']) > 0)
												@if($show_apply_button == false)
													<button  data-style="zoom-in" class="btn coss-primary-btn apply-btn job-apply-button ladda-button hide" id="job-apply-button-{{$jobs['id']}}" data-form-id="{{$jobs['id']}}" data-company-id="{{$jobs['company_id']}}">
							    					Apply Now
							    					</button>
												@else
													<button  data-style="zoom-in" class="btn coss-primary-btn apply-btn job-apply-button ladda-button" id="job-apply-button-{{$jobs['id']}}" data-form-id="{{$jobs['id']}}" data-company-id="{{$jobs['company_id']}}">
							    					Apply Now
							    					</button>
												@endif
											@else
												<button  data-style="zoom-in" class="btn coss-primary-btn apply-btn job-apply-button ladda-button" id="job-apply-button-{{$jobs['id']}}" data-form-id="{{$jobs['id']}}" data-company-id="{{$jobs['company_id']}}">
						    					Apply Now
						    					</button>
											@endif
										@endif
					    			</div>
					    		</div>
					    	</div>
					    @endforeach
						<div class="row p-t-10 flex-height">
							<div class="col-md-12 col-xs-12">
								<div class="pagi">
					                <div class="search-count p-t-10">
					                    Showing {{$pagination_data['from']}} - {{$pagination_data['to']}} of {{$pagination_data['total']}} Jobs
					                </div>
					                <nav>
					                <ul class="pagination pagination-sm">
					                    {!! $pagination->render() !!}
					                </ul>
					                </nav>
					            </div>
				            </div>
			            </div>
					</div>
					@else
						<div class="row no-results-found">No results found. Try again with different search criteria.</div>
					@endif
					</div>
				</div>
			</div>
		</div>
	</div>
	  <!-- Modal -->
	<div class="modal fade" id="search-listing-filter-modal" role="dialog">
		<div class="modal-dialog">

		  <!-- Modal content-->
			<div class="modal-content">
			    <div class="modal-header">
			      <button type="button" class="close" data-dismiss="modal">&times;</button>
			      <h4 class="modal-title text-center">Filter</h4>
			    </div>
			    <div class="modal-body">
			      	<div class="filter-container-sm">
			      		<form id="seafarer-job-search-filter" action="{{route('site.seafarer.job.search')}}" method="get">
							{{csrf_field()}}
							<?php
								$data1 = $_GET;
								if(isset($data1['param'])){
									$data1 = base64_decode($data1['param']);

						            $data1 = parse_url($data1);
						            parse_str($data1['path'], $data1);
						        }
							?>
							<input type="hidden" name="auto_apply" id="auto_apply" value="{{ isset($data1['auto_apply']) ? $data1['auto_apply'] : ''}}">
							<input type="hidden" name="job_id" id="job_id" value="{{ isset($data1['job_id']) ? $data1['job_id'] : ''}}">
							<input type="hidden" name="user_id" id="user_id" value="{{ isset($data1['user_id']) ? $data1['user_id'] : ''}}">
							<input type="hidden" name="company_id" id="company_id" value="{{ isset($data1['company_id']) ? $data1['company_id'] : ''}}">

							<div class="content-container">
								<div class="form-group">
				                    <label class="input-label" for="company_name">Company Name</label>
				                    <input type="text" class="form-control" id="company_name" name="company_name" value="{{ isset($search_data['company_name']) && !empty($search_data['company_name']) ? $search_data['company_name'] : ''}}" placeholder="Enter Company Name">
				                </div>
								<div class="form-group">
				                    <label class="input-label" for="date_of_joining">Joining Date</label>
				                    <i class="fa fa-calendar calendar-icon" aria-hidden="true"></i>
				                    <input type="text" class="form-control datepicker" id="date_of_joining" name="date_of_joining" value="" placeholder="dd-mm-yyyy">
				                </div>
				                <div class="form-group">
									<label class="input-label">Nationality</label>
									<select id="nationality" name="nationality" class="form-control">
										<option value=>Select nationality</option>
										@foreach(\CommonHelper::countries() as $n_index => $nationality)
			                                <option value="{{$n_index}}" {{ !empty($jobs['nationality1']) ? $jobs['nationality1'] == $n_index ? $nationality1 : '' : ''}}>{{$nationality}}</option>
			                            @endforeach
									</select>
								</div>
								<div class="form-group">
									<label class="input-label">Rank</label>
									<select id="rank" name="rank" class="form-control">
		                                <option value=>Select rank</option>
		                                @foreach(\CommonHelper::new_rank() as $index => $category)
                                            <optgroup label="{{$index}}">
                                            @foreach($category as $r_index => $rank)
                                                <option value="{{$r_index}}" {{ !empty($search_data['rank']) ? $search_data['rank'] == $r_index ? 'selected' : '' : ''}}>{{$rank}} </option>
                                            @endforeach
                                        @endforeach
		                            </select>
								</div>
								<div class="form-group">
				                    <label class="input-label" for="ship">Ship type</label>
					                <select id="ship_type" name="ship_type" class="form-control">
			                            <option value=''>Select ship type</option>
			                            @foreach(\CommonHelper::ship_type() as $index => $ships)
			                                <option value="{{$index}}" {{ !empty($search_data['ship_type']) ? $search_data['ship_type'] == $index ? 'selected' : '' : ''}}>{{$ships}}</option>
			                            @endforeach
			                        </select>
				                </div>
				                <div class="form-group">
				                    <label class="input-label" for="min_rank_exp">Minimum Work Exp</label>
					                <!-- <input type="text" class="form-control" id="min_rank_exp" name="min_rank_exp" value="{{isset($search_data['min_rank_exp']) ? $search_data['min_rank_exp'] : ''}}" placeholder="Enter exp"> -->
					                <select id="min_rank_exp" name="min_rank_exp" class="form-control">
                                        <option value=''>Min Exp</option>
                                        @foreach(\CommonHelper::work_experience() as $w_index => $exp)
                                            <option value="{{$w_index}}" {{ !empty($search_data['min_rank_exp']) ? $search_data['min_rank_exp'] == $w_index ? 'selected' : '' : ''}}>{{$exp}} </option>
                                        @endforeach
                                    </select>
				                </div>
				                <div class="form-group">
				                	<button type="submit" data-style="zoom-in" class="btn coss-primary-btn ladda-button" id="seafarer-job-search-button">Search</button>
				                </div>
							</div>
						</form>
					</div>
			    </div>
			    <div class="modal-footer">
			      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			    </div>
			</div>

		</div>
	</div>
</div>

<div id="job-apply-modal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Job Application</h4>
          </div>
          <div class="modal-body">
            <p>You have successfully applied for this job. You can continue searching for similar jobs using our job search.</p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Ok</button>
          </div>
        </div>

    </div>
</div>

@stop

@section('js_script')
	<script type="text/javascript" src="/js/site/job_search.js"></script>
@stop