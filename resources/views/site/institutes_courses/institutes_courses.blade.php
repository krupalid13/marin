    
@extends('site.index')

@section('content')
    <div class="institutesCourses">
        <div class="institutesCourses_banner">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="search-option-container">
                            <div class="search-card course-search home-search-active" id="course-search">
                                <div class="arrow-up"></div>
                                <div class="img-container text-center">
                                    <img src="{{asset('images/home/course_search.png')}}" alt="">
                                </div>
                                <div class="title">
                                    Courses
                                </div>
                            </div>
                            <div class="search-card institute-search" id="institute-search">
                                <div class="arrow-up"></div>
                                <div class="img-container text-center">
                                    <img src="{{asset('images/home/job_search.png')}}" alt="">
                                </div>
                                <div class="title">
                                    Institutes
                                </div>
                            </div>
                        </div>
                        <div class="banner-search-container">
                            <form id="seafarer-course-search" class="course-search-filter" method="get" action="{{route('site.seafarer.course.search')}}">
                                <div class="row search-container-row">
                                    <div class="col-xs-12 col-sm-4 back-white border-rad-l per-search">
                                        <div class="form-group"> 
                                            <label class="col-xs-4 col-sm-12 col-md-4 p-0 control-label">Course Type
                                            </label> 
                                            <div class="col-xs-8 col-sm-12 col-md-8 p-0"> 
                                                <select name="course_type" class="form-control course_type_filter">
                                                    <option value=''>Select Course Type</option>
                                                    @foreach(\CommonHelper::institute_course_types() as $r_index => $rank)
                                                        <option value="{{$r_index}}" {{ isset($filter['course_type']) ? $filter['course_type'] == $r_index ? 'selected' : '' : ''}}>{{$rank}} </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-3 back-white per-search">
                                        <div class="form-group"> 
                                            <label class="col-xs-4 col-sm-12 col-md-3 control-label">Name
                                            </label> 
                                            <div class="col-xs-8 col-sm-12 col-md-9 p-0"> 
                                                <select id="course_name" name="course_name" class="form-control institute_course_name">
                                                    <option value=''>Select Course Name</option>
                                                    @if(isset($course_name))
                                                        @foreach($course_name as $r_index => $rank)
                                                            <option value="{{$r_index}}" {{ isset($filter['course_name']) ? $filter['course_name'] == $r_index ? 'selected' : '' : ''}}>{{$rank}} </option>
                                                        @endforeach
                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-3 back-white border-rad-r per-search">
                                        <div class="form-group">
                                            <label class="col-xs-4 col-sm-12 col-md-4 p-0 control-label">Start Date</label>
                                            <div class="col-xs-8 col-sm-12 col-md-8 p-0">
                                                <input type="text" name="start_date" class="form-control m-y-datepicker m-t-0" value="{{isset($filter['start_date']) ? !empty($filter['start_date']) ? date('d-m-Y',strtotime('01-'.$filter['start_date'])) : '' : ''}}" placeholder="mm-yyyy">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-2">
                                        <div class="section_cta">
                                            <button type="submit" class="btn coss-primary-btn"><i class="fa fa-search" aria-hidden="true"></i>Search</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <form id="institute-search" class="institute-search-filter hide" method="get" action="{{route('site.seafarer.job.search')}}">
                                <div class="row search-container-row">
                                    <div class="col-xs-12 col-sm-3 back-white border-rad-l per-search">
                                        <div class="form-group"> 
                                            <label class="col-xs-4 col-sm-12 col-md-2 p-0 control-label">Rank
                                            </label> 
                                            <div class="col-xs-8 col-sm-12 col-md-10 p-0"> 
                                                <select id="rank" name="rank" class="form-control">
                                                    <option value=''>Select Rank</option>
                                                    @foreach(\CommonHelper::new_rank() as $index => $category)
                                                        <optgroup label="{{$index}}">
                                                        @foreach($category as $r_index => $rank)
                                                            <option value="{{$r_index}}" {{ !empty($job_data[0]['rank']) ? $job_data[0]['rank'] == $r_index ? 'selected' : '' : ''}}>{{$rank}} </option>
                                                        @endforeach
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-4 back-white per-search">
                                        <div class="form-group"> 
                                            <label class="col-xs-4 col-sm-12 col-md-4 p-0 control-label">Ship Type
                                            </label> 
                                            <div class="col-xs-8 col-sm-12 col-md-8 p-0"> 
                                                <select id="ship_type" name="ship_type" class="form-control">
                                                    <option value=''>Select Ship Type</option>
                                                    @foreach(\CommonHelper::ship_type() as $r_index => $ship_type)
                                                        <option value="{{$r_index}}" {{ !empty($job_data[0]['ship_type']) ? $job_data[0]['ship_type'] == $r_index ? 'selected' : '' : ''}}>{{$ship_type}} </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-3 back-white border-rad-r per-search">
                                        <div class="form-group">
                                            <label class="col-xs-4 col-sm-12 col-md-5 p-0 control-label">Work Exp</label>
                                            <div class="col-xs-8 col-sm-12 col-md-7 p-0">
                                                <!-- <input type="text" class="form-control" id="exampleInputName2" name="min_rank_exp" placeholder="Ex: 2Yrs"> -->
                                                <select id="min_rank_exp" name="min_rank_exp" class="form-control">
                                                    <option value=''>Min Exp</option>
                                                    @foreach(\CommonHelper::work_experience() as $w_index => $exp)
                                                        <option value="{{$w_index}}" {{ !empty($job_data[0]['exp']) ? $job_data[0]['exp'] == $w_index ? 'selected' : '' : ''}}>{{$exp}} </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-2 search-btn">
                                        <div class="section_cta">
                                            <button type="submit" class="btn coss-primary-btn"><i class="fa fa-search" aria-hidden="true"></i>Search</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="courses_section">
            <div class="container">
                <div class="flexRow">
                    @if(!empty($selected_courses_by_type))
                        @foreach($selected_courses_by_type as $index => $courses)
                            <div class="col-xs-12 col-sm-3 flexCol">
                                <a href="{{ route('site.seafarer.course.search',['course_type' => $courses['course_type'],'course_name' => $courses['id']]) }}">
                                    <div class="course_card">
                                        <div class="course_card-title">
                                            {{ strtoupper($courses['course_name']) }}
                                        </div>
                                        <div class="course_card-desc">
                                            {{ !empty($courses['institute_courses'][0]['institute_batches']) ? count($courses['institute_courses'][0]['institute_batches']) : 'NO'}} BATCHES
                                        </div>
                                    </div>
                                </a>
                            </div>  
                        @endforeach
                    @else
                        @foreach($course_type as $index => $courses)
                            <div class="col-xs-12 col-sm-3 flexCol">
                                <a href="{{ route('site.seafarer.homepage.course.search',$index) }}">
                                    <div class="course_card">
                                        <div class="course_card-title">
                                            {{strtoupper($courses)}}
                                        </div>
                                        <div class="course_card-desc">
                                            {{ isset($course_by_type[$index]) ? count($course_by_type[$index]) : '0'}} COURSES
                                        </div>
                                    </div>
                                </a>
                            </div>
                        @endforeach
                    @endif
                </div>        
            </div>
        </div>
    </div>
@stop
@section('js_script')
    <script type="text/javascript" src="{{ URL:: asset('public/assets/js/site/institutes_courses/institutes_search.js')}}"></script>
@stop