
@extends('site.index')

@section('content')
    <div class="institutesCourses">
        <div class="institutesCourses_banner">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="search-option-container">
                            <div class="search-card course-search" id="course-search">
                                <div class="arrow-up"></div>
                                <div class="img-container text-center">
                                    <img src="{{asset('images/home/course_search.png')}}" alt="">
                                </div>
                                <div class="title">
                                    Courses
                                </div>
                            </div>
                            <div class="search-card institute-search home-search-active" id="institute-search">
                                <div class="arrow-up"></div>
                                <div class="img-container text-center">
                                    <img src="{{asset('images/home/job_search.png')}}" alt="">
                                </div>
                                <div class="title">
                                    Institutes
                                </div>
                            </div>
                        </div>
                        <div class="banner-search-container">
                            <form id="institute-search" class="institute-search-filter" method="get" action="{{route('site.seafarer.course.search')}}">
                                <div class="row search-container-row">
                                    <div class="col-xs-12 col-sm-10 back-white border-rad-l per-search">
                                        <div class="form-group"> 
                                            <label class="col-xs-4 col-sm-12 col-md-2 p-0 control-label">Institute
                                            </label> 
                                            <div class="col-xs-8 col-sm-12 col-md-10 p-0">
                                                <select id="institute_name" name="institute_name[]" class="form-control">
                                                    <option value=''>Select Institute</option>
                                                    @foreach($institute_list as $institute)
                                                        <option value="{{$institute['id']}}"> {{ ucwords($institute['institute_name']) }} </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-2 search-btn">
                                        <div class="section_cta">
                                            <button type="submit" class="btn coss-primary-btn"><i class="fa fa-search" aria-hidden="true"></i>Search</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="courses_section">
            <div class="container">
                <div class="flexRow">
                    @if(!empty($institute_list))
                        @foreach($institute_list as $index => $institute)
                            <div class="col-xs-12 col-sm-3 flexCol">
                                <a href="{{route('site.seafarer.course.search',['institute_name' => [$institute['id']]])}}">
                                    <div class="course_card">
                                        <div class="course_card-title">
                                            {{strtoupper($institute['institute_name'])}}
                                        </div>
                                        <div class="course_card-desc">
                                            {{ count($institute['institute_courses']) }} COURSES
                                        </div>
                                    </div>
                                </a>
                            </div>  
                        @endforeach
                    @endif
                </div>        
            </div>
        </div>
    </div>
@stop