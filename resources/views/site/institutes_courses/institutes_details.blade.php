@extends('site.index')

@section('content')
    <?php 
        $start_date = isset($data['start_date']) ? $data['start_date'] : '';
        $expiry_date = date('d-m-Y', strtotime('-7 days', strtotime($start_date))); 
    ?>
    <div class="institutesDetails">
        <div class="container">
            @if(isset($status) && ($status == '0' || $status != ''))
            <div class="row">
                <div class="col-xs-12">
                    <div class="alert alert-success">
                        @if($status == '0')
                            Your booking for this course is awaiting approval from the institute. They will get back to you shortly.
                        @elseif($status == '4')
                            Your booking for this course has been approved by the institute. Your seat will be reserved once your payment has been made.
                        @elseif($status == '1')
                            You have a booking reserved for this course.
                        @elseif($status == '5')
                            Your booking has been denied by the institute.
                        @elseif($status == '6')
                            Please make the pending payment before {{$expiry_date}}. If you fail to make the payment before the {{$expiry_date}} you booking will be cancelled.
                        @endif
                    </div>
                </div>
            </div>
            @endif
            <div class="row">
                <div class="col-xs-12 col-sm-5 col-md-4 col-lg-3 lg_border_right">
                    <div class="details_sidebar">
                        <div class="detailsBlock">
                            <div class="detailsBlock_title">
                                Who and When
                            </div>
                            <div class="detailsBlock_content">
                                <div class="detail_list">
                                    <span class="is_redText is_bold text">Course Status : </span>
                                    <span class="value">
                                        @if($data['batch_type'] == 'on_demand')
                                            On Demand
                                        @else
                                            Confirmed
                                        @endif
                                    </span>
                                </div>
                                <!-- <div class="detail_list">
                                    <span class="is_redText is_bold text">Course Starts on : </span>
                                    <span class="value">Only on Mondays</span>
                                </div> -->
                                <div class="detail_list">
                                    <span class="is_redText is_bold text">Course Duration : </span>
                                    <span class="value"> {{ !empty($data['duration']) ? $data['duration'] : '-'}} Days</span>
                                </div>
                                @if($status != '' || $status == '0')
                                <div class="detail_list">
                                    <span class="is_redText is_bold text">Course Start Date : </span>
                                    <span class="value"> {{ !empty($data['start_date']) ? date('d-m-Y',strtotime($data['start_date'])) : '-'}}</span>
                                </div>
                                @endif
                                
                                @if($status != '' || $status == '0')
                                    <div class="detail_list">
                                        <span class="is_redText is_bold text">Order Status : </span>
                                        <span class="value"> 
                                            @foreach(\CommonHelper::order_status() as $r_index => $status_text)
                                                {{ isset($status) ? $status == $r_index ? $status_text : '' : ''}}
                                            @endforeach
                                        </span>
                                    </div>
                                @endif
                                <!-- <div class="detail_list">
                                    <span class="is_redText is_bold text">Course Timing : </span>
                                    <span class="value">9:00 Hrs to 17:00 Hrs</span>
                                </div> -->
                            </div>
                        </div>
                        <input type="hidden" class="selected_date" value="{{ isset($data['start_date']) ? $data['start_date'] : ''}}">

                        @if($status == '' && $status != '0' && isset($data['batch_type']) && $data['batch_type'] != 'on_demand')
                        <div class="detailsBlock">
                            <div class="detailsBlock_title">
                                Preferred Date
                            </div>
                            <div class="detailsBlock_content">
                                <div class="select_date"></div>
                            </div>
                        </div>

                        <div class="info_block hide">
                            <span class="is_redText is_bold">Leading Preferred Date : </span>
                            <span class="date">23 May 2018</span>
                            <p>(Final Date will be decided by the institute once minimum booking done.)</p>
                        </div>
                        @endif

                        @if(isset($data['batch_type']) && $data['batch_type'] == 'on_demand')
                        <div class="detailsBlock">
                            <div class="detailsBlock_title">
                                Attendance
                            </div>
                            <div class="detailsBlock_content">
                                @if($data['on_demand_batch_type'] == '1')
                                    <div class="attendance_list">
                                        <span class="name">Week 1</span>
                                        <span class="status_label is_green">{{ isset($batches_count['group'][1]) ? count($batches_count['group'][1]) : 'No'}} Bookings</span>
                                    </div>
                                    <div class="attendance_list">
                                        <span class="name">Week 2</span>
                                        <span class="status_label is_green"> {{ isset($batches_count['group'][2]) ? count($batches_count['group'][2]) : 'No'}} Bookings</span>
                                    </div>
                                    <div class="attendance_list">
                                        <span class="name">Week 3</span>
                                        <span class="status_label is_green">{{ isset($batches_count['group'][3]) ? count($batches_count['group'][3]) : 'No'}} Bookings</span>
                                    </div>
                                    <div class="attendance_list">
                                        <span class="name">Week 4</span>
                                        <span class="status_label is_green">{{ isset($batches_count['group'][4]) ? count($batches_count['group'][4]) : 'No'}} Bookings</span>
                                    </div>
                                @elseif($data['on_demand_batch_type'] == '2')
                                    <div class="attendance_list">
                                        <span class="name">Week 1 & 2</span>
                                        <span class="status_label is_green">{{ isset($batches_count['group'][1-2]) ? count($batches_count['group'][1-2]) : 'No'}} Bookings</span>
                                    </div>
                                    <div class="attendance_list">
                                        <span class="name">Week 3 & 4</span>
                                        <span class="status_label is_green">{{ isset($batches_count['group'][3-4]) ? count($batches_count['group'][3-4]) : 'No'}} Bookings</span>
                                    </div>
                                @elseif($data['on_demand_batch_type'] == '3')
                                    <div class="attendance_list">
                                        <span class="name">Monthly</span>
                                        <span class="status_label is_green">{{ isset($batches_count['group']['monthly']) ? count($batches_count['group']['monthly']) : 'No'}} Bookings</span>
                                    </div>
                                @endif
                            </div>
                        </div>
                        @endif
                    </div>
                </div>

                <div class="col-xs-12 col-sm-7 col-md-8 col-lg-9">
                    <div class="details_content">

                        @if($data['batch_type'] == 'on_demand' && $status == '' && $status != '0')
                            <?php
                                $confirmed_batches = [];
                                if(isset($data['child_batches']) && !empty($data['child_batches'])){
                                    $collection = Collect($data['child_batches']);
                                    $confirmed_batches = $collection->pluck('preffered_type')->toArray();
                                    $data['child_batches'] = $data['child_batches']->toArray();
                                }
                            ?>

                            <form id="block-form">

                                <div class="detailsBlock has_border">
                                    <div class="detailsBlock_title">
                                        About Course
                                    </div>
                                    <div class="detailsBlock_content p_lr_15">
                                        <div class="list_wrapper">
                                            <div class="detail_list">
                                                <span class="is_redText is_bold text">Course Type : </span>
                                                <span class="value">
                                                    @foreach(\CommonHelper::institute_course_types() as $index => $course)
                                                        {{ isset($data['course_details']['course_type']) ? $data['course_details']['course_type'] == $index ? $course : '' : ''}}
                                                    @endforeach
                                                </span>
                                            </div>
                                            <div class="detail_list">
                                                <span class="is_redText is_bold text">Course Name : </span>
                                                <span class="value">
                                                    @foreach($all_courses as $index => $category)
                                                        {{ !empty($data['course_details']['course_id']) ? $data['course_details']['course_id'] == $category['id'] ? $category['course_name'] : '' : ''}}
                                                    @endforeach
                                                </span>
                                            </div>
                                        </div>
                                        @if(!empty($data['course_details']['description']))
                                        <div class="detail_desc">
                                            {{ isset($data['course_details']['description']) ? $data['course_details']['description'] : '-'}}
                                        </div>
                                        @endif
                                    </div>
                                </div>

                                @if(isset($data['child_batches']) && !empty($data['child_batches']))
                                <div class="detailsBlock has_border">
                                    <div class="detailsBlock_title">
                                        Confirmed Batches For The Same Course
                                    </div>
                                    @foreach($data['child_batches'] as $child_batches)
                                        <?php 
                                            $user_ids = [];
                                            if(isset($child_batches['orders']) && !empty($child_batches['orders'])){
                                                $collection = collect($child_batches['orders']);

                                                $user_ids = $collection->pluck('user_id')->toArray();
                                            }
                                        ?>
                                            <div class="detailsBlock_content p_lr_15">

                                            <span class="is_redText is_bold text">Start Date ({{ $child_batches['preffered_type'] == 'monthly' ? '' : 'Week'}} {{ucwords($child_batches['preffered_type'])}}) : </span>
                                            <span class="value">
                                                {{ !empty($child_batches['start_date']) ? date('d-m-Y',strtotime($child_batches['start_date'])) : '-'}}
                                            </span>
                                            @if(!in_array($user_id,$user_ids))

                                                <a class="book_cta book_institute_batch pull-right" href="{{route('site.seafarer.course.book',['batch_id' => $child_batches['id'],'location_id' => $child_batches['batch_location'][0]['location_id']])}}">
                                                    <span class="btn coss-primary-book-btn">Book Course</span>
                                                </a>
                                            @else
                                                <span class="is_redText is_bold text pull-right">
                                                    You have already blocked seat for this course.
                                                </span>
                                            @endif
                                            <br>
                                            </div>
                                    @endforeach
                                </div>
                                @endif

                                <div class="detailsBlock has_border">
                                    <div class="detailsBlock_title">
                                        Select Preferable Date
                                    </div>
                                    <div class="detailsBlock_content p_lr_15">
                                        <div class="list_wrapper">
                                            <div class="detail_list">
                                                <input type="hidden" class="on_demand_batch_type" value="{{isset($data['on_demand_batch_type']) ? $data['on_demand_batch_type'] : ''}}">
                                                <span class="is_redText is_bold text">Batch Type : </span>
                                                <span class="value">
                                                    @if(isset($data['on_demand_batch_type']) && !empty($data['on_demand_batch_type']))
                                                        {{ isset($data['on_demand_batch_type']) ? $data['on_demand_batch_type'] == '1' ? 'Weekly Course' : '' : ''}}
                                                        {{ isset($data['on_demand_batch_type']) ? $data['on_demand_batch_type'] == '2' ? 'Bi-Monthly Course' : '' : ''}}
                                                        {{ isset($data['on_demand_batch_type']) ? $data['on_demand_batch_type'] == '3' ? 'Monthly Course' : '' : ''}}
                                                    @else
                                                        -
                                                    @endif
                                                </span>
                                            </div>
                                            <div class="detail_list">
                                                <!-- <span class="is_redText is_bold text">Start Date : </span>
                                                <span class="value">
                                                    {{ !empty($data['start_date']) ? date('d-m-Y',strtotime($data['start_date'])) : '-'}}
                                                </span> -->
                                            </div>
                                        </div>
                                        <div class="detail_desc">
                                            @if(isset($data['on_demand_batch_type']) && !empty($data['on_demand_batch_type']))
                                                <?php 
                                                    $start_date = $data['start_date'];

                                                    $split_date = explode('-', $start_date);

                                                    $year = $split_date[0];
                                                    $month = $split_date[1];

                                                    $start_date = date("$year-$month-1");
                                                    if(date('Y-m-d') > $start_date){
                                                        $start_date = date('Y-m-d');
                                                    }

                                                    $end_date = date("Y-m-t", strtotime($start_date));

                                                    $week=1;
                                                    $date = Carbon\Carbon::now();
                                                    $date->setISODate($year,$week);
                                                    //echo $date->startOfWeek();
                                                    //echo $date->endOfWeek();
                                                    $start_date = Carbon\Carbon::createFromFormat('Y-m-d', $start_date);
                                                    $end_date = Carbon\Carbon::createFromFormat('Y-m-d', $end_date);

                                                    //dd($start_date->addWeek(1),$end_date);
                                                ?>
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        @if($data['on_demand_batch_type'] == '1')
                                                            <span class="is_redText is_bold text">Select Week : </span>
                                                            <span class="value">
                                                                <span class="p-l-5">
                                                                    @if(!in_array('1',$confirmed_batches))
                                                                    <label class="radio-inline week-1">
                                                                        <input type="radio" class="week_change" name="week_change" data-week='1' value="1"> Week 1 
                                                                    </label>
                                                                    @endif
                                                                    @if(!in_array('2',$confirmed_batches))
                                                                    <label class="radio-inline week-2">
                                                                        <input type="radio" class="week_change" name="week_change" data-week='2' value="2"> Week 2
                                                                    </label>
                                                                    @endif
                                                                    @if(!in_array('3',$confirmed_batches))
                                                                    <label class="radio-inline week-3">
                                                                        <input type="radio" class="week_change" name="week_change" data-week='3' value="3"> Week 3
                                                                    </label>
                                                                    @endif
                                                                    @if(!in_array('4',$confirmed_batches))
                                                                    <label class="radio-inline week-4">
                                                                        <input type="radio" class="week_change" name="week_change" data-week='4' value="4"> Week 4
                                                                    </label>
                                                                    @endif
                                                                </span>
                                                            </span>
                                                        @elseif($data['on_demand_batch_type'] == '2')
                                                            <span class="is_redText is_bold text">Select Week : </span>
                                                            <span class="value">
                                                                <span class="p-l-5">
                                                                    @if(!in_array('1-2',$confirmed_batches))
                                                                    <label class="radio-inline week-1-2">
                                                                        <input type="radio" class="week_change" name="week_change" data-week='1-2' value="1-2"> Week 1 & 2
                                                                    </label>
                                                                    @endif
                                                                    @if(!in_array('3-4',$confirmed_batches))
                                                                    <label class="radio-inline week-3-4">
                                                                        <input type="radio" class="week_change" name="week_change" data-week='3-4' value="3-4"> Week 3 & 4
                                                                    </label>
                                                                    @endif
                                                                </span>
                                                            </span>
                                                        @elseif($data['on_demand_batch_type'] == '3')
                                                            @if(!in_array('Monthly',$confirmed_batches))
                                                            <span class="is_redText is_bold text">Select Week : </span>
                                                            <span class="value">
                                                                <span class="p-l-5">
                                                                    <label class="radio-inline">
                                                                        <input type="radio" class="week_change" name="week_change" data-week='monthly' value="monthly"> Monthly
                                                                    </label>
                                                                </span>
                                                            </span>
                                                            @endif
                                                        @endif
                                                        <br>
                                                        <span class="is_redText is_bold text">
                                                            <label for="week_change" class="week_change_error hide">Please select week.</label>
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-12 p-t-10">
                                                        <span class="is_redText is_bold text">Select Preffered Date : </span>
                                                        <span class="value">
                                                            <span class="p-l-5">
                                                                <!-- <input type="text" class="form-control preffered-datepicker" id="preffered-datepicker"> -->
                                                                <div id="preffered-datepicker" class="preffered-datepicker" style="width:100%;height:200px;"></div>
                                                            </span>
                                                            <span class="p-l-5 d-flex">
                                                                <input type="hidden" id="dbdatepicker" class="dbdatepicker" name="dbdatepicker" data-rule-dbdatepickervalue="true">
                                                            </span>
                                                        </span>
                                                    </div>
                                                </div>
                                                
                                            @endif

                                        </div>
                                    </div>
                                </div>
                            </form>
                        @endif

                        <div class="detailsBlock has_border">
                            <div class="detailsBlock_title">
                                About Institute
                            </div>
                            <div class="detailsBlock_content p_lr_15">
                                <div class="detail_list">
                                    <span class="is_redText is_bold text">Institute Name : </span>
                                    <span class="value">
                                        {{ isset($data['course_details']['institute_registration_detail']['institute_name']) ? $data['course_details']['institute_registration_detail']['institute_name'] : '-'}}
                                    </span>
                                </div>
                                <?php $location_found = 0;?>
                                <div class="detail_list">
                                    <span class="is_redText is_bold text">Institute Address : </span>
                                    <span class="value">
                                        @if(isset($data['batch_location']) && !empty(isset($data['batch_location'])))
                                            @foreach($data['batch_location'] as $index => $location)
                                                @if($location['location']['headbranch'] == '1')
                                                    {{$location['location']['address']}},
                                                    {{$location['location']['city']['name']}} -
                                                    {{$location['location']['pincode_text']}}.
                                                    <?php $location_found = 1;?>
                                                @endif
                                            @endforeach
                                        @endif
                                        
                                        @if($location_found == '0')
                                            @if(isset($data['batch_location'][0]) && !empty(isset($data['batch_location'][0])))
                                                {{$data['batch_location'][0]['location']['address']}},
                                                {{$data['batch_location'][0]['location']['city']['name']}} -
                                                {{$data['batch_location'][0]['location']['pincode_text']}}.
                                            @endif
                                        @endif
                                    </span>
                                </div>
                                <div class="detail_list">
                                    <span class="is_redText is_bold text">Institute Contact No : </span>
                                    <span class="value">
                                        {{ isset($data['course_details']['institute_details']['institute_contact_number']) ? $data['course_details']['institute_details']['institute_contact_number'] : '-'}}
                                    </span>
                                </div>
                                <div class="detail_list">
                                    <span class="is_redText is_bold text">Institute Email : </span>
                                    <span class="value">
                                        {{ isset($data['course_details']['institute_details']['institute_email']) ? $data['course_details']['institute_details']['institute_email'] : '-'}}
                                    </span>
                                </div>
                                <div class="detail_list">
                                    <span class="is_redText is_bold text">Course Conducted at : </span>
                                    <span class="value">
                                        @if(isset($data['batch_location']) && !empty(isset($data['batch_location'])))
                                            @foreach($data['batch_location'] as $index => $location)
                                                @if($location['location']['id'] == $selected_location_id)
                                                    {{$location['location']['address']}},
                                                    {{$location['location']['city']['name']}} -
                                                    {{$location['location']['pincode_text']}}.
                                                    <?php $location_found = 1;?>
                                                @endif
                                            @endforeach
                                        @endif
                                    </span>
                                </div>
                            </div>
                        </div>


                        <?php 
                            $cgst = env('CGST_PERCENT');
                            $sgst = env('SGST_PERCENT');

                            $cost = $data['cost'];
                            
                            if(isset($discount) && !empty($discount)){
                                $discount_amt = round(($cost*$discount)/100);
                                $cost = $cost-$discount_amt;
                            }

                            $cgst_cost = round(($cost*$cgst)/100);
                            $sgst_cost = round(($cost*$sgst)/100);
                            $inr_symbol = "<i class='fa fa-inr'></i>";

                            $total = $cost+$cgst_cost+$sgst_cost;

                            if(isset($data['payment_type']) && $data['payment_type'] == 'split'){
                                $initial_per = $data['initial_per'];
                                $pending_per = $data['pending_per'];

                                $initial_amt = round(($total*$data['initial_per'])/100);
                                $pending_amt = round(($total*$data['pending_per'])/100);
                            }
                        ?>

                        <div class="detailsBlock has_border">
                            <div class="detailsBlock_title">
                                Required From You
                            </div>
                            <div class="detailsBlock_content p_lr_15">
                                <div class="detail_list">
                                    <span class="is_redText is_bold text">Course Eligibility : </span>
                                    <span class="value">
                                        {{ isset($data['course_details']['eligibility']) ? $data['course_details']['eligibility'] : '-'}}
                                    </span>
                                </div>
                                <div class="detail_list">
                                    <span class="is_redText is_bold text">Required Documents : </span>
                                    <span class="value">
                                        {{ isset($data['course_details']['documents_req']) ? $data['course_details']['documents_req'] : '-'}}
                                    </span>
                                </div>
                                <div class="detail_list">
                                    <span class="is_redText is_bold text">Course Fees : </span>
                                    <span class="value">
                                        <span class="fa fa-inr"></span>
                                        {{ isset($data['cost']) ? $data['cost'] : '-'}}
                                    </span>
                                </div>
                                @if(isset($discount) && !empty($discount))
                                <div class="detail_list">
                                    <span class="is_redText is_bold text">Course Discount : </span>
                                    <span class="value">
                                        {{ isset($discount) ? $discount : '-'}}%
                                    </span>
                                </div>
                                <input type="hidden" name="discount" class="discount" value="{{ isset($discount) ? $discount : '-'}}">
                                <?php
                                    $discount_applied = round(($data['cost']*$discount)/100);
                                    $discounted_amount = $data['cost']-$discount_applied;
                                ?>
                                    @if(isset($discounted_amount) && !empty($discounted_amount))
                                    <div class="detail_list">
                                        <span class="is_redText is_bold text">Discounted Course Fees : </span>
                                        <span class="value">
                                            <span class="fa fa-inr"></span>
                                            {{ isset($discounted_amount) ? $discounted_amount : '-'}}
                                        </span>
                                    </div>
                                    @endif
                                @endif
                                <div class="detail_list">
                                    <span class="is_redText is_bold text">CGST : </span>
                                    <span class="value">
                                        <span class="fa fa-inr"></span>
                                        {{ isset($cgst_cost) ? $cgst_cost : '-'}}
                                    </span>
                                </div> 
                                <div class="detail_list">
                                    <span class="is_redText is_bold text">SGST : </span>
                                    <span class="value">
                                        <span class="fa fa-inr"></span>
                                        {{ isset($sgst_cost) ? $sgst_cost : '-'}}
                                    </span>
                                </div>
                                <div class="detail_list single_payment_details">
                                    <span class="is_redText is_bold text">Amount to be paid : </span>
                                    <span class="value">
                                        <span class="fa fa-inr"></span>
                                        {{ isset($total) ? $total : '-'}}
                                    </span>
                                </div>
                            </div>
                        </div>
                       
                        @if($make_payment == '1' && !empty($status) && $status == '4')
                        <div class="detailsBlock has_border">
                            <div class="detailsBlock_title">
                                Booking Payment
                            </div>
                            <div class="detailsBlock_content p_lr_15">
                                <div class="detail_list">
                                    <span class="is_redText is_bold text">Payment By : </span>
                                    <span class="value">
                                        <span class="p-l-5">
                                            <label class="radio-inline">
                                                <input type="radio" class="payment_by" name="payment_by" value="single" checked> Single Payment 
                                            </label>
                                            @if($data['payment_type'] == 'split')
                                                @if($allow_split_payment)
                                                <label class="radio-inline">
                                                    <input type="radio" class="payment_by" name="payment_by" value="split"> Split Payment
                                                </label>
                                                @endif
                                            @endif
                                        </span>
                                    </span>
                                </div>
                                <div class="detail_list">
                                    <span class="is_redText is_bold text">Course Fees : </span>
                                    <span class="value">
                                        <span class="fa fa-inr"></span>
                                        {{ isset($data['cost']) ? $data['cost'] : '-'}}
                                    </span>
                                </div>
                                @if(isset($discounted_amount) && !empty($discounted_amount))
                                <div class="detail_list">
                                    <span class="is_redText is_bold text">Discounted Course Fees : </span>
                                    <span class="value">
                                        <span class="fa fa-inr"></span>
                                        {{ isset($discounted_amount) ? $discounted_amount : '-'}}
                                    </span>
                                </div>
                                @endif
                                <div class="detail_list">
                                    <span class="is_redText is_bold text">CGST : </span>
                                    <span class="value">
                                        <span class="fa fa-inr"></span>
                                        {{ isset($cgst_cost) ? $cgst_cost : '-'}}
                                    </span>
                                </div> 
                                <div class="detail_list">
                                    <span class="is_redText is_bold text">SGST : </span>
                                    <span class="value">
                                        <span class="fa fa-inr"></span>
                                        {{ isset($sgst_cost) ? $sgst_cost : '-'}}
                                    </span>
                                </div>
                                <div class="detail_list single_payment_details">
                                    <span class="is_redText is_bold text">Amount to be paid now : </span>
                                    <span class="value">
                                        <span class="fa fa-inr"></span>
                                        {{ isset($total) ? $total : '-'}}
                                    </span>
                                </div>
                                @if($data['payment_type'] == 'split')
                                <div class="detail_list split_payment_details hide">
                                    <div class="detail_list">
                                        <span class="is_redText is_bold text">Initial Payment : </span>
                                        <span class="value">
                                            <span class="fa fa-inr"></span>
                                            {{ isset($initial_amt) ? $initial_amt : '-'}}
                                        </span>
                                    </div>
                                    <div class="detail_list">
                                        <span class="is_redText is_bold text">Pending Payment : </span>
                                        <span class="value">
                                            <span class="fa fa-inr"></span>
                                            {{ isset($pending_amt) ? $pending_amt : '-'}}
                                        </span>
                                    </div>
                                    <div class="detail_list">
                                        <span class="is_redText is_bold text">Amount to be paid now : </span>
                                        <span class="value">
                                            <span class="fa fa-inr"></span>
                                            {{ isset($initial_amt) ? $initial_amt : '-'}}
                                        </span>
                                    </div>
                                </div>
                                @endif
                            </div>
                        </div>
                        @endif

                        <?php
                            $batch_id = isset($data['batch_location'][0]['batch_id']) ? $data['batch_location'][0]['batch_id'] : '';
                            $location_id = isset($data['batch_location'][0]['location_id']) ? $data['batch_location'][0]['location_id'] : '';
                            $order_id = isset($order_id) && !empty($order_id) ? $order_id : '';
                        ?>
                        <div class="detailsBlock no-border">
                        @if($role == 'seafarer')
                            @if($make_payment == '1')
                                
                                @if(!empty($status) && $status == '4')
                                <span>
                                    <button type="button" data-style="zoom-in" class="btn coss-primary-btn ladda-button pull-right make-payment" data-batch="{{$batch_id}}" data-order-id="{{$order_id}}" data-location="{{$location_id}}">Make Payment</button>
                                </span>
                                <span class="">
                                    <button type="button" data-style="zoom-in" class="m-r-10 btn coss-primary-btn ladda-button pull-right cancel-payment" data-batch="{{$batch_id}}" data-order-id="{{$order_id}}" data-location="{{$location_id}}">Cancel Booking</button>
                                </span>
                                @endif
                            @else
                                @if($prev_batch_count == 0)
                                    <button type="button" data-style="zoom-in" class="btn coss-primary-btn ladda-button pull-right block-seat" data-batch="{{$batch_id}}" data-location="{{$location_id}}">Block Seat</button>
                                @endif
                            @endif

                            @if(isset($prev_batch_details[0]) && !empty($status) && $status == '6')
                            <div class="detailsBlock has_border">
                                <div class="detailsBlock_title">
                                    Booking Payment
                                </div>
                                <div class="detailsBlock_content p_lr_15">
                                    <div class="detail_list">
                                        <span class="is_redText is_bold text">Payment Done By : </span>
                                        <span class="value">
                                            Split Payment
                                        </span>
                                    </div>
                                    <div class="detail_list">
                                        <span class="is_redText is_bold text">Amount Paid : </span>
                                        <span class="value">
                                            <span class="fa fa-inr"></span>
                                            {{ isset($prev_batch_details[0]['order_payments'][0]['amount']) ? $prev_batch_details[0]['order_payments'][0]['amount'] : '-'}}
                                        </span>
                                    </div>
                                    <div class="detail_list">
                                        <span class="is_redText is_bold text">Remaining Course Fees : </span>
                                        <span class="value">
                                            <span class="fa fa-inr"></span>
                                            {{ isset($pending_amt) ? $pending_amt : '-'}}
                                        </span>
                                    </div>
                                    <div class="detail_list single_payment_details">
                                        <span class="is_redText is_bold text">Amount to be paid : </span>
                                        <span class="value">
                                            <span class="fa fa-inr"></span>
                                            {{ isset($pending_amt) ? $pending_amt : '-'}}
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="detailsBlock no-border">
                                <span>
                                    <button type="button" data-style="zoom-in" class="btn coss-primary-btn ladda-button pull-right make-payment" data-pay="split" data-batch="{{$batch_id}}" data-order-id="{{$order_id}}" data-location="{{$location_id}}">Make Pending Payment</button>
                                </span>
                            </div>
                            @endif
                        @endif
                    </div>
                    <form id="payumoney-form">
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div id="login-popup-reg-modal" class="modal fade" role="dialog">
        <div id="login-popup" class="white-popup signin-sigup-default" role="dialog" style="margin-top: 5%;">
            <button type="button" class="close" data-dismiss="modal" style="margin: 10px;">&times;</button>
            <div class="signin-signup-heading">
                <div class="signin-book new-sigin-link active"><a class="open-popup-link">Login</a></div>
                <div class="signin-book pre-register-link"><a class="open-popup-link">Register</a></div>
            </div>

            <!-- login form -->
            <form id="loginFormModal-preRegister" method="post" action="{{route('site.login')}}">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="new-login-content">
                    <div class="row">
                        
                        <div class="small-12 medium-12 columns">
                            <div class="new-login-form-container">
                                <div id="login-form" @if(session('login_error')) style="display: block;" @endif style="display: block;">
                                    <div class="row margin-vert-5">
                                        <div class="col-sm-3 padding-top-5">
                                          <label class="mar-b-10"><span class="label-icon"><i class="fa fa-envelope" aria-hidden="true"></i></span><span class="label-text mandatory">Email</span>
                                        </label>
                                        </div>
                                        <div class="col-sm-9">
                                            <input class="form-control" type="email" name="email" placeholder="Enter email" value="{{ old('email') }}"/>
                                        </div>
                                    </div>
                                    <div class="row margin-vert-5">
                                        <div class="col-sm-3 padding-top-5">
                                          <label class="mar-b-10"><span class="label-icon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span><span class="label-text mandatory">Password</span>
                                          </label>
                                        </div>
                                        <div class="col-sm-9">
                                            <input class="form-control" type="password" name="password" placeholder="Enter password"/>
                                            <label id="login_error" class="error"></label>
                                        </div>
                                    </div>
                                    <div class="row margin-vert-5">
                                        <div class="col-sm-6">
                                            <input  name="remember" type="checkbox">
                                            <label for="remember">Remember Me</label>  
                                        </div>
                                        <div class="col-sm-6">
                                            <a href="{{route('site.reset.password.view')}}" class="pull-right">Forget Password?</a> 
                                        </div>
                                    </div>
                                    <div class="row margin-vert-5">
                                        <div class="col-sm-12">
                                            <button type="button" data-style='zoom-in' class="btn-1 light-blue new-sign-up-btn fullwidth ladda-button" id="sign-in-btn-pre-registration">Sign In</button>
                                        </div>
                                    </div>
                                    <!-- <div class="row margin-vert-5">
                                        <div class="col-sm-12">
                                            <div class="forget-pass-link">
                                                <a data-mfp-src="#forgot-password-popup" class="open-popup-link">Forgot Password</a>
                                            </div>
                                        </div>
                                    </div> -->
                                    <div class="row">
                                        <div class="small-12 columns info" align="center" style="display:none">
                                            Logging in. Please wait...
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="ajaxError small-12 columns" align="center"
                                             style="display:{{ session('login_error') ? 'block' : 'none' }}">
                                            <div class="" style="color:#ff0000;">
                                                <strong>Login failed!!!</strong>
                                            </div>
                                            <div class="reason">
                                                {{ session('login_error') ? session('login_error') : '' }}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>

            <!-- new pre registration -->
            <form id="preRegistrationFormModal" class="hide" method="post" action="{{route('site.seafarer.pre.register')}}">
                <input type="hidden" name="user_id" value="{{isset($user_data[0]['id']) ? $user_data[0]['id'] : ''}}">
                <div class="new-login-content">
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label class="input-label" for="email">Email Address<span class="symbol required"></span></label>
                            </div>
                        </div>
                        <div class="col-sm-9">
                            <div class="form-group">
                                <input type="email" class="form-control" id="user_email" name="email" placeholder="Type your email address">
                                <span class="hide" id="email-error" style="color: red"></span>
                            </div>  
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label class="input-label" for="password">Password<span class="symbol required"></span></label>
                            </div>
                        </div>
                        <div class="col-sm-9">
                            <div class="form-group">
                                <input type="password" class="form-control" id="password" name="password" placeholder="Type your password">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label class="input-label" for="cpassword">Confirm Password<span class="symbol required"></span></label>
                            </div>
                        </div>
                        <div class="col-sm-9">
                            <div class="form-group">
                                <input type="password" class="form-control" name="cpassword" placeholder="Type password again">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label class="input-label" for="firstname">Full Name<span class="symbol required"></span></label>
                            </div>
                        </div>
                        <div class="col-sm-9">
                            <div class="form-group">    
                                <input type="text" class="form-control" name="firstname" placeholder="Type your full name">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label class="input-label" for="dob">Date Of Birth<span class="symbol required"></span></label>
                            </div>
                        </div>
                        <div class="col-sm-9">
                            <div class="form-group">
                                <input type="text" class="form-control birth-date-datepicker" name="dob" placeholder="dd-mm-yyyy">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label class="input-label" id="mobile" for="mobile">Mobile Number<span class="symbol required"></span></label>
                            </div>
                        </div>
                        <div class="col-sm-9">
                            <div class="form-group">
                                <input type="text" class="form-control" id="mobile" name="mobile" placeholder="Type your mobile number">
                                <span class="hide" id="mobile-error" style="color: red"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label class="input-label" for="current_rank">Current Rank<span class="symbol required"></span></label>
                            </div>
                        </div>
                        <div class="col-sm-9">
                            <div class="form-group">
                                <select id="current_rank" name="current_rank" class="form-control">
                                    <option value="">Select Your Rank</option>
                                    @foreach(\CommonHelper::new_rank() as $index => $category)
                                        <optgroup label="{{$index}}">
                                        @foreach($category as $r_index => $rank)
                                            <option value="{{$r_index}}">{{$rank}} </option>
                                        @endforeach
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label class="input-label" for="passport">Passport Number<span class="symbol required"></span></label>
                            </div>
                        </div>
                        <div class="col-sm-9">
                            <div class="form-group">
                                <input type="text" class="form-control" id="passport" name="passport" placeholder="Type your passport number">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label class="input-label" for="passport">Indos Number<span class="symbol required"></span></label>
                            </div>
                        </div>
                        <div class="col-sm-9">
                            <div class="form-group">
                                <input type="text" class="form-control" id="indosno" name="indosno" placeholder="Type the indos number">
                            </div>
                        </div>
                    </div>
                    <div class="row margin-vert-5">
                        <div class="col-sm-12">
                            <button type="button" data-style='zoom-in' class="btn-1 new-sign-up-btn fullwidth ladda-button" id="pre-register-user">Register</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div id="display-advertise-modal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title text-center">Booking discount is sponsored by {{isset($advertisment_details['company_registration']['company_name']) ? ucwords($advertisment_details['company_registration']['company_name']) : ''}}</h4>
                </div>
                <div class="modal-body">
                    <div class="img-display">
                        <?php
                            $image_path = '';
                            if(isset($advertisment_details['company_registration']) && !empty($advertisment_details['company_registration'])){
                                $image_path = asset(env('ADVERTISE_PATH').$advertisment_details['company_registration']['advertisment_details'][0]['company_id'].'/'.$advertisment_details['company_registration']['advertisment_details'][0]['img_path']);
                            }
                        ?>
                        <img class="display-advertise" src="{{$image_path}}" style="max-height: 250px;width: 100%;box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12);">
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="adv_timer text-center">
                        This advertise will close in <span class='ad_timer'></span> seconds
                    </div>
                </div>
            </div>

        </div>
    </div>
@stop

@section('js_script')
    <script type="text/javascript" src="/js/site/institutes_details.js"></script>
@stop