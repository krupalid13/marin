
@extends('site.index')
<style type="text/css">
    #top-user-paginator{
        margin: 1px 0 !important;
    }
    table.table {
    font-size: 14px;
    }
</style>
@section('content')
    <div class="institutesSearchResult">
        <div class="container">
            <button class="btn search-listing-sm-filter-btn" type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#search-listing-filter-modal"><i class="fa fa-filter" aria-hidden="true"></i></button>
            <div class="row">
                <div class="col-xs-12 col-lg-3">
                    <div class="filter_grid filter_grid_section">
                        <div class="filter_grid-title is_redText is_bold">
                            Filter Results
                        </div>

                        <div class="filter_grid-block">
                            <div class="form_section">
                                <div class="form_section-title">Institute Name</div>
    
                                <div class="form_section-block">
                                    <form class="instituteFilterForm">
                                        @foreach($institute_list as $institute)
                                            <?php
                                                $checked = '';
                                                if(isset($filter['institute_name']) && !empty($filter['institute_name'])){
                                                    if(in_array($institute['id'],$filter['institute_name'])){
                                                        $checked = 'checked';
                                                    }
                                                }
                                            ?>
                                            <div class="form_group">
                                                <input type="checkbox" class="filled-in inst_chkbox" data-name="{{ ucwords($institute['institute_name']) }}" name="inst_chkbox-{{$institute['id']}}" id="inst_chkbox-{{$institute['id']}}" data-id="inst_chkbox-{{$institute['id']}}" value="{{$institute['id']}}" {{$checked}}>
                                                <label for="inst_chkbox-{{$institute['id']}}">
                                                    {{ isset($institute['institute_name']) && !empty($institute['institute_name']) ? ucwords($institute['institute_alias']) : '' }}
                                                </label>
                                            </div>
                                        @endforeach
                                    </form>
                                </div>
                            </div>

                            <div class="form_section">
                                <div class="form_section-title">Location</div>
    
                                <div class="form_section-block">
                                    <form class="instituteFilterForm">
                                        @foreach($locations as $location)
                                            <div class="form_group">
                                                <input type="checkbox" class="filled-in loc_chkbox" data-name="{{ ucwords($location['city']['name']) }}" name="loc_chkbox-{{$location['city_id']}}" id="loc_chkbox-{{$location['city_id']}}" data-id="loc_chkbox-{{$location['city_id']}}" value="{{$location['city_id']}}">
                                                <label for="loc_chkbox-{{$location['city_id']}}">
                                                    {{ ucwords($location['city']['name']) }}
                                                </label>
                                            </div>
                                        @endforeach
                                    </form>
                                </div>
                            </div>

                            <div class="form_section">
                                <div class="form_section-title">Institute Start Date</div>
    
                                <div class="calender">
                                    <div class="instituteFilterForm">
                                        <div id="datepicker" class="coss_datepicker"></div>
                                    </div>
                                </div>
                            </div>

                            <div class="form_section">
                                <div class="form_section-title">Fee</div>
                                <div class="form_section-block">
                                    <div class="switch_block">
                                        <span class="text">All</span>
                                        <label class="switch">
                                            <input class="switch_checkbox" type="checkbox"/>
                                            <span></span>
                                        </label>
                                        <span class="text">Discount</span>
                                    </div>
                                </div>
                            </div>

                            <div class="form_section">
                                   &nbsp;
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-lg-9">
                    <div class="section_title">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="from-group">
                                    <label class="is_redText is_bold">Course Type : </label>
                                    <select name="course_type" class="form-control course_type_filter">
                                        <option value=>Select Course Type</option>
                                        @foreach(\CommonHelper::institute_course_types() as $r_index => $rank)
                                            <option value="{{$r_index}}" {{ isset($filter['course_type']) ? $filter['course_type'] == $r_index ? 'selected' : '' : ''}}>{{$rank}} </option>
                                        @endforeach
                                    </select>
                                </div>
                                <!-- <div class="section_title-item">
                                    <span ></span>
                                    <span>
                                        
                                    </span>
                                </div> -->
                            </div>
                            <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="is_redText is_bold">Course Name : </label>
                                        <select id="course_name" name="course_name" class="form-control institute_course_name">
                                            <option value=''>Select Course Name</option>
                                            @if(isset($course_name) && !empty($course_name))
                                                @foreach($course_name as $r_index => $val)
                                                    <option value="{{$val['id']}}" {{ isset($filter['course_name']) ? $filter['course_name'] == $val['id'] ? 'selected' : '' : ''}}>{{$val['course_name']}} </option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                <!-- <div class="section_title-item">
                                    <span class="is_redText is_bold"></span>
                                    <span>
                                        
                                    </span>
                                </div> -->
                            </div>
                        </div>
                    </div>

                    <div class="searchResult_tags">
                        <span class="is_redText is_bold">Filter : </span>
                    </div>
                    <div class="table-responsive">
                        <div class="loader_overlay hide">
                            <div class="loader_ele loader"></div>
                        </div>
                        <table class="table">
                            <thead class="instituteGrid-heading">
                                <tr>
                                    <th style="width: 10%">Institute</th>
                                    <th style="width: 25%">Course</th>
                                    <th style="width: 10%">Approve</th>
                                    <th style="width: 15%">Venue</th>
                                    <!-- <th>Batch Type</th> -->
                                    <th style="width: 10%">Duration</th>
                                    <th style="width: 10%">On</th>
                                    <th style="width: 10%">Fee</th>
                                    
                                </tr>
                            </thead>
                            <tbody class="institute_list">
                                @if(isset($data) && !empty($data))
                                    @foreach($data as $index => $course)
                                        <tr class="flexRow">
                                            <td class="flexCol">
                                                <div class="content name">
                                                    <!-- <span class="title hidden-sm hidden-md hidden-lg">Institute Name : </span> -->
                                                    <span class="value">
                                                        {{ isset($course['course_details']['institute_registration_details']['institute_name']) ? ucfirst($course['course_details']['institute_registration_details']['institute_alias']) : '-'}}
                                                    </span>
                                                </div>
                                            </td>
                                            <td class="flexCol">
                                                <div class="content name">
                                                    <!-- <span class="title hidden-sm hidden-md hidden-lg">Course Details : </span> -->
                                                    <span class="value">
                                                       <!--  @foreach(\CommonHelper::institute_course_types() as $r_index => $course_name)
                                                            {{ isset($course['course_details']['course_type']) ? $course['course_details']['course_type'] == $r_index ? $course_name.'-' : '' : ''}}
                                                        @endforeach -->

                                                        @foreach($all_courses as $index => $category)
                                                            {{ !empty($course['course_details']['course_id']) ? $course['course_details']['course_id'] == $category['id'] ? $category['course_name'] : '' : ''}}
                                                        @endforeach
                                                    </span>
                                                </div>
                                            </td>
                                            <td class="flexCol">
                                                <div class="content name">
                                                   
                                                    <span class="value">
                                                        <?php 
                                                         
                                                         $approved_by = '';
                                                         if($course['course_details']['approved_by'] == '1') {
                                                            $approved_by = 'DG India';
                                                         }elseif ($course['course_details']['approved_by'] == '2') {
                                                             $approved_by = 'OPITO';
                                                         }else {
                                                              $approved_by = 'Value Added';
                                                         }
                                                        ?>
                                                        {{$approved_by}}
                                                    </span>
                                                </div>
                                            </td>
                                            <td class="flexCol">
                                                <div class="content loc">
                                                    <!-- <span class="title hidden-sm hidden-md hidden-lg">Location : </span> -->
                                                    <span class="value">
                                                        @foreach($course['batch_location'] as $index => $location)
                                                            {{ !empty( $location['location']['city']['name']) ?  $location['location']['city']['name'] : ''}}@if(count($course['batch_location']) > $index+1),@endif
                                                        @endforeach
                                                    </span>
                                                </div>
                                            </td>
                                            <!-- <td class="flexCol">
                                                <div class="content duration">
                                                    <span class="value">
                                                        {{ !empty($course['batch_type']) ? $course['batch_type'] == 'confirmed' ? 'Confirmed' : 'On Demand' : '-'}}
                                                    </span>
                                                </div>
                                            </td> -->
                                            <td class="flexCol">
                                                <div class="content duration">
                                                    <!-- <span class="title hidden-sm hidden-md hidden-lg">Duration : </span> -->
                                                    <span class="value">
                                                        {{ !empty($course['duration']) ? $course['duration'] : ''}} days
                                                    </span>
                                                </div>
                                            </td>
                                            <td class="flexCol">
                                                <div class="content date">
                                                   
                                                    <span class="value">
                                                         {{ !empty($course['course_start_date']) ? date('d M Y', strtotime($course['course_start_date'])) : 'On Demand'}}

                                                        <?php
                                                            if($course['batch_type'] == 'on_demand' && empty($course['course_start_date']) ) {
                                                                echo "<span class='sub-date'> ( " . date('M y', strtotime($course['start_date'])) . ")</span>";
                                                            }
                                                        ?>
                                                    </span>
                                                </div>
                                            </td>                            
                                            <td class="flexCol">
                                                <div class="content fee">
                                                    
                                                    <span class="value">
                                                        <?php $discount = ''; ?>
                                                        @if(isset($course['batch_discount']['discount']) && !empty($course['batch_discount']['discount']) && isset($course['batch_discount']['company_registration']['advertisment_details']) && !empty($course['batch_discount']['company_registration']['advertisment_details']))
                                                            <?php
                                                                $discount = $course['batch_discount']['discount'];

                                                                $cost = !empty($course['cost']) ? $course['cost'] : '';
                                                                $discounted_cost = $cost - ($cost*$discount/100);
                                                            ?>
                                                            <div class="striked_cost">{{$cost}}</div><br>
                                                            <div class="new_cost">{{ !empty($discounted_cost) ? $discounted_cost : ''}}</div>

                                                        @else
                                                            <div class="new_cost">{{ !empty($course['cost']) ? $course['cost'] : ''}}</div> 
                                                        @endif

                                                        <?php
                                                            if($course['available_size'] <= 3) {
                                                                echo "<span class='sub-date span-redColor'>".$course['available_size']." seat left</span>";
                                                            }
                                                        ?>
                                                        
                                                    </span>
                                                </div>
                                            </td>
                                            
                                            @if(isset($discount) && !empty($discount))
                                                <td class="flexCol">
                                                    <div class="content discount">
                                                        <div class="burst">{{$discount}}%</div>
                                                        <div class="text">Discount</div>
                                                    </div>
                                                </td>
                                            @else
                                                <td class="flexCol">
                                                    <div class="content discount">
                                                        <div class="">&nbsp;</div>
                                                        <div class="text">&nbsp;</div>
                                                    </div>
                                                </td>
                                            @endif
                                            <td class="flexCol">
                                                <div class="content cta">
                                                    <a data-batch="{{$course['id']}}" class="book_cta book_institute_batch">Book</a>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="paginator-content  pull-right no-margin homepage-pg">
                                                <?php $data1 = $pagination; ?>
                                                <ul id="top-user-paginator" class="pagination">
                                                    {!! $data1->render() !!}
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                @else
                                    <tr class="flexRow">
                                        <td colspan="7"  class="flexCol">
                                            No results found. Try again with different search criteria.
                                        </td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                   
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="search-listing-filter-modal" role="dialog">
        <div class="modal-dialog">

          <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title text-center">Filter Results</h4>
                </div>
                <div class="modal-body no-padding">
                    <div class="institutesSearchResult">
                        <div class="container">
                            <div class="row">
                                <div class="col-xs-12 col-lg-3">
                                    <div class="filter_grid">
                                        <div class="filter_grid-title is_redText is_bold">
                                            Filter Results
                                        </div>

                                        <div class="filter_grid-block">
                                            <div class="form_section">
                                                <div class="form_section-title">Institute Name</div>
                    
                                                <div class="form_section-block">
                                                    <form class="instituteFilterForm">
                                                        @foreach($institute_list as $institute)
                                                            <div class="form_group">
                                                                <input type="checkbox" class="filled-in inst_chkbox" data-name="{{ ucwords($institute['institute_name']) }}" name="inst_chkbox-{{$institute['id']}}" id="inst_checkbox-{{$institute['id']}}" data-id="inst_checkbox-{{$institute['id']}}" value="{{$institute['id']}}">
                                                                <label for="inst_checkbox-{{$institute['id']}}">
                                                                    {{ ucwords($institute['institute_alias']) }}
                                                                </label>
                                                            </div>
                                                        @endforeach
                                                    </form>
                                                </div>
                                            </div>

                                            <div class="form_section">
                                                <div class="form_section-title">Location</div>
                    
                                                <div class="form_section-block">
                                                    <form class="instituteFilterForm">
                                                        @foreach($locations as $location)

                                                            <div class="form_group">
                                                                <input type="checkbox" class="filled-in loc_chkbox" data-name="{{ ucwords($location['city']['name']) }}" name="loc_chkbox-{{$location['city_id']}}" id="loc_checkbox-{{$location['city_id']}}" data-id="loc_checkbox-{{$location['city_id']}}" value="{{$location['city_id']}}">
                                                                <label for="loc_checkbox-{{$location['city_id']}}">
                                                                    {{ ucwords($location['city']['name']) }}
                                                                </label>
                                                            </div>
                                                        @endforeach
                                                    </form>
                                                </div>
                                            </div>

                                            <div class="form_section">
                                                <div class="form_section-title">Date</div>
                    
                                                <div class="calender">
                                                    <div class="instituteFilterForm">
                                                        <div id="datepicker" class="coss_datepicker"></div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form_section">
                                                <div class="form_section-title">Fee</div>
                                                <div class="form_section-block">
                                                    <div class="switch_block">
                                                        <span class="text">Discount</span>
                                                        <label class="switch">
                                                            <input type="checkbox"/>
                                                            <span></span>
                                                        </label>
                                                        <span class="text">All</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="location-selector" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title text-center">Select Batch Location</h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" class="selected_batch_id">
                    <div class="location-radio">
                        
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-default book-institute-by-location" data-dismiss="modal">Book</button>
                </div>
            </div>
        </div>
    </div>
@stop

@section('js_script')
    <script type="text/javascript" src="{{ URL:: asset('public/assets/js/site/institutes_search.js')}}"></script>
@stop