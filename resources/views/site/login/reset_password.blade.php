@extends('site.index')
@section('content')
<div class="login_home_page content-section-wrapper">
    <div class="section container">
        <div class="row">
            <div class="col-xs-12">
                <div class="login_page login-box">
                    <div id="login-popup" class="white-popup signin-sigup-default no-margin" role="dialog" >
                        <div class="signin-signup-heading">
                            <div class="signin-signup new-sigin-link active login_page"><a class="open-popup-link">Change Password</a></div>
                        </div>
                        <form id="loginForm" class="form-horizontal" role="form" method="POST" action="{{ url('/password/email') }}">
                            <input type="hidden" name="_token" value="{{{ csrf_token() }}}">
                           <!--  <input type="hidden" name="redirect_url" value="{{ isset($_GET['redirect_url']) ? !empty($_GET['redirect_url']) ? $_GET['redirect_url'] : '' : ''}}"> -->

                            <div class="new-login-content">
                                <div class="row">
                                    <div class="new-login-form-container">
                                        <div class="small-12 medium-12 columns">
                                            <div class="new-login-form-container">
                                                <div id="login-form" @if(session('login_error')) style="display: block;" @endif style="display: block;"">
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <b>Please enter your registered email address.</b>
                                                        </div>
                                                    </div>
                                                    <div class="row margin-vert-5">
                                                        <div class="col-sm-3 padding-top-5">
                                                            <label class="mar-b-10"><span class="label-icon"><i class="fa fa-envelope" aria-hidden="true"></i></span><span class="label-text mandatory">Email</span>
                                                            </label>
                                                        </div>
                                                        <div class="col-sm-9">
                                                            <input class="form-control" type="email" name="email" id="email" placeholder="Enter email address" value="{{ old('email') }}"/>
                                                        </div>
                                                    </div>
                                                    @if(Session::has('status') == '1')
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <div class="alert alert-success m-b-0">
                                                                Password reset link has been sent on your email address.
                                                            </div>
                                                        </div>
                                                    </div>
                                                    @endif
                                                    <div class="row margin-vert-5">
                                                        <div class="col-sm-12">
                                                            <button type="submit" data-style='zoom-in' class="btn-1 light-blue new-sign-up-btn fullwidth ladda-button m-t-15">
                                                                Send Password Reset Link
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@stop
@section('js_script')
    <script type="text/javascript">
        $("#loginForm").validate({
            rules: {
                email: {
                  required: true,
                  email: true
                }
            }
        });
    </script>
@stop