@extends('site.index')

@section('content')
<div class="home-new home featured_company_section">
    <div class="content-wrapper search-listing content-section-wrapper">
        <div class="container">
            <div class="row search-listing-container">
                <button class="btn search-listing-sm-filter-btn" type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#search-listing-filter-modal"><i class="fa fa-filter" aria-hidden="true"></i></button>
                <div class="col-sm-3 col-xs-hidden">
                    <div class="filter-container filter-container-lg">
                        <div class="heading">
                            Filter
                        </div>
                        <form id="company-search-filter" method="get">
                            {{csrf_field()}}
                            <div class="content-container">
                                <div class="form-group">
                                    <label class="input-label" for="company_name">Company Name</label>
                                    <input type="text" class="form-control" id="company_name" name="company_name" value="{{ isset($filter['company_name']) ? $filter['company_name'] : ''}}" placeholder="Enter Company Name">
                                </div>
                                <div class="form-group">
                                    <label class="input-label">Country</label>
                                    <select name="country" class="form-control search-select country-search">
                                        <option value="">Select Your Country</option>
                                        @foreach( \CommonHelper::countries() as $c_index => $country)
                                            <option value="{{ $c_index }}" {{ isset($filter['country']) ? $filter['country'] == $c_index ? 'selected' : '' : ''}}> {{ $country }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label class="input-label">State</label>
                                    @if(isset($filter))
                                        @if(isset($filter['country']) && $filter['country'] == '95' )
                                        <select id="state" name="state" class="form-control search-select state fields-for-india">
                                            <option value="">Select Your State</option>
                                            @foreach($state as $states)
                                                <option value="{{$states['id']}}" {{ isset($filter['state']) ? $states['id'] == $filter['state'] ? 'selected' : '' : ''}}>{{ucwords(strtolower($states['name']) )}}</option>
                                            @endforeach
                                        </select>
                                        <input type="text" id="state" name="state_name" class="form-control state_text hide fields-not-for-india" placeholder="Enter State Name" value="">

                                        @else
                                            <select id="state" name="state" class="form-control search-select state hide fields-for-india">
                                                <option value="">Select Your State</option>
                                            </select>
                                            <input type="text" name="state_text" class="form-control state_text fields-not-for-india" placeholder="Enter State Name" value="{{ isset($filter['state_text']) ? $filter['state_text'] : ''}}">
                                        @endif
                                    @else

                                        <select id="state" name="state" class="form-control search-select state hide fields-for-india">
                                            <option value="">Select Your State</option>
                                        </select>
                                        <input type="text" name="state_text" class="form-control state_text fields-not-for-india" placeholder="Enter State Name" value="{{ isset($filter['state_text']) ? $filter['state_text'] : ''}}">
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label class="input-label" for="ship">City</label>
                                    @if(isset($filter['country']))
                                        @if($filter['country']) && $filter['country'] == '95')
                                            <select id="city" name="city" class="form-control city fields-for-india">
                                                <option value="">Select Your City</option>
                                                @if(isset($city) && !empty($city))
                                                    @foreach($city as $c)
                                                        <option value="{{$c['id']}}" {{ isset($filter['city']) ? $c['id'] == $filter['city'] ? 'selected' : '' : ''}}>{{ucwords(strtolower($c['name']) )}}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                            <input type="text" name="city_text" class="form-control city_text hide fields-not-for-india" placeholder="Enter City Name" value="">

                                    @else
                                        <select id="city" name="city" class="form-control city hide fields-for-india">
                                            <option value="">Select Your City</option>
                                        </select>

                                        <input type="text" name="city_text" class="form-control city_text fields-not-for-india" placeholder="Enter City Name" value="{{ isset($filter['city_text']) ? $filter['city_text'] : ''}}">
                                    @endif
                                @else
                                    <select id="city" name="city" class="form-control city hide fields-for-india">
                                        <option value="">Select Your City</option>
                                    </select>

                                    <input type="text" name="city_text" class="form-control city_text fields-not-for-india" placeholder="Enter City Name" value="{{ isset($value['city_text']) ? $value['city_text'] : ''}}">
                                @endif
                                </div>
                                <div class="form-group">
                                    <button type="submit" data-style="zoom-in" class="btn coss-primary-btn ladda-button" id="seafarer-job-search-button">Search</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    @include('site.partials.featured_advertisement_template')
                </div>
                <div class="col-sm-9 col-xs-12">
                    <div class="sort-by-container">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="search-title">
                                    Featured Companies
                                </div>
                            </div>
                        </div>
                    </div>
                    @if(isset($advertisements) && !empty($advertisements))
                        <?php
                            $advertisement_details = $advertisements->toArray();
                        ?>
                        @if(isset($advertisement_details['data']) && !empty($advertisement_details['data']))
                        <div class="section_2" style="background-color: transparent !important;">
                            <div class="row">
                                <div class="col-xs-12 col-md-12">
                                    <div class="row">
                                        <div class="col-xs-12 col-md-12">
                                            <div class="pagi pagi-up p-t-10">
                                                <span class="search-count p-t-10">
                                                    Showing {{$advertisement_details['from']}} - {{$advertisement_details['to']}} of {{$advertisement_details['total']}} Companies
                                                </span>
                                                <nav> 
                                                <ul class="pagination pagination-sm">
                                                    {!! $advertisements->render() !!}
                                                </ul> 
                                                </nav>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                        if(isset($advertisement_details['data']) && !empty($advertisement_details['data'])){
                                            $all_advertisements = array_chunk($advertisement_details['data'],4);
                                        }
                                    ?>
                                    @foreach($all_advertisements as $section)
                                        <div class="row">
                                            @foreach($section as $index => $advertisement)
                                            <div class="col-xs-12 col-sm-6 col-md-3 flex_col">
                                                <div class="coss_card">
                                                    <div class="coss_card-image" style="background-image:url('{{!empty($advertisement['user_details']['profile_pic']) ? "/".env('COMPANY_LOGO_PATH').$advertisement['user_id']."/".$advertisement['user_details']['profile_pic'] : 'images/user_default_image.png'}}'); ">
                                            
                                                </div>
                                                    
                                                    <div class="coss_card-content">
                                                        <div class="coss_card-contentTitle">
                                                            {{ isset($advertisement['company_name']) ? strtoupper($advertisement['company_name']) : ''}}
                                                        </div>
                                                        <div class="coss_card-contentDesc">

                                                            @if(isset($advertisement['next_company_jobs']) && !empty($advertisement['next_company_jobs']))

                                                                <?php
                                                                    $total_jobs = count($advertisement['next_company_jobs']);
                                                                    $count = 3;
                                                                    if($total_jobs < 3){
                                                                        $count = $total_jobs;
                                                                    }

                                                                    // $count = 5;
                                                                    $job_count = 0;
                                                                ?>
                                                                Urgently requires
                                                                @foreach($advertisement['next_company_jobs'] as $a_index => $jobs)
                                                                    @foreach(\CommonHelper::new_rank() as $index => $category)
                                                                        @foreach($category as $r_index => $rank)
                                                                            @if(isset($jobs['rank']) && !empty($jobs['rank']))
                                                                                @if($job_count < 3)
                                                                                    @if($jobs['rank'] == $r_index)
                                                                                        {{$rank}}{{!($count == $a_index+1) ? ',' : '....'}}
                                                                                        <?php $job_count++; ?>
                                                                                    @endif
                                                                                @endif
                                                                            @endif   
                                                                        @endforeach
                                                                    @endforeach
                                                                @endforeach
                                                            @else
                                                                <div class="coss_card-no-opening">
                                                                    Currently there are no active jobs.
                                                                </div>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="coss_card-footer_new">
                                                        {{ isset($advertisement['created_at']) ? date('dS M, Y', strtotime($advertisement['created_at'])) : ''}}
                                                        <a href="{{route('site.show.company.details.user',$advertisement['user_id'])}}" class="coss_card-footerLink pull-right" target="_blank">Read More</a>
                                                    </div>
                                                </div>
                                            </div>

                                            @endforeach
                                        </div>
                                    @endforeach
                                        

                                    <div class="row">
                                        <div class="col-xs-12 col-md-12">
                                            <span class="pagi pagi-up">
                                                <span class="search-count m-t-10">
                                                    Showing {{$advertisement_details['from']}} - {{$advertisement_details['to']}} of {{$advertisement_details['total']}} Companies
                                                </span>
                                                <nav> 
                                                <ul class="pagination pagination-sm">
                                                    {!! $advertisements->render() !!}
                                                </ul> 
                                                </nav>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @else
                            <div class="card-container">
                                <div class="row no-results-found">No results found. Try again with different search criteria.</div>
                            </div>
                        @endif
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="search-listing-filter-modal" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title text-center">Filter</h4>
            </div>
            <div class="modal-body">
                <div class="filter-container-sm">
                    <form id="company-search-filter" method="get">
                        {{csrf_field()}}
                        <div class="content-container">
                            <div class="form-group">
                                <label class="input-label" for="company_name">Company Name</label>
                                <input type="text" class="form-control" id="company_name" name="company_name" value="{{ isset($filter['company_name']) ? $filter['company_name'] : ''}}" placeholder="Enter Company Name">
                            </div>
                            <div class="form-group">
                                <label class="input-label">Country</label>
                                <select name="country" class="form-control search-select country-search">
                                    <option value="">Select Your Country</option>
                                    @foreach( \CommonHelper::countries() as $c_index => $country)
                                        <option value="{{ $c_index }}" {{ isset($filter['country']) ? $filter['country'] == $c_index ? 'selected' : '' : ''}}> {{ $country }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label class="input-label">State</label>
                                @if(isset($filter))
                                    @if(isset($filter['country']) && $filter['country'] == '95' )
                                    <select id="state" name="state" class="form-control search-select state fields-for-india">
                                        <option value="">Select Your State</option>
                                        @foreach($state as $states)
                                            <option value="{{$states['id']}}" {{ isset($filter['state']) ? $states['id'] == $filter['state'] ? 'selected' : '' : ''}}>{{ucwords(strtolower($states['name']) )}}</option>
                                        @endforeach
                                    </select>
                                    <input type="text" id="state" name="state_name" class="form-control state_text hide fields-not-for-india" placeholder="Enter State Name" value="">

                                    @else
                                        <select id="state" name="state" class="form-control search-select state hide fields-for-india">
                                            <option value="">Select Your State</option>
                                        </select>
                                        <input type="text" name="state_text" class="form-control state_text fields-not-for-india" placeholder="Enter State Name" value="{{ isset($filter['state_text']) ? $filter['state_text'] : ''}}">
                                    @endif
                                @else

                                    <select id="state" name="state" class="form-control search-select state hide fields-for-india">
                                        <option value="">Select Your State</option>
                                    </select>
                                    <input type="text" name="state_text" class="form-control state_text fields-not-for-india" placeholder="Enter State Name" value="{{ isset($filter['state_text']) ? $filter['state_text'] : ''}}">
                                @endif
                            </div>
                            <div class="form-group">
                                <label class="input-label" for="ship">City</label>
                                @if(isset($filter['country']))
                                    @if($filter['country']) && $filter['country'] == '95')
                                        <select id="city" name="city" class="form-control city fields-for-india">
                                            <option value="">Select Your City</option>
                                            @if(isset($city) && !empty($city))
                                                @foreach($city as $c)
                                                    <option value="{{$c['id']}}" {{ isset($filter['city']) ? $c['id'] == $filter['city'] ? 'selected' : '' : ''}}>{{ucwords(strtolower($c['name']) )}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                        <input type="text" name="city_text" class="form-control city_text hide fields-not-for-india" placeholder="Enter City Name" value="">

                                @else
                                    <select id="city" name="city" class="form-control city hide fields-for-india">
                                        <option value="">Select Your City</option>
                                    </select>

                                    <input type="text" name="city_text" class="form-control city_text fields-not-for-india" placeholder="Enter City Name" value="{{ isset($filter['city_text']) ? $filter['city_text'] : ''}}">
                                @endif
                            @else
                                <select id="city" name="city" class="form-control city hide fields-for-india">
                                    <option value="">Select Your City</option>
                                </select>

                                <input type="text" name="city_text" class="form-control city_text fields-not-for-india" placeholder="Enter City Name" value="{{ isset($value['city_text']) ? $value['city_text'] : ''}}">
                            @endif
                            </div>
                            <div class="form-group">
                                <button type="submit" data-style="zoom-in" class="btn coss-primary-btn ladda-button" id="seafarer-job-search-button">Search</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>
@stop

@section('js_script')
    <script type="text/javascript" src="/js/site/job.js"></script>
@stop