@extends('site.cms-index')
@section('content')
    <div class="footer-page-container" style="margin-top: 60px;">
        <h1 class="footer-page-title">COOKIE POLICY</h1>

        <h3 class="footer-page-midtitle">The User Restriction</h3>
        <p class="footer-page-para">
            User are not allowed to Sell, transmit, host or otherwise commercially exploit the website or the Company.
        </p>
        <p class="footer-page-para">
            The website, its content and trademarks are exclusive intellectual properties of the company.
        </p>
        <h3 class="footer-page-midtitle">Updates on The website</h3>
        <p class="footer-page-para">
            The website is open for updates with new features, bug fixes etc. as needed.
        </p>
        <h3 class="footer-page-midtitle">Information shared by user</h3>
        <p class="footer-page-para">
            Professional details are included in the resume.
        </p>
        <p class="footer-page-para">
            Contact & location details.
        </p>
        <h3 class="footer-page-midtitle">Promotions on and about website</h3>
        <p class="footer-page-para">
            There will be promotion content on the website.
        </p>
        <p class="footer-page-para">
            Google Ads (AdWords)/ Bing ADs / Facebook / Youtube / pinterest can be used as Advertisement on the website.
        </p>
        <p class="footer-page-para">
            remarketing services are used to advertise our website.
        </p>
        <p class="footer-page-para">
            Tracking your movement on the website via Google Analytics or other related tools will ensure better
            coordination between user and the institute/company.
        </p>
        <h3 class="footer-page-midtitle">This website uses cookies</h3>
        <p class="footer-page-para">
            We use cookies to personalize content and ads, to provide social media features and to analyze our traffic. We
            also share information about your use of our site with our social media, advertising and analytics partners who
            may combine it with other information that you’ve provided to them or that they’ve collected from your use of
            their services.
        </p>
    </div>
@stop
