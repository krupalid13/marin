@extends('site.index')
@section('content')
	<div class="t-c content-section-wrapper">
		<div class="section container">
			<div class="section_heading">
				Terms &amp; Conditions
			</div>
			<ul style="padding-left: 20px;">
				<li>
					We are a virtual platform that offers a plethora of job opportunities and services from the shipping industry.
				</li>
				<li>
					Course4Sea does not promote profiles that are incompletely or inaccurately filled.
				</li>
				<li>
					Course4Sea requests job applicants, employers and agents alike to share accurate and authentic data on the site for the benefit of extracting the best service.
				</li>
				<li>
					Course4Sea is just a platform for sharing information and does not guarantee jobs to an applicant. We also do not guarantee employers that the job postings will be viewed by a certain population of job seekers in a certain span or otherwise.
				</li>
				<li>
					Course4Sea does not allow proxy users to use and operate your account.
				</li>
				<li>
					Course4Sea restricts users from posting any non-job information like franchise
					models, commission schemes, distributorship, membership, agency modules,
					business offerings or any such opportunities.
				</li>
				<li>
					Attempting to spam other users with information.
				</li>
				<li>
					Refrain from monetary transactions on behalf of Course4Sea for services offered.
				</li>
				<li>
					Breaching security measures like probing, scanning, attacking the system under
					technical influence.
				</li>
				<li>
					Forging records and presenting to job applicants and employers.
				</li>
				<li>
					Unwelcome mails, calls, social media messages and other means of communication
					with individuals and companies are strictly prohibited.
				</li>
				<li>
					We offer specialized services for displaying your advertisements on our site. Our
					database should not be merely used for sending unsolicited mails bearing our
					promotional mails and advertising of services &amp; products without the knowledge of
					Course4Sea.
				</li>
				<li>
					All the monetary transactions, if any between the parties involved like companies
					and smart advertisers and any synergies and business established thereof, has no
					association with Course4Sea.
				</li>
				<li>
					We are merely a platform to offer services; and should not be considered liable for
					any fraudulent matter arising out of the scope of Course4Sea.
				</li>
				<li>
					Companies advertising job requirements should establish clear communication and
					understanding on the requirements posted. An authentic and time-bound
					requirement that can fetch better and faster results.
				</li>
				<li>
					Marine Institutions and smart advertisers are third party associations. Marine
					Institutions and Smart advertisers are separate entities and proposing any offers and
					discounts on Course4Sea site does not attract any offers and discounts from
					Course4Sea itself.
				</li>
			</ul>
		</div>
	</div>
@stop


