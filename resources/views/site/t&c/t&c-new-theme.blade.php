@extends('site.cms-index')
@section('content')
    <div class="footer-page-container" style="margin-top: 60px;">
        <h1 class="footer-page-title">TERMS & CONDITIONS</h1>
        <p class="footer-page-para">
            This document is an electronic record in terms of Information Technology Act, 2000 and rules there under as
            applicable and the amended provisions pertaining to electronic records in various statutes as amended by the
            Information Technology Act, 2000.
        </p>
        <p class="footer-page-para">
            The domain name www.flanknot.com (hereinafter referred to as "Website") is owned by Flankknot (hereinafter referred to as "The Company") a company incorporated under the Companies Act, 1956.
        </p>
        <p class="footer-page-para">
            Your use of the Website and services and tools are governed by the following terms and conditions ("Terms of
            Use") as applicable to the Website including the applicable policies which are incorporated herein by way of
            reference. If You transact on the Website, you shall be subject to the policies that are applicable to the
            Website for such transaction.
            By mere use of the Website, you shall be contracting with Flankknot the company and these terms and
            conditions including the policies constitute Your binding obligations, with Flankknot the company.
        </p>
        <p class="footer-page-para">
            For the purpose of these Terms of Use, wherever the context so requires "You" or "User" shall mean any natural
            or legal person who has agreed to transact on the Website by providing Registration Data while registering on
            the Website as Registered User using the computer systems.
        </p>
        <p class="footer-page-para">
            The website flanknot.com on registration allows the user to create his/her professional seafaring profile along
            with dynamic resumes and also upload the his/her professional documents.
        </p>
        <p class="footer-page-para">
            When You use any of the services provided by Us through the Website, including but not limited to, (getting best
            course rates and job openings) You will be subject to the rules, guidelines, policies, terms, and conditions
            applicable to such service, and they shall be deemed to be incorporated into this Terms of Use and shall be
            considered as part and parcel
            of this Terms of Use.
        </p>
        <p class="footer-page-para">
            We reserve the right, at Our sole discretion, to change, modify, add or remove portions of these Terms of Use,
            at any time without any prior written notice to You. It is Your responsibility to review these Terms of Use
            periodically for updates / changes. Your continued use of the Website following the posting of changes will mean
            that You accept and
            agree to the revisions. As long as You comply with these Terms of Use, we grant You a personal,
            non-exclusive,non-transferable, limited privilege to enter and use the Website.
        </p>
        <h3 class="footer-page-midtitle">Membership Eligibility</h3>
        <p class="footer-page-para">
            Members that are minors, un-discharged insolvents etc. are not eligible to use the Website. If you are a minor
            i.e. under the age of 18 years, you shall not register as a User of the website and shall not transact on or use
            the website. As a minor if you wish to use or transact on website, such use or transaction may be made by your
            legal guardian or parents on the Website. The Company reserves the right to terminate your membership and / or
            refuse to provide you with access to the Website if it is brought to the Company’s notice.
        </p>
        <h3 class="footer-page-midtitle">Your Account and Registration Obligations</h3>
        <p class="footer-page-para">
            If You use the Website, you shall be responsible for maintaining the confidentiality of your Username and
            Password and You shall be responsible for all activities that occur under your Username.
        </p>
        <p class="footer-page-para">
            You agree that if You provide any information that is untrue, inaccurate, not current or incomplete. And/or we
            have reasonable grounds to suspect that your provided information is untrue, inaccurate, not current or
            incomplete, or not in accordance with Terms of Use, we shall have the right to indefinitely suspend or terminate
            or block access of your
            membership on the Website and refuse to provide You with access to the Website.
        </p>
        <p class="footer-page-para">
            Your mobile phone number and/or e-mail address is treated as Your primary identifier on the Website. It is your
            responsibility to ensure that Your mobile phone number and your email address is up to date on the Website at
            all times. You agree to notify Us promptly if your mobile phone number or e-mail address changes by updating the
            same on the Website through a onetime password verification.
        </p>
        <p class="footer-page-para">
            Your Passport number / Indos number (for Indian seafarers) along with your Date of Birth are unique data
            connected to your primary identifier.
        </p>
        <p class="footer-page-para">
            You agree that flanknot.com shall not be liable or responsible for the activities or consequences of use or
            misuse of any information that occurs under your display name in cases where You have failed to update Your
            revised mobile phone number and/or e-mail address on the Website or shared your login details with someone.
        </p>
        <h3 class="footer-page-midtitle">Communications</h3>
        <p class="footer-page-para">
            When You use the Website to share your professional data with a company or friends, you agree and understand
            that you are communicating with us through electronic records and You consent to receive communications like
            quick responses via electronic records from companies and friends via Flanknot.
        </p>
        <p class="footer-page-para">
            We also present your information’s to institutes / companies or advertisers aligned with us, periodically and as
            and when required.
        </p>
        <p class="footer-page-para">
            You can apply for job openings through this website or take our assist in booking a course.
            We may communicate with you by email or by such other mode of communication, electronic or otherwise.
        </p>
        <h3 class="footer-page-midtitle">Contents Posted on Site</h3>
        <p class="footer-page-para">
            For Shipping Companies, all data including and not limited to details about the companies, licence, vessel
            details, job requirement, contact information, hiring procedure mentioned are sole responsibility of the
            company.
        </p>
        <p class="footer-page-para">
            The job openings are posted by Shipping Companies registered with us on the website.
        </p>
        <p class="footer-page-para">
            The website also displays jobs and openings that are circulating on various electronic and print formats.
            However, the authenticity of such requirements cannot be confirmed and it is the sole responsibility of the user
            to check genuineness of such job requirements.
        </p>
        <p class="footer-page-para">
            The website will present the user with options for course and best price possible. To avail the best price, the
            user will however have to book the course through the website.
        </p>
        <p class="footer-page-para">
            The Company does not make any representation or Warranty as to specifics (such as quality of course, premise
            accuracy,etc) offered by the institute.
        </p>
        <p class="footer-page-para">
            The Company is not responsible for any non-performance or breach of any contract entered into between User and
            Institute. The Company cannot and does not guarantee that the Institute will perform the course on the given
            date.
        </p>
        <p class="footer-page-para">
            The Company shall not and is not required to mediate or resolve any dispute or disagreement between the user and
            Institute.
        </p>
        <p class="footer-page-para">
            The Company is not responsible for unsatisfactory or delayed performance of courses.
        </p>
        <p class="footer-page-para">
            The website is only a platform that can be utilized by Users to reach a larger base to find the course required.
        </p>
        <p class="footer-page-para">
            The Company is not responsible to the outcome of job application. A positive QR does not guarantee a confirmed
            job. The QR feature is presented so that the user can know the status for his job application or sharing of
            resume to companies.
        </p>
        <p class="footer-page-para">
            The user has an option of blocking the resume viewer of giving a quick response QR.
        </p>
        <p class="footer-page-para">
            The content posted by user are details used in preparation of the user resume. The user can also upload
            documents related only to his profession.
        </p>
        <p class="footer-page-para">
            The user content will be open for viewing by companies, Institutes & Promoters.
        </p>
        <p class="footer-page-para">
            The data posted by the user will be monitored and if found inappropriate, will be taken off immediately without
            any prior notice.
        </p>
        <h3 class="footer-page-midtitle">Promotions on your website</h3>
        <p class="footer-page-para">
            There will be promotion content on the website.
        </p>
        <h3 class="footer-page-midtitle">Registration Charges</h3>
        <p class="footer-page-para">
            Registration on the Website is free for users. The Company does not charge any fee for browsing the Website. The
            Company reserves the right to change its Fee Policy from time to time. In particular, The Company may at its
            sole discretion introduce new services and modify some or all of the existing services offered on the Website.In
            such an event The Company reserves the right to introduce fees for the new services offered or amend/introduce
            fees for existing services, as the case may be.
        <p class="footer-page-para">
            Changes to the Fee Policy shall be posted on the Website and such changes shall automatically become effective
            immediately after they are posted on the Website. Unless otherwise stated, all fees shall be quoted in Indian
            Rupees or US dollars.
        </p>
        <h3 class="footer-page-midtitle">Use of the Website</h3>
        <p class="footer-page-para">
            You agree, undertake and confirm that Your use of Website shall be strictly governed by the following binding
            principles.
        </p>
        <p class="footer-page-para">
            You shall not host, display, upload, modify, publish, transmit, update or share any information which belongs to
            another person and to which You does not have any right to.
        </p>
        <p class="footer-page-para">
            Harmful, harassing, blasphemous, defamatory, obscene, pornographic, paedophilic, libellous, invasive of
            another's privacy, hateful, or racially, ethnically objectionable, disparaging, relating or encouraging money
            laundering or gambling, or otherwise unlawful in any manner whatever; or unlawfully threatening or unlawfully
            harassing including but not limited to "indecent representation of women" is prohibited.
        </p>
        <p class="footer-page-para">
            No data offensive to the online community, such as sexually explicit content, or content that promotes
            obscenity, paedophilia, racism, bigotry, hatred or physical harm of any kind against any group or individual.
        </p>
        <p class="footer-page-para">
            No entries that Harasses or advocates harassment of another person.
        </p>
        <p class="footer-page-para">
            No involvement in the transmission of "junk mail", "chain letters", or unsolicited mass mailing or "spamming".
        </p>
        <p class="footer-page-para">
            No Promotions of illegal activities or conduct that is abusive, threatening, obscene, defamatory or libellous.
            Or Infringes upon or violates any third party's rights [including, but not limited to, intellectual property
            rights, rights of privacy (including without limitation unauthorized disclosure of a person's name, email
            address, physical address or phone number) or rights of publicity.
        </p>
        <p class="footer-page-para">
            Does not promotes illegal or unauthorized copy of another person's copyrighted work, such as providing pirated
            computer programs or links to them, providing information to circumvent manufacture-installed copy-protect
            devices, or providing pirated music, videos, images or links to pirated files.
        </p>
        <p class="footer-page-para">
            No entries Containing restricted or password-only access pages, or hidden pages or images (those not linked to
            or from another accessible page).
        </p>
        <p class="footer-page-para">
            No material that exploits people in a sexual, violent or otherwise inappropriate manner or solicits personal
            information from anyone.
        </p>
        <p class="footer-page-para">
            No information about illegal activities such as making or buying illegal weapons, violating someone's privacy,
            or providing or creating computer viruses.
        </p>
        <p class="footer-page-para">
            No video, photographs, or images of another person (with a minor or an adult).
        </p>
        <p class="footer-page-para">
            No effort in trying to gain unauthorized access or exceeds the scope of authorized access to the Website or
            other areas of the Website or solicits passwords or personal identifying information for commercial or unlawful
            purposes from other users
        </p>
        <p class="footer-page-para">
            Infringes any patent, trademark, copyright or other proprietary rights or third party's trade secrets or rights
            of publicity or privacy or shall not be fraudulent or involve the sale of counterfeit or stolen products.
        </p>
        <p class="footer-page-para">
            No Impersonation of another person.
        </p>
        <p class="footer-page-para">
            Refrain from using data that contains software viruses or any other computer code, files or programs designed to
            interrupt, destroy or limit the functionality of any computer resource; or contains any trojan horses, worms,
            time bombs, cancelbots, easter eggs or other computer programming routines that may damage, detrimentally
            interfere with,
            diminish value of, surreptitiously intercept or expropriate any system, data or personal information.
        </p>
        <p class="footer-page-para">
            No Data that threatens the unity, integrity, defence, security or sovereignty of India, friendly relations with
            foreign states, or public order or causes incitement to the commission of any cognizable offence or prevents
            investigation of any offence or is insulting any other nation.
        </p>
        <p class="footer-page-para">
            You shall not use any "deep-link", "page-scrape", "robot", "spider" or other automatic device, program,
            algorithm or methodology, or any similar or equivalent manual process, to access, acquire, copy or monitor any
            portion of the Website or any Content.
        </p>
        <p class="footer-page-para">
            You shall not attempt to gain unauthorized access to any portion or feature of the Website, or any other systems
            or networks connected to the Website or to any server, computer, network, or to any of the services offered on
            or through the Website, by hacking, password "mining" or any other illegitimate means.
        </p>
        <p class="footer-page-para">
            You shall not make any negative, denigrating or defamatory statement(s) or comment(s) about us or the brand name
            or domain name used by us.
        </p>
        <p class="footer-page-para">
            You may not use the Website or any content for any purpose that is unlawful or prohibited by these Terms of Use,
            or to solicit the performance of any illegal activity or other activity.
        </p>
        <p class="footer-page-para">
            The Company shall have all the rights to take necessary action and claim damages that may occur due to
            involvement/participation in wrong doings and unlawful activities.
        </p>
        <h3 class="footer-page-midtitle">Indemnity</h3>
        <p class="footer-page-para">
            You shall indemnify and hold harmless flanknot.com the website and Flankknot the company, its owner, licensee, affiliates,
            subsidiaries, group companies (as applicable) and their respective officers, directors, agents, and employees,
            from any claim or demand, or actions including reasonable attorneys' fees, made by any third party or penalty
            imposed due to or arising out of Your
            breach of this Terms of Use, privacy Policy and other Policies, or Your violation of any law, rules or
            regulations or the rights (including infringement of intellectual property rights) of a third party.
        </p>
        <h3 class="footer-page-midtitle">Applicable Law</h3>
        <p class="footer-page-para">
            Terms of Use shall be governed by and interpreted and construed in accordance with the laws of India. The place
            of jurisdiction shall be exclusively in Mumbai.
        </p>
        <h3 class="footer-page-midtitle">Trademark, Copyright and Restriction</h3>
        <p class="footer-page-para">
            TThis Flanknot.com website is controlled and operated by Flankknot the company and courses are sold by respective Institutes. All shipping
            Companies & Maritime Institutes on this site, including images, illustrations, and video clips, are protected by
            copyrights, trademarks, and other intellectual property rights. Material on Website is solely for Your
            personal,non-commercial use.
        </p>
        <h3 class="footer-page-midtitle">Grievance officer</h3>
        <p class="footer-page-para">
            For any grievance contact our grievance officer at Email <a href="mailto:grievance@flanknot.com">grievance@flanknot.com</a>
        </p>

    </div> <!-- <div class="footer-page-container"> -->
@stop
