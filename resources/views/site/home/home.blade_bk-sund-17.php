@extends('site.index')

@section('content')
<div class="cs-home">
    <div class="banner" style="background-image:url('/images/home/home.png')">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="banner-wrapper">
                        <div class="banner-content">
                            <div class="banner-heading">Lorem Ipsum is simply dummy text of the 
                                        printing and typesetting industry?</div>
                            <div class="banner-search">
                                <div class="search_tabs">
                                    <ul class="nav nav-pills">
                                        <li class="active">
                                            <a href="" class="curvy-square" data-toggle="pill">
                                                <img src="/images/home/course.png" alt="">
                                                <div>Course</div>
                                            </a>
                                        </li>
                                        <li> 
                                            <a href="" class="curvy-square" data-toggle="pill"> 
                                                <img src="/images/home/institute.png" alt="">
                                                <div>Institute</div>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="banner-filter">
                                    <form id="seafarer-course-search" class="course-search-filter" method="get" action="{{route('site.seafarer.course.search')}}">
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-3 pr-0 mt-15 br-r-0">
                                                <div class="form-group">
                                                    <div class="form-input">
                                                        <select name="course_type" class="form-control course_type_filter">
		                                                    <option value=''>Select Course Type</option>
		                                                    @foreach(\CommonHelper::institute_course_types() as $r_index => $rank)
		                                                        <option value="{{$r_index}}" {{ isset($filter['course_type']) ? $filter['course_type'] == $r_index ? 'selected' : '' : ''}}>{{$rank}} </option>
		                                                    @endforeach
		                                                </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-3 plr-0 mt-15 br-0">
                                                <div class="form-group">
                                                    <div class="form-input">
                                                        <!-- <select name="location" class="form-control">
                                                            <option selected="selected">Location</option>
                                                        </select> -->
                                                        <select id="course_name" name="course_name" class="form-control institute_course_name">
		                                                    <option value=''>Select Course Name</option>
		                                                    @if(isset($course_name))
		                                                        @foreach($course_name as $r_index => $rank)
		                                                            <option value="{{$r_index}}" {{ isset($filter['course_name']) ? $filter['course_name'] == $r_index ? 'selected' : '' : ''}}>{{$rank}} </option>
		                                                        @endforeach
		                                                    @endif
		                                                </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-3 plr-0 mt-15 br-l-0">
                                                <div class="form-group">
                                                    <div class="form-input">
                                                        <select name="start_date" class="form-control">
                                                            <option selected="selected">Start Date</option> 
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-3 mt-15 text-left">
                                                <div class="filter-button">
                                                    <button class="btn cs-primary-btn">Search</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>                    
                </div>
            </div>
        </div>
    </div>
    <div class="section section-one">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-xs-12">  
                    <div class="course-wrapper">
                        <div class="courses">
                        	@if(!empty($course_type))
                        		@foreach($course_type as $index => $courses)
                        			<a class="card course-item" href="{{route('site.seafarer.course.search',['course_type' => $index,'course_name' => ''])}}">
		                                <div class="card-body">{{ strtoupper($courses) }}</div>
		                                <div class="card-footer">
		                                    {{ isset($course_by_type[$index]) ? count($course_by_type[$index]) : '0'}}  Courses <span class="icon">→</span>
		                                </div>
		                            </a>
                        		@endforeach
                        	@endif
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-xs-12">
                    <div class="white_board">
                        <div class="white_board-bg" style="background-image:url('/images/home/whiteboard.png')"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="cs-custom-footer">
    <div class="container">
        <div class="row mr-bottom-30">
        <div class="col-md-2 col-xs-12">
                <div class="footer-title">Quick links</div>
                <ul class="links-list">
                    <li class="links-list-menu"><a href="" class=""><span class="show_on_hover">-</span> About Us</a></li>
                    <li class="links-list-menu"><a href="" class=""><span class="show_on_hover">-</span> Conatct Us</a></li>
                    <li class="links-list-menu"><a href="" class=""><span class="show_on_hover">-</span> Disclaimer</a></li>
                    <li class="links-list-menu"><a href="" class=""><span class="show_on_hover">-</span> Policy</a></li>
                </ul>
            </div>
            <div class="col-md-2 col-xs-12">
                <div class="footer-title">Contact Us</div>
                <ul class="links-list">
                    <li class="links-list-menu flex mt-5"> 
                        <span class="flex--one icon-image" style="background-image:url('/images/home/map.png')"></span>
                        <span  class="flex--two">508/8, Coral Crest, Nerul East, Navi Mumbai - 400708</span>
                    </li>
                </ul>
            </div>
            <div class="col-md-3 col-xs-12">
               <div class="footer-title"></div>
                <ul class="links-list">
                    <li class="links-list-menu flex mt-5">
                        <span class="flex--one icon-image" style="background-image:url('/images/home/Email.png')"></span>
                        <span class="flex--two"><a href="mailto:techsupport@course4sea.com">techsupport@course4sea.com</a></span>  
                    </li>
                    <li class="links-list-menu flex mt-5">
                        <span class="flex--one icon-image" style="background-image:url('/images/home/Email.png')"></span>
                        <span class="flex--two"><a href="mailto:usersupport@course4sea.com">usersupport@course4sea.com</a></span>  
                    </li>
                    <li class="links-list-menu flex mt-5">
                        <span class="flex--one icon-image" style="background-image:url('/images/home/Email.png')"></span>
                        <span class="flex--two"><a href="mailto:course4sea@gmail.com">course4sea@gmail.com</a> </span>  </li>
                </ul>
            </div>
            <div class="col-md-2 col-xs-12">
                <div class="footer-title"></div>
                 <ul class="links-list">
                    <li class="links-list-menu mt-5"><a href="" class="flex"><span class="icon-image flex--one" style="background-image:url('/images/home/Mobile.png')"></span>  <span  class="flex--two">7738169926</span>   </a></li>
                    <li class="links-list-menu mt-5"><a href="" class="flex"><span class="icon-image flex--one" style="background-image:url('/images/home/Mobile.png')"></span>  <span  class="flex--two">7738169926</span>   </a></li>
                </ul>
            </div>
            <div class="col-md-3 col-xs-12">
                <div class="footer-title" style="padding:20px 10px;">Connect with us on </div>
                <ul class="social-icons">
                    <li class="social-icons-menu"><a href=""><img src="/images/home/fb.png" alt="facebooklogo"></a></li>
                    <li class="social-icons-menu"><a href=""><img src="/images/home/ln.png" alt="linked in"></a></li>
                    <li class="social-icons-menu"><a href=""><img src="/images/home/g.png" alt="google"></a></li>
                    <li class="social-icons-menu"><a href=""><img src="/images/home/utube.png" alt="youtube"></a></li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div id="enquire-advertisements-modal" class="modal extended-modal fade no-display" tabindex="-1" data-width="760">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="advertise-enquiry-form-container">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                          &times;
                        </button>
                        <div class="heading"> 
                            <h4 class="modal-title input-label">Advertisement Enquiry</h4>
                        </div>
                    </div>
                    <div class="modal-body enquire-advertisements-container">

                        <form id="advertisements-enquiry-modal-form" action="{{ route('site.advertisements.enquiry') }}">
                            {{csrf_field()}}
                            <input type="hidden" name="advertisement_id" id="advertisement_id" value="">
                            <input type="hidden" name="company_id" id="company_id" value="">
                            <div class="form-group">
                                <label class="input-label" for="name">Name</label>
                                <input type="text" class="form-control" id="name" name="name" value="{{isset($data['first_name']) ? $data['first_name'] : ''}} ">
                            </div>
                            <div class="form-group">
                                <label class="input-label" for="name">Phone Number</label>
                                <input type="text" class="form-control" id="phone" name="phone" value="{{isset($data['mobile'])?$data['mobile']:''}}">
                            </div>
                            <div class="form-group">
                                <label class="input-label" for="name">Email</label>
                                <input type="email" class="form-control" id="email" name="email" value="{{isset($data['email']) ? $data['email'] : ''}} ">
                            </div>
                            <div class="form-group">
                                <label class="input-label" for="name">Message</label>
                                <textarea class="form-control" rows="6" name="message" id="message"></textarea>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <div class="row right">
                            <div class="col-sm-12">
                                <button type="button" data-style="zoom-in" class="btn btn-blue btn-default ladda-button" id="send-advertise-enquiry-button">
                                    Send Enquiry
                                </button>   
                            </div>
                        </div>  
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="welcome-modal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Welcome {{ Auth::check() ? ucfirst(Auth::user()->first_name) : ''}}</h4>
              </div>
              <div class="modal-body">
                <p>Thank you for registering with us.</p>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>
            </div>

        </div>
    </div>

    <div id="email-verification-modal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Welcome {{ Auth::check() ? ucfirst(Auth::user()->first_name) : ''}}</h4>
              </div>
              <div class="modal-body">
                <p>Your email has been verified.</p>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>
            </div>

        </div>
    </div>

    <div id="reset-password-modal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Welcome {{ Auth::check() ? ucfirst(Auth::user()->first_name) : ''}}</h4>
              </div>
              <div class="modal-body">
                <p>Your password has been changed successfully.</p>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>
            </div>

        </div>
    </div>

    <div id="another-user-modal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Email Verification</h4>
                  </div>
                  <div class="modal-body">
                    <p>The email you are trying to verify does not match with your current account email address. Please <a href="{{route('logout')}}">logout</a> and try again</p>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  </div>
            </div>

        </div>
    </div>

    <div id="already-login-modal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Reset Password</h4>
                  </div>
                  <div class="modal-body">
                    <p>You are already login. Please <a href="{{route('logout')}}">logout</a> and try again</p>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  </div>
            </div>

        </div>
    </div>

    <div id="another-user-update-availabilty" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Email Verification</h4>
                  </div>
                  <div class="modal-body">
                    <p>The email with which you are trying to update your availability does not match with your current account email address. Please <a href="{{route('logout')}}">logout</a> and try again</p>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  </div>
            </div>

        </div>
    </div>

    <div id="job_apply" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Job Application</h4>
                  </div>
                  <div class="modal-body">
                    <p>Please <a href="{{route('logout')}}">logout</a> and apply to this job again.</p>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Ok</button>
                  </div>
            </div>

        </div>
    </div>
    
    <?php 
        $another_user = false;
        $another_user_update_availability = false;
        $auto_action = '';
        $job_apply = '';
        $already_login = '';
        
        if(isset($_GET['email_verification']) && !empty($_GET['email_verification'])){
            if( $_GET['email_verification'] == 'another_user'){
                $another_user = true;
            }
        }

        if(isset($_GET['update_availabilty']) && !empty($_GET['update_availabilty'])){
            if( $_GET['update_availabilty'] == 'another_user'){
                $another_user_update_availability = true;
            }
        }

        if(isset($_GET['auto_action']) && !empty($_GET['auto_action'])){
            if( $_GET['auto_action'] == 'verified'){
                $auto_action = 'verified';
            }
            if( $_GET['auto_action'] == 'reset_password'){
                $auto_action = 'reset_password';
            }
        }

        if(isset($_GET['auto_action']) && !empty($_GET['auto_action'])){
            if( $_GET['auto_action'] == 'welcome'){
                $auto_action = 'welcome';
            }
        }

        if(isset($_GET['job_apply']) && !empty($_GET['job_apply'])){
            if( $_GET['job_apply'] == 'another_user'){
                $job_apply = 'another_user';
            }
        }

        if(isset($_GET['already_login']) && !empty($_GET['already_login'])){
            if( $_GET['already_login'] == '1'){
                $already_login = 'already_login';
            }
        }

        if(isset($_GET['availability']) && !empty($_GET['availability'])){
            if($_GET['availability'] == '1'){
                $result = Auth::User()->professional_detail;
                if(isset($result) && !empty($result)){
                    $result = $result->toArray();
                    if(isset($result['availability']) && !empty($result['availability'])){
                        $now = time(); // or your date as well
                        $your_date = strtotime($result['availability']);
                        $datediff = $now - $your_date;

                        $day_diff = floor($datediff / (60 * 60 * 24));
                        if($day_diff >= 15 && $your_date < $now){
                            $availability = $result['availability'];
                            $current_rank = $result['applied_rank'];
                            $auto_action = 'availability';
                        }
                    }
                }

            }
        }
    ?>


    <div id="availability-modal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <form id='rank_availability' action="{{route('site.user.availability.change')}}" method="post">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Update Availability</h4>
                    </div>
                    <div class="modal-body">
                        <p>Please update your availability so that we can match your resume to the appropriate jobs for you.</p>
                        <div class="row">
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label class="input-label">Rank Applied For <span class="symbol required"></span>
                                    </label>
                                    <select id="applied_rank" name="applied_rank" class="form-control">
                                        <option value="">Select Your Rank</option>
                                        @foreach(\CommonHelper::new_rank() as $index => $category)
                                            <optgroup label="{{$index}}"></optgroup>
                                            @foreach($category as $r_index => $rank)
                                                <option value="{{$r_index}}" {{ !empty($current_rank) ? $current_rank == $r_index ? 'selected' : '' : ''}}>{{$rank}} </option>
                                            @endforeach
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label class="input-label">Date of Availability <span class="symbol required"></span>
                                    </label>
                                    <input type="text" class="form-control datepicker" id="date_avaibility" name="date_avaibility" value="{{ isset($availability) ? date('d-m-Y',strtotime($availability)) : ''}}" placeholder="dd-mm-yyyy">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default availability_submit_btn ladda-button" data-style="zoom-in">Save</button>
                    </div>
                </form>
            </div>

        </div>
    </div>

    <script type="text/javascript">
        another_user = '{{$another_user}}';
        auto_action = '{{$auto_action}}';
        already_login = '{{$already_login}}';
        another_user_update_availability = '{{$another_user_update_availability}}';
        job_apply = '{{$job_apply}}';
    </script>
@stop
<script type="text/javascript" src="/js/site/home.js"></script>