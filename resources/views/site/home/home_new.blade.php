@extends('site.index')


@section('content')
    <div class="home home-new">
        <div class="section_1 z-depth-1">
            <div class="lg_curved_overlay_new"></div>
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <!-- <div class="banner_text_new">
                            Where-ever you drop an anchor, we are with you ! 
                        </div> -->
                        <div class="search-option-container">
                            <div class="search-card job-search home-search-active" id="job-search">
                                <div class="arrow-up"></div>
                                <div class="img-container text-center">
                                    <img src="{{asset('images/home/job_search.png')}}" alt="">
                                </div>
                                <div class="title">
                                    Job Search
                                </div>
                            </div>
                            <div class="search-card home-course-search" id="home-course-search">
                                <div class="arrow-up"></div>
                                <div class="img-container text-center">
                                    <img src="{{asset('images/home/course_search.png')}}" alt="">
                                </div>
                                <div class="title">
                                    Course Search
                                </div>
                            </div>
                        </div>
                        <div class="banner-search-container">
                            <form id="seafarer-job-search" class="job-search-filter" method="get" action="{{route('site.seafarer.job.search')}}">
                                <div class="row search-container-row">
                                    <div class="col-xs-12 col-sm-3 back-white border-rad-l per-search">
                                        <div class="form-group"> 
                                            <label class="col-xs-4 col-sm-12 col-md-2 p-0 control-label">Rank
                                            </label> 
                                            <div class="col-xs-8 col-sm-12 col-md-10 p-0"> 
                                                <select id="rank" name="rank" class="form-control">
                                                    <option value=''>Select Rank</option>
                                                    @foreach(\CommonHelper::new_rank() as $index => $category)
                                                        <optgroup label="{{$index}}">
                                                        @foreach($category as $r_index => $rank)
                                                            <option value="{{$r_index}}" {{ !empty($job_data[0]['rank']) ? $job_data[0]['rank'] == $r_index ? 'selected' : '' : ''}}>{{$rank}} </option>
                                                        @endforeach
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-4 back-white per-search">
                                        <div class="form-group"> 
                                            <label class="col-xs-4 col-sm-12 col-md-4 p-0 control-label">Ship Type
                                            </label> 
                                            <div class="col-xs-8 col-sm-12 col-md-8 p-0"> 
                                                <select id="ship_type" name="ship_type" class="form-control">
                                                    <option value=''>Select Ship Type</option>
                                                    @foreach(\CommonHelper::ship_type() as $r_index => $ship_type)
                                                        <option value="{{$r_index}}" {{ !empty($job_data[0]['ship_type']) ? $job_data[0]['ship_type'] == $r_index ? 'selected' : '' : ''}}>{{$ship_type}} </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-3 back-white border-rad-r per-search">
                                        <div class="form-group">
                                            <label class="col-xs-4 col-sm-12 col-md-5 p-0 control-label">Work Exp</label>
                                            <div class="col-xs-8 col-sm-12 col-md-7 p-0">
                                                <!-- <input type="text" class="form-control" id="exampleInputName2" name="min_rank_exp" placeholder="Ex: 2Yrs"> -->
                                                <select id="min_rank_exp" name="min_rank_exp" class="form-control">
                                                    <option value=''>Min Exp</option>
                                                    @foreach(\CommonHelper::work_experience() as $w_index => $exp)
                                                        <option value="{{$w_index}}" {{ !empty($job_data[0]['exp']) ? $job_data[0]['exp'] == $w_index ? 'selected' : '' : ''}}>{{$exp}} </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-2 search-btn">
                                        <div class="section_cta">
                                            <button type="submit" class="btn coss-primary-btn"><i class="fa fa-search" aria-hidden="true"></i>Search</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <form id="seafarer-course-search" class="course-search-filter hide" method="get" action="{{route('site.seafarer.course.search')}}">
                                <div class="row search-container-row">
                                    <div class="col-xs-12 col-sm-4 back-white border-rad-l per-search">
                                        <div class="form-group"> 
                                            <label class="col-xs-4 col-sm-12 col-md-4 p-0 control-label">Course Type
                                            </label> 
                                            <div class="col-xs-8 col-sm-12 col-md-8 p-0"> 
                                                <select name="course_type" class="form-control course_type_filter">
                                                    <option value=''>Select Course Type</option>
                                                    @foreach(\CommonHelper::institute_course_types() as $r_index => $rank)
                                                        <option value="{{$r_index}}" {{ isset($filter['course_type']) ? $filter['course_type'] == $r_index ? 'selected' : '' : ''}}>{{$rank}} </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-3 back-white per-search">
                                        <div class="form-group"> 
                                            <label class="col-xs-4 col-sm-12 col-md-3 control-label">Name
                                            </label> 
                                            <div class="col-xs-8 col-sm-12 col-md-9 p-0"> 
                                                <select id="course_name" name="course_name" class="form-control institute_course_name">
                                                    <option value=''>Select Course Name</option>
                                                    @if(isset($course_name))
                                                        @foreach($course_name as $r_index => $rank)
                                                            <option value="{{$r_index}}" {{ isset($filter['course_name']) ? $filter['course_name'] == $r_index ? 'selected' : '' : ''}}>{{$rank}} </option>
                                                        @endforeach
                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-3 back-white border-rad-r per-search">
                                        <div class="form-group">
                                            <label class="col-xs-4 col-sm-12 col-md-4 p-0 control-label">Start Date</label>
                                            <div class="col-xs-8 col-sm-12 col-md-8 p-0">
                                                <input type="text" name="start_date" class="form-control m-y-datepicker m-t-0" value="{{isset($filter['start_date']) ? !empty($filter['start_date']) ? date('d-m-Y',strtotime('01-'.$filter['start_date'])) : '' : ''}}" placeholder="mm-yyyy">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-2">
                                        <div class="section_cta">
                                            <button type="submit" class="btn coss-primary-btn"><i class="fa fa-search" aria-hidden="true"></i>Search</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php $class = 'p-t-40'; ?>
        @if(isset($advertisements) && !empty($advertisements))
            <?php
                $advertisement_details = $advertisements->toArray();
                $class = 'p-t-0';
            ?>
            @if(isset($advertisement_details) && !empty($advertisement_details))
            <div class="section_2 section">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-md-12">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="section_heading_new">
                                        Featured Companies
                                        <span class="pull-right">
                                            <a href="{{route('site.view.company.list')}}" style="font-size: 18px;">View All</a>
                                        </span>
                                    </div>
                                    <!-- <div class="section_subHeading">
                                        <span class="is_redText">Lorem ipsum</span> dolar sit amet, his ex justo audim habemus
                                    </div> -->
                                </div>
                            </div>

                            <div class="featured-company-slick row dont-break">
                                @foreach($advertisement_details as $index => $advertisement)
                                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-2 flex_col">
                                    <div class="coss_card">
                                        <div class="coss_card-image" style="background-image:url('{{!empty($advertisement['user_details']['profile_pic']) ? "/".env('COMPANY_LOGO_PATH').$advertisement['user_id']."/".$advertisement['user_details']['profile_pic'] : 'images/user_default_image.png'}}'); ">
                                            
                                        </div>
                                        <div class="coss_card-content">
                                            <div class="coss_card-contentTitle">
                                                {{ isset($advertisement['company_name']) ? strtoupper($advertisement['company_name']) : ''}}
                                            </div>
                                            <div class="coss_card-contentDesc">

                                                @if(isset($advertisement['next_company_jobs']) && !empty($advertisement['next_company_jobs']))

                                                    <?php

                                                        $total_jobs = count($advertisement['next_company_jobs']);
                                                        $count = 5;
                                                        if($total_jobs < 5){
                                                            $count = $total_jobs;
                                                        }

                                                        // $count = 5;
                                                        $job_count = 0;
                                                    ?>
                                                    Urgently requires
                                                    @foreach($advertisement['next_company_jobs'] as $a_index => $jobs)
                                                        @foreach(\CommonHelper::new_rank() as $index => $category)
                                                            @foreach($category as $r_index => $rank)
                                                                @if(isset($jobs['rank']) && !empty($jobs['rank']))
                                                                    @if($job_count < 5)
                                                                        @if($jobs['rank'] == $r_index)
                                                                            {{$rank}}{{!($count == $a_index+1) ? ',' : '....'}}
                                                                            <?php $job_count++; ?>
                                                                        @endif
                                                                    @endif
                                                                @endif   
                                                            @endforeach
                                                        @endforeach
                                                    @endforeach
                                                @else
                                                    <div class="coss_card-no-opening">
                                                        Currently there are no active jobs.
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="coss_card-footer_new">
                                           
                                            {{ isset($advertisement['created_at']) ? date('dS M, Y', strtotime($advertisement['created_at'])) : ''}}
                                        
                                            <a href="{{route('site.show.company.details.user',$advertisement['user_id'])}}" class="coss_card-footerLink pull-right" target="_blank">Read More</a>
                                               
                                        </div>
                                    </div>
                                </div>

                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endif
        @endif

        <div class="section_2 section {{$class}}">
            <div class="container">
                <div class="row">
                    @if(isset($institutes) && !empty($institutes))
                    <div class="col-xs-12 col-sm-7 col-md-12 institute_list">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="section_heading_new">
                                    Institutes
                                </div>
                            </div>

                            <div class="col-xs-12">
                                @foreach($institutes as $institute)
                                    <div class="col-xs-6 col-sm-3 institute_logo">
                                        @if(!empty($institute['profile_pic']))
                                            <img id="preview" src="/{{ env('INSTITUTE_LOGO_PATH')}}{{$institute['id']}}/{{$institute['profile_pic']}}" height="200" width="200">
                                        @else
                                            <img id="preview" src="{{ asset('images/user_default_image.png') }}" height="200" width="200">
                                        @endif
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    @endif
                    <div class="col-xs-12 col-sm-5 col-md-6 hide" id="featured_advertise_section">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="section_heading_new">
                                    Featured Advertisements
                                    <span class="pull-right">
                                        <a href="{{route('site.view.advertisements.list')}}" style="font-size: 18px;">View All</a>
                                    </span>
                                </div>
                            </div>

                            <div class="col-xs-12 p-0">
                                <div class="ad_card_box">
                                    <div class="loader"></div>                        
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="section_3 section z-depth-1 hide">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="section_heading_new is_centerAligned">
                            Top Companies
                        </div>
                        <!-- <div class="section_subHeading is_centerAligned">
                            Lorem ipsum dolar sit amet, his ex justo audim habemus
                        </div> -->
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-12">
                        <div class="clients dont-break height-equalizer-wrapper">
                            <div class="client_slide z-depth-1 ">
                                <div class="client_slide-logo">
                                    <img src="{{asset('/images/home/featured_companies/selandia.jpg')}}">
                                </div>
                                <div class="client_slide-name height-equalizer">Selandia Crew Management(India) Pvt. Ltd.</div>
                            </div>
                            <div class="client_slide z-depth-1 ">
                                <div class="client_slide-logo">
                                    <img src="{{asset('/images/home/featured_companies/ocs.jpg')}}">
                                </div>
                                <div class="client_slide-name height-equalizer">OCS SERVICES (INDIA) PVT LTD NORTRANS</div>
                            </div>
                            <div class="client_slide z-depth-1 ">
                                <div class="client_slide-logo">
                                    <img src="{{asset('/images/home/featured_companies/darya.jpg')}}">
                                </div>
                                <div class="client_slide-name height-equalizer">Darya Shipping Solutions</div>
                            </div>
                            <div class="client_slide z-depth-1 ">
                                <div class="client_slide-logo">
                                    <img src="{{asset('/images/home/featured_companies/wallem.jpg')}}">
                                </div>
                                <div class="client_slide-name height-equalizer">Wallem Ship Management</div>
                            </div>
                            <div class="client_slide z-depth-1 ">
                                <div class="client_slide-logo">
                                    <img src="{{asset('/images/home/featured_companies/selandia.jpg')}}">
                                </div>
                                <div class="client_slide-name height-equalizer">Selandia Crew Management(India) Pvt. Ltd.</div>
                            </div>
                            <div class="client_slide z-depth-1 ">
                                <div class="client_slide-logo">
                                    <img src="{{asset('/images/home/featured_companies/darya.jpg')}}">
                                </div>
                                <div class="client_slide-name height-equalizer">Darya Shipping Solutions</div>
                            </div>

                            <div class="client_slide z-depth-1 ">
                                <div class="client_slide-logo">
                                    <img src="{{asset('/images/home/featured_companies/selandia.jpg')}}">
                                </div>
                                <div class="client_slide-name height-equalizer">Selandia Crew Management(India) Pvt. Ltd.</div>
                            </div>
                            <div class="client_slide z-depth-1 ">
                                <div class="client_slide-logo">
                                    <img src="{{asset('/images/home/featured_companies/ocs.jpg')}}">
                                </div>
                                <div class="client_slide-name height-equalizer">OCS SERVICES (INDIA) PVT LTD NORTRANS</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="section_4 section hide">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="section_heading_new is_centerAligned">
                            Maritime Traning Centeres
                        </div>
                        <!-- <div class="section_subHeading is_centerAligned">
                            Lorem ipsum dolar sit amet, his ex justo audim habemus
                        </div> -->
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-12 col-md-10 col-md-offset-1">
                        <div class="row">
                            <div class="col-xs-12 col-sm-6 col-md-3">
                                <div class="training_center-logo center_1"></div>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-3">
                                <div class="training_center-logo center_2"></div>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-3">
                                <div class="training_center-logo center_1"></div>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-3">
                                <div class="training_center-logo center_2"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- <div class="row">
                    <div class="col-xs-12">
                        <div class="section_cta text-center m_t_25">
                            <a href="" class="btn coss-primary-btn">Know More</a>
                        </div>
                    </div>
                </div> -->
            </div>
        </div>
        <div class="section_6 section_new">
            <div class="banner_overlay"></div>
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="section_heading_new is_centerAligned">
                            How it Works?
                        </div>
                        <!-- <div class="section_subHeading is_centerAligned">
                            <span class="is_redText">Lorem ipsum</span> dolar sit amet, his ex justo audim habemus
                        </div> -->
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-12 col-md-4">
                        <div class="howItWorks_block text-center">
                            <div class="howItWorks_block-icon">
                                <img src="{{ asset('/images/home/how_it_works/icons/find.png') }}">
                            </div>
                            <div class="howItWorks_block-title">
                                Find your dream company
                            </div>
                            <!-- <div class="howItWorks_block-desc">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ullam recusandae atque cum. Consequuntur explicabo 
                            </div> -->
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-4">
                        <div class="howItWorks_block text-center">
                            <div class="howItWorks_block-icon">
                                <img src="{{ asset('/images/home/how_it_works/icons/connected.png') }}">
                            </div>
                            <div class="howItWorks_block-title">
                                Get connected with recruiters
                            </div>
                            <!-- <div class="howItWorks_block-desc">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ullam recusandae atque cum. Consequuntur explicabo 
                            </div> -->
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-4">
                        <div class="howItWorks_block text-center no_beforeElement">
                            <div class="howItWorks_block-icon">
                                <img src="{{ asset('/images/home/how_it_works/icons/search.png') }}">
                            </div>
                            <div class="howItWorks_block-title">
                                Search jobs on the go
                            </div>
                            <!-- <div class="howItWorks_block-desc">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ullam recusandae atque cum. Consequuntur explicabo 
                            </div> -->
                        </div>
                    </div>
                </div>

                <!-- <div class="row">
                    <div class="col-xs-12">
                        <div class="section_cta m_t_25 text-center">
                            <a class="btn coss-primary-btn pointer" data-toggle="modal" data-target="#contact-us-modal">Contact Us</a>
                        </div>
                    </div>
                </div> -->
            </div>
        </div>
        <div class="section_5 section z-depth-1">
            <div class="lg_curved_overlay"></div>
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="section_heading_new">
                            Services Offered
                        </div>
                        <!-- <div class="section_subHeading">
                            <span class="is_redText">Lorem ipsum</span> dolar sit amet, his ex justo audim habemus
                        </div> -->
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-12 col-md-10">
                        <div class="row">
                            <div class="col-xs-12 col-sm-3 col-md-3">
                                <div class="service_miniCard">
                                    <a href="{{route('services')}}">
                                        <div class="service_miniCard-icon smb"></div>
                                        <div class="service_miniCard-title">
                                            Sea Man Book
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-3 col-md-3">
                                <div class="service_miniCard">
                                    <a href="{{route('services',['tab' => 'coc'])}}">
                                        <div class="service_miniCard-icon coc"></div>
                                        <div class="service_miniCard-title">
                                            COC (Certificate of Competency)
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-3 col-md-3">
                                <div class="service_miniCard">
                                    <a href="{{route('services',['tab' => 'watchkeeping'])}}">
                                        <div class="service_miniCard-icon wtk"></div>
                                        <div class="service_miniCard-title">
                                            Watch Keeping
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-3 col-md-3">
                                <div class="service_miniCard">
                                    <a href="{{route('services',['tab' => 'endorsement'])}}">
                                        <div class="service_miniCard-icon end"></div>
                                        <div class="service_miniCard-title">
                                            Endorsements
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- <div class="row">
                    <div class="col-xs-12">
                        <div class="section_cta m_t_25">
                            <a href="" class="btn coss-primary-btn">Know More</a>
                        </div>
                    </div>
                </div> -->
            </div>
        </div>

        

        <div class="section_7 section hide">
            <div class="banner_overlay"></div>
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="section_heading_new is_centerAligned">
                            Testimonials
                        </div>
                        <!-- <div class="section_subHeading is_centerAligned">
                            Lorem ipsum dolar sit amet, his ex justo audim habemus
                        </div> -->
                    </div>
                </div>

                <div class="row testimonials">
                    <div class="col s12 m4 testimonials-slide">
                        <div class="testimonials_card">
                            <div class="testimonials_card-image z-depth-1">
                                <img src="{{asset('images/home/testimonials/client_1.jpg')}}">
                            </div>
                            <div class="testimonials_card-content">
                                The SquashFit Kit is a great tool for anyone keen to improve their Squash game. The drills are very squash focused
                                and are similar to thoseused by Squash professionals (such as myself) around the world.
                            </div>

                            <div class="testimonials_card-footer">
                                <div class="client-name">Cameroon Pilley</div>
                                <div class="client-designation">Professional Squash Player - World Ranking #14</div>
                            </div>
                        </div>
                    </div>
                    <div class="col s12 m4 testimonials-slide">
                        <div class="testimonials_card">
                            <div class="testimonials_card-image z-depth-1">
                                <img src="{{asset('images/home/testimonials/client_2.jpg')}}">
                            </div>
                            <div class="testimonials_card-content">
                                The SquashFit Kit is a great tool for anyone keen to improve their Squash game. The drills are very squash focused
                                and are similar to thoseused by Squash professionals (such as myself) around the world.
                            </div>

                            <div class="testimonials_card-footer">
                                <div class="client-name">John Doe</div>
                                <div class="client-designation">Professional Squash Player - World Ranking #14</div>
                            </div>
                        </div>
                    </div>
                    <div class="col s12 m4 testimonials-slide">
                        <div class="testimonials_card">
                            <div class="testimonials_card-image z-depth-1">
                                <img src="{{asset('images/home/testimonials/client_1.jpg')}}">
                            </div>
                            <div class="testimonials_card-content">
                                The SquashFit Kit is a great tool for anyone keen to improve their Squash game. The drills are very squash focused
                                and are similar to thoseused by Squash professionals (such as myself) around the world.
                            </div>

                            <div class="testimonials_card-footer">
                                <div class="client-name">A M Chavan</div>
                                <div class="client-designation">Officer Engineering Services</div>
                            </div>
                        </div>
                    </div>
                    <div class="col s12 m4 testimonials-slide">
                        <div class="testimonials_card">
                            <div class="testimonials_card-image z-depth-1">
                                <img src="{{asset('images/home/testimonials/client_2.jpg')}}">
                            </div>
                            <div class="testimonials_card-content">
                                The SquashFit Kit is a great tool for anyone keen to improve their Squash game. The drills are very squash focused
                                and are similar to thoseused by Squash professionals (such as myself) around the world.
                            </div>

                            <div class="testimonials_card-footer">
                                <div class="client-name">A M Chavan</div>
                                <div class="client-designation">Officer Engineering Services</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="section_8 section bg-white">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="section_heading_new is_centerAligned">
                            Reach Us
                        </div>
                        <div class="section_subHeading is_centerAligned">
                            <span class="is_redText">Let discuss</span> this further over a coffee, what say?
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-12 col-md-8 col-md-offset-2">
                        <form class="reachUsForm" id="reach-us-modal-form" action="{{ route('site.contact.us') }}">
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="col-xs-12 col-md-5">
                                    <div class="form-group">
                                        <span class="form_icon">
                                            <img src="{{ asset('/images/home/reach_us/user.png') }}">
                                        </span>
                                        <input type="text" class="form-control" id="yourName" name="name" placeholder="Your Name">
                                    </div>
                                    <label for="yourName" class="p-l-45 error"></label>
                                </div>
                                <div class="col-xs-12 col-md-5 col-md-offset-2">
                                    <div class="form-group">
                                        <span class="form_icon">
                                            <img src="{{ asset('/images/home/reach_us/phone.png') }}">
                                        </span>
                                        <input type="text" class="form-control" id="yourNumber" name="phone" placeholder="Your Number">
                                    </div>
                                    <label for="yourNumber" class="p-l-45 error"></label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-5">
                                    <div class="form-group">
                                        <span class="form_icon">
                                            <img src="{{ asset('/images/home/reach_us/envelope.png') }}">
                                        </span>
                                        <input type="text" class="form-control" id="yourEmail" name="email" placeholder="Your Email">
                                    </div>
                                    <label for="yourEmail" class="p-l-45 error"></label>
                                </div>
                                <div class="col-xs-12 col-md-5 col-md-offset-2">
                                    <div class="form-group">
                                        <span class="form_icon">
                                            <img src="{{ asset('/images/home/reach_us/comment.png') }}">
                                        </span>
                                        <textarea class="form-control" id="yourMessage" name="message" rows="1" placeholder="Your Message"></textarea>
                                    </div>
                                    <label for="yourMessage" class="p-l-45 error"></label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-4 col-md-offset-4">
                                    <div id="recaptcha2" name="recaptcha2"></div>
                                    <label for="yourMessage" class="recaptcha2-error error"></label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="section_cta m_t_25 text-center">
                                        <button type="button" data-style="zoom-in" class="ladda-button btn coss-primary-btn btn-default contact-modal-btn" id="reach-us-submit-button" placeholder="Contact Us">Contact Us</button>
                                    </div>
                                </div>
                            </div>
                        </form>                        
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div id="enquire-advertisements-modal" class="modal extended-modal fade no-display" tabindex="-1" data-width="760">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="advertise-enquiry-form-container">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                          &times;
                        </button>
                        <div class="heading"> 
                            <h4 class="modal-title input-label">Advertisement Enquiry</h4>
                        </div>
                    </div>
                    <div class="modal-body enquire-advertisements-container">

                        <form id="advertisements-enquiry-modal-form" action="{{ route('site.advertisements.enquiry') }}">
                            {{csrf_field()}}
                            <input type="hidden" name="advertisement_id" id="advertisement_id" value="">
                            <input type="hidden" name="company_id" id="company_id" value="">
                            <div class="form-group">
                                <label class="input-label" for="name">Name</label>
                                <input type="text" class="form-control" id="name" name="name" value="{{isset($data['first_name']) ? $data['first_name'] : ''}} ">
                            </div>
                            <div class="form-group">
                                <label class="input-label" for="name">Phone Number</label>
                                <input type="text" class="form-control" id="phone" name="phone" value="{{isset($data['mobile'])?$data['mobile']:''}}">
                            </div>
                            <div class="form-group">
                                <label class="input-label" for="name">Email</label>
                                <input type="email" class="form-control" id="email" name="email" value="{{isset($data['email']) ? $data['email'] : ''}} ">
                            </div>
                            <div class="form-group">
                                <label class="input-label" for="name">Message</label>
                                <textarea class="form-control" rows="6" name="message" id="message"></textarea>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <div class="row right">
                            <div class="col-sm-12">
                                <button type="button" data-style="zoom-in" class="btn btn-blue btn-default ladda-button" id="send-advertise-enquiry-button">
                                    Send Enquiry
                                </button>   
                            </div>
                        </div>  
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="welcome-modal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Welcome {{ Auth::check() ? ucfirst(Auth::user()->first_name) : ''}}</h4>
              </div>
              <div class="modal-body">
                <p>Thank you for registering with us.</p>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>
            </div>

        </div>
    </div>

    <div id="email-verification-modal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Welcome {{ Auth::check() ? ucfirst(Auth::user()->first_name) : ''}}</h4>
              </div>
              <div class="modal-body">
                <p>Your email has been verified.</p>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>
            </div>

        </div>
    </div>

    <div id="reset-password-modal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Welcome {{ Auth::check() ? ucfirst(Auth::user()->first_name) : ''}}</h4>
              </div>
              <div class="modal-body">
                <p>Your password has been changed successfully.</p>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>
            </div>

        </div>
    </div>

    <div id="another-user-modal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Email Verification</h4>
                  </div>
                  <div class="modal-body">
                    <p>The email you are trying to verify does not match with your current account email address. Please <a href="{{route('logout')}}">logout</a> and try again</p>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  </div>
            </div>

        </div>
    </div>

    <div id="already-login-modal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Reset Password</h4>
                  </div>
                  <div class="modal-body">
                    <p>You are already login. Please <a href="{{route('logout')}}">logout</a> and try again</p>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  </div>
            </div>

        </div>
    </div>

    <div id="another-user-update-availabilty" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Email Verification</h4>
                  </div>
                  <div class="modal-body">
                    <p>The email with which you are trying to update your availability does not match with your current account email address. Please <a href="{{route('logout')}}">logout</a> and try again</p>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  </div>
            </div>

        </div>
    </div>

    <div id="job_apply" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Job Application</h4>
                  </div>
                  <div class="modal-body">
                    <p>Please <a href="{{route('logout')}}">logout</a> and apply to this job again.</p>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Ok</button>
                  </div>
            </div>

        </div>
    </div>
    
    <?php 
        $another_user = false;
        $another_user_update_availability = false;
        $auto_action = '';
        $job_apply = '';
        $already_login = '';
        
        if(isset($_GET['email_verification']) && !empty($_GET['email_verification'])){
            if( $_GET['email_verification'] == 'another_user'){
                $another_user = true;
            }
        }

        if(isset($_GET['update_availabilty']) && !empty($_GET['update_availabilty'])){
            if( $_GET['update_availabilty'] == 'another_user'){
                $another_user_update_availability = true;
            }
        }

        if(isset($_GET['auto_action']) && !empty($_GET['auto_action'])){
            if( $_GET['auto_action'] == 'verified'){
                $auto_action = 'verified';
            }
            if( $_GET['auto_action'] == 'reset_password'){
                $auto_action = 'reset_password';
            }
        }

        if(isset($_GET['auto_action']) && !empty($_GET['auto_action'])){
            if( $_GET['auto_action'] == 'welcome'){
                $auto_action = 'welcome';
            }
        }

        if(isset($_GET['job_apply']) && !empty($_GET['job_apply'])){
            if( $_GET['job_apply'] == 'another_user'){
                $job_apply = 'another_user';
            }
        }

        if(isset($_GET['already_login']) && !empty($_GET['already_login'])){
            if( $_GET['already_login'] == '1'){
                $already_login = 'already_login';
            }
        }

        if(isset($_GET['availability']) && !empty($_GET['availability'])){
            if($_GET['availability'] == '1'){
                $result = Auth::User()->professional_detail;
                if(isset($result) && !empty($result)){
                    $result = $result->toArray();
                    if(isset($result['availability']) && !empty($result['availability'])){
                        $now = time(); // or your date as well
                        $your_date = strtotime($result['availability']);
                        $datediff = $now - $your_date;

                        $day_diff = floor($datediff / (60 * 60 * 24));
                        if($day_diff >= 15 && $your_date < $now){
                            $availability = $result['availability'];
                            $current_rank = $result['applied_rank'];
                            $auto_action = 'availability';
                        }
                    }
                }

            }
        }
    ?>


    <div id="availability-modal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <form id='rank_availability' action="{{route('site.user.availability.change')}}" method="post">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Update Availability</h4>
                    </div>
                    <div class="modal-body">
                        <p>Please update your availability so that we can match your resume to the appropriate jobs for you.</p>
                        <div class="row">
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label class="input-label">Rank Applied For <span class="symbol required"></span>
                                    </label>
                                    <select id="applied_rank" name="applied_rank" class="form-control">
                                        <option value="">Select Your Rank</option>
                                        @foreach(\CommonHelper::new_rank() as $index => $category)
                                            <optgroup label="{{$index}}"></optgroup>
                                            @foreach($category as $r_index => $rank)
                                                <option value="{{$r_index}}" {{ !empty($current_rank) ? $current_rank == $r_index ? 'selected' : '' : ''}}>{{$rank}} </option>
                                            @endforeach
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label class="input-label">Date of Availability <span class="symbol required"></span>
                                    </label>
                                    <input type="text" class="form-control datepicker" id="date_avaibility" name="date_avaibility" value="{{ isset($availability) ? date('d-m-Y',strtotime($availability)) : ''}}" placeholder="dd-mm-yyyy">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default availability_submit_btn ladda-button" data-style="zoom-in">Save</button>
                    </div>
                </form>
            </div>

        </div>
    </div>

    <script type="text/javascript">
        another_user = '{{$another_user}}';
        auto_action = '{{$auto_action}}';
        already_login = '{{$already_login}}';
        another_user_update_availability = '{{$another_user_update_availability}}';
        job_apply = '{{$job_apply}}';
    </script>

    <!-- <script type="text/javascript" src="/js/site/home.js"></script> -->
@stop