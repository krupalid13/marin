@extends('site.home')

@section('content')
    <section class="banner_main">
        <div class="container">
            <div class="log_reg_block login-div collapse">
                <div class="tab-content">
                    <div class="tab-pane active" id="login-tab">
                        <div class="form-block">
                            <form method="POST" action="{{ route('site.login.submit') }}">
                                {{ csrf_field() }}
                                <input class="form-control" name="email" autofocus type="text" placeholder="Enter Email">
                                <input class="form-control" name="password" type="password" placeholder="Enter Password">
                                @if (session('error'))
                                    <label id="login_error" class="error">{{ session('error') }}</label>
                                @endif
                                <div class="box">
                                    <!--<label><input type="checkbox" name="remember" id="remember"> Remeber Me</label>-->
                                    <a class="link" href="{{ route('site.reset.password.view') }}">Forgot Password?</a>
                                </div>
                                <input class="submit" type="submit" value="Sign In">
                            </form>
                        </div>
                    </div>
                    <div class="tab-pane" id="register-tab">
                        <form id="preRegistrationFormModalTop" method="post" action="">
                            <div class="form-block">
                                {{ csrf_field() }}
                                <input class="form-control" type="email" id="user_email" name="email"
                                    placeholder="Email Address">
                                <input class="form-control" type="password" id="o_password" name="password"
                                    placeholder="Password">
                                <input class="form-control" type="password" name="cpassword" id="c_password"
                                    placeholder="Confirm Password">
                                <input class="form-control" type="text" name="username" id="username"
                                    placeholder="Username">
                                <input class="form-control" type="text" name="dob" placeholder="Date of birth">
                                <input class="form-control" type="text" name="mobile" id="mobile"
                                    placeholder="Mobile number">

                                <select id='nationality' name="nationality" class="form-control" id="nationality">
                                    <option value=''>Select Your Nationality</option>
                                    @foreach (\CommonHelper::countries() as $c_index => $country)
                                        <option value="{{ $c_index }}"> {{ $country }}</option>
                                    @endforeach
                                </select>
                                <select id="current_rank" name="current_rank" class="form-control">
                                    <option value="">Select Your Rank</option>
                                    @foreach (\CommonHelper::new_rank() as $index => $category)
                                        <optgroup label="{{ $index }}">
                                            @foreach ($category as $r_index => $rank)
                                                <option value="{{ $r_index }}">{{ $rank }} </option>
                                            @endforeach
                                    @endforeach
                                </select>
                                <input class="form-control" type="text" name="passport" id="passport"
                                    placeholder="Passport number">
                                <div class="box">
                                    <label><input type="checkbox"> I accept the <a href="{{ route('tnc') }}">Terms and
                                            Conditions.</a></label>
                                </div>
                                <input class="submit" type="submit" value="Register">
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <div class="cruise_block">
                <img src="{{ asset('public/images/homepage/banner/cruise-img.jpg') }}" alt="">
                <div class="profile-box">
                    <div class="block-1">
                        <div class="avtaar">
                            <a href="#"><img src="{{ asset('public/images/homepage/banner/crew-img1.PNG') }}" alt=""></a>
                        </div>
                    </div>
                    <div class="block-2">
                        <div class="avtaar">
                            <a href="#"><img src="{{ asset('public/images/homepage/banner/crew-img2.PNG') }}" alt=""></a>
                        </div>
                    </div>
                    <div class="block-3">
                        <div class="avtaar">
                            <a href="#"><img src="{{ asset('public/images/homepage/banner/crew-img3.PNG') }}" alt=""></a>
                        </div>
                    </div>
                </div>
                <div class="profile-box1">
                    <div class="block-4">
                        <div class="avtaar">
                            <a href="#"><img src="{{ asset('public/images/homepage/banner/crew-img4.PNG') }}" alt=""></a>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </section>

    <div class="slideshow">
        <div class="sec_title">
            <br><br>
            <h3>Register With Flanknot And Get Access To</h3>
        </div>
        <div class="slideshow-item">
            <img src="{{ asset('public/images/homepage/slider/profile-page.png') }}" alt="Informative User Profile Page">
            <div class="slideshow-item-text">
                <h5 class="slider-image-title">Informative Profile</h5>
                <p class="slider-image-text">A dedicated page for seafarers. </p>
                <p class="slider-image-text">display professional growth and achievements.</p>
            </div>
        </div>

        <div class="slideshow-item">
            <img src="{{ asset('public/images/homepage/slider/resumedynamic.png') }}" alt="Dynamic Resume">
            <div class="slideshow-item-text">
                <h5 class="slider-image-title">Dynamic Resume</h5>
                <p class="slider-image-text">Attractive layouts, easy to create & maintain.</p>
                <p class="slider-image-text">Forward your Resume with ease.</p>
            </div>
        </div>

        <div class="slideshow-item">
            <img src="{{ asset('public/images/homepage/slider/documents-organised.png') }}" alt="Store your Documents">
            <div class="slideshow-item-text">
                <h5 class="slider-image-title">Documents Organised</h5>
                <p class="slider-image-text">Secure and organised storage for all your documents and certificates.</p>
                <p class="slider-image-text">Easy to access and easy to share.</p>

            </div>
        </div>

        <div class="slideshow-item">
            <img src="{{ asset('public/images/homepage/slider/infographics.png') }}"
                alt="Graph representing career growth">
            <div class="slideshow-item-text">
                <h5 class="slider-image-title">Career Infographics</h5>
                <p class="slider-image-text">Express your skills & experience as charts and graphs.</p>
                <p class="slider-image-text">Easy to analyse and Interperate.</p>
            </div>
        </div>

    </div>

    <section class="sec_intrests">
        <div class="container">

            <div class="sec_title">
                <h3>Overview Your Interests</h3>
            </div>
            <div class="row">
                <div class="col-md-4 col-sm-6">
                    <div class="img-block"><img src="{{ asset('public/images/homepage/interest/intrests-img1.jpg') }}"
                            alt=""></div>
                    <div class="text-block">
                        <h4>Share your Portfolio</h4>
                        <p>No need to carry Documents at all times. Share your Resume, Doc or Profile on the go. Maintain
                            Sharing History.</p>
                    </div>
                </div>
                <!-- Col end -->
                <div class="col-md-4 col-sm-6">
                    <div class="img-block"><img src="{{ asset('public/images/homepage/interest/intrests-img2.jpg') }}"
                            alt=""></div>
                    <div class="text-block">
                        <h4>Quick Response</h4>
                        <p>Get immediate replies from recruiters that view your Resume. Track updates. Get reminders for
                            follow ups.</p>
                    </div>
                </div>
                <!-- Col end -->
                <div class="col-md-4 col-sm-6">
                    <div class="img-block"><img src="{{ asset('public/images/homepage/interest/intrests-img3.jpg') }}"
                            alt=""></div>
                    <div class="text-block">
                        <h4>Assessment & Review</h4>
                        <p>Describe your onboard experience. Get Assessment form your company. Evaluate & Rate your service.
                        </p>
                    </div>
                </div>
                <!-- Col end -->
                <div class="col-md-4 col-sm-6">
                    <div class="img-block"><img src="{{ asset('public/images/homepage/interest/intrests-img4.jpg') }}"
                            alt=""></div>
                    <div class="text-block">
                        <h4>Notifications & Remainders</h4>
                        <p>Know when your Resume was Viewed. Be Notified On Jobs that match your Profile. Find the lowest
                            price for the courses around you.</p>
                    </div>
                </div>
                <!-- Col end -->
                <div class="col-md-4 col-sm-6">
                    <div class="img-block"><img src="{{ asset('public/images/homepage/interest/intrests-img5.jpg') }}"
                            alt=""></div>
                    <div class="text-block">
                        <h4>Build Your Community</h4>
                        <p>Connect with your onboard colleagues. Get Job Referrals from friends. Save Contacts for Companies
                        </p>
                    </div>
                </div>
                <!-- Col end -->
                <div class="col-md-4 col-sm-6">
                    <div class="img-block"><img src="{{ asset('public/images/homepage/interest/intrests-img6.jpg') }}"
                            alt=""></div>
                    <div class="text-block">
                        <h4>Recruiters are Watching</h4>
                        <p>Attract them with your Resume & Hook them your Profile. Rank targeted job Notifications. We find
                            Job requirements across the Globe.</p>
                    </div>
                </div>
                <!-- Col end -->
            </div>
        </div>
    </section>

    <section class="sec_network">
        <div class="container">
            <br><br>
            <div class="sec_title">
                <h3>We're Socially Active</h3>
            </div>
            <div class="network_block">
                <h3>Our Network</h3>
                <div class="row">
                    <div class="col-lg-3 col-sm-6">
                        <span class="icon"><img
                                src="{{ asset('public/images/homepage/our-network/people-icon.png') }}"></span>
                        <p><span></span> Seafarers Community</p>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <span class="icon"><img
                                src="{{ asset('public/images/homepage/our-network/globe-icon.png') }}"></span>
                        <p><span></span> Global Coverage</p>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <span class="icon"><img
                                src="{{ asset('public/images/homepage/our-network/recruite-icon.png') }}"></span>
                        <p><span></span> Job Recruiters</p>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <span class="icon"><img
                                src="{{ asset('public/images/homepage/our-network/server-icon.png') }}"></span>
                        <p><span></span> Free Storage </p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="sec_enquiry">
        <div class="container">
            <br>
            <div class="sec_title">
                <h3>Enquiry for</h3>
            </div>
            <div class="row">
                <div class="card-box">
                    <div class="circle"><a href="{{ route('company-inquiry') }}"><img
                                src="{{ asset('public/images/homepage/enquiry/enquiry-img1.jpg') }}" alt=""></a></div>
                    <h6><a href="{{ route('company-inquiry') }}"> Maritime Companies</a></h6>
                    <!-- <a class="button" href="./company-enquiry.html">Contact Us</a> -->
                </div>
                <div class="card-box">
                    <div class="circle"><a href="{{ route('institutes-inquiry') }}"><img
                                src="{{ asset('public/images/homepage/enquiry/enquiry-img2.jpg') }}" alt=""></a></div>
                    <h6><a href="{{ route('institutes-inquiry') }}">Maritime Institutes</a></h6>
                </div>
            </div>
        </div>
    </section>
    <style>
        .error {
            color: #e2412d;
            font-size: 14px;
            display: inline-block;
            max-width: 100%;
            margin-bottom: 5px;
            font-weight: bold;
        }

    </style>
@stop
