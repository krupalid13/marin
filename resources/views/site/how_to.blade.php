@extends('site.cms-index')
@section('content')
<meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0" />
<link rel="stylesheet" href="{{ URL:: asset('public/how_to/css/know_more.css')}}" type="text/css" media="all">
<link rel="stylesheet" href="{{ URL:: asset('public/how_to/css/responsive.css')}}" type="text/css" media="all">
<link rel="stylesheet" type="text/css" href="{{ URL:: asset('public/how_to/css/bootstrap.min.css')}}" />
<link rel="stylesheet" type="text/css" href="{{ URL:: asset('public/how_to/css/easy-responsive-tabs.css')}}" />
<link rel="stylesheet" type="text/css" href="{{ URL:: asset('public/how_to/css/all.css')}}" />
<link rel="stylesheet" type="text/css" href="{{ URL:: asset('public/how_to/css/font-awesome.min.css')}}" />
<link rel="shortcut icon" type="image/x-icon" href="{{asset('public/images/favicon.ico')}}">
<!-- content -->
<section id="content" style="display: flex !important;margin: 16px;">
    <div class="container16 container17">
        <h2 class="resp-accordion" style="text-align: center;">How To ?</h2>
        <div class="resp-tabs-container hor_1">
            <div>
                <p>
                    <!--vertical Tabs-->

                <div id="ChildVerticalTab_1">
                    <ul class="resp-tabs-list ver_1">
                        <h2 style="font-style: normal;">How To ?</h2>
                        <li>Edit Profile</li>
                        <li>Upload Documents</li>
                        <li>View  Resume</li>
                        <li>View Documents</li>
                        <li>Download Documents</li>
                        <li>Apply for Jobs</li>
                        <li>Save Contacts</li>
                        <li>Share Resume</li>
                        <li>Share Documents</li>
                        <li>Share Profile</li>
                        <li>Check Share History</li>
                    </ul>
                    <div class="resp-tabs-container ver_1">

                        <div>
                            <div class="edit_main">
                                <b>Where to Edit Profile and Create your Resume?</b><br><br>
                                <div class="edit_left">
                                    <img src="{{URL::to('public/how_to/images/edit-left.jpg')}}" class="image_full">
                                    <img src="{{URL::to('public/how_to/images/edit-profile-menu-resp.png')}}" class="image_mobile_two">

                                </div>
                                <div class="edit_right">
                                    <div class="get-started">Create Your Profile and Resume through 
                                        <i class="fa fa-edit"></i><a href="#">Edit Profile</a></div>.
                                    <div class="menu-option">
                                        <span class="menu-left"><img src="{{URL::to('public/how_to/images/res-menu.jpg')}}"></span>
                                        <span class="menu-right">If using hand held devices, you will find the 
                                            <i class="fa fa-edit"></i><a href="#">Edit Profile</a> option on clicking the menu icon.</span>
                                    </div>
                                </div>
                            </div>
                            <div class="edit_main_two">
                                <b>How to Edit Profile and Resume?</b><br><br>
                                <div class="edit_left">
                                    <img src="{{URL::to('public/how_to/images/personal-img.jpg')}}" class="image_full">
                                    <img src="{{URL::to('public/how_to/images/personal-resp.jpg')}}" class="image_mobile">
                                    <img src="{{URL::to('public/how_to/images/edit-profile-side-menu-resp-mob.png')}}" class="image_mobile_one">

                                </div>

                                <div class="edit_right">
                                    <div class="get-started">Enter Your Personal, Document, Course, Sea Service, General details.<br><br>
                                        These details will be reflected in your Profile & Resume.<br><br>

                                        The data entered in Personal, Document, Course, Sea Service & General
                                        sectons will create containers in the Upload section where you can upload all relevant
                                        documents and certificates.<br><br>

                                        The data and uploaded documents can be shared via email through 
                                        Share Documents in Share Details. <br><br>

                                        ID and Banking details entered in the General section will not be
                                        visible in either your resume or profile. They are PRIVATE but can be shared by you if required.<br><br>

                                        <i class="fa fa-ship"></i> Sea Service entered will be depicted in form of your<br>
                                        <a href=""><i class="fa fa-ship"></i>Skill Infographics</a>.<br><br>

                                        You can Review and Rate your <i class="fa fa-ship"></i> Sea Service through <br> 
                                        <a href=""><i class="fa fa-ship"></i>Onboard Feedback</a>.</div><br><br>

                                </div>
                            </div>
                        </div>
                        <div>
                            <div class="edit_main_three">
                                <b>Where to Upload the Documents?</b><br><br>
                                <div class="edit_left_one">
                                    <img src="{{URL::to('public/how_to/images/upload-documents1.png')}}" class="image_full">
                                    <img src="{{URL::to('public/how_to/images/upload-document -rasponsive.png')}}" class="image_mobile">
                                    <img src="{{URL::to('public/how_to/images/upload-document-side-menu-resp-mob.png')}}" class="image_mobile_one">

                                </div>
                                <div class="edit_right_one">
                                    <div class="hl-call">
                                        <img src="{{URL::to('public/how_to/images/documents-smaller.png')}}" class="image_full">
                                        <img src="{{URL::to('public/how_to/images/upload-documents-container.png')}}" class="image_mobile">
                                        <img src="{{URL::to('public/how_to/images/upload-documents-container.png')}}" class="image_mobile_one">
                                    </div>
                                    <div class="get-started">
                                        Dynamic storage containers created as per your input helps you store your documents online 
                                        in an organised manner through <i class="fa fa-cloud-upload"></i><a href="#">Upload Documents</a>.
                                        <div class="user-bar">
                                            <img src="{{URL::to('public/how_to/images/document-storage-bar.png')}}" />
                                        </div><br>
                                        The Storage bar indicates the storage capacity used. <br> 250mb storage is available for FREE.
                                    </div>
                                </div>
                            </div>
                            <div class="edit_main_three">
                                <b>How to Upload and mangae the visibility of Documents?</b> <br><br>
                                <div class="edit_left_one">
                                    <img src="{{URL::to('public/how_to/images/upload-image5.jpg')}}" class="image_full">
                                    <img src="{{URL::to('public/how_to/images/upload-image5.jpg')}}" class="image_mobile_two">
                                </div>
                                <div class="edit_right_one">

                                    <div class="get-started">
                                        Each Uploaded Document should not be greater than than 5mb. <br>
                                        .jpg and .png type Documents can only be uploaded.
                                        <br>
                                        <img src="{{URL::to('public/how_to/images/upload-image6.jpg')}}">

                                        <br>
                                        <br>
                                        Upload your Documents under the correct label in their repective containers.
                                        <br>
                                        <br>
                                        <img src="{{URL::to('public/how_to/images/upload-document-contract-article.png')}}">
                                        <br>
                                        For every Sea Service you can also upload its respective Contact and Aricle <br> 
                                        (Article upload option availale for Indian flag vessels only).
                                        <br>
                                        <br>
                                        <br>
                                        These Documents can be download at <a href=""><i class="fa fa-ship"></i>Download Documents</a>.
                                        <br>
                                        <br>
                                        <img src="{{URL::to('public/how_to/images/private-public1.png')}}">
                                        <br>
                                        You can set your Upload to Private to restrict its visibility in <a href="#">My Documents</a>.
                                        <br>
                                        <br>
                                        <img src="{{URL::to('public/how_to/images/public-private.png')}}">
                                        <br>
                                        Only Documents set as Public will be visible to all visitors on your 
                                        <a href="#">My Documents</a>.

                                        <!-- Only Documents set as Public will be visible to all visitors on your 
                                        <a href="#">Profile</a> / <a href="#">My Documents</a> page. -->
                                        <br>
                                        <br>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div>

                            <div class="full-docoment">
                                <b>Where to view the Resume?</b> <br>
                                <div class="depend-p">
                                    <br>
                                    View your Resume by clicking on <a href="#"><i class="fa fa-ship"></i>Chekout My resume</a>.
                                    <br>
                                    <img src="{{URL::to('public/how_to/images/checkout-button.png')}}">
                                    <br>

                                    <br>
                                    The Resume format and styling depends on your Rank.
                                    <br>
                                    <br>

                                </div>
                                <div class="pic-88l">
                                    <img src="{{URL::to('public/how_to/images/dynamic-st.png')}}">
                                </div>
                                <div class="pic-88l">
                                    <img src="{{URL::to('public/how_to/images/dynamic-st2.png')}}">
                                </div>
                                <div class="pic-88l">
                                    <img src="{{URL::to('public/how_to/images/dynamic-st3.png')}}">
                                </div>
                            </div>
                            <div class="full-docoment">
                                <div class="depend-p">
                                    <b>How is the Resume created?</b><br><br>
                                    The data on your resume can be updated through <a href="#"><i class="fa fa-ship"></i>Edit Profile</a>.
                                    <br>
                                    <br>
                                    All your Important data is distrbuted in sections and organised systematically for best visibility.
                                    <br>
                                    <br>

                                </div>

                                <br>
                                <br>



                                <div class="personal-st" >
                                    <img src="{{URL::to('public/how_to/images/personal-777.jpg')}}" class="image_full">
                                    <img src="{{URL::to('public/how_to/images/personal-responsive.jpg')}}" class="image_mobile">
                                    <img src="{{URL::to('public/how_to/images/personal-responsive.jpg')}}" class="image_mobile_one">
                                </div>
                                <br>
                                <br>
                                <br>
                                <br>
                                <div style="clear:both"></div>
                                <br>

                                To Download Resume, Click.
                                <br>
                                <img src="{{URL::to('public/how_to/images/dynamic-resume-download-button.png')}}">
                                <br>
                                <br>
                                This will take time as data is collected and a PDF file is generated.
                                <br>
                                <img src="{{URL::to('public/how_to/images/downloading-please-wait-button.png')}}" class="extra-pic">

                                <!-- To Download Resume, Click.
                                <br>
                                <img src="public/how_to/images/download-5.png">
                                <img src="public/how_to/images/dynamic-resume-download-button.png" class="image_mobile">
                                <img src="public/how_to/images/dynamic-resume-download-button.png" class="image_mobile_one">
                                <br>
                                <br>
                                You will have to wait for some time while your data is collected and a PDF file is generated.
                                <br>
                                <img src="public/how_to/images/wait-button.png" class="extra-pic">
                                <img src="public/how_to/images/downloading-please-wait-button.png" class="image_mobile">
                                <img src="public/how_to/images/downloading-please-wait-button.png" class="image_mobile_one"> -->
                                <br>
                                <br>
                            </div>

                            <div class="full-docoment">
                                <div class="depend-p">
                                    <b>How a Quick Response 'QR' is given on a Shared Resume?</b>
                                    <br>
                                    <br>
                                    <span>(Keep this marked when sending your resume to a company)</span></div>

                                <br>
                                <img src="{{URL::to('public/how_to/images/tik-1.png')}}"> Allow the resume viewer to give response.
                                <br>
                                <br>
                                If the user has shared the Resume with the allow QR marked,<br>
                                The viewer to whom the Resume has been shared,
                                will have to give a QR to enable downloading.
                                <br>

                                <img src="{{URL::to('public/how_to/images/kindly-quickresponse.png')}}" class="extra-pic">

                                <br>
                                <br>
                                <br>
                                The most appropriate QR message can be selected and submited.
                                <br>
                                <img src="{{URL::to('public/how_to/images/quick-response.png')}}" class="extra-pic">
                                <br>
                                The Response will be delivered to the user sharing the Resume and can be viewed in Shared History->Resume.
                                <br>
                                <br>
                                <br>
                                The Resume can now be downloaded.<br>
                                To Download Resume, Click.
                                <br>
                                <img src="{{URL::to('public/how_to/images/dynamic-resume-download-button.png')}}">
                                <br>
                                <br>
                                This will take time as data is collected and a PDF file is generated.

                                <br>
                                <img src="{{URL::to('public/how_to/images/downloading-please-wait-button.png')}}" class="extra-pic">
                            </div>
                        </div>        

                        <div>
                            <div class="section-five">
                                <b>What's in My Documents?</b><br><br>
                                <div class="showcases-se">
                                    <div class="dgl-p">
                                        <img src="{{URL::to('public/how_to/images/my-doc-icon5.png')}}"></div>
                                    <div class="dgl-text">

                                        <a href="">My Document</a> showcases all your documents uploaded
                                        <br>
                                        <br>
                                        Only Document that are set as Public in 
                                        <br>
                                        <a href="">Edit Profile</a>-><a href="">Upload Document</a> section are visiable here.
                                        <br>

                                        <img src="{{URL::to('public/how_to/images/public-private.png')}}">
                                        <br>
                                    </div>
                                </div>


                                <div class="showcases-se">
                                    <div class="dgl-p">
                                        <img src="{{URL::to('public/how_to/images/my-doc-icon2a.png')}}"></div>
                                    <div class="dgl-text">
                                        Document that are set as set as Private in <br>
                                        <a href="">Edit Profile</a>-><a href="">Upload Document</a> section will not show.
                                        <br>
                                        <!-- <br> -->

                                        <!-- Document that are marked as Public in 
                                        <br>
                                        <a href="">Upload Document</a> are visiable here.
                                        <br> -->

                                        <img src="{{URL::to('public/how_to/images/private-public1.png')}}">
                                        <br>
                                    </div>
                                </div>
                                <div class="showcases-se">
                                    <div class="dgl-p">
                                        <img src="{{URL::to('public/how_to/images/my-doc-icon4a.png')}}"></div>
                                    <div class="dgl-text">
                                        Document thumbnails are sequentially organized. <br>
                                        Click to enlarged image.
                                        <br>

                                        To avoid unathourized dowloads, all enlarged images are watermarked with Flanknot logo.

                                    </div>
                                </div>
                            </div>
                            <div class="section-five">
                                <b>How do others view My Documents?</b> <br><br>
                                <div class="showcases-se">
                                    <div class="dgl-p">
                                        <img src="{{URL::to('public/how_to/images/my-doc-icon5.png')}}"></div>
                                    <div class="dgl-text">
                                        My Documnents can only be accessed by others through a Shared Profile. <br>
                                        <!-- User Douments can  also be viewed by visitors on your profile page.<br> -->
                                        <a href="">My Document</a> page showcases all your documents uploaded
                                        <br>
                                        <br>
                                        Only Document that are set as Public in 
                                        <br>
                                        <a href="">Edit Profile</a>-><a href="">Upload Document</a> section are visiable here.
                                        <br>

                                        <img src="{{URL::to('public/how_to/images/public-private.png')}}">
                                        <br>
                                    </div>
                                </div>
                                <div class="showcases-se">
                                    <div class="dgl-p">
                                        <img src="{{URL::to('public/how_to/images/no-image-upload.png')}}"></div>
                                    <div class="dgl-text">
                                        If No Documents are uploaded. <br>

                                        <br>


                                    </div>
                                </div>


                                <div class="showcases-se">
                                    <div class="dgl-p">
                                        <img src="{{URL::to('public/how_to/images/my-doc-icon2a.png')}}"></div>
                                    <div class="dgl-text">
                                        Document that you have set as Private in 
                                        <br>
                                        <a href="">Edit Profile</a>-><a href="">Upload Document</a> section wil not show.
                                        <br>



                                        <img src="{{URL::to('public/how_to/images/public-private.png')}}">
                                        <br>
                                        <br>
                                    </div>
                                </div>
                                <div class="showcases-se">
                                    <div class="dgl-p">
                                        <img src="{{URL::to('public/how_to/images/my-doc-icon4a.png')}}"></div>
                                    <div class="dgl-text">
                                        Document thumbnails are sequentially organized. <br>
                                        Click to enlarged image.
                                        <br>

                                        To avoid unathourized dowloads, all enlarged images are watermarked with Flanknot logo.


                                    </div>
                                </div>

                                <div class="showcases-se">
                                    <div class="dgl-p">
                                        <img src="{{URL::to('public/how_to/images/my-doc-icon3a.png')}}"></div>
                                    <div class="dgl-text">
                                        <h2>To download any document</h2>
                                        <ul>
                                            <li>1. The visitor will have to select from the list of documens.<br>
                                                <img src="{{URL::to('public/how_to/images/tik-1.png')}}"> Photo<br>
                                                <img src="{{URL::to('public/how_to/images/tik-1.png')}}"> Passport India
                                            </li>
                                            <li>2. Type an email id<br>
                                                <img src="public/how_to/images/text-fild.jpg" class="extra-pic">
                                            </li>
                                            <li>3. Submit Request<br>
                                                <img src="public/how_to/images/request-bt.jpg"><br><br>
                                                This will send a request to the document user to share the Documents required.
                                                if approved by the user,a link for documents will be generated and 
                                                recived on the submited email.

                                            </li>
                                        </ul>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div>
                            <div class="edit_main">
                                <div class="edit_left">
                                    <b>How to download the uploaded Documents?</b> <br><br>
                                    <img src="public/how_to/images/left-menu1a.png" class="image_full">
                                    <img src="public/how_to/images/left-menu-responsive.png" class="image_mobile">
                                    <img src="public/how_to/images/left-menu-responsive.png" class="image_mobile_one">

                                </div>
                                <div class="edit_right">
                                    <br>
                                    <div class="get-started">To Download your uploaded documents go to <br>
                                        <a href="#"> <a href=""><i class="fa fa-ship"></i> Download document</a>.</div>
                                    <img src="public/how_to/images/select-clear-all1a.png"><br>
                                    Select all the documents together or select individually the document <br> 
                                    that needs to be downloaded.<br><br>
                                    <img src="public/how_to/images/selected-image-icon.png" class="extra-pic"><br><br><br>
                                    <img src="public/how_to/images/download-zip1a.png"><br>
                                    Download all the selected document in a structured Zip File.
                                    <br>
                                    <br>
                                    <div class="get-started">Other can also download the Documents by clicking on a link received,<br> 
                                        that gets generated when the user has shared the documents via <br>
                                        <i class="fa fa-ship"></i><a href="">Share Details</a>-><a href="">Share Documents</a></div>
                                    <br>
                                    <br>

                                </div>
                            </div>
                        </div>
                        <div>
                            <div class="edit_main">
                                <b>How to Apply to New Job Openings?</b><br><br>
                                <div class="edit_left">
                                    <img src="public/how_to/images/name-drop-menu.png" class="image_full">
                                    <img src="public/how_to/images/name-drop-menu.png" class="image_mobile_two">
                                </div>
                                <div class="edit_right">
                                    <div class="get-started">To view new valid job requirements go to 
                                        <a href="#"><i class="fa fa-ship"></i>New jobs</a></div>
                                    <br><br>

                                    You will be able to view new jobs requirements only <br> if your Profile is completed and up to date.<br>
                                    Regular using your account will also help display better results. 
                                    <br><br>
                                    At Flanknot we search for the latest Job Openings across the Globe <br>
                                    through direct contact with companies. <br>
                                    Our search also includes other mediums such as Print magazines, newsletters
                                    websites, Social Networking platforms etc.
                                    <br><br>
                                    Any new Job we find, we enter it into our Flanknot software. <br> 
                                    Our Algorithm will go through your Profile Keywords and forward you the Job Requirement 
                                    if found suitable.
                                    <br>
                                    <br>
                                    <br>

                                    You have an option to Apply for the Job by clicking on<br>
                                    <img src="public/how_to/images/apply-job-button.png" class="image_full">
                                    <img src="public/how_to/images/apply-job-button.png" class="image_mobile_two">
                                    This will forward your Resume to Company wth the Job Requirement. 
                                    <br><br><br>

                                    You also have an option to Reject by clicking on <br>
                                    <img src="public/how_to/images/not-Interested.png" class="image_full">
                                    <img src="public/how_to/images/not-Interested.png" class="image_mobile_two">
                                    This will Allows you to givea us a suggestion, why? 
                                    <br><br><br>
                                </div>
                            </div>

                        </div>                       

                        <div>
                            <div class="edit_main">
                                <b>How to Save a Contact?</b> <br><br><br>
                                <div class="edit_right_one_l">

                                    <img src="public/how_to/images/create-contact.jpg" class="image_full">
                                    <img src="public/how_to/images/create-contact.jpg" class="image_mobile_two">
                                    <!-- <div class="get-started">You can create a new contact</div> -->
                                    <br><br>
                                    <img src="public/how_to/images/edit2.jpg" class="image_mobile_two">

                                    <strong>Action</strong><br><img src="public/how_to/images/edit2.jpg" class="image_full">
                                    You can also edit your existing contacts.
                                    i.e the email id on which your have earlier shared your resume,profile or Document<br><br><br>
                                    <img src="public/how_to/images/create-contacts-box.png" class="image_full">
                                    <img src="public/how_to/images/create-contacts-box.png" class="image_mobile_two">

                                    <br>
                                    You can Create a new contract or edit an existing email id. <br>
                                    save your contact to Company or Person<br><br>
                                    <br><br>
                                    Your Contact Log <br><br>
                                    <img src="public/how_to/images/contact-log.png" class="image_full">
                                    <img src="public/how_to/images/contact-log-responsive.png" class="image_mobile_two">
                                    <br>
                                    Share your Resume | Profile | Document to your saved contacts.
                                    <br><br><br>
                                </div>
                            </div>
                        </div>
                        <div>
                            <div class="edit_main">
                                <div class="edit_left_one">
                                    <b>How to share Resume?</b><br><br>
                                    <img src="public/how_to/images/share-sesume-icon.png">

                                </div>
                                <div class="edit_right_one">
                                    <br><br>
                                    <h2>Share your Resume</h2><br>
                                    <span>(keep this marked when sending your resume to a company)</span>
                                    <br>
                                    <img src="public/how_to/images/tik-1.png">  Allow the resume viewer to give responce
                                    <br><br>
                                    keep this checked to allow, to allow your Resume viewer to give QR (Ouick Response) <br><br><br>
                                    <p>*This resume is created by the data entered in Edit Profile -> Personal Detail/Document 
                                        Details/Course Details/Sea service Details /Ganeral Datails<br>
                                        *You can view and dowload your resume through Checkout my resume on your Profile.</p><br><br>

                                    Share Resume on Email    
                                    <img src="public/how_to/images/share-resume-via-email.png"  class="image_full">
                                    <img src="public/how_to/images/share-resume-via-email.png" class="image_mobile_two"><br>

                                    Type a new Email and press enter or space or click in the outer frame. <br>
                                    The Email gets selected.
                                    <br>
                                    <br>
                                    <br>
                                    <img src="public/how_to/images/share-resume-to.png"  class="image_full">
                                    <img src="public/how_to/images/share-resume-to.png" class="image_mobile_two"><br>
                                    Or select an email id from the already sent email or created contacts. <br>
                                    click on them to select.
                                    <br>
                                    <br>
                                    <br>
                                    <img src="public/how_to/images/add-email.png"  class="image_full">
                                    <img src="public/how_to/images/add-email.png" class="image_mobile_two">
                                    <br>
                                    Once Email selected,press the Send button to share your Resume.
                                    <br><br><br>
                                </div>
                            </div>
                        </div>
                        <div>
                            <div class="edit_main">
                                <!--<b>How to Share Documents?</b><br><br>-->
                                <div class="edit_left_one">
                                    <img src="public/how_to/images/share-document.png" >


                                </div>
                                <div class="edit_right_one">
                                    <h2>Share your Documents</h2><br>
                                    <br>
                                    Selcect the document to share <br>
                                    <img src="public/how_to/images/select-document.png"  class="image_full">
                                    <img src="public/how_to/images/select-document.png"  class="image_mobile_two">
                                    <p>*This list is generated by the document upload in Edit Profile->Upload Docoment section.</p>
                                    <br><br><br>
                                    Share Documents on Email 
                                    <img src="public/how_to/images/share-documents-via.png"  class="image_full"><br>
                                    <img src="public/how_to/images/share-documents-via.png"  class="image_mobile_two"><br>

                                    Type a new Email and press enter or space or click in the outer frame. <br>
                                    The Email gets selected.
                                    <br>
                                    <br>
                                    <br>
                                    <img src="public/how_to/images/share-documents.png" class="image_full">
                                    <img src="public/how_to/images/share-documents.png" class="image_mobile_two">
                                    <br>
                                    Or select an email id from the already sent email or created contacts. <br>
                                    click on them to select.
                                    <br>
                                    <br>
                                    <br>
                                    <img src="public/how_to/images/add-email.png"  class="image_full">
                                    <img src="public/how_to/images/add-email.png"  class="image_mobile_two">
                                    <br>
                                    Once Email selected,press the Send button to share your Documents.<br><br>
                                    <span>For Security reasons, Each Document link will be valid for 48 hrs only.</span>
                                    <br><br><br>
                                </div>
                            </div>
                        </div>
                        <div>
                            <div class="edit_main">
                                <b>How to Share Profile?</b><br><br>
                                <div class="edit_left_one">
                                    <img src="public/how_to/images/share-profile.png">

                                </div>
                                <div class="edit_right_one">
                                    <h2>Share your profile </h2><br><br>
                                    Share Profile on Email <br>
                                    <img src="public/how_to/images/share-profile-via-email.png" class="image_full">
                                    <img src="public/how_to/images/share-profile-via-email.png" class="image_mobile_two">
                                    <br>

                                    Type a new Email and press enter or space or click in the outer frame. <br>
                                    The Email gets selected.
                                    <br>
                                    <br>
                                    <br>
                                    <img src="public/how_to/images/share-profile-to.png" class="image_full">
                                    <img src="public/how_to/images/share-profile-to.png" class="image_mobile_two">
                                    <br>
                                    Or select an email id from the already sent email or created contacts. <br>
                                    click on them to select.
                                    <br>
                                    <br>
                                    <br>
                                    <img src="public/how_to/images/add-email.png" class="image_full">
                                    <img src="public/how_to/images/add-email.png" class="image_mobile_two">
                                    <br>
                                    Once Email selected,press the Send button to share your Profile.
                                    <br><br><br>

                                </div>
                            </div>
                        </div>
                        <div>
                            <div class="edit_main">
                                <b>Where to check Quick Response 'QR' from Companies?</b><br><br>
                                <img src="public/how_to/images/resume-history-title.png" class="image_full">
                                To View the history of your Resume shared or to know when your shared Resume was viewed.<br>
                                Click on Resume History<br><br><br>

                                <img src="public/how_to/images/history-dt9.png" class="image_full"><br>
                                <img src="public/how_to/images/history-dt-responsive9.png" class="image_mobile_two">

                                The Data table gives a record of the history of Resume shared such as on which email, on what date,
                                whether the link is still active and when was the last response received<br><br>

                                Detailed Info allows the user to view the log of when the resume was viewed and a QR if received.<br><br>

                                The records can be deleted.<br><br>

                                The 'New' Notification in detailed info indicates an upadate in the log.
                                <br>
                                <br>
                                <br>

                                <img src="public/how_to/images/resume-share-log9.png" class="image_full"><br>
                                <img src="public/how_to/images/resume-share-log9.png" class="image_mobile_two"><br>

                                The Log shows when your shared Resume was viewed.<br><br>

                                Quick Response ‘QR’ is the resume receivers response to your resume.<br>

                                It is mandatory for the resume receiver to give a QR before download a resume while sharing your resume.<br><br>

                                Multiple QR can be received for a single share which could indicate a progress from<br>
                                1.Thank you Resume Received<br>
                                2.Your Resume is shortlisted<br><br><br>
                            </div>

                            <div class="edit_main">
                                <b>How to know if Document shared was received and downloaded?</b><br><br>
                                <img src="public/how_to/images/document-history-title9.png" class="image_full">
                                <img src="public/how_to/images/document-history-title9.png" class="image_mobile_two">
                                To View the list of documents shared and history of when the documents were viewed and downloaded.<br>
                                Click on Document History<br><br><br>

                                <img src="public/how_to/images/history-dt9.png" class="image_full"><br>
                                <img src="public/how_to/images/history-dt-responsive9.png" class="image_mobile_two"><br>
                                The Data table give a record of the history of Document shared such as on wiich email, on what date, 
                                whether the link is still active.<br><br>

                                Detailed Info allows the user to view the log of the shared Document viewed or downloaded.<br><br>

                                The records can be deleted.<br><br>

                                The 'New' Notification in detailed info indicates an upadate in the log.
                                <br>
                                <br>
                                <br>

                                <img src="public/how_to/images/document-share9.png" class="image_full"><br>
                                <img src="public/how_to/images/document-share9.png" class="image_mobile_two"><br>

                                The Log shows when your Shared Document was viewed and whether it was dowloaded
                                <br><br><br>

                            </div>
                        </div>

                    </div>

                </div>

            </div>
        </div>
    </div>

</section>


<!--<script type="text/javascript" src="{{ URL:: asset('public/how_to/js/jquery.min.js')}}"></script>--> 
<!--<script type="text/javascript" src="{{ URL:: asset('public/how_to/js/bootstrap.min.js')}}"></script>-->
<script src="{{ URL:: asset('public/how_to/js/easyResponsiveTabs.js')}}"></script>
<script type="text/javascript">
$(document).ready(function () {


    // Child Tab
    $('#ChildVerticalTab_1').easyResponsiveTabs({
        type: 'vertical',
        width: 'auto',
        fit: true,
        tabidentify: 'ver_1', // The tab groups identifier
        activetab_bg: '#fff', // background color for active tabs in this group
        inactive_bg: '#F5F5F5', // background color for inactive tabs in this group
        active_border_color: '#c1c1c1', // border color for active tabs heads in this group
        active_content_border_color: '#5AB1D0' // border color for active tabs contect in this group so that it matches the tab head border
    });

});
</script>
@stop