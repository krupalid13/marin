@extends('site.index')
@section('content')
<div class="seafarer-reg-page content-section-wrapper">
    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-white panel-tabs">
                <div class="panel-heading">
                    <h4 class="panel-title">Panel <span class="text-bold">Tabs</span></h4>
                </div>
                <button class="btn btn-success" type="button" id="show-hide" value="show">Show</button>
                <div class="panel-body">
                    <div class="tabbable">
                        <ul class="nav nav-tabs" id="userInfoTab">
                            <li class="active">
                                <a data-toggle="tab" href="#user">
                                    Personal Details
                                </a>
                            </li>
                            <li>
                                <a data-toggle="tab" href="#documents" id="step2">
                                    Important Documents
                                </a>
                            </li>{{--
                            <li>
                                <a data-toggle="tab" href="#documents" id="step2">
                                    Important Documents
                                </a>
                            </li>--}}
                        </ul>
                        <div class="tab-content">
                            <div id="user" class="tab-pane fade in active">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="panel-body">

                                            @if (count($errors) > 0)
                                                <div class="alert alert-danger">
                                                    <ul>
                                                        @foreach ($errors->all() as $error)
                                                            <li>{{ $error}}</li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            @endif
                                            <form role="form" class="form-horizontal" id="Seafarer-profile-registration-step1" action="{{ route('site.user.profile.registration') }}" method="post">
                                                {{ csrf_field() }}

                                                <input type="hidden" name="image-x" required>
                                                <input type="hidden" name="image-y" required>
                                                <input type="hidden" name="image-x2" required>
                                                <input type="hidden" name="image-y2" required>
                                                <input type="hidden" name="crop-w" required>
                                                <input type="hidden" name="crop-h" required>
                                                <input type="hidden" name="image-w" required>
                                                <input type="hidden" name="image-h" required>
                                                <?php
                                                    $user_data = $data[0];
                                                ?>
                                                <input type="hidden" name="logged_user_id" value="{{$user_data['id']}}">
                                                <div class="col-md-12">
                                                        <a class="registration-profile-image btn-file no-padding overflow-hidden pos-relative">
                                                            <div id="image-content">
                                                                <input type="file" name="profile_pic" id="profile-pic" class="cursor-pointer" onChange="profilePicSelectHandler('Seafarer-profile-registration-step1', 'profile_pic', 'pic')" disabled>
                                                                @if($user_data['profile_pic'])<img id="preview"
                                 src="/{{ env('SEAFARER_PROFILE_PATH')}}{{$user_data['id']}}/{{$user_data['profile_pic']}}" avatar-holder-src="{{ asset('/images/user_default_image.png') }}">
                                                                @else
                                                                    <img id="preview" src="{{ asset('/images/user_default_image.png') }}" avatar-holder-src="{{ asset('/images/user_default_image.png') }}">
                                                                @endif
                                                            </div>
                                                        </a>
                                                        <div class="m-b-10 m-t-5">File can be jpg, jpeg, png of size lesser than 2MB. </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label" for="firstname">
                                                            First Name
                                                        </label>
                                                        <div class="col-sm-9">
                                                            <div class="show-data" style="display: none">
                                                                <input type="text" placeholder="First Name" id="firstname" name="firstname" class="form-control" value="{{ $user_data['first_name'] }}">
                                                            </div>
                                                            <div class="show-label">
                                                                <label>{{ $user_data['first_name'] }}</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label" for="lastname">
                                                            Last Name
                                                        </label>
                                                        <div class="col-sm-9">
                                                            <div class="show-data" style="display: none">
                                                                <input type="text" placeholder="Last Name" id="lastname" name="lastname" class="form-control" value="{{ $user_data['last_name'] }}">
                                                            </div>
                                                            <div class="show-label">
                                                                <label>{{ $user_data['last_name'] }}</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label" for="gender">
                                                            Gender
                                                        </label>
                                                        <div class="col-sm-9">
                                                            <div class="show-data" style="display: none">
                                                                <div class="gender-option-container">
                                                                    <label class="radio-inline">
                                                                        <div class="iradio_minimal-grey" style="position: relative;">
                                <input type="radio" value="M" name="gender" class="grey" {{

                                !empty($user_data['gender']) ?  $user_data['gender'] == 'M'? 'checked' : '' : 'checked' }}></div>
                                                                        Male
                                                                    </label>
                                                                    <label class="radio-inline">
                                                                        <div class="iradio_minimal-grey" style="position: relative;">
                                 <input type="radio" value="F" name="gender" class="grey" {{
                                 !empty($user_data['gender']) ? $user_data['gender'] == 'F'? 'checked' : '' : '' }}></div>
                                                                        Female
                                                                    </label>
                                                                </div>
                                                            </div>
                                                            <div class="show-label">
                                                                <label>{{$user_data['gender'] == 'M'? 'Male' : 'Female'}}</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label" for="email">
                                                            Email
                                                        </label>
                                                        <div class="col-sm-9" id="email-box">
                                                            <div class="show-data" style="display: none">
                                                                <input type="text" placeholder="Email" id="email" name="email" class="form-control" value="{{ $user_data['email'] }}">
                                                            </div>
                                                            <div class="show-label">
                                                                <label>{{ $user_data['email'] }}</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label" for="mobile">
                                                            Mobile
                                                        </label>
                                                        <div class="col-sm-9" id="mobile-box">
                                                            <div class="show-data" style="display: none">
                                                                <input type="text" placeholder="Mobile Number" id="mobile" name="mobile" class="form-control" value="{{ $user_data['mobile'] }}">
                                                            </div>
                                                            <div class="show-label">
                                                                <label>{{ $user_data['mobile'] }}</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label" for="country">
                                                            Country
                                                        </label>
                                                        <div class="col-sm-9">
                                                            <div class="show-data" style="display: none">
                                                                <select id="country" name="country" class="form-control search-select">
                                                                    @foreach( \CommonHelper::countries() as $c_index => $country)
                                                                        <option value="{{ $c_index }}" {{ !empty($user_data['personal_detail']) ? $user_data['personal_detail']['country'] == $c_index ? 'selected' : '' : ''  }}> {{ $country }}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                            <div class="show-label">
                                                                <label>
                                                                    @foreach( \CommonHelper::countries() as $c_index => $country)
                                                                        {{ $user_data['personal_detail']['country'] == $c_index ? $country : '' }}
                                                                    @endforeach
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label" for="state">
                                                            State
                                                        </label>
                                                        <div class="col-sm-9">
                                                            <div class="show-data" style="display: none">
                                                                <select id="state" name="state" class="form-control search-select">
                                                                    @foreach(\CommonHelper::states() as $s_index => $state)
                                                                        <option value="{{$s_index}}" {{ isset($user_data['personal_detail']['state']) ? $user_data['personal_detail']['state'] == $s_index ? 'selected' : '' : ''  }}>{{$state}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                            <div class="show-label">
                                                                <label>
                                                                    @foreach( \CommonHelper::countries() as $s_index => $state)
                                                                        {{ $user_data['personal_detail']['state'] == $s_index ? $state : '' }}
                                                                    @endforeach
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label" for="city">
                                                            City
                                                        </label>
                                                        <div class="col-sm-9">
                                                            <div class="show-data" style="display: none">
                                                                <select id="city" name="city" class="form-control search-select"
                                                                        value="{{ $user_data['personal_detail']['city']}}">
                                                                    @foreach(\CommonHelper::cities() as $ct_index => $city)
                                                                        <option value="{{$ct_index}}" {{ !empty($user_data['personal_detail']) ? $user_data['personal_detail']['city'] == $ct_index ? 'selected' : '' : ''  }}>{{$city}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                            <div class="show-label">
                                                                <label>
                                                                    @foreach( \CommonHelper::cities() as $ct_index => $city)
                                                                        {{ $user_data['personal_detail']['city'] == $ct_index ? $city : '' }}
                                                                    @endforeach
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label" for="pincode">
                                                            Pincode
                                                        </label>
                                                        <div class="col-sm-9">
                                                            <div class="show-data" style="display: none">
                                                                <input type="text" placeholder="Pincode" id="pincode" name="pincode" class="form-control" value="{{ $user_data['personal_detail']['pincode']}}">
                                                            </div>
                                                            <div class="show-label">
                                                                <label>{{ $user_data['personal_detail']['pincode']}}</label>
                                                            </div>

                                                        </div>
                                                    </div>
                                                <div class="form-group">
                                                    <input type="hidden" id="rank_id" name="rank_id" value="{{ $user_data['professional_detail']['current_rank']}}">
                                                    <label class="col-sm-2 control-label" for="rank" >
                                                        Current Rank
                                                    </label>
                                                    <div class="col-sm-9">
                                                        <div class="show-data" style="display: none">
                                                            <select id="rank" name="rank" class="form-control search-select">
                                                                <option value=>Select</option>

                                                                @foreach(\CommonHelper::new_rank() as $rank_index => $category)
                                                                    @foreach($category as $r_index => $rank)
                                                                        <option value="{{$r_index}}" {{ !empty($user_data['professional_detail']) ? $user_data['professional_detail']['current_rank'] == $r_index ? 'selected' : '' : ''}}>{{$rank}} </option>
                                                                    @endforeach
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                        <div class="show-label">
                                                            <label>
                                                                @foreach(\CommonHelper::new_rank() as $rank_index => $category)
                                                                    @foreach( $category as $r_index => $rank)
                                                                        {{ $user_data['professional_detail']['current_rank'] == $r_index ? $rank : '' }}
                                                                    @endforeach
                                                                @endforeach
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label" for="rank_exp">
                                                        Rank Experience
                                                    </label>
                                                    <div class="col-sm-9">
                                                        <div class="show-data" style="display: none">
                                                            <input type="text" class="form-control" placeholder="Rank Experience" id="rank_exp" name="rank_exp"
                                                               value="{{$user_data['professional_detail']['current_rank_exp']}}">
                                                        </div>
                                                        <div class="show-label">
                                                            <label>{{ $user_data['professional_detail']['current_rank_exp'] }}</label>
                                                        </div>
                                                    </div>
                                                </div>

                                                <button type="submit" data-style="zoom-in" class="ladda-button" id="submitProfileBasicDetailButton" disabled>
                                                    Update
                                                </button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="documents" class="tab-pane fade">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="panel-body">

                                            @if (count($errors) > 0)
                                                <div class="alert alert-danger">
                                                    <ul>
                                                        @foreach ($errors->all() as $error)
                                                            <li>{{ $error}}</li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            @endif

                                            <form role="form" class="form-horizontal" id="registration-step2" action="{{ route('site.store.user.profile.documents') }}" method="post">
                                                {{ csrf_field() }}
                                                <input type="hidden" name="existing_user" value="1">
                                                <div class="form-group">
                                                    <div class="title-header">
                                                        Passport Details
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-1">Passport</label>
                                                        <div class="col-md-2">
                                                            <div class="show-data" style="display: none">
                                                                <select id="passcountry" name="passcountry" class="form-control search-select">
                                                                    @foreach( \CommonHelper::countries() as $c_index => $country)
                                                                        <option value="{{ $c_index }}" {{ !empty($user_data['passport_detail']) ? $user_data['passport_detail']['pass_country'] == $c_index ? 'selected' : '' : ''  }}> {{ $country }}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                            <div class="show-label">
                                                                <label>
                                                                    @foreach( \CommonHelper::countries() as $c_index => $country)
                                                                        {{ $user_data['passport_detail']['pass_country'] == $c_index ? $country : '' }}
                                                                    @endforeach
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <label class="col-md-1">Passport Number</label>
                                                        <div class="col-md-2">
                                                            <div class="show-data" style="display: none">
                                                                <input type="text" name="passno" class="form-control" id="passno" placeholder="Passport Number" value="{{$user_data['passport_detail']['pass_number']}}">
                                                            </div>
                                                            <div class="show-label">
                                                                <labal>{{$user_data['passport_detail']['pass_number']}}</labal>
                                                            </div>
                                                        </div>
                                                        <label class="col-md-1">Place Of Issue</label>
                                                        <div class="col-md-2">
                                                            <div class="show-data" style="display: none">
                                                                <input type="text" name="passplace" class="form-control" id="passplace" placeholder="Place Of Issue" value="{{$user_data['passport_detail']['place_of_issue']}}">
                                                            </div>
                                                            <div class="show-label">
                                                                <label>{{$user_data['passport_detail']['place_of_issue']}}</label>
                                                            </div>
                                                        </div>
                                                        <label class="col-md-1">Expiry Date</label>
                                                        <div class="col-md-2">
                                                            <div class="show-data" style="display: none">
                                                                <input type="date" name="passdateofexp" class="form-control" id="passdateofexp" placeholder="mm/dd/yyyy" value="{{$user_data['passport_detail']['pass_expiry_date']}}">
                                                            </div>
                                                            <div class="show-label">
                                                                <label>{{$user_data['passport_detail']['pass_expiry_date']}}</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-1">US Visa</label>
                                                        <div class="col-md-2">
                                                            <div class="show-data" style="display: none">
                                                                <select id="us" name="us" class="form-control selectpicker" value="{{$user_data['passport_detail']['us_visa']}}">
                                                                    <option value="" label="Select">Select</option>
                                                                    <option value="1" {{ $user_data['passport_detail']['us_visa'] == 1 ? 'selected' : '' }}>Yes</option>
                                                                    <option value="0" {{ $user_data['passport_detail']['us_visa'] == 0 ? 'selected' : '' }}>No</option>
                                                                </select>
                                                            </div>
                                                            <div class="show-label">
                                                                <label>{{ $user_data['passport_detail']['us_visa'] == 1 ? 'Yes' : 'No' }}</label>
                                                            </div>
                                                        </div>
                                                        <label class="col-md-1">Expiry Date</label>
                                                        <div class="col-md-2">
                                                            <div class="show-data" style="display: none">
                                                                <input type="date" name="usvalid" class="form-control" id="usvalid" placeholder="dd/mm/yyyy" value="{{$user_data['passport_detail']['us_visa_expiry_date']}}">
                                                            </div>
                                                            <div class="show-label">
                                                                <label>{{$user_data['passport_detail']['us_visa_expiry_date']}}</label>
                                                            </div>
                                                        </div>
                                                        <input type="hidden" id="indian_pcc" value="{{$user_data['passport_detail']['indian_pcc']}}" >
                                                        <label class="col-md-1">Indian PCC</label>
                                                        <div class="col-md-2">
                                                            <div class="show-data" style="display: none">
                                                                <div class="gender-option-container">
                                                                    <label class="radio-inline">
                                                                        <div class="iradio_minimal-grey" style="position: relative;">
                                                                            <input type="radio" value="1" name="PCC" class="grey" {{!empty($user_data['passport_detail']) ? $user_data['passport_detail']['indian_pcc'] == '1'? 'checked' : '' : 'checked' }}></div>
                                                                        Yes
                                                                    </label>
                                                                    <label class="radio-inline">
                                                                        <div class="iradio_minimal-grey" style="position: relative;">
                                                                            <input type="radio" value="0" name="PCC" class="grey" {{
                                 !empty($user_data['passport_detail']) ? $user_data['passport_detail']['indian_pcc'] == '0'? 'checked' : '' : '' }}
                                                                            ></div>
                                                                        No
                                                                    </label>
                                                                </div>
                                                            </div>
                                                            <div class="show-label">
                                                                <label>{{ $user_data['passport_detail']['indian_pcc'] == '1'? 'Yes' : 'No' }}</label>
                                                            </div>
                                                        </div>
                                                        <label class="col-md-1">Issue Date </label>
                                                        <div class="col-md-2">
                                                            <div class="show-data" style="display: none">
                                                                <input type="date" name="pccdate" class="form-control" id="pccdate" placeholder="dd/mm/yyyy" value="{{$user_data['passport_detail']['indian_pcc_issue_date']}}">
                                                            </div>
                                                            <div class="show-label">
                                                                <label>{{$user_data['passport_detail']['indian_pcc_issue_date']}}</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <div class="title-header">
                                                        Seaman Book Details
                                                    </div>
                                                    <div class="form-group required">
                                                        <label class="col-md-2"> CDC</label>
                                                        <div class="col-md-2">
                                                            <div class="show-data" style="display: none">
                                                                <select id="cdccountry" name="cdccountry" class="form-control show-menu-arrow selectpicker">
                                                                    @foreach( \CommonHelper::countries() as $c_index => $country)
                                                                        <option value="{{ $c_index }}" {{ !empty($user_data['seaman_book_detail']) ? $user_data['seaman_book_detail']['cdc'] == $c_index ? 'selected' : '' : ''  }}> {{ $country }}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                            <div class="show-label">
                                                                <label>
                                                                    @foreach( \CommonHelper::countries() as $c_index => $country)
                                                                        {{ $user_data['seaman_book_detail']['cdc'] == $c_index ? $country : '' }}
                                                                    @endforeach
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <label class="col-md-2">CDC Number</label>
                                                        <div class="col-md-2">
                                                            <div class="show-data" style="display: none">
                                                                <input type="text" name="cdcno" class="form-control" id="cdcno" placeholder="CDC Number" value="{{$user_data['seaman_book_detail']['cdc_number']}}">
                                                            </div>
                                                            <div class="show-label">
                                                                <label>{{$user_data['seaman_book_detail']['cdc_number']}}</label>
                                                            </div>
                                                        </div>
                                                        <label class="col-md-2">Expiry Date</label>
                                                        <div class="col-md-2">
                                                            <div class="show-data" style="display: none">
                                                                <input type="date" name="cdc_exp"  class="form-control" id="cdc_exp" placeholder="dd/mm/yyyy" value="{{$user_data['seaman_book_detail']['cdc_expiry_date']}}">
                                                            </div>
                                                            <div class="show-label">
                                                                <label>{{$user_data['seaman_book_detail']['cdc_expiry_date']}}</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-2">Other CDC</label>
                                                        <div class="col-md-2">
                                                            <div class="show-data" style="display: none">
                                                                <select id="ocdccountry" name="ocdccountry" class="form-control selectpicker">
                                                                    @foreach( \CommonHelper::countries() as $c_index => $country)
                                                                        <option value="{{ $c_index }}" {{ !empty($user_data['seaman_book_detail']) ? $user_data['seaman_book_detail']['other_cdc'] == $c_index ? 'selected' : '' : ''  }}> {{ $country }}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                            <div class="show-label">
                                                                <label>
                                                                    @foreach( \CommonHelper::countries() as $c_index => $country)
                                                                        {{ $user_data['seaman_book_detail']['other_cdc'] == $c_index ? $country : '' }}
                                                                    @endforeach
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <label class="col-md-2">Other CDC Number</label>
                                                        <div class="col-md-2">
                                                            <div class="show-data" style="display: none">
                                                                <input type="text" name="ocdcno" class="form-control" id="ocdcno" placeholder="" value="{{$user_data['seaman_book_detail']['other_cdc_number']}}">
                                                            </div>
                                                            <div class="show-label">
                                                                <label>{{$user_data['seaman_book_detail']['other_cdc_number']}}</label>
                                                            </div>
                                                        </div>
                                                        <label class="col-md-2">Expiry Date</label>
                                                        <div class="col-md-2">
                                                            <div class="show-data" style="display: none">
                                                                <input type="date" name="ocdc_exp" class="form-control" id="ocdc_exp" placeholder="dd/mm/yyyy" value="{{$user_data['seaman_book_detail']['other_cdc_expiry_date']}}">
                                                            </div>
                                                            <div class="show-label">
                                                                <label>{{$user_data['seaman_book_detail']['other_cdc_expiry_date']}}</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <div class="title-header">
                                                        Certificate Of Competency
                                                    </div>
                                                    <div class="form-group required">
                                                        <label class="col-md-1">COC</label>
                                                        <div class="col-md-2">
                                                            <div class="show-data" style="display: none">
                                                                <select id="coc_country" name="coc_country" class="form-control selectpicker">
                                                                    @foreach( \CommonHelper::countries() as $c_index => $country)
                                                                        <option value="{{ $c_index }}" {{ !empty($user_data['coc_detail']) ? $user_data['coc_detail']['coc'] == $c_index ? 'selected' : '' : ''  }}> {{ $country }}</option>
                                                                    @endforeach

                                                                </select>
                                                            </div>
                                                            <div class="show-label">
                                                                <label>
                                                                    @foreach( \CommonHelper::countries() as $c_index => $country)
                                                                        {{ $user_data['coc_detail']['coc'] == $c_index ? $country : '' }}
                                                                    @endforeach
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <label class="col-md-1">No</label>
                                                        <div class="col-md-2">
                                                            <div class="show-data" style="display: none">
                                                                <input type="text" name="coc_no" class="form-control" id="coc_no" placeholder="COC Number" value="{{$user_data['coc_detail']['coc_number']}}">
                                                            </div>
                                                            <div class="show-label">
                                                                <label>{{$user_data['coc_detail']['coc_number']}}</label>
                                                            </div>
                                                        </div>
                                                        <label class="col-md-1">Grade</label>
                                                        <div class="col-md-2">
                                                            <div class="show-data" style="display: none">
                                                                <input type="text" name="grade" class="form-control" id="grade" placeholder="COC Grade" value="{{$user_data['coc_detail']['coc_grade']}}">
                                                            </div>
                                                            <div class="show-label">
                                                                <label>{{$user_data['coc_detail']['coc_grade']}}</label>
                                                            </div>
                                                        </div>
                                                        <label class="col-md-1">Expiry Date</label>
                                                        <div class="col-md-2">
                                                            <div class="show-data" style="display: none">
                                                                <input type="date" name="coc_exp" class="form-control" id="coc_exp" placeholder="dd/mm/yyyy" value="{{$user_data['coc_detail']['coc_expiry_date']}}">
                                                            </div>
                                                            <div class="show-label">
                                                                <label>{{$user_data['coc_detail']['coc_expiry_date']}}</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-1">Other COC</label>
                                                        <div class="col-md-2">
                                                            <div class="show-data" style="display: none">
                                                                <select id="ococ_country" name="ococ_country" class="form-control selectpicker">
                                                                    @foreach( \CommonHelper::countries() as $c_index => $country)
                                                                        <option value="{{ $c_index }}" {{ !empty($user_data['coc_detail']) ? $user_data['coc_detail']['other_coc'] == $c_index ? 'selected' : '' : ''  }}> {{ $country }}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                            <div class="show-label">
                                                                <label>
                                                                    @foreach( \CommonHelper::countries() as $c_index => $country)
                                                                        {{ $user_data['coc_detail']['other_coc'] == $c_index ? $country : '' }}
                                                                    @endforeach
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <label class="col-md-1"> No</label>
                                                        <div class="col-md-2">
                                                            <div class="show-data" style="display: none">
                                                                <input type="text" name="ococ_no" class="form-control" id="ococ_no" placeholder="Other COC Number" value="{{$user_data['coc_detail']['other_coc_number']}}">
                                                            </div>
                                                            <div class="show-label">
                                                                <label>{{$user_data['coc_detail']['other_coc_number']}}</label>
                                                            </div>
                                                        </div>
                                                        <label class="col-md-1">Grade</label>
                                                        <div class="col-md-2">
                                                            <div class="show-data" style="display: none">
                                                                <input type="text" name="ograde" class="form-control" id="ograde" placeholder="Other COC Grade" value="{{$user_data['coc_detail']['other_coc_grade']}}">
                                                            </div>
                                                            <div class="show-label">
                                                                <label>{{$user_data['coc_detail']['other_coc_grade']}}</label>
                                                            </div>
                                                        </div>
                                                        <label class="col-md-1">Expiry Date</label>
                                                        <div class="col-md-2">
                                                            <div class="show-data" style="display: none">
                                                                <input type="date" name="ococ_exp" class="form-control" id="ococ_exp" placeholder="dd/mm/yyyy" value="{{$user_data['coc_detail']['other_coc_expiry_date']}}">
                                                            </div>
                                                            <div class="show-label">
                                                                <label>{{$user_data['coc_detail']['other_coc_expiry_date']}}</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <div class="title-header">
                                                        GMDSS Details
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-1">GMDSS</label>
                                                        <div class="col-md-2">
                                                            <div class="show-data" style="display: none">
                                                                <select id="gmdss_country" name="gmdss_country" class="form-control selectpicker">
                                                                    @foreach( \CommonHelper::countries() as $c_index => $country)
                                                                        <option value="{{ $c_index }}" {{ !empty($user_data['gmdss_detail']) ? $user_data['gmdss_detail']['gmdss'] == $c_index ? 'selected' : '' : ''  }}> {{ $country }}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                            <div class="show-label">
                                                                <label>
                                                                    @foreach( \CommonHelper::countries() as $c_index => $country)
                                                                        {{ $user_data['gmdss_detail']['gmdss'] == $c_index ? $country : '' }}
                                                                    @endforeach
                                                                </label>
                                                            </div>
                                                        </div>

                                                        <label class="col-md-1">No</label>
                                                        <div class="col-md-2">
                                                            <div class="show-data" style="display: none">
                                                                <input type="text" name="gmdss_no" class="form-control" id="gmdss_no" placeholder="GMDSS Number" value="{{$user_data['gmdss_detail']['gmdss_number']}}">
                                                            </div>
                                                            <div class="show-label">
                                                                <label>{{$user_data['gmdss_detail']['gmdss_number']}}</label>
                                                            </div>
                                                        </div>


                                                        <label class="col-md-2">Expiry Date</label>
                                                        <div class="col-md-2">
                                                            <div class="show-data" style="display: none">
                                                                <input type="date" name="gmdss_doe" class="form-control" id="gmdss_doe" placeholder="dd/mm/yyyy" value="{{$user_data['gmdss_detail']['gmdss_expiry_date']}}">
                                                            </div>
                                                            <div class="show-label">
                                                                <label>{{$user_data['gmdss_detail']['gmdss_expiry_date']}}</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <div class="title-header">
                                                        Watch Keeping For Rating
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-1">Watch Keeping</label>
                                                        <div class="col-md-2">
                                                            <div class="show-data" style="display: none">
                                                                <select id="watch_country" name="watch_country" class="form-control selectpicker">
                                                                    @foreach( \CommonHelper::countries() as $c_index => $country)
                                                                        <option value="{{ $c_index }}" {{ !empty($user_data['wkfr_detail']) ? $user_data['wkfr_detail']['watch_keeping'] == $c_index ? 'selected' : '' : ''  }}> {{ $country }}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                            <div class="show-label">
                                                                <label>
                                                                    @foreach( \CommonHelper::countries() as $c_index => $country)
                                                                        {{ $user_data['wkfr_detail']['watch_keeping'] == $c_index ? $country : '' }}
                                                                    @endforeach
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <label class="col-md-1">Deck / Engine</label>
                                                        <div class="col-md-2">
                                                            <div class="show-data" style="display: none">
                                                                <select id="deckengine" name="deckengine" class="form-control selectpicker" value="{{$user_data['wkfr_detail']['type']}}">
                                                                    <option value="" label="Select">Select</option>
                                                                    <option value="Deck" {{ $user_data['wkfr_detail']['type'] == 'Deck' ? 'selected' : ''}}>Deck</option>
                                                                    <option value="Engine" {{ $user_data['wkfr_detail']['type'] == 'Engine'? 'selected' : ''}}>Engine</option>
                                                                </select>
                                                            </div>
                                                            <div class="show-label">
                                                                <label>{{$user_data['wkfr_detail']['type'] == 'Deck' ? 'Deck' : 'Engine'}}</label>
                                                            </div>
                                                        </div>
                                                        <label class="col-md-1">No</label>
                                                        <div class="col-md-2">
                                                            <div class="show-data" style="display: none">
                                                                <input type="text" name="watch_no" class="form-control" id="watch_no" placeholder="Number" value="{{$user_data['wkfr_detail']['wkfr_number']}}">
                                                            </div>
                                                            <div class="show-label">
                                                                <label>{{$user_data['wkfr_detail']['wkfr_number']}}</label>
                                                            </div>
                                                        </div>


                                                        <label class="col-md-1">Issue Date</label>
                                                        <div class="col-md-2">
                                                            <div class="show-data" style="display: none">
                                                                <input type="date" name="watchissud" class="form-control" id="watchissud" placeholder="dd/mm/yyyy" value="{{$user_data['wkfr_detail']['issue_date']}}">
                                                            </div>
                                                            <div class="show-label">
                                                                <label>{{$user_data['wkfr_detail']['issue_date']}}</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-1">INDOS No </label>
                                                        <div class="col-md-2">
                                                            <div class="show-data" style="display: none">
                                                                <input type="text" name="indosno" class="form-control" id="indosno" placeholder="INDOS Number" value="{{$user_data['wkfr_detail']['indos_number']}}">
                                                            </div>
                                                            <div class="show-label">
                                                                <label>{{$user_data['wkfr_detail']['indos_number']}}</label>
                                                            </div>
                                                        </div>
                                                        <input type="hidden" id="fever" value="{{$user_data['wkfr_detail']['yellow_fever']}}">
                                                        <label class="col-md-1">Yellow Fever</label>
                                                        <div class="col-md-2">
                                                            <div class="show-data" style="display: none">
                                                                <div class="gender-option-container">
                                                                    <label class="radio-inline">
                                                                        <div class="iradio_minimal-grey" style="position: relative;">
                                                                            <input type="radio" value="1" name="yellowfever" class="grey" {{!empty($user_data['wkfr_detail']) ? $user_data['wkfr_detail']['yellow_fever'] == '1'? 'checked' : '' : '' }}
                                                                            ></div>
                                                                        Yes
                                                                    </label>
                                                                    <label class="radio-inline">
                                                                        <div class="iradio_minimal-grey" style="position: relative;">
                                                                            <input type="radio" value="0" name="yellowfever" class="grey" {{!empty($user_data['wkfr_detail']) ? $user_data['wkfr_detail']['yellow_fever'] == '0'? 'checked' : '' : 'checked' }}></div>
                                                                        No
                                                                    </label>
                                                                </div>
                                                            </div>
                                                            <div class="show-label">
                                                                <label>{{ $user_data['wkfr_detail']['yellow_fever'] == '1'? 'Yes' : 'No'}}</label>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                                {{--<button type="submit" id="seaferer-doc-submit-button" class="btn btn-primary">
                                                    Save
                                                </button>--}}
                                                <button type="submit" data-style="zoom-in" class="ladda-button" id="seaferer-doc-submit-button" disabled>
                                                    Update
                                                </button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->

            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Mobile Verification</h4>
                </div>

                <div class="modal-body">
                    <form id="mob-verification" method="post" action="/mob-verification">
                        <div class="row form-group">
                            <div class="col-md-12">
                                <p>You have changed your mobile number. Please verify your Mobile Number. </p>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-md-7">
                                <p>Please Enter OTP (One Time Password) :</p>
                            </div>
                            <div class="col-md-5">
                                <input class="form-control" name="mob_otp" type="text">
                            </div>
                        </div>
                        <div class="alert alert-danger hide" id="invalid_mob_error">
                            <strong>Invalid!</strong> Entered OTP is Invalid.
                        </div>
                        <div class="alert alert-danger hide" id="otp_failure">

                        </div>
                        <div class="alert alert-success hide" id="otp_resend_success">
                            <strong>Success..</strong> New OTP has been sent to your mobile number..
                        </div>
                        <div class="alert alert-danger hide" id="mob_error_resend_otp">
                            <strong>Error!</strong> Error while generating OTP.
                        </div>
                    </form>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="resend-otp-button" style="float: left;">Resend OTP</button>
                    <button type="button" class="btn btn-success" id="mob_otp_submit_button" name="mob_otp_submit_button">Verify</button>
                    <a href="{{ route('home') }}" type="button" class="btn btn-danger">Close</a>
                </div>
            </div>

        </div>
    </div>
@endsection