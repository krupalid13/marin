@extends('site.index')
@section('content')

<div class="login_home_page content-section-wrapper">
	<div class="section">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					@include('site.partials.new_register')
				</div>
			</div>
		</div>
	</div>
</div>

@stop