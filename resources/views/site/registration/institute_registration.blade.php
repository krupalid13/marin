@extends('site.index')
@section('content')

    <?php
        $initialize_pincode_maxlength_validation = false;
        $india_value = array_search('India',\CommonHelper::countries());

        $pincode_block_indexes = [];
    ?>
    <div class="company-registration content-section-wrapper">

        <div class="container">
            <div class="row">

                <div class="col-sm-12">
                    <div class="alert alert-danger alert-box" style="display: none">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error}}</li>
                            @endforeach
                        </ul>
                    </div>
                    <form id="institute-registration-form" class="form-data" method="post" action="{{ isset($user_data['current_route']) ? route('site.institute.profile.registration') : route('site.store.institute.details')}}">
                        <input type="hidden" id="institute_id" value="{{ isset($user_data[0]['id']) ? $user_data[0]['id'] : ''}}">
                        {{ csrf_field() }}
                        <div class="user-details-container" style="margin-top: 80px;">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="main-heading">
                                        Login Details
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="heading">
                                        ACCOUNT CREDENTIALS<span class="symbol required"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="input-label" for="email">Login Email Address<span class="symbol required"></span></label>
                                        <input type="email" class="form-control" id="email" name="email" placeholder="Type your email address"
                                               value="{{ isset($user_data[0]['email']) ? $user_data[0]['email'] : ''}}">
                                        <span class="hide" id="email-error" style="color: red"></span>
                                    </div>
                                </div>
                            @if(!isset($user_data['current_route']))
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="input-label" for="password">Password<span class="symbol required"></span></label>
                                        <input type="password" class="form-control" id="password" name="password" placeholder="Type your password">
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="input-label" for="cpassword">Confirm Password<span class="symbol required"></span></label>
                                        <input type="password" class="form-control" id="cpassword" name="cpassword" placeholder="Type password again">
                                    </div>
                                </div>
                            @endif
                            </div>
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="input-label" for="contact_person">Login Name<span class="symbol required"></span></label>
                                        <input type="text" class="form-control" id="contact_person" name="contact_person" placeholder="Type your login name" value="{{ isset($user_data[0]['institute_registration_detail']['contact_person']) ? $user_data[0]['institute_registration_detail']['contact_person'] : ''}}">
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="input-label" for="contact_person_number">Login Mobile Number<span class="symbol required"></span></label>
                                        <input type="text" class="form-control" id="contact_person_number" name="contact_person_number" placeholder="Type your login mobile number" value="{{ isset($user_data[0]['institute_registration_detail']['contact_number']) ? $user_data[0]['institute_registration_detail']['contact_number'] : ''}}">
                                    </div>
                                </div>
                                <!-- <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="input-label" for="contact_person_email">Contact Person Email ID</label>
                                        <input type="text" class="form-control" id="contact_person_email" name="contact_person_email" placeholder="Type institute person email id" value="{{ isset($user_data[0]['institute_registration_detail']['contact_email']) ? $user_data[0]['institute_registration_detail']['contact_email'] : ''}}">
                                    </div>
                                </div> -->
                            </div>
                        </div>

                        <div id="institute-details-form">
                            <input type="hidden" name="image-x" required>
                            <input type="hidden" name="image-y" required>
                            <input type="hidden" name="image-x2" required>
                            <input type="hidden" name="image-y2" required>
                            <input type="hidden" name="crop-w" required>
                            <input type="hidden" name="crop-h" required>
                            <input type="hidden" name="image-w" required>
                            <input type="hidden" name="image-h" required>

                            <div class="user-details-container">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="heading">
                                            INSTITUTE DETAILS<span class="symbol required"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="upload-photo-container">
                                    {{ csrf_field() }}
                                    <div class="image-content">
                                        <div class="registration-profile-image">
                                            <?php
                                            $image_path = '';
                                            if(isset($user_data[0]['profile_pic'])){
                                                $image_path = env('INSTITUTE_LOGO_PATH')."".$user_data[0]['id']."/".$user_data[0]['profile_pic'];
                                            }
                                            ?>
                                            @if(empty($image_path))
                                                <div class="icon profile_pic_text"><i class="fa fa-camera" aria-hidden="true"></i></div>
                                                <div class="image-text profile_pic_text">Upload Logo <br> Picture</div>
                                            @endif

                                            <input type="file" name="profile_pic" id="profile-pic" class="cursor-pointer" onChange="profilePicSelectHandler('institute-details-form', 'profile_pic', 'logo' , 'institute')">

                                            @if(!empty($image_path))
                                                <img id="preview" src="/{{ $image_path }}">
                                            @else
                                                <img id="preview">
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="input-label" for="institute_name">Institute Name<span class="symbol required"></span></label>
                                            <input type="text" class="form-control" id="institute_name" name="institute_name" placeholder="Type institute name" value="{{ isset($user_data[0]['institute_registration_detail']['institute_name']) ? $user_data[0]['institute_registration_detail']['institute_name'] : ''}}">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="input-label" for="institute_alias">Institute Alias<span class="symbol required"></span></label>
                                            <input type="text" class="form-control" id="institute_alias" name="institute_alias" placeholder="Type institute alias" value="{{ isset($user_data[0]['institute_registration_detail']['institute_alias']) ? $user_data[0]['institute_registration_detail']['institute_alias'] : ''}}">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label class="input-label" for="institute_description">Description<span class="symbol required"></span></label>
                                            <textarea class="form-control" id="institute_description" name="institute_description" placeholder="Type institute description" rows="6">{{ isset($user_data[0]['institute_registration_detail']['institute_detail']['institute_description']) ? $user_data[0]['institute_registration_detail']['institute_detail']['institute_description'] : ''}}</textarea>
                                        </div>
                                    </div>
                                </div><!-- 
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="input-label" for="institute_email">Institute Email ID<span class="symbol required"></span></label>
                                            <input type="text" class="form-control" id="institute_email" name="institute_email" placeholder="Type institute email" value="{{ isset($user_data[0]['institute_registration_detail']['institute_detail']['institute_email']) ? $user_data[0]['institute_registration_detail']['institute_detail']['institute_email'] : ''}}">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="input-label" for="institute_contact_number">Institute Phone Number<span class="symbol required"></span></label>
                                            <input type="text" class="form-control" id="cmpany_contact_number" name="institute_contact_number" placeholder="Please enter mobile number" value="{{ isset($user_data[0]['institute_registration_detail']['institute_detail']['institute_contact_number']) ? $user_data[0]['institute_registration_detail']['institute_detail']['institute_contact_number'] : ''}}">
                                        </div>
                                    </div>
                                </div> -->
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="input-label" for="website">Institute Website</label>
                                            <div class="input-group">
                                                <span class="input-group-addon http-label">http://</span>
                                                <input type="text" class="form-control p-l-5" id="website" name="website" placeholder="Type your website address" value="{{ isset($user_data[0]['institute_registration_detail']['website']) ? $user_data[0]['institute_registration_detail']['website'] : ''}}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="input-label" for="fax">Institute Fax</label>
                                            <input type="text" class="form-control" id="fax" name="fax" placeholder="Type your fax number" value="{{ isset($user_data[0]['institute_registration_detail']['institute_detail']['fax']) ? $user_data[0]['institute_registration_detail']['institute_detail']['fax'] : ''}}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div id="location-details-form">
                            <div class="user-details-container add-location-template">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="heading">
                                            LOCATION DETAILS<span class="symbol required"></span>
                                        </div>
                                    </div>
                                </div>
                                @if(isset($user_data[0]['institute_registration_detail']['institute_locations']) && !empty($user_data[0]['institute_registration_detail']['institute_locations']))
                                    @foreach($user_data[0]['institute_registration_detail']['institute_locations'] as $index => $value)
                                    <input type="hidden" class="total_location_block_index" value={{$index}}>
                                    <div id="locations-template" class="locations-template">
                                        <input type="hidden" class="location_id" name="location_id[{{$index}}]" value={{$value['id']}}>
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <div class="input-label p-t-10 location-name pull-left">Location {{$index+1}}</div>
                                                @if($index > 0)
                                                    <div class="pull-right p-t-10 close-button close-button-{{$index}}">
                                                        <i class="fa fa-times" aria-hidden="true"></i>
                                                    </div>
                                                @else
                                                    <div class="pull-right p-t-10 close-button hide close-button-{{$index}}">
                                                        <i class="fa fa-times" aria-hidden="true"></i>
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="input-label" for="is_headoffice">Is Head Branch<span class="symbol required"></span></label>
                                                    <label class="radio-inline">
                                                        <input type="radio" class="is_headoffice" name="is_headoffice[{{$index}}]" value="1" {{ !empty($value['headbranch']) ? $value['headbranch'] == 1 ? 'checked' : '' : ''}}> Yes
                                                    </label>
                                                    <label class="radio-inline">
                                                        <input type="radio" class="is_headoffice" name="is_headoffice[{{$index}}]" value="0" {{ !empty($value['headbranch']) ? $value['headbranch'] == 0 ? 'checked' : '' : 'checked'}}> No
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-xs-6">
                                                <div class="form-group">
                                                    <label class="input-label">Branch Type<span class="symbol required"></span></label>
                                                    <select name="branch_type[{{$index}}]" class="form-control search-select branch_type" block-index="{{$index}}">
                                                        <option value="">Select Your Branch Type</option>
                                                        @foreach( \CommonHelper::institute_branch_type() as $b_index => $branch_type)
                                                            <option value="{{ $b_index }}" {{ isset($value['branch_type']) ? $value['branch_type'] == $b_index ? 'selected' : '' : ''}}> {{ $branch_type }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="input-label" for="institute_email">Institute Email ID</label>
                                                    <input type="text" class="form-control loc_institute_email" name="loc_institute_email[{{$index}}]" placeholder="Type institute email" value="{{ isset($value['email']) ? $value['email'] : ''}}">
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="input-label" for="institute_contact_number">Institute Phone Number</label>
                                                    <input type="text" class="form-control loc_phone_number" name="loc_phone_number[{{$index}}]" placeholder="Please enter mobile number" value="{{ isset($value['number']) ? $value['number'] : ''}}">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="input-label" for="contact_person_one">Contact Person Name</label>
                                                    <input type="text" class="form-control loc_contact_person" name="loc_contact_person[{{$index}}]" placeholder="Type your contact person name" value="{{ isset($value['location_contact'][0]['contact_name']) ? $value['location_contact'][0]['contact_name'] : '' }}">
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="input-label" for="conatct_designation_one">Designation</label>
                                                    <input type="text" class="form-control loc_contact_designation"  name="loc_contact_designation[{{$index}}]" placeholder="Enter your Designation" value="{{ isset($value['location_contact'][0]['designation']) ? $value['location_contact'][0]['designation'] : '' }}">
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="input-label" for="contact_phone_one">Contact Person Number</label>
                                                    <input type="phone" class="form-control loc_contact_phone" name="loc_contact_phone[{{$index}}]" placeholder="Enter your Phone Number" value="{{ isset($value['location_contact'][0]['contact_number']) ? $value['location_contact'][0]['contact_number'] : '' }}">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="input-label">Country<span class="symbol required"></span></label>
                                                    <select name="country[{{$index}}]" class="form-control search-select country" block-index="{{$index}}">
                                                        <option value="">Select Your Country</option>
                                                        @foreach( \CommonHelper::countries() as $c_index => $country)
                                                            <option value="{{ $c_index }}" {{ isset($value['country']) ? $value['country'] == $c_index ? 'selected' : '' : ''}}> {{ $country }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group pincode-block pincode-block-{{$index}}">
                                                    <label class="input-label" for="pin_code">Postal Code<span class="symbol required"></span></label>
                                                    <div>
                                                        <input type="hidden" class="pincode-id" name="pincode_id[{{$index}}]" value="{{ isset($value['pincode_id']) ? $value['pincode_id'] : ''}}">
                                                        
                                                        <i class="fa fa-spin fa-refresh select-loader hide pincode-loader pincode-loader-{{$index}}"></i>
                                                        <input type="text" block-index="{{$index}}" data-form-id="institute-registration-form" class="form-control pincode pin_code_fetch_input" name="pincode[{{$index}}]" value="{{ isset($value['pincode_text']) ? $value['pincode_text'] : ''}}" placeholder="Type your pincode">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group state-block state-block-{{$index}}">
                                                    <label class="input-label">State<span class="symbol required"></span></label>
                                                    @if(isset($value))
                                                        @if($value['country'] == $india_value )
                                                        <select id="state" name="state[{{$index}}]" class="form-control search-select state fields-for-india">
                                                            <option value="">Select Your State</option>
                                                            @foreach($value['pincode']['pincodes_states'] as $pincode_states)
                                                                <option value="{{$pincode_states['state_id']}}" {{$pincode_states['state_id'] == $value['state_id'] ? 'selected' : ''}}>{{ucwords(strtolower($pincode_states['state']['name']))}}</option>
                                                            @endforeach
                                                        </select>
                                                        <input type="text" id="state" name="state_name[{{$index}}]" class="form-control state_text hide fields-not-for-india" placeholder="Enter State Name" value="">

                                                        @else
                                                            <select id="state" name="state[{{$index}}]" class="form-control search-select state hide fields-for-india">
                                                                <option value="">Select Your State</option>
                                                            </select>
                                                            <input type="text" name="state_text[{{$index}}]" class="form-control state_text fields-not-for-india" placeholder="Enter State Name" value="{{ isset($value['state_text']) ? $value['state_text'] : ''}}">
                                                        @endif
                                                    @else
                                                        <select id="state" name="state[{{$index}}]" class="form-control search-select state hidden fields-for-india">
                                                            <option value="">Select Your State</option>
                                                        </select>
                                                        <input type="text" id="state" name="state_text[{{$index}}]" class="form-control state_text fields-not-for-india" placeholder="Enter State Name" value="{{ isset($value['state_text']) ? $value['state_text'] : ''}}">
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group city-block city-block-{{$index}}">
                                                    <label class="input-label">City<span class="symbol required"></span></label>

                                                    @if(isset($value['country']))
                                                        @if( $value['country'] == $india_value )
                                                            <select id="city" name="city[{{$index}}]" class="form-control city fields-for-india">
                                                                <option value="">Select Your City</option>
                                                                @foreach($value['pincode']['pincodes_cities'] as $pincode_city)
                                                                    <option value="{{$pincode_city['city_id']}}" {{$pincode_city['city_id'] == $value['city_id'] ? 'selected' : ''}}>{{ucwords(strtolower($pincode_city['city']['name']))}}</option>
                                                                @endforeach
                                                            </select>
                                                            <input type="text" name="city_text[{{$index}}]" class="form-control city_text hide fields-not-for-india" placeholder="Enter City Name" value="">

                                                        @else
                                                            <select id="city" name="city[{{$index}}]" class="form-control city hide fields-for-india">
                                                                <option value="">Select Your City</option>
                                                            </select>

                                                            {{--<input type="text" name="city_text[{{$index}}]" class="form-control city_text fields-not-for-india" placeholder="Enter City Name" value="">--}}
                                                            <input type="text" name="city_text[{{$index}}]" class="form-control city_text fields-not-for-india" placeholder="Enter City Name" value="{{ isset($value['city_text']) ? $value['city_text'] : ''}}">
                                                        @endif
                                                    @else
                                                        <select id="city" name="city[{{$index}}]" class="form-control city hide fields-for-india">
                                                            <option value="">Select Your City</option>
                                                        </select>

                                                        {{--<input type="text" name="city_text[{{$index}}]" class="form-control city_text fields-not-for-india" placeholder="Enter City Name" value="">--}}
                                                        <input type="text" name="city_text[{{$index}}]" class="form-control city_text fields-not-for-india" placeholder="Enter City Name" value="{{ isset($value['city_text']) ? $value['city_text'] : ''}}">
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="col-sm-6">
                                                <div class="form-group address-block address-block-{{$index}}">
                                                    <label class="input-label" for="address">Address<span class="symbol required"></span></label>
                                                    <textarea class="form-control address" id="address" name="address[{{$index}}]" placeholder="Type your address" rows="6">{{ isset($value['address']) ? $value['address'] : ''}}</textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <hr>
                                    </div>
                                    @endforeach
                                @else
                                    <div id="locations-template" class="locations-template">
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <div class="input-label p-t-10 location-name pull-left">Location 1</div>
                                                <div class="pull-right close-button hide close-button-0">
                                                    <i class="fa fa-times" aria-hidden="true"></i>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="input-label" for="is_headoffice">Is Head Branch<span class="symbol required"></span></label>
                                                    <label class="radio-inline">
                                                        <input type="radio" class="is_headoffice" name="is_headoffice[0]" value="1" {{ !empty($value['headbranch']) ? $value['headbranch'] == 1 ? 'checked' : '' : ''}}> Yes
                                                    </label>
                                                    <label class="radio-inline">
                                                        <input type="radio" class="is_headoffice" name="is_headoffice[0]" value="0" {{ !empty($value['headbranch']) ? $value['headbranch'] == 0 ? 'checked' : '' : 'checked'}}> No
                                                    </label>
                                                </div>
                                            </div>        
                                            <div class="col-xs-6">
                                                <div class="form-group">
                                                    <label class="input-label">Branch Type<span class="symbol required"></span></label>
                                                    <select name="branch_type[0]" class="form-control search-select branch_type" block-index="0">
                                                        <option value="">Select Your Branch Type</option>
                                                        @foreach( \CommonHelper::institute_branch_type() as $b_index => $branch_type)
                                                            <option value="{{ $b_index }}" {{ isset($value['branch_type']) ? $value['branch_type'] == $b_index ? 'selected' : '' : ''}}> {{ $branch_type }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="input-label" for="institute_email">Institute Email ID</label>
                                                    <input type="text" class="form-control loc_institute_email" name="loc_institute_email[0]" placeholder="Type institute email" value="{{ isset($value['email']) ? $value['email'] : ''}}">
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="input-label" for="institute_contact_number">Institute Phone Number</label>
                                                    <input type="text" class="form-control loc_phone_number" name="loc_phone_number[0]" placeholder="Please enter mobile number" value="{{ isset($value['number']) ? $value['number'] : ''}}">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="input-label" for="contact_person_one">Contact Person Name</label>
                                                    <input type="text" class="form-control loc_contact_person" name="loc_contact_person[0]" placeholder="Type your contact person name" value="{{ isset($value['location_contact'][0]['contact_name']) ? $value['location_contact'][0]['contact_name'] : '' }}">
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="input-label" for="conatct_designation_one">Designation</label>
                                                    <input type="text" class="form-control loc_contact_designation"  name="loc_contact_designation[0]" placeholder="Enter your Designation" value="{{ isset($value['location_contact'][0]['designation']) ? $value['location_contact'][0]['designation'] : '' }}">
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="input-label" for="contact_phone_one">Contact Person Number</label>
                                                    <input type="phone" class="form-control loc_contact_phone" name="loc_contact_phone[0]" placeholder="Enter your Phone Number" value="{{ isset($value['location_contact'][0]['contact_number']) ? $value['location_contact'][0]['contact_number'] : '' }}">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="input-label">Country<span class="symbol required"></span></label>
                                                    <select name="country[0]" class="form-control search-select country" block-index="0">
                                                        <option value="">Select Your Country</option>
                                                        @foreach( \CommonHelper::countries() as $c_index => $country)
                                                            @if(isset($value['country']))
                                                                <option value="{{ $c_index }}" {{ isset($value['country']) ? $value['country'] == $c_index ? 'selected' : '' : ''}}> {{ $country }}</option>
                                                            @else
                                                                <option value="{{ $c_index }}" {{ $india_value == $c_index ? 'selected' : ''}}> {{ $country }}</option>
                                                            @endif
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group pincode-block pincode-block-0">
                                                    <label class="input-label" for="pin_code">Postal Code<span class="symbol required"></span></label>
                                                    <div>
                                                        <input type="hidden" class="pincode-id" name="pincode_id[0]" value="{{ isset($value['pincode_id']) ? $value['pincode_id'] : ''}}">
                                                        
                                                        <i class="fa fa-spin fa-refresh select-loader hide pincode-loader pincode-loader-0"></i>
                                                        <input type="text" block-index="0" data-form-id="institute-registration-form" class="form-control pincode pin_code_fetch_input" name="pincode[0]" value="{{ isset($value['pincode_text']) ? $value['pincode_text'] : ''}}" placeholder="Type your pincode">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group state-block state-block-0">
                                                    <label class="input-label">State<span class="symbol required"></span></label>
                                                    @if(isset($value))
                                                        @if($value['country'] == $india_value )
                                                        <select name="state[0]" class="form-control search-select state fields-for-india">
                                                            <option value="">Select Your State</option>
                                                            @foreach($value['pincode']['pincodes_states'] as $pincode_states)
                                                                <option value="{{$pincode_states['state_id']}}" {{$pincode_states['state_id'] == $value['state_id'] ? 'selected' : ''}}>{{ucwords(strtolower($pincode_states['state']['name']))}}</option>
                                                            @endforeach
                                                        </select>
                                                        <input type="text" id="state" name="state_name[0]" class="form-control state_text hide fields-not-for-india" placeholder="Enter State Name" value="">

                                                        @else
                                                            <select name="state[0]" class="form-control search-select state hide fields-for-india">
                                                                <option value="">Select Your State</option>
                                                            </select>
                                                            <input type="text" name="state_text[0]" class="form-control state_text fields-not-for-india" placeholder="Enter State Name" value="{{ isset($value['state_text']) ? $value['state_text'] : ''}}">
                                                        @endif
                                                    @else
                                                        <select name="state[0]" class="form-control search-select state fields-for-india">
                                                            <option value="">Select Your State</option>
                                                        </select>
                                                        <input type="text" id="state" name="state_text[0]" class="form-control hide state_text fields-not-for-india" placeholder="Enter State Name" value="{{ isset($value['state_text']) ? $value['state_text'] : ''}}">
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group city-block city-block-0">
                                                    <label class="input-label">City<span class="symbol required"></span></label>

                                                    @if(isset($value['country']))
                                                        @if( $value['country'] == $india_value )
                                                            <select name="city[0]" class="form-control city fields-for-india">
                                                                <option value="">Select Your City</option>
                                                                @foreach($value['pincode']['pincodes_cities'] as $pincode_city)
                                                                    <option value="{{$pincode_city['city_id']}}" {{$pincode_city['city_id'] == $value['city_id'] ? 'selected' : ''}}>{{ucwords(strtolower($pincode_city['city']['name']))}}</option>
                                                                @endforeach
                                                            </select>
                                                            <input type="text" name="city_text[0]" class="form-control city_text hide fields-not-for-india" placeholder="Enter City Name" value="">

                                                        @else
                                                            <select name="city[0]" class="form-control city hide fields-for-india">
                                                                <option value="">Select Your City</option>
                                                            </select>

                                                            {{--<input type="text" name="city_text[0]" class="form-control city_text fields-not-for-india" placeholder="Enter City Name" value="">--}}
                                                            <input type="text" name="city_text[0]" class="form-control city_text fields-not-for-india" placeholder="Enter City Name" value="{{ isset($value['city_text']) ? $value['city_text'] : ''}}">
                                                        @endif
                                                    @else
                                                        <select name="city[0]" class="form-control city fields-for-india">
                                                            <option value="">Select Your City</option>
                                                        </select>

                                                        {{--<input type="text" name="city_text[0]" class="form-control city_text fields-not-for-india" placeholder="Enter City Name" value="">--}}
                                                        <input type="text" name="city_text[0]" class="form-control city_text hide fields-not-for-india" placeholder="Enter City Name" value="{{ isset($value['city_text']) ? $value['city_text'] : ''}}">
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="col-sm-6">
                                                <div class="form-group address-block address-block-0">
                                                    <label class="input-label" for="address">Address<span class="symbol required"></span></label>
                                                    <textarea class="form-control address" name="address[0]" placeholder="Type your address" rows="6">{{ isset($value['address']) ? $value['address'] : ''}}</textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                                <div class="row" id="add-more-location-row">
                                    <div class="col-sm-12">
                                        <button type="button" data-form-id="institute-registration-form" class="btn add-institute-location-button location-button-0" id="add-location">Add Location</button>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 m_t_25">
                                        @if(Route::currentRouteName() == 'site.institute.registration')
                                              <input type="checkbox" id="agree" name="agree"/>
                                              <label class="terms-conditions" for="agree">
                                                I agree with the <a href="/terms&condition" target="_blank">terms and conditions</a>.
                                              </label>
                                            <div>
                                                <label for="agree" class="error"></label>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="reg-form-btn-container">
                            <button type="submit" data-style="zoom-in" class="reg-form-btn ladda-button blue institute-details-submit ladda-button-save" id="institute-details-submit"  style="margin-bottom:55px;">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>

    <script>
        initialize_pincode_maxlength_validation = '{{$initialize_pincode_maxlength_validation}}';
        var pincode_block_indexes = <?php echo json_encode($pincode_block_indexes)?>;

    </script>
    <script type="text/javascript" src="/js/site/institute-registration.js"></script>

@stop