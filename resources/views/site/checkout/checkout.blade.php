@extends('site.index')
@section('content')
<div class="checkout-page content-section-wrapper">
    <div class="container page-header">
        <h3>View Cart</h3>
    </div>
    <div class="shopping-cart container">
        <div class="row ">
            <div class="col-md-4">
                <b>Title</b>
            </div>
            <div class="col-md-2">
                <b>Duration</b>
            </div>
            <div class="col-md-2">
                <b>Quantity</b>
            </div>
            <div class="col-md-2">
                <b>Price</b>
            </div>
            <div class="col-md-2">
                <b>Total</b>
            </div>
        </div>
        <hr>
        <?php $total=0; ?>
        @foreach($cart_details as $cart_detail)
            <div id="cart_{{$cart_detail['id']}}">
                <div class="row" >
                    <div class="col-md-4">
                        <dl>
                            <dt>{{$cart_detail['subscription']['title']}}</dt>
                            <dd>{{$cart_detail['subscription']['description']}}</dd>
                        </dl>
                    </div>
                    <div class="col-md-2">
                        {{$cart_detail['subscription']['duration_title']}}
                    </div>
                    <div class="col-md-2">
                        {{$cart_detail['quantity']}}
                    </div>
                    <div class="col-md-2">
                        {{$cart_detail['price']}}
                    </div>
                    <div class="col-md-2">
                        {{ $cart_detail['quantity'] * $cart_detail['price']}}
                    </div>
                    <?php $total = $total + ($cart_detail['quantity'] * $cart_detail['price'])?>
                </div>
                <hr>
            </div>
        @endforeach
        <div class="row">
            <div class="col-md-8"></div>
            <div class="col-md-2"><h3>Total</h3></div>
            <div class="col-md-2">
                <h3 class="cart_total"><b>{{$total}}</b></h3>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <a class="btn btn-success pull-right" style="margin-right: 35px;" id="proceed-to-payment">Proceed To Payment</a>
                <a class="btn btn-success pull-right" style="margin-right: 35px;" href="{{ route('site.subscription.cart') }}">Go To Cart</a>
            </div>
        </div>
    </div>
    <div id="payumoney-form" style="display: none;"> 
    </div>
</div>

    <script type="text/javascript" src="/js/site/checkout.js"></script>
@stop