<form id="payumoney_form" action="{{ $parameters['payumoney_url']}}" class="hide" method="POST">
	  <input name="key" value="{{ $parameters['key'] }}" />
	  <input name="hash" value="{{ $parameters['hash'] }}"/>
	  <input name="txnid" value="{{ $parameters['txnid'] }}" />
	  <input name="amount" value="{{ $parameters['amount'] }}" />
	  <input name="productinfo" value="{{ $parameters['productinfo'] }}" />
	  <input name="surl" value="{{ $parameters['surl'] }}"/>
	  <input name="furl" value="{{ $parameters['furl'] }}"/>
	  <input name="firstname" value="{{ $parameters['firstname'] }}" />
	  <input name="email" value="{{ $parameters['email'] }}" />
	  <input name="phone" value="{{ $parameters['phone'] }}" />
</form>
