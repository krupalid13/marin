@extends('site.cms-index')
@section('content')
<div class="aboutus-pagecontainer" style="margin-top: 60px;">
    <div class="about-heading-container">
        <br>
        <h3 class="head1">Welcome to a smart platform for the marine community.</h3>
        <br>
        <h2 class="head2">Maintaining a professional status is now much simpler and enjoyable.</h2>
        <br>
    </div>

    <div class="about">
        <div class="col-about-data">
            <div class="col-title-about">
                <h1 class="a-title">About Us</h1>
            </div>
            <div class="about-data">
                <p class="au-data">Owner companies & manning companies, likewise are using flanknot.com to connect
                    to seafarers across the globe.</p>
                <p class="au-data1">From Captains and Chief Engineers to Deck cadets and Trainee Engineers,
                    thousands are coming on board to connect with
                    each other and find suitable jobs and required courses with the lowest price.</p>
                <br>
            </div>
        </div>
        <div class="col-alt-title-vision">
            <h1 class="aa-title">About Us</h1>
        </div>
        <div class="col-about-image">
            <img src="{{ asset('public/images/about-us/Collaboration.PNG') }}" alt="A Great Collaboration">
        </div>
    </div>
    <!--end about section-->


    <div class="vision">
        <div class="col-alt-title-vision">
            <h1 class="v-title">Our Vision</h1>
        </div>
        <div class="col-vision-image">
            <img src="{{ asset('public/images/about-us/Vision.PNG') }}" alt="Our Vision">
        </div>
        <div class="col-vision-data">
            <div class="col-title-vision">
                <h1 class="va-title">Our Vision</h1>
            </div>
            <div class="vision-data">
                <p class="v-data">Create progress opportunity for every seafarer of the global workforce.</p>
                <p class="v-data1">Helping seafarer organise their career past to empower their future.</p>
                <br>
            </div>
        </div>
    </div>
    <!--end vision section-->

    <div class="mission">
        <div class="col-mission-data">
            <div class="col-title-mission">
                <h1 class="m-title">The Mission</h1>
            </div>
            <div class="mission-data">
                <p class="m-data">The mission of course4sea is simple: connect the world’s seafarers to make them
                    more productive and successful.</p>
                <p class="m-data1">We live by our mission, improving seafarers working life through new
                    functionalities and
                    endeavours taken by our company.</p>
                <p class="m-data2">We’re building a platform and products we believe in.</p>
                <p class="m-data3">Building better ways to explore, find, and manage information about seafarers and
                    their
                    requirements because being informed matters.</p>
                <br>
            </div>
        </div>
        <div class="col-alt-title-vision">
            <h1 class="am-title">The Mission</h1>
        </div>
        <div class="col-mission-image">
            <img src="{{ asset('public/images/about-us/Mission.PNG') }}" alt="The Mission">
        </div>

    </div>
    <!--end mission section-->


    <div class="process">
        <div class="col-alt-title-process">
            <h1 class="ap-title">Our Process</h1>
        </div>
        <div class="col-process-image">
            <img src="{{ asset('public/images/about-us/Process.PNG') }}" alt="Our Process">
        </div>
        <div class="col-process-data">
            <div class="col-title-process">
                <h1 class="pa-title">Our Process</h1>
            </div>
            <div class="process-data">
                <p class="p-data">Flanknot is building the smartest online platform for seafarers.</p>
                <p class="p-data1">Earlier carrying or forwarding required documents to companies was such a hassle.
                    But now flanknot.com provides a free storage that not only stacks your e-documents in an
                    organised manner but also is very easy to share.</p>
                <p class="p-data2">Receive <span class="bold-p">Q</span>uick <span class="bold-p">R</span>esponses
                    on resumes sent.</p>
                <p class="p-data3">Always providing information of new job requirements to seafarers.</p>
                <p class="p-data4">Calculate and analytically displays details about your career.</p>
                <p class="p-data5">On flanknot.com save & share your sailing memories with other.</p>
                <br>
            </div>
        </div>
    </div>
    <!--end process section-->

    <div class="team">
        <div class="col-team-data">
            <div class="col-title-team">
                <h1 class="t-title">The Team</h1>
            </div>
            <div class="team-data">
                <p class="t-data">We are a strong, diverse team of curious, creative Sea and shore officers who have
                    worked in this industries for many years and want to bring together the experience and knowledge
                    of both sides to support each other in this collaboration for the betterment of this industry.
                </p>
                <p class="t-data1"><span class="bold-p">Our Leaders </span>includes some of the most inventive and
                    experienced CEO’s, directors and senior managers from this industry.</p>
                <p class="t-data2"><span class="bold-p">Technology</span> at Flanknot, we are utilizing the best
                    tech of cloud storage and securities, we are updating our algorithms frequently to ensures
                    better connectivity between seafarers, companies and institutes.</p>
                <p class="t-data3"><span class="bold-p">Suggestion</span> are always welcomed and encouraged, we are
                    always open to improve our concept and services. Feel free to suggest an idea or a functionality
                    that we can use. If it is implemented, you are rewarded for your contribution.</p>
                <p class="t-data4">For suggestion email us on <span class="email">idea@flanknot.com</span> </p>
                <p class="t-data5"> </p>
                <br>
            </div>
        </div>
        <div class="col-alt-title-vision">
            <h1 class="at-title">The Team</h1>
        </div>
        <div class="col-team-image">
            <img src="{{ asset('public/images/about-us/Team.PNG') }}" alt="Our Team">
        </div>
    </div>
    <!--end team section-->

    <div class="trust">
        <div class="col-alt-title-trust">
            <h1 class="atr-title">A Trust</h1>
        </div>
        <div class="col-trust-image">
            <img src="{{ asset('public/images/about-us/Trust.PNG') }}" alt="A Trust">
        </div>
        <div class="col-trust-data">
            <div class="col-title-trust">
                <h1 class="tr-title">A Trust</h1>
            </div>
            <div class="trust-data">
                <p class="tr-data">Flanknot is committed to earning trust every day. Since day one, we have
                    encouraged seafarers to use their real information, so companies can reach out to them. We also
                    have entrusted the shipping companies registered with us to showcase job requirements which are
                    only valid.</p>
                <p class="tr-data1">Flanknot is made up of real seafarers with real experience, shipping companies
                    with actual job openings and maritime institutes with courses at the lowest price. This creates
                    the trust and accountability that cultivates assurance online and into the real world.</p>
                <p class="tr-data2">We have a dedicated Trust and Safety team to ensure that:</p>
                <ul class="ul">
                    <li class="li">Document sharing and conversations are not indexed in search engines.</li>
                    <li class="li">Your documents are protected by password and encrypted by HTTPS.</li>
                    <li class="li">We do not share your name, address, or email address with advertisers.</li>
                </ul>
                <br>
            </div>
        </div>
    </div>
    <!--end trust section-->

    <div class="value">
        <div class="col-value-data">
            <div class="col-title-value">
                <h1 class="cv-title">Core Values</h1>
            </div>
            <div class="value-data">
                <p class="cv-data"><span class="bold-p">Balance</span></p>
                <p class="cv-data1">Our policies and web algorithms ensures that real jobs get applications from
                    genuine seafarers only.</p>
                <p class="cv-data2"><span class="bold-p">Collaboration</span></p>
                <p class="cv-data3">We endorse transparent communication and trust in each other. This oneness makes
                    this community work reliable.</p>
                <p class="cv-data4"><span class="bold-p">Continue Improvement</span></p>
                <p class="cv-data5">We are never done with improvement. We are always testing, enhancing and
                    listening to you and learning from the world around us, our leaders, our partners, our users and
                    employees.</p>
                <p class="cv-data6">Together we take new ideas to unexpected places.</p>
                <p class="cv-data7"><span class="bold-p">Empowerment</span></p>
                <p class="cv-data8">Path to success starts by trusting the community we belong to.</p>
                <br>
            </div>
        </div>
        <div class="col-alt-title-value">
            <h1 class="acv-title">Core Values</h1>
        </div>
        <div class="col-value-image">
            <img src="{{ asset('public/images/about-us/DemandSupply.PNG') }}" alt="Balancing Demand & Supply">
        </div>
    </div>
</div>
@stop

@section('addStyle')
<link rel="stylesheet" href="{{asset('public/assets/css/aboutus.css') }}">
@stop