@extends('site.index')
@section('content')
	<div class="about-us">
		<div class="about-us-banner">
			<div class="heading">
				About Us
			</div>
		</div>
		<div class="container">
			<div class="about-us-content-container">
				<div class="row">
					<div class="col-xs-12">
						<div class="content-para">
							ConsultanSeas is an exclusive platform for showcasing Job openings and resume database from the
							shipping industry. The medium is built to bring together opportunities for both employers and job
							seekers.
						</div>
						<div class="content-para">
							ConsultanSeas is an Indian Company, based at Navi Mumbai. The team behind ConsultanSeas comes
							with a seasoned expertise in Ship Management and Marine Industry. The dearth of a versaile and
							online platform to display job posting and resumes and a dedicated service portal catering to the
							‘ship management industry’ inspired them to form ConsultanSeas 2 years back.
						</div>
						<div class="content-para">
							ConsultanSeas is of the belief that there has always been a considerable gap between the job
							requirements and the job seekers. Some big shipping companies hold requirements but are not able
							to reach the applicants, where as at some instances middleman claim benefit for getting a job for an
							aspiring job seeker. ConsultanSeas is a direct medium where an applicant and employer can share a
							common ground, discuss, negotiate and work together.
						</div>
						<div class="content-para">
							Consultantseas is a one-point access point for seafarers, shipping companies, recruitment
							consultants, manning agents and maritime institutes. The module of this online portal is designed in
							a way to generate synergy. ConsultanSeas displays job opportunities, advertisements and resume
							database across the globe.
						</div>
						<div class="row">
							<div class="col-xs-12 col-sm-4 col-sm-offset-1">
								<div class="a-u-card">
									<div class="para-head text-center"><i class="fa fa-bullseye" aria-hidden="true"></i><div>Mission</div></div>
									<div class="para-content text-center">
										Bridging the gaps between Employers &amp; Job applicants by eliminating the culture of middlemen.
									</div>
								</div>
							</div>
							<div class="col-xs-12 col-sm-4 col-sm-offset-2">
								<div class="a-u-card">
									<div class="para-head text-center"><i class="fa fa-binoculars" aria-hidden="true"></i><div>Vision</div></div>
									<div class="para-content text-center">
										Create a strong platform for the shipping industry that serves as an exhaustive classified.
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-sm-4">
								<div class="content-para">
									<div class="para-head">USP</div>
									Our uniqueness is we are one of its kind portal that brings you all services pertaining to the Shipping
									Industry. Ship spares, Working gears, Doctors, Facilities, Consultants and everything you can name
									under one banner.
								</div>
							</div>
							<div class="col-xs-12 col-sm-4">
								<div class="content-para">
									<div class="para-head">Business Policy</div>
									Our approach to business has always been inclined towards Synergy. By synergising we are one
									powerful force and can work wonders across any need or requirement.
								</div>
							</div>
							<div class="col-xs-12 col-sm-4">
								<div class="content-para">
									<div class="para-head">Services</div>
									At ConsultanSeas, we offer a gamut of services suiting different requirements. It is one domain that
									brings different services together for the benefit of all.
								</div>
							</div>
						</div>
						<div class="feature-container">
						  <ul class="nav nav-tabs">
						  	<li class="small-feature-card active">
						  	<div class="test-arrow"></div>
						  		<a data-toggle="tab" href="#seafarer">
						  			<div class="feature-icon feature1">
									
									</div>
									<div class="feature-title hidden-xs">
										Seafarers
									</div>
						  		</a>
							</li>
							<li class="small-feature-card">
							<div class="test-arrow"></div>
						  		<a data-toggle="tab" href="#company">
						  			<div class="feature-icon feature2">
									
									</div>
									<div class="feature-title hidden-xs">
										Shipping Companies
									</div>
						  		</a>
							</li>
							<li class="small-feature-card">
							<div class="test-arrow"></div>
						  		<a data-toggle="tab" href="#advertiser">
						  			<div class="feature-icon feature3">
									
									</div>
									<div class="feature-title hidden-xs">
										Advertiser
								</div>
						  		</a>
							</li>
							<li class="small-feature-card">
							<div class="test-arrow"></div>
						  		<a data-toggle="tab" href="#institutes">
						  			<div class="feature-icon feature4">
									
									</div>
									<div class="feature-title hidden-xs">
										Institutes
									</div>
						  		</a>
							</li>
						  </ul>

						  <div class="tab-content">
						    <div id="seafarer" class="tab-pane fade in active">
							    <div class="tab-content-head visible-xs-block">
							    	SeaFarers
							    </div>
							    <div class="content-para">
									We are here to offer professional, safe and secure job requirements across the globe. Bring yourself to 400 plus shipping companies across the World. Our automated resume builder helps you generate a professional resume which can be downloaded in pdf format.
							    </div>
							    <div class="content-para">
							      	Our basic services come free. Job applicants can opt for premium services that come at a nominal
								cost and enjoy added benefits.
							    </div>
								<div class="tab-content-sub-head">
									Features
								</div>
								<ul style="font-size: 14px;padding-left: 14px;">
									<li>
										Register and Create Own Profile which can be updated any time anywhere in the World.
									</li>
									<li>
										In special cases, an applicant can notify the exact time availability onshore to attract the best
										opportunities within the stipulated time frame. (Applicant availability will be valid for 15
										days and service will expire if not accessed or updated.)
									</li>
									<li>
										Premium Member can avail the below benefits -
										<ul style="list-style-type: lower-latin;padding-left: 14px;">
											<li>
												Verification of your documents like Seaman’s Book and COC.
											</li>
											<li>
												The verified documents will be reflected on the top right of the resume highlighting your resume to employers.
											</li>
											<li>
												Automated alerts on documents about to expire.
											</li>
											<li>
											    Automated weekly notifications through email on vacancies posted by the Shipping Companies.
											</li>
											<li>
												Boosting profile on Company Search.
											</li>
											<li>
												Customer support &amp; assistance with resume generation.
											</li>
										</ul>
									</li>
								</ul>
						    </div>
						    <div id="company" class="tab-pane fade in">
						    	<div class="tab-content-head visible-xs-block">
							    	Shipping Companies
							    </div>
							    <div class="content-para">
									Shipping Companies can now avail of one platform that can provide exhaustive and exclusive database suiting to various recruitment needs. ConsultanSeas offers resumes that can be shortlisted on basis of availability, experience, particular type of ship, rank, documents verification, and various criteria. The resumes can be viewed online as well as downloaded in pdf format.
							    </div>
							    <div class="content-para">
							      	Shipping companies can also post Job requirement ads based on various criteria. Shipping companies can derive maximum benefits by opting for paid services.
							    </div>
								<div class="tab-content-sub-head">
									Features
								</div>
								<ul style="font-size: 14px;padding-left: 14px;">
									<li>
										Registering and creating profile.
									</li>
									<li>
										Posting regular Ads which will be displayed on ConsultanSeas site.
									</li>
									<li>
										Ads can be customised and designed if required by our team at a nominal charge.
									</li>
									<li>
										Enjoy free unlimited posting of specific Job requirements ( Validity 1 year).
									</li>
									<li>
										Job posting could be as per the Ranks, Date of Joining and Type of Vessel.
									</li>
									<li>
										Search for Seafarers as per Rank, Experience and Download resumes.
									</li>
									<li>
										Resumes can be downloaded on Company Profile and also email to registered email id.
									</li>
									<li>
										Updated and cerified resumes.
									</li>
									<li>
										Download resumes according to different plans monthly and yearly.
									</li>
								</ul>
						    </div>
						    <div id="advertiser" class="tab-pane fade in">
						    	<div class="tab-content-head visible-xs-block">
							    	Smart Advertisers
							    </div>
							    <div class="content-para">
									Smart advertiser is a platform provided by ConsultanSeas to bring you different everyday services to
									your benefit under one banner. While a shipping company would require associate services like
									hotel bookings for boarding / off-boarding staff, Travel arrangements, Visa, Medical and other
									services, they all can be found at ConsultanSeas. Trusted and verified vendors who display their
									product / service ads are smart advertisers. Smart advertisers represent different industries and
									services. Smart advertisers can advertise year round and attract business targeting shipping industry.
							    </div>
								<div class="tab-content-sub-head">
									Features
								</div>
								<ul style="font-size: 14px;padding-left: 14px;">
									<li>
										Advertisers can display their Company Profile.
									</li>
									<li>
										Display advertisements ranging from one to 4 Ads as per the scheme opted.
									</li>
									<li>
										Promote location specific advertisements.
									</li>
									<li>
										The advertisements can be kept in rolling with different creatives across the year.
									</li>
									<li>
										Smart Advertisers can cater to different requirements of the shipping industry.
									</li>
									<li>
										Create your advertisement with the aid of ConsultanSeas team.
									</li>
									<li>
										Professional support from ConsultanSeas team to promote and explore the business opportunities within the Shipping fraternity.
									</li>
								</ul>
						    </div>
						    <div id="institutes" class="tab-pane fade in">
						    	<div class="tab-content-head visible-xs-block">
							    	Maritime Institutes
							    </div>
							    <div class="content-para">
									Maritime Institutes can remain in constant touch with an exclusive database of Seafarers and Shipping Companies. ConsultanSeas offers a medium to Maritime Institutes to advertise on the various courses and offers from time to time.
							    </div>
								<div class="tab-content-sub-head">
									Features
								</div>
								<ul style="font-size: 14px;padding-left: 14px;">
									<li>
										Generate automated notifications to registered seafarers on course expiry every fortnight.
									</li>
									<li>
										Assist Seafarers to book courses in advance, at select location and available institutes.
									</li>
									<li>
										Display location specific advertisements that can attract seafarers.
									</li>
									<li>
										Display Time Table.
									</li>
								</ul>
						    </div>
						  </div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@stop