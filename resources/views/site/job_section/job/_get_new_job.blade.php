@extends('site.index')

@section('addStyle')
<link rel="stylesheet" type="text/css" href="{{asset('public/assets/css/_new_job.css')}}">			
<style type="text/css">
.hashag{color: green}
.add_border_class {
    border: 1px solid black;
    padding: 6px;
}
.add_border_class{margin-bottom: 5px}
strong.official_dsno {
    font-size: 15px;
}
</style>
@endsection
@section('content')
<?php 
  $countryData = \CommonHelper::countries();
?>
<div class="container" style="padding: 50px;margin-top: 60px">
	<section id="blog" class="blog">
            <div class="container">
                <div class="row">
                    <div class="col-md-11 form-group">
                        <a href="{{route("job.getUserJobHistoryList")}}" class="btn btn-primary" style="float: right;">Job History</a>
                    </div>
                </div>
	      <div class="row">
	        <div class="col-lg-8 entries col-md-offset-1">
	            
					@if(isset($getAllJob) && !$getAllJob->isEmpty())
					    @foreach($getAllJob as $key=>$job)
					        <article class="entry" data-aos="fade-up">
					          @if($job->courseJob->jobStatus !=null)
						          <div class="statusJ">
						          	<span class="label label-primary">{{$job->courseJob->jobStatus->name}}</span>
						          </div>
					          @endif
					          <div class="pull-right">
					          	{{ date('d-M-Y',strtotime($job->created_at)) }}
					          	@if($job->courseJob->source !=null)
						          	<br>
						          	<a href="javascript:void(0)" data-source="{{$job->courseJob->source}}" class="get_source">Source</a>
					          	@endif
					          </div>
							  <h2 class="entry-title">
					            <a href="">{{$job->courseJob->title}}</a>
					          </h2>
				              	@if($job->courseJob->company !=null)	
						        	<div class="add_border_class">
							          <div class="entry-meta">
							            <ul>
							              <li class="d-flex align-items-center"><strong class="official_dsno">{{$job->courseJob->company->name}}</strong></li>
							              
							              @if($job->courseJob->companyEmailAddress !=null)	
							              	<li class="d-flex align-items-center">Email: <a href="mailto:{{$job->courseJob->companyEmailAddress->email}}">{{$job->courseJob->companyEmailAddress->email}}</a></li>
										  @endif

							              @if($job->courseJob->company->website !=null)	
							              	<li class="d-flex align-items-center">websie:<a href="{{ $job->courseJob->company->website }}" target="_blank">{{$job->courseJob->company->website}}</a></li>
										  @endif
										  <br>
										  @if($job->courseJob->company->official_no !=null)	
							              	<li class="d-flex align-items-center">Official No:{{$job->courseJob->company->official_no}}</li>
										  @endif
							            </ul>
							          </div>
						              	@if($job->courseJob->company->address !=null)	
								          <div class="entry-meta">
								            <ul>
								              <li class="d-flex align-items-center">{{$job->courseJob->company->address}}</li>
								            </ul>
								          </div>
						          		@endif
					          		</div>
					            @endif

					            @if($job->courseJob->message !=null)
						          <div class="entry-content">
						          	<h5>About Job</h5>
						          	{{$job->courseJob->message}}
						          </div>
						        @endif
					          
					          <div class="entry-content">
					          	<h5>Requirement</h5>
					          	<div class="requirement">
						          	@if($job->courseJob->rank_id !=null)
						          	<span class="hashag">#</span>Rank - {{getRankCommonFuncion($job->courseJob->rank_id)}}
									@endif
									@if(!$job->courseJob->jobMultiplePassportCountry->isEmpty())
										<?php 
											$passCounry = $job->courseJob->jobMultiplePassportCountry->pluck('passport_country_id')->toArray();
										?>
										<span class="hashag">#</span>Passport Country - {{getMultipleCountryName($passCounry)}}  
									@endif
									
									@if(!empty($job->courseJob->coc_country_id))
                    					<span class="hashag">#</span>COC Country:
			                            @if(in_array($job->courseJob->coc_country_id,array_keys($countryData)))
			                                {{$countryData[$job->courseJob->coc_country_id]}}
			                            @endif

			                            @if($job->courseJob->coc_experience_year !=0 && $job->courseJob->coc_experience_month !=0)
											{{getCountryWithExperience($job->courseJob->coc_experience_year,$job->courseJob->coc_experience_month)}}	
			                            @endif


			                        @endif

			                    	@if(!empty($job->courseJob->dp_country_id))
                            			<span class="hashag">#</span>DP Counttry:
			                            @if(in_array($job->courseJob->dp_country_id,array_keys($countryData)))
			                                {{$countryData[$job->courseJob->dp_country_id]}}
			                            @endif

			                            @if($job->courseJob->dp_experience_year !=0 && $job->courseJob->dp_experience_month !=0)
											{{getCountryWithExperience($job->courseJob->dp_experience_year,$job->courseJob->dp_experience_month)}}	
			                            @endif

									@endif

			                        @if(!empty($job->courseJob->coe_country_id))
                            			<span class="hashag">#</span>COE Counttry:
			                            @if(in_array($job->courseJob->coe_country_id,array_keys($countryData)))
			                                {{$countryData[$job->courseJob->coe_country_id]}}
			                            @endif

			                            @if($job->courseJob->coe_experience_year !=0 && $job->courseJob->coe_experience_month !=0)
											{{getCountryWithExperience($job->courseJob->coe_experience_year,$job->courseJob->coe_experience_month)}}	
			                            @endif


			                        @endif

			                        @if(!empty($job->courseJob->gmdss_country_id))
                            			<span class="hashag">#</span>GMDSS Counttry:
			                            @if(in_array($job->courseJob->gmdss_country_id,array_keys($countryData)))
			                                {{$countryData[$job->courseJob->gmdss_country_id]}}
			                            @endif

			                            @if($job->courseJob->gmdss_experience_year !=0 && $job->courseJob->gmdss_experience_month !=0)
											{{getCountryWithExperience($job->courseJob->gmdss_experience_year,$job->courseJob->gmdss_experience_month)}}	
			                            @endif
									@endif

			                        @if(!empty($job->courseJob->cop_country_id))
                            		    <span class="hashag">#</span>COP Country:
                            		    @if(in_array($job->courseJob->cop_country_id,array_keys($countryData)))
			                                {{$countryData[$job->courseJob->cop_country_id]}}
			                            @endif

			                            @if($job->courseJob->cop_experience_year !=0 && $job->courseJob->cop_experience_month !=0)
											{{getCountryWithExperience($job->courseJob->cop_experience_year,$job->courseJob->cop_experience_month)}}	
			                            @endif
									@endif



									@if(isset($job->courseJob->courseJobDce) && !$job->courseJob->courseJobDce->isEmpty())
										<span class="hashag">#</span>DCE:
			                            @foreach($job->courseJob->courseJobDce as $key=>$v)
			                                    @if($v->dce !=null)
			                                        {{ $v->dce->name }}
			                                        @if($job->courseJob->courseJobDce->reverse()->keys()->first() != $key) 
			                                           <b> | </b> 
			                                        @endif
			                                    @endif
			                            @endforeach
									@endif

									@if(isset($job->courseJob->jobMultipleCourse) && !$job->courseJob->jobMultipleCourse->isEmpty())
			                        	<span class="hashag">#</span>Course:
			                            @foreach($job->courseJob->jobMultipleCourse as $key=>$v)
			                                    @if($v->course !=null)
			                                        {{ $v->course->course_name }}
			                                        @if($job->courseJob->jobMultipleCourse->reverse()->keys()->first() != $key) 
			                                           <b> | </b> 
			                                        @endif
			                                    @endif
			                            @endforeach
									@endif

									@if(isset($job->courseJob->jobMultipleShipType) && !$job->courseJob->jobMultipleShipType->isEmpty())
			                            <?php $shipType = \CommonHelper::ship_type(); ?>
			                            <span class="hashag">#</span>Experience Ship Type:
			                            @foreach($job->courseJob->jobMultipleShipType as $key=>$v)
			                                    @if(in_array($v->experience_ship_type_id,array_keys($shipType)))
			                                        {{ $shipType[$v->experience_ship_type_id] }}
			                                        @if($job->courseJob->jobMultipleShipType->reverse()->keys()->first() != $key) 
			                                           <b> | </b> 
			                                        @endif
			                                    @endif
			                            @endforeach
									@endif
									@if(isset($job->courseJob->jobMultipleEngineType) && !$job->courseJob->jobMultipleEngineType->isEmpty())
			                            <?php $engineType = \CommonHelper::engine_type(); ?>
			                            @foreach($job->courseJob->jobMultipleEngineType as $key=>$v)
			                                    @if(in_array($v->experience_engine_type_id,array_keys($engineType)))
			                                        {{ $engineType[$v->experience_engine_type_id] }}
			                                        @if($job->courseJob->jobMultipleEngineType->reverse()->keys()->first() != $key) 
			                                           <b> | </b> 
			                                        @endif
			                                    @endif
			                            @endforeach
									@endif
									@if(!empty($job->certification))
			                    		<span class="hashag">#</span>Character Certificate:
			                    		@if($job->certification ==  1)
			                    		Yes
			                    		@else
			                    		No
			                    		@endif
			                    	@endif
									@if(!empty($job->getVaccination))
										<span class="hashag">#</span>Vaccination:
			                    		{{$job->getVaccination->name}}
			                    	@endif
									
					          	</div>
					          </div>
					            <div class="apply_buton">
					          	  @if($job->applied == 2)
					              	<a href="javascript:void(0)" data-job-id="{{\Crypt::encrypt($job->id)}}" class="btn btn-primary applied_job">APPLY</a>
								  @else
								  	<span class="label label-success">APPLIED</span>
					          	  @endif	
					          	  <br><br>
					          	  Valid till: {{$job->courseJob->valid_till_date}}
					            </div>
					        </article>
					        <hr>
					    @endforeach
					    <div class="blog-pagination">
					        {!! $getAllJob->links() !!}
					    </div>
					@else
                                            @if(isset($seamanBookDetail[0]['seaman_book_detail']) && !empty($seamanBookDetail[0]['seaman_book_detail']))
                                                <div class="data_not_found text-center">Sorry ! We could not find any valid job openings for you !!</div>
                                            @else
                                                <div class="mt-30 panel panel-bricky">
                                                    <div class="panel-heading text-center">
                                                        <h4 class="h4">To view latest jobs. Kindly <a href="{{route('site.seafarer.edit.profile')}}">edit profile.</a> </h4>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <img class="img-responsive center-block graph-img" src="{{ URL:: asset('public/images/new_jobs.png')}}">
                                                </div>
                                            @endif
					@endif        	

	        </div>
		  </div>
	    </div>
  	</section>
</div>

@stop

@section('js_script')
	<script type="text/javascript" src="/js/site/registration.js"></script>
	<script type="text/javascript" src="/js/site/company-registration.js"></script>
	<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
	
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-jgrowl/1.4.7/jquery.jgrowl.min.js"></script>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-jgrowl/1.4.7/jquery.jgrowl.css" />

	<script>

		$(document).on("click",".applied_job",function(){

			var sendJobId = $(this).attr('data-job-id'); 
			swal({
                title: "Are you sure want to apply job ?",
                icon: "info",
                buttons: true,
                dangerMode: true,
                })
                .then((apply) => {
                  if (apply) {
					$.ajax({
                        type: "POST",
                        url: "{{route('site.seafarer.job.applied')}}",
                        data:{"_token": "{{ csrf_token() }}",send_job_id:sendJobId},
                        success: function (response) {
                            if(response.success == 1){

                                location.reload();
                                
                            }else{
                            	// return false;
                                flashMessage('danger', response.msg);
                            }
                        },
                        error: function (jqXhr) {
                      }
                    });
                }
          	});

		});

		$(document).on("click",".get_source",function(){

			var attribue = $(this).attr('data-source');
			swal(attribue);

		});

	</script>
	@include('admin.partials.flashmessage')
@stop