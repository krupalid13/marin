@extends('site.index')

@section('content')
	
	<div class="row">
        <div class="col-md-offset-1 col-md-10">
            <div class="jumbotron how-to-create" >

                <h3>Images <span id="photoCounter"></span></h3>
                <br />

                <form class="dropzone" id="real-dropzone" files="true" action="{{route('site.user.upload')}}">
                	{{ csrf_field() }}
	                <div class="dz-message">

	                </div>

	                <div class="fallback">
	                    <input name="file" type="file" multiple />
	                </div>

	                <div class="dropzone-previews" id="dropzonePreview"></div>

	                <h4 style="text-align: center;color:#428bca;"> Drop images in this area  <span class="glyphicon glyphicon-hand-down"></span></h4>

                </form>

            </div>
            <div class="jumbotron how-to-create">
                <ul>
                    <li>Images are uploaded as soon as you drop them</li>
                    <li>Maximum allowed size of image is 8MB</li>
                </ul>
            </div>
        </div>
    </div>

    <!-- Dropzone Preview Template -->
    <div id="preview-template" style="display: none;">

        <div class="dz-preview dz-file-preview">
            <div class="dz-image"><img data-dz-thumbnail=""></div>

            <div class="dz-details">
                <div class="dz-size"><span data-dz-size=""></span></div>
                <div class="dz-filename"><span data-dz-name=""></span></div>
            </div>
            <div class="dz-progress"><span class="dz-upload" data-dz-uploadprogress=""></span></div>
            <div class="dz-error-message"><span data-dz-errormessage=""></span></div>
        </div>
    </div>
    <!-- End Dropzone Preview Template -->
@stop
@section('js_script')
	<script type="text/javascript" src="/js/site/registration.js"></script>
@stop