@php
    $user_id=Auth::id();
    $bank_flag=true;
    $vaccination=true;
@endphp
@extends('layouts.main')
@section('content')
<style>
    .affiliate{margin:10px;}
</style>
    <div class="container">
      
        <div class="row">
            <br/>
            <div class="panel panel-footer text-center mt-2">
                Affiliate Form
            </div>
          
                <hr/>
                <form id="affiliate_form">
                      <div class="radio-inline">
                        <h5>  How did you get to know about Flanknot.com?</h5>
                    </div>
                    <input type="hidden" name="_token" value="{{csrf_token()}}" />  
                    <input type="hidden" name="url" id="url" value="{{route('user.add_affiliate_form')}}">     
                   <input type="hidden" name="redirectUrl" id="redirectUrl" value="{{route('user.profile')}}">     
                      <div class="col-12">
                      
                                <ul class="m-b-5" >
                                    <div class="col-md-12 affiliate">
                                        <label class="radio-inline" style="margin-top: 5px; color: grey;">
                                            <input type="radio" class="referralOption"  name="referal" value="1" id="referal_id" checked>
                                            Refered By &nbsp;&nbsp;&nbsp;
                                            <input type="text" name="referalCode" id="referalCode" placeholder="Enter Referral code"> 
                                            <span class="error" id="referal_name_error"></span>
                                            
                                        </label>
                                    </div>
                                      <div class="col-md-12 affiliate">
                                        <label class="radio-inline" style="margin-top: 0px; color: grey;">
                                            <input type="radio" class="referralOption"  name="referal" value="2">
                                       Through an Invitation
                                        </label>
                                    </div>
                                      <div class="col-md-12 affiliate">
                                        <label class="radio-inline" style="margin-top: 0px; color: grey;">
                                            <input type="radio" class="referralOption" name="referal" value="3">
                                            By a Seafaring Friend
                                        </label>
                                    </div>
                                      <div class="col-md-12 affiliate">
                                        <label class="radio-inline" style="margin-top: 0px; color: grey;">
                                            <input type="radio" class="referralOption" name="referal" value="4">
                                       Received Promotional Email
                                        </label>
                                    </div>  
                                    <div class="col-md-12 affiliate">
                                        <label class="radio-inline" style="margin-top: 0px; color: grey;">
                                            <input type="radio" class="referralOption" name="referal" value="5">
                                       Received Promotional Whatsapp/SMS
                                        </label>
                                    </div>  
                                     <div class="col-md-12 affiliate">
                                        <label class="radio-inline" style="margin-top: 0px; color: grey;">
                                            <input type="radio" class="referralOption" name="referal" value="6">
                                      Seen an advertisement
                                        </label>
                                    </div>                                  

                                </ul>
                            
                            <div class="panel panel-footer text-center">
                                  <button class="btn btn-primary" type="submit"  id="submit_affiliate_form">Submit</button>
                            </div>
                
                </form>
          
        </div>
        <div class="clearfix"></div>
    </div>
    <script>

    $('.referralOption').change('on',function(e){
           var referralOptionValue = $(this).val();
           if(referralOptionValue != 1){
                $('#referalCode').val('');
                $('#referalCode').attr('disabled',true);
           }else{
                $('#referalCode').removeAttr('disabled');
           }
    });

    $('#submit_affiliate_form').click('on',function(e){
        e.preventDefault();
        if(!$("input:radio[name='referal']").is(":checked")) {
            toastr.error('Please select any option', 'Error!');
            return false;
        }

        var referalName;
        if( $('#referal_id').is(':checked')){
            referalName = $('#referalCode').val();
            if(referalName == ''){
                $('#referal_name_error').html('Please enter code!');
                return false;
            }
        }
      
        $.ajaxSetup({
          headers: {
            'X-CSRF-TOKEN': $("input[name=_token]").val()
          }
        });
        var form = $('#affiliate_form');
         var formData = new FormData($('#affiliate_form')[0]);
        $.ajax({
            url:$('#url').val(),
            type: 'POST',           
            dataType : 'json',
            data: formData,
            processData: false,
            contentType: false,
            beforeSend: function () {
                    $(form).find('button[type="submit"]').html('Submit').attr('disabled');
            },
            success: function (response) {
                $(form).find('button[type="submit"]').html('Submit').removeAttr('disabled');
                if(response.status == 'success'){
                    toastr.success(response.message, 'Success');
                    setTimeout(function () {
                        window.location.href = $('#redirectUrl').val();
                    }, 1000);
                }else{
                     toastr.error(response.message, 'Error!');
                }
                    
            },
            error: function(response) {
                    toastr.error(response.message, 'Error!');
                    $(form).find('button[type="submit"]').html('Submit').removeAttr('disabled');
            },
        });
        
    });
    </script>
@stop
