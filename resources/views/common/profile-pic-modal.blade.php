<div id="profile-photo-modal" aria-hidden="true" aria-labelledby="add-doctor-profile-pic-modal-label" role="dialog" tabindex="-1"
     class="modal fade site-default-modal-style">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 id="add-doctor-profile-pic-modal-label" class="modal-title">Change Profile Pic</h4>
            </div>
            <div class="modal-body">
                <div class="form-group clearfix">
                    <div class="col-sm-12">
                        <div class="" style="position: relative;">
                            <div id="avatar-holder" class="">
                                <img class="vertical-align" src="{{ asset('images/user_default_image.png') }}" alt=""/>
                            </div>
                            <div id="profile-pic-crop-area-wrapper">
                                <div id="profile-pic-crop-area" class="" style="position:relative;display:none;">
                                    <div style="position:relative;max-width: 100%;margin-right:auto;margin-left:auto;">
                                        <img id="profile-pic-preview" src="" style="max-width: 100%;">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <a id="cancel-profile-pic-change" class="cursor-pointer btn margin-right-10 link-button default-cancel-button-style">
                    Cancel
                </a>
                <button id="save-profile-pic-details" class="btn action-button ladda-button default-site-button-style" data-style="zoom-in" type="button">
                    Save & Close
                </button>
            </div>
        </div>
    </div>
</div>