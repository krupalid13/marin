<?php

return [
	'feature1' => 'cv-download',
	'feature2' => 'email',
	'feature3' => 'visible-advertise',
	'candidateDownloadCV' => 'feature1',
	'changeAdvertisementStatus' => 'feature3',
	'storeAdvertise' => 'feature3',
];