<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/preemail', 'Site\UserController@preemail')->name('preemail');

/*Route::get('/tejalsachin', ['as'=>'pass' , 'uses' => 'HomeController@test123']);
Route::get('/tms/{id?}', ['as'=>'pass' , 'uses' => 'HomeController@sendsms']);
Route::get('/ts/{file?}', ['as'=>'ts' , 'uses' => 'HomeController@ts']);
Route::get('/love', ['as'=>'pass' , 'uses' => 'HomeController@love']);*/

Route::get('/', ['as'=>'home' , 'uses' => 'HomeController@home']);
Route::get('/register', ['as'=>'register' , 'uses' => 'HomeController@register']);
Route::get('/uploads1', ['as'=>'home' , 'uses' => 'WebsiteController@uploads']);
Route::get('/uploads', ['as'=>'home' , 'uses' => 'WebsiteController@uploads']);
Route::get('/my-profile',['as'=>'home', 'uses'=>'WebsiteController@profileview']);

//Route::get('/', function () {
    // return redirect()->intended('/courses');
//});

Route::get('/.env', function () {
    return view('errors.404');
});

Route::get("/homepage-new", function(){
   return view('site.homepage-new');
});
Route::post('/otp-verify', ['as' => 'otp.verify', 'uses' => 'UserController@otpVerify']);
Route::get('/home', ['as'=>'home' , 'uses' => 'HomeController@home']);
Route::get('/services', ['as'=>'services' , 'uses' => 'HomeController@services']);
Route::get('/about-us', ['as'=>'aboutUs' , 'uses' => 'HomeController@aboutUs']);
Route::get('/pricing', ['as'=>'pricing' , 'uses' => 'HomeController@pricing']);
Route::get('/disclaimer', ['as'=>'disclaimer' , 'uses' => 'HomeController@disclaimer']);
Route::get('/faq', ['as'=>'faq' , 'uses' => 'HomeController@faq']);
Route::get('/terms&condition', ['as'=>'tnc' , 'uses' => 'HomeController@tnc']);
Route::get('/report', ['as'=>'report' , 'uses' => 'HomeController@report']);
Route::get('/privacy-policy', ['as'=>'privacy_policy' , 'uses' => 'HomeController@privacy_policy']);
Route::get('/cookie-policy', ['as'=>'cookie-policy' , 'uses' => 'HomeController@cookie_policy']);
Route::get('/company-inquiry', ['as'=>'company-inquiry' , 'uses' => 'HomeController@company_inquiry']);
Route::get('/institutes-inquiry', ['as'=>'institutes-inquiry' , 'uses' => 'HomeController@institutes_inquiry']);
Route::post('/send-inquiry', ['as'=>'post-inquiry' , 'uses' => 'HomeController@post_inquiry']);
Route::post('/send-report', ['as'=>'send-report' , 'uses' => 'HomeController@send_report']);
Route::get('/contact-us', ['as'=>'site.contact-us' , 'uses' => 'HomeController@contact_us']);
Route::get('/how-to', ['as'=>'site.how-to' , 'uses' => 'HomeController@how_to']);

// routes by mukhtar
Route::get('/my-subcription', ['as'=>'mySubcription' , 'uses' => 'HomeController@mySubcription']);
Route::get('/job-application-listing', ['as'=>'jobApplicationListing' , 'uses' => 'HomeController@jobApplicationListing']);
Route::get('/add-job-post', ['as'=>'addJobPost' , 'uses' => 'HomeController@addJobPost']);
Route::get('/candidate-search', ['as'=>'candidateSearch' , 'uses' => 'HomeController@candidateSearch']);
Route::any('/search-listing', ['as'=>'searchListing' , 'uses' => 'HomeController@searchListing']);
Route::post('/seaman-book/enquiry', ['as' => 'site.seaman.book.enquiry', 'uses' => 'HomeController@seamanBookEnquiry']);

// end

//Route::any('/test', ['as' => 'test', 'uses' => 'HomeController@test']);

Route::group(['prefix' => 'admin', 'namespace' => 'Admin'], function(){

    Route::get('/login', ['as'=>'admin.login' , 'uses' => 'LoginController@login']);

    Route::get('/logout', ['as'=>'admin.logout' , 'uses' => 'LoginController@logout']);

    Route::post('/check-admin-login', ['as' => 'check.admin.login' , 'uses' => 'LoginController@checkLogin']);
    
    Route::get('/state/{id}', ['as' => 'admin.state.list', 'uses' => 'CompanyController@city_list']);

    Route::get('/download/excel', ['as' => 'admin.download.excel', 'uses' => 'SeafarerController@downloadXL']);

    Route::get('/company/download/excel', ['as' => 'admin.download.company.excel', 'uses' => 'CompanyController@downloadXL']);

    Route::get('/seafarer/company', ['as' => 'site.get.company.name' , 'uses' => 'UserController@getUserShippingCompanyWithId']);
    Route::get('/seafarer/ship/name', ['as' => 'site.get.company.name' , 'uses' => 'UserController@getUserShipName']);

    Route::post('/delete-service-details', ['as' => 'admin.user.delete.service.details', 'uses' => 'SeafarerController@deleteServiceDetails']);

    Route::post('/get-service-details', ['as' => 'admin.user.get.service.details', 'uses' => 'SeafarerController@getSeaServiceDetails']);

    Route::post('/store-single-service-details', ['as' => 'admin.user.store.single.service.details', 'uses' => 'SeafarerController@storeSpecificServiceDetails']);

    Route::group(['middleware'=>'auth','middleware'=>'checkRole:admin'], function (){


        // Company ~Chirag G
        
        Route::any('job/company/get', 'CompanyController@getAjaxCompanyList')->name('admin.getAjaxCompanyList');
        Route::any('job/company/index', 'CompanyController@getCompanyList')->name('admin.getCompanyList');
        Route::any('job/company/create', 'CompanyController@createCompany')->name('admin.createCompany');
        Route::any('job/company/edit/{id}', 'CompanyController@editcompanyDetail')->name('admin.editCompany');
        Route::any('job/company/destroy/{id}', 'CompanyController@destroyCompany')->name('admin.destroyCompany');
        Route::any('job/company/store', 'CompanyController@storeCompanyDetail')->name('admin.storeCompany');
        Route::any('job/company/{id}/update', 'CompanyController@updateCompanyDetail')->name('admin.updateCompany');
        Route::any('job/company/{id}/delete', 'CompanyController@deleteCompanyDetail')->name('admin.deleteCompanyDetail');
        Route::any('job/company/{id}/company_address/delete', 'CompanyController@deleteCompanyEmailAddress')->name('admin.deleteCompanyEmailAddress');

        ///////////Job posting///////////
        
        Route::any('j/job/getCompanyEmailAddress', 'CourseJobController@getCompanyEmailAddress')->name('admin.j.getCompanyEmailAddress');
        
        // Job
        Route::any('j/job/get', 'CourseJobController@get')->name('admin.j.get');
        Route::any('j/job/index', 'CourseJobController@index')->name('admin.j.index');

        // Get Job Preview
        Route::any('j/job/preview', 'CourseJobController@preview')->name('admin.j.preview');

        // Send
        Route::any('j/job/sen/get', 'CourseJobController@sendGet')->name('admin.j.sendGet');
        Route::any('j/job/send', 'CourseJobController@sendIndex')->name('admin.j.sendIndex');
        Route::any('j/job/job_send_to_user', 'CourseJobController@jobSendToUser')->name('admin.j.jobSendToUser');


        // View All
        Route::any('j/job/getViewAllJob', 'CourseJobController@getViewAllJob')->name('admin.j.getViewAllJob');
        Route::any('j/job/viewJobAll', 'CourseJobController@viewJobAll')->name('admin.j.viewJobAll');

        // View Company
        Route::any('j/job/getViewCompanyJob', 'CourseJobController@getViewCompanyJob')->name('admin.j.getViewCompanyJob');
        Route::any('j/job/viewJobCompany', 'CourseJobController@viewJobCompany')->name('admin.j.viewJobCompany');

        // View User
        Route::any('j/job/getViewAllUser', 'CourseJobController@getViewAllUser')->name('admin.j.getViewAllUser');
        Route::any('j/job/viewJobUser', 'CourseJobController@viewJobUser')->name('admin.j.viewJobUser');

        // Job 
        Route::any('j/job/create', 'CourseJobController@create')->name('admin.j.create');
        Route::any('j/job/edit/{id}', 'CourseJobController@edit')->name('admin.j.edit');
        Route::any('j/job/{id}/destroy', 'CourseJobController@destroy')->name('admin.j.destroy');
        Route::any('j/job/store', 'CourseJobController@store')->name('admin.j.store');
        Route::any('j/job/{id}/update', 'CourseJobController@update')->name('admin.j.update');
        


        // End Chirag G
    
        Route::post('/check/email', ['as' => 'admin.check.email', 'uses' => 'SeafarerController@checkEmail']);

        Route::post('/check/mobile', ['as' => 'admin.check.mobile', 'uses' => 'SeafarerController@checkMobile']);

        Route::post('/reset-password', ['as' => 'admin.reset.password' , 'uses' => 'LoginController@resetPassword']);

        Route::get('/reset-password/{email}', ['as' => 'admin.set.new.password' , 'uses' => 'LoginController@setNewPassword']);

        Route::post('/change-password', ['as' => 'admin.change.password' , 'uses' => 'LoginController@changePassword']);

        Route::get('/dashboard', ['as' => 'admin.dashboard', 'uses' => 'DashboardController@dashboard']);

        Route::get('/requested/document/list', ['as' => 'admin.seafarer.requested.document.list', 'uses' => 'SeafarerController@getRequestedDocumentList']);

        Route::get('/add/seafarer', ['as' => 'admin.add.seafarer', 'uses' => 'SeafarerController@addSeafarer']);

        Route::get('/pincode/get/states_cities', ['as' => 'admin.pincode.get.states_cities', 'uses' => 'PincodeController@getStatesCities']);

        Route::post('/seafarer/store/profile', ['as' => 'admin.seafarer.store.profilePic' , 'uses' => 'SeafarerController@storeProfilePic']);
        
        Route::any('/seafarer/store/{type}/{id?}', ['as' => 'admin.store.seafarer' , 'uses' => 'SeafarerController@storeSeafarerDetails']);

        Route::get('/edit/seafarer/{id}', ['as' => 'admin.edit.seafarer', 'uses' => 'SeafarerController@editSeafarer']);

        Route::post('/update/seafarer/{id}/{type}', ['as' => 'admin.update.seafarer', 'uses' => 'SeafarerController@updateSeafarer']);

        Route::get('/search/seafarer', ['as' => 'admin.view.seafarer', 'uses' => 'SeafarerController@viewSeafarer']);

        Route::post('/seafarer/status', ['as' => 'admin.change.seafarer.status', 'uses' => 'SeafarerController@changeStatus']);

        Route::post('/seafarer/resend-welcome-email', ['as' => 'admin.send.seafarer.welcome.email', 'uses' => 'SeafarerController@resendWelcomeEmail']);

        Route::post('/store-seafarer-course-details', ['as' => 'admin.user.store.seafarer.course.details', 'uses' => 'SeafarerController@storeSeafarerCourseDetails']);

        Route::post('/delete-seafarer-course-details', ['as' => 'admin.user.seafarer.delete.details', 'uses' => 'SeafarerController@deleteSeafarerCourseDetails']);

        Route::get('/get-seafarer-course-details', ['as' => 'admin.user.seafarer.get.course.details', 'uses' => 'SeafarerController@getSeafarerCourseByCourseId']);

        Route::get('/get-seafarer-course-by-type', ['as' => 'admin.user.seafarer.get.course.by.course.type', 'uses' => 'SeafarerController@getSeafarerCoursesByCourseType']);

        Route::get('/add/company', ['as' => 'admin.add.company', 'uses' => 'DashboardController@addCompany']);

        Route::get('/add/company/job', ['as' => 'admin.add.company.jobs', 'uses' => 'JobController@addCompanyJob']);

        Route::get('/edit/company/job/{company_id}/{job_id}', ['as' => 'admin.edit.company.jobs', 'uses' => 'JobController@editCompanyJob']); 

        Route::post('/update/company/job/{job_id}', ['as' => 'admin.update.company.job', 'uses' => 'JobController@updateCompanyJob']);

        Route::post('/view/disable/{job_id?}', ['as' => 'admin.company.disable.jobs', 'uses' => 'JobController@disableJob']);

        Route::post('/view/enable/{job_id?}', ['as' => 'admin.company.disable.jobs', 'uses' => 'JobController@enableJob']);

        Route::post('/store/job', ['as' => 'admin.store.company.job', 'uses' => 'JobController@store']);

        Route::get('/view/jobs', ['as' => 'admin.view.company.jobs', 'uses' => 'JobController@viewALLJobs']);

        Route::get('/job/applicant', ['as' => 'admin.view.job.applicant', 'uses' => 'JobController@viewALLJobApplicant']);

        Route::get('/company/subscriptions', ['as' => 'admin.view.company.subscriptions', 'uses' => 'SubscriptionController@viewALLCompanySubscriptions']);

        Route::any('/get/ships/{id}', ['as' => 'admin.get.company.ships', 'uses' => 'JobController@getShipDetailsByCompanyId']);

        Route::post('/company/ships/{company_id}/{ship_id}', ['as' => 'admin.company.related.ships', 'uses' => 'JobController@getShipDetailsByshipId']);

        Route::post('/company/store/profile', ['as' => 'admin.company.store.profilePic' , 'uses' => 'CompanyController@storeProfilePic']);
        
        Route::post('/company/store/advertise', ['as' => 'admin.company.advertise.store', 'uses' => 'CompanyController@storeAdvertise']);

        Route::any('/company/store/{type}/{id?}', ['as' => 'admin.store.company' , 'uses' => 'CompanyController@storeCompanyDetails']);

        Route::get('/edit/company/{id}', ['as' => 'admin.edit.company', 'uses' => 'CompanyController@editcompany']);

        Route::post('/update/company/{type}/{id}', ['as' => 'admin.update.company', 'uses' => 'CompanyController@updateCompany']);

        Route::get('/view/company', ['as' => 'admin.view.company', 'uses' => 'CompanyController@viewcompany']);

        Route::get('/company/add/advertise', ['as' => 'admin.add.company.advertise', 'uses' => 'CompanyController@addAdvertise']);

        Route::get('/company/list/advertisements', ['as' => 'admin.list.company.advertise', 'uses' => 'CompanyController@ListAdvertise']);

        Route::get('/company/downloaded/document/list', ['as' => 'admin.list.downloaded.document', 'uses' => 'CompanyController@ListDownloadedDocuments']);

        Route::get('/view/company/{id}', ['as' => 'admin.view.company.id', 'uses' => 'CompanyController@view']);

        Route::get('/view/seafarer/{id}', ['as' => 'admin.view.seafarer.id', 'uses' => 'SeafarerController@view']);

        Route::get('/add/advertiser', ['as' => 'admin.add.advertiser', 'uses' => 'DashboardController@addAdvertiser']);

        Route::post('/store/advertiser', ['as' => 'admin.store.advertiser', 'uses' => 'AdvertiseController@store']);

        Route::get('/edit/advertiser/{id}', ['as' => 'admin.edit.advertiser', 'uses' => 'AdvertiseController@edit']);

        Route::get('/view/advertiser/{id}', ['as' => 'admin.view.advertiser.id', 'uses' => 'AdvertiseController@view']);

        Route::get('/search/advertiser', ['as' => 'admin.view.advertiser', 'uses' => 'AdvertiseController@viewAdvertiserList']);

        Route::get('/add/advertise', ['as' => 'admin.add.advertise', 'uses' => 'AdvertiseController@addAdvertise']);

        Route::post('/advertise/store', ['as' => 'admin.advertiser.store', 'uses' => 'AdvertiseController@storeAdvertise']);

        Route::get('/advertise/enquiries', ['as'=>'admin.advertisements.list.enquiries' , 'uses' => 'AdvertiseController@enquiryList']);

        Route::get('/advertisements/', ['as'=>'admin.view.advertisements' , 'uses' => 'AdvertiseController@advertisementsList']);

        Route::post('/advertisements/status', ['as'=>'admin.change.advertisements.status' , 'uses' => 'AdvertiseController@changeStatus']);

        Route::get('/seaman-book/enquiries', ['as'=>'admin.seaman.book.enquiries' , 'uses' => 'DashboardController@seamanBookEnquiryList']); 

        Route::get('/contact/enquiries', ['as'=>'admin.contact.us.enquiries' , 'uses' => 'DashboardController@contactUsEnquiryList']);

        //institute
        Route::get('/search/institute', ['as' => 'admin.view.institutes', 'uses' => 'InstituteController@viewInstituteList']);

        Route::get('/search/institute/batch', ['as' => 'admin.view.institute.batches', 'uses' => 'InstituteController@viewAllBatchesLists']);

        Route::get('/add/institute', ['as' => 'admin.add.institute', 'uses' => 'InstituteController@addInstitute']);

        Route::get('/edit/institute/{id}', ['as' => 'admin.edit.institute', 'uses' => 'InstituteController@editInstitute']);

        Route::post('/update/institute/{id}', ['as' => 'admin.update.institute', 'uses' => 'InstituteController@updateInstitute']);

        Route::post('/store/institute', ['as' => 'admin.store.institute', 'uses' => 'InstituteController@storeInstitute']);

        Route::post('/logo', ['as' => 'admin.institute.store.logo', 'uses' => 'InstituteController@storeLogo']);

        Route::get('/add/institute/batch/{institute_id?}', ['as' => 'admin.add.course.batch', 'uses' => 'InstituteController@addInstisuteBatch']);

        Route::get('/institute/location/{id}', ['as' => 'admin.get.institute.location.id', 'uses' => 'InstituteController@getInstituteLocationByInstituteId']);

        Route::post('/institute/add/batch', ['as' => 'admin.institute.store.course.batch', 'uses' => 'InstituteController@storeCourseBatches']);

        Route::get('/institute/edit/batch/{institute_id}/{batch_id}', ['as' => 'admin.institute.edit.course.batch', 'uses' => 'InstituteController@editCourseBatches']);

        Route::post('/institute/update/batch/{batch_id}', ['as' => 'admin.institute.update.course.batch', 'uses' => 'InstituteController@updateCourseBatches']);

        Route::get('/course/applicant', ['as' => 'admin.institute.list.course.applicant', 'uses' => 'InstituteController@courseApplicantListing']);

        Route::get('/course/discount', ['as' => 'admin.institute.add.course.discount.mapping', 'uses' => 'InstituteController@addCourseDiscountMapping']);

        Route::get('/edit/course/discount', ['as' => 'admin.institute.edit.course.discount.mapping', 'uses' => 'InstituteController@editCourseDiscountMapping']);

        Route::post('/delete/course/discount', ['as' => 'admin.institute.delete.course.discount.mapping', 'uses' => 'InstituteController@deleteCourseDiscountMapping']);

        Route::get('/get/institute/list', ['as' => 'admin.institute.get.institute.list.by.course', 'uses' => 'InstituteController@getInstituteByCourse']);

        Route::post('/store/institute/list', ['as' => 'admin.institute.store.institute.list.by.course', 'uses' => 'InstituteController@storeCourseDiscountMapping']);

        Route::get('/institute/view/course', ['as' => 'admin.institute.view.course', 'uses' => 'InstituteController@ViewCourses']);

        Route::post('/view/institute/course/disable/{course_id}', ['as' => 'site.institute.disable.course', 'uses' => 'InstituteController@disableCourse']);

        Route::post('/view/institute/course/enable/{course_id}', ['as' => 'site.institute.enable.course', 'uses' => 'InstituteController@enableCourse']); 

        Route::post('/store/course', ['as' => 'admin.institute.store.course', 'uses' => 'InstituteController@storeCourse']);

        Route::any('/delete/course/{course_id}/{institute_id}', ['as' => 'admin.institute.delete.course', 'uses' => 'InstituteController@deleteCourse']);

        Route::get('/view/institute/{id}', ['as' => 'admin.view.institute.id', 'uses' => 'InstituteController@view']);

        Route::get('/schedule', ['as' => 'admin.schedule', 'uses' => 'ScheduleController@schedule']);

        Route::post('/schedule-add', ['as' => 'admin.schedule.add', 'uses' => 'ScheduleController@addSchedule']);

        Route::get('/schedule-list', ['as' => 'admin.schedule.list', 'uses' => 'ScheduleController@listSchedule']);

        Route::post('/schedule-delete', ['as' => 'admin.schedule.delete', 'uses' => 'ScheduleController@deleteSchedule']);

        // Route::get('/schedule-edit/{id}', ['as' => 'admin.schedule.edit', 'uses' => 'ScheduleController@editSchedule']);

        Route::get('/get/subscription', ['as' => 'admin.get.user.subscription', 'uses' => 'SubscriptionController@getAllUserSubscription']);

        Route::get('/get/vessel/details/{company_id}', ['as' => 'admin.company.get.vessel.details', 'uses' => 'CompanyController@getVesselByVesselId']);

        Route::post('/store/ship/details/{company_id}', ['as' => 'admin.company.store.ship.details', 'uses' => 'CompanyController@storeShipDetails']);

        Route::post('/delete/vessel/details/{company_id}', ['as' => 'admin.company.delete.vessel.details', 'uses' => 'CompanyController@deleteVesselDetails']);

        Route::get('/get/agent/details/{company_id}', ['as' => 'admin.company.get.agent.details', 'uses' => 'CompanyController@getAgentByAgentId']);

        Route::post('/store/agent/{company_id}', ['as' => 'admin.company.store.agent', 'uses' => 'CompanyController@storeAgents']);

        Route::post('/delete/agent/details/{company_id}', ['as' => 'admin.company.delete.agent.details', 'uses' => 'CompanyController@deleteAgentDetails']);

        Route::post('/agent/profile-pic', ['as' => 'admin.company.store.agent.profilePic', 'uses' => 'CompanyController@uploadAgentProfilePic']);

        Route::post('/team/update/{company_id}', ['as'=> 'admin.company.update.team.details', 'uses' => 'CompanyController@updateTeamDetails']);

        Route::post('/team/profile-pic', ['as' => 'admin.company.team.store.profilePic', 'uses' => 'CompanyController@uploadTeamProfilePic']);

        Route::post('/team', ['as'=> 'admin.company.team.details', 'uses' => 'CompanyController@storeTeamDetails']);

        Route::post('/team/delete/{company_id}', ['as'=> 'admin.company.delete.team.details', 'uses' => 'CompanyController@deleteTeamDetails']);

        Route::get('/user/notification', ['as' => 'admin.get.user.notification', 'uses' => 'NotificationController@getUserNotifications']);

        Route::post('/user/notification/upload', ['as' => 'admin.upload.user.notification', 'uses' => 'NotificationController@uploadUserNotifications']);

        Route::post('/set/notification/read', ['as' => 'admin.set.user.notification.read', 'uses' => 'NotificationController@setUserNotificationsRead']);

        Route::get('/view/notification', ['as' => 'admin.notifications', 'uses' => 'NotificationController@viewNotifications']);
        
        Route::get('/promotional-email', ['as' => 'admin.promotionalEmail', 'uses' => 'NotificationController@viewNotifications']);
        
        // Job
        Route::any('email/get', 'PromotionalEmailController@get')->name('admin.get.emails');
        Route::any('email/index', 'PromotionalEmailController@index')->name('admin.email');
        Route::any('email/send', 'PromotionalEmailController@send')->name('admin.email.send');
        Route::post('email/sendEmail', 'PromotionalEmailController@sendEmail')->name('admin.email.sendEmail');
        
        Route::get('affiliate', 'AffiliateController@index')->name('admin.affiliate');
        Route::any('affiliate/getAjaxAffiliateList', 'AffiliateController@getAjaxAffiliateList')->name('admin.getAjaxAffiliateList');
        Route::get('affiliate/add', 'AffiliateController@addForm')->name('admin.affiliate.add');
        Route::get('affiliate/view/{id}', 'AffiliateController@viewData')->name('admin.affiliate.view');
        Route::get('affiliate/edit/{id}', 'AffiliateController@editForm')->name('admin.affiliate.edit');
         Route::any('affiliate/delete/{id}', 'AffiliateController@deleteData')->name('admin.affiliate.delete');
        Route::post('affiliate/store', 'AffiliateController@store')->name('admin.affiliate.insert');
      
    });
});

Route::group(['namespace' => 'Auth'], function () {

    Route::post('/password/email', ['as' => 'site.reset.password.email' , 'uses' => 'ForgotPasswordController@sendResetLinkEmail']);

    Route::post('/password/reset', ['uses'=> 'ResetPasswordController@reset']);

    //Route::get('/password/reset', ['uses'=> 'ForgotPasswordController@showLinkRequestForm']);

    Route::get('/password/reset/{token}', ['as' => 'site.reset.password.with.token','uses'=> 'ResetPasswordController@showResetForm']);

});


Route::group(['namespace' => 'Site'], function () {
    Route::get('/test_email', ['as' => 'site.test_email' , 'uses' => 'UserController@testEmail']);

    Route::get('/login', ['as' => 'site.login', 'uses' => 'LoginController@doLogin']);

    // Route::post('/login', ['as' => 'site.login', 'uses' => 'LoginController@login']);
    Route::post('/login', ['as' => 'site.login.submit', 'uses' => 'LoginController@login']);

    Route::post('/check/email', ['as' => 'site.check.email', 'uses' => 'UserController@checkEmail']);

    Route::post('/check/mobile', ['as' => 'site.check.mobile', 'uses' => 'UserController@checkMobile']);

    Route::get('/seafarer/view/profile/{id?}', ['as'=>'user.view.profile' , 'uses' => 'UserController@profile']);

    Route::get('/reset-password', ['as' => 'site.reset.password.view' , 'uses' => 'LoginController@resetPasswordView']);

    Route::post('/reset-password', ['as' => 'site.reset.password' , 'uses' => 'LoginController@resetPassword']);

    Route::get('/reset-password/{email}', ['as' => 'site.set.new.password' , 'uses' => 'LoginController@setNewPassword']);

    Route::get('/seafarer/company', ['as' => 'site.get.company.name' , 'uses' => 'UserController@getUserShippingCompanyWithId']);
    
    Route::get('/seafarer/manning', ['as' => 'site.get.manning_by' , 'uses' => 'UserController@getUserShippingManningWithId']);

    Route::get('/seafarer/course/issue_by', ['as' => 'site.get.course.issue.by' , 'uses' => 'UserController@getCourseIssueBy']);
    
    Route::get('/seafarer/ship/name', ['as' => 'site.get.company.name' , 'uses' => 'UserController@getUserShipName']);

    Route::get('/state/{country_id}', ['as' => 'site.get.state.by.country.id' , 'uses' => 'UserController@getStateByCountryId']);

    Route::get('/courses/{course_id?}', ['as' => 'site.seafarer.homepage.course.search', 'uses' => 'InstituteController@homepage_course_listing']);

    Route::get('/institutes', ['as' => 'site.seafarer.homepage.institute.search', 'uses' => 'InstituteController@homepage_institute_listing']);

    Route::get('/course/search', ['as' => 'site.seafarer.course.search', 'uses' => 'InstituteController@seafarer_course_search']);

    Route::get('/course/book/{batch_id}/{location_id}', ['as' => 'site.seafarer.course.book', 'uses' => 'InstituteController@seafarer_book_course']);

    //Notification routes

    Route::get('/user/notification', ['as' => 'site.get.user.notification', 'uses' => 'NotificationController@getUserNotifications']);

    Route::post('/user/notification/upload', ['as' => 'site.upload.user.notification', 'uses' => 'NotificationController@uploadUserNotifications']);

    Route::post('/set/notification/read', ['as' => 'site.set.user.notification.read', 'uses' => 'NotificationController@setUserNotificationsRead']);
    
    
    Route::group(['prefix' => 'seafarer'], function () {

        Route::any('/postUpload', ['as' => 'site.user.upload', 'uses' => 'UserController@dropzone']);
        Route::any('/postUpload/delete', ['as' => 'site.user.upload.document.delete', 'uses' => 'UserController@deleteUpload']);

        Route::get('/documents/list', ['as' => 'site.user.get.documents.list', 'uses' => 'UserController@getUserDocumentsList']);

        Route::get('/documents/status/change', ['as' => 'site.user.change.documents.status', 'uses' => 'UserController@changeUserDocumentStatus']);

        Route::post('/documents/request/store', ['as' => 'site.user.store.requested.documents.list', 'uses' => 'UserController@storeRequestedUserDocumentsList']);

        Route::post('/documents/request/status', ['as' => 'site.user.requested.documents.status', 'uses' => 'UserController@updateRequestedDocumentStatus']);

        Route::get('/document/download/{user_id}/{type?}/{option?}', ['as' => 'site.user.get.documents.path', 'uses' => 'UserController@getDocumentPath']);

        Route::get('/document/path/{user_id}/{type?}/{type_id?}/{file_name?}', ['as' => 'site.user.view.documents.path', 'uses' => 'UserController@viewDocumentPath']);

        Route::post('/documents/permissions/store', ['as' => 'site.user.store.permissions', 'uses' => 'UserController@storeDocumentsPermission']);

        Route::post('/documents/request/store', ['as' => 'site.user.store.requested.documents.list', 'uses' => 'UserController@storeRequestedUserDocumentsList']);

        Route::post('/change/availability', ['as' => 'site.user.availability.change', 'uses' => 'UserController@changeAvailability']);

        Route::post('/apply/job', ['as' => 'site.user.job.apply', 'uses' => 'JobController@seafarerJobApply']);

        Route::post('/apply/course', ['as' => 'site.user.course.apply', 'uses' => 'CourseController@seafarerCourseApply']);

        Route::group(['middleware'=>'auth','middleware'=>'checkRole:seafarer'], function () {

            Route::get('/profile/edit', ['as' => 'site.seafarer.edit.profile', 'uses' => 'UserController@editProfile']);

            Route::get('/job/new', ['as' => 'site.seafarer.job.new', 'uses' => 'JobController@getNewJobs']);

            Route::post('/job/applied', ['as' => 'site.seafarer.job.applied', 'uses' => 'JobController@appliedJob']);

            
            Route::get('/profile', ['as'=>'old.user.profile' , 'uses' => 'UserController@profile']);

//            Route::get('/profile', ['as'=>'user.profile' , 'uses' => 'UserController@profile']);
            //Route::get('/profile1', ['as'=>'user.profile' , 'uses' => 'UserController@profile1']);

            Route::get('/update/availability/{id}', ['as'=>'user.update.availability' , 'uses' => 'UserController@updateAvailability']);

            Route::get('/mybookings', ['as'=>'user.bookings' , 'uses' => 'UserController@mybookings']);  
        });
       
        Route::get('/registration', ['as'=>'site.seafarer.registration' , 'uses' => 'UserController@registration']);

        Route::get('/user-registration/{step?}', ['as' => 'site.user.registration', 'uses' => 'UserController@registration']);

        Route::post('/store', ['as' => 'site.user.registration', 'uses' => 'UserController@storeDetails']);

        Route::post('/store-documents', ['as' => 'site.user.store.documents', 'uses' => 'UserController@storeDocuments']);

        Route::post('/mob-verification', ['as' => 'site.user.mob.verification', 'uses' => 'UserController@verifyMobile']);

        Route::post('/resend-otp', ['as' => 'site.user.mob.resendOtp', 'uses' => 'UserController@resendOtp']);

        Route::post('/resend-email', ['as' => 'site.user.email.resendEmail', 'uses' => 'UserController@resendEmail']);

        Route::get('/verify-email/{email}', ['as' => 'site.user.verify.email', 'uses' => 'UserController@verifyEmail']);

        Route::get('/seafarer-profile', ['as' => 'site.user.display.profilePic', 'uses' => 'UserController@displayProfile']);

        Route::post('/update', ['as' => 'site.user.profile.registration', 'uses' => 'UserController@updateProfileDetails']);

        Route::post('/update-documents', ['as' => 'site.store.user.profile.documents', 'uses' => 'UserController@storeDocuments']);

        Route::post('/profile-pic', ['as' => 'site.user.store.profilePic', 'uses' => 'UserController@uploadUserProfile']);

        Route::post('/profile-pic/delete', ['as' => 'site.user.store.delete.profilePic', 'uses' => 'UserController@deleteUserProfilePic']);

        Route::any('/job/search', ['as' => 'site.seafarer.job.search', 'uses' => 'JobController@seafarer_job_search']);

        Route::post('/store-service-details', ['as' => 'site.user.service.details', 'uses' => 'UserController@storeServiceDetails']);
        Route::post('/create-share-coontact', ['as' => 'site.user.service.details', 'uses' => 'UserController@createShareCoontact']);
		Route::post('/store-general-details', ['as' => 'site.user.general.details', 'uses' => 'UserController@storeGeneralDetails']);

        Route::post('/store-course-details', ['as' => 'site.user.course.details', 'uses' => 'UserController@storeCourseDetails']);

        Route::post('/delete-service-details', ['as' => 'site.user.delete.service.details', 'uses' => 'UserController@deleteServiceDetails']);
        
        Route::post('/get-service-details', ['as' => 'site.user.get.service.details', 'uses' => 'UserController@getSeaServiceDetails']);

        Route::post('/store-single-service-details', ['as' => 'site.user.store.single.service.details', 'uses' => 'UserController@storeSpecificServiceDetails']);

        Route::post('/store-seafarer-course-details', ['as' => 'site.user.store.seafarer.course.details', 'uses' => 'UserController@storeSeafarerCourseDetails']); 

        Route::post('/delete-seafarer-course-details', ['as' => 'site.user.seafarer.delete.details', 'uses' => 'UserController@deleteSeafarerCourseDetails']);

        Route::get('/get-seafarer-course-details', ['as' => 'site.user.seafarer.get.course.details', 'uses' => 'UserController@getSeafarerCourseByCourseId']);

        Route::get('/get-seafarer-courses-by-type', ['as' => 'site.user.seafarer.get.course.by.course.type', 'uses' => 'UserController@getSeafarerCoursesByCourseType']);

        Route::any('/download-cv', ['as' => 'site.user.download-cv', 'uses' => 'UserController@DownloadCV']);

        Route::any('/download-file', ['as' => 'site.user.download-file', 'uses' => 'UserController@DownloadFile']);
		Route::any('/share-file', ['as' => 'site.user.share-file', 'uses' => 'UserController@ShareFile']);

        //institute batch payment section
        Route::post('/proceed-to-payment', ['as' => 'site.institute.batch.proceed.payment', 'uses' => 'InstituteController@proceedToPayment']);
        Route::any('/cancel/order', ['as' => 'site.institute.batch.cancel.order', 'uses' => 'InstituteController@orderCancel']);

        Route::any('/order/payment-gateway-response/{order_id}/{payment_type}/{split_type?}', ['as' => 'site.institute.batch.payment.response', 'uses' => 'InstituteController@paymentGatewayResponse']);

        //WK COP
        Route::post('/wk_cop/update',[
            'as' => 'wk_cop.update',
            'uses' => 'UserController@wkUpdate'
        ]);
        
        Route::post('/save-share-coontact', ['as' => 'site.user.save.shareContact', 'uses' => 'UserController@saveShareContact']);
        Route::post('/delete-share-coontact', ['as' => 'site.user.delete.shareContact', 'uses' => 'UserController@deleteShareContact']);
        Route::get('/get-share-coontact', ['as' => 'site.user.get.shareContact', 'uses' => 'UserController@getShareContact']);
        Route::get('/get-share-history-resume', ['as' => 'site.user.get.shareHistoryResume', 'uses' => 'UserController@getShareHistoryResume']);
        Route::get('/view-share-histroy', ['as' => 'site.user.get.viewHistory', 'uses' => 'UserController@viewShareHistory']);
        Route::get('/get-share-coontact-list', ['as' => 'site.user.get.shareContactList', 'uses' => 'UserController@getShareContactList']);
        Route::get('/get-share-history-document', ['as' => 'site.user.get.shareHistoryDocument', 'uses' => 'UserController@getShareHistoryDocument']);
        Route::post('/share-history-document/request-new-link', ['as' => 'site.user.post.requestNewLink', 'uses' => 'UserController@requestNewLink']);
        Route::post('/share-history-document/delete', ['as' => 'site.user.post.deleteSharedDocuments', 'uses' => 'UserController@deleteSharedDocuments']);
        Route::post('/share-history-resume/delete', ['as' => 'site.user.post.deleteSharedResume', 'uses' => 'UserController@deleteSharedResume']);
    });

    Route::group(['prefix' => 'company'], function () {

        Route::group(['middleware'=>'auth','middleware'=>'checkRole:company'], function () {

            Route::get('/profile', ['as'=> 'site.show.company.details', 'uses' => 'CompanyController@view']); 

            Route::get('/scope', ['as'=> 'site.show.company.scope', 'uses' => 'CompanyController@viewScope']); 

            Route::get('/profile/edit', ['as'=> 'site.edit.company.details', 'uses' => 'CompanyController@editProfile']);

            Route::get('/mysubscription', ['as' => 'site.company.mysubscription', 'uses' => 'SubscriptionController@subscriptionList']);

            Route::get('/add/job', ['as' => 'site.company.add.jobs', 'uses' => 'JobController@addJob']);

            Route::get('/jobs', ['as' => 'site.company.list.jobs', 'uses' => 'JobController@jobListing']);

            Route::post('/store-job', ['as' => 'site.company.store.jobs', 'uses' => 'JobController@store']);

            Route::post('/update/job', ['as' => 'site.company.update.jobs', 'uses' => 'JobController@updateJob']);

            Route::get('/edit/job/{job_id?}', ['as' => 'site.company.edit.jobs', 'uses' => 'JobController@editJob']);

            Route::post('/disable/{job_id?}', ['as' => 'site.company.disable.jobs', 'uses' => 'JobController@disableJob']);

            Route::post('/enable/{job_id?}', ['as' => 'site.company.disable.jobs', 'uses' => 'JobController@enableJob']);

            Route::get('/job/applicant', ['as' => 'site.company.list.job.applicant', 'uses' => 'JobController@jobApplicantListing']);

            Route::any('/shipdetails/{id}',['as' => 'site.company.details.related.ships', 'uses' => 'JobController@dataByCompanyShipType']);

            Route::any('/shipdetails/shiptype/{ship_id}',['as' => 'site.company.details.related.ships.by.ship.type', 'uses' => 'JobController@dataByCompanyShipId']);

            Route::any('/shipname/{id}/{company_id?}',['as' => 'site.company.details.ship.name.by.ship.id', 'uses' => 'JobController@shipNameByCompanyShipType']);

            Route::any('/candidate/search', ['as' => 'site.company.candidate.search', 'uses' => 'CompanyController@candidateSearch']);

            Route::any('/resume/download', ['as' => 'site.company.resume.download.list', 'uses' => 'CompanyController@resumeList']);

            Route::get('/add/advertise', ['as' => 'site.company.add.advertise', 'uses' => 'CompanyController@addAdvertise']);

            Route::post('/store/advertise', ['as' => 'site.company.advertise.store', 'uses' => 'CompanyController@storeAdvertise']);

            Route::get('/permission/request', ['as' => 'site.company.list.permission.requests', 'uses' => 'CompanyController@getAllSeafarerPermissionRequest']);

            Route::get('/document/download/{permission_id}', ['as' => 'site.company.download.document.by.permission.id', 'uses' => 'CompanyController@downloadDocumentsByPermissionId']);

            Route::post('/store/ship/details', ['as' => 'site.company.store.ship.details', 'uses' => 'CompanyController@storeShipDetails']);

            Route::post('/delete/vessel/details', ['as' => 'site.company.delete.vessel.details', 'uses' => 'CompanyController@deleteVesselDetails']);

            Route::get('/get/vessel/details', ['as' => 'site.company.get.vessel.details', 'uses' => 'CompanyController@getVesselByVesselId']);

            Route::post('/store/agent', ['as' => 'site.company.store.agent', 'uses' => 'CompanyController@storeAgents']);

            Route::post('/delete/agent/details', ['as' => 'site.company.delete.agent.details', 'uses' => 'CompanyController@deleteAgentDetails']);

            Route::get('/get/agent/details', ['as' => 'site.company.get.agent.details', 'uses' => 'CompanyController@getAgentByAgentId']);

            Route::post('/agent/profile-pic', ['as' => 'site.company.store.agent.profilePic', 'uses' => 'CompanyController@uploadAgentProfilePic']);

        });

        Route::get('/profile/{id}', ['as'=> 'site.show.company.details.user', 'uses' => 'CompanyController@viewProfile']);

        Route::get('/team', ['as'=> 'site.show.company.team.details', 'uses' => 'CompanyController@viewTeamDetails']);

        Route::post('/team', ['as'=> 'site.company.team.details', 'uses' => 'CompanyController@storeTeamDetails']);

        Route::post('/team/delete', ['as'=> 'site.company.delete.team.details', 'uses' => 'CompanyController@deleteTeamDetails']);

        Route::post('/team/update', ['as'=> 'site.company.update.team.details', 'uses' => 'CompanyController@updateTeamDetails']);

        Route::get('/registration', ['as' => 'site.company.registration', 'uses' => 'CompanyController@registration']);

        Route::post('/store', ['as'=> 'site.store.company.details', 'uses' => 'CompanyController@store']);

        Route::post('/store-location-ship-details', ['as'=> 'site.store.company.location-ship-details', 'uses' => 'CompanyController@store_location_ship_details']);

        Route::post('/update', ['as' => 'site.company.profile.registration', 'uses' => 'CompanyController@updateCompanyDetails']);

        Route::post('/logo', ['as' => 'site.company.store.logo', 'uses' => 'CompanyController@uploadLogo']);

        Route::post('/team/profile-pic', ['as' => 'site.company.store.profilePic', 'uses' => 'CompanyController@uploadTeamProfilePic']);

        Route::any('/candidate/download-cv/{user_id}', ['middleware'=>'checkFeature','as' => 'site.company.candidate.download-cv', 'uses' => 'CompanyController@candidateDownloadCV']);

        Route::post('/check-subscription-availability', ['as' => 'site.checkSubscriptionAvailability', 'uses' => 'UserController@checkSubscriptionAvailability']);

    });

    Route::group(['prefix' => 'institute'], function () {

        Route::get('/registration', ['as' => 'site.institute.registration', 'uses' => 'InstituteController@registration']);

        Route::post('/logo', ['as' => 'site.institute.store.logo', 'uses' => 'InstituteController@uploadLogo']);

        Route::post('/store', ['as'=> 'site.store.institute.details', 'uses' => 'InstituteController@store']);
        
        Route::group(['middleware'=>'auth','middleware'=>'checkRole:institute'], function () {

            Route::get('/profile', ['as'=> 'site.show.institute.details', 'uses' => 'InstituteController@view']);

            Route::get('/profile/edit', ['as'=> 'site.edit.institute.details', 'uses' => 'InstituteController@editProfile']);

            Route::post('/update', ['as' => 'site.institute.profile.registration', 'uses' => 'InstituteController@updateInstituteDetails']);

            Route::get('/courses', ['as' => 'site.institute.course.listing', 'uses' => 'InstituteController@courseListing']);

            Route::get('/mysubscription', ['as' => 'site.institute.mysubscription', 'uses' => 'SubscriptionController@subscriptionList']);

            Route::get('/course/batches/add', ['as' => 'site.institute.course.batches', 'uses' => 'InstituteController@courseBatches']);

            Route::get('/course/batches/edit/{id}', ['as' => 'site.institute.course.batches.edit', 'uses' => 'InstituteController@editCourseBatches']);

            Route::post('/store/course', ['as' => 'site.institute.store.course', 'uses' => 'InstituteController@storeCourse']);

            Route::post('/delete/course/{course_id}', ['as' => 'site.institute.delete.course', 'uses' => 'InstituteController@deleteCourse']);

            Route::post('/store/course/batches', ['as' => 'site.institute.store.course.batches', 'uses' => 'InstituteController@storeCourseBatches']);

            Route::post('/update/course/batches/{id}', ['as' => 'site.institute.update.course.batches', 'uses' => 'InstituteController@updateCourseBatches']);

            Route::get('/batch/{type?}', ['as' => 'site.institute.batch.listing', 'uses' => 'InstituteController@batchListing']);

            Route::get('/getdetailsByBatchId/{batch_id}', 'InstituteController@getBatchByBatchAndInstituteId');

            Route::post('/course/edit/{course_id}', ['as' => 'site.institute.edit.course', 'uses' => 'InstituteController@editCourse']);

            Route::post('/course/disable/{course_id}', ['as' => 'site.institute.disable.course', 'uses' => 'InstituteController@disableCourse']);

            Route::post('/course/enable/{course_id}', ['as' => 'site.institute.enable.course', 'uses' => 'InstituteController@enableCourse']); 

            Route::post('/batch/disable/{batch_id}', ['as' => 'site.institute.disable.batch', 'uses' => 'InstituteController@disableBatch']);

            Route::post('/batch/enable/{batch_id}', ['as' => 'site.institute.enable.batch', 'uses' => 'InstituteController@enableBatch']);

            Route::get('/course/booking', ['as' => 'site.institute.list.batch.applicant', 'uses' => 'InstituteController@courseApplicantListing']);

            Route::get('/permission/request', ['as' => 'site.institute.list.permission.requests', 'uses' => 'InstituteController@getAllSeafarerPermissionRequest']);

            Route::get('/document/download/{permission_id}', ['as' => 'site.institute.download.document.by.permission.id', 'uses' => 'InstituteController@downloadDocumentsByPermissionId']);

            Route::post('/batch/status', ['as' => 'site.institute.change.batch.status', 'uses' => 'InstituteController@changeInstituteBatchOrderStatus']);

            Route::get('/add/advertise', ['as' => 'site.institute.add.advertise', 'uses' => 'InstituteController@addAdvertise']);

            Route::post('/store/advertise', ['as' => 'site.institute.advertise.store', 'uses' => 'InstituteController@storeAdvertise']);

            Route::get('/add/notice', ['as' => 'site.institute.add.notice', 'uses' => 'InstituteController@addNotice']);

            Route::post('/store/notice', ['as' => 'site.institute.store.notice', 'uses' => 'InstituteController@storeNotice']);

            Route::get('/image/gallery', ['as' => 'site.institute.add.image.gallery', 'uses' => 'InstituteController@addImageGallery']);

            Route::post('/store/image/gallery', ['as' => 'site.institute.store.image.gallery', 'uses' => 'InstituteController@storeImageGallery']);

            Route::post('/delete/image/gallery', ['as' => 'site.institute.delete.image.gallery', 'uses' => 'InstituteController@deleteImageGallery']);

            Route::get('/ondemand/booking', ['as' => 'site.institute.on.demand.booking', 'uses' => 'InstituteController@onDemandBookings']);

            Route::post('/batch/confirm', ['as' => 'site.institute.on.demand.batch.confirm', 'uses' => 'InstituteController@onDemandBatchConfirm']);

            Route::get('/view/notification', ['as' => 'site.institute.view.notification', 'uses' => 'NotificationController@viewNotifications']);

        });

        Route::get('/course/details/{id}', ['as' => 'site.institute.course.details', 'uses' => 'InstituteController@getInstituteCourseDetailsById']);
        
    });

    Route::group(['prefix' => 'advertiser'], function () {

        Route::group(['middleware'=>'auth','middleware'=>'checkRole:advertiser'], function () {


            Route::get('/add/advertise', ['as' => 'site.advertiser.add.advertise', 'uses' => 'AdvertiserController@addAdvertise']);
            
            Route::get('/enquiries', ['as'=>'site.advertisements.list.enquiries' , 'uses' => 'AdvertiserController@enquiryList']);

            Route::get('/mysubscription', ['as' => 'site.advertiser.mysubscription', 'uses' => 'SubscriptionController@subscriptionList']);

            Route::post('/advertise/status-change', ['middleware'=>'checkFeature','as' => 'site.advertiser.statusChange', 'uses' => 'AdvertiserController@changeAdvertisementStatus']);

            Route::post('/advertise/status-degrade', ['as' => 'site.advertiser.statusDegrade', 'uses' => 'AdvertiserController@changeAdvertisementStatus']);
            
        });
        Route::get('/profile', ['as' => 'site.advertiser.profile', 'uses' => 'AdvertiserController@view']);

        Route::post('/advertise/store', ['middleware'=>'checkFeature','as' => 'site.advertiser.store', 'uses' => 'AdvertiserController@storeAdvertise']);

        Route::get('/profile/{id}', ['as' => 'site.advertiser.profile.id', 'uses' => 'AdvertiserController@viewAdvertiserProfile']);

       Route::get('/get/advertisments', ['as'=>'site.advertisements.list' , 'uses' => 'AdvertiserController@get_advertisements']);
       Route::get('/registration', ['as' => 'site.advertiser.registration', 'uses' => 'AdvertiserController@registration']);

       Route::post('/store', ['as' => 'site.advertise.store', 'uses' => 'AdvertiserController@store']);

       Route::get('/edit/profile', ['as' => 'site.advertiser.edit.profile', 'uses' => 'AdvertiserController@edit']);

       Route::post('/advertisement/enquiry', ['as' => 'site.advertisements.enquiry', 'uses' => 'AdvertiserController@advertiseEnquiry']);

    });

    Route::get('/companies', ['as' => 'site.view.company.list', 'uses' => 'CompanyController@getAllCompanyWithAavertisements']);

    Route::get('/advertisements', ['as' => 'site.view.advertisements.list', 'uses' => 'AdvertiserController@getAllAdvertiserWithAavertisements']);

    Route::get('/advertise', ['as' => 'site.get.advertise', 'uses' => 'AdvertiseController@getAdvertises']);

    Route::get('/pincode/get/states_cities', ['as' => 'site.pincode.get.states_cities', 'uses' => 'PincodeController@getStatesCities']);

    Route::get('/logout' , ['as' => 'logout' , 'uses' => 'UserController@logout']);

    Route::get('/subscription', ['as' => 'site.user.subscription', 'uses' => 'SubscriptionController@subscription']);

    Route::post('/add-to-cart', ['as' => 'site.subscription.add.to.cart', 'uses' => 'CartController@addToCart']);

    Route::get('/cart', ['as' => 'site.subscription.cart', 'uses' => 'CartController@viewCart']);

    Route::post('/delete-from-cart/{id}', ['as' => 'site.delete.from.cart', 'uses' => 'CartController@deleteFromCart']);

    Route::get('/checkout', ['as' => 'site.cart.checkout', 'uses' => 'CartController@checkout']);

    Route::post('/proceed-to-payment', ['as' => 'site.proceed.payment', 'uses' => 'OrderController@proceedToPayment']);

    Route::any('/order/payment-gateway-response/{order_id}/{subscription_date}', ['as' => 'site.payment.response', 'uses' => 'OrderController@paymentGatewayResponse']);

    Route::get('order-status/{order_id}', ['as' => 'site.view.order', 'uses' => 'OrderController@viewOrder']);

    Route::get('batch/order-status/{order_id}/{type?}', ['as' => 'site.view.batch.order', 'uses' => 'OrderController@viewBatchOrder']);

    Route::post('contact-us', ['as' => 'site.contact.us', 'uses' => 'ContactUsController@store']);

    Route::post('activate/subscription', ['as' => 'site.activate.subscription', 'uses' => 'SubscriptionController@activateFreeSubscription']);

    Route::get('/course/{id}/{institute_id?}', ['as' => 'site.institute.get.course.name', 'uses' => 'InstituteController@getCourseNameByCourseType']);

    Route::get('/batch/{batch_id}', ['as' => 'site.institute.get.batch.details.by.id', 'uses' => 'InstituteController@getDataByBatchId']);

    Route::post('/block/seat', ['as' => 'site.institute.block.seat', 'uses' => 'InstituteController@blockSeat']);

    Route::post('/pre-register', ['as' => 'site.seafarer.pre.register', 'uses' => 'UserController@preRegistration']);

    Route::get('/institute/profile/{id}', ['as'=> 'site.show.institute.details.id', 'uses' => 'InstituteController@view']);

    Route::get('/institute/advertise', ['as'=> 'site.get.institute.advertise.by.batch.id', 'uses' => 'InstituteController@getInstituteAdvertiseByBatchId']);

});

//Route::get('/preemail', ['as' => 'site.seafarer.pre.register', 'uses' => 'UserController@preemail']);


Route::get('/institutes-and-courses', ['as' => 'institutesCourses', 'uses' => 'HomeController@institutesCourses']);
Route::get('/institutes-search-result', ['as' => 'institutesSearchResult', 'uses' => 'HomeController@institutesSearchResult']);
Route::get('/institutes-details', ['as' => 'institutesDetails', 'uses' => 'HomeController@institutesDetails']);

Route::group(['middleware'=>'auth'], function () {
    // rate and review route
    Route::get('rate-review','Admin\RateReviewController@index');
    Route::get('get-ship-data/{shipId}','Admin\RateReviewController@getShipData');
    Route::post('store-ratedata','Admin\RateReviewController@store');
    Route::get('get-rate-and-review','Admin\RateReviewController@rateData');
    Route::get('experience-onboard','Admin\RateReviewController@showdata');
   
    // End rate review route

    // user profile route
    Route::get('user-profile', ['as' => 'user.profile', 'uses' => 'Site\UserController@userProfile']);
    Route::get('do-not-show-again', ['as' => 'user.do_not_show_again', 'uses' => 'Site\UserController@doNotShowAgain']);    
    Route::get('get-userinfo','Site\UserController@getUserInfo');


    Route::get('affiliate_form','Admin\AffiliateController@getaffiliate_form');
    Route::any('getRewardsUserData','Admin\AffiliateController@getRewardsUserData')->name('user.getRewardsUserData');

    Route::post('add_affiliate_form','Admin\AffiliateController@add_affiliate_form')->name('user.add_affiliate_form');
    
    Route::get('/graph', 'Admin\GraphController@index')->name('graph.index');
    Route::get('/documents/download', 'Admin\ManageDocumentsController@downloadIndex')->name('documents.download');
    Route::get('/documents/view', 'Admin\ManageDocumentsController@viewDocument')->name('documents.view');
    Route::get('/documents/download/zip','Admin\ManageDocumentsController@downloadZip')->name('documents.zip');
    Route::post('/documents/download/zip/checking','Admin\ManageDocumentsController@downloadZipPost')->name('documents.zip.post');
    Route::get('/share', 'Admin\ManageDocumentsController@shareIndex')->name('share.index');
    
    Route::get('/job/history', 'Admin\ManageDocumentsController@getUserJobHistory')->name('job.getUserJobHistory');
    Route::get('/job-history', 'Admin\ManageDocumentsController@getUserJobHistoryList')->name('job.getUserJobHistoryList');


    Route::get('/share/documents', 'Admin\ManageDocumentsController@shareDocuments')->name('share.documents');
    Route::post('/share/resume', 'Admin\ManageDocumentsController@shareResume')->name('share.resume');
    Route::post('/share/via-email','Admin\ManageDocumentsController@viaEmail')->name('share.documents.post');
    Route::post('/share/profile','Admin\ManageDocumentsController@shareProfile')->name('share.profile');
    Route::get('/share-contacts','Admin\ShareContactsController@contactsIndex')->name('share.contacts');
    Route::get('/share-history-resume','Admin\HistoryLogsController@historyLogIndex')->name('share.history.resume');
    Route::get('/share-history-document','Admin\HistoryLogsController@historyLogDocument')->name('share.history.document');
});
Route::any('/shared-documents/{token}','Admin\ManageDocumentsController@getSharedData')->name('documents.sharedData');
Route::post('/shared-documents/download/{token}','Admin\ManageDocumentsController@downloadZipPost')->name('shared.documents.zip');
Route::any('/share-pdf', 'Admin\PdfController@sharePdf')->name('share-pdf');
Route::post('/save-qr', 'Admin\PdfController@saveQr')->name('save-qr');

Route::any('/share-profile/{id}', 'Site\UserController@shareUserProfile')->name('share-profile');
Route::any('/demo/share-profile', 'Site\UserController@shareUserProfile')->name('demo-share-profile');
Route::get('get-share-userinfo','Site\UserController@getShareUserInfo');

Route::get('/share-graph', 'Admin\GraphController@shareGraph')->name('share-graph');
Route::get('/share-documents', 'Admin\ManageDocumentsController@shareDocument')->name('share-documents');
Route::get('/share-experience-onboard','Admin\RateReviewController@showdata')->name('share-experience-onboard');
Route::get('/test-zip', 'HomeController@testZip');
 Route::get('experience-onboard/load-more', ['as' => 'experienceShowDataLoadMore', 'uses' => 'Admin\RateReviewController@showDataLoadMore']);
 Route::any('/create-pdf', 'Admin\PdfController@pdfview')->name('create-pdf');