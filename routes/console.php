<?php

use Illuminate\Foundation\Inspiring;
use App\Console\Commands\SendEmailsJobAvailability;
use App\Console\Commands\SendEmailsUpdateAvailability;
use App\Console\Commands\SendEmailsCompanyAccordingJob;
use App\Console\Commands\SendEmailsInstituteSeafarersAccordingToCourse;
use App\Console\Commands\SendSubscriptionExpiryEmail;
use App\Console\Commands\SendEmailExpireAvailability;
use App\Console\Commands\SendEmailSeafarerForFullPayment;

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

Artisan::command('inspire', function () {
    $this->comment(Inspiring::quote());
})->describe('Display an inspiring quote');

Artisan::command('send_emails_job_availability', function () {
    $this->comment(SendEmailsJobAvailability::handle());
})->describe('Job availability for seafarer.');

Artisan::command('send_emails_update_availability', function () {
    $this->comment(SendEmailsUpdateAvailability::handle());
})->describe('Availability expiry for seafarer.');

Artisan::command('send_emails_company_candidate_according_job', function () {
    $this->comment(SendEmailsCompanyAccordingJob::handle());
})->describe('Email candidates to company according to job.');

Artisan::command('send_emails_institute_seafarer_according_to_course', function () {
    $this->comment(SendEmailsInstituteSeafarersAccordingToCourse::handle());
})->describe('Email candidates to institute according to course.');

Artisan::command('send_subscription_expiry_email', function () {
    $this->comment(SendSubscriptionExpiryEmail::handle());
})->describe('send subscription expiry email.');

Artisan::command('send_email_expire_availability', function () {
	$this->comment('Job started...');
    $this->comment(SendEmailExpireAvailability::handle());
    $this->info('Mail sent successfully.');
})->describe('send subscription expiry email after some days.');

Artisan::command('send_emails_to_seafarer_batch_full_payment', function () {
	$this->comment('Job started...');
    $this->comment(SendEmailSeafarerForFullPayment::handle());
    $this->info('Mail sent successfully.');
})->describe('send batch full payment notification before 15 days.');

