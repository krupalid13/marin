$(document).ready(function(){
    
    $.ajaxSetup({
        headers:
        { 'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content') }
    });

    var searchAjaxCall = "undefined";
    date = moment().format('DD-MM-YYYY');

    birth_date = moment().subtract(17, 'years').format('DD-MM-YYYY');

    $(document).on('change', '.country', function () {
       var block_index = $(this).attr('block-index');
       if( $(this).val() == india_value ) {
           $(".state-block-"+block_index).find('.fields-for-india').removeClass('hide');
           $(".state-block-"+block_index).find('.fields-not-for-india').addClass('hide');

           $(".city-block-"+block_index).find('.fields-for-india').removeClass('hide');
           $(".city-block-"+block_index).find('.fields-not-for-india').addClass('hide');

           $("[name='pincode["+block_index+"]']").rules("add", {
               maxlength: 6,
               messages: {
                   maxlength: "Please provide valid 6 digit pincode"
               }
           });

           $("[name='pincode["+block_index+"]']").addClass('pin_code_fetch_input');
       } else {
           $(".state-block-"+block_index).find('.fields-for-india').addClass('hide');
           $(".state-block-"+block_index).find('.fields-not-for-india').removeClass('hide');

           $(".city-block-"+block_index).find('.fields-for-india').addClass('hide');
           $(".city-block-"+block_index).find('.fields-not-for-india').removeClass('hide');

           $("[name='pincode["+block_index+"]']").removeClass('pin_code_fetch_input');
           
           $("[name='pincode["+block_index+"]']").rules('remove','maxlength');


       }
    });


    $('.issue-datepicker').datepicker({
        format: "dd-mm-yyyy",
        endDate: date,
        autoClose: true,
        startView: 2
    }).on('changeDate', function(ev) {
        if(ev.viewMode === 0){
            $('.issue-datepicker').datepicker('hide');
        }
    });

    $('.birth-date-datepicker').datepicker({
        format: "dd-mm-yyyy",
        endDate: birth_date,
        autoClose: true,
        startView: 2
    }).on('changeDate', function(ev) {
        if(ev.viewMode === 0){
            $('.birth-date-datepicker').datepicker('hide');
        }
    });

    $('.datepicker').datepicker({
        format: "dd-mm-yyyy",
        startDate: date,
        autoClose: true,
        startView: 2
    }).on('changeDate', function(ev) {
        if(ev.viewMode === 0){
            $('.datepicker').datepicker('hide');
        }
    });

    
    $('.m-y-datepicker').datepicker({
        format: "mm-yyyy",
        startDate: date,
        autoClose: true,
        startView: 2,
        minViewMode: "months"
    }).on('changeDate', function(ev) {
        if(ev.viewMode === 1){
            $('.m-y-datepicker').datepicker('hide');
        }
    });
    
    var loadDetails = false;
    $(document).on("keyup", ".pin_code_fetch_input", function(){
        var value = $(this).val();
        var form_id = $(this).data("form-id");
        var block_index = $(this).attr('block-index');

        $(".pincode-error-message").remove();

        if($.isNumeric(value) && value.length == 6){
            while(loadDetails == false) {
                $(".pincode-loader-"+block_index).removeClass("hide");
                var url = $("#api-pincode-related-data").val();
                var pincode = value;
                loadDetails = true;
                $.ajax({
                    url : url,
                    method : "GET",
                    data : {
                        pincode : pincode
                    },
                    statusCode : {
                        200 : function(response) {

                            if(response.result.length > 0) {
                                $("#" + form_id).find(".pincode-block-"+block_index+" .pincode-id").val(response.result[0].id);

                                $(".pincode-loader-"+block_index).addClass("hide");
                                // state
                                var states_list = response.result[0].pincodes_states;
                                $("#" + form_id).find("select[name='state["+block_index+"]'] option").remove();
                               
                                if(states_list.length == 1) {

                                    for(var s = 0; s < states_list.length; s++) {
                                        var s_id = states_list[s].state.id;
                                        str = states_list[s].state.name.toLowerCase().replace(/\b[a-z]/g, function(letter) {
                                            return letter.toUpperCase();
                                        });
                                        var s_name = str;
                                        $("#" + form_id).find("select[name='state["+block_index+"]']").append("<option value='"+s_id+"' selected>"+s_name+"</option>");
                                    }

                                } else {

                                    $("#" + form_id).find("select[name='state["+block_index+"]']").append("<option value='' selected> Select a state </option>");
                                    for(var s = 0; s < states_list.length; s++) {
                                        var s_id = states_list[s].state.id;
                                        str = states_list[s].state.name.toLowerCase().replace(/\b[a-z]/g, function(letter) {
                                            return letter.toUpperCase();
                                        });
                                        var s_name = str;
                                        $("#" + form_id).find("select[name='state["+block_index+"]']").append("<option value='"+s_id+"'>"+s_name+"</option>");
                                    }

                                }

                                // city
                                var cities_list = response.result[0].pincodes_cities;
                                $("#" + form_id).find("select[name='city["+block_index+"]'] option").remove();

                                if(cities_list.length == 1) {

                                    for(var c = 0; c < cities_list.length; c++) {
                                        var c_id = cities_list[c].city.id;
                                        str = cities_list[c].city.name.toLowerCase().replace(/\b[a-z]/g, function(letter) {
                                            return letter.toUpperCase();
                                        });
                                        var c_name = str;
                                        $("#" + form_id).find("select[name='city["+block_index+"]']").append("<option value='"+c_id+"' selected>"+c_name+"</option>");
                                    }

                                } else {

                                    $("#" + form_id).find("select[name='city["+block_index+"]']").append("<option value='' selected> Select a city </option>");

                                    for(var c = 0; c < cities_list.length; c++) {
                                        var c_id = cities_list[c].city.id;
                                        str = cities_list[c].city.name.toLowerCase().replace(/\b[a-z]/g, function(letter) {
                                            return letter.toUpperCase();
                                        });
                                        var c_name = str;
                                        $("#" + form_id).find("select[name='city["+block_index+"]']").append("<option value='"+c_id+"'>"+c_name+"</option>");
                                    }

                                }

                            } else {

                                // state
                                $("#" + form_id).find("select[name='state["+block_index+"]'] option").remove();
                                $("#" + form_id).find("select[name='state["+block_index+"]']").append("<option value='' selected disabled> Select a state </option>");

                                // city
                                $("#" + form_id).find("select[name='city["+block_index+"]'] option").remove();
                                $("#" + form_id).find("select[name='city["+block_index+"]']").append("<option value='' selected disabled> Select a city </option>");

                                // area
                                // $("#" + form_id).find("select[name='area["+block_index+"]'] option").remove();
                                // $("#" + form_id).find("select[name='area["+block_index+"]']").append("<option value='' selected disabled> Select a area </option>");
                            }
                        },
                        400 :  function(response) {
                            $(".pincode-loader-"+block_index).addClass("hide");
                            $("#"+form_id +" .pin_code_fetch_input").closest("div").append("<div class='pincode-error-message'>Invalid pincode. Please enter a correct pin code.</div>");
                            // state
                            $("#"+form_id).find("select[name='state["+block_index+"]'] option").remove();
                            $("#"+form_id).find("select[name='state["+block_index+"]']").append("<option value=''>Select a state</option>");

                            // city
                            $("#"+form_id).find("select[name='city["+block_index+"]'] option").remove();
                            $("#"+form_id).find("select[name='city["+block_index+"]']").append("<option value=''>Select a city</option>");

                            // area
                            // $("#"+form_id).find("select[name='area["+block_index+"]'] option").remove();
                            // $("#"+form_id).find("select[name='area["+block_index+"]']").append("<option value=''>Select a area</option>");

                        }
                    }
                });
            }
        } else if($.isNumeric(value) && value.length < 6) {
            loadDetails = false;
            $(".pincode-loader"+block_index).addClass("hide");

        }
    });

    $(document).on('submit', '.listing-search-form', function(e){
        e.preventDefault();
        var formId = $(this).closest('form');
        changeSearchState(formId);
    });

    $(document).on('submit', '.filter-search-form', function(e){
        e.preventDefault();
        var formId = $(".listing-search-form");
        var filtersformId = $(this);
        changeSearchState(formId,filtersformId);
    });


    $(document).on('click', 'ul.pagination a', function(e) {
        if (typeof dont_triger_history == 'undefined') {
            e.preventDefault();
            var url = $(this).attr('href');
            var page = getUrlParameter('page',url);
            setGetParameter('page',page);
        }
    });

    /* uSed by all listing search and filtering pages in the admin panel */
    function changeSearchState(formId,filtersformId)
    {   
        var formData = formId.serializeArray();
        if(typeof filtersformId == "undefined")
        {
        var params = '?'+$.param(formData);
        
        }
        else
        {
        var filtersformData = filtersformId.serializeArray();

        var params = '?'+$.param(formData)+'&'+$.param(filtersformData);
        
        }    
        History.pushState(params, null, params);
    }

    /* uSed by all listing search and filtering pages in the admin panel */
    function fetchSearchResults(url, successCallback, errorCallback)
    {
        /*$.each(filtersformData, function( index, value ) {
            console.log($("#"+filtersformData[index]['name']) ,filtersformData[index][value]);
            //console.log(filtersformData[index]['name']);
        });*/

        if(searchAjaxCall != "undefined")
        {
            searchAjaxCall.abort();
            searchAjaxCall = "undefined";
        }

        $('#search-results-main-container').addClass('hide');
        $('.full-cnt-loader').removeClass('hidden');

        searchAjaxCall = $.ajax({
            url: url,
            dataType: 'json',
            statusCode: {
                200: function(response) {

                    if (typeof response.data != 'undefined' && response.data.status == 'success') {
                       
                        $('#search-results-main-container').removeClass('hide');
                        $('.full-cnt-loader').addClass('hidden');

                        $('#search-results-main-container').html(response.template);
                        

                        if(typeof successCallback != "undefined")
                        {
                            successCallback();
                        }

                    }
                }
            },
            error: function(error, response) {

                if(typeof errorCallback != "undefined")
                {
                    errorCallback();
                }
            }
        });
    }

    var getUrlParameter = function getUrlParameter(sParam,url) {
        var sPageURL = url,
            sURLVariables = sPageURL.split('?'),
            sParameterName,
            i;

        
        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');

            if (sParameterName[0] === sParam) {
                return sParameterName[1] === undefined ? true : sParameterName[1];
            }
        }
    }

    function setGetParameter(paramName, paramValue)
    {
        var url = document.URL;
        var hash = location.hash;
        url = url.replace(hash, '');
        if (url.indexOf(paramName + "=") >= 0)
        {
            var prefix = url.substring(0, url.indexOf(paramName));
            var suffix = url.substring(url.indexOf(paramName));
            suffix = suffix.substring(suffix.indexOf("=") + 1);
            suffix = (suffix.indexOf("&") >= 0) ? suffix.substring(suffix.indexOf("&")) : "";
            url = prefix + paramName + "=" + paramValue + suffix;
        }
        else
        {
        if (url.indexOf("?") < 0)
            url += "?" + paramName + "=" + paramValue;
        else
            url += "&" + paramName + "=" + paramValue;
        }
        var params = url + hash;
        History.pushState(params, null, params);
    }

    $("#state").on('change', function(e){
        e.preventDefault();
        var temp_url = $("#api-admin-state-list").val();
        var url_arr =temp_url.split("%");
        var url = url_arr[0] + parseInt($(this).val());

        $.ajax({
            type: "get",
            url: url,
            statusCode: {
                200:function (response) {
                    $("#city option").remove();
                    $("#city").append("<option value=''>Select City</option>");
                    $.each(response,function(index,value){
                        $("#city").append('<option value='+value.id+'>'+value.name+'</option>');
                    }); 
                },
                400:function (response) {
                   
                },
                422:function (response) {

                }
            }
        });
    });

    $(document).on('click',"#download_excel",function(e){

        $type = $(this).data('type');

        if($type == 'company'){
            var url = $("#api-admin-company-download-excel").val();
            url = url+'?'+$(".filter-search-form").serialize();
        }
        if($type == 'seafarer'){
            var url = $("#api-admin-seafarer-download-excel").val();
            url = url+'?'+$(".filter-search-form").serialize();
        }
        
        $("#download_excel").attr('href',url);
        $("#download_excel").attr('target','_blank');

        //$("#download_excel").click();
        /*var newWindow = window.open("","");
        newWindow.location.href = url;
        window.close();*/
        /*$.ajax({
            type: "get",
            url: url,
            data: $(".filter-search-form").serialize(),
            statusCode: {
                200:function (response) {
                    
                },
                400:function (response) {
                   
                }
            }
        });*/
    });

    $(".notification_bell").on('click',function(e){
        
        $(".overlay_section").addClass('menu_overlay');
        if ($('.notification-main-view').css('display') == 'block')
        {
            $('.notification-main-view').css('display','none');
        }else
        {
            $('.notification-main-view').css('display','block');
        }
    });

    $(".overlay_section").on('click',function(e){
        $(this).removeClass('menu_overlay');
        $('.notification-main-view').css('display','none');
    });

    $.ajax({
        type: "GET",
        url: $("#api-get-user-notification").val(),
        headers: {
            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
        },
        statusCode: {
            200:function (response) {
                if(typeof(response.template) != 'undefined'){
                    if(response.template.length > 0){
                        $('.notification-content-view').append(response.template);
                    }

                    if(typeof(response.notification_count) != 'undefined'){
                        $('.notification-count').attr('data-notification',response.notification_count);

                        if(response.notification_count != '0'){
                            $('.notification-count').html(response.notification_count);
                            $('.notification-count').removeClass('hide');
                        }

                    }
                }
            },
            400:function (response) {
               
            }
        }
    });

    if(typeof(user_id) != 'undefined' && user_id != ''){

        var pubnub = new PubNub({
            subscribeKey: subscribeKey,
            publishKey: publishKey,
            ssl: true
        });

        pubnub.addListener({
            status: function(statusEvent) {

                if (statusEvent.category === "PNConnectedCategory") {
                    var payload = {
                        my: 'payload'
                    };
                    pubnub.publish(
                        { 
                            message: payload
                        }, 
                        function (status) {
                            // handle publish response
                        }
                    );
                }
            },
            message: function(data) {
                // handle data
                if(typeof(data.message) != 'undefined'){

                    if(typeof(data.message.notification_id) != 'undefined'){
                        var notification_id = data.message.notification_id;

                        $.ajax({
                            type: "POST",
                            url: $("#api-upload-user-notification").val(),
                            data: {
                                notification_id : notification_id,
                            },
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                            },
                            statusCode: {
                                200:function (response) {
                                   if(typeof(response.template) != 'undefined'){
                                        $('.no-notification').remove();
                                        $('.notification-content-view').prepend(response.template);
                                        $('.notification-count').removeClass('hide');

                                        var notification_count = $('.notification-count').data('notification') + 1;
                                        $('.notification-count').attr('data-notification',notification_count);
                                        $('.notification-count').html(notification_count);
                                    }

                                },
                                400:function (response) {
                                   

                                }
                            }
                        });
                    }
                }
            },
            presence: function(presenceEvent) {
                // handle presence
            }
        });

        var listen_to = user_id+'_Notification';

        pubnub.subscribe({
            channels: [listen_to],
        });
    }

    $(document).on('click','.notification',function (e) {
        var notification_id = $(this).data('notification');
        var url = $(this).data('url');

        $(this).removeClass('unread');

        $.ajax({
            type: "POST",
            url: $("#api-set-user-notification-read").val(),
            data: {
                notification_id : notification_id,
                url : url,
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            },
            statusCode: {
                200:function (response) {
                    var curr_notification = $('.notification-count').attr('data-notification');
                    console.log(curr_notification);

                    if(curr_notification != 1){
                        curr_notification = curr_notification - 1;
                        $('.notification-count').attr('data-notification',curr_notification);
                        $('.notification-count').html(curr_notification);
                    }else{
                        $('.notification-count').attr('data-notification',0);
                        $('.notification-count').html(0);
                        $('.notification-count').addClass('hide');
                    }
                    //window.location.href = url;
                },
                400:function (response) {

                }
            }
        });

    });

});

$(document).ready(function() {

    $('.form-forgot').validate({

        rules: {
            email: {
                required: true,
                email: true
            },
            messages: {
                email: {
                    required: "We need your email address to contact you",
                    email: "Your email address must be in the format of name@domain.com"
                },
            },
        }
    });

    $("#reset-password-submit-button").on('click',function(e){
        e.preventDefault();
        var l = Ladda.create(this);
            if($('.form-forgot').valid()) {
                l.start();
                $.ajax({
                    type: "POST",
                    url: $(".form-forgot").attr('action'),
                    data: $(".form-forgot").serialize(),
                    statusCode: {
                        200: function (response) {
                            toastr.success(response.message, 'success');
                            $(".go-back").trigger( "click" );
                            l.stop();
                        },
                        400: function (response) {
                            toastr.error(response.responseJSON.message, 'error');
                            l.stop();

                        },
                        422: function (response) {

                        }
                    }
                });
            }
    });

    $('.form-change-password').validate({

        rules: {
            password: {
                minlength: 6,
                required: true
            },
            cpassword: {
                required: true,
                minlength: 5,
                equalTo: "#password"
            },
            messages: {
                email: {
                    required: "We need your email address to contact you",
                    email: "Your email address must be in the format of name@domain.com"
                },
                cpassword: {
                    equalTo: "Confirm password is not matching with password "
                }
            },
        }
    });

    $("#admin-change-password-button").on('click',function(e){
        e.preventDefault();
        var l = Ladda.create(this);
        if($('.form-change-password').valid()) {
            l.start();
            $.ajax({
                type: "POST",
                url: $(".form-change-password").attr('action'),
                data: $(".form-change-password").serialize(),
                statusCode: {
                    200: function (response) {
                        toastr.success(response.message, 'success');
                        window.location.href = response.redirect_url;
                        l.stop();
                    },
                    400: function (response) {
                        toastr.error(response.responseJSON.message, 'error');
                        l.stop();

                    },
                    422: function (response) {

                    }
                }
            });
        }
    });
});
var SeafarerFormWizard = function () {
	"use strict";
    var wizardContent = $('#wizard');
    var wizardForm = $('#add-seafarer-form');
    var numberOfSteps = $('.swMain > ul > li').length;
    var enable = false;
    if($("#edit").length > 0){
        enable = true;
    }
    var initWizard = function () {
        // function to initiate Wizard Form
        wizardContent.smartWizard({
            selected: 0,
            keyNavigation: false,
            onLeaveStep: leaveAStepCallback,
            onShowStep: onShowStep,
            enableAllSteps: enable,
        });
        var numberOfSteps = 0;
        animateBar();
        initValidator();
    };
    var animateBar = function (val) {
        if ((typeof val == 'undefined') || val == "") {
            val = 1;
        };
        
        var valueNow = Math.floor(100 / numberOfSteps * val);
        $('.step-bar').css('width', valueNow + '%');
    };
    var validateCheckRadio = function (val) {
        $("input[type='radio'], input[type='checkbox']").on('ifChecked', function(event) {
			$(this).parent().closest(".has-error").removeClass("has-error").addClass("has-success").find(".help-block").remove().end().find('.symbol').addClass('ok');
		});
    };    
    var initValidator = function () {
        $.validator.addMethod("cardExpiry", function () {
            //if all values are selected
            if ($("#card_expiry_mm").val() != "" && $("#card_expiry_yyyy").val() != "") {
                return true;
            } else {
                return false;
            }
        }, 'Please select a month and year');
        $.validator.setDefaults({
            errorElement: "span", // contain the error msg in a span tag
            errorClass: 'help-block',
            errorPlacement: function (error, element) { // render error placement for each input type
                if (element.attr("type") == "radio" || element.attr("type") == "checkbox") { // for chosen elements, need to insert the error after the chosen container
                    error.insertAfter($(element).closest('.form-group').children('div').children().last());
                } else if (element.attr("name") == "card_expiry_mm" || element.attr("name") == "card_expiry_yyyy") {
                    error.appendTo($(element).closest('.form-group').children('div'));
                } else {
                    error.insertAfter(element);
                    // for other inputs, just perform default behavior
                }
            },
            ignore: ':hidden',
        //     rules: {
        //     firstname: {
        //         alpha: true,
        //         required: true
        //     },
        //     /*lastname: {
        //         lettersonly: true,
        //         minlength: 2,
        //         required: true
        //     },*/
        //     email: {
        //         required: true,
        //         email: true
        //     },
        //     password: {
        //         minlength: 6,
        //         required: true
        //     },
        //     cpassword: {
        //         required: true,
        //         minlength: 6,
        //         equalTo: "#password"
        //     },
        //     gender: {
        //         required: true
        //     },
        //     pincode: {
        //         required: true,
        //         number: true,
        //         minlength: 5
        //     },
        //     dob: {
        //         required: true,
        //     },
        //     mobile: {
        //         required: true,
        //         number: true,
        //         minlength: 10
        //     },
        //     '[country[0]': {
        //         required: true
        //     },
        //     'pincode[0]': {
        //         required: true
        //     },
        //     'state_name[0]': {
        //         required: true
        //     },
        //     'state[0]': {
        //         required: true
        //     },
        //     'city[0]': {
        //         required: true
        //     },
        //     'city_text[0]': {
        //         required: true
        //     },
        //     rank: {
        //         required: true
        //     },
        //     years: {
        //         required: true
        //     },
        //     months: {
        //         required: true
        //     }
        // },
        // messages: {
        //     firstname: {
        //         alpha: "Please enter only alphabets",
        //         required: "Please specify your full name"
        //     },
        //     lastname: {
        //         required: "Please specify your last name",
        //         lettersonly: "Please enter characters only"
        //     },
        //     email: {
        //         required: "We need your email address to contact you",
        //         email: "Your email address must be in the format of name@domain.com"
        //     },
        //     cpassword: {
        //         equalTo: "Confirm password is not matching with password "
        //     },
        //     gender: "Please check a gender!",
        //     rank_exp: {
        //         number: "Please enter experience in numbers"
        //     },
        //     mobile: {
        //         number: "Please enter valid Mobile number"
        //     }
        // },
            highlight: function (element) {
                $(element).closest('.help-block').removeClass('valid');
                // display OK icon
                $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
                // add the Bootstrap error class to the control group
            },
            unhighlight: function (element) { // revert the change done by hightlight
                $(element).closest('.form-group').removeClass('has-error');
                // set error class to the control group
            },
            success: function (label, element) {
                label.addClass('help-block valid');
                // mark the current input as valid and display OK icon
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
            }
        });
    };
    var displayConfirm = function () {
        $('.display-value', wizardForm).each(function () {
            var input = $('[name="' + $(this).attr("data-display") + '"]', wizardForm);
            if (input.attr("type") == "text" || input.attr("type") == "email" || input.is("textarea")) {
                $(this).html(input.val());
            } else if (input.is("select")) {
                $(this).html(input.find('option:selected').text());
            } else if (input.is(":radio") || input.is(":checkbox")) {

                $(this).html(input.filter(":checked").closest('label').text());
            } else if ($(this).attr("data-display") == 'card_expiry') {
                $(this).html($('[name="card_expiry_mm"]', wizardForm).val() + '/' + $('[name="card_expiry_yyyy"]', wizardForm).val());
            }
        });
    };


    $(document).on('click',".seafarer-next-btn",function(e){
        e.preventDefault();
        wizardForm.valid();
    });


    function check_form(form_id,type,l,finish_data){
        var formdata = new FormData();
        var action = wizardForm.attr('action');

        var optional = new Array();
        var current_rank = $("#current_rank").val();
        if(current_rank){
            var fields = required_fields[current_rank];

            for(var i=0; i<fields.length; i++) {
                var field_arr = fields[i].split('-');
                if(field_arr.length > 1) {
                    optional.push(field_arr[0]);
                }
            }
        }

        $("[name^='cdccountry']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select country"
                }
            });
        });

        $("[name^='cdcno']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please enter number"
                }
            });
        });

        $("[name^='cdc_issue']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select issue date"
                }
            });
        });

        $("[name^='cdc_exp']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select expiry date"
                }
            });
        });

       /* $("[name^='coecountry']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select country"
                }
            });
        });

        $("[name^='coeno']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please enter number"
                }
            });
        });

        $("[name^='cop_no']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please enter number"
                }
            });
        }); 

        $("[name^='cop_grade']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please enter grade"
                }
            });
        });*/

        if(jQuery.inArray('WATCH_KEEPING',optional) == -1){

            /*$("[name='watch_country']").each(function(){
                $(this).rules("add", {
                    required: true
                });
            });

            $("[name='deckengine']").each(function(){
                $(this).rules("add", {
                    required: true
                });
            });
            $("[name='watch_no']").each(function(){
                $(this).rules("add", {
                    required: true
                });
            });
            $("[name='watchissud']").each(function(){
                $(this).rules("add", {
                    required: true
                });
            });*/
        }

        if(jQuery.inArray('COC',optional) == -1){
            $("[name^='coc_country']").each(function(){
                $(this).rules("add", {
                    required: true,
                    messages: {
                        required: "Please enter country"
                    }
                });
            });

            $("[name^='coc_no']").each(function(){
                $(this).rules("add", {
                    required: true,
                    messages: {
                        required: "Please enter number"
                    }
                });
            });

            $("[name^='grade']").each(function(){
                $(this).rules("add", {
                    required: true,
                    messages: {
                        required: "Please enter grade"
                    }
                });
            }); 

            $("[name^='coc_exp']").each(function(){
                $(this).rules("add", {
                    required: true,
                    messages: {
                        required: "Please enter expiry date"
                    }
                });
            });
        }

        /*$("[name^='certificate_name']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select certificate name"
                }
            });
        });*/

        /*$("[name^='date_of_issue']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select issue date"
                }
            });
        });*/

        /*$("[name^='date_of_expiry']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select expiry date"
                }
            });
        });*/

        /*$("[name^='issue_by']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select issue by"
                }
            });
        });*/

        $("[name^='issuing_authority']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select issuing authority"
                }
            });
        });

        $("[name^='place_of_issue']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select place of issue"
                }
            });
        });

        /*$("[name^='certificate_no']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please enter certificate number"
                }
            });
        });*/

        if(form_id == 'step-3'){
            $("[name^='shipping_company']").each(function(){
                $(this).rules("add", {
                    required: true,
                    messages: {
                        required: "Please enter shipping company name"
                    }
                });
            });

            $("[name^='name_of_ship']").each(function(){
                $(this).rules("add", {
                    required: true,
                    messages: {
                        required: "Please enter ship name"
                    }
                });
            });

            $("[name^='ship_type']").each(function(){
                $(this).rules("add", {
                    required: true,
                    messages: {
                        required: "Please select ship type"
                    }
                });
            });

            $("[name^='rank']").each(function(){
                $(this).rules("add", {
                    required: true,
                    messages: {
                        required: "Please select rank"
                    }
                });
            });

            $("[name^='other_engine_type']").each(function(){
                $(this).rules("add", {
                    required: true,
                    messages: {
                        required: "Please enter other engine type",
                    }
                });
            });

            $("[name^='grt']").each(function(){
                $(this).rules("add", {
                    number: true,
                    messages: {
                        number: "Please enter grt in numbers"
                    }
                });
            });

            $("[name^='bhp']").each(function(){
                $(this).rules("add", {
                    number: true,
                    messages: {
                        number: "Please enter bhp in numbers"
                    }
                });
            });

            $("[name^='date_from']").each(function(){
                $(this).rules("add", {
                    required: true,
                    messages: {
                        required: "Please select sign on date"
                    }
                });
            });

            $("[name^='date_to']").each(function(){
                $(this).rules("add", {
                    required: true,
                    messages: {
                        required: "Please select sign off date"
                    }
                });
            });
        }
        
        if(form_id == 'step-4'){
            $("[name^='course_date_of_issue']").each(function(){
                $(this).rules("add", {
                    required: true,
                    messages: {
                        required: "Please select date of issue"
                    }
                });
            });

            
            $("[name^='value_added_course_date_of_issue']").each(function(){
                $(this).rules("add", {
                    required: true,
                    messages: {
                        required: "Please select date of issue"
                    }
                });
            });
        }

        if(wizardForm.valid()) {
            $('#'+form_id+' input').each(function(ele, input) {
                formdata.append(input.name,input.value);
            });
            $('#'+form_id+' textarea').each(function(ele, textarea) {
                formdata.append(textarea.name,textarea.value);
            });
            $('#'+form_id+' select').each(function(ele, input) {
                formdata.append(input.name,input.value);
            });
            $('#'+form_id+' input[type="radio"]:checked').each(function(ele, input) {
                formdata.append(input.name,input.value);
            });
            if(action == 'update'){
                var temp_url = $("#api-admin-store-seafarer-step-update-route").val();
                var url = temp_url.split('%');
                
                url = url[0]+$("#user_id").val()+'/'+type;
            }
            if(action == 'store'){
                var temp_url = $("#api-admin-store-seafarer-step-route").val();
                var url = temp_url.split('%');
                if($("#user_id").val())
                    url = url[0]+type+'/'+$("#user_id").val();
                else
                    url = url[0]+type;
            }
            
            l.start();
            $.ajax({
                type: "POST",
                url: url,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                },
                data: formdata,
                contentType: false,
                processData: false,
                statusCode: {
                    200:function (response) {
                        if(form_id == 'step-4'){
                            if(finish_data == 'finish'){
                                window.location.href = $('#api-admin-add-seafarer').val();
                            }else{
                                window.location.href = response.redirect_url;
                            }
                        }
                        if(response.user_id){
                            $('#user_id').val(response.user_id);
                        }
                        wizardContent.smartWizard("goForward");
                        l.stop();
                    },
                    400:function (response) {
                        l.stop();
                    },
                    422:function (response) {
                        for (var prop in response.responseJSON){
                            l.stop();
                            if(prop == "email"){
                                $("#email-box").addClass('has-error');
                                $("#email-error").removeClass('hide');
                                $("#email-error").text(response.responseJSON['email'][0]);
                            }
                            if(prop == "mobile"){
                                $("#mobile-box").addClass('has-error');
                                $("#mobile-error").removeClass('hide');
                                $("#mobile-error").text(response.responseJSON['mobile'][0]);
                            }
                        }
                    }
                }
            });
            return true;
        }
        else{
            return false;
        }
    }
    var onShowStep = function (obj, context) {
    	if(context.toStep == numberOfSteps){
    		$('.anchor').children("li:nth-child(" + context.toStep + ")").children("a").removeClass('wait');
            displayConfirm();
    	}
        $(".next-step").unbind("click").click(function (e) {
            e.preventDefault();
            var data = new FormData();
            var form_id = $(this).attr('data-form');
            var type = $(this).attr('data-type');
            var finish_data = $(this).attr('data-redirect');
            var l = Ladda.create(this);
            check_form(form_id,type,l,finish_data);
            
        });
        $(".back-step").unbind("click").click(function (e) {
            e.preventDefault();
            wizardContent.smartWizard("goBackward");
        });
        $(".three-step-skip").unbind("click").click(function(e) {
            e.preventDefault();
            $("#add-seafarer-form").removeData("validator");
            wizardContent.smartWizard("goForward");
        });
        $(".forth-step-skip").unbind("click").click(function(e) {
            e.preventDefault();
            var url = $("#api-admin-view-seafarer").val();
            window.location = url;
        });
        $(".finish-step").unbind("click").click(function (e) {
            e.preventDefault();
            onFinish(obj, context);
        });
        window.scrollTo(0,0);
    };
    var leaveAStepCallback = function (obj, context) {
        return validateSteps(context.fromStep, context.toStep);
        // return false to stay on step and true to continue navigation
    };
    var onFinish = function (obj, context) {
        if (validateAllSteps()) {
            alert('form submit function');
            $('.anchor').children("li").last().children("a").removeClass('wait').removeClass('selected').addClass('done').children('.stepNumber').addClass('animated tada');
            //wizardForm.submit();
        }
    };
    var validateSteps = function (stepnumber, nextstep) {
        var isStepValid = false;

        if (numberOfSteps >= nextstep && nextstep > stepnumber) {
        	
            // cache the form element selector
            if (wizardForm.valid()) { // validate the form
                wizardForm.validate().focusInvalid();
                for (var i=stepnumber; i<=nextstep; i++){
        		$('.anchor').children("li:nth-child(" + i + ")").not("li:nth-child(" + nextstep + ")").children("a").removeClass('wait').addClass('done').children('.stepNumber').addClass('animated tada');
        		}
                //focus the invalid fields
                animateBar(nextstep);
                isStepValid = true;
                return true;
            };
        } else if (nextstep < stepnumber) {
        	for (i=nextstep; i<=stepnumber; i++){
        		$('.anchor').children("li:nth-child(" + i + ")").children("a").addClass('wait').children('.stepNumber').removeClass('animated tada');
        	}
            
            animateBar(nextstep);
            return true;
        } 
    };
    var validateAllSteps = function () {
        var isStepValid = true;
        // all step validation logic
        return isStepValid;
    };
    return {
        init: function () {
            initWizard();
            validateCheckRadio();
        }
    };
}();
var FormWizard = function () {
	"use strict";
    var wizardContent = $('#wizard');
    var wizardForm = $('#add-company-form');
    var numberOfSteps = $('.swMain > ul > li').length;
    var enable = false;
    if($("#edit").length > 0){
        enable = true;
    }
    var initWizard = function () {
        // function to initiate Wizard Form
        wizardContent.smartWizard({
            selected: 0,
            keyNavigation: false,
            onLeaveStep: leaveAStepCallback,
            onShowStep: onShowStep,
            enableAllSteps: enable,
        });
        var numberOfSteps = 0;
        animateBar();
        initValidator();
    };
    var animateBar = function (val) {
        if ((typeof val == 'undefined') || val == "") {
            val = 1;
        };
        
        var valueNow = Math.floor(100 / numberOfSteps * val);
        $('.step-bar').css('width', valueNow + '%');
    };
    var validateCheckRadio = function (val) {
        $("input[type='radio'], input[type='checkbox']").on('ifChecked', function(event) {
			$(this).parent().closest(".has-error").removeClass("has-error").addClass("has-success").find(".help-block").remove().end().find('.symbol').addClass('ok');
		});
    };    
    var initValidator = function () {
        $.validator.addMethod("cardExpiry", function () {
            //if all values are selected
            if ($("#card_expiry_mm").val() != "" && $("#card_expiry_yyyy").val() != "") {
                return true;
            } else {
                return false;
            }
        }, 'Please select a month and year');
        $.validator.setDefaults({
            errorElement: "span", // contain the error msg in a span tag
            errorClass: 'help-block',
            errorPlacement: function (error, element) { // render error placement for each input type
                if (element.attr("type") == "radio" || element.attr("type") == "checkbox") { // for chosen elements, need to insert the error after the chosen container
                    error.insertAfter($(element).closest('.form-group').children('div').children().last());
                } else if (element.attr("name") == "card_expiry_mm" || element.attr("name") == "card_expiry_yyyy") {
                    error.appendTo($(element).closest('.form-group').children('div'));
                } else {
                    error.insertAfter(element);
                    // for other inputs, just perform default behavior
                }
            },
            ignore: ':hidden',
        //     rules: {
        //     firstname: {
        //         alpha: true,
        //         required: true
        //     },
        //     /*lastname: {
        //         lettersonly: true,
        //         minlength: 2,
        //         required: true
        //     },*/
        //     email: {
        //         required: true,
        //         email: true
        //     },
        //     password: {
        //         minlength: 6,
        //         required: true
        //     },
        //     cpassword: {
        //         required: true,
        //         minlength: 6,
        //         equalTo: "#password"
        //     },
        //     gender: {
        //         required: true
        //     },
        //     pincode: {
        //         required: true,
        //         number: true,
        //         minlength: 5
        //     },
        //     dob: {
        //         required: true,
        //     },
        //     mobile: {
        //         required: true,
        //         number: true,
        //         minlength: 10
        //     },
        //     '[country[0]': {
        //         required: true
        //     },
        //     'pincode[0]': {
        //         required: true
        //     },
        //     'state_name[0]': {
        //         required: true
        //     },
        //     'state[0]': {
        //         required: true
        //     },
        //     'city[0]': {
        //         required: true
        //     },
        //     'city_text[0]': {
        //         required: true
        //     },
        //     rank: {
        //         required: true
        //     },
        //     years: {
        //         required: true
        //     },
        //     months: {
        //         required: true
        //     }
        // },
        // messages: {
        //     firstname: {
        //         alpha: "Please enter only alphabets",
        //         required: "Please specify your full name"
        //     },
        //     lastname: {
        //         required: "Please specify your last name",
        //         lettersonly: "Please enter characters only"
        //     },
        //     email: {
        //         required: "We need your email address to contact you",
        //         email: "Your email address must be in the format of name@domain.com"
        //     },
        //     cpassword: {
        //         equalTo: "Confirm password is not matching with password "
        //     },
        //     gender: "Please check a gender!",
        //     rank_exp: {
        //         number: "Please enter experience in numbers"
        //     },
        //     mobile: {
        //         number: "Please enter valid Mobile number"
        //     }
        // },
            highlight: function (element) {
                $(element).closest('.help-block').removeClass('valid');
                // display OK icon
                $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
                // add the Bootstrap error class to the control group
            },
            unhighlight: function (element) { // revert the change done by hightlight
                $(element).closest('.form-group').removeClass('has-error');
                // set error class to the control group
            },
            success: function (label, element) {
                label.addClass('help-block valid');
                // mark the current input as valid and display OK icon
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
            }
        });
    };
    var displayConfirm = function () {
        $('.display-value', wizardForm).each(function () {
            var input = $('[name="' + $(this).attr("data-display") + '"]', wizardForm);
            if (input.attr("type") == "text" || input.attr("type") == "email" || input.is("textarea")) {
                $(this).html(input.val());
            } else if (input.is("select")) {
                $(this).html(input.find('option:selected').text());
            } else if (input.is(":radio") || input.is(":checkbox")) {

                $(this).html(input.filter(":checked").closest('label').text());
            } else if ($(this).attr("data-display") == 'card_expiry') {
                $(this).html($('[name="card_expiry_mm"]', wizardForm).val() + '/' + $('[name="card_expiry_yyyy"]', wizardForm).val());
            }
        });
    };

    function check_rpsl(){
        if($("#company_type").val() == 1){
            $("#rpsl-file-container").removeClass('hide');
        }else{
            $("#rpsl-file-container").addClass('hide');
        }
    }
    function check_form(form_id,type,l){
        var formdata = new FormData();
        var action = wizardForm.attr('action');

         $("[name^='country']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select country"
                }
            });
        });

        $("[name^='pincode']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please enter pincode"
                }
            } );
        });

        $("[name^='state']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select state"
                }
            } );
        });

        $("[name^='state_text']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select state"
                }
            } );
        });

        $("[name^='city']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select city"
                }
            } );
        });

        $("[name^='city_text']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select city"
                }
            } );
        });

        $("[name^='address']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please enter address"
                }
            } );
        });

        $("[name^='ship_type']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select ship type"
                }
            } );
        });

        $("[name^='telephone']").each(function(){
            $(this).rules("add", {
                number: true,
                messages: {
                    required: "Please enter numbers only"
                }
            } );
        });

        /*$("[name^='ship_flag']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select ship flag"
                }
            } );
        });*/

        $("[name^='grt']").each(function(){
            $(this).rules("add", {
                required: true,
                number: true,
                messages: {
                    required: "Please enter GRT"
                }
            } );
        });

        $("[name^='engine_type']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select engine type"
                }
            } );
        });

        $("[name^='bhp']").each(function(){
            $(this).rules("add", {
                required: true,
                number: true,
                messages: {
                    required: "Please enter bhp"
                }
            } );
        });

        /*$("[name^='built-year']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select year of built"
                }
            } );
        });*/

        $("[name^='p_i_cover_company_name']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please enter company name"
                }
            } );
        });

        if(wizardForm.valid()) {
            check_rpsl();
            $('#'+form_id+' input').each(function(ele, input) {
                formdata.append(input.name,input.value);
            });
            $('#'+form_id+' select').each(function(ele, input) {
                formdata.append(input.name,input.value);
            });
            $('#'+form_id+' input[type="radio"]:checked').each(function(ele, input) {
                formdata.append(input.name,input.value);
            });
            $('#'+form_id+' textarea').each(function(ele, input) {
                formdata.append(input.name,input.value);
            });

            if(action == 'update'){
                var temp_url = $("#api-admin-store-company-step-update-route").val();
                var url = temp_url.split('%');
                
                url = url[0]+type+'/'+$("#company_id").val();
            }
            if(action == 'store'){
                var temp_url = $("#api-admin-store-company-step-route").val();
                var url = temp_url.split('%');
                if($("#company_id").val())
                    url = url[0]+type+'/'+$("#company_id").val();
                else
                     url = url[0]+type;
            }
            

            $("input[type='file']").each(function(index,element){
                if($(this)[0].files.length > 0){
                    var temp_file_anme = $(this).attr('name');
                    var temp_arr = temp_file_anme.split('_1');
                    var file_name = temp_arr[0];
                    formdata.append(file_name,$(this)[0].files[0]);
                }
            });

            l.start();
            $.ajax({
                type: "POST",
                url: url,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                },
                data: formdata,
                contentType: false,
                processData: false,
                statusCode: {
                    200:function (response) {
                        if(response.company_id){
                            $('#company_id').val(response.company_id);
                        }
                        if(form_id == 'step-2'){
                            window.location.href = response.redirect_url;
                        }
                        wizardContent.smartWizard("goForward");
                        l.stop();
                    },
                    400:function (response) {
                        l.stop();
                    },
                    422:function (response) {
                        for (var prop in response.responseJSON){
                            if(prop == "email"){
                                $("#email-box").addClass('has-error');
                                $("#email-error").removeClass('hide');
                                $("#email-error").text(response.responseJSON['email'][0]);
                            }
                            if(prop == "mobile"){
                                $("#mobile-box").addClass('has-error');
                                $("#mobile-error").removeClass('hide');
                                $("#mobile-error").text(response.responseJSON['mobile'][0]);
                            }
                        }
                    }
                }
            });
            return true;
            //wizardContent.smartWizard("goForward");
        }else{
            return false;
        }
    }

    var onShowStep = function (obj, context) {
    	if(context.toStep == numberOfSteps){
    		$('.anchor').children("li:nth-child(" + context.toStep + ")").children("a").removeClass('wait');
            displayConfirm();
    	}
        $(".next-step").unbind("click").click(function (e) {
            e.preventDefault();
            var data = new FormData();
            var form_id = $(this).attr('data-form');
            var type = $(this).attr('data-type');
            var l = Ladda.create(this);
            check_form(form_id,type,l);

        });
        $(".back-step").unbind("click").click(function (e) {
            e.preventDefault();
            wizardContent.smartWizard("goBackward");
        });
        $(".finish-step").unbind("click").click(function (e) {
            e.preventDefault();
            onFinish(obj, context);
        });
        window.scrollTo(0,0);
    };
    var leaveAStepCallback = function (obj, context) {
        return validateSteps(context.fromStep, context.toStep);
        // return false to stay on step and true to continue navigation
    };
    var onFinish = function (obj, context) {
        if (validateAllSteps()) {
            alert('form submit function');
            $('.anchor').children("li").last().children("a").removeClass('wait').removeClass('selected').addClass('done').children('.stepNumber').addClass('animated tada');
            //wizardForm.submit();
        }
    };
    var validateSteps = function (stepnumber, nextstep) {
        var isStepValid = false;
        
        
        if (numberOfSteps >= nextstep && nextstep > stepnumber) {
        	
            // cache the form element selector
            if (wizardForm.valid()) { // validate the form
                wizardForm.validate().focusInvalid();
                for (var i=stepnumber; i<=nextstep; i++){
        		$('.anchor').children("li:nth-child(" + i + ")").not("li:nth-child(" + nextstep + ")").children("a").removeClass('wait').addClass('done').children('.stepNumber').addClass('animated tada');
        		}
                //focus the invalid fields
                animateBar(nextstep);
                isStepValid = true;
                return true;
            };
        } else if (nextstep < stepnumber) {
        	for (i=nextstep; i<=stepnumber; i++){
        		$('.anchor').children("li:nth-child(" + i + ")").children("a").addClass('wait').children('.stepNumber').removeClass('animated tada');
        	}
            
            animateBar(nextstep);
            return true;
        } 
    };
    var validateAllSteps = function () {
        var isStepValid = true;
        // all step validation logic
        return isStepValid;
    };
    return {
        init: function () {
            initWizard();
            validateCheckRadio();
        }
    };
}();
$(document).ready(function() {

    $("#seafarerResetButton").on('click', function(){
        $(':input','.filter-search-form')
            .not(':button, :submit, :reset, :hidden')
            .val('')
            .removeAttr('checked')
            .removeAttr('selected');
    });

    $("#us").on('change', function(e){
        var us_check = $(this).val();
        if(us_check == '1'){
            $('.us-block').removeClass('hide');
        }else{
            $('.us-block').addClass('hide');
        }
    });

    $("input[name='pcc']").on('change', function(e){
        var pcc_check = $(this).val();
        if(pcc_check == '1'){
            $('.pcc-block').removeClass('hide');
        }else{
            $('.pcc-block').addClass('hide');
        }
    });

    $("input[name='yellowfever']").on('change', function(e){
        var yellowfever_check = $(this).val();
        if(yellowfever_check == '1'){
            $('.yellowfever-block').removeClass('hide');
        }else{
            $('.yellowfever-block').addClass('hide');
        }
    });

    $("#current_rank").on('change', function(e){
        
        $("#current_rank_id").val($(this).val());
        var fields = required_fields[$(this).val()];
        
        $(".required_fields").addClass('hide');
        //$(".required_fields input").val('');

        for(var i=0; i<fields.length; i++) {
            var field_arr = fields[i].split('-');
            if(field_arr.length > 1) {
                $("#"+field_arr[0]+"-details").removeClass('hide');
                $("#"+field_arr[0]+"-details span").addClass('hide' );
            } else{
                $("#"+fields[i]+"-details").removeClass('hide');
            }
        }
    });
    
  $(".change-status-user").on('click',function(e){
      var status = $(this).attr('data-status');
      var id = $(this).attr('data-id');
      var url = $('#api-admin-seafarer-change-status').val();

      $.ajax({
              type: "POST",
              url: url,
              headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
              },
              data: {
                status: status,
                id: id,
              },
              statusCode: {
                  200:function (response) {
                    if(status == 'active'){
                      $('#deactive-'+id).removeClass('hide');
                      $('#active-'+id).addClass('hide');
                    }else{
                      $('#active-'+id).removeClass('hide');
                      $('#deactive-'+id).addClass('hide');
                    }
                    toastr.success(response.message, 'Success');
                  },
                  400:function (response) {
                    toastr.error(response.message, 'Error');
                  }
              }
          });
  });

	$('#add-seafarer-form').validate({
		rules: {
            firstname: {
                alpha: true,
                required: true
            },
            /*lastname: {
                lettersonly: true,
                minlength: 2,
                required: true
            },*/
            email: {
                required: true,
                email: true,
                remote: {
                    url: $("#api-check-email").val(),
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content'),
                    },
                    type: "post",
                    async: false,
                    data: {
                        id: function(){ return $("input[name='user_id']").val()},
                        email: function(){ return $("#email").val(); }
                    }
                }
            },
            password: {
                minlength: 6,
                required: true
            },
            cpassword: {
                required: true,
                minlength: 6,
                equalTo: "#password"
            },
            gender: {
                required: true
            },
            pincode: {
                required: true,
                number: true,
                minlength: 5
            },
            dob: {
                required: true,
            },
            mobile: {
                required: true,
                number: true,
                minlength: 10,
                remote: {
                    url: $("#api-check-mobile").val(),
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content'),
                    },
                    type: "post",
                    async: false,
                    data: {
                        id: function(){ return $("input[name='user_id']").val()},
                        mobile: function(){ return $("#mobile").val(); }
                    }
                },
                notEqualTo: "#kin_number",
            },
            '[country[0]': {
                required: true
            },
            'pincode[0]': {
                required: true
            },
            'state_name[0]': {
                required: true
            },
            'state[0]': {
                required: true
            },
            'city[0]': {
                required: true
            },
            'city_text[0]': {
                required: true
            },
            current_rank: {
                required: true
            },
            years: {
                required: true
            },
            months: {
                required: true
            },
            passcountry: {
                required: true
            },
            passno: {
                required: true
            },
            passplace: {
                required: true
            },
            passdateofissue: {
                required: true
            },
            passdateofexp: {
                required: true
            },
            yf_issue_date: {
                required: true
            },
            us: {
                required: true
            },
            usvalid: {
                required: true
            },
            pccdate: {
                required: true
            },
            cdccountry: {
                required: true
            },
            cdcno: {
                required: true
            },
            cdc_exp: {
                required: true
            },
            ocdccountry: {
                required: true
            },
            ocdcno: {
                required: true
            },
            ocdc_exp: {
                required: true
            },
            coc_no: {
                required: true
            },
            coc_exp: {
                required: true
            },
            coc_country: {
                required: true
            },
            grade: {
                required: true
            },

            ococ_no: {
                required: true
            },
            ococ_exp: {
                required: true
            },
            ococ_country: {
                required: true
            },
            ograde: {
                required: true
            },
            /*gmdss_country: {
                required: true
            },
            gmdss_no: {
                required: true
            },
            gmdss_doe: {
                required: true
            },*/
            place_of_birth: {
                required: true
            },
            nationality: {
                required: true,
            },
            marital_status: {
                required: true,
            },
            height: {
                required: true,
                number: true,
            },
            weight: {
                required: true,
                number: true,
            },
            kin_name: {
                required: true,
                alpha: true
            },
            kin_relation: {
                required: true
            },
            kin_number: {
                required: true,
                number: true,
                minlength: 10,
                notEqualTo: "#mobile",
            },
            kin_alternate_no: {
                number: true,
            },
            permananent_address: {
                required: true
            },
            landline: {
                number: true
            },
            applied_rank: {
                required: true
            },
            date_avaibility: {
                required: true
            },
            last_wages: {
                required: true,
                number: true
            },
            /*name_of_school: {
                required: true
            },
            school_from: {
                required: true
            },
            school_to: {
                required: true
            },
            school_qualification: {
                required: true
            },
            institute_name: {
                required: true
            },
            institute_from: {
                required: true
            },
            institute_to: {
                required: true
            },
            institute_degree: {
                required: true
            }*/
        },
        messages: {
            firstname: {
                alpha: "Please enter only alphabets",
                required: "Please specify your full name"
            },
            lastname: {
                required: "Please specify your last name",
                lettersonly: "Please enter characters only"
            },
            email: {
                required: "We need your email address to contact you",
                email: "Your email address must be in the format of name@domain.com",
                remote: "Email already exist"
            },
            cpassword: {
                equalTo: "Confirm password is not matching with password "
            },
            gender: "Please check a gender!",
            rank_exp: {
                number: "Please enter experience in numbers"
            },
            mobile: {
                number: "Please enter valid Mobile number",
                remote: "Mobile already exist"
            },
            passcountry: "Please select passport country",
            passno: "Please enter Passport Number",
        }
	});

	$(document).on('click',".seafarer-next-btn",function(e){
        e.preventDefault();

        var optional = new Array();
        var current_rank = $("#current_rank").val();
        if(current_rank){
            var fields = required_fields[current_rank];

            for(var i=0; i<fields.length; i++) {
                var field_arr = fields[i].split('-');
                if(field_arr.length > 1) {
                    optional.push(field_arr[0]);
                }
            }
        }

        $("[name^='cdccountry']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select country"
                }
            });
        });

        $("[name^='cdcno']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please enter number"
                }
            });
        });

        $("[name^='cdc_issue']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select issue date"
                }
            });
        });

        $("[name^='cdc_exp']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select expiry date"
                }
            });
        });

        /*$("[name^='coecountry']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select country"
                }
            });
        });

        $("[name^='coeno']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please enter number"
                }
            });
        });*/

        /*$("[name^='coe_grade']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please enter grade"
                }
            });
        });

        $("[name^='coe_exp']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select expiry date"
                }
            });
        });

        $("[name^='cop_issue']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select issue date"
                }
            });
        });

        $("[name^='cop_exp']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select expiry date"
                }
            });
        });*/

        /*$("[name^='cop_no']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please enter number"
                }
            });
        }); 

        $("[name^='cop_grade']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please enter grade"
                }
            });
        });*/

        if(jQuery.inArray('WATCH_KEEPING',optional) == -1){

            $("[name='watch_country']").each(function(){
                $(this).rules("add", {
                    required: true
                });
            });

            $("[name='deckengine']").each(function(){
                $(this).rules("add", {
                    required: true
                });
            });
            $("[name='watch_no']").each(function(){
                $(this).rules("add", {
                    required: true
                });
            });
            $("[name='watchissud']").each(function(){
                $(this).rules("add", {
                    required: true
                });
            });
        }

        if(jQuery.inArray('COC',optional) == -1){
            $("[name^='coc_country']").each(function(){
                $(this).rules("add", {
                    required: true,
                    messages: {
                        required: "Please enter country"
                    }
                });
            });

            $("[name^='coc_no']").each(function(){
                $(this).rules("add", {
                    required: true,
                    messages: {
                        required: "Please enter number"
                    }
                });
            });

            $("[name^='grade']").each(function(){
                $(this).rules("add", {
                    required: true,
                    messages: {
                        required: "Please enter grade"
                    }
                });
            }); 

            $("[name^='coc_exp']").each(function(){
                $(this).rules("add", {
                    required: true,
                    messages: {
                        required: "Please enter expiry date"
                    }
                });
            });
        }

        /*$("[name^='value_added_certificate_name']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select certificate name"
                }
            });
        });

        $("[name^='value_added_date_of_issue']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select issue date"
                }
            });
        });

        $("[name^='value_added_issue_by']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select issue by"
                }
            });
        });

        $("[name^='value_added_issuing_authority']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select issuing authority"
                }
            });
        });

        $("[name^='value_added_place_of_issue']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select place of issue"
                }
            });
        });

        $("[name^='value_added_certificate_no']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please enter certificate number"
                }
            });
        });*/

        /*$("[name^='certificate_name']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select certificate name"
                }
            });
        });

        $("[name^='date_of_issue']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select issue date"
                }
            });
        });*/

        /*$("[name^='date_of_expiry']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select expiry date"
                }
            });
        });*/

        /*$("[name^='issue_by']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select issue by"
                }
            });
        });*/

        $("[name^='issuing_authority']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select issuing authority"
                }
            });
        });

        $("[name^='place_of_issue']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select place of issue"
                }
            });
        });

        /*$("[name^='certificate_no']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please enter certificate number"
                }
            });
        });*/

        $("[name^='shipping_company']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please enter shipping company name"
                }
            });
        });

        $("[name^='name_of_ship']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please enter ship name"
                }
            });
        });

        $("[name^='ship_type']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select ship type"
                }
            });
        });

        $("[name^='rank']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select rank"
                }
            });
        });

        $("[name^='grt']").each(function(){
            $(this).rules("add", {
                number: true,
                messages: {
                    number: "Please enter grt in numbers"
                }
            });
        });

        $("[name^='bhp']").each(function(){
            $(this).rules("add", {
                number: true,
                messages: {
                    number: "Please enter bhp in numbers"
                }
            });
        });

        /*$("[name^='engine_type']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select engine type",
                }
            });
        });*/

        $("[name^='date_from']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select sign on date"
                }
            });
        });

        $("[name^='date_to']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select sign off date"
                }
            });
        });

       /* $("[name^='ship_flag']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select ship flag"
                }
            });
        });*/

        $("[name^='course_date_of_issue']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select date of issue"
                }
            });
        });

        
        $("[name^='value_added_course_date_of_issue']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select date of issue"
                }
            });
        });

        $('#add-seafarer-form').valid();
    });

    $('#add-seafarer-finish-button').on('click', function(){

      if($('#add-seafarer-form').valid()){
        var l = Ladda.create(this);
        l.start();
        $.ajax({
            type: "POST",
            url: $("#add-seafarer-form").attr('action'),
            data: $("#add-seafarer-form").serialize(),
            statusCode: {
                200:function (response) {
                    toastr.success(response.message, 'Success');
                    l.stop();
                    window.location.href = response.redirect_url;
                },
                400:function (response) {
                    toastr.error(response.message, 'Error');
                    l.stop();
                },
                422:function (response) {
                    l.stop();
                    $("#step-4").hide();
                    $("#step-1").show();
                    for (var prop in response.responseJSON){
                        if(prop == "email"){
                            $("#email-box").addClass('has-error');
                            $("#email-error").removeClass('hide');
                            $("#email-error").text(response.responseJSON['email'][0]);
                        }
                        if(prop == "mobile"){
                            $("#mobile-box").addClass('has-error');
                            $("#mobile-error").removeClass('hide');
                            $("#mobile-error").text(response.responseJSON['mobile'][0]);
                        }
                    }
                    l.stop();
                }
            }
        });

      }

    });

    $(document).on('click','.resend_welcome_email',function (e) {
        $.ajax({
            type: "POST",
            url: $("#api-admin-send-welcome-email").val(),
            data: {
                id: $(this).data('id'),
            },
            statusCode: {
                200:function (response) {
                    toastr.success(response.message, 'Success');
                },
                400:function (response) {
                    toastr.error(response.responseJSON.message, 'Error');
                },
                422:function (response) {
                    
                }
            }
        });

    });

});
$(document).ready(function() {

    function check_rpsl(){
        if($("#company_type").val() == 1){
            $("#rpsl-file-container").removeClass('hide');
        }else{
            $("#rpsl-file-container").addClass('hide');
        }

    }

	$(document).on('click',".company-next-btn",function(e){
        e.preventDefault();
        check_rpsl();

        $("[name^='country']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select country"
                }
            });
        });

        $("[name^='pincode']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please enter pincode"
                }
            } );
        });

        $("[name^='state']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select state"
                }
            } );
        });

        $("[name^='state_text']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select state"
                }
            } );
        });

        $("[name^='city']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select city"
                }
            } );
        });

        $("[name^='city_text']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select city"
                }
            } );
        });

        $("[name^='address']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please enter address"
                }
            } );
        });

        $("[name^='ship_type']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select ship type"
                }
            } );
        });

        $("[name^='telephone']").each(function(){
            $(this).rules("add", {
                number: true,
                messages: {
                    required: "Please enter numbers only"
                }
            } );
        });

        /*$("[name^='ship_flag']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select ship flag"
                }
            } );
        });*/

        $("[name^='grt']").each(function(){
            $(this).rules("add", {
                required: true,
                number: true,
                messages: {
                    required: "Please enter GRT"
                }
            } );
        });

        /*$("[name^='engine_type']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select engine type"
                }
            } );
        });*/

        $("[name^='bhp']").each(function(){
            $(this).rules("add", {
                required: true,
                number: true,
                messages: {
                    required: "Please enter bhp"
                }
            } );
        });

        /*$("[name^='built-year']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select year of built"
                }
            } );
        });*/

        $("[name^='p_i_cover_company_name']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please enter company name"
                }
            } );
        });

        $("#add-company-form").valid();
        
    });

    $('#add-company-form').validate({
    	rules: {
            email: {
                required: true,
                email: true,
                remote: {
                    url: $("#api-check-email").val(),
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content'),
                    },
                    type: "post",
                    async: false,
                    data: {
                        id: function(){ return $("input[name='company_id']").val()},
                        email: function(){ return $("#email").val(); }
                    }
                }
            },
            password: {
                minlength: 6,
                required: true
            },
            cpassword: {
                required: true,
                minlength: 5,
                equalTo: "#password"
            },
            contact_person_number: {
                required: true,
                number: true,
                minlength: 10,
                remote: {
                    url: $("#api-check-mobile").val(),
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content'),
                    },
                    type: "post",
                    async: false,
                    data: {
                        id: function(){ return $("input[name='company_id']").val()},
                        mobile: function(){ return $("#contact_person_number").val(); }
                    }
                }
            },
            contact_person: {
                required: true,
                number: false
            },
            contact_person_email: {
                required: true,
                email: true
            },
            company_name: {
                minlength: 2,
                required: true
            },
            company_type: {
                required: true
            },
            company_description: {
                required: true
            },
            company_email: {
                required: true,
                email: true
            },
            company_contact_number: {
                number: true
            },
            website: {
                complete_url: true
            },
            fax : {
                minlength: 6
            },
            rpsl_no : {
                required: true
            },
            incorporation_number : {
                required: true
            }/*,
            pancard_number : {
                required: true
            }*/
        },
        messages: {
            email: {
                required: "We need your email address to contact you",
                email: "Your email address must be in the format of name@domain.com",
                remote: "Email already exist"
            },
            contact_person_email: {
                required: "We need your email address to contact you",
                email: "Your email address must be in the format of name@domain.com"
            },
            contact_person_number: {
                remote: "Mobile already exist"
            },
            cpassword: {
                equalTo: "Confirm password is not matching with password "
            },
            rank_exp: {
                number: "Please enter experience in numbers"
            },
            mobile: {
                number: "Please enter valid Mobile number"
            }
        },
   	});

    $('#add-company-finish-button').on('click', function(){
        
        if($("#add-company-form").valid()){
                
                var l = Ladda.create(this);
                var data = new FormData();
                var other_fields = $("#add-company-form").serializeArray();
                $.each(other_fields, function(key,input){
                    data.append(input.name,input.value);
                });
                $("input[type='file']").each(function(index,element){
                    if($(this)[0].files.length > 0){
                        var temp_file_anme = $(this).attr('name');
                        var temp_arr = temp_file_anme.split('_1');
                        var file_name = temp_arr[0];
                        data.append(file_name,$(this)[0].files[0]);
                    }
                });
                l.start();
                $.ajax({
                    type: "POST",
                    url: $("#add-company-form").attr('action'),
                    data: data,
                    contentType: false,
                    processData: false,
                    statusCode: {
                        200:function (response) {
                            window.location.href = response.redirect_url;
                            l.stop();
                        },
                        400:function (response) {
                            l.stop();

                        },
                        422:function (response) {
                            $("#step-2").hide();
                            $("#step-1").show();
                            var foo = response.responseJSON;
                            var text = '';
                            for (var prop in foo) {
                                text += foo[prop][0]+ '<br />';
                            }
                            $(".alert-box").show();
                            $(".alert-box").html(text);
                            l.stop();

                        }
                    }
                });
            }
        });

    $("#company_type").on('change', function(e){
        if($(this).val() == 1){
            $(".rpsl_number_textbox").removeClass('hide');
        }else{
            $(".rpsl_number_textbox").addClass('hide');
        }
    });

});
$(document).on('ready', function(){

	$('#add-schedule-form').validate({
        rules : {
            'date' : 'required',
            'time' : 'required',
            'country' : 'required',
            'msg_email' : 'required',
        },
        messages : {
            'schedule_date' : 'Please provide the date of schedule',
            'schedule_time' : "Please provide the time to schedule",
            'country' : 'Please select country',
            'msg_email' : 'Please enter message',
        },
    });


	$(document).on('ifClicked', '.city-checkbox', function(){

        $("#city-required-block").addClass('hide');

    	var sub_category = $(this);
    	var state_id = sub_category.closest('.city-select').attr('id');
    	var new_value = state_id.replace('state-id-', '');
		var category_checkbox = $('.state-location-container .state-select .checkbox').find(".state-checkbox[value='"+new_value+"']");
    	setTimeout(function () {
        	if (sub_category.is(':checked')) {
        		category_checkbox.iCheck('check');
        		$('.state-location-container').animate({
    		        scrollTop: category_checkbox.offset().top - $('.state-location-container').offset().top + $('.state-location-container').scrollTop() - 10
    		    }, 300);
        	} else {
        		var check_count = 0;
    			var all_sub_category = sub_category.closest('.city-select').find('.checkbox .city-checkbox').is(':checked');
    			if(!all_sub_category){
    				category_checkbox.iCheck('uncheck');
    			}
        	}

            if( $(".city-checkbox:checked").length == 0 ) {
                $("#city-required-block").removeClass('hide');
                $("#state-required-block").removeClass('hide');
            } else {
                $("#state-required-block").addClass('hide');
            }
    	}, 200);
	});

	$(document).on('change', '#email-country', function(){
    	var country_val  = $(this).val();
		if (country_val == 95) {
			$('.state-city-container').css('display','block');
		} else {
			$('.state-city-container').css('display','none');
		}
    });

    $(document).on('ifChanged', '.select-all-state', function(){

        $("#state-required-block").addClass('hide');

    	var select_all = $(this);
		if (select_all.is(':checked')) {
			$('.email-location-checkbox').iCheck('check'); 
		} else {
			$('.email-location-checkbox').iCheck('uncheck');
		}
        if( $(".state-checkbox:checked").length == 0 ) {
            $("#state-required-block").removeClass('hide');
            $("#city-required-block").removeClass('hide');
        } else {
            $("#city-required-block").addClass('hide');
        }
    });

    $(document).on('ifClicked', '.state-checkbox', function(){

        $("#state-required-block").addClass('hide');

		var state = $(this);
		var state_id = state.val();
    	var city = 'state-id-'+state_id;
    	setTimeout(function () {
            if (state.is(':checked')) {
            	$('#'+city+' .checkbox .email-location-checkbox').iCheck('check');

    		    $('.city-location-container').animate({
    		        scrollTop: $('#'+city).offset().top - $('.city-location-container').offset().top + $('.city-location-container').scrollTop()
    		    }, 300);
            } else {
            	$('#'+city+' .checkbox .email-location-checkbox').iCheck('uncheck');
            }
            if( $(".state-checkbox:checked").length == 0 ) {
                $("#state-required-block").removeClass('hide');
                $("#city-required-block").removeClass('hide');
            } else {
                $("#city-required-block").addClass('hide');
            }
    	}, 200);
    });


});
$(document).on('ready', function(){
	$(document).on('click', '.view-more-cities-button', function(e){
		$("#view-more-cities-modal").modal('show');
		$("#view-more-cities-modal .city-list-content").text($(this).data('cities'));
	});

	$(document).on('click', '.delete-schedule-button', function(e){
		var schedule_id = $(this).data('scheduled-id');
		var el = $(this);
		const modal = new Promise(function(resolve, reject){
	       $('#confirm-reject-modal').modal('show');
	       $('#confirm-reject-modal .modal-heading').html('Delete schedule confirm');
	       $('#confirm-reject-modal .modal-body-content').html('This action will delete the schedule. Are you sure you want to continue?');
	       $('#confirm-reject-modal #modal-confirm-btn').click(function(){
	           resolve(1);
	       });
	       $('#confirm-reject-modal #modal-reject-btn').click(function(){
	           reject(0);
	       });
        }).then(function(val){
        	$('#confirm-reject-modal').modal('hide');
        	$.ajax({
				url : delete_schedule_url,
				method : "POST",
				data : {
					id : schedule_id
				},
				statusCode : {
					200 : function(response) {
						var parent_el = $('#schedules-listing-container').find('tbody tr');
						if (parent_el.length == 1) {
							$('#schedules-listing-container').html('<div>NO schedule to show</div>');
							$('.search-count-content').remove();
						} else {
							el.closest('tr').remove();
						}
					},
					400 : function(response) {
						
					}
				}
				 
			});
	    }).catch(function(err){
	    	$('#confirm-reject-modal').modal('hide');
	    	
	    });

	});




});
// global variable

var jcrop_api;
var minimumProfilePicSize = 200 * 200;
var profilePicMinCropSize = { width: 200, height: 200 };

var image_width = 0;
var image_height = 0;

var xsize = $('.registration-profile-image').outerWidth(),
    ysize = $('.registration-profile-image').outerHeight(),
    crop_type = '';
    pic_type = '';


// ----------------- profile pic cropping ------------------------------------------------------------

function showToastr(message,type,closeButton,timeOut,title){
    toastr.options.timeOut = timeOut;
    toastr.options.closeButton = closeButton;

    if(typeof title != 'undefined'){

        if(type == 'success'){
            toastr.success(message,title);
        } else if(type == 'error') {
            toastr.error(message,title);
        } else if(type == 'info') {
            toastr.info(message,title);
        }

    } else {

        if(type == 'success'){
            toastr.success(message);
        } else if(type == 'error') {
            toastr.error(message);
        } else if(type == 'info') {
            toastr.info(message);
        }
    }

}


// -------- Reset profile pic crop area -----

function resetProfilePicCropArea(destroy_thisFormID) {
    if (typeof jcrop_api != 'undefined') {
        jcrop_api.destroy();
        jcrop_api = undefined;
        if(destroy_thisFormID)
            thisFormID = undefined;
        $('#profile-pic-crop-area').html('<div style="position:relative;max-width: 100%;margin-right:auto;margin-left:auto;"><img id="profile-pic-preview" src="" style="max-width: 100%;"></div>');
    }
}


// --- update  crop details ---

function updateCropDetails(e) {
    $('#'+thisFormID+' input[name="image-x"]').val(e.x);
    $('#'+thisFormID+' input[name="image-y"]').val(e.y);
    $('#'+thisFormID+' input[name="image-x2"]').val(e.x2);
    $('#'+thisFormID+' input[name="image-y2"]').val(e.y2);
    $('#'+thisFormID+' input[name="crop-w"]').val(e.w);
    $('#'+thisFormID+' input[name="crop-h"]').val(e.h);

    if( crop_type == 'logo' ) {
        xsize = e.w;
        ysize = e.h;

        if( e.w > 200 ) {
            xsize = 200;
        }

        if( e.h > 200 ) {
            ysize = 200;
        }
        $('#'+thisFormID+' .registration-profile-image #image-content').css({'width' : xsize+'px', 'height' : ysize+'px'});
    }

    // console.log(xsize, ysize, e.w, e.h,e.x,e.y);

    if(e.w > 0) {
        var rx = xsize / e.w;
        var ry = ysize / e.h;

        var new_width = Math.round(rx * image_width) + 'px';
        var new_height = Math.round(ry * image_height) + 'px';
        if( crop_type == 'logo' ){
            // new_width = Math.round(e.w)+'px';
            // new_height = Math.round(e.h)+'px';
        }

        // $('#'+thisFormID+' .registration-profile-image #preview').css({
        //     width: new_width,
        //     height: new_height,
        //     marginLeft: '-' + Math.round(rx * e.x) + 'px',
        //     marginTop: '-' + Math.round(ry * e.y) + 'px'
        // });
    }


}



// -------- profile pic handler

function profilePicSelectHandler(formID, inputName, type , pictype)
{
    thisFormID = formID;

    $('#'+thisFormID+' .registration-profile-image #preview').attr('src', $('#'+thisFormID+' .registration-profile-image #preview').attr('avatar-holder-src')).css({
       'max-width': '100%',
       'width': 'auto',
       'margin-left': 0,
       'margin-top': 0,
       'height': 'auto',
    });

    $('#'+thisFormID+' .registration-profile-image #image-content').css({'width' : '150px'});


    image_width = 0;
    image_height = 0;

    crop_type = type;
    pic_type = pictype;

    if(typeof type != 'undefined' && type == 'logo') {
        minimumProfilePicSize = 50 * 50;
        profilePicMinCropSize = { width: 100, height: 50 };  
    } else {
        minimumProfilePicSize = 200 * 200;
        profilePicMinCropSize = { width: 200, height: 200 };
    }
    
    $(".jcrop-handle").css({'width' : '10px !important','height' : '10px !important'});

    // get selected file
    var oFile = $('#'+thisFormID+' input[name="'+ inputName +'"]')[0].files[0];

    if(typeof oFile == 'undefined')
        return false;

    // check for image type (jpg and png are allowed)
    var rFilter = /^(image\/jpeg|image\/png|image\/bmp|image\/jpg)$/i;
    if (! rFilter.test(oFile.type)) {
        showToastr('Please select a valid image file (jpg, jpg, png, bmp are allowed).', 'error', true, 2000);
        return;
    }

    //check for the image size 1 MB

    var FileSize = oFile.size / 1024 / 1024; // in MB
    if (FileSize > 5) {
        showToastr('File size exceeds 5 MB.', 'error', true, 2000);
        return;
    }

    // destroy Jcrop if it is existed
    resetProfilePicCropArea(false);

    // preview element
    //var oImage = document.getElementById('profile-pic-preview');

    var oImage = document.getElementById('profile-pic-preview');

    // prepare HTML5 FileReader
    var oReader = new FileReader();
    oReader.onload = function(e) {
        // e.target.result contains the DataURL which we can use as a source of the image
        oImage.src = e.target.result;
        oImage.onload = function () { // onload event handler
            // check for image dimensions
            if (this.width * this.height < minimumProfilePicSize && typeof onlyPreview == 'undefined') {
                if(typeof type != 'undefined' && type == 'logo') {
                   showToastr('Please select an image having minimum dimension 50 * 50', 'error', true, 2000);
                } else {
                    showToastr('Please select an image having minimum dimension 200 * 200', 'error', true, 2000);
                }
                this.src = '';
                return;
            }

            if(typeof onlyPreview == 'undefined') {
                $('#profile-photo-modal').modal({
                    backdrop: 'static',
                    keyboard: false
                });
                $('#profile-photo-modal').modal('show');

                //$('#'+thisFormID).find('.fileupload-preview').html(oFile.name);

                $('#'+thisFormID+' input[name^="image-"]').val('');
                $('#'+thisFormID+' input[name^="crop-"]').val('');

                //hide avatar holder
                $('#avatar-holder').hide();
                $('#profile-pic-crop-area').show();

                setTimeout(function(){
                    // initialize Jcrop

                    if(typeof type != 'undefined' && type == 'logo') {
                        var jcrop_config_options = {
                            minSize: [profilePicMinCropSize.width, profilePicMinCropSize.height], // min crop size
                            // maxSize: [200, 200], // max crop size
                            bgFade: true, // use fade effect
                            bgOpacity: .3, // fade opacity
                            allowMove: true,
                            onChange: updateCropDetails,
                            // onSelect: updateCropDetails,
                            onRelease: function() {
                                $('#'+formID+' input[name="image-x"]').val('');
                                $('#'+formID+' input[name="image-y"]').val('');
                            } 
                        };
                    } else {
                        var jcrop_config_options = {
                            minSize: [profilePicMinCropSize.width, profilePicMinCropSize.height], // min crop size
                            aspectRatio : 1, // keep aspect ratio 1:1
                            // maxSize: [200, 200],
                            bgFade: true, // use fade effect
                            bgOpacity: .3, // fade opacity
                            allowMove: true,
                            // onSelect: updateCropDetails,
                            onChange: updateCropDetails,
                            onRelease: function() {
                                $('#'+formID+' input[name="image-x"]').val('');
                                $('#'+formID+' input[name="image-y"]').val('');
                            }
                        };
                    }

                    $('#profile-pic-crop-area div').Jcrop(jcrop_config_options, function() {
                        $('#profile-pic-crop-area .jcrop-holder>div').show();
                        showToastr('please crop the image and upload', 'info', true, 2000);
                        $('#'+thisFormID+' input[name="image-w"]').val($('#profile-pic-preview').width());
                        $('#'+thisFormID+' input[name="image-h"]').val($('#profile-pic-preview').height());

                        jcrop_api = this;

                        image_width = $("#profile-pic-crop-area").width();
                        image_height = $("#profile-pic-crop-area").height();

                        jcrop_api.setSelect([0,0,profilePicMinCropSize.width, profilePicMinCropSize.height]);
                        $("#"+thisFormID+" .profile_pic_text").hide();
                        //$("#"+thisFormID+" .upload-photo-container").css('border','0px');
                    });
                   
                }, 300);
            } else {
                $(onlyPreview).siblings('.preview').attr('src', $('#profile-pic-preview').attr('src'));
            }


        };
    };

    oReader.readAsDataURL(oFile);
}


// ---- preview image select handler -------
function previewImageSelectHandler(file_input, image_ele, callback)
{
    // get selected file
    var oFile = file_input[0].files[0];

    if(typeof oFile == 'undefined')
        return false;

    // check for image type (jpg and png are allowed)
    var rFilter = /^(image\/jpeg|image\/png|image\/bmp|image\/jpg)$/i;
    if (! rFilter.test(oFile.type)) {
        showToastr('Please select a valid image file (jpg, jpg, png, bmp are allowed).', 'error', true, 3000);
        image_ele.closest('li').remove();
        return;
    }

    image_ele.attr('alt', 'loading...');

    // prepare HTML5 FileReader
    var oReader = new FileReader();

    oReader.onload = function(e) {

        //$('#images-container').prepend('<li class="uploaded-img-preview"><div><img src="' + e.target.result + '"><div><a class="remove-uploaded-img" href="" image-index-"' + file_input_index + '">Remove</a></div></div></li>');

        var img;
        img = document.createElement("img");
        img.src = e.target.result;

        img.onload = function() {
            var canvas, ctx, resizeInfo, thumbnail, _ref, _ref1, _ref2, _ref3;

            canvas = document.createElement("canvas");
            ctx = canvas.getContext('2d');

            resizeInfo = thumnailResizeInfo(img);

            if (resizeInfo.trgWidth == null) {
                resizeInfo.trgWidth = resizeInfo.optWidth;
            }
            if (resizeInfo.trgHeight == null) {
                resizeInfo.trgHeight = resizeInfo.optHeight;
            }

            canvas.width = resizeInfo.trgWidth;
            canvas.height = resizeInfo.trgHeight;

            drawImageIOSFix(ctx, img, (_ref = resizeInfo.srcX) != null ? _ref : 0, (_ref1 = resizeInfo.srcY) != null ? _ref1 : 0, resizeInfo.srcWidth, resizeInfo.srcHeight, (_ref2 = resizeInfo.trgX) != null ? _ref2 : 0, (_ref3 = resizeInfo.trgY) != null ? _ref3 : 0, resizeInfo.trgWidth, resizeInfo.trgHeight);
            thumbnail = canvas.toDataURL("image/png");
            image_ele.attr('src', thumbnail);
            window[callback]();
            //$('#images-container').append('<li class="uploaded-img-preview"><div><img src="' + thumbnail + '"><div><a class="remove-uploaded-img" href="" image-index-"' + file_input_index + '">Remove</a></div></div><input type="file" name="images[' + file_input_index + '][file]" id="image-input-' + file_input_index + '" class="upload-image pos-absolute"><input type="hidden" class="image-order-input" name="images[' + file_input_index + '][order]"></li>');
        };
    };

    oReader.readAsDataURL(oFile);
}

// thumbnail resize function----
function thumnailResizeInfo(file)
{   
    var info, srcRatio, trgRatio;
    info = {
      srcX: 0,
      srcY: 0,
      srcWidth: file.width,
      srcHeight: file.height
    };
    srcRatio = file.width / file.height;
    info.optWidth = 100;
    info.optHeight = 100;
    if ((info.optWidth == null) && (info.optHeight == null)) {
      info.optWidth = info.srcWidth;
      info.optHeight = info.srcHeight;
    } else if (info.optWidth == null) {
      info.optWidth = srcRatio * info.optHeight;
    } else if (info.optHeight == null) {
      info.optHeight = (1 / srcRatio) * info.optWidth;
    }
    trgRatio = info.optWidth / info.optHeight;
    if (file.height < info.optHeight || file.width < info.optWidth) {
      info.trgHeight = info.srcHeight;
      info.trgWidth = info.srcWidth;
    } else {
      if (srcRatio > trgRatio) {
        info.srcHeight = file.height;
        info.srcWidth = info.srcHeight * trgRatio;
      } else {
        info.srcWidth = file.width;
        info.srcHeight = info.srcWidth / trgRatio;
      }
    }
    info.srcX = (file.width - info.srcWidth) / 2;
    info.srcY = (file.height - info.srcHeight) / 2;
    return info;    
}


//  delete vertical squash function --------
detectVerticalSquash = function(img) {
    var alpha, canvas, ctx, data, ey, ih, iw, py, ratio, sy;
    iw = img.naturalWidth;
    ih = img.naturalHeight;
    canvas = document.createElement("canvas");
    canvas.width = 1;
    canvas.height = ih;
    ctx = canvas.getContext("2d");
    ctx.drawImage(img, 0, 0);
    data = ctx.getImageData(0, 0, 1, ih).data;
    sy = 0;
    ey = ih;
    py = ih;
    while (py > sy) {
      alpha = data[(py - 1) * 4 + 3];
      if (alpha === 0) {
        ey = py;
      } else {
        sy = py;
      }
      py = (ey + sy) >> 1;
    }
    ratio = py / ih;
    if (ratio === 0) {
      return 1;
    } else {
      return ratio;
    }
};



// --- draw image IOSFIX function ----
drawImageIOSFix = function(ctx, img, sx, sy, sw, sh, dx, dy, dw, dh) {
    var vertSquashRatio;
    vertSquashRatio = detectVerticalSquash(img);
    return ctx.drawImage(img, sx, sy, sw, sh, dx, dy, dw, dh / vertSquashRatio);
};

// ---- save profile pic details
$(document).on('click', '#save-profile-pic-details', function(e) {
       //check if user has cropped the image before uploading
        if(!$('#'+thisFormID+' input[name="image-x2"]').val()) {
           var message = 'Please crop the image before uploading';
           showToastr(message, 'error', true, 2000);
           return;
        }

        var loader = Ladda.create(this);
        loader.start();

        if(thisFormID == 'add-brand-form') {
            $(".error-brand-logo").text("");
        }

        var file = $("#"+thisFormID+" #profile-pic")[0].files[0];
        if(crop_type == 'logo') {
            var url = $("#api-upload-company-logo-route").val();
        } else {
            var url = $("#api-upload-user-profile-route").val();
        }

        var data = new FormData();

        data.append("profile_pic",file);
        data.append("image-x", $('#'+thisFormID+' input[name="image-x"]').val());
        data.append("image-y",$('#'+thisFormID+' input[name="image-y"]').val());
        data.append("image-x2",$('#'+thisFormID+' input[name="image-x2"]').val());
        data.append("image-y2",$('#'+thisFormID+' input[name="image-y2"]').val());
        data.append("image-w",$('#'+thisFormID+' input[name="image-w"]').val());
        data.append("image-h",$('#'+thisFormID+' input[name="image-h"]').val());
        data.append("crop-w",$('#'+thisFormID+' input[name="crop-w"]').val());
        data.append("crop-h",$('#'+thisFormID+' input[name="crop-h"]').val());
        
        if($('#'+thisFormID+' input[name="role"]').length > 0){
            data.append("role",$('#'+thisFormID+' input[name="role"]').val());
        }

        if( crop_type == 'logo' ){
            if( $('#'+thisFormID+' input[name="company_id"]').val() != '' ) {
                data.append("company_id",$('#'+thisFormID+' input[name="company_id"]').val());
            }
            if( $('#'+thisFormID+' input[name="institute_id"]').val() != '' ) {
                data.append("institute_id",$('#'+thisFormID+' input[name="institute_id"]').val());
            }
            if($('#'+thisFormID+' input[name="logged_company_id"]').length > 0) {
                if( $('#'+thisFormID+' input[name="company_id"]').val() == '' ) {
                    data.append("company_id",$('#'+thisFormID+' input[name="logged_company_id"]').val());
                }
            }
        } else {
            if( $('#'+thisFormID+' input[name="user_id"]').val() != '' ) {
                data.append("user_id",$('#'+thisFormID+' input[name="user_id"]').val());
            }
        }

        if( pic_type == 'company'){
            var url = $("#api-company-logo").val();
            if( typeof admin_site != 'undefined'){
                var url = $("#api-admin-company-profile-path").val();
            }
        }
        else if( pic_type == 'institute' ){
            var url = $("#api-institute-logo").val();
            if( typeof admin_site != 'undefined'){
                var url = $("#api-admin-institute-logo").val();
            }
        }
        else{
            var url = $("#api-seafarer-profile-pic").val();
            if( typeof admin_site != 'undefined'){
                var url = $("#api-admin-seafarer-profile-path").val();
            }
        }
        
        if( pic_type == 'company_team' ){
            var url = $("#api-company-team-profile-pic").val();
        }

        if( pic_type == 'agent_team' ){
            var url = $("#api-agent-team-profile-pic").val();
        }
        
        $.ajax({
            url : url,
            method : "POST",
            data : data,
            headers: { 'X-CSRF-TOKEN': $('#'+thisFormID+' input[name="_token"]').val() },
            contentType : false,
            processData : false,
            statusCode : {
                200 : function(response) {
                    $('#'+thisFormID+' .registration-profile-image #preview').attr('src', '/'+response.stored_file);
                    $('#'+thisFormID+' input[name="uploaded-file-name"]').val(response.filename);
                    $('#'+thisFormID+' input[name="uploaded-file-path"]').val(response.stored_file);
                    $('#'+thisFormID+' input[name="image_stored_type"]').val(response.stored_type);
                    $('#'+thisFormID+' input[name="image_stored_file"]').val(response.stored_file);
                    $('#'+thisFormID+' input[name="image_file_name"]').val(response.filename);
                    if( crop_type == 'logo' ){
                        $('#'+thisFormID+' input[name="temp_company_id"]').val(response.company_id);
                    } else {
                        $('#'+thisFormID+' input[name="temp_user_id"]').val(response.user_id);
                    }
                    if($("#"+thisFormID+" img.view-value").length > 0) {
                        $("#"+thisFormID+" img.view-value").attr('src', '/'+response.stored_file);
                    }

                    loader.stop();

                    $('#profile-photo-modal').modal('hide');

                    // toastr.success(response.message, 'Success');
                },
                400 : function(response) {
                    loader.stop();
                    toastr.error(response.responseJSON.message, 'Error');
                }
            }
        });

   });

$(document).on('change', '.advertise-file-uplaod', function(){
    setTimeout(function(){
        $(".fileupload-new.thumbnail img").attr('src',$("#advertise_img img").attr('src'));
    },100);
});

// --- cancel profile pic changes
$(document).on('click', '#cancel-profile-pic-change', function(e) {

   e.preventDefault();

   if($('#'+thisFormID+' .registration-profile-image #preview').attr('src') == null){
        $(".profile_pic_text").show();
        $(".upload-photo-container").css('border','2px dashed #d8d7da');
   }

   $('input[name="profile_photo"]').val('');
   $('#'+thisFormID+' .registration-profile-image #preview').attr('src', $('#'+thisFormID+' .registration-profile-image #preview').attr('avatar-holder-src')).css({
       'max-width': '100%',
       'width': 'auto',
       'margin-left': 0,
       'margin-top': 0,
       'height': 'auto',
   });
   $('#profile-photo-modal').modal('hide');
});


$('#resend-otp-button').on('click', function(){
    $("#resend_otp_Modal").modal();
    $.ajax({
         type: "POST",
         url: $("#api-resend-otp-route").val(),
         data: {
             '_token' : $("input[name='_token']").val()
         },
         statusCode: {
             200:function (response) {
                $('#otp_resend_success').removeClass('hide');
                $("#resend-otp-button").addClass('disabled');
                $('#invalid_mob_error').addClass('hide');
             },
             400:function (response) {
                $('#mob_error_resend_otp').removeClass('hide');
             }
         }
    });
});

$('#resend-otp-button-again').on('click', function(){
    var l = Ladda.create(this);
    $(".alert").addClass('hide');
    l.start();
    $.ajax({
         type: "POST",
         url: $("#api-resend-otp-route").val(),
         data: {
             '_token' : $("input[name='_token']").val()
         },
         statusCode: {
             200:function (response) {
                $('#otp_resend_success').removeClass('hide');
                $("#resend-otp-button").addClass('disabled');
                $('#invalid_mob_error').addClass('hide');
                l.stop();
             },
             400:function (response) {
                $('#mob_error_resend_otp').removeClass('hide');
                l.stop();
             }
         }
    });
});

$('#mob-verification').validate({

    rules:{
        mob_otp: {
            required: true,
            minlength: 6,
            maxlength: 6,
            number: true
        }
    }
});

$("#mob_otp_submit_button").on('click',function(e){
    e.preventDefault();
    if($("#mob-verification").valid()){
        $.ajax({
            type: "POST",
            url: $("#mob-verification").attr('action'),
            data: $("#mob-verification").serialize(),
            statusCode: {
                200:function (response) {

                    if($("#current-route-name").val() == 'site.seafarer.registration'){
                        window.location.href = $("#api-auto-welcome-route").val();
                    }
                    if($("#current-route-name").val() == 'site.seafarer.edit.profile'){
                        window.location.href = $("#api-seafarer-profile-route").val();
                    }
                    toastr.success(response.message, 'Success');
                    setTimeout(function () {
                        location.reload();
                    }, 1000);
                },
                400:function (response) {
                    if(response.responseJSON.status == 'error'){
                        $('#otp_resend_success').addClass('hide');
                        $('#invalid_mob_error').addClass('hide');
                        $('#otp_failure').removeClass('hide');
                        $('#otp_failure').text(response.responseJSON.message);
                    }else{
                        $('#otp_resend_success').addClass('hide');
                        $('#invalid_mob_error').removeClass('hide');
                    }
                }
            }
        });
    }
});

$(document).on('click', '.dashboard-menu', function(e){
        
    e.preventDefault();
    var x = $(".navbar-container");
    if (x.hasClass('slider-add')) {
        x.removeClass("slider-add");
        $(".menubar-btn-down").css("display", 'block');
        $(".menubar-btn-up").css("display", 'none');
        $(".overlay").removeClass("overlay-fadein");
        setTimeout(function () {
          $(".overlay").css("display", 'none');
        }, 200);
    } else {
        x.addClass("slider-add");
        $(".menubar-btn-down").css("display", 'none');
        $(".menubar-btn-up").css("display", 'block');
        $(".overlay").css("display", 'block');
        setTimeout(function () {
          $(".overlay").addClass("overlay-fadein");
        }, 100);
    }
});

$(".overlay").click(function(){
    var x = $(this);
     $(".overlay").removeClass("overlay-fadein");
     $(".navbar-container").removeClass("slider-add");
     $(".overlay").removeClass("overlay-fadein");
     $(".menubar-btn-down").css("display", 'block');
     $(".menubar-btn-up").css("display", 'none');
    setTimeout(function () {
      x.css("display", 'none');
    }, 200);
});

$('#email_verify').on('click', function(){
    $('.resend_email_loader').removeClass('hide');
    $.ajax({
        type: "POST",
        url: $("#api-resend-email-route").val(),
        data: {
            '_token' : $("input[name='_token']").val()
        },
        statusCode: {
            200:function (response) {
                toastr.success(response.message, 'Success');
                $('#otp_resend_success').removeClass('hide');
                $("#resend-otp-button").addClass('disabled');
                $('#invalid_mob_error').addClass('hide');
                $('.resend_email_loader').addClass('hide');
                $('.alert-box-verification-email').addClass('hide');
            },
            400:function (response) {
                $('#mob_error_resend_otp').removeClass('hide');
                $('.resend_email_loader').addClass('hide');
                toastr.error(response.responseJSON.message, 'Error');
            },
            500:function (response) {
                $('.resend_email_loader').addClass('hide');
            }
        }
     });
});

$(window).load(function(e){
    if($("#verify_email").length > 0){
        if($("#verify_email").val() == '0'){
            $(".alert-box-verification-email").removeClass('hide');
        }
    }
    if($("#verify_mobile").length > 0){
        if($("#verify_mobile").val() == '0'){
            $(".alert-box-verification-mob").removeClass('hide');
        }
    }
});

$(document).on('ready', function(){

    $(document).on('click','.check-company-subscription',function (e) {
        e.preventDefault();
        var el = $(this);
        var feature_type = $(this).data('type');
        var extra_data = [];
        extra_data['seafarer_id'] = $(this).attr('seafarer-index');
        
        checkSubscriptionFeatureAvaiblity(feature_type,extra_data);
       $('#feature-avaibility-modal .feature-success-modal-btn').click(function(){
            var data = [];
            var l = Ladda.create(this);

            data['feature'] = feature_type;
            data['seafarer_id'] = extra_data['seafarer_id'];
            var temp_url =$("#api-site-company-candidate-download-cv").val();
            if(temp_url){
                var temp_arr = temp_url.split('%');
                var url = temp_arr[0]+data['seafarer_id'];
            }

            l.start();
            $.ajax({
                url: url,
                type: "post",
                data: {
                    data : data
                },
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                },
                statusCode: {
                    200:function (response) {
                        l.stop();
                        $("#feature-avaibility-modal").modal('hide');
                        if (data['feature'] == feature_1 || data['feature'] == cv_download) {
                            window.open(response.file_path,'_blank');
                        }
                    },
                    400:function (response) {
                       l.stop();
                       $("#feature-avaibility-modal").modal('hide');
                       toastr.error(response.responseJSON.message, 'Error');
                    },
                    422:function (response) {
                       l.stop();
                       $("#feature-avaibility-modal").modal('hide');
                       toastr.error(response.responseJSON.message, 'Error');

                    }
                }
            });
       });
       $('#feature-avaibility-modal .feature-failed-modal-btn').click(function(){
           
       });

        
        // if (feature_type == feature_1) {
        //     var seafarer_id = $(this).attr('seafarer-index');
        // } else if (feature_type == feature_3) {
        //     var feature_action = $(this).attr('data-action');
        // }

        
    });


    // $(".feature-success-modal-btn").on('click',function(e){
    //     var data = [];
    //     var l = Ladda.create(this);

    //     data['feature'] = $(this).attr('data-type');
    //     if (data['feature'] == feature_1) {
    //         data['seafarer_id'] = $(this).attr('data-id');
    //         var temp_url =$("#api-site-company-candidate-download-cv").val();
    //         if(temp_url){
    //             var temp_arr = temp_url.split('%');
    //             var url = temp_arr[0]+data['seafarer_id'];
    //         }
    //     }

    //     l.start();
    //     $.ajax({
    //         url: url,
    //         type: "post",
    //         data: {
    //             data : data
    //         },
    //         headers: {
    //             'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
    //         },
    //         statusCode: {
    //             200:function (response) {
    //                 l.stop();
    //                 $("#feature-avaibility-modal").modal('hide');
    //                 if (data['feature'] == feature_1) {
    //                     window.open(response.file_path,'_blank');
    //                 }
    //             },
    //             400:function (response) {
    //                l.stop();
    //                $("#feature-avaibility-modal").modal('hide');
    //                toastr.error(response.responseJSON.message, 'Error');
    //             },
    //             422:function (response) {
    //                l.stop();
    //                $("#feature-avaibility-modal").modal('hide');
    //                toastr.error(response.responseJSON.message, 'Error');

    //             }
    //         }
    //     });
        
    // });

});

if($(".featured_company_section").length > 0){
    var maxHeight = -1;

    $('.coss_card-contentTitle').each(function() {
        maxHeight = maxHeight > $(this).height() ? maxHeight : $(this).height();
    });
    
    $('.coss_card-contentTitle').css('height', maxHeight);
}

function checkSubscriptionFeatureAvaiblity(feature_type,extra_data){

    var url = check_company_subscription_url;
    $.ajax({
        url: url,
        type: "post",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
        },
        statusCode: {
            200:function (response) {
                if (response.status == 'success') {
                    $("#feature-avaibility-modal").modal('show');
                    $("#feature-avaibility-modal .modal-plan-name-container .modal-plan-name").html(response.result.plan_name);
                    var feature_html;
                    $.each( response.result.result, function( key, value ) {
                      feature_html += '<tr><td>'+ value.feature.title +'</td><td>'+ value.feature.count+' / '+value.feature.duration_type +'</td><td >'+ value.used +'</td><td >'+ value.available +'</td></tr>';
                    });
                    $("#feature-avaibility-modal .modal-body .table-body").html(feature_html);
                }

                $.each( response.result.result, function( key, value ) {
                  if (key == feature_type) {
                    if (value.status) {
                        $("#feature-avaibility-modal .modal-footer .feature-success-modal-btn").css('display','inline-block');
                        $("#feature-avaibility-modal .msg-error").css('display','none');
                        $("#feature-avaibility-modal .msg-success").css('display','block');

                        if (feature_type == feature_1) {
                            // $("#feature-avaibility-modal .modal-footer .feature-success-modal-btn").attr('data-id',seafarer_id);
                            // $("#feature-avaibility-modal .modal-footer .feature-success-modal-btn").attr('data-type',feature_type);
                            $("#feature-avaibility-modal .msg-success").text('CV download feature limit is available. Click continue to download');
                        } else if(feature_type == feature_3){
                            $("#feature-avaibility-modal .modal-footer .feature-success-modal-btn").attr('data-type',feature_type);
                            $("#feature-avaibility-modal .msg-success").text('Advertisement feature is available. Click continue to add advertisement');
                        }
                        $("#feature-avaibility-modal .msg-error").text('');
                    } else {
                        $("#feature-avaibility-modal .modal-footer .feature-success-modal-btn").css('display','none');
                        $("#feature-avaibility-modal .msg-error").css('display','block');
                        $("#feature-avaibility-modal .msg-success").css('display','none');
                        if (feature_type == feature_1) {
                            // $("#feature-avaibility-modal .modal-footer .feature-success-modal-btn").attr('data-id',seafarer_id);
                            // $("#feature-avaibility-modal .modal-footer .feature-success-modal-btn").attr('data-type',feature_type);
                            $("#feature-avaibility-modal .msg-error").text('CV download feature limit is over');
                        } else if(feature_type == feature_3){
                            $("#feature-avaibility-modal .msg-error").text('Advertisement feature is over, deactivate another advertisement to add or upgrade subscription');
                        }
                        $("#feature-avaibility-modal .msg-success").text('');
                    }

                  }
                });
                
            },
            400:function (response) {
               toastr.error(response.responseJSON.message, 'Error');
            }
        }
    });

    $('#feature-avaibility-modal .feature-success-modal-btn').off('click');
    $('#feature-avaibility-modal .feature-failed-modal-btn').off('click');

}
//# sourceMappingURL=admin_app.js.map
