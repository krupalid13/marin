
$(document).ready(function(){	

    $(function(){
	    var index = 0;
	    $('.content-block ul li').hover(function() {
	        $(this).addClass("active");
	        $(this).siblings("li").removeClass("active");
	        $('.content-block .img_block img').eq($(this).index()).addClass("active");
	        $('.content-block .img_block img').eq($(this).index()).siblings("img").removeClass("active");
	    }, function() {
	        $('.content-block .img_block img').eq($(this).index()).removeClass("active");
	        $('.content-block .img_block img').eq(index).addClass("active");
	    });

	    $('.content-block ul li').click(function(){
	        $(this).addClass("active");
	        $(this).siblings("li").removeClass("active");
	        index = $(this).index();
	        var element = $('.content-block .img_block img').eq($(this).index());
	        element.addClass("active");
	        element.siblings('img').removeClass("active");
	    });
	});

});


