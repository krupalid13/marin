$(document).on('ready', function(){
	$("#free-trial-button").on('click', function(e){
		e.preventDefault();
		window.location.href = sub_redirect_to;
	});


	$(".buy-feature-button").on('click', function(e){
		var el = $(this);
		var buy_type = el.data('buy-type');
		$('#buy-subcription-date-form input[name="subscription_date"]').prop( "checked", false );
		var subscription_id = el.data('subscription-id');
		$.each(subscription_arr, function( index, value ) {
		 	if (subscription_id == value.id) {
		 		$('#buy-subcription-date-modal').modal('show');
		 		$('#buy-subcription-date-form input[name="subscription_id"]').val(value.id);
		 		$('#buy-subcription-date-form .plan-name').html(value.title);
		 		$('#buy-subcription-date-form .plan-cost').html(value.amount);
		 		$('#buy-subcription-date-form .tax-cgst').html(cgst_percent);
		 		$('#buy-subcription-date-form .tax-sgst').html(sgst_percent);
		 		$('#buy-subcription-date-form .total-cost').html((((value.amount*cgst_percent)/100)+((value.amount*sgst_percent)/100)+(value.amount*1)).toFixed(2));
		 	}
		});

		if (buy_type == 'upgrade') {
			if ($('#buy-subcription-date-modal .upgrade-date').hasClass('hide')) {
				$('#buy-subcription-date-modal .upgrade-date').removeClass('hide');
			}
		} else {
			if (!$('#buy-subcription-date-modal .upgrade-date').hasClass('hide')) {
				$('#buy-subcription-date-modal .upgrade-date').addClass('hide');
			}
			// buySubscription(el);
		}
	});

	$(".buy-subscription-modal-confirm-btn").on('click', function(e){
		e.preventDefault();
		if ($('#buy-subcription-date-form').valid()) {
	    	var url = $("#api-proceed-to-payment-route").val();
	    	var loader = Ladda.create(this);
			loader.start();
			var form_data = $('#buy-subcription-date-form').serialize();
			$.ajax({
				url : url,
				method : "POST",
				data : form_data,
				statusCode: {
					200 : function(response) {
						loader.stop();
						$('#buy-subcription-date-modal').modal('hide');

						$("#payumoney-form").empty();
	                    $("#payumoney-form").append(response.payumoney_form);
	                    $("#payumoney_form").submit();
					},
					400 : function(response) {
						loader.stop();
						toastr.error(response.responseJSON.message, 'Error');
					}
				}
			});
		}
	});

    $(".buy-free-subscription-button").on('click', function(e){
    	e.preventDefault();
    	// window.location.href = $("#api-auto-welcome-route").val();
    	// var subscription_id = $(this).data('subscription-id');
    	var url = $("#api-activate-subscription-route").val();
    	var loader = Ladda.create(this);
		loader.start();
		// var form_data = $('#buy-subcription-date-form').serialize();
		$.ajax({
			url : url,
			method : "POST",
			headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                },
			statusCode: {
				200 : function(response) {
					loader.stop();
					toastr.success(response.message, 'Success');
					setTimeout(function(){
						 window.location.href = $("#api-auto-welcome-route").val();
					},800);
					
				},
				400 : function(response) {
					loader.stop();
					toastr.error(response.responseJSON.message, 'Error');
					setTimeout(function(){
						 window.location.href = home_route;
					},800);
				}
			}
		});
    });

    $("#buy-subcription-date-form").validate({
		errorElement: "div", 
        errorPlacement: function (error, element) { // render error placement for each input type
            if (element.attr("type") == "radio" || element.attr("type") == "checkbox" || element.attr("type") == "file" || element.attr("type") == "hidden") { // for chosen elements, need to insert the error after the chosen container
                error.insertAfter($(element).closest('.form-group').children('div').children().last());
            } else if (element.attr("name") == "card_expiry_mm" || element.attr("name") == "card_expiry_yyyy") {
                error.appendTo($(element).closest('.form-group').children('div'));
            } else {
                error.insertAfter(element);
                // for other inputs, just perform default behavior
            }
        },
		rules : {
			subscription_date : 'required',
		}
	});
});

//# sourceMappingURL=subscription.js.map
