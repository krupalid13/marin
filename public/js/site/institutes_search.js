$(document).ready(function() {
    

    if ($(".institutesSearchResult").is(":visible")) {

        var today = new Date();
        var dd = today.getDate();

        var mm = today.getMonth()+1; 
        var yyyy = today.getFullYear();
        $('.coss_datepicker').datetimepicker({
            startDate: yyyy+"-"+mm+"-"+dd,
            minView: 2,
            maxView: 4,
            setDate: '',
            sideBySide: true
        }).on('changeDate', function(ev) {
            var d = new Date(Date.parse(ev.date)).toLocaleString('sq-AL');
            var sp = d.split(" ");
            var selected_date = sp[0];

            const monthNames = ["January", "February", "March", "April", "May", "June","July", "August", "September", "October", "November", "December"];

            var splited_date = selected_date.split("-");
            $(".date-class").remove();
            $(".searchResult_tags").append('<span class="coss_tag date-class">'+splited_date[2]+" "+monthNames[splited_date[1]-1]+'<span class="close_tag date_close_tag">+</span></span>');
            getInstituteListByFilters('',selected_date);
        });
        $('.coss_datepicker .datetimepicker-days').find('.day').removeClass('active');

    }
    
    $(document).on('click', '.date_close_tag', function(e) {
        $('.coss_datepicker .datetimepicker-days').find('.day').removeClass('active');
    });

    $('.course_type_filter').on( 'change', function(){ getInstituteListByFilters(); } );
    $('.institute_course_name').on( 'change', function(){ getInstituteListByFilters(); } );

    $('.inst_chkbox').on( 'change', function(){ 
        if(this.checked) {
            $(".searchResult_tags").append('<span class="coss_tag '+$(this).data('id')+'" data-class='+$(this).data('id')+'>'+$(this).data('name')+'<span class="close_tag">+</span></span>');
        }else{
            $("."+$(this).data('id')).remove();
        }
        getInstituteListByFilters(); 
    });

    $('.loc_chkbox').on('change', function(){ 
        if(this.checked) {
            $(".searchResult_tags").append('<span class="coss_tag '+$(this).data('id')+'" data-class='+$(this).data('id')+'>'+$(this).data('name')+'<span class="close_tag">+</span></span>');
        }else{
            $("."+$(this).data('id')).remove();
        }
        getInstituteListByFilters(); 
    });

    var discount;
    $('.switch_checkbox').on('change', function(){ 
        if(this.checked) {
            discount = 1;
        }else{
            discount = 0;
        }
        getInstituteListByFilters(); 
    });

    $(document).on('click','.coss_tag',function (e) {
        $(this).remove();
        $('#'+$(this).data('class')).attr('checked', false);
        getInstituteListByFilters();
    });

    function getInstituteListByFilters(url=null,selected_date=null){
        var institute_name = [];
        // $(".loader_ele").addClass('loader');
        $(".loader_overlay").removeClass('hide');
        $(".loader_overlay").css('display','');

        institute_name = $('.inst_chkbox:checked').map(function(){
            return $(this).val();
        }).toArray();

        institute_location= $('.loc_chkbox:checked').map(function(){
            return $(this).val();
        }).toArray();

        if(!(url))
            var url = $('#api-site-list-course-search').val();

        course_name = $(".institute_course_name").val();
        
        $.ajax({
            type: "GET",
            url: url,
            data: {
                'course_type' : $(".course_type_filter").val(),
                'course_name' : course_name,
                'institute_name' : institute_name,
                'institute_location' : institute_location,
                'institute_date' : selected_date,
                'discount' : discount,
                '_token' : $("input[name='_token']").val()
            },
            statusCode: {
                200:function (response) {
                    $(".institute_list").empty();
                    $(".homepage-pg").empty();
                    $(".institute_list").append(response.template);
                    let pg_div = (response.template);
                    var parser = new DOMParser();
                    var $doc = parser.parseFromString(pg_div, 'text/html');
                    $As = $('#couse_pagi', $doc);
                    $(".loader_overlay").addClass('hide');
                    $(".loader_overlay").css('display','none');
                    $(".homepage-pg").append($As);
                },
                400:function (response) {
                    
                }
            }
        });
    }

    //New code for homage pagiantion

    $(document).on('click', 'ul.pagination a', function(e) {
        if (typeof dont_triger_history == 'undefined') {
            e.preventDefault();
            var url = $(this).attr('href');
            var page = getUrlParameter('page',url);
            setGetParameter('page',page);
        }
    });

    var getUrlParameter = function getUrlParameter(sParam,url) {
        var sPageURL = url,
            sURLVariables = sPageURL.split('?'),
            sParameterName,
            i;

        
        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');

            if (sParameterName[0] === sParam) {
                return sParameterName[1] === undefined ? true : sParameterName[1];
            }
        }
    }

    function setGetParameter(paramName, paramValue)
    {
        var url = document.URL;
        var hash = location.hash;
        url = url.replace(hash, '');
        if (url.indexOf(paramName + "=") >= 0)
        {
            var prefix = url.substring(0, url.indexOf(paramName));
            var suffix = url.substring(url.indexOf(paramName));
            suffix = suffix.substring(suffix.indexOf("=") + 1);
            suffix = (suffix.indexOf("&") >= 0) ? suffix.substring(suffix.indexOf("&")) : "";
            url = prefix + paramName + "=" + paramValue + suffix;
        }
        else
        {
        if (url.indexOf("?") < 0)
            url += "?" + paramName + "=" + paramValue;
        else
            url += "&" + paramName + "=" + paramValue;
        }
        var params = url + hash;
        History.pushState(params, null, params);
    }

    var searchAjaxCall = "undefined";

    $(document).ready(function(){
        History = window.History; // Note: We are using a capital H instead of a lower h
        
        History.Adapter.bind(window,'statechange', function() {

            var State = History.getState(); // Note: We are using History.getState() instead of event.state
            
            getInstituteListByFilters(State.url);

        }); 

    });

    function fetchSearchResults(url, successCallback, errorCallback)
    {
        var temp_current_state_url = url.split('?');
        if(temp_current_state_url.length > 1 && temp_current_state_url[1] != ''){
            var parameters = temp_current_state_url[1].split('&');
            var dropdown_elements = [];

            for(var i=0; i<parameters.length; i++){

                var param_array = parameters[i].split('=');
                if($.inArray(param_array[0],dropdown_elements) > -1){
                    $('select[name="'+param_array[0]+'"]').val(param_array[1]);
                }else if($.inArray(param_array[0],['seafarer_name','job_id','email','mobile']) > -1){
                    $('input[name="'+param_array[0]+'"]').val(param_array[1]);
                }
            }
        } else {
            $(':input','.filter-search-form')
                .not(':button, :submit, :reset, :hidden')
                .val('')
                .removeAttr('checked')
                .removeAttr('selected');
             }

        if(searchAjaxCall != "undefined")
        {
            searchAjaxCall.abort();
            searchAjaxCall = "undefined";
        }

        var institute_name = [];
        $(".institute_list").empty();
        $(".institute_list").addClass('loader');

        institute_name = $('.inst_chkbox:checked').map(function(){
            return $(this).val();
        }).toArray();

        searchAjaxCall = $.ajax({
            url: url,
            cache: false,
            dataType: 'json',
            statusCode: {
                200: function(response) {
                    if (typeof response.template != 'undefined') {
                        $(".institute_list").append(response.template);
                        $(".institute_list").removeClass('loader');
                    }
                }
            },
            error: function(error, response) {

                if(typeof errorCallback != "undefined")
                {
                    errorCallback();
                }
            }
        });
    }

    $(document).on('click','.book_institute_batch', function(e) {
        var batch_id = $(this).data('batch');
       
        // $(".location-radio").html('');
        // $("#location-selector").modal('show');
        var temp_url = $("#api-site-institute-batch-details-by-id").val();

        var url_arr = temp_url.split("%");
        var url = url_arr[0] + batch_id;
        var data = [];
        $.ajax({
            type: "GET",
            url: url,
            statusCode: {
                200:function (response) {
                    var location = response.batch_details.batch_location[0].location_id;
                    var batch = response.batch_details.id;

                    var temp_url = $("#api-site-institute-batch-book-by-location").val();
                    var url_arr = temp_url.split("%");
                    var url = url_arr[0] + batch +'/'+ location;
                    
                    window.location.href = url;

                    //removed location popup
                    // $.each(response.batch_details.batch_location, function(key,input){
                    //     var checked = '';
                    //     if(key == '0'){
                    //         checked = 'checked';
                    //     }

                    //     $(".selected_batch_id").val(input.batch_id);

                    //     data = data+'<div class="row">'+'<div class="col-sm-12">'+
                    //         '<label class="radio-inline">'+
                    //             '<input type="radio" name="location" value="'+input.location_id+'" '+ checked +'>'+input.location.city.name+'</input>'+
                    //         '</label>'+
                    //     '</div></div>';
                    // });
                    // $(".location-radio").append(data);
                },
                400:function (response) {
                    
                }
            }
        });
    });

    $(document).on('click','.book-institute-by-location', function(e) {
        var location = $("input[name='location']:checked").val();
        var batch = $(".selected_batch_id").val();

        var temp_url = $("#api-site-institute-batch-book-by-location").val();
        var url_arr = temp_url.split("%");
        var url = url_arr[0] + batch +'/'+ location;
        
        window.location.href = url;
    });

});
//# sourceMappingURL=institutes_search.js.map
