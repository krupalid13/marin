$.ajaxSetup({
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  }
});
$('form.FromSubmit').submit(function (event) {

    var formId = $(this).attr('id');
    // return false;
    if ($('#'+formId).valid()) {

        var formAction = $(this).attr('action');
        var buttonText = $('#'+formId+' button[type="submit"]').text();
        var $btn = $('#'+formId+' button[type="submit"]').attr('disabled','disabled').html("Loading...");
        var redirectURL = $(this).data("redirect_url");
        $.ajax({
            type: "POST",
            url: formAction,
            data: new FormData(this),
            contentType: false,
            processData: false,
            enctype: 'multipart/form-data',
            success: function (response) {
                if (response.status == 1 && response.msg !="") {
                    $('#'+formId+' button[type="submit"]').text(buttonText);
                    $('#'+formId+' button[type="submit"]').removeAttr('disabled','disabled');
                    window.location=redirectURL;
                }else if (response.status == 2 && response.msg !="") {
                      showErrorMessages(formId, response.msg);
                }
            },
            error: function (jqXhr) {

                var errors = $.parseJSON(jqXhr.responseText);
                showErrorMessages(formId, errors);
                $('#'+formId+' button[type="submit"]').text(buttonText);
                $('#'+formId+' button[type="submit"]').removeAttr('disabled','disabled');
            }
        });

        return false; 

    }else{

        return false;
    }
    
});
function showErrorMessages(formId, errorResponse) {
    var msgs = "";
    console.log(errorResponse);
    $.each(errorResponse, function(key, value) {
        
        msgs += value + " <br>";
    });
    flashMessage('danger', msgs,formId);
}
function flashMessage($type,messages,formId) {
    
    $.bootstrapGrowl(messages, {
        ele:'body',
        type: $type,
        delay: 5000,
        position: {
            from: "top",
            align: "right"
        },
        

    });
    
    // $.growl.error({ message: messages });
}

function statusAll(formName,url){
        var id = [];
        var checked = $("#" + formName + " input:checked").length;

        var jobId = $("#courseJobIdHidden").val();

        if (jobId =="" || jobId ==null) {

            swal("Job!", "Select a job.", "warning");

        }else if (checked > 0){

          $("#" + formName + " input:checked").each(function(){
              if($(this).val()!=1){
                  id.push($(this).val());
              }
          });
          swal({
                title: "Are you sure want to send job?",
                icon: "info",
                buttons: true,
                dangerMode: true,
                })
                .then((willDelete) => {
                  if (willDelete) {
                    var formAction = url;
                    $.ajax({
                        type: "POST",
                        url: formAction,
                        data:{checkbox:id,course_job_id:jobId},
                        success: function (response) {
                            if(response.success == 1){

                                location.reload();
                                
                            }else{

                                flashMessage('danger', response.msg);
                            }
                        },
                        error: function (jqXhr) {
                      }
                    });
                }
          });
        }
        else
        {
            swal("User!", "Select at list one user.", "warning");
        }
}

$(document).on("click","#select_all",function(){
    check_uncheck_data();
});
function check_uncheck_data(){
  if($("#select_all").prop("checked")){
    $(".select_checkbox_value").prop("checked",true);
  }else{
    $(".select_checkbox_value").prop("checked",false);
  }
}
$(document).ready(function () {
 $(".allow_numeric_with_decimal").on("input", function(evt) {
       var self = $(this);
       self.val(self.val().replace(/[^0-9\.]/g, ''));
       if ((evt.which != 46 || self.val().indexOf('.') != -1) && (evt.which < 48 || evt.which > 57)) 
       {
         evt.preventDefault();
       }
    });

    $(".allow_numeric_only").on("input", function(evt) {
       var self = $(this);
       self.val(self.val().replace(/[^0-9\.]/g, ''));
       if ((evt.which != 46 || self.val().indexOf('.') != -1) && (evt.which < 48 || evt.which > 57)) 
       {
         evt.preventDefault();
       }
    });
    });