var stoploader = false;
var loaderelement = '';
$("#jobResetButton").on('click', function(){
    $(':input','#candidate-filter-form')
        .not(':button, :submit, :reset, :hidden')
        .val('')
        .removeAttr('checked')
        .removeAttr('selected');

   window.location = window.location.href.split("?")[0];  
});

$(".candidate-filter-search-button").on('click', function(e){
    e.preventDefault();
    var l = Ladda.create(this);
    
    stoploader = true;
    loaderelement = l;

    var url = $("#candidate-filter-form").attr('action')+'?'+$("#candidate-filter-form").serialize();

    // fetchCandidateResult(url, l);
    History.pushState(null,null,url);
    
});

function changeView(displayType) {
    $('#display_type').val(displayType);
    var url = $("#candidate-filter-form").attr('action')+'?'+$("#candidate-filter-form").serialize();
    History.pushState(null,null,url);
    location.reload();
}

$(document).on('click','ul.pagination li',function(e){
    e.preventDefault();
    var url = $(this).find('a').attr('href')+'&'+$("#candidate-filter-form").serialize();
    // fetchCandidateResult(url);
    History.pushState(null,null,url);
});

function fetchCandidateResult(url){
    console.log(url);
    if(stoploader == true){
        loaderelement.start();
    }

    $.ajax({
        url: url,
        type: "GET",
        statusCode: {
            200:function (response) {
                $("#filter-results").empty();
                $("#filter-results").append(response.template);
                if(response.template == '')
                    $("#filter-results").append('<div class="row no-results-found">No results found. Try again with different search criteria.</div>');

                if(stoploader == true){
                    loaderelement.stop();
                }
                
                //$(".batches").slick('destroy');
                if($(".batches").length > 0){
                    $(".batches").removeClass("dont-break");

                    $('.batches').slick({
                        arrows: true,
                        autoplay: false,
                        centerMode: false,
                        slidesToShow: 9,
                        slidesToScroll: 1,
                        dots: false,
                        infinite: false,
                        lazyLoad: 'ondemand',
                        prevArrow: '<span class="navArrow_prev navArrow" style="left: 0;position: absolute;top: 50%;transform: translateY(-6px);"><i class="fa fa-chevron-left"></i></span>',
                        nextArrow: '<span class="navArrow_next navArrow" style="right: 0;position: absolute;top: 50%;transform: translateY(-6px);"><i class="fa fa-chevron-right"></i></span>',
                        responsive: [
                            {
                                breakpoint: 1024,
                                settings: {
                                    slidesToShow: 2,
                                    slidesToScroll: 1,
                                    dots: false
                                }
                            },
                            {
                                breakpoint: 600,
                                settings: {
                                    initialSlide: 1,
                                    slidesToShow: 1,
                                    slidesToScroll: 1,
                                    centerMode: true
                                }
                            }
                        ]
                    });
                }

            },
            400:function (response) {
                if(stoploader == true){
                    loaderelement.stop();
                }
            }
        }
    });
}

// if($('.max-limit-download-resume').val()){
//     $("#resume-message").text($("#max-limit-download-resume").val());
//     $("#max-limit-resume-download-reached").modal('show');
// }

$(document).on('ready', function(){
    $(document).on('click','.download-cv',function (e) {
        e.preventDefault();
        var index = $(this).attr('seafarer-index');
        var url = $("#download-cv").val();

        $.ajax({
            url: url,
            type: "post",
            data: {
                index : index
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            },
            statusCode: {
                200:function (response) {
                    
                },
                400:function (response) {
                   

                }
            }
        });
    });


});








//# sourceMappingURL=dashboard.js.map
