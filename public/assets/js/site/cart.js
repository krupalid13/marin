
$(".remove_subscription_button").on('click', function(e){

    e.preventDefault();
    var id= this.id;
    var cart_id = id.substr(id.indexOf("_") + 1);
    $("#cart_"+cart_id).remove();
    var temp_url = $("#api-remove-subscription-route").val();
    var temp_arr = temp_url.split('%');
    var url = temp_arr[0]+cart_id;
   $.ajax({
        type: "POST",
        url: url,
        data: {
            '_token' : $("input[name='_token']").val(),
        },
        statusCode: {
            200:function (response) {
                $(".cart_total b").text(response.cart_total);
                toastr.success(response.message, 'success');
            },
            400:function (response) {
                toastr.error(response.message, 'Error');
            }
        }
    });
});
//# sourceMappingURL=cart.js.map
