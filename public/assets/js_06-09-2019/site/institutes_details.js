$(document).ready(function() {

	if ($(".course_blocking").is(":visible")) {
		
		var date = $('.selected_date').val();

	    $('.select_date').datetimepicker({
	        format: 'DD-MM-YYYY',
	        minView: 2,
	        maxView: 4,
	        startDate: date,
	        endDate: date,
	        defaultDate: date,
	        inline: true,
	    });

	    $('.select_date').find(".day").removeClass("active");
		$('.datetimepicker').find(".day").not('.disabled').addClass("active");
		$('.datetimepicker').find(".datetimepicker-days .switch").removeClass("switch");
	}

    mydate = moment().format('DD-MM-YYYY');
    
    $("#preffered-datepicker").datepicker({
        format: "dd-mm-yyyy",
        startDate: mydate,
        altField: '#dbdatepicker',
        startView: 0
        }).on('changeDate', function(e) {
        var d = new Date(Date.parse(e.date)).toLocaleString('sq-AL');
        $('#dbdatepicker').val(d);
    });

    $('input:radio[name=week_change]').change(function () {
        var selected_week = $(this).data('week');
        $('#dbdatepicker').val('');
        
        var split_date = date.split('-');

        var year = split_date[0];
        var month = split_date[1];
        var curr_day = mydate.split('-')[0];
        let weeks = [];

        firstDate = new Date(year, month, 1);
        lastDate = new Date(year, month + 1, 0);
        numDays = lastDate.getDate();

        let start = 1;
        let end = endFirstWeek(firstDate, 3);
        
        while (start <= numDays) {
            weeks.push({start: start, end: end});
            start = end + 1;
            end = end + 7;
            end = start === 1 && end === 8 ? 1 : end;
            if (end > numDays) {
                end = numDays;
            }
        }

        var set_start_date = week_start+"-"+month+"-"+year;
        var set_end_date = week_end+"-"+month+"-"+year;


        if(selected_week == 'monthly'){
            $("#preffered-datepicker").datepicker("destroy");
            var week_start = weeks[0]['start'];
            var week_end = '31';
            var set_end_date = week_end+"-"+month+"-"+year;
            mydate = moment().format('YYYY-MM-DD');

            if(new Date(year+"-"+month+"-"+week_start) >= new Date(mydate)){
                set_start_date = week_start+"-"+month+"-"+year;
            }else{
                set_start_date = mydate;
            }
            
            $("#preffered-datepicker").datepicker({
                format: "dd-mm-yyyy",
                startDate: set_start_date,
                altField: '#dbdatepicker',
                endDate: set_end_date,
                startView: 0
            }).on('changeDate', function(e) {
                var d = new Date(Date.parse(e.date)).toLocaleString('sq-AL');
                $('#dbdatepicker').val(d);
            });
            return false;
        }

        if(selected_week == '1-2' || selected_week == '3-4'){
            selected_split_week = selected_week.split('-');
            if(selected_week == '1-2'){
                var week_start = 1;
                var week_end = 15;
            }else{
                 var week_start = 16;
                var week_end = 31;
            }
        }else{
            var week_start = weeks[selected_week-1]['start'];
            var week_end = weeks[selected_week-1]['end'];
        }

        var set_start_date = week_start+"-"+month+"-"+year;
        var set_end_date = week_end+"-"+month+"-"+year;

        mydate1 = moment().format('YYYY-MM-DD');

        if((new Date(year+"-"+month+"-"+week_start) >= new Date(mydate1)) || (new Date(year+"-"+month+"-"+week_end) >= new Date(mydate1))){
            $("#preffered-datepicker").datepicker("destroy");

            if(new Date(year+"-"+month+"-"+week_start) >= new Date(mydate1)){
                set_start_date = week_start+"-"+month+"-"+year;
            }else{
                set_start_date = moment().format('DD-MM-YYYY');
            }

            $("#preffered-datepicker").datepicker({
                format: "dd-mm-yyyy",
                startDate: set_start_date,
                altField: '#dbdatepicker',
                endDate: set_end_date,
                startView: 0
            }).on('changeDate', function(e) {
                var d = new Date(Date.parse(e.date)).toLocaleString('sq-AL');
                $('#dbdatepicker').val(d);
            });
            
        }else{
            toastr.error("Not Allowed", 'Error');

            $('input:radio[name=week_change]').prop("checked", false);
        }
    });
    
    function endFirstWeek(firstDate, firstDay) {
        if (! firstDay) {
            return 7 - firstDate.getDay();
        }
        if (firstDate.getDay() < firstDay) {
            return firstDay - firstDate.getDay();
        } else {
            return 7 - firstDate.getDay() + firstDay;
        }
    }

	$(".signin-book").on('click', function(e){
        if($(this).hasClass('new-sigin-link')){
           $("#login-popup").removeClass('hide');
           $("#signup-popup").addClass('hide');
           $(".new-sigin-link").addClass('active');
           $(".pre-register-link").removeClass('active');
           $("#loginFormModal-preRegister").removeClass('hide');
            $("#preRegistrationFormModal").addClass('hide');
        }
        else{
            $(".pre-register-link").addClass('active');
            $(".new-sigin-link").removeClass('active');
            $("#loginFormModal-preRegister").addClass('hide');
            $("#preRegistrationFormModal").removeClass('hide');
        }
    });

    $(document).on('click','.search-reset-btn' , function(e){
        $(':input', $(this).closest("form[id]"))
            .not(':button, :submit, :reset, :hidden')
            .val('')
            .removeAttr('checked')
            .removeAttr('selected');
    });

    
   $('#block-form').validate({
        ignore: []
    });

    $.validator.addMethod("dbdatepickervalue", function(value, element) {

        if($('.dbdatepicker').val() != ''){
            return true;
        }else{
            return false;
        }

    }, "Please select date.");

    function showAdvertiseInModal(batch_id){
        $.ajax({
            type: "GET",
            url: $("#api-get-institute-advertise-by-batch-id").val(),
            data:{
                batch_id : batch_id,
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content'),
            },
            statusCode: {
                200:function (response) {
                    $('.display-advertise').attr('src',response.image_path);
                    $('#display-advertise-modal').modal('show');
                },
                400:function (response) {

                }
            }
        });
    }

	$(document).on('click','.block-seat', function(e){
        
        var batch_id = $(this).data('batch');
        var discount = '';
        var reload = false;
        var force_reload = false;

        if(typeof($('.discount').val()) != 'undefined'){
            discount = $('.discount').val();
        }
        
        if($('#block-form').length){
            if($('#block-form').valid()){
                if($('input[name=week_change]:checked').length == 0)
                {
                    $(".week_change_error").removeClass('hide');
                    return false;
                }else{
                    $(".week_change_error").addClass('hide');
                }

        		var l = Ladda.create(this);
        		l.start();

                if($('.display-advertise').attr('src') != ''){
                    $('#display-advertise-modal').modal('show');

                    $('.adv_timer').removeClass('hide');
                    var counter = 11;
                    var interval = setInterval(function() {
                        counter--;
                        // Display 'counter' wherever you want to display it.
                        if (counter == 0) {
                           $('#display-advertise-modal').modal('hide');
                           if(reload){
                                window.location.reload();
                           }

                        }else{
                            $('.ad_timer').html(counter);
                        }

                    }, 1000);
                }else{
                    force_reload = true;
                }
                
                var location_id = $(this).data('location');
                var pref_date = $('.dbdatepicker').val();
                var pref_type = $('input[name="week_change"]:checked').val();

        		$.ajax({
        			type: "POST",
                    url: $("#api-site-institute-block-seat").val(),
                    data:{
                        batch_id : batch_id,
                        location_id : location_id,
                        pref_date : pref_date,
                        pref_type : pref_type,
                        discount : discount,
                    },
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    },
                    statusCode: {
                    	200:function (response) {
                            $('.adv_timer').removeClass('hide');
                            /*var counter = 11;
                            var interval = setInterval(function() {
                                counter--;
                                // Display 'counter' wherever you want to display it.
                                if (counter == 0) {
                                    toastr.success(response.message, 'Success');
                                    reload = true;
                                    if(force_reload){
                                        window.location.reload();
                                   }

                                    l.stop();
                                }else{
                                    $('.ad_timer').html(counter);
                                }

                            }, 1000);*/
                            window.location.reload();

                        },
                        400:function (response) {
                            toastr.error(response.responseJSON.message, 'Error');
                            $('#display-advertise-modal').modal('hide');
                        	if(response.responseJSON.login == '1'){
                                $('#login-popup-reg-modal').modal('show');
                            }
                            if(response.responseJSON.mob_verification_req == '1'){
                                $("#resend_otp_Modal").modal();
                                $.ajax({
                                     type: "POST",
                                     url: $("#api-resend-otp-route").val(),
                                     data: {
                                         '_token' : $("input[name='_token']").val()
                                     },
                                     statusCode: {
                                         200:function (response) {
                                            $('#otp_resend_success').removeClass('hide');
                                            $("#resend-otp-button").addClass('disabled');
                                            $('#invalid_mob_error').addClass('hide');
                                         },
                                         400:function (response) {
                                            $('#mob_error_resend_otp').removeClass('hide');
                                         }
                                     }
                                });
                        	}
                            l.stop();
                        },
                        405:function (response) {
                            l.stop();
                        },
                    }
                });
    		}
        }else{
            var l = Ladda.create(this);
            var reload = false;
            var force_reload = false;
            l.start();

            if($('.display-advertise').attr('src') != ''){
                $('#display-advertise-modal').modal('show');

                $('.adv_timer').removeClass('hide');
                var counter = 11;
                var interval = setInterval(function() {
                    counter--;
                    // Display 'counter' wherever you want to display it.
                    if (counter == 0) {
                       $('#display-advertise-modal').modal('hide');
                       if(reload){
                            window.location.reload();
                       }

                    }else{
                        $('.ad_timer').html(counter);
                    }

                }, 1000);
            }else{
                force_reload = true;
            }

            //showAdvertiseInModal(batch_id);
            var location_id = $(this).data('location');

            var pref_date = $('.dbdatepicker').val();
            var pref_type = $('input[name="week_change"]:checked').val();

            $.ajax({
                type: "POST",
                url: $("#api-site-institute-block-seat").val(),
                data:{
                    batch_id : batch_id,
                    location_id : location_id,
                    pref_date : pref_date,
                    pref_type : pref_type,
                    discount : discount,
                },
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                },
                statusCode: {
                    200:function (response) {
                        toastr.success(response.message, 'Success');
                        force_reload = true;
                        if(force_reload){
                            window.location.reload();
                        }
                        window.location.reload();
                        l.stop();
                    },
                    400:function (response) {
                        toastr.error(response.responseJSON.message, 'Error');
                        $('#display-advertise-modal').modal('hide');
                        if(response.responseJSON.login == '1'){
                            $('#login-popup-reg-modal').modal('show');
                        }
                        if(response.responseJSON.mob_verification_req == '1'){
                            $("#resend_otp_Modal").modal();
                            $.ajax({
                                 type: "POST",
                                 url: $("#api-resend-otp-route").val(),
                                 data: {
                                     '_token' : $("input[name='_token']").val()
                                 },
                                 statusCode: {
                                     200:function (response) {
                                        $('#otp_resend_success').removeClass('hide');
                                        $("#resend-otp-button").addClass('disabled');
                                        $('#invalid_mob_error').addClass('hide');
                                     },
                                     400:function (response) {
                                        $('#mob_error_resend_otp').removeClass('hide');
                                     }
                                 }
                            });
                        }
                        l.stop();
                    },
                    405:function (response) {
                        l.stop();
                    },
                }
            });
        }
	});

	$(document).on('click','#pre-register-user', function(e){
    	if($('#preRegistrationFormModal').valid()){
            var l = Ladda.create(this);
            l.start();
            $.ajax({
                type: "POST",
                url: $("#preRegistrationFormModal").attr('action'),
                data: $("#preRegistrationFormModal").serialize(),
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content'),
                },
                statusCode: {
                    200:function (response) {
                        $('#login-popup-reg-modal').modal('hide');
                        if(response.block_seat == '1'){
                            $(".block-seat").trigger('click');
                        }
                        l.stop();
                    },
                    400:function (response) {
                        l.stop();
                    }
                }
            });
        }
    });

    $.validator.addMethod("alpha", function(value, element) {
        return this.optional(element) || value == value.match(/^[a-zA-Z ]+$/);
    });



    
	$('#preRegistrationFormModal').validate({
	    rules: {
            firstname: {
                alpha: true,
                required: true
            },
            email: {
                required: true,
                email: true,
                remote: {
                    url: $("#api-check-email").val(),
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content'),
                    },
                    type: "post",
                    async: false,
                    data: {
                        id: function(){ return $("input[name='user_id']").val()},
                        email: function(){ return $("#user_email").val(); }
                    }
                }
            },
            password: {
                minlength: 6,
                required: true
            },
            cpassword: {
                required: true,
                minlength: 6,
                equalTo: "#password"
            },
            gender: {
                required: true
            },
            passport: {
                required: true,
            },
            current_rank: {
                required: true,
            },
            indosno: {
                required: true,
            },
            dob: {
                required: true,
            },
            mobile: {
                required: true,
                number: true,
                minlength: 10,
                remote: {
                    url: $("#api-check-mobile").val(),
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content'),
                    },
                    type: "post",
                    async: false,
                    data: {
                    	id: function(){ return $("input[name='user_id']").val()},
                        mobile: function(){ return $("input[name='mobile']").val(); }
                    }
                }
            },
        },
        messages: {
            firstname: {
                alpha: "Please enter only alphabets",
                required: "Please specify your full name"
            },
            email: {
                required: "We need your email address to contact you",
                email: "Your email address must be in the format of name@domain.com",
                remote: "Email already exist"
            },
            cpassword: {
                equalTo: "Confirm password is not matching with password "
            },
            mobile: {
                number: "Please enter valid Mobile number",
                remote: "Mobile already exist"
            },
            gender: "Please check a gender!",
        },
        errorPlacement: function(error, element) {
            if( element.attr('name') == 'gender'){
                error.insertAfter($('.gender-option-container'));
            }else{
                error.insertAfter(element);
            }
        }
    });



















	/* $('#preRegistrationFormModal').validate({
        rules: {
            firstname: {
                alpha: true,
                required: true
            },
            email: {
                required: true,
                email: true,
                remote: {
                    url: $("#api-check-email").val(),
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content'),
                    },
                    type: "post",
                    async: false,
                    data: {
                        id: function(){ return $("input[name='user_id']").val()},
                        email: function(){ return $("#user_email").val(); }
                    }
                }
            },
            password: {
                minlength: 6,
                required: true
            },
            cpassword: {
                required: true,
                minlength: 6,
                equalTo: "#password"
            },
            gender: {
                required: true
            },
            user_passport: {
                required: true,
            },
            current_rank: {
                required: true,
            },
            indosno: {
                required: true,
            },
            dob: {
                required: true,
            },
            mobile: {
                required: true,
                number: true,
                minlength: 10,
                remote: {
                    url: $("#api-check-mobile").val(),
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content'),
                    },
                    type: "post",
                    async: false,
                    data: {
                    	id: function(){ return $("input[name='user_id']").val()},
                        mobile: function(){ return $("input[name='mobile']").val(); }
                    }
                }
            },
        },
        messages: {
            firstname: {
                alpha: "Please enter only alphabets",
                required: "Please specify your full name"
            },
            email: {
                required: "We need your email address to contact you",
                email: "Your email address must be in the format of name@domain.com",
                remote: "Email already exist"
            },
            cpassword: {
                equalTo: "Confirm password is not matching with password "
            },
            mobile: {
                number: "Please enter valid Mobile number",
                remote: "Mobile already exist"
            },
            gender: "Please check a gender!",
        },
        errorPlacement: function(error, element) {
            if( element.attr('name') == 'gender'){
                error.insertAfter($('.gender-option-container'));
            }else{
                error.insertAfter(element);
            }
        }
    }); */






















    $('#loginFormModal-preRegister').validate({
        rules: {
            email: {
                required: true,
                email: true
            },
            password: {
                required: true
            },
        },
        messages: {
            email: {
                required: "Please provide your email address"
            },
        }
    });

    $('#sign-in-btn-pre-registration').on('click', function(e){
        if($('#loginFormModal-preRegister').valid()){
            var l = Ladda.create(this);
            l.start();
            $.ajax({
                type: "POST",
                url: $("#loginFormModal-preRegister").attr('action'),
                data: $("#loginFormModal-preRegister").serialize(),
                headers: {
	                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
	            },
                statusCode: {
                    200:function (response) {
                        //window.location.href = response.redirect_url;
                        $('#login-popup-reg-modal').modal('hide');
                        $(".block-seat").trigger('click');
                        l.stop();
                    },
                    400:function (response) {
                        $("#login_error").text(response.responseJSON.message);
                        $("#login_error").css('display','block');
                        l.stop();
                    }
                }
            });
        }
    });
    
    function batchStatusChange(batch_id,order_id,status,reason,l) {
        $('.loader_overlay').removeClass('hide');
        l.start();
        $.ajax({
            type: "POST",
            url: $("#api-site-institute-change-batch-status").val(),
            data: {
                order_id : order_id,
                batch_id : batch_id,
                status : status,
                reason : reason,
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            },
            statusCode: {
                200:function (response) {
                    $('.setting-'+order_id).remove();
                    $('.status-'+order_id).text(response.status);
                    toastr.success(response.message,'Success');
                    $('.loader_overlay').addClass('hide');
                    $("#batch-denied-modal").modal('hide');
                    l.stop();
                },
                400:function (response) {
                    toastr.error(response.responseJSON.message,'Error');
                    $('.loader_overlay').addClass('hide');
                    $("#batch-denied-modal").modal('hide');
                    l.stop();
                }
            }
        });
    }

    $(document).on('click','.batchStatusChangeButton' , function(e){
       
        var batch_id = $(this).data('batch-id');
        var order_id = $(this).data('order-id');
        var status = $(this).data('status');
        var l = Ladda.create(this);

        if(status == '5'){
            $("#batch-denied-modal").modal('show');

            $(document).on('click','#batch-denied-ok' , function(e){
                valid = true;   
                if (valid && $('.reason').val() == '') {
                    alert ("please enter reason for batch deniel");
                    valid = false;
                }
                if(valid){
                    var l = Ladda.create(this);

                    var reason = $('.reason').val();
                    batchStatusChange(batch_id,order_id,status,reason,l);
                }
            });
        }else{
            batchStatusChange(batch_id,order_id,status,'',l);
        }
        
    });

    $(document).on('click','.payment_by',function (e) {
        if($(this).val() == 'split'){
            $('.split_payment_details').removeClass('hide');
            $('.single_payment_details').addClass('hide');
        }else{
            $('.split_payment_details').addClass('hide');
            $('.single_payment_details').removeClass('hide');
        }
    });

    $(".make-payment").on('click', function(e){
        e.preventDefault();
        var payment_by = $('input[type="radio"][class="payment_by"]:checked').val();
        var batch_id = $(this).data('batch');
        var order_id = $(this).data('order-id');
        var pay = $(this).data('pay');

        if(pay == 'split'){
            payment_by = 'split';
        }
        
        var url = $("#api-institute-batch-proceed-to-payment-route").val();
        var loader = Ladda.create(this);
        loader.start();
        
        if($('.display-advertise').attr('src') != ''){
            $('#display-advertise-modal').modal('show');
        }

        $.ajax({
            url : url,
            method : "POST",
            data : {
                payment_by : payment_by,
                order_id : order_id,
                batch_id : batch_id,
                pay : pay,
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            },
            statusCode: {
                200 : function(response) {
                    var counter = 11;
                    var interval = setInterval(function() {
                        counter--;
                        // Display 'counter' wherever you want to display it.
                        if (counter == 0) {
                            loader.stop();
                            $('#display-advertise-modal').modal('hide');
                            $("#payumoney-form").empty();
                            $("#payumoney-form").append(response.payumoney_form);
                            $("#payumoney_form").submit();
                        }else{
                            $('.ad_timer').html(counter);
                        }

                    }, 1000);
                    
                },
                400 : function(response) {
                    loader.stop();
                    $('#display-advertise-modal').modal('hide');
                    toastr.error(response.responseJSON.message, 'Error');
                }
            }
        });
    });

    $(document).on('click','.batchConfirmButton' , function(e){
        var batch_id = $(this).data('batch');
        $('.batch_id').val(batch_id);

        var batch_start_date = $(this).data('batch-start-date');
        var selected_week = $(this).data('week');
        $('.selected_week').val(selected_week);
        $('#batch-confirm-modal').modal('show');
        
        var split_date = batch_start_date.split('-');

        var year = split_date[0];
        var month = split_date[1];
        var curr_day = mydate.split('-')[0];
        let weeks = [];

        firstDate = new Date(year, month, 1);
        lastDate = new Date(year, month + 1, 0);
        numDays = lastDate.getDate();

        let start = 1;
        let end = endFirstWeek(firstDate, 3);
        
        while (start <= numDays) {
            weeks.push({start: start, end: end});
            start = end + 1;
            end = end + 7;
            end = start === 1 && end === 8 ? 1 : end;
            if (end > numDays) {
                end = numDays;
            }
        }

        var set_start_date = week_start+"-"+month+"-"+year;
        var set_end_date = week_end+"-"+month+"-"+year;

        var week_start = weeks[0]['start'];
        var week_end = '31';
        var set_end_date = week_end+"-"+month+"-"+year;
        mydate = moment().format('YYYY-MM-DD');

        if(selected_week == 'Monthly'){
            $("#preffered-datepicker").datepicker("destroy");
            var week_start = weeks[0]['start'];
            var week_end = '31';
            var set_end_date = week_end+"-"+month+"-"+year;
            mydate = moment().format('YYYY-MM-DD');

            if(new Date(year+"-"+month+"-"+week_start) <= new Date(mydate)){
                set_start_date = mydate;
            }else{
                set_start_date = week_start+"-"+month+"-"+year;
            }

            $("#preffered-datepicker").datepicker({
                format: "dd-mm-yyyy",
                startDate: set_start_date,
                 altField: '#dbdatepicker',
                endDate: set_end_date,
                startView: 0
            }).on('changeDate', function(e) {
                var d = new Date(Date.parse(e.date)).toLocaleString('sq-AL');
                $('#dbdatepicker').val(d);
            });

            return false;
        }
           
        if(selected_week == '1-2' || selected_week == '3-4'){
            selected_split_week = selected_week.split('-');
            
            if(selected_week == '1-2'){
                var week_start = 1;
                var week_end = 15;
            }else{
                var week_start = 16;
                var week_end = 31;
            }
            set_end_date = week_end+"-"+month+"-"+year;
        }else{
            var week_start = weeks[selected_week-1]['start'];
            var week_end = weeks[selected_week-1]['end'];
        }
        
        if(new Date(year+"-"+month+"-"+week_start) <= new Date(mydate)){
            set_start_date = moment().format('DD-MM-YYYY');
        }else{
            set_start_date = week_start+"-"+month+"-"+year;
            set_end_date = week_end+"-"+month+"-"+year;
        }

        $("#preffered-datepicker").datepicker("destroy");
        
        $("#preffered-datepicker").datepicker({
            format: "dd-mm-yyyy",
            startDate: set_start_date,
            altField: '#dbdatepicker',
            endDate: set_end_date,
            startView: 0
        }).on('changeDate', function(e) {
            var d = new Date(Date.parse(e.date)).toLocaleString('sq-AL');
            $('#dbdatepicker').val(d);
        });

        $("#preffered-datepicker").datepicker("refresh");
        return false;

    });

    $('#batch-confirm-modal-form').validate({
        ignore: []
    });

    $(document).on('click','#batch-confirm-modal-btn', function(e){
        if($('#batch-confirm-modal-form').valid()){
            var l = Ladda.create(this);
            l.start();
            $.ajax({
                type: "POST",
                url: $("#batch-confirm-modal-form").attr('action'),
                data: $("#batch-confirm-modal-form").serialize(),
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content'),
                },
                statusCode: {
                    200:function (response) {
                        $('#batch-confirm-modal').modal('hide');
                        toastr.success(response.message,'Success');
                        window.location.reload();
                        l.stop();
                    },
                    400:function (response) {
                        l.stop();
                    }
                }
            });
        }
    });

    $(".cancel-payment").on('click', function(e){
        e.preventDefault();
        var batch_id = $(this).data('batch');
        var order_id = $(this).data('order-id');
        
        var url = $("#api-institute-batch-cancel-payment-route").val();
        var loader = Ladda.create(this);
        loader.start();
        
        $.ajax({
            url : url,
            method : "POST",
            data : {
                order_id : order_id,
                batch_id : batch_id,
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            },
            statusCode: {
                200 : function(response) {
                    loader.stop();
                    toastr.success(response.message,'Success');
                    window.location.reload();
                },
                400 : function(response) {
                    loader.stop();
                    toastr.error(response.responseJSON.message, 'Error');
                }
            }
        });
    });
    
    $( document ).ready(function() {
        $('[data-toggle="tooltip"]').tooltip();

        $('#dbdatepicker').val('');
            
        var on_demand_batch_type = $('.on_demand_batch_type').val();
        if(typeof(date) !=  'undefined'){
            var split_date = date.split('-');

            var year = split_date[0];
            var month = split_date[1];

            var curr_day = mydate.split('-')[0];
            let weeks = [];

            firstDate = new Date(year, month, 1);
            lastDate = new Date(year, month + 1, 0);
            numDays = lastDate.getDate();

            let start = 1;
            let end = endFirstWeek(firstDate, 3);
            
            while (start <= numDays) {
                weeks.push({start: start, end: end});
                start = end + 1;
                end = end + 7;
                end = start === 1 && end === 8 ? 1 : end;
                if (end > numDays) {
                    end = numDays;
                }
            }
            

            if(on_demand_batch_type == '1'){//weekly
                for (var i = 0; i <= 3; i++) {
                    var week_start = weeks[i]['start'];
                    var week_end = weeks[i]['end'];

                    mydate1 = moment().format('YYYY-MM-DD');

                    if(new Date(year+"-"+month+"-"+week_start) <= new Date(mydate1) && new Date(year+"-"+month+"-"+week_end) <= new Date(mydate1)){
                        $(".week-"+(i+1)).remove();
                    }
                }

            }
            else if(on_demand_batch_type == '2'){

                mydate1 = moment().format('YYYY-MM-DD');

                var week_start = 1;
                var week_end = 15;
                
                set_end_date = week_end+"-"+month+"-"+year;

                if(new Date(year+"-"+month+"-"+week_start) <= new Date(mydate1) && new Date(year+"-"+month+"-"+week_end) <= new Date(mydate1)){
                    $(".week-"+(1)+'-'+(2)).remove();
                }

                var week_start = 16;
                var week_end = 31;

                if(new Date(year+"-"+month+"-"+week_start) <= new Date(mydate1) && new Date(year+"-"+month+"-"+week_end) <= new Date(mydate1)){
                    $(".week-"+(3)+'-'+(4)).remove();
                }

            }

            setTimeout(function () {
                $('input:radio[name=week_change]:first').trigger('click');
            }, 200);
        }

        return true;

    });

});

$(".edit_link").on('click',function(e){
    var l = Ladda.create(this);
    l.start();
    window.location.href = $(this).attr('href')+'#'+$(this).attr('data-form');
});
//# sourceMappingURL=institutes_details.js.map
