$(document).ready(function() {

    if ($(".institutesSearchResult").is(":visible")) {
        var date = moment().format('DD-MM-YYYY');

        $('#datepicker_coss').datepicker({
            format: 'dd-mm-yyyy',
            endDate: date,
            startView: 0
        }).on('changeDate', function(ev) {
            $('#my_hidden_input').val(
                $('#datepicker_coss').datepicker('getFormattedDate')
            );
        });

        $('#datetimepicker12').datetimepicker({
            inline: true,
            sideBySide: true,
            dateFormat: 'dd-mm-yyyy',
            altFieldTimeOnly: false,
            altFormat: 'dd-mm-yy',
            altField: "#my_hidden_input",
            autoClose: true,
            Default: 'MMMM YYYY',
            minView: 2
        });
    }

    $('.course_type_filter').on( 'change', function(){ getInstituteListByFilters(); } );
    $('.institute_course_name').on( 'change', function(){ getInstituteListByFilters(); } );
    $('.inst_chkbox').on( 'change', function(){ 
        console.log($(this).data('name'),123);
        if(this.checked) {
            $(".searchResult_tags").append('<span class="coss_tag" data-class='+$(this).data('id')+'>'+$(this).data('name')+'<span class="close_tag">+</span></span>');
        }else{
            $("."+$(this).data('id')).remove();
        }
        getInstituteListByFilters(); 
    });

    $(document).on('click','.coss_tag',function (e) {
        $(this).remove();
        $('#'+$(this).data('class')).attr('checked', false);
        getInstituteListByFilters();
    });

    function getInstituteListByFilters(){

        var institute_name = [];
        $(".institute_list").empty();
        $(".institute_list").addClass('loader');

        institute_name = $('.inst_chkbox:checked').map(function(){
            return $(this).val();
        }).toArray();
       
        var url = $('#api-site-list-course-search').val();
        $.ajax({
            type: "GET",
            url: url,
            data: {
                'course_type' : $(".course_type_filter").val(),
                'course_name' : $(".institute_course_name").val(),
                'institute_name' : institute_name,
                '_token' : $("input[name='_token']").val()
            },
            statusCode: {
                200:function (response) {
                    $(".institute_list").append(response.template);
                    $(".institute_list").removeClass('loader');
                },
                400:function (response) {
                    
                }
            }
        });
    }
});
//# sourceMappingURL=institutes_search.js.map
