   
$(document).ready(function() {
	
    /*$(".datepicker").on('click',function(){
        var dateAr = $(this).val().split('-');
        var newDate = dateAr[1] + '/' + dateAr[2] + '/' + dateAr[0];
        $(this).datepicker( "setDate", $(this).val() );
    });*/

    var sea_service_block_index = 1;
    var certificate_block_index = 1;
    var cdc_block_index = 1;
    var coc_block_index = 1;
    var cop_block_index = 1;
    var coe_block_index = 1;
    var value_added_certificate_block_index  = 1;
    var service_check = false;
    var course_check = false;
    var cdc_check = false;
    var coe_check = false;
    var value_added_course_check = false;

    if(typeof $(".location-container") != 'undefined'){
        if($('.location-container').height() > 170 && ($(".user-imp-details").height() > 80 && $(".user-imp-details").height() < 100)){
            $('.photo-card').css('height','390'); 
        }
    }

    $("input[name='us']").on('change', function(e){
        var us_check = $(this).val();
        if(us_check == '1'){
            $('.us-block').removeClass('hide');
        }else{
            $('.us-block').addClass('hide');
        }
    });

    $("input[name='pcc']").on('change', function(e){
        var pcc_check = $(this).val();
        if(pcc_check == '1'){
            $('.pcc-block').removeClass('hide');
        }else{
            $('.pcc-block').addClass('hide');
        }
    });

	$("input[name='cc']").on('change', function(e){
		
        var cc_check = $(this).val();
        if(cc_check == '1'){
            $('.cc-block').removeClass('hide');
        }else{
            $('.cc-block').addClass('hide');
        }
    });
	
    $("input[name='yellowfever']").on('change', function(e){
        var yf_check = $(this).val();
        if(yf_check == '1'){
            $('.yf-block').removeClass('hide');
        }else{
            $('.yf-block').addClass('hide');
        }
    });
	
	$("input[name='ilo_medical']").on('change', function(e){
        var ilo_check = $(this).val();
        if(ilo_check == '1'){
            $('.ilo-block').removeClass('hide');
        }else{
            $('.ilo-block').addClass('hide');
        }
    });
	
	
	

    $(document).on('change','.check_engine_type',function (e) {
        var index = $(this).attr('block-index');
        if($(this).val() == '11'){
            $(".other_engine_type-"+index).removeClass('hide');
        }else{
            $(".other_engine_type-"+index).addClass('hide');
        }
    });

    $("#current_rank").on('change', function(e){
        
        $("#current_rank_id").val($(this).val());
        var fields = required_fields[$(this).val()];
     
        $(".required_fields").addClass('hide');
        //$(".required_fields input").val('');

        for(var i=0; i<fields.length; i++) {
            var field_arr = fields[i].split('-');
            
            $("input[name='fromo'][value=0]").prop("checked", true);
            $("#FROMO-details").addClass('hide');

            if(field_arr.length > 1) {
                $("#"+field_arr[0]+"-details").removeClass('hide');
                $("#"+field_arr[0]+"-details span").addClass('hide');
            } else{
                $("#"+fields[i]+"-details").removeClass('hide');
                $("#"+field_arr[0]+"-details span").removeClass('hide');
            }
        }
    });

    $(".edit-details-tab").on('click',function(e){
        $(".edit-details-tab").addClass('page-done page-active');
        $("form").addClass('hide');
        var form_id=$(this).attr('data-form-id');

        $('#'+form_id).removeClass('hide');
        $('#'+form_id).show();
        $(this).removeClass('page-done page-active');
        window.scrollTo(0,0);

        if(form_id === 'upload_documents'){
            getUserDocuments();
        }
    });

    $(".details-tab").on('click',function(e){

        if(!$(this).hasClass('disabled')){
            $("form").addClass('hide');
            var form_id=$(this).attr('data-form-id');
            $('#'+form_id).removeClass('hide');
            $('#'+form_id).show();

            // $(".details-tab").removeClass('page-done page-active');
            $(this).addClass('page-done page-active');
            window.scrollTo(0,0);
        }
    });

    $('#Seafarer-profile-registration-step1').validate({
        focusInvalid: false,
        invalidHandler: function(form, validator) {

            if (!validator.numberOfInvalids())
                return;

            $('html, body').animate({
                scrollTop: $(validator.errorList[0].element).offset().top + (-200)
            }, 200);

        },
        rules: {
            firstname: {
                number: false,
                required: true
            },
            /*lastname: {
                lettersonly: true,
                minlength: 2,
                required: true
            },*/
            email: {
                required: true,
                email: true
            },
            gender: {
                required: true
            },
            pincode: {
                required: true,
                number: true,
                minlength: 5
            },
            mobile: {
                required: true,
                number: true,
                minlength: 10
            },
            country: {
                required: true
            },
            state: {
                required: true
            },
            city: {
                required: true
            },
            current_rank: {
                required: true
            },
            years: {
                required: true
            },
            months: {
                required: true
            },
            landline: {
                number: true
            },
            landline_code: {
                number: true
            },
            applied_rank: {
                required: true
            },
            date_avaibility: {
                required: true
            },
            permananent_address: {
                required: true
            },
            last_wages: {
                required: true,
                number: true
            },
        },
        messages: {
            firstname: {
                required: "Please specify your full name"
            },
            lastname: {
                required: "Please specify your last name",
                lettersonly: "Please enter characters only"
            },
            email: {
                required: "We need your email address to contact you",
                email: "Your email address must be in the format of name@domain.com"
            },
            gender: "Please check a gender!",
            rank_exp: {
                number: "Please enter experience in numbers"
            },
            mobile: {
                number: "Please enter valid Mobile number"
            }
        },
        errorPlacement: function(error, element) {
            if( element.attr('name') == 'gender'){
                error.insertAfter($('.gender-option-container'));
            }else{
                error.insertAfter(element);
            }
        }
    });

    $("#submitProfileBasicDetailButton").on('click',function(e){
        e.preventDefault();
        var l = Ladda.create(this);
        if($('#Seafarer-profile-registration-step1').valid()){
            l.start();
            $.ajax({
                type: "POST",
                url: $("#Seafarer-profile-registration-step1").attr('action'),
                data: $("#Seafarer-profile-registration-step1").serialize(),
                statusCode: {
                    200:function (response) {
                        if(response.update_mobile == 1){
                            $('#myModal').modal('show');
                        }
                        /*$("#step2").attr('data-toggle','tab');
                        $("#step2").removeClass('cursor-not-allow');
                        $("#step2").trigger("click");*/
                        l.stop();
                    },
                    400:function (response) {
                        l.stop();

                    },
                    422:function (response) {
                        for (var prop in response.responseJSON){
                            if(prop == "email"){
                                $("#email-box").addClass('has-error');
                                $("#email-error").removeClass('hide');
                                $("#email-error").text(response.responseJSON['email'][0]);
                            }
                            if(prop == "mobile"){
                                $("#mobile-box").addClass('has-error');
                                $("#mobile-error").removeClass('hide');
                                $("#mobile-error").text(response.responseJSON['mobile'][0]);
                            }
                        }
                        l.stop();

                    }
                }
            });
        }
    });

    $.validator.addMethod("alpha", function(value, element) {
        return this.optional(element) || value == value.match(/^[a-zA-Z ]+$/);
     });

    $.validator.addMethod("notEqualTo", function(value, element, param) {
       return this.optional(element) || value != $(param).val();
    }, "Mobile and kin number can't be same.Please change it.");

    $('#basic-details-form').validate({
        focusInvalid: false,
        invalidHandler: function(form, validator) {

            if (!validator.numberOfInvalids())
                return;

            $('html, body').animate({
                scrollTop: $(validator.errorList[0].element).offset().top + (-200)
            }, 200);

        },
        rules: {
            firstname: {
                alpha: true,
                required: true
            },
            email: {
                required: true,
                email: true,
                remote: {
                    url: $("#api-check-email").val(),
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content'),
                    },
                    type: "post",
                    async: false,
                    data: {
                        id: function(){ return $("input[name='user_id']").val()},
                        email: function(){ return $("#email").val(); }
                    }
                }
            },
            password: {
                minlength: 6,
                required: true
            },
            cpassword: {
                required: true,
                minlength: 6,
                equalTo: "#password"
            },
            gender: {
                required: true
            },
            pincode: {
                required: true,
                number: true,
                minlength: 5
            },
            dob: {
                required: true,
            },
            place_of_birth: {
                required: true,
                alpha: true,
            },
            nationality: {
                required: true,
            },
            marital_status: {
                required: true,
            },
            height: {
                required: true,
                number: true,
            },
            weight: {
                required: true,
                number: true,
            },
            mobile: {
                required: true,
                number: true,
                minlength: 10,
                remote: {
                    url: $("#api-check-mobile").val(),
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content'),
                    },
                    type: "post",
                    async: false,
                    data: {
                        id: function(){ return $("input[name='user_id']").val()},
                        mobile: function(){ return $("#mobile").val(); }
                    }
                },
                notEqualTo: "#kin_number",
            },
            landline: {
                number: true
            },
            landline_code: {
                number: true
            },
            '[country[0]': {
                required: true
            },
            'pincode[0]': {
                required: true
            },
            'state_name[0]': {
                required: true
            },
            'state[0]': {
                required: true
            },
            'city[0]': {
                required: true
            },
            'city_text[0]': {
                required: true
            },
            current_rank: {
                required: true
            },
            years: {
                required: true
            },
            months: {
                required: true
            },
            agree: {
                required: true
            },
            permananent_address: {
                required: true
            },
            last_wages: {
                required: true,
                number: true
            },
            applied_rank: {
                required: true
            },
            date_avaibility: {
                required: true
            },
        },
        messages: {
            firstname: {
                alpha: "Please enter only alphabets",
                required: "Please specify your full name"
            },
            lastname: {
                required: "Please specify your last name",
                lettersonly: "Please enter characters only"
            },
            email: {
                required: "We need your email address to contact you",
                email: "Your email address must be in the format of name@domain.com",
                remote: "Email already exist"
            },
            cpassword: {
                equalTo: "Confirm password is not matching with password "
            },
            gender: "Please check a gender!",
            rank_exp: {
                number: "Please enter experience in numbers"
            },
            mobile: {
                number: "Please enter valid Mobile number",
                remote: "Mobile already exist"
            },
            agree: {
                required: "Please agree to the terms and conditions."
            },
            place_of_birth: {
                alpha: "Please enter only alphabets."
            },
            /*kin_name: {
                alpha: "Please enter only alphabets."
            },*/
        },
        errorPlacement: function(error, element) {
            if( element.attr('name') == 'gender'){
                error.insertAfter($('.gender-option-container'));
            }else{
                error.insertAfter(element);
            }
        }
    });

    $("#imp-documents-back-button").on('click',function(e){
        $("#imp-documents").removeClass('page-done page-active');
        $("#basic-details").addClass('page-done page-active');
        $("#documents-details-form").hide();
        $("#basic-details-form").show();
    });

    $("#basic-details").on('click',function(e){
        $("#imp-documents").removeClass('page-done page-active');
        $("#basic-details").addClass('page-done page-active');
        $("#documents-details-form").hide();
        $("#basic-details-form").show();
    });

    $("#submitBasicDetailButton").on('click',function(e){
        e.preventDefault();

        var tab_index = parseInt($(this).attr('data-tab-index'));
        var next_tab = tab_index+1;

        $("[name^='country']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select country"
                }
            });
        });

        $("[name^='pincode']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please enter pincode"
                }
            } );
        });

        $("[name^='state']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select state"
                }
            } );
        });

        $("[name^='state_text']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select state"
                }
            } );
        });

        $("[name^='city']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select city"
                }
            } );
        });

        $("[name^='city_text']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select city"
                }
            } );
        });

        var l = Ladda.create(this);

        $('#basic-details-form').valid();
        if($('#basic-details-form').valid()){
            
            if(!($("#email-error").hasClass('hide'))){
                $("#email-error").addClass('hide');
            }
            if(!($("#mobile-error").hasClass('hide'))){
                $("#mobile-error").addClass('hide');
            }
        
            l.start();
            $.ajax({
                type: "POST",
                url: $("#basic-details-form").attr('action'),
                data: $("#basic-details-form").serialize(),
                statusCode: {
                    200:function (response) {
                        //$("#basic-details").removeClass('page-done page-active');
                        // console.log(response);
                        if(response.update_mobile == 0){
                            $("#mob-verification").removeClass('hide');
                            $("#first-step-mob-verification").removeClass('hide');
                            $('#firstStepModal').modal('show');
                        }
                        else{
                            $(".basic-details-tab").addClass('page-done page-active');
                            $(".imp-document-tab").removeClass('page-done page-active');
                            $("#documents-details-form").show();
                            $("#basic-details-form").hide();
                            $("#documents-details-form").removeClass('hide');

                            $("#tab-"+next_tab).addClass('page-active page-done').removeClass('disabled');

                            if($("#tab-1").hasClass('edit-details-tab')){
                                $("#tab-2").removeClass('page-done page-active');
                                $("#tab-1").addClass('page-done page-active');
                            }
                        }

                        window.scrollTo(0,0);
                        l.stop();
                    },
                    400:function (response) {
                        l.stop();

                    },
                    422:function (response) {
                        var text = '';
                        for (var prop in response.responseJSON){
                            if(prop == "mobile"){
                                $("#mobile-error").removeClass('hide');
                                $("#mobile-error").text(response.responseJSON['mobile'][0]);
                                $("#mobile").focus();
                            }
                            if(prop == "email"){
                                $("#email-error").removeClass('hide');
                                $("#email-error").text(response.responseJSON['email'][0]);
                                $("#email").focus();
                            }
                        }
                        for (var prop in response.responseJSON) {
                            text += response.responseJSON[prop][0]+ '<br />';
                        }
                        $("#alert-box-documents").show();
                        $("#alert-box-documents").html(text);

                        $('html,body').animate({
                            scrollTop: ($("#alert-box-documents").offset().top - 100) + 'px'},
                        'slow');
                        l.stop();

                    }
                }
            });
        }
    });

    $('#documents-details-form').validate({
        focusInvalid: false,
        invalidHandler: function(form, validator) {

            if (!validator.numberOfInvalids())
                return;

            $('html, body').animate({
                scrollTop: $(validator.errorList[0].element).offset().top + (-200)
            }, 200);

        },
        rules: {
            passcountry: {
                required: true
            },
            passno: {
                required: true
            },
            passplace: {
                required: true
            },
            passdateofexp: {
                required: true
            },
            us: {
                required: true
            },
            usvalid: {
                required: true
            },
            pccdate: {
                required: true
            },
            yf_issue_date: {
                required: true
            },
            cdccountry: {
                required: true
            },
            cdcno: {
                required: true
            },
            cdc_exp: {
                required: true
            },
            ocdccountry: {
                required: true
            },
            ocdcno: {
                required: true
            },
            ocdc_exp: {
                required: true
            },
            coc_no: {
                required: true
            },
            coc_exp: {
                required: true
            },
            coc_country: {
                required: true
            },
            grade: {
                required: true
            },
            ococ_no: {
                required: true
            },
            ococ_exp: {
                required: true
            },
            ococ_country: {
                required: true
            },
            ograde: {
                required: true
            }
        },
        messages: {
            passcountry: "Please select passport country",
            passno: "Please enter Passport Number",
            email: {
                required: "We need your email address to contact you",
                email: "Your email address must be in the format of name@domain.com"
            },
            gender: "Please check a gender!",
            rank_exp: {
                number: "Please enter experience in numbers"
            },
            mobile: {
                number: "Please enter valid Mobile number"
            }
        }
    });

    $(".close-otp-modal-button").on('click', function(e){
        if($("#current-route-name").val() == 'site.seafarer.registration'){
            window.location.href = $("#api-auto-welcome-route").val();
        }else{
             window.location.href = '/seafarer/profile';
        }
    });

    $(".first-step-close-otp-modal-button").on('click', function(e){
        
        $("#firstStepModal").modal('hide');
        $(".basic-details-tab").addClass('page-done page-active');
        $(".imp-document-tab").removeClass('page-done page-active');
        $("#documents-details-form").show();
        $("#basic-details-form").hide();
        $("#documents-details-form").removeClass('hide');

        $("#tab-2").addClass('page-active page-done').removeClass('disabled');

        if($("#tab-1").hasClass('edit-details-tab')){
            $("#tab-2").removeClass('page-done page-active');
            $("#tab-1").addClass('page-done page-active');
        }
    });

    $("[name='gmdss_country']").change(function(){
        $gmdss_value = $("[name='gmdss_country']").val();
        $india_value = $("[name='india_value']").val();
        if ($gmdss_value == $india_value) {
            $('.gmdss_endorsement').removeClass('hide').addClass('show');
            $('.gmdss_valid_till').removeClass('hide').addClass('show');
        }
        else{
            $('.gmdss_endorsement').removeClass('show').addClass('hide');
            $('.gmdss_valid_till').removeClass('show').addClass('hide');
        }
    });

    $("#submit-imp-documents-button").on('click',function(e){
        e.preventDefault();

        var optional = new Array();
        var current_rank = $("#current_rank").val();
        var fields = required_fields[current_rank];

        for(var i=0; i<fields.length; i++) {
            var field_arr = fields[i].split('-');
            if(field_arr.length > 1) {
                optional.push(field_arr[0]);
            }
        }

        $("[name^='cdccountry']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select country"
                }
            });
        });

        $("[name^='cdcno']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please enter number"
                }
            });
        });

        $("[name^='cdc_issue']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select issue date"
                }
            });
        });

        $("[name^='cdc_exp']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select expiry date"
                }
            });
        });

        $("[name^='cdc_ver']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select expiry date"
                }
            });
        });

        /*$("[name^='coecountry']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select country"
                }
            });
        });

        $("[name^='coeno']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please enter number"
                }
            });
        });

        $("[name^='coe_grade']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please enter grade"
                }
            });
        });

        $("[name^='coe_exp']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select expiry date"
                }
            });
        });*/

        if(jQuery.inArray('COP',optional) == -1){  
            $("[name^='cop_issue']").each(function(){
                $(this).rules("add", {
                    required: true,
                    messages: {
                        required: "Please select issue date"
                    }
                });
            });

            $("[name^='cop_exp']").each(function(){
                $(this).rules("add", {
                    required: true,
                    messages: {
                        required: "Please select expiry date"
                    }
                });
            });

            $("[name^='cop_no']").each(function(){
                $(this).rules("add", {
                    required: true,
                    messages: {
                        required: "Please enter number"
                    }
                });
            }); 

            $("[name^='cop_grade']").each(function(){
                $(this).rules("add", {
                    required: true,
                    messages: {
                        required: "Please enter grade"
                    }
                });
            });
        }

        if(jQuery.inArray('GMDSS',optional) == -1){

            $("[name='gmdss_country']").each(function(){
                $(this).rules("add", {
                    required: true,
                    messages: {
                        required: "Please select country"
                    }
                });
            });

            $("[name='gmdss_no']").each(function(){
                $(this).rules("add", {
                    required: true,
                    messages: {
                        required: "Please enter number"
                    }
                });
            });

            $("[name='gmdss_doe']").each(function(){
                $(this).rules("add", {
                    required: true,
                    messages: {
                        required: "Please enter expiry date"
                    }
                });
            });
        }
        
        if(jQuery.inArray('WATCH_KEEPING',optional) == -1){

            $("[name='watch_country']").each(function(){
                $(this).rules("add", {
                    required: true
                });
            });

            $("[name='deckengine']").each(function(){
                $(this).rules("add", {
                    required: true
                });
            });
            $("[name='watch_no']").each(function(){
                $(this).rules("add", {
                    required: true
                });
            });
            $("[name='watchissud']").each(function(){
                $(this).rules("add", {
                    required: true
                });
            });
        }

        if(jQuery.inArray('COC',optional) == -1){  
            $("[name^='coc_country']").each(function(){
                $(this).rules("add", {
                    required: true,
                    messages: {
                        required: "Please select country"
                    }
                });
            });

            $("[name^='coc_no']").each(function(){
                $(this).rules("add", {
                    required: true,
                    messages: {
                        required: "Please enter number"
                    }
                });
            });

            $("[name^='grade']").each(function(){
                $(this).rules("add", {
                    required: true,
                    messages: {
                        required: "Please enter grade"
                    }
                });
            }); 

            $("[name^='coc_exp']").each(function(){
                $(this).rules("add", {
                    required: true,
                    messages: {
                        required: "Please enter expiry date"
                    }
                });
            });

            $("[name^='coc_ver']").each(function(){
                $(this).rules("add", {
                    required: true,
                    messages: {
                        required: "Please enter verification date"
                    }
                });
            });
        }

        var tab_index = parseInt($(this).attr('data-tab-index'));
        var next_tab = tab_index+1;

        var l = Ladda.create(this);
        if($("#documents-details-form").valid()){
            l.start();
            $.ajax({
                type: "POST",
                url: $("#documents-details-form").attr('action'),
                data: $("#documents-details-form").serialize(),
                statusCode: {
                    200:function (response) {
                        $("#alert-box-documents").hide();
                        $(".certificate-document-tab").removeClass('page-done page-active');
                        $(".sea-service-document-tab").addClass('page-done page-active');

                        $("#seafarer-certificates-form").removeClass('hide');
                        $("#documents-details-form").addClass('hide');

                        $("#tab-"+next_tab).addClass('page-active page-done').removeClass('disabled');

                        if($("#tab-2").hasClass('edit-details-tab')){
                            $("#tab-3").removeClass('page-done page-active');
                            $("#tab-2").addClass('page-done page-active');
                        }
                        window.scrollTo(0,0);
                        l.stop();
                    },
                    400:function (response) {
                        l.stop();
                    },
                    422:function (response) {

                        var foo = response.responseJSON;
                        var text = '';
                        for (var prop in foo) {
                            text += foo[prop][0]+ '<br />';
                        }
                        $("#alert-box-documents").show();
                        $("#alert-box-documents").html(text);

                        $('html,body').animate({
                            scrollTop: ($("#alert-box-documents").offset().top - 100) + 'px'},
                        'slow');
                        l.stop();
                    }
                }
            });
        }
    });

    $('#mob-verification').validate({

        rules:{
            mob_otp: {
                required: true,
                minlength: 6,
                maxlength: 6,
                number: true
            }
        }
    });

    $("#show-hide").click(function(){

        if($("#show-hide").val() == 'show'){
            $('.show-data').show();
            $('.show-label').hide();
            $("#show-hide").val('hide');
            $("#show-hide").text('Hide');
            $("#submitProfileBasicDetailButton").prop("disabled", false);
            $("#profile-pic").prop("disabled", false);
            $("#seaferer-doc-submit-button").prop("disabled", false);
        }else{
            $('.show-data').hide();
            $('.show-label').show();
            $("#show-hide").val('show');
            $("#show-hide").text('Show');
            $("#submitProfileBasicDetailButton").attr('disabled', 'disabled');
            $("#profile-pic").attr('disabled', 'disabled');
            $("#seaferer-doc-submit-button").attr('disabled', 'disabled');
        }
    });

    $(document).on('change','.seafarer_course_type',function (e) {
        var course = $(this).val();

        $(".seafarer_courses").addClass('hide');
        $(".certificate_name ").val('');

        if(course == 'normal'){
            $(".normal_course").removeClass('hide');
        }else if(course == 'value_added'){
            $(".value_added_course").removeClass('hide');
        }
    });

    $(document).on('click','.add-certificate-button',function (e) {

        var form_id = $(this).attr('data-form-id');

        var html = $('#certificate-template').clone();

        if($(".course_total_block_index").length > 0 && course_check == false){
            certificate_block_index = parseInt($(".course_total_block_index").length);
            course_check = true;
        }

        var certificate_index = $(".course_total_block_index").length + 1;
        html.find(".certificate-name").text('Certificate '+certificate_index);

        html.find("input[type='text']").val("");
        html.find("textarea").val("");
        html.find("select").val("");

        html.find("label.error").addClass('hide');
        html.find(".certificate_name").attr('name','certificate_name['+certificate_block_index+']');
        html.find(".date_of_issue").attr('name','date_of_issue['+certificate_block_index+']');
        html.find(".date_of_expiry").attr('name','date_of_expiry['+certificate_block_index+']');
        html.find(".place_of_issue").attr('name','place_of_issue['+certificate_block_index+']');
        html.find(".issue_by").attr('name','issue_by['+certificate_block_index+']');
        html.find(".issuing_authority").attr('name','issuing_authority['+certificate_block_index+']');
        html.find(".certificate_no").attr('name','certificate_no['+certificate_block_index+']');

        html.find(".certificate-close-button").addClass('certificate-close-button-'+certificate_block_index).removeClass('certificate-close-button-0').removeClass('hide');
        html.find(".certificate-close-button").attr('id','certificate-close-button-'+certificate_block_index);

        $("#add-more-certificate-row").before(html);
        certificate_block_index++;

        $('.issue-datepicker').datepicker({
        format: "dd-mm-yyyy",
        endDate: date,
        autoClose: true,
        startView: 2
        }).on('changeDate', function(ev) {
            if(ev.viewMode === 0){
                $('.issue-datepicker').datepicker('hide');
            }
       });

        $('.datepicker').datepicker({
            format: "dd-mm-yyyy",
            startDate: date,
            autoClose: true,
            startView: 2
        }).on('changeDate', function(ev) {
            if(ev.viewMode === 0){
                $('.datepicker').datepicker('hide');
            }
       });
    });

    $(document).on('click','.add-value-added-certificate-button',function (e) {
        var html = $('#value-added-certificate-template').clone();

        if($(".value_added_course_total_block_index").length > 0 && value_added_course_check == false){
            value_added_certificate_block_index = parseInt($(".value_added_course_total_block_index").length);
            value_added_course_check = true;
        }

        var value_added_certificate_index = $(".value_added_course_total_block_index").length + 1;
        html.find(".Value_added_certificate-name").text('Certificate '+value_added_certificate_index);

        html.find("input[type='text']").val("");
        html.find("textarea").val("");
        html.find("select").val("");

        html.find("label.error").addClass('hide');
        html.find(".value_added_certificate_name").attr('name','value_added_certificate_name['+value_added_certificate_block_index+']');
        html.find(".value_added_date_of_issue").attr('name','value_added_date_of_issue['+value_added_certificate_block_index+']');
        html.find(".value_added_date_of_expiry").attr('name','value_added_date_of_expiry['+value_added_certificate_block_index+']');
        html.find(".value_added_place_of_issue").attr('name','value_added_place_of_issue['+value_added_certificate_block_index+']');
        html.find(".value_added_issue_by").attr('name','value_added_issue_by['+value_added_certificate_block_index+']');
        html.find(".value_added_issuing_authority").attr('name','value_added_issuing_authority['+value_added_certificate_block_index+']');
        html.find(".value_added_certificate_no").attr('name','value_added_certificate_no['+value_added_certificate_block_index+']');

        html.find(".value_added_certificate-close-button").addClass('value_added_certificate-close-button-'+value_added_certificate_block_index).removeClass('certificate-close-button-0').removeClass('hide');
        html.find(".value_added_certificate-close-button").attr('id','value_added_certificate-close-button-'+value_added_certificate_block_index);

        $("#add-value-added-certificate").before(html);
        value_added_certificate_block_index++;

        $('.issue-datepicker').datepicker({
        format: "dd-mm-yyyy",
        endDate: date,
        autoClose: true,
        startView: 2
        }).on('changeDate', function(ev) {
            if(ev.viewMode === 0){
                $('.issue-datepicker').datepicker('hide');
            }
       });

        $('.datepicker').datepicker({
            format: "dd-mm-yyyy",
            startDate: date,
            autoClose: true,
            startView: 2
        }).on('changeDate', function(ev) {
            if(ev.viewMode === 0){
                $('.datepicker').datepicker('hide');
            }
       });
    });

    /*$(document).on('click','.add-sea-service-button',function (e) {

        var form_id = $(this).attr('data-form-id');

        var html = $('#sea-service-template').clone();

        if($(".service_total_block_index").length > 0 && service_check == false){
            sea_service_block_index = parseInt($(".service_total_block_index").length);
            service_check = true;
        }

        var certificate_index = $(".service_total_block_index").length + 1;
        html.find(".sea-service-name").text('Sea Service '+certificate_index);

        html.find("input[type='text']").val("");
        html.find("textarea").val("");
        html.find("select").val("");

        html.find("label.error").addClass('hide');
        html.find(".shipping_company").attr('name','shipping_company['+sea_service_block_index+']');
        html.find(".name_of_ship").attr('name','name_of_ship['+sea_service_block_index+']');
        html.find(".ship_type").attr('name','ship_type['+sea_service_block_index+']');
        html.find(".rank").attr('name','rank['+sea_service_block_index+']');
        html.find(".grt").attr('name','grt['+sea_service_block_index+']');
        //html.find(".nrt").attr('name','nrt['+sea_service_block_index+']');
        html.find(".bhp").attr('name','bhp['+sea_service_block_index+']');
        html.find(".engine_type").attr('name','engine_type['+sea_service_block_index+']');
        html.find(".engine_type").attr('block-index',sea_service_block_index);
        html.find(".other_engine_type").attr('name','other_engine_type['+sea_service_block_index+']');
        html.find(".date_from").attr('name','date_from['+sea_service_block_index+']');
        html.find(".date_to").attr('name','date_to['+sea_service_block_index+']');
        html.find(".ship_flag").attr('name','ship_flag['+sea_service_block_index+']');

        html.find(".other_engine_type-0").addClass('other_engine_type-'+sea_service_block_index).removeClass('other_engine_type-0');
        html.find(".sea-service-close-button").addClass('sea-service-close-button-'+sea_service_block_index).removeClass('sea-service-close-button-0').removeClass('hide');
        html.find(".sea-service-close-button").attr('id','sea-service-close-button-'+sea_service_block_index);

        $("#add-more-service-row").before(html);
        sea_service_block_index++;

        $('.issue-datepicker').datepicker({
        format: "dd-mm-yyyy",
        endDate: date,
        autoClose: true,
        startView: 2
        }).on('changeDate', function(ev) {
            if(ev.viewMode === 0){
                $('.issue-datepicker').datepicker('hide');
            }
       });
    });*/

    $(document).on('click','.course-close-button',function (e) {

        var course_id = $(this).closest('.course-close-button').attr('data-id');

        if(typeof $("#add-seafarer-form").val() != 'undefined'){
            url = $("#api-admin-seafarer-delete-course").val();
            url = url+"?user_id="+$("#user_id").val();
        }else{
            url = $("#api-site-seafarer-delete-course").val();
        }

        $.ajax({
            type: "POST",
            url: url,
            data: {
                'course_id' : course_id
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            },
            statusCode: {
                200:function (response) {
                    $(".course_detail_row_"+course_id).remove();

                    if($(".normal-course-name").length == '0'){

                        $(".normal_course_section").empty();
                        $(".normal_course_section").append("<div class='row no-data-found'>"+
                            "<div class='col-xs-12 text-center'>"+
                                "<div class='discription'>"+
                                    "<span class='content-head'>No Data Found</span>"+
                                "</div>"+
                            "</div>"+
                        "</div>");
                    }else{
                        $(".normal-course-name").each(function(index,element){
                            var index = index + 1;
                            $(this).text('Certificate '+index);
                        });    
                    }

                    if($(".value-added-course-name").length == '0'){

                        $(".value_added_course_section").empty()
                        $(".value_added_course_section").append("<div class='row no-data-found'>"+
                            "<div class='col-xs-12 text-center'>"+
                                "<div class='discription'>"+
                                    "<span class='content-head'>No Data Found</span>"+
                                "</div>"+
                            "</div>"+
                        "</div>");
                    }else{
                        $(".value-added-course-name").each(function(index,element){
                            var index = index + 1;
                            $(this).text('Certificate '+index);
                        });    
                    }

                    toastr.success(response.message, 'Success');
                },
                400:function (response) {
                    toastr.error(response.responseJSON.message, 'Error');
                }
            }

        });
    });

    $(document).on('change','.seafarer_course_type',function (e) {
        
        if(typeof $("#add-seafarer-form").val() != 'undefined'){
            var url = $("#api-admin-get-course-by-course-type").val();
        }else{
            var url = $("#api-seafarer-get-course-by-course-type").val();
        }

        var course_type = $(".seafarer_course_type").val();
        
        $.ajax({
            type: "GET",
            url: url,
            data: {
                'course_type' : course_type,
                '_token' : $("input[name='_token']").val()
            },
            statusCode: {
                200:function (response) {
                    $("select[name='certificate_name'] option").remove();
                    $.each( response.courses, function( key, value ) {
                        $('select[name="certificate_name"]').append(value);
                    });
                    $('select[name="certificate_name"]').select('refresh');
                },
                400:function (response) {
                    $("select[name='certificate_name'] option").remove();
                    $('select[name="certificate_name"]').append("<option value=''>Select Your Certificate</option>");
                    $('select[name="certificate_name"]').select('refresh');
                }
            }
        });

    });

    $(document).on('click','.course-edit-button',function (e) {

        var course_id = $(this).closest('.course-edit-button').attr('data-id');
        $(".existing_course_id").val(course_id);

        window.scrollTo(0,200);

        if(typeof $("#add-seafarer-form").val() != 'undefined'){
            url = $("#api-admin-seafarer-get-course-by-course-id").val();
            url = url+"?user_id="+$("#user_id").val();
        }else{
            url = $("#api-seafarer-get-course-by-course-id").val();
        }

        //$(".sea_service_detail_row_"+service_id).remove();

        $.ajax({
            type: "GET",
            url: url,
            data: {
                'course_id' : course_id,
                '_token' : $("input[name='_token']").val()
            },
            statusCode: {
                200:function (response) {

                    $("select[name='certificate_name'] option").remove();
                    $.each( response.courses, function( key, value ) {
                        $('select[name="certificate_name"]').append(value);
                    });
                    $('select[name="certificate_name"]').select('refresh');

                    if(typeof(response.result.result.issue_date) != 'object'){
                        var doi = response.result.result.issue_date.split('-');
                        $(".date_of_issue").val(doi[2]+'-'+doi[1]+'-'+doi[0]);
                    }

                    if(typeof(response.result.result.expiry_date) != 'object'){
                        var doe = response.result.result.expiry_date.split('-');
                        $(".date_of_expiry").val(doe[2]+'-'+doe[1]+'-'+doe[0]);
                    }else{
                        if(typeof(response.result.result.issue_date) != 'object'){
                            var doi = response.result.result.issue_date.split('-');
                            doi[0] = (parseInt(doi[0]) + 5).toString();
                            $(".date_of_expiry").val(doi[2]+'-'+doi[1]+'-'+doi[0]);
                        }
                    } 

                    if(typeof(response.result.result.issue_by) != 'object'){
                        $(".issue_by").val(response.result.result.issue_by);
                    } 
                    if(typeof(response.result.result.certification_number) != 'object'){
                        $(".certificate_no").val(response.result.result.certification_number);
                    }
                   
                    if(typeof(response.result.result.course_type) != 'object'){
                        setTimeout(function(){ 
                            if(response.result.result.course_type == 'Normal'){
                                $(".seafarer_course_type").val('normal');
                            }else{
                                $(".seafarer_course_type").val('value_added');
                            }

                            if(response.result.result.course_id){
                                $(".certificate_name").val(response.result.result.course_id);
                            }
                        }, 50);

                    }

                },
                400:function (response) {
                   
                }
            }
         });
    });

	
	
	
    $(document).on('click','.add-seafarer-course-button',function (e) {

        $("#seafarer_course_type").rules( "add", {
            required: true,
            messages: {
                required: "Please select course type",
            }
        });

        $(".certificate_name").rules( "add", {
            required: true,
            messages: {
                required: "Please select certificate name",
            }
        });

        $(".date_of_issue").rules( "add", {
            required: true,
            messages: {
                required: "Please enter date of issue",
            }
        });

        if(typeof($("#add-seafarer-form").val()) != 'undefined'){
            var url = $("#api-admin-seafarer-store-course").val();
            url = url+"?user_id="+$("#user_id").val();
            var form = $("#add-seafarer-form");
        }
        else{
            var url = $("#api-site-seafarer-store-course").val();
            var form = $("#seafarer-certificates-form");
        }
		//var url = $("#api-site-seafarer-store-course").val();
		//var form = $("#seafarer-certificates-form");
        if(form.valid()){
            var data = $('#seafarer_courses_section :input').serialize();
            var course_type = $(".seafarer_course_type").val();
            
            var l = Ladda.create(this);
            l.start();

            $.ajax({
                type: "POST",
                url: url,
                data: data,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                },
                statusCode: {
                    200:function (response) {
                        $(".existing_course_id").val('');
                        $(':input','#seafarer_courses_section')
                            .not(':button, :submit, :reset, :hidden')
                            .val('')
                            .removeAttr('checked');

                        $("select[name='certificate_name'] option").remove();
                        $('select[name="certificate_name"]').append("<option value=''>Select Your Certificate</option>");
                        $('select[name="certificate_name"]').select('refresh');

                        if(course_type == 'value_added'){
                            $(".value_added_course_section").empty();
                            $(".value_added_course_section").addClass('sea-service-loader full-cnt-loader');
                        }else{
                            $(".normal_course_section").empty();
                            $(".normal_course_section").addClass('sea-service-loader full-cnt-loader');    
                        }

                        if(course_type == 'value_added'){
                            setTimeout(function(){ 
                                $(".value_added_course_section").removeClass('sea-service-loader full-cnt-loader');
                                $(".value_added_course_section").append(response.courses);
                            }, 500);

                        }else{
                            setTimeout(function(){ 
                                $(".normal_course_section").removeClass('sea-service-loader full-cnt-loader');
                                $(".normal_course_section").append(response.courses);
                            }, 500);    
                        }

                        $("#seafarer_course_type").rules( "remove", 'required');
                        $(".certificate_name").rules( "remove", 'required');
                        $(".date_of_issue").rules( "remove", 'required');

                        toastr.success(response.message, 'Success');
                        l.stop();
                    },
                    400:function (response) {
                        toastr.error(response.responseJSON.message, 'Error');
                        l.stop();
                    }
                }
            });
        }
    });


    $(document).on('click','.add-sea-service-button',function (e) {
        
        var append = false;
        $("[name^='shipping_company']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please enter shipping company name"
                }
            });
        });

        $("[name^='name_of_ship']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please enter ship name"
                }
            });
        });

        $("[name^='ship_type']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select ship type"
                }
            });
        });

        /*$("[name^='ship_flag']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select ship flag"
                }
            });
        });*/

        $("[name^='rank']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select rank"
                }
            });
        });

        $("[name^='other_engine_type']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please enter other engine type",
                }
            });
        });

        $("[name^='date_from']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select sign on date"
                }
            });
        });

        $("[name^='date_to']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select sign off date"
                }
            });
        });


        if(typeof($("#add-seafarer-form").val()) != 'undefined'){

            $(".engine_type").attr('disabled',false);
            $(".ship_flag").attr('disabled',false);
            $(".ship_type").attr('disabled',false);

            var form = $("#add-seafarer-form");
            var formdata = new FormData();

            var temp_url = $("#api-admin-seafarer-store-single-service-details").val();
            var url = temp_url.split('%');
            
            url = url[0]+'user_id='+$("#user_id").val();

            var data = $("#add-seafarer-form").serializeArray();
        }
        else{

            $(".engine_type").attr('disabled',false);
            $(".ship_flag").attr('disabled',false);
            $(".ship_type").attr('disabled',false);

            var form = $("#seafarer-sea-service-form");

            var url = $("#api-seafarer-store-single-service-details").val();

            var data = $("#seafarer-sea-service-form").serialize();
        }

        if(form.valid()){

            var l = Ladda.create(this);
            l.start();

            $.ajax({
                type: "POST",
                url: url,
                data: data,
                statusCode: {
                    200:function (response) {
                        toastr.success(response.message, 'Success');

                        $(".no-data-found").addClass('hide');
                        $(".sea-service-skip-button").addClass('hide');
                        $("#sea-service-details-submit").removeClass('hide');
                        
                        $(".sea_service_detail_section").empty();
                        
                        $(".sea_service_detail_section").addClass('sea-service-loader full-cnt-loader');

                        setTimeout(function(){ 
                            $(".sea_service_detail_section").removeClass('sea-service-loader full-cnt-loader');
                            $(".sea_service_detail_section").append(response.template);
                        }, 500);

                        $(".ship_type").removeAttr('readonly');
                        $(".grt").removeAttr('readonly');
                        $(".bhp").removeAttr('readonly');
                        $(".engine_type").removeAttr('readonly');
                        $(".ship_flag").removeAttr('readonly');

                        $(':input','#seafarer-sea-service-form')
                            .not(':button, :submit, :reset, :hidden')
                            .val('')
                            .removeAttr('checked');

                        $(':input','#sea-service-template')
                            .not(':button, :submit, :reset, :hidden')
                            .val('')
                            .removeAttr('checked');

                        l.stop();
                    },
                    400:function (response) {
                        toastr.error(response.responseJSON.message, 'Error');
                        l.stop();
                    }
                }
            });

        }

    });


    $("#seafarer-certificates-form").validate({
        focusInvalid: false,
        invalidHandler: function(form, validator) {

            if (!validator.numberOfInvalids())
                return;

            $('html, body').animate({
                scrollTop: $(validator.errorList[0].element).offset().top + (-200)
            }, 200);

        },
        rules:{
            /*name_of_school: {
                required: true
            },
            school_from: {
                required: true
            },
            school_to: {
                required: true
            },
            school_qualification: {
                required: true
            },
            institute_name: {
                required: true
            },
            institute_from: {
                required: true
            },
            institute_to: {
                required: true
            },
            institute_degree: {
                required: true
            }*/
            checkbox: {
                required: function (element) {
                    var boxes = $('.course_checkbox');
                    if (boxes.filter(':checked').length == 0) {
                        return true;
                    }
                    return false;
                }
            }
        }, 
        messages: { 
            checkbox: {
                required: "Please select at least one course."
            }
        }    
    });

    $("#certificate-details-submit").on('click',function(e){

        var tab_index = parseInt($(this).attr('data-tab-index'));
        var next_tab = tab_index+1;

        $("[name^='course_date_of_issue']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select date of issue"
                }
            });
        });

        
        $("[name^='value_added_course_date_of_issue']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select date of issue"
                }
            });
        });

        /*
        $("[name^='certificate_name']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select certificate name"
                }
            });
        });

        $("[name^='date_of_issue']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select issue date"
                }
            });
        });*/

        /*$("[name^='date_of_expiry']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select expiry date"
                }
            });
        });*/

        /*$("[name^='issue_by']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please enter issue by"
                }
            });
        });*/

        /*$("[name^='issuing_authority']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select issuing authority"
                }
            });
        });

        $("[name^='place_of_issue']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select place of issue"
                }
            });
        });*/

        /*$("[name^='certificate_no']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please enter certificate number"
                }
            });
        });*/

        /*$("[name^='value_added_certificate_name']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select certificate name"
                }
            });
        });

        $("[name^='value_added_date_of_issue']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select issue date"
                }
            });
        });

        $("[name^='value_added_issue_by']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select issue by"
                }
            });
        });

        $("[name^='value_added_issuing_authority']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select issuing authority"
                }
            });
        });

        $("[name^='value_added_place_of_issue']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select place of issue"
                }
            });
        });

        $("[name^='value_added_certificate_no']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select certificate number"
                }
            });
        });*/

         if($("#seafarer-certificates-form").valid()){
            
            var l = Ladda.create(this);
            l.start();

            $.ajax({
                type: "POST",
                url: $("#seafarer-certificates-form").attr('action'),
                data: $("#seafarer-certificates-form").serialize(),
                statusCode: {
                    200:function (response) {

                        l.stop();
						
						$(".sea-service-document-tab").addClass('page-done page-active');
                        $(".certificate-document-tab").removeClass('page-done page-active');

                        $("#seafarer-sea-service-form").removeClass('hide');
                        $("#seafarer-certificates-form").addClass('hide');
                        //$("#upload_documents").addClass('page-done page-active');
                       // $("#upload_documents").removeClass('hide');
                        //$("#seafarer-certificates-form").hide();

						
						$("#tab-"+next_tab).addClass('page-active page-done').removeClass('disabled');

                        if($("#tab-3").hasClass('edit-details-tab')){
                            $("#tab-4").removeClass('page-done page-active');
                            $("#tab-3").addClass('page-done page-active');
                        }
                        //if($("#tab-4").hasClass('edit-details-tab')){
                            //$("#tab-5").removeClass('page-done page-active');
                            //$("#tab-4").addClass('page-done page-active');
                       // }

                        getUserDocuments();

                        window.scrollTo(0,0);
                    },
                    400:function (response) {
                        l.stop();
                    }
                }
            });
        }
    });


    $("#certificate-skip-button").on('click', function(e){
        
        /*if($("#current-route-name").val() == 'site.seafarer.registration'){
            window.location.href = $("#api-auto-welcome-route").val();
        }else{
            window.location.href = $("#api-seafarer-profile-route").val();
        }*/

        /*if($('#basic-details').hasClass('basic-details-tab')){
            $("#sea-service-documents-circle").css("background-color","red");
            $("#sea-service-documents-circle").css("border-color","red");
            $(".page-name").css("color","red");
        }*/

        $("#upload_documents").addClass('page-done page-active');
        $("#upload_documents").removeClass('hide');
        $("#seafarer-certificates-form").hide();

        if($("#tab-4").hasClass('edit-details-tab')){
            $("#tab-5").removeClass('page-done page-active');
            $("#tab-4").addClass('page-done page-active');
        }

        getUserDocuments();

        window.scrollTo(0,0);
        
    });

	
	 $("#general-form-submit").on('click',function(e){

        var tab_index = parseInt($(this).attr('data-tab-index'));
        var next_tab = tab_index+1;

        

         if($("#general-tab-form").valid()){
            
            var l = Ladda.create(this);
            l.start();

            $.ajax({
                type: "POST",
                url: $("#general-tab-form").attr('action'),
                data: $("#general-tab-form").serialize(),
                statusCode: {
                    200:function (response) {

                        l.stop();
						
						
                        $("#upload-document-tab").addClass('page-done page-active');
                        $("#upload_documents").removeClass('hide');
                        $("#general-tab-form").hide();
						
						$("#tab-"+next_tab).addClass('page-active page-done').removeClass('disabled');
						
                        if($("#tab-5").hasClass('edit-details-tab')){
                            $("#tab-6").removeClass('page-done page-active');
                            $("#tab-5").addClass('page-done page-active');
                        }
                       

                       getUserDocuments();

                        window.scrollTo(0,0);
                    },
                    400:function (response) {
                        l.stop();
                    }
                }
            });
        }
    });

	
	$("#general-form-skip-button").on('click', function(e){
        
        $("#upload_documents").addClass('page-done page-active');
        $("#upload_documents").removeClass('hide');
        $("#general-tab-form").hide();

        if($("#tab-5").hasClass('edit-details-tab')){
            $("#tab-6").removeClass('page-done page-active');
            $("#tab-5").addClass('page-done page-active');
        }
        window.scrollTo(0,0);
    });
    $("#sea-service-skip-button").on('click', function(e){
        
        $("#general-tab").addClass('page-done page-active');
        $("#general-tab-form").removeClass('hide');
        $("#seafarer-sea-service-form").hide();

        if($("#tab-4").hasClass('edit-details-tab')){
            $("#tab-5").removeClass('page-done page-active');
            $("#tab-4").addClass('page-done page-active');
        }
        window.scrollTo(0,0);
    });

    $("#document-skip-button").on('click', function(e){

        if($("#current-route-name").val() == 'site.seafarer.registration'){
            window.location.href = $("#api-auto-welcome-route").val();
        }else{
            window.location.href = $("#api-seafarer-profile-route").val();
        }
    });


    $("#seafarer-sea-service-form").validate();

    $("#sea-service-details-submit").on('click',function(e){
        $("#seafarer-certificates-form").addClass('page-done page-active');
        $("#seafarer-certificates-form").removeClass('hide');
        $("#seafarer-sea-service-form").hide();
        $("#tab-4").addClass('page-active page-done').removeClass('disabled');

        if($("#tab-3").hasClass('edit-details-tab')){
            $("#tab-4").removeClass('page-done page-active');
            $("#tab-3").addClass('page-done page-active');
        }
        window.scrollTo(0,0);
    });

    $(document).on('click','.certificate-close-button',function (e) {
        
        $(this).closest('#certificate-template').remove();

        $(".certificate-name").each(function(index,element){
            var index = index + 1;
            $(this).text('Certificate '+index);
        });
    });

    $(document).on('click','.value_added_certificate-close-button',function (e) {
        
        $(this).closest('#value-added-certificate-template').remove();

        $(".Value_added_certificate-name").each(function(index,element){
            var index = index + 1;
            $(this).text('Certificate '+index);
        });
    });

    $(document).on('click','.sea-service-close-button',function (e) {
        //$(this).closest('#sea-service-template').remove();

        var service_id = $(this).attr('data-service-id');
        var close_btn_id = $(this).attr('data-id');
            
        if(typeof $("#api-admin-seafarer-delete-service-by-service-id").val() != 'undefined'){
            url = $("#api-admin-seafarer-delete-service-by-service-id").val();
            url = url+"?user_id="+$("#user_id").val();
        }else{
            url =$("#api-seafarer-delete-service-by-service-id").val();
        }

        $.ajax({
            type: "POST",
            url: url,
            data: {
                'service_id' : service_id,
                '_token' : $("input[name='_token']").val()
            },
            statusCode: {
                200:function (response) {
                    toastr.success(response.message, 'Success');
                    $(".sea_service_detail_row_"+close_btn_id).remove();

                    if($(".sea-service-name").length == '0'){
                        $(".sea_service_detail_section").append("<div class='row no-data-found'>"+
                            "<div class='col-xs-12 text-center'>"+
                                "<div class='discription'>"+
                                    "<span class='content-head'>No Data Found</span>"+
                                "</div>"+
                            "</div>"+
                        "</div>");

                        $(".sea-service-skip-button").removeClass('hide');
                        $("#sea-service-details-submit").addClass('hide');
                    }else{
                        $(".sea-service-name").each(function(index,element){
                            var index = index + 1;
                            $(this).text('Sea Service '+index);
                        });    
                    }
                    
                },
                400:function (response) {
                    toastr.error(response.responseJSON.message, 'Error');
                }
            }
         });
    });

    $(document).on('click','.sea-service-edit-button',function (e) {

        var index = $(this).attr('data-index-id');
        var service_id = $(this).attr('data-service-id');

        window.scrollTo(0,0);

        //$(".sea_service_detail_row_"+service_id).remove();
        
        $(".sea-service-name").each(function(index,element){
            var index = index + 1;
            $(this).text('Sea Service '+index);
        });

        if(typeof $("#api-admin-seafarer-get-service-by-service-id").val() != 'undefined'){
            url = $("#api-admin-seafarer-get-service-by-service-id").val();
            url = url+"?user_id="+$("#user_id").val();
        }else{
            url =$("#api-seafarer-get-service-by-service-id").val();
        }

        $.ajax({
            type: "POST",
            url: url,
            data: {
                'service_id' : service_id,
                '_token' : $("input[name='_token']").val()
            },
            statusCode: {
                200:function (response) {
                    $(".exiting_service_id").val(service_id);

                    if(response.sea_service_details.grt.length > 0){
                        $(".grt").val(response.sea_service_details.grt);
                    }
                    if(response.sea_service_details.company_name.length > 0){
                        $(".shipping_company").val(response.sea_service_details.company_name);
                    } 
                    if(response.sea_service_details.ship_name.length > 0){
                        $(".name_of_ship").val(response.sea_service_details.ship_name);
                    }
                    if(response.sea_service_details.from.length > 0){
                        $(".date_from").val(response.sea_service_details.from);
                    }
                    if(response.sea_service_details.to.length > 0){
                        $(".date_to").val(response.sea_service_details.to);
                    }
                    if(response.sea_service_details.ship_type){
                        $(".ship_type").val(response.sea_service_details.ship_type);
                    }
                    if(response.sea_service_details.ship_flag){
                        $(".ship_flag").val(response.sea_service_details.ship_flag);
                    }
                    if(response.sea_service_details.rank_id){
                        $(".rank").val(response.sea_service_details.rank_id);
                    }
                    if(response.sea_service_details.bhp.length > 0){
                        $(".bhp").val(response.sea_service_details.bhp);
                    }
                    if(typeof(response.sea_service_details.engine_type) != 'object' && response.sea_service_details.engine_type.length > 0){
                        $(".engine_type").val(response.sea_service_details.engine_type);
                    }
                    if(response.sea_service_details.ship_type.length > 0){
                        $("#ship_type").val(response.sea_service_details.ship_type);
                        $(".ship_type").val(response.sea_service_details.ship_type);
                    }

                    if(response.sea_service_details.engine_type == '11'){
                        $("#other_engine_type").removeClass('hide');
                        $(".other_engine_type").val(response.sea_service_details.other_engine_type);
                    }else{
                        $("#other_engine_type").addClass('hide');
                        $(".other_engine_type").val('');
                    }

                },
                400:function (response) {
                   
                }
            }
         });
    });

    $(document).on('change', '.country', function () {
       var block_index = $(this).attr('block-index');
       if( $(this).val() == india_value ) {
           $(".state-block-"+block_index).find('.fields-for-india').removeClass('hide');
           $(".state-block-"+block_index).find('.fields-not-for-india').addClass('hide');

           $(".city-block-"+block_index).find('.fields-for-india').removeClass('hide');
           $(".city-block-"+block_index).find('.fields-not-for-india').addClass('hide');

           $("[name='pincode["+block_index+"]']").rules("add", {
               maxlength: 6,
               messages: {
                   maxlength: "Please provide valid 6 digit pincode"
               }
           });

           $("[name='pincode["+block_index+"]']").addClass('pin_code_fetch_input');
       } else {
           $(".state-block-"+block_index).find('.fields-for-india').addClass('hide');
           $(".state-block-"+block_index).find('.fields-not-for-india').removeClass('hide');

           $(".city-block-"+block_index).find('.fields-for-india').addClass('hide');
           $(".city-block-"+block_index).find('.fields-not-for-india').removeClass('hide');

           $("[name='pincode["+block_index+"]']").removeClass('pin_code_fetch_input');
           
           $("[name='pincode["+block_index+"]']").rules('remove','maxlength');


       }
    });

    /*$("#us").on('click',function(e){
        if($(this).val() == 1){
            $("#us-block").show();
        }else{
            $("#us-block").hide();
        }
    });*/

    $(document).on('click','.add-cdc-button',function (e) {

        var form_id = $(this).attr('data-form-id');

        var html = $('#cdc-template').clone();

        if($(".total-count-cdc").length > 0 && cdc_check == false){
            cdc_block_index = $(".total-count-cdc").length;
            cdc_check = true;
        }

        var cdc_index = $(".total-count-cdc").length + 1;
        html.find(".cdc-name").text('CDC Details '+cdc_index).addClass('cdc-name-'+cdc_block_index).removeClass('cdc-name-0');

        html.find("input[type='text']").val("");
        html.find("textarea").val("");
        html.find("select").val("");

        html.find("label.error").addClass('hide');
        html.find(".cdccountry").attr('name','cdccountry['+cdc_block_index+']');
        html.find(".cdcno").attr('name','cdcno['+cdc_block_index+']');
        html.find(".cdc_issue").attr('name','cdc_issue['+cdc_block_index+']');
        html.find(".cdc_exp").attr('name','cdc_exp['+cdc_block_index+']');
        html.find(".cdc-status-tab").addClass('hide');
        html.find(".cdc_ver").attr('name','cdc_ver['+cdc_block_index+']');
        

        html.find(".cdc-close-button").addClass('cdc-close-button-'+sea_service_block_index).removeClass('cdc-close-button-0').removeClass('hide');
        html.find(".cdc-close-button").attr('id','cdc-close-button-'+sea_service_block_index);

        $("#add-more-cdc-row").before(html);
        cdc_block_index++;

        $('.issue-datepicker').datepicker({
            format: "dd-mm-yyyy",
            endDate: date,
            autoClose: true,
            startView: 2
        }).on('changeDate', function(ev) {
            if(ev.viewMode === 0){
                $('.issue-datepicker').datepicker('hide');
            }
       });

        $('.datepicker').datepicker({
            format: "dd-mm-yyyy",
            startDate: date,
            autoClose: true,
            startView: 2
        }).on('changeDate', function(ev) {
            if(ev.viewMode === 0){
                $('.datepicker').datepicker('hide');
            }
       });
    });

    
    if(typeof($(".shipping_company").val()) != 'undefined'){
        $(".shipping_company").autocomplete({

            minLength: 2,
            source: function(request, response) {
                $.ajax({
                    url: "/seafarer/company",
                    dataType: "json",
                    data: {
                        term : request.term,
                        user_id : $('#user_id').val(),
                    },
                    success: function(data) {
                        if (data.length > 0) {
                            response(data);
                        }else{
                            $("#name_of_ship").val('');
                            $(".ship_type").val('');
                            $(".grt").val('');
                            $(".bhp").val('');
                            $(".engine_type").val('');
                            $(".ship_flag").val('');

                            $(".ship_type").removeAttr('readonly');
                            $(".grt").removeAttr('readonly');
                            $(".bhp").removeAttr('readonly');
                            $(".engine_type").removeAttr('readonly');
                            $(".ship_flag").removeAttr('readonly');
                        }
                    }
                });
            },
            select: function(event, ui) {
                setTimeout(function(){ 
                    $(".shipping_company").val(ui.item.company_name);
                }, 10);
            }
        }).data( "ui-autocomplete" )._renderItem = function( ul, item ) {
            return $( "<div></div>" )
                .data( "item.autocomplete", item )
                .append('<a>' + item.company_name  +'</a>')
                .appendTo( ul );
        };
    }

    if(typeof($("#name_of_ship").val()) != 'undefined'){
        $("#name_of_ship").autocomplete({
            minLength: 2,
            source: function(request, response) {
                $.ajax({
                    url: "/seafarer/ship/name",
                    dataType: "json",
                    data: {
                        term : request.term,
                        shipping_company : $(".shipping_company").val(),
                        user_id : $('#user_id').val(),
                    },
                    success: function(data) {
                        if (data.length > 0) {
                            response(data);
                        }else{
                            $(".ship_type").val('');
                            $(".grt").val('');
                            $(".bhp").val('');
                            $(".engine_type").val('');
                            $(".ship_flag").val('');
                            $(".other_engine_type").val('');
                            $("#other_engine_type").addClass('hide');
                            
                            $(".grt").removeAttr('readonly');
                            $(".bhp").removeAttr('readonly');
                            $(".engine_type").removeAttr('readonly');
                            $(".other_engine_type").removeAttr('readonly');
                            $(".engine_type").attr('disabled',false);
                            $(".ship_flag").attr('disabled',false);
                            $(".ship_type").attr('disabled',false);
                        }
                    }
                });
            },
            select: function(event, ui) {
                setTimeout(function(){ 
                    $("#name_of_ship").val(ui.item.ship_name);
                    $(".ship_type").val(ui.item.ship_type);
                    $(".grt").val(ui.item.grt);
                    $(".bhp").val(ui.item.bhp);
                    $(".engine_type").val(ui.item.engine_type);
                    $(".ship_flag").val(ui.item.ship_flag);

                    if(ui.item.engine_type == '11'){
                        $("#other_engine_type").removeClass('hide');
                        $(".other_engine_type").val(ui.item.other_engine_type);
                        $(".other_engine_type").attr('readonly',true);
                    }else{
                        $("#other_engine_type").addClass('hide');
                        $(".other_engine_type").val('');
                    }

                    $(".grt").attr('readonly',true);
                    $(".bhp").attr('readonly',true);
                    /*$(".engine_type").attr('readonly',true);*/
                    $(".engine_type").attr('disabled',true);
                    $(".ship_flag").attr('disabled',true);
                    $(".ship_type").attr('disabled',true);
                }, 10);
            }
        }).data( "ui-autocomplete" )._renderItem = function( ul, item ) {
        
            return $( "<div></div>" )
                .data( "item.autocomplete", item )
                .append('<a>' + item.ship_name +'</a>')
                .appendTo( ul );
        };
    }
    
    if(typeof($(".issue_by").val()) != 'undefined'){
        $(".issue_by").autocomplete({

            minLength: 2,
            source: function(request, response) {
                $.ajax({
                    url: "/seafarer/course/issue_by",
                    dataType: "json",
                    data: {
                        term : request.term,
                        user_id : $('#user_id').val(),
                    },
                    success: function(data) {
                        if (data.length > 0) {
                            response(data);
                        }
                    }
                });
            },
            select: function(event, ui) {
                setTimeout(function(){ 
                    $(".issue_by").val(ui.item.issue_by);
                }, 10);
            }
        }).data( "ui-autocomplete" )._renderItem = function( ul, item ) {
            return $( "<div></div>" )
                .data( "item.autocomplete", item )
                .append('<a>' + item.issue_by  +'</a>')
                .appendTo( ul );
        };
    }

    //coe
    $(document).on('click','.add-coe-button',function (e) {

        var form_id = $(this).attr('data-form-id');

        var html = $('#coe-template').clone();

        if($(".total-count-coe").length > 0 && coe_check == false){
            coe_block_index = $(".total-count-coe").length;
            coe_check = true;
        }

        var coe_index = $(".total-count-coe").length + 1;
        html.find(".coe-name").text('COE Details '+coe_index).addClass('coe-name-'+coe_block_index).removeClass('coe-name-0');

        html.find("input[type='text']").val("");
        html.find("textarea").val("");
        html.find("select").val("");

        html.find("label.error").addClass('hide');
        html.find(".coecountry").attr('name','coecountry['+coe_block_index+']');
        html.find(".coeno").attr('name','coeno['+coe_block_index+']');
        html.find(".coe_grade").attr('name','coe_grade['+coe_block_index+']');
        html.find(".coe_exp").attr('name','coe_exp['+coe_block_index+']');

        html.find(".coe-close-button").addClass('coe-close-button-'+sea_service_block_index).removeClass('coe-close-button-0').removeClass('hide');
        html.find(".coe-close-button").attr('id','coe-close-button-'+sea_service_block_index);

        $("#add-more-coe-row").before(html);
        coe_block_index++;

        $('.issue-datepicker').datepicker({
            format: "dd-mm-yyyy",
            endDate: date,
            autoClose: true,
            startView: 2
        }).on('changeDate', function(ev) {
            if(ev.viewMode === 0){
                $('.issue-datepicker').datepicker('hide');
            }
       });

        $('.datepicker').datepicker({
            format: "dd-mm-yyyy",
            startDate: date,
            autoClose: true,
            startView: 2
        }).on('changeDate', function(ev) {
            if(ev.viewMode === 0){
                $('.datepicker').datepicker('hide');
            }
       });
    });
    //coe

    $(document).on('click','.add-coc-button',function (e) {

        var form_id = $(this).attr('data-form-id');

        var html = $('#coc-template').clone();

        if($(".total-count-coc").length > 0 && service_check == false){
            coc_block_index = $(".total-count-coc").length+1;
            service_check = true;
        }

        var coc_index = $(".total-count-coc").length + 1;
        html.find(".coc-name").text('COC Details '+coc_index);

        html.find("input[type='text']").val("");
        html.find("textarea").val("");
        html.find("select").val("");

        html.find("label.error").addClass('hide');

        html.find(".coc_country").attr('name','coc_country['+coc_block_index+']');
        html.find(".coc_no").attr('name','coc_no['+coc_block_index+']');
        html.find(".grade").attr('name','grade['+coc_block_index+']');
        html.find(".coc_exp").attr('name','coc_exp['+coc_block_index+']');
        html.find(".coc_ver").attr('name','coc_ver['+coc_block_index+']');

        html.find(".coc-close-button").addClass('coc-close-button-'+coc_block_index).removeClass('coc-close-button-0').removeClass('hide');
        html.find(".coc-close-button").attr('id','coc-close-button-'+coc_block_index);
        html.find(".coc-status-tab").addClass('hide');

        $("#add-more-coc-row").before(html);
        coc_block_index++;

        $('.issue-datepicker').datepicker({
            format: "dd-mm-yyyy",
            endDate: date,
            autoClose: true,
            startView: 2
        }).on('changeDate', function(ev) {
            if(ev.viewMode === 0){
                $('.issue-datepicker').datepicker('hide');
            }
       });

        $('.datepicker').datepicker({
            format: "dd-mm-yyyy",
            startDate: date,
            autoClose: true,
            startView: 2
        }).on('changeDate', function(ev) {
            if(ev.viewMode === 0){
                $('.datepicker').datepicker('hide');
            }
       });

    });

    $(document).on('click','.add-cop-button',function (e) {

        var form_id = $(this).attr('data-form-id');

        var html = $('#cop-template').clone();

        /*if($(".service_total_block_index").length > 0 && service_check == false){
            sea_service_block_index = parseInt($(".service_total_block_index").last().val())+1;
            service_check = true;
        }*/

        var cop_index = $(".total-count-cop").length + 1;
        html.find(".cop-name").text('COP Details '+cop_index);

        html.find("input[type='text']").val("");
        html.find("textarea").val("");
        html.find("select").val("");

        html.find("label.error").addClass('hide');

        html.find(".cop_no").attr('name','cop_no['+cop_block_index+']');
        html.find(".cop_grade").attr('name','cop_grade['+cop_block_index+']');
        html.find(".cop_issue").attr('name','cop_issue['+cop_block_index+']');
        html.find(".cop_exp").attr('name','cop_exp['+cop_block_index+']');

        html.find(".cop-close-button").addClass('cop-close-button-'+cop_block_index).removeClass('cop-close-button-0').removeClass('hide');
        html.find(".cop-close-button").attr('id','cop-close-button-'+cop_block_index);

        $("#add-more-cop-row").before(html);
        cop_block_index++;

        $('.issue-datepicker').datepicker({
            format: "dd-mm-yyyy",
            endDate: date,
            autoClose: true,
            startView: 2
        }).on('changeDate', function(ev) {
            if(ev.viewMode === 0){
                $('.issue-datepicker').datepicker('hide');
            }
       });

        $('.datepicker').datepicker({
            format: "dd-mm-yyyy",
            startDate: date,
            autoClose: true,
            startView: 2
        }).on('changeDate', function(ev) {
            if(ev.viewMode === 0){
                $('.datepicker').datepicker('hide');
            }
       });

    });

    $(document).on('click','.close-button',function (e) {
        var type= $(this).attr('data-type');
        $(this).closest('#'+type+'-template').remove();

        $("."+type+"-name").each(function(index,element){
            var index = index + 1;
            type = type.toUpperCase();
            $(this).text(type+' Details '+index);
        });
    });


    $(".edit_link").on('click',function(e){
        window.location.href = $(this).attr('href')+'#'+$(this).attr('data-form');
    });

   $(window).load(function() {
        var x = location.hash;
        if( x != ''){
            var res = x.split("#");
            if(res[1] == 'documents'){
                $(".basic-details-tab").addClass('page-done page-active');
                $(".imp-document-tab").removeClass('page-done page-active');
                $("#documents-details-form").show();
                $("#basic-details-form").hide();
                $("#documents-details-form").removeClass('hide');
            }
            if(res[1] == 'courses'){
                $(".basic-details-tab").addClass('page-done page-active');
                $(".certificate-document-tab").removeClass('page-done page-active');
                $("#basic-details-form").hide();
                $("#seafarer-certificates-form").removeClass('hide');
            }
            if(res[1] == 'sea_service'){
                $(".certificate-document-tab").addClass('page-done page-active');
                $(".sea-service-document-tab").removeClass('page-done page-active');
                $(".basic-details-tab").addClass('page-done page-active');
                $("#basic-details-form").hide();
                $("#seafarer-sea-service-form").removeClass('hide');
                $("#documents-details-form").addClass('hide');
            }
            if(res[1] == 'user_document_request'){
                $(".user-tabs li").removeClass('active');
                $('.user-tabs a[href="#user_document"]').closest('li').addClass('active');
                $('.tab-content .tab-pane').removeClass('active');
                $('.tab-content #user_document').addClass('active in');


                $(".user-tabs-document li").removeClass('active');
                $('.user-tabs-document a[href="#request"]').closest('li').addClass('active');
                $('#user_document .tab-content .tab-pane').removeClass('active');
                $('#user_document .tab-content #request').addClass('active in');
            }
            if(res[1] == 'user_document'){
                $(".basic-details-tab").addClass('page-done page-active');
                $(".upload-document-tab").removeClass('page-done page-active');
                $("#basic-details-form").hide();
                $("#upload_documents").removeClass('hide');

                getUserDocuments();
            }
        }
   });

   $(".course_checkbox").on('click', function(e){
        
        var id = $(this).attr('data-id');
        var check = $(".date_of_issue_"+id).attr('data-check');
        if(check == 'off'){
            $(".date_of_issue_"+id).removeClass('hide').attr('data-check','on');
        }else{
             $(".date_of_issue_"+id).addClass('hide').attr('data-check','off');
        }
   });

    $(".value_added_course_checkbox").on('click', function(e){
        
        var id = $(this).attr('data-id');
        var check = $(".value_added_date_of_issue_"+id).attr('data-check');
        if(check == 'off'){
            $(".value_added_date_of_issue_"+id).removeClass('hide').attr('data-check','on');
        }else{
             $(".value_added_date_of_issue_"+id).addClass('hide').attr('data-check','off');
        }
   });

   /* $(window).load(function(e){
        if($("#verify_email").length > 0){
            if($("#verify_email").val() == '0'){
                $(".alert-box-verification-email").removeClass('hide');
            }
        }
        if($("#verify_mobile").length > 0){
            if($("#verify_mobile").val() == '0'){
                $(".alert-box-verification-mob").removeClass('hide');
            }
        }
    });*/

    /*$('#email_verify').on('click', function(){
        $.ajax({
             type: "POST",
             url: $("#api-resend-email-route").val(),
             data: {
                 '_token' : $("input[name='_token']").val()
             },
             statusCode: {
                 200:function (response) {
                    $('#otp_resend_success').removeClass('hide');
                    $("#resend-otp-button").addClass('disabled');
                     $('#invalid_mob_error').addClass('hide');
                 },
                 400:function (response) {
                     $('#mob_error_resend_otp').removeClass('hide');
                 }
             }
         });
    });*/

    /*$(".seafarer_subscription").on('click',function(e){
        e.preventDefault();
        var id = $("#seafarer_subscription").data('id');
        var seafarer_id = $("#seafarer_subscription").data('seafarer');
        var role = $("#seafarer_subscription").data('role');
        var url = $("#api-admin-get-user-subscription").val();

        $(".full-cnt-loader").removeClass('hidden');

        $.ajax({
            type: "GET",
            url: url,
            data: {
                id: id,
                role: role,
                seafarer_id: seafarer_id,
            },
            statusCode: {
                200:function (response) {
                    $(".full-cnt-loader").addClass('hidden');
                    $(".data").html(response.template);
                },
                400:function (response) {
                    $(".data").text(response.message);
                    $(".full-cnt-loader").addClass('hidden');
                }
            }
        });
        
    });*/

    $('#myTab2 a').not(".fa-edit, .fa-download").click(function (e) {
        e.preventDefault();

        var url = $(this).attr("data-url");
        var href = this.hash;
        var pane = $(this);
        
        // ajax load from data-url
        $(href).load(url,function(result){
            pane.tab('show');
        });
    });

    $(document).on('click','.pagination li a',function (e) {

        var url = $(this).attr("href");

        var id = $("#seafarer_subscription").data('id');
        var seafarer_id = $("#seafarer_subscription").data('seafarer');
        var role = $("#seafarer_subscription").data('role');
        url = url+'&seafarer_id='+seafarer_id+'&role='+role+'&id='+id;

        e.preventDefault();

        var href = "#seafarer_subscription";
        var pane = $(this);

        // ajax load from data-url

        $(href).load(url,function(result){
            $("#seafarer_subscription").tab('show');
        });

    });

    $(document).on('click','.view-sub-modal',function (e) {
        var modal_id = $(this).data('modal-id');
        $(".sub-"+modal_id).modal('show');
    });

    $('.resend-otp-button').on('click', function(){
        //$("#resend_otp_Modal").modal();
        var l = Ladda.create(this);
        l.start();
        $.ajax({
             type: "POST",
             url: $("#api-resend-otp-route").val(),
             data: {
                 '_token' : $("input[name='_token']").val()
             },
             statusCode: {
                 200:function (response) {
                    l.stop();
                    $('.otp_resend_success').removeClass('hide');
                    $(".resend-otp-button").addClass('disabled');
                    $('.invalid_mob_error').addClass('hide');
                 },
                 400:function (response) {
                    l.stop();
                    $('.mob_error_resend_otp').removeClass('hide');
                 }
             }
        });
    });

    $('#first-step-mob-verification').validate({

        rules:{
            mob_otp: {
                required: true,
                minlength: 6,
                maxlength: 6,
                number: true
            }
        }
    });

    $(".mob_otp_submit_button").on('click',function(e){
        e.preventDefault();
        var l = Ladda.create(this);
        if($("#first-step-mob-verification").valid()){
            l.start();
            $.ajax({
                type: "POST",
                url: $("#first-step-mob-verification").attr('action'),
                data: $("#first-step-mob-verification").serialize(),
                statusCode: {
                    200:function (response) {
                        l.stop();
                        toastr.success(response.message, 'Success');
                        setTimeout(function () {
                            $("#firstStepModal").modal('hide');
                            $(".basic-details-tab").addClass('page-done page-active');
                            $(".imp-document-tab").removeClass('page-done page-active');
                            $("#documents-details-form").show();
                            $("#basic-details-form").hide();
                            $("#documents-details-form").removeClass('hide');

                            $("#tab-2").addClass('page-active page-done').removeClass('disabled');

                            if($("#tab-1").hasClass('edit-details-tab')){
                                $("#tab-2").removeClass('page-done page-active');
                                $("#tab-1").addClass('page-done page-active');
                            }
                        }, 1000);
                    },
                    400:function (response) {
                        l.stop();
                        if(response.responseJSON.status == 'error'){
                            $('.otp_resend_success').addClass('hide');
                            $('.invalid_mob_error').addClass('hide');
                            $('.otp_failure').removeClass('hide');
                            $('.otp_failure').text(response.responseJSON.message);
                        }else{
                            $('.otp_resend_success').addClass('hide');
                            $('.invalid_mob_error').removeClass('hide');
                        }
                    }
                }
            });
        }
    });

    $(document).on('click','.resume-download',function (e) {
        e.preventDefault();
        var el = $(this);
        var feature_type = $(this).data('type');
        var extra_data = [];
        extra_data['seafarer_id'] = $(this).attr('seafarer-index');
    
        var data = [];
        var l = Ladda.create(this);

        data['feature'] = feature_type;
        data['seafarer_id'] = extra_data['seafarer_id'];
        var url =$("#api-site-seafarer-download-cv").val();

        l.start();
        $.ajax({
            url: url,
            type: "post",
            data: {
                data : data
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            },
            statusCode: {
                200:function (response) {
                    l.stop();
                    window.open(response.file_path,'_blank');
                },
                400:function (response) {
                   l.stop();
                   toastr.error(response.responseJSON.message, 'Error');
                },
                422:function (response) {
                   l.stop();
                   toastr.error(response.responseJSON.message, 'Error');
                }
            }
        });
    });

    function getUserDocuments(){
        
        var url = $("#api-seafarer-get-document-list").val();
        var view_profile = false;
        var profile_status = '';
        var user_id = '';
        //user_id = $("#user_id").val();
        if(typeof($('.profile_status').val()) != 'undefined'){
			
            if($('.profile_status').val() == 'view profile'){
                view_profile = true;
                profile_status = 'view_profile';
                
            }
			if($('.profile_status').val() == 'own profile'){
                view_profile = true;
                profile_status = 'view_profile';
               
            }
			
			
        }
        //user_id = $(".user_id").val();
        $.ajax({
            url: url,
            type: "get",
            data : {
                profile_status : profile_status,
                user_id : user_id,
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            },
            statusCode: {
                200:function (response) {
                    $(".documents_section").empty();
                    $(".documents_section").append(response.template);

                    $('.document_slide').slick({
                        arrows: true,
                        autoplay: false,
                        centerMode: false,
                        centerPadding: '0',
                        slidesToShow: 7,
                        slidesToScroll: 1,
                        dots: false,
                        infinite: false,
                        lazyLoad: 'ondemand',
                        prevArrow: '<span class="navArrow_prev navArrow left_arrow"><i class="fa fa-chevron-left"></i></span>',
                        nextArrow: '<span class="pull-right navArrow_next navArrow right_arrow"><i class="fa fa-chevron-right"></i></span>',
                        responsive: [
                            {
                                breakpoint: 1200,
                                settings: {
                                    slidesToShow: 5,
                                    slidesToScroll: 1,
                                    centerMode: false,
                                    centerPadding: '0',
                                    dots: false
                                }
                            },
                            {
                                breakpoint: 1024,
                                settings: {
                                    slidesToShow: 3,
                                    slidesToScroll: 1,
                                    centerMode: false,
                                    centerPadding: '0',
                                    dots: false
                                }
                            },
                            {
                                breakpoint: 600,
                                settings: {
                                    initialSlide: 0,
                                    slidesToShow: 2,
                                    slidesToScroll: 1,
                                    centerMode: true,
                                    centerPadding: '0',
                                }
                            },
                            {
                                breakpoint: 450,
                                settings: {
                                    initialSlide: 1,
                                    slidesToShow: 1,
                                    slidesToScroll: 1,
                                    centerMode: true,
                                    centerPadding: '0',
                                }
                            }
                        ]
                    });

                    /*$('.document_slide').on('setPosition', function () {
                        $(this).find('.slick-slide').height('auto');
                        var slickTrack = $(this).find('.slick-track');
                        var slickTrackHeight = $(slickTrack).height();
                        $(this).find('.slick-slide').css('height', slickTrackHeight + 'px');
                    });*/

                },
                400:function (response) {
                   //toastr.error(response.responseJSON.message, 'Error');
                },
                422:function (response) {
                   //toastr.error(response.responseJSON.message, 'Error');
                }
            }
        });
    }

    $(document).on('click','.documents-tab',function() {
        $(".doc-section").addClass('hide');
        $(".documents-tab").removeClass('active');
        $(this).addClass('active');

        var block = $(this).data('block');

        $("."+block).removeClass('hide');

        if(block == 'passport-block'){
            if($(".visa-block").length > 0){
                $(".visa-block").removeClass('hide');
            }
        }

        if(block == 'coc-block'){
            if($(".coe-block").length > 0){
                $(".coe-block").removeClass('hide');
            }
        }

        if(block == 'gmdss-block'){
            if($(".wk-block").length > 0){
                $(".wk-block").removeClass('hide');
            }
        }

        if(block == 'sea service-block'){
            if($(".service-block").length > 0){
                $(".service-block").removeClass('hide');
            }
        }

        if(block == 'yellow fever-block'){
            if($(".fever-block").length > 0){
                $(".fever-block").removeClass('hide');
            }
        }

    });

    $(document).on('click','.doc-permission-radio-btn',function() {

        var type_id = 0;
        var type = $(this).data('name');
        var value = $(this).val();
        if(typeof($(this).data('type-id')) != 'undefined'){
            type_id = $(this).data('type-id');
        }

        var url = $("#api-seafarer-change-document-status").val();

        $.ajax({
            url: url,
            type: "get",
            data:{
                type:type,
                type_id:type_id,
                value:value,
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            },
            statusCode: {
                200:function (response) {
                    toastr.success(response.message, 'Success');
                },
                400:function (response) {
                    toastr.error(response.responseJSON.message, 'Error');
                },
                422:function (response) {
                    $(this.name).filter('[value="0"]').attr('checked', true);
                    toastr.error(response.responseJSON.message, 'Error');
                }
            }
        });
    });

    $(document).on('change','.checkbox_list .main_list .con_parent_document_category',function() {
        var class_checkbox = $(this);
        var child_class = class_checkbox.closest('.main_list').find('.main_sub_list .con_child_document_category');
        if(this.checked){
            child_class.each(function(){
                $(this).prop('checked',true);
            });
        }else{
            child_class.each(function(){
                $(this).prop('checked',false);
            });
        }
    });

    $(document).on('change','.checkbox_list .main_list .con_child_document_category',function() {
        var class_child_checkbox = $(this);
        var closet_list = class_child_checkbox.closest('.main_list');
        var parent_class = closet_list.find('.main_list_input .con_parent_document_category');

        var all_checkboxes_checked = true;
        closet_list.find('.con_child_document_category').each(function(){
            if (!$(this).is(':checked')){
                all_checkboxes_checked = false;
            }
        });

        if(all_checkboxes_checked == true){
            parent_class.prop('checked',true);
        }
        else{
            parent_class.prop('checked',false);
        }

    });
    
    $(document).on('click','#request_button',function () {
        var l = Ladda.create(this);
        getCheckboxValue(l);
    });

    function getCheckboxValue(l) {
        var user_document_id  = [];
        var checkbox_onwer_id;

        $(".get_value:checked").each(function() {
            var checkbox_value = $(this).val();
            checkbox_onwer_id = $(this).attr('data-onwer-id');
            if(checkbox_value != ''){
                user_document_id.push(checkbox_value);
            }
        });

        var url = $("#api-seafarer-store-requested-document-list").val();

        l.start();
        $.ajax( {
            url:url,
            type:'POST',
            data: {'user_document_id' : user_document_id , 'owner_id' : checkbox_onwer_id},
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            },
            statusCode: {
                200:function (response) {
                    l.stop();
                    toastr.success(response.message, 'Success');
                    $('#download_document_list').modal('hide');
                },
                400:function (response) {
                    l.stop();
                    toastr.error(response.responseJSON.message, 'Error');
                }
            }
        });
    }

    $(document).on('click','.dropzone-document-details-submit',function () { 

        var permissions  = [];
        var l = Ladda.create(this);

        $(".permission_checkbox input[type='radio'][value='1']:checked").each(function() {
            var type_id = 0;
            var checkbox_value = $(this).data('name');

            if($(this).data('type-id')){
                var type_id = $(this).data('type-id');
            }

            permissions.push({
                type: checkbox_value, 
                type_id: type_id
            });
        });

        var url = $("#api-seafarer-store-permissions").val();

        l.start();

        $.ajax( {
            url:url,
            type:'POST',
            data: {
                'permissions' : permissions 
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            },
            statusCode: {
                200:function (response) {
                    l.stop();
                    toastr.success(response.message, 'Success');

                    if(response.update_mobile == 0){
                        $("#mob-verification").removeClass('hide');
                        $('#myModal').modal('show');
                    }
                    else{
                        if($("#current-route-name").val() == 'site.seafarer.registration'){
                            window.location.href = $("#api-site-subscription").val();
                        }else{
                            window.location.href = $("#api-seafarer-profile-route").val();
                        }
                    }
                },
                400:function (response) {
                    l.stop();
                    toastr.error(response.responseJSON.message, 'Error');
                }
            }
        });

    });

    $(document).on('click','.permission_status',function () {
        var button_value = $(this).data('value');
        var document_status;
        if(button_value == 'Accept'){
            document_status = '1';
        }
        else{
            document_status = '2';
        }

        var document_permission_id = $(this).data('document-permission-id');
        var requester_id = $(this).data('requester-id');
        var owner_id = $(this).data('owner-id');
        var url = $("#api-seafarer-requested-document-status").val();

        var l = Ladda.create(this);
        l.start();

        $.ajax( {
            url:url,
            type:'POST',
            data: {'document_permission_id' : document_permission_id ,'requester_id' : requester_id ,'owner_id' : owner_id,'status' : document_status},
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            },
            statusCode: {
                200:function (response) {
                    l.stop();
                    toastr.success(response.message, 'Success');
                    $('.status-'+document_permission_id).html(response.status_value);
                    $('.button-status-'+document_permission_id).addClass('hide');
                },
                400:function (response) {
                    l.stop();
                    toastr.error(response.responseJSON.message, 'Error');
                }
            }
        });
    });

    $(".user-tabs a").click(function(){
		
        if($(this).attr('href') == "#user_document"){
            getUserDocuments();
        }
    });

    $(".course_list_item a").click(function(){
		
        if($(this).attr('href') == "#user_document"){
            getUserDocuments();
        }
    });

    $(".user-tabs-document a").click(function(){
        if($(this).attr('href') == "#upload_documents"){
            getUserDocuments();
        }
    });

    $(document).on('click','.dz-image-preview',function () {
        var path = $(this).data('img-download');
        console.log('123',path);
        window.open(path,'_blank');
    });
});
//# sourceMappingURL=registration.js.map
