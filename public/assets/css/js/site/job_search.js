$("#seafarer-job-search-button1").on('click', function(e){

    e.preventDefault();
    var l = Ladda.create(this);
    l.start();

    $.ajax({
        url: $("#seafarer-job-search-filter").attr('action'),
        data: $("#seafarer-job-search-filter").serialize(),
        type: "GET",
        statusCode: {
            200:function (response) {
                $(".card-container").empty();
                $(".card-container").append(response.template);
                if(response.template == '')
                    $(".card-container").append('<div class="row no-results-found">No results found. Try again with different search criteria.</div>');

                l.stop();
            },
            400:function (response) {
                
                l.stop();
            }
        }
    });
});


$("#seafarer-course-search-button").on('click', function(e){
    e.preventDefault();
    var l = Ladda.create(this);
    l.start();

    $.ajax({
        url: $("#seafarer-course-search-filter").attr('action'),
        data: $("#seafarer-course-search-filter").serialize(),
        type: "GET",
        statusCode: {
            200:function (response) {
                $(".card-container").empty();
                $(".card-container").append(response.template);
                if(response.template == '')
                    $(".card-container").append('<div class="row no-results-found">No results found. Try again with different search criteria.</div>');

                l.stop();

                if($(".batches").length > 0){
                    $(".batches").removeClass("dont-break");

                    $('.batches').slick({
                        arrows: true,
                        autoplay: false,
                        centerMode: false,
                        slidesToShow: 4,
                        slidesToScroll: 1,
                        dots: false,
                        infinite: false,
                        lazyLoad: 'ondemand',
                        prevArrow: '<span class="navArrow_prev navArrow" style="left: 0;position: absolute;top: 50%;transform: translateY(-6px);"><i class="fa fa-chevron-left"></i></span>',
                        nextArrow: '<span class="navArrow_next navArrow" style="right: 0;position: absolute;top: 50%;transform: translateY(-6px);"><i class="fa fa-chevron-right"></i></span>',
                        responsive: [
                            {
                                breakpoint: 1024,
                                settings: {
                                    slidesToShow: 2,
                                    slidesToScroll: 1,
                                    dots: false
                                }
                            },
                            {
                                breakpoint: 600,
                                settings: {
                                    initialSlide: 1,
                                    slidesToShow: 1,
                                    slidesToScroll: 1,
                                    centerMode: true
                                }
                            }
                        ]
                    });
                }

            },
            400:function (response) {
                
                l.stop();
            }
        }
    });
});

$(document).on('click','ul.pagination li',function(e){
    e.preventDefault();
    var url = $(this).find('a').attr('href')+'&'+$("#seafarer-job-search-filter").serialize();
    fetch_result_ajax(url);
});

function fetch_result_ajax(url){
    $.ajax({
        url: url,
        type: "POST",
        statusCode: {
            200:function (response) {
                $(".card-container").empty();
                $(".card-container").append(response.template);
                if(response.template == '')
                    $(".card-container").append('<div class="row no-results-found">No results found. Try again with different search criteria.</div>');
            },
            400:function (response) {
                
            }
        }
    });
}

$(".sort-by-relavance").on('click',function(){
    $(".sort-by-relavance").addClass('sort-active');
    $(".sort-by-date").removeClass('sort-active');
});

$(".sort-by-date").on('click',function(){
    $(".sort-by-relavance").removeClass('sort-active');
    $(".sort-by-date").addClass('sort-active');
});

$( document ).ready(function() {
    if(typeof($('#auto_apply').val()) != 'undefined' && $('#auto_apply').val() != ''){
        $('.job-apply-button').trigger('click');
    }
});

$(document).on('click','.job-apply-button',function(e){
    e.preventDefault();
    var l = Ladda.create(this);
    l.start();

    var auto_apply = '';
    if(typeof($('#auto_apply').val()) != 'undefined' && $('#auto_apply').val() != ''){
        auto_apply = $('#auto_apply').val();
    }

    if($('#job_id').length > 0){
        var job_id = $('#job_id').val();
    }
    if($('#user_id').length > 0){
        var user_id = $('#user_id').val();
    }
    if($('#company_id').length > 0){
        var company_id = $('#company_id').val();
    }

    if(auto_apply == ''){
        var job_id = $(this).attr('data-form-id');
        var company_id = $(this).attr('data-company-id');
    }
    
    var url = $("#api-seafarer-job-apply-route").val();
    var current_url = window.location.href;
    current_url = current_url.replace(/[&]/g,'|');

    $.ajax({
        url: url,
        type: "POST",
        data: {
            'company_id' : company_id,
            'job_id' : job_id,
            '_token' : $("input[name='_token']").val()
        },
        statusCode: {
            200:function (response) {
                
                $("#job-apply-modal").modal('show');
                $("#job-apply-button-"+job_id).hide();
                $(".apply-after-15-"+job_id).removeClass('hide');
                $("#applied-for-job-"+job_id).removeClass('hide');
                l.stop();
            },
            400:function (response) {
                l.stop();

                toastr.error(response.responseJSON['message'], 'Error');
                if(response.responseJSON['redirect'].length > 0){
                   
                    setTimeout(function(){
                        window.location.href = response.responseJSON['redirect']+'?redirect_url='+current_url;
                    },500);
                }
            }
        }
    });
});
//# sourceMappingURL=job_search.js.map
