$(document).ready(function() {

    var _URL = window.URL || window.webkitURL;

    $("#advertise-file-uplaod").change(function(e) {
        e.preventDefault();
        var file, img;
        var ext = $("#advertise-file-uplaod").val().split('.').pop().toLowerCase();
        var size = this.files[0].size / 1024;

        if ((file = this.files[0])) {
            img = new Image();
            img.src = _URL.createObjectURL(file);
            img.onload = function() {
                if(size < 250 && this.width >= '400' && this.height >= '400' && size < '250'){
                    $("#advertise_upload_error").text('');
                }else{
                    setTimeout(function(){ 
                        $(".advertise_upload #advertise_upload_error").show();
                        $(".advertise_upload #advertise_upload_error").text('Please upload 400*400 size image. Image size should be less than 250kb.');
                        $(".advertise_upload #advertise-file-uplaod").val('');
                        $('.advertise_upload .fileupload-new img').attr('src','/images/no-image-advertisements.png');
                        $('#advertise_img img').attr('src','/images/no-image-advertisements.png');
                    }, 100);
                }
            };
            img.onerror = function() {
                $(".advertise_upload #advertise-file-uplaod").val('');
                $('.advertise_upload .fileupload-new img').attr('src','/images/no-image-advertisements.png');
                $('#advertise_img').attr('src','/images/no-image-advertisements.png');
                $('.advertise_upload .fileupload-preview.fileupload-exists.thumbnail').empty();
                $('.advertise_upload .fileupload-preview.fileupload-exists.thumbnail').append('<img src="/images/no-image-advertisements.png" alt="">');
                $(".advertise_upload #advertise_upload_error").text('Please upload image file.');
                
            };
        }
    });

    $("#advertise-full-image-upload").change(function(e) {
        e.preventDefault();
        var file, img;
        var ext = $("#advertise-file-uplaod").val().split('.').pop().toLowerCase();
        var size = this.files[0].size / 1024;
        
        if ((file = this.files[0])) {
            img = new Image();
            img.src = _URL.createObjectURL(file);
            img.onload = function() {
                if(size < 1024){
                    $("#advertise_full_img_upload_error").text('');
                }else{
                    console.log(1111);
                    setTimeout(function(){ 
                        $("#advertise_full_img_upload_error").show();
                        $("#advertise_full_img_upload_error").text('Image size should be less than 1MB.');
                        $(".advertise_full_img_upload #advertise_full_img").val('');
                        $('.advertise_full_img_upload .fileupload-new img').attr('src','/images/no-image-advertisements.png');
                        $('#advertise_full_img img').attr('src','/images/no-image-advertisements.png');
                    }, 100);
                    console.log(222);
                }
            };
            img.onerror = function() {
                $(".advertise_full_img_upload #advertise_full_img").val('');
                $('.advertise_full_img_upload .fileupload-new img').attr('src','/images/no-image-advertisements.png');
                $('#advertise_full_img').attr('src','/images/no-image-advertisements.png');
                $('.advertise_full_img_upload .fileupload-preview.fileupload-exists.thumbnail').empty();
                $('.advertise_full_img_upload .fileupload-preview.fileupload-exists.thumbnail').append('<img src="/images/no-image-advertisements.png" alt="">');
                $("#advertise_full_img_upload_error").text('Please upload image file.');
                
            };
        }
    });

    $(".remove_advertise_btn").on('click', function(e){
        $(".advertise_upload #advertise_upload_error").show();
        $("#advertise_upload_error").text('');
        $("#advertise-file-uplaod").val('');
        $('.fileupload-new img').attr('src','/images/no-image-advertisements.png');
        $('.fileupload-preview img').attr('src','/images/no-image-advertisements.png');
    });

    $(".remove_full_advertise_btn").on('click', function(e){
        $("#advertise_full_img_upload_error").show();
        $("#advertise_full_img_upload_error").text('');
        $(".advertise_full_img_upload #advertise_full_img").val('');
        $('.advertise_full_img_upload .fileupload-new img').attr('src','/images/no-image-advertisements.png');
        $('#advertise_full_img img').attr('src','/images/no-image-advertisements.png');
    });

	$("#submitAdvertiserDetailButton").on('click',function(e){
		e.preventDefault();
        var l = Ladda.create(this);
        var login_data = $('#advertise-login-form').serialize();
        var company_data = $('#advertise-company-form').serialize();

        var url = $("#api-store-advertiser-form-route").val();
        if($("#role").val() == 'admin'){
            url = $("#api-admin-store-advertiser-form-route").val();
        }
        $('#advertise-login-form').valid();

        if($('#advertise-login-form').valid() && $('#advertise-company-form').valid()){
            l.start();
			$.ajax({
                type: "POST",
                url: url,
                headers: {
		            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
		        },
                data: {
                	login_data: login_data,
                	company_data: company_data,
                },
                statusCode: {
                    200:function (response) {
                        window.scrollTo(0,0);
                        window.location.href = response.redirect_url;
                        l.stop();
                    },
                    400:function (response) {
                        l.stop();

                    },
                    422:function (response) {
                        for (var prop in response.responseJSON){
                            if(prop == "mobile"){
                                $("#mobile-error").removeClass('hide');
                                $("#mobile-error").text(response.responseJSON['mobile'][0]);
                                $("#mobile").focus();
                            }
                            if(prop == "email"){
                                $("#email-error").removeClass('hide');
                                $("#email-error").text(response.responseJSON['email'][0]);
                                $("#email").focus();
                            }
                        }
                        l.stop();

                    }
                }
            });
        }
	});

	$('#advertise-login-form').validate({
        focusInvalid: false,
        invalidHandler: function(form, validator) {

            if (!validator.numberOfInvalids())
                return;

            $('html, body').animate({
                scrollTop: $(validator.errorList[0].element).offset().top + (-200)
            }, 200);

        },
		rules: {
            firstname: {
                number: false,
                required: true
            },
            mobile: {
                required: true,
                number: true,
                minlength: 10,
                remote: {
                    url: $("#api-check-mobile").val(),
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content'),
                    },
                    type: "post",
                    async: false,
                    data: {
                        id: function(){ return $("#advertise_id").val()},
                        mobile: function(){ return $("#mobile").val(); }
                    }
                }
            },
            email: {
                required: true,
                email: true,
                remote: {
                    url: $("#api-check-email").val(),
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content'),
                    },
                    type: "post",
                    async: false,
                    data: {
                        id: function(){ return $("#advertise_id").val() },
                        email: function(){ return $("#email").val(); }
                    }
                }
            },
            password: {
                minlength: 6,
                required: true
            },
            cpassword: {
                required: true,
                minlength: 6,
                equalTo: "#password"
            },
        },
        messages: {
            email: {
                required: "We need your email address to contact you",
                email: "Your email address must be in the format of name@domain.com",
                remote: "Email already exist"
            },
            mobile: {
                number: "Please enter valid Mobile number",
                remote: "Mobile already exist"
            }
        },
	});

    jQuery.validator.addMethod("complete_url", function(val, elem) {
        if (val.length == 0) { return true; }
        return /^(www\.)[A-Za-z0-9_-]+\.+[A-Za-z0-9.\/%&=\?_:;-]+$/.test(val);
    },"Enter valid URL");

	$('#advertise-company-form').validate({
        focusInvalid: false,
        invalidHandler: function(form, validator) {

            if (!validator.numberOfInvalids())
                return;

            $('html, body').animate({
                scrollTop: $(validator.errorList[0].element).offset().top + (-200)
            }, 200);

        },
		rules: {
			company_name: {
				required: true
			},
			company_address: {
				required: true
			},
			bussiness_category: {
				required: true
			},
			bussiness_description: {
				required: true
			},
            'country[0]': {
                required: true
            },
            'pincode[0]': {
                required: true
            },
            'state[0]': {
                required: true
            },
            'state_name[0]': {
                required: true
            },
            'state_text[0]': {
                required: true
            },
            'city[0]': {
                required: true
            },
            'city_text[0]': {
                required: true
            },
            website: {
                complete_url: true
            },
            company_contact_number:{
                number: true,
                minlength: 10
            },
            contact_display:{
                required: true,
                number: true,
                minlength: 10
            },
            product_description: {
                required: true
            },
            email_display: {
                required: true,
                email: true
            },
            buss_years: {
                required: true,
                number: true,
                maxlength: 2
            },
            product_description: {
                required: true
            },
            agree: {
                required: true
            },
            other_buss_category: {
                required: true
            },
            other_b_i_member: {
                required: true
            },
            b_i_member: {
                required: true
            },
        },
        messages: {
        	agree: {
                required: "Please agree to the terms and conditions."
            },
        }
		
	});

    $("#submitAdvertiseDetailButton").on('click',function(e){
        e.preventDefault();
        var l = Ladda.create(this);

        $("[name^='state']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select state",
                }
            });
        });

        $("[name^='city']").each(function(){
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please select city",
                }
            });
        });

        if($('#add-advertise-form').valid()){ 
            
            var data = new FormData();
            var other_fields = $("#add-advertise-form").serializeArray();
            
            var cities = {};
            var cities_count = 0;
            $.each(other_fields, function(key,input){
                data.append(input.name,input.value);
            });
            var form_selected_states = [];
            $.each($("#add-advertise-form select[name^='state']"), function(index, ele){
                var state_index = $(this).data('index');
                var state_id = $(this).val();
                form_selected_states[index] = {};
                form_selected_states[index]['id'] = state_id;
                form_selected_states[index]['cities'] = {};
                $.each( $('select[name="city['+state_index+']"]').val(), function(value_index, value){
                    form_selected_states[index]['cities'][value_index] = value; 
                });
            });

            if( form_selected_states.length > 0 ) {
                data.append('states', JSON.stringify(form_selected_states));
            }
            $("input[type='file']").each(function(index,element){
                console.log(index,element);
                data.append('img_obj'+[index],$(".fileupload-exists input[type='file']")[0].files[0]);
            });

            l.start();

            var el = $(this);
            var feature_type = $(this).data('type');
            var extra_data = [];

            if((typeof($("#role").val()) != 'undefined') && ($("#role").val() == 'admin')){

                $.ajax({
                    type: "POST",
                    url: $("#add-advertise-form").attr('action'),
                    data: data,
                    contentType : false,
                    processData: false,
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    },
                    statusCode: {
                        200:function (response) {
                            window.location.href = response.redirect_url;
                            window.scrollTo(0,0);
                            l.stop();
                        },
                        400:function (response) {
                            l.stop();
                            toastr.error('Some error occured, try again later', 'Error');

                        },
                        422:function (response) {
                            l.stop();
                           toastr.error(response.responseJSON.message, 'Error');

                        }
                    }
                });
            }else{
                checkSubscriptionFeatureAvaiblity(feature_type,extra_data);
            }

            $('#feature-avaibility-modal .feature-success-modal-btn').click(function(){
                $.ajax({
                    type: "POST",
                    url: $("#add-advertise-form").attr('action'),
                    data: data,
                    contentType : false,
                    processData: false,
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    },
                    statusCode: {
                        200:function (response) {
                            window.location.href = response.redirect_url;
                            window.scrollTo(0,0);
                            l.stop();
                        },
                        400:function (response) {
                            l.stop();
                            toastr.error('Some error occured, try again later', 'Error');

                        },
                        422:function (response) {
                            l.stop();
                           toastr.error(response.responseJSON.message, 'Error');

                        }
                    }
                });
            });
            $('#feature-avaibility-modal .feature-failed-modal-btn').click(function(){
                l.stop();
            });
           $("#feature-avaibility-modal").modal('hide');

        }
    });

    $('#add-advertise-form').validate({
        focusInvalid: false,
        invalidHandler: function(form, validator) {

            if (!validator.numberOfInvalids())
                return;

            $('html, body').animate({
                scrollTop: $(validator.errorList[0].element).offset().top + (-200)
            }, 200);

        },
        rules: {

            advertise_upload: {
                required: true
            },
            full_img: {
                required: true
            },
            adv_from_date: {
                required: true
            },
            adv_to_date: {
                required: true
            },
            state: {
                required: true
            },
            city: {
                required: true
            },
            company_id: {
                required: true
            }
        }
    });

    $(document).on('change','.advertisement-state', function(e){
        e.preventDefault();
        var temp_url = $("#api-admin-state-list").val();
        var url_arr = temp_url.split("%");
        var url = url_arr[0] + parseInt($(this).val());
        var state_index = $(this).data('index');
        $('select[name="city['+state_index+']"]').val('').trigger('change');
        $('select[name="city['+state_index+']"] option').remove();

        $.ajax({
                type: "get",
                url: url,
                statusCode: {
                    200:function (response) {
                        $("#city option").remove();
                        $.each(response,function(index,value){
                            $('select[name="city['+state_index+']"]').append('<option value='+value.id+'>'+value.name+'</option>');
                        }); 
                    },
                    400:function (response) {
                       
                    },
                    422:function (response) {

                    }
                }
            });
    });

    $(document).on('change', '.country', function () {
       var block_index = $(this).attr('block-index');
       if( $(this).val() == india_value ) {
           $(".state-block-"+block_index).find('.fields-for-india').removeClass('hide');
           $(".state-block-"+block_index).find('.fields-not-for-india').addClass('hide');

           $(".city-block-"+block_index).find('.fields-for-india').removeClass('hide');
           $(".city-block-"+block_index).find('.fields-not-for-india').addClass('hide');

           $("[name='pincode["+block_index+"]']").rules("add", {
               maxlength: 6,
               messages: {
                   maxlength: "Please provide valid 6 digit pincode"
               }
           });

           $("[name='pincode["+block_index+"]']").addClass('pin_code_fetch_input');
       } else {
           $(".state-block-"+block_index).find('.fields-for-india').addClass('hide');
           $(".state-block-"+block_index).find('.fields-not-for-india').removeClass('hide');

           $(".city-block-"+block_index).find('.fields-for-india').addClass('hide');
           $(".city-block-"+block_index).find('.fields-not-for-india').removeClass('hide');

           $("[name='pincode["+block_index+"]']").removeClass('pin_code_fetch_input');
           
           $("[name='pincode["+block_index+"]']").rules('remove','maxlength');


       }
    });

    $(document).on('change','.advertisement-state-filter', function(e){
        e.preventDefault();
        var temp_url = $("#api-admin-state-list").val();
        var url_arr = temp_url.split("%");
        var url = url_arr[0] + parseInt($(this).val());
        var state_index = $(this).data('index');
        $('select[name="city['+state_index+']"]').val('').trigger('change');
        $('select[name="city['+state_index+']"] option').remove();

        $.ajax({
                type: "get",
                url: url,
                statusCode: {
                    200:function (response) {
                        $("#city option").remove();
                        $('select[name="city"]').append('<option value=""> Select City </option>');
                        $.each(response,function(index,value){
                            $('select[name="city"]').append('<option value='+value.id+'>'+value.name+'</option>');
                        }); 
                    },
                    400:function (response) {
                       
                    },
                    422:function (response) {

                    }
                }
            });
    });

    var block_index = 1;
    $(document).on('click','.add-state-button',function (e) {
        
        if($('select[name="state[0]"]').val() == ''){
            $(".state_error").removeClass('hide').addClass('error');
        }else{
            $(".state_error").addClass('hide').removeClass('error');
            var form_id = $(this).attr('data-form-id');
            var html = $('#advertise-state-template').clone();
            html.attr("id",'');
            html.removeClass('hide');

            var selected_state = [];
            $.each($("#add-advertise-form select[name^='state']"), function(index, ele){
                if( $(this).val() != '' ) {
                    selected_state[index] = parseInt($(this).val());
                }
            });
            var prev_index = block_index - 1;

            html.find(".state").attr('name','state['+block_index+']').attr('data-index', block_index);
            html.find(".city").attr('name','city['+block_index+']');
            html.find(".adv-close-button").attr('template-index',block_index);
            html.find(".add-state-button").attr('id','add-more-state-'+block_index).attr('template-index',block_index);
            html.find(".city").attr('placeholder','Please select city');
            html.find(".select2-container").css('height','auto');
            html.find(".adv-close-button").addClass('adv-close-button-'+block_index).removeClass('adv-close-button-0');
            html.find(".city").css('height','auto');

            if( selected_state.length > 0) {
                $.each(state, function(index, data){
                    if(  $.inArray(data.id, selected_state) == -1 ) {
                        html.find("select").append('<option value="'+data.id+'">'+data.name+'</option>');
                    }
                });
            }

            $("#add-more-state-"+prev_index).addClass('hide');
            $("#add-more-state-row").before(html);
            $('select[name="city['+block_index+']"]').select2({
                closeOnSelect: false
            });
            $('select[name="city['+block_index+']"]').empty();
            block_index++;
        }
    });

    $(document).on('click','.adv-close-button',function (e) {
        $(this).closest('.adv-template').remove();
        block_index -= 1;
        $("#add-advertise-form .adv-template").last().find('.add-state-button').removeClass('hide');
    });

    $(".change-adv-status").on('click',function(e){
        var el = $(this);
        var status = el.attr('data-status');
        var id = el.attr('data-id');
        var url = $('#api-admin-advertisements-change-status').val();

        $.ajax({
                type: "POST",
                  url: url,
                  headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                  },
                  data: {
                    status: status,
                    id: id,
                  },
                  statusCode: {
                      200:function (response) {

                          el.closest('.dropdown-menu').find('.change-adv-status').removeClass('hide');
                          $('#'+status+'-'+id).addClass('hide');
                          if (status == 'active') {
                            el.closest('.advertisement-card').find('.advertisement-label-container').html('<div class="label label-info">Admin approved</div>');
                          } else if (status == 'deactive') {
                            el.closest('.advertisement-card').find('.advertisement-label-container').html('<div class="label label-danger">Admin Unapproved</div>');
                          }
                        
                        toastr.success(response.message, 'Success');
                      },
                      400:function (response) {
                        toastr.error(response.message, 'Error');
                      }
                  }
              });
        });

    $(document).on('click','.bussiness_category',function (e) {
        if($(this).val() == 'other'){
            $("#other_bussiness_category").removeClass('hide');
        }else{
            $("#other_bussiness_category").addClass('hide');
        }
    });

    $(document).on('click','.b_i_member',function (e) {
        if($(this).val() == 'other'){
            $("#other_b_i_member").removeClass('hide');
        }else{
            $("#other_b_i_member").addClass('hide');
        }
    });

    $('#myTab2 a').not(".fa-edit").click(function (e) {
        e.preventDefault();
        var url = $(this).attr("data-url");
        var href = this.hash;
        var pane = $(this);
        
        // ajax load from data-url
        $(href).load(url,function(result){
            pane.tab('show');
        });
    });

    $(document).on('click','.pagination li a',function (e) {

        var url = $(this).attr("href");

        var id = $("#advertiser_subscription").data('id');
        var advertiser_id = $("#advertiser_subscription").data('advertiser');
        var role = $("#advertiser_subscription").data('role');
        url = url+'&advertiser_id='+advertiser_id+'&role='+role+'&id='+id;

        e.preventDefault();

        var href = "#advertiser_subscription";
        var pane = $(this);

        // ajax load from data-url

        $(href).load(url,function(result){
            $("#advertiser_subscription").tab('show');
        });

    });

    /*$(".advertiser_subscription").on('click',function(e){
        e.preventDefault();
        var id = $("#advertiser_subscription").data('id');
        var advertiser_id = $("#advertiser_subscription").data('advertiser');
        var role = $("#advertiser_subscription").data('role');
        var url = $("#api-admin-get-user-subscription").val();

        console.log(advertiser_id,role,url);
        $(".full-cnt-loader").removeClass('hidden');

        $.ajax({
            type: "GET",
            url: url,
            data: {
                id: id,
                role: role,
                advertiser_id: advertiser_id,
            },
            statusCode: {
                200:function (response) {
                    $(".full-cnt-loader").addClass('hidden');
                    $(".data").html(response.template);
                },
                400:function (response) {
                    $(".data").text(response.message);
                    $(".full-cnt-loader").addClass('hidden');
                }
            }
        });
        
    });*/

    $(document).on('click','.view-sub-modal',function (e) {
        var modal_id = $(this).data('modal-id');
        $(".sub-"+modal_id).modal('show');
    });

    $(document).on('click','.activate-deactivate-advertise',function (e) {
        e.preventDefault();
        var el = $(this);
        el.closest('.advertise-card').find('.advertise-status-loader').removeClass('hide');
        
        var data_id = el.attr('data-id');
        var data_status = el.attr('data-status');

        if (data_status == 'live') {

            var feature_type = el.data('type');
            var extra_data = [];
            var url = advertisement_status_change_url;
            checkSubscriptionFeatureAvaiblity(feature_type,extra_data);
               $('#feature-avaibility-modal .feature-success-modal-btn').click(function(){
                    $.ajax({
                        type: "POST",
                        url: url,
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                        },
                        data: {
                            id: data_id,
                            status: data_status,
                        },
                        statusCode: {
                            200:function (response) {
                                if (data_status == 'active') {
                                    el.attr('data-status','live');
                                    el.find('.activate-deactivate-text').text('Activate');
                                    el.closest('.advertise-card').find('.advertise-label').html('<div class="label label-info">Deactivated</div>');
                                    toastr.success('Advertise deactivated', 'Success');
                                } else if (data_status == 'live') {
                                    el.attr('data-status','active');
                                    el.find('.activate-deactivate-text').text('Deactivate');
                                    el.closest('.advertise-card').find('.advertise-label').html('<div class="label label-success">Active</div>');
                                    toastr.success('Advertise activated', 'Success');
                                }
                                el.closest('.advertise-card').find('.advertise-status-loader').addClass('hide');

                            },
                            400:function (response) {
                                toastr.error('Some error occured, try again later', 'Error');
                                el.closest('.advertise-card').find('.advertise-status-loader').addClass('hide');
                            },
                            422:function (response) {
                                el.closest('.advertise-card').find('.advertise-status-loader').addClass('hide');
                               toastr.error(response.responseJSON.message, 'Error');

                            }
                        }
                    });
                    $("#feature-avaibility-modal").modal('hide');
                });
               $('#feature-avaibility-modal .feature-failed-modal-btn').click(function(){

               });
        } else {
            var url = advertisement_status_degrade_url;
            $.ajax({
                    type: "POST",
                    url: url,
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    },
                    data: {
                        id: data_id,
                        status: data_status,
                    },
                    statusCode: {
                        200:function (response) {
                            if (data_status == 'active') {
                                el.attr('data-status','live');
                                el.find('.activate-deactivate-text').text('Activate');
                                el.closest('.advertise-card').find('.advertise-label').html('<div class="label label-info">Deactivated</div>');
                                toastr.success('Advertise deactivated', 'Success');
                            } else if (data_status == 'live') {
                                el.attr('data-status','active');
                                el.find('.activate-deactivate-text').text('Deactivate');
                                el.closest('.advertise-card').find('.advertise-label').html('<div class="label label-success">Active</div>');
                                toastr.success('Advertise activated', 'Success');
                            }
                            el.closest('.advertise-card').find('.advertise-status-loader').addClass('hide');

                        },
                        400:function (response) {
                            toastr.error('Some error occured, try again later', 'Error');
                            el.closest('.advertise-card').find('.advertise-status-loader').addClass('hide');
                        },
                        422:function (response) {
                            el.closest('.advertise-card').find('.advertise-status-loader').addClass('hide');
                           toastr.error(response.responseJSON.message, 'Error');

                        }
                    }
                });
        }

        el.closest('.advertise-card').find('.advertise-status-loader').addClass('hide');
        $("#feature-avaibility-modal").modal('hide');

    });

    $(".pop").on("click", function() {
       $('#imagepreview').attr('src', $(this).find('img.imageresource').attr('src'));
       $('#imagemodal').modal('show');
    });

});
//# sourceMappingURL=advertise-registration.js.map
