$("#jobResetButton").on('click', function(){
        $(':input','#add-job-form')
            .not(':button, :submit, :reset, :hidden')
            .val('')
            .removeAttr('checked')
            .removeAttr('selected');
    });

$(document).ready(function() {

    $('#add-job-form').validate({
        focusInvalid: false,
        invalidHandler: function(form, validator) {

            if (!validator.numberOfInvalids())
                return;

            $('html, body').animate({
                scrollTop: $(validator.errorList[0].element).offset().top + (-200)
            }, 200);

        },
        rules: {
            job_title: {
                required: true
            },
            company_id: {
                required: true
            },
            job_category: {
                required: true
            },
            rank: {
                required: true
            },
            ship_type: {
                required: true
            },
            ship_detail_id: {
                required: true
            },
            valid_from: {
                required: true
            },
            valid_to: {
                required: true
            },
            /*date_of_joining: {
                required: true
            },*/
            nationality: {
                required: true
            },
            min_rank_exp: {
                required: true
            },
            grt: {
                required: true,
                number: true
            },
            bhp: {
                required: true,
                number: true
            },
            engine_type: {
                required: true
            },
            ship_flag: {
                required: true
            },
            wages_offered: {
                number: true
            }
        }
    });

    $('.submitCompanyJobsButton').on('click', function(e){
        e.preventDefault();
        var reload = false;
        if($(this).hasClass('data-add-more')){
            reload = true;
        }
        var l = Ladda.create(this);
        if($('#add-job-form').valid()){
            l.start();
            $.ajax({
                type: "POST",
                url: $("#add-job-form").attr('action'),
                data: $("#add-job-form").serialize(),
                statusCode: {
                    200:function (response) {
                        if(reload){
                            window.location.href = $("#api-site-add-job").val();
                        }else{
                            window.location.href = response.redirect_url;
                            l.stop();
                        }
                        
                    },
                    400:function (response) {
                        l.stop();
                    }

                }
            });
        }

    })

    $('.jobDisableButton').on('click', function(e){
            e.preventDefault();
            var job_id = ($(this).data('id'));
            var status = ($(this).data('status'));

            if(status == 'enable'){
                url = './enable/'+job_id;
                $(".disable-"+job_id).removeClass('hide');
                $(".enable-"+job_id).addClass('hide');
            }else{
                url = './disable/'+job_id;
                $(".enable-"+job_id).removeClass('hide');
                $(".disable-"+job_id).addClass('hide');
            }
            $.ajax({
                type: "POST",
                url: url,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                },
                statusCode: {
                    200:function (response) {
                        toastr.success(response.message, 'Success');
                        //window.location.href = response.redirect_url;
                        //l.stop();
                    },
                    400:function (response) {
                        //l.stop();
                    }

                }
            });

    });

    $('.ship_type_change_name').on('change', function(e){
        e.preventDefault();
        var ship_type_id = $(this).val();

        var temp_url =$("#api-site-company-data-ship-name").val();
        if(temp_url){
            var temp_arr = temp_url.split('%');
            var url = temp_arr[0]+ship_type_id;
        }
        else{
            temp_url =$("#api-admin-company-data-ship-name").val();
            var company_id = $("#company_id").val();
            var temp_arr = temp_url.split('%');
            var url = temp_arr[0]+ship_type_id+'/'+company_id;
        }

        $.ajax({
            type: "POST",
            url: url,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            },
            statusCode: {
                200:function (response) {
                    var options;
                    $("select[name='ship_detail_id'] option").remove();
                    $('select[name="ship_detail_id"]').append('<option value=""> Select ship name </option>');
                    if(response.result.length > 0){
                        for (i = 0; i < response.result.length; i++) { 
                            options += '<option value="'+response.result[i].id+'">'+response.result[i].ship_name+'</option>';
                        }
                        $('select[name="ship_detail_id"]').append(options);
                        $('select[name="ship_detail_id"]').select('refresh');
                    }
                    $(".grt").val('');
                    $(".bhp").val('');
                    $(".engine_type").val('');
                },
                400:function (response) {
                    $("select[name='ship_detail_id'] option").remove();
                    $('select[name="ship_detail_id"]').append('<option value=""> Select ship name </option>');
                    $('select[name="ship_detail_id"]').select('refresh');
                    $(".grt").val('');
                    $(".bhp").val('');
                    $(".engine_type").val('');
                }
            }
        });
    });

     $('.ship_name_change').on('change', function(e){
            e.preventDefault();
            var id = $(this).val();

            var temp_url = $("#api-site-company-data-ships-by-ship-type").val();
            if(temp_url){
                var temp_arr = temp_url.split('%');
                var url = temp_arr[0]+id;
            }else{
                temp_url =$("#api-admin-company-data-ships-by-ship-type").val();
                var temp_arr = temp_url.split('%');
                var url = temp_arr[0]+id;
            }
            
            $.ajax({
                type: "POST",
                url: url,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                },
                statusCode: {
                    200:function (response) {
                        
                        if(response.ship_details){
                            if(response.ship_details.grt.length > 0){
                                $(".grt").val(response.ship_details.grt);
                            }
                            if(response.ship_details.bhp.length > 0){
                                $(".bhp").val(response.ship_details.bhp);
                            }
                            if(response.ship_details.ship_flag.length > 0){
                                $(".ship_flag").val(response.ship_details.ship_flag);
                            }
                            if(typeof(response.ship_details.engine_type) != 'object' && response.ship_details.engine_type.length > 0){
                                $(".engine_type").val(response.ship_details.engine_type);
                            }
                            if(response.ship_details.ship_type.length > 0){
                                $("#ship_type").val(response.ship_details.ship_type);
                                $(".ship_type").val(response.ship_details.ship_type);
                            }
                        }
                    },
                    400:function (response) {
                        $("#grt").val('');
                        $("#bhp").val('');
                        $(".engine_type").val('');
                        $(".ship_type").val('');
                        $("#ship_type").val('');
                        $(".ship_flag").val('');
                        $("#ship_flag").val('');
                    },
                    404:function (response) {
                        $("#grt").val('');
                        $("#bhp").val('');
                        $(".engine_type").val('');
                        $(".ship_type").val('');
                        $("#ship_type").val('');
                        $(".ship_flag").val('');
                        $("#ship_flag").val('');
                    }

                }
            });
    });

    $('.ship_type_change').on('change', function(e){
            e.preventDefault();
            var ship_id = $(this).val();

            var temp_url =$("#api-site-company-data-ships").val();
            if(temp_url){
                var temp_arr = temp_url.split('%');
                var url = temp_arr[0]+ship_id;
            }else{
                temp_url =$("#api-admin-company-data-ships").val();
                var temp_arr = temp_url.split('%');
                var company_id = $('#company_id').val();
                var url = temp_arr[0]+company_id+'/'+ship_id;
            }
            

            $.ajax({
                type: "POST",
                url: url,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                },
                statusCode: {
                    200:function (response) {
                        if(response.ship_details){
                            if(response.ship_details.grt.length > 0){
                                $("#grt").val(response.ship_details.grt);
                            }
                            if(response.ship_details.bhp.length > 0){
                                $("#bhp").val(response.ship_details.bhp);
                            }
                            if(response.ship_details.engine_type.length > 0){
                                $(".engine_type").val(response.ship_details.engine_type);
                            }
                        }
                    },
                    400:function (response) {
                        $("#grt").val('');
                        $("#bhp").val('');
                        $(".engine_type").val('');
                    },
                    404:function (response) {
                        $("#grt").val('');
                        $("#bhp").val('');
                        $(".engine_type").val('');
                    }

                }
            });
    });

    $('#company_id').on('change', function(e){
        var company_id = $(this).val();
        var temp_url = $("#api-admin-get-company-ships").val();
        var temp_arr = temp_url.split('%');
        var url = temp_arr[0]+company_id;

        $.ajax({
            type: "POST",
            url: url,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            },
            statusCode: {
                200:function (response) {
                    $("select[name='ship_detail_id'] option").remove();
                    $("select[name='ship_type'] option").remove();
                    $('select[name="ship_detail_id"]').append('<option value=""> Select Ship Name </option>');
                    $('select[name="ship_type"]').append('<option value=""> Select Ship Type </option>');
                    if(response.options.length > 0){
                        $.each( response.options, function( key, value ) {
                            $('select[name="ship_type"]').append(value);
                        });
                        $('select[name="ship_type"]').select('refresh');
                    }
                    if(response.ship_names.length > 0){
                        $.each( response.ship_names, function( key, value ) {
                            $('select[name="ship_detail_id"]').append(value);
                        });
                        $('select[name="ship_detail_id"]').select('refresh');
                    }
                },
                400:function (response) {
                    $("select[name='ship_detail_id'] option").remove();
                    $("select[name='ship_type'] option").remove();
                    $('select[name="ship_detail_id"]').append('<option value=""> Select Ship Name </option>');
                    $('select[name="ship_type"]').append('<option value=""> Select Ship Type </option>');
                },
                422:function (response) {

                }
            }
        });

   });

    $(document).on('change', '.country-search', function () {
        
        if($(this).val() == '95') {
           $('.fields-for-india').removeClass('hide');
           $('.fields-not-for-india').addClass('hide');

           $('.fields-for-india').removeClass('hide');
           $('.fields-not-for-india').addClass('hide');

            var temp_url = $("#api-site-state-list-by-country-id").val();
            var url_arr = temp_url.split("%");
            var url = url_arr[0] + parseInt($(this).val());

            $.ajax({
                type: "get",
                url: url,
                statusCode: {
                    200:function (response) {
                        $("#state option").remove();
                        $("#state").append('<option value="">Select State</option>');
                        $.each(response,function(index,value){
                            $("#state").append('<option value='+value.id+'>'+value.name+'</option>');
                        }); 
                    },
                    400:function (response) {
                       
                    },
                    422:function (response) {

                    }
                }
            });

        } else {
           $('.fields-for-india').addClass('hide');
           $('.fields-not-for-india').removeClass('hide');

           $('.fields-for-india').addClass('hide');
           $('.fields-not-for-india').removeClass('hide');
       }
    });

});
//# sourceMappingURL=job.js.map
